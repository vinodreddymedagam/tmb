gblAccntTypeIB = "";
globalAccountDreamID = "";
var gblAccNo = "";
var gblDreamNickname = "";
var gbldreamtargetID = "";
var linkedAccType = "";
//var gblAccountFromforRexIBFor = "";
var gblAccountFromforRexIB = "";
var gblFourteenDigAccountIB = "";
var gblDreamflag = "false";
var DreamNicknameUniq = "";
var gblpersonalizedId = "";
var gblshowAccountBox = false;
var gblAccountSummaryFlag = "false";
//FromProductId = "";
gblPersonalizedRecordIdFromDreamSavingsIB = "";

function clearDreamDataMaint() {


    frmIBDreamSavingMaintenance.lblAccountFrom.text = "";
    frmIBDreamSavingMaintenance.lblAccountNicknameValue.text = "";
    frmIBDreamSavingMaintenance.lblAcntNoValue.text = "";
    frmIBDreamSavingMaintenance.lblBranchVal.text = "";
    frmIBDreamSavingMaintenance.lblDreamDesc.text = "";
    frmIBDreamSavingMaintenance.lblInterestRateVal.text = "";
    frmIBDreamSavingMaintenance.lblMnthlySavingAmntVal.text = "";
    frmIBDreamSavingMaintenance.lblOpeningDateVal.text = "";
    frmIBDreamSavingMaintenance.lblTargetAmount.text = "";
    frmIBDreamSavingMaintenance.lblTranfrEveryMnthVal.text = "";
    frmIBDreamSavingMaintenance.label866795318717757.text = "";
    if (gblAccountSummaryFlag == "true") {

    } else {
        DreamcalldepositAccountInqDMSIB();
    }

}

/*function DreamActNickNameUniq(uniqText) {
    var dreamActNum = frmIBDreamSavingMaintenance.lblAcntNoValue.text;

    for (i = 0; i < DreamNicknameUniq["custAcctRec"].length; i++) {
        if ((DreamNicknameUniq.custAcctRec[i].acctNickName) != null && (DreamNicknameUniq.custAcctRec[i].acctNickName) != "") {
            //
            //

            if (dreamActNum != DreamNicknameUniq.custAcctRec[i].accountNoFomatted) {
                if (uniqText.toLowerCase() == DreamNicknameUniq["custAcctRec"][i]["acctNickName"].toLowerCase()) {
                    return false;
                }
            }

        }
    }
}*/

/*function DreamActAccountNameUniq(uniqText) {


    for (i = 0; i < DreamNicknameUniq["custAcctRec"].length; i++) {
        accountIdNAme = DreamNicknameUniq["custAcctRec"][i]["accId"];
        accountIdNAme = accountIdNAme.substr(accountIdNAme.length - 4)
        if ((DreamNicknameUniq.custAcctRec[i].ProductNameEng) != null && (DreamNicknameUniq.custAcctRec[i].ProductNameEng) != "") {
            
            var uniqText = uniqText.replace(/ /g, "");
            var AccountNamecheck = DreamNicknameUniq["custAcctRec"][i]["ProductNameEng"].replace(/ /g, "");
            if (uniqText.toLowerCase() == AccountNamecheck.toLowerCase().trim() + accountIdNAme) {
                return false;
            }
        }
    }
}*/

function otptest(text) {
    var pat1 = /^0|[1-9]\d*$/;
    //var pat2 = /[0-9]/g
    var isAlpha = pat1.test(text);
    //var isNum = pat2.test(text);

    
    if (isAlpha == false) {
        return false;
    }

    return true;
}

function validationForDSMIB() {

    var nickNameTxt = frmIBDreamSAvingEdit.lblNicknameVal.text;
    var myDreamTargetAMnt = frmIBDreamSAvingEdit.lblTargetAmntVal.text;
    var DreamDescText = frmIBDreamSAvingEdit.lblDreamDescVal.text;
    myDreamTargetAMnt = myDreamTargetAMnt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    myDreamTargetAMnt = myDreamTargetAMnt.replace(/,/g, "");
    var MnthlySAvingAmnt = frmIBDreamSAvingEdit.lblmnthsaveval.text;

    MnthlySAvingAmnt = MnthlySAvingAmnt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(kony.i18n.getLocalizedString("keyCalendarMonth"), "").replace("/", "");
    
    MnthlySAvingAmnt = MnthlySAvingAmnt.replace(/,/g, "");
    var num = new Number(myDreamTargetAMnt)
    var num1 = new Number(MnthlySAvingAmnt)

    var DreamDesc = frmIBDreamSAvingEdit.lbldreamValue.text;
    // var transfer = frmIBDreamSAvingEdit.combobox86679531842314.selectedKeyValue[0][1];

    if (DreamDesc == null || DreamDesc == "") {
        showAlertIB(kony.i18n.getLocalizedString("keySelectDream"), kony.i18n.getLocalizedString("info"))
        return false;
    }
    if (frmIBDreamSAvingEdit.combobox86679531842314.selectedKey == "transfer"|| frmIBDreamSAvingEdit.combobox86679531842314.selectedKey == null) {
        showAlertIB(kony.i18n.getLocalizedString("keypleaseSelecttransfer"), kony.i18n.getLocalizedString("info"))
        return false;
    }
    if (DreamDescText != null && DreamDescText != "") {
        /*
    	 *To allow only alphanumeric values, onkeyUp event is added on dream description 
   		/* if (!(benNameAlpNumValidation(DreamDescText))) {
     	   	alert("Dream Description is wrong or Should not  Empty");
        	frmIBDreamSAvingEdit.lblDreamDescVal.skin = "txtErrorBG";
        	return false;
   		 }
   		 */
    }

    if (nickNameTxt == null || nickNameTxt == "") {
        showAlertIB(kony.i18n.getLocalizedString("keydreaminputNickname"), kony.i18n.getLocalizedString("info"))
        frmIBDreamSAvingEdit.lblNicknameVal.skin = "txtErrorBG";
        return false;
    }
 /*   var nickName = DreamActNickNameUniq(frmIBDreamSAvingEdit.lblNicknameVal.text);
    accntName = DreamActAccountNameUniq(frmIBDreamSAvingEdit.lblNicknameVal.text);
    if (nickName == false || accntName == false) {
        showAlertIB(kony.i18n.getLocalizedString("keyDreamUniqueName"), kony.i18n.getLocalizedString("info"));
        frmIBDreamSAvingEdit.lblNicknameVal.skin = "txtErrorBG";
        return false
    }*/

    if (!NickNameValid(nickNameTxt)) {
        showAlertIB(kony.i18n.getLocalizedString("keydreamaccountnickname"), kony.i18n.getLocalizedString("info"));
        frmIBDreamSAvingEdit.lblNicknameVal.skin = "txtErrorBG";
        return false;
    }


    if (myDreamTargetAMnt == null || myDreamTargetAMnt == "") {
        showAlertIB(kony.i18n.getLocalizedString("keyTargetAmt"), kony.i18n.getLocalizedString("info"));
        frmIBDreamSAvingEdit.lblTargetAmntVal.skin = "txtErrorBG";
        return false;
    }
    if (!kony.string.isNumeric(myDreamTargetAMnt)) {
        showAlertIB(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), kony.i18n.getLocalizedString("info"));
        frmIBDreamSAvingEdit.lblTargetAmntVal.skin = "txtErrorBG";
        return false;
    }
    if (!kony.string.isNumeric(MnthlySAvingAmnt)) {
        showAlertIB(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), kony.i18n.getLocalizedString("info"));
        // showAlertIB(kony.i18n.getLocalizedString("keydreamIncorrectMnthlySavingAmnt "), kony.i18n.getLocalizedString("info"));
        frmIBDreamSAvingEdit.lblmnthsaveval.skin = "txtErrorBG";
        return false;
    }
    if (MnthlySAvingAmnt == null || MnthlySAvingAmnt == "") {
        showAlertIB(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), kony.i18n.getLocalizedString("info"));
        //   showAlertIB(kony.i18n.getLocalizedString("keydreaminputMonthlySavingAmnt "), kony.i18n.getLocalizedString("info"));
        frmIBDreamSAvingEdit.lblmnthsaveval.skin = "txtErrorBG";
        return false;
    }


    if ((kony.os.toNumber(myDreamTargetAMnt)) < (kony.os.toNumber(gblMinDreamAmt))) {
        //showAlert("Please enter minimum target amount to proceed.", kony.i18n.getLocalizedString("info"));
        showAlertIB("Target amount value should be not be less than the minimum deposit amount configured for the Dream Savings account ", kony.i18n.getLocalizedString("info"));
        frmIBDreamSAvingEdit.lblTargetAmntVal.skin = txtErrorBG;
        return false
    }
    if ((kony.os.toNumber(MnthlySAvingAmnt)) < (kony.os.toNumber(gblMinDreamAmt))) {
        showAlert(kony.i18n.getLocalizedString("keydreamminimumAmount"), kony.i18n.getLocalizedString("info"));
        frmIBDreamSAvingEdit.lblmnthsaveval.skin = txtErrorBG;
        return false
    }

    if (gblMaxDreamAmt != "No Limit") {
        if ((kony.os.toNumber(MnthlySAvingAmnt)) > (kony.os.toNumber(gblMaxDreamAmt))) {
            showAlert(kony.i18n.getLocalizedString("keydreamminimumAmount"), kony.i18n.getLocalizedString("info"));
            frmIBDreamSAvingEdit.lblmnthsaveval.skin = txtErrorBG;
            return false
        }

    }
    if (num1 > num) {
        showAlert(kony.i18n.getLocalizedString("keyMonthlyLessTarget"), kony.i18n.getLocalizedString("info"));
        return false;
    }


    gblOTPDream = "true";
    frmIBDreamSAvingEdit.txtBxOTP.text = "";
	
	//saveAmountinSession();
	checkeditdreamBeforeSavingsIB();
  /*  if (gblSwitchToken == false && gblTokenSwitchFlag == false) {
        IBDScheckTokenFlag();
    } else if (gblSwitchToken == false && gblTokenSwitchFlag == true) {
        showingDSToken();
        frmIBDreamSAvingEdit.hbxToken.setVisibility(true);
        frmIBDreamSAvingEdit.hbxOTPEntry.setVisibility(false);
    } else if (gblSwitchToken == true) {

        callOTPServiceDSMIB();
    }
    frmIBDreamSAvingEdit.txtBxOTP.setFocus(true);
    frmIBDreamSAvingEdit.tbxToken.setFocus(true);
    */
}
function checkeditdreamBeforeSavingsIB() {
	var inputParam = [];
	showLoadingScreenPopup();
	invokeServiceSecureAsync("tokenSwitching", inputParam, checkeditdreamSavingsIBCallback);
}

function checkeditdreamSavingsIBCallback(status,resulttable){
	 if (status == 400) {
		 if(resulttable["opstatus"] == 0){
			 saveAmountinSession();
			 dismissLoadingScreenPopup();
		 }else{
			 dismissLoadingScreenPopup();
			 alert(kony.i18n.getLocalizedString("keyErrResponseOne"));
			}
		 }
}
function IBDScheckTokenFlag() {
    var inputParam = [];
    inputParam["crmId"] = gblcrmId;
    showLoadingScreenPopup();
    invokeServiceSecureAsync("tokenSwitching", inputParam, IBDSibTokenFlagCallbackfunction);
}

function IBDSibTokenFlagCallbackfunction(status, callbackResponse) {
    if (status == 400) {
        if (callbackResponse["opstatus"] == 0) {
            
            if (callbackResponse["deviceFlag"].length == 0) {
                
                frmIBDreamSAvingEdit.hbxToken.setVisibility(false);
                frmIBDreamSAvingEdit.hbxOTPEntry.setVisibility(true);
                frmIBDreamSAvingEdit.lblOTPMsgReq.text=kony.i18n.getLocalizedString("keyotpmsgreq");
                dismissLoadingScreenPopup()
                callOTPServiceDSMIB();
                //return
            }
            if (callbackResponse["deviceFlag"].length > 0) {
                var tokenFlag = callbackResponse["deviceFlag"][0]["TOKEN_DEVICE_FLAG"];
                var mediaPreference = callbackResponse["deviceFlag"][0]["MEDIA_PREFERENCE"];

                if (tokenFlag == "Y" && mediaPreference == "Token") {
                    gblTokenSwitchFlag = true;
                    showingDSToken();
                    frmIBDreamSAvingEdit.hbxToken.setVisibility(true);
                    frmIBDreamSAvingEdit.hbxOTPEntry.setVisibility(false);
                      dismissLoadingScreenPopup()
                    frmIBDreamSAvingEdit.tbxToken.setFocus(true);
                } else {
                    frmIBDreamSAvingEdit.hbxToken.setVisibility(false);
                    frmIBDreamSAvingEdit.hbxOTPEntry.setVisibility(true);
                    frmIBDreamSAvingEdit.lblOTPMsgReq.text=kony.i18n.getLocalizedString("keyotpmsgreq");
                    gblTokenSwitchFlag = false;
                    callOTPServiceDSMIB();
                      dismissLoadingScreenPopup()
                    frmIBDreamSAvingEdit.txtBxOTP.setFocus(true);
                }
                
            } else {

            }
        } else {
              dismissLoadingScreenPopup()
        }
    }

}



function showingDSToken() {
    frmIBDreamSAvingEdit.hbxOTPBankRef.isVisible = false;
    frmIBDreamSAvingEdit.hbxOTPsnt.isVisible = false;
    frmIBDreamSAvingEdit.lblOTPMsgReq.isVisible = true;
    frmIBDreamSAvingEdit.lblOTPMsgReq.text=kony.i18n.getLocalizedString("Receipent_tokenId");
    frmIBDreamSAvingEdit.hbxToken.isVisible = true;
    frmIBDreamSAvingEdit.hbxotp.isVisible = true;
    frmIBDreamSAvingEdit.hbxcancel.isVisible = false;
    frmIBDreamSAvingEdit.hbxcancelOTP.isVisible = true;
}

function validationForDSMbtnIB() {
    frmIBDreamSAvingEdit.lbltargetamntstudio21.text = kony.i18n.getLocalizedString("keyMbTargetAmnt");//Modified by Studio Viz
    frmIBDreamSAvingEdit.lblMnthlySavingVal.skin = "txtIB20pxBlack";
    var nickNameTxt = frmIBDreamSAvingEdit.lblNicknameVal.text;
    var myDreamTargetAMnt = frmIBDreamSAvingEdit.lblTargetAmntVal.text;
    myDreamTargetAMnt = myDreamTargetAMnt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    myDreamTargetAMnt = myDreamTargetAMnt.replace(/,/g, "");
    MnthlySAvingAmnt = frmIBDreamSAvingEdit.lblmnthsaveval.text;

    MnthlySAvingAmnt = MnthlySAvingAmnt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace("/", "").replace(kony.i18n.getLocalizedString("keyCalendarMonth"), "");
      MnthlySAvingAmnt = MnthlySAvingAmnt.replace(/,/g, "");
    var num = new Number(myDreamTargetAMnt)
    var num1 = new Number(MnthlySAvingAmnt)
    var DreamDesctextbtn = frmIBDreamSAvingEdit.lblDreamDescVal.text;
    var DreamDesc = frmIBDreamSAvingEdit.lbldreamValue.text;
    // var transfer = frmIBDreamSAvingEdit.combobox86679531842314.selectedKeyValue[0][1];
    if (DreamDesc == null || DreamDesc == "") {
        showAlertIB(kony.i18n.getLocalizedString("keySelectDream"), kony.i18n.getLocalizedString("info"))
        return false;
    }
    if ((kony.os.toNumber(myDreamTargetAMnt)) < (kony.os.toNumber(gblMinDreamAmt))) {
        //showAlert("Please enter minimum target amount to proceed.", kony.i18n.getLocalizedString("info"));
        showAlert(kony.i18n.getLocalizedString("keyLessTargetAmt"), kony.i18n.getLocalizedString("info"));
        frmIBDreamSAvingEdit.lblTargetAmntVal.skin = txtErrorBG;
        return false
    }
    if ((kony.os.toNumber(MnthlySAvingAmnt)) < (kony.os.toNumber(gblMinDreamAmt))) {
        showAlert(kony.i18n.getLocalizedString("keydreamminimumAmount"), kony.i18n.getLocalizedString("info"));
        frmIBDreamSAvingEdit.lblSetSavingVal.skin = txtErrorBG;
        return false
    }

    if (gblMaxDreamAmt != "No Limit") {
        if ((kony.os.toNumber(MnthlySAvingAmnt)) > (kony.os.toNumber(gblMaxDreamAmt))) {
            showAlert(kony.i18n.getLocalizedString("keydreamminimumAmount"), kony.i18n.getLocalizedString("info"));
            frmIBDreamSAvingEdit.lblSetSavingVal.skin = txtErrorBG;
            return false
        }

    }
    /* if (DreamDesctextbtn == null || DreamDesctextbtn == "" ) {
         showAlertIB(kony.i18n.getLocalizedString("keydreaminputDreamDesc"), kony.i18n.getLocalizedString("info"))
        frmIBDreamSAvingEdit.lblDreamDescVal.skin = "txtErrorBG";
        return false;
    }*/

    if (nickNameTxt == null || nickNameTxt == "") {
        showAlertIB(kony.i18n.getLocalizedString("keydreaminputNickname"), kony.i18n.getLocalizedString("info"))
        frmIBDreamSAvingEdit.lblNicknameVal.skin = "txtErrorBG";
        return false;
    }
    if (!(benNameAlpNumValidation(nickNameTxt))) {
        showAlertIB(kony.i18n.getLocalizedString("keydreamaccountnickname"), kony.i18n.getLocalizedString("info"));
        frmIBDreamSAvingEdit.lblNicknameVal.skin = "txtErrorBG";
        return false;
    }

    if (myDreamTargetAMnt == null || myDreamTargetAMnt == "") {
        showAlertIB(kony.i18n.getLocalizedString("keyTargetAmt"), kony.i18n.getLocalizedString("info"));
        frmIBDreamSAvingEdit.lblTargetAmntVal.skin = "txtErrorBG";
        return false;
    }
    if (!kony.string.isNumeric(myDreamTargetAMnt)) {
        showAlertIB(kony.i18n.getLocalizedString("keyincorrectTargetAmount"), kony.i18n.getLocalizedString("info"));
        frmIBDreamSAvingEdit.lblTargetAmntVal.skin = "txtErrorBG";
        return false;
    }
    if (!kony.string.isNumeric(MnthlySAvingAmnt)) {
        showAlertIB(kony.i18n.getLocalizedString("keydreamIncorrectMnthlySavingAmnt"), kony.i18n.getLocalizedString("info"));
        frmIBDreamSAvingEdit.lblmnthsaveval.skin = "txtErrorBG";
        return false;
    }
    if (MnthlySAvingAmnt == null || MnthlySAvingAmnt == "") {
        showAlertIB(kony.i18n.getLocalizedString("keydreaminputMonthlySavingAmnt"), kony.i18n.getLocalizedString("info"));
        frmIBDreamSAvingEdit.lblmnthsaveval.skin = "txtErrorBG";
        return false;
    }
    if ((kony.os.toNumber(myDreamTargetAMnt)) < (kony.os.toNumber(gblMinDreamAmt))) {
        //showAlert("Please enter minimum target amount to proceed.", kony.i18n.getLocalizedString("info"));
        showAlertIB("Target amount value should be not be less than the minimum deposit amount configured for the Dream Savings account ", kony.i18n.getLocalizedString("info"));
        frmIBDreamSAvingEdit.lblTargetAmntVal.skin = txtErrorBG;
        return false
    }
    if ((kony.os.toNumber(MnthlySAvingAmnt)) < (kony.os.toNumber(gblMinDreamAmt))) {
        showAlert(kony.i18n.getLocalizedString("keydreamminimumAmount"), kony.i18n.getLocalizedString("info"));
        frmIBDreamSAvingEdit.lblmnthsaveval.skin = txtErrorBG;
        return false
    }

    if (gblMaxDreamAmt != "No Limit") {
        if ((kony.os.toNumber(MnthlySAvingAmnt)) > (kony.os.toNumber(gblMaxDreamAmt))) {
            showAlert(kony.i18n.getLocalizedString("keydreamminimumAmount"), kony.i18n.getLocalizedString("info"));
            frmIBDreamSAvingEdit.lblmnthsaveval.skin = txtErrorBG;
            return false
        }

    }

    if (num1 > num) {
        showAlertIB(kony.i18n.getLocalizedString("keyDreamTargetAlert"), kony.i18n.getLocalizedString("info"));
        return false;
    }

    

    var formatedAccount = myDreamTargetAMnt
    frmIBDreamSAvingEdit.lblTargetAmount.text = commaFormattedOpenAct(formatedAccount) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
    frmIBDreamSAvingEdit.lblMnthlySavingVal.text = commaFormattedOpenAct(MnthlySAvingAmnt) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
    myTargetAmnt = frmIBDreamSAvingEdit.lblTargetAmount.text
    myTargetAmnt = myTargetAmnt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    //alert(myTargetAmnt);

    calculatebtn();
    frmIBDreamSAvingEdit.hbxcalculator.isVisible = true;
    frmIBDreamSAvingEdit.hbxAccounts.isVisible = false;
    frmIBDreamSAvingEdit.imgdreamarrw.isVisible = false;
    frmIBDreamSAvingEdit.hbxotp.isVisible = false;
    frmIBDreamSAvingEdit.imgcal.isVisible = true;
    frmIBDreamSAvingEdit.hbxcancel.isVisible = true;
    frmIBDreamSAvingEdit.hbxcancelOTP.isVisible = false;

}

function checkSymbolconfirm() {
   
    var TotamntMnth = frmIBDreamSAvingEdit.lblTargetAmount.text;
    var Totamnt = TotamntMnth.replace(/,/g, "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    var TotamntMonths = frmIBDreamSAvingEdit.lblMnthlySavingVal.text;
     TotamntMonths = TotamntMonths.replace(/,/g, "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    noOfMnths = frmIBDreamSAvingEdit.lblmnthTodr.text;
  
    if (noOfMnths != null && noOfMnths !="" ) {

      months =noOfMnths.replace(kony.i18n.getLocalizedString("keymonths"), "");
   }
    if (months == "" || months == null) {
        frmIBDreamSAvingEdit.lblMnthlySavingVal.text = "";
    } else {
        var MnthlySavAMnt1 = (Totamnt / months);
        MnthlySavAMnt = MnthlySavAMnt1.toFixed(2);

       frmIBDreamSAvingEdit.lblmnthsaveval.text = commaFormattedOpenAct(MnthlySavAMnt) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");


    }
    
  //  frmIBDreamSAvingEdit.lblmnthsaveval.text = commaFormattedOpenAct(MnthlySavAMnt) +"/"+ kony.i18n.getLocalizedString("keyCalendarMonth");
    var myDreamTargetAMnt = frmIBDreamSAvingEdit.lblTargetAmount.text;
    myDreamTargetAMnt = myDreamTargetAMnt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    myDreamTargetAMnt = myDreamTargetAMnt.replace(",", "");
    MnthlySAvingAmnt = frmIBDreamSAvingEdit.lblMnthlySavingVal.text;
    MnthlySAvingAmnt = MnthlySAvingAmnt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    MnthlySAvingAmnt = MnthlySAvingAmnt.replace(",", "");
    var num = new Number(myDreamTargetAMnt)
    var num1 = new Number(MnthlySAvingAmnt)
    var noOfMnths = frmIBDreamSAvingEdit.lblmnthTodr.text;
    
    if (noOfMnths == "1") {

        noOfMnths = noOfMnths.replace(kony.i18n.getLocalizedString("keymonths"), "")
    } else {

        noOfMnths = noOfMnths.replace(kony.i18n.getLocalizedString("keymonths"), "")
    }
    
    if ((kony.os.toNumber(MnthlySAvingAmnt)) < (kony.os.toNumber(gblMinDreamAmt))) {
        showAlert(kony.i18n.getLocalizedString("keydreamminimumAmount"), kony.i18n.getLocalizedString("info"));
        frmIBDreamSAvingEdit.lblMnthlySavingVal.skin = txtErrorBG;
        return false
    }

    if (gblMaxDreamAmt != "No Limit") {
        if ((kony.os.toNumber(MnthlySAvingAmnt)) > (kony.os.toNumber(gblMaxDreamAmt))) {
            showAlert(kony.i18n.getLocalizedString("keydreamminimumAmount"), kony.i18n.getLocalizedString("info"));
            frmIBDreamSAvingEdit.lblMnthlySavingVal.skin = txtErrorBG;
            return false
        }

    }
    if (num1 > num) {
        showAlertIB(kony.i18n.getLocalizedString("keyDreamTargetAlert"), kony.i18n.getLocalizedString("info"));
        return false;
    }
    var indexMnt = noOfMnths.indexOf(".");
    if (indexMnt > 0 || kony.string.isNumeric(noOfMnths) == false) {
        showAlertIB(kony.i18n.getLocalizedString("keydreamMnthlySavingANdMnthToDream"), kony.i18n.getLocalizedString("info"));
        return false;
    }
    MnthlySAvingAmnt = MnthlySAvingAmnt.trim();
    //alert(MnthlySAvingAmnt)
    if (MnthlySAvingAmnt == null || MnthlySAvingAmnt == "") {
        showAlertIB(kony.i18n.getLocalizedString("keydreamMnthlySavingANdMnthToDream"), kony.i18n.getLocalizedString("info"));
        frmIBDreamSAvingEdit.lblMnthlySavingVal.skin = "txtErrorBG";
        return false;
    }
    if (!kony.string.isNumeric(MnthlySAvingAmnt)) {
        showAlertIB(kony.i18n.getLocalizedString("keydreamIncorrectMnthlySavingAmnt"), kony.i18n.getLocalizedString("info"));
        frmIBDreamSAvingEdit.lblMnthlySavingVal.skin = "txtErrorBG";
        return false;
    }
    frmIBDreamSAvingEdit.hbxcalculator.isVisible = false;
    frmIBDreamSAvingEdit.hbxAccounts.isVisible = true;
    frmIBDreamSAvingEdit.imgdreamarrw.isVisible = true;
    frmIBDreamSAvingEdit.imgcal.isVisible = true;
    frmIBDreamSAvingEdit.lblMnthlySavingVal.text = "";
    frmIBDreamSAvingEdit.lblmnthTodr.text = "";
 
    //frmIBDreamSavingMaintenance.lblMnthlySavingAmntVal.text=frmIBDreamSAvingEdit.lblMnthlySavingVal.text;

    //  frmIBDreamSAvingEdit.lblmnthsaveval.text = frmIBDreamSAvingEdit.lblMnthlySavingVal.text + "/Months";

    // frmIBDreamSAvingEdit.vboxBillers.isVisible = false;
}

function checkSymbolIB() {



    var stringstr = frmIBDreamSAvingEdit.lblTargetAmntVal.text;
    if (stringstr == null || stringstr == "") {
        showAlertIB(kony.i18n.getLocalizedString("keyTargetAmt"), kony.i18n.getLocalizedString("info"));
        frmIBDreamSAvingEdit.lblTargetAmntVal.skin = txtErrorBG;
        return false;
    }

    var dotcount = stringstr.split(".");
    var lendot = dotcount.length - 1;
    if (lendot > 1) {
        showAlertIB(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), kony.i18n.getLocalizedString("info"));
        frmIBDreamSAvingEdit.lblTargetAmntVal.skin = txtErrorBG;
        return false;
    }

    // var decimal= dotcout[1];

    var stringstr = stringstr.replace(/,/g, "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    var name = frmIBDreamSAvingEdit.lblTargetAmntVal.text;
    var charExists;
    if (charExists = (name.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht")) >= 0)) {} else {

        frmIBDreamSAvingEdit.lblTargetAmntVal.text = commaFormattedOpenAct(stringstr) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
    }

}

function checkSymbolMonthlyIB() {


   var mnthlysavingSymboledit = frmIBDreamSAvingEdit.lblmnthsaveval.text;
    if (mnthlysavingSymboledit == null || mnthlysavingSymboledit == "") {
        showAlertIB(kony.i18n.getLocalizedString("keydreaminputMonthlySavingAmnt"), kony.i18n.getLocalizedString("info"));
        frmIBDreamSAvingEdit.lblmnthsaveval.skin = txtErrorBG;
        return false;
    }
    if (mnthlysavingSymboledit.indexOf("0") == 0) {
        showAlert(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), kony.i18n.getLocalizedString("info"));
        frmIBDreamSAvingEdit.lblmnthsaveval.skin = txtErrorBG;
        return false
    }
    var mnthlysavingSymboldotcount = mnthlysavingSymboledit.split(".");
    var mnthlysavingSymbollendot = mnthlysavingSymboldotcount.length - 1;
    if (mnthlysavingSymbollendot > 1) {
        showAlertIB(kony.i18n.getLocalizedString("keydreamIncorrectMnthlySavingAmnt"), kony.i18n.getLocalizedString("info"));
        frmIBDreamSAvingEdit.lblmnthsaveval.skin = txtErrorBG;
        return false;
    }
    var mnthlysavingSymbol123 = mnthlysavingSymboledit.replace(/,/g, "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");

    var mnthlysavingSymbol1new = mnthlysavingSymbol123.trim();
    var name = frmIBDreamSAvingEdit.lblmnthsaveval.text;
    var charExists;
    if (charExists = (name.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht") + "/"+ kony.i18n.getLocalizedString("keyCalendarMonth")) >= 0)) {} else {

        frmIBDreamSAvingEdit.lblmnthsaveval.text = commaFormattedOpenAct(mnthlysavingSymbol1new) + kony.i18n.getLocalizedString("currencyThaiBaht") + "/"+kony.i18n.getLocalizedString("keyCalendarMonth")
    }
}

function calMonthlySavingCalculatorIB() {



    if (frmIBDreamSAvingEdit.lblmnthTodr.text == 0) {
        showAlert(kony.i18n.getLocalizedString("keyValidMonths"), kony.i18n.getLocalizedString("info"));
        return false;
    }
    var entAmountDream = frmIBDreamSAvingEdit.lblmnthTodr.text;

    var entAmountindex = entAmountDream.indexOf(".");
    if (entAmountindex > 0 || kony.string.isNumeric(entAmountDream) == false) {
        showAlert(kony.i18n.getLocalizedString("keyValidMonths"), kony.i18n.getLocalizedString("info"));
        return false;

    }



    var TotamntMnth = frmIBDreamSAvingEdit.lblTargetAmount.text;
    var Totamnt = TotamntMnth.replace(/,/g, "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    noOfMnths = frmIBDreamSAvingEdit.lblmnthTodr.text;
    if (noOfMnths != null || noOfMnths != null) {

        months = (noOfMnths.replace(kony.i18n.getLocalizedString("keyCalendarMonth"), ""))||noOfMnths.replace(kony.i18n.getLocalizedString("keymonths"), "");
   }
    if (months == "" || months == null) {
        frmIBDreamSAvingEdit.lblMnthlySavingVal.text = "";
    } else {
        var MnthlySavAMnt1 = (Totamnt / months);
        MnthlySavAMnt = MnthlySavAMnt1.toFixed(2);

        frmIBDreamSAvingEdit.lblMnthlySavingVal.text = commaFormattedOpenAct(MnthlySavAMnt) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");


    }
    


}


function calculateMnthCalculatorIB() {

    // if (frmIBDreamSAvingEdit.lblMnthlySavingVal.text == "" || frmIBDreamSAvingEdit.lblMnthlySavingVal.text == "0") {
    var Totamntnew = frmIBDreamSAvingEdit.lblTargetAmount.text;
    Totamnt = Totamntnew.replace(/,/g, "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "")
    //frmIBOpenNewDreamAcc.txtODTargetAmt.text = commaFormattedOpenAct(Totamnt);
    var MontSvgamntnew = frmIBDreamSAvingEdit.lblMnthlySavingVal.text;
    MontSvgamnt = MontSvgamntnew.replace(/,/g, "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "")
    //  var stringstr = frmIBDreamSAvingEdit.lblMnthlySavingVal.text;
    if (MontSvgamnt.indexOf("0") == 0) {
        showAlert(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), kony.i18n.getLocalizedString("info"));
        frmIBDreamSAvingEdit.lblMnthlySavingVal.skin = txtErrorBG;
        return false
    }
    var dotcount = MontSvgamnt.split(".");
    var lendot = dotcount.length - 1;
    if (lendot > 1) {
        showAlertIB(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), kony.i18n.getLocalizedString("info"));
        frmIBDreamSAvingEdit.lblMnthlySavingVal.skin = txtErrorBG;
        return false;
    }
    var MontSvgamnt = MontSvgamnt.replace(/,/g, "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    if (MontSvgamnt == null || MontSvgamnt == "") {
        //frmDreamSavingEdit.lblTargetAmntVal.text = frmDreamSavingEdit.lblTargetAmntVal.text
    } else {
        var extra = Totamnt;
        extra = extra % 10;
        if (extra != 0) {
            extra = 1;
        }

        noOfMnths = ((Math.ceil(Totamnt / MontSvgamnt) + extra).toString());
        if (noOfMnths == "1") {
           
            frmIBDreamSAvingEdit.lblmnthTodr.text = noOfMnths + " " + kony.i18n.getLocalizedString("keymonths");
        } else {
            
            frmIBDreamSAvingEdit.lblmnthTodr.text = noOfMnths + " " + kony.i18n.getLocalizedString("keymonths");
        }

        frmIBDreamSAvingEdit.lblMnthlySavingVal.text = commaFormattedOpenAct(MontSvgamnt) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
    }

}

function clearDreamDataIB() 
{
    frmIBDreamSAvingEdit.lblNicknameVal.text = "";
    frmIBDreamSAvingEdit.lblTargetAmntVal.text = "";
    frmIBDreamSAvingEdit.lblmnthsaveval.text = "";
    frmIBDreamSAvingEdit.lblDreamDescVal.text = "";

}

function calculatebtn() {


    //frmIBDreamSAvingEdit.lblMnthlySavingVal.text =frmIBDreamSAvingEdit.lblMnthlySavingVal.text+" "+ kony.i18n.getLocalizedString("currencyThaiBaht");

    
    tarAmount = frmIBDreamSAvingEdit.lblTargetAmntVal.text;
    tarAmount = tarAmount.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    
    tarAmount = tarAmount.replace(/,/g, "");
    MnthlySAvingAmnt = MnthlySAvingAmnt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace("/", "").replace( kony.i18n.getLocalizedString("keyCalendarMonth"),"");
    MnthlySAvingAmnt = MnthlySAvingAmnt.replace(/,/g, "");
   entAmount = MnthlySAvingAmnt.trim();
    if (kony.string.isNumeric(entAmount) == false) {
        showAlertIB(kony.i18n.getLocalizedString("keydreamIncorrectMnthlySavingAmnt"), kony.i18n.getLocalizedString("info"));

    }

    var indexdot = entAmount.indexOf(".");
    var decimal = "";
    var remAmt = "";
    
    if (indexdot > 0) {
        decimal = entAmount.substr(indexdot);
        
        if (decimal.length > 3) {
            alert("Enter only 2 decimal values");
            return false;
        }
    }


    var noOfMnths;
    tarAmount = kony.os.toNumber(tarAmount);
    entAmount = kony.os.toNumber(entAmount);
    //alert("entAmount::"+entAmount +"tarAmount ::"+tarAmount)

    if (entAmount > tarAmount) {
        showAlertIB(kony.i18n.getLocalizedString("keyDreamTargetAlert"), kony.i18n.getLocalizedString("info"));
        return false;
    }

    noOfMnths = tarAmount / entAmount;
    
    noOfMnths = Math.ceil(noOfMnths);
    

    //#ifdef android
        noOfMnths = noOfMnths + "";
    //#endif

    
    //if(frmOpenAcDreamSaving.txtODMyDream.text == "" || frmOpenAcDreamSaving.txtODMyDream.text == null){
    if (noOfMnths == "1") {
        frmIBDreamSAvingEdit.lblmnthTodr.text = "";
        frmIBDreamSAvingEdit.lblmnthTodr.text = noOfMnths + " " + kony.i18n.getLocalizedString("keyCalendarMonth");
    } else {
        frmIBDreamSAvingEdit.lblmnthTodr.text = "";
        frmIBDreamSAvingEdit.lblmnthTodr.text = noOfMnths + " " + kony.i18n.getLocalizedString("keymonths");
    }
    gblDreamMnths = noOfMnths;
    //}


}

function callDepositAccntsummmryIB() {

    gblAccountSummaryFlag = "true"
    clearDreamDataMaint();
    var inputparam = {};
    inputparam["acctId"] = gblAccountTable["custAcctRec"][gblIndex]["accId"];
    gblAccNoAcbtSumry = inputparam["acctId"];
    var gblAccNoAcbtSumry12 = gblAccNoAcbtSumry.length
    gblAccNo = gblAccNoAcbtSumry.substr(gblAccNoAcbtSumry12 - 14);
    gblPersonalizedRecordIdFromDreamSavingsIB = gblAccNo;
    inputparam["acctType"] = gblAccountTable["custAcctRec"][gblIndex]["accType"];
    
    
    gblAccntTypeIB = inputparam["acctType"]
    inputparam["spName"] = "com.fnf.xes.ST"
    inputparam["rqUUId"] = "";
    showLoadingScreenPopup();
    invokeServiceSecureAsync("depositAccountInquiry", inputparam, dreamdepositAcctInqCallBackIB);
}

function DreamcalldepositAccountInqDMSIB() {

    var inputparam = {};
    
    inputparam["acctId"] = "" + frmIBMyAccnts.segTMBAccntsList.selectedItems[0].personalizedAcctId;

    gblAccNo = "" + frmIBMyAccnts.segTMBAccntsList.selectedItems[0].personalizedAcctId;
    gblPersonalizedRecordIdFromDreamSavingsIB = gblAccNo;

    inputparam["acctType"] = frmIBMyAccnts.segTMBAccntsList.selectedItems[0].hiddenAcctType;
    gblAccntTypeIB = inputparam["acctType"];

    //inputparam["acctId"] = gblAccountTable["custAcctRec"][gblIndex]["accId"];
    //inputparam["acctType"] = gblAccountTable["custAcctRec"][gblIndex]["accType"];
    inputparam["spName"] = "com.fnf.xes.ST"
    inputparam["rqUUId"] = "";
    invokeServiceSecureAsync("depositAccountInquiry", inputparam, dreamdepositAcctInqCallBackIB);
}

function dreamdepositAcctInqCallBackIB(status, resulttable) {
    if (status == 400) {
        var gblAccountFromforRexIBFor = "";
        if (resulttable["opstatus"] == 0) {

            if (resulttable["AcctBal"].length > 0) {
                
                
                for (var i = 0; i < resulttable["AcctBal"].length; i++) {
                    
                    if (resulttable["AcctBal"][i]["BalType"] == "Avail") {
                        availableBalDream = resulttable["AcctBal"][i]["Amt"];
                        avaliableCurrCode = resulttable["AcctBal"][i]["CurCode"]
                        
                        //    alert("availableBalDream" + availableBalDream);
                        
                    }

                }
            }
            //**** For Opening Date Account Fetch ***************//
            if (resulttable["openingDate"] != null && resulttable["openingDate"] != "") {
                frmIBDreamSavingMaintenance.lblOpeningDateVal.text = resulttable["openingDate"];

            }
            /*  if (resulttable["productId"] != null && resulttable["productId"] != "") {
                fromProductId = resulttable["productId"];
            }*/
            if (resulttable["linkedAccType"] != null && resulttable["linkedAccType"] != ""){
                linkedAccType = resulttable["linkedAccType"];
            }
            if (resulttable["AccountID"] != null && resulttable["AccountID"] != "") {
                globalAccountDreamID = resulttable["AccountID"];
            }

            if (resulttable["linkedacc"] != null && resulttable["linkedacc"] != "") {
                
                gblAccountFromforIB = resulttable["linkedacc"];
                AcountlenforRexIB = gblAccountFromforIB.length;
                
                gblAccountFromforRexIB = gblAccountFromforIB.substr(AcountlenforRexIB - 10);
                
                gblAccountFromforRexIBFor = gblAccountFromforIB.substr(AcountlenforRexIB - 14);
                if (resulttable["linkedAccType"] == "SDA") {
                    gblAccountFromforRexIBFor = gblAccountFromforIB.substr(AcountlenforRexIB - 14);
                    gblAccountFromIB = formatAccountNo(resulttable["linkedacc"].substring(20))
                    frmIBDreamSavingMaintenance.lblAccountFrom.text = gblAccountFromIB;
                } else {
                    gblAccountFromforRexIBFor = gblAccountFromforIB.substr(AcountlenforRexIB - 10);
                    gblAccountFromIB = formatAccountNo(resulttable["linkedacc"].substring(16))
                    frmIBDreamSavingMaintenance.lblAccountFrom.text = gblAccountFromIB;
                }
            } else {
            	gblAccountFromforIB= "";
                showAlertIB(kony.i18n.getLocalizedString("keyLinkAcntNotFound"), kony.i18n.getLocalizedString("info"));
            }
            DreamcallReqInquiryIB();
        } else {}
    }


}

function DreamcallReqInquiryIB() {


    var inputparam = {};
    if (linkedAccType == "SDA") {
        inputparam["spName"] = "com.fnf.xes.ST"; //14digits
		if(gblAccountFromforIB!= null && gblAccountFromforIB !=""){
        gblFourteenDigAccountIB = gblAccountFromforIB.substr(AcountlenforRexIB - 14);
    }else{
    	gblFourteenDigAccountIB= "";
    }
    }
    if (linkedAccType == "DDA") {
        inputparam["spName"] = "com.fnf.xes.IM"; //14digits
		if(gblAccountFromforIB!= null && gblAccountFromforIB !=""){
        gblFourteenDigAccountIB = gblAccountFromforIB.substr(AcountlenforRexIB - 10);
    }else{
    		gblFourteenDigAccountIB= "";
    }
    }
    inputparam["acctId"] = gblAccountFromforRexIB;
    inputparam["acctType"] = linkedAccType;
    
    inputparam["clientDt"] = "";
    inputparam["name"] = "IB-INQ";
    invokeServiceSecureAsync("RecurringFundTransinq", inputparam, DreamReqInquiryCallBackIB);

}

function DreamReqInquiryCallBackIB(status, resulttable) {
    if (status == 400) {
        
        if (resulttable["opstatus"] == 0) {
            
            

            if (resulttable["frequency"] != null && resulttable["frequency"] != "" && resulttable["frequency"] == "EndOfMonth") {

                frmIBDreamSavingMaintenance.lblTranfrEveryMnthVal.text = "31" + "st";
                mnthlyTransferDate = "31";
            }
            if (resulttable["monIntDate"] != null && resulttable["monIntDate"] != "") {
            
            
                mnthlyTransferDate = resulttable["monIntDate"];
                
                var rstmnth="";
                 if(mnthlyTransferDate.charAt("0")=="0"){
                	rstmnth = mnthlyTransferDate.slice(1, 2);
                }else{
               		 rstmnth = mnthlyTransferDate;
                }
                
                
                
                if (mnthlyTransferDate == 1 || mnthlyTransferDate == 21 || mnthlyTransferDate == 31){
                    frmIBDreamSavingMaintenance.lblTranfrEveryMnthVal.text = rstmnth + "st";
                } else if (mnthlyTransferDate == 2 || mnthlyTransferDate == 22) {
                    frmIBDreamSavingMaintenance.lblTranfrEveryMnthVal.text = rstmnth + "nd";
                } else if (mnthlyTransferDate == 3 || mnthlyTransferDate == 23) {
                
               
                    frmIBDreamSavingMaintenance.lblTranfrEveryMnthVal.text = rstmnth + "rd";
                } else {
                    frmIBDreamSavingMaintenance.lblTranfrEveryMnthVal.text = rstmnth + "th";
                }

                
            }

            if (resulttable["monInstAmt"] != null && resulttable["monInstAmt"] != "") {
                
           //     formatedAmount = commaFormattedOpenAct(resulttable["monInstAmt"]);
                frmIBDreamSavingMaintenance.lblMnthlySavingAmntVal.text = commaFormattedOpenAct(resulttable["monInstAmt"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                
            }
            if (resulttable["recXferId"] != null) {
                recXferId = resulttable["recXferId"];
                
            }

            if (resulttable["dueDate"] != null) {
                dueDate = resulttable["dueDate"];
                
            }
            if (resulttable["frmAccId"] != null) {
                recurringFrmAcctIdIB = resulttable["frmAccId"];
                recurringToAcctIdIB = resulttable["toAcctId"];
            }
            recurringFrmAcctIdIB = resulttable["frmAccId"];
            recurringToAcctIdIB = resulttable["toAcctId"];
            recurringFrmAcctTypeIB = linkedAccType;
            recurringToAcctTypeIB = gblAccntTypeIB;
        }

        var inputparam = {};

        var acountDisplay = gblAccNo;
        var Acountlengbl = acountDisplay.length
        x = acountDisplay.substr(Acountlengbl - 14);
        gblAccNo = x;
        gblPersonalizedRecordIdFromDreamSavingsIB = gblAccNo;
        inputparam["PERSONALIZED_ACCT_ID"] = gblAccNo;
        

        invokeServiceSecureAsync("DreamSavingEnquiry", inputparam, DreamInquiryIB);

    } else {

        //  alert(" " + resulttable["errMsg"]);
        // dismissLoadingScreen();
    }

}

function DreamInquiryIB(status, resulttable) {
    if (status == 400) {
        
        if (resulttable["opstatus"] == 0) {
            //dreamCustomerEnquiry();
            
            
            
            var inputparam = {};
            dreamSAvingLength = resulttable["dreamsavingDS"].length;

            
            var result1 = resulttable;
            
            if (resulttable["dreamsavingDS"].length > 0) {
                gbldreamtargetID = resulttable["dreamsavingDS"][0]["DREAM_TARGET_ID"];
                ChannelID12 = resulttable["dreamsavingDS"][0]["CHANNEL_ID"];
                if (resulttable["dreamsavingDS"][0]["PERSONALIZED_ACCT_STATUS"] == "02") {
                    gblshowAccountBox = true;
                    frmIBDreamSavingMaintenance.hbox866795318717754.isVisible = false;
                    frmIBDreamSavingMaintenance.richtext476042680167636.isVisible = true;

                } else {
                    frmIBDreamSavingMaintenance.hbox866795318717754.isVisible = true;
                    frmIBDreamSavingMaintenance.richtext476042680167636.isVisible = false;
                }

                if (ChannelID12 == "01" || ChannelID12 == "02") {

                    if (resulttable["dreamsavingDS"][0]["DREAM_TARGET_AMOUNT"] != null && resulttable["dreamsavingDS"][0]["DREAM_TARGET_AMOUNT"] != "") {
                        taramount = resulttable["dreamsavingDS"][0]["DREAM_TARGET_AMOUNT"];
                        
                        
                        frmIBDreamSavingMaintenance.lblTargetAmount.text = commaFormattedOpenAct(taramount) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                        targetAmoutDream = resulttable["dreamsavingDS"][0]["DREAM_TARGET_AMOUNT"];
                        
                    }
                    if (resulttable["dreamsavingDS"][0]["PERSONALIZED_ID"] != null && resulttable["dreamsavingDS"][0]["PERSONALIZED_ID"] != "") {
                        frmIBDreamSavingMaintenance.lblDreamDesc.text = resulttable["dreamsavingDS"][0]["DREAM_DESC"];
                        
                        gblpersonalizedId = resulttable["dreamsavingDS"][0]["PERSONALIZED_ID"];
                    }
                    DreamcalculatePercentageIB();


                } else {

                    frmIBDreamSavingMaintenance.lblTargetAmount.isVisible = false;
                    frmIBDreamSavingMaintenance.label866795318717734.isVisible = false;
                    frmIBDreamSavingMaintenance.lblTargetAmount.text = gblDreamNickname;
                    percentage = "0";
                    frmIBDreamSavingMaintenance.btnPercentage.text = percentage + "%";
                    frmIBDreamSavingMaintenance.vboxSkin.skin = "samplevbox00";
                    frmIBDreamSavingMaintenance.vboxSkinVal.margin = [1, 55, 1, 1];
                    if (resulttable["dreamsavingDS"][0]["DREAM_DESC"] != null && resulttable["dreamsavingDS"][0]["DREAM_DESC"] != "") {
                        frmIBDreamSavingMaintenance.lblDreamDesc.text = resulttable["dreamsavingDS"][0]["DREAM_DESC"];
                        
                        gblpersonalizedId = resulttable["dreamsavingDS"][0]["PERSONALIZED_ID"];
                    }

                   // showAlertIB(kony.i18n.getLocalizedString("keyLinkAcntNotFound"), kony.i18n.getLocalizedString("info"));
                    DreamgetInterestRateIB()

                }

            } else {

                DreamgetInterestRateIB()
            }


        }
    }
}



function DreamcalculatePercentageConfirmIB() {

    
    var dreamamntConfirm = frmIBDreamSAvingEdit.lblTargetAmntVal.text;
   var  targetconfirm1 = dreamamntConfirm.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    targetconfirm2 = targetconfirm1.replace(/,/g, "");

    //alert(targetconfirm2)
    if (dreamSAvingLength > 0) {
    if(targetAmoutDream!= "" && targetAmoutDream!= "")
        percentageBarbeforeRoundIB = (availableBalDream / targetAmoutDream) * 100;
        else
        	percentageBarbeforeRoundIB = "0";
     
        percentageBar = Math.round(percentageBarbeforeRoundIB);
        
        
        barGraphIB();
    }
}

function DreamcalculatePercentageIB() {
    

    if (dreamSAvingLength > 0) {
    	if(targetAmoutDream!= "" && targetAmoutDream!= "")
        percentageBarbeforeRoundIB = (availableBalDream / targetAmoutDream) * 100;
        else
        	percentageBarbeforeRoundIB = "0";
        percentageBar = Math.round(percentageBarbeforeRoundIB);

        
        DreamgetInterestRateIB();
    } else {
        frmIBDreamSavingMaintenance.imgProd.src = "prod_car_inv.png";
        percentageBar = 10;
        DreamgetInterestRateIB();
    }


}

function DreamgetInterestRateIB() {
    var inputparam = {};
    
    inputparam["productCode"] = "206";
	showLoadingScreenPopup();
    var status = invokeServiceSecureAsync("getInterestRate", inputparam, CustAcntEnqDreamInterestRateIB);
}

function CustAcntEnqDreamInterestRateIB(status, callBackResponse) {

    var interestRateDreamTH = "";
    var interestRateDreamEN = "";
    if (status == 400) {

        
        if (callBackResponse["opstatus"] == 0) {
			if(callBackResponse["SavingTargets"]!=null && callBackResponse["SavingTargets"].length != 0 )
			{
	            frmIBDreamSavingMaintenance.lblInterestRateVal.text = callBackResponse["SavingTargets"][0]["DATA_VALUE"];
	            var InterestRate = frmIBDreamSavingMaintenance.lblInterestRateVal.text + " %";
	            frmIBDreamSavingMaintenance.lblInterestRateVal.text = InterestRate;
	
	            var locale = kony.i18n.getCurrentLocale();
	            if (kony.i18n.getCurrentLocale() == "en_US") {
	                frmIBDreamSavingMaintenance.lblInterestRate.text = callBackResponse["SavingTargets"][0]["CATEGORY_EN"] + ":";
	                interestRateDreamEN = callBackResponse["SavingTargets"][0]["CATEGORY_EN"] + ":";
	            } 
	            else 
	            {
	                frmIBDreamSavingMaintenance.lblInterestRate.text = callBackResponse["SavingTargets"][0]["CATEGORY_TH"] + ":"
	                interestRateDreamTH = callBackResponse["SavingTargets"][0]["CATEGORY_TH"] + ":";
	                //	 alert("interestRateDreamTH"+interestRateDreamTH)
	            }
	         gblVarFori18En = callBackResponse["SavingTargets"][0]["CATEGORY_EN"] + ":";
            interestRateDreamEN = callBackResponse["SavingTargets"][0]["CATEGORY_EN"] + ":";
            gblVarFori18Th = callBackResponse["SavingTargets"][0]["CATEGORY_TH"] + ":"
            interestRateDreamTH = callBackResponse["SavingTargets"][0]["CATEGORY_TH"] + ":";
            }
            else
            {
            	frmIBDreamSavingMaintenance.lblInterestRateVal.text="0.00%";
            }
            gblMinDreamAmt = callBackResponse["minLimitOpenAct"];
            gblMaxDreamAmt = callBackResponse["maxLimitOpenAct"];

            
            DreamTargetIDIB();
            // dreamCustomerEnquiry();
            
        }

    }
}

function DreamTargetIDIB() {
    var inputparam = {};

    inputparam["dreamTargetID"] = gbldreamtargetID;
    invokeServiceSecureAsync("getDreamDesc", inputparam, getTargetIDDreamIB);
}

function getTargetIDDreamIB(status, resulttable) {
    if (status == 400) {
        
        ResulTnotFound = resulttable["dreamsavingDS"]
        if (resulttable["opstatus"] == 0) {
            if (ResulTnotFound == null || ResulTnotFound == "") {
                dreamCustomerEnquiryIB();
            } else {
                
                if (resulttable["dreamsavingDS"] != null || resulttable["dreamsavingDS"] != "") {
                    if (resulttable["dreamsavingDS"].length > 0) {
                        gblProductImage = resulttable["dreamsavingDS"][0]["productimages"];
                        targetIDDream = resulttable["dreamsavingDS"][0]["DREAM_TARGET_ID"];
                        
                        if (gbldreamtargetID == targetIDDream) {
                            frmIBDreamSavingMaintenance.imgProd.src = gblProductImage;
                        }
                        
                    }
                }
                dreamCustomerEnquiryIB();
            }
        }
    }
}

function DreamTargetImage() {
    var inputparam = {};

    inputparam["dreamTargetID"] = gbldreamtargetID;
    invokeServiceSecureAsync("getDreamDesc", inputparam, getTargetIDDreamIBimage);
}

function getTargetIDDreamIBimage(status, resulttable) {

    if (status == 400) {

        
        if (resulttable["opstatus"] == 0) {


            
            gblProductImageinv = resulttable["dreamsavingDS"][0]["productimages"];
            targetIDDream = resulttable["dreamsavingDS"][0]["DREAM_TARGET_ID"];
            

            if (gbldreamtargetID == targetIDDream) {
                frmIBDreamSavingMaintenance.imgProd.src = gblProductImageinv;

            }

            

        }
        //  DreamSavingUpdateIB();
    }

}




function dreamCustomerEnquiryIB() {
    var inputparam = {};
    inputparam["activationCompleteFlag"] = "true";
    var status = invokeServiceSecureAsync("customerAccountInquiry", inputparam, DreamCallBackCustomerEnquiryIB);

}

function DreamCallBackCustomerEnquiryIB(status, resulttable) {
    var iconDream = "";
    var iconDreamFrom = "";
    DreamProductnameTH = "";
    DreamProductnameENG = "";

    if (status == 400) {
        
        if (resulttable["opstatus"] == 0) {
            //    DreamgetInterestRate();
            DreamNicknameUniq = resulttable;
            var editDream = "false";
            
            


            for (var i = 0; i < resulttable["custAcctRec"].length; i++) {
                
                if (resulttable.custAcctRec[i].accType == kony.i18n.getLocalizedString("Saving")) {
                    if (resulttable.custAcctRec[i].productID == "206") {
                        iconDream = "prod_savingd.png"
                        frmIBDreamSavingMaintenance.imgDepositAccnt.src = iconDream;
                    }
                }
				
					
					if(linkedAccType=="SDA")
                	{
                	  accTypeDream=kony.i18n.getLocalizedString("keySavingIB");
                	}else if (linkedAccType == "CDA"){
                		accTypeDream=kony.i18n.getLocalizedString("keyTermDepositIB");
                	}else if (linkedAccType == "DDA"){
                		accTypeDream=kony.i18n.getLocalizedString("keyCurrentIB");
                	}else if (linkedAccType == "LOC"){
                		accTypeDream=kony.i18n.getLocalizedString("keylblLoan");
                	}
                	else {
                        accTypeDream = ""
                    }
                
                frmIBDreamSavingMaintenance.lblAccType.text = accTypeDream;


                if (editDream == "false") {
                    if (resulttable.custAcctRec[i]["accId"] == gblFourteenDigAccountIB) {
                        fromproductID = resulttable["custAcctRec"][i]["productID"];
                        //alert(FromProductId);

                        
                        if (resulttable.custAcctRec[i]["VIEW_NAME"] == "PRODUCT_CODE_DREAM_SAVING" || resulttable.custAcctRec[i]["VIEW_NAME"] == "PRODUCT_CODE_NOFIXED" || resulttable.custAcctRec[i]["VIEW_NAME"] == "PRODUCT_CODE_SAVINGCARE") iconDreamFrom = "prod_savingd.png"
                        else if (resulttable.custAcctRec[i]["VIEW_NAME"] == "PRODUCT_CODE_CREDITCARD_TABLE") iconDreamFrom = "prod_creditcard.png"
                        else if (resulttable.custAcctRec[i]["VIEW_NAME"] == "PRODUCT_CODE_TD_TABLE") iconDreamFrom = "prod_termdeposits.png"
                        else if (resulttable.custAcctRec[i]["VIEW_NAME"] == "PRODUCT_CODE_NOFEESAVING_TABLE") iconDreamFrom = "prod_nofee.png"
                        else if (resulttable.custAcctRec[i]["VIEW_NAME"] == "PRODUCT_CODE_LOAN_HOMELOAN") iconDreamFrom = "prod_homeloan.png"
                        else if (resulttable.custAcctRec[i]["VIEW_NAME"] == "PRODUCT_CODE_OLDREADYCASH_TABLE" || resulttable.custAcctRec[i]["VIEW_NAME"] == "PRODUCT_CODE_NEWREADYCASH_TABLE" || resulttable.custAcctRec[i]["VIEW_NAME"] == "PRODUCT_CODE_LOAN_CASH2GO") iconDreamFrom = "prod_cash2go.png"
                        else if (resulttable.custAcctRec[i]["VIEW_NAME"] == "PRODUCT_CODE_CURRENT_TABLE" || resulttable.custAcctRec[i]["VIEW_NAME"] == "PRODUCT_CODE_SAVING_TABLE") iconDreamFrom = "prod_currentac.png"
                        else if (resulttable.custAcctRec[i]["VIEW_NAME"] == "PRODUCT_CODE_READYCASH_TABLE") iconDreamFrom = "prod_cash2go.png"
                        accountId = resulttable["custAcctRec"][i]["accId"];
                        accountId = accountId.substr(accountId.length - 4)
                        DreamCustomerNicknameIB = resulttable.custAcctRec[i]["acctNickName"];
                        if (resulttable.custAcctRec[i]["acctNickName"] != null) {
                            frmIBDreamSavingMaintenance.label866795318717757.text = resulttable.custAcctRec[i]["acctNickName"];
                       //     accountnickname = resulttable.custAcctRec[i]["acctNickName"]

                        } else {
                            DreamProductnameENG = resulttable["custAcctRec"][i]["ProductNameEng"] + accountId;
                            DreamProductnameTH = resulttable["custAcctRec"][i]["ProductNameThai"] + accountId;
                            if (kony.i18n.getCurrentLocale() == "en_US") {

                                frmIBDreamSavingMaintenance.label866795318717757.text = DreamProductnameENG;

                            } else {

                                frmIBDreamSavingMaintenance.label866795318717757.text = DreamProductnameTH;

                            }
                            // 
                            //frmIBDreamSavingMaintenance.label866795318717757.text = resulttable["custAcctRec"][i]["ProductNameEng"] + accountId;
                        }
                        frmIBDreamSavingMaintenance.lblnotAvaliable.isVisible = false;
                        if (gblshowAccountBox == true) {
                            frmIBDreamSavingMaintenance.hbox866795318717754.isVisible = false;
                        } else {
                            frmIBDreamSavingMaintenance.hbox866795318717754.isVisible = true;
                        }
                        frmIBDreamSavingMaintenance.btnEditDepositAccnt.setEnabled(true);
                        frmIBDreamSavingMaintenance.image2866795318717755.src = iconDreamFrom;
                        frmIBDreamSavingMaintenance.btnEditDepositAccnt.skin = "btnEdic";
                        editDream = "true";
                        //break;
                    } else {
                        frmIBDreamSavingMaintenance.btnEditDepositAccnt.setEnabled(false);
                        frmIBDreamSavingMaintenance.btnEditDepositAccnt.skin = "editDisable"
                        frmIBDreamSavingMaintenance.lblnotAvaliable.isVisible = true;
                        frmIBDreamSavingMaintenance.hbox866795318717754.isVisible = false;
                        frmIBDreamSavingMaintenance.lblnotAvaliable.text = kony.i18n.getLocalizedString("keyAccntNotAvaliable");
                    }
                }
                var acctIdPersonalixed = gblAccNo;
                
                
                
                
			//	alert("acctIdPersonalixed value::" + acctIdPersonalixed)
              //     alert("gblpersonalizedId" + gblpersonalizedId)
                
                if (resulttable["custAcctRec"][i]["productID"] == "206" && resulttable["custAcctRec"][i]["persionlizeAcctId"] == acctIdPersonalixed.trim() && resulttable["custAcctRec"][i]["persionlizedId"] == gblpersonalizedId.trim()) {
                  
                    prodCode = resulttable["custAcctRec"][i]["productID"];
                   
                    frmIBDreamSavingMaintenance.lblAcntNoValue.text = resulttable["custAcctRec"][i]["accId"];
                    var acountDisplay = frmIBDreamSavingMaintenance.lblAcntNoValue.text;
                    var Acountlen = acountDisplay.length
                    AccountNoDream = acountDisplay.substring(Acountlen - 10);
                    prodName = acountDisplay.substr(Acountlen - 4);
                    frmIBDreamSavingMaintenance.lblAcntNoValue.text = formatAccountNo(AccountNoDream);
                    
                    ToAccountType = resulttable["custAcctRec"][i]["accType"];
                    
                    if (resulttable.custAcctRec[i]["BranchNameEh"] != null && resulttable.custAcctRec[i]["BranchNameEh"] != "") {
                        branchName = resulttable.custAcctRec[i]["BranchNameEh"];
                        branchNameTh = resulttable.custAcctRec[i]["BranchNameTh"];
                        
                        frmIBDreamSavingMaintenance.lblBranchVal.text = branchName;
                    }
                    if (resulttable.custAcctRec[i]["acctNickName"] == null && resulttable.custAcctRec[i]["acctNickName"] == "") {
                        gblDreamNickname = resulttable.custAcctRec[i]["ProductNameEng"] + prodName;
                    //    NicknameDream = resulttable["custAcctRec"][i]["ProductNameEng"]
                        frmIBDreamSavingMaintenance.lblAccountNicknameValue.text = gblDreamNickname;
                    }
                    if (resulttable.custAcctRec[i]["acctNickName"] != null && resulttable.custAcctRec[i]["acctNickName"] != "") {
                        frmIBDreamSavingMaintenance.lblAccountNicknameValue.text = resulttable.custAcctRec[i]["acctNickName"];
                    } else {
                        frmIBDreamSavingMaintenance.lblAccountNicknameValue.text = resulttable["custAcctRec"][i]["ProductNameEng"] + prodName;
                        
                    }
                }
                 else if(resulttable["custAcctRec"][i]["productID"] == "206" && resulttable["custAcctRec"][i]["accId"] == gblAccNo.trim()){
               		
               		prodCode = resulttable["custAcctRec"][i]["productID"];
                    openFrmBranch();
                    frmIBDreamSavingMaintenance.lblAcntNoValue.text = resulttable["custAcctRec"][i]["accId"];
                    var acountDisplay = frmIBDreamSavingMaintenance.lblAcntNoValue.text;
                    var Acountlen = acountDisplay.length
                    AccountNoDream = acountDisplay.substring(Acountlen - 10);
                    prodName = acountDisplay.substr(Acountlen - 4);
                    frmIBDreamSavingMaintenance.lblAcntNoValue.text = formatAccountNo(AccountNoDream);
                    
                    ToAccountType = resulttable["custAcctRec"][i]["accType"];
                    
                    if (resulttable.custAcctRec[i]["BranchNameEh"] != null && resulttable.custAcctRec[i]["BranchNameEh"] != "") {
                        branchName = resulttable.custAcctRec[i]["BranchNameEh"];
                        branchNameTh = resulttable.custAcctRec[i]["BranchNameTh"];
                        
                        frmIBDreamSavingMaintenance.lblBranchVal.text = branchName;
                    }
                    if (resulttable.custAcctRec[i]["acctNickName"] == null && resulttable.custAcctRec[i]["acctNickName"] == "") {
                        gblDreamNickname = resulttable.custAcctRec[i]["ProductNameEng"] + prodName;
                      // NicknameDream = resulttable["custAcctRec"][i]["ProductNameEng"]
                        frmIBDreamSavingMaintenance.lblAccountNicknameValue.text = gblDreamNickname;
                    }
                    if (resulttable.custAcctRec[i]["acctNickName"] != null && resulttable.custAcctRec[i]["acctNickName"] != "") {
                        frmIBDreamSavingMaintenance.lblAccountNicknameValue.text = resulttable.custAcctRec[i]["acctNickName"];
                    } else {
                        frmIBDreamSavingMaintenance.lblAccountNicknameValue.text = resulttable["custAcctRec"][i]["ProductNameEng"] + prodName;
                        
                    }
               		}
                
            }
            
            
            if (ResulTnotFound == null || ResulTnotFound == "") {

            } else {
                if (ChannelID12 == "01" || ChannelID12 == "02") {
                    barGraphIB();
                }
            }
			dismissLoadingScreenPopup();
            frmIBDreamSavingMaintenance.show();
        }
    }
}

function barGraphIB() {
    // var percentage = frmDreamSavingMB.textpercentage.text;
    var percentage = percentageBar; // frmIBDreamSavingMaintenance.textbox2866825109104393.text;
    var deviceinfo = gblDeviceInfo;
    
    if (percentage == 0) {
        frmIBDreamSavingMaintenance.vboxSkin.skin = "samplevbox00";
        frmIBDreamSavingMaintenance.vboxSkinVal.margin = [1, 100, 1, 1];
    } else if (percentage >= 1 && percentage <= 15) {
        frmIBDreamSavingMaintenance.vboxSkin.skin = "samplevbox10";
        frmIBDreamSavingMaintenance.vboxSkinVal.margin = [1, 95, 1, 1];

    } else if (percentage >= 16 && percentage <= 25) {
        frmIBDreamSavingMaintenance.vboxSkin.skin = "samplevbox20";
        frmIBDreamSavingMaintenance.vboxSkinVal.margin = [1, 84, 1, 1];
    } else if (percentage >= 26 && percentage <= 35) {
        frmIBDreamSavingMaintenance.vboxSkin.skin = "samplevbox30";
        frmIBDreamSavingMaintenance.vboxSkinVal.margin = [1, 75, 1, 1];

    } else if (percentage >= 36 && percentage <= 45) {
        frmIBDreamSavingMaintenance.vboxSkin.skin = "samplevbox40";
        frmIBDreamSavingMaintenance.vboxSkinVal.margin = [1, 62, 1, 1];
    } else if (percentage >= 46 && percentage <= 55) {
        frmIBDreamSavingMaintenance.vboxSkin.skin = "samplevbox50";
        frmIBDreamSavingMaintenance.vboxSkinVal.margin = [1, 52, 1, 1];
    } else if (percentage >= 56 && percentage <= 65) {
        frmIBDreamSavingMaintenance.vboxSkin.skin = "samplevbox60";
        frmIBDreamSavingMaintenance.vboxSkinVal.margin = [1, 45, 1, 1];
    } else if (percentage >= 66 && percentage <= 75) {
        frmIBDreamSavingMaintenance.vboxSkin.skin = "samplevbox70";
        frmIBDreamSavingMaintenance.vboxSkinVal.margin = [1, 30, 1, 1];
    } else if (percentage >= 76 && percentage <= 85) {
        frmIBDreamSavingMaintenance.vboxSkin.skin = "samplevbox80";
        frmIBDreamSavingMaintenance.vboxSkinVal.margin = [1, 18, 1, 1];
    } else if (percentage >= 86 && percentage <= 99) {
        frmIBDreamSavingMaintenance.vboxSkin.skin = "samplevbox90";
        frmIBDreamSavingMaintenance.vboxSkinVal.margin = [1, 12, 1, 1];
    } else if (percentage == 100) {
        frmIBDreamSavingMaintenance.vboxSkin.skin = "samplevbox100";
        frmIBDreamSavingMaintenance.vboxSkinVal.margin = [1, 0, 1, 1];
    } else {
        frmIBDreamSavingMaintenance.vboxSkin.skin = "skn100";
        frmIBDreamSavingMaintenance.vboxSkinVal.margin = [1, 1, 1, 1];
    }
    //   frmIBDreamSavingMaintenance.btnPercentage.text = percentage
    
    frmIBDreamSavingMaintenance.btnPercentage.text = percentage + "%";
    //frmIBDreamSavingMaintenance.show();
}


function checkUserStatusDSMIB() {
    var inputParam = {
        rqUUId: "",
        channelName: "IB-INQ",
        editDreamFlag: "true"
    }
     if(getCRMLockStatus()){	
  	curr_form=kony.application.getCurrentForm().id;
	popIBBPOTPLocked.show();
 	return false;
 	}
  	showLoadingScreenPopup()
    invokeServiceSecureAsync("crmProfileInq", inputParam, callBackUSerStatusDSMIB)
}

function callBackUSerStatusDSMIB(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == 0) {
            
            var MBUserStatus = callBackResponse["mbFlowStatusIdRs"];
            
            if (MBUserStatus == "04") {

                 dismissLoadingScreenPopup();
                alert("Your account is locked")
            } else {



                if (callBackResponse["editDreamBusinessHrsFlag"] == "true") {
                    DreamcheckOpenActBusHrsIB();
                } else {
                   dismissLoadingScreenPopup();
                    if (GLOBAL_EDITDREAM_STARTTIME == null || GLOBAL_EDITDREAM_STARTTIME == "") {
                        GLOBAL_EDITDREAM_STARTTIME = "03:00";
                    }
                    if (GLOBAL_EDITDREAM_ENDTIME == null || GLOBAL_EDITDREAM_ENDTIME == "") {
                        GLOBAL_EDITDREAM_ENDTIME = "22:55";
                    }

                    showAlert(kony.i18n.getLocalizedString("keyDreamcheckhrs"), kony.i18n.getLocalizedString("info"));
                    return false;
                }


            }
            
        } else {

           dismissLoadingScreenPopup();
            alert("Your account is locked");
        }
    }

}

function DreamcheckOpenActBusHrsIB() {

    var inputParam = {};
    invokeServiceSecureAsync("editDreamActBusinessHours", inputParam, DreamcallBackCheckBusHrsIB);

}

function DreamcallBackCheckBusHrsIB(status, callBackResponse) {
    if (status == 400) {
        

        if (callBackResponse["opstatus"] == 0) {
            
            if (callBackResponse["editDreamBusinessHrsFlag"] == "true") {
                DreamonClickAgreeOpenDSActsIB();
            } else {
             dismissLoadingScreenPopup();
                if (GLOBAL_EDITDREAM_STARTTIME == null || GLOBAL_EDITDREAM_STARTTIME == "") {
                    GLOBAL_EDITDREAM_STARTTIME = "03:00";
                }
                if (GLOBAL_EDITDREAM_ENDTIME == null || GLOBAL_EDITDREAM_ENDTIME == "") {
                    GLOBAL_EDITDREAM_ENDTIME = "22:55";
                }

                showAlert(kony.i18n.getLocalizedString("keyDreamcheckhrs"), kony.i18n.getLocalizedString("info"));
                return false;
            }
        } else {
            dismissLoadingScreenPopup();
            showAlert(resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}

function DreamonClickAgreeOpenDSActsIB() {

   
    var inputparam = {};
    invokeServiceSecureAsync("getDreamSavingTargets", inputparam, oncallbackDreamIB);

}

function oncallbackDreamIB(status, resulttable) {
    if (status == 400) {
        
        if (resulttable["opstatus"] == 0) {
            //alert("result:"+resulttable["SavingTargets"]);
            //dismissLoadingScreen();
            gblOpenDrmAcctIB = resulttable;
            setDataforDreamActsIB(gblOpenDrmAcctIB);
        }
    }
}

function dreamDataIB() {

    gblDreamflag = "true";
    glb_selectedDataDream = frmIBDreamSAvingEdit.customFromAccountIB.data[gblCWSelectedItem]
    frmIBDreamSAvingEdit.lbldreamValue.text = glb_selectedDataDream.lblDream;
    gblProductImage1 = glb_selectedDataDream.img1;
    gbltargetIdSelected = glb_selectedDataDream.lblhiddentarget;
    oldTargetAmnt = frmIBDreamSavingMaintenance.lblTargetAmount.text;
    oldTargetAmnt = oldTargetAmnt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "")
    oldMnthlyAmnt = frmIBDreamSavingMaintenance.lblMnthlySavingAmntVal.text;
    oldMnthlyAmnt = oldMnthlyAmnt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "")
    oldRecurringDate= mnthlyTransferDate;
    
   
    //gbldreamtargetID = gbltargetIdSelected

}

function setDataforDreamActsIB(resulttable) {
    var list = [];
    var count = 0;
    var firstElement = [];
    // var selected=gbldreamtargetID;

    //for (var j = gbldreamtargetID-1,k=0; k < (gbldreamtargetID+resulttable["SavingTargets"].length)%resulttable["SavingTargets"].length; j++,k++) {
    for (j = 0; j < resulttable["SavingTargets"].length; j++) {
        var dreamName;
        var dreamimage;
        var locale = kony.i18n.getCurrentLocale();

        var TargetHid = resulttable["SavingTargets"][j]["DREAM_TARGET_ID"];
        if (locale == "en_US") {
            dreamName = resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"];
        } else {
            dreamName = resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_TH"];
        }
        if (resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"] == "My Vacation") {
            dreamimage = "prod_vacation.png";
            //  frmIBDreamSAvingEdit.lbldreamValue.text= "My Vacation"
            TargetHid = resulttable["SavingTargets"][j]["DREAM_TARGET_ID"];
        } else if (resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"] == "My Dream") {
            dreamimage = "prod_dream.png";
            //   frmIBDreamSAvingEdit.lbldreamValue.text =  "My Dream"
            TargetHid = resulttable["SavingTargets"][j]["DREAM_TARGET_ID"];
        } else if (resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"] == "My Car") {
            dreamimage = "prod_car.png";
            TargetHid = resulttable["SavingTargets"][j]["DREAM_TARGET_ID"];
            // frmIBDreamSAvingEdit.lbldreamValue.text = "My Car"
        } else if (resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"] == "My Education") {
            dreamimage = "prod_education.png";
            TargetHid = resulttable["SavingTargets"][j]["DREAM_TARGET_ID"];
            // frmIBDreamSAvingEdit.lbldreamValue.text= "My Education"
        } else if (resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"] == "My Own House") {
            dreamimage = "prod_homeloan.png";
            TargetHid = resulttable["SavingTargets"][j]["DREAM_TARGET_ID"];
            // frmIBDreamSAvingEdit.lbldreamValue.text= "My Own House"
        } else if (resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"] == "My Business") {
            dreamimage = "prod_business.png";
            TargetHid = resulttable["SavingTargets"][j]["DREAM_TARGET_ID"];
            // frmIBDreamSAvingEdit.lbldreamValue.text = "My Business"
        } else if (resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"] == "My Saving") {
            dreamimage = "prod_saving.png";
            TargetHid = resulttable["SavingTargets"][j]["DREAM_TARGET_ID"];
            // frmIBDreamSAvingEdit.lbldreamValue.text="My Saving"
        }
        // alert(gbldreamtargetID)
        
        if (gbldreamtargetID == TargetHid) {

            firstElement.push({
                img1: dreamimage,
                lblDream: dreamName,
                lblhiddentarget: TargetHid
            });
            

        } else {
            list.push({
                img1: dreamimage,
                lblDream: dreamName,
                lblhiddentarget: TargetHid
            })
        }
        //if(j==resulttable["SavingTargets"].length-1)j=-1;
    }



    var data1 = firstElement.concat(list);
    frmIBDreamSAvingEdit.customFromAccountIB.data = data1;
    var currForm = kony.application.getCurrentForm();
    frmIBDreamSAvingEdit.customFromAccountIB.onSelect = dreamDataIB;
    if (GLOBAL_DREAM_DESC_MAXLENGTH != null || GLOBAL_DREAM_DESC_MAXLENGTH != "") {
        frmIBDreamSAvingEdit.lblDreamDescVal.maxTextLength = kony.os.toNumber(GLOBAL_DREAM_DESC_MAXLENGTH);
    } else {
        frmIBDreamSAvingEdit.lblDreamDescVal.maxTextLength = 30;
    }
    if (GLOBAL_NICKNAME_LENGTH != null || GLOBAL_NICKNAME_LENGTH != "") {
        frmIBDreamSAvingEdit.lblNicknameVal.maxTextLength = kony.os.toNumber(GLOBAL_NICKNAME_LENGTH);
    } else {
        frmIBDreamSAvingEdit.lblNicknameVal.maxTextLength = 20;
    }
    dreamDataIB()
   

    frmIBDreamSAvingEdit.lblDreamDescVal.text = frmIBDreamSavingMaintenance.lblDreamDesc.text;
    var mnthsSave = frmIBDreamSavingMaintenance.lblMnthlySavingAmntVal.text +"/"+ kony.i18n.getLocalizedString("keyCalendarMonth");
    frmIBDreamSAvingEdit.lblmnthsaveval.text = mnthsSave;
    frmIBDreamSAvingEdit.lblNicknameVal.text = frmIBDreamSavingMaintenance.lblAccountNicknameValue.text;

    frmIBDreamSAvingEdit.lblTargetAmntVal.text = frmIBDreamSavingMaintenance.lblTargetAmount.text;
    combobxValue = frmIBDreamSavingMaintenance.lblTranfrEveryMnthVal.text;
    if (mnthlyTransferDate == 1 || mnthlyTransferDate == 21 || mnthlyTransferDate == 31) {
        combobxValue = combobxValue.replace("st", "");
    } else if (mnthlyTransferDate == 2 || mnthlyTransferDate == 22) {
        combobxValue = combobxValue.replace("nd", "");
    } else if (mnthlyTransferDate == 3 || mnthlyTransferDate == 23) {
        combobxValue = combobxValue.replace("rd", "");
    } else {
        combobxValue = combobxValue.replace("th", "");
    }
    frmIBDreamSAvingEdit.combobox86679531842314.selectedKey = combobxValue;
	 dismissLoadingScreenPopup();
    frmIBDreamSAvingEdit.show();
}

function callOTPServiceDSMIB(){
    frmIBDreamSAvingEdit.lblOTPinCurr.text = " ";//kony.i18n.getLocalizedString("invalidOTP"); //
    frmIBDreamSAvingEdit.lblPlsReEnter.text = " "; 
    frmIBDreamSAvingEdit.hbxOTPincurrect.isVisible = false;
    frmIBDreamSAvingEdit.hbxOTPBankRef.isVisible = true;
    frmIBDreamSAvingEdit.hbxOTPsnt.isVisible = true;
    frmIBDreamSAvingEdit.txtBxOTP.setFocus(true);
    
    var inputParams = {};
    var locale = kony.i18n.getCurrentLocale();
    inputParams["Channel"] = "UpdateDreamSavings";
    inputParams["locale"] = locale;
    
    if(gblAccountSummaryFlag == "false"){
    	inputParams["accountNo"] = frmIBMyAccnts.segTMBAccntsList.data[frmIBMyAccnts.segTMBAccntsList.selectedIndex[1]]["hiddenAccountNo"];
    	inputParams["productNameEN"] = frmIBMyAccnts.segTMBAccntsList.data[frmIBMyAccnts.segTMBAccntsList.selectedIndex[1]]["hiddenProductNameEng"];
    	inputParams["productNameTH"] = frmIBMyAccnts.segTMBAccntsList.data[frmIBMyAccnts.segTMBAccntsList.selectedIndex[1]]["hiddenProductNameThai"];
    }else{
    	inputParams["productNameEN"]=gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"];
    	inputParams["productNameTH"]=gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"];
    	inputParams["accountNo"]=gblAccountTable["custAcctRec"][gblIndex]["persionlizeAcctId"];
    }	
    
    frmIBDreamSAvingEdit.btnOTPReq.skin = "btnIBREQotp";
    //invokeServiceSecureAsync("generateOTP", inputParams, callBackIBreqOTPIBMYA);
    invokeServiceSecureAsync("generateOTPWithUser", inputParams, callBackIBreqOTPIBDSM);
}

function callBackIBreqOTPIBDSM(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["errCode"] == "GenOTPRtyErr00001") {
            
            dismissLoadingScreenPopup();
            showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
            return false;
        }
        if (callBackResponse["errCode"] == "GenOTPRtyErr00002") {
            
            dismissLoadingScreenPopup();
            showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00002"), kony.i18n.getLocalizedString("info"));
            return false;
        }
        if (callBackResponse["opstatus"] == 0) {
            
            dismissLoadingScreenPopup();
            if (callBackResponse["errCode"] == "GenOTPRtyErr00001") {
                

                showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                dismissLoadingScreenPopup();
                return false;
            } else if (callBackResponse["errCode"] == "JavaErr00001") {
                


                dismissLoadingScreenPopup();
                showAlertIB(kony.i18n.getLocalizedString("ECJavaErr00001"), kony.i18n.getLocalizedString("info"));
                return false;
            }
            //gblOTPdisabletime = kony.os.toNumber(callBackResponse["requestOTPEnableTime"]) * 100;
            gblRetryCountRequestOTP = kony.os.toNumber(callBackResponse["retryCounterRequestOTP"]);
            gblOTPLENGTH = kony.os.toNumber(callBackResponse["otpLength"]);
            
            var reqOtpTimer = kony.os.toNumber(callBackResponse["requestOTPEnableTime"]);
            curr_form = kony.application.getCurrentForm();
            try {
                kony.timer.cancel("OTPTimer");
            } catch (e) {
                // todo: handle exception
            }
            if (gblOTPReqLimit < 3) {
                kony.timer.schedule("OtpTimer", OTPTimercallbackDreamSaving, reqOtpTimer, false);
            }
            frmIBDreamSAvingEdit.btnOTPReq.skin = "btnIBREQotp";
            frmIBDreamSAvingEdit.btnOTPReq.focusSkin = "btnIBREQotp";
            frmIBDreamSAvingEdit.btnOTPReq.onClick = "";
            gblOTPReqLimit = gblOTPReqLimit + 1;
            var refVal = "";
            for (var d = 0; d < callBackResponse["Collection1"].length; d++) {
                if (callBackResponse["Collection1"][d]["keyName"] == "pac") {
                    refVal = callBackResponse["Collection1"][d]["ValueString"];
                    break;
                }
            }
            frmIBDreamSAvingEdit.lblOTPPhoneNumber.text = maskingIB(gblPHONENUMBER);
            frmIBDreamSAvingEdit.lblOTPBankRefValue.text = refVal;
            frmIBDreamSAvingEdit.txtBxOTP.setFocus(true);
            //setTimeout('OTPReqButton()',gblOTPdisabletime);
            //	frmIBDreamSAvingEdit = kony.application.getCurrentForm();
            frmIBDreamSAvingEdit.txtBxOTP.maxTextLength = gblOTPLENGTH
            if (gblOTPDream != null && gblOTPDream == "true") {
                // frmIBDreamSAvingEdit.hbxcancel.isVisible = false;
                frmIBDreamSAvingEdit.hbxotp.isVisible = true;
                frmIBDreamSAvingEdit.hbxcancel.isVisible = false;
                frmIBDreamSAvingEdit.hbxcancelOTP.isVisible = true;
                frmIBDreamSAvingEdit.txtBxOTP.setFocus(true);
            }
            //	curr_form.lblRefNumValue.text = callBackResponse["Collection1"][8]["ValueString"]; //callBackResponse["pac"]; //callBackResponse["bankRefNo"];

            //dismissLoadingScreenPopup();
        } else {
            
            dismissLoadingScreenPopup();
            alert("Error " + callBackResponse["errMsg"]);
        }
    } else {
        
        if (status == 300) {
            dismissLoadingScreenPopup();
            alert("Error");
        }
    }

}

function OTPTimercallbackDreamSaving() {
    frmIBDreamSAvingEdit.btnOTPReq.skin = "btnIBREQotpFocus";
    frmIBDreamSAvingEdit.btnOTPReq.onClick = callOTPServiceDSMIB;
    
    try {
        kony.timer.cancel("OtpTimer");
    } catch (e) {
        
    }
}

function onClickConfirmDreamSavingIB() {

    if (frmIBDreamSAvingEdit.hbxOTPEntry.setVisibility == "true") {
        var OTPtext = otptest(frmIBDreamSAvingEdit.txtBxOTP.text)
        if (OTPtext == false) {

            showAlertIB(kony.i18n.getLocalizedString("keyIBOtpNumeric"), kony.i18n.getLocalizedString("info"));
            frmIBDreamSAvingEdit.txtBxOTP.skin = "txtErrorBG"
            return false;
        }
    }
    if (frmIBDreamSAvingEdit.txtBxOTP.text == "" && gblTokenSwitchFlag == false) {
        //alert("Please Enter  OTP ");
        alert(kony.i18n.getLocalizedString("Receipent_alert_correctOTP"));
        return false;
    } else if (frmIBDreamSAvingEdit.tbxToken.text == "" && gblTokenSwitchFlag == true) {
        //alert("Please Enter Token")
        alert(kony.i18n.getLocalizedString("Receipent_tokenId"));
        return false;
    } else {
        gblVerifyOTP = gblVerifyOTP + 1
        
        // validateOTPforDSM();
        editDreamSavingCompositeServiceIB();
        //transferFlowAckIB()
    }
}

/*function RecxModelUpdateIB() {

    MonthlySavingAmntUpdateIB = frmIBDreamSAvingEdit.lblmnthsaveval.text;
    MonthlySavingAmntUpdateIB = MonthlySavingAmntUpdateIB.replace(/,/g, "").replace("/", "").replace(kony.i18n.getLocalizedString("keyCalendarMonth"), "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    mnthlyTransferDateUpdate = frmIBDreamSAvingEdit.combobox86679531842314.selectedKeyValue[0];
    var inputparam = {};
    
    inputparam["clientDt"] = "";
    inputparam["accIdFrom"] = recurringFrmAcctIdIB;
    
    inputparam["accTypeFrom"] = recurringFrmAcctTypeIB;
    
    inputparam["accIdTo"] = recurringToAcctIdIB;
    
    inputparam["accTypeTo"] = recurringToAcctTypeIB;
    
    inputparam["curAmt"] = MonthlySavingAmntUpdateIB;
    
    // inputparam["dueDate"] = dueDate;
    

    inputparam["dayofMonth"] = mnthlyTransferDateUpdate;
    
    inputparam["freq"] = "Monthly";
    
    inputparam["curCode"] = avaliableCurrCode;
    
    inputparam["frmAcctProdId"] = FromProductIdDream;
    
    inputparam["toAcctProdId"] = prodCode;
    
    inputparam["channel"] = "IB";
    
    inputparam["rqUID"] = "";
    inputparam["recXferId"] = recXferId;
    
    inputparam["name"] = "IB-INQ";
    invokeServiceSecureAsync("RecurringFundTransModelUpdate", inputparam, OnClickConfirmUpdateIB);
}*/

/*function OnClickConfirmUpdateIB(status, resulttable, gblDreamflg) {
    if (status == 400) {
        

        if (resulttable["opstatus"] == 0) {
            var StatusCode = resulttable["status"][0]["statusCode"];
            var Severity = resulttable["status"][0]["severity"];
            var StatusDesc = resulttable["status"][0]["statusDesc"];

            
            
            

            if (StatusCode == "0") {
                productCodename = prodCode + prodName;
                //  activityLogServiceCall("073", "", "01", "", productCodename, frmIBDreamSAvingEdit.lblTargetAmntVal.text, AccountNoDream, frmIBDreamSAvingEdit.lblMnthlySavingVal.text, frmIBDreamSAvingEdit.lbldreamValue.text, "")
                if (gblDreamflag == "true") {

                    gbldreamtargetID = gbltargetIdSelected;
                    //	alert("gbldreamtargetID:::"+gbldreamtargetID)
                    DreamTargetImage()
                } else {
                    DreamSavingUpdateIB();
                }
            } else {
                productCodename = prodCode + prodName;
                //    activityLogServiceCall("073", "", "02", "", productCodename, frmIBDreamSAvingEdit.lblTargetAmntVal.text, AccountNoDream, frmIBDreamSAvingEdit.lblMnthlySavingVal.text, frmIBDreamSAvingEdit.lbldreamValue.text, "")
                

                //    DreamUpdateDataIB();
            }
        }
    }
}*/

/*function DreamSavingUpdateIB() {

    var inputparam = {};
    inputparam["CRM_ID"] = "";
    inputparam["PERSONALIZED_ACCT_ID"] = gblAccNo;
    inputparam["PERSONALIZED_ID"] = gblpersonalizedId;
    var removebhat = frmIBDreamSAvingEdit.lblTargetAmntVal.text;
    removebhat1 = removebhat.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(/,/g, "").replace(".00", "");
    inputparam["DREAM_TARGET_AMOUNT"] = removebhat1;
    // inputparam["DREAM_TARGET_AMOUNT"] = frmIBDreamSAvingEdit.lblTargetAmntVal.text;
    inputparam["PERSONALIZED_ACCT_NICKNAME"] = frmIBDreamSAvingEdit.lblNicknameVal.text.trim();
    inputparam["DREAM_DESC"] = frmIBDreamSAvingEdit.lblDreamDescVal.text.trim();
    inputparam["DREAM_TARGET_ID"] = gbldreamtargetID;

    invokeServiceSecureAsync("updateEditDreamSaveInfo", inputparam, OncalBackDreamSavingUpdateIB);


}*/

/*function OncalBackDreamSavingUpdateIB(status, resulttable) {

    if (status == 400) {
        
        if (resulttable["opstatus"] == "0") {
            
            // 	
            calculateDreamDataIB();
        }
    }
}*/


/*function sendDreamEmailNotificationIB(status, resulttable) {
    var removebhatnotification = frmIBDreamSAvingEdit.lblTargetAmntVal.text;
    newtarget = removebhatnotification.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    NicknameDreamNotificationIB = frmIBDreamSAvingEdit.lblNicknameVal.text;
    DreamDescGblNotificationIB = frmIBDreamSAvingEdit.lblDreamDescVal.text;
    var mnthnotification = frmIBDreamSAvingEdit.lblmnthsaveval.text.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace("/", "").replace(kony.i18n.getLocalizedString("keyCalendarMonth"), "");
        // alert(mnthnotification)
    mnthlyTransferDateNotificationIB = frmIBDreamSAvingEdit.combobox86679531842314.selectedKeyValue[0];

    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            var inputParam = {}
            var deliveryMethod = "Email";
            inputParam["notificationType"] = deliveryMethod;
            var currentLocale = getCurrentLocale();
            inputParam["Locale"] = currentLocale;
            inputParam["source"] = "editDreamSaving";
            // inputParam["emailId"] = gblEmailId;
            gblCustomerName = resulttable["customerName"];
            inputParam["custName"] = gblCustomerName;
            //inputParam["channelName"] = "MB-INQ";
            inputParam["accountNo"] = AccountNoDream; // sunstringing is beinng done at both client and server side have to check
            inputParam["actno"] = AccountNoDream; //why this is being passed to service
            inputParam["nickname"] = NicknameDreamNotificationIB;
            inputParam["DREAM_DESC"] = DreamDescGblNotificationIB;
            inputParam["targetAmt"] = newtarget;
            inputParam["monthlySavingAmt"] = mnthnotification;
            inputParam["monthlyTransferDate"] = mnthlyTransferDateNotificationIB;
            inputParam["channelName"] = "IB"; // added by considering DEF9140 
            invokeServiceSecureAsync("NotificationAdd", inputParam, callBackNotificationEditDreamIB)
        }
    }
}

function callBackNotificationEditDreamIB(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            
        }
    }
}*/
gblSpecial = "0";

function showCoverFlow(){

frmIBOpenNewDreamAcc.vbxBPRight.hbxImage.setVisibility(false);
frmIBOpenNewDreamAcc.vbxBPRight.hbxBuildMyDream.setVisibility(false);
frmIBOpenNewDreamAcc.hbxCoverflow.setVisibility(true);
frmIBOpenNewDreamAcc.image24573708914676.isVisible = false;
frmIBOpenNewDreamAcc.image2867539252331117.isVisible = true;
frmIBOpenNewDreamAcc.vboxMiddle.skin = "vbxMiddle13px";
frmIBOpenNewDreamAcc.vbxBPRight.skin = "vbxRightColumnGrey";
frmIBOpenNewDreamAcc.hbxPictureIt.skin="hbxProperFocus";
frmIBOpenNewDreamAcc.hbxNickVal.skin="hbxProper";
frmIBOpenNewDreamAcc.hboxTarget.skin="hbxProper";
frmIBOpenNewDreamAcc.hboxMonthlySaving.skin="hbxProper";
frmIBOpenNewDreamAcc.hboxMonthsToDream.skin="hbxProper";
frmIBOpenNewDreamAcc.hbxBPAll.skin="hbxIBSizelimiterRightGrey";
frmIBOpenNewDreamAcc.show();


}

function DisableIBForDreamDesc(e) {
    var keycode;
    var isCtrl;
    var e = e || event

    if (window.event) {
        keycode = window.event.keyCode; //IE
        if (window.event.shiftKey) isCtrl = true;
    } else {
        keycode = e.which;
        
        if (e.shiftKey) isCtrl = true;
        else isCtrl = false;
    }

    
    //if ctrl is pressed check if other key is in forbidenKeys array
    if (gblSpecial == "0") {}
    if (gblSpecial == "1") {
        var pattern1 = /^[!@#$%^&*()_+\-=\[\]`~{};':"\\|,.<>\/?]*$/;
        var dreamNickNameChar = frmIBOpenNewDreamAcc.txtODNickNameVal.text;
        var len = dreamNickNameChar.length;
        var lastChar = dreamNickNameChar.charAt(len - 1);
        if (lastChar.match(pattern1)) {
            frmIBOpenNewDreamAcc.txtODNickNameVal.text = dreamNickNameChar.substring(0, len - 1);
        }
        //alert("e.keyCode::"+e.keyCode);
        /*if (
        (e.keyCode > 7 && e.keyCode <= 32) || 
		(e.keyCode >= 34 && e.keyCode <= 40) || 
		(e.keyCode >= 44 && e.keyCode <= 46)|| 
		(e.keyCode >= 48 && e.keyCode <= 57)|| 
		(e.keyCode >= 65 && e.keyCode < 93) ||
		(e.keyCode > 95 && e.keyCode < 106)|| 
		(e.keyCode > 160 && e.keyCode < 250) ){
            
            return true;
        } else {
            return false;
        }*/
    }
    if (gblSpecial == "2") {
        var num = e.keyCode;
        if ((num > 95 && num < 106) || (num > 36 && num < 41) || num == 9 || num == 110 || num == 190) {
            return;
        }

        if (e.shiftKey || e.ctrlKey || e.altKey) {
            e.preventDefault();
        } else if (num != 46 && num != 8) {
            if (isNaN(parseInt(String.fromCharCode(e.which)))) {
                e.preventDefault();
            }
        }


    }
    return true;
}
//document.onkeypress = DisableCopyPaste
function disablealphaDream() {
    //var deviceInfo=kony.os.deviceInfo();
    //if(deviceInfo["name"] == "thinclient")
    document.onkeydown = DisableIBForDreamDesc;
}


function DisableIBAmountMnth(e) {

    key = window.event.keyCode
    if (frmIBDreamSAvingEdit.lblMnthlySavingVal.text.length == 0 && e.keyCode == 48) {

        return false;
    }

}
//document.onkeypress = DisableCopyPaste
function disableAmntMnthDream() {
    //var deviceInfo=kony.os.deviceInfo();
    //if(deviceInfo["name"] == "thinclient")
    document.onkeydown = DisableIBAmountMnth
}

//Start of Composite service

function editDreamSavingCompositeServiceIB() {

    
    curr_form = kony.application.getCurrentForm();
    var otpToken = frmIBDreamSAvingEdit.tbxToken.text

    var editDreamSavingCompositeService = {};
    showLoadingScreenPopup();
    if (gblTokenSwitchFlag == true) {
        editDreamSavingCompositeService["verifyToken_loginModuleId"] = "IB_HWTKN";
        editDreamSavingCompositeService["verifyToken_userStoreId"] = "DefaultStore";
        editDreamSavingCompositeService["verifyToken_retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
        editDreamSavingCompositeService["verifyToken_retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
        editDreamSavingCompositeService["verifyToken_userId"] = gblUserName;
        editDreamSavingCompositeService["verifyToken_password"] = otpToken;
        editDreamSavingCompositeService["verifyToken_sessionVal"] = "";
        editDreamSavingCompositeService["verifyToken_segmentId"] = "segmentId";
        editDreamSavingCompositeService["verifyToken_segmentIdVal"] = "MIB";
        editDreamSavingCompositeService["TokenSwitchFlag"] = "true";
    } else {
        var otpText = frmIBDreamSAvingEdit.txtBxOTP.text;
        editDreamSavingCompositeService["verifyPwd_retryCounterVerifyOTP"] = gblVerifyOTP;
        editDreamSavingCompositeService["verifyPwd_segmentId"] = "MIB"
        editDreamSavingCompositeService["verifyPwd_loginModuleId"] = "IBSMSOTP"
        editDreamSavingCompositeService["verifyPwd_userStoreId"] = "DefaultStore";
        editDreamSavingCompositeService["verifyPwd_password"] = otpText
        editDreamSavingCompositeService["verifyPwd_userId"] = gblUserName;
    }
    editDreamSavingCompositeService["appID"] = appConfig.appId;
    editDreamSavingCompositeService["channel"] = "IB"
    editDreamSavingCompositeService["gblVerifyOTP"] = gblVerifyOTP;
    editDreamSavingCompositeService["prodCode"] = prodCode;
    editDreamSavingCompositeService["prodName"] = prodName;
    editDreamSavingCompositeService["gblDreamflag"] = gblDreamflag;
    
    //editDreamSavingCompositeService["gbltargetIdSelected"] =gbltargetIdSelected;			
 
    //activity logging
 newtarget1 = frmIBDreamSAvingEdit.lblTargetAmntVal.text;
 newtarget1 = newtarget1.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "")
 newmnthamnt = frmIBDreamSAvingEdit.lblmnthsaveval.text
  newmnthamnt = newmnthamnt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace("/", "").replace(kony.i18n.getLocalizedString("keyCalendarMonth"), "");

    editDreamSavingCompositeService["activityLog_activityTypeID"] = "073";
    editDreamSavingCompositeService["activityLog_errorCd"] = "";
    editDreamSavingCompositeService["activityLog_activityStatus"] = "";
    editDreamSavingCompositeService["activityLog_deviceNickName"] = "";
    editDreamSavingCompositeService["activityLog_activityFlexValues1"] = prodCode + prodName
    if((oldTargetAmnt)== (newtarget1)){
     editDreamSavingCompositeService["activityLog_activityFlexValues2"] = newtarget1;
    }else{
    editDreamSavingCompositeService["activityLog_activityFlexValues2"] = oldTargetAmnt + "+"+newtarget1;
    }
    editDreamSavingCompositeService["activityLog_activityFlexValues3"] = AccountNoDream;
    if( oldMnthlyAmnt ==  (newmnthamnt)){
   
    editDreamSavingCompositeService["activityLog_activityFlexValues4"] = newmnthamnt;
    }else{
      editDreamSavingCompositeService["activityLog_activityFlexValues4"] = oldMnthlyAmnt+" + "+newmnthamnt;
    }
    if( oldRecurringDate ==  frmIBDreamSAvingEdit.combobox86679531842314.selectedKeyValue[0]){
    editDreamSavingCompositeService["activityLog_activityFlexValues5"] = frmIBDreamSAvingEdit.combobox86679531842314.selectedKeyValue[0]
    }else{
    editDreamSavingCompositeService["activityLog_activityFlexValues5"] = oldRecurringDate+" + "+ frmIBDreamSAvingEdit.combobox86679531842314.selectedKeyValue[0]
    }
    editDreamSavingCompositeService["activityLog_logLinkageId"] = "";
    
    editDreamSavingCompositeService["finActivityLog_channelId"] = "01";

    //productCodename, frmIBDreamSAvingEdit.lblTargetAmntVal.text,
    // AccountNoDream, frmIBDreamSAvingEdit.lblMnthlySavingVal.text, frmIBDreamSAvingEdit.lbldreamValue.text, ""

    MonthlySavingAmntUpdateIB = frmIBDreamSAvingEdit.lblmnthsaveval.text;
    MonthlySavingAmntUpdateIB = MonthlySavingAmntUpdateIB.replace(/,/g, "").replace("/", "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(kony.i18n.getLocalizedString("keyCalendarMonth"), "");
    var mnthlyTransferDateUpdate = frmIBDreamSAvingEdit.combobox86679531842314.selectedKeyValue[0];
	 if(mnthlyTransferDateUpdate.indexOf("st") > -1)
    {
    	mnthlyTransferDateUpdate=mnthlyTransferDateUpdate.replace("st", "");
    }
    if(mnthlyTransferDateUpdate.indexOf("nd") > -1)
    {
    	mnthlyTransferDateUpdate=mnthlyTransferDateUpdate.replace("nd", "");
    }
    if(mnthlyTransferDateUpdate.indexOf("rd") > -1)
    {
    	mnthlyTransferDateUpdate=mnthlyTransferDateUpdate.replace("rd", "");
    }
    //RecurringFundTransModelUpdate inputs :
    //	alert("recurringToAcctTypeIB"+recurringToAcctTypeIB)

    editDreamSavingCompositeService["RecurringFundTransModelUpdate_clientDt"] = "";
    editDreamSavingCompositeService["RecurringFundTransModelUpdate_accIdFrom"] = recurringToAcctIdIB;
    editDreamSavingCompositeService["RecurringFundTransModelUpdate_accTypeFrom"] = recurringToAcctTypeIB;
    editDreamSavingCompositeService["RecurringFundTransModelUpdate_accIdTo"] = recurringFrmAcctIdIB;
    editDreamSavingCompositeService["RecurringFundTransModelUpdate_accTypeTo"] = recurringFrmAcctTypeIB;
    editDreamSavingCompositeService["RecurringFundTransModelUpdate_curAmt"] = MonthlySavingAmntUpdateIB;
    editDreamSavingCompositeService["RecurringFundTransModelUpdate_dayofMonth"] = mnthlyTransferDateUpdate;
    editDreamSavingCompositeService["RecurringFundTransModelUpdate_freq"] = "Monthly";
    editDreamSavingCompositeService["RecurringFundTransModelUpdate_curCode"] = avaliableCurrCode;
    editDreamSavingCompositeService["RecurringFundTransModelUpdate_frmAcctProdId"] = prodCode;
    editDreamSavingCompositeService["RecurringFundTransModelUpdate_toAcctProdId"] = fromproductID;
    editDreamSavingCompositeService["RecurringFundTransModelUpdate_channel"] = "IB";
    editDreamSavingCompositeService["RecurringFundTransModelUpdate_rqUID"] = "";
    editDreamSavingCompositeService["RecurringFundTransModelUpdate_recXferId"] = recXferId;
    editDreamSavingCompositeService["RecurringFundTransModelUpdate_name"] = "IB-INQ";
    editDreamSavingCompositeService["RecurringFundTransModelUpdate_spName"] = "com.fnf.xes.ST";

    //getDreamDesc inputs


    if (gblDreamflag == "true") {

        gbldreamtargetID = gbltargetIdSelected;
        DreamTargetImage()

    }
    editDreamSavingCompositeService["getDreamDesc_dreamTargetID"] = gbldreamtargetID;
    editDreamSavingCompositeService["updateEditDreamSaveInfo_CRM_ID"] = "";
    editDreamSavingCompositeService["updateEditDreamSaveInfo_PERSONALIZED_ACCT_ID"] = gblAccNo;
    editDreamSavingCompositeService["updateEditDreamSaveInfo_PERSONALIZED_ID"] = gblpersonalizedId;
    var removebhat = frmIBDreamSAvingEdit.lblTargetAmntVal.text;
    var removebhat1 = removebhat.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(/,/g, "");
    editDreamSavingCompositeService["updateEditDreamSaveInfo_DREAM_TARGET_AMOUNT"] = removebhat1;
    editDreamSavingCompositeService["updateEditDreamSaveInfo_PERSONALIZED_ACCT_NICKNAME"] = frmIBDreamSAvingEdit.lblNicknameVal.text.trim();
    editDreamSavingCompositeService["updateEditDreamSaveInfo_DREAM_DESC"] = frmIBDreamSAvingEdit.lblDreamDescVal.text.trim();
    editDreamSavingCompositeService["updateEditDreamSaveInfo_DREAM_TARGET_ID"] = gbldreamtargetID;

    //invokeServiceSecureAsync("updateEditDreamSaveInfo", inputparam, OncalBackDreamSavingUpdateIB);


    //notification

    var removebhatnotification = frmIBDreamSAvingEdit.lblTargetAmntVal.text;
    var newtarget = removebhatnotification.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(/,/g, "");
    var NicknameDreamNotificationIB = frmIBDreamSAvingEdit.lblNicknameVal.text;
    var DreamDescGblNotificationIB = frmIBDreamSAvingEdit.lblDreamDescVal.text;
    var mnthnotification = frmIBDreamSAvingEdit.lblmnthsaveval.text.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace("/", "").replace(kony.i18n.getLocalizedString("keyCalendarMonth"), "").replace(/,/g, "");
    var mnthlyTransferDateNotificationIB = frmIBDreamSAvingEdit.combobox86679531842314.selectedKeyValue[0];

    var deliveryMethod = "Email";
    editDreamSavingCompositeService["NotificationAdd_notificationType"] = deliveryMethod;
    var currentLocale = getCurrentLocale();
    editDreamSavingCompositeService["NotificationAdd_Locale"] = currentLocale;
    editDreamSavingCompositeService["NotificationAdd_source"] = "editDreamSaving";
    //gblCustomerName = resulttable["NotificationAdd_customerName"]=""
    editDreamSavingCompositeService["NotificationAdd_custName"] = gblCustomerName;
    editDreamSavingCompositeService["NotificationAdd_accountNo"] = AccountNoDream; // sunstringing is beinng done at both client and server side have to check
    editDreamSavingCompositeService["NotificationAdd_actno"] = AccountNoDream; //why this is being passed to service
    editDreamSavingCompositeService["NotificationAdd_nickname"] = NicknameDreamNotificationIB;
    editDreamSavingCompositeService["NotificationAdd_DREAM_DESC"] = DreamDescGblNotificationIB;
    editDreamSavingCompositeService["NotificationAdd_targetAmt"] = newtarget;
    editDreamSavingCompositeService["NotificationAdd_monthlySavingAmt"] = mnthnotification;
    editDreamSavingCompositeService["NotificationAdd_monthlyTransferDate"] = mnthlyTransferDateNotificationIB;
    editDreamSavingCompositeService["NotificationAdd_channelName"] = "IB"; // added by considering DEF9140 


    //modifyUpdateUser service params

    editDreamSavingCompositeService["modifyUpdateUser_userStoreId"] = gblUserName;
    editDreamSavingCompositeService["modifyUpdateUser_userId"] = gblUserName;
    editDreamSavingCompositeService["modifyUpdateUser_segmentId"] = "MIB";
    editDreamSavingCompositeService["modifyUpdateUser_loginModuleIdSMS"] = "IBSMSOTP";
    editDreamSavingCompositeService["modifyUpdateUser_loginModuleIdHW"] = "IB_HWTKN";
    editDreamSavingCompositeService["modifyUpdateUser_badLoginCount"] = gblVerifyOTP;
    editDreamSavingCompositeService["modifyUpdateUser_status"] = "";

    invokeServiceSecureAsync("editDreamSavingComposite", editDreamSavingCompositeService, editDreamSavingCompositeCallBackIB);
}

function editDreamSavingCompositeCallBackIB(status, callbackResponse) {
	
	
	//alert(JSON.stringify(callbackResponse));
	var DreamcalculatePercentageConfirmIB = callbackResponse["DreamcalculatePercentageConfirmIB"];
	if (DreamcalculatePercentageConfirmIB != null && DreamcalculatePercentageConfirmIB != "" && DreamcalculatePercentageConfirmIB == "success") 
	{
        dismissLoadingScreenPopup();
        calculateDreamDataIB();
		 gblVerifyOTP = 0
    }
    else
    {
  		
  		if(callbackResponse["opstatus"] == "8005")
  		{
  		   if(callbackResponse["errCode"] == "VrfyOTPErr00001")
  		   {
  		   		gblVerifyOTPCounter = callbackResponse["retryCounterVerifyOTP"];
  		   		dismissLoadingScreenPopup(); 		   		
				//alert("" + kony.i18n.getLocalizedString("invalidOTP"));//commented by swapna
                    frmIBDreamSAvingEdit.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");//kony.i18n.getLocalizedString("invalidOTP"); //
                    frmIBDreamSAvingEdit.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
                    frmIBDreamSAvingEdit.hbxOTPincurrect.isVisible = true;
                    frmIBDreamSAvingEdit.hbxOTPBankRef.isVisible = false;
                    frmIBDreamSAvingEdit.hbxOTPsnt.isVisible = false;
                    frmIBDreamSAvingEdit.txtBxOTP.text = "";
                    frmIBDreamSAvingEdit.tbxToken.text = "";
                    if(gblTokenSwitchFlag == true)
                    frmIBDreamSAvingEdit.tbxToken.setFocus(true);
                    else
                    frmIBDreamSAvingEdit.txtBxOTP.setFocus(true);
				return false;			 
  		   }
  		   else if(callbackResponse["errCode"] == "VrfyOTPErr00002")
  		   {
  		   		dismissLoadingScreenPopup();
				handleOTPLockedIB(callbackResponse);
				return false;
  		   }
  		   else if (callbackResponse["errCode"] == "VrfyOTPErr00005") 
  		   {
                dismissLoadingScreenPopup();
                alert("" + kony.i18n.getLocalizedString("invalidOTP"));
                return false;
           }
           else if (callbackResponse["errCode"] == "VrfyOTPErr00006")
            {
            	dismissLoadingScreenPopup();
                gblVerifyOTPCounter = callbackResponse["retryCounterVerifyOTP"];
                alert("" + callbackResponse["errMsg"]);
                return false;
            }
            else if (callbackResponse["errCode"] == "GenOTPRtyErr00001") 
            {
                dismissLoadingScreenPopup();
                showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                return false;
           	} 
           	else if (callbackResponse["statusDesc"] == "General Error")
           	{    
            	showAlertIB(kony.i18n.getLocalizedString("keyOpenActGenErr"), kony.i18n.getLocalizedString("info"))
        	}
  		   else {
  		            frmIBDreamSAvingEdit.lblOTPinCurr.text = " ";//kony.i18n.getLocalizedString("invalidOTP"); //
                    frmIBDreamSAvingEdit.lblPlsReEnter.text = " "; 
                    frmIBDreamSAvingEdit.hbxOTPincurrect.isVisible = false;
                    frmIBDreamSAvingEdit.hbxOTPBankRef.isVisible = true;
                    frmIBDreamSAvingEdit.hbxOTPsnt.isVisible = true;
                    frmIBDreamSAvingEdit.txtBxOTP.setFocus(true);
  		   		dismissLoadingScreenPopup();
				alert(" "+callbackResponse["errMsg"]);
				return false;
  		   }
  		}
  		else if ( callbackResponse["serverStatusCode"] == "IM0013") 
  		{
	        dismissLoadingScreenPopup();
            showAlertIB(kony.i18n.getLocalizedString("keyCurrentDream"), kony.i18n.getLocalizedString("info"))
        } 
  		else if (callbackResponse["serverStatusCode"] == "TS0789"  )
           	{    	
           		dismissLoadingScreenPopup()
            	showAlertIB(kony.i18n.getLocalizedString("keyOpenActGenErr"), kony.i18n.getLocalizedString("info"))
        	} 
  		else
  			{
  				dismissLoadingScreenPopup();
  				showAlert(kony.i18n.getLocalizedString("keyOpenActGenErr"), kony.i18n.getLocalizedString("info"))
  			}
  	}
}
    //

	/*
    
    var wrongOTP = callbackResponse["wrongOTP"];
    var DreamcalculatePercentageConfirmIB = callbackResponse["DreamcalculatePercentageConfirmIB"];
    if (wrongOTP != null && wrongOTP != "" && wrongOTP == "wrongOTP") {
        showAlertIB(kony.i18n.getLocalizedString("wrongOTP"), kony.i18n.getLocalizedString("info"))
        dismissLoadingScreenPopup();
    } else if (callbackResponse["errMsg"] != null) {
        alert(" " + callbackResponse["errMsg"]);
        dismissLoadingScreenPopup();
    } else if (DreamcalculatePercentageConfirmIB != null && DreamcalculatePercentageConfirmIB != "" && DreamcalculatePercentageConfirmIB == "success") {
        dismissLoadingScreenPopup();
        calculateDreamDataIB();

    } else  {
        var StatusDesc = callbackResponse["statusDesc"];
        if (StatusDesc == "General Error") {
            showAlertIB(kony.i18n.getLocalizedString("keyOpenActGenErr"), kony.i18n.getLocalizedString("info"))
        } else {
            alert(" " + StatusDesc);
        }
         var serverStatusCode = callbackResponse["serverStatusCode"];
        if (serverStatusCode == "IM0013") {
            showAlertIB(kony.i18n.getLocalizedString("keyCurrentDream"), kony.i18n.getLocalizedString("info"))
        }
        
        
        dismissLoadingScreenPopup();
    }*/



function calculateDreamDataIB() {


    frmIBDreamSavingMaintenance.lblAccountNicknameValue.text = frmIBDreamSAvingEdit.lblNicknameVal.text;
    frmIBDreamSavingMaintenance.lblTargetAmount.text = frmIBDreamSAvingEdit.lblTargetAmntVal.text;
    var mnthamount = frmIBDreamSAvingEdit.lblmnthsaveval.text.replace("/", "").replace(kony.i18n.getLocalizedString("keyCalendarMonth"), "");
    frmIBDreamSavingMaintenance.lblDreamDesc.text = frmIBDreamSAvingEdit.lblDreamDescVal.text;
    //alert(mnthamount);
    frmIBDreamSavingMaintenance.lblMnthlySavingAmntVal.text = mnthamount;
    updatedComboValue=  frmIBDreamSAvingEdit.combobox86679531842314.selectedKeyValue[0] 
    var rstmnth="";
                 if(updatedComboValue.charAt("0")=="0"){
                	rstmnth = updatedComboValue.slice(1, 2);
                }else{
               		 rstmnth = updatedComboValue;
                }
    if (updatedComboValue == 1 || updatedComboValue == 21 || updatedComboValue == 31) {
                    frmIBDreamSavingMaintenance.lblTranfrEveryMnthVal.text = rstmnth + "st";
                } else if (updatedComboValue == 2 || updatedComboValue == 22) {
                    frmIBDreamSavingMaintenance.lblTranfrEveryMnthVal.text = rstmnth + "nd";
                } else if (updatedComboValue == 3 || updatedComboValue == 23) {
                    frmIBDreamSavingMaintenance.lblTranfrEveryMnthVal.text = rstmnth + "rd";
                } else {
                    frmIBDreamSavingMaintenance.lblTranfrEveryMnthVal.text = rstmnth + "th";
                }
   
    DreamcalculatePercentageConfirmIB();
    frmIBDreamSavingMaintenance.show();
}

function syncIBDreamSavingMaintainance() {
    if (kony.application.getCurrentForm().id == "frmIBDreamSAvingEdit") {
        frmIBDreamSAvingEdit.label86679531838298.text = kony.i18n.getLocalizedString("keyMBMydream");
        frmIBDreamSAvingEdit.lblNickname.text = kony.i18n.getLocalizedString("keyNickname");
        frmIBDreamSAvingEdit.lblSetSaving.text = kony.i18n.getLocalizedString("kayMBSetNewSaving");
        frmIBDreamSAvingEdit.lblHdrTxtview.text = kony.i18n.getLocalizedString("keylblCalculateMonthlySAving");
        frmIBDreamSAvingEdit.lbltargetamntstudio21.text = kony.i18n.getLocalizedString("keyMbTargetAmnt");//Modified by Studio Viz
    } else if (kony.application.getCurrentForm().id == "frmIBDreamSavingMaintenance") {
        frmIBDreamSavingMaintenance.label866795318717734.text = kony.i18n.getLocalizedString("keyMbTargetAmnt");
        frmIBDreamSavingMaintenance.label866795318717753.text = kony.i18n.getLocalizedString("KeyMBMonthlySavingfrm");
        frmIBDreamSavingMaintenance.button866825109123369.text = kony.i18n.getLocalizedString("keyBackSpa");
        if (kony.i18n.getCurrentLocale() == "en_US") {
            frmIBDreamSavingMaintenance.lblInterestRate.text = gblVarFori18En;
            frmIBDreamSavingMaintenance.lblBranchVal.text = branchName;
        } else {
            frmIBDreamSavingMaintenance.lblInterestRate.text = gblVarFori18Th;
            frmIBDreamSavingMaintenance.lblBranchVal.text = branchNameTh;
        }
    }
}


function myAccountDreamDeleteIB() {
    showLoadingScreenPopup();
    var mydream_inputparam = {}

    //  mydream_inputparam["personalizedId"] = gblpersonalizedId; 
    mydream_inputparam["personalizedAcctId"] = gblPersonalizedRecordIdFromDreamSavingsIB;
    //   mydream_inputparam["recordPersonalizedId"] = gblpersonalizedId;
    mydream_inputparam["acctNickName"] = frmIBDreamSavingMaintenance.lblAccountNicknameValue.text
    mydream_inputparam["bankCD"] = "11";
    var Number = mydream_inputparam["personalizedAcctId"];
    var NickName = mydream_inputparam["acctNickName"];
    mydream_inputparam["acctStatus"] = "";
    mydream_inputparam["BankName"] = "TMB";
    mydream_inputparam["Number"] = Number;
    invokeServiceSecureAsync("MyAccountDeleteService", mydream_inputparam, myAccountDelServiceCallBackIB);
}

function openFrmBranch(){
var inputParams = {};

				inputParams["dreamTargetId"] ="";
				//inputParams["dreamDesc"] =frmIBDreamSAvingEdit.lblDreamDescVal.text.trim();
				inputParams["dreamDesc"] ="";
				inputParams["personalizedActId"] =gblAccNo;
				//var tarAmountDream = frmIBDreamSAvingEdit.lblTargetAmntVal.text;
			    inputParams["dreamTargetAmnt"] = "0";
				inputParams["flowIdOpenAct"] = "dreamSaving";
				inputParams["personalizedId"] = "";
				inputParams["channelId"] = "01";
        	invokeServiceSecureAsync("getDreamActInfo", inputParams, BranchService);

}
function BranchService(status, resulttable) {
   //dismissLoadingScreenPopup();
var personalizedid = "";
var personalizedId = "";
var zeroPersonalizedId="";
var inputParams = {};
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
        zeroPersonalizedId = resulttable["zeroPersonalizedId"];
        personalizedid = resulttable["PERSONALIZED_ID"];
        personalizedId = resulttable["personalizedId"];
        var lengthId = zeroPersonalizedId.length;
        var DreamlengthId = personalizedid.length;
          if(lengthId == 0 && DreamlengthId ==0)
          {
				inputParams["dreamTargetId"] ="";
				//inputParams["dreamDesc"] =frmIBDreamSAvingEdit.lblDreamDescVal.text.trim();
				inputParams["dreamDesc"] ="";
				inputParams["personalizedActId"] =gblAccNo;
				//var tarAmountDream = frmIBDreamSAvingEdit.lblTargetAmntVal.text;
			    //inputParams["dreamTargetAmnt"] = tarAmountDream.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(/,/g, "");
				inputParams["dreamTargetAmnt"]="";
				inputParams["flowIdOpenAct"] = "dreamSaving";
				inputParams["personalizedId"] = personalizedId;
				inputParams["channelId"] = "01";
				invokeServiceSecureAsync("addDreamSavingCareDataOpenAct", inputParams, callBackServiceDream);
			}
        }
    }
}
function callBackServiceDream(status, resulttable)
{
	dismissLoadingScreenPopup();
	if (status == 400) 
	{
	        if (resulttable["opstatus"] == 0) 
	        {
	        }
	}
}

function saveAmountinSession(){
    
     var removeSybolTarget = frmIBDreamSAvingEdit.lblTargetAmntVal.text;
    removeSybolTarget = removeSybolTarget.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(/,/g, "");
    
    var MonthlySavingAmntIB = frmIBDreamSAvingEdit.lblmnthsaveval.text;
    MonthlySavingAmntIB = MonthlySavingAmntIB.replace(/,/g, "").replace("/", "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(kony.i18n.getLocalizedString("keyCalendarMonth"), "");
    
    inputParam = {};
	
	inputParam["DreamMonthlyAmnt"] = MonthlySavingAmntIB;						//removeCommas(txtBalMaxVal);
	inputParam["DreamTargetAmnt"] = removeSybolTarget;
		inputParam["DreamFromAccount"] = recurringToAcctIdIB.toString();
		inputParam["DreamToAccount"] = recurringFrmAcctIdIB.toString();
										//removeCommas(txtBalMinVal);
	
	//added below activity logging lines to fix DEF11 UAT defect
 	    
    inputParam["activityFlexValues1"] = prodCode + prodName
    if((oldTargetAmnt)== (removeSybolTarget)){
    	inputParam["activityFlexValues2"] = removeSybolTarget;
    }else{
    	inputParam["activityFlexValues2"] = oldTargetAmnt + "+"+removeSybolTarget;
    }
    inputParam["activityFlexValues3"] = AccountNoDream;
    if( oldMnthlyAmnt ==  (MonthlySavingAmntIB)){
        inputParam["activityFlexValues4"] = MonthlySavingAmntIB;
    }else{
        inputParam["activityFlexValues4"] = oldMnthlyAmnt+" + "+MonthlySavingAmntIB;
    }
    if( oldRecurringDate ==  frmIBDreamSAvingEdit.combobox86679531842314.selectedKeyValue[0]){
    	inputParam["activityFlexValues5"] = frmIBDreamSAvingEdit.combobox86679531842314.selectedKeyValue[0]
    }else{
    	inputParam["activityFlexValues5"] = oldRecurringDate+" + "+ frmIBDreamSAvingEdit.combobox86679531842314.selectedKeyValue[0]
    }        
    
	invokeServiceSecureAsync("EditDreamSavingCheck", inputParam, saveInputinSessioncallBackIBDream);
}
function saveInputinSessioncallBackIBDream(status,result){
    if (status == 400) {
		
		
		if (result["opstatus"] == "0") {
		
			if (gblSwitchToken == false && gblTokenSwitchFlag == false) {
		    IBDScheckTokenFlag();
		    } else if (gblSwitchToken == false && gblTokenSwitchFlag == true) {
		        showingDSToken();
		        frmIBDreamSAvingEdit.hbxToken.setVisibility(true);
		        frmIBDreamSAvingEdit.hbxOTPEntry.setVisibility(false);
		    } else if (gblSwitchToken == true) {
		
		        callOTPServiceDSMIB();
		    }
		    frmIBDreamSAvingEdit.txtBxOTP.setFocus(true);
		    frmIBDreamSAvingEdit.tbxToken.setFocus(true);
			
			}
		}
}
