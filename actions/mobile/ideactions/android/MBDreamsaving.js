var dsSelActSelIndex = [0, 0];
var dsSelActLength = 0;
//mnthlysavingSymbol1 = "";
function onClickConnectDMSSeg() {
    var currForm = kony.application.getCurrentForm()
    dsSelActSelIndex = currForm.segSlider.selectedIndex;
}
var MnthTODream;
var mnthlySavingEdit
var targetAmntVal;

function calMonthlySavingCalculator() {
    var tarAmount = myTargetAmnt
    if (tarAmount != null && tarAmount != "" && tarAmount.indexOf(".") != -1) {
        var tarAmount1 = tarAmount.split(".");
        var tarAmount2 = tarAmount1.length - 1;
    }
    var tarAmount = tarAmount.replace(/,/g, "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    var noOfMnths = frmDreamCalculator.lblMnthTODReamVal.text;
    noOfMnths = noOfMnths.replace(kony.i18n.getLocalizedString("keymonths"), "");
    /*if (gblDreamMnths == noOfMnths){
 
 return false;
 }*/
    if (noOfMnths != null && noOfMnths != "" && noOfMnths.indexOf(".") != -1) {
        var indexMnt = noOfMnths.indexOf(".");
    } else {
        var indexMnt = -1;
    }
    //if(indexMnt > 0 || kony.string.isNumeric(noOfMnths) ==false )
    //		{	
    //			showAlert(kony.i18n.getLocalizedString("keyenterMnths"), kony.i18n.getLocalizedString("info"));
    //			 return false;
    //			 
    //		}
    var entAmount;
    tarAmount = kony.os.toNumber(tarAmount);
    noOfMnths = kony.os.toNumber(noOfMnths);
    entAmount = tarAmount / noOfMnths;
    entAmount = entAmount + "";
    entAmount = entAmount.trim();
    if (entAmount != null && entAmount != "" && entAmount.indexOf(".") != -1) {
        var indexdot = entAmount.indexOf(".");
    } else {
        var indexdot = -1;
    }
    var decimal = "";
    var remAmt = "";
    if (indexdot > 0) {
        decimal = entAmount.substr(indexdot, 3);
        remAmt = entAmount.substr(0, indexdot);
        entAmount = remAmt + decimal;
    }
    //noOfMnths = Math.round(noOfMnths);
    //if(frmDreamCalculator.txtODMnthSavAmt.text == ""|| frmDreamCalculator.txtODMnthSavAmt.text == null)	{
    frmDreamCalculator.lblMnthlySavingVal.text = commaFormattedOpenAct(entAmount) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
    StringnoOfMnths = frmDreamCalculator.lblMnthTODReamVal.text;
    StringnoOfMnths = StringnoOfMnths.trim();
    if (StringnoOfMnths == null || StringnoOfMnths == "" || StringnoOfMnths == "0") {
        frmDreamCalculator.lblMnthlySavingVal.text = "";
    } else {
        frmDreamCalculator.lblMnthTODReamVal.text = frmDreamCalculator.lblMnthTODReamVal.text + kony.i18n.getLocalizedString("keymonths");
    }
    //}
}
//*************************************
function calculateMnthCalculator() {
    var tarAmount = frmDreamCalculator.lblTargetAmount.text;
    tarAmount = tarAmount.replace(/,/g, "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    var num = new Number(tarAmount.trim())
    var entAmount = frmDreamCalculator.lblMnthlySavingVal.text;
    entAmount = entAmount.replace(/,/g, "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    var num1 = new Number(entAmount)
    var stringstr = entAmount;
    if (entAmount != null && entAmount != "" && entAmount.indexOf("0") == 0) {
        //showAlert(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), kony.i18n.getLocalizedString("info"));
        frmDreamCalculator.lblMnthlySavingVal.skin = txtErrorBG;
        frmDreamCalculator.lblMnthTODReamVal.text = "";
        return false
    }
    if (num1 > num) {
        frmDreamCalculator.lblMnthTODReamVal.text = "";
        showAlert(kony.i18n.getLocalizedString("keyDreamTargetAlert"), kony.i18n.getLocalizedString("info"));
        frmDreamCalculator.lblMnthlySavingVal.skin = txtErrorBG;
        return false;
    }
    entAmount = entAmount.trim();
    if (entAmount != null && entAmount != "" && entAmount.indexOf(".") != -1) {
        var indexdot = entAmount.indexOf(".");
    } else {
        var indexdot = -1;
    }
    //var indexdot = entAmount.indexOf(".");
    var decimal = "";
    var remAmt = "";
    if (indexdot > 0) {
        decimal = entAmount.substr(indexdot);
        if (decimal.length > 3) {
            alert("Enter only 2 decimal values");
            return false;
        }
    }
    if (!kony.string.isNumeric(num1)) {
        // frmDreamSavingEdit.lblTargetAmntVal.text = "";
        frmDreamCalculator.lblMnthlySavingVal.skin = txtErrorBG;
        showAlert(kony.i18n.getLocalizedString("keydreamIncorrectMnthlySavingAmnt"), kony.i18n.getLocalizedString("info"));
        return false;
    }
    var stringstr1 = entAmount.replace(/,/g, "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    if (stringstr1 == null || stringstr1 == "") {
        frmDreamCalculator.lblMnthlySavingVal.text = "";
    } else {
        frmDreamCalculator.lblMnthlySavingVal.text = commaFormattedOpenAct(stringstr1) + kony.i18n.getLocalizedString("currencyThaiBaht");
    }
    tarAmount = kony.os.toNumber(tarAmount);
    stringstr = kony.os.toNumber(entAmount);
    var noOfMnths;
    if (stringstr1 == "") {
        frmDreamCalculator.lblMnthTODReamVal.text = "";
    } else {
        noOfMnths = tarAmount / stringstr1;
        noOfMnths = Math.ceil(noOfMnths);
        noOfMnths = noOfMnths + "";
        //if(frmOpenAcDreamSaving.txtODMyDream.text == "" || frmOpenAcDreamSaving.txtODMyDream.text == null){
        frmDreamCalculator.lblMnthTODReamVal.text = "";
        frmDreamCalculator.lblMnthTODReamVal.text = noOfMnths + kony.i18n.getLocalizedString("keymonths")
            //frmDreamCalculator.lblMnthlySavingVal.text = frmDreamCalculator.lblMnthlySavingVal.text+ " " + kony.i18n.getLocalizedString("currencyThaiBaht");
        var StringnOfMnthSaving = frmDreamCalculator.lblMnthlySavingVal.text;
        StringnOfMnthSaving = StringnOfMnthSaving.trim();
        StringnOfMnthSaving = StringnOfMnthSaving.replace(/,/g, "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
        frmDreamCalculator.lblMnthlySavingVal.text = commaFormattedOpenAct(StringnOfMnthSaving) + kony.i18n.getLocalizedString("currencyThaiBaht");
        gblDreamMnths = noOfMnths;
    }
}
//****************************
//function textChangeSymbol(){
//	var splitIndex=frmDreamSavingEdit.lblTargetAmntVal.text.indexOf(".");
//	frmDreamSavingEdit.lblTargetAmntVal.text =commaFormattedOpenAct(frmDreamSavingEdit.lblTargetAmntVal.text.substring(0, splitIndex-1))+" "+kony.i18n.getLocalizedString("currencyThaiBaht");
//}
function checkSymbol() {
    var targetNo = "";
    var stringstr = "";
    stringstr = frmDreamSavingEdit.lblTargetAmntVal.text;
    /*	if (stringstr == null || stringstr == "") {
        showAlert(kony.i18n.getLocalizedString("keyPleaseEnterAmount"), kony.i18n.getLocalizedString("info"));
        frmDreamSavingEdit.lblTargetAmntVal.skin = txtErrorBG;
        return false;
    }*/
    if (stringstr != null && stringstr != "" && stringstr.indexOf("0") == 0) {
        showAlert(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), kony.i18n.getLocalizedString("info"));
        frmDreamSavingEdit.lblTargetAmntVal.skin = txtErrorBG;
        return false
    }
    if (stringstr != null && stringstr != "" && stringstr.indexOf(".") != -1) {
        var dotcount = stringstr.split(".");
        var lendot = dotcount.length - 1;
        if (lendot > 1) {
            showAlert(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), kony.i18n.getLocalizedString("info"));
            frmDreamSavingEdit.lblTargetAmntVal.skin = txtErrorBG;
            return false;
        }
    }
    if (stringstr != null && stringstr != "" && stringstr.indexOf(",") != -1) {
        stringstr = stringstr.replace(/,/g, "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
        targetNo = new Number(stringstr.trim())
    }
    if (stringstr == null || stringstr == "" || !kony.string.isNumeric(stringstr)) {
        //frmDreamSavingEdit.lblTargetAmntVal.text = frmDreamSavingEdit.lblTargetAmntVal.text
    } else {
        frmDreamSavingEdit.lblTargetAmntVal.text = commaFormattedOpenAct(stringstr) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
    }
}

function checkSymbolMonthly() {
    var mnthlysavingSymbol1 = "";
    var mnthlysavingSymbol = "";
    mnthlysavingSymbol = frmDreamSavingEdit.lblSetSavingVal.text;
    /*if (mnthlysavingSymbol == null || mnthlysavingSymbol == "") {
        showAlert(kony.i18n.getLocalizedString("keyPleaseEnterAmount"), kony.i18n.getLocalizedString("info"));
        frmDreamSavingEdit.lblSetSavingVal.skin = txtErrorBG;
        return false;
    }*/
    if (mnthlysavingSymbol != null && mnthlysavingSymbol != "" && mnthlysavingSymbol.indexOf("0") == 0) {
        showAlert(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), kony.i18n.getLocalizedString("info"));
        frmDreamSavingEdit.lblSetSavingVal.skin = txtErrorBG;
        return false
    }
    if (mnthlysavingSymbol != null && mnthlysavingSymbol != "" && mnthlysavingSymbol.indexOf(".") != -1) {
        var mnthlysavingSymboldotcount = mnthlysavingSymbol.split(".");
        var mnthlysavingSymbollendot = mnthlysavingSymboldotcount.length - 1;
        if (mnthlysavingSymbollendot > 1) {
            showAlert(kony.i18n.getLocalizedString("keydreamIncorrectMnthlySavingAmnt"), kony.i18n.getLocalizedString("info"));
            frmDreamSavingEdit.lblSetSavingVal.skin = txtErrorBG;
            return false;
        }
    }
    if (mnthlysavingSymbol != null && mnthlysavingSymbol.trim() != "" && mnthlysavingSymbol.indexOf("/") != -1) {
        mnthlysavingSymbol = mnthlysavingSymbol.replace(/,/g, "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace("/", "").replace(kony.i18n.getLocalizedString("keyCalendarMonth"), "")
        mnthlysavingSymbol1 = mnthlysavingSymbol.trim();
    } else {
        mnthlysavingSymbol1 = mnthlysavingSymbol;
    }
    var num = new Number(mnthlysavingSymbol1);
    if (mnthlysavingSymbol == null || mnthlysavingSymbol == "" || !kony.string.isNumeric(num)) {} else {
        frmDreamSavingEdit.lblSetSavingVal.text = commaFormattedOpenAct(mnthlysavingSymbol1) + " " + kony.i18n.getLocalizedString("currencyThaiBaht") + "/" + kony.i18n.getLocalizedString("keyCalendarMonth")
    }
}

function Calculatebtn() {
    var nickNameTxt = frmDreamSavingEdit.lblNicknameVal.text;
    var myDreamTargetAMnt = frmDreamSavingEdit.lblTargetAmntVal.text;
    var myDreamTargetAMnt = myDreamTargetAMnt.replace(/,/g, "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "")
    var DreamDesc = frmDreamSavingEdit.lblDreamDescVal.text;
    MnthlySAvingAmnt = frmDreamSavingEdit.lblSetSavingVal.text;
    var MnthlySAvingAmnt = MnthlySAvingAmnt.replace(/,/g, "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace("/", "").replace(kony.i18n.getLocalizedString("keyCalendarMonth"), "");
    var num = new Number(myDreamTargetAMnt.trim())
    var num1 = new Number(MnthlySAvingAmnt.trim())
    if (!(benNameAlpNumValidation(nickNameTxt))) {
        showAlert(kony.i18n.getLocalizedString("keyincorrectNickName"), kony.i18n.getLocalizedString("info"));
        return false;
    }
    if (nickNameTxt == null || nickNameTxt == "") {
        showAlert(kony.i18n.getLocalizedString("Valid_BillerNicknameMandatory"), kony.i18n.getLocalizedString("info"))
        return false;
    }
    if (DreamDesc == null || DreamDesc == "") {
        showAlert(kony.i18n.getLocalizedString("keyinputdreamDesy"), kony.i18n.getLocalizedString("info"))
        return false;
    }
    if (!(benNameAlpNumValidation(nickNameTxt))) {
        alert("Dream Description  should be alphanumeric or Should not  Empty");
        return false;
    }
    if (myDreamTargetAMnt == null || myDreamTargetAMnt == "") {
        frmDreamSavingEdit.lblTargetAmntVal.skin = txtErrorBG;
        frmDreamSavingEdit.lblTargetAmntVal.text = "";
        showAlert(kony.i18n.getLocalizedString("keyTargetAmt"), kony.i18n.getLocalizedString("info"));
        return false;
    }
    if (!kony.string.isNumeric(num)) {
        frmDreamSavingEdit.lblTargetAmntVal.skin = txtErrorBG;
        showAlert(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), kony.i18n.getLocalizedString("info"));
        return false;
    }
    if (MnthlySAvingAmnt == null || MnthlySAvingAmnt == "") {
        frmDreamSavingEdit.lblSetSavingVal.skin = txtErrorBG;
        showAlert(kony.i18n.getLocalizedString("keydreamIncorrectMnthlySavingAmnt"), kony.i18n.getLocalizedString("info"));
        return false;
    }
    if (!kony.string.isNumeric(num1)) {
        showAlert(kony.i18n.getLocalizedString("keydreamIncorrectMnthlySavingAmnt"), kony.i18n.getLocalizedString("info"));
        frmDreamSavingEdit.lblSetSavingVal.skin = txtErrorBG;
        return false;
    }
    if (myDreamTargetAMnt != null && myDreamTargetAMnt != "" && myDreamTargetAMnt.indexOf(".") != -1) {
        var indexdot = myDreamTargetAMnt.indexOf(".");
    } else {
        var indexdot = -1;
    }
    var decimal = "";
    var remAmt = "";
    if (indexdot > 0) {
        decimal = myDreamTargetAMnt.substr(indexdot);
        if (decimal.length > 4) {
            frmDreamSavingEdit.lblTargetAmntVal.skin = txtErrorBG;
            alert("Enter only 2 decimal values");
            return false;
        }
    }
    /*	if ((num) < (kony.os.toNumber(gblMinDreamAmt))){
    		showAlert(kony.i18n.getLocalizedString("keyLessTargetAmt"), kony.i18n.getLocalizedString("info"));
    		frmDreamSavingEdit.lblTargetAmntVal.skin = txtErrorBG;
    		return false				
    	}
    	
    	
    		if ((num1) < (kony.os.toNumber(gblMinDreamAmt))){
    		showAlert(kony.i18n.getLocalizedString("keydreamminimumAmount"), kony.i18n.getLocalizedString("info"));
    		frmDreamSavingEdit.lblSetSavingVal.skin = txtErrorBG;
    		return false				
    	}
    				
    	if (gblMaxDreamAmt != "No Limit")
    		{
    			if ((num1) > (kony.os.toNumber(gblMaxDreamAmt))){
    			showAlert(kony.i18n.getLocalizedString("keydreamminimumAmount"), kony.i18n.getLocalizedString("info"));
    			 frmDreamSavingEdit.lblSetSavingVal.skin = txtErrorBG;
    			return false				
    			}
    				
    	}*/
    if (num1 > num) {
        showAlert(kony.i18n.getLocalizedString("keyDreamTargetAlert"), kony.i18n.getLocalizedString("info"));
        return false;
    }
    if (myDreamTargetAMnt != null && myDreamTargetAMnt != "" && myDreamTargetAMnt.indexOf(".") != -1) {
        var dotcountCalculte = myDreamTargetAMnt.split(".");
        var lendotCalculate = dotcountCalculte.length - 1;
        if (lendotCalculate > 1) {
            showAlert(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), kony.i18n.getLocalizedString("info"));
            frmDreamSavingEdit.lblTargetAmntVal.skin = txtErrorBG;
            return false;
        }
    }
    if ((kony.os.toNumber(myDreamTargetAMnt)) < (kony.os.toNumber(gblMinDreamAmt))) {
        //showAlert("Please enter minimum target amount to proceed.", kony.i18n.getLocalizedString("info"));
        showAlert(kony.i18n.getLocalizedString("keyLessTargetAmt"), kony.i18n.getLocalizedString("info"));
        frmDreamSavingEdit.lblTargetAmntVal.skin = txtErrorBG;
        return false
    }
    // alert("myDreamTargetAMnt::"+myDreamTargetAMnt)
    if (MnthlySAvingAmnt != null && MnthlySAvingAmnt != "" && MnthlySAvingAmnt.indexOf(".") != -1) {
        var dotcountMnthlySAvingAmnt = MnthlySAvingAmnt.split(".");
        var lendotMnthlySAvingAmnt = dotcountMnthlySAvingAmnt.length - 1;
        if (lendotMnthlySAvingAmnt > 1) {
            showAlert(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), kony.i18n.getLocalizedString("info"));
            frmDreamSavingEdit.lblSetSavingVal.skin = txtErrorBG;
            return false;
        }
    }
    //mnthlySavingEditcal = MnthlySAvingAmnt.replace("฿/Months", "").trim();
    //
    frmDreamCalculator.lblMnthlySavingVal.text = commaFormattedOpenAct(MnthlySAvingAmnt) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
    var formatedAccount = myDreamTargetAMnt
    frmDreamCalculator.lblTargetAmount.text = commaFormattedOpenAct(formatedAccount) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");;
    myTargetAmnt = frmDreamCalculator.lblTargetAmount.text
    myTargetAmnt = myTargetAmnt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    //alert(myTargetAmnt);
    calculatemnth();
}

function calculatemnth() {
    tarAmount = frmDreamSavingEdit.lblTargetAmntVal.text;
    if (tarAmount != null && tarAmount != "" && tarAmount.indexOf(".") != -1) {
        var tarAmountdotcount = tarAmount.split(".");
        var tarAmountlendot = tarAmountdotcount.length - 1;
        if (tarAmountlendot > 1) {
            showAlert(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), kony.i18n.getLocalizedString("info"));
            frmDreamSavingEdit.lblTargetAmntVal.skin = txtErrorBG;
            return false;
        }
    }
    var tarAmount = tarAmount.replace(/,/g, "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    MnthlySAvingAmnt = frmDreamSavingEdit.lblSetSavingVal.text;
    if (MnthlySAvingAmnt != null && MnthlySAvingAmnt != "" && MnthlySAvingAmnt.indexOf(".") != -1) {
        var MnthlySAvingAmntdotcount = MnthlySAvingAmnt.split(".");
        var MnthlySAvingAmntlendot = MnthlySAvingAmntdotcount.length - 1;
        if (MnthlySAvingAmntlendot > 1) {
            showAlert(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), kony.i18n.getLocalizedString("info"));
            frmDreamSavingEdit.lblSetSavingVal.skin = txtErrorBG;
            return false;
        }
    }
    var MnthlySAvingAmnt = MnthlySAvingAmnt.replace(/,/g, "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace("/", "").replace(kony.i18n.getLocalizedString("keyCalendarMonth"), "")
    entAmount = MnthlySAvingAmnt;
    entAmount = entAmount.trim();
    if (entAmount != null && entAmount != "" && entAmount.indexOf(".") != -1) {
        var indexdot = entAmount.indexOf(".");
    } else {
        var indexdot = -1;
    }
    var decimal = "";
    var remAmt = "";
    if (indexdot > 0) {
        decimal = entAmount.substr(indexdot);
        if (decimal.length > 3) {
            frmDreamSavingEdit.lblSetSavingVal.skin = txtErrorBG;
            alert("Enter only 2 decimal values");
            return false;
        }
    }
    var noOfMnths;
    tarAmount = kony.os.toNumber(tarAmount);
    entAmount = kony.os.toNumber(entAmount);
    //alert("entAmount::"+entAmount +"tarAmount ::"+tarAmount)
    noOfMnths = tarAmount / entAmount;
    noOfMnths = Math.ceil(noOfMnths);
    noOfMnths = noOfMnths + "";
    //if(frmOpenAcDreamSaving.txtODMyDream.text == "" || frmOpenAcDreamSaving.txtODMyDream.text == null){
    frmDreamCalculator.lblMnthTODReamVal.text = "";
    frmDreamCalculator.lblMnthTODReamVal.text = noOfMnths + kony.i18n.getLocalizedString("keymonths");
    gblDreamMnths = noOfMnths;
    //}
    frmDreamCalculator.show();
}
//function editDataDSM(){
//	targetAmoutDreamedit = frmDreamSavingMB.lblTargetAmount.text;
//	frmDreamSavingEdit.lblNicknameVal.text = frmDreamSavingMB.lblAccountNicknameValue.text;
//	frmDreamSavingEdit.lblDreamDescVal.text = frmDreamSavingMB.lblDreamDesc.text;
//	frmDreamSavingEdit.lblTargetAmntVal.text = targetAmoutDreamedit
//	frmDreamSavingEdit.btnDreamSavecombo.text = frmDreamSavingMB.lblTranfrEveryMnthVal.text;
//	frmDreamSavingEdit.lblSetSavingVal.text = "";
//}
//function clearDataDSM(){
//
//	frmDreamSavingEdit.lblDreamDescVal.text = "";
//	frmDreamSavingEdit.lblNicknameVal.text = "";
//
//	frmDreamSavingEdit.lblTargetAmntVal.text = "";
//	frmDreamSavingEdit.btnDreamSavecombo.text = kony.i18n.getLocalizedString("keyMBTransferEvryMnth");
//}
function calculateConfirm() {
    var tarAmount = frmDreamCalculator.lblTargetAmount.text;
    tarAmount = tarAmount.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(/,/g, "");
    var noOfMnths = frmDreamCalculator.lblMnthTODReamVal.text;
    noOfMnths = noOfMnths.replace(kony.i18n.getLocalizedString("keymonths"), "");
    var entAmount = frmDreamCalculator.lblMnthlySavingVal.text;
    entAmount = entAmount.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(/,/g, "")
    var targetnumber = new Number(tarAmount.trim());
    var mounthlynumber = new Number(entAmount.trim());
    var entAmountDreamMB = frmDreamCalculator.lblMnthTODReamVal.text;
    if (entAmount == "0") {
        showAlert(kony.i18n.getLocalizedString("keydreamminimumAmount"), kony.i18n.getLocalizedString("info"));
        frmDreamCalculator.lblMnthlySavingVal.skin = txtErrorBG;
        return false
    }
    if (entAmount == "" || entAmount == null || noOfMnths == "" || noOfMnths == null) {
        frmDreamCalculator.lblMnthTODReamVal.text = "";
        frmDreamCalculator.lblMnthlySavingVal.skin = txtErrorBG;
        showAlert(kony.i18n.getLocalizedString("keydreamMnthlySavingANdMnthToDream"), kony.i18n.getLocalizedString("info"));
        return false;
    }
    if (!kony.string.isNumeric(mounthlynumber)) {
        frmDreamCalculator.lblMnthTODReamVal.text = "";
        frmDreamCalculator.lblMnthlySavingVal.skin = txtErrorBG;
        showAlert(kony.i18n.getLocalizedString("keydreamIncorrectMnthlySavingAmnt"), kony.i18n.getLocalizedString("info"));
        return false;
    }
    if (noOfMnths == "" || noOfMnths == null) {
        frmDreamCalculator.lblMnthlySavingVal.text = "";
        frmDreamCalculator.lblMnthTODReamVal.skin = txtErrorBG;
        showAlert(kony.i18n.getLocalizedString("keydreamMnthlySavingANdMnthToDream"), kony.i18n.getLocalizedString("info"));
        return false;
    }
    if ((mounthlynumber) < (kony.os.toNumber(gblMinDreamAmt))) {
        frmDreamCalculator.lblMnthTODReamVal.text = "";
        frmDreamCalculator.lblMnthlySavingVal.skin = txtErrorBG;
        showAlert(kony.i18n.getLocalizedString("keydreamminimumAmount"), kony.i18n.getLocalizedString("info"));
        return false;
    }
    if (gblMaxDreamAmt != "No Limit") {
        if ((mounthlynumber) > (kony.os.toNumber(gblMaxDreamAmt))) {
            showAlert(kony.i18n.getLocalizedString("keydreamminimumAmount"), kony.i18n.getLocalizedString("info"));
            frmDreamCalculator.lblMnthlySavingVal.skin = txtErrorBG;
            return false;
        }
    }
    if (entAmountDreamMB != null && entAmountDreamMB != "" && entAmountDreamMB.indexOf(".") != -1) {
        var entAmountindexMB = entAmountDreamMB.indexOf(".");
    } else {
        var entAmountindexMB = -1;
    }
    if (entAmountindexMB > 0) {
        frmDreamCalculator.lblMnthTODReamVal.text = "";
        showAlert(kony.i18n.getLocalizedString("keyValidMonths"), kony.i18n.getLocalizedString("info"));
        return false;
    }
    if (frmDreamCalculator.lblMnthTODReamVal.text == "0" || frmDreamCalculator.lblMnthTODReamVal.text == "") {
        frmDreamCalculator.lblMnthTODReamVal.text = "";
        showAlert(kony.i18n.getLocalizedString("keyValidMonths"), kony.i18n.getLocalizedString("info"));
        return false;
    }
    if (mounthlynumber > targetnumber) {
        frmDreamCalculator.lblMnthTODReamVal.text = "";
        frmDreamSavingEdit.lblSetSavingVal.skin = txtErrorBG;
        showAlert(kony.i18n.getLocalizedString("keyMonthlyLessTarget"), kony.i18n.getLocalizedString("info"));
        return false;
    }
    if (noOfMnths != null && noOfMnths != "" && noOfMnths.indexOf(".") != -1) {
        var indexMnt = noOfMnths.trim().indexOf(".");
    } else {
        var indexMnt = -1;
    }
    /*if(indexMnt > 0 || kony.string.isNumeric(noOfMnths.trim()) == false )
	{	
			 showAlert(kony.i18n.getLocalizedString("keyenterMnths"), kony.i18n.getLocalizedString("info"));
			 return false;
	}*/
    entAmount = entAmount.trim();
    if (entAmount != null && entAmount != "" && entAmount.indexOf(".") != -1) {
        var indexdot = entAmount.indexOf(".");
    } else {
        var indexdot = -1;
    }
    var decimal = "";
    var remAmt = "";
    if (indexdot > 0) {
        decimal = entAmount.substr(indexdot);
        if (decimal.length > 3) {
            alert("Enter only 2 decimal values");
            return false;
        }
    }
    /*if (gblDreamMnths == noOfMnths)
 {
	 
	 return false;
 }*/
    if (flowSpa) {
        gblcarouselwidgetflow = "Savings"
    }
    calMonthlySavingCalculator()
    frmDreamSavingEdit.lblSetSavingVal.text = frmDreamCalculator.lblMnthlySavingVal.text + "/" + kony.i18n.getLocalizedString("keyCalendarMonth");
    frmDreamSavingEdit.show();
}
//function calldelete() {
//	
//	popupdeleteDreamSave("delico.png", "", ConfirmBtn, CancelBtnDream);
//}
function popupdeleteDreamSave(imgIcon, confText, callBackOnConfirm, callBackCancel) {
    //popupConfirmation.imgPopConfirmIcon.src = imgIcon;
    //popupConfirmation.containerHeight = 40;
    //popupConfirmation.containerHeightReference = constants.HEIGHT_BY_DEVICE_REFERENCE;
    popupConfirmation.lblPopupConfText.text = kony.i18n.getLocalizedString("keyMBDreamDeleteAcnt");
    popupConfirmation.btnpopConfConfirm.onClick = callBackOnConfirm;
    popupConfirmation.btnPopupConfCancel.onClick = callBackCancel;
    popupConfirmation.btnPopupConfCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
    popupConfirmation.btnpopConfConfirm.text = kony.i18n.getLocalizedString("keyConfirm");
    popupConfirmation.show();
}

function DreamshowDateDreamSav() {
    var SelDate = [];
    for (var i = 1; i <= 31; i++) {
        i = i + "";
        //i = kony.string.split(i, ".")
        //i =i[0];	
        if (i < 10) {
            i = "0" + i;
        }
        i = i.toString();
        var temp = {
            "lblDreamSaveDate": i,
            "imgDreamSaveDate": "bg_arrow_right_grayb.png"
        }
        kony.table.insert(SelDate, temp)
    }
    popSelDate.segDreamSaveDate.data = SelDate;
    showPopSelDate(DreamonclickSelDateOnPop);
}

function DreamonclickSelDateOnPop() {
    frmDreamSavingEdit.btnDreamSavecombo.text = popSelDate.segDreamSaveDate.selectedItems[0].lblDreamSaveDate;
    popSelDate.dismiss();
}

function afterLogoutMBInPreshowSnippetCode() {
    kony.print("preshow called");
    isMenuShown = false;
    // Commented this code coz of QB and Menu integration(Pre login)
    //frmMBPreLoginAccessesPin.scrollboxMain.scrollToEnd();
    popupTractPwd.dismiss();
    //	if((null != gblDeviceInfo.name) && (kony.string.equalsIgnoreCase("android", gblDeviceInfo.name)))
    //	{
    //		frmAfterLogoutMB.tbxAccessPIN.keyBoardStyle = constants.TEXTBOX_NUMERIC_PASSWORD;
    //	}
    frmMBPreLoginAccessesPinPreShow();
}