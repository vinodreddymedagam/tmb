//function passwordChangedIB() {
//	frmIBCreateUserID.lblPasswdStrength.setVisibility(true);
//	//	alert("Text changed")
//	var strongRegex = new RegExp("^(?=.{9,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$", "g");
//	var mediumRegex = new RegExp("^(?=.{8,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$", "g");
//	//	var enoughRegex = new RegExp("(?=.{6,}).*", "g");
//	var pwd = frmIBCreateUserID.txtPassword.text;
//	if (pwd != null && pwd.length < 2) {
//		frmIBCreateUserID.lblPasswdStrength.setVisibility(false);
//	}
//	else {
//		if (strongRegex.test(pwd)) {
//			frmIBCreateUserID.hbxPasswdStrength.skin = hboxGreen;
//			frmIBCreateUserID.lblPasswdStrength.text = ("Password Strength: Strong");
//			//	alert("Your Password is Strong");
//		}
//		else if (mediumRegex.test(pwd)) {
//			frmIBCreateUserID.hbxPasswdStrength.skin = hboxYellow;
//			frmIBCreateUserID.lblPasswdStrength.text = ("Password Strength: Medium");
//		}
//		else {
//			frmIBCreateUserID.hbxPasswdStrength.skin = hboxRed;
//			frmIBCreateUserID.lblPasswdStrength.text = ("Password Strength: Weak");
//		}
//	}
//}
//function userBubbleChange(curr_form){
//if (frmIBCreateUserID.txtUserID.text.length != 0) {
//       // alert(curr_form.txtUserID.text.length);
//        frmIBCreateUserID.vbxRules.setVisibility(false);
//        frmIBCreateUserID.hbxUserIDBubble.setVisibility(false);
//    }
//
//}
function passwordChangedIB(curr_form) {
    if (curr_form.txtPassword.text.length != 0) {
        curr_form.lblPasswdStrength.setVisibility(true);
        frmIBCreateUserID.vbxRules.setVisibility(false);
        frmIBCreateUserID.hbxPasswordRulesBubble.setVisibility(false);
    } else {
        curr_form.lblPasswdStrength.setVisibility(false);
        frmIBCreateUserID.vbxRules.setVisibility(true);
        frmIBCreateUserID.hbxPasswordRulesBubble.setVisibility(true);
    }
    var strength = 0;
    //var strongRegex = new RegExp("^(?=.{9,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$", "g");
    //var mediumRegex = new RegExp("^(?=.{8,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$", "g");
    //var enoughRegex = new RegExp("^(?=.{6,}).*", "g");
    var isUpper = new RegExp("[A-Z]");
    var isLower = new RegExp("[a-z]");
    var isDigit = new RegExp("[0-9]");
    var isSpecial = new RegExp("[^a-zA-Z0-9\s]");
    var pwd = curr_form.txtPassword.text;
    var i = 0;
    var count = 0;
    var len = pwd.length;
    for (i = 0; i < len; i++) {
        if (pwd[i] == ' ') return false;
    }
    for (i = 0; i < len; i++) {
        var temp = pwd[i] + "";
        if (isSpecial.test(temp)) {
            count = count + 1;
        }
    }
    if (len > 6) {
        strength++;
    }
    //if (len < 8) {
    //		curr_form.lblPasswdStrength.setVisibility(false);
    //		//return false;
    //	}
    if (isDigit.test(pwd) && (isUpper.test(pwd) || isLower.test(pwd))) {
        strength++;
        //
    }
    if (isLower.test(pwd) && isUpper.test(pwd)) {
        strength++;
        //
    }
    if (isSpecial.test(pwd)) {
        strength++;
        //
    }
    if (count >= 2) {
        strength++;
        //
    }
    //if (pwd != null && len < 8) {
    //		curr_form.lblPasswdStrength.setVisibility(false);
    //	}
    //	else {
    //alert("stren :: " + strength)
    //curr_form.lblPasswdStrength.setVisibility(true);
    if (strength > 4) {
        curr_form.hbxPasswdStrength.skin = hboxGreen;
        curr_form.lblPasswdStrength.text = kony.i18n.getLocalizedString("passStrong");
        return true;
    } else if (strength >= 2 && strength <= 4) {
        curr_form.hbxPasswdStrength.skin = hboxYellow;
        curr_form.lblPasswdStrength.text = kony.i18n.getLocalizedString("passMedium");
        return true;
    } else if (strength < 2) {
        curr_form.hbxPasswdStrength.skin = hboxRed;
        curr_form.lblPasswdStrength.text = kony.i18n.getLocalizedString("passWeak");
        return false;
    }
    //}
}