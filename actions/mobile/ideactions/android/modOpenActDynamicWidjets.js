function savingsCareFixedForm() {
    var availPerct = "";
    if (kony.i18n.getCurrentLocale() == "th_TH") {
        availPerct = kony.i18n.getLocalizedString("keyAvailable") + "100%";
    } else {
        availPerct = "100%" + kony.i18n.getLocalizedString("keyAvailable");
    }
    var lblOccupationinfo = new kony.ui.Label({
        "id": "lblOccupationinfo",
        "top": "0dp",
        "left": "0dp",
        "width": "100%",
        "centerY": "50%",
        "zIndex": 1,
        "isVisible": true,
        "text": kony.i18n.getLocalizedString("keySavingCareAct"),
        "skin": "lblGrey48px"
    }, {
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {
        "textCopyable": false
    });
    var flxSubTitle = new kony.ui.FlexContainer({
        "id": "flxSubTitle",
        "top": "0%",
        "left": "0dp",
        "width": "100%",
        "height": "10%",
        "centerX": "50%",
        "zIndex": 1,
        "isVisible": true,
        "clipBounds": true,
        "Location": "[0,45]",
        "skin": "flexGreyBG",
        "focusSkin": "flexGreyBG",
        "layoutType": kony.flex.FREE_FORM
    }, {
        "padding": [0, 0, 0, 0]
    }, {});;
    flxSubTitle.setDefaultUnit(kony.flex.DP)
    flxSubTitle.add(lblOccupationinfo);
    var imgBaht = new kony.ui.Image2({
        "id": "imgBaht",
        "top": "0%",
        "left": "0%",
        "width": "17%",
        "height": "70.0%",
        "centerY": "50%",
        "zIndex": 1,
        "isVisible": true,
        "src": "newbaht.png",
        "imageWhenFailed": null,
        "imageWhileDownloading": null
    }, {
        "padding": [0, 0, 0, 0],
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {});
    var txtAmount = new kony.ui.TextBox2({
        "id": "txtAmount",
        "top": "15.0%",
        "left": "0%",
        "width": "100%",
        "height": "50%",
        "zIndex": 1,
        "isVisible": true,
        //"text": "5000.00",
        "secureTextEntry": false,
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "placeholder": null,
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "skin": "txtBlueNormal200",
        "focusSkin": "txtBlueNormal200",
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT
    }, {
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "autoFilter": false,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var fromActId = ""
    var showAmount = true;
    if (gblSavingsCareFlow == "openAccount") {
        fromActId = frmMBSavingsCareAddBal["segNSSlider"]["selectedItems"][0].lblActNoval;
        fromActId = "xxx-x-" + fromActId.substring(6, 11) + "-x";
        kony.print("fromActId in dynamic widjets creation" + fromActId);
    } else {
        showAmount = false;
    }
    var lblAmountDesc = new kony.ui.Label({
        "id": "lblAmountDesc",
        "top": "-17.0%",
        "left": "0dp",
        "width": "100%",
        "height": "50%",
        "zIndex": 1,
        "isVisible": true,
        "text": kony.i18n.getLocalizedString("deduct_amt") + " " + fromActId,
        //"skin": "lblGrey36px"
        "skin": "lblGreyRegular142"
    }, {
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {
        "textCopyable": false
    });
    var FlexContainer0217f7f5bf4704b = new kony.ui.FlexContainer({
        "id": "FlexContainer0217f7f5bf4704b",
        "top": "0%",
        "width": "68%",
        "height": "100.0%",
        "centerX": "52%",
        "centerY": "50%",
        "zIndex": 1,
        "isVisible": true,
        "clipBounds": true,
        "Location": "[81,0]",
        "skin": "flexWhiteBG",
        "focusSkin": "flexWhiteBG",
        "layoutType": kony.flex.FLOW_VERTICAL
    }, {
        "padding": [0, 0, 0, 0]
    }, {});;
    FlexContainer0217f7f5bf4704b.setDefaultUnit(kony.flex.DP)
    FlexContainer0217f7f5bf4704b.add(txtAmount, lblAmountDesc);
    var Image02998cd599c864e = new kony.ui.Image2({
        "id": "Image02998cd599c864e",
        "top": "0%",
        "left": "90%",
        "right": "5%",
        "width": "5%",
        "height": "30.0%",
        "centerY": "50%",
        "zIndex": 1,
        "isVisible": true,
        "src": "navarrowblue.png",
        "imageWhenFailed": null,
        "onTouchEnd": showSavingsCareAddAccount,
        "imageWhileDownloading": null
    }, {
        "padding": [0, 0, 0, 0],
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {});
    var flxAmount = new kony.ui.FlexContainer({
        "id": "flxAmount",
        "top": "0%",
        "left": "0dp",
        "width": "100%",
        "height": "14%",
        "zIndex": 1,
        "isVisible": showAmount,
        "clipBounds": true,
        "Location": "[0,0]",
        "skin": "flexWhiteBG",
        "onClick": navigateToAddBal,
        "layoutType": kony.flex.FREE_FORM
    }, {
        "padding": [0, 0, 0, 0]
    }, {});;
    flxAmount.setDefaultUnit(kony.flex.DP)
    flxAmount.add(imgBaht, FlexContainer0217f7f5bf4704b, Image02998cd599c864e);
    var flxLine1 = new kony.ui.FlexContainer({
        "id": "flxLine1",
        "top": "0dp",
        "left": "0dp",
        "width": "100%",
        "height": "12dp",
        "zIndex": 1,
        "isVisible": showAmount,
        "clipBounds": true,
        "Location": "[0,45]",
        "skin": "flexBlueLine",
        "layoutType": kony.flex.FREE_FORM
    }, {
        "padding": [0, 0, 0, 0]
    }, {});;
    flxLine1.setDefaultUnit(kony.flex.DP)
    flxLine1.add();
    var imgPiggy = new kony.ui.Image2({
        "id": "imgPiggy",
        "top": "13.33%",
        "left": "2.0%",
        "width": "16%",
        "height": "68.89%",
        "centerY": "50.0%",
        "zIndex": 1,
        "isVisible": true,
        "src": "piggynew.png",
        "imageWhenFailed": null,
        "imageWhileDownloading": null
    }, {
        "padding": [0, 0, 0, 0],
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {});
    if (gblOpenSavingsCareNickName == null || gblOpenSavingsCareNickName == "") {
        gblOpenSavingsCareNickName = "";
    }
    var txtNickName = new kony.ui.TextBox2({
        "id": "txtNickName",
        "top": "15.0%",
        "left": "0%",
        "width": "100%",
        "height": "50%",
        "zIndex": 1,
        "isVisible": true,
        "maxTextLength": 20,
        "text": gblOpenSavingsCareNickName,
        //"text":"",
        "onDone": onDoneNickName,
        "onTextChange": onDoneTextNickName,
        "secureTextEntry": false,
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "placeholder": kony.i18n.getLocalizedString("add_actNickname"),
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "skin": "txtBlueNormal200",
        "focusSkin": "txtBlueNormal200",
        //"placeholderSkin":"txtBlueNormal200",
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT
    }, {
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "autoFilter": false,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT,
        "placeholderSkin": "txtBlueNormal200"
    });
    var nickNameAddBal = removeColonFromEnd(kony.i18n.getLocalizedString("keylblNickname"));
    var lblNickNameDesc = new kony.ui.Label({
        "id": "lblNickNameDesc",
        "top": "-16%",
        "left": "0dp",
        "width": "100%",
        "height": "50.0%",
        "zIndex": 1,
        "isVisible": true,
        "text": nickNameAddBal,
        "skin": "lblGrey36px"
    }, {
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {
        "textCopyable": false
    });
    var CopyFlexContainer021afc48d83ed41 = new kony.ui.FlexContainer({
        "id": "CopyFlexContainer021afc48d83ed41",
        "top": "0%",
        "width": "68%",
        "height": "100.0%",
        "centerX": "52.0%",
        "centerY": "50%",
        "zIndex": 1,
        "isVisible": true,
        "clipBounds": true,
        "Location": "[81,0]",
        "skin": "flexWhiteBG",
        "focusSkin": "flexWhiteBG",
        "layoutType": kony.flex.FLOW_VERTICAL
    }, {
        "padding": [0, 0, 0, 0]
    }, {});;
    CopyFlexContainer021afc48d83ed41.setDefaultUnit(kony.flex.DP)
    CopyFlexContainer021afc48d83ed41.add(txtNickName, lblNickNameDesc);
    var CopyImage0ad994c96b0764a = new kony.ui.Image2({
        "id": "CopyImage0ad994c96b0764a",
        "top": "0%",
        "left": "90%",
        "right": "5%",
        "width": "9%",
        "height": "100%",
        "centerY": "50%",
        "zIndex": 1,
        "isVisible": false,
        "src": "navarrow.png",
        "imageWhenFailed": null,
        "imageWhileDownloading": null
    }, {
        "padding": [0, 0, 0, 0],
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {});
    var CopyflxAmount0a9be84b3f66c43 = new kony.ui.FlexContainer({
        "id": "CopyflxAmount0a9be84b3f66c43",
        "top": "0%",
        "left": "0dp",
        "width": "100%",
        "height": "14%",
        "centerX": "50%",
        "zIndex": 1,
        "isVisible": true,
        "clipBounds": true,
        "Location": "[0,57]",
        "skin": "flexWhiteBG",
        "layoutType": kony.flex.FREE_FORM
    }, {
        "padding": [0, 0, 0, 0]
    }, {});;
    CopyflxAmount0a9be84b3f66c43.setDefaultUnit(kony.flex.DP)
    CopyflxAmount0a9be84b3f66c43.add(imgPiggy, CopyFlexContainer021afc48d83ed41, CopyImage0ad994c96b0764a);
    var flxLine2 = new kony.ui.FlexContainer({
        "id": "flxLine2",
        "top": "1dp",
        "left": "0dp",
        "width": "102%",
        "height": "12dp",
        "centerX": "50%",
        "zIndex": 1,
        "isVisible": true,
        "clipBounds": true,
        "Location": "[-4,103]",
        "skin": "flexBlueLine",
        "layoutType": kony.flex.FREE_FORM
    }, {
        "padding": [0, 0, 0, 0]
    }, {});;
    flxLine2.setDefaultUnit(kony.flex.DP)
    flxLine2.add();
    var lblBenifitHeading = new kony.ui.Label({
        "id": "lblBenifitHeading",
        "top": "0.0%",
        "right": "3%",
        "width": "97%",
        "height": "50%",
        "zIndex": 1,
        "isVisible": true,
        "text": kony.i18n.getLocalizedString("benefitKey"),
        "skin": "lblGrey36px"
    }, {
        "padding": [0, 0, 3, 0],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {
        "textCopyable": false
    });
    var lblBenifTop = new kony.ui.Label({
        "id": "lblBenifTop",
        "top": "6%",
        "left": "20dp",
        "width": "50%",
        "height": "100%",
        "zIndex": 1,
        "isVisible": true,
        "text": kony.i18n.getLocalizedString("keyBenificiaries"),
        "skin": "lblGrey48px"
    }, {
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {
        "textCopyable": false
    });
    var lblavailPercent = new kony.ui.Label({
        "id": "lblavailPercent",
        "top": "10%",
        "right": "20dp",
        "width": "50%",
        "height": "100%",
        "zIndex": 1,
        "isVisible": true,
        "text": availPerct, //"100% "+kony.i18n.getLocalizedString("keyAvailable"),
        "skin": "lblGrey36px"
    }, {
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {
        "textCopyable": false
    });
    var FlexContainer06c42888154b14c = new kony.ui.FlexContainer({
        "id": "FlexContainer06c42888154b14c",
        "top": "-10.0%",
        "left": "0%",
        "width": "100%",
        "height": "50%",
        "zIndex": 1,
        "isVisible": true,
        "clipBounds": true,
        "Location": "[0,17]",
        "skin": "flexWhiteBG",
        "focusSkin": "flexWhiteBG",
        "layoutType": kony.flex.FREE_FORM
    }, {
        "padding": [0, 0, 0, 0]
    }, {});;
    FlexContainer06c42888154b14c.setDefaultUnit(kony.flex.DP)
    FlexContainer06c42888154b14c.add(lblBenifTop, lblavailPercent);
    var flxBenifDetailsHeadings = new kony.ui.FlexContainer({
        "id": "flxBenifDetailsHeadings",
        "top": "0.0%",
        "left": "0%",
        "width": "100%",
        "height": "13.0%",
        "zIndex": 1,
        "isVisible": true,
        "clipBounds": true,
        "Location": "[0,115]",
        "skin": "flexWhiteBG",
        "focusSkin": "flexWhiteBG",
        "layoutType": kony.flex.FLOW_VERTICAL
    }, {
        "padding": [0, 0, 0, 0]
    }, {});;
    flxBenifDetailsHeadings.setDefaultUnit(kony.flex.DP)
    flxBenifDetailsHeadings.add(lblBenifitHeading, FlexContainer06c42888154b14c);
    frmMBSavingsCareAddNickName.flexBodyScroll.add(flxSubTitle, flxAmount, flxLine1, CopyflxAmount0a9be84b3f66c43, flxLine2, flxBenifDetailsHeadings);
}

function addRichTextToTheForm() {
    var rchBenifDesc = new kony.ui.RichText({
        "id": "rchBenifDesc",
        "top": "0%",
        "left": "0%",
        "width": "94%",
        "height": "100%",
        "centerX": "50%",
        "zIndex": 1,
        "isVisible": true,
        "text": kony.i18n.getLocalizedString("benefinotSpecifyDesc"),
        "skin": "richNormal"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {});
    var flxBenefDesc = new kony.ui.FlexContainer({
        "id": "flxBenefDesc",
        "top": "0%",
        "left": "0%",
        //"width": "100%",
        "height": kony.flex.USE_PREFERED_SIZE,
        "width": "100%",
        "minHeight": "40%",
        "centerX": "50%",
        "zIndex": 1,
        "isVisible": true,
        "clipBounds": true,
        "Location": "[0,157]",
        "skin": "flexWhiteBG",
        "layoutType": kony.flex.FREE_FORM
    }, {
        "padding": [0, 0, 0, 0]
    }, {});;
    flxBenefDesc.setDefaultUnit(kony.flex.DP)
    flxBenefDesc.add(rchBenifDesc);
    frmMBSavingsCareAddNickName.flexBodyScroll.add(flxBenefDesc);
}

function setPropertiesForIphoneSmall() {
    frmMBSavingsCareAddNickName["label52440721750230"].skin = "lblBlue40px"; // Need to create the skin here
    frmMBSavingsCareAddNickName["FlexContainer0217f7f5bf4704b"].centerX = "48%"; //
    frmMBSavingsCareAddNickName["FlexContainer0217f7f5bf4704b"].width = "75%"; //
    frmMBSavingsCareAddNickName["imgBaht"].width = "14%";
    frmMBSavingsCareAddNickName["lblAmountDesc"].left = "6dp"
    frmMBSavingsCareAddNickName["CopyFlexContainer021afc48d83ed41"].centerX = "50%"; //
    frmMBSavingsCareAddNickName["CopyFlexContainer021afc48d83ed41"].width = "75%"; //
    frmMBSavingsCareAddNickName["imgPiggy"].width = "12%";
    frmMBSavingsCareAddNickName["lblAmountDesc"].left = "5dp"
    frmMBSavingsCareAddNickName["lblNickNameDesc"].left = "6dp"
    frmMBSavingsCareAddNickName["lblBenifitHeading"].right = "1%"
    frmMBSavingsCareAddNickName["lblBenifTop"].left = "10dp"
    frmMBSavingsCareAddNickName["lblavailPercent"].right = "10dp"
}