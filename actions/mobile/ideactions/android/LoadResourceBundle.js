/*
************************************************************************
            Name    : loadResourceBundle()
            Author  : 
            Date    : 
            Purpose : To update the Resource Bundle 
        	Input params: 
       		Output params:
        	ServiceCalls: loadResourceBundle
       		Called From : pre app init 
************************************************************************
*/
function loadResourceBundle() {
    //var deviceInfo = kony.os.deviceInfo();
    //	if (gblDeviceInfo == null) {
    //	       gblDeviceInfo = kony.os.deviceInfo();
    //    }
    if (gblDeviceInfo["name"] == "thinclient") {
        kony.application.showLoadingScreen("frmLoading", "", constants.LOADING_SCREEN_POSITION_FULL_SCREEN, true, false, null);
    } else {
        showLoadingScreen();
        registerPushCallBacks(); //for push notification purpose
    }
    var updatedOn = kony.i18n.getLocalizedString("keyphrasesUpdatedOn");
    var i18n_inputparam = {};
    i18n_inputparam["timestamp"] = updatedOn;
    var deviceLanguage = kony.i18n.getCurrentDeviceLocale();
    var getcurAppLocale = kony.store.getItem("curAppLocale");
    if (getcurAppLocale != null) {
        kony.i18n.setCurrentLocaleAsync(getcurAppLocale["appLocale"], onsuccesscallback, onfailurecallback, "");
    } else {
        if (kony.string.startsWith(deviceLanguage.language, "th", true)) {
            kony.i18n.setCurrentLocaleAsync("th_TH", onsuccesscallback, onfailurecallback, "");
        } else {
            kony.i18n.setCurrentLocaleAsync("en_US", onsuccesscallback, onfailurecallback, "");
        }
    }
    i18n_inputparam["localeId"] = kony.i18n.getCurrentLocale();
    i18n_inputparam["platform1"] = "M";
    var status = invokeServiceSecureAsync("getPhrases", i18n_inputparam, callbackgetphrasesMB);
}

function loadResourceBundleIB() {
    //var deviceInfo = kony.os.deviceInfo();
    //if (gblDeviceInfo == null) {
    //gblDeviceInfo = kony.os.deviceInfo();
    //}
    if (gblDeviceInfo["name"] == "thinclient") {
        if (document.getElementById("popupIBLoading") == null) showIBLoadingScreen();
    } else {
        showLoadingScreen();
        registerPushCallBacks(); //for push notification purpose
    }
    var i18n_inputparam = {};
    var getcurAppLocale = kony.store.getItem("curAppLocale");
    var updatedOn = ""; // have to send all the data
    i18n_inputparam["timestamp"] = updatedOn;
    if (getcurAppLocale != null) {
        kony.i18n.setCurrentLocaleAsync(getcurAppLocale["appLocale"], onsuccess(), onfailure(), "");
        //setLocaleEng();     			
    }
    i18n_inputparam["localeId"] = kony.i18n.getCurrentLocale();
    i18n_inputparam["platform1"] = "M";
    var status = invokeServiceSecureAsync("getPhrases", i18n_inputparam, callbackgetphrases);
}

function callbackgetphrases(status, getContentUpdate) {
    if (status == 400) {
        if (getContentUpdate["opstatus"] == 0) {
            dynamicCampaignBrowser();
            //localStorage.clear();
            localStorage.removeItem("TMB_en_US");
            localStorage.removeItem("TMB_th_TH");
            //MIB-4884-Allow special characters for My Note and Note to recipient field
            GBL_ALLOW_SPECIAL_CHAR_KEY_NAMES = getContentUpdate["ALLOW_SPECIAL_CHAR_KEY_NAMES"];
            GBL_VALID_CHARS_MY_NOTE_NOW = getContentUpdate["VALID_CHARS_MY_NOTE_NOW"];
            GBL_VALID_CHARS_MY_NOTE_FUTURE = getContentUpdate["VALID_CHARS_MY_NOTE_FUTURE"];
            gblSMART_FREE_TRANS_CODES = getContentUpdate["SMART_FREE_TRANS_CODES"];
            gblALL_SMART_FREE_TRANS_CODES = getContentUpdate["ALL_SMART_FREE_TRANS_CODES"];
            gblORFT_FREE_TRANS_CODES = getContentUpdate["ORFT_FREE_TRANS_CODES"];
            gblITMX_FREE_TRANS_CODES = getContentUpdate["ITMX_FEE_TRANS_CODES"];
            GLOBAL_ERROR_CODES_FOR_TOPUP = getContentUpdate["ERROR_CODES_FOR_TOPUP"];
            GLOBAL_PDF_DOWNLOAD_URL_en_US = getContentUpdate["GLOBAL_PDF_DOWNLOAD_URL_en_US"]
            GLOBAL_IMAGE_DOWNLOAD_URL_en_US = getContentUpdate["GLOBAL_IMAGE_DOWNLOAD_URL_en_US"]
            GLOBAL_PDF_DOWNLOAD_URL_th_TH = getContentUpdate["GLOBAL_PDF_DOWNLOAD_URL_th_TH"]
            GLOBAL_IMAGE_DOWNLOAD_URL_th_TH = getContentUpdate["GLOBAL_IMAGE_DOWNLOAD_URL_th_TH"]
            GLOBAL_KONY_IDLE_TIMEOUT = getContentUpdate["KONY_IDLE_TIMEOUT"];
            GLOBAL_MB_CHANNEL = getContentUpdate["MB_CHANNEL_ID"];
            GLOBAL_IB_CHANNEL = getContentUpdate["IB_CHANNEL_ID"];
            GLOBAL_ACCT_NUM_POSITION = getContentUpdate["ACCT_NUM_POSITION"];
            GLOBAL_NUM_AT_POS_CA = getContentUpdate["NUM_AT_POS_CA"];
            GLOBAL_NUM_AT_POS_TD = getContentUpdate["NUM_AT_POS_TD"];
            GLOBAL_NUM_AT_POS_SA = getContentUpdate["NUM_AT_POS_SA"];
            gblShowPinPwdSecs = kony.os.toNumber(getContentUpdate["showPinPwdSecs"]);
            gblShowPwdNo = kony.os.toNumber(getContentUpdate["showPinPwdCount"]);
            GLOBAL_INI_MAX_LIMIT_AMT = getContentUpdate["INI_MAX_LIMIT_AMT"];
            GLOBAL_CEILING_LIMIT = getContentUpdate["CEILING_LIMIT"];
            GLOBAL_MAX_LIMIT_HIST = getContentUpdate["EB_MAX_LIMIT_AMT_HIST"];
            var getcurAppLocale = kony.store.getItem("curAppLocale");
            //FATCA TnC Add
            GLOBAL_FATCA_TNC_en_US = getContentUpdate["FATCA_TERMS_CONDITIONS_ENGLISH"];
            GLOBAL_FATCA_TNC_th_TH = getContentUpdate["FATCA_TERMS_CONDITIONS_THAI"];
            GLOBAL_BLOCKED_TRANS = getContentUpdate["TRANS_PWD_BLOCKED"];
            GLOBAL_BLOCKED_ACCESS = getContentUpdate["ACCESS_PWD_BLOCKED"];
            GLOBAL_BLOCKED_USERIDS = getContentUpdate["USER_ID_BLOCKED"];
            GLOBAL_LOAD_MORE = getContentUpdate["GLOBAL_LOAD_MORE"];
            GLOBAL_MAX_BILL_COUNT = getContentUpdate["MAX_BILL_COUNT"];
            GLOBAL_MAX_BILL_ADD = getContentUpdate["MAX_BILL_ADD"];
            BILLER_LOGO_URL = getContentUpdate["BILLER_LOGO_URL"];
            GLOBAL_NICKNAME_LENGTH = getContentUpdate["NICKNAME_MAX_LENGTH_OPEN"];
            GLOBAL_DREAM_DESC_MAXLENGTH = getContentUpdate["DREAM_DESC_MAX_LENGTH"];
            DEFAULT_SERVICE_TIMEOUT = kony.os.toNumber(getContentUpdate["DEFAULT_SERVICE_TIMEOUT"]);
            GLOBAL_OPENACT_STARTTIME = getContentUpdate["OPENACT_STARTTIME"];
            GLOBAL_OPENACT_ENDTIME = getContentUpdate["OPENACT_ENDTIME"];
            GLOBAL_EDITDREAM_STARTTIME = getContentUpdate["EDIT_DREAM_STARTTIME"];
            GLOBAL_EDITDREAM_ENDTIME = getContentUpdate["EDIT_DREAM_ENDTIME"];
            GLOBAL_ACTIVATION_HELP_TEXT_EN = getContentUpdate["MB_ACTIVATION_CODE_HELP_TEXT_EN"];
            GLOBAL_ACTIVATION_HELP_TEXT_TH = getContentUpdate["MB_ACTIVATION_CODE_HELP_TEXT_TH"];
            GLOBAL_REACTIVATION_HELP_TEXT_EN = getContentUpdate["MB_REACTIVATION_CODE_HELP_TEXT_EN"];
            GLOBAL_REACTIVATION_HELP_TEXT_TH = getContentUpdate["MB_REACTIVATION_CODE_HELP_TEXT_TH"];
            //E-STATEMENT
            GLOBAL_E_STATEMENT_PRODUCTS = getContentUpdate["E_STATEMENT_PRODUCTS"];
            GLOBAL_E_STATEMENT_IMAGES = getContentUpdate["E_STATEMENT_IMAGES"];
            Gbl_Image_Size_Limit = getContentUpdate["Max_FileSize"];
            Gbl_Image_Size_Limit = getContentUpdate["Prod_Code_MEACC"];
            if (getContentUpdate["Start_Digs_MobileNum"] != null && getContentUpdate["Start_Digs_MobileNum"] != undefined && getContentUpdate["Start_Digs_MobileNum"] != "") Gbl_StartDigsMobileNum = getContentUpdate["Start_Digs_MobileNum"];
            else Gbl_StartDigsMobileNum = "09,08,06"; // Initialising with default.
            Gbl_StartDigsMobileNum.split(',');
            if (getContentUpdate["FATCA_MAX_SKIP_COUNTER"] != null && getContentUpdate["FATCA_MAX_SKIP_COUNTER"] != undefined && getContentUpdate["FATCA_MAX_SKIP_COUNTER"] != "") GLOBAL_FATCA_MAX_SKIP_COUNT = parseInt(getContentUpdate["FATCA_MAX_SKIP_COUNTER"]);
            else GLOBAL_FATCA_MAX_SKIP_COUNT = 3;
            GLOBAL_JOINT_CODES = getContentUpdate["JOINT_ACCNT_CODES"];
            GLOBAL_MB_APPLY_CODES = getContentUpdate["APPLY_MB_STATUS_CODES"];
            Gbl_Prod_Code_MEACC = getContentUpdate["Prod_Code_MEACC"];
            GLOBAL_IB_ACTIVATION_HELP_TEXT_EN = getContentUpdate["IB_ACTIVATION_HELP_TEXT_EN"];
            GLOBAL_IB_ACTIVATION_HELP_TEXT_TH = getContentUpdate["IB_ACTIVATION_HELP_TEXT_TH"];
            GLOBAL_IB_REACTIVATION_HELP_TEXT_EN = getContentUpdate["IB_REACTIVATION_HELP_TEXT_EN"];
            GLOBAL_IB_REACTIVATION_HELP_TEXT_TH = getContentUpdate["IB_REACTIVATION_HELP_TEXT_TH"];
            firstTimeActivationPopUp = getContentUpdate["FirstTimeActivation_Popup"];
            GLOBAL_TODAY_DATE = getContentUpdate["TODAY_DATE"]; //To show the server date on Calendar
            //ENH_209 start
            GLOBAL_Exchange_Rates_Link_EN = getContentUpdate["Exchange_Rates_Link_EN"];
            GLOBAL_Exchange_Rates_Link_TH = getContentUpdate["Exchange_Rates_Link_TH"];
            //ENH_209 end
            //ENH_230 start
            GLOBAL_SecurityAdvice_Link_EN = getContentUpdate["SecurityAdvice_Link_EN"];
            GLOBAL_SecurityAdvice_Link_TH = getContentUpdate["SecurityAdvice_Link_TH"];
            //ENH_230 end
            // Online Payment based on MWA
            GLOBAL_ONLINE_PAYMENT_VERSION_MWA = getContentUpdate["ONLINE_PAYMENT_VERSION_MWA"];
            // MF migration Change : -- Moving campaign prelogin service call to here..
            if (isMFEnabled == true && !flowSpa && kony.application.getCurrentForm().id == "frmIBPreLogin") {
                campaignPreLoginService("segCampaignImage", "frmIBPreLogin", "I");
            }
            if (getContentUpdate["Results"] != null) {
                if (getContentUpdate["Results"][0]["EnglishPhrases"].length > 0) {
                    try {
                        //kony.i18n.updateResourceBundle(getContentUpdate["Results"][0]["EnglishPhrases"][0], "en_US");
                        setTimeout(function() {
                            kony.i18n.updateResourceBundle(getContentUpdate["Results"][0]["EnglishPhrases"][0], "en_US")
                        }, 5000);
                    } catch (i18nError) {
                        alert("Exception While getting updateResourceBundle english  : " + i18nError);
                    }
                }
                if (getContentUpdate["Results"][1]["ThaiPhrases"].length > 0) {
                    try {
                        //kony.i18n.updateResourceBundle(getContentUpdate["Results"][1]["ThaiPhrases"][0], "th_TH");
                        setTimeout(function() {
                            kony.i18n.updateResourceBundle(getContentUpdate["Results"][1]["ThaiPhrases"][0], "th_TH")
                        }, 2000);
                    } catch (i18nError) {
                        alert("Exception While getting updateResourceBundle thai  : " + i18nError);
                    }
                }
                //kony.i18n.updateResourceBundle(getContentUpdate["Results"][0]["EnglishPhrases"][0], "en_US");
                //kony.i18n.updateResourceBundle(getContentUpdate["Results"][1]["ThaiPhrases"][0], "th_TH");
                var getEncrKeyFromDevice = kony.store.getItem("encrytedText");
                if (getEncrKeyFromDevice != null) {
                    //TMBUtil.DestroyForm(frmAfterLogoutMB);
                    if (kony.application.getCurrentForm() == frmMBPreLoginAccessesPin) { //frmAfterLogoutMB
                        frmMBPreLoginAccessesPin.show(); //frmAfterLogoutMB
                    }
                } else {
                    //TMBUtil.DestroyForm(frmMBanking);
                    if (kony.application.getCurrentForm() == frmMBanking) {
                        frmMBanking.show();
                    }
                }
            }
        } else {}
        dismissLoadingScreen();
    }
}

function callbackgetphrasesMB(status, getContentUpdate) {
    if (status == 400) {
        if (getContentUpdate["opstatus"] == 0) {
            dynamicCampaignBrowser();
            //null issue fix on SPA
            //var deviceInfo = kony.os.deviceInfo();
            if (gblDeviceInfo["name"] == "thinclient" & gblDeviceInfo["type"] == "spa") {
                //localStorage.clear();
                localStorage.removeItem("TMB_en_US");
                localStorage.removeItem("TMB_th_TH");
            }
            gblRedirect = false;
            gblrefcontweight = "";
            gblrefcontwidth = "";
            gblcontheight = "";
            //MIB-4884-Allow special characters for My Note and Note to recipient field
            GBL_ALLOW_SPECIAL_CHAR_KEY_NAMES = getContentUpdate["ALLOW_SPECIAL_CHAR_KEY_NAMES"];
            GBL_VALID_CHARS_MY_NOTE_NOW = getContentUpdate["VALID_CHARS_MY_NOTE_NOW"];
            GBL_VALID_CHARS_MY_NOTE_FUTURE = getContentUpdate["VALID_CHARS_MY_NOTE_FUTURE"];
            GLOBAL_ERROR_CODES_FOR_TOPUP = getContentUpdate["ERROR_CODES_FOR_TOPUP"];
            gblSMART_FREE_TRANS_CODES = getContentUpdate["SMART_FREE_TRANS_CODES"];
            gblALL_SMART_FREE_TRANS_CODES = getContentUpdate["ALL_SMART_FREE_TRANS_CODES"];
            gblORFT_FREE_TRANS_CODES = getContentUpdate["ORFT_FREE_TRANS_CODES"];
            gblITMX_FREE_TRANS_CODES = getContentUpdate["ITMX_FEE_TRANS_CODES"];
            GLOBAL_PDF_DOWNLOAD_URL_en_US = getContentUpdate["GLOBAL_PDF_DOWNLOAD_URL_en_US"]
            GLOBAL_IMAGE_DOWNLOAD_URL_en_US = getContentUpdate["GLOBAL_IMAGE_DOWNLOAD_URL_en_US"]
            GLOBAL_PDF_DOWNLOAD_URL_th_TH = getContentUpdate["GLOBAL_PDF_DOWNLOAD_URL_th_TH"]
            GLOBAL_IMAGE_DOWNLOAD_URL_th_TH = getContentUpdate["GLOBAL_IMAGE_DOWNLOAD_URL_th_TH"]
            GLOBAL_KONY_IDLE_TIMEOUT = getContentUpdate["KONY_IDLE_TIMEOUT"];
            GLOBAL_MB_CHANNEL = getContentUpdate["MB_CHANNEL_ID"];
            GLOBAL_IB_CHANNEL = getContentUpdate["IB_CHANNEL_ID"];
            GLOBAL_ACCT_NUM_POSITION = getContentUpdate["ACCT_NUM_POSITION"];
            GLOBAL_NUM_AT_POS_CA = getContentUpdate["NUM_AT_POS_CA"];
            GLOBAL_NUM_AT_POS_TD = getContentUpdate["NUM_AT_POS_TD"];
            GLOBAL_NUM_AT_POS_SA = getContentUpdate["NUM_AT_POS_SA"];
            gblShowPinPwdSecs = kony.os.toNumber(getContentUpdate["showPinPwdSecs"]);
            gblShowPwdNo = kony.os.toNumber(getContentUpdate["showPinPwdCount"]);
            gblShowPwd = kony.os.toNumber(getContentUpdate["showPinPwdCount"]);
            gblShowPinPwdSecsFinal = kony.os.toNumber(getContentUpdate["showPinPwdSecs"]);
            gblShowPwdNoFinal = kony.os.toNumber(getContentUpdate["showPinPwdCount"]);
            gblShowPwdFinal = kony.os.toNumber(getContentUpdate["showPinPwdCount"]);
            GLOBAL_INI_MAX_LIMIT_AMT = getContentUpdate["INI_MAX_LIMIT_AMT"];
            GLOBAL_CEILING_LIMIT = getContentUpdate["CEILING_LIMIT"];
            GLOBAL_MAX_LIMIT_HIST = getContentUpdate["EB_MAX_LIMIT_AMT_HIST"];
            var getcurAppLocale = kony.store.getItem("curAppLocale");
            GLOBAL_TERMS_CONDITIONS_th_TH_1 = getContentUpdate["TERMS_CONDITIONS_THAI_1"];
            GLOBAL_TERMS_CONDITIONS_th_TH_2 = getContentUpdate["TERMS_CONDITIONS_THAI_2"];
            GLOBAL_TERMS_CONDITIONS_th_TH_3 = getContentUpdate["TERMS_CONDITIONS_THAI_3"];
            GLOBAL_TERMS_CONDITIONS_en_US = getContentUpdate["TERMS_CONDITIONS_ENGLISH"];
            //FATCA TnC Add
            GLOBAL_FATCA_TNC_en_US = getContentUpdate["FATCA_TERMS_CONDITIONS_ENGLISH"];
            GLOBAL_FATCA_TNC_th_TH = getContentUpdate["FATCA_TERMS_CONDITIONS_THAI"];
            GLOBAL_BLOCKED_TRANS = getContentUpdate["TRANS_PWD_BLOCKED"];
            GLOBAL_BLOCKED_ACCESS = getContentUpdate["ACCESS_PWD_BLOCKED"];
            GLOBAL_BLOCKED_USERIDS = getContentUpdate["USER_ID_BLOCKED"];
            GLOBAL_LOAD_MORE = getContentUpdate["GLOBAL_LOAD_MORE"];
            GLOBAL_MAX_BILL_COUNT = getContentUpdate["MAX_BILL_COUNT"];
            GLOBAL_MAX_BILL_ADD = getContentUpdate["MAX_BILL_ADD"];
            BILLER_LOGO_URL = getContentUpdate["BILLER_LOGO_URL"];
            GLOBAL_NICKNAME_LENGTH = getContentUpdate["NICKNAME_MAX_LENGTH_OPEN"];
            GLOBAL_DREAM_DESC_MAXLENGTH = getContentUpdate["DREAM_DESC_MAX_LENGTH"];
            DEFAULT_SERVICE_TIMEOUT = kony.os.toNumber(getContentUpdate["DEFAULT_SERVICE_TIMEOUT"]);
            GLOBAL_OPENACT_STARTTIME = getContentUpdate["OPENACT_STARTTIME"];
            GLOBAL_OPENACT_ENDTIME = getContentUpdate["OPENACT_ENDTIME"];
            GLOBAL_EDITDREAM_STARTTIME = getContentUpdate["EDIT_DREAM_STARTTIME"];
            GLOBAL_EDITDREAM_ENDTIME = getContentUpdate["EDIT_DREAM_ENDTIME"];
            GLOBAL_ACTIVATION_HELP_TEXT_EN = getContentUpdate["MB_ACTIVATION_CODE_HELP_TEXT_EN"];
            GLOBAL_ACTIVATION_HELP_TEXT_TH = getContentUpdate["MB_ACTIVATION_CODE_HELP_TEXT_TH"];
            GLOBAL_REACTIVATION_HELP_TEXT_EN = getContentUpdate["MB_REACTIVATION_CODE_HELP_TEXT_EN"];
            GLOBAL_REACTIVATION_HELP_TEXT_TH = getContentUpdate["MB_REACTIVATION_CODE_HELP_TEXT_TH"];
            //E-STATEMENT
            GLOBAL_E_STATEMENT_PRODUCTS = getContentUpdate["E_STATEMENT_PRODUCTS"];
            GLOBAL_E_STATEMENT_IMAGES = getContentUpdate["E_STATEMENT_IMAGES"];
            Gbl_Image_Size_Limit = getContentUpdate["Max_FileSize"];
            Gbl_Prod_Code_MEACC = getContentUpdate["Prod_Code_MEACC"];
            GBL_TOUCH_ID_STATUS_COUNT = getContentUpdate["TOUCH_ID_COUNT"];
            gblQuickBalanceEnable = getContentUpdate["QUICK_BALANCE_ENABLE"];
            if (getContentUpdate["MENUCONFIG"] != null || getContentUpdate["MENUCONFIG"] != undefined) {
                gblMenuConfig = getContentUpdate["MENUCONFIG"];
            } else {
                gblMenuConfig = "";
            }
            //kony.print("gblQuickBalanceEnable:::::"+gblQuickBalanceEnable)
            if (gblQuickBalanceEnable != "ON") {
                showQuickBalanceButton();
            }
            if (isMFEnabled == true && flowSpa) campaignPreLoginService("imgMBPreLogin", "frmSPALogin", "M");
            if (getContentUpdate["FATCA_MAX_SKIP_COUNTER"] != null && getContentUpdate["FATCA_MAX_SKIP_COUNTER"] != undefined && getContentUpdate["FATCA_MAX_SKIP_COUNTER"] != "") GLOBAL_FATCA_MAX_SKIP_COUNT = parseInt(getContentUpdate["FATCA_MAX_SKIP_COUNTER"]);
            else GLOBAL_FATCA_MAX_SKIP_COUNT = 3;
            if (getContentUpdate["Start_Digs_MobileNum"] != null && getContentUpdate["Start_Digs_MobileNum"] != undefined && getContentUpdate["Start_Digs_MobileNum"] != "") Gbl_StartDigsMobileNum = getContentUpdate["Start_Digs_MobileNum"];
            else Gbl_StartDigsMobileNum = "09,08,06";
            Gbl_StartDigsMobileNum.split(',');
            GLOBAL_JOINT_CODES = getContentUpdate["JOINT_ACCNT_CODES"];
            GLOBAL_MB_APPLY_CODES = getContentUpdate["APPLY_MB_STATUS_CODES"];
            GLOBAL_Prod_code_ME = getContentUpdate["Prod_Code_MEACC"];
            GLOBAL_IB_ACTIVATION_HELP_TEXT_EN = getContentUpdate["IB_ACTIVATION_HELP_TEXT_EN"];
            GLOBAL_IB_ACTIVATION_HELP_TEXT_TH = getContentUpdate["IB_ACTIVATION_HELP_TEXT_TH"];
            GLOBAL_IB_REACTIVATION_HELP_TEXT_EN = getContentUpdate["IB_REACTIVATION_HELP_TEXT_EN"];
            GLOBAL_IB_REACTIVATION_HELP_TEXT_TH = getContentUpdate["IB_REACTIVATION_HELP_TEXT_TH"];
            firstTimeActivationPopUp = getContentUpdate["FirstTimeActivation_Popup"];
            //var deviceInfo = kony.os.deviceInfo();
            GLOBAL_TODAY_DATE = getContentUpdate["TODAY_DATE"];
            //ENH_209 start
            GLOBAL_Exchange_Rates_Link_EN = getContentUpdate["Exchange_Rates_Link_EN"];
            GLOBAL_Exchange_Rates_Link_TH = getContentUpdate["Exchange_Rates_Link_TH"];
            //ENH_209 end
            //ENH_230 start
            GLOBAL_SecurityAdvice_Link_EN = getContentUpdate["SecurityAdvice_Link_EN"];
            GLOBAL_SecurityAdvice_Link_TH = getContentUpdate["SecurityAdvice_Link_TH"];
            MBTnC = false;
            //ENH_230 end
            // Online Payment based on MWA
            GLOBAL_ONLINE_PAYMENT_VERSION_MWA = getContentUpdate["ONLINE_PAYMENT_VERSION_MWA"];
            if (getContentUpdate["Results"] != null) {
                if (getContentUpdate["Results"][0]["EnglishPhrases"].length > 0) {
                    try {
                        if (gblDeviceInfo["name"] == "thinclient" & gblDeviceInfo["type"] == "spa") {
                            if (kony.i18n.getLocalizedString("keyphrasesUpdatedOn") == "01-JAN-13 12\:00\:00") {
                                kony.i18n.deleteResourceBundle("en_US")
                                kony.i18n.updateResourceBundle(getContentUpdate["Results"][0]["EnglishPhrases"][0], "en_US");
                            } else {
                                kony.i18n.updateResourceBundle(getContentUpdate["Results"][0]["EnglishPhrases"][0], "en_US");
                            }
                        } else {
                            kony.i18n.updateResourceBundle(getContentUpdate["Results"][0]["EnglishPhrases"][0], "en_US");
                        }
                    } catch (i18nError) {
                        alert("Exception While getting updateResourceBundle english  : " + i18nError);
                    }
                }
                if (getContentUpdate["Results"][1]["ThaiPhrases"].length > 0) {
                    try {
                        if (gblDeviceInfo["name"] == "thinclient" & gblDeviceInfo["type"] == "spa") {
                            if (kony.i18n.getLocalizedString("keyphrasesUpdatedOn") == "01-JAN-13 12\:00\:00") {
                                kony.i18n.deleteResourceBundle("th_TH")
                                kony.i18n.updateResourceBundle(getContentUpdate["Results"][1]["ThaiPhrases"][0], "th_TH");
                            } else {
                                kony.i18n.updateResourceBundle(getContentUpdate["Results"][1]["ThaiPhrases"][0], "th_TH");
                            }
                        } else {
                            kony.i18n.updateResourceBundle(getContentUpdate["Results"][1]["ThaiPhrases"][0], "th_TH");
                        }
                    } catch (i18nError) {
                        alert("Exception While getting updateResourceBundle thai  : " + i18nError);
                    }
                }
                //kony.i18n.updateResourceBundle(getContentUpdate["Results"][0]["EnglishPhrases"][0], "en_US");
                //kony.i18n.updateResourceBundle(getContentUpdate["Results"][1]["ThaiPhrases"][0], "th_TH");
                var getEncrKeyFromDevice = kony.store.getItem("encrytedText");
                if (getEncrKeyFromDevice != null) {
                    //TMBUtil.DestroyForm(frmAfterLogoutMB);
                    if (kony.application.getCurrentForm() == frmMBPreLoginAccessesPin) { //frmAfterLogoutMB
                        //dismissLoadingScreen();
                        //frmMBPreLoginAccessesPin.show(); //frmAfterLogoutMB
                        if (isAuthUsingTouchSupported() && hasRegisteredFinger() && isTouchIDEnabled()) {
                            kony.print("FPRINT CALLING TOUCH POPUP");
                            kony.print("FPRINT AT APPLAUNCH");
                            showLoadingScreen();
                            GBL_FLOW_ID_CA = 13;
                            getUniqueID();
                        } else {
                            dismissLoadingScreen();
                        }
                    }
                } else {
                    //TMBUtil.DestroyForm(frmMBanking);
                    if (kony.application.getCurrentForm() == frmMBanking) {
                        dismissLoadingScreen();
                        frmMBanking.show();
                    }
                }
                checkUpgrades(getContentUpdate["updatestatus"], getContentUpdate["updateurl"]);
            }
        } else {
            dismissLoadingScreen();
        }
    }
}

function checkUpgrades(updatestatus, updateurl) {
    if (updatestatus != null) {
        //global variable
        GLOBAL_UPDATE_URL = updateurl;
        if (updatestatus == "optional") {
            //window.Alert(getLocaleSpecificText("update_available"), updateAvailableAlerthandler, "confirmation", getLocaleSpecificText("btn_confirm"),getLocaleSpecificText("btn_cancel"),getLocaleSpecificText("alert_citi_mobile"))
            var message = kony.i18n.getLocalizedString("optional_upgrade_message");
            if (message == null || message == "") {
                message = "A new version of TMB Mobile application has been released. Choose OK to update the application or Cancel to continue with sign on.";
            }
            var title = kony.i18n.getLocalizedString("optional_upgrade_title");
            var yeslbl = kony.i18n.getLocalizedString("optional_upgrade_yes_label");
            var nolbl = kony.i18n.getLocalizedString("optional_upgrade_no_label");
            if (title == null || title == "") title = "Upgrade Information";
            if (yeslbl == null || yeslbl == "") yeslbl = "Upgrade";
            if (nolbl == null || nolbl == "") nolbl = "Cancel";
            var basicConf = {
                message: message,
                alertType: constants.ALERT_TYPE_CONFIRMATION,
                alertTitle: title,
                yesLabel: yeslbl,
                noLabel: nolbl,
                alertHandler: callbackOptionalUpgrade
            };
            //Defining pspConf parameter for alert
            var pspConf = {};
            //Alert definition
            var infoAlert = kony.ui.Alert(basicConf, pspConf);
        } else if (updatestatus == "mandatory") {
            var message = kony.i18n.getLocalizedString("mandatory_upgrade_message");
            if (message == null || message == "") {
                message = "Your current application is running an outdated version. The application will now exit and direct you to update site.";
            }
            var title = kony.i18n.getLocalizedString("mandatory_upgrade_title");
            var yeslbl = kony.i18n.getLocalizedString("mandatory_upgrade_yes_label");
            var nolbl = kony.i18n.getLocalizedString("mandatory_upgrade_no_label");
            if (title == null || title == "") title = "Upgrade Error";
            if (yeslbl == null || yeslbl == "") yeslbl = "OK";
            if (nolbl == null || nolbl == "") nolbl = "Exit";
            var basicConf = {
                message: message,
                alertType: constants.ALERT_TYPE_CONFIRMATION,
                alertTitle: title,
                yesLabel: yeslbl,
                noLabel: nolbl,
                alertHandler: callbackMandatoryUpgrade
            };
            //Defining pspConf parameter for alert
            var pspConf = {};
            //Alert definition
            var infoAlert = kony.ui.Alert(basicConf, pspConf);
        }
    }
}

function callbackMandatoryUpgrade(response) {
    if (response == true) {
        //alert("Need to implement the code");
        //GLOBAL_UPDATE_URL now available
        if (GLOBAL_UPDATE_URL != null && GLOBAL_UPDATE_URL != "" && GLOBAL_UPDATE_URL != undefined) {
            //window.open(GLOBAL_UPDATE_URL);
            //frmFBLogin.browserFB.clearHistory();
            //TMBUtil.DestroyForm(frmFBLogin);
            //frmFBLogin.browserFB.url = GLOBAL_UPDATE_URL;
            //frmFBLogin.show();
            if (gblDeviceInfo["name"] == "android") {
                var PlayStoreLinkObject = new Linker.PlayStoreLink();
                PlayStoreLinkObject.displayPlayStore("com.TMBTOUCH.PRODUCTION");
            } else {
                kony.application.openURL(GLOBAL_UPDATE_URL);
            }
            kony.application.exit();
        } else {
            var message = "";
            //if(kony.os.deviceInfo().name == "android") {
            message = kony.i18n.getLocalizedString("upgrade_from_google_store");
            if (message == null || message == "") message = "Please update the application from Google play store.";
            alert(message);
        }
    } else {
        kony.application.exit();
    }
}

function callbackOptionalUpgrade(response) {
    if (response == true) {
        //alert("Need to implement the code");
        if (GLOBAL_UPDATE_URL != null && GLOBAL_UPDATE_URL != "" && GLOBAL_UPDATE_URL != undefined) {
            //window.open(GLOBAL_UPDATE_URL);
            //frmFBLogin.browserFB.clearHistory();
            //TMBUtil.DestroyForm(frmFBLogin);
            //frmFBLogin.browserFB.url = GLOBAL_UPDATE_URL;
            //frmFBLogin.show();
            if (gblDeviceInfo["name"] == "android") {
                var PlayStoreLinkObject = new Linker.PlayStoreLink();
                PlayStoreLinkObject.displayPlayStore("com.TMBTOUCH.PRODUCTION");
            } else {
                kony.application.openURL(GLOBAL_UPDATE_URL);
            }
        } else {
            var message = "";
            //if(kony.os.deviceInfo().name == "android") {
            message = kony.i18n.getLocalizedString("upgrade_from_google_store");
            if (message == null || message == "") message = "Please update the application from Google play store.";
            //} else {
            alert(message);
        }
    } else {
        //Do Nothing, so that exit form will be shown as is.
    }
}
gblBrwCmpObject = "";
gblLnkCmpObject = "";

function dynamicCampaignBrowser() {
    gblBrwCmpObject = "";
    gblLnkCmpObject = "";
    gblVbxCmp = "";
    gblBrwCmpObject = new kony.ui.Browser({
        "id": "gblBrwCmpObject",
        "text": "",
        "isVisible": true,
        "htmlString": "",
        "detectTelNumber": false
    }, {
        "margin": [0, 0, 0, 0],
        "containerHeight": null,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 54
    }, {});
    gblVbxCmp = new kony.ui.Box({
        "id": "gblVbxCmp",
        "isVisible": true,
        "orientation": constants.BOX_LAYOUT_VERTICAL
    }, {
        "containerWeight": 100,
        "margin": [0, 0, 0, 0],
        "padding": [0, 0, 0, 0],
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "vExpand": false,
        "hExpand": true,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {});
    //gblVbxCmp.add(gblBrwCmpObject, gblLnkCmpObject);
}