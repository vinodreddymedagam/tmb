function dynamicMenu_segMenuOptions_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    menuSegmentEnabling.call(this);
};

function dynamicMenu_btnMenuTransfer_onClick_seq0(eventobject) {
    transferMenuClick.call(this);
};
/*
 *Not part of go live 1 
 
function dynamicMenu_btnMenuCardlessWithdrawal_onClick_seq0(eventobject) {
    boloHi.call(this);
};
*/
function dynamicMenu_btnMenuAboutMe_onClick_seq0(eventobject) {
    segAboutMeLoad.call(this);
    gblMenuSelection = 1;
    var isIE8 = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
    if (isIE8) langspecificmenuIE8();
    else langspecificmenu();
    nameofform = kony.application.getCurrentForm();
    var isEmptyTotal = parseInt(gblMyInboxTotalCount) == 0 ? true : false;
    if (kony.i18n.getCurrentLocale() == "th_TH") {
        nameofform.btnMenuAboutMe.skin = "btnIBMenuAboutMeFocusThai";
        if (isEmptyTotal) {
            nameofform.btnMenuMyInbox.skin = "btnIBMenuMyInboxThaiZero";
        } else {
            nameofform.btnMenuMyInbox.skin = "btnIBMenuMyInboxThai";
        }
        nameofform.btnMenuConvenientServices.skin = "btnIBMenuConvenientServicesThai";
        if (!isIE8) {
            nameofform.btnMenuAboutMe.hoverSkin = "btnIBMenuAboutMeFocusThai";
            if (isEmptyTotal) {
                nameofform.btnMenuMyInbox.hoverSkin = "btnIBMenuMyInboxHoverThaiZero";
            } else {
                nameofform.btnMenuMyInbox.hoverSkin = "btnIBMenuMyInboxHoverThai";
            }
            nameofform.btnMenuConvenientServices.hoverSkin = "btnIBMenuConvenientServicesHoverThai";
        }
    } else {
        nameofform.btnMenuAboutMe.skin = "btnIBMenuAboutMeFocus";
        if (isEmptyTotal) {
            nameofform.btnMenuMyInbox.skin = "btnIBMenuMyInboxZero";
        } else {
            nameofform.btnMenuMyInbox.skin = "btnIBMenuMyInbox";
        }
        nameofform.btnMenuConvenientServices.skin = "btnIBMenuConvenientServices";
        if (!isIE8) {
            nameofform.btnMenuAboutMe.hoverSkin = "btnIBMenuAboutMeFocus";
            if (isEmptyTotal) {
                nameofform.btnMenuMyInbox.hoverSkin = "btnIBMenuMyInboxHoverZero";
            } else {
                nameofform.btnMenuMyInbox.hoverSkin = "btnIBMenuMyInboxHover";
            }
            nameofform.btnMenuConvenientServices.hoverSkin = "btnIBMenuConvenientServicesHover";
        }
    }
};

function dynamicMenu_btnMenuMyActivities_onClick_seq0(eventobject) {
    onMenuMyActivitiesClick.call(this);
};

function dynamicMenu_btnMenuMyAccountSummary_onClick_seq0(eventobject) {
    if (kony.application.getCurrentForm().id == "frmIBAccntSummary") {
        if (window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8") langspecificmenuIE8();
        else langspecificmenu();
    }
    accountsummaryLangToggleIB.call(this);
    frmIBAccntSummary.segAccountDetails.selectedIndex = [0, 0];
    //showIBAccountDetails.call(this);
};

function dynamicMenu_btnMenuMyInbox_onClick_seq0(eventobject) {
    segMyInboxLoad.call(this);
    gblMenuSelection = 3;
    var isIE8 = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
    if (isIE8) langspecificmenuIE8();
    else langspecificmenu();
    curr_form = kony.application.getCurrentForm();
    var isEmptyTotal = parseInt(gblMyInboxTotalCount) == 0 ? true : false;
    if (curr_form.id == "frmIBNotificationHome") {
        if (kony.i18n.getCurrentLocale() != "th_TH") {
            if (isEmptyTotal) {
                curr_form.btnMenuMyInbox.skin = "btnIBMenuMyInboxFocusArrowZero";
            } else {
                curr_form.btnMenuMyInbox.skin = "btnIBMenuMyInboxFocusArrow";
            }
            curr_form.btnMenuAboutMe.skin = "btnIBMenuAboutMe";
            curr_form.btnMenuConvenientServices.skin = "btnIBMenuConvenientServices";
            if (!isIE8) {
                if (isEmptyTotal) {
                    curr_form.btnMenuMyInbox.hoverSkin = "btnIBMenuMyInboxFocusArrowZero";
                } else {
                    curr_form.btnMenuMyInbox.hoverSkin = "btnIBMenuMyInboxFocusArrow";
                }
                curr_form.btnMenuAboutMe.hoverSkin = "btnIBMenuAboutMeHover";
                curr_form.btnMenuConvenientServices.hoverSkin = "btnIBMenuConvenientServicesHover";
            }
        } else {
            if (isEmptyTotal) {
                curr_form.btnMenuMyInbox.skin = "btnIBMenuMyInboxFocusThaiArrowZero";
            } else {
                curr_form.btnMenuMyInbox.skin = "btnIBMenuMyInboxFocusThaiArrow";
            }
            curr_form.btnMenuAboutMe.skin = "btnIBMenuAboutMeThai";
            curr_form.btnMenuConvenientServices.skin = "btnIBMenuConvenientServicesThai";
            if (!isIE8) {
                if (isEmptyTotal) {
                    curr_form.btnMenuMyInbox.hoverSkin = "btnIBMenuMyInboxFocusThaiArrowZero";
                } else {
                    curr_form.btnMenuMyInbox.hoverSkin = "btnIBMenuMyInboxFocusThaiArrow";
                }
                curr_form.btnMenuAboutMe.hoverSkin = "btnIBMenuAboutMeHoverThai";
                curr_form.btnMenuConvenientServices.hoverSkin = "btnIBMenuConvenientServicesHoverThai";
            }
        }
    } else if (curr_form.id == "frmIBInboxHome") {
        if (kony.i18n.getCurrentLocale() != "th_TH") {
            if (isEmptyTotal) {
                curr_form.btnMenuMyInbox.skin = "btnIBMenuMyInboxFocusZero";
            } else {
                curr_form.btnMenuMyInbox.skin = "btnIBMenuMyInboxFocus";
            }
            curr_form.btnMenuAboutMe.skin = "btnIBMenuAboutMe";
            curr_form.btnMenuConvenientServices.skin = "btnIBMenuConvenientServices";
            if (!isIE8) {
                if (isEmptyTotal) {
                    curr_form.btnMenuMyInbox.hoverSkin = "btnIBMenuMyInboxFocusZero";
                } else {
                    curr_form.btnMenuMyInbox.hoverSkin = "btnIBMenuMyInboxFocus";
                }
                curr_form.btnMenuAboutMe.hoverSkin = "btnIBMenuAboutMeHover";
                curr_form.btnMenuConvenientServices.hoverSkin = "btnIBMenuConvenientServicesHover";
            }
        } else {
            if (isEmptyTotal) {
                curr_form.btnMenuMyInbox.skin = "btnIBMenuMyInboxFocusThaiZero";
            } else {
                curr_form.btnMenuMyInbox.skin = "btnIBMenuMyInboxFocusThai";
            }
            curr_form.btnMenuAboutMe.skin = "btnIBMenuAboutMeThai";
            curr_form.btnMenuConvenientServices.skin = "btnIBMenuConvenientServicesThai";
            if (!isIE8) {
                if (isEmptyTotal) {
                    curr_form.btnMenuMyInbox.hoverSkin = "btnIBMenuMyInboxFocusThaiZero";
                } else {
                    curr_form.btnMenuMyInbox.hoverSkin = "btnIBMenuMyInboxFocusThai";
                }
                curr_form.btnMenuAboutMe.hoverSkin = "btnIBMenuAboutMeHoverThai";
                curr_form.btnMenuConvenientServices.hoverSkin = "btnIBMenuConvenientServicesHoverThai";
            }
        }
    } else {
        if (kony.i18n.getCurrentLocale() != "th_TH") {
            if (isEmptyTotal) {
                curr_form.btnMenuMyInbox.skin = "btnIBMenuMyInboxFocusZero";
            } else {
                curr_form.btnMenuMyInbox.skin = "btnIBMenuMyInboxFocus";
            }
            curr_form.btnMenuAboutMe.skin = "btnIBMenuAboutMe";
            curr_form.btnMenuConvenientServices.skin = "btnIBMenuConvenientServices";
            if (!isIE8) {
                if (isEmptyTotal) {
                    curr_form.btnMenuMyInbox.hoverSkin = "btnIBMenuMyInboxFocusZero";
                } else {
                    curr_form.btnMenuMyInbox.hoverSkin = "btnIBMenuMyInboxFocus";
                }
                curr_form.btnMenuAboutMe.hoverSkin = "btnIBMenuAboutMeHover";
                curr_form.btnMenuConvenientServices.hoverSkin = "btnIBMenuConvenientServicesHover";
            }
        } else {
            if (isEmptyTotal) {
                curr_form.btnMenuMyInbox.skin = "btnIBMenuMyInboxFocusThaiZero";
            } else {
                curr_form.btnMenuMyInbox.skin = "btnIBMenuMyInboxFocusThai";
            }
            curr_form.btnMenuAboutMe.skin = "btnIBMenuAboutMeThai";
            curr_form.btnMenuConvenientServices.skin = "btnIBMenuConvenientServicesThai";
            if (!isIE8) {
                if (isEmptyTotal) {
                    curr_form.btnMenuMyInbox.hoverSkin = "btnIBMenuMyInboxFocusThaiZero";
                } else {
                    curr_form.btnMenuMyInbox.hoverSkin = "btnIBMenuMyInboxFocusThai";
                }
                curr_form.btnMenuAboutMe.hoverSkin = "btnIBMenuAboutMeHoverThai";
                curr_form.btnMenuConvenientServices.hoverSkin = "btnIBMenuConvenientServicesHoverThai";
            }
        }
    }
};

function dynamicMenu_btnMenuBillPayment_onClick_seq0(eventobject) {
    gblPaynow = true;
    gblFromAccountSummary = false;
    gblFromAccountSummaryBillerAdded = false
    gblBillPaymentEdit = false;
    gblFirstTimeBillPayment = true;
    gblRetryCountRequestOTP = 0;
    callBillPaymentCustomerAccountServiceIB.call(this);
};

function dynamicMenu_btnMenuTopUp_onClick_seq0(eventobject) {
    gblPaynow = true;
    gblRetryCountRequestOTP = 0;
    gblEditTopUp = false;
    clearAllTopUpGlobalsIB();
    callTopUpPaymentCustomerAccountServiceIB.call(this);
};

function dynamicMenu_btnMenuConvenientServices_onClick_seq0(eventobject) {
    segConvenientServicesLoad.call(this);
    gblMenuSelection = 2;
    var isIE8 = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
    if (isIE8) langspecificmenuIE8();
    else langspecificmenu();
    nameofform = kony.application.getCurrentForm();
    var isEmptyTotal = parseInt(gblMyInboxTotalCount) == 0 ? true : false;
    if (kony.i18n.getCurrentLocale() == "th_TH") {
        nameofform.btnMenuConvenientServices.skin = "btnIBMenuConvenientServicesFocusThai"
        nameofform.btnMenuAboutMe.skin = "btnIBMenuAboutMeThai"
        if (isEmptyTotal) {
            nameofform.btnMenuMyInbox.skin = "btnIBMenuMyInboxThaiZero";
        } else {
            nameofform.btnMenuMyInbox.skin = "btnIBMenuMyInboxThai";
        }
        if (!isIE8) {
            nameofform.btnMenuConvenientServices.hoverSkin = "btnIBMenuConvenientServicesFocusThai"
            nameofform.btnMenuAboutMe.hoverSkin = "btnIBMenuAboutMeHoverThai"
            if (isEmptyTotal) {
                nameofform.btnMenuMyInbox.hoverSkin = "btnIBMenuMyInboxHoverThaiZero";
            } else {
                nameofform.btnMenuMyInbox.hoverSkin = "btnIBMenuMyInboxHoverThai";
            }
        }
    } else {
        nameofform.btnMenuConvenientServices.skin = "btnIBMenuConvenientServicesFocus"
        nameofform.btnMenuAboutMe.skin = "btnIBMenuAboutMe"
        if (isEmptyTotal) {
            nameofform.btnMenuMyInbox.skin = "btnIBMenuMyInboxZero"
        } else {
            nameofform.btnMenuMyInbox.skin = "btnIBMenuMyInbox"
        }
        if (!isIE8) {
            nameofform.btnMenuConvenientServices.hoverSkin = "btnIBMenuConvenientServicesFocus"
            nameofform.btnMenuAboutMe.hoverSkin = "btnIBMenuAboutMeHover"
            if (isEmptyTotal) {
                nameofform.btnMenuMyInbox.hoverSkin = "btnIBMenuMyInboxHoverZero"
            } else {
                nameofform.btnMenuMyInbox.hoverSkin = "btnIBMenuMyInboxHover"
            }
        }
    }
};

function addIBMenu() {
    var currentFormId = kony.application.getCurrentForm();
    var btnMenuTransfer = new kony.ui.Button({
        "id": "btnMenuTransfer",
        "isVisible": true,
        "text": null,
        "skin": "btnIBMenuTransfer",
        "onClick": dynamicMenu_btnMenuTransfer_onClick_seq0
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "padding": [0, 3, 0, 3],
        "displayText": true,
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "displayText": true,
        "marginInPixel": false,
        "paddingInPixel": false,
        "gridCell": {
            "colSpan": 1,
            "rowSpan": 3,
            "rowNo": 0,
            "colNo": 0
        },
        "containerWeight": 35
    }, {
        "toolTip": null,
        "hoverskin": "btnIBMenuTransferHover"
    });
    var btnMenuAboutMe = new kony.ui.Button({
        "id": "btnMenuAboutMe",
        "isVisible": true,
        "text": null,
        "skin": "btnIBMenuAboutMe",
        "onClick": dynamicMenu_btnMenuAboutMe_onClick_seq0
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "padding": [0, 3, 0, 3],
        "displayText": true,
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "marginInPixel": false,
        "paddingInPixel": false,
        "gridCell": {
            "colSpan": 1,
            "rowSpan": 1,
            "rowNo": 3,
            "colNo": 0
        },
        "containerWeight": 30
    }, {
        "toolTip": null,
        "hoverskin": "btnIBMenuAboutMeHover"
    });
    var btnMenuMyActivities = new kony.ui.Button({
        "id": "btnMenuMyActivities",
        "isVisible": true,
        "text": null,
        "skin": "btnIBMenuMyActivities",
        "onClick": dynamicMenu_btnMenuMyActivities_onClick_seq0
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "padding": [0, 3, 0, 3],
        "displayText": true,
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "marginInPixel": false,
        "paddingInPixel": false,
        "gridCell": {
            "colSpan": 2,
            "rowSpan": 1,
            "rowNo": 0,
            "colNo": 1
        },
        "containerWeight": 0
    }, {
        "toolTip": null,
        "hoverskin": "btnIBMenuMyActivitiesHover"
    });
    var btnMenuMyAccountSummary = new kony.ui.Button({
        "id": "btnMenuMyAccountSummary",
        "isVisible": true,
        "text": null,
        "skin": "btnIBMenuMyAccountSummary",
        "onClick": dynamicMenu_btnMenuMyAccountSummary_onClick_seq0
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "padding": [0, 3, 0, 3],
        "displayText": true,
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "marginInPixel": false,
        "paddingInPixel": false,
        "gridCell": {
            "colSpan": 1,
            "rowSpan": 2,
            "rowNo": 1,
            "colNo": 1
        },
        "containerWeight": 0
    }, {
        "toolTip": null,
        "hoverskin": "btnIBMenuMyAccountSummaryHover"
    });
    var isEmptyTotal = parseInt(gblMyInboxTotalCount) == 0 ? true : false;
    var btnMenuMyInbox = new kony.ui.Button({
        "id": "btnMenuMyInbox",
        "isVisible": true,
        "text": (isEmptyTotal ? "" : gblMyInboxTotalCount),
        "skin": (isEmptyTotal ? "btnIBMenuMyInboxZero" : "btnIBMenuMyInbox"),
        "onClick": dynamicMenu_btnMenuMyInbox_onClick_seq0
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "padding": [0, 48, 35, 0],
        "displayText": true,
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "marginInPixel": false,
        "paddingInPixel": false,
        "gridCell": {
            "colSpan": 1,
            "rowSpan": 1,
            "rowNo": 3,
            "colNo": 1
        },
        "containerWeight": 0
    }, {
        "toolTip": null,
        "hoverskin": (isEmptyTotal ? "btnIBMenuMyInboxHoverZero" : "btnIBMenuMyInboxHover")
    });
    var btnMenuBillPayment = new kony.ui.Button({
        "id": "btnMenuBillPayment",
        "isVisible": true,
        "text": null,
        "skin": "btnIBMenuBillPayment",
        "onClick": dynamicMenu_btnMenuBillPayment_onClick_seq0
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "padding": [0, 3, 0, 3],
        "displayText": true,
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "marginInPixel": false,
        "paddingInPixel": false,
        "gridCell": {
            "colSpan": 1,
            "rowSpan": 1,
            "rowNo": 1,
            "colNo": 2
        },
        "containerWeight": 0
    }, {
        "toolTip": null,
        "hoverskin": "btnIBMenuBillPaymentHover"
    });
    var btnMenuTopUp = new kony.ui.Button({
        "id": "btnMenuTopUp",
        "isVisible": true,
        "text": null,
        "skin": "btnIBMenuTopUp",
        "onClick": dynamicMenu_btnMenuTopUp_onClick_seq0
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "padding": [0, 3, 0, 3],
        "displayText": true,
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "marginInPixel": false,
        "paddingInPixel": false,
        "gridCell": {
            "colSpan": 1,
            "rowSpan": 1,
            "rowNo": 2,
            "colNo": 2
        },
        "containerWeight": 0
    }, {
        "toolTip": null,
        "hoverskin": "btnIBMenuTopUpHover"
    });
    var btnMenuConvenientServices = new kony.ui.Button({
        "id": "btnMenuConvenientServices",
        "isVisible": true,
        "text": null,
        "skin": "btnIBMenuConvenientServices",
        "onClick": dynamicMenu_btnMenuConvenientServices_onClick_seq0
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "padding": [0, 3, 0, 3],
        "displayText": true,
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "marginInPixel": false,
        "paddingInPixel": false,
        "gridCell": {
            "colSpan": 1,
            "rowSpan": 1,
            "rowNo": 3,
            "colNo": 2
        },
        "containerWeight": 0
    }, {
        "toolTip": null,
        "hoverskin": "btnIBMenuConvenientServicesHover"
    });
    var hbox451137409789493 = new kony.ui.Box({
        "id": "hbox451137409789493",
        "isVisible": true,
        "position": constants.BOX_POSITION_AS_NORMAL,
        "orientation": constants.BOX_LAYOUT_HORIZONTAL
    }, {
        "containerWeight": 74,
        "percent": true,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
        "margin": [0, 0, 0, 0],
        "padding": [0, 0, 0, 0],
        "vExpand": false,
        "marginInPixel": false,
        "paddingInPixel": false,
        "layoutType": constants.CONTAINER_LAYOUT_GRID,
        "layoutMeta": {
            "cols": 3,
            "colmeta": [{
                "width": "34"
            }, {
                "width": "33"
            }, {
                "width": "33"
            }],
            "rows": 4
        }
    }, {});
    hbox451137409789493.add(btnMenuTransfer, btnMenuAboutMe, btnMenuMyActivities, btnMenuMyAccountSummary, btnMenuMyInbox, btnMenuBillPayment, btnMenuTopUp, btnMenuConvenientServices);
    var segMenuOptionsbox = new kony.ui.Box({
        "id": "segMenuOptionsbox",
        "isVisible": true,
        "orientation": constants.BOX_LAYOUT_VERTICAL
    }, {
        "layoutAlignment": constants.BOX_LAYOUT_ALIGN_FROM_LEFT,
        "containerWeight": 44
    }, {});
    var segMenuOptions = new kony.ui.SegmentedUI2({
        "id": "segMenuOptions",
        "isVisible": true,
        "retainSelection": false,
        "widgetDataMap": {
            "lblSegData": "lblSegData"
        },
        "rowTemplate": segMenuOptionsbox,
        "rowSkin": "seg2Normal",
        "rowFocusSkin": "seg2Focus",
        "sectionHeaderSkin": "seg2Header",
        "separatorRequired": true,
        "separatorThickness": 1,
        "separatorColor": "256a9900",
        "showScrollbars": false,
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "groupCells": false,
        "screenLevelWidget": false,
        "onRowClick": dynamicMenu_segMenuOptions_onRowClick_seq0,
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR
    }, {
        "margin": [0, 0, 0, 0],
        "padding": [0, 0, 0, 0],
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 44
    }, {});
    var lblSegData = new kony.ui.Label({
        "id": "lblSegData",
        "isVisible": true,
        "skin": "lblIBSegMenu"
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "padding": [3.5, 3, 1, 3],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 13
    }, {
        "hoverSkin": "lblIBsegMenuFocus",
        "toolTip": null
    });
    segMenuOptionsbox.add(lblSegData);
    var scrollMenu = new kony.ui.ScrollBox({
        "id": "scrollMenu",
        "isVisible": true,
        "orientation": constants.BOX_LAYOUT_VERTICAL,
        "scrollDirection": constants.SCROLLBOX_SCROLL_VERTICAL,
        "showScrollbars": false,
        "position": constants.BOX_POSITION_AS_NORMAL,
        "enableScrollByPage": false
    }, {
        "percent": true,
        "margin": [0, 0, 0, 0],
        "padding": [0, 0, 0, 0],
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 100
    }, {});
    /*
    if(currentFormId.scrollMenu != undefined)
    {
    	while (currentFormId.scrollMenu.removeAt(0) != undefined){
    		currentFormId.scrollMenu.removeAt(0);
    	}
    	currentFormId.scrollMenu.add(hbox451137409789493, segMenuOptions);
    }
    else
    */
    // Code to add the Menu after removing all the previous occurences.
    while (currentFormId.vbxTotalMenu.removeAt(0) != undefined) {
        currentFormId.vbxTotalMenu.removeAt(0);
    }
    currentFormId.vbxTotalMenu.add(hbox451137409789493, segMenuOptions);
    if (window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8") langspecificmenuIE8();
    else langspecificmenu();
}

function langspecificmenu() {
    var currentFormId = kony.application.getCurrentForm();
    var isEmptyTotal = parseInt(gblMyInboxTotalCount) == 0 ? true : false;
    if (kony.i18n.getCurrentLocale() != "th_TH") {
        currentFormId.btnMenuAboutMe.skin = "btnIBMenuAboutMe";
        //currentFormId.btnMenuAboutMe.focusSkin = "btnIBMenuAboutMeFocus";
        currentFormId.btnMenuAboutMe.hoverSkin = "btnIBMenuAboutMeHover";
        //bill payment special skin assignment starting here ,Do Not Edit or Delete --- I have done it and handled highlighting in your specific lang function
        currentFormId.btnMenuBillPayment.skin = "btnIBMenuBillPayment";
        currentFormId.btnMenuBillPayment.hoverSkin = "btnIBMenuBillPaymentHover";
        // ending bill payment skins
        //currentFormId.btnMenuCardlessWithdrawal.skin = "btnIBMenuCardlessWithdrawal";
        //currentFormId.btnMenuCardlessWithdrawal.focusSkin = "btnIBMenuCardlessWithdrawalFocus";
        //currentFormId.btnMenuCardlessWithdrawal.hoverSkin = "btnIBMenuCardlessWithdrawalHover";
        currentFormId.btnMenuConvenientServices.skin = "btnIBMenuConvenientServices";
        //currentFormId.btnMenuConvenientServices.focusSkin = "btnIBMenuConvenientServicesFocus";
        currentFormId.btnMenuConvenientServices.hoverSkin = "btnIBMenuConvenientServicesHover";
        currentFormId.btnMenuMyAccountSummary.skin = "btnIBMenuMyAccountSummary";
        //currentFormId.btnMenuMyAccountSummary.focusSkin = "btnIBMenuMyAccountSummaryFocus";
        currentFormId.btnMenuMyAccountSummary.hoverSkin = "btnIBMenuMyAccountSummaryHover";
        currentFormId.btnMenuMyActivities.skin = "btnIBMenuMyActivities";
        //currentFormId.btnMenuMyActivities.focusSkin = "btnIBMenuMyActivitiesFocus";
        currentFormId.btnMenuMyActivities.hoverSkin = "btnIBMenuMyActivitiesHover";
        //currentFormId.btnMenuMyInbox.focusSkin = "btnIBMenuMyInboxFocus";
        if (isEmptyTotal) {
            currentFormId.btnMenuMyInbox.skin = "btnIBMenuMyInboxZero";
            currentFormId.btnMenuMyInbox.hoverSkin = "btnIBMenuMyInboxHoverZero";
        } else {
            currentFormId.btnMenuMyInbox.skin = "btnIBMenuMyInbox";
            currentFormId.btnMenuMyInbox.hoverSkin = "btnIBMenuMyInboxHover";
        }
        currentFormId.btnMenuMyInbox.text = (isEmptyTotal ? "" : gblMyInboxTotalCount);
        //topup module skins assignment ,do not edit
        currentFormId.btnMenuTopUp.skin = "btnIBMenuTopUp";
        currentFormId.btnMenuTopUp.hoverSkin = "btnIBMenuTopUpHover";
        currentFormId.btnMenuTransfer.skin = "btnIBMenuTransfer";
        // currentFormId.btnMenuTransfer.focusSkin = "btnIBMenuTransferFocus";
        currentFormId.btnMenuTransfer.hoverSkin = "btnIBMenuTransferHover";
    } else {
        currentFormId.btnMenuAboutMe.skin = "btnIBMenuAboutMeThai";
        // currentFormId.btnMenuAboutMe.focusSkin = "btnIBMenuAboutMeFocusThai";
        currentFormId.btnMenuAboutMe.hoverSkin = "btnIBMenuAboutMeHoverThai";
        //bill payment special skin assignment starting here ,Do Not Edit or Delete
        currentFormId.btnMenuBillPayment.skin = "btnIBMenuBillPaymentThai";
        currentFormId.btnMenuBillPayment.hoverSkin = "btnIBMenuBillPaymentHoverThai";
        //currentFormId.btnMenuCardlessWithdrawal.skin = "btnIBMenuCardlessWithdrawalThai";
        //currentFormId.btnMenuCardlessWithdrawal.focusSkin = "btnIBMenuCardlessWithdrawalFocusThai";
        //currentFormId.btnMenuCardlessWithdrawal.hoverSkin = "btnIBMenuCardlessWithdrawalHoverThai";
        currentFormId.btnMenuConvenientServices.skin = "btnIBMenuConvenientServicesThai";
        //  currentFormId.btnMenuConvenientServices.focusSkin = "btnIBMenuConvenientServicesFocusThai";
        currentFormId.btnMenuConvenientServices.hoverSkin = "btnIBMenuConvenientServicesHoverThai";
        currentFormId.btnMenuMyAccountSummary.skin = "btnIBMenuMyAccountSummaryThai";
        //  currentFormId.btnMenuMyAccountSummary.focusSkin = "btnIBMenuMyAccountSummaryFocusThai";
        currentFormId.btnMenuMyAccountSummary.hoverSkin = "btnIBMenuMyAccountSummaryHoverThai";
        currentFormId.btnMenuMyActivities.skin = "btnIBMenuMyActivitiesThai";
        //  currentFormId.btnMenuMyActivities.focusSkin = "btnIBMenuMyActivitiesFocusThai";
        currentFormId.btnMenuMyActivities.hoverSkin = "btnIBMenuMyActivitiesHoverThai";
        if (isEmptyTotal) {
            currentFormId.btnMenuMyInbox.skin = "btnIBMenuMyInboxThaiZero";
        } else {
            currentFormId.btnMenuMyInbox.skin = "btnIBMenuMyInboxThai";
        }
        //  currentFormId.btnMenuMyInbox.focusSkin = "btnIBMenuMyInboxFocusThai";
        if (isEmptyTotal) {
            currentFormId.btnMenuMyInbox.hoverSkin = "btnIBMenuMyInboxHoverThaiZero";
        } else {
            currentFormId.btnMenuMyInbox.hoverSkin = "btnIBMenuMyInboxHoverThai";
        }
        currentFormId.btnMenuMyInbox.text = (isEmptyTotal ? "" : gblMyInboxTotalCount);
        currentFormId.btnMenuTopUp.skin = "btnIBMenuTopUpThai";
        currentFormId.btnMenuTopUp.hoverSkin = "btnIBMenuTopUpHoverThai";
        currentFormId.btnMenuTransfer.skin = "btnIBMenuTransferThai";
        //   currentFormId.btnMenuTransfer.focusSkin = "btnIBMenuTransferFocusThai";
        currentFormId.btnMenuTransfer.hoverSkin = "btnIBMenuTransferHoverThai";
    }
}

function langspecificmenuIE8() {
    langspecificmenu();
    return;
    var currentFormId = kony.application.getCurrentForm();
    var isEmptyTotal = parseInt(gblMyInboxTotalCount) == 0 ? true : false;
    if (kony.i18n.getCurrentLocale() != "th_TH") {
        currentFormId.btnMenuAboutMe.skin = "btnIBMenuAboutMe";
        // currentFormId.btnMenuAboutMe.focusSkin = "btnIBMenuAboutMeFocus";
        // currentFormId.btnMenuAboutMe.hoverSkin = "btnIBMenuAboutMeHover";
        //bill payment special skin assignment starting here ,Do Not Edit or Delete
        currentFormId.btnMenuBillPayment.skin = "btnIBMenuBillPayment";
        // currentFormId.btnMenuBillPayment.focusSkin = "btnIBMenuBillPaymentFocus";
        //  currentFormId.btnMenuBillPayment.hoverSkin = "btnIBMenuBillPaymentHover";
        // ending bill payment skins
        //currentFormId.btnMenuCardlessWithdrawal.skin = "btnIBMenuCardlessWithdrawal";
        //currentFormId.btnMenuCardlessWithdrawal.focusSkin = "btnIBMenuCardlessWithdrawalFocus";
        //currentFormId.btnMenuCardlessWithdrawal.hoverSkin = "btnIBMenuCardlessWithdrawalHover";
        currentFormId.btnMenuConvenientServices.skin = "btnIBMenuConvenientServices";
        // currentFormId.btnMenuConvenientServices.focusSkin = "btnIBMenuConvenientServicesFocus";
        //  currentFormId.btnMenuConvenientServices.hoverSkin = "btnIBMenuConvenientServicesHover";
        currentFormId.btnMenuMyAccountSummary.skin = "btnIBMenuMyAccountSummary";
        //  currentFormId.btnMenuMyAccountSummary.focusSkin = "btnIBMenuMyAccountSummaryFocus";
        //  currentFormId.btnMenuMyAccountSummary.hoverSkin = "btnIBMenuMyAccountSummaryHover";
        currentFormId.btnMenuMyActivities.skin = "btnIBMenuMyActivities";
        // currentFormId.btnMenuMyActivities.focusSkin = "btnIBMenuMyActivitiesFocus";
        //  currentFormId.btnMenuMyActivities.hoverSkin = "btnIBMenuMyActivitiesHover";
        if (isEmptyTotal) {
            currentFormId.btnMenuMyInbox.skin = "btnIBMenuMyInboxZero";
        } else {
            currentFormId.btnMenuMyInbox.skin = "btnIBMenuMyInbox";
        }
        currentFormId.btnMenuMyInbox.text = (isEmptyTotal ? "" : gblMyInboxTotalCount);
        //  currentFormId.btnMenuMyInbox.focusSkin = "btnIBMenuMyInboxFocus";
        //  currentFormId.btnMenuMyInbox.hoverSkin = "btnIBMenuMyInboxHover";
        //topup module skins assignment ,do not edit
        currentFormId.btnMenuTopUp.skin = "btnIBMenuTopUp";
        // currentFormId.btnMenuTopUp.focusSkin = "btnIBMenuTopUpFocus";
        // currentFormId.btnMenuTopUp.hoverSkin = "btnIBMenuTopUpHover";
        currentFormId.btnMenuTransfer.skin = "btnIBMenuTransfer";
        //  currentFormId.btnMenuTransfer.focusSkin = "btnIBMenuTransferFocus";
        //  currentFormId.btnMenuTransfer.hoverSkin = "btnIBMenuTransferHover";
    } else {
        currentFormId.btnMenuAboutMe.skin = "btnIBMenuAboutMeThai";
        //  currentFormId.btnMenuAboutMe.focusSkin = "btnIBMenuAboutMeFocusThai";
        //  currentFormId.btnMenuAboutMe.hoverSkin = "btnIBMenuAboutMeHoverThai";
        //bill payment special skin assignment starting here ,Do Not Edit or Delete
        currentFormId.btnMenuBillPayment.skin = "btnIBMenuBillPaymentThai";
        //Ending Here
        //  currentFormId.btnMenuBillPayment.focusSkin = "btnIBMenuBillPaymentFocusThai";
        //  currentFormId.btnMenuBillPayment.hoverSkin = "btnIBMenuBillPaymentHoverThai";
        //currentFormId.btnMenuCardlessWithdrawal.skin = "btnIBMenuCardlessWithdrawalThai";
        //currentFormId.btnMenuCardlessWithdrawal.focusSkin = "btnIBMenuCardlessWithdrawalFocusThai";
        //currentFormId.btnMenuCardlessWithdrawal.hoverSkin = "btnIBMenuCardlessWithdrawalHoverThai";
        currentFormId.btnMenuConvenientServices.skin = "btnIBMenuConvenientServicesThai";
        //   currentFormId.btnMenuConvenientServices.focusSkin = "btnIBMenuConvenientServicesFocusThai";
        //   currentFormId.btnMenuConvenientServices.hoverSkin = "btnIBMenuConvenientServicesHoverThai";
        currentFormId.btnMenuMyAccountSummary.skin = "btnIBMenuMyAccountSummaryThai";
        //   currentFormId.btnMenuMyAccountSummary.focusSkin = "btnIBMenuMyAccountSummaryFocusThai";
        //   currentFormId.btnMenuMyAccountSummary.hoverSkin = "btnIBMenuMyAccountSummaryHoverThai";
        currentFormId.btnMenuMyActivities.skin = "btnIBMenuMyActivitiesThai";
        //  currentFormId.btnMenuMyActivities.focusSkin = "btnIBMenuMyActivitiesFocusThai";
        //  currentFormId.btnMenuMyActivities.hoverSkin = "btnIBMenuMyActivitiesHoverThai";
        if (isEmptyTotal) {
            currentFormId.btnMenuMyInbox.skin = "btnIBMenuMyInboxThaiZero";
        } else {
            currentFormId.btnMenuMyInbox.skin = "btnIBMenuMyInboxThai";
        }
        //   currentFormId.btnMenuMyInbox.focusSkin = "btnIBMenuMyInboxFocusThai";
        //   currentFormId.btnMenuMyInbox.hoverSkin = "btnIBMenuMyInboxHoverThai";
        currentFormId.btnMenuTopUp.skin = "btnIBMenuTopUpThai";
        //   currentFormId.btnMenuTopUp.focusSkin = "btnIBMenuTopUpFocusThai";
        //   currentFormId.btnMenuTopUp.hoverSkin = "btnIBMenuTopUpHoverThai";
        currentFormId.btnMenuTransfer.skin = "btnIBMenuTransferThai";
        //   currentFormId.btnMenuTransfer.focusSkin = "btnIBMenuTransferFocusThai";
        //   currentFormId.btnMenuTransfer.hoverSkin = "btnIBMenuTransferHoverThai";
    }
}

function submenuLangChange() {
    var currentFormId = kony.application.getCurrentForm();
    if (undefined != currentFormId.segMenuOptions) {
        if (currentFormId.segMenuOptions.data != "") {
            if (gblMenuSelection == 1) currentFormId.btnMenuAboutMe.onClick();
            if (gblMenuSelection == 2) currentFormId.btnMenuConvenientServices.onClick();
            if (gblMenuSelection == 3) currentFormId.btnMenuMyInbox.onClick();
        }
    }
}

function footerPagesMenu() {
    var currentFormId = kony.application.getCurrentForm();
    var isEmptyTotal = parseInt(gblMyInboxTotalCount) == 0 ? true : false;
    if (kony.i18n.getCurrentLocale() != "th_TH") {
        currentFormId.btnMenuAboutMe.skin = "btnIBMenuAboutMeHover";
        currentFormId.btnMenuBillPayment.skin = "btnIBMenuBillPaymentHover";
        currentFormId.btnMenuConvenientServices.skin = "btnIBMenuConvenientServicesHover";
        currentFormId.btnMenuMyAccountSummary.skin = "btnIBMenuMyAccountSummaryHover";
        currentFormId.btnMenuMyActivities.skin = "btnIBMenuMyActivitiesHover";
        if (isEmptyTotal) {
            currentFormId.btnMenuMyInbox.skin = "btnIBMenuMyInboxHoverZero";
        } else {
            currentFormId.btnMenuMyInbox.skin = "btnIBMenuMyInboxHover";
        }
        currentFormId.btnMenuTopUp.skin = "btnIBMenuTopUpHover";
        currentFormId.btnMenuTransfer.skin = "btnIBMenuTransferHover";
    } else {
        currentFormId.btnMenuAboutMe.skin = "btnIBMenuAboutMeHoverThai";
        currentFormId.btnMenuBillPayment.skin = "btnIBMenuBillPaymentHoverThai";
        currentFormId.btnMenuConvenientServices.skin = "btnIBMenuConvenientServicesHoverThai";
        currentFormId.btnMenuMyAccountSummary.skin = "btnIBMenuMyAccountSummaryHoverThai";
        currentFormId.btnMenuMyActivities.skin = "btnIBMenuMyActivitiesHoverThai";
        if (isEmptyTotal) {
            currentFormId.btnMenuMyInbox.skin = "btnIBMenuMyInboxHoverThaiZero";
        } else {
            currentFormId.btnMenuMyInbox.skin = "btnIBMenuMyInboxHoverThai";
        }
        currentFormId.btnMenuTopUp.skin = "btnIBMenuTopUpHoverThai";
        currentFormId.btnMenuTransfer.skin = "btnIBMenuTransferHoverThai";
    }
    currentFormId.segMenuOptions.removeAll();
}