function menuReset() {
    //nameofform = kony.application.getCurrentForm();
    //	nameofform.btnMenuAboutMe.skin = "hbxIBAboutMeMenu";
    //	nameofform.hbxMenuBillPayment.skin = "hbxIBBillPayMenu";
    //	nameofform.hbxMenuCardlessWithdrawal.skin = "hbxIBCardlessMenu";
    //	nameofform.btnMenuConvenientServices.skin = "hbxIBConvServMenu"
    //	nameofform.hbxMenuMyAccountSummary.skin = "hbxIBMyAcctSummaryMenu"
    //	nameofform.hbxMenuMyActivities.skin = "hbxIBMyActivitiesMenu"
    //	nameofform.hbxMenuMyInbox.skin = "hbxIBMyInboxMenu"
    //	nameofform.hbxMenuTopUp.skin = "hbxIBTopUpMenu";
    //	nameofform.btnMenuTransfer.skin = "hbxIBTransferMenu";
    //	nameofform.segMenuOptions.removeAll();
}

function menuReset2() {
    /* removing as per ENH_028 - IB Menu on Home Screen
	if(kony.i18n.getCurrentLocale() == "th_TH")
	{
		frmIBPostLoginDashboard.btnMenuAboutMe.skin = "btnIBDashboardAboutMeThai"
		//frmIBPostLoginDashboard.btnMenuAboutMe.focusSkin = "btnIBDashboardAboutMeFocusThai"
		frmIBPostLoginDashboard.btnMenuBillPayment.skin = "btnIBDashboardBillPaymentThai"
		//frmIBPostLoginDashboard.btnMenuBillPayment.focusSkin = "btnIBDashboardBillPaymentFocusThai"
		frmIBPostLoginDashboard.btnMenuConvenientServices.skin = "btnIBDashboardConvenientServicesThai"
		//frmIBPostLoginDashboard.btnMenuConvenientServices.focusSkin = "btnIBDashboardConvenientServicesFocusThai"
		frmIBPostLoginDashboard.btnMenuMyInbox.skin = "btnIBDashboardMyInboxThai"
		//frmIBPostLoginDashboard.btnMenuMyInbox.focusSkin = "btnIBDashboardMyInboxFocusThai"
		frmIBPostLoginDashboard.btnMenuTopUp.skin = "btnIBDashboardTopUpThai"
		//frmIBPostLoginDashboard.btnMenuTopUp.focusSkin = "btnIBDashboardTopUpFocusThai"
		frmIBPostLoginDashboard.btnMenuTransfer.skin = "btnIBDashboardTransferThai"
		//frmIBPostLoginDashboard.btnMenuTransfer.focusSkin = "btnIBDashboardTransferFocusThai"
	}
	else
	{	
		frmIBPostLoginDashboard.btnMenuAboutMe.skin = "btnIBDashboardAboutMe"
		//frmIBPostLoginDashboard.btnMenuAboutMe.focusSkin = "btnIBDashboardAboutMeFocus"
		frmIBPostLoginDashboard.btnMenuBillPayment.skin = "btnIBDashboardBillPayment"
		//frmIBPostLoginDashboard.btnMenuBillPayment.focusSkin = "btnIBDashboardBillPaymentFocus"
		frmIBPostLoginDashboard.btnMenuConvenientServices.skin = "btnIBDashboardConvenientServices"
		//frmIBPostLoginDashboard.btnMenuConvenientServices.focusSkin = "btnIBDashboardConvenientServicesFocus"
		frmIBPostLoginDashboard.btnMenuMyInbox.skin = "btnIBDashboardMyInbox"
		//frmIBPostLoginDashboard.btnMenuMyInbox.focusSkin = "btnIBDashboardMyInboxFocus"
		frmIBPostLoginDashboard.btnMenuTopUp.skin = "btnIBDashboardTopUp"
		//frmIBPostLoginDashboard.btnMenuTopUp.focusSkin = "btnIBDashboardTopUpFocus"
		frmIBPostLoginDashboard.btnMenuTransfer.skin = "btnIBDashboardTransfer"
		//frmIBPostLoginDashboard.btnMenuTransfer.focusSkin = "btnIBDashboardTransferFocus"
	}
    frmIBPostLoginDashboard.segMenuOptions.removeAll();
    */
}

function segAboutMeLoad() {
    var addDevice = false;
    var resetPwd = false;
    var activatedCust = false;
    curr_form = kony.application.getCurrentForm();
    curr_form.segMenuOptions.setData([{
        "lblSegData": {
            "hoverSkin": "lblIBSegMenu",
            "text": kony.i18n.getLocalizedString("keyMyProfile")
        }
    }, {
        "lblSegData": {
            "skin": "lblIBSegMenu",
            "text": kony.i18n.getLocalizedString("keyMyAccountsIB")
        }
    }, {
        "lblSegData": {
            "skin": "lblIBSegMenu",
            "text": kony.i18n.getLocalizedString("keyMyRecipients")
        }
    }, {
        "lblSegData": {
            "skin": "lblIBSegMenu",
            "text": kony.i18n.getLocalizedString("keyMyBills")
        }
    }, {
        "lblSegData": {
            "skin": "lblIBSegMenu",
            "text": kony.i18n.getLocalizedString("myTopUpsMB")
        }
    }, {
        "lblSegData": {
            "skin": "lblIBSegMenu",
            "text": kony.i18n.getLocalizedString("ActivateTokenIB")
        }
    }]);
    if (!gblTokenActivationFlag) curr_form.segMenuOptions.removeAt(5);
    //Removed as part of ENH_207_7	
}

function isShowEStatementMenu() {
    //var isCash2Go = gblAccountTable["haveCash2Go"] == "yes" ? isCash2Go = true : isCash2Go = false;
    //var isCreditCard = gblAccountTable["haveCreditCard"] == "yes" ? isCreditCard = true : isCreditCard = false;
    //var isReadyCash = gblAccountTable["haveReadyCash"] == "yes" ? isReadyCash = true : isReadyCash = false;
    var isCreditCard = gblAccountTable["haveCreditCardProd"] !== undefined && gblAccountTable["haveCreditCard"] == "yes" && gblAccountTable["haveCreditCardImg"] !== undefined;
    var isReadyCash = gblAccountTable["haveReadyCashProd"] !== undefined && gblAccountTable["haveReadyCash"] == "yes" && gblAccountTable["haveReadyCashImg"] !== undefined;
    var isCash2Go = gblAccountTable["haveCash2GoProd"] !== undefined && gblAccountTable["haveCash2Go"] == "yes" && gblAccountTable["haveCash2GoImg"] !== undefined;
    return (isCash2Go || isCreditCard || isReadyCash);
}

function segConvenientServicesLoad() {
    var activatedCust = false;
    curr_form = kony.application.getCurrentForm();
    curr_form.segMenuOptions.setData([{
        lblSegData: {
            "skin": "lblIBSegMenu",
            "text": kony.i18n.getLocalizedString("MIB_AnyIDMenu")
        }
    }, {
        lblSegData: {
            "skin": "lblIBSegMenu",
            "text": kony.i18n.getLocalizedString("kelblOpenNewAccount")
        }
    }, {
        lblSegData: {
            "skin": "lblIBSegMenu",
            "text": kony.i18n.getLocalizedString("lblChequeServiceIB")
        }
    }, {
        lblSegData: {
            "skin": "lblIBSegMenu",
            "text": kony.i18n.getLocalizedString("keyEStmt")
        }
    }, {
        lblSegData: {
            "skin": "lblIBSegMenu",
            "text": kony.i18n.getLocalizedString("keyMBankinglblMobileBanking")
        }
    }]);
    if (gblShowAnyIDRegistration == "false") {
        curr_form.segMenuOptions.removeAt(0);
        if (!isShowEStatementMenu()) curr_form.segMenuOptions.removeAt(2);
        //Removed as part of ENH_207_7
    } else {
        if (!isShowEStatementMenu()) curr_form.segMenuOptions.removeAt(3);
    }
}

function segMyInboxLoad() {
    curr_form = kony.application.getCurrentForm();
    var isChromeN = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
    var isIE8N = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
    var isIE9N = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "9";
    var isIE10N = navigator.userAgent.match(/MSIE (\d+)/) != null && RegExp.$1 == "10";
    var isiPad = navigator.userAgent.match(/iPad/i) != null;
    var isAndroidTab = /Android|webOS/i.test(navigator.userAgent);
    var space29 = " 							 ";
    var space30 = "                               ";
    var space31 = "                               ";
    var space32 = "                                ";
    var n = gblMYInboxCount.toString();
    var notificationCount = "";
    var messagesCount = "";
    if (kony.i18n.getCurrentLocale() == "th_TH") {
        if (n.length == 2) {
            var outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + space30 + gblMYInboxCount;
            if (isChromeN) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + " " + space32 + gblMYInboxCount;
            if (isIE8N) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + space32 + gblMYInboxCount;
            if (isIE9N || isIE10N) {
                //same as Firefox
                outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + space30 + " " + gblMYInboxCount;
            }
            if (isiPad) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + space32 + gblMYInboxCount;
            if (isAndroidTab) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + space32 + gblMYInboxCount;
        }
        if (n.length == 1) {
            var outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + space30 + gblMYInboxCount;
            if (isChromeN) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + "   " + space31 + gblMYInboxCount;
            if (isIE8N) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + " " + space32 + gblMYInboxCount;
            if (isIE9N || isIE10N) {
                //same as Firefox
            }
            if (isiPad) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + space32 + gblMYInboxCount;
            if (isAndroidTab) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + space32 + gblMYInboxCount;
        }
    } else {
        if (n.length == 2) {
            var outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + space30 + gblMYInboxCount;
            if (isChromeN) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + space31 + gblMYInboxCount;
            if (isIE8N) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + space32 + gblMYInboxCount;
            if (isIE9N || isIE10N) {
                //same as Firefox
                outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + "                              " + gblMYInboxCount;
            }
            if (isiPad) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + space32 + gblMYInboxCount;
            if (isAndroidTab) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + space30 + gblMYInboxCount;
        }
        if (n.length == 1) {
            var outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + space30 + gblMYInboxCount;
            if (isChromeN) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + space32 + gblMYInboxCount;
            if (isIE8N) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + " " + space32 + gblMYInboxCount;
            if (isIE9N || isIE10N) {
                //same as Firefox
            }
            if (isiPad) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + " " + space32 + gblMYInboxCount;
            if (isAndroidTab) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + space31 + gblMYInboxCount;
        }
    }
    if (kony.i18n.getCurrentLocale() == "en_US") {
        ntfSpace = "                   ";
        msgSpace = "                       ";
    } else if (kony.i18n.getCurrentLocale() == "th_TH") {
        ntfSpace = "                    ";
        msgSpace = "                           ";
    }
    notificationCount = kony.i18n.getLocalizedString("keylblNotification") + ntfSpace;
    if (parseInt(gblMYInboxCount) > 0) {
        notificationCount += "<font style='background-color:#CE0221; border-radius:3px' size='3'> " + gblMYInboxCount + " </font>";
    }
    messagesCount = kony.i18n.getLocalizedString("keylblmessage") + msgSpace;
    if (parseInt(gblMyInboxMSGTotalCount) > 0) {
        messagesCount += "<font style='background-color:#CE0221; border-radius:3px' size='3'> " + gblMyInboxMSGTotalCount + " </font>";
    }
    curr_form.segMenuOptions.setData([{
        lblSegData: {
            "skin": curr_form.id == "frmIBNotificationHome" ? "lblIBsegMenuFocus" : "lblIBSegMenu",
            "text": notificationCount
        }
    }, {
        lblSegData: {
            "skin": curr_form.id == "frmIBInboxHome" ? "lblIBsegMenuFocus" : "lblIBSegMenu",
            "text": messagesCount
        }
    }]);
    var isEmptyTotal = parseInt(gblMyInboxTotalCount) == 0 ? true : false;
    if (curr_form.id == "frmIBNotificationHome") {
        if (kony.i18n.getCurrentLocale() != "th_TH") {
            if (isEmptyTotal) {
                curr_form.btnMenuMyInbox.skin = "btnIBMenuMyInboxFocusArrowZero";
            } else {
                curr_form.btnMenuMyInbox.skin = "btnIBMenuMyInboxFocusArrow";
            }
            curr_form.btnMenuAboutMe.skin = "btnIBMenuAboutMe";
            curr_form.btnMenuConvenientServices.skin = "btnIBMenuConvenientServices";
            if (!isIE8N) {
                if (isEmptyTotal) {
                    curr_form.btnMenuMyInbox.hoverSkin = "btnIBMenuMyInboxFocusArrowZero";
                } else {
                    curr_form.btnMenuMyInbox.hoverSkin = "btnIBMenuMyInboxFocusArrow";
                }
                curr_form.btnMenuAboutMe.hoverSkin = "btnIBMenuAboutMeHover";
                curr_form.btnMenuConvenientServices.hoverSkin = "btnIBMenuConvenientServicesHover";
            }
        } else {
            if (isEmptyTotal) {
                curr_form.btnMenuMyInbox.skin = "btnIBMenuMyInboxFocusThaiArrowZero";
            } else {
                curr_form.btnMenuMyInbox.skin = "btnIBMenuMyInboxFocusThaiArrow";
            }
            curr_form.btnMenuAboutMe.skin = "btnIBMenuAboutMeThai";
            curr_form.btnMenuConvenientServices.skin = "btnIBMenuConvenientServicesThai";
            if (!isIE8N) {
                if (isEmptyTotal) {
                    curr_form.btnMenuMyInbox.hoverSkin = "btnIBMenuMyInboxFocusThaiArrowZero";
                } else {
                    curr_form.btnMenuMyInbox.hoverSkin = "btnIBMenuMyInboxFocusThaiArrow";
                }
                curr_form.btnMenuAboutMe.hoverSkin = "btnIBMenuAboutMeHoverThai";
                curr_form.btnMenuConvenientServices.hoverSkin = "btnIBMenuConvenientServicesHoverThai";
            }
        }
    } else if (curr_form.id == "frmIBInboxHome") {
        if (kony.i18n.getCurrentLocale() != "th_TH") {
            if (isEmptyTotal) {
                curr_form.btnMenuMyInbox.skin = "btnIBMenuMyInboxFocusZero";
            } else {
                curr_form.btnMenuMyInbox.skin = "btnIBMenuMyInboxFocus";
            }
            curr_form.btnMenuAboutMe.skin = "btnIBMenuAboutMe";
            curr_form.btnMenuConvenientServices.skin = "btnIBMenuConvenientServices";
            if (!isIE8N) {
                if (isEmptyTotal) {
                    curr_form.btnMenuMyInbox.hoverSkin = "btnIBMenuMyInboxFocusZero";
                } else {
                    curr_form.btnMenuMyInbox.hoverSkin = "btnIBMenuMyInboxFocus";
                }
                curr_form.btnMenuAboutMe.hoverSkin = "btnIBMenuAboutMeHover";
                curr_form.btnMenuConvenientServices.hoverSkin = "btnIBMenuConvenientServicesHover";
            }
        } else {
            if (isEmptyTotal) {
                curr_form.btnMenuMyInbox.skin = "btnIBMenuMyInboxFocusThaiZero";
            } else {
                curr_form.btnMenuMyInbox.skin = "btnIBMenuMyInboxFocusThai";
            }
            curr_form.btnMenuAboutMe.skin = "btnIBMenuAboutMeThai";
            curr_form.btnMenuConvenientServices.skin = "btnIBMenuConvenientServicesThai";
            if (!isIE8N) {
                if (isEmptyTotal) {
                    curr_form.btnMenuMyInbox.hoverSkin = "btnIBMenuMyInboxFocusThaiZero";
                } else {
                    curr_form.btnMenuMyInbox.hoverSkin = "btnIBMenuMyInboxFocusThai";
                }
                curr_form.btnMenuAboutMe.hoverSkin = "btnIBMenuAboutMeHoverThai";
                curr_form.btnMenuConvenientServices.hoverSkin = "btnIBMenuConvenientServicesHoverThai";
            }
        }
    }
}

function menuSegmentEnabling() {
    curr_form = kony.application.getCurrentForm();
    var isEmptyTotal = parseInt(gblMyInboxTotalCount) == 0 ? true : false;
    //below code show Applyservices to show mobile/reset/add device
    var addDevice = false;
    var resetPwd = false;
    var activatedCust = false;
    var removeIndx = 0;
    if (GLOBAL_MB_APPLY_CODES == undefined) GLOBAL_MB_APPLY_CODES = ""
    if (gblMBFlowStatus == undefined) gblMBFlowStatus = ""
    if (GLOBAL_MB_APPLY_CODES.indexOf(gblMBFlowStatus) >= 0) {
        activatedCust = false;
    }
    if (gblMBFlowStatus == "02") {
        activatedCust = true;
        addDevice = true;
    }
    if (gblMBFlowStatus == "02" || gblMBFlowStatus == "03" || gblMBFlowStatus == "05" || gblMBFlowStatus == "06") {
        resetPwd = true;
        activatedCust = true;
    }
    if (gblMenuSelection == 1) {
        var selectedSubMenuIndex = curr_form.segMenuOptions.selectedIndex[1];
        if (selectedSubMenuIndex == 0) {
            if (curr_form.id != "frmIBPostLoginDashboard") menuReset();
            TMBUtil.DestroyForm(frmIBCMChgMobNoTxnLimit);
            TMBUtil.DestroyForm(frmIBCMEditMyProfile);
            gblEditMobileFromAnyId = false; // to fix MIB-2132
            IBtoCallViewProfileonSave(); //Invoke party inquiry service
            //frmIBCMMyProfile.show(); 
            //frmIBCMMyProfile.segMenuOptions.setVisibility(true);
        }
        if (selectedSubMenuIndex == 1) {
            if (curr_form.id != "frmIBPostLoginDashboard") menuReset();
            frmIBMyAccnts.hbxTMBImg.setVisibility(true);
            frmIBMyAccnts.hboxAddNewAccnt.setVisibility(false);
            //frmIBMyAccnts.imgArrow.setVisibility(false);
            frmIBMyAccnts.hbxViewAccnt.setVisibility(false);
            //frmIBMyAccnts.arrwImgSeg1.setVisibility(false);
            //frmIBMyAccnts.arrwImgSeg2.setVisibility(false);
            frmIBMyAccnts.cmbobxAccntType.setVisibility(false);
            frmIBMyAccnts.lineComboAccntype.setVisibility(false);
            frmIBMyAccnts.hbxEditAccntNN.setVisibility(false);
            if (curr_form.id != "frmIBMyAccnts") {
                showLoadingScreenPopup();
                frmIBMyAccnts.show();
            } else frmIBMyAccnts.postShow();
            //segAboutMeLoad();
            //frmIBMyAccnts.segMenuOptions.setVisibility(true);
        }
        if (selectedSubMenuIndex == 2) {
            recipientAddFromTransfer = false; //enh_069
            gblTransferFromRecipient = false; //enh_211
            if (curr_form.id != "frmIBPostLoginDashboard") menuReset();
            if (curr_form.id != "frmIBMyReceipentsHome") {
                showLoadingScreenPopup();
                frmIBMyReceipentsHome.show();
            } else frmIBMyReceipentsHome.postShow();
            //frmIBMyReceipentsHome.segMenuOptions.setVisibility(true);
        }
        if (selectedSubMenuIndex == 3) {
            if (curr_form.id != "frmIBPostLoginDashboard") menuReset();
            gblBillerFromMenu = true;
            gblSuggestedBiller = false;
            frmIBMyBillersHome.segBillersConfirm.removeAll();
            if (curr_form.id == "frmIBMyBillersHome") {
                frmIBMyBillersHome_frmIBMyBillersHome_preshow_seq0();
                frmIBMyBillersHome_frmIBMyBillersHome_postshow_seq0();
            }
            getMyBillSuggestListIB();
            //frmIBMyBillersHome.show();
            //frmIBMyReceipentsHome.segMenuOptions.setVisibility(true);
        }
        if (selectedSubMenuIndex == 4) {
            if (curr_form.id != "frmIBPostLoginDashboard") menuReset();
            //frmIBMyTopUpsHome.show();
            gblTopupFromMenu = true;
            frmIBMyTopUpsHome.segBillersConfirm.removeAll();
            if (curr_form.id == "frmIBMyTopUpsHome") {
                frmIBMyTopUpsHome_frmIBMyTopUpsHome_postshow_seq0()
                frmIBMyTopUpsHome_frmIBMyTopUpsHome_preshow_seq0()
            }
            getMyTopUpSuggestListIB();
        }
        if (curr_form.segMenuOptions.data[selectedSubMenuIndex].lblSegData.text == kony.i18n.getLocalizedString("ActivateTokenIB")) {
            // Reset MB Password flow
            if (curr_form.id != "frmIBPostLoginDashboard") menuReset();
            //frmApplyMBviaIBStep1.show();
            frmIBTokenActivationPage.tbxTokenSerialNo.text = "";
            frmIBTokenActivationPage.tbxTokenOTP.text = "";
            frmIBTokenActivationPage.tbxTokenOTP.setFocus(true);
            frmIBTokenActivationPage.hbxConfirm.isVisible = false;
            frmIBTokenActivationPage.hbxCompleteConfirm.isVisible = false;
            if (curr_form.id != "frmIBTokenActivationPage") {
                showLoadingScreenPopup();
                frmIBTokenActivationPage.show();
            } else frmIBTokenActivationPage.postShow();
        }
    }
    if (gblMenuSelection == 2) {
        var selectedSubMenuText = curr_form.segMenuOptions.data[curr_form.segMenuOptions.selectedIndex[1]].lblSegData.text;
        if (selectedSubMenuText == kony.i18n.getLocalizedString("MIB_AnyIDMenu")) {
            curr_form = kony.application.getCurrentForm().id;
            if (curr_form.id != "frmIBPostLoginDashboard") menuReset();
            navigatetoAnyIDBrief();
        }
        if (selectedSubMenuText == kony.i18n.getLocalizedString("kelblOpenNewAccount")) {
            if (getCRMLockStatus()) {
                curr_form = kony.application.getCurrentForm().id;
                popIBBPOTPLocked.show();
            } else {
                isCmpFlow = false;
                if (curr_form.id != "frmIBPostLoginDashboard") menuReset();
                checkopenActCustStatus();
            }
        }
        if (selectedSubMenuText == kony.i18n.getLocalizedString("lblChequeServiceIB")) {
            if (getCRMLockStatus()) {
                curr_form = kony.application.getCurrentForm().id;
                popIBBPOTPLocked.show();
            } else {
                callcustomerAccountChequeService();
            }
        }
        if (selectedSubMenuText == kony.i18n.getLocalizedString("keyEStmt")) {
            if (curr_form.id != "frmIBPostLoginDashboard") menuReset();
            if (curr_form.id != "frmIBEStatementProdFeature") {
                showLoadingScreenPopup();
                checkCustStatuseStmt();
                //frmIBEStatementProdFeature.show();
            } else frmIBEStatementProdFeature.postShow();
            //frmIBMyReceipentsHome.segMenuOptions.setVisibility(true);
        }
        if (selectedSubMenuText == kony.i18n.getLocalizedString("keyMBankinglblMobileBanking")) {
            if (curr_form.id != "frmIBPostLoginDashboard") menuReset();
            frmIBGetTMBTouch.show();
        }
    }
    if (gblMenuSelection == 3) {
        if (curr_form.segMenuOptions.selectedIndex[1] == 0) {
            if (curr_form.id != "frmIBPostLoginDashboard") menuReset();
            SortByTypeIN = "000";
            SortByOrderIN = "000";
            var isChromeN = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
            var isIE8N = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
            var isIE9N = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "9";
            var isIE10N = navigator.userAgent.match(/MSIE (\d+)/) != null && RegExp.$1 == "10";
            var isiPad = navigator.userAgent.match(/iPad/i) != null;
            var isAndroidTab = /Android|webOS/i.test(navigator.userAgent);
            var space30 = "                               ";
            var space31 = "                               ";
            var space32 = "                           ";
            var n = gblMYInboxCount.toString();
            if (kony.i18n.getCurrentLocale() == "th_TH") {
                if (n.length == 2) {
                    var outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + space30 + gblMYInboxCount;
                    if (isChromeN) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + "      " + space32 + gblMYInboxCount;
                    if (isIE8N) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + "     " + space32 + gblMYInboxCount;
                    if (isIE9N || isIE10N) {
                        //same as Firefox
                        outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + space31 + " " + +gblMYInboxCount;
                    }
                    if (isiPad) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + space32 + gblMYInboxCount;
                    if (isAndroidTab) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + "    " + space32 + gblMYInboxCount;
                }
                if (n.length == 1) {
                    var outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + space30 + gblMYInboxCount;
                    if (isChromeN) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + "   " + space31 + gblMYInboxCount;
                    if (isIE8N) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + "    " + space32 + gblMYInboxCount;
                    if (isIE9N || isIE10N) {
                        //same as Firefox
                    }
                    if (isiPad) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + space32 + gblMYInboxCount;
                    if (isAndroidTab) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + "     " + space32 + gblMYInboxCount;
                }
            } else {
                if (n.length == 2) {
                    var outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + space30 + gblMYInboxCount;
                    if (isChromeN) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + space30 + gblMYInboxCount;
                    if (isIE8N) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + "     " + space32 + gblMYInboxCount;
                    if (isIE9N || isIE10N) {
                        //same as Firefox
                        outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + "                              " + gblMYInboxCount;
                    }
                    if (isiPad) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + space32 + gblMYInboxCount;
                    if (isAndroidTab) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + "                              " + gblMYInboxCount;
                }
                if (n.length == 1) {
                    var outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + space30 + gblMYInboxCount;
                    if (isChromeN) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + " " + space31 + gblMYInboxCount;
                    if (isIE8N) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + "    " + space32 + gblMYInboxCount;
                    if (isIE9N || isIE10N) {
                        //same as Firefox
                    }
                    if (isiPad) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + " " + space32 + gblMYInboxCount;
                    if (isAndroidTab) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + space32 + "    " + gblMYInboxCount;
                }
            }
            if (curr_form.id != "frmIBNotificationHome") {
                showLoadingScreenPopup();
                frmIBNotificationHome.show();
            } else {
                //form post show call is not working in IE8
                //frmIBNotificationHome.postShow();
                frmIBNotificationHome_frmIBNotificationHome_postShow();
            }
            frmIBNotificationHome.label474082218268567.text = kony.i18n.getLocalizedString("keylblNotification");
            frmIBNotificationHome.lblNoNotfn.text = kony.i18n.getLocalizedString("keyNoNotifications");
            frmIBNotificationHome.label476822990280082.text = kony.i18n.getLocalizedString("keylblNotificationDetails");
            gblDetailPageStateChanger = "start";
            var menuNotificationCount = kony.i18n.getLocalizedString("keylblNotification") + ntfSpace;
            if (parseInt(gblMYInboxCount) > 0) {
                menuNotificationCount += "<font style='background-color:#CE0221; border-radius:3px' size='3'> " + gblMYInboxCount + " </font>";
            }
            var menuMessagesCount = kony.i18n.getLocalizedString("keylblmessage") + msgSpace;
            if (parseInt(gblMyInboxMSGTotalCount) > 0) {
                menuMessagesCount += "<font style='background-color:#CE0221; border-radius:3px' size='3'> " + gblMyInboxMSGTotalCount + " </font>";
            }
            frmIBNotificationHome.segMenuOptions.setData([{
                lblSegData: {
                    "skin": "lblIBsegMenuFocus",
                    "text": menuNotificationCount
                }
            }, {
                lblSegData: {
                    "skin": "lblIBSegMenu",
                    "text": menuMessagesCount
                }
            }]);
            var isIE8 = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
            if (kony.i18n.getCurrentLocale() != "th_TH") {
                if (isEmptyTotal) {
                    frmIBNotificationHome.btnMenuMyInbox.skin = "btnIBMenuMyInboxFocusArrowZero";
                } else {
                    frmIBNotificationHome.btnMenuMyInbox.skin = "btnIBMenuMyInboxFocusArrow";
                }
                if (!isIE8) {
                    if (isEmptyTotal) {
                        frmIBNotificationHome.btnMenuMyInbox.hoverSkin = "btnIBMenuMyInboxFocusArrowZero";
                    } else {
                        frmIBNotificationHome.btnMenuMyInbox.hoverSkin = "btnIBMenuMyInboxFocusArrow";
                    }
                }
            } else {
                if (isEmptyTotal) {
                    frmIBNotificationHome.btnMenuMyInbox.skin = "btnIBMenuMyInboxFocusThaiArrowZero";
                } else {
                    frmIBNotificationHome.btnMenuMyInbox.skin = "btnIBMenuMyInboxFocusThaiArrow";
                }
                if (!isIE8) {
                    if (isEmptyTotal) {
                        frmIBNotificationHome.btnMenuMyInbox.hoverSkin = "btnIBMenuMyInboxFocusThaiArrowZero";
                    } else {
                        frmIBNotificationHome.btnMenuMyInbox.hoverSkin = "btnIBMenuMyInboxFocusThaiArrow";
                    }
                }
            }
        }
        if (curr_form.segMenuOptions.selectedIndex[1] == 1) {
            if (curr_form.id != "frmIBPostLoginDashboard") menuReset();
            SortByTypeIN = "000";
            SortByOrderIN = "000";
            var isChromeN = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
            var isIE8N = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
            var isIE9N = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "9";
            var isIE10N = navigator.userAgent.match(/MSIE (\d+)/) != null && RegExp.$1 == "10";
            var isiPad = navigator.userAgent.match(/iPad/i) != null;
            var isAndroidTab = /Android|webOS/i.test(navigator.userAgent);
            var space30 = "                               ";
            var space31 = "                               ";
            var space32 = "                           ";
            var n = gblMYInboxCount.toString();
            if (kony.i18n.getCurrentLocale() == "th_TH") {
                if (n.length == 2) {
                    var outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + space30 + gblMYInboxCount;
                    if (isChromeN) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + "                                 " + gblMYInboxCount;
                    if (isIE8N) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + "     " + space32 + gblMYInboxCount;
                    if (isIE9N || isIE10N) {
                        //same as Firefox
                        outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + space32 + "     " + gblMYInboxCount;
                    }
                    if (isiPad) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + space32 + gblMYInboxCount;
                    if (isAndroidTab) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + "    " + space32 + gblMYInboxCount;
                }
                if (n.length == 1) {
                    var outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + space30 + gblMYInboxCount;
                    if (isChromeN) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + "   " + space31 + gblMYInboxCount;
                    if (isIE8N) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + "    " + space32 + gblMYInboxCount;
                    if (isIE9N || isIE10N) {
                        //same as Firefox
                    }
                    if (isiPad) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + space32 + gblMYInboxCount;
                    if (isAndroidTab) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + space32 + "      " + gblMYInboxCount;
                }
            } else {
                if (n.length == 2) {
                    var outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + space30 + gblMYInboxCount;
                    if (isChromeN) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + space31 + gblMYInboxCount;
                    if (isIE8N) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + "     " + space32 + gblMYInboxCount;
                    if (isIE9N || isIE10N) {
                        //same as Firefox
                        outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + "                              " + gblMYInboxCount;
                    }
                    if (isiPad) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + space32 + gblMYInboxCount;
                    if (isAndroidTab) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + "                              " + gblMYInboxCount;
                }
                if (n.length == 1) {
                    var outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + space30 + gblMYInboxCount;
                    if (isChromeN) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + space32 + gblMYInboxCount;
                    if (isIE8N) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + "    " + space32 + gblMYInboxCount;
                    if (isIE9N || isIE10N) {
                        //same as Firefox
                    }
                    if (isiPad) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + space32 + gblMYInboxCount;
                    if (isAndroidTab) outstandingUnread = kony.i18n.getLocalizedString("keylblNotification") + space32 + "    " + gblMYInboxCount;
                }
            }
            if (curr_form.id != "frmIBInboxHome") {
                showLoadingScreenPopup();
                frmIBInboxHome.show();
            } else {
                //form postShow method is not working in IE8.
                //frmIBInboxHome.postShow();
                frmIBInboxHome_frmIBInboxHome_postShow();
            }
            frmIBInboxHome.label474082218268567.text = kony.i18n.getLocalizedString("keylblmessage");
            frmIBInboxHome.lblSorBy.text = kony.i18n.getLocalizedString("keyCalendarSortBy");
            frmIBInboxHome.lblNoNotfn.text = kony.i18n.getLocalizedString("keyNoNotifications");
            frmIBInboxHome.label476822990280082.text = kony.i18n.getLocalizedString("keyMessageDatails");
            gblDetailPageStateChangerIN = "start";
            var menuNotificationCount = kony.i18n.getLocalizedString("keylblNotification") + ntfSpace;
            if (parseInt(gblMYInboxCount) > 0) {
                menuNotificationCount += "<font style='background-color:#CE0221; border-radius:3px' size='3'> " + gblMYInboxCount + " </font>";
            }
            var menuMessagesCount = kony.i18n.getLocalizedString("keylblmessage") + msgSpace;
            if (parseInt(gblMyInboxMSGTotalCount) > 0) {
                menuMessagesCount += "<font style='background-color:#CE0221; border-radius:3px' size='3'> " + gblMyInboxMSGTotalCount + " </font>";
            }
            frmIBInboxHome.segMenuOptions.setData([{
                lblSegData: {
                    "skin": "lblIBSegMenu",
                    "text": menuNotificationCount
                }
            }, {
                lblSegData: {
                    "skin": "lblIBsegMenuFocus",
                    "text": menuMessagesCount
                }
            }]);
            if (kony.i18n.getCurrentLocale() != "th_TH") {
                if (curr_form.id == "frmIBNotificationHome") {
                    if (isEmptyTotal) {
                        curr_form.btnMenuMyInbox.skin = "btnIBMenuMyInboxFocusArrowZero";
                    } else {
                        curr_form.btnMenuMyInbox.skin = "btnIBMenuMyInboxFocusArrow";
                    }
                } else if (curr_form.id == "frmIBInboxHome") {
                    if (isEmptyTotal) {
                        curr_form.btnMenuMyInbox.skin = "btnIBMenuMyInboxFocusZero";
                    } else {
                        curr_form.btnMenuMyInbox.skin = "btnIBMenuMyInboxFocus";
                    }
                }
            } else {
                if (curr_form.id == "frmIBNotificationHome") {
                    if (isEmptyTotal) {
                        curr_form.btnMenuMyInbox.skin = "btnIBMenuMyInboxFocusThaiArrowZero";
                    } else {
                        curr_form.btnMenuMyInbox.skin = "btnIBMenuMyInboxFocusThaiArrow";
                    }
                } else if (curr_form.id == "frmIBInboxHome") {
                    if (isEmptyTotal) {
                        curr_form.btnMenuMyInbox.skin = "btnIBMenuMyInboxFocusThaiZero";
                    } else {
                        curr_form.btnMenuMyInbox.skin = "btnIBMenuMyInboxFocusThai";
                    }
                }
            }
        }
    }
}

function transferMenuClick() {
    if (getCRMLockStatus()) {
        curr_form = kony.application.getCurrentForm().id;
        popIBBPOTPLocked.show();
    } else {
        //TMBUtil.DestroyForm(frmIBTranferLP);
        frmIBTranferLP.info = {};
        gblPaynow = true;
        Recp_category = 0;
        recipientAddFromTransfer = false;
        gblTransferFromRecipient = false;
        setTabBankAccountOrMobile(true);
        gblSelTransferMode = 1; //default account transfer
        getFromXferAccountsIB();
    }
}

function indexFinderinSegment(form_element, menu_text) {
    for (var i = 0; i < form_element.segMenuOptions.data.length; i++) {
        if (form_element.segMenuOptions.data[i].lblSegData.text == menu_text) return i;
    }
    return -1;
}
/*
 * Given an array of formnames and a specific formname, it says whether it belongs to that category or not
 * Function returns boolean
 */
function formBelongs(formlist, f_name) {
    for (var i = 0; i < formlist.length; i++) {
        if (f_name == formlist[i]) return true;
    }
    return false;
}

function pagespecificSubMenu() {
    //sets the current form names and current locale in local variables to be used
    var form_name = kony.application.getCurrentForm();
    var locale = kony.i18n.getCurrentLocale();
    var isIE8 = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
    //All IB forms accesible from sub-menu categorised and put into single array
    var my_profile_forms = new Array("frmIBCMChgMobNoTxnLimit", "frmIBCMChgPwd", "frmIBCMChngUserID", "frmIBCMConfirmation", "frmIBCMConfirmationPwd", "frmIBCMChgTxnLimit", "frmIBCMConfirmEdit", "frmIBCMEditMyProfile", "frmIBCMFreqUsedAcnt", "frmIBCMMyProfile");
    var my_accounts_forms = new Array("frmIBMyAccnts", "frmIBAddMyAccnt", "frmIBDreamSavingMaintenance", "frmIBDreamSAvingEdit", "frmIBEditMyAccounts", "frmIBEditMyAccountsConfirmComplete");
    var cheque_service_forms = new Array("frmIBChequeServiceConfirmation", "frmIBChequeServiceReturnedChequeLanding", "frmIBChequeServiceStopChequeAck", "frmIBChequeServiceStopChequeLanding", "frmIBChequeServiceViewReturnedCheque");
    var open_new_account_forms = new Array("frmIBOpenActSelProd", "frmIBOpenNewDreamAcc", "frmIBOpenNewDreamAccComplete", "frmIBOpenNewDreamAccConfirmation", "frmIBOpenNewSavingsAcc", "frmIBOpenNewSavingsAccComplete", "frmIBOpenNewSavingsAccConfirmation", "frmIBOpenNewSavingsCareAcc", "frmIBOpenNewSavingsCareAccComplete", "frmIBOpenNewSavingsCareAccConfirmation", "frmIBOpenNewTermDepositAcc", "frmIBOpenNewTermDepositAccComplete", "frmIBOpenNewTermDepositAccConfirmation", "frmIBOpenProdDetnTnC", "frmIBOpenProdMailAddress", "frmIBOpenProdMailAddressEdit", "frmIBOpenAccountContactKYC", "frmIBOpenAccountOccupationKYC");
    var beep_and_bill_payment_forms = new Array("frmIBBeepAndBillTandC", "frmIBBeepAndBillApply", "frmIBBeepAndBillApplyCW", "frmIBBeepAndBillConfAndComp", "frmIBBeepAndBillExecuteConfComp", "frmIBBeepAndBillExecutedTxn", "frmIBBeepAndBillList", "frmIBBBTnC");
    var my_bills_forms = new Array("frmIBMyBillersHome");
    var my_top_ups_forms = new Array("frmIBMyTopUpsHome");
    var my_recipient_forms = new Array("frmIBMyReceipentsAccounts", "frmIBMyReceipentsAddBankAccnt", "frmIBMyReceipentsAddContactFB", "frmIBMyReceipentsAddContactManually", "frmIBMyReceipentsAddContactManuallyConf", "frmIBMyReceipentsEditAccountConf", "frmIBMyReceipentsFBContactConf", "frmIBMyReceipentsHome");
    var send_to_save_forms = new Array("ApplyS2SIB.js", "frmIBSSApply", "frmIBSSApplyCmplete", "frmIBSSApplyCnfrmtn", "frmIBSSSDetails", "frmIBSSSExcuteTrnsfr", "frmIBSSSExcuteTrnsfrCmplete", "frmIBSSSTnC");
    var activating_token_forms = new Array("frmIBTokenActivationPage");
    var mobile_all_forms = new Array("frmApplyMBviaIBStep1", "frmApplyMbviaIBConf", "frmApplyMBviaIBConfirmation", "frmIBGetTMBTouch");
    var my_activities_forms = new Array("frmIBBillPaymentView", "frmIBEditFutureBillPaymentPrecnf", "frmIBTopUpViewNEdit", "frmIBEditFutureTopUpPrecnf", "frmIBFTrnsrView", "frmIBFTrnsrEditCnfmtn");
    var eStatement_forms = new Array("frmIBEStatementProdFeature", "frmIBEStatementTnC", "frmIBEStatementLanding", "frmIBEStatementConfirm", "frmIBEStatementComplete");
    var anyID_forms = new Array("frmIBAnyIDRegistration", "frmIBAnyIDProdFeature", "frmIBAnyIDRegistrationComplete");
    //If current form belongs to my profile
    if (formBelongs(my_profile_forms, form_name.id)) {
        gblMenuSelection = 1;
        if (locale == "th_TH") {
            form_name.btnMenuAboutMe.skin = btnIBMenuAboutMeFocusThaiProfile;
            if (!isIE8) form_name.btnMenuAboutMe.hoverSkin = btnIBMenuAboutMeFocusThaiProfile;
        } else {
            form_name.btnMenuAboutMe.skin = btnIBMenuAboutMeFocusProfile;
            if (!isIE8) form_name.btnMenuAboutMe.hoverSkin = btnIBMenuAboutMeFocusProfile;
        }
        var key = kony.i18n.getLocalizedString("keyMyProfile");
        var data = {
            "lblSegData": {
                "skin": "lblIBsegMenuFocus",
                "text": key
            }
        }
        form_name.segMenuOptions.setDataAt(data, 0);
    }
    //If current form belongs to my accounts
    if (formBelongs(my_accounts_forms, form_name.id)) {
        gblMenuSelection = 1;
        if (locale == "th_TH") {
            form_name.btnMenuAboutMe.skin = btnIBMenuAboutMeFocusThai;
            if (!isIE8) form_name.btnMenuAboutMe.hoverSkin = btnIBMenuAboutMeFocusThai;
        } else {
            form_name.btnMenuAboutMe.skin = btnIBMenuAboutMeFocus;
            if (!isIE8) form_name.btnMenuAboutMe.hoverSkin = btnIBMenuAboutMeFocus;
        }
        var data = {
            "lblSegData": {
                "skin": "lblIBsegMenuFocus",
                "text": kony.i18n.getLocalizedString("keyMyAccountsIB")
            }
        }
        form_name.segMenuOptions.setDataAt(data, 1)
    }
    //If current form belongs to cheque service
    if (formBelongs(cheque_service_forms, form_name.id)) {
        gblMenuSelection = 2;
        if (locale == "th_TH") {
            form_name.btnMenuConvenientServices.skin = btnIBMenuConvenientServicesFocusThai;
            if (!isIE8) form_name.btnMenuConvenientServices.hoverSkin = btnIBMenuConvenientServicesFocusThai;
        } else {
            form_name.btnMenuConvenientServices.skin = btnIBMenuConvenientServicesFocus;
            if (!isIE8) form_name.btnMenuConvenientServices.hoverSkin = btnIBMenuConvenientServicesFocus;
        }
        var data = {
            "lblSegData": {
                "skin": "lblIBsegMenuFocus",
                "text": kony.i18n.getLocalizedString("lblChequeServiceIB")
            }
        }
        if (gblShowAnyIDRegistration == "false") {
            form_name.segMenuOptions.setDataAt(data, 1)
        } else {
            form_name.segMenuOptions.setDataAt(data, 2)
        }
    }
    //If current form belongs to open new account
    if (formBelongs(open_new_account_forms, form_name.id)) {
        gblMenuSelection = 2;
        if (locale == "th_TH") {
            form_name.btnMenuConvenientServices.skin = btnIBMenuConvenientServicesFocusThai;
            if (!isIE8) form_name.btnMenuConvenientServices.hoverSkin = btnIBMenuConvenientServicesFocusThai;
        } else {
            form_name.btnMenuConvenientServices.skin = btnIBMenuConvenientServicesFocus;
            if (!isIE8) form_name.btnMenuConvenientServices.hoverSkin = btnIBMenuConvenientServicesFocus;
        }
        var data = {
            "lblSegData": {
                "skin": "lblIBsegMenuFocus",
                "text": kony.i18n.getLocalizedString("kelblOpenNewAccount")
            }
        }
        if (gblShowAnyIDRegistration == "false") {
            form_name.segMenuOptions.setDataAt(data, 0)
        } else {
            form_name.segMenuOptions.setDataAt(data, 1)
        }
    }
    //If current form belongs to beep and bill payment
    if (formBelongs(beep_and_bill_payment_forms, form_name.id)) {
        gblMenuSelection = 2;
        if (locale == "th_TH") {
            form_name.btnMenuConvenientServices.skin = btnIBMenuConvenientServicesFocusThai;
            if (!isIE8) form_name.btnMenuConvenientServices.hoverSkin = btnIBMenuConvenientServicesFocusThai;
        } else {
            form_name.btnMenuConvenientServices.skin = btnIBMenuConvenientServicesFocus;
            if (!isIE8) form_name.btnMenuConvenientServices.hoverSkin = btnIBMenuConvenientServicesFocus;
        }
        var data = {
            "lblSegData": {
                "skin": "lblIBsegMenuFocus",
                "text": kony.i18n.getLocalizedString("keylblbeep&Bill")
            }
        }
        form_name.segMenuOptions.setDataAt(data, 1)
    }
    //If current form belongs to my bills
    if (formBelongs(my_bills_forms, form_name.id)) {
        gblMenuSelection = 1;
        if (locale == "th_TH") {
            form_name.btnMenuAboutMe.skin = btnIBMenuAboutMeFocusThai;
            if (!isIE8) form_name.btnMenuAboutMe.hoverSkin = btnIBMenuAboutMeFocusThai;
        } else {
            form_name.btnMenuAboutMe.skin = btnIBMenuAboutMeFocus;
            if (!isIE8) form_name.btnMenuAboutMe.hoverSkin = btnIBMenuAboutMeFocus;
        }
        var data = {
            "lblSegData": {
                "skin": "lblIBsegMenuFocus",
                "text": kony.i18n.getLocalizedString("keyMyBills")
            }
        }
        form_name.segMenuOptions.setDataAt(data, 3)
    }
    //If current form belongs to my top up
    if (formBelongs(my_top_ups_forms, form_name.id)) {
        gblMenuSelection = 1;
        if (locale == "th_TH") {
            form_name.btnMenuAboutMe.skin = btnIBMenuAboutMeFocusThai;
            if (!isIE8) form_name.btnMenuAboutMe.hoverSkin = btnIBMenuAboutMeFocusThai;
        } else {
            form_name.btnMenuAboutMe.skin = btnIBMenuAboutMeFocus;
            if (!isIE8) form_name.btnMenuAboutMe.hoverSkin = btnIBMenuAboutMeFocus;
        }
        var data = {
            "lblSegData": {
                "skin": "lblIBsegMenuFocus",
                "text": kony.i18n.getLocalizedString("myTopUpsMB")
            }
        }
        form_name.segMenuOptions.setDataAt(data, 4)
    }
    //If current form belongs to my recipients
    if (formBelongs(my_recipient_forms, form_name.id)) {
        gblMenuSelection = 1;
        if (locale == "th_TH") {
            form_name.btnMenuAboutMe.skin = btnIBMenuAboutMeFocusThai;
            if (!isIE8) form_name.btnMenuAboutMe.hoverSkin = btnIBMenuAboutMeFocusThai;
        } else {
            form_name.btnMenuAboutMe.skin = btnIBMenuAboutMeFocus;
            if (!isIE8) form_name.btnMenuAboutMe.hoverSkin = btnIBMenuAboutMeFocus;
        }
        var data = {
            "lblSegData": {
                "skin": "lblIBsegMenuFocus",
                "text": kony.i18n.getLocalizedString("keyMyRecipients")
            }
        }
        form_name.segMenuOptions.setDataAt(data, 2)
    }
    //If current form belongs to E-Stateent
    if (formBelongs(eStatement_forms, form_name.id)) {
        gblMenuSelection = 2;
        if (locale == "th_TH") {
            form_name.btnMenuConvenientServices.skin = btnIBMenuConvenientServicesFocusThai;
            if (!isIE8) form_name.btnMenuConvenientServices.hoverSkin = btnIBMenuConvenientServicesFocusThai;
        } else {
            form_name.btnMenuConvenientServices.skin = btnIBMenuConvenientServicesFocus;
            if (!isIE8) form_name.btnMenuConvenientServices.hoverSkin = btnIBMenuConvenientServicesFocus;
        }
        var data = {
            "lblSegData": {
                "skin": "lblIBsegMenuFocus",
                "text": kony.i18n.getLocalizedString("keyEStmt")
            }
        }
        if (gblShowAnyIDRegistration == "false") {
            form_name.segMenuOptions.setDataAt(data, 2)
        } else {
            form_name.segMenuOptions.setDataAt(data, 3)
        }
    }
    //If current for belongs to Any ID Registration flow
    if (formBelongs(anyID_forms, form_name.id)) {
        gblMenuSelection = 2;
        if (locale == "th_TH") {
            form_name.btnMenuConvenientServices.skin = btnIBMenuConvenientServicesFocusThaiArrow;
            if (!isIE8) form_name.btnMenuConvenientServices.hoverSkin = btnIBMenuConvenientServicesFocusThaiArrow;
        } else {
            form_name.btnMenuConvenientServices.skin = btnIBMenuConvenientServicesFocusArrow;
            if (!isIE8) form_name.btnMenuConvenientServices.hoverSkin = btnIBMenuConvenientServicesFocusArrow;
        }
        var data = {
            "lblSegData": {
                "skin": "lblIBsegMenuFocus",
                "text": kony.i18n.getLocalizedString("MIB_AnyIDMenu")
            }
        }
        if (gblShowAnyIDRegistration == "true") {
            form_name.segMenuOptions.setDataAt(data, 0)
        }
    }
    //If current form belongs to send to save
    if (formBelongs(send_to_save_forms, form_name.id)) {
        gblMenuSelection = 2;
        if (locale == "th_TH") {
            form_name.btnMenuConvenientServices.skin = btnIBMenuConvenientServicesFocusThai;
            if (!isIE8) form_name.btnMenuConvenientServices.hoverSkin = btnIBMenuConvenientServicesFocusThai;
        } else {
            form_name.btnMenuConvenientServices.skin = btnIBMenuConvenientServicesFocus;
            if (!isIE8) form_name.btnMenuConvenientServices.hoverSkin = btnIBMenuConvenientServicesFocus;
        }
        var data = {
            "lblSegData": {
                "skin": "lblIBsegMenuFocus",
                "text": kony.i18n.getLocalizedString("keylblsendToSave")
            }
        }
        form_name.segMenuOptions.setDataAt(data, 2)
    }
    //If current form belongs to activating token
    if (formBelongs(activating_token_forms, form_name.id)) {
        gblMenuSelection = 1;
        if (locale == "th_TH") {
            form_name.btnMenuAboutMe.skin = btnIBMenuAboutMeFocusThai;
            if (!isIE8) form_name.btnMenuAboutMe.hoverSkin = btnIBMenuAboutMeFocusThai;
        } else {
            form_name.btnMenuAboutMe.skin = btnIBMenuAboutMeFocus;
            if (!isIE8) form_name.btnMenuAboutMe.hoverSkin = btnIBMenuAboutMeFocus;
        }
        var text_index = indexFinderinSegment(form_name, kony.i18n.getLocalizedString("ActivateTokenIB"));
        if (text_index != -1) {
            var data = {
                "lblSegData": {
                    "skin": "lblIBsegMenuFocus",
                    "text": kony.i18n.getLocalizedString("ActivateTokenIB")
                }
            }
            form_name.segMenuOptions.setDataAt(data, text_index);
        }
    }
    //If current form belongs to mobile related
    if (formBelongs(mobile_all_forms, form_name.id)) {
        gblMenuSelection = 2;
        if (locale == "th_TH") {
            form_name.btnMenuConvenientServices.skin = btnIBMenuConvenientServicesFocusThai;
            if (!isIE8) form_name.btnMenuConvenientServices.hoverSkin = btnIBMenuConvenientServicesFocusThai;
        } else {
            form_name.btnMenuConvenientServices.skin = btnIBMenuConvenientServicesFocus;
            if (!isIE8) form_name.btnMenuConvenientServices.hoverSkin = btnIBMenuConvenientServicesFocus;
        }
        var data = {
            "lblSegData": {
                "skin": "lblIBsegMenuFocus",
                "text": kony.i18n.getLocalizedString("keyMBankinglblMobileBanking")
            }
        }
        if (gblShowAnyIDRegistration == "false") {
            form_name.segMenuOptions.setDataAt(data, 3)
        } else {
            form_name.segMenuOptions.setDataAt(data, 4)
        }
    }
    //If current form belongs to my activities
    if (formBelongs(my_activities_forms, form_name.id)) {
        //gblMenuSelection = 1;
        if (locale == "th_TH") {
            form_name.btnMenuMyActivities.skin = btnIBMenuMyActivitiesFocusThai;
            if (!isIE8) form_name.btnMenuMyActivities.hoverSkin = btnIBMenuMyActivitiesFocusThai;
        } else {
            form_name.btnMenuMyActivities.skin = btnIBMenuMyActivitiesFocus;
            if (!isIE8) form_name.btnMenuMyActivities.hoverSkin = btnIBMenuMyActivitiesFocus;
        }
    }
}