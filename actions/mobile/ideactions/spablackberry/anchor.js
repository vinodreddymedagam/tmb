/*
************************************************************************
            Name    : anchorup
            Author  : Developer
            Date    : April, 2013
            Purpose : to show the popup on entering text into Transaction Password text box
        	Output params: shows popup on entring text into tranaction password text box 
        	ServiceCalls: nil
        	Called From : on editing text
*************************************************************************/
function anchorup() {
    var context1
        //deviceInfo = kony.os.deviceInfo();
    var type = gblDeviceInfo["type"];
    var category = gblDeviceInfo["category"];
    /*	   	
    	if(type == "spa" && category == "iphone"){
     		context1= {"widget":frmMBsetPasswd.txtTransPass,"anchor":"bottom","sizetoanchorwidth":false};
     	}else if(type == "spa" && category == "blackberry"){
     		context1= {"widget":frmMBsetPasswd.txtTransPass,"anchor":"bottom","sizetoanchorwidth":false};	
     	}else if (type == "spa" && category == "android"){
     		context1= {"widget":frmMBsetPasswd.txtTransPass,"anchor":"bottom","sizetoanchorwidth":false};	
     	}else
     	{
     		context1= {"widget":frmMBsetPasswd.txtTransPass,"anchor":"bottom","sizetoanchorwidth":false};	
     	}
   */
    popBubble.setContext(context1);
    var str1;
    if (!gblflag) {
        str1 = frmMBsetPasswd.txtTransPass.text;
    } else {
        str1 = frmMBsetPasswd.txtTemp.text;
    }
    if (str1 != null && str1.length < 2) {
        popBubble.show();
    }
}
/*
************************************************************************
            Name    : anchorupAccessPIN
            Author  : Developer
            Date    : April, 2013
            Purpose : to show the popup on entering text into Access PIN text box
        	Output params: shows popup on entring text into  Access PIN text box 
        	ServiceCalls: nil
        	Called From : on editing text
************************************************************************
 */
function anchorupAccessPIN() {
    var currForm = kony.application.getCurrentForm();
    var context1
        //deviceInfo = kony.os.deviceInfo();
    var type = gblDeviceInfo["type"];
    var category = gblDeviceInfo["category"];
    /*	   	
    	if(type == "spa" && category == "iphone"){
     		context1= {"widget":frmMBsetPasswd.txtTransPass,"anchor":"bottom","sizetoanchorwidth":false};
     	}else if(type == "spa" && category == "blackberry"){
     		context1= {"widget":frmMBsetPasswd.txtTransPass,"anchor":"bottom","sizetoanchorwidth":false};	
     	}else if (type == "spa" && category == "android"){
     		context1= {"widget":frmMBsetPasswd.txtTransPass,"anchor":"bottom","sizetoanchorwidth":false};	
     	}else
     	{
     		context1= {"widget":frmMBsetPasswd.txtTransPass,"anchor":"bottom","sizetoanchorwidth":false};	
     	}
   */
    popAccessPinBubble.setContext(context1);
    var str1;
    if (!gblflag || currForm.id == "frmCMChgAccessPin") {
        str1 = currForm.txtAccessPwd.text;
    } else {
        str1 = currForm.txtTempAccess.text;
    }
    if (str1 != null && str1.length < 2) {
        if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPhone Simulator" || gblDeviceInfo["name"] == "iPad") {
            kony.timer.schedule("bubbleTimer", callbackShowpop, 1, false);
        } else {
            popAccessPinBubble.show();
        }
    }
}

function callbackShowpop() {
    popAccessPinBubble.show();
}

function anchorupAccessPINunmask() {
    var currForm = kony.application.getCurrentForm();
    var context1
        //deviceInfo = kony.os.deviceInfo();
    var type = gblDeviceInfo["type"];
    var category = gblDeviceInfo["category"];
    /*	   	
    	if(type == "spa" && category == "iphone"){
     		context1= {"widget":frmMBsetPasswd.txtTransPass,"anchor":"bottom","sizetoanchorwidth":false};
     	}else if(type == "spa" && category == "blackberry"){
     		context1= {"widget":frmMBsetPasswd.txtTransPass,"anchor":"bottom","sizetoanchorwidth":false};	
     	}else if (type == "spa" && category == "android"){
     		context1= {"widget":frmMBsetPasswd.txtTransPass,"anchor":"bottom","sizetoanchorwidth":false};	
     	}else
     	{
     		context1= {"widget":frmMBsetPasswd.txtTransPass,"anchor":"bottom","sizetoanchorwidth":false};	
     	}
   */
    popAccessPinBubble.setContext(context1);
    var str1;
    if (!gblflag || currForm.id == "frmCMChgAccessPin") {
        str1 = currForm.txtAccessPwd.text;
    } else {
        str1 = currForm.txtTempAccess.text;
    }
    if (str1 != null && str1.length < 2) {
        popAccessPinBubble.show();
    }
}

function anchorupTransPass() {
    var currForm = kony.application.getCurrentForm();
    var context1
        //deviceInfo = kony.os.deviceInfo();
    var type = gblDeviceInfo["type"];
    var category = gblDeviceInfo["category"];
    /*	   	
    	if(type == "spa" && category == "iphone"){
     		context1= {"widget":frmMBsetPasswd.txtTransPass,"anchor":"bottom","sizetoanchorwidth":false};
     	}else if(type == "spa" && category == "blackberry"){
     		context1= {"widget":frmMBsetPasswd.txtTransPass,"anchor":"bottom","sizetoanchorwidth":false};	
     	}else if (type == "spa" && category == "android"){
     		context1= {"widget":frmMBsetPasswd.txtTransPass,"anchor":"bottom","sizetoanchorwidth":false};	
     	}else
     	{
     		context1= {"widget":frmMBsetPasswd.txtTransPass,"anchor":"bottom","sizetoanchorwidth":false};	
     	}
   */
    popBubble.setContext(context1);
    var str1;
    if (!gblflag || currForm.id == "frmCMChgTransPwd") {
        str1 = currForm.txtTransPass.text;
    } else {
        str1 = currForm.txtTemp.text;
    }
    if (str1 != null && str1.length < 2) {
        if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPhone Simulator" || gblDeviceInfo["name"] == "iPad") {
            kony.timer.schedule("bubbleTransTimer", callbackTransShowpop, 1, false);
        } else {
            popBubble.show();
        }
    }
}

function callbackTransShowpop() {
    popBubble.show();
}

function anchorupTransPassunmask() {
    var currForm = kony.application.getCurrentForm();
    var context1
        //deviceInfo = kony.os.deviceInfo();
    var type = gblDeviceInfo["type"];
    var category = gblDeviceInfo["category"];
    /*	   	
    	if(type == "spa" && category == "iphone"){
     		context1= {"widget":frmMBsetPasswd.txtTransPass,"anchor":"bottom","sizetoanchorwidth":false};
     	}else if(type == "spa" && category == "blackberry"){
     		context1= {"widget":frmMBsetPasswd.txtTransPass,"anchor":"bottom","sizetoanchorwidth":false};	
     	}else if (type == "spa" && category == "android"){
     		context1= {"widget":frmMBsetPasswd.txtTransPass,"anchor":"bottom","sizetoanchorwidth":false};	
     	}else
     	{
     		context1= {"widget":frmMBsetPasswd.txtTransPass,"anchor":"bottom","sizetoanchorwidth":false};	
     	}
   */
    popBubble.setContext(context1);
    var str1;
    if (!gblflag || currForm.id == "frmCMChgTransPwd") {
        str1 = currForm.txtTransPass.text;
    } else {
        str1 = currForm.txtTemp.text;
    }
    if (str1 != null && str1.length < 2) {
        popBubble.show();
    }
}