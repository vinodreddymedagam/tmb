editIndex = "";
deleteIndex = "";
arrayBenificiary = [];
gblTest = true;
isNickNameForm = false;
isBacktoNickName = false;
amountValueFromNickName = "";
gblSwipedBenificiary = "";
gblButtonId = "";
gblSelectedIndexSc = "";

function setDatatoform(results, i) {
	var relation="";
	kony.print("results  "+ JSON.stringify(results));
	if (kony.i18n.getCurrentLocale() == "th_TH"){
		//var code=gblRelationDescAndCodeTH[results["relation"]];
		//kony.print("th code"+ code);
		relation=gblRelationCodeAndDescTH[results["relationCode"]];
		kony.print("relation "+ relation);
	}else{
		//var code=gblRelationDescAndCodeEN[results["relation"]];
		//kony.print("en code"+ code);
		relation=gblRelationCodeAndDescEN[results["relationCode"]];
		kony.print("relation "+ relation);
	}
	

//adding labels to the flexdetails
var ButtonHidden = new kony.ui.Button({
        "id": "ButtonHidden"+i,
        "top": "0dp",
        "left": "0dp",
        "width": "217dp",
        "minHeight": "50dp",
        "zIndex": 2,
        "isVisible": true,
		"onClick": onClickEditBeneficiary,
        "text": "asdssddssd",
        "skin": "btnTranslogoutWBG",
        "focusSkin": "btnTranslogoutWBG"
    }, {
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": false,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {});
    var lblBenificiaryName = new kony.ui.Label({
        "id": "lblBenificiaryName"+i,
        "top": "0dp",
        "left": "12dp",
        "width": "90%",
        "zIndex": 1,
        "isVisible": true,
        "text": results["firstName"] + " " + results["lastName"],
        "skin": "lblBlue48px"
    }, {
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {
        "textCopyable": false
    });
    var lblBenificiaryRelation = new kony.ui.Label({
        "id": "lblBenificiaryRelation"+i,
        "top": "3dp",
        "left": "13dp",
        "zIndex": 1,
        "isVisible": true,
        "text": relation,
        "skin": "lblDarkBlueDBOzone128"
    }, {
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {
        "textCopyable": false
    });
    var FlexSubtwo = new kony.ui.FlexContainer({
        "id": "FlexSubtwo"+i,
        "top": "0dp",
        "left": "0dp",
        "width": "100%",
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "zIndex": 1,
        "isVisible": true,
        "clipBounds": true,
        "Location": "[0,0]",
        "skin": "slFbox",
        "layoutType": kony.flex.FLOW_VERTICAL
    }, {
        "padding": [0, 0, 0, 0]
    }, {});;
    FlexSubtwo.setDefaultUnit(kony.flex.DP)
    FlexSubtwo.add(lblBenificiaryName, lblBenificiaryRelation);
    var flexBenificiaryDetails = new kony.ui.FlexContainer({
        "id": "flexBenificiaryDetails"+i,
        "top": "0dp",
        "left": "0dp",
        "width": "75%",
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "zIndex": 1,
        "isVisible": true,
        "clipBounds": true,
        "Location": "[0,0]",
        "skin": "slFbox",
        "layoutType": kony.flex.FREE_FORM
    }, {
        "padding": [0, 0, 0, 0]
    }, {});;
    flexBenificiaryDetails.setDefaultUnit(kony.flex.DP)
    flexBenificiaryDetails.add(ButtonHidden, FlexSubtwo);
    var btnBenficiaryEdit = new kony.ui.Button({
        "id": "btnBenficiaryEdit"+i,
        "top": "0dp",
        "left": "0dp",
        "width": "50%",
        "height": "100%",
        "zIndex": 1,
        "isVisible": true,
        "text": null,
        "skin": "btnEditNew",
		"onClick": onClickEditBeneficiary,
        "focusSkin": "btnEditNewFocus"
    }, {
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {});
    var btnBenficiaryDelete = new kony.ui.Button({
        "id": "btnBenficiaryDelete"+i,
        "top": "0dp",
        "left": "0dp",
        "width": "50%",
        "height": "100%",
        "zIndex": 1,
        "isVisible": true,
        "text": null,
        "skin": "btnDeleteNew",
		"onClick": onClickDeleteBeneficiary,
        "focusSkin": "btnDeleteNewFocus"
    }, {
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {});
    var flexButtons = new kony.ui.FlexContainer({
        "id": "flexButtons"+i,
        "top": "0dp",
        "left": "150dp",
        "width": "30%",
        "height": "50dp",
        "zIndex": 1,
        "isVisible": false,
        "clipBounds": true,
        "Location": "[215,0]",
        "skin": "slFbox",
        "layoutType": kony.flex.FLOW_HORIZONTAL
    }, {
        "padding": [0, 0, 0, 0]
    }, {});;
    flexButtons.setDefaultUnit(kony.flex.DP)
    flexButtons.add(btnBenficiaryEdit, btnBenficiaryDelete);
      // Set swipe functions here
      var setSwipe = {
                              fingers: 1,
                              swipedistance: 90,
                              swipevelocity: 25,
                              swipeDirection:2
                              
                        };
    gblSwipedBenificiary = "";
    flexBenificiaryDetails.addGestureRecognizer(2, setSwipe, mySwipeCallback); 
    var per = 0;
      if(gblTest){
                  per = "100";
                  arrayBenificiary[0]["percentage"] = "100";
                  gblTest = false;
      }
      else  if(results["percentage"] != null && results["percentage"] != undefined ){
      per = results["percentage"];
    }
    var txtPercentage = new kony.ui.TextBox2({
        "id": "txtPercentage"+i,
        "top": "6dp",
        "left": "0dp",
        "width": "100dp",
        "height": "40dp",
        "zIndex": 1,
        "isVisible": true,
		"maxTextLength": 3,
        "text": per,
        "secureTextEntry": false,
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "placeholder": null,
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "skin": "txtBlueNormal200",
		"focusSkin": "txtGreyNormal200Focus",
		"onDone": onDonePercentage,
		"onTouchStart":onTouchStartPercentage,
        "onTextChange": onTextChangePercentage,
       // "onBeginEditing": clearPercentageValue,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD
		//"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DECIMAL
    }, {
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {
    	"onEndEditing": onDonePercentage,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "autoFilter": false,
        "showCloseButton": true,
        "closeButtonText": "Done",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flexBenificiaryPercentage = new kony.ui.FlexContainer({
        "id": "flexBenificiaryPercentage"+i,
        "top": "0dp",
        "left": "275dp",
        "width": "24%",
        "height": "50dp",
        "zIndex": 1,
        "isVisible": true,
        "clipBounds": true,
        "Location": "[275,0]",
        "skin": "slFbox",
        "layoutType": kony.flex.FREE_FORM
    }, {
        "padding": [0, 0, 0, 0]
    }, {});;
    flexBenificiaryPercentage.setDefaultUnit(kony.flex.DP)
    flexBenificiaryPercentage.add(txtPercentage);
    var flexsubone = new kony.ui.FlexContainer({
        "id": "flexsubone"+i,
        "top": "5dp",
        "left": "0dp",
        "width": "100%",
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "zIndex": 1,
        "isVisible": true,
        "clipBounds": true,
        "Location": "[0,0]",
        "skin": "slFbox",
        "layoutType": kony.flex.FREE_FORM
    }, {
        "padding": [0, 0, 0, 0]
    }, {});;
    flexsubone.setDefaultUnit(kony.flex.DP)
    flexsubone.add(flexBenificiaryDetails, flexButtons, flexBenificiaryPercentage);
    //
	    var seperatorLeft = new kony.ui.Label({
        "id": "seperatorLeft" + i,
        "left": "4.0%",
        "bottom": "0%",
        "width": "60%",
        "height": "1px",
        "zIndex": 1,
        "isVisible": true,
        "text": ".",
        "skin": "lblBlueNew"
    }, {
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {
        "textCopyable": false
    });
    var seperatorRight = new kony.ui.Label({
        "id": "seperatorRight" + i,
        "left": "3.0%",
        "bottom": "0%",
        "width": "29%",
        "height": "1dp",
        "zIndex": 1,
        "isVisible": true,
        "text": null,
        "skin": "lblBlueNew"
    }, {
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {
        "textCopyable": false
    });
    var flexBenificiarySeperator = new kony.ui.FlexContainer({
        "id": "flexBenificiarySeperator"+i,
        "top": "0dp",
        "left": "2dp",
        "width": "99.49%",
        "height": "3dp",
        "zIndex": 1,
        "isVisible": true,
        "clipBounds": true,
        "Location": "[2,220]",
        "skin": "slFbox",
        "layoutType": kony.flex.FLOW_HORIZONTAL
    }, {
        "padding": [0, 0, 0, 0]
    }, {});;
    flexBenificiarySeperator.setDefaultUnit(kony.flex.DP)
    flexBenificiarySeperator.add(seperatorLeft, seperatorRight);
    var flexBenificiary = new kony.ui.FlexContainer({
        "id": "flexBenificiary"+i,
        "top": "0dp",
        "left": "0dp",
        "width": "100%",
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "zIndex": 1,
        "isVisible": true,
        "clipBounds": true,
        "Location": "[0,0]",
        "skin": "flexWhiteBG",
        "layoutType": kony.flex.FLOW_VERTICAL
    }, {
        "padding": [0, 0, 0, 0]
    }, {});;
    flexBenificiary.setDefaultUnit(kony.flex.DP)
    flexBenificiary.add(flexsubone, flexBenificiarySeperator);
    frmMBSavingsCareAddNickName.flexBodyScroll.add(flexBenificiary);

}

//arrayBenificiary = [{firstName:"bala", lastName:"Don", relation:"Father"},{firstName:"Chiru", lastName:"Don1", relation:"Son"}];



function addBeneficiaryDetails() {
    var firstName,lastName, relation = "", code="";
    firstName = frmMBSavingsCareAddBeneficiary.txtfirstName.text.trim();
    lastName = frmMBSavingsCareAddBeneficiary.txtlastName.text.trim();
    lastName = lastName.replace(/  +/g, ' ');
    relation = frmMBSavingsCareAddBeneficiary.lblrelation.text;
    //[{firstName:firstName, lastName:lastName, relation:relation},{firstName:"Chiru", lastName:"Don1", relation:"Son"}];
    kony.print("firstName---->" + firstName + "lastName---->" + lastName + "relation----->" + relation);

   if (kony.i18n.getCurrentLocale() == "th_TH"){
		kony.print("new gblRelationDescAndCodeEN "+JSON.stringify(gblRelationDescAndCodeTH));
		code=gblRelationDescAndCodeTH[relation];
	}else{
		kony.print("new gblRelationDescAndCodeEN "+JSON.stringify(gblRelationDescAndCodeEN));
		code=gblRelationDescAndCodeEN[relation];
	}
	if(arrayBenificiary.length==0){
		arrayBenificiary.push({"firstName":firstName, "lastName":lastName, "relation":relation,"relationCode":code,"percentage":"100"});
	}else{
		arrayBenificiary.push({"firstName":firstName, "lastName":lastName, "relation":relation,"relationCode":code,"percentage":"0"});
	}	
	kony.print("new Array "+JSON.stringify(arrayBenificiary));
    //showNickNameForm();
    frmMBSavingsCareAddNickName.show();
}

function showNickNameForm() {
    for (var i = 0; i < arrayBenificiary.length; i++) {
        setDatatoform(arrayBenificiary[i], i);
    }
    addBenificiaryLink();
    frmMBSavingsCareAddNickName.show(); // Commented for testing through postshow();
}





function mySwipeCallback(eventobject,gestureInfo) {

    var selectedFlexId = eventobject["id"];
    var selectedFlexIdIndex = eventobject["id"].replace("flexBenificiaryDetails", "");
    if(gestureInfo.swipeDirection == 1){
		    frmMBSavingsCareAddNickName["ButtonHidden"+selectedFlexIdIndex].onClick = callDummy;
		    frmMBSavingsCareAddNickName[selectedFlexId].forceLayout();
		    if(gblSwipedBenificiary  != ""){
			     if(gblSwipedBenificiary != selectedFlexIdIndex){
			     	var randGen = parseInt(Math.random()*10000)+""	     	
			     	kony.timer.schedule(randGen, setEditBenCallbackForFlex, 1, false);
			    	frmMBSavingsCareAddNickName["flexBenificiaryDetails"+gblSwipedBenificiary].left = "0.0%";
			    	frmMBSavingsCareAddNickName["flexButtons"+gblSwipedBenificiary].isVisible=false;
			   	 }
		    }
		        frmMBSavingsCareAddNickName["flexBenificiaryDetails" + selectedFlexIdIndex].left = "-30%";
		        gblSwipedBenificiary = selectedFlexIdIndex;
		        frmMBSavingsCareAddNickName["flexButtons"+selectedFlexIdIndex].isVisible=true;
    } 
    	if(gestureInfo.swipeDirection == 2){
	        gblSwipedBenificiary = "";
	        var randGen = parseInt(Math.random()*10000)+""
		    kony.timer.schedule(randGen, setEditBenCallbackForFlex, 2, false);
	    	//frmMBSavingsCareAddNickName["ButtonHidden"+selectedFlexIdIndex].onClick = onClickEditBeneficiary;
	        frmMBSavingsCareAddNickName["flexBenificiaryDetails" + selectedFlexIdIndex].left = "0.0%";
	        frmMBSavingsCareAddNickName["flexButtons"+selectedFlexIdIndex].isVisible=false;
        }
        
    function setEditBenCallbackForFlex(){
		frmMBSavingsCareAddNickName["ButtonHidden"+selectedFlexIdIndex].onClick = onClickEditBeneficiary;
	}
}



function addBenificiaryLink() {
    var showAddBenif = true;
    var showlastline=false
    if (arrayBenificiary.length == gblMaxBeneficiaryConfig) {
        showAddBenif = false;
        showlastline = true;
    }
    var label52440721750230 = new kony.ui.Label({
        "id": "label52440721750230",
        "top": "0%",
        "left": "0%",
        "width": "50%",
        "centerX": "50%",
        "centerY": "50%",
        "zIndex": 1,
        "isVisible": true,
        "text": kony.i18n.getLocalizedString("btn_addBenefi"),
        "skin": "lblBlue48px"
		//"skin": "lblBlue40px"
    }, {
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {
        "textCopyable": false
    });

    var flexAddBeneficiary = new kony.ui.FlexContainer({
        "id": "flexAddBeneficiary",
        "top": "0%",
        "left": "0%",
        "width": "102%",
        "height": "10.0%",
        "centerX": "50%",
        "zIndex": 1,
        "isVisible": showAddBenif,
        "clipBounds": true,
        "Location": "[5,363]",
        "skin": "flexWhiteBGBlueBorder",
        "focusSkin": "flexWhiteBGBlueBorder",
        "onClick": onClickAddBeneficiary,
        "layoutType": kony.flex.FLOW_HORIZONTAL
    }, {
        "padding": [0, 0, 0, 0]
    }, {});;
    flexAddBeneficiary.setDefaultUnit(kony.flex.DP)
    flexAddBeneficiary.add(
    label52440721750230);
    var flexlastline = new kony.ui.FlexContainer({
        "id": "flexlastline",
        "top": "1dp",
        "left": "0dp",
        "width": "102%",
        "height": "1dp",
        "centerX": "50%",
        "zIndex": 1,
        "isVisible": true,
        "clipBounds": true,
        "Location": "[-4,396]",
        "skin": "flexBlueLine",
        "focusSkin": "flexBlueLine",
        "layoutType": kony.flex.FREE_FORM
    }, {
        "padding": [0, 0, 0, 0]
    }, {});;
    flexlastline.setDefaultUnit(kony.flex.DP);
    frmMBSavingsCareAddNickName.flexBodyScroll.add(flexAddBeneficiary,flexlastline);

}

function onClickEditBeneficiary(eventobject) {
    var selectedFlexId = eventobject["id"];
    var index = selectedFlexId.substring(selectedFlexId.length - 1);
    kony.print("selected index---->" + JSON.stringify(index));
    var editableRecord = arrayBenificiary[index];
    editIndex = "";
    editIndex = index;
    kony.print("editableRecord---->" + JSON.stringify(editableRecord));
    if (editableRecord != null) {
        frmMBSavingsCareEditBeneficiary.txtfirstName.text = editableRecord["firstName"];
        frmMBSavingsCareEditBeneficiary.txtlastName.text = editableRecord["lastName"];
        var rel="";
		if (kony.i18n.getCurrentLocale() == "th_TH"){
			
			rel=gblRelationCodeAndDescTH[editableRecord["relationCode"]];
		}else{
			
			rel=gblRelationCodeAndDescEN[editableRecord["relationCode"]];
		}
		frmMBSavingsCareEditBeneficiary.lblrelation.text =rel;//editableRecord["relation"];
        frmMBSavingsCareEditBeneficiary.flxDeleteBenif.setVisibility(true);
        frmMBSavingsCareEditBeneficiary.txtfirstName.placeholder = removeColonFromEnd(kony.i18n.getLocalizedString("keyFirstName"));
        frmMBSavingsCareEditBeneficiary.txtlastName.placeholder = removeColonFromEnd(kony.i18n.getLocalizedString("keyLastName"));
        frmMBSavingsCareEditBeneficiary.lblHeading.text = kony.i18n.getLocalizedString("edit_benefi");
        frmMBSavingsCareEditBeneficiary.button761510865391611.text = kony.i18n.getLocalizedString("Back");
        frmMBSavingsCareEditBeneficiary.button761510865391613.text = kony.i18n.getLocalizedString("Next");
        frmMBSavingsCareEditBeneficiary.label1759835057888695.text = kony.i18n.getLocalizedString("delete_benefi");
        //frmMBSavingsCareEditBeneficiary.Label09f769a45499649.text=kony.i18n.getLocalizedString("editBenefit");
        //frmMBSavingsCareEditBeneficiary.flexLastLine.setVisibility(true);
        //frmMBSavingsCareEditBeneficiary.lblDelete.text=kony.i18n.getLocalizedString("keyDelete");
        accEdit = "true";
        frmMBSavingsCareEditBeneficiary.show();
    } else {
        kony.print("No records found to edit in arrayBenificiary array");
    }
}




function onClickDeleteBeneficiary(eventobject) {
    //alert("Are you Sure you want to Delete Beneficiary");
    var index = eventobject["id"].replace("btnBenficiaryDelete", "");

    kony.print("index=" + index);
    index = parseInt(index);
    //index = parseInt(index);
    deleteIndex = index;
    popupConfrmDelete.lblheader.text = kony.i18n.getLocalizedString("delete_benefi");
    popupConfrmDelete.lblPopupConfText.text = kony.i18n.getLocalizedString("deleteBeniftxt");
    popupConfrmDelete.btnpopConfDelete.text = kony.i18n.getLocalizedString("keyDeletebenefit");
    popupConfrmDelete.btnPopupConfCancel.text = kony.i18n.getLocalizedString("keyCancelbenefit");
    popupConfrmDelete.btnpopConfDelete.onClick = deleteBenifRecord;
/*
				kony.print("index=="+index)
				kony.print("Length before=="+arrayBenificiary.length);
				for(var i=0;i<arrayBenificiary.length;i++)
					kony.print("BEFORE splice deleteBenifRecord" + JSON.stringify(arrayBenificiary[i]));
				
				arrayBenificiary.splice(index,1);
				for(var i=0;i<arrayBenificiary.length;i++)
					kony.print("AFTER splice deleteBenifRecord" + JSON.stringify(arrayBenificiary[i]));
				//kony.print("After splice deleteBenifRecord" + arrayBenificiary[0]);
kony.print("Length AFTER=="+arrayBenificiary.length);
				popupConfrmDelete.dismiss();
				showLoadingScreen();
				postshowSavingsCare();
				
		};
		**/
    popupConfrmDelete.show();
}

function onClickAddBeneficiary() {
    frmMBSavingsCareAddBeneficiary.lblHeading.text = kony.i18n.getLocalizedString("addBenefi");
    frmMBSavingsCareAddBeneficiary.txtfirstName.placeholder = removeColonFromEnd(kony.i18n.getLocalizedString("keyFirstName"));
    frmMBSavingsCareAddBeneficiary.txtlastName.placeholder = removeColonFromEnd(kony.i18n.getLocalizedString("keyLastName"));
    frmMBSavingsCareAddBeneficiary.lblrelation.text = kony.i18n.getLocalizedString("keyOpenRelation");
    frmMBSavingsCareAddBeneficiary.button761510865391611.text = kony.i18n.getLocalizedString("Back");
    frmMBSavingsCareAddBeneficiary.button761510865391613.text = kony.i18n.getLocalizedString("Next");
    frmMBSavingsCareAddBeneficiary.txtfirstName.text = "";
    frmMBSavingsCareAddBeneficiary.txtlastName.text = "";
    accEdit = "false";
    gblSelectedRel="";
    frmMBSavingsCareAddBeneficiary.show();
}

function showBeneficiariesForm() {
    isNickNameForm = false;
    //addBenificiaryLink();	
    if (undefined != gblAccountTable["SAVING_CARE_MAX_BENEFICIARIES"]) {
        gblMaxBeneficiaryConfig = parseInt(gblAccountTable["SAVING_CARE_MAX_BENEFICIARIES"]);
    } else {
        gblMaxBeneficiaryConfig = 0;
    }
    if (!validateAmount()) {
        return false;
    } else {
        amountValueFromNickName = "";
        isBacktoNickName = false;
        kony.print("Validations success navigate to beneficiaries form");
        gblOpenSavingsCareNickName = "";
        arrayBenificiary = [];
        gblTest = true;
        //frmMBSavingsCareAddNickName.txtAmount.text = commaFormatted(parseFloat(removeCommos(frmMBSavingsCareAddBal.txtAmountVal.text)).toFixed(2));
		gblSelectedIndexSc = frmMBSavingsCareAddBal.segNSSlider.selectedIndex;								
        frmMBSavingsCareAddNickName.show();
    }
}

function saveBeneficiaries() {
    var fNameSave,lNameSave, relationSave = "", code="";
    fNameSave = frmMBSavingsCareEditBeneficiary.txtfirstName.text.trim();
    lNameSave = frmMBSavingsCareEditBeneficiary.txtlastName.text.trim();
    lNameSave = lNameSave.replace(/  +/g, ' ');
    relationSave = frmMBSavingsCareEditBeneficiary.lblrelation.text;
    kony.print("Index--->" + editIndex + "fNameSave--->" + fNameSave + "lNameSave---->" + lNameSave + "relationSave---->" + relationSave);
    //arrayBenificiary[editIndex] = {"firstName":fNameSave, "lastName":lNameSave, "relation":relationSave:"percentage":"50%"};
	if (kony.i18n.getCurrentLocale() == "th_TH"){
		kony.print("gblRelationDescAndCodeTH "+JSON.stringify(gblRelationDescAndCodeTH));
		code=gblRelationDescAndCodeTH[relationSave];
	}else{
		kony.print("gblRelationDescAndCodeTH "+JSON.stringify(gblRelationDescAndCodeTH));
		code=gblRelationDescAndCodeEN[relationSave];
	}
    arrayBenificiary[editIndex]["firstName"] = fNameSave;
    arrayBenificiary[editIndex]["lastName"] = lNameSave;
    arrayBenificiary[editIndex]["relation"] = relationSave;
    arrayBenificiary[editIndex]["relationCode"] = code;
    //showNickNameForm();
    frmMBSavingsCareAddNickName.show();
}


// This has to be called in the postshow

function postshowSavingsCare() {

    if (gblFromNickNameBack) {
        if (frmMBSavingsCareAddNickName.flexBodyScroll != undefined && frmMBSavingsCareAddNickName.flexBodyScroll != null) {
            frmMBSavingsCareAddNickName.flexBodyScroll.removeAll();
        }
        savingsCareFixedForm(); // Fixed content to be displayed on the page
        //arrayBenificiary.length = gblMaxBeneficiaryConfig;
        //kony.print("arrayBenificiary.length in postshowSavingsCare" + arrayBenificiary.length)
        if (arrayBenificiary.length <= 0) {
            kony.print("inside length in postshowSavingsCare");
            addRichTextToTheForm(); // First time on the landing
            frmMBSavingsCareAddNickName.lblBenifitHeading.text = "";
            frmMBSavingsCareAddNickName.lblavailPercent.text = "";
        } else {
            kony.print("testing 1111111");
            for (var i = 0; i < arrayBenificiary.length; i++) {
                setDatatoform(arrayBenificiary[i], i);
            }
            //frmMBSavingsCareAddNickName.lblavailPercent.text = "0" + " %" +" " + kony.i18n.getLocalizedString("keyAvailable");
            calculatePercentage();
        }
        kony.print("testing 2222222");
        //if(isBacktoNickName){
        //	kony.print("inside postshowSavingsCare amountValueFromNickName" + amountValueFromNickName)
        //	frmMBSavingsCareAddNickName.txtAmount.text = amountValueFromNickName;
        //}else{
        frmMBSavingsCareAddNickName.txtAmount.text = commaFormatted(parseFloat(removeCommos(frmMBSavingsCareAddBal.txtAmountVal.text)).toFixed(2));
        //}
        kony.print("testing 33333333");
        frmMBSavingsCareAddNickName.txtAmount.setEnabled(false);
        kony.print("testing 4444444");
	        addBenificiaryLink();  
	        var deviceHeight = parseInt(kony.os.deviceInfo()["deviceHeight"])
	         var deviceName = kony.os.deviceInfo()["name"]
	     	  if((deviceHeight < 1400) && deviceName.toLowerCase() == "iphone" ){
	       		setPropertiesForIphoneSmall();  
	       }	
        kony.print("testing 555555555");
        if (gblSavingsCareFlow == "AccountDetailsFlow" || gblSavingsCareFlow == "MyAccountFlow") {
            frmMBSavingsCareAddNickName.flxAmount.setVisibility(false);
            frmMBSavingsCareAddNickName.flxLine1.setVisibility(false);
        } else {
            kony.print("testing 666666666");
            frmMBSavingsCareAddNickName.flxAmount.setVisibility(true);
            kony.print("testing 777777777");
        }
        dismissLoadingScreen();
    }
    gblFromNickNameBack = true;
}




flg="Y";
var oldval="";
function onTouchStartPercentage(eventobject){
	var percentagetxtName = eventobject["id"];
	frmMBSavingsCareAddNickName[percentagetxtName].maxTextLength=4;
	var len=frmMBSavingsCareAddNickName[percentagetxtName].maxTextLength;
	oldval=frmMBSavingsCareAddNickName[percentagetxtName].text;
	kony.print("GOWRI222 N flg="+flg);
	kony.print("GOWRI222 N oldval="+oldval);
	 flg="N";
}

function onTextChangePercentage(eventobject) {
var percentagetxtName = eventobject["id"];
kony.print("GOWRI222 flg="+flg);
var val="";
	if(flg=="N"){
			val=frmMBSavingsCareAddNickName[percentagetxtName].text;
			kony.print("GOWRI222 her1 oldval="+oldval);
			kony.print("GOWRI222 her1 val="+val);
			var oldvalArr=oldval.split("");
			var valArr=val.split("");
			var finalArr=val.split("");
			kony.print("GOWRI SPLICE BEFORE oldvalArr="+oldvalArr);
			kony.print("GOWRI SPLICE BEFORE finalArr="+finalArr);			
				for(var x=0;x<oldvalArr.length;x++){
				var o=oldvalArr[x];
				kony.print("GOWRI SPLICE in oldvalArr["+x+"]="+o);
				for(var y=0;y<finalArr.length;y++){
					var n=finalArr[y];
					kony.print("GOWRI SPLICE in finalArr["+y+"]="+n);
					if(n==o){
						kony.print("GOWRI SPLICE DELETING");
						finalArr.splice(y,1);
						break;
					}
				}
				
			}
			kony.print("GOWRI SPLICE AFTER oldvalArr="+oldvalArr);
			kony.print("GOWRI SPLICE AFTER finalArr="+finalArr);	
			if(oldvalArr.length==3){
				frmMBSavingsCareAddNickName[percentagetxtName].text ="";
				frmMBSavingsCareAddNickName[percentagetxtName].text =finalArr.toString();
			}else{
				frmMBSavingsCareAddNickName[percentagetxtName].text =finalArr.toString();
			}
		flg="Y";
		frmMBSavingsCareAddNickName[percentagetxtName].maxTextLength=3;
	}
	    var index = eventobject["id"].replace("txtPercentage", ""); //txtPercentage1,txtPercentage2,1
	    index = parseInt(index);
	    var percentagetxtName = eventobject["id"];
	     percentagetxtVal = frmMBSavingsCareAddNickName[percentagetxtName].text; //100
	    percentagetxtVal = percentagetxtVal.replace(/^[0]+/g, "");
	    arrayBenificiary[index]["percentage"] = percentagetxtVal;
	    frmMBSavingsCareAddNickName[percentagetxtName].text = percentagetxtVal;
}


/*function onTextChangePercentage(eventobject) {
    var index = eventobject["id"].replace("txtPercentage", ""); //txtPercentage1,txtPercentage2,1
    index = parseInt(index);
    var percentagetxtName = eventobject["id"];
    var percentagetxtVal = frmMBSavingsCareAddNickName[percentagetxtName].text; //100
    percentagetxtVal = percentagetxtVal.replace(/^[0]+/g, "");
    arrayBenificiary[index]["percentage"] = percentagetxtVal;
    frmMBSavingsCareAddNickName[percentagetxtName].text = percentagetxtVal;
}*/

function onNextPercentage(){
	var enteredPercentage = 0;
	var strEnteredPercentage="";
	var totalEnteredPercentage = 0;
	var fixedBenefit = 100;
	var errMsg="";
	var validFlag=true;
	if(arrayBenificiary.length!=0){
		for(var i = 0;i<arrayBenificiary.length;i++){
			strEnteredPercentage=arrayBenificiary[i]["percentage"];
			if(strEnteredPercentage==null || strEnteredPercentage==""){
				enteredPercentage = 0;
			}else{
				enteredPercentage = parseInt(strEnteredPercentage);
			}	
	        
	        kony.print("enteredPercentage  "+enteredPercentage);	
	        if( enteredPercentage <= 0){
	        	validFlag=false;
	        	kony.print("validFlag  "+validFlag);
	            //errMsg="You have not allocated benefits to one of your beneficiaries. If a beneficiary is set to receive 0%, that person will be removed from the system."//012_001;
	            //errMsg=kony.i18n.getLocalizedString("benefit_notAllocatedBenfi");
	            totalEnteredPercentage +=  enteredPercentage;	
	            //break;
	            
	        }
	        else
	        {
	        	totalEnteredPercentage +=  enteredPercentage;
	        }
	        
					
		}
		
		
	     if(totalEnteredPercentage!=fixedBenefit){
	    	
	        //errMsg="The total amount allocated must be equal to 100%. Please check the amount." 012_002;
			errMsg=kony.i18n.getLocalizedString("check_totalAmt");
			showAlert(errMsg, kony.i18n.getLocalizedString("info")); 
			return false;
	    }	
		else if(!validFlag){
			
			//errMsg="You have not allocated benefits to one of your beneficiaries. If a beneficiary is set to receive 0%, that person will be removed from the system."//012_001;
	    	errMsg=kony.i18n.getLocalizedString("benefit_notAllocatedBenfi");
	    	showtotalPercentagePopup(errMsg);
			return false;
		}
	}
 return true;

}

function showtotalPercentagePopup(errMsg){
    popupConfrmDelete.lblheader.text=kony.i18n.getLocalizedString("info");
    popupConfrmDelete.btnpopConfDelete.text=kony.i18n.getLocalizedString("keyOK");
    popupConfrmDelete.btnPopupConfCancel.text=kony.i18n.getLocalizedString("keyCancelButton");
    popupConfrmDelete.btnpopConfDelete.onClick=onClickPopUpOkPercentage;
    //popupConfrmDelete.btnPopupConfCancel.text=popupConfrmDelete.dismiss();
    popupConfrmDelete.lblPopupConfText.text=errMsg;
    popupConfrmDelete.show();
 
}
function onClickPopUpOkPercentage(){
	if(arrayBenificiary.length!=0){
		for(var i = 0;i<arrayBenificiary.length;i++){
			strEnteredPercentage=arrayBenificiary[i]["percentage"];
			if(strEnteredPercentage==null || strEnteredPercentage==""){
				enteredPercentage = 0;
			}else{
				enteredPercentage = parseInt(strEnteredPercentage);
			}	
	        //enteredPercentage = parseInt(strEnteredPercentage);	
	        if(enteredPercentage <= 0){
	        	arrayBenificiary.splice(i,1);
	        	i--;
	        }		
		}
	}
	popupConfrmDelete.dismiss();
	invokePartyInquiryOpenAct();	
}






// Function to take percentage values from the textboxes and insert to array

function onDonePercentage(eventobject) {
    var index = eventobject["id"].replace("txtPercentage", ""); //txtPercentage1,txtPercentage2,1
    index = parseInt(index);
    var percentagetxtName = eventobject["id"];
    var percentagetxtVal = frmMBSavingsCareAddNickName[percentagetxtName].text; //100
    var percentageDispVal = percentagetxtVal;
    var enteredPercentage = 0;
    var strEnteredPercentage = "";
    var totalEnteredPercentage = 0;
    var remainingPercentage = 0;
    var fixedBenefit = 100;
    var errMsg = "";
    var validFlag = true;

    percentagetxtVal = percentagetxtVal.replace(/^[0]+/g, "");
    kony.print("GOWRI percentagetxtVal==" + percentagetxtVal);
    frmMBSavingsCareAddNickName[percentagetxtName].text = percentagetxtVal;

/*var percentSymbol = percentageDispVal.substring(percentageDispVal.length -1);
	if(percentSymbol!="%"){
		frmMBSavingsCareAddNickName[percentagetxtName].text = percentageDispVal + "%";
	}else{
		frmMBSavingsCareAddNickName[percentagetxtName].text = percentageDispVal;
	}
	//frmMBSavingsCareAddNickName[percentagetxtName].text = percentageDispVal + "%";
	percentagetxtVal = percentagetxtVal.replace("%", "");*/
/*s=strEnteredPercentage.replace(/^[0]+/g, "");
document.write("s="+s);
document.write("<br>");*/
    kony.print("Entered percentagetxtVal in onDonePercentage()" + percentagetxtVal);
    //arrayBenificiary[index]["percentage"] = percentagetxtVal;	
    for (var i = 0; i < arrayBenificiary.length; i++) {
        strEnteredPercentage = arrayBenificiary[i]["percentage"];
        if (strEnteredPercentage == "") {
            strEnteredPercentage = 0;
        }
        enteredPercentage = parseInt(strEnteredPercentage);
        totalEnteredPercentage += enteredPercentage;
    }
    kony.print("totalEnteredPercentage--->" + totalEnteredPercentage);
    remainingPercentage = fixedBenefit - totalEnteredPercentage;
    /*if (remainingPercentage < 0) {
        remainingPercentage = 0;
    }*/
    if (kony.i18n.getCurrentLocale() == "th_TH") {
        frmMBSavingsCareAddNickName.lblavailPercent.text = kony.i18n.getLocalizedString("keyAvailable") + " " + remainingPercentage + " %";
    } else {
        frmMBSavingsCareAddNickName.lblavailPercent.text = remainingPercentage + " %" + " " + kony.i18n.getLocalizedString("keyAvailable");
    }

}

function calculatePercentage() {
    var enteredPercentage = 0;
    var strEnteredPercentage = "";
    var totalEnteredPercentage = 0;
    var remainingPercentage = 0;
    var fixedBenefit = 100;

    for (var i = 0; i < arrayBenificiary.length; i++) {
        strEnteredPercentage = arrayBenificiary[i]["percentage"];
        if (strEnteredPercentage == "") {
            strEnteredPercentage = 0;
        }
        enteredPercentage = parseInt(strEnteredPercentage);
        kony.print("GOWRI enteredPercentage=" + enteredPercentage);
        totalEnteredPercentage += enteredPercentage;
    }
    kony.print("totalEnteredPercentage--->" + totalEnteredPercentage);
    remainingPercentage = fixedBenefit - totalEnteredPercentage;
    if (remainingPercentage < 0) {
        remainingPercentage = 0;
    }
    if (kony.i18n.getCurrentLocale() == "th_TH") {
        frmMBSavingsCareAddNickName.lblavailPercent.text = kony.i18n.getLocalizedString("keyAvailable") + " " + remainingPercentage + " %";
    } else {
        frmMBSavingsCareAddNickName.lblavailPercent.text = remainingPercentage + " %" + " " + kony.i18n.getLocalizedString("keyAvailable");
    }
    //frmMBSavingsCareAddNickName.lblavailPercent.text = remainingPercentage + " %" +" " + kony.i18n.getLocalizedString("keyAvailable");
}






//TO DO - Add this function onclick of next in AddBal --  Adding validations to Add Balance form()

function validateAmount() {
    var BalanceinCommo = frmMBSavingsCareAddBal.segNSSlider.selectedItems[0].lblBalance;
    var amountBalance = BalanceinCommo.replace(/,/g, "");
    amountBalance = parseFloat(amountBalance);
    kony.print("Amount in From Account balance" + amountBalance + "gblMinOpenAmt minimum amount for user---->" + gblMinOpenAmt);

    var amountKey = kony.i18n.getLocalizedString("minDepositAmt");
    amountKey = amountKey.replace("{First_Deposit_Amount}", commaFormatted(gblMinOpenAmt));

    var amount = frmMBSavingsCareAddBal.txtAmountVal.text;

    if (!isNotBlank(amount) || (parseFloat(removeCommos(amount)) < parseFloat(gblMinOpenAmt))) {
        showAlert(amountKey, kony.i18n.getLocalizedString("info"));
        return false;
    } else if (parseFloat(removeCommos(amount)) > amountBalance) {
        showAlert(kony.i18n.getLocalizedString("msg_exceedAmt"), kony.i18n.getLocalizedString("info"));
        return false;
    }
    return true;
}

// TO-DO -- Add this function on click of nickname for validating nickname and percentages

function validateBeneficiariesDetails() {
    var nickName = frmMBSavingsCareAddNickName.txtNickName.text;
    if (!NickNameValid(nickName)) {
        showAlert(kony.i18n.getLocalizedString("keyInvalidNickName"), kony.i18n.getLocalizedString("info"));
        return false;
    }
    if(gblSavingsCareFlow=="AccountDetailsFlow" || gblSavingsCareFlow=="MyAccountFlow"){
			var newNickName = frmMBSavingsCareAddNickName.txtNickName.text;
			var oldNickName="";
			kony.print("GOWRI modOpenActBeneficiaries.js newNickName="+newNickName);
			kony.print("GOWRI modOpenActBeneficiaries.js gblSavingsCareFlow="+gblSavingsCareFlow);
			if(gblSavingsCareFlow=="AccountDetailsFlow"){
				oldNickName = frmAccountDetailsMB.lblAccountNameHeader.text;
			}else{
				oldNickName = frmMyAccountView.lblNickName.text;
			}	
			kony.print("GOWRI modOpenActBeneficiaries.js oldNickName="+oldNickName);
			gblEditNickNameValue = oldNickName + " + " + newNickName;
			
			var textformat=NickNameValid(newNickName.trim());
			if(textformat== false) {
			
				TextBoxErrorSkin(frmMBSavingsCareAddNickName.txtNickName, txtErrorBG);
				showAlert(kony.i18n.getLocalizedString("keyInvalidNickName"), kony.i18n.getLocalizedString("info"));
				dismissLoadingScreen();
				kony.print("GOWRI modOpenActBeneficiaries.js FALSE1");
				return false;
			}
			kony.print("GOWRI modOpenActBeneficiaries.js gblsegID=="+gblsegID);
			if (newNickName.toLocaleLowerCase() != oldNickName.toLocaleLowerCase()) {			
				if(gblSavingsCareFlow=="AccountDetailsFlow"){
					for (var i = 0; i < gblAccountTable["custAcctRec"].length; i++) {
						kony.print("GOWRI modOpenActBeneficiaries.js gblAccountTable nickName="+gblAccountTable["custAcctRec"][i]["acctNickName"]+" accntStatus="+gblAccountTable["custAcctRec"][i]["personalisedAcctStatusCode"]);
						var nic=gblAccountTable["custAcctRec"][i]["acctNickName"];
						var actStatus=gblAccountTable["custAcctRec"][i]["personalisedAcctStatusCode"];
						if(nic!=undefined && actStatus!=undefined){
							 if (gblAccountTable["custAcctRec"][i]["personalisedAcctStatusCode"] != "02") 
							 {
							 	if (nic.toLocaleLowerCase() == newNickName.toLocaleLowerCase()) {
				                    TextBoxErrorSkin(frmMBSavingsCareAddNickName.txtNickName, txtErrorBG);
				                    showAlert(kony.i18n.getLocalizedString("Receipent_alert_correctnickname"), kony.i18n.getLocalizedString("info"));
				                    kony.print("GOWRI modOpenActBeneficiaries.js FALSE2");
				                    return false;
				                }
				             }
				         }    
					}
					
				}else{			
					kony.print("GOWRI modOpenActBeneficiaries.js gblAccntData.length=="+gblAccntData.length);
					for (var i = 0; i < gblAccntData.length; i++) {
					kony.print("GOWRI modOpenActBeneficiaries.js nickName="+gblAccntData[i].nickName+" accntStatus="+gblAccntData[i].accntStatus);
						 if (gblAccntData[i].accntStatus != "02") 
						 {
						 	if (gblAccntData[i].nickName.toLocaleLowerCase() == newNickName.toLocaleLowerCase()) {
			                    TextBoxErrorSkin(frmMBSavingsCareAddNickName.txtNickName, txtErrorBG);
			                    showAlert(kony.i18n.getLocalizedString("Receipent_alert_correctnickname"), kony.i18n.getLocalizedString("info"));
			                    kony.print("GOWRI modOpenActBeneficiaries.js FALSE2");
			                    return false;
			                }
			             }
					}
				}
				frmMBSavingsCareAddNickName.txtNickName.text = newNickName.trim();
				kony.print("GOWRI modOpenActBeneficiaries.js HERE 1 CHANGE IN NICKNAME... calling myAccountEditServiceNew");
			}
    }else if(gblSavingsCareFlow == "openAccount"){
	    	var nickNameUnique = openActNickNameUniq(frmMBSavingsCareAddNickName.txtNickName.text);
	    	if(nickNameUnique == false){
				showAlert(kony.i18n.getLocalizedString("keyuniqueNickName"), kony.i18n.getLocalizedString("info"));
				return false;
			}
    }
    if (!onNextPercentage()) {
        return false;
    }
	invokePartyInquiryOpenAct();
}
// To pass input params as strings for back end





function setOpenSavingsCareConfirmationSegdata(){
	    //kony.print("arrayBenificiary in setSegdata()" + JSON.stringify(arrayBenificiary));
	    var fName = "";
	    var lName = "";
	    var relationShip = "";
	    var percentage = "";
	    var data = [];
	    frmMBSavingsCareConfirmation.segBeneficiaryList.widgetDataMap={"lblnameVal": "lblname",
	                      "lblrelationVal":"lblrelation",
	                      "lblBenefitVal": "lblbenefit",
	                      "lblline":"lineval"};
	    kony.print("arrayBenificiary in setSegdata()" + arrayBenificiary);
	    if(arrayBenificiary.length>0){
	          for(var i = 0; i<arrayBenificiary.length; i++){
	                fName = arrayBenificiary[i]["firstName"] + " " + arrayBenificiary[i]["lastName"];
	                if (kony.i18n.getCurrentLocale() == "th_TH"){
						//var code=gblRelationDescAndCodeTH[arrayBenificiary[i]["relationCode"]];
						relationShip=gblRelationCodeAndDescTH[arrayBenificiary[i]["relationCode"]];
					}else{
						//var code=gblRelationDescAndCodeEN[arrayBenificiary[i]["relation"]];
						relationShip=gblRelationCodeAndDescEN[arrayBenificiary[i]["relationCode"]];
					}
	                //relationShip = arrayBenificiary[i]["relation"];
	                percentage = arrayBenificiary[i]["percentage"]+" %"; 
	                var benifDataTemp = {};
	                if(i == (arrayBenificiary.length - 1)){
	                       benifDataTemp = {"lblname": fName,"lblrelation":relationShip,"lblbenefit": percentage,"lineval":{isVisible:false}, template: flexBeneficRow};                 
	                }else{
	                       benifDataTemp = {"lblname": fName,"lblrelation":relationShip,"lblbenefit": percentage,"lineval":{isVisible:true}, template: flexBeneficRow}; 
	                }           
	                data.push(benifDataTemp);
	          }
	          frmMBSavingsCareConfirmation.lblBeneficiary.text=kony.i18n.getLocalizedString("keyBenificiaries")+" " + "(" + arrayBenificiary.length + ")";        
	          
	          frmMBSavingsCareConfirmation.flxbeneficiaryHeader.setVisibility(true);
	          frmMBSavingsCareConfirmation.segBeneficiaryList.setVisibility(true);
	          frmMBSavingsCareConfirmation.flexLine2.setVisibility(true);
	          frmMBSavingsCareConfirmation.richtextnonSpecifyDis.setVisibility(false);                  
	          frmMBSavingsCareConfirmation.lblname.text=removeColonFromEnd(kony.i18n.getLocalizedString("name"));
	          frmMBSavingsCareConfirmation.lblrelation.text=removeColonFromEnd(kony.i18n.getLocalizedString("keyOpenRelation"));
	          frmMBSavingsCareConfirmation.lblbenefit.text=removeColonFromEnd(kony.i18n.getLocalizedString("benefitKey"));
	          frmMBSavingsCareConfirmation.segBeneficiaryList.setData(data);
	    }else{
	          
	          frmMBSavingsCareConfirmation.richtextnonSpecifyDis.setVisibility(true);
	          frmMBSavingsCareConfirmation.lblBeneficiary.text=kony.i18n.getLocalizedString("keyBenificiaries")+" : "+kony.i18n.getLocalizedString("keynotSpecify");
	          frmMBSavingsCareConfirmation.flxbeneficiaryHeader.setVisibility(false);
	          frmMBSavingsCareConfirmation.segBeneficiaryList.setVisibility(false);
	          frmMBSavingsCareConfirmation.flexLine2.setVisibility(false);
	    }     
            
}

function preshowfrmSCConfirmation() {
    frmMBSavingsCareConfirmation.lblHdrTxt.text = kony.i18n.getLocalizedString("keyConfirmation");
    frmMBSavingsCareConfirmation.lblsubHeader.text = removeColonFromEnd(kony.i18n.getLocalizedString("savingCareActInfo"));
    frmMBSavingsCareConfirmation.btnBack.text = kony.i18n.getLocalizedString("Back");
    frmMBSavingsCareConfirmation.btnNext.text = kony.i18n.getLocalizedString("keyConfirm");
    var fromActNickName = frmMBSavingsCareAddNickName.txtNickName.text;

    //alert("branchName" + branchName);
    // var fromBranchName = frmMBSavingsCareAddBal["segNSSlider"]["selectedItems"][0].lblRemainFeeValue; // Branch Name populated from segment


    if (flow == "myAccount" || gblSavingsCareFlow == "MyAccountFlow" || gblSavingsCareFlow == "AccountDetailsFlow") {
        frmMBSavingsCareConfirmation.lblHdrTxt.centerY = "50%"
        frmMBSavingsCareConfirmation.flxAccuntName.setVisibility(false);
        frmMBSavingsCareConfirmation.imgHeader.setVisibility(false);
        frmMBSavingsCareConfirmation.flxBranch.setVisibility(false);
        frmMBSavingsCareConfirmation.flxDeductAmt.setVisibility(false);
        frmMBSavingsCareConfirmation.flxOpeningAmt.setVisibility(false);
        frmMBSavingsCareConfirmation.flxIntRate.setVisibility(false);
        frmMBSavingsCareConfirmation.flxOpeningDate.setVisibility(false);
        frmMBSavingsCareConfirmation.flxRefNo.setVisibility(false);
        frmMBSavingsCareConfirmation.lblnickname.text = removeColonFromEnd(kony.i18n.getLocalizedString("keyNickname"));
        frmMBSavingsCareConfirmation.lblnicknameVal.text = fromActNickName;
    } else {
    	frmMBSavingsCareConfirmation.flxAccuntName.setVisibility(true);
        frmMBSavingsCareConfirmation.imgHeader.setVisibility(true); 
        frmMBSavingsCareConfirmation.flxBranch.setVisibility(true);
        frmMBSavingsCareConfirmation.flxDeductAmt.setVisibility(true);
        frmMBSavingsCareConfirmation.flxOpeningAmt.setVisibility(true);
        frmMBSavingsCareConfirmation.flxIntRate.setVisibility(true);
        frmMBSavingsCareConfirmation.flxOpeningDate.setVisibility(true);
        frmMBSavingsCareConfirmation.flxRefNo.setVisibility(true);
        var fromActId = frmMBSavingsCareAddBal["segNSSlider"]["selectedItems"][0].lblActNoval;
        fromActId = fromActId.toString().replace(/-/g, "");
        var fromActName = frmMBSavingsCareAddBal["segNSSlider"]["selectedItems"][0].hiddenActName;
        var branchName = frmMBSavingsCareAddBal["segNSSlider"]["selectedItems"][0].lblRemainFeeValue;
        var accountNickName = frmMBSavingsCareAddBal["segNSSlider"]["selectedItems"][0].lblCustName;
        var actOpeningAmount = frmMBSavingsCareAddNickName.txtAmount.text;
        var availBalance = frmMBSavingsCareAddBal["segNSSlider"]["selectedItems"][0].lblBalance;
        availBalance = availBalance.replace(/,/g, "");
        var remainingBal = parseFloat(availBalance) - parseFloat(actOpeningAmount);
        kony.print("actOpeningAmount==" + actOpeningAmount + "");
        kony.print("availBalance==" + availBalance);
        kony.print("remainingBal==" + remainingBal);
        frmMBSavingsCareConfirmation.lblnickname.text = removeColonFromEnd(kony.i18n.getLocalizedString("keyNickname"));
        frmMBSavingsCareConfirmation.lblAccountname.text = removeColonFromEnd(kony.i18n.getLocalizedString("AccountName"));
        frmMBSavingsCareConfirmation.lblbranch.text = removeColonFromEnd(kony.i18n.getLocalizedString("Branch"));
        frmMBSavingsCareConfirmation.lblOpeningAmt.text = removeColonFromEnd(kony.i18n.getLocalizedString("keyLblOpnAmount"));
        frmMBSavingsCareConfirmation.lblDeductAmt.text = removeColonFromEnd(kony.i18n.getLocalizedString("deduct_amt"));
        frmMBSavingsCareConfirmation.lblIntRate.text = removeColonFromEnd(kony.i18n.getLocalizedString("Interest_rate_Annual"));
        frmMBSavingsCareConfirmation.lblOpeningDate.text = removeColonFromEnd(kony.i18n.getLocalizedString("keyLblOpeningDate"));
        frmMBSavingsCareConfirmation.lblRefNo.text = removeColonFromEnd(kony.i18n.getLocalizedString("keyTransactionRefNumber"));
        //Population of values
        frmMBSavingsCareConfirmation.lblnicknameVal.text = fromActNickName;
        frmMBSavingsCareConfirmation.lblAccountnameVal.text = fromActName;
        frmMBSavingsCareConfirmation.lblbranchVal.text = branchName;
        frmMBSavingsCareConfirmation.lblOpeningAmtVal.text = frmMBSavingsCareAddNickName.txtAmount.text;
        frmMBSavingsCareConfirmation.lblDeductAmtVal.text = accountNickName;
        frmMBSavingsCareConfirmation.lblIntRateVal.text = gblInterest;
        frmMBSavingsCareConfirmation.lblOpeningDateVal.text = gblFinActivityLogOpenAct["openingDate"];
        frmMBSavingsCareConfirmation.lblHdrTxt.centerY = "32%"
    }
    setOpenSavingsCareConfirmationSegdata();
}

function compareJSONStrings(str1,str2){
	str1=str1.replace(/\"/g, "").toLocaleLowerCase();
	str2=str2.replace(/\"/g, "").toLocaleLowerCase();
	kony.print("GOWRI str1="+str1);
	kony.print("GOWRI str2="+str2);
	if(str1==str2){
		return true;
	}else{
		return false;
	}
}


function callBeneficiaryInquiryServiceOnEdit(){
	kony.print("EDIT NickName is sucess calling Inquiry service");
	storingOldBenificiaries();
	var keyArray=["firstName","lastName"];
	sortArrayOfJSONObjects(gblArrayBenificiaryOld,keyArray,true);
	sortArrayOfJSONObjects(arrayBenificiary,keyArray,true);
	kony.print("GOWRI AFTER SORTING gblArrayBenificiaryOld="+JSON.stringify(gblArrayBenificiaryOld));
	kony.print("GOWRI AFTER SORTING arrayBenificiary="+JSON.stringify(arrayBenificiary));
	kony.print("gblOpeningMethod="+gblOpeningMethod);
	kony.print("gblNeverSetBeneficiary="+gblNeverSetBeneficiary);
	if(compareJSONStrings(JSON.stringify(gblArrayBenificiaryOld),JSON.stringify(arrayBenificiary)) && (gblNeverSetBeneficiary=="N")){
	kony.print("GOWRI NO CHANGE IN THE BENIFICIARIES");
			kony.print("GOWRI NO TRANSACTION PWD gblSavingsCareAccId="+gblSavingsCareAccId+" gblSavingsCareFlow="+gblSavingsCareFlow);
			//No change in Benifciaries..calling only Inq service no Transaction pwd.
			if(gblSavingsCareFlow=="MyAccountFlow"){	
				//callBeneficiaryInquiryService(gblSavingsCareAccId);
				frmMyAccountView.show();
			}
			else if	(gblSavingsCareFlow == "AccountDetailsFlow"){
				kony.print("GOWRI modOpenActBeneficiaries.js calling callBeneficiaryInquiryServiceOnEdit in AccountDetailsFlow");
				//callBeneficiaryInquiryService(gblSavingsCareAccId);
				frmAccountDetailsMB.show();
			}
	}else{
		//Change in Benifciaries..calling Transaction pwd.
		kony.print("GOWRI TRANSACTION PWD");
		showOTPPopup(kony.i18n.getLocalizedString("TransactionPass")+":","","",onClickConfirmPopDS,3);
	}
}

function showTransPwdforOpenSavingsCare(){
	if(gblSavingsCareFlow=="MyAccountFlow" || gblSavingsCareFlow=="AccountDetailsFlow"){
		//Editing Nickname for MyAccount and Account details.
		showfrmViewAccntSavingsCare();
	}else{
			kony.print("GOWRI TRANSACTION PWD NOT IN MYACCOUNT OR ACCOUNT DETAILS");
			showOTPPopup(kony.i18n.getLocalizedString("TransactionPass")+":","","",onClickConfirmPopDS,3);
		}
}



/*
function onClickConfirmTransPwd(){
	if (popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text==""){
		setTransPwdFailedError(kony.i18n.getLocalizedString("emptyMBTransPwd"))
		return false;
	}else{
		kony.print("inside onClickConfirmTransPwd() Success");
		var fname = "";
		var lname = "";
		var relation = ""
		var percentage = "";
		var fromActId;
		var fromActName;
		var fromActNickName;
		var fromFiident;
		var fromActType;
		var toActNum;
		var toActType;
		var toFiident;
		var dreamAmt;
		var transferAmount;
		var fromProdId;
		var bName = "";
		for(var i = 0;i<arrayBenificiary.length;i++){
			fname = fname + arrayBenificiary[i]["firstName"] + arrayBenificiary[i]["lastName"]  + " " + arrayBenificiary[i]["relation"] + " " + arrayBenificiary[i]["percentage"] + "," ;
			relation = relation + arrayBenificiary[i]["relation"]+",";
			percentage = percentage + arrayBenificiary[i]["percentage"]+",";
			bName = bName + arrayBenificiary[i]["firstName"]+ " " + arrayBenificiary[i]["lastName"] + ",";		
		}
		bName = bName.substring(bName, bName.length - 1);
		relation = relation.substring(relation, relation.length - 1);
		percentage = percentage.substring(percentage, percentage.length - 1);
		//fname = fname.substring(fname, fname.length - 1);
		
		kony.print("fnames------>" + fname);		
		var tarAmount = "5000"; // TO-DO - assign target amount 
		//tarAmount = tarAmount.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(/,/g, "");	
		fromActName = frmMBSavingsCareAddBal["segNSSlider"]["selectedItems"][0].hiddenActName; 
		fromActId = frmMBSavingsCareAddBal["segNSSlider"]["selectedItems"][0].lblActNoval;
		fromActId = fromActId.toString().replace(/-/g, "");
		fromActNickName = frmMBSavingsCareAddBal["segNSSlider"]["selectedItems"][0].lblCustName;
		fromFiident = frmMBSavingsCareAddBal["segNSSlider"]["selectedItems"][0].hiddenFiident;
		fromActType = frmMBSavingsCareAddBal["segNSSlider"]["selectedItems"][0].hiddenActType;
		fromProdId = frmMBSavingsCareAddBal["segNSSlider"]["selectedItems"][0].hiddenProductId;
		kony.print("fromActName-->" + fromActName + "fromActId--->" + fromActId + "fromActNickName---->" + fromActNickName + "fromFiident--->" + fromFiident + "fromActType--->" + fromActType + "fromProdId--->" + fromProdId);
		var inputParam = {};
		var otpPwd = popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text;
		inputParam["password"] = otpPwd;
        // TOKEN
        inputParam["TokenSwitchFlag"] = gblTokenSwitchFlag;
        inputParam["verifyToken_loginModuleId"] = "IB_HWTKN";
        inputParam["verifyToken_userStoreId"] = "DefaultStore";
        inputParam["verifyToken_retryCounterVerifyAccessPin"] = "0";
        inputParam["verifyToken_retryCounterVerifyTransPwd"] = "0";
        inputParam["verifyToken_userId"] = gblUserName;
        
        //inputParam["verifyToken_password"] = Pwd;
        inputParam["verifyToken_sessionVal"] = "";
        inputParam["verifyToken_segmentId"] = "segmentId";
        inputParam["verifyToken_segmentIdVal"] = "MIB";
        inputParam["verifyToken_channel"] = "Internet Banking";
        
        //OTP
        //inputParam["verifyPwd_TokenSwitchFlag"] = gblTokenSwitchFlag;
        inputParam["verifyPwd_retryCounterVerifyOTP"] = "0",
        inputParam["verifyPwd_userId"] = gblUserName,
        inputParam["verifyPwd_userStoreId"] = "DefaultStore",
        inputParam["verifyPwd_loginModuleId"] = "IBSMSOTP",
        //inputParam["verifyPwd_password"] = Pwd,
        inputParam["verifyPwd_segmentId"] = "MIB"
        //Mobile Pwd 
        inputParam["verifyPwdMB_loginModuleId"] = "MB_TxPwd";
        inputParam["verifyPwdMB_retryCounterVerifyAccessPin"] = "0";
        inputParam["verifyPwdMB_retryCounterVerifyTransPwd"] = "0";
        //inputParam["verifyPwdMB_password"] = Pwd;

       //DepositAOXFER
	   var productCode = gblAccountTable["SAVING_CARE_PRODUCT_CODES"].indexOf(gblSelOpenActProdCode);
       inputParam["productCode"] = gblSelOpenActProdCode;// TO-DO - read from product table
       inputParam["isDisToActTD"] = "";       
       inputParam["fromDepositacctVal"] = "";
       inputParam["fromactFiidentvalue"] = fromFiident;
       inputParam["fromDepositActType"] = fromActType;      
       inputParam["transferAmount"] = frmMBSavingsCareAddNickName.txtAmount.text; // TO-DO - Check with Shireesh
       inputParam["fromProdId"] = fromProdId;
       inputParam["depositAoxfer_miscDate"] = gblPartyInqOARes["birthDate"];
		for (var i = 0; i < gblPartyInqOARes["Persondata"].length; i++) {
			if (gblPartyInqOARes["Persondata"][i]["AddrType"] != null && gblPartyInqOARes["Persondata"][i]["AddrType"] != "") 
			{
				if (gblPartyInqOARes["Persondata"][i]["AddrType"] == "Primary"){
				inputParam["depositAoxfer_addrType"] = gblPartyInqOARes["Persondata"][i]["AddrType"];
				inputParam["depositAoxfer_addr1"] = gblPartyInqOARes["Persondata"][i]["addr1"];
				inputParam["depositAoxfer_addr2"] = gblPartyInqOARes["Persondata"][i]["addr2"];
				inputParam["depositAoxfer_addr3"] = gblPartyInqOARes["Persondata"][i]["addr3"];
				inputParam["depositAoxfer_city"] = gblPartyInqOARes["Persondata"][i]["City"];
				if(gblPartyInqOARes["Persondata"][i]["PostalCode"] != "" && gblPartyInqOARes["Persondata"][i]["PostalCode"] != null && gblPartyInqOARes["Persondata"][i]["PostalCode"] != undefined){
				inputParam["depositAoxfer_postalCode"] = gblPartyInqOARes["Persondata"][i]["PostalCode"];
				}else{
				inputParam["depositAoxfer_postalCode"] = "";
				}
				inputParam["depositAoxfer_cntryCodeVal"] = "TH"//gblPartyInqOARes["Persondata"][i]["countryCode"];
				break;
				}
				
				if (gblPartyInqOARes["Persondata"][i]["AddrType"] == "Registered"){
				inputParam["depositAoxfer_addrType"] = gblPartyInqOARes["Persondata"][i]["AddrType"];
				inputParam["depositAoxfer_addr1"] = gblPartyInqOARes["Persondata"][i]["addr1"];
				inputParam["depositAoxfer_addr2"] = gblPartyInqOARes["Persondata"][i]["addr2"];
				inputParam["depositAoxfer_addr3"] = gblPartyInqOARes["Persondata"][i]["addr3"];
				inputParam["depositAoxfer_city"] = gblPartyInqOARes["Persondata"][i]["City"];
				if(gblPartyInqOARes["Persondata"][i]["PostalCode"] != "" && gblPartyInqOARes["Persondata"][i]["PostalCode"] != null && gblPartyInqOARes["Persondata"][i]["PostalCode"] != undefined){
				inputParam["depositAoxfer_postalCode"] = gblPartyInqOARes["Persondata"][i]["PostalCode"];
				}else{
				inputParam["depositAoxfer_postalCode"] = "";
				}
				inputParam["depositAoxfer_cntryCodeVal"] ="TH"// gblPartyInqOARes["Persondata"][i]["countryCode"];
				break;
				}
			
			}
		}
        if (gblPartyInqOARes["issuedIdentType"] == "CI" ){
			inputParam["acctTitle"] = gblPartyInqOARes["FullName"];
		}else{
			inputParam["acctTitle"] = gblPartyInqOARes["PrefName"];
		}
		
		inputParam["activityTypeID"] = "081";
		inputParam["activityFlexValues1"] = gblSelOpenActProdCode; // + "+" + frmOpenActSavingCareCnfNAck.lblOASCTitle.text;
		inputParam["activityFlexValues3"] = frmMBSavingsCareAddNickName.txtNickName.text; // To-DO - Assign nick name to this param
		inputParam["activityFlexValues4"] = frmMBSavingsCareAddNickName.txtAmount.text; // To-DO - Assign nick name to this param
		inputParam["activityFlexValues5"] = fname;
		
        //Customer beneficiaries
		inputParam["beneficiaryfullNames"] = bName;
		inputParam["beneficiarypercentage"] = percentage;
		inputParam["beneficiaryrelationship"] = relation;
		kony.print("inputParam in onClickConfirmTransPwd()--------->" + JSON.stringify(inputParam))
		invokeServiceSecureAsync("openActConfirmCompositeService", inputParam, validateOpenAccountSavingsCareServiceCallback);
	}
} 

function validateOpenAccountSavingsCareServiceCallback(status, callBackResponse){
	
	
}
**/


// function to save the values into session as per shireesh

function callSaveOpenAccounts() {

    //validatePercentage();
    var inputparam = {};
    var fromActId;
    fromActId = frmMBSavingsCareAddBal["segNSSlider"]["selectedItems"][0].lblActNoval;
    fromActId = fromActId.toString().replace(/-/g, "");
    inputparam["productCode"] = gblSelOpenActProdCode;
    inputparam["amount"] = "5000";
    inputparam["userAccountNum"] = fromActId;
    inputparam["ProductName"] = gblSelProduct;
    inputparam["openAccType"] = "SDA";
    kony.print("inputparam---->" + JSON.stringify(inputparam));
    var fname = "";
    var relation = "";
    var percentage = "";
    var bName = "";
    for (var i = 0; i < arrayBenificiary.length; i++) {
        fname = fname + arrayBenificiary[i]["firstName"] + arrayBenificiary[i]["lastName"] + " " + arrayBenificiary[i]["relation"] + " " + arrayBenificiary[i]["percentage"] + ",";
        relation = relation + arrayBenificiary[i]["relation"] + ",";
        percentage = percentage + arrayBenificiary[i]["percentage"] + ",";
        bName = bName + arrayBenificiary[i]["firstName"] + " " + arrayBenificiary[i]["lastName"] + ",";
    }
    bName = bName.substring(bName, bName.length - 1);
    relation = relation.substring(relation, relation.length - 1);
    percentage = percentage.substring(percentage, percentage.length - 1);
    //fname = fname.substring(fname, fname.length - 1);

    kony.print("fnames------>" + fname);

    inputparam["activityTypeID"] = "081";
    inputparam["activityFlexValues1"] = gblSelOpenActProdCode; // TO-DO - check later on product code
    inputparam["activityFlexValues3"] = "MyNickName";
    inputparam["activityFlexValues4"] = "5000";
    inputparam["activityFlexValues5"] = fname;
    showLoadingScreen();
    invokeServiceSecureAsync("SaveOpenAccountTxn", inputparam, SaveOpenAccountSavingscareSessionCallBack);
}

function SaveOpenAccountSavingscareSessionCallBack(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == 0) {
            dismissLoadingScreen();
            showOpenSavingscareConfirm();
        }
    }
}

function showOpenSavingscareConfirm() {
    kony.print("GOWRI 44444444444");
    frmMBSavingsCareConfirmation.show();
}


// method written to populate confirmation page details




function frmMBSavingsCareAddBalPreShow() {
	changeStatusBarColor();
    if (isNickNameForm) {
        frmMBSavingsCareAddBal.txtAmountVal.text = commaFormatted(parseFloat(removeCommos(frmMBSavingsCareAddNickName.txtAmount.text)).toFixed(2));
        amountValueFromNickName = commaFormatted(parseFloat(removeCommos(frmMBSavingsCareAddNickName.txtAmount.text)).toFixed(2));
        kony.print("amountValueFromNickName in frmMBSavingsCareAddBalPreShow" + amountValueFromNickName);
        backToAddNickName();
        saveToAddNickName();
        isNickNameForm = false;
    } else {
        if (isNotBlank(gblMinOpenAmt)) {
            frmMBSavingsCareAddBal.txtAmountVal.text = commaFormatted(parseFloat(removeCommos(gblMinOpenAmt)).toFixed(2)); //5000.00
        } else {
            kony.print("Logging -- Minimum amount is empty check it");
        }
        amountValueFromNickName = "";
        frmMBSavingsCareAddBal.btnOATDConfCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
        frmMBSavingsCareAddBal.btnPopUpTermination.text = kony.i18n.getLocalizedString("Next");
        frmMBSavingsCareAddBal.btnPopUpTermination.onClick = showBeneficiariesForm;
        frmMBSavingsCareAddBal.btnOATDConfCancel.onClick = OnClickOpenMore;

    }
    frmMBSavingsCareAddBal.lblAmount.text = removeColonFromEnd(kony.i18n.getLocalizedString("amount"));

    var amountKeyDesc = kony.i18n.getLocalizedString("firstDepositDesc");
    amountKeyDesc = amountKeyDesc.replace("{First_Deposit_Amount}", commaFormatted(gblMinOpenAmt));
    frmMBSavingsCareAddBal.lblAmountDesc.text = amountKeyDesc;

    frmMBSavingsCareAddBal.lblHeadertxt.text = removeColonFromEnd(kony.i18n.getLocalizedString("keyLblOpnAmount"));
}

function navigateToAddBal() {
    isNickNameForm = true;
    frmMBSavingsCareAddBal.segNSSlider.selectedIndex = gblSelectedIndexSc;
    frmMBSavingsCareAddBal.show();
    //setSavingCareAddAccountData();
}

function onClickEditBenifBack() {
    frmMBSavingsCareAddNickName.show();
}

function OnClickOpenMore() {
    openMoreSave = true;
    OnClickOpenNewAccount();
}

function showMBOpenSavingscareComplete() {
                var fName = "";
                var lName = "";
                var relationShip = "";
                var percentage = "";
                var masterdata = [];
                if (arrayBenificiary.length > 0) {
                                frmMBSavingsCareComplete.segBeneficiaryList.widgetDataMap = {
                                                "lblnameVal": "lblname",
                                                "lblrelationVal": "lblrelation",
                                                "lblBenefitVal": "lblbenefit",
                                                "lblline": "lineval"
                                };
                                kony.print("arrayBenificiary in setSegdatacompletescreen setSegdata()" + JSON.stringify(arrayBenificiary));
                                for (var i = 0; i < arrayBenificiary.length; i++) {
                                                fName = arrayBenificiary[i]["firstName"] + " " + arrayBenificiary[i]["lastName"];
                                                if (kony.i18n.getCurrentLocale() == "th_TH") {
                                                                relationShip = gblRelationCodeAndDescTH[arrayBenificiary[i]["relationCode"]];
                                                } else {
                                                                relationShip = gblRelationCodeAndDescEN[arrayBenificiary[i]["relationCode"]];
                                                }
                                                //relationShip = arrayBenificiary[i]["relation"];
                                                percentage = arrayBenificiary[i]["percentage"] + " %";
                                                var benifDataTemp = {};
                                                if (i == (arrayBenificiary.length - 1)) {
                                                                benifDataTemp = {
                                                                                "lblname": fName,
                                                                                "lblrelation": relationShip,
                                                                                "lblbenefit": percentage,
                                                                                "lineval": {
                                                                                                isVisible: false
                                                                                },
                                                                                template: flexBeneficRow
                                                                };
                                                } else {
                                                                benifDataTemp = {
                                                                                "lblname": fName,
                                                                                "lblrelation": relationShip,
                                                                                "lblbenefit": percentage,
                                                                                "lineval": {
                                                                                                isVisible: true
                                                                                },
                                                                                template: flexBeneficRow
                                                                };
                                                }
                                                masterdata.push(benifDataTemp);
                                }
                                kony.print("data-->" + JSON.stringify(masterdata));

                                frmMBSavingsCareComplete.segBeneficiaryList.setVisibility(true);
                                frmMBSavingsCareComplete.flexLine2.setVisibility(true);
                                frmMBSavingsCareComplete.richtextnonSpecifyDis.setVisibility(false);
                                frmMBSavingsCareComplete.lblBeneficiary.text = kony.i18n.getLocalizedString("keyBenificiaries") + " " + "(" + arrayBenificiary.length + ")";
                                frmMBSavingsCareComplete.segBeneficiaryList.setData(masterdata);
                }else {
                                frmMBSavingsCareComplete.richtextnonSpecifyDis.setVisibility(true);
                                frmMBSavingsCareComplete.flexLine2.setVisibility(false);
                                frmMBSavingsCareComplete.lblBeneficiary.text = kony.i18n.getLocalizedString("keyBenificiaries") + " : " + kony.i18n.getLocalizedString("keynotSpecify");
                                frmMBSavingsCareComplete.segBeneficiaryList.setVisibility(false);
                                frmMBSavingsCareComplete.richtextnonSpecifyDis.text = kony.i18n.getLocalizedString("benefinotSpecifyDesc");

                }
                
                            frmMBSavingsCareComplete.lblHdrTxt.text = kony.i18n.getLocalizedString("MIB_P2PCompleteTitle");
                                frmMBSavingsCareComplete.lblsubHeader.text = removeColonFromEnd(kony.i18n.getLocalizedString("savingCareActInfo"));
                                frmMBSavingsCareComplete.btnBack.text = kony.i18n.getLocalizedString("TRComplete_Btn_Return");
                                frmMBSavingsCareComplete.btnNext.text = kony.i18n.getLocalizedString("btnOpenMore");
                                frmMBSavingsCareComplete.btnBack.onClick = showAccountSummaryFromMenu;
                                frmMBSavingsCareComplete.btnNext.onClick = OnClickOpenMore;
                                if(gblOpenAccountFail){
                                    frmMBSavingsCareComplete.lblWelcomeText.text=""; 
                                    frmMBSavingsCareComplete.richtextnotificationDesc.text = kony.i18n.getLocalizedString("keyCanotTransferOpen");          	
                                }else{
                                	frmMBSavingsCareComplete.lblWelcomeText.text=kony.i18n.getLocalizedString("keySavingcareWelcome"); 
                                	frmMBSavingsCareComplete.richtextnotificationDesc.text = kony.i18n.getLocalizedString("keyOpenNotifyOneNew");                                               
                                }
                                frmMBSavingsCareComplete.lblCompleteNotification.text = kony.i18n.getLocalizedString("keyOpenCompNotifi");
                                frmMBSavingsCareComplete.lblnickname.text = removeColonFromEnd(kony.i18n.getLocalizedString("keyNickname"));
                                frmMBSavingsCareComplete.lblnicknameVal.text = frmMBSavingsCareConfirmation.lblnicknameVal.text;
                                //frmMBSavingsCareComplete.richtextnonSpecifyDis.setVisibility(false);
                                if (flow == "myAccount") {
                                                frmMBSavingsCareComplete.flxAccuntName.setVisibility(false);
                                                frmMBSavingsCareComplete.flxBranch.setVisibility(false);
                                                frmMBSavingsCareComplete.flxDeductAmt.setVisibility(false);
                                                frmMBSavingsCareComplete.flxOpeningAmt.setVisibility(false);
                                                frmMBSavingsCareComplete.flxIntRate.setVisibility(false);
                                                frmMBSavingsCareComplete.flxOpeningDate.setVisibility(false);
                                                frmMBSavingsCareComplete.flxRefNo.setVisibility(false);
                                                frmMBSavingsCareComplete.flxAccountNum.setVisibility(false);
                                                //frmMBSavingsCareComplete.lblnickname.text = removeColonFromEnd(kony.i18n.getLocalizedString("keyNickname"));
                                                //frmMBSavingsCareComplete.lblnicknameVal.text = frmMBSavingsCareConfirmation.lblnicknameVal.text;
                                } else {
                                                //frmMBSavingsCareComplete.lblnickname.text = removeColonFromEnd(kony.i18n.getLocalizedString("keyNickname"));
                                                frmMBSavingsCareComplete.lblAccountname.text = removeColonFromEnd(kony.i18n.getLocalizedString("AccountName"));
                                                frmMBSavingsCareComplete.lblAccountNum.text=removeColonFromEnd(kony.i18n.getLocalizedString("AccNo"));
                                                frmMBSavingsCareComplete.lblbranch.text = removeColonFromEnd(kony.i18n.getLocalizedString("Branch"));
                                                frmMBSavingsCareComplete.lblOpeningAmt.text = removeColonFromEnd(kony.i18n.getLocalizedString("keyLblOpnAmount"));
                                                frmMBSavingsCareComplete.lblDeductAmt.text = removeColonFromEnd(kony.i18n.getLocalizedString("deduct_amt"));
                                                frmMBSavingsCareComplete.lblIntRate.text = removeColonFromEnd(kony.i18n.getLocalizedString("Interest_rate_Annual"));
                                                frmMBSavingsCareComplete.lblOpeningDate.text = removeColonFromEnd(kony.i18n.getLocalizedString("keyLblOpeningDate"));
                                                frmMBSavingsCareComplete.lblRefNo.text = removeColonFromEnd(kony.i18n.getLocalizedString("keyTransactionRefNumber"));
                                                //Populating Values
                                                //frmMBSavingsCareComplete.lblnicknameVal.text = frmMBSavingsCareConfirmation.lblnicknameVal.text;
                                                frmMBSavingsCareComplete.lblAccountnameVal.text = frmMBSavingsCareConfirmation.lblAccountnameVal.text;
                                                var fromActId = frmMBSavingsCareAddBal["segNSSlider"]["selectedItems"][0].lblActNoval;
        										//fromActId = fromActId.toString().replace(/-/g, "");
                                                //frmMBSavingsCareComplete.lblAccountNumVal.text=fromActId;
                                                frmMBSavingsCareComplete.lblbranchVal.text = frmMBSavingsCareConfirmation.lblbranchVal.text;
                                                //frmMBSavingsCareComplete.lblOpeningAmtVal.text = frmMBSavingsCareConfirmation.lblOpeningAmtVal.text;
                                                frmMBSavingsCareComplete.lblDeductAmtVal.text = frmMBSavingsCareConfirmation.lblDeductAmtVal.text;
                                                frmMBSavingsCareComplete.lblIntRateVal.text = frmMBSavingsCareConfirmation.lblIntRateVal.text;
                                                frmMBSavingsCareComplete.lblOpeningDateVal.text = frmMBSavingsCareConfirmation.lblOpeningDateVal.text;
                                                frmMBSavingsCareComplete.lblRefNoVal.text = frmMBSavingsCareConfirmation.lblRefNoVal.text;
                                                

                } 
}





function onDoneNickName() {
    gblOpenSavingsCareNickName = "";
    gblOpenSavingsCareNickName = frmMBSavingsCareAddNickName.txtNickName.text;
    kony.print("gblOpenSavingsCareNickName" + gblOpenSavingsCareNickName);
    if(undefined!=frmMBSavingsCareAddNickName.flexlastline){
		frmMBSavingsCareAddNickName.flexlastline.setFocus(true);
	}
}

function onDoneTextNickName() {
    gblOpenSavingsCareNickName = "";
    gblOpenSavingsCareNickName = frmMBSavingsCareAddNickName.txtNickName.text;
}


function deleteBenifRecord() {
    kony.print("deleteIndex Before splice" + deleteIndex);
    arrayBenificiary.splice(deleteIndex, 1);
    kony.print("After splice deleteBenifRecord" + arrayBenificiary);
    gblSwipedBenificiary = "";
    popupConfrmDelete.dismiss();
    showLoadingScreen();
    postshowSavingsCare();
}



function getBalanceIndex() {}
getBalanceIndex.prototype.paginationSwitch = function(sectionIndex, rowIndex) {
    var segdata = frmMBSavingsCareAddBal.segNSSlider.data;
    rowIndex = parseFloat(rowIndex);
    frmMBSavingsCareAddBal.segNSSlider.selectedIndex = [0, rowIndex];
}



function callDummy() {

}

function onDoneAmount() {
    var amountVal = frmMBSavingsCareAddBal.txtAmountVal.text;
    if (isNotBlank(amountVal)) {
        amountVal = kony.string.replace(amountVal, ",", "");
        frmMBSavingsCareAddBal.txtAmountVal.text = commaFormatted(parseFloat(removeCommos(amountVal)).toFixed(2));

    }
}

function onTextChangeAmount() {
    var amountVal = frmMBSavingsCareAddBal.txtAmountVal.text;
    if (isNotBlank(amountVal)) {
        amountVal = kony.string.replace(amountVal, ",", "");
        frmMBSavingsCareAddBal.txtAmountVal.text = commaFormattedTransfer(amountVal);
    }
}



function backToAddNickName() {
    frmMBSavingsCareAddNickName.txtAmount.text = amountValueFromNickName;
    frmMBSavingsCareAddBal.btnOATDConfCancel.text = kony.i18n.getLocalizedString("Back");
    frmMBSavingsCareAddBal.btnOATDConfCancel.onClick = goToAddNickNameForm;
}

function goToAddNickNameForm() {
    isBacktoNickName = true;
    gblFromNickNameBack = false;
    gblSelectedIndexSc = frmMBSavingsCareAddBal.segNSSlider.selectedIndex;
    frmMBSavingsCareAddNickName.show();
}

function saveToAddNickName() {
    amountValueFromNickName = "";
    isBacktoNickName = false;
    //frmMBSavingsCareAddNickName.txtAmount.text = commaFormatted(parseFloat(removeCommos(frmMBSavingsCareAddBal.txtAmountVal.text)).toFixed(2));
    frmMBSavingsCareAddBal.btnPopUpTermination.text = kony.i18n.getLocalizedString("keysave");
    frmMBSavingsCareAddBal.btnPopUpTermination.onClick = saveNewAmountToBeneficiaries;
}

function saveNewAmountToBeneficiaries() {
    if (undefined != gblAccountTable["SAVING_CARE_MAX_BENEFICIARIES"]) {
        gblMaxBeneficiaryConfig = parseInt(gblAccountTable["SAVING_CARE_MAX_BENEFICIARIES"]);
    } else {
        gblMaxBeneficiaryConfig = 0;
    }
    if (!validateAmount()) {
        return false;
    } else {
        kony.print("Validations success navigate to beneficiaries form");
        amountValueFromNickName = "";
        frmMBSavingsCareAddNickName.txtAmount.text = commaFormatted(parseFloat(removeCommos(frmMBSavingsCareAddBal.txtAmountVal.text)).toFixed(2));
        gblSelectedIndexSc = frmMBSavingsCareAddBal.segNSSlider.selectedIndex;
        frmMBSavingsCareAddNickName.show();
    }
}

function displaySelectedFromAct(){
	if(gblSavingsCareFlow == "openAccount"){
		frmMBSavingsCareAddBal.segNSSlider.selectedIndex = gblSelectedIndexSc;
	}
}


