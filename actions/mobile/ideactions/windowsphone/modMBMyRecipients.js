var isChMyRecipientsRs = true;
var gblRcPersonalizedIdList = "";
var gblRcImageUrlList = [];
var gblRcBase64List = "";
var gblRcBase64ListFromContacts = "";
var gblRcImageUrlConstant = "";
var gblUserLockStatusMB = "";
// global variables specific to the module variables 
var transPwdCount = 0;
var dontChangeFBID = null;
var currentRecipient = null;
var currentRecipientDetails = {
    crmId: "",
    name: "",
    rcId: "",
    mobile: "",
    email: "",
    facebook: "",
    fav: "",
    pic: "",
    accNo: ""
};
var crmId = " ";
var gblSelectedBankAccLength = null;
var isRecipientNew = false;
var currentAccount = null;
var originalPh = "";
var formattedPh = null;
var contactListToggle = false;
var contactListToggle1 = false;
var previousSelectedIndex = false; //used in contact form to track previous
var previousSelectedItems = false;
var fl = null; //to check if previousSelectedIndex is set in code
function getMBMyRecepientBankList() {
    var inputParams = {};
    showLoadingScreen();
    invokeServiceSecureAsync("receipentgetBankService", inputParams, callBackSetBackListMB)
    frmMyRecipientAddAcc.txtAccNo.text = "";
    frmMyRecipientAddAcc.txtNickname.text = "";
    frmMyRecipientAddAcc.tbxAccName.text = "";
}

function callBackSetBackListMB(status, resultTable) {
    var masterData = [];
    globalSelectBankData = [];
    var tempDataDefault = {
        lblBanklist: "Bank",
        code: "Bank"
    };
    //globalSelectBankData.push(tempData);
    //	masterData.push(tempDataDefault);
    if (status == 400) {
        if (resultTable["opstatus"] == 0) {
            var collectionData = resultTable["Results"];
            var len = null,
                cutlen = null,
                x = "",
                srtr = "";
            for (var i = 0; i < collectionData.length; i++) {
                var bankName = "";
                if (collectionData[i].bankStatus.toUpperCase() == "ACTIVE") {
                    if (kony.i18n.getCurrentLocale() == 'en_US') {
                        bankName = collectionData[i]["bankNameEng"];
                    } else {
                        bankName = collectionData[i]["bankNameThai"];
                    }
                    len = bankName.length;
                    if (len > 37) {
                        cutlen = len - 37;
                        x = bankName.substr(len - cutlen);
                        srtr = bankName.replace(x, "..");
                    } else {
                        srtr = bankName;
                    }
                    if (collectionData[i].smartFlag.toUpperCase() == "Y" || collectionData[i].orftFlag.toUpperCase() == "Y") {
                        var tempData = {
                            code: collectionData[i]["bankCode"],
                            lblBanklist: srtr,
                            //lblBanklist: collectionData[i]["bankNameEng"],
                            bankAccntLen: collectionData[i]["bankAccntLen"],
                            bankAccntLengths: collectionData[i]["bankAccntLengths"]
                        };
                        masterData.push(tempData);
                    }
                    var tempRecord;
                    if (locale == "en_US") {
                        tempRecord = [collectionData[i]["bankCode"], collectionData[i]["bankNameEng"], collectionData[i]["bankShortName"], collectionData[i]["orftFlag"],
                            collectionData[i]["smartFlag"], collectionData[i]["bankAccntLen"], collectionData[i]["bankAccntLengths"], collectionData[i]["bankNameEng"], collectionData[i]["bankNameThai"]
                        ];
                    } else {
                        tempRecord = [collectionData[i]["bankCode"], collectionData[i]["bankNameThai"], collectionData[i]["bankShortName"], collectionData[i]["orftFlag"],
                            collectionData[i]["smartFlag"], collectionData[i]["bankAccntLen"], collectionData[i]["bankAccntLengths"], collectionData[i]["bankNameEng"], collectionData[i]["bankNameThai"]
                        ];
                    }
                    globalSelectBankData.push(tempRecord);
                }
            }
            popRecipientBankList.segBanklist.setData(masterData);
            popRecipientBankList.show();
        } else {
            popRecipientBankList.segBanklist.setData(tempDataDefault);
            showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_Error"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
        }
    } else {
        if (status == 300) {
            showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
        }
    }
    kony.application.dismissLoadingScreen();
}

function getMBMyRecepientBankListCache() {
    var inputParams = {};
    invokeServiceSecureAsync("receipentgetBankService", inputParams, callBackSetBackListMBCache);
}

function callBackSetBackListMBCache(status, resultTable) {
    if (status == 400) {
        if (resultTable["opstatus"] == 0) {
            globalSelectBankData = [];
            var collectionData = resultTable["Results"];
            for (var i = 0; i < resultTable["Results"].length; i++) {
                var tempData;
                if (locale == "en_US") {
                    tempData = [collectionData[i]["bankCode"], collectionData[i]["bankNameEng"], collectionData[i]["bankShortName"], collectionData[i]["orftFlag"],
                        collectionData[i]["smartFlag"], collectionData[i]["bankAccntLen"], collectionData[i]["bankAccntLengths"], collectionData[i]["bankNameEng"], collectionData[i]["bankNameThai"]
                    ];
                } else {
                    tempData = [collectionData[i]["bankCode"], collectionData[i]["bankNameThai"], collectionData[i]["bankShortName"], collectionData[i]["orftFlag"],
                        collectionData[i]["smartFlag"], collectionData[i]["bankAccntLen"], collectionData[i]["bankAccntLengths"], collectionData[i]["bankNameEng"], collectionData[i]["bankNameThai"]
                    ];
                }
                globalSelectBankData.push(tempData);
            }
            //getUserLockStatus();
        } else {
            //Error
        }
    } else {
        //Error"
    }
}

function popupBankListSelect() {
    frmMyRecipientAddAcc.btnBanklist.text = popRecipientBankList.segBanklist.selectedItems[0].lblBanklist;
    gblSelectedBankAccLength = popRecipientBankList.segBanklist.selectedItems[0].bankAccntLen;
    MAX_ACC_LEN_LIST = popRecipientBankList.segBanklist.selectedItems[0].bankAccntLengths;
    gblSelectedBankCode = popRecipientBankList.segBanklist.selectedItems[0].code;
    frmMyRecipientAddAcc.txtAccNo.maxTextLength = (parseInt(gblSelectedBankAccLength));
    if (!(getORFTFlagIB(gblSelectedBankCode) == "Y")) {
        frmMyRecipientAddAcc.hbxAccName.setVisibility(true);
        frmMyRecipientAddAcc.line363582120275411.setVisibility(true);
    } else {
        frmMyRecipientAddAcc.hbxAccName.setVisibility(false);
        frmMyRecipientAddAcc.line363582120275411.setVisibility(false);
    }
    popRecipientBankList.dismiss();
}

function accLengthValidation() {
    var len = null;
    if (parseInt(gblSelectedBankAccLength) <= 3) {
        len = 0;
    }
    if (parseInt(gblSelectedBankAccLength) == 4) {
        len = 1;
    }
    if (parseInt(gblSelectedBankAccLength) > 4 && parseInt(gblSelectedBankAccLength) < 10) {
        len = 2;
    }
    if (parseInt(gblSelectedBankAccLength) >= 10) {
        len = 3;
    }
    if (frmMyRecipientAddAcc.txtAccNo.text.length == (parseInt(gblSelectedBankAccLength) + len)) {
        frmMyRecipientAddAcc.txtAccNo.setFocus(false);
        //frmMyRecipientAddAcc.txtNickname.setFocus(true);
    }
    frmMyRecipientAddAcc.txtAccNo.maxTextLength = (parseInt(gblSelectedBankAccLength) + len);
}
var charsArr = ["="];

function addNewAccountForReceipent() {
    var accNoFlg, accNckNmeFlg;
    var bank = frmMyRecipientAddAcc.btnBanklist.text;
    var accNickName = frmMyRecipientAddAcc.txtNickname.text;
    var accNumber = frmMyRecipientAddAcc.txtAccNo.text;
    var accNumberSpa = stripDashAcc(accNumber);
    var x = MAX_ACC_LEN_LIST;
    var y = x.split(",");
    var z = parseInt(y[0]);
    if (accNumber.trim() == "" || MAX_ACC_LEN_LIST.indexOf(stripDashAcc(accNumber).length) < 0 || stripDashAcc(accNumber).length < z) {
        accNoFlg = false;
    } else {
        accNoFlg = true;
    }
    if (frmMyRecipientAddAcc.btnBanklist.text == kony.i18n.getLocalizedString("keyBank")) {
        showAlertRcMB(kony.i18n.getLocalizedString("keySelectBank"), kony.i18n.getLocalizedString("info"), "info")
        return false;
    }
    if (stripDashAcc(accNumber).trim().length == 0) {
        TextBoxErrorSkin(frmMyRecipientAddAcc.txtAccNo, txtErrorBG);
        showAlertRcMB(kony.i18n.getLocalizedString("keyErrAccntNoLen"), kony.i18n.getLocalizedString("info"), "info");
        return false;
    }
    if (accNickName.trim() == "") {
        TextBoxErrorSkin(frmMyRecipientAddAcc.txtNickname, txtErrorBG);
        showAlertRcMB(kony.i18n.getLocalizedString("keyMandRecAcctNickname"), kony.i18n.getLocalizedString("info"), "info")
        return;
    }
    if (ThaiOrEnglish(accNickName)) {
        if (kony.string.containsChars(accNickName, charsArr)) {
            TextBoxErrorSkin(frmMyRecipientAddAcc.txtNickname, txtErrorBG);
            showAlertRcMB(kony.i18n.getLocalizedString("keyInvalidNickName"), kony.i18n.getLocalizedString("info"), "info")
            accNckNmeFlg = false; //added for optimizing the code reg DEF1171
            return false;
        }
        accNckNmeFlg = true;
    } else {
        //var accountNicknameRgx = /^[0-9A-Za-z'\s]{1,20}$/ig;
        //accNckNmeFlg = true;//accountNicknameRgx.test(accNickName);
        //if(kony.string.containsChars(accNickName, charsArr))
        //accNckNmeFlg = false;
        //-------------cheking for the validation of nick name hile adding account DEF1171-----------------
        showAlertRcMB(kony.i18n.getLocalizedString("keyInvalidNickName"), kony.i18n.getLocalizedString("info"), "info")
        return false;
    }
    if (flowSpa) {
        if (!(kony.string.isNumeric(accNumberSpa))) {
            showAlertRcMB(kony.i18n.getLocalizedString("keyErrAccntNoInvalid"), kony.i18n.getLocalizedString("info"), "info")
            return false;
        }
    }
    if (!(accNoFlg && accNckNmeFlg)) {
        if (accNoFlg == false) {
            frmMyRecipientAddAcc.txtAccNo.skin = txtErrorBG;
            showAlertRcMB(kony.i18n.getLocalizedString("keyErrAccntNoInvalid"), kony.i18n.getLocalizedString("info"), "info");
        } else {
            if (accNckNmeFlg == false) {
                frmMyRecipientAddAcc.txtNickname.skin = txtErrorBG;
                showAlertRcMB(kony.i18n.getLocalizedString("keyInvalidNickName"), kony.i18n.getLocalizedString("info"), "info");
            }
        }
        return false;
    }
    if (hasWhiteSpace(accNumber)) {
        showAlertRcMB(kony.i18n.getLocalizedString("keyErrAccntNoInvalid"), kony.i18n.getLocalizedString("info"), "info");
        TextBoxErrorSkin(frmMyRecipientAddAcc.txtAccNo, txtErrorBG);
        return false;
    }
    if (hasWhiteSpace(accNumberSpa)) {
        showAlertRcMB(kony.i18n.getLocalizedString("keyErrAccntNoInvalid"), kony.i18n.getLocalizedString("info"), "info");
        return false;
    }
    if (checkAccountForDuplicateNickNamesMB()) {
        return;
    }
    if (!checkAccountDuplicate(accNumber)) {
        return;
    }
    if (!(getORFTFlagIB(gblSelectedBankCode) == "Y") && AccountNameCheck(frmMyRecipientAddAcc.tbxAccName.text) == false) {
        showAlert(kony.i18n.getLocalizedString("keyInvalidaccountName"), kony.i18n.getLocalizedString("info"));
        return false;
    }
    checknValidateAccountToProceedMB();
}

function checkAccountForDuplicateNickNamesMB() {
    var arg = new Array();
    var args = new Array();
    if (isRecipientNew == true) {
        //AddAccountList
        //kony.string.flipCase(args, false)
        if (AddAccountList.length != 0 && AddAccountList != null) {
            for (var i = 0; i < AddAccountList.length; i++) {
                //Check nickname
                arg[0] = frmMyRecipientAddAcc.txtNickname.text;
                args[0] = AddAccountList[i].lblNick.text
                if (frmMyRecipientAddAcc.txtNickname.text == AddAccountList[i].lblNick.text) {
                    TextBoxErrorSkin(frmMyRecipientAddAcc.txtNickname, txtErrorBG);
                    showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_correctnickname"), kony.i18n.getLocalizedString("info"), "info")
                    return true;
                }
                //Check account number
                if (frmMyRecipientAddAcc.txtAccNo.text == AddAccountList[i].lblAccountNo.text) {
                    TextBoxErrorSkin(frmMyRecipientAddAcc.txtAccNo, txtErrorBG);
                    showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_correctnickname"), kony.i18n.getLocalizedString("info"), "info")
                    return true;
                }
            }
        }
    } else if (isRecipientNew == false) {
        if (AddAccountList.length != 0 && AddAccountList != null) {
            for (var i = 0; i < AddAccountList.length; i++) {
                //Check nickname
                arg[0] = frmMyRecipientAddAcc.txtNickname.text;
                args[0] = AddAccountList[i].lblNick.text
                    //if (kony.string.flipCase(arg, false) == kony.string.flipCase(args, false)) {
                if (frmMyRecipientAddAcc.txtNickname.text == AddAccountList[i].lblNick.text) {
                    TextBoxErrorSkin(frmMyRecipientAddAcc.txtNickname, txtErrorBG);
                    showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_correctnickname"), kony.i18n.getLocalizedString("info"), "info")
                    return true;
                }
                //Check account number
                if (frmMyRecipientAddAcc.txtAccNo.text == AddAccountList[i].lblAccountNo.text) {
                    TextBoxErrorSkin(frmMyRecipientAddAcc.txtAccNo, txtErrorBG);
                    showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_correctaccount"), kony.i18n.getLocalizedString("info"), "info")
                    return true;
                }
            }
        }
        if (AccountList != null && AccountList.length != 0) {
            for (var i = 0; i < AccountList.length; i++) {
                //Check nickname
                arg[0] = AccountList[i].lblNick.text;
                args[0] = frmMyRecipientAddAcc.txtNickname.text
                if (frmMyRecipientAddAcc.txtNickname.text == AccountList[i].lblNick.text) {
                    TextBoxErrorSkin(frmMyRecipientAddAcc.txtNickname, txtErrorBG);
                    showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_correctnickname"), kony.i18n.getLocalizedString("info"), "info")
                    return true;
                }
                //Check account number
                if (frmMyRecipientAddAcc.txtAccNo.text == AccountList[i].lblAccountNo.text) {
                    TextBoxErrorSkin(frmMyRecipientAddAcc.txtAccNo, txtErrorBG);
                    showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_correctaccount"), kony.i18n.getLocalizedString("info"), "info")
                    return true;
                }
            }
        }
    }
    return false;
}

function checknValidateAccountToProceedMB() {
    var toAccnt = stripDashAcc(frmMyRecipientAddAcc.txtAccNo.text);
    if (gblSelectedBankCode == TMB_BANK_CODE) {
        startDepositAccountEnquiryServiceMB();
    } else {
        var flag = "Y";
        flag = getORFTFlagIB(gblSelectedBankCode);
        if (flag == "Y") {
            startORFTAccountEnquiryServiceMB();
        } else {
            var toAccnt = stripDashAcc(frmMyRecipientAddAcc.txtAccNo.text);
            gblDefaultAccountNum = "0012005864";
            var inputParam = {};
            inputParam["fromAcctNo"] = gblDefaultAccountNum;
            inputParam["toAcctNo"] = toAccnt;
            inputParam["toFIIdent"] = gblSelectedBankCode;
            inputParam["isFromRecipient"] = "yes";
            inputParam["AccountCheck"] = "yes";
            inputParam["bankcode"] = gblSelectedBankCode;
            showLoadingScreen();
            invokeServiceSecureAsync("ORFTInq", inputParam, startORFTAccountEnquiryServiceAsyncCallbackSMARTMB);
        }
    }
}

function startORFTAccountEnquiryServiceAsyncCallbackSMARTMB(status, callBackResponse) {
    if (status == 400) {
        kony.application.dismissLoadingScreen();
        if (callBackResponse["opstatus"] == 0) {
            proceedAfterORFTValidation(frmMyRecipientAddAcc.tbxAccName.text);
        } else {
            if (callBackResponse["errMsg"] == undefined || callBackResponse["errMsg"] == null) showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            else alert(" " + callBackResponse["errMsg"]);
        }
    }
}

function startDepositAccountEnquiryServiceMB() {
    var toAccnt = stripDashAcc(frmMyRecipientAddAcc.txtAccNo.text);
    if (toAccnt[3] == "2" || toAccnt[3] == "7" || toAccnt[3] == "9") {
        //dont delete this ravi
    } else if (toAccnt[3] == "1") {
        //dont delete this
    } else if (toAccnt[3] == "3") {
        //dont delete this
    } else if (toAccnt[3] == "5" || toAccnt[3] == "6") { //LoanAccountError
        showAlertRcMB(kony.i18n.getLocalizedString("LoanAccountError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
        return;
    }
    //below else blok is commented as we moved this validation to server side
    //else {//CCAccountError
    //showAlertRcMB(kony.i18n.getLocalizedString("CCAccountError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
    //return;
    //}
    var inputParams = {
        acctId: toAccnt,
        isFromRecipient: "yes",
        toAcctNo: toAccnt,
        bankcode: "11"
    };
    showLoadingScreen();
    invokeServiceSecureAsync("depositAccountInquiryNonSec", inputParams, startDepositAccountEnquiryServiceAsyncCallbackMB);
}

function startDepositAccountEnquiryServiceAsyncCallbackMB(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == 0) {
            if (callBackResponse["StatusCode"] == 0) {
                if (callBackResponse["accountTitle"] != null && callBackResponse["accountTitle"] != "") {
                    BANK_LOGO_URL = "https://" + appConfig.serverIp + "/" + appConfig.middlewareContext + "/Bank-logo/bank-logo-";
                    var tempRecord = {
                        "bankLogo": "myrecpicon.png",
                        "lblBankName": {
                            "text": frmMyRecipientAddAcc.btnBanklist.text
                        },
                        "button47408221857154": {
                            "skin": "btnDelete"
                        },
                        "lblAccountNo": {
                            "text": frmMyRecipientAddAcc.txtAccNo.text
                        },
                        "lblNick": {
                            "text": frmMyRecipientAddAcc.txtNickname.text
                        },
                        "lblAcctValue": {
                            "text": callBackResponse["accountTitle"]
                        },
                        "btnTransfer": {
                            text: kony.i18n.getLocalizedString("Transfer"),
                            skin: "btnBlueSkin"
                        },
                        "bankCode": gblSelectedBankCode,
                        //"lblBank": kony.i18n.getLocalizedString('keyBank'),
                        "lblNumber": kony.i18n.getLocalizedString('keyNumber'),
                        "lblNickName": kony.i18n.getLocalizedString('Nickname'),
                        "lblAccountName": kony.i18n.getLocalizedString('AccountName')
                    }
                    AddAccountList.push(tempRecord);
                    myRecAccAddConfLoad();
                    frmMyRecipientAddAccConf.show();
                }
            } else {
                showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_correctdetails"), kony.i18n.getLocalizedString("info"), "info")
            }
        } else if (callBackResponse["opstatus"] == 1) {
            if (callBackResponse["errMsg"] != undefined) {
                alert(callBackResponse["errMsg"]);
            } else {
                alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
            }
        } else if (callBackResponse["opstatus"] == 2) {
            alert(kony.i18n.getLocalizedString("keyReceipent_alert_MEAccountAdd"));
        } else {
            showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_Error"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
        }
        kony.application.dismissLoadingScreen();
    }
}

function startORFTAccountEnquiryServiceMB() {
    var toAccnt = stripDashAcc(frmMyRecipientAddAcc.txtAccNo.text);
    //gblDefaultAccountNum = "0012005864";
    var inputParam = {};
    //inputParam["fromAcctNo"] = gblDefaultAccountNum;
    inputParam["toAcctNo"] = toAccnt;
    inputParam["toFIIdent"] = gblSelectedBankCode;
    inputParam["isFromRecipient"] = "yes";
    inputParam["bankcode"] = gblSelectedBankCode;
    showLoadingScreen();
    invokeServiceSecureAsync("ORFTInq", inputParam, startORFTAccountEnquiryServiceAsyncCallbackMB);
}

function startORFTAccountEnquiryServiceAsyncCallbackMB(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == 0) {
            if (callBackResponse["StatusCode"] == 0) {
                //
                kony.application.dismissLoadingScreen();
                if (callBackResponse["ORFTTrnferInqRs"][0]["toAcctName"] != null && callBackResponse["ORFTTrnferInqRs"][0]["toAcctName"] != "") {
                    proceedAfterORFTValidation(callBackResponse["ORFTTrnferInqRs"][0]["toAcctName"]);
                } else {
                    proceedAfterORFTValidation(""); //Ticket#28938
                }
            } else {
                kony.application.dismissLoadingScreen();
                if (callBackResponse["errMsg"] == undefined || callBackResponse["errMsg"] == null) showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
                else alert(" " + callBackResponse["errMsg"]);
            }
        } else {
            kony.application.dismissLoadingScreen();
            if (callBackResponse["errMsg"] == undefined || callBackResponse["errMsg"] == null) showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            else alert(" " + callBackResponse["errMsg"]);
        }
        kony.application.dismissLoadingScreen();
    }
}

function proceedAfterORFTValidation(accname) {
    BANK_LOGO_URL = "https://" + appConfig.serverIp + "/" + appConfig.middlewareContext + "/Bank-logo/bank-logo-";
    var accntNo = stripDashAcc(frmMyRecipientAddAcc.txtAccNo.text)
    if (accntNo.length == 10) accntNo = accntNo.substring(0, 3) + "-" + accntNo.substring(3, 4) + "-" + accntNo.substring(4, 9) + "-" + accntNo.substring(9, 10);
    if (accname != null) {
        var tempRecord = {
            "bankLogo": "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + gblSelectedBankCode + "&modIdentifier=BANKICON",
            "lblBankName": {
                "text": frmMyRecipientAddAcc.btnBanklist.text
            },
            "button47408221857154": {
                "skin": "btnDelete"
            },
            "lblAccountNo": {
                "text": accntNo
            },
            "lblNick": {
                "text": frmMyRecipientAddAcc.txtNickname.text
            },
            "lblAcctValue": {
                "text": accname
            },
            "btnTransfer": {
                text: kony.i18n.getLocalizedString("Transfer"),
                skin: "btnBlueSkin"
            },
            "bankCode": gblSelectedBankCode,
            //"lblBank": kony.i18n.getLocalizedString('keyBank'),
            "lblNumber": kony.i18n.getLocalizedString('keyNumber'),
            "lblNickName": kony.i18n.getLocalizedString('Nickname'),
            "lblAccountName": kony.i18n.getLocalizedString('AccountName')
        }
        AddAccountList.push(tempRecord);
    } else {
        var tempRecord = {
            "bankLogo": "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + gblSelectedBankCode + "&modIdentifier=BANKICON",
            "lblBankName": {
                "text": frmMyRecipientAddAcc.btnBanklist.text
            },
            "button47408221857154": {
                "skin": "btnDelete"
            },
            "lblAccountNo": {
                "text": accntNo
            },
            "lblNick": {
                "text": frmMyRecipientAddAcc.txtNickname.text
            },
            "lblAcctValue": {
                "text": ""
            },
            "btnTransfer": {
                text: kony.i18n.getLocalizedString("Transfer"),
                skin: "btnBlueSkin"
            },
            "bankCode": gblSelectedBankCode,
            //"lblBank": kony.i18n.getLocalizedString('keyBank'),
            "lblNumber": kony.i18n.getLocalizedString('keyNumber'),
            "lblNickName": kony.i18n.getLocalizedString('Nickname'),
            "lblAccountName": "" //kony.i18n.getLocalizedString('AccountName')
        }
        AddAccountList.push(tempRecord);
    }
    myRecAccAddConfLoad();
    frmMyRecipientAddAccConf.show();
}

function completeRecepientAddAccount() {
    //var tempData = [];
    var selectedRcId = currentRecipientDetails["rcId"]
    var totalData = [];
    for (var i = 0; i < AddAccountList.length; i++) {
        var acctName = "";
        var BankName = AddAccountList[i].lblBankName.text;
        var Number = stripDashAcc(AddAccountList[i].lblAccountNo.text);
        var NickName = AddAccountList[i].lblNick.text;
        var bankCode = AddAccountList[i].bankCode;
        if (AddAccountList[i] != undefined && AddAccountList[i].lblAcctValue != undefined) {
            acctName = AddAccountList[i].lblAcctValue.text;
        }
        if (acctName == null || acctName == undefined || acctName == "") {
            acctName = "Not returned from GSB";
        }
        var bankData = [crmId, selectedRcId, bankCode, Number, NickName, "Added", acctName];
        totalData.push(bankData);
    }
    return totalData.toString();
}
/*function getecepientAddAccountNumebrs() {
	//var tempData = [];
	var totalData = "";
	for (var i = 0; i < AddAccountList.length; i++) {
		totalData = totalData+stripDashAcc(AddAccountList[i].lblAccountNo.text);
	}
	
	return totalData.toString();
}*/
function myRecAccAddConfLoad() {
    if (!isRecipientNew) {
        frmMyRecipientAddAccConf.lblName.text = currentRecipientDetails["name"];
        if (currentRecipientDetails["mobile"] == null || currentRecipientDetails["mobile"] == "") {
            frmMyRecipientAddAccConf.hbox47502979411849.setVisibility(false);
        } else {
            frmMyRecipientAddAccConf.hbox47502979411849.setVisibility(true);
            frmMyRecipientAddAccConf.lblMobile.text = currentRecipientDetails["mobile"];
        }
        if (currentRecipientDetails["email"] == null || currentRecipientDetails["email"] == "") {
            frmMyRecipientAddAccConf.hbox47502979411850.setVisibility(false);
        } else {
            frmMyRecipientAddAccConf.hbox47502979411850.setVisibility(true);
            frmMyRecipientAddAccConf.lblEmail.text = currentRecipientDetails["email"];
        }
        if (currentRecipientDetails["facebook"] == null || currentRecipientDetails["facebook"] == "") {
            frmMyRecipientAddAccConf.hbox47502979411851.setVisibility(false);
        } else {
            frmMyRecipientAddAccConf.hbox47502979411851.setVisibility(true);
            frmMyRecipientAddAccConf.lblFbIDstudio5.text = currentRecipientDetails["facebook"]; //Modified by Studio Viz
        }
        frmMyRecipientAddAccConf.imgprofilepic.src = currentRecipientDetails["pic"];
    } else {
        frmMyRecipientAddAccConf.lblName.text = frmMyRecipientAddProfile.tbxRecipientName.text.replace(/^\s+|\s+$/g, "");
        if (frmMyRecipientAddProfile.tbxMobileNo.text == null || frmMyRecipientAddProfile.tbxMobileNo.text == "") {
            frmMyRecipientAddAccConf.lblMobile.text = "";
            frmMyRecipientAddAccConf.hbox47502979411849.setVisibility(false);
        } else {
            frmMyRecipientAddAccConf.hbox47502979411849.setVisibility(true);
            frmMyRecipientAddAccConf.lblMobile.text = frmMyRecipientAddProfile.tbxMobileNo.text;
        }
        if (frmMyRecipientAddProfile.tbxEmail.text == null || frmMyRecipientAddProfile.tbxEmail.text == "") {
            frmMyRecipientAddAccConf.lblEmail.text = "";
            frmMyRecipientAddAccConf.hbox47502979411850.setVisibility(false);
        } else {
            frmMyRecipientAddAccConf.hbox47502979411850.setVisibility(true);
            frmMyRecipientAddAccConf.lblEmail.text = frmMyRecipientAddProfile.tbxEmail.text;
        }
        if (frmMyRecipientAddProfile.tbxFbID.text == null || frmMyRecipientAddProfile.tbxFbID.text == "") {
            frmMyRecipientAddAccConf.lblFbIDstudio5.text = ""; //Modified by Studio Viz
            frmMyRecipientAddAccConf.hbox47502979411851.setVisibility(false);
        } else {
            frmMyRecipientAddAccConf.hbox47502979411851.setVisibility(true);
            frmMyRecipientAddAccConf.lblFbIDstudio5.text = frmMyRecipientAddProfile.tbxFbID.text; //Modified by Studio Viz
        }
        if (flowSpa) {
            if (profilePicFlag == "picture") {
                frmMyRecipientAddAccConf.imgprofilepic.src = frmMyRecipientAddProfile.imgProfilePic.src;
            } else if (frmMyRecipientAddProfile.imgProfilePic.src.indexOf("fbcdn") >= 0) {
                frmMyRecipientAddAccConf.imgprofilepic.src = frmMyRecipientAddProfile.imgProfilePic.src;
            } else {
                frmMyRecipientAddAccConf.imgprofilepic.src = "avatar_dis.png";
            }
        } else {
            if (gblRcBase64List != "") {
                frmMyRecipientAddAccConf.imgprofilepic.rawBytes = frmMyRecipientAddProfile.imgProfilePic.rawBytes;
            } else {
                frmMyRecipientAddAccConf.imgprofilepic.src = frmMyRecipientAddProfile.imgProfilePic.src;
            }
            if (gblAddContactFlow == true) {
                frmMyRecipientAddAccConf.imgprofilepic.base64 = frmMyRecipientAddProfile.imgProfilePic.base64;
            }
        }
    }
    frmMyRecipientAddAccConf.segMyRecipientDetail.removeAll();
    if (AddAccountList.length > 0) {
        if (recipientAddFromTransfer) {
            frmMyRecipientAddAccConf.btnAddAcc.setVisibility(false);
        } else {
            frmMyRecipientAddAccConf.btnAddAcc.setVisibility(true);
        }
        if (AddAccountList.length > MAX_BANK_ACCNT_BULK_ADD - 1 || recipientAddFromTransfer) {
            frmMyRecipientAddAccConf.btnAddAcc.skin = "btnAddDisable";
            frmMyRecipientAddAccConf.btnAddAcc.setEnabled(false);
            frmMyRecipientAddAccConf.lblAccount.text = kony.i18n.getLocalizedString("Receipent_Accounts");
        } else {
            frmMyRecipientAddAccConf.btnAddAcc.skin = "btnAdd";
            frmMyRecipientAddAccConf.btnAddAcc.setEnabled(true);
            frmMyRecipientAddAccConf.lblAccount.text = kony.i18n.getLocalizedString("Receipent_Account");
        }
        frmMyRecipientAddAccConf.segMyRecipientDetail.setData(AddAccountList);
    } else {
        frmMyRecipientAddAccConf.btnAddAcc.skin = "btnAdd";
        frmMyRecipientAddAccConf.btnAddAcc.setEnabled(true);
    }
}

function btnAddAccOnclick() {
    frmMyRecipientAddAcc.show();
}

function addAccountRecipentCompletePre() {
    gblFlagTransPwdFlow = "addAccount";
    checkVerifyPWDMBRC();
}

function addAccountRecipentComplete() {
    if (flowSpa) {} else {
        popTransactionPwd.dismiss();
    }
    if (isRecipientNew == false) {
        var inputParams = {}
        if (AddAccountList.length == 0) {
            showAlert(kony.i18n.getLocalizedString("Receipent_alert_atleastonebankaccount"), kony.i18n.getLocalizedString("info"));
            return;
        }
        inputParams["personalizedAccList"] = completeRecepientAddAccount();
        showLoadingScreen();
        invokeServiceSecureAsync("receipentAddBankAccntService", inputParams, addAccountServiceCallbackMB);
    } else {
        if (AddAccountList.length > 0) {
            gblRcPersonalizedIdList = frmMyRecipientAddAccConf.lblName.text;
            startNewRcAddwithAccntServiceMB();
        } else {
            addNewRecipientDetailsMB();
            isRecipientNew = true;
        }
    }
}

function addAccountServiceCallbackMB(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == 0) {
            isChMyRecipientsRs = true;
            isChRecipientAccountsRs = true;
            frmMyRecipientAddAccComplete.lblName.text = currentRecipientDetails["name"];
            if (currentRecipientDetails["mobile"] == null || currentRecipientDetails["mobile"] == "") {
                frmMyRecipientAddAccComplete.hbox47502979411849.setVisibility(false);
            } else {
                frmMyRecipientAddAccComplete.hbox47502979411849.setVisibility(true);
                frmMyRecipientAddAccComplete.lblMobile.text = currentRecipientDetails["mobile"];
            }
            if (currentRecipientDetails["email"] == null || currentRecipientDetails["email"] == "") {
                frmMyRecipientAddAccComplete.hbox47502979411850.setVisibility(false);
            } else {
                frmMyRecipientAddAccComplete.hbox47502979411850.setVisibility(true);
                frmMyRecipientAddAccComplete.lblEmail.text = currentRecipientDetails["email"];
            }
            if (currentRecipientDetails["facebook"] == null || currentRecipientDetails["facebook"] == "") {
                frmMyRecipientAddAccComplete.hbox47502979411851.setVisibility(false);
            } else {
                frmMyRecipientAddAccComplete.hbox47502979411851.setVisibility(true);
                frmMyRecipientAddAccComplete.lblFbID.text = currentRecipientDetails["facebook"];
            }
            if (flowSpa) {
                frmMyRecipientAddAccComplete.imgprofilepic.base64 = frmMyRecipientAddAccConf.imgprofilepic.base64;
            } else {
                if (gblAddContactFlow == true) {
                    frmMyRecipientAddAccComplete.imgprofilepic.base64 = frmMyRecipientAddAccConf.imgprofilepic.base64;
                } else {
                    frmMyRecipientAddAccComplete.imgprofilepic.src = frmMyRecipientAddAccConf.imgprofilepic.src;
                }
            }
            frmMyRecipientAddAccComplete.segMyRecipientDetail.removeAll();
            frmMyRecipientAddAccComplete.lblAccounts.isVisible = false;
            if (AddAccountList.length > 0) {
                frmMyRecipientAddAccComplete.segMyRecipientDetail.setData(AddAccountList);
                frmMyRecipientAddAccComplete.lblAccounts.isVisible = true;
            }
            if (frmMyRecipientAddAccConf.segMyRecipientDetail.data.length > 1) {
                frmMyRecipientAddAccComplete.lblAccounts.text = kony.i18n.getLocalizedString("Receipent_Accounts");
            } else {
                frmMyRecipientAddAccComplete.lblAccounts.text = kony.i18n.getLocalizedString("Receipent_Account");
            }
            sendDeleteNotification("addaccount", "", "", "", "", "", "", "");
            frmMyRecipientAddAccComplete.show();
        } else if (callBackResponse["opstatus"] == 1) {
            if (callBackResponse["errMsg"] != null) {
                showAlertRcMB("" + callBackResponse["errMsg"], kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
                frmMyRecipients.show();
                return;
            }
        } else if (callBackResponse["opstatus"] == 4) { //duplicate account addition
            showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_correctaccount"), kony.i18n.getLocalizedString("info"), "info")
            frmMyRecipients.show();
            return;
        } else {
            showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_Error"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
        }
        kony.application.dismissLoadingScreen();
    }
}
//account format
function onEdittAccNum(txt) {
    if (parseInt(gblSelectedBankAccLength) == 10 && MAX_ACC_LEN_LIST.indexOf(",") < 0) {
        if (txt == null) return false;
        var numChars = txt.length;
        var temp = "";
        var i, txtLen = numChars;
        var currLen = numChars;
        if (gblPrevLen < currLen) {
            for (i = 0; i < numChars; ++i) {
                if (txt[i] != '-') {
                    temp = temp + txt[i];
                } else {
                    txtLen--;
                }
            }
            var iphenText = "";
            for (i = 0; i < txtLen; i++) {
                iphenText += temp[i];
                if (i == 2 || i == 3 || i == 8) {
                    iphenText += '-';
                }
            }
            frmMyRecipientAddAcc.txtAccNo.text = iphenText;
        }
        gblPrevLen = currLen;
        accLengthValidation();
    } else {
        if (frmMyRecipientAddAcc.txtAccNo.text.length == (parseInt(gblSelectedBankAccLength))) {
            frmMyRecipientAddAcc.txtAccNo.setFocus(false);
        }
    }
}

function deleteRecepientAccountFromCache() {
    var row = frmMyRecipientAddAccConf.segMyRecipientDetail.selectedIndex;
    // below code chnaged for the defect DEF16512
    if (flowSpa) {
        frmMyRecipientAddAccConf.segMyRecipientDetail.removeAt(frmMyRecipientAddAccConf.segMyRecipientDetail.selectedIndex[1], 0);
        AddAccountList.splice(row[1], 0);
    } else {
        frmMyRecipientAddAccConf.segMyRecipientDetail.removeAt(row[1]);
        AddAccountList.splice(row[1], 1);
    }
    frmMyRecipientAddAccConf.btnAddAcc.setVisibility(true);
    if (AddAccountList.length == MAX_BANK_ACCNT_BULK_ADD) {
        frmMyRecipientAddAccConf.btnAddAcc.skin = "btnAddDisable";
        frmMyRecipientAddAccConf.btnAddAcc.setEnabled(false);
    } else {
        frmMyRecipientAddAccConf.btnAddAcc.skin = "btnAdd";
        frmMyRecipientAddAccConf.btnAddAcc.setEnabled(true);
    }
    if (recipientAddFromTransfer) {
        frmMyRecipientAddAcc.show();
    }
}

function onClickContactList() {
    var btnskin = "btnMyRecipient";
    var btnFocusSkin = "btnMyRecipientFoc";
    if (frmMyRecipients.hboxaddfblist.isVisible) {
        frmMyRecipients.hboxaddfblist.isVisible = false;
        frmMyRecipients.btnRight.skin = btnskin;
        frmMyRecipients.imgHeaderMiddle.src = "arrowtopblue.png";
        frmMyRecipients.imgHeaderRight.src = "empty.png";
    } else {
        frmMyRecipients.hboxaddfblist.isVisible = true;
        frmMyRecipients.btnRight.skin = btnFocusSkin;
        frmMyRecipients.imgHeaderMiddle.src = "empty.png";
        frmMyRecipients.imgHeaderRight.src = "arrowtop.png";
    }
}

function dynamicSort(property) {
    var sortOrder = 1;
    if (property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1, property.length - 1);
    }
    return function(a, b) {
        var result = (a[property]["text"].toLowerCase() < b[property]["text"].toLowerCase()) ? -1 : (a[property]["text"].toLowerCase() > b[property]["text"].toLowerCase()) ? 1 : 0;
        return result * sortOrder;
    }
}

function dynamicSortOther(property) {
    var sortOrder = 1;
    if (property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1, property.length - 1);
    }
    return function(a, b) {
        var result = (a[property].toLowerCase() < b[property].toLowerCase()) ? -1 : (a[property].toLowerCase() > b[property].toLowerCase()) ? 1 : 0;
        return result * sortOrder;
    }
}

function invokeCameraFromPopup() {
    var rawBytesString = popProfilePic.camera1.rawBytes;
    gblRcBase64List = kony.convertToBase64(rawBytesString);
    var curSize = (gblRcBase64List.length - 814) / 1.37;
    if (curSize > Gbl_Image_Size_Limit) {
        alert("" + kony.i18n.getLocalizedString("keyImageSizeTooLarge"));
        return;
    }
    if (kony.application.getCurrentForm().id == "frmMyRecipientAddProfile") {
        frmMyRecipientAddProfile.imgProfilePic.rawBytes = rawBytesString;
        gblRcPersonalizedIdList = frmMyRecipientAddProfile.tbxRecipientName.text;
        gblRcBase64List = gblRcBase64List.replace(/[\n\r\s\f\t\v]+/g, '');
    } else if (kony.application.getCurrentForm().id == "frmMyRecipientEditProfile") {
        frmMyRecipientEditProfile.imgprofilepic.rawBytes = rawBytesString;
        gblRcPersonalizedIdList = frmMyRecipientEditProfile.tbxRecipientName.text;
        gblRcBase64List = gblRcBase64List.replace(/[\n\r\s\f\t\v]+/g, '');
    }
    if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad") {
        //if(true){
        //	popProfilePic.camera1.releaseRawBytes(rawBytesString);
    }
    popProfilePic.dismiss();
}

function openMediaGalleryinPopUp() {
    try {
        function onselectioncallback(rawbytes) {
            if (rawbytes == null) {
                showAlertRcMB(kony.i18n.getLocalizedString("KeyImageNotSelected"), kony.i18n.getLocalizedString("info"), "info")
                return;
            }
            gblRcBase64List = "";
            gblRcPersonalizedIdList = "";
            gblRcBase64List = kony.convertToBase64(rawbytes);
            //alert(gblRcBase64List.length);
            var curSize = (gblRcBase64List.length - 814) / 1.37;
            if (curSize > Gbl_Image_Size_Limit) {
                alert("" + kony.i18n.getLocalizedString("keyImageSizeTooLarge"));
                return;
            }
            if (kony.application.getCurrentForm().id == "frmMyRecipientAddProfile") {
                gblRcPersonalizedIdList = frmMyRecipientAddProfile.tbxRecipientName.text;
                frmMyRecipientAddProfile.imgProfilePic.rawBytes = rawbytes;
                gblRcBase64List = gblRcBase64List.replace(/[\n\r\s\f\t\v]+/g, '');
            } else if (kony.application.getCurrentForm().id == "frmMyRecipientEditProfile") {
                gblRcPersonalizedIdList = frmMyRecipientEditProfile.tbxRecipientName.text;
                frmMyRecipientEditProfile.imgprofilepic.rawBytes = rawbytes;
                gblRcBase64List = gblRcBase64List.replace(/[\n\r\s\f\t\v]+/g, '');
            }
            popProfilePic.dismiss();
        }
        var querycontext = {
            mimetype: "image/*"
        };
        returnStatus = kony.phone.openMediaGallery(onselectioncallback, querycontext);
    } catch (err) {
        showAlertRcMB(kony.i18n.getLocalizedString("KeyMediaGalleryError"), kony.i18n.getLocalizedString("info"), "info")
    }
}
var RecipientList = new Array();
var RecipientListFav = new Array();
var myRecipientsRs = new Array();
var AddAccountList = new Array();
var noOfAccountsAdded = null;
var AccountsList = new Array();
var AccountsListFav = new Array();
var isChRecipientAccountsRs = true;
var recipientAccountsRs = [];
var ContactList = new Array();
var ContactList1 = new Array();
var multiSelectedContacts = new Array();
var selectContactsList = new Array();
var notAllowedContacts = new Array();
var originalName = false; //stored temporarily in edit recipient form to update myRecipientsRs accordingly
var originalAccNickname = false;
var requestFromForm = null;
var selectedMobile = null;
var selectedFbID = null;
var selectedFBUserName = null;
var selectedFBImage = null;

function transCancelAddAccount() {
    popTransactionPwd.dismiss();
    frmMyRecipientAddAccConf.show();
}

function deleteRecipientCancel() {
    popDelRecipientProfile.dismiss();
}

function deleteRecipientConfirm() {
    var recipientToDelete = frmMyRecipientDetail.lblName.text;
    for (var i in myRecipientsRs) {
        if (myRecipientsRs[i]["lblName"] == recipientToDelete) {
            myRecipientsRs.splice(i, 1);
            //also delete all accounts related to this recipient
            isChMyRecipientsRs = true;
            //send a mail to user about his delete operation
        }
    }
    popDelRecipientProfile.dismiss();
    frmMyRecipients.show();
}

function stripDashAcc(accountNo) {
    if (accountNo != null) {
        for (var i = 0; i < (accountNo.length); i++) {
            if (accountNo[i] == '-') {
                accountNo = accountNo.replace("-", "");
            }
        }
    }
    return accountNo;
}

function addDashPh(txt) {
    /*if(txt.length>10){
	
    txt = txt.substring(txt.length - 9,txt.length) ;
    txt = "0"+txt;
    }
	
    var i = txt.substr(0, 3);
    var j = txt.substr(3, 3);
    var k = txt.substr(6, 4);
    return i + "-" + j + "-" + k;*/
    return txt;
}

function stripDashPh(txt) {
    if (txt == null || txt == undefined) return "";
    for (var i = 0; i < (txt.length - 1); i++) {
        if (txt[i] == '-') {
            txt = txt.replace("-", "");
        }
    }
    return txt;
}

function addCrossPh(txt) {
    if (txt == null || txt == undefined || txt == "") return "";
    return txt.substring(0, 3) + "XXX" + txt.substring(6)
}

function addCrossAccount(txt) {
    if (txt == null || txt == undefined || txt == "") return "";
    if (txt.length == 10) {
        return "XXXX" + txt.substr(4, 5) + "X";
    } else {
        return txt;
    }
}

function addCrossEmail(txt) {
    if (txt == null || txt == undefined || txt == "") return "";
    var from = txt.indexOf("@");
    var to = txt.indexOf(".");
    return txt.substring(0, from + 1) + "XXX" + txt.substring(to);
}

function addDashAc(txt) {
    if (txt.length != 10) return txt;
    var i = txt.substr(0, 3);
    var j = txt.substr(3, 1);
    var k = txt.substr(4, 5);
    var l = txt.substr(9);
    var acc = i + "-" + j + "-" + k + "-" + l;
    var len = acc.length;
    if (acc.charAt(len - 1) == '-') {
        return acc.substr(0, len - 1);
    } else {
        return acc;
    }
}

function charCapitalize(txt) {
    var result = "";
    var n = txt.split(" ");
    for (var i = 0; i < n.length; i++) {
        if (i == (n.length - 1)) {
            result = result + n[i].charAt(0).toUpperCase() + n[i].slice(1);
        } else {
            result = result + n[i].charAt(0).toUpperCase() + n[i].slice(1) + " ";
        }
    }
    return result;
}
//function onClickeditAccountConfirm() {
//	var isORFT=isOrftEligible(frmMyRecipientDetail.segMyRecipientDetail.selectedItems[0]["lblBankCD"])
//	if(ThaiOrEnglish(frmMyRecipientEditAccount.txtAccountNickName.text)){
//		if(kony.string.containsChars(frmMyRecipientEditAccount.txtAccountNickName.text, charsArr)){
//			TextBoxErrorSkin(frmMyRecipientEditAccount.txtAccountNickName, txtErrorBG);
//			frmMyRecipientEditAccount.txtAccountNickName.focusSkin = txtErrorBG;
//			showAlertRcMB(kony.i18n.getLocalizedString("keyInvalidNickName"), kony.i18n.getLocalizedString("info"), "info")
//			return false;
//		}
//	}
//	else{
//		var recipientNickname = /^[a-zA-Z0-9\s]{1,20}$/ig;
//		if(frmMyRecipientEditAccount.txtAccountNickName.text.trim() == "" || frmMyRecipientEditAccount.txtAccountNickName.text.trim() == null){
//			TextBoxErrorSkin(frmMyRecipientEditAccount.txtAccountNickName, txtErrorBG);
//			frmMyRecipientEditAccount.txtAccountNickName.focusSkin = txtErrorBG;
//			showAlertRcMB(kony.i18n.getLocalizedString("keyMandRecAcctNickname"), kony.i18n.getLocalizedString("info"), "info");
//			return;
//		}
//		var resultRecipientNickname = true;//recipientNickname.test(frmMyRecipientEditAccount.txtAccountNickName.text);
//		if(kony.string.containsChars(frmMyRecipientEditAccount.txtAccountNickName.text, charsArr))
//			resultRecipientNickname = false;
//		if (!(resultRecipientNickname)) {
//			TextBoxErrorSkin(frmMyRecipientEditAccount.txtAccountNickName, txtErrorBG);
//			frmMyRecipientEditAccount.txtAccountNickName.focusSkin = txtErrorBG;
//			showAlertRcMB(kony.i18n.getLocalizedString("keyInvalidNickName"), kony.i18n.getLocalizedString("info"), "info")
//			return false;
//			//success=false;
//		}
//		originalAccNickname = frmMyRecipientViewAccount.lblNick.text;
//		//add logic to check if recipient nickname is unique
//		if(!isORFT){
//			if(frmMyRecipientEditAccount.txtAccountNickName.text == frmMyRecipientViewAccount.lblNick.text && frmMyRecipientEditAccount.tbxAccName.text == frmMyRecipientViewAccount.lblAcctName.text ){
//				frmMyRecipientViewAccount.show();
//				return false;
//			} 
//		
//		}
//		if(frmMyRecipientEditAccount.txtAccountNickName.text != frmMyRecipientViewAccount.lblNick.text) {
//			for (var i in recipientAccountsRs) {
//				if (recipientAccountsRs[i]["lblAccountNickname"] == frmMyRecipientEditAccount.txtAccountNickName.text) {
//					//recipientAccountsRs[i]["lblAccountNickname"]=frmMyRecipientEditAccount.txtAccountNickName.text;
//					TextBoxErrorSkin(frmMyRecipientEditAccount.txtAccountNickName, txtErrorBG);
//					frmMyRecipientEditAccount.txtAccountNickName.focusSkin = txtErrorBG;
//					showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_correctnickname"), kony.i18n.getLocalizedString("info"), "info");
//					return;
//				}
//			}
//		}else{
//				if(isORFT){
//					frmMyRecipientViewAccount.show();
//					return false;
//				}
//		}
//		
//		if (!isOrftEligible(frmMyRecipientDetail.segMyRecipientDetail.selectedItems[0]["lblBankCD"])) {
//			if( AccountNameCheck(frmMyRecipientEditAccount.tbxAccName.text) == false){
//		  		showAlert(kony.i18n.getLocalizedString("keyInvalidaccountName"), kony.i18n.getLocalizedString("info"));
//				return false;
//			}
//		}
//		
//	}
//	var accNo = frmMyRecipientViewAccount.lblAccountNo.text; //AccountList[ind].personalizedId.text;
//	accNo = stripDashAcc(accNo);
//	var nickName = frmMyRecipientEditAccount.txtAccountNickName.text;
//	var accName=frmMyRecipientViewAccount.lblAcctName.text;
//	if(!isORFT)
//	accName= frmMyRecipientEditAccount.tbxAccName.text;
//	startExistingRcAccountMBEditService(accNo, nickName,accName, "Added");
//}
function onClickeditAccountConfirm() {
    var isORFT = getORFTFlagIB(frmMyRecipientDetail.segMyRecipientDetail.selectedItems[0]["lblBankCD"]) == "Y"
    if (ThaiOrEnglish(frmMyRecipientEditAccount.txtAccountNickName.text)) {
        if (kony.string.containsChars(frmMyRecipientEditAccount.txtAccountNickName.text, charsArr)) {
            TextBoxErrorSkin(frmMyRecipientEditAccount.txtAccountNickName, txtErrorBG);
            frmMyRecipientEditAccount.txtAccountNickName.focusSkin = txtErrorBG;
            showAlertRcMB(kony.i18n.getLocalizedString("keyInvalidNickName"), kony.i18n.getLocalizedString("info"), "info")
            return false;
        }
    } else {
        TextBoxErrorSkin(frmMyRecipientEditAccount.txtAccountNickName, txtErrorBG);
        frmMyRecipientEditAccount.txtAccountNickName.focusSkin = txtErrorBG;
        showAlertRcMB(kony.i18n.getLocalizedString("keyInvalidNickName"), kony.i18n.getLocalizedString("info"), "info")
        return false;
    }
    originalAccNickname = frmMyRecipientViewAccount.lblNick.text;
    //add logic to check if recipient nickname is unique
    if (!isORFT) {
        if (frmMyRecipientEditAccount.txtAccountNickName.text == frmMyRecipientViewAccount.lblNick.text && frmMyRecipientEditAccount.tbxAccName.text == frmMyRecipientViewAccount.lblAcctName.text) {
            frmMyRecipientViewAccount.show();
            return false;
        }
    }
    if (frmMyRecipientEditAccount.txtAccountNickName.text != frmMyRecipientViewAccount.lblNick.text) {
        for (var i in recipientAccountsRs) {
            if (recipientAccountsRs[i]["lblAccountNickname"] == frmMyRecipientEditAccount.txtAccountNickName.text) {
                //recipientAccountsRs[i]["lblAccountNickname"]=frmMyRecipientEditAccount.txtAccountNickName.text;
                TextBoxErrorSkin(frmMyRecipientEditAccount.txtAccountNickName, txtErrorBG);
                frmMyRecipientEditAccount.txtAccountNickName.focusSkin = txtErrorBG;
                showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_correctnickname"), kony.i18n.getLocalizedString("info"), "info");
                return;
            }
        }
    } else {
        if (isORFT) {
            frmMyRecipientViewAccount.show();
            return false;
        }
    }
    if (!(getORFTFlagIB(frmMyRecipientDetail.segMyRecipientDetail.selectedItems[0]["lblBankCD"]) == "Y")) {
        if (AccountNameCheck(frmMyRecipientEditAccount.tbxAccName.text) == false) {
            showAlert(kony.i18n.getLocalizedString("keyInvalidaccountName"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
    var accNo = frmMyRecipientViewAccount.lblAccountNo.text; //AccountList[ind].personalizedId.text;
    accNo = stripDashAcc(accNo);
    var nickName = frmMyRecipientEditAccount.txtAccountNickName.text;
    var accName = frmMyRecipientViewAccount.lblAcctName.text;
    if (!isORFT) accName = frmMyRecipientEditAccount.tbxAccName.text;
    //if(AccountNameValid(accName))
    startExistingRcAccountMBEditService(accNo, nickName, accName, "Added");
    //else
    //return false;
}

function recipientNameEditValidations() {
    var recipientName = true; //recipientNameVal(frmMyRecipientEditProfile.tbxRecipientName.text.replace(/^\s+|\s+$/g, ""));
    if (recipientName == false) {
        TextBoxErrorSkin(frmMyRecipientEditProfile.tbxRecipientName, txtErrorBG);
        showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_differentRcname"), kony.i18n.getLocalizedString("info"), "info")
        return false;
    }
    if (isNotBlank(frmMyRecipientDetail.lblName.text)) {
        if (!CheckRecipientNameUniq(frmMyRecipientEditProfile.tbxRecipientName.text.replace(/^\s+|\s+$/g, ""), frmMyRecipientDetail.lblName.text.replace(/^\s+|\s+$/g, ""))) {
            recipientName = false;
            TextBoxErrorSkin(frmMyRecipientEditProfile.tbxRecipientName, txtErrorBG);
            showAlertRcMB(kony.i18n.getLocalizedString("RecipientExists"), kony.i18n.getLocalizedString("info"), "info")
            return false;
        }
    }
    if (kony.string.containsChars(frmMyRecipientEditProfile.tbxRecipientName.text.replace(/^\s+|\s+$/g, ""), charsArr)) {
        recipientName = false;
        TextBoxErrorSkin(frmMyRecipientEditProfile.tbxRecipientName, txtErrorBG);
        showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_differentRcname"), kony.i18n.getLocalizedString("info"), "info");
        return false;
    }
    return true;
}

function ThaiOrEnglish(text) {
    if ((text == null) || (text == "") || text.trim().length == "") return false;
    var txtLen = text.length;
    var pat2 = /^[A-Za-z0-9\u0E00-\u0E7F\s._@-]+$/
    var isAlphNum = pat2.test(text);
    //Below fix has been done for UAT DEF2596 
    if (isAlphNum) {
        if (txtLen <= 20 && txtLen >= 3) {
            return true;
        } else {
            return false;
        }
    }
    return false;
}

function onClickeditProfileConfirm() {
    //var originalName=false;
    var success = false;
    var recipientName = false;
    if (frmMyRecipientEditProfile.tbxRecipientName.text == "" || frmMyRecipientEditProfile.tbxRecipientName.text == undefined || frmMyRecipientEditProfile.tbxRecipientName.text == null) {
        TextBoxErrorSkin(frmMyRecipientEditProfile.tbxRecipientName, txtErrorBG);
        showAlertRcMB(kony.i18n.getLocalizedString("keyRecipientBlank"), kony.i18n.getLocalizedString("info"), "info");
        return false;
    }
    /*if(frmMyRecipientEditProfile.tbxRecipientName.text.length < 3){
    	TextBoxErrorSkin(frmMyRecipientEditProfile.tbxRecipientName, txtErrorBG);
    	showAlertRcMB(kony.i18n.getLocalizedString("RecipientNameShort"), kony.i18n.getLocalizedString("info"), "info");
    	return false;
    }*/
    if (ThaiOrEnglish(frmMyRecipientEditProfile.tbxRecipientName.text)) {
        var allowedRcName = frmMyRecipientDetail.lblName.text;
        recipientName = true;
        for (var i = 0; i < myRecipientsRs.length; i++) {
            if (myRecipientsRs[i]["lblName"]["text"] != null && myRecipientsRs[i]["lblName"]["text"] != undefined && (allowedRcName != myRecipientsRs[i]["lblName"]["text"])) {
                if (ThaiOrEnglish(myRecipientsRs[i]["lblName"]["text"])) {
                    if (myRecipientsRs[i]["lblName"]["text"] == frmMyRecipientEditProfile.tbxRecipientName.text) {
                        recipientName = false;
                        TextBoxErrorSkin(frmMyRecipientEditProfile.tbxRecipientName, txtErrorBG);
                        showAlertRcMB(kony.i18n.getLocalizedString("RecipientExists"), kony.i18n.getLocalizedString("info"), "info")
                        return false;
                    }
                } else {
                    recipientName = true;
                }
            }
        }
        if (!CheckRecipientNameUniq(frmMyRecipientEditProfile.tbxRecipientName.text.replace(/^\s+|\s+$/g, ""), allowedRcName)) {
            recipientName = false;
            TextBoxErrorSkin(frmMyRecipientEditProfile.tbxRecipientName, txtErrorBG);
            showAlertRcMB(kony.i18n.getLocalizedString("RecipientExists"), kony.i18n.getLocalizedString("info"), "info");
            return false;
        }
        if (kony.string.containsChars(frmMyRecipientEditProfile.tbxRecipientName.text, charsArr)) {
            recipientName = false;
            TextBoxErrorSkin(frmMyRecipientEditProfile.tbxRecipientName, txtErrorBG);
            showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_differentRcname"), kony.i18n.getLocalizedString("info"), "info");
            return false;
        }
    } else {
        if (!recipientNameEditValidations()) {
            return;
        } else {
            recipientName = true;
        }
    }
    var recipientMobile = null;
    var recipientEmail = null;
    if (frmMyRecipientEditProfile.tbxMobileNo.text == "" || frmMyRecipientEditProfile.tbxMobileNo.text == null || frmMyRecipientEditProfile.tbxMobileNo.text.trim() == "") {
        recipientMobile = true;
        frmMyRecipientEditProfile.tbxMobileNo.text == null;
    } else {
        var ph = frmMyRecipientEditProfile.tbxMobileNo.text;
        ph = stripDashPh(ph);
        recipientMobile = recipientMobileVal(ph);
        if (recipientMobile == false) {
            TextBoxErrorSkin(frmMyRecipientEditProfile.tbxMobileNo, txtErrorBG);
            showAlertRcMB(kony.i18n.getLocalizedString("keyEnteredMobileNumberisnotvalid"), kony.i18n.getLocalizedString("info"), "info");
            return;
        }
    }
    if (frmMyRecipientEditProfile.tbxEmail.text == "" || frmMyRecipientEditProfile.tbxEmail.text == null || frmMyRecipientEditProfile.tbxEmail.text.trim() == "") {
        recipientEmail = true;
        frmMyRecipientEditProfile.tbxEmail.text == null;
    } else {
        if (emailValidatn(frmMyRecipientEditProfile.tbxEmail.text) == false && !kony.string.containsChars(frmMyRecipientEditProfile.tbxEmail.text, charsArr)) {
            recipientEmail = false;
            TextBoxErrorSkin(frmMyRecipientEditProfile.tbxEmail, txtErrorBG);
            showAlertRcMB(kony.i18n.getLocalizedString("invalidEmail"), kony.i18n.getLocalizedString("info"), "info");
            return;
        } else {
            recipientEmail = true;
        }
    }
    if (!(recipientName && recipientEmail && recipientMobile)) {
        var i = 0,
            j = 0,
            k = 0;
        if (recipientName == false) {
            i = "Recipient Name Invalid\n";
        } else {
            i = "";
        }
        if (recipientEmail == false) {
            j = "Recipient Email Invalid\n";
        } else {
            j = "";
        }
        if (recipientMobile == false) {
            k = "Recipient Mobile Invalid\n";
        } else {
            k = "";
        }
        showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_correctdetails"), kony.i18n.getLocalizedString("info"), "info")
        success = false;
    } else {
        if (flowSpa) {
            var inputParams = {}
            spaChnage = "editProfile"
            gblFlagTransPwdFlow = "editProfile";
            gblOTPFlag = true;
            try {
                kony.timer.cancel("otpTimer")
            } catch (e) {}
            //input params for SPA OTP
            gblSpaChannel = "EditRecipient";
            /*
            var locale = kony.i18n.getCurrentLocale();
            if (locale == "en_US") {
            SpaEventNotificationPolicy = "MIB_EditRecipient_EN";
            SpaSMSSubject = "MIB_EditRecipient_EN";
            } else {
            	SpaEventNotificationPolicy = "MIB_EditRecipient_TH";
            	SpaSMSSubject = "MIB_EditRecipient_TH";
            }*/
            gblToSpaAccountName = frmMyRecipientEditProfile.tbxRecipientName.text;
            var mobNo = frmMyRecipientEditProfile.tbxMobileNo.text;
            var fbId = frmMyRecipientEditProfile.tbxFbID.text
            if (mobNo != null && mobNo != "" && mobNo != undefined) {
                mobNo = kony.string.replace(mobNo, "-", "");
                gblAccountDetails = "Mobile x" + mobNo.substring(6, 10)
            }
            if ((fbId != null && fbId != "" && fbId != undefined)) {
                gblAccountDetails = gblAccountDetails + " FB" + fbId.substring(0, 10);
            }
            saveRecipientDeatilsMB();
        } else {
            showOTPPopup(kony.i18n.getLocalizedString("TransactionPass") + ":", "", "", transConfirmEditProfile, "3")
        }
    }
}

function transConfirmEditProfile() {
    gblFlagTransPwdFlow = "editProfile";
    checkVerifyPWDMBRC();
}
//----------------------------------------------------------------------------------------------------------------------
//new recipient profile with accounts functions
function recipientNameAddValidations() {
    var recipientName = true; //recipientNameVal(frmMyRecipientAddProfile.tbxRecipientName.text.replace(/^\s+|\s+$/g, ""));
    if (recipientName == false) {
        TextBoxErrorSkin(frmMyRecipientAddProfile.tbxRecipientName, txtErrorBG);
        showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_differentRcname"), kony.i18n.getLocalizedString("info"), "info");
        return false;
    }
    if (!CheckRecipientNameUniq(frmMyRecipientAddProfile.tbxRecipientName.text.replace(/^\s+|\s+$/g, ""), "")) {
        recipientName = false;
        TextBoxErrorSkin(frmMyRecipientAddProfile.tbxRecipientName, txtErrorBG);
        showAlertRcMB(kony.i18n.getLocalizedString("RecipientExists"), kony.i18n.getLocalizedString("info"), "info");
        return false;
    }
    if (kony.string.containsChars(frmMyRecipientAddProfile.tbxRecipientName.text, charsArr)) {
        recipientName = false;
        showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_differentRcname"), kony.i18n.getLocalizedString("info"), "info");
        return false;
    }
    return true;
}

function addNewRecipientAddNewAcc(eventobject) {
    var recipientName = false;
    if (frmMyRecipientAddProfile.tbxRecipientName.text == null || frmMyRecipientAddProfile.tbxRecipientName.text == undefined || frmMyRecipientAddProfile.tbxRecipientName.text == "") {
        TextBoxErrorSkin(frmMyRecipientAddProfile.tbxRecipientName, txtErrorBG);
        showAlertRcMB(kony.i18n.getLocalizedString("keyRecipientBlank"), kony.i18n.getLocalizedString("info"), "info");
        return false;
    }
    /*if(frmMyRecipientAddProfile.tbxRecipientName.text.length < 3){
    	TextBoxErrorSkin(frmMyRecipientAddProfile.tbxRecipientName, txtErrorBG);
    	showAlertRcMB(kony.i18n.getLocalizedString("RecipientNameShort"), kony.i18n.getLocalizedString("info"), "info");
    	return false;
    }*/
    if (frmMyRecipientAddProfile.tbxRecipientName.text.length > 40) {
        TextBoxErrorSkin(frmMyRecipientAddProfile.tbxRecipientName, txtErrorBG);
        showAlertRcMB(kony.i18n.getLocalizedString("RecipientNameMaxlength"), kony.i18n.getLocalizedString("info"), "info");
        return false;
    }
    if (ThaiOrEnglish(frmMyRecipientAddProfile.tbxRecipientName.text)) {
        recipientName = true;
        for (var i = 0; i < myRecipientsRs.length; i++) {
            if (myRecipientsRs[i]["lblName"]["text"] != null && myRecipientsRs[i]["lblName"]["text"] != undefined) {
                if (ThaiOrEnglish(myRecipientsRs[i]["lblName"]["text"])) {
                    if (myRecipientsRs[i]["lblName"]["text"] == frmMyRecipientAddProfile.tbxRecipientName.text) {
                        recipientName = false;
                        TextBoxErrorSkin(frmMyRecipientAddProfile.tbxRecipientName, txtErrorBG);
                        showAlertRcMB(kony.i18n.getLocalizedString("RecipientExists"), kony.i18n.getLocalizedString("info"), "info")
                        return false;
                        //break;
                    }
                } else {
                    recipientName = true;
                }
            }
        }
        if (!CheckRecipientNameUniq(frmMyRecipientAddProfile.tbxRecipientName.text.replace(/^\s+|\s+$/g, ""), "")) {
            recipientName = false;
            TextBoxErrorSkin(frmMyRecipientAddProfile.tbxRecipientName, txtErrorBG);
            showAlertRcMB(kony.i18n.getLocalizedString("RecipientExists"), kony.i18n.getLocalizedString("info"), "info");
            return false;
        }
        if (kony.string.containsChars(frmMyRecipientAddProfile.tbxRecipientName.text, charsArr)) {
            recipientName = false;
            TextBoxErrorSkin(frmMyRecipientAddProfile.tbxRecipientName, txtErrorBG);
            showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_differentRcname"), kony.i18n.getLocalizedString("info"), "info");
            return false;
        }
    } else {
        if (!recipientNameAddValidations()) {
            return;
        } else {
            recipientName = true;
        }
    }
    var recipientMobile = null;
    var recipientEmail = null;
    if (frmMyRecipientAddProfile.tbxMobileNo.text == "" || frmMyRecipientAddProfile.tbxMobileNo.text == null || frmMyRecipientAddProfile.tbxMobileNo.text.trim() == "") {
        recipientMobile = true;
        frmMyRecipientAddProfile.tbxMobileNo.text == null;
    } else {
        var ph = frmMyRecipientAddProfile.tbxMobileNo.text;
        ph = stripDashPh(ph);
        recipientMobile = recipientMobileVal(ph);
        if (recipientMobile == false) {
            TextBoxErrorSkin(frmMyRecipientAddProfile.tbxMobileNo, txtErrorBG);
            showAlertRcMB(kony.i18n.getLocalizedString("keyEnteredMobileNumberisnotvalid"), kony.i18n.getLocalizedString("info"), "info");
            return;
        }
    }
    if (frmMyRecipientAddProfile.tbxEmail.text == "" || frmMyRecipientAddProfile.tbxEmail.text == null || frmMyRecipientAddProfile.tbxEmail.text.trim() == "") {
        recipientEmail = true;
        frmMyRecipientAddProfile.tbxEmail.text == null;
    } else {
        if (emailValidatn(frmMyRecipientAddProfile.tbxEmail.text) == false) {
            recipientEmail = false;
            TextBoxErrorSkin(frmMyRecipientAddProfile.tbxEmail, txtErrorBG);
            showAlertRcMB(kony.i18n.getLocalizedString("invalidEmail"), kony.i18n.getLocalizedString("info"), "info");
            return;
        } else {
            recipientEmail = true;
        }
    }
    if (!(recipientName && recipientEmail && recipientMobile)) {
        var i = 0,
            j = 0,
            k = 0;
        if (recipientName == false) {
            i = "Recipient Name Invalid\n";
        } else {
            i = "";
        }
        if (recipientEmail == false) {
            j = "Recipient Email Invalid\n";
        } else {
            j = "";
        }
        if (recipientMobile == false) {
            k = "Recipient Mobile Invalid\n";
        } else {
            k = "";
        }
        showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_correctdetails"), kony.i18n.getLocalizedString("info"), "info")
    } else {
        //cache new recipient details in current details
        isRecipientNew = true;
        if (eventobject.id == "btnNext") {
            if (recipientAddFromTransfer) {
                frmMyRecipientAddAcc.show();
            } else {
                myRecAccAddConfLoad();
                frmMyRecipientAddAccConf.show();
            }
        } else {
            frmMyRecipientAddAcc.show();
        }
    }
}

function onClickConfirmAddRcManually() {
    popTransactionPwd.tbxPopTransactionPwd.text = "";
    popTransactionPwd.btnPopTransactionCancel.onClick = transCancelAddRcManually;
    popTransactionPwd.btnPopTransactionConf.onClick = transConfirmAddRcManually;
    popTransactionPwd.show();
}

function transCancelAddRcManually() {
    popTransactionPwd.dismiss();
}

function transConfirmAddRcManually() {
    gblFlagTransPwdFlow = "addRcManually";
    checkVerifyPWDMBRC();
}

function onclickeditinRecipientDetail() {
    frmMyRecipientEditProfile.tbxRecipientName.text = frmMyRecipientDetail.lblName.text;
    frmMyRecipientEditProfile.tbxMobileNo.text = frmMyRecipientDetail.lblMobile.text;
    frmMyRecipientEditProfile.tbxEmail.text = frmMyRecipientDetail.lblEmail.text;
    frmMyRecipientEditProfile.tbxFbID.text = frmMyRecipientDetail.lblFbstudio3.text; //Modified by Studio Viz
    frmMyRecipientEditProfile.imgprofilepic.src = frmMyRecipientDetail.imgprofilepic.src;
    frmMyRecipientEditProfile.show();
}

function viewAccountShow() {
    //frmMyRecipientViewAccount.lblBank.text = kony.i18n.getLocalizedString("keyBank");
    frmMyRecipientViewAccount.lblNumber.text = kony.i18n.getLocalizedString("keyNumber");
    frmMyRecipientViewAccount.lblNickName.text = kony.i18n.getLocalizedString("Nickname");
    frmMyRecipientViewAccount.lblBankCD.text = frmMyRecipientDetail.segMyRecipientDetail.selectedItems[0]["lblBankCD"];
    var banName = getBankNameOfcurrentLocale(frmMyRecipientDetail.segMyRecipientDetail.selectedItems[0]["lblBankCD"]);
    frmMyRecipientViewAccount.lblBankName.text = banName; //frmMyRecipientDetail.segMyRecipientDetail.selectedItems[0]["lblBankName"]["text"];
    frmMyRecipientViewAccount.lblAccountNo.text = frmMyRecipientDetail.segMyRecipientDetail.selectedItems[0]["lblAccountNo"]["text"];
    frmMyRecipientViewAccount.lblNick.text = frmMyRecipientDetail.segMyRecipientDetail.selectedItems[0]["lblNick"]["text"];
    frmMyRecipientViewAccount.lblPersonalisedID.text = frmMyRecipientDetail.segMyRecipientDetail.selectedItems[0]["personalizedId"]["text"];
    if (frmMyRecipientDetail.segMyRecipientDetail.selectedItems[0]["lblAcctName"]["text"] != null && frmMyRecipientDetail.segMyRecipientDetail.selectedItems[0]["lblAcctName"]["text"] != "") {
        frmMyRecipientViewAccount.hbox47327209476200.setVisibility(true);
        frmMyRecipientViewAccount.lblAcctName.text = frmMyRecipientDetail.segMyRecipientDetail.selectedItems[0]["lblAcctName"]["text"];
        frmMyRecipientViewAccount.lblAccountName.text = kony.i18n.getLocalizedString("AccountName");
    } else {
        frmMyRecipientViewAccount.hbox47327209476200.setVisibility(false);
        frmMyRecipientViewAccount.lblAcctName.text = "";
        frmMyRecipientViewAccount.lblAccountName.text = "";
    }
    if (!(getORFTFlagIB(frmMyRecipientDetail.segMyRecipientDetail.selectedItems[0]["lblBankCD"]) == "Y")) {
        frmMyRecipientEditAccount.hbxAccName.setVisibility(true);
        frmMyRecipientEditAccount.hbxAccNameTbx.setVisibility(true);
    } else {
        frmMyRecipientEditAccount.hbxAccName.setVisibility(false);
        frmMyRecipientEditAccount.hbxAccNameTbx.setVisibility(false);
    }
}
// On swipe functionality for AddAccountComplete screen.
function myTap(myWidget, gestureInfo) {
    var swipeEnable = true;
    //handleMenuBtn();//for DEF299
    if (swipeEnable == true && gestureInfo.gestureType == 2 && gestureInfo.swipeDirection == 2) {
        handleMenuBtn();
    }
    if (flowSpa) {
        if (swipeEnable == true && gestureInfo.gestureType == 2 && gestureInfo.swipeDirection == 1) {
            var CurrForm = kony.application.getCurrentForm();
            CurrForm.scrollboxMain.scrollToEnd();
        }
    }
}

function getBankNameOfcurrentLocale(bankCd) {
    var locale = kony.i18n.getCurrentLocale();
    var bankName = "";
    for (var i = 0; i < globalSelectBankData.length; i++) {
        if (bankCd == globalSelectBankData[i][0]) {
            if (kony.string.startsWith(locale, "en", true) == true) bankName = globalSelectBankData[i][7];
            else bankName = globalSelectBankData[i][8];
            break;
        }
    }
    return bankName;
}
/**
 * Method to invoke service that deletes the receipent
 * @returns {}
 */
var gblFlagTransPwdFlow = null;

function startRcMBDeleteService() {
    var name = frmMyRecipientDetail.lblName.text;
    var mobile = addCrossPh(stripDashPh(frmMyRecipientDetail.lblMobile.text));
    var email = addCrossEmail(frmMyRecipientDetail.lblEmail.text);
    var facebook = frmMyRecipientDetail.lblFbstudio3.text; //Modified by Studio Viz
    //activityLogServiceCall("045", "", "01", "","Delete", name, mobile, email, facebook, "");
    var inputParams = {
        personalizedId: frmMyRecipients.segMyRecipient.selectedItems[0].recipientID,
        mobile: mobile,
        mail: email,
        fbid: facebook,
        rcName: name
    };
    showLoadingScreen();
    invokeServiceSecureAsync("ExecuteMyRecipientDeleteService", inputParams, startRcDeleteMBServiceAsyncCallback);
}
/**
 * Callback method for startRcDeleteService()
 * @returns {}
 */
function startRcDeleteMBServiceAsyncCallback(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == 0) {
            isChMyRecipientsRs = true;
            //sendDeleteNotification("recipient", "", "", "", "", "", "", "");
            frmMyRecipients.show();
        } else {
            showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_Error"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
        }
        kony.application.dismissLoadingScreen();
    }
}

function startExistingRcAccountMBEditService(accountId, nickname, accName, editStatus) {
    var BankName = "";
    for (var i = 0; i < globalSelectBankData.length; i++) {
        if (frmMyRecipientViewAccount.lblBankCD.text == globalSelectBankData[i][0]) {
            BankName = globalSelectBankData[i][2];
            break;
        }
    }
    if (editStatus == "Deleted") {
        var NickName = frmMyRecipientViewAccount.lblNick.text;
        var Number = frmMyRecipientViewAccount.lblAccountNo.text;
        //activityLogServiceCall("046", "", "01", "","Delete", NickName, BankName, Number, "", "");
        var inputParams = {
            personalizedAcctId: stripDashAcc(frmMyRecipientViewAccount.lblAccountNo.text),
            acctNickName: frmMyRecipientViewAccount.lblNick.text,
            acctStatus: "Deleted",
            personalizedId: currentRecipientDetails["rcId"],
            bankCD: frmMyRecipientViewAccount.lblBankCD.text,
            nickname: NickName,
            bankname: BankName,
            accnumber: Number,
            AccountName: frmMyRecipientViewAccount.lblAcctName.text
        };
        showLoadingScreen();
        invokeServiceSecureAsync("ExecuteMyRecipientAccountEditDeleteService", inputParams, startExistingRcAccountDeleteServiceMBAsyncCallback);
    } else {
        var NickName = frmMyRecipientViewAccount.lblNick.text + "+" + frmMyRecipientEditAccount.txtAccountNickName.text;
        var Number = stripDashAcc(frmMyRecipientViewAccount.lblAccountNo.text);
        //activityLogServiceCall("046", "", "01", "","Edit", NickName, BankName, Number, "", "");
        var inputParams = {
            personalizedAcctId: stripDashAcc(frmMyRecipientViewAccount.lblAccountNo.text),
            acctNickName: frmMyRecipientEditAccount.txtAccountNickName.text,
            acctStatus: "Added",
            personalizedId: currentRecipientDetails["rcId"],
            bankCD: frmMyRecipientViewAccount.lblBankCD.text,
            nickname: NickName,
            bankname: BankName,
            accnumber: Number,
            AccountName: accName,
            AccountNameOLDNEW: frmMyRecipientViewAccount.lblAcctName.text + " + " + accName
        };
        showLoadingScreen();
        invokeServiceSecureAsync("ExecuteMyRecipientAccountEditDeleteService", inputParams, startExistingRcAccountMBEditServiceAsyncCallback);
    }
}
/**
 * Callback method for startExistingRcAccountEditService
 * @param {} startExistingRcAccountEditService
 * @returns {}
 */
function startExistingRcAccountDeleteServiceMBAsyncCallback(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == 0) {
            isChMyRecipientsRs = true;
            isChRecipientAccountsRs = true;
            //sendDeleteNotification("account", "", "", "", "", "", "", "")
            frmMyRecipientDetail.show();
            kony.application.dismissLoadingScreen();
        } else {
            kony.application.dismissLoadingScreen();
            showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_Error"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
        }
    } else {
        if (status == 300) {
            showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
        }
    }
}
/**
 * Callback method for startExistingRcAccountEditService
 * @param {} startExistingRcAccountEditService
 * @returns {}
 */
function startExistingRcAccountMBEditServiceAsyncCallback(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == 0) {
            kony.application.dismissLoadingScreen();
            frmMyRecipientEditAccComplete.bankLogo.src = frmMyRecipientViewAccount.bankLogo.src
            frmMyRecipientEditAccComplete.lblNumber.text = frmMyRecipientViewAccount.lblNumber.text
            frmMyRecipientEditAccComplete.lblNickName.text = frmMyRecipientViewAccount.lblNickName.text
            frmMyRecipientEditAccComplete.lblAccountName.text = frmMyRecipientViewAccount.lblAccountName.text
            frmMyRecipientEditAccComplete.lblBankName.text = frmMyRecipientViewAccount.lblBankName.text
            frmMyRecipientEditAccComplete.lblAccountNo.text = frmMyRecipientViewAccount.lblAccountNo.text
            frmMyRecipientEditAccComplete.lblNick.text = frmMyRecipientEditAccount.txtAccountNickName.text
            if (!(getORFTFlagIB(frmMyRecipientDetail.segMyRecipientDetail.selectedItems[0]["lblBankCD"]) == "Y")) {
                frmMyRecipientEditAccComplete.lblAcctName.text = frmMyRecipientEditAccount.tbxAccName.text;
            } else {
                frmMyRecipientEditAccComplete.lblAcctName.text = frmMyRecipientViewAccount.lblAcctName.text;
            }
            frmMyRecipientEditAccCompletePreShow();
            frmMyRecipientEditAccComplete.show();
            originalAccNickname = true;
            isChMyRecipientsRs = true;
            isChRecipientAccountsRs = true;
        } else {
            kony.application.dismissLoadingScreen();
            showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_Error"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
        }
    }
}

function startExistingRcMBProfileEditService(param) {
    if (param == "imageNotAdded") {
        if (currentRecipientDetails.pic == "avatar_dis.png") {
            gblRcImageUrlList[0] = "";
        } else {
            if (currentRecipientDetails.pic.substring(0, 4) == "http") {
                gblRcImageUrlList[0] = currentRecipientDetails.pic;
            } else {
                var n = currentRecipientDetails.pic.split("/ImageRender");
                gblRcImageUrlList[0] = n[1];
            }
        }
    }
    var inputParams = {
        serviceID: "receipentEditService",
        personalizedId: currentRecipientDetails["rcId"],
        personalizedName: frmMyRecipientEditProfile.tbxRecipientName.text.replace(/^\s+|\s+$/g, ""),
        personalizedPictureId: gblRcImageUrlList[0], //frmMyRecipientEditProfile.imgprofilepic.src,
        personalizedMailId: frmMyRecipientEditProfile.tbxEmail.text,
        personalizedMobileNumber: frmMyRecipientEditProfile.tbxMobileNo.text,
        personalizedFacebookId: frmMyRecipientEditProfile.tbxFbID.text,
        personalizedStatus: "Added"
    };
    invokeServiceSecureAsync("receipentEditService", inputParams, startExistingRcMBProfileEditServiceAsyncCallback);
}
/**
 * Callback method for startExistingRcEditService()
 * @returns {}
 */
function startExistingRcMBProfileEditServiceAsyncCallback(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == 0) {
            if (currentRecipientDetails["name"] == frmMyRecipientEditProfile.tbxRecipientName.text.replace(/^\s+|\s+$/g, "")) {
                var name = "";
            } else {
                var name = currentRecipientDetails["name"] + "+" + frmMyRecipientEditProfile.tbxRecipientName.text.replace(/^\s+|\s+$/g, "");
            }
            if (currentRecipientDetails["mobile"] == frmMyRecipientEditProfile.tbxMobileNo.text) {
                var mobile = "";
            } else {
                if (currentRecipientDetails["mobile"] == "" || currentRecipientDetails["mobile"] == undefined) {
                    var mobile = "+" + stripDashPh(frmMyRecipientEditProfile.tbxMobileNo.text);
                } else if (frmMyRecipientEditProfile.tbxMobileNo.text == "" || frmMyRecipientEditProfile.tbxMobileNo.text == undefined) {
                    var mobile = stripDashPh(currentRecipientDetails["mobile"]) + "+";
                } else {
                    var mobile = stripDashPh(currentRecipientDetails["mobile"]) + "+" + stripDashPh(frmMyRecipientEditProfile.tbxMobileNo.text);
                }
            }
            if (currentRecipientDetails["email"] == null || currentRecipientDetails["email"] == undefined) currentRecipientDetails["email"] = "";
            if (currentRecipientDetails["email"] == frmMyRecipientEditProfile.tbxEmail.text) {
                var email = "";
            } else {
                if (currentRecipientDetails["email"] == "" || currentRecipientDetails["email"] == undefined) {
                    var email = "+" + addCrossEmail(frmMyRecipientEditProfile.tbxEmail.text);
                } else if (frmMyRecipientEditProfile.tbxEmail.text == "" || frmMyRecipientEditProfile.tbxEmail.text == undefined) {
                    var email = addCrossEmail(currentRecipientDetails["email"]) + "+";
                } else {
                    var email = addCrossEmail(currentRecipientDetails["email"]) + "+" + addCrossEmail(frmMyRecipientEditProfile.tbxEmail.text);
                }
            }
            if (currentRecipientDetails["facebook"] != undefined && currentRecipientDetails["facebook"] == frmMyRecipientEditProfile.tbxFbID.text) {
                var facebook = "";
            } else {
                if (currentRecipientDetails["facebook"] == "" || currentRecipientDetails["facebook"] == undefined) {
                    var facebook = "+" + frmMyRecipientEditProfile.tbxFbID.text;
                } else if (frmMyRecipientEditProfile.tbxFbID.text == "" || frmMyRecipientEditProfile.tbxFbID.text == undefined) {
                    var facebook = currentRecipientDetails["facebook"] + "+";
                } else {
                    var facebook = currentRecipientDetails["facebook"] + "+" + frmMyRecipientEditProfile.tbxFbID.text;
                }
            }
            activityLogServiceCall("045", "", "01", "", "Edit", name, mobile, email, facebook, "");
            kony.application.dismissLoadingScreen();
            isChMyRecipientsRs = true;
            isChRecipientAccountsRs = true;
            frmMyRecipientDetail.show();
        } else {
            kony.application.dismissLoadingScreen();
            showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_Error"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
        }
    } else {
        kony.application.dismissLoadingScreen();
        showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
    }
}
/**
 * Method to invoke service that Add Manually  new recipient  along with Bank accounts list
 * @returns {}
 */
function startNewRcAddwithAccntServiceMB() {
    var totalData = [];
    var name = frmMyRecipientAddProfile.tbxRecipientName.text;
    var PictureId = gblPhotoSource; //frmMyRecipientAddProfile.imgProfilePic.src;
    var Email = frmMyRecipientAddProfile.tbxEmail.text;
    var fbId = frmMyRecipientAddProfile.tbxFbID.text;
    var mobileNo = frmMyRecipientAddProfile.tbxMobileNo.text;
    var recepientdata = [name, PictureId, Email, mobileNo, fbId, "Added", "N"];
    gblRcPersonalizedIdList = name;
    totalData.push(recepientdata);
    var inputParams = {
        receipentList: totalData.toString()
    };
    showLoadingScreen();
    invokeServiceSecureAsync("receipentNewAddService", inputParams, startNewRcListAddWithAccMBServiceAsyncCallback);
}

function startNewRcListAddWithAccMBServiceAsyncCallback(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == 0) {
            isChMyRecipientsRs = true;
            if (gblRcBase64List != "") {
                imageServiceCall();
            }
            //call add account service
            var inputParams = {}
            var selectedRcId = callBackResponse["personalizedIdList"][0]["personalizedId"]
            var totalData = [];
            for (var i = 0; i < AddAccountList.length; i++) {
                var acctName = "";
                var BankName = AddAccountList[i].lblBankName.text;
                var Number = stripDashAcc(AddAccountList[i].lblAccountNo.text);
                var NickName = AddAccountList[i].lblNick.text;
                var bankCode = AddAccountList[i].bankCode;
                //var acctName = AddAccountList[i].lblAcctValue.text;
                if (AddAccountList[i] != undefined && AddAccountList[i].lblAcctValue != undefined) {
                    acctName = AddAccountList[i].lblAcctValue.text;
                }
                if (acctName == null || acctName == undefined || acctName == "") {
                    acctName = "Not returned from GSB";
                }
                var bankData = [crmId, selectedRcId, bankCode, Number, NickName, "Added", acctName];
                totalData.push(bankData);
            }
            inputParams["personalizedAccList"] = totalData.toString()
            invokeServiceSecureAsync("receipentAddBankAccntService", inputParams, addAccountWithRecServiceCallbackMB);
        } else {
            gblMixedCallsTrack1 = "done";
        }
        if (gblMixedCallsTrack1 == "") {
            gblMixedCallsTrack1 = "done";
        } else {
            kony.application.dismissLoadingScreen();
        }
    }
}

function addAccountWithRecServiceCallbackMB(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == 0) {
            isChRecipientAccountsRs = true;
            frmMyRecipientAddAccComplete.lblName.text = frmMyRecipientAddAccConf.lblName.text;
            if (frmMyRecipientAddAccConf.lblMobile.text == null || frmMyRecipientAddAccConf.lblMobile.text == "") {
                frmMyRecipientAddAccComplete.hbox47502979411849.setVisibility(false);
            } else {
                frmMyRecipientAddAccComplete.hbox47502979411849.setVisibility(true);
                frmMyRecipientAddAccComplete.lblMobile.text = frmMyRecipientAddAccConf.lblMobile.text;
            }
            if (frmMyRecipientAddAccConf.lblEmail.text == null || frmMyRecipientAddAccConf.lblEmail.text == "") {
                frmMyRecipientAddAccComplete.hbox47502979411850.setVisibility(false);
            } else {
                frmMyRecipientAddAccComplete.hbox47502979411850.setVisibility(true);
                frmMyRecipientAddAccComplete.lblEmail.text = frmMyRecipientAddAccConf.lblEmail.text;
            }
            if (frmMyRecipientAddAccConf.lblFbIDstudio5.text == null || frmMyRecipientAddAccConf.lblFbIDstudio5.text == "") { //Modified by Studio Viz
                frmMyRecipientAddAccComplete.hbox47502979411851.setVisibility(false);
            } else {
                frmMyRecipientAddAccComplete.hbox47502979411851.setVisibility(true);
                frmMyRecipientAddAccComplete.lblFbID.text = frmMyRecipientAddAccConf.lblFbIDstudio5.text; //Modified by Studio Viz
            }
            if (frmMyRecipientAddAccConf.imgprofilepic.rawBytes != null) {
                frmMyRecipientAddAccComplete.imgprofilepic.rawBytes = frmMyRecipientAddAccConf.imgprofilepic.rawBytes;
            } else {
                frmMyRecipientAddAccComplete.imgprofilepic.src = frmMyRecipientAddAccConf.imgprofilepic.src;
            }
            setToAddDatatoManualConfirmationSegMB();
            sendDeleteNotification("addrecipientacc", "", "", "", "", "", "", "");
        } else if (callBackResponse["opstatus"] == 1) {
            if (callBackResponse["errMsg"].search("DuplicateKeyException")) {
                showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_correctaccount"), kony.i18n.getLocalizedString("info"), "info")
                frmMyRecipients.show();
                return;
            } else {
                showAlertRcMB(callBackResponse["errMsg"], kony.i18n.getLocalizedString("info"), "info")
                frmMyRecipients.show();
                return;
            }
        } else if (callBackResponse["opstatus"] == 4) { //duplicate account addition
            showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_correctaccount"), kony.i18n.getLocalizedString("info"), "info")
            frmMyRecipients.show();
            return;
        } else {
            showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_Error"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
        }
        if (gblMixedCallsTrack1 == "") {
            gblMixedCallsTrack1 = "done";
        } else {
            kony.application.dismissLoadingScreen();
        }
    }
}

function setToAddDatatoManualConfirmationSegMB() {
    var tempData = [];
    tempData.push(frmMyRecipientAddAccConf.segMyRecipientDetail.data);
    var totalData = [];
    frmMyRecipientAddAccComplete.lblAccounts.isVisible = false;
    if (tempData[0] != null) {
        frmMyRecipientAddAccComplete.lblAccounts.isVisible = true;
        for (var i = 0; i < tempData[0].length; i++) {
            var acctName = "",
                acctNameLbl = "";
            //if(tempData[0][i].lblAccountName != "" || tempData[0][i].lblAcctValue != ""){
            acctName = tempData[0][i].lblAccountName;
            acctNameLbl = tempData[0][i].lblAcctValue;
            //	}
            var segDataMap = {
                bankLogo: tempData[0][i].bankLogo,
                //lblBank: tempData[0][i].lblBank,
                bankCode: tempData[0][i].bankCode,
                lblNumber: tempData[0][i].lblNumber,
                lblNickName: tempData[0][i].lblNickName,
                lblBankName: tempData[0][i].lblBankName,
                lblAccountNo: tempData[0][i].lblAccountNo,
                lblNick: tempData[0][i].lblNick,
                lblAccountName: acctName,
                lblAcctValue: acctNameLbl,
                "btnTransfer": {
                    text: kony.i18n.getLocalizedString("Transfer"),
                    skin: "btnBlueSkin"
                }
            };
            totalData.push(segDataMap);
        }
        frmMyRecipientAddAccComplete.segMyRecipientDetail.setData(totalData);
        if (frmMyRecipientAddAccConf.segMyRecipientDetail.data.length > 1) {
            frmMyRecipientAddAccComplete.lblAccounts.text = kony.i18n.getLocalizedString("Receipent_Accounts");
        } else {
            frmMyRecipientAddAccComplete.lblAccounts.text = kony.i18n.getLocalizedString("Receipent_Account");
        }
        isChMyRecipientsRs = true;
        isChRecipientAccountsRs = true;
        frmMyRecipientAddAccComplete.show();
    } else {
        showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_Error"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
    }
}

function addNewRecipientDetailsMB() {
    var inputParams = {
        receipentList: prepareNewRcListMB()
    };
    showLoadingScreen();
    invokeServiceSecureAsync("receipentNewAddService", inputParams, startNewRcListAddMBServiceAsyncCallback);
}
/**
 * Callback method for startNewRcFBListAddService()
 * @returns {}
 */
function startNewRcListAddMBServiceAsyncCallback(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == 0) {
            //alert("Profile without account");
            sendDeleteNotification("addrecipient", "", "", "", "", "", "", "");
            isChMyRecipientsRs = true;
            isChRecipientAccountsRs = true;
            frmMyRecipientAddAccComplete.lblName.text = frmMyRecipientAddAccConf.lblName.text;
            if (frmMyRecipientAddAccConf.lblMobile.text == null || frmMyRecipientAddAccConf.lblMobile.text == "") {
                frmMyRecipientAddAccComplete.hbox47502979411849.setVisibility(false);
            } else {
                frmMyRecipientAddAccComplete.hbox47502979411849.setVisibility(true);
                frmMyRecipientAddAccComplete.lblMobile.text = frmMyRecipientAddAccConf.lblMobile.text;
            }
            if (frmMyRecipientAddAccConf.lblEmail.text == null || frmMyRecipientAddAccConf.lblEmail.text == "") {
                frmMyRecipientAddAccComplete.hbox47502979411850.setVisibility(false);
            } else {
                frmMyRecipientAddAccComplete.hbox47502979411850.setVisibility(true);
                frmMyRecipientAddAccComplete.lblEmail.text = frmMyRecipientAddAccConf.lblEmail.text;
            }
            if (frmMyRecipientAddAccConf.lblFbIDstudio5.text == null || frmMyRecipientAddAccConf.lblFbIDstudio5.text == "") { //Modified by Studio Viz
                frmMyRecipientAddAccComplete.hbox47502979411851.setVisibility(false);
            } else {
                frmMyRecipientAddAccComplete.hbox47502979411851.setVisibility(true);
                frmMyRecipientAddAccComplete.lblFbID.text = frmMyRecipientAddAccConf.lblFbIDstudio5.text; //Modified by Studio Viz
            }
            if (flowSpa) {
                frmMyRecipientAddAccComplete.imgprofilepic.src = frmMyRecipientAddAccConf.imgprofilepic.src;
            } else {
                if (frmMyRecipientAddAccConf.imgprofilepic.rawBytes != null) {
                    frmMyRecipientAddAccComplete.imgprofilepic.rawBytes = frmMyRecipientAddAccConf.imgprofilepic.rawBytes;
                } else {
                    frmMyRecipientAddAccComplete.imgprofilepic.src = frmMyRecipientAddAccConf.imgprofilepic.src;
                }
            }
            frmMyRecipientAddAccComplete.segMyRecipientDetail.removeAll();
            frmMyRecipientAddAccComplete.lblAccounts.isVisible = false;
            if (AddAccountList.length > 0) {
                frmMyRecipientAddAccComplete.segMyRecipientDetail.setData(AddAccountList);
                frmMyRecipientAddAccComplete.lblAccounts.isVisible = true;
            }
            frmMyRecipientAddAccComplete.show();
            if (flowSpa) {
                if (profilePicFlag == "picture") {
                    var perID = callBackResponse["personalizedIdList"][0]["personalizedId"]
                    startImageServiceCallRecipientsSpa(perID);
                }
            } else {
                if (gblRcBase64List != "") {
                    imageServiceCall();
                }
            }
        } else if (callBackResponse["opstatus"] == 1) {
            if (callBackResponse["errMsg"] != null && callBackResponse["errMsg"] != undefined) {
                showAlertRcMB(callBackResponse["errMsg"], kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
                frmMyRecipients.show();
                return;
            }
        } else {
            showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_Error"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
        }
        kony.application.dismissLoadingScreen();
    }
}

function prepareNewRcListMB() {
    var totalData = [];
    var name = frmMyRecipientAddProfile.tbxRecipientName.text;
    var PictureId = gblPhotoSource; //frmMyRecipientAddProfile.imgProfilePic.src;
    var Email = frmMyRecipientAddProfile.tbxEmail.text;
    var fbId = frmMyRecipientAddProfile.tbxFbID.text;
    var mobileNo = frmMyRecipientAddProfile.tbxMobileNo.text;
    var recepientdata = [name, PictureId, Email, mobileNo, fbId, "Added", "N"];
    gblRcPersonalizedIdList = name;
    totalData.push(recepientdata);
    return totalData.toString();
}

function searchContacts(eventobject) {
    frmMyRecipientSelectMobile.label475095112172022.setVisibility(false);
    frmMyRecipientSelectMobile.segMyRecipient.setVisibility(true);
    var search = frmMyRecipientSelectMobile.textbox247327209467957.text;
    if (search != null && search != "") {
        if (kony.string.containsChars(frmMyRecipientSelectMobile.textbox247327209467957.text, charsArr)) {
            alert("Enter Valid Search String");
            return;
        }
    }
    var manualSearch = false;
    if (eventobject.id == "btnSearchRecipeints") {
        manualSearch = true;
    }
    if (search.length >= 1 || manualSearch) {
        var showList = new Array();
        var searchtxt = search;
        var j = 0;
        var regexp = new RegExp("(" + searchtxt + ")", "ig");
        for (i in ContactList) {
            if (ContactList[i]["lblName"]) {
                if (regexp.test(ContactList[i]["lblName"]) == true) {
                    showList[j] = ContactList[i];
                    j++;
                }
            }
        }
        if (showList.length == 0 || showList.length == undefined) {
            frmMyRecipientSelectMobile.label475095112172022.setVisibility(true);
            frmMyRecipientSelectMobile.segMyRecipient.setVisibility(false);
            frmMyRecipientSelectMobile.label475095112172022.text = kony.i18n.getLocalizedString("keybillernotfound");
            //showAlertRcMB(kony.i18n.getLocalizedString("keybillernotfound"), kony.i18n.getLocalizedString("info"), "info")
        } else {
            frmMyRecipientSelectMobile.segMyRecipient.setData(showList);
        }
    } else {
        if (ContactList != null && frmMyRecipientSelectMobile.segMyRecipient.data != null && frmMyRecipientSelectMobile.segMyRecipient.data.length != ContactList.length) frmMyRecipientSelectMobile.segMyRecipient.setData(ContactList);
    }
}

function permissionCheckForCameraMyReceipientProfile() {
    if (isAndroidM()) {
        kony.print(": CALLIING PERMISSIONS FFI....");
        var permissionsArr = [gblPermissionsJSONObj.CAMERA_GROUP];
        //Creates an object of class 'PermissionFFI'
        var RuntimePermissionsChkFFIObject = new MarshmallowPermissionChecks.RuntimePermissionsChkFFI();
        //Invokes method 'checkpermission' on the object
        RuntimePermissionsChkFFIObject.checkPermissions(permissionsArr, null, permissionCheckForCameraMyReceipientProfileCallBack);
    } else {
        navigateToPOPUpProfilePic();
    }
}

function permissionCheckForCameraMyReceipientProfileCallBack(result) {
    if (result == "1") {
        kony.print("IN FINAL CALLBACK PERMISSION IS AVAIALABLE");
        navigateToPOPUpProfilePic();
    } else {
        kony.print(" IN FINAL CALLBACK PERMISSION IS UNAVAIALABLE");
        var messageErr = "App cannot proceed further unless the required permisssions are granted!" //Need to chk with customer on msg text
        alert(messageErr);
        return false;
    }
}

function navigateToPOPUpProfilePic() {
    popProfilePic.btnPopDeleteCancel.onClick = openMediaGalleryinPopUp;
    popProfilePic.camera1.onCapture = invokeCameraFromPopup;
    popProfilePic.show();
}