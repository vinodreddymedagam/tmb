function frmAccountSummaryLandingMenuPostshow() { //k
    assignGlobalForMenuPostshow();
}

function onClickTnC() { //k
    kony.print("Insdie onClickTnC");
    //frmMBAnyIdRegTnC.btnCheckBox.skin = "btnCheckFoc";
    if (frmMBAnyIdRegTnC.btnCheckBox.skin == "btnCheck") {
        frmMBAnyIdRegTnC.btnCheckBox.skin = "btnCheckFoc";
    } else if (frmMBAnyIdRegTnC.btnCheckBox.skin == "btnCheckFoc") {
        frmMBAnyIdRegTnC.btnCheckBox.skin = "btnCheck";
    }
}

function onClickPhoneNum() { //k
    kony.print("Insdie onClickPhoneNum");
    if (frmMBActivateAnyId.checkboxgroupPhones.skin == "cbxNormal") {
        frmMBActivateAnyId.checkboxgroupPhones.skin = "cbxFocus";
        frmMBActivateAnyId.hboxPhoneNums.setVisibility(true);
    } else if (frmMBActivateAnyId.checkboxgroupPhones.skin == "cbxFocus") {
        frmMBActivateAnyId.checkboxgroupPhones.skin = "cbxNormal";
        frmMBActivateAnyId.hboxPhoneNums.setVisibility(false);
    }
}

function onSelectCitizenID() { //k
    kony.print("Insdie onSelectCitizenID");
    if (frmMBActivateAnyId.checkboxgroupCitizenid.skin == "cbxNormal") {
        frmMBActivateAnyId.checkboxgroupCitizenid.skin = "cbxFocus";
        frmMBActivateAnyId.hboxSocialNums.setVisibility(true);
    } else if (frmMBActivateAnyId.checkboxgroupCitizenid.skin == "cbxFocus") {
        frmMBActivateAnyId.checkboxgroupCitizenid.skin = "cbxNormal";
        frmMBActivateAnyId.hboxSocialNums.setVisibility(false);
    }
}

function OnClickAnyIDRegNext() { //k
    var phoneNum = frmMBActivateAnyId.lblPhoneNum.text;
    if (phoneNum != null && phoneNum != "" && phoneNum != undefined) {
        //showOTPPopupForOTPValidation(kony.i18n.getLocalizedString("keyOTP"), "ABCD", phoneNum, callBackOTPRequest);
    }
}

function onClickOfAnyIDCallBack() { //k
    frmMBAnyIdRegCompleted.hbxSuccess.setVisibility(false);
    frmMBAnyIdRegCompleted.hbxFail.setVisibility(true);
    frmMBAnyIdRegCompleted.lblAnyIdFailure.setVisibility(true);
    frmMBAnyIdRegCompleted.lblAnyIdSuccess.setVisibility(false);
    //frmMBAnyIdRegCompleted.lblAnyIdSuccessAdditional.setVisibility(false);
    frmMBAnyIdRegCompleted.lblAnyIdSuccessEdit.setVisibility(false);
    frmMBAnyIdRegCompleted.show();
}

function frmRegAnyIdAnnouncementPostShow() //k
{
    gblAnyIdAnnoceShowedCount = parseInt(gblAnyIdAnnoceShowedCount) + 1;
    //storing the count in device if application is force closed after form display.
    kony.store.setItem("AnyIdAnnoceShowedCount", gblAnyIdAnnoceShowedCount);
}
// this method is used to get the max and min amounts which are configurable
function getMinMaxAmounts() { //k
    var inputParams = {};
    invokeServiceSecureAsync("getMinMaxAmountVals", inputParams, startMinMaxAmtsAsyncCallback);
}
// this method is used to get the min and max amounts and set to global variables
function startMinMaxAmtsAsyncCallback(status, collectionData) { //k
    if (status == 400) {
        if (collectionData["opstatus"] == 0) {
            gblSSTrnsLmtMax = collectionData["fromMaxAmount"];
            gblSSTrnMaxofMin = collectionData["fromMinAmount"];
            gblSSTrnMinofMax = collectionData["toMaxAmount"];
            gblSSTrnsLmtMin = collectionData["toMinAmount"];
            if (gblSSApply == "true") {
                frmSSService.txtBalMax.text = commaFormatted(collectionData["fromMaxDefaultAmount"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                frmSSService.txtBalMin.text = commaFormatted(collectionData["toMinDefaultAmount"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
            }
        } else {
            s2s_errorCode = collectionData["opstatus"];
            s2s_activityStatus = "02";
            activityLogServiceCall(s2s_activityTypeID, s2s_errorCode, s2s_activityStatus, s2s_deviceNickName, s2s_activityFlexValues1, s2s_activityFlexValues2, s2s_activityFlexValues3, s2s_activityFlexValues4, s2s_activityFlexValues5, s2s_logLinkageId);
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}
// populate segmentslider information on confirm page
function s2sSegmentSliderMappingForConfirm() { //k
    frmSSConfirmation.hboxApplyDate.isVisible = false;
    if (flowSpa) {
        gblnormSelIndex = gbltranFromSelIndex[1];
        gblmbSpaselectedData = frmSSService.segSliderSpa.data[gblnormSelIndex];
        frmSSConfirmation.imgLinkAcc.src = gblmbSpaselectedData.img1;
        frmSSConfirmation.lblLinkAccName.text = gblmbSpaselectedData.lblCustName;
        var amnt1 = gblmbSpaselectedData.lblBalance;
        frmSSConfirmation.lblLinkAccBal.text = amnt1;
        frmSSConfirmation.lblLinkAccType.text = gblmbSpaselectedData.lblSliderAccN2;
        frmSSConfirmation.lblAcc.text = gblmbSpaselectedData.lblACno;
        frmSSConfirmation.lblLinkAccNo.text = gblmbSpaselectedData.lblActNoval;
    } else {
        frmSSConfirmation.imgLinkAcc.src = frmSSService["segSlider"]["selectedItems"][0].img1;
        frmSSConfirmation.lblLinkAccName.text = frmSSService["segSlider"]["selectedItems"][0]["lblCustName"];
        var amnt1 = frmSSService["segSlider"]["selectedItems"][0]["lblBalance"];
        frmSSConfirmation.lblLinkAccBal.text = amnt1;
        frmSSConfirmation.lblLinkAccType.text = frmSSService["segSlider"]["selectedItems"][0]["lblSliderAccN2"];
        frmSSConfirmation.lblAcc.text = frmSSService["segSlider"]["selectedItems"][0]["lblACno"];
        frmSSConfirmation.lblLinkAccNo.text = frmSSService["segSlider"]["selectedItems"][0]["lblActNoval"];
    }
}
// populate NoFixedAcct information on confirm page
function s2sNoFixedAcctMappingForConfirm() { //k
    //frmSSConfirmation.imgNoFix.src = frmSSService.image2448366816169765.src;
    frmSSConfirmation.lblNFAccName.text = frmSSService.lblNFAccName.text;
    var amnt1 = frmSSService.lblNFAccBal.text;
    frmSSConfirmation.lblNFAccBal.text = amnt1;
    frmSSConfirmation.lblAccNo.text = frmSSService.lblAccNo.text;
    frmSSConfirmation.lblNFAccNo.text = frmSSService.lblNFAccNo.text;
    frmSSConfirmation.lblNFAccType.text = frmSSService.lblNFAccType.text;
}

function loadTermsNConditionForS2SMB() { //k
    var input_param = {};
    var locale = kony.i18n.getCurrentLocale();
    if (locale == "en_US") {
        input_param["localeCd"] = "en_US";
    } else {
        input_param["localeCd"] = "th_TH";
    }
    input_param["moduleKey"] = "SendtoSave";
    invokeServiceSecureAsync("readUTFFile", input_param, loadTNCS2SCallBackMB);
}

function loadTNCS2SCallBackMB(status, result) { //k
    if (status == 400) {
        if (result["opstatus"] == 0) {
            frmSSTnC.lblTnCMsg.text = result["fileContent"];
        } else {
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}

function saveInputinSessionMB() { //k
    var inputParam = [];
    showLoadingScreen();
    invokeServiceSecureAsync("tokenSwitching", inputParam, saveInputTokeSwithchInSessionMBCallBack);
}

function saveInputTokeSwithchInSessionMBCallBack(status, resulttable) { //k
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            var txtBalMaxVal = frmSSService.txtBalMax.text
            txtBalMaxVal = txtBalMaxVal.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
            var txtBalMinVal = frmSSService.txtBalMin.text
            txtBalMinVal = txtBalMinVal.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
            inputParam = {};
            //inputParam["linkedAcctValue"] = removeUnwatedSymbols(frmSSService.lblFromAccNo.text);
            if (gblSSApply == "true") {
                if (flowSpa) {
                    // gbltranFromSelIndex = [0, 0];
                    gblnormSelIndex = gbltranFromSelIndex[1];
                    gblmbSpaselectedData = frmSSService.segSliderSpa.data[gblnormSelIndex];
                    inputParam["linkedAcctValue"] = removeUnwatedSymbols(gblmbSpaselectedData.lblActNoval);
                } else inputParam["linkedAcctValue"] = removeUnwatedSymbols(frmSSService["segSlider"]["selectedItems"][0]["lblActNoval"]);
                inputParam["s2sType"] = "ApplyS2S";
            } else {
                inputParam["linkedAcctValue"] = removeUnwatedSymbols(frmSSService.lblFromAccNo.text);
                inputParam["s2sType"] = "EditS2S";
            }
            inputParam["partyId"] = removeUnwatedSymbols(frmSSService.lblNFAccNo.text);
            inputParam["maxAmt"] = removeCommas(txtBalMaxVal);
            inputParam["minAmt"] = removeCommas(txtBalMinVal);
            invokeServiceSecureAsync("sendToSaveApplyValidationService", inputParam, saveInputinSessioncallBackMB);
        }
    }
}

function saveInputinSessioncallBackMB(status, result) { //k
    if (status == 400) {
        dismissLoadingScreen();
        if (result["opstatus"] == 0) {
            if (gblSSApply == "false") {
                checkBusinessHoursForS2SEditDel();
            }
            dismissLoadingScreen();
        }
    }
}

function frmMBSoGooodTnCMenuPostshow() { //k
    if (gblCallPrePost) {
        // Add the new code in the post show please add here
    }
    assignGlobalForMenuPostshow();
}

function callApplySoGooODService(transactionDataList, times) { //k
    loadExcutedSoGooODTxnsInSegment(transactionDataList);
}

function disableNextPlanList() { //k
    frmMBSoGooodPlanList.btnNext.setEnabled(false);
    frmMBSoGooodPlanList.btnNext.skin = btnDisabledGray;
    frmMBSoGooodPlanList.btnNext.focusSkin = btnDisabledGray;
}

function onApplySogoodEmailClick() { //k
    if (gblEmailTCSelect == false) {
        gblEmailTCSelect = true;
    } else {
        gblEmailTCSelect = false;
    }
    if (gblEmailTCSelect) {
        alert(kony.i18n.getLocalizedString("keyTandCApplySoGooODPopUp"));
    }
}

function onClickOfCancelSogooodIB() { //k 
    frmIBAccntSummary.show();
}

function calculateInterestOnplanSelect(planId, plan, channel) { //channel is to check whther for IB or MB
    gblPlan = {
        "planId": planId,
        "plan": plan,
        "installment": "333.33" + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
        "interestVal": "0.00" + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
        "totalAmount": "1,000.00" + " " + kony.i18n.getLocalizedString("currencyThaiBaht")
    };
    if (channel == "MB") {
        frmMBSoGooodPlanSelect.lblMonthlyInstallmentVal.text = "333.33" + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
        frmMBSoGooodPlanSelect.lblTotalInterestValue.text = "0.00" + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
        frmMBSoGooodPlanSelect.lblTotalAmountVal.text = "1,000" + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
    } else {
        frmIBApplySoGooODPlanList.lblMonthlyInstallmentVal.text = "333.33" + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
        frmIBApplySoGooODPlanList.lblTotalInterestValue.text = "0.00" + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
        frmIBApplySoGooODPlanList.lblTotalAmountValue.text = "1,000" + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
    }
}

function onClickOfMBBankAssuranceSummaryBack() { //k  
    var curr_form_name = kony.application.getCurrentForm().id; //Added to fix blank screen issue on clicking of alert popups in Menu
    if (curr_form_name != "frmAccountSummaryLanding") TMBUtil.DestroyForm(frmAccountSummaryLanding);
    gaccType = "";
    glb_viewName = "";
    gblAccountTable = "";
    GLOBAL_DEBITCARD_TABLE = ""
    if (flowSpa) {
        isMenuShown = false
            //frmAccountSummaryLanding = null;
            //     frmAccountSummaryLandingGlobals();    
        TMBUtil.DestroyForm(frmAccountSummaryLanding);
        gblAccountTable = "";
    }
    callCustomerAccountService()
    frmAccountSummaryLanding.lblAccntHolderName.text = gblCustomerName;
}

function searchSuggestedBillPayment() { //k
    search = "";
    // alert("Search Text "+ frmIBMyBillersHome.tbxSearch.text)
    search = frmIBBillPaymentLP.txtBPSearch.text;
    if (search.length >= 3) {
        frmIBBillPaymentLP.lblBPSgstdBillerList.setVisibility(true);
        //frmIBBillPaymentLP.segSuggestedBillersList.setVisibility(true);
        isSearchedBiller = true;
        getBillerLPSuggestListIB();
    } else {
        //frmIBBillPaymentLP.lblBPSgstdBillerList.setVisibility(false);
        //frmIBBillPaymentLP.segBPSgstdBillerList.removeAll();
        //onBillPaySelectMoreDynamicIB(MySelectBillSuggestListRs);
        //isSearchedBiller = false;
        //getBillerLPSuggestListIB();
    }
}

function getBillerLPSuggestListIB() { //k
    if (isSearchedBiller) var inputParams = {
        billerName: search,
        IsActive: "1",
        BillerGroupType: gblGroupTypeBiller,
        flagBillerList: "IB"
            //clientDate: getCurrentDate()not working on IE 8
    };
    else var inputParams = {
        IsActive: "1",
        BillerGroupType: gblGroupTypeBiller,
        flagBillerList: "IB"
            // clientDate: getCurrentDate()not working on IE 8
    };
    showLoadingScreenPopup();
    invokeServiceSecureAsync("masterBillerInquiry", inputParams, getBillerLPSuggestListIBsyncCallback);
}

function getBillerLPSuggestListIBsyncCallback(status, callBackResponse) { //k
    if (status == 400) {
        if (callBackResponse["opstatus"] == "0") {
            var responseData = callBackResponse["MasterBillerInqRs"];
            if (responseData.length > 0) {
                setBillerCollectionDataBillPay(callBackResponse["MasterBillerInqRs"]);
                // getMyBillListIB(); // calling customer Bill Inquiry	
                dismissLoadingScreenPopup();
            } else {
                var collectionData = [];
                setBillerCollectionDataBillPay(collectionData)
                frmIBBillPaymentLP.segBPSgstdBillerList.removeAll();
                dismissLoadingScreenPopup();
                //alert(kony.i18n.getLocalizedString("keyNoSuggestBillers"));
            }
        }
    }
}

function setBillerCollectionDataBillPay(collectionData) { //k
    var tmpRef2Len = "";
    var tmpRef2label = "";
    var tmpRef2Len = 0;
    var billername = "";
    var ref1label = "";
    var ref2label = "";
    var tempRecord = [];
    var tmpIsRef2Req = "";
    if (kony.i18n.getCurrentLocale() == "en_US") {
        billername = "BillerNameEN";
        ref1label = "LabelReferenceNumber1EN";
        ref2label = "LabelReferenceNumber2EN";
    } else if (kony.i18n.getCurrentLocale() == "th_TH") {
        billername = "BillerNameTH";
        ref1label = "LabelReferenceNumber1TH";
        ref2label = "LabelReferenceNumber2TH";
    }
    gblBillerLastSearchNoCat.length = 0;
    for (var i = 0; i < collectionData.length; i++) {
        if (collectionData[i]["BillerGroupType"] == gblGroupTypeBiller) {
            tmpIsRef2Req = collectionData[i]["IsRequiredRefNumber2Add"];
            //if (tmpIsRef2Req == "Y" || tmpIsRef2Req == "y") {
            tmpRef2Len = collectionData[i]["Ref2Len"];
            tmpRef2label = collectionData[i][ref2label];
            /*} else {
            	tmpRef2Len = 0;
            	tmpRef2Label = "";
            }*/
            //ENH113 
            gblbillerStartTime = "";
            gblbillerEndTime = "";
            gblbillerFullPay = "";
            gblbillerAllowSetSched = "";
            gblbillerTotalPayAmtEn = "";
            gblbillerTotalPayAmtTh = "";
            gblbillerTotalIntEn = "";
            gblbillerTotalIntTh = "";
            gblbillerDisconnectAmtEn = "";
            gblbillerDisconnectAmtTh = "";
            gblbillerServiceType = "";
            gblbillerTransType = "";
            gblMeaFeeAmount = "";
            gblbillerAmountEn = "";
            gblbillerAmountTh = "";
            gblbillerMeterNoEn = "";
            gblbillerMeterNoTh = "";
            gblbillerCustNameEn = "";
            gblbillerCustNameTh = "";
            gblbillerCustAddressEn = "";
            gblbillerCustAddressTh = "";
            if (collectionData[i]["BillerCompcode"] == "2533") {
                if (collectionData[i]["BillerMiscData"] != undefined && collectionData[i]["BillerMiscData"].length > 0) {
                    for (var j = 0; j < collectionData[i]["BillerMiscData"].length; j++) {
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "BILLER.StartTime") {
                            gblbillerStartTime = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "BILLER.EndTime") {
                            gblbillerEndTime = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "BILLER.FullPayment") {
                            gblbillerFullPay = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "BILLER.AllowSetSchedule") {
                            gblbillerAllowSetSched = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD1.LABEL.EN") {
                            gblbillerMeterNoEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD1.LABEL.TH") {
                            gblbillerMeterNoTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD2.LABEL.EN") {
                            gblbillerCustNameEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD2.LABEL.TH") {
                            gblbillerCustNameTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD3.LABEL.EN") {
                            gblbillerCustAddressEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD3.LABEL.TH") {
                            gblbillerCustAddressTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD4.LABEL.EN") {
                            gblbillerTotalPayAmtEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD4.LABEL.TH") {
                            gblbillerTotalPayAmtTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD5.LABEL.EN") {
                            gblbillerAmountEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD5.LABEL.TH") {
                            gblbillerAmountTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD6.LABEL.EN") {
                            gblbillerTotalIntEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD6.LABEL.TH") {
                            gblbillerTotalIntTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD7.LABEL.EN") {
                            gblbillerDisconnectAmtEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD7.LABEL.TH") {
                            gblbillerDisconnectAmtTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "BILLER.ServiceType") {
                            gblbillerServiceType = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "BILLER.TransactionType") {
                            gblbillerTransType = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                    }
                }
            }
            kony.print("gblbillerStartTime: " + gblbillerStartTime + "gblbillerEndTime :" + gblbillerEndTime);
            //ENH113 end
            if (kony.i18n.getCurrentLocale() == "en_US") {
                var billerName = collectionData[i]["BillerNameEN"] + "(" + collectionData[i]["BillerCompcode"] + ")";
            } else if (kony.i18n.getCurrentLocale() == "th_TH") {
                var billerName = collectionData[i]["BillerNameTH"] + "(" + collectionData[i]["BillerCompcode"] + ")";
            }
            var imagesUrl = BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + collectionData[i]["BillerCompcode"];
            var tempRecord = {
                "lblSgstdBillerName": billerName,
                "lblSgstdBillerNameEN": collectionData[i]["BillerNameEN"] + "(" + collectionData[i]["BillerCompcode"] + ")",
                "lblSgstdBillerNameTH": collectionData[i]["BillerNameTH"] + "(" + collectionData[i]["BillerCompcode"] + ")",
                "btnAddSgstdBiller": {
                    "skin": "btnIBaddsmall"
                },
                "imgSgstdBiller": {
                    "src": imagesUrl
                },
                "BillerID": collectionData[i]["BillerID"],
                "BillerCompCode": collectionData[i]["BillerCompcode"],
                "Ref1Label": collectionData[i][ref1label],
                "Ref2Label": tmpRef2label,
                "Ref2Len": tmpRef2Len,
                "Ref1EN": collectionData[i]["LabelReferenceNumber1EN"],
                "Ref1TH": collectionData[i]["LabelReferenceNumber1TH"],
                "Ref2EN": collectionData[i]["LabelReferenceNumber2EN"],
                "Ref2TH": collectionData[i]["LabelReferenceNumber2TH"],
                "BillerGroupType": collectionData[i]["BillerGroupType"],
                "ToAccountKey": collectionData[i]["ToAccountKey"],
                "IsRequiredRefNumber2Add": collectionData[i]["IsRequiredRefNumber2Add"],
                "IsRequiredRefNumber2Pay": collectionData[i]["IsRequiredRefNumber2Pay"],
                "EffDt": collectionData[i]["EffDt"],
                "BillerMethod": collectionData[i]["BillerMethod"],
                "BillerCategoryID": collectionData[i]["BillerCategoryID"],
                "Ref1Len": collectionData[i]["Ref1Len"],
                "IsFullPayment": collectionData[i]["IsFullPayment"],
                "BarcodeOnly": collectionData[i]["BarcodeOnly"],
                "billerStartTime": gblbillerStartTime,
                "billerEndTime": gblbillerEndTime,
                "billerFullPay": gblbillerFullPay,
                "billerAllowSetSched": gblbillerAllowSetSched,
                "billerTotalPayAmtEn": gblbillerTotalPayAmtEn,
                "billerTotalPayAmtTh": gblbillerTotalPayAmtTh,
                "billerTotalIntEn": gblbillerTotalIntEn,
                "billerTotalIntTh": gblbillerTotalIntTh,
                "billerDisconnectAmtEn": gblbillerDisconnectAmtEn,
                "billerDisconnectAmtTh": gblbillerDisconnectAmtTh,
                "billerServiceType": gblbillerServiceType,
                "billerTransType": gblbillerTransType,
                "billerFeeAmount": gblMeaFeeAmount,
                "billerAmountEn": gblbillerAmountEn,
                "billerAmountTh": gblbillerAmountTh,
                "billerMeterNoEn": gblbillerMeterNoEn,
                "billerMeterNoTh": gblbillerMeterNoTh,
                "billerCustNameEn": gblbillerCustNameEn,
                "billerCustNameTh": gblbillerCustNameTh,
                "billerCustAddressEn": gblbillerCustAddressEn,
                "billerCustAddressTh": gblbillerCustAddressTh,
                "billerBancassurance": collectionData[i]["IsBillerBancassurance"],
                "allowRef1AlphaNum": collectionData[i]["AllowRef1AlphaNum"]
            }
            gblBillerLastSearchNoCat.push(tempRecord);
        }
    }
    sugBillerCatChangeBillPay();
}

function sugBillerCatChangeBillPay() { //k
    var showCatSuggestList = [];
    var tmpCatSelected = frmIBBillPaymentLP.cbxBillCategory.selectedKey;
    var j = 0;
    var tmp = "";
    var lengthBillersSuggestList = gblBillerLastSearchNoCat.length;
    if (null != tmpCatSelected && tmpCatSelected != 0) {
        for (j = 0, i = 0; i < lengthBillersSuggestList; i++) {
            if (gblBillerLastSearchNoCat[i].BillerCategoryID.text == tmpCatSelected || gblBillerLastSearchNoCat[i].BillerCategoryID == tmpCatSelected) {
                showCatSuggestList[j] = gblBillerLastSearchNoCat[i];
                j++;
            }
        }
        gblTopUpSelectFormDataHolderIB = showCatSuggestList;
        onBillPaySelectMoreDynamicIB(gblTopUpSelectFormDataHolderIB);
        //frmIBMyBillersHome.segSuggestedBillersList.setData(showCatSuggestList);
    } else {
        onBillPaySelectMoreDynamicIB(gblBillerLastSearchNoCat);
        //frmIBMyBillersHome.segSuggestedBillersList.setData(gblBillerLastSearchNoCat);
    }
}

function onBillPaySelectMoreDynamicIB(gblTopUpSelectFormDataHolderIB) { //k
    var currentForm = kony.application.getCurrentForm();
    var maxLength = kony.os.toNumber(GLOBAL_LOAD_MORE); //configurable parameter also initilaize gblTopUpMore to max length
    gblTopUpMore = gblTopUpMore + maxLength;
    var newData = [];
    var totalData = gblTopUpSelectFormDataHolderIB; //data from static population 
    var totalLength = totalData.length;
    if (totalLength == 0) {
        frmIBBillPaymentLP.lblBPSgstdBillerList.setVisibility(false);
        frmIBBillPaymentLP.segBPSgstdBillerList.removeAll();
    } else {
        frmIBBillPaymentLP.lblBPSgstdBillerList.setVisibility(true);
    }
    // if (totalLength > gblTopUpMore) {
    //     frmIBBillPaymentLP.lblBPSgstdBillerList.setVisibility(true);
    //    var endPoint = gblTopUpMore;
    //} else {
    // 	frmIBBillPaymentLP.lblBPSgstdBillerList.setVisibility(true);
    var endPoint = totalLength;
    //}
    for (i = 0; i < totalLength; i++) {
        newData.push(gblTopUpSelectFormDataHolderIB[i])
    }
    frmIBBillPaymentLP.segBPSgstdBillerList.data = newData;
    dismissLoadingScreenPopup();
    frmIBBillPaymentLP.txtBPSearch.setFocus(true);
}

function searchBillPayBillsIB() { //k
    var showSearchList = [];
    var showCatList = [];
    var searchCustomerData = [];
    var searchMasterData = [];
    var lowerCaseName = "";
    var showSearchSuggestList = [];
    search = "";
    // alert("Search Text "+ frmIBMyBillersHome.tbxSearch.text)
    search = frmIBBillPaymentLP.txtBPSearch.text;
    var tmpCatSelected = frmIBBillPaymentLP.cbxBillCategory.selectedKey;
    var j = 0;
    var lengthBillersList = MySelectBillListRs.length;
    if (search.length >= 3) {
        frmIBBillPaymentLP.lblBPSgstdBillerList.setVisibility(true);
        var searchtxt = search.toLowerCase();
        var lowerCaseNameBiller = "";
        // var regexp = new RegExp("(" + searchtxt + ")", "ig");
        for (j = 0, i = 0; i < lengthBillersList; i++) {
            if (MySelectBillListRs[i]["lblBillsName"]) {
                lowerCaseName = MySelectBillListRs[i]["lblBillsName"].toLowerCase();
                // added biller name search to fix DEF1512
                lowerCaseNameBiller = MySelectBillListRs[i]["lblBillCompCode"].toLowerCase();
                //if (regexp.test(tmpBillerData[i]["lblBillerNickname"]["text"]) == true) {
                // if (lowerCaseName.contains(searchtxt)) {
                //if (kony.string.startsWith(lowerCaseName, searchtxt, true)) {
                if (lowerCaseName.indexOf(searchtxt) >= 0 || lowerCaseNameBiller.indexOf(searchtxt) >= 0) {
                    showSearchList[j] = MySelectBillListRs[i];
                    j++;
                }
            }
        }
        searchCustomerData = showSearchList;
    } else {
        searchCustomerData = MySelectBillListRs;
        //frmIBBillPaymentLP.lblBPSgstdBillerList.setVisibility(false);
        //frmIBBillPaymentLP.segBPSgstdBillerList.removeAll();
    }
    if (null != tmpCatSelected && tmpCatSelected != 0) {
        var searchLen = searchCustomerData.length;
        for (j = 0, i = 0; i < searchLen; i++) {
            tmp = searchCustomerData[i].BillerCategoryID
            if (tmpCatSelected == tmp) {
                showCatList[j] = searchCustomerData[i];
                j++;
            }
        }
        searchCustomerData = showCatList;
    } else {
        showCatList = searchCustomerData;
    }
    //frmIBMyBillersHome.segSuggestedBillersList.setData(showSearchSuggestList);
    onMyCustBillPaySelectMoreDynamicIB(showCatList);
    //frmIBMyBillersHome.segBillersList.setData(showCatList);
}

function onMyCustBillPaySelectMoreDynamicIB(gblCustTopUpSelectFormDataHolderIB) { //k
    var currentForm = kony.application.getCurrentForm();
    var maxLength = kony.os.toNumber(GLOBAL_LOAD_MORE); //configurable parameter also initilaize gblCustTopUpMore to max length
    gblCustTopUpMore = gblCustTopUpMore + maxLength;
    var newData = [];
    var totalData = gblCustTopUpSelectFormDataHolderIB; //data from static population 
    var totalLength = totalData.length;
    if (totalLength == 0) {
        frmIBBillPaymentLP.lblBPBillsList.setVisibility(false);
        frmIBBillPaymentLP.segBPBillsList.removeAll();
    } else {
        frmIBBillPaymentLP.lblBPBillsList.setVisibility(true);
    }
    if (totalLength > gblCustTopUpMore) {
        // currentForm.hbxMore.setVisibility(true);
        var endPoint = gblCustTopUpMore;
    } else {
        //currentForm.hbxMore.setVisibility(false);
        var endPoint = totalLength;
    }
    for (i = 0; i < endPoint; i++) {
        newData.push(gblCustTopUpSelectFormDataHolderIB[i])
    }
    frmIBBillPaymentLP.segBPBillsList.data = newData;
}

function eh_frmIBBillPaymentLP_btnAddBill_onClick() { //k
    if (checkMaxBillerCount()) {
        /* 
addSuggestedBillPaymentIB.call(this);

 */
        //gblAddToMyBill =true;
        frmIBMyBillersHome.segBillersConfirm.removeAll();
        frmIBMyBillersHome.imgTMBLogo.setVisibility(false);
        frmIBMyBillersHome.hbxBillersAddContainer.setVisibility(true);
        frmIBMyBillersHome.hbxBillersConfirmContainer.setVisibility(false);
        frmIBMyBillersHome.hbxBillersEditContainer.setVisibility(false);
        frmIBMyBillersHome.hbxBillersViewContainer.setVisibility(false);
        frmIBMyBillersHome.hbxCanConfBtnContainer.setVisibility(false);
        frmIBMyBillersHome.hbxBillersCompleteContainer.setVisibility(false);
        frmIBMyBillersHome.lblAddBillerName.text = "";
        frmIBMyBillersHome.txtAddBillerNickName.text = "";
        frmIBMyBillersHome.txtAddBillerRef1.text = "";
        //frmIBMyBillersHome.lblAddBillerRef1.text=kony.i18n.getLocalizedString("keyRef1");
        //frmIBMyBillersHome.lblAddBillerRef2.text=kony.i18n.getLocalizedString("keyRef2");
        frmIBMyBillersHome.txtAddBillerRef2.text = "";
        frmIBMyBillersHome.imgAddBillerLogo.src = "empty.png"
        frmIBMyBillersHome.imgArrowAddBiller.setVisibility(true);
        frmIBMyBillersHome.imgArrowSegBiller.setVisibility(false);
        frmIBMyBillersHome.txtAddBillerNickName.setEnabled(false);
        frmIBMyBillersHome.txtAddBillerRef1.setEnabled(false);
        frmIBMyBillersHome.txtAddBillerRef2.setEnabled(false);
        showLoadingScreenPopup();
        frmIBMyBillersHome.show()
    } else {
        var alert_seq2_act0 = kony.ui.Alert({
            "message": kony.i18n.getLocalizedString("Valid_MoreThan50"),
            "alertType": constants.ALERT_TYPE_ERROR,
            "alertTitle": "",
            "yesLabel": "Ok",
            "noLabel": "",
            "alertIcon": "",
            "alertHandler": null
        }, {});
    }
}

function eh_frmBillPayment_preshow() {
    if (!flowSpa) {
        frmBillPayment.scrollboxMain.scrollToEnd();
    }
    frmBillPaymentPreShow();
    isMenuShown = false;
    isSignedUser = true;
    gblMyBillerTopUpBB = 0;
    GblBillTopFlag = true;
    frmBillPayment.tbxMyNoteValue.numberOfVisibleLines = 1;
    if (gblFirstTimeBillPayment) {
        launchBillPaymentFirstTime();
    }
    if (gblPaynow) {
        frmBillPayment.lblPayBillOnValue.text = getFormattedDate(currentSystemDate(), kony.i18n.getCurrentLocale());
    }
    if (gblFullPayment) {}
    DisableFadingEdges(frmBillPayment);
};

function eh_frmBillPayment_postshow() {
    if (flowSpa) {
        commonMBPostShow();
        frmBillPayment.scrollboxMain.scrollToEnd();
    }
    kony.print("SPA====>>>>");
    frmBillPayment.tbxAmount.textInputMode = constants.TEXTBOX_INPUT_MODE_ANY;
    frmBillPayment.tbxAmount.maxTextLength = 16;
    if (GblBillTopFlag) {
        gblFirstTimeBillPayment = false;
    } else {
        gblFirstTimeTopUp = false;
    }
};

function eh_frmBillPayment_btnNext_onClick() {
    validateChannelMBBillPay();
    getAccountNameOfUser(frmBillPayment["segSlider"]["selectedItems"][0].lblActNoval);
};

function toggleKeyPadMB(command) {
    isConfirmPin = command;
    var isConfirmBox = frmMBAssignAtmPin.hbxAtmPinConfirm.isVisible;
    /*if(!isConfirmBox){
    	if(command){
    		frmIBCardActivation.hbxKeyPad.margin = [0,5,0,0];
    	}else{
    		frmIBCardActivation.hbxKeyPad.margin = [0,28.7,0,0];
    	}
    }*/
}

function getAccountNameOfUser(accountNum) {
    gblAccountNameOfUser = "";
    var inputparam = {};
    inputparam["acctId"] = replaceCommon(accountNum, "-", "");
    invokeServiceSecureAsync("depositAccountInquiryNonSec", inputparam, getAccountNameOfUserCallBackIB);
}

function getAccountNameOfUserCallBackIB(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            gblAccountNameOfUser = resulttable["accountTitle"];
        } else {
            alert(" " + resulttable["errMsg"]);
        }
    } else if (status == 300) {}
}

function onDoneEditingAmountBillPayment() {
    frmBillPayment.tbxAmount.text = onDoneEditingAmountValue(frmBillPayment.tbxAmount.text);
}

function eh_frmBillPayment_segSlider_onswipe(seguiWidget, sectionIndex, rowIndex) {
    if (!flowSpa) {
        var modScroll = new getBillPaymentIndex();
        modScroll.paginationSwitch(sectionIndex, rowIndex);
    }
};

function eh_frmBillPayment_btnselBill_onClick() {
    frmSelectBiller.linkMoreBpTopUp.text = kony.i18n.getLocalizedString("More");
    if (flowSpa) {
        gblMyBillerTopUpBB = "0";
        GblBillTopFlag = true
        showLoadingScreen();
        gblmbSpaselectedData = "";
        gblnormSelIndex = gbltranFromSelIndex[1];
        gblmbSpaselectedData = frmBillPayment.segSliderSpa.data[gblnormSelIndex];
        frmSelectBiller.show();
    } else {
        gblMyBillerTopUpBB = "0";
        GblBillTopFlag = true
        showLoadingScreen();
        frmSelectBiller.segMyBills.removeAll();
        frmSelectBiller.show();
    }
};

function eh_frmBillPayment_btnselBill_onClick() {
    if (flowSpa) {
        gblMyBillerTopUpBB = "0";
        GblBillTopFlag = true;
        showLoadingScreen();
        gblmbSpaselectedData = "";
        gblnormSelIndex = gbltranFromSelIndex[1];
        gblmbSpaselectedData = frmBillPayment.segSliderSpa.data[gblnormSelIndex];
        //alert("the array is after"+gbltranFromSelIndex);
        //alert("the index is"+gblnormSelIndex);
        frmSelectBiller.show();
    } else {
        gblMyBillerTopUpBB = "0";
        GblBillTopFlag = true;
        showLoadingScreen();
        frmSelectBiller.segMyBills.removeAll();
        frmSelectBiller.show();
    }
}

function eh_frmBillPayment_button475004897849_onClick() {
    fullMinSpecButtonColorSet();
    fullPress();
};

function fullMinSpecButtonColorSet(eventObject) {
    frmBillPayment.button475004897849.skin = btnScheduleEndLeft;
    frmBillPayment.button475004897851.skin = btnScheduleMid;
    frmBillPayment.button475004897853.skin = btnScheduleEndRight;
    var buttonId = eventObject.id;
    if (kony.string.equalsIgnoreCase(buttonId, "button475004897849")) {
        eventObject.skin = "btnScheduleLeftFocus";
    } else if (kony.string.equalsIgnoreCase(buttonId, "button475004897851")) {
        eventObject.skin = "btnScheduleMidFocus";
    } else if (kony.string.equalsIgnoreCase(buttonId, "button475004897853")) {
        eventObject.skin = "btnScheduleRightFocus";
    }
}

function eh_frmBillPayment_tbxAmount_onBeginEditing() {
    frmBillPayment.tbxAmount.text = '';
};

function eh_frmBillPayment_tbxAmount_onEndEditing() {
    if (frmBillPayment.tbxAmount.text == "") {
        //frmBillPayment.tbxAmount.text = frmBillPayment.tbxAmount.placeholder;
        //"0.00" + kony.i18n.getLocalizedString("currencyThaiBaht");
    } else {
        frmBillPayment.tbxAmount.text = commaFormatted(parseFloat(removeCommos(frmBillPayment.tbxAmount.text)).toFixed(2));
        frmBillPayment.tbxAmount.placeholder = commaFormatted(parseFloat(removeCommos(frmBillPayment.tbxAmount.text)).toFixed(2));
    }
};

function eh_frmBillPayment_btnSpecified2_onClick(eventobject) {
    fullSpecButtonColorSet.call(this, eventobject);
    frmBillPayment.tbxAmount.setVisibility(true);
    frmBillPayment.tbxAmount.placeholder = commaFormatted(fullAmt);
    frmBillPayment.lblForFullPayment.setVisibility(false);
    frmBillPayment.tbxAmount.text = commaFormatted(fullAmt);
    gblFullPayment = false;
};

function act0_frmBillPaymentConfirmationFuture_btnCancel_onClick_seq0(response) {
    if (response == true) {
        frmBillPaymentConfirmationFuture_btnCancel_onClick_seq1()
    } else {}
};

function act0_frmBillPaymentConfirmationFuture_btnCancelSpa_onClick_seq0(response) {
    if (response == true) {
        frmBillPaymentConfirmationFuture_btnCancelSpa_onClick_seq1()
    } else {}
};

function eh_frmBillPaymentConfirmationFuture_btnCancelSpa_onClick(eventobject) {
    /*var alert_seq0_act0 = kony.ui.Alert({
        "message": kony.i18n.getLocalizedString("keyCancelTransaction"),
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": "Cancel Transaction",
        "yesLabel": "Yes",
        "noLabel": "No",
        "alertIcon": "",
        "alertHandler": act0_frmBillPaymentConfirmationFuture_btnCancelSpa_onClick_seq0}, {});*/
    frmBillPaymentConfirmationFuture_btnCancelSpa_onClick_seq1()
};

function eh_frmBillPaymentConfirmationFuture_btnCancel_onClick(eventobject) {
    /*var alert_seq0_act0 = kony.ui.Alert({
        "message": kony.i18n.getLocalizedString("keyCancelTransaction"),
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": "",
        "yesLabel": kony.i18n.getLocalizedString("keyConfirm"),
        "noLabel": kony.i18n.getLocalizedString("keyCancelButton"),
        "alertIcon": "",
        "alertHandler": act0_frmBillPaymentConfirmationFuture_btnCancel_onClick_seq0}, {});*/
    frmBillPaymentConfirmationFuture_btnCancel_onClick_seq1()
};

function eh_frmBillPaymentComplete_btnEmailto_onClick() {
    gblPOWcustNME = gblCustomerName;
    if (GblBillTopFlag) {
        gblPOWtransXN = "BillPayment";
    } else {
        gblPOWtransXN = "TopUpPayment";
    }
    kony.print("gblPOWtransXN " + gblPOWtransXN);
    gblPOWchannel = "MB";
    requestFromForm = "frmBillPaymentComplete";
    postOnWall();
};

function eh_frmBillPaymentComplete_hbox101271281131304_onClick() {
    /*if (gblBpBalHideUnhide == "true") {
        frmBillPaymentComplete.lblBalanceAfterPayment.setVisibility(true);
        frmBillPaymentComplete.lblBalAfterPayValue.setVisibility(false);
        frmBillPaymentComplete.lblHide.text = kony.i18n.getLocalizedString("keyUnhide");
        gblBpBalHideUnhide = "false";
    } else {
        frmBillPaymentComplete.lblBalanceAfterPayment.setVisibility(true);
        frmBillPaymentComplete.lblBalAfterPayValue.setVisibility(true);
        frmBillPaymentComplete.lblHide.text = kony.i18n.getLocalizedString("Hide");
        gblBpBalHideUnhide = "true";
    }*/
    if (gblDisplayBalanceBillPayment) {
        frmBillPaymentComplete.hbxBalanceAfter.setVisibility(false);
        frmBillPaymentComplete.hbxFreeTrans.setVisibility(false);
        frmBillPaymentComplete.lblHide.text = kony.i18n.getLocalizedString("show");
        gblDisplayBalanceBillPayment = false;
    } else {
        frmBillPaymentComplete.hbxBalanceAfter.setVisibility(true);
        if (frmBillPaymentConfirmationFuture.hbxFreeTrans.isVisible) {
            //if (gblBillpaymentNoFee){
            frmBillPaymentComplete.hbxFreeTrans.setVisibility(true);
        } else {
            frmBillPaymentComplete.hbxFreeTrans.setVisibility(false);
        }
        frmBillPaymentComplete.lblHide.text = kony.i18n.getLocalizedString("Hide");
        gblDisplayBalanceBillPayment = true;
    }
};

function eh_frmBillPaymentComplete_btnCancel_onClick() {
    TMBUtil.DestroyForm(frmBillPayment);
    TMBUtil.DestroyForm(frmBillPaymentConfirmationFuture);
    onClickOfAccountDetailsBack();
    gblScannedBiller = false;
};

function eh_frmBillPaymentComplete_btnConfirm_onClick() {
    callMasterBillerService();
};

function eh_frmBillPaymentComplete_btnCancelSpa_onClick() {
    TMBUtil.DestroyForm(frmBillPayment);
    TMBUtil.DestroyForm(frmBillPaymentConfirmationFuture);
    onClickOfAccountDetailsBack();
};

function eh_frmBillPaymentComplete_btnConfirmSpa_onClick() {
    TMBUtil.DestroyForm(frmSelectBiller);
    TMBUtil.DestroyForm(frmBillPaymentConfirmationFuture);
    if (GblBillTopFlag) {
        gblMyBillerTopUpBB = 0;
        callBillPaymentCustomerAccountService();
    } else {
        gblMyBillerTopUpBB = 1;
        callBillPaymentCustomerAccountService();
    }
};

function eh_frmSelectBiller_frmSelectBiller_postshow() {
    if (flowSpa) {
        commonMBPostShow.call(this);
    }
}

function eh_frmSelectBiller_segSuggestedBillers_onRowClick() {
    if (gblBlockBillerTopUpAdd) {
        kony.print("Adding biller is disabled");
    } else {
        if (checkMaxBillerCountMB()) {
            onclickOfSelectedSuggestedBillersAdd();
        } else {
            var alert_seq4_act0 = kony.ui.Alert({
                "message": kony.i18n.getLocalizedString("Valid_MoreThan50"),
                "alertType": constants.ALERT_TYPE_ERROR,
                "alertTitle": "",
                "yesLabel": "Ok",
                "noLabel": "",
                "alertIcon": "",
                "alertHandler": null
            }, {});
        }
    }
};

function eh_frmSelectBiller_btnSelectCat_onClick() {
    if (GblBillTopFlag) {
        popUpMyBillers.lblFlag.text = "frmSelectBiller";
        startDisplayTopUpCategoryService();
    } else {
        popUpMyBillers.lblFlag.text = "frmSelectBiller";
        startDisplayTopUpCategoryService();
    }
};

function frmBillPaymentBillerCategoriesMenuPostshow() {
    assignGlobalForMenuPostshow();
}

function frmBillPaymentEditMenuPostshow() {
    assignGlobalForMenuPostshow();
}

function frmScheduleBillPayEditFutureMenuPostshow() {}
/**below method is aded to fix DEF14198 and DEF14380
 * below method will add the params to HTTP get url
 * sample below url when cmp_type to Y
 * http://www.tmbbank.com/cpm/index-en.php?cust_name=Worawut&cust_surname=Nathasan&account_no=1234567890&balance=100,000
 */
function getCampaignUrl() {
    var params = "?";
    if (undefined != gblCustomerName && null != gblCustomerName && "" != gblCustomerName) {
        params = params + "cust_name=" + gblCustomerName + "&";
    }
    if (undefined != gblCustomerSurName && null != gblCustomerSurName && "" != gblCustomerSurName) {
        params = params + "cust_surname=" + gblCustomerName + "&";
    }
    if (undefined != gblCmpAccountNo && null != gblCmpAccountNo && "" != gblCmpAccountNo) {
        params = params + "account_no=" + gblCustomerName + "&";
    }
    if (undefined != gblCmpBalance && null != gblCmpBalance && "" != gblCmpBalance) {
        params = params + "balance=" + gblCustomerName;
    }
    return params;
}

function frmScheduleBillPayEditFutureMenuPreshow() {
    frmScheduleBillPayEditFuturePreShow.call(this);
    frmScheduleBillPayEditFuture.calScheduleStartDate.validStartDate = currentDateForcalender();
    frmScheduleBillPayEditFuture.calScheduleEndDate.validStartDate = currentDateForcalender();
    frmScheduleBillPayEditFuture.tbxAfterTimes.keyBoardStyle = constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD;
}

function frmBillPaymentConfirmationFutureMenuPostshow() {
    if (gblCallPrePost) {
        eh_frmBillPaymentConfirmationFuture_postshow.call(this);
    }
    assignGlobalForMenuPostshow();
}

function mbValidateAtmCardPin() {
    showLoadingScreen();
    var atmPin = decryptPinDigit(glbPin);
    encryptVerifyCardPinUsingE2EE(atmPin);
}

function activationCreditCard(cardNumber) {
    var inputParam = {};
    var cardNumber = "4215839200005555";
    inputParam["cardId"] = cardNumber;
    inputParam["applyChannel"] = "MB";
    inputParam["cardFn"] = "A";
    showLoadingScreen();
    invokeServiceSecureAsync("cardDetailAdd", inputParam, activationCreditCardCallBack);
}

function activationCreditCardCallBack(status, resulttable) {
    if (status == 400) {
        dismissLoadingScreen();
        if (resulttable["opstatus"] == 0 && resulttable["StatusCode"] == 0) {
            alert("Activate Complete");
            return true;
        } else {
            var errorCode = resulttable["XPServerStatCode"];
            alert(kony.i18n.getLocalizedString("ActCC_Err_FailAct") + " (" + errorCode + ")");
            return false;
        }
    } else {
        alert(kony.i18n.getLocalizedString("ActCC_Err_FailAct"));
        return false;
    }
}

function showfrmCardActivationDetails() {
    frmCardActivationDetails.show();
}

function showCreditCardManual() {
    frmCardActivationDetails.show();
}

function showCCActivationComplete() {
    frmCardActivationComplete.show();
}

function onClickBtnShare(currForm) {
    var deviceHght = gblDeviceInfo.deviceHeight;
    var osType = gblDeviceInfo.name;
    //	alert("Screen Name is: "+currForm);
    if (currForm.hboxSaveCamEmail.isVisible == false) {
        currForm.hboxSaveCamEmail.isVisible = true;
        currForm.imgHeaderRight.src = "arrowtop.png"
        currForm.imgHeaderMiddle.src = "empty.png"
        currForm.btnRight.skin = "btnShareFoc";
        /*if (deviceHght > 567 && osType == "iPhone") {
        	currForm.scrollbox474135225108084.containerHeight = 71.5; //previous value 69.5
        } else
        	currForm.scrollbox474135225108084.containerHeight = 66.2;	 // previous value 64
        */
    } else {
        currForm.hboxSaveCamEmail.isVisible = false;
        currForm.imgHeaderMiddle.src = "arrowtop.png"
        currForm.imgHeaderRight.src = "empty.png"
        currForm.btnRight.skin = "btnShare";
        /*if (deviceHght > 567 && osType == "iPhone") {
        	currForm.scrollbox474135225108084.containerHeight = 80.5; // previous value 78.5
        } else
        	currForm.scrollbox474135225108084.containerHeight = 77;  // previous value 75
        */
    }
}
// function to control on click of Header Right button Share
function toMBConnectAccPS() {
    var lblText = kony.i18n.getLocalizedString("keyConnectAccount");
    getHeader(0, lblText, 0, 0, 0)
    hbxArrow.imgHeaderMiddle.src = "arrowtopblue.png";
}

function sessionCheckAddResetPwd() {
    showLoadingScreen();
    var inputparam = {};
    invokeServiceSecureAsync("duplicateSessionCheckMB", inputparam, callBackSessionCheckMB);
}

function changeMobileTransLimit_PostShow_Snippet() {
    frmChangeMobNoTransLimitMB.scrollboxMain.scrollToEnd();
    //frmChangeMobNoTransLimitMB.txtChangeMobileNumber.textInputMode=constants.TEXTBOX_INPUT_MODE_ANY;
    //frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.textInputMode=constants.TEXTBOX_INPUT_MODE_ANY;
    isMenuRendered = false;
    isMenuShown = false;
    isSignedUser = true;
    frmChangeMobNoTransLimitMB.txtChangeMobileNumber.keyBoardStyle = constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD;
    frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.keyBoardStyle = constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD;
}

function calBackEditProfileLockAlert(response) {
    if (response == true) {
        crmprofilemodCallStatusLock(); //onIdleTimeOut();
    }
}

function callBackExchangeRates(status, resulttable) {
    try {
        //
        var exgRateResultTable = [];
        var updatedTime = "";
        if (status == 400) {
            if (resulttable["opstatus"] == 0) {
                for (var i = 0; i < resulttable["exchangeRates"].length; i++) {
                    var tempData = [];
                    var countryCode = resulttable["exchangeRates"][i]["countryCode"];
                    var flagImage = setExchangeRateFlag(countryCode);
                    updatedTime = resulttable["exchangeRates"][i]["updateDateTime"];
                    //
                    tempData = {
                        imgFalg: {
                            src: flagImage.trim()
                        },
                        lblCurrency: resulttable["exchangeRates"][i]["description"],
                        lblSells: resulttable["exchangeRates"][i]["sellingNotes"],
                        lblBuys: resulttable["exchangeRates"][i]["buyingNotes"]
                    };
                    exgRateResultTable.push(tempData);
                }
                frmExchangeRate.segExchangeRates.setData(exgRateResultTable);
                if (flowSpa) {
                    frmExchangeRate.lblTimeSpa.text = updatedTime;
                } else {
                    frmExchangeRate.lblTime.text = updatedTime;
                }
                dismissLoadingScreen();
                if (resulttable["exchangeRates"].length == 0) {
                    alert("Data not found please try again later.");
                } else {
                    frmExchangeRate.show();
                }
            } else {
                dismissLoadingScreen();
                alert(kony.i18n.getLocalizedString("ECGenericError"));
            }
        }
    } catch (e) {}
}

function onclickDisclaimer() {
    try {
        if (flowSpa) {
            popExgRateDisclaimer.btnClose.skin = "btnBlueClose";
            popExgRateDisclaimer.btnClose.focusSkin = "btnBlueClose";
            popExgRateDisclaimer.skin = "popUpSpaWhiteBg";
            popExgRateDisclaimer.lbl1.skin = "lblBalckSmall";
            popExgRateDisclaimer.lbl2.skin = "lblBalckSmall";
            popExgRateDisclaimer.lbl3.skin = "lblBalckSmall";
            popExgRateDisclaimer.lbl4.skin = "lblBalckSmall";
        }
        popExgRateDisclaimer.lbl1.text = "* " + kony.i18n.getLocalizedString("ExgRages_Disclm1");
        popExgRateDisclaimer.lbl2.text = "** " + kony.i18n.getLocalizedString("ExgRages_Disclm2");
        popExgRateDisclaimer.lbl3.text = "*** " + kony.i18n.getLocalizedString("ExgRages_Disclm3");
        popExgRateDisclaimer.lbl4.text = "**** " + kony.i18n.getLocalizedString("ExgRages_Disclm4");
        popExgRateDisclaimer.btnClose.text = kony.i18n.getLocalizedString("ExgRates_Close");
        popExgRateDisclaimer.show();
    } catch (e) {}
}

function frmAppTour_PostShow() {
    if (flowSpa) {
        commonMBPostShow();
        frmAppTour.scrollboxMain.scrollToEnd();
    }
    //frmAppTourPreShow();
}

function IBLogoutServiceForceChng() {
    showLoadingScreenPopup();
    inputParam = {};
    inputParam["channelId"] = "01";
    inputParam["timeOut"] = GBL_Time_Out;
    invokeServiceSecureAsync("logOutTMB", inputParam, callBackIBLogoutForceChangeService);
}

function callBackIBLogoutForceChangeService(status, resulttable) {
    if (status == 400) {
        gblRetryCountRequestOTP = "0";
        if (flowSpa) {
            frmSPALogin.show();
        } else {
            frmIBPreLogin.show();
        }
    }
}
/**
 * Method to validate the passwords for IB
 * @returns {}
 */
function validatePasswordChngProfileIB() {
    if (frmIBForceChangePassword.txtCurrentPwd.text == null || frmIBForceChangePassword.txtCurrentPwd.text == "") {
        showAlertIB(kony.i18n.getLocalizedString("keyEmptyCurrentPassIB"), kony.i18n.getLocalizedString("info"));
        frmIBForceChangePassword.txtCurrentPwd.setFocus(true);
        frmIBForceChangePassword.btnForcePwdConfirm.setEnabled(false);
        return false
    }
    if (frmIBForceChangePassword.txtUserId.text == null || frmIBForceChangePassword.txtUserId.text == "") {
        showAlertIB(kony.i18n.getLocalizedString("keyEnterUserId"), kony.i18n.getLocalizedString("info"));
        frmIBForceChangePassword.txtUserId.setFocus(true);
        frmIBForceChangePassword.btnForcePwdConfirm.setEnabled(false);
        return false
    }
    if (frmIBForceChangePassword.txtPassword.text == null || frmIBForceChangePassword.txtPassword.text == "") {
        showAlertIB(kony.i18n.getLocalizedString("keyEmptyNewPasswordIB"), kony.i18n.getLocalizedString("info"));
        frmIBForceChangePassword.txtPassword.setFocus(true);
        frmIBForceChangePassword.btnForcePwdConfirm.setEnabled(false);
        return false
    }
    if (frmIBForceChangePassword.txtConfirmPassword.text == null || frmIBForceChangePassword.txtConfirmPassword.text == "") {
        showAlertIB(kony.i18n.getLocalizedString("keyEnterConfirmPassword"), kony.i18n.getLocalizedString("info"));
        frmIBForceChangePassword.txtConfirmPassword.setFocus(true);
        frmIBForceChangePassword.btnForcePwdConfirm.setEnabled(false);
        return false
    }
    frmIBForceChangePassword.btnForcePwdConfirm.setEnabled(true); // Business rule 1
    frmIBForceChangePassword.btnForcePwdConfirm.skin = "btnIB158";
    frmIBForceChangePassword.btnForcePwdConfirm.focusSkin = "btnIB158active";
    //Business rule 2,3,4,5 are implemted at backend
    if (frmIBForceChangePassword.txtUserId.text.length < 8 || frmIBForceChangePassword.txtUserId.text.length > 20) {
        showAlertIB(kony.i18n.getLocalizedString("keyUserIdMinRequirement"), kony.i18n.getLocalizedString("info"));
        frmIBForceChangePassword.txtUserId.setFocus(true);
        frmIBForceChangePassword.txtUserId.text = "";
        return;
    }
    if (!(frmIBForceChangePassword.txtUserId.text.match(/[a-z]/ig))) {
        showAlertIB(kony.i18n.getLocalizedString("keyUserIdMinRequirement"), kony.i18n.getLocalizedString("info"));
        frmIBForceChangePassword.txtUserId.setFocus(true);
        frmIBForceChangePassword.txtUserId.text = "";
        return;
    }
    if ((frmIBForceChangePassword.txtPassword.text.length < 8 || frmIBForceChangePassword.txtPassword.text.length > 20)) {
        showAlertIB(kony.i18n.getLocalizedString("keyPasswordMinRequirement"), kony.i18n.getLocalizedString("info"));
        frmIBForceChangePassword.txtPassword.setFocus(true);
        return;
    }
    if (!(frmIBForceChangePassword.txtPassword.text.match(/[a-z]/gi) != null && frmIBForceChangePassword.txtPassword.text.match(/[0-9]/g) != null)) {
        showAlertIB(kony.i18n.getLocalizedString("keyPasswordMinRequirement"), kony.i18n.getLocalizedString("info"));
        frmIBForceChangePassword.txtPassword.setFocus(true);
        return;
    }
    if (frmIBForceChangePassword.txtPassword.text != frmIBForceChangePassword.txtConfirmPassword.text) {
        showAlertIB(kony.i18n.getLocalizedString("PassNoMatch"), kony.i18n.getLocalizedString("info"));
        frmIBForceChangePassword.txtPassword.text = "";
        frmIBForceChangePassword.txtConfirmPassword.text = "";
        frmIBForceChangePassword.txtPassword.setFocus(true);
        return;
    }
    /*if (frmIBForceChangePassword.txtCurrentPwd.text == frmIBForceChangePassword.txtPassword.text || frmIBForceChangePassword.txtCurrentPwd.text ==
    	frmIBForceChangePassword.txtConfirmPassword.text) {
    	showAlertIB(kony.i18n.getLocalizedString("KeyIncorrectPWD"), kony.i18n.getLocalizedString("info"));
    	return;
    }
    if (frmIBForceChangePassword.txtPassword.text  == frmIBForceChangePassword.txtUserId.text) {
    	showAlertIB(kony.i18n.getLocalizedString("keyUseridEqualsNewPassIB"), kony.i18n.getLocalizedString("info"));
    	return;
    }*/
    invokeMigrationUserComposite(frmIBForceChangePassword.txtCurrentPwd.text, frmIBForceChangePassword.txtPassword.text, frmIBForceChangePassword.txtUserId.text);
}

function validatePasswordFrcChngProfileSpa() {
    if (frmSpaMigrationSetuserid.txtCnfCrntPwd.text == null || frmSpaMigrationSetuserid.txtCnfCrntPwd.text == "") {
        showAlertRcMB(kony.i18n.getLocalizedString("keyEmptyCurrentPassIB"), kony.i18n.getLocalizedString("info"));
        frmSpaMigrationSetuserid.txtCnfCrntPwd.setFocus(true);
        frmSpaMigrationSetuserid.btnConfirmSpa.setEnabled(false);
        return false
    }
    if (frmSpaMigrationSetuserid.txtUserID.text == null || frmSpaMigrationSetuserid.txtUserID.text == "") {
        showAlertRcMB(kony.i18n.getLocalizedString("keyEnterUserId"), kony.i18n.getLocalizedString("info"));
        frmSpaMigrationSetuserid.txtUserID.setFocus(true);
        frmSpaMigrationSetuserid.btnConfirmSpa.setEnabled(false);
        return false
    }
    if (frmSpaMigrationSetuserid.txtTransPass.text == null || frmSpaMigrationSetuserid.txtTransPass.text == "") {
        showAlertRcMB(kony.i18n.getLocalizedString("keyEmptyNewPasswordIB"), kony.i18n.getLocalizedString("info"));
        frmSpaMigrationSetuserid.txtTransPass.setFocus(true);
        frmSpaMigrationSetuserid.btnConfirmSpa.setEnabled(false);
        return false
    }
    if (frmSpaMigrationSetuserid.txtConfirmPassword.text == null || frmSpaMigrationSetuserid.txtConfirmPassword.text == "") {
        showAlertRcMB(kony.i18n.getLocalizedString("keyEnterConfirmPassword"), kony.i18n.getLocalizedString("info"));
        frmSpaMigrationSetuserid.txtConfirmPassword.setFocus(true);
        frmSpaMigrationSetuserid.btnConfirmSpa.setEnabled(false);
        return false
    }
    frmSpaMigrationSetuserid.btnConfirmSpa.setEnabled(true); // Business rule 1
    frmSpaMigrationSetuserid.btnConfirmSpa.skin = "btnBlueSkin";
    frmSpaMigrationSetuserid.btnConfirmSpa.focusSkin = "btnBlueSkin";
    //Business rule 2,3,4,5 are implemted at backend
    if (frmSpaMigrationSetuserid.txtUserID.text.length < 8 || frmSpaMigrationSetuserid.txtUserID.text.length > 20) {
        showAlertRcMB(kony.i18n.getLocalizedString("keyUserIdMinRequirement"), kony.i18n.getLocalizedString("info"));
        frmSpaMigrationSetuserid.txtUserID.setFocus(true);
        frmSpaMigrationSetuserid.txtUserID.text = "";
        return;
    }
    if (!(frmSpaMigrationSetuserid.txtUserID.text.match(/[a-z]/ig))) {
        showAlertRcMB(kony.i18n.getLocalizedString("keyUserIdMinRequirement"), kony.i18n.getLocalizedString("info"));
        frmSpaMigrationSetuserid.txtUserID.setFocus(true);
        frmSpaMigrationSetuserid.txtUserID.text = "";
        return;
    }
    if ((frmSpaMigrationSetuserid.txtTransPass.text.length < 8 || frmSpaMigrationSetuserid.txtTransPass.text.length > 20)) {
        showAlertRcMB(kony.i18n.getLocalizedString("keyPasswordMinRequirement"), kony.i18n.getLocalizedString("info"));
        frmSpaMigrationSetuserid.txtTransPass.setFocus(true);
        return;
    }
    if (!(frmSpaMigrationSetuserid.txtTransPass.text.match(/[a-z]/gi) != null && frmSpaMigrationSetuserid.txtTransPass.text.match(/[0-9]/g) != null)) {
        showAlertRcMB(kony.i18n.getLocalizedString("keyPasswordMinRequirement"), kony.i18n.getLocalizedString("info"));
        frmSpaMigrationSetuserid.txtTransPass.setFocus(true);
        return;
    }
    if (frmSpaMigrationSetuserid.txtTransPass.text != frmSpaMigrationSetuserid.txtConfirmPassword.text) {
        showAlertRcMB(kony.i18n.getLocalizedString("PassNoMatch"), kony.i18n.getLocalizedString("info"));
        frmSpaMigrationSetuserid.txtTransPass.text = "";
        frmSpaMigrationSetuserid.txtConfirmPassword.text = "";
        frmSpaMigrationSetuserid.txtTransPass.setFocus(true);
        return;
    }
    /*if (frmSpaMigrationSetuserid.txtCurrentPwd.text == frmSpaMigrationSetuserid.txtPassword.text || frmSpaMigrationSetuserid.txtCurrentPwd.text ==
    	frmSpaMigrationSetuserid.txtConfirmPassword.text) {
    	showAlertRcMB(kony.i18n.getLocalizedString("KeyIncorrectPWD"), kony.i18n.getLocalizedString("info"));
    	return;
    }
    if (frmSpaMigrationSetuserid.txtPassword.text  == frmSpaMigrationSetuserid.txtUserId.text) {
    	showAlertRcMB(kony.i18n.getLocalizedString("keyUseridEqualsNewPassIB"), kony.i18n.getLocalizedString("info"));
    	return;
    }*/
    invokeMigrationUserCompositeSpa(frmSpaMigrationSetuserid.txtCnfCrntPwd.text, frmSpaMigrationSetuserid.txtTransPass.text, frmSpaMigrationSetuserid.txtUserID.text);
}

function ForceChngPwdemailProcessForTC() {
    if (gblTCEmailTriggerFlag) {
        showLoadingScreen();
        var inputparam = {}
        invokeServiceSecureAsync("partyInquiry", inputparam, FstTmeActivatnPartySrviceCallback);
    }
}

function handleMenuBtnCB() {
    //kony.application.showLoadingScreen(frmLoading, "", constants.LOADING_SCREEN_POSITION_FULL_SCREEN, true, true, "")
    //showLoadingScreen();
    showLoadingScreenWithNoIndicator();
    var calledForm = kony.application.getCurrentForm();
    //if(flowSpa)
    //{
    //calledForm.destroy();
    //calledForm.hboxMenuHeader=null;	
    //}
    if (isMenuShown == false) {
        calledForm.vboxLeft.skin = "VbxBlackBG";
        calledForm.vboxLeft.focusSkin = "VbxBlackBG";
        if (calledForm.id == "frmAccountSummaryLanding") {
            calledForm.vbox47407564342877.skin = "scrBG"; //use below name for scrollbox
        } else {
            calledForm.scrollboxLeft.skin = "scrBG";
        }
        //if (!calledForm.hboxMenuHeader) {
        //createMenuDynamically(calledForm);
        //}
        if (calledForm.info == undefined) {
            createMenuDynamically(calledForm);
        } else if (calledForm.info != undefined && !calledForm.info.menuCreated) {
            createMenuDynamically(calledForm);
        }
        if (calledForm.id == "frmMBPreLoginAccessesPin") {
            calledForm.btnEng.skin = checkBtnSkins()[0];
            calledForm.btnThai.skin = checkBtnSkins()[1];
        }
        if (gblFlagMenu == "MyInbox") {
            callOnClickMyInbox();
        } else if (gblFlagMenu == "Services") {
            callOnClickService();
        } else if (gblFlagMenu == "AboutMenu") {
            callOnClickAboutMenu();
        } else {}
        menuLocaleChange(calledForm);
        if (flowSpa) {} else {
            settingGestureForAccntSummary(calledForm);
        }
        calledForm.scrollboxMain.scrollToBeginning();
        //adding code as workaround for Spa due to platform/code limitations
        if (flowSpa && gblFlagMenu == "AboutMenu") {
            if (!gblTokenActivationFlag) {
                var currentFormId = kony.application.getCurrentForm();
                currentFormId.SegAboutMenu.removeAt(7);
            }
        }
        isMenuShown = true;
    } else {
        calledForm.scrollboxMain.scrollToEnd();
        if (flowSpa) {} else {
            removeGestureForAccntSummary(calledForm);
        }
        isMenuShown = false;
    }
    if (calledForm.lblBadge != null) calledForm.lblBadge.text = gblMyInboxTotalCountMB.toString();
    kony.application.dismissLoadingScreen();
}

function DashboardLangChange() {
    /* removing as per ENH_028 - IB Menu on Home Screen
    if(kony.i18n.getCurrentLocale() == "th_TH"){
    	frmIBPostLoginDashboard.btnMenuAboutMe.skin = "btnIBDashboardAboutMeThai"
    	frmIBPostLoginDashboard.btnMenuAboutMe.focusSkin = "btnIBDashboardAboutMeFocusThai"
    	frmIBPostLoginDashboard.btnMenuBillPayment.skin = "btnIBDashboardBillPaymentThai"
    	frmIBPostLoginDashboard.btnMenuBillPayment.focusSkin = "btnIBDashboardBillPaymentFocusThai"
    	frmIBPostLoginDashboard.btnMenuConvenientServices.skin = "btnIBDashboardConvenientServicesThai"
    	frmIBPostLoginDashboard.btnMenuConvenientServices.focusSkin = "btnIBDashboardConvenientServicesFocusThai"
    	frmIBPostLoginDashboard.btnMenuMyInbox.skin = "btnIBDashboardMyInboxThai"
    	frmIBPostLoginDashboard.btnMenuMyInbox.focusSkin = "btnIBDashboardMyInboxFocusThai"
    	frmIBPostLoginDashboard.btnMenuTopUp.skin = "btnIBDashboardTopUpThai"
    	frmIBPostLoginDashboard.btnMenuTopUp.focusSkin = "btnIBDashboardTopUpFocusThai"
    	frmIBPostLoginDashboard.btnMenuTransfer.skin = "btnIBDashboardTransferThai"
    	frmIBPostLoginDashboard.btnMenuTransfer.focusSkin = "btnIBDashboardTransferFocusThai"
    }
    else{
    	frmIBPostLoginDashboard.btnMenuAboutMe.skin = "btnIBDashboardAboutMe"
    	frmIBPostLoginDashboard.btnMenuAboutMe.focusSkin = "btnIBDashboardAboutMeFocus"
    	frmIBPostLoginDashboard.btnMenuBillPayment.skin = "btnIBDashboardBillPayment"
    	frmIBPostLoginDashboard.btnMenuBillPayment.focusSkin = "btnIBDashboardBillPaymentFocus"
    	frmIBPostLoginDashboard.btnMenuConvenientServices.skin = "btnIBDashboardConvenientServices"
    	frmIBPostLoginDashboard.btnMenuConvenientServices.focusSkin = "btnIBDashboardConvenientServicesFocus"
    	frmIBPostLoginDashboard.btnMenuMyInbox.skin = "btnIBDashboardMyInbox"
    	frmIBPostLoginDashboard.btnMenuMyInbox.focusSkin = "btnIBDashboardMyInboxFocus"
    	frmIBPostLoginDashboard.btnMenuTopUp.skin = "btnIBDashboardTopUp"
    	frmIBPostLoginDashboard.btnMenuTopUp.focusSkin = "btnIBDashboardTopUpFocus"
    	frmIBPostLoginDashboard.btnMenuTransfer.skin = "btnIBDashboardTransfer"
    	frmIBPostLoginDashboard.btnMenuTransfer.focusSkin = "btnIBDashboardTransferFocus"
    }
    */
}

function AccountSumDel() {
    showLoadingScreenPopup()
    var myAccountList_inputparam = {};
    myAccountList_inputparam["accountsFlag"] = "true";
    var status = invokeServiceSecureAsync("customerAccountInquiry", myAccountList_inputparam, myAcountSummaryDelete);
}

function myAcountSummaryDelete(status, resulttable) {
    dismissLoadingScreenPopup()
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {}
    }
}

function verifyOTPIBMobile() {
    validateOTPIBMobile();
}

function onMyProfileMobNextIB() {
    frmIBCMChgMobNoTxnLimit.hbxTokenEntry.setVisibility(false);
    frmIBCMChgMobNoTxnLimit.hbxOTPEntry.setVisibility(true);
    frmIBCMChgMobNoTxnLimit.txtBxOTP.setFocus(true);
    frmIBCMChgMobNoTxnLimit.lblkeyOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
    IBNewMobNoOTPRequestService();
}

function validateOTPIBMobile() {
    if (gblTokenSwitchFlag == true) {
        if (frmIBCMChgMobNoTxnLimit.hbxChangeMobileNumber.isVisible) {
            var otpToken = frmIBCMChgMobNoTxnLimit.txtBxOTP.text;
            if (otpToken != null) {
                otpToken = otpToken.trim();
            } else {
                frmIBCMChgMobNoTxnLimit.txtBxOTP.text = "";
                alert(kony.i18n.getLocalizedString("Receipent_alert_correctOTP"));
                return false;
            }
            if (otpToken == "") {
                frmIBCMChgMobNoTxnLimit.txtBxOTP.text = "";
                alert(kony.i18n.getLocalizedString("Receipent_alert_correctOTP"));
                return false;
            }
        } else {
            var otpToken = frmIBCMChgMobNoTxnLimit.tbxToken.text
            if (otpToken != null) otpToken = otpToken.trim();
            else {
                frmIBCMChgMobNoTxnLimit.tbxToken.text = "";
                alert(kony.i18n.getLocalizedString("Receipent_tokenId"));
                return false;
            }
            if (otpToken == "") {
                frmIBCMChgMobNoTxnLimit.tbxToken.text = "";
                alert(kony.i18n.getLocalizedString("Receipent_tokenId"));
                return false;
            }
        }
        validateOTPtextIBMobile(otpToken);
    } else {
        var otpText = frmIBCMChgMobNoTxnLimit.txtBxOTP.text
        if (otpText != null) otpText = otpText.trim();
        else {
            frmIBCMChgMobNoTxnLimit.txtBxOTP.text = "";
            alert(kony.i18n.getLocalizedString("Receipent_alert_correctOTP"));
            return false;
        }
        if (otpText == "") {
            frmIBCMChgMobNoTxnLimit.txtBxOTP.text = "";
            alert(kony.i18n.getLocalizedString("Receipent_alert_correctOTP"));
            return false;
        }
        validateOTPtextIBMobile(otpText);
    }
}

function segMyBillsData() {
    frmSelectBiller.segMyBills.widgetDataMap = {
        lblBillerName: "lblBillerName",
        lblRef1: "lblRef1",
        lblRef1Value: "lblRef1Value",
        lblRef2: "lblRef2",
        lblRef2Value: "lblRef2Value",
        imgBillerArrow: "imgBillerArrow",
        imgBillerpic: "imgBillerpic",
        billerMethod: "billerType",
        billerGroupType: "billerGroupType",
        waiverCode: "waiverCode",
        feeAmount: "feeAmount",
        fullAmount: "fullAmount"
    }
    frmSelectBiller.segMyBills.data = [{
        lblBillerName: "Offline",
        lblRef1: "Mobile:",
        imgBillerArrow: "navarrow3.png",
        lblRef1Value: "9087656764",
        lblRef2: "Ref2",
        lblRef2Value: "211-33243-1",
        imgBillerpic: "img_bill_1.png",
        billerMethod: "0",
        billerGroupType: "0",
        waiverCode: false,
        feeAmount: "0",
        fullAmount: "1234567"
    }, {
        lblBillerName: "Online ",
        lblRef1: "MailID:",
        imgBillerArrow: "navarrow3.png",
        lblRef1Value: "poituny@guima.com",
        lblRef2: "Ref2",
        lblRef2Value: "221-33243-2",
        imgBillerpic: "img_bill_2.png",
        billerMethod: "1",
        billerGroupType: "0",
        waiverCode: true,
        feeAmount: "100.0",
        fullAmount: "9873456"
    }, {
        lblBillerName: "TMB Credit Card ",
        lblRef1: "PAN No:",
        imgBillerArrow: "navarrow3.png",
        lblRef1Value: "POIUC8978H",
        lblRef2: "Ref2",
        lblRef2Value: "231-37653-3",
        imgBillerpic: "img_bill_3.png",
        billerMethod: "2",
        billerGroupType: "0",
        waiverCode: false,
        feeAmount: "0",
        fullAmount: "2346790"
    }, {
        lblBillerName: "Loan ",
        lblRef1: "Mobile:",
        imgBillerArrow: "navarrow3.png",
        lblRef1Value: "6709347612",
        lblRef2: "Ref2",
        lblRef2Value: "241-34233-4",
        imgBillerpic: "img_bill_4.png",
        billerMethod: "3",
        billerGroupType: "0",
        waiverCode: false,
        feeAmount: "0",
        fullAmount: "9898989"
    }, {
        lblBillerName: "Ready Cash ",
        lblRef1: "MailID:",
        imgBillerArrow: "navarrow3.png",
        lblRef1Value: "luckydf@yahrr@com",
        lblRef2: "Ref2",
        lblRef2Value: "251-76543-5",
        imgBillerpic: "img_bill_5.png",
        billerMethod: "4",
        billerGroupType: "0",
        waiverCode: false,
        feeAmount: "0",
        fullAmount: "12212121212"
    }, {
        lblBillerName: "TMB Credit Card",
        lblRef1: "PassportID:",
        imgBillerArrow: "navarrow3.png",
        lblRef1Value: "897/899/4567",
        lblRef2: "Ref2",
        lblRef2Value: "291-34356-9",
        imgBillerpic: "img_bill_1.png",
        billerMethod: "2",
        billerGroupType: "0",
        waiverCode: false,
        feeAmount: "0",
        fullAmount: "675434678"
    }]
}

function segSuggestedBillersData() {
    frmSelectBiller.segSuggestedBillers.widgetDataMap = {
        imgSuggestedBiller: "imgSuggestedBiller",
        lblSuggestedBiller: "lblSuggestedBiller"
    }
    frmSelectBiller.segSuggestedBillers.data = [{
        imgSuggestedBiller: "mes2.png",
        lblSuggestedBiller: "Mobile:"
    }, {
        imgSuggestedBiller: "mes2.png",
        lblSuggestedBiller: "MailID:"
    }, {
        imgSuggestedBiller: "mes2.png",
        lblSuggestedBiller: "MailID:"
    }, {
        imgSuggestedBiller: "mes2.png",
        lblSuggestedBiller: "PassportID:"
    }]
}

function onClicklblChooseDifferentReward() {
    var rewardlink = kony.i18n.getLocalizedString("keyChooseDifferentRewardLink");
    kony.application.openURL(rewardlink);
}

function onClickNextToConfirmPointRedemption() {
    frmIBPointRedemptionConfirmation.imgCredicard.src = frmIBPointRedemptionLanding.imgCredicard.src;
    frmIBPointRedemptionConfirmation.lblCardNickName.text = frmIBPointRedemptionLanding.lblCardNickName.text;
    frmIBPointRedemptionConfirmation.lblCardNumberValue.text = frmIBPointRedemptionLanding.lblCardNumberValue.text;
    frmIBPointRedemptionConfirmation.lblAvailablePointsValue.text = frmIBPointRedemptionLanding.lblAvailablePointsValue.text;
    frmIBPointRedemptionConfirmation.lblExpireDateValue.text = frmIBPointRedemptionLanding.lblExpireDateValue.text;
    frmIBPointRedemptionConfirmation.lblExpireDate.text = kony.i18n.getLocalizedString("keyPointExpiringOn") + " " + reformatDate(gblAccountTable["custAcctRec"][gblIndex]["stmtDate"]) + ":";
}

function segSuggestedTpUpBillersData() {
    frmIBTopUpLandingPage.segSuggestedBiller.widgetDataMap = {
        "lblSuggestedBiller": "lblSuggestedBiller",
        "hbxSuggested": "hbxSuggested",
        "imgSuggestedBiller": "imgSuggestedBiller",
        "btnAddSuggBiller": "btnAddSuggBiller"
    }
    frmIBTopUpLandingPage.segSuggestedBiller.data = [{
        "lblSuggestedBiller": "Mobile",
        "imgSuggestedBiller": "bill_sug_01.png",
        "btnAddSuggBiller": "."
    }, {
        "lblSuggestedBiller": "Mail Id",
        "imgSuggestedBiller": "bill_sug_02.png",
        "btnAddSuggBiller": "."
    }, {
        "lblSuggestedBiller": "PAN No",
        "imgSuggestedBiller": "bill_sug_02.png",
        "btnAddSuggBiller": "."
    }, {
        "lblSuggestedBiller": "Passport",
        "imgSuggestedBiller": "bill_sug_01.png",
        "btnAddSuggBiller": "."
    }]
}

function executePointRedemptionBeforeconfirm() {
    if (frmIBPointRedemptionConfirmation.hbxCashBack.isVisible || frmIBPointRedemptionConfirmation.hbxOtpBox.isVisible) {
        frmIBPointRedemptionComplete.show();
    } else {
        preOTPScreenPointRedemption();
    }
}

function enableDisableMidSectionTopUpIB(pageFlag) {
    frmIBTopUpLandingPage.vboxBillPayMain.setEnabled(pageFlag);
}

function licenseDisableforIEFF() {
    //var trident = !!navigator.userAgent.match(/Trident\/7.0/);
    //var net = !!navigator.userAgent.match(/.NET4.0E/);
    //var IE11 = trident && net
    //if((navigator.userAgent.indexOf("Firefox") > 0 || (window.navigator.appVersion.match(/MSIE (\d+)/) != null)) || IE11)
    //{
    if (kony.license && kony.license.disableMetricReporting) kony.license.disableMetricReporting(true);
    //}
}

function resethzRoundAbtTransfer() {
    /*&ResetTransferHomePage();
	    frmTransferLanding = null;
	    gblPaynow = true
	    Recp_category = 0;
	    frmTransferLandingGlobals();
	    GBLFINANACIALACTIVITYLOG = {}*/
    var inputParam = {}
    inputParam["transferFlag"] = "true";
    showLoadingScreen();
    //destroyMenuSpa();
    invokeServiceSecureAsync("customerAccountInquiry", inputParam, callBackTransferFromAccounts)
}
/*****************************************************************
 *     Name    : forgotPin
 *     Module  : Touch ID
 *     Author  : Kony IT Services
 *     Purpose : Function to Navigate to Forgot PIN form
 *     Params  : None
 * 	   Changes :
 ******************************************************************/
function forgotPin() {
    frmMBForgotPin.show();
}

function richTextFun() {
    //var msge = 
    //frmMBForgotPin.richMsge.text = msge;
}
/*****************************************************************
 *     Name    : showAlertTouchErrCode
 *     Module  : Touch ID
 *     Author  : Kony IT Services
 *     Purpose : Function to  unfill small circles when we click on back button
 *     Params  : None
 * 	   Changes :
 ******************************************************************/
function showAlertTouchErrCode(keyMsg, KeyTitle) {
    var Cancel = kony.i18n.getLocalizedString("keyCancelButton");
    //Defining basicConf parameter for alert
    var basicConf = {
        message: keyMsg,
        alertType: constants.ALERT_TYPE_INFO,
        alertTitle: KeyTitle,
        yesLabel: Cancel,
        noLabel: "",
        alertHandler: handle2
    };
    //Defining pspConf parameter for alert
    var pspConf = {};
    //Alert definition
    var infoAlert = kony.ui.Alert(basicConf, pspConf);

    function handle2(response) {}
    return;
}

function imageSize() {
    //	var refHeight = 480;
    //	var refWidth = 300;
    //		if (gblDeviceInfo["model"] == "iPhone 6 Plus") {
    //		    alert("in 6 Plus");
    //
    //	        frmTouchIdIntermediateLogin.imgTouch.referenceHeight = 618; //1863//291;
    //			frmTouchIdIntermediateLogin.imgTouch.referenceWidth = 618;//1170;//388;
    //		    //frmTouchIdIntermediateLogin.imgTouch.imageScaleMode = "IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO";
    //		} else if (gblDeviceInfo["model"] == "iPhone 6") {
    //		    alert("in 6");
    //
    //			frmTouchIdIntermediateLogin.imgTouch.referenceHeight = 400;//1126;//263;
    //			frmTouchIdIntermediateLogin.imgTouch.referenceWidth = 400;//352;
    //		   // frmTouchIdIntermediateLogin.imgTouch.imageScaleMode = IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO;
    //		} else if (gblDeviceInfo["model"] == "iPhone 5S") {
    //		    var mode = gblDeviceInfo["model"]
    //		    alert("model is--->"+ mode);
    //		    
    //		    frmTouchIdIntermediateLogin.imgTouch.referenceWidth = 302;//352;
    //		    frmTouchIdIntermediateLogin.imgTouch.referenceHeight = 465;//1126;//263;
    //           // frmTouchIdIntermediateLogin.imgTouch.imageScaleMode = "IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS";
    //            
    //			
    //		}
}
/*****************************************************************
 *     Name    : touchCount
 *     Module  : Touch ID
 *     Author  : Kony IT Services
 *     Purpose : Function use to Count to touch failures.
 *     Params  : None
 * 	   Changes :
 ******************************************************************/
function touchCount() {
    alert("gblTouchCount--->" + gblTouchCount);
    if (gblSuccess == false) {
        gblTouchCount++;
    } else {
        gblTouchCount = 0;
    }
    if (gblTouchCount == 3) {
        //popup.dissmis() need to dismis the popup
        onClickChangeTouchIcon();
    } else {
        frmMBPreLoginAccessesPin.btnTouchnBack.skin = "btntouchiconstudio5";
        frmMBPreLoginAccessesPin.btnTouchnBack.focusSkin = "btntouchiconfoc";
    }
}
/*function callLiquidityInqService() {
	showLoadingScreen();
	var inputparam = {};
  	//inputparam["locale"] = kony.i18n.getCurrentLocale();
	invokeServiceSecureAsync("doLiquidityInq", inputparam, liquidityInqCallBack);
}*/
function liquidityInqCallBack(status, resulttable) {
    if (status == 400) {
        var StatusCode = resulttable["StatusCode"];
        var Severity = resulttable["Severity"];
        var StatusDesc = resulttable["StatusDesc"];
        if (resulttable["opstatus"] == 0) {
            if (resulttable["StatusCode"] == 0) {
                if (resulttable["maxCurAmt"] != null && resulttable["maxCurAmt"] != "") {
                    frmAccountDetailsMB.lblUsefulValue2.text = commaFormatted(resulttable["maxCurAmt"]) + kony.i18n.getLocalizedString("currencyThaiBaht");
                    frmAccountDetailsMB.hbxUseful2.isVisible = true;
                }
                if (resulttable["minCurAmt"] != null && resulttable["minCurAmt"] != "") {
                    frmAccountDetailsMB.lblUsefulValue3.text = commaFormatted(resulttable["minCurAmt"]) + kony.i18n.getLocalizedString("currencyThaiBaht");
                    frmAccountDetailsMB.hbxUseful3.isVisible = true;
                }
                if (null != resulttable["linkedAccount"] && resulttable["linkedAccount"] != "" && resulttable["linkedAccount"] != 0) {
                    var disPlayLinkAccount;
                    if (resulttable["linkedAccount"].length == 14) {
                        disPlayLinkAccount = resulttable["linkedAccount"].substring(4);
                    } else {
                        disPlayLinkAccount = resulttable["linkedAccount"]
                    }
                    frmAccountDetailsMB.lblUsefulValue11.text = formatAccountNo(disPlayLinkAccount);
                }
                dismissLoadingScreen();
            } else {
                dismissLoadingScreen();
                if (resulttable["additionalDS"][2]["StatusDesc"] == "LIQ NOT FOUND" && resulttable["additionalDS"][2]["StatusCode"] == "-1000") {
                    //frmAccountDetailsMB.hboxApplySendToSave.isVisible = true;
                    frmAccountDetailsMB.hbxUseful2.isVisible = false;
                    frmAccountDetailsMB.hbxUseful3.isVisible = false;
                    frmAccountDetailsMB.hbxUseful11.isVisible = false;
                } else showCommonAlert(resulttable["errMsg"], resulttable["XPServerStatCode"]);
            }
        } else {
            dismissLoadingScreen();
            alert(" " + resulttable["errMsg"]);
        }
    }
}

function populateBenfDetails(frmName) {
    kony.print("enetered in preshow");
    showLoadingScreen();
    handleSavingCareMBDetails(frmName);
    /*if(gblBeneficiaryData.length >0){
    kony.print("calling handleSavingCareMBDetails");
    	handleSavingCareMBDetails(true,frmName);
    }else{
    	handleSavingCareMBDetails(false,frmName);
    }*/
    dismissLoadingScreen();
}

function onBeepAndBillApplyConfirm() {
    if (flowSpa) {
        if (gblIBFlowStatus == "04") {
            alertUserStatusLocked();
        } else {
            var inputParams = {}
            spaChnage = "bbflow"
            gblOTPFlag = true;
            try {
                kony.timer.cancel("otpTimer")
            } catch (e) {}
            gblSpaChannel = "ApplyBB";
            /*
            if (kony.i18n.getCurrentLocale() == "en_US") {
                SpaEventNotificationPolicy = "MIB_OTPApply_SVC_EN";
                SpaSMSSubject = "MIB_OTPApply_SVC_EN";
            } else {
                SpaEventNotificationPolicy = "MIB_OTPApply_SVC_TH";
                SpaSMSSubject = "MIB_OTPApply_SVC_TH";
            }*/
            onClickOTPRequestSpa();
        }
    } else {
        var lblText = kony.i18n.getLocalizedString("transPasswordSub");
        var refNo = "";
        var mobNO = "";
        popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = txtFocusBG;
        popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = txtFocusBG;
        popupTractPwd.hbxPopupTranscPwd.skin = hbxPopupTrnsPwdBlue;
        showOTPPopup(lblText, refNo, mobNO, onBBApplyConfirmationAgreeComposite, 3);
    }
}

function onBeepAndBillExecuteConfirm() {
    if (flowSpa) {
        if (gblIBFlowStatus == "04") {
            alertUserStatusLocked();
        } else {
            var inputParams = {}
            spaChnage = "bbflow"
            gblOTPFlag = true;
            try {
                kony.timer.cancel("otpTimer")
            } catch (e) {}
            gblSpaChannel = "BillPayment";
            /*
            if (kony.i18n.getCurrentLocale() == "en_US") {
                SpaEventNotificationPolicy = "MIB_BillPayment_EN";
                SpaSMSSubject = "MIB_BillPayment_EN";
            } else {
                SpaEventNotificationPolicy = "MIB_BillPayment_TH";
                SpaSMSSubject = "MIB_BillPayment_TH";
            }*/
            onClickOTPRequestSpa();
        }
    } else {
        var lblText = kony.i18n.getLocalizedString("transPasswordSub");
        var refNo = "";
        var mobNO = "";
        popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = txtFocusBG;
        popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = txtFocusBG;
        popupTractPwd.hbxPopupTranscPwd.skin = hbxPopupTrnsPwdBlue;
        showOTPPopup(lblText, refNo, mobNO, onBBApplyConfirmationAgreeComposite, 3);
    }
}
/*function invokecustomerBBPaymentAdd() {
    var inputParam = {};
    inputParam["rmNum"] = "";
    inputParam["PINcode"] = messageObj["pinCode"];
    showLoadingScreen();
    invokeServiceSecureAsync("customerBBPaymentAdd", inputParam, callBackCustomerBBPaymentAdd);
}*/
/*function callBackCustomerBBPaymentAdd(status, result) {
    
    
    if (status == 400) {
        
        if (result["opstatus"] == 0) {
            if (result["additionalStatus"][0]["statusCode"] == 0) {
                dismissLoadingScreen();
                frmBBExecuteConfirmAndComplete.hbxAdvertisement.setVisibility(true)
                frmBBExecuteConfirmAndComplete.hbxStartImage.setVisibility(true);

                frmBBExecuteConfirmAndComplete.lblHdrTxt.text = kony.i18n.getLocalizedString("Complete");

                frmBBExecuteConfirmAndComplete.btnRight.setVisibility(true);
                if (flowSpa) {
                    frmBBExecuteConfirmAndComplete.hbxbpconfcancelspa.setVisibility(false);
                    frmBBExecuteConfirmAndComplete.hbxReturnSpa.setVisibility(true);
                } else {
                    frmBBExecuteConfirmAndComplete.hbxbpconfcancel.setVisibility(false);
                    frmBBExecuteConfirmAndComplete.hbxReturn.setVisibility(true);
                }

                var tellerId = result["custAcctRec"][0]["TellerID"];
                var errorCd = result["additionalStatus"][0]["statusDesc"];

                createFinActivityLogObjBBMB(errorCd, tellerId);

                //setFinancialActivityLogBB(finTxnRefId, finTxnAmount, finTxnFee, finTxnStatus, tellerId, finLinkageId, errorCd)
                sendNotificationForBBExecuteMB();
                
            } else {
                dismissLoadingScreen();
                showAlert(result["additionalStatus"][0]["addStatusDesc"], kony.i18n.getLocalizedString("info"));
                return false;
            }
        } else {
            
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
        kony.application.dismissLoadingScreen();
    }
}*/
function setDataOnSegSelectBB() {
    var newData = [];
    var maxLength = 15; //configurable parameter
    segSelectData = [{
        imgSuggestedBiller: {
            src: "img_bill_3.png"
        },
        lblSuggestedBiller: "True Internate(1234)",
        IsRequiredRefNumber2Add: "Y",
        BillerID: 12340,
        BillerMethod: 1,
        EffDt: "12/12/13",
        BillerCompCode: "9999",
        Ref1Label: {
            text: "Ref1:"
        },
        Ref1Len: 10,
        BarcodeOnly: "0",
        btnAddBiller: {
            skin: "btnAdd"
        }
    }, {
        imgSuggestedBiller: {
            src: "img_bill_2.png"
        },
        lblSuggestedBiller: "MEA(1234)",
        IsRequiredRefNumber2Add: "N",
        BillerID: 12341,
        BillerMethod: 1,
        EffDt: "11/12/13",
        BillerCompCode: "9998",
        Ref1Label: {
            text: "Ref1:"
        },
        Ref1Len: 10,
        BarcodeOnly: "0",
        btnAddBiller: {
            skin: "btnAdd"
        }
    }, {
        imgSuggestedBiller: {
            src: "img_bill_3.png"
        },
        lblSuggestedBiller: "True Internate(1234)",
        IsRequiredRefNumber2Add: "Y",
        BillerID: 12342,
        BillerMethod: 2,
        EffDt: "10/12/13",
        BillerCompCode: "9997",
        Ref1Label: {
            text: "Ref1:"
        },
        Ref1Len: 10,
        BarcodeOnly: "0",
        btnAddBiller: {
            skin: "btnAdd"
        }
    }, {
        imgSuggestedBiller: {
            src: "img_bill_3.png"
        },
        lblSuggestedBiller: "True Internate(1234)",
        IsRequiredRefNumber2Add: "Y",
        BillerID: 12343,
        BillerMethod: 1,
        EffDt: "09/12/13",
        BillerCompCode: "9996",
        Ref1Label: {
            text: "Ref1:"
        },
        Ref1Len: 10,
        BarcodeOnly: "0",
        btnAddBiller: {
            skin: "btnAdd"
        }
    }, {
        imgSuggestedBiller: {
            src: "img_bill_3.png"
        },
        lblSuggestedBiller: "True Internate(1234)",
        IsRequiredRefNumber2Add: "Y",
        BillerID: 12344,
        BillerMethod: 2,
        EffDt: "8/12/13",
        BillerCompCode: "9995",
        Ref1Label: {
            text: "Ref1:"
        },
        Ref1Len: 10,
        BarcodeOnly: "0",
        btnAddBiller: {
            skin: "btnAdd"
        }
    }, {
        imgSuggestedBiller: {
            src: "img_bill_3.png"
        },
        lblSuggestedBiller: "True Internate(1234)",
        IsRequiredRefNumber2Add: "N",
        BillerID: 12345,
        BillerMethod: 1,
        EffDt: "07/12/13",
        BillerCompCode: "9992",
        Ref1Label: {
            text: "Ref1:"
        },
        Ref1Len: 10,
        BarcodeOnly: "0",
        btnAddBiller: {
            skin: "btnAdd"
        }
    }];
    if (segSelectData.length <= maxLength) {
        frmMyTopUpSelect.hbxLink.isVisible = false;
        frmMyTopUpSelect.segSelectList.data = segSelectData;
    } else {
        frmMyTopUpSelect.hbxLink.isVisible = true;
        for (i = 0; i < maxLength; i++) {
            newData.push(segSelectData[i])
                //alert("data "+[segSelectData[i]])
        }
        frmMyTopUpSelect.segSelectList.data = newData;
    }
}

function deleteSeletcedBB() {
    var tempAccNo = frmBBMyBeepAndBill.lblAccNo.text;
    var accNo = "";
    for (var i = 0; i < tempAccNo.length; i++) {
        if (tempAccNo[i] != '-') accNo = accNo + tempAccNo[i]
    }
    accNo = accNo.substring(accNo.length - 10, accNo.length)
    inputParam = {};
    inputParam["rmNum"] = "";
    inputParam["rqUUId"] = "";
    inputParam["BBNickName"] = frmBBMyBeepAndBill.lblBillerNickName.text;
    inputParam["BBCompCode"] = gblBillerCompCodeBBMB;
    inputParam["Ref1"] = frmBBMyBeepAndBill.lblRef1Val.text;
    if (frmBBMyBeepAndBill.hbxRef2.isVisible == true) inputParam["Ref2"] = frmBBMyBeepAndBill.lblRef2Val.text;
    else inputParam["Ref2"] = "";
    inputParam["AcctIdentValue"] = accNo;
    //inputParam["mobileNumber"] = gblPHONENUMBER;//from session
    inputParam["ActionFlag"] = "C";
    inputParam["MIBFlag"] = "N";
    inputParam["Channel"] = "MIB";
    inputParam["ApplyDate"] = getTodaysDate();
    inputParam["ApplyTime"] = "155712";
    if (flowSpa) {
        inputParam["channel"] = "IB";
        inputParam["flowSpa"] = true;
    } else {
        inputParam["channel"] = "rc";
        inputParam["flowSpa"] = false;
    }
    showLoadingScreen();
    popDelTopUp.dismiss();
    invokeServiceSecureAsync("customerBBDelete", inputParam, deleteBeepAndBillCallBack);
}

function onValidateBiller() {
    if (frmAddTopUpToMB.lblAddbillerName.text == null || frmAddTopUpToMB.lblAddbillerName.text == "") {
        return false;
    }
    return true;
}
//function onClickPdfImgForBBTandCMB(filetype) {
//	var inputParam = {};
//	var pdfImagedata = {};
//	var fileType = filetype;
// 
//	var pdfImagedata = {
// 		"tctext" : frmBBApplyNow.lblTandC.text
//    }
//
//	inputParam["filetype"] = fileType;
//	inputParam["templatename"] = "TermsAndConditions";
//	inputParam["datajson"] = JSON.stringify(pdfImagedata, "", "");
//	invokeServiceSecureAsync("generatePdfImage", inputParam, mbRenderFileCallbackfunction);
//}
//for apply
function onClickGeneratePDFImageForBBMB(filetype) {
    var inputParam = {};
    var scheduleDetails = "";
    var pdfImagedata = {};
    fileType = filetype;
    var accNo = frmBBConfirmAndComplete.lblAccNo.text;
    accNo = removeUnwatedSymbols(accNo);
    var pdfImagedata = {
        "AccountNo": "xxx-x-" + accNo.substring(4, 9) + "-x",
        "AccountName": frmBBConfirmAndComplete.lblAccHolderName.text,
        "BillerName": frmBBConfirmAndComplete.lblBillerCompcode.text,
        "Ref1Label": frmBBConfirmAndComplete.lblAddedRef1.text,
        "Ref1Val": frmBBConfirmAndComplete.lblAddedRef1Val.text,
        "localeId": kony.i18n.getCurrentLocale()
    }
    if (frmBBConfirmAndComplete.hbxRef2.isVisible == true) {
        inputParam["Ref2Label"] = frmBBConfirmAndComplete.lblAddedRef2.text;
        inputParam["Ref2Val"] = frmBBConfirmAndComplete.lblAddedRef2Val.text;
    } else {
        inputParam["Ref2Label"] = "";
        inputParam["Ref2Val"] = "";
    }
    inputParam["filetype"] = fileType;
    inputParam["templatename"] = "ApplyBeepAndBillTemplate";
    inputParam["outputtemplatename"] = "Apply_Beep_And_Bill_Set_Details_" + kony.os.date("DDMMYYYY");
    inputParam["localeId"] = kony.i18n.getCurrentLocale();
    inputParam["datajson"] = JSON.stringify(pdfImagedata, "", "");
    invokeServiceSecureAsync("generatePdfImage", inputParam, mbRenderFileCallbackfunction);
}
//for execute
function onClickGeneratePDFImageForBBMBEx(filetype) {
    var inputParam = {};
    var scheduleDetails = "";
    var pdfImagedata = {};
    fileType = filetype;
    var accNo = frmBBExecuteConfirmAndComplete.lblAccountNum.text;
    accNo = removeUnwatedSymbols(accNo);
    var pdfImagedata = {
        "AccountNo": "xxx-x-" + accNo.substring(4, 9) + "-x",
        "AccountName": frmBBExecuteConfirmAndComplete.lblAccountName.text,
        "BillerName": frmBBExecuteConfirmAndComplete.lblBillerNameCompCode.text,
        "Ref1Label": frmBBExecuteConfirmAndComplete.lblRef1.text,
        "Ref1Value": frmBBExecuteConfirmAndComplete.lblRef1Value.text,
        "Amount": frmBBExecuteConfirmAndComplete.lblAmountVal.text,
        "Fee": frmBBExecuteConfirmAndComplete.lblPaymentFeeVal.text,
        "PaymentDate": frmBBExecuteConfirmAndComplete.lblPaymentDateVal.text,
        "TransactionRefNo": frmBBExecuteConfirmAndComplete.lblTransRefNoVal.text,
        "localeId": kony.i18n.getCurrentLocale()
    }
    if (frmBBExecuteConfirmAndComplete.hbxRef2.isVisible == true) {
        pdfImagedata["Ref2Labe1"] = frmBBExecuteConfirmAndComplete.lblRef2.text;
        pdfImagedata["Ref2Value"] = frmBBExecuteConfirmAndComplete.lblRef2Value.text;
    } else {
        pdfImagedata["Ref2Labe1"] = "";
        pdfImagedata["Ref2Value"] = "";
    }
    inputParam["filetype"] = fileType;
    inputParam["templatename"] = "ExecuteBeepAndBillPaymentTemplate";
    inputParam["outputtemplatename"] = "Execute_Beep_And_Bill_Set_Details_" + kony.os.date("DDMMYYYY");
    inputParam["localeId"] = kony.i18n.getCurrentLocale();
    inputParam["datajson"] = JSON.stringify(pdfImagedata, "", "");
    invokeServiceSecureAsync("generatePdfImage", inputParam, mbRenderFileCallbackfunction);
}

function IBTnCTextBBMB() {
    var input_param = {};
    input_param["localeCd"] = kony.i18n.getCurrentLocale();
    input_param["moduleKey"] = "BeepBill";
    invokeServiceSecureAsync("readUTFFile", input_param, setIBTnCBBMB);
}

function enableBillPaymentSharing() {
    var btnskin = "btnShare";
    var btnFocusSkin = "btnShareFoc";
    if (frmBillPaymentComplete.hboxaddfblist.isVisible) {
        frmBillPaymentComplete.hboxaddfblist.isVisible = false;
        frmBillPaymentComplete.btnRight.skin = btnskin;
        frmBillPaymentComplete.imgHeaderMiddle.src = "arrowtop.png";
        frmBillPaymentComplete.imgHeaderRight.src = "empty.png";
    } else {
        frmBillPaymentComplete.hboxaddfblist.isVisible = true;
        frmBillPaymentComplete.btnRight.skin = btnFocusSkin;
        frmBillPaymentComplete.imgHeaderMiddle.src = "empty.png";
        frmBillPaymentComplete.imgHeaderRight.src = "arrowtop.png";
    }
}

function checkDuplicateNicknameScanned() {
    var BillerList = scannedresponseData;
    var ConfirmationList = frmAddTopUpBillerconfrmtn.segConfirmationList.data;
    var nickname = BillPayTopUpConf.tbxScanBillerNickname.text;
    gblScannedNickname = nickname;
    if (nickname.trim() == "" || nickname.trim() == null) {
        return false;
    }
    if (!gblAddToMyBill) {
        return true;
    }
    var check = "";
    var val = "";
    for (var i = 0;
        (((BillerList) != null) && i < BillerList.length); i++) {
        val = BillerList[i]["BillerNickName"];
        if (nickname == val) {
            //alert("Duplicate Nickname" + val);
            return false;
        }
    }
    for (var j = 0;
        (((ConfirmationList) != null) && j < ConfirmationList.length); j++) {
        val = ConfirmationList[j].lblCnfrmNickName;
        if (nickname == val) {
            //alert("Duplicate Nickname" + val);
            return false;
        }
    }
    return true;
}

function billPayConfirmCheckChangeSkin() {
    if (gblPaynow) {
        frmBillPaymentConfirmationFuture.hboxMyNote.skin = "";
        frmBillPaymentConfirmationFuture.hboxTxnNo.skin = "hboxLightGrey";
        frmBillPaymentConfirmationFuture.hbxEasyPassTxnId.skin = "";
    } else {
        frmBillPaymentConfirmationFuture.hboxMyNote.skin = "hboxLightGrey";
        frmBillPaymentConfirmationFuture.hboxTxnNo.skin = "";
        frmBillPaymentConfirmationFuture.hbxEasyPassTxnId.skin = "hboxLightGrey";
    }
}

function billPayCompleteCheckChangeSkin() {
    if (gblPaynow) {
        frmBillPaymentComplete.hboxMyNote.skin = "";
        frmBillPaymentComplete.hboxTxnNo.skin = "hboxLightGrey";
        frmBillPaymentComplete.hbxEasyPassTxnId.skin = "";
    } else {
        frmBillPaymentComplete.hboxMyNote.skin = "hboxLightGrey";
        frmBillPaymentComplete.hboxTxnNo.skin = "";
        frmBillPaymentComplete.hbxEasyPassTxnId.skin = "hboxLightGrey";
    }
}

function onClickLeftArrowBpMB() {
    billpaymentcvindex = frmBillPayment.segSlider.selectedIndex;
    if (billpaymentNoOfAccounts <= 1) {
        return false;
    }
    if (billpaymentcvindex[1] == 0) {
        frmBillPayment.segSlider.selectedIndex = [0, (billpaymentNoOfAccounts - 1)];
        // alert("here 1");
    } else {
        frmBillPayment.segSlider.selectedIndex = [0, (billpaymentcvindex[1] - 1)];
        // alert("here 2");
    }
    billpaymentcvindex = frmBillPayment.segSlider.selectedIndex;
}

function onClickRightArrowBpMB() {
    billpaymentcvindex = frmBillPayment.segSlider.selectedIndex;
    if (billpaymentNoOfAccounts <= 1) {
        return false;
    }
    if (billpaymentcvindex[1] == (billpaymentNoOfAccounts - 1)) frmBillPayment.segSlider.selectedIndex = [0, 0];
    else frmBillPayment.segSlider.selectedIndex = [0, (billpaymentcvindex[1] + 1)];
    billpaymentcvindex = frmBillPayment.segSlider.selectedIndex;
}

function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}

function calcBalanceAfterPay() {
    var balBeforePayment = parseFloat(removeCommos(gblSelectedAccBal)).toFixed(2);
    var amountValue = parseFloat(removeCommos(frmBillPaymentConfirmationFuture.lblAmountValue.text)).toFixed(2);
    var fee = parseFloat(removeCommos(frmBillPaymentConfirmationFuture.lblPaymentFeeValue.text)).toFixed(2);
    var totalAmount = parseFloat(amountValue) + parseFloat(fee);
    var balAfterPayment = parseFloat(balBeforePayment) - parseFloat(totalAmount);
    return balAfterPayment.toFixed(2);
}

function savePDFBillPaymentMB(filetype) {
    var inputParam = {};
    if (GblBillTopFlag) {
        inputParam["templatename"] = "ExecutedBillPaymentTemplate";
    } else {
        inputParam["templatename"] = "ExecutedTopUpPaymentComplete";
    }
    inputParam["filetype"] = filetype;
    var accNo = frmBillPaymentComplete.lblAccountNum.text.split("-")[2];
    accNo = "XXX-X-" + accNo + "-X";
    var activityTypeId = "";
    var pdfImagedata = {
        "localeId": kony.i18n.getCurrentLocale(),
        "AccountNo": accNo,
        "AccountName": frmBillPaymentComplete.lblAccountName.text,
        "BillerName": frmBillPaymentComplete.lblBillerNameCompCode.text,
        "Ref1Label": frmBillPaymentComplete.lblRef1.text,
        "Ref1Value": frmBillPaymentComplete.lblRef1Value.text,
        "Amount": frmBillPaymentComplete.lblAmountValue.text,
        "Fee": frmBillPaymentComplete.lblPaymentFeeValue.text,
        "PaymentOrderDate": frmBillPaymentComplete.lblPaymentDateValue.text,
        "MyNote": frmBillPaymentComplete.lblMyNoteValue.text,
        "TransactionRefNo": frmBillPaymentComplete.lblTxnNumValue.text
    };
    if (GblBillTopFlag) {
        if (frmBillPaymentComplete.lblRef2.text != null && frmBillPaymentComplete.lblRef2.text != "" && frmBillPaymentComplete.lblRef2Value.text != null && frmBillPaymentComplete.lblRef2Value.text != "") {
            pdfImagedata["Ref2Label"] = frmBillPaymentComplete.lblRef2.text;
            pdfImagedata["Ref2Value"] = frmBillPaymentComplete.lblRef2Value.text;
        }
    }
    if (GblBillTopFlag) {
        if (gblPaynow) {
            activityTypeId = "027";
            inputParam["outputtemplatename"] = "Bill_Payment_Details_" + kony.os.date("DDMMYYYY");
        } else {
            activityTypeId = "028";
            inputParam["outputtemplatename"] = "Future_Bill_Payment_Set_Details_" + kony.os.date("DDMMYYYY");
            pdfImagedata["BillPaySchedule"] = "Payment Schedule";
            if (frmBillPaymentComplete.lblEndOnValue.text == "-") {
                pdfImagedata["PaymentSchedule"] = frmBillPaymentComplete.lblStartOnValue.text + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + frmBillPaymentComplete.lblEveryMonth.text;
            } else {
                pdfImagedata["PaymentSchedule"] = frmBillPaymentComplete.lblStartOnValue.text + " " + kony.i18n.getLocalizedString("keyTo") + " " + frmBillPaymentComplete.lblEndOnValue.text + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + frmBillPaymentComplete.lblEveryMonth.text + " for " + frmBillPaymentComplete.lblExecuteValue.text;
            }
        }
    } else {
        if (gblPaynow) {
            activityTypeId = "030";
            inputParam["outputtemplatename"] = "Top-Up_Payment_Details_" + kony.os.date("DDMMYYYY");
        } else {
            activityTypeId = "031";
            inputParam["outputtemplatename"] = "Future_Top-Up_Payment_Set_Details_" + kony.os.date("DDMMYYYY");
            pdfImagedata["TopUpSchedule"] = "Top-Up Schedule";
            if (frmBillPaymentComplete.lblEndOnValue.text == "-") {
                pdfImagedata["PaymentSchedule"] = frmBillPaymentComplete.lblStartOnValue.text + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + frmBillPaymentComplete.lblEveryMonth.text;
            } else {
                pdfImagedata["PaymentSchedule"] = frmBillPaymentComplete.lblStartOnValue.text + " " + kony.i18n.getLocalizedString("keyTo") + " " + frmBillPaymentComplete.lblEndOnValue.text + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + frmBillPaymentComplete.lblEveryMonth.text + " for " + frmBillPaymentComplete.lblExecuteValue.text;
            }
        }
    }
    inputParam["datajson"] = JSON.stringify(pdfImagedata, "", "");
    if (gblPaynow) {
        savePDFIB(filetype, activityTypeId, frmBillPaymentComplete.lblTxnNumValue.text);
    } else {
        saveFuturePDF(filetype, activityTypeId, frmBillPaymentComplete.lblTxnNumValue.text);
        //invokeServiceSecureAsync("generatePdfImage", inputParam, mbRenderFileCallbackfunction);
    }
}

function topupAmountServiceAsyncCallback(status, result) {
    if (status == 400) //success responce
    {
        TranId = ""; //remove comments after testing
        gblRef1 = "";
        gblRef2 = "";
        gblRef3 = "";
        gblRef4 = "";
        dismissLoadingScreen();
        if (result["opstatus"] == 0) {
            if (result["StatusCode"] != 0) {
                dismissLoadingScreen();
                alert(" " + result["errMsg"]);
                return;
            }
            var masterData = [];
            for (var i = 0; i < result["StepAmount"].length; i++) {
                var temp = {
                    "lblAmount": commaFormatted(result["StepAmount"][i]["Amt"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht")
                };
                masterData.push(temp);
            }
            if (result["OnlinePmtInqRs"] != undefined) {
                BankRefId = result["OnlinePmtInqRs"][0]["BankRefId"];
                TranId = result["OnlinePmtInqRs"][0]["TrnId"];
                gblRef1 = result["OnlinePmtInqRs"][0]["Ref1"];
                gblRef2 = result["OnlinePmtInqRs"][0]["Ref2"];
                gblRef3 = result["OnlinePmtInqRs"][0]["Ref3"];
                gblRef4 = result["OnlinePmtInqRs"][0]["Ref4"];
            }
            frmTopUpAmount.segPop.data = masterData;
            frmTopUpAmount.show();
            //	popUpTopUpAmount.segBanklist.setData(masterData);        	
        } else {
            var masterData = [];
            var tempData = ["Amount", "Amount"];
            masterData.push(tempData);
            frmTopUpAmount.segPop.setData(masterData);
            frmTopUpAmount.show();
            alert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"));
        }
    } else {
        if (status == 300) {
            var masterData = [];
            var tempData = ["Amount", "Amount"];
            masterData.push(tempData);
            frmTopUpAmount.segPop.setData(masterData);
            frmTopUpAmount.show();
            alert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"));
        }
    }
}

function savePDFBillPaymentMB(filetype) {
    var inputParam = {};
    if (GblBillTopFlag) {
        inputParam["templatename"] = "ExecutedBillPaymentTemplate";
    } else {
        inputParam["templatename"] = "ExecutedTopUpPaymentComplete";
    }
    inputParam["filetype"] = filetype;
    var accNo = frmBillPaymentComplete.lblAccountNum.text.split("-")[2];
    accNo = "XXX-X-" + accNo + "-X";
    var activityTypeId = "";
    var pdfImagedata = {
        "localeId": kony.i18n.getCurrentLocale(),
        "AccountNo": accNo,
        "AccountName": frmBillPaymentComplete.lblAccountName.text,
        "BillerName": frmBillPaymentComplete.lblBillerNameCompCode.text,
        "Ref1Label": frmBillPaymentComplete.lblRef1.text,
        "Ref1Value": frmBillPaymentComplete.lblRef1Value.text,
        "Amount": frmBillPaymentComplete.lblAmountValue.text,
        "Fee": frmBillPaymentComplete.lblPaymentFeeValue.text,
        "PaymentOrderDate": frmBillPaymentComplete.lblPaymentDateValue.text,
        "MyNote": frmBillPaymentComplete.lblMyNoteValue.text,
        "TransactionRefNo": frmBillPaymentComplete.lblTxnNumValue.text
    };
    if (GblBillTopFlag) {
        if (frmBillPaymentComplete.lblRef2.text != null && frmBillPaymentComplete.lblRef2.text != "" && frmBillPaymentComplete.lblRef2Value.text != null && frmBillPaymentComplete.lblRef2Value.text != "") {
            pdfImagedata["Ref2Label"] = frmBillPaymentComplete.lblRef2.text;
            pdfImagedata["Ref2Value"] = frmBillPaymentComplete.lblRef2Value.text;
        }
    }
    if (GblBillTopFlag) {
        if (gblPaynow) {
            activityTypeId = "027";
            inputParam["outputtemplatename"] = "Bill_Payment_Details_" + kony.os.date("DDMMYYYY");
        } else {
            activityTypeId = "028";
            inputParam["outputtemplatename"] = "Future_Bill_Payment_Set_Details_" + kony.os.date("DDMMYYYY");
            pdfImagedata["BillPaySchedule"] = "Payment Schedule";
            if (frmBillPaymentComplete.lblEndOnValue.text == "-") {
                pdfImagedata["PaymentSchedule"] = frmBillPaymentComplete.lblStartOnValue.text + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + frmBillPaymentComplete.lblEveryMonth.text;
            } else {
                pdfImagedata["PaymentSchedule"] = frmBillPaymentComplete.lblStartOnValue.text + " " + kony.i18n.getLocalizedString("keyTo") + " " + frmBillPaymentComplete.lblEndOnValue.text + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + frmBillPaymentComplete.lblEveryMonth.text + " for " + frmBillPaymentComplete.lblExecuteValue.text;
            }
        }
    } else {
        if (gblPaynow) {
            activityTypeId = "030";
            inputParam["outputtemplatename"] = "Top-Up_Payment_Details_" + kony.os.date("DDMMYYYY");
        } else {
            activityTypeId = "031";
            inputParam["outputtemplatename"] = "Future_Top-Up_Payment_Set_Details_" + kony.os.date("DDMMYYYY");
            pdfImagedata["TopUpSchedule"] = "Top-Up Schedule";
            if (frmBillPaymentComplete.lblEndOnValue.text == "-") {
                pdfImagedata["PaymentSchedule"] = frmBillPaymentComplete.lblStartOnValue.text + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + frmBillPaymentComplete.lblEveryMonth.text;
            } else {
                pdfImagedata["PaymentSchedule"] = frmBillPaymentComplete.lblStartOnValue.text + " " + kony.i18n.getLocalizedString("keyTo") + " " + frmBillPaymentComplete.lblEndOnValue.text + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + frmBillPaymentComplete.lblEveryMonth.text + " for " + frmBillPaymentComplete.lblExecuteValue.text;
            }
        }
    }
    inputParam["datajson"] = JSON.stringify(pdfImagedata, "", "");
    if (gblPaynow) {
        savePDFIB(filetype, activityTypeId, frmBillPaymentComplete.lblTxnNumValue.text);
    } else {
        saveFuturePDF(filetype, activityTypeId, frmBillPaymentComplete.lblTxnNumValue.text);
        //invokeServiceSecureAsync("generatePdfImage", inputParam, mbRenderFileCallbackfunction);
    }
}

function btnConfrimBillPaySPA() {
    if (gblIBFlowStatus == "04") {
        alertUserStatusLocked();
    } else {
        var inputParams = {}
        spaChnage = "topbillpayments"
        gblOTPFlag = true;
        try {
            kony.timer.cancel("otpTimer")
        } catch (e) {}
        //input params for SPA OTP
        var locale = kony.i18n.getCurrentLocale();
        if (GblBillTopFlag) {
            gblSpaChannel = "BillPayment";
            /*
            if (locale == "en_US") {
            	SpaEventNotificationPolicy = "MIB_BillPayment_EN";
            	SpaSMSSubject = "MIB_BillPayment_EN";
            }
            else {
            	SpaEventNotificationPolicy = "MIB_BillPayment_TH";
            	SpaSMSSubject = "MIB_BillPayment_TH";
            }*/
        } else {
            gblSpaChannel = "TopUpPayment";
            /*
            if (locale == "en_US") {
            	SpaEventNotificationPolicy = "MIB_MobileTopup_EN";
            	SpaSMSSubject = "MIB_MobileTopup_EN"; 
            }
            else {
               SpaEventNotificationPolicy = "MIB_MobileTopup_TH";
               SpaSMSSubject = "MIB_MobileTopup_TH";
            }
            */
        }
        gblSpaAmount = parseFloat(removeCommos(frmBillPaymentConfirmationFuture.lblAmountValue.text)).toFixed(2);
        gblSpaAmount = commaFormatted(gblSpaAmount);
        gblSpaBillerName = frmBillPaymentConfirmationFuture.lblBillerNickname.text;
        gblSpaRef1Value = frmBillPaymentConfirmationFuture.lblRef1Value.text;
        onClickOTPRequestSpa();
    }
}

function onClickHideMB() {
    var currForm = kony.application.getCurrentForm();
    if (currForm.hbxAmountDetailsMEA.isVisible) {
        currForm.hbxAmountDetailsMEA.setVisibility(false);
        currForm.lnkExpand.text = kony.i18n.getLocalizedString("show")
    } else {
        currForm.hbxAmountDetailsMEA.setVisibility(true);
        currForm.lnkExpand.text = kony.i18n.getLocalizedString("Hide")
    }
}

function dynamicListCardIPhone(cardCode, index) {
    var imgCard = "";
    var lblRibbon = "";
    var flexCard = "";
    var ribbonID = "lblRibbon" + cardCode + index;
    imgCard = new kony.ui.Image2({
        "id": "imgCard" + cardCode + index,
        "top": "0.0%",
        "width": "100%",
        "height": "100.0%",
        "zIndex": 1,
        "isVisible": true,
        "src": "https://dev.tau2904.com/tmb/ImageRender?crmId=&&personalizedId=&billerId=cardoneiphone6plus&modIdentifier=PRODUCTPACKAGEIMG&dummy=",
        "imageWhenFailed": null,
        "imageWhileDownloading": null,
        "skin": "slImage",
        "onTouchStart": onTouchStart,
        "onTouchEnd": cardEvent
    }, {
        "padding": [0, 0, 0, 0],
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {
        "glossyEffect": constants.IMAGE_GLOSSY_EFFECT_DEFAULT
    });
    lblRibbon = new kony.ui.Label({
        "id": "lblRibbon" + cardCode + index,
        "top": "17.0%",
        "right": "-10.0%",
        "width": "44.76%",
        "height": "10.0%",
        "zIndex": 1,
        "isVisible": true,
        "text": index + " Days Left!",
        "skin": "lblbgred"
    }, {
        "padding": [0, 1, 0, 1],
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var trans100 = kony.ui.makeAffineTransform();
    trans100.rotate(-45);
    lblRibbon.animate(kony.ui.createAnimation({
        "100": {
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            },
            "transform": trans100
        }
    }), {});
    flexCard = new kony.ui.FlexContainer({
        "id": "flexCard" + cardCode + index,
        "top": "0dp",
        "left": "10.89%",
        "width": "83%",
        "height": "40.0%",
        "centerX": "50.0%",
        "zIndex": 2,
        "isVisible": true,
        "clipBounds": true,
        "Location": "[39,0]",
        "skin": "noSkinFlexContainer",
        "layoutType": kony.flex.FREE_FORM
    }, {
        "padding": [0, 0, 0, 0]
    }, {});
    flexCard.setDefaultUnit(kony.flex.DP)
    flexCard.add(imgCard, lblRibbon);
    frmCCTest.FlexMain.add(flexCard);
    lblLinkText = new kony.ui.Label({
        "id": "lblLinkText" + cardCode + index,
        "top": "0dp",
        "centerX": "50%",
        "zIndex": 1,
        "isVisible": true,
        "text": "New! Activate Now",
        "skin": "lblbluemedium"
    }, {
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {
        "textCopyable": false
    });
    flexLink = new kony.ui.FlexContainer({
        "id": "flexLink" + cardCode + index,
        "bottom": "0dp",
        "left": "0dp",
        "width": "100%",
        "height": "35dp",
        "zIndex": 1,
        "isVisible": true,
        "clipBounds": true,
        "Location": "[0,217]",
        "skin": "slFbox",
        "layoutType": kony.flex.FREE_FORM
    }, {
        "padding": [0, 0, 0, 0]
    }, {});;
    flexLink.setDefaultUnit(kony.flex.DP)
    flexLink.add(lblLinkText);
    frmCCTest.FlexMain.add(flexLink);
}

function onClickShowCardsMenu() {
    frmMBManageCard.show();
}

function onClickManageCardsOfCashAdvCompleteScreen() {
    preShowMBCardList();
}

function AccountSumDelMB() {
    showLoadingScreen()
    var myAccountList_inputparam = {};
    myAccountList_inputparam["accountsFlag"] = "true";
    var status = invokeServiceSecureAsync("customerAccountInquiry", myAccountList_inputparam, myAcountSummaryDeleteMB);
}
/**
 * Function to skip the FATCA form if max limit not reached
 */
function onClickFATCASkip() {
    kony.print("onClickFATCASkip...entered");
    if (gblFATCASkipCounter >= GLOBAL_FATCA_MAX_SKIP_COUNT) {
        kony.print("onClickFATCASkip...if  case");
        //		FATCAGeneralPopup.hbxCloseInfo.setVisibility(true);
        FATCAGeneralPopup.lblHead.text = kony.i18n.getLocalizedString("keyFATCAError_004");
        FATCAGeneralPopup.btnClose.text = kony.i18n.getLocalizedString("keyClose");
        FATCAGeneralPopup.show();
    } else {
        gblInfoFlow = false;
        kony.print("onClickFATCASkip...else case");
        showLoadingScreen();
        if (flowSpa) callCustomerAccountService(gblFATCASkipCounter + "");
        else {
            if (GBL_Fatca_Flow == "pushnote") {
                gblPushNotificationFlag = false;
                pushNotificationAllow(gblPayLoadKpns, gblFATCASkipCounter + "");
            } else LoginProcessServExecMB(gblFATCASkipCounter + "");
        }
    }
}
//function AfterLogoutPreshow() {
//	getHeader(0, 0, 0, 0);
//	hbxArrow.imgHeaderMiddle.src = "empty.png";
//}
function onLogoutClick() {
    if (flowSpa) {
        assignPostLoginFormSetUndefined();
        popUpLogout.lblPopupLogoutSPA.text = kony.i18n.getLocalizedString("keyIBLogoutMesg");
    } else {
        popUpLogout.lblPopupLogout.text = kony.i18n.getLocalizedString("keyLogoutMessageLabel");
    }
    popUpLogout.show();
}
/*function IBcallLiquidityInqService() {
	//showLoadingScreenPopup();
	var inputparam = {};
 	
	invokeServiceSecureAsync("doLiquidityInq", inputparam, IBliquidityInqCallBack);
}*/
function IBliquidityInqCallBack(status, resulttable) {
    if (status == 400) {
        var StatusCode = resulttable["StatusCode"];
        var Severity = resulttable["Severity"];
        var StatusDesc = resulttable["StatusDesc"];
        if (resulttable["opstatus"] == 0) {
            if (resulttable["StatusCode"] == 0) {
                if (resulttable["maxCurAmt"] != null && resulttable["maxCurAmt"] != "") {
                    frmIBAccntSummary.lblUsefulValue2.text = commaFormatted(resulttable["maxCurAmt"]) + kony.i18n.getLocalizedString("currencyThaiBaht");
                    frmIBAccntSummary.hbxUseful2.isVisible = true;
                }
                if (resulttable["minCurAmt"] != null && resulttable["minCurAmt"] != "") {
                    frmIBAccntSummary.lblUsefulValue3.text = commaFormatted(resulttable["minCurAmt"]) + kony.i18n.getLocalizedString("currencyThaiBaht");
                    frmIBAccntSummary.hbxUseful3.isVisible = true;
                }
                if (null != resulttable["linkedAccount"] && resulttable["linkedAccount"] != "" && resulttable["linkedAccount"] != 0) {
                    var disPlayLinkAccount;
                    if (resulttable["linkedAccount"].length == 14) disPlayLinkAccount = resulttable["linkedAccount"].substring(4);
                    else disPlayLinkAccount = resulttable["linkedAccount"]
                    frmIBAccntSummary.lblUsefulValue11.text = formatAccountNo(disPlayLinkAccount);
                    frmIBAccntSummary.hbxUseful11.isVisible = true;
                }
                dismissLoadingScreenPopup();
                frmIBAccntSummary.show();
            } else {
                dismissLoadingScreenPopup();
                if (resulttable["additionalDS"][2]["StatusDesc"] == "LIQ NOT FOUND" && resulttable["additionalDS"][2]["StatusCode"] == "-1000") {
                    //frmIBAccntSummary.hboxApplySendToSave.isVisible = true;
                    frmIBAccntSummary.hbxUseful2.isVisible = false;
                    frmIBAccntSummary.hbxUseful3.isVisible = false;
                    frmIBAccntSummary.hbxUseful11.isVisible = false;
                } else showCommonAlert(resulttable["errMsg"], resulttable["XPServerStatCode"]);
            }
        } else {
            dismissLoadingScreenPopup();
            alert(" " + resulttable["errMsg"]);
        }
    }
}
//Not working as expected
function checkCitizenIDIB_old(text) {
    if (text == null) return false;
    var temp = "" + text;
    var textLen = temp.length;
    var digit_citizenID = [];
    var citizenID = [];
    var i = 0;
    for (i = 0; i < textLen; i++) {
        citizenID[i] = 0;
    }
    i = 0;
    while (text) {
        citizenID[textLen - 1 - i] = text % 10;
        text = (text - (text % 10)) / 10;
        i++;
    }
    var weight = [];
    if (textLen != 13) {
        return false;
    } else {
        weight[0] = 13;
        weight[1] = 12;
        weight[2] = 11;
        weight[3] = 10;
        weight[4] = 9;
        weight[5] = 8;
        weight[6] = 7;
        weight[7] = 6;
        weight[8] = 5;
        weight[9] = 4;
        weight[10] = 3;
        weight[11] = 2;
        var length = 1;
        var digit_multiply_result = 0;
        var summary_multiply_result = 0;
        var mod_result = 0;
        var subtract_result = 0;
        var position = 0;
        for (position = 0; position < 12; position++) {
            digit_citizenID[position] = citizenID[position];
            digit_multiply_result = digit_citizenID[position] * weight[position];
            summary_multiply_result = summary_multiply_result + digit_multiply_result;
        }
        mod_result = summary_multiply_result % 11;
        if (mod_result < 1) subtract_result = 1 - mod_result;
        else subtract_result = 11 - mod_result;
        if (subtract_result != citizenID[12]) {
            return false;
        }
    }
    return true;
}

function AccountNickName(text) {
    var tf = true;
    if ((text == null) || (text == "")) return false;
    var txtLen = text.length;
    if (txtLen > 20 || txtLen < 3) {
        //alert("INVALID MOBILE NICK NAME, PLEASE RE-ENTER");
        return false;
    }
    //below 2 lines are commented since we are calling this function in many places 
    // and giving wrong message if we need to call the  alphNumvalidationIB please handle separatly
    //dont uncomment the below code
    //tf=alphNumvalidationIB(text);
    //return tf;
}

function mbConnectAccValidatn() {
    var emailChk = frmConnectAccMB.tbxPopupTractPwdtxt.text;
    var deviceChk = frmConnectAccMB.txtDeviceName.text
    var emailFlag = emailValidatn(frmConnectAccMB.tbxPopupTractPwdtxt.text);
    var devNameFlag = MblNickName(frmConnectAccMB.txtDeviceName.text);
    var blankEmail = kony.i18n.getLocalizedString("keyMBEnteremailID");
    var blankDevicename = kony.i18n.getLocalizedString("keyMBEnterDevice");
    var invalidEmail1 = kony.i18n.getLocalizedString("invalidEmail");
    var invalidDivName = kony.i18n.getLocalizedString("invalidDeviceName");
    var info1 = kony.i18n.getLocalizedString("info");
    var okk = kony.i18n.getLocalizedString("keyOK");
    //gblActionCode = "21";
    //gActivationCode = "erT12345";
    if (emailChk == null || emailChk == "") {
        frmConnectAccMB.tbxPopupTractPwdtxt.skin = txtErrorBG;
        showAlert(blankEmail, info1);
        return false;
    }
    if (emailFlag == false) {
        frmConnectAccMB.tbxPopupTractPwdtxt.skin = txtErrorBG;
        showAlert(invalidEmail1, info1);
        return false;
        //	alert("INVALID EMAIL ID");
    }
    if (deviceChk == null || deviceChk == "") {
        frmConnectAccMB.txtDeviceName.skin = txtErrorBG;
        showAlert(blankDevicename, info1);
        return false;
    }
    if (devNameFlag == false) {
        frmConnectAccMB.txtDeviceName.skin = txtErrorBG;
        showAlert(invalidDivName, info1);
        return false;
        //	alert("INVALID DEVICE NAME");
    } else {
        GBL_FLOW_ID_CA = 1;
        //var deviceInfo = kony.os.deviceInfo();
        //if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPhone Simulator") {
        TrusteerDeviceId();
        //}else if (gblDeviceInfo["name"] == "android"){
        //}
        //updateDeviceServiceCall()
        //frmMBActiComplete.show();
    }
}
//Not working as expected
function checkCitizenID_old(text) {
    if (text == null) return false;
    var temp = "" + text;
    var textLen = temp.length;
    var digit_citizenID = [];
    var citizenID = [];
    var i = 0;
    for (i = 0; i < textLen; i++) {
        citizenID[i] = 0;
    }
    i = 0;
    while (text) {
        citizenID[textLen - 1 - i] = text % 10;
        text = (text - (text % 10)) / 10;
        i++;
    }
    var weight = [];
    if (textLen != 13) {
        return false;
    } else {
        weight[0] = 13;
        weight[1] = 12;
        weight[2] = 11;
        weight[3] = 10;
        weight[4] = 9;
        weight[5] = 8;
        weight[6] = 7;
        weight[7] = 6;
        weight[8] = 5;
        weight[9] = 4;
        weight[10] = 3;
        weight[11] = 2;
        var length = 1;
        var digit_multiply_result = 0;
        var summary_multiply_result = 0;
        var mod_result = 0;
        var subtract_result = 0;
        var position = 0;
        for (position = 0; position < 12; position++) {
            digit_citizenID[position] = citizenID[position];
            digit_multiply_result = digit_citizenID[position] * weight[position];
            summary_multiply_result = summary_multiply_result + digit_multiply_result;
        }
        mod_result = summary_multiply_result % 11;
        if (mod_result < 1) subtract_result = 1 - mod_result;
        else subtract_result = 11 - mod_result;
        if (subtract_result != citizenID[12]) {
            return false;
        }
    }
    return true;
}

function frmIBPreLoginGlobalsforIE8() {
    var MenuId = [];
    frmIBPreLogin = new kony.ui.Form2({
        "id": "frmIBPreLogin",
        "needAppMenu": true,
        "title": null,
        "headers": [hbxIBIE8PreLogin, hbxIBPreLogin],
        "footers": [hbox59324032046854, hbxFooterPrelogin],
        "enabledForIdleTimeout": true,
        "skin": "frmbgpage",
        "preShow": frmIBPreLogin_frmIBPreLogin_preshow_seq0,
        "postShow": frmIBPreLogin_frmIBPreLogin_postshow_seq0,
        "onHide": frmIBPreLogin_frmIBPreLogin_onhide_seq0,
        "addWidgets": addWidgetsfrmIBPreLogin
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {
        "retainScrollPosition": false,
        "onDeviceBack": frmIBPreLogin_frmIBPreLogin_onDeviceBack_seq0,
        "inTransitionConfig": {
            "formTransition": "None"
        },
        "outTransitionConfig": {
            "formTransition": "None"
        }
    });
    frmIBPreLogin.headers[0].isVisible = true;
    frmIBPreLogin.headers[1].isVisible = false;
}

function appMFPostInitOld() {
    try {
        kony.print("appPreInit()---->START");
        navigationStack = [];
        kony.print("appPreInit()---->STOP");
        // initialize Mobile Fabric SDK
        var appkey = "56d96f56889f2d7609dc1e01dfa03aa6";
        var appsecret = "4d40d7e51c752b9257c2e2afb78b1af3";
        var serviceurl = "http://10.21.12.36:9081/authService/100000002/appconfig";
        mfClient = new kony.sdk();
        kony.sdk.setXdomainLibPath("https://" + appConfig.serverIp + "/authService/resources/js/xdomain.min.js");
        kony.sdk.setXdomainSlaves("{https://" + appConfig.serverIp + ":/xdomain}");
        mfClient.init(appkey, appsecret, serviceurl, function(response) {
            kony.print("====>Successfully initialized as client of Mobile Fabric.");
            loadResourceBundle();
        }, function(error) {
            kony.print("====>Failed to initialize as client of Mobile Fabric.");
        });
    } catch (e) {
        kony.print("====>Exception in appPreInit:" + e.message);
    }
}

function loadTopupPage() {
    gblTopupFromMenu = true;
    getMyTopUpSuggestListIB();
    frmIBMyTopUpsHome.segMenuOptions.setData([{
        "lblSegData": {
            "skin": "lblIBSegMenu",
            "text": kony.i18n.getLocalizedString("keyMyProfile")
        }
    }, {
        "lblSegData": {
            "skin": "lblIBSegMenu",
            "text": kony.i18n.getLocalizedString("keyMyAccountsIB")
        }
    }, {
        "lblSegData": {
            "skin": "lblIBSegMenu",
            "text": kony.i18n.getLocalizedString("kelblOpenNewAccount")
        }
    }, {
        "lblSegData": {
            "skin": "lblIBSegMenu",
            "text": kony.i18n.getLocalizedString("keyMyRecipients")
        }
    }, {
        "lblSegData": {
            "skin": "lblIBSegMenu",
            "text": kony.i18n.getLocalizedString("keyMyBills")
        }
    }, {
        "lblSegData": {
            "skin": "lblIBsegMenuFocus",
            "text": kony.i18n.getLocalizedString("myTopUpsMB")
        }
    }]);
    if (kony.i18n.getCurrentLocale() != "th_TH") frmIBMyTopUpsHome.btnMenuAboutMe.skin = "btnIBMenuAboutMeFocus";
    else frmIBMyTopUpsHome.btnMenuAboutMe.skin = "btnIBMenuAboutMeFocusThai";
}
//Empty function to control the SPA browser back
function onDeviceSpaback() {
    //please dont commnet this function as it prevents the use of browser back for SPA
}

function onClickEmailTnCMB(tnckeyword) {
    //call notification add service for email with the tnc keyword
    //alert("tnckeyword::"+tnckeyword);
    var inputparam = {};
    inputparam["channelName"] = "Mobile Banking";
    inputparam["channelID"] = "02";
    inputparam["notificationType"] = "Email"; // always email
    inputparam["phoneNumber"] = gblPHONENUMBER;
    inputparam["mail"] = gblEmailId;
    inputparam["customerName"] = gblCustomerName;
    inputparam["localeCd"] = kony.i18n.getCurrentLocale();
    if (tnckeyword == undefined || tnckeyword == "") {
        inputparam["moduleKey"] = "TermsAndConditions";
    } else {
        inputparam["moduleKey"] = tnckeyword;
    }
    invokeServiceSecureAsync("TCEMailService", inputparam, callBackNotificationAddServiceMB);
}
/*****************************************************************
 *     Module  : Touch ID
 *     Purpose : Dismiss the TouchId Info Pop Up on click of cancel of pop up
 *     Params  : Password
 ******************************************************************/
function onClickNoTouchIdPopup() {
    popUpTouchIdInfo.dismiss();
}
/*****************************************************************
 *     Module  : Touch ID
 *     Purpose : on click of continue button of TouchId Info Pop Up for showing transaction password popup
 *     Params  : None
 ******************************************************************/
function onClickYesTouchIdPopup() {
    popUpTouchIdTransPwd.tbxPopTouchPwd.text = "";
    popUpTouchIdTransPwd.btnPopTouchConf.text = kony.i18n.getLocalizedString("keyConfirm");
    popUpTouchIdTransPwd.btnPopTouchCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
    popUpTouchIdTransPwd.show();
}
/*****************************************************************
 *     Module  : Touch ID
 *     Purpose : To authenticate a user for login
 *     Params  : None
 ******************************************************************/
function authenticateUser() {
    var touchStatus = TouchId.authenicateButtonTapped(popUpTouchIdCheck.btnYes.id);
    return touchStatus;
}
/*****************************************************************
 *     Module  : Touch ID
 *     Purpose : Show Touch Id Info Pop Up
 *     Params  : None
 ******************************************************************/
function onDisableTouchLogin() {
    //	setTouchPwdNormal();
    //	popUpTouchIdTransPwd.lblTouchPopupErrMsg.text =  kony.i18n.getLocalizedString("keyTransPwdMsg");
    //	popUpTouchIdTransPwd.lblTouchPopupErrMsg.skin = "lblPopupLabelTxt";
    //	//kony.store.setItem("usesTouchId", "N");
    //	popUpTouchIdTransPwd.show();
    onConfirmGetDeviceID();
}

function getTokenActivationCodeforSpa() {
    var tokenActiInputParams = {};
    tokenActiInputParams["crmId"] = gblcrmId;
    tokenActiInputParams["serialNum"] = frmSpaTokenactivationstartup.tbxTokenSerialNo.text;
    if (frmSpaTokenactivationstartup.tbxTokenSerialNo.text == null || frmSpaTokenactivationstartup.tbxTokenSerialNo.text.trim() == "") {
        alert(kony.i18n.getLocalizedString("keyTokenSerialNo"));
        //dismissLoadingScreen();
        return false;
    }
    showLoadingScreen();
    invokeServiceSecureAsync("getTokenActivationCode", tokenActiInputParams, getTokenActivationCodeCallBackforSpa);
}

function getTokenActivationCodeCallBackforSpa(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == "0") {
            var statusNumber = callBackResponse["TokenActivation"][0]["TOKEN_SERIAL_NO"];
            //alert(statusNumber);
            kony.application.dismissLoadingScreen();
            tokenSPAConfimScreen();
            frmSpaTokenactivationstartup.lblHdrTxt.text = kony.i18n.getLocalizedString("Confirmation");
            frmSpaTokenactivationstartup.lblTokenNumber.text = statusNumber;
            tokenActivationParamsInSession(frmSpaTokenactivationstartup.tbxTokenSerialNo.text);
        } else if (callBackResponse["opstatus"] == "9") {
            alert(kony.i18n.getLocalizedString("keyTokenAssignStatus"));
            dismissLoadingScreen();
            return false;
        } else if (callBackResponse["opstatus"] == "1") {
            alert(kony.i18n.getLocalizedString("KeyTokenStatusError"));
            dismissLoadingScreen();
            return false;
        } else if (callBackResponse["opstatus"] == "2") {
            alert(kony.i18n.getLocalizedString("KeyTokenSerialNumError"));
            dismissLoadingScreen();
            return false;
        } else {
            alert(kony.i18n.getLocalizedString("KeyTokenSerialNumError"));
            dismissLoadingScreen();
            return false;
        }
    } else {}
}

function cancelActivationPageforSpa() {
    showLoadingScreen();
    frmSpaTokenactivationstartup.lblHdrTxt.text = "Token Activation"
    frmSpaTokenactivationstartup.hbox10412207892974.setVisibility(false);
    frmSpaTokenactivationstartup.hbxConfirm.setVisibility(false);
    frmSpaTokenactivationstartup.hbxSerialNo.setVisibility(true);
    frmSpaTokenactivationstartup.tbxTokenSerialNo.text = "";
    frmSpaTokenactivationstartup.hbox10412207892954.setVisibility(true);
    dismissIBLoadingScreen();
}

function verifyHWTKNforSpa() {
    if (frmSpaTokenactivationstartup.tbxTokenOTP.text == undefined || frmSpaTokenactivationstartup.tbxTokenOTP.text.trim() == "") {
        alert(kony.i18n.getLocalizedString("keyOTPFrmToken"));
        //alert("Please Enter Token No");
        return false;
    } else {
        gblVerifyToken = gblVerifyToken + 1;
        inputParam = {};
        inputParam["loginModuleId"] = "IB_HWTKN";
        inputParam["userStoreId"] = "DefaultStore";
        inputParam["retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
        inputParam["retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
        inputParam["userId"] = gblUserName;
        inputParam["password"] = frmSpaTokenactivationstartup.tbxTokenOTP.text;
        inputParam["sessionVal"] = "";
        inputParam["segmentId"] = "segmentId";
        inputParam["segmentIdVal"] = "MIB";
        inputParam["channel"] = "InterNet Banking";
        inputParam["serialNum"] = frmSpaTokenactivationstartup.tbxTokenSerialNo.text;
        invokeServiceSecureAsync("verifyTokenEx", inputParam, verifyPasswordCallBackforSpa);
    }
}

function verifyPasswordCallBackforSpa(status, callBackResponse) {
    frmSpaTokenactivationstartup.tbxTokenOTP.text = "";
    if (status == 400) {
        if (callBackResponse["opstatus"] == 0) {
            gblTokenActivationFlag = false;
            frmSpaTokenConfirmation.show()
        } else {
            if (callBackResponse["errCode"] == 'VrfyOTPErr00002') {
                popIBTransNowOTPLocked.show();
                popIBTransNowOTPLocked.lblDetails1.text = kony.i18n.getLocalizedString("tokenLockText");
            }
            if (callBackResponse["errCode"] == 'VrfyOTPErr00001') {
                if (callBackResponse["errMsg"] != undefined) {
                    //alert(callBackResponse["errMsg"]);
                    alert(kony.i18n.getLocalizedString("invalidOTP"));
                } else {
                    alert(kony.i18n.getLocalizedString("invalidOTP"));
                }
                return false;
            }
        }
    }
}

function tokenSPAConfimScreen() {
    frmSpaTokenactivationstartup.lblHdrTxt.text = kony.i18n.getLocalizedString("Confirmation");
    frmSpaTokenactivationstartup.hbox10412207892974.setVisibility(true);
    frmSpaTokenactivationstartup.hbxConfirm.setVisibility(true);
    frmSpaTokenactivationstartup.lblTokenSerialNoConfirm.text = kony.i18n.getLocalizedString("keyPleaseEnterToken");
    frmSpaTokenactivationstartup.btnTokenConfirm.text = kony.i18n.getLocalizedString("keyConfirm");
    frmSpaTokenactivationstartup.tbxTokenOTP.setFocus(true);
    //frmSpaTokenactivationstartup.tbxTokenOTP.placeholder=kony.i18n.getLocalizedString("keyOTP");
    frmSpaTokenactivationstartup.button506459299456960.text = kony.i18n.getLocalizedString("keyCancelButton");
    frmSpaTokenactivationstartup.hbxSerialNo.setVisibility(false);
    frmSpaTokenactivationstartup.hbox10412207892954.setVisibility(false);
}

function menuOptionEnableTokenActivationSPA() {
    var tokenMenuEnableParams = {};
    tokenMenuEnableParams["crmId"] = gblcrmId;
    invokeServiceSecureAsync("menuOptionEnableTokenActivation", tokenMenuEnableParams, menuOptionEnableTokenActivationCallBackSPA);
}

function menuOptionEnableTokenActivationCallBackSPA(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == 0) {
            var tokenMenuFlag = false
            var tokenEnable = callBackResponse["menuEnableStatus"];
            if (tokenEnable == "true") {
                gblTokenActivationFlag = true;
            } else {
                gblTokenActivationFlag = false;
            }
        }
    }
}

function changeuseridbubble() {
    //frmCMChgAccessPin.txtCurUserID.skin = txtBottomBorderFocus;
    var temp = frmCMChgAccessPin.txtCurUserID.text;
    if (temp.length != null && temp.length < 1) {
        popupBubbleUserId.show();
        //alert("control here")
    } else {
        popupBubbleUserId.dismiss();
    }
}
/**
 *  method for navigating from menu to Bill Payment
 * @param {}
 * @returns {}
 */
function navigateMenuBillPayment() {
    nameofform = kony.application.getCurrentForm();
    //nameofform.hbxMenuBillPayment.skin = "hbxIBBillPayMenuFocus"
    nameofform.segMenuOptions.removeAll();
}
/**
 * method for navigating from menu to Topup Payment
 * @param {}
 * @returns {}
 */
function navigateMenuTopupPayment() {
    nameofform = kony.application.getCurrentForm();
    //nameofform.hbxMenuTopUp.skin = "hbxIBTopUpMenuFocus"
    nameofform.segMenuOptions.removeAll();
}

function loadAddBillerValuesFromBillPayIB() {
    frmIBMyBillersHome.segBillersConfirm.removeAll();
    var indexOfSelectedIndex = frmIBBillPaymentLP.segBPSgstdBillerList.selectedItems[0];
    gblBillerCompCode = "";
    gblBillerId = "";
    gblBillerMethod = "";
    gblRef2Flag = "";
    gblIsRef2RequiredIB = "";
    gblRef1LenIB = "";
    gblRef2LenIB = "";
    gblBillerId = indexOfSelectedIndex.BillerID;
    gblToAccountKey = indexOfSelectedIndex.ToAccountKey;
    gblPayFull = indexOfSelectedIndex.IsFullPayment;
    gblBillerMethod = indexOfSelectedIndex.BillerMethod;
    gblbillerGroupType = indexOfSelectedIndex.BillerGroupType;
    gblreccuringDisableAdd = indexOfSelectedIndex.IsRequiredRefNumber2Add;
    gblreccuringDisablePay = indexOfSelectedIndex.IsRequiredRefNumber2Pay;
    gblEffDt = indexOfSelectedIndex.EffDt;
    gblRef1LenIB = indexOfSelectedIndex.Ref1Len;
    frmIBMyBillersHome.txtAddBillerRef1.maxTextLength = gblRef1LenIB;
    gblBillerCompCode = indexOfSelectedIndex.BillerCompCode;
    if (gblBillerCompCode == "2533") {
        gblSegBillerData = indexOfSelectedIndex;
    }
    gblBillerBancassurance = indexOfSelectedIndex.billerBancassurance;
    gblAllowRef1AlphaNum = indexOfSelectedIndex.allowRef1AlphaNum;
    if (isNotBlank(frmIBBillPaymentLP.segBPSgstdBillerList.selectedItems[0].BillerCategoryID.text)) {
        gblBillerCategoryID = frmIBBillPaymentLP.segBPSgstdBillerList.selectedItems[0].BillerCategoryID.text;
    } else {
        gblBillerCategoryID = frmIBBillPaymentLP.segBPSgstdBillerList.selectedItems[0].BillerCategoryID;
    }
    frmIBMyBillersHome.lblAddBillerName.text = indexOfSelectedIndex.lblSgstdBillerName;
    frmIBMyBillersHome.lblAddBillerRef1.text = indexOfSelectedIndex.Ref1Label + ":";
    gblCurRef1LblTH = indexOfSelectedIndex.Ref1TH + ":";
    gblCurRef1LblEN = indexOfSelectedIndex.Ref1EN + ":";
    gblCurBillerNameEN = indexOfSelectedIndex.lblSgstdBillerNameEN;
    gblCurBillerNameTH = indexOfSelectedIndex.lblSgstdBillerNameTH;
    frmIBMyBillersHome.imgAddBillerLogo.src = indexOfSelectedIndex.imgSgstdBiller.src;
    gblIsRef2RequiredIB = indexOfSelectedIndex.IsRequiredRefNumber2Add;
    gblRef2LenIB = indexOfSelectedIndex.Ref2Len;
    if (kony.string.equalsIgnoreCase(gblIsRef2RequiredIB, "N")) {
        frmIBMyBillersHome.txtAddBillerRef2.text = "";
        //gblCurRef2LblTH = ""
        //gblCurRef2LblEN = ""
        frmIBMyBillersHome.hbxBillerRef2.setVisibility(false);
        frmIBMyBillersHome.line4belowRef2.setVisibility(false); //Added for DF2248
        //gblRef2LenIB = 0;
    } else if (kony.string.equalsIgnoreCase(gblIsRef2RequiredIB, "Y")) {
        gblRef2Flag = true;
        frmIBMyBillersHome.txtAddBillerRef2.setEnabled(true);
        frmIBMyBillersHome.line4belowRef2.setVisibility(true); //Added for DF2248
        frmIBMyBillersHome.lblAddBillerRef2.text = indexOfSelectedIndex.Ref2Label + ":";
        //gblCurRef2LblTH = indexOfSelectedIndex.Ref2TH + ":";
        //gblCurRef2LblEN = indexOfSelectedIndex.Ref2EN + ":";
        //gblRef2LenIB = indexOfSelectedIndex.Ref2Len;
        frmIBMyBillersHome.txtAddBillerRef2.maxTextLength = gblRef2LenIB;
        frmIBMyBillersHome.hbxBillerRef2.setVisibility(true);
    }
    gblCurRef2LblTH = indexOfSelectedIndex.Ref2TH + ":";
    gblCurRef2LblEN = indexOfSelectedIndex.Ref2EN + ":";
    gblRef1LblEN = gblCurRef1LblEN;
    gblRef1LblTH = gblCurRef1LblTH;
    gblRef2LblEN = gblCurRef2LblEN;
    gblRef2LblTH = gblCurRef2LblTH;
    gblBillerCompCodeEN = gblCurBillerNameEN;
    gblBillerCompCodeTH = gblCurBillerNameTH;
    showLoadingScreenPopup();
    frmIBMyBillersHome.show();
}

function loadAddTopupValuesFromTopUpPay() {
    frmIBMyTopUpsHome.segBillersConfirm.removeAll();
    var indexOfSelectedIndex = frmIBTopUpLandingPage.segSuggestedBiller.selectedItems[0];
    gblBillerCompCode = "";
    gblBillerId = "";
    gblBillerMethod = "";
    gblRef2Flag = "";
    gblRef1LenIB = "";
    gblBillerId = indexOfSelectedIndex.BillerID;
    gblBillerMethod = indexOfSelectedIndex.BillerMethod;
    gblEffDt = indexOfSelectedIndex.EffDt;
    gblRef1LenIB = indexOfSelectedIndex.Ref1Len;
    gblreccuringDisableAdd = indexOfSelectedIndex.IsRequiredRefNumber2Add;
    gblreccuringDisablePay = indexOfSelectedIndex.IsRequiredRefNumber2Pay;
    gblbillerGroupType = indexOfSelectedIndex.BillerGroupType;
    gblToAccountKey = indexOfSelectedIndex.ToAccountKey;
    gblPayFull = indexOfSelectedIndex.IsFullPayment;
    frmIBMyTopUpsHome.txtAddBillerRef1.maxTextLength = gblRef1LenIB;
    gblBillerCompCode = indexOfSelectedIndex.BillerCompCode;
    gblBillerCategoryID = frmIBTopUpLandingPage.segSuggestedBiller.selectedItems[0].BillerCategoryID.text;
    frmIBMyTopUpsHome.imgAddBillerLogo.src = indexOfSelectedIndex.imgSuggestedBiller.src;
    frmIBMyTopUpsHome.lblAddBillerName.text = indexOfSelectedIndex.lblSuggestedBiller;
    frmIBMyTopUpsHome.lblAddBillerRef1.text = indexOfSelectedIndex.Ref1Label + ":";
    gblCurRef1LblEN = indexOfSelectedIndex.Ref1EN + ":";
    gblCurRef1LblTH = indexOfSelectedIndex.Ref1TH + ":";
    gblCurBillerNameEN = indexOfSelectedIndex.lblSuggestedBillerEN;
    gblCurBillerNameTH = indexOfSelectedIndex.lblSuggestedBillerTH;
    gblRef1LblEN = gblCurRef1LblEN;
    gblRef1LblTH = gblCurRef1LblTH;
    gblBillerCompCodeEN = gblCurBillerNameEN;
    gblBillerCompCodeTH = gblCurBillerNameTH;
    showLoadingScreenPopup();
    frmIBMyTopUpsHome.show();
}

function frmMBFtScheduleMenuPostshow() {
    if (gblCallPrePost) {
        ehFrmMBFtSchedule_frmMBFtSchedule_postshow.call(this);
    }
    assignGlobalForMenuPostshow();
}

function startRCTransactionSecurityValidationServiceIB() {
    var inputParam = {}
    showLoadingScreenPopup();
    invokeServiceSecureAsync("crmProfileInq", inputParam, startTransactionSecurityValidationServiceAsyncCallbackIB)
}

function ehFrmIBCMChgMobNoTxnLimit_preShow(eventobject, neworientation) {
    frmIBCMChgMobNoTxnLimit.hbxTokenEntry.setVisibility("false");
    frmIBCMChgMobNoTxnLimit.image2474288733627.setVisibility(false);
    IBPreShowChngeMobNuTransLimit();
};

function ehFrmIBCMChgMobNoTxnLimit_postshow(eventobject, neworientation) {
    addIBMenu();
    segAboutMeLoad();
    pagespecificSubMenu();
    IBCMCommonMyProfilePreShow();
    uploadmyprofilemobile();
    frmIBCMChgMobNoTxnLimit.link508395714289050.hoverSkin = "lnkMyReqHisFoc";
    frmIBCMChgMobNoTxnLimit.txtChangeTransactionLimit.setFocus(true);
    addNumberCheckListner(txtChangeTransactionLimit);
};

function ehFrmIBCMChgMobNoTxnLimit_txtChangeMobileNumber_onBeginEditing(eventobject, changedtext) {
    var noniphendata = "";
    var txt = frmIBCMChgMobNoTxnLimit.txtChangeMobileNumber.text
    var txtLen = frmIBCMChgMobNoTxnLimit.txtChangeMobileNumber.text.length;
    for (i = 0; i < txtLen; i++)
        if (txt[i] != "-") noniphendata += txt[i];
    frmIBCMChgMobNoTxnLimit.txtChangeMobileNumber.text = noniphendata;
};

function ehFrmIBCMChgMobNoTxnLimit_txtChangeMobileNumber_onEndEditing(eventobject, changedtext) {
    if (kony.appinit.isIE8) {
        onEditMobileNumberIB(frmIBCMChgMobNoTxnLimit.txtChangeMobileNumber.text);
    }
};

function ehFrmIBCMChgMobNoTxnLimit_txtChangeMobileNumber_onKeyUp(eventobject, changedtext) {
    dontAllowNonNeumaric(frmIBCMChgMobNoTxnLimit.txtChangeMobileNumber);
    if (!kony.appinit.isIE8) { // your function not for IE8
        onEditMobileNumberIB(frmIBCMChgMobNoTxnLimit.txtChangeMobileNumber.text);
    }
};

function ehFrmIBCMChgMobNoTxnLimit_txtChangeTransactionLimit_onEndEditing(eventobject, changedtext) {
    if (frmIBCMChgMobNoTxnLimit.txtChangeTransactionLimit.text) {
        frmIBCMChgMobNoTxnLimit.txtChangeTransactionLimit.text = commaFormattedOpenAct(frmIBCMChgMobNoTxnLimit.txtChangeTransactionLimit.text);
    }
};

function ehFrmIBCMChgMobNoTxnLimit_txtBxOTP_onBeginEditing(eventobject, changedtext) {
    frmIBCMChgMobNoTxnLimit.hbxOTPEntry.skin = "hbxOtpTextField"
};

function ehFrmIBCMChgMobNoTxnLimit_txtBxOTP_onEndEditing(eventobject, changedtext) {
    frmIBCMChgMobNoTxnLimit.hbxOTPEntry.skin = "hbxOtpTextFieldNormal"
};

function ehFrmIBCMChgMobNoTxnLimit_btnOTPReq_onClick(eventobject) {
    var currform = kony.application.getCurrentForm();
    currform.txtBxOTP.text = "";
    requestButtonForOTPIBmobile();
};

function ehFrmIBCMChgMobNoTxnLimit_tbxToken_onBeginEditing(eventobject, changedtext) {
    frmIBCMChgMobNoTxnLimit.hbxTokenEntry.skin = "hbxOtpTextField"
};

function ehFrmIBCMChgMobNoTxnLimit_tbxToken_onEndEditing(eventobject, changedtext) {
    frmIBCMChgMobNoTxnLimit.hbxTokenEntry.skin = "hbxOtpTextFieldNormal"
};

function ehFrmIBCMChgMobNoTxnLimit_button478902715243193_onClick(eventobject) {
    if (frmIBCMChgMobNoTxnLimit.hbxChangeMobileNumber.isVisible) {
        IBNewMobNoOTPRequestService1();
    } else {
        IBTransLimitOTPRequestService();
    }
    frmIBCMChgMobNoTxnLimit.hbxTokenEntry.setVisibility(false);
    frmIBCMChgMobNoTxnLimit.hbxOTPEntry.setVisibility(true);
    frmIBCMChgMobNoTxnLimit.lblkeyOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
    gblTokenSwitchFlag = false;
    gblSwitchToken = true;
};

function ehFrmIBCMChgMobNoTxnLimit_button474230331217991_onClick(eventobject) {
    frmIBCMMyProfile.lblRequestHeader.setVisibility(false);
    frmIBCMMyProfile.hbxListBox.setVisibility(false);
    frmIBCMMyProfile.hbxData.setVisibility(false);
    frmIBCMMyProfile.hbximage.setVisibility(true);
    frmIBCMMyProfile.lnkHeader.setVisibility(false);
    TMBUtil.DestroyForm(frmIBCMChgMobNoTxnLimit);
    completeicon = false;
    frmIBCMMyProfile.show();
};

function ehfrmIBCMChgMobNoTxnLimit_button474288733632_onClick(eventobject) {
    editbuttonflag = "profile";
    showLoadingScreenPopup();
    editConfirm = false;
    gblAddressFlag = 1;
    gblMyProfileAddressFlag = "state";
    frmIBCMEditMyProfile.hbxRequest.setVisibility(false);
    frmIBCMEditMyProfile.arrowrequest.setVisibility(false);
    frmIBCMEditMyProfile.label475124774164.setVisibility(true);
    frmIBCMEditMyProfile.hboxEdit.setVisibility(true);
    frmIBCMEditMyProfile.hbxCancelSave.setVisibility(true);
    getIBEditProfileStatus();
};

function ehFrmIBCMChgMobNoTxnLimit_button449081826392299_onClick(eventobject) {
    editbuttonflag = "number"
    gblMobNoTransLimitFlag = true;
    flagMobNum = "new";
    frmIBCMChgMobNoTxnLimit.hbxRequest.setVisibility(false);
    frmIBCMChgMobNoTxnLimit.lblHead.setVisibility(true);
    frmIBCMChgMobNoTxnLimit.hbxChangeMobileNumber.setVisibility(true);
    frmIBCMChgMobNoTxnLimit.arrowrequest.setVisibility(false);
    frmIBCMChgMobNoTxnLimit.ArrowMobNu.setVisibility(true);
    getIBEditProfileStatus();
};

function ehFrmTransfersAckCalendar_hbox101271281131304_onClick() {
    if (gblAckFlage == "true") {
        frmTransfersAckCalendar.lblTransNPbAckBalAfter.setVisibility(false);
        frmTransfersAckCalendar.lblTransNPbAckFrmBal.setVisibility(false)
        frmTransfersAckCalendar.lblHide.text = kony.i18n.getLocalizedString("keyUnhide")
        gblAckFlage = "false"
    } else {
        frmTransfersAckCalendar.lblTransNPbAckBalAfter.setVisibility(true);
        frmTransfersAckCalendar.lblTransNPbAckFrmBal.setVisibility(true)
        frmTransfersAckCalendar.lblHide.text = kony.i18n.getLocalizedString("Hide")
        gblAckFlage = "true"
    }
};

function ehFrmTransfersAckCalendar_hbox47792425956441_onClick() {
    if (isMenuShown == false) {
        handleMenuBtn();
    } else {
        onClickForInnerBoxes();
    }
};

function ehFrmTransfersAckCalendar_btnTransNPbAckReturnSpa_onClick() {
    frmMBMyActivities.show();
    showCalendar(gsSelectedDate, frmMBMyActivities);
    MBMyActivitiesShowCalendar();
};

function ehFrmTransfersAckCalendar_btnRight_onClick() {
    var btnskin = "btnShare";
    var btnFocusSkin = "btnShareFoc";
    if (frmTransfersAckCalendar.hboxaddfblist.isVisible) {
        frmTransfersAckCalendar.hboxaddfblist.isVisible = false;
        frmTransfersAckCalendar.btnRight.skin = btnskin;
        frmTransfersAckCalendar.imgHeaderMiddle.src = "arrowtop.png";
        frmTransfersAckCalendar.imgHeaderRight.src = "empty.png";
    } else {
        frmTransfersAckCalendar.hboxaddfblist.isVisible = true;
        frmTransfersAckCalendar.btnRight.skin = btnFocusSkin;
        frmTransfersAckCalendar.imgHeaderMiddle.src = "empty.png";
        frmTransfersAckCalendar.imgHeaderRight.src = "arrowtop.png";
    }
};

function ehFrmTransfersAckCalendar_frmTransfersAckCalendar_postshow() {};

function ehFrmTransfersAck_hbox47792425956439_onClick() {
    if (isMenuShown == false) {
        handleMenuBtn();
    } else {
        onClickForInnerBoxes()
    }
};

function ehFrmTransfersAck_btnTransNPbAckReturnSpa_onClick() {
    TMBUtil.DestroyForm(frmTransferLanding);
    TMBUtil.DestroyForm(frmAccountSummaryLanding);
    cleanUpGlobalVariableTransfersMB();
    //frmAccountSummaryLanding = null;
    //    frmAccountSummaryLandingGlobals();
    gblAccountTable = "";
    callCustomerAccountService();
};

function ehFrmTransfersAck_btnEmailto_onClick() {
    requestFromForm = "frmTransfersAck";
    gblPOWcustNME = gblUserName
    if (gblPaynow) {
        gblPOWtransXN = kony.i18n.getLocalizedString("Transfer");
    } else {
        gblPOWtransXN = kony.i18n.getLocalizedString("Transfer_ScheduledTransfer");
    }
    gblPOWchannel = "MB"
    postOnWall();
};

function ehFrmTransferLanding_btnTranLandNextSpa_onClick() {
    if (isMenuShown == false) {
        onTransferLndngNext();
    } else {
        frmTransferLanding.scrollboxMain.scrollToEnd();
        isMenuShown = false;
    }
};

function ehFrmIBCMChgMobNoTxnLimit_button449081826392311_onClick(eventobject) {
    editbuttonflag = "userid";
    gblCMUserIDRule = 1;
    frmIBCMChngUserID.txtCurUserID.text = ""
    frmIBCMChngUserID.txtNewUserID.text = ""
    frmIBCMChngUserID.txtCurUserID.setFocus(true);
    frmIBCMChngUserID.hbxRequest.setVisibility(false);
    frmIBCMChngUserID.lblChngUID.setVisibility(true);
    frmIBCMChngUserID.hbox47428873349614.setVisibility(true);
    frmIBCMChngUserID.hbxCancelSave.setVisibility(true);
    frmIBCMChngUserID.arrowUserId.setVisibility(true);
    frmIBCMChngUserID.arrowrequest.setVisibility(false);
    getIBEditProfileStatus();
};

function ehFrmIBCMChgMobNoTxnLimit_button474288733659_onClick(eventobject) {
    editbuttonflag = "password";
    gblShowPwd = 1;
    frmIBCMChgPwd.tbxTranscCrntPwd.text = ""
    frmIBCMChgPwd.txtPassword.text = ""
    frmIBCMChgPwd.txtRetypePwd.text = ""
    frmIBCMChgPwd.tbxTranscCrntPwd.setFocus(true);
    frmIBCMChgPwd.hbxRequest.setVisibility(false);
    frmIBCMChgPwd.lnkHeader.setVisibility(true);
    frmIBCMChgPwd.lblConfirmation.setVisibility(true);
    frmIBCMChgPwd.hbox47428873359193.setVisibility(true);
    frmIBCMChgPwd.hbxCancelSaveOld.setVisibility(true);
    frmIBCMChgPwd.ArrowEditProf.setVisibility(true);
    frmIBCMChgPwd.arrowrequest.setVisibility(false);
    getIBEditProfileStatus();
};

function ehFrmIBCMChgMobNoTxnLimit_button474288733664_onClick(eventobject) {
    editbuttonflag = "limit"
    flagMobNum = "new";
    gblMobNoTransLimitFlag = false;
    frmIBCMChgMobNoTxnLimit.hbxRequest.setVisibility(false);
    frmIBCMChgMobNoTxnLimit.lblHead.setVisibility(true);
    frmIBCMChgMobNoTxnLimit.hbxChangeTransactionLimit.setVisibility(true);
    frmIBCMChgMobNoTxnLimit.hbox47505957819121.setVisibility(true);
    frmIBCMChgMobNoTxnLimit.arrowlimit.setVisibility(true);
    frmIBCMChgMobNoTxnLimit.arrowrequest.setVisibility(false);
    getIBEditProfileStatus();
};

function ehFrmIBCMChgTxnLimit_button473305630173680_onClick(eventobject) {
    frmIBCMMyProfile.show();
    TMBUtil.DestroyForm(frmIBCMChgPwd);
};

function ehFrmIBCMChgTxnLimit_button474288733654_onClick(eventobject) {
    gblCMUserIDRule = 1;
    frmIBCMChngUserID.txtCurUserID.text = "";
    frmIBCMChngUserID.txtNewUserID.text = "";
    frmIBCMChngUserID.show();
};

function ehFrmIBCMChgTxnLimit_button474288733659_onClick(eventobject) {
    gblShowPwd = 1;
    frmIBCMChgPwd.tbxTranscCrntPwd.text = "";
    frmIBCMChgPwd.txtPassword.text = "";
    frmIBCMChgPwd.txtRetypePwd.text = "";
    frmIBCMChgPwd.show();
};

function ehFrmIBExecutedTransaction_frmIBExecutedTransaction_postshow(eventobject, neworientation) {
    addIBMenu();
    if (kony.i18n.getCurrentLocale() == "th_TH") {
        frmIBExecutedTransaction.btnMenuMyActivities.skin = "btnIBMenuMyActivitiesFocusThai";
    } else {
        frmIBExecutedTransaction.btnMenuMyActivities.skin = "btnIBMenuMyActivitiesFocus";
    }
};

function ehFrmIBExecutedTransaction_vbox4740477151923676_onClick(eventobject) {
    if (frmIBExecutedTransaction.lblBBPaymentValue.isVisible) {
        frmIBExecutedTransaction.lblBBPaymentValue.setVisibility(false);
        frmIBExecutedTransaction.lblHide.text = kony.i18n.getLocalizedString("keyUnhide");
    } else {
        frmIBExecutedTransaction.lblBBPaymentValue.setVisibility(true);
        frmIBExecutedTransaction.lblHide.text = kony.i18n.getLocalizedString("Hide");
    }
};

function ehFrmTransferLanding_hbxTranLandRecNote_onClick() {
    frmTransferLanding.txtTranLandRecNote.setFocus(true)
};

function ehFrmTransferLanding_hbxTranLandToSel_onClick() {};

function ehFrmTransferLanding_btnTDcombo_onClick() {
    selectTDDetails();
};

function ehFrmIBTransferNowConfirmation_frmIBTransferNowConfirmation_preshow(eventobject, neworientation) {
    validateSecurityForTransactionServiceIB();
};

function ehFrmIBTransferNowConfirmation_frmIBTransferNowConfirmation_onDeviceBack(eventobject, neworientation) {
    disableBackButton();
};

function ehFrmAccTrcPwdInter_frmAccTrcPwdInter_preshow(eventobject, neworientation) {
    popupTractPwd.destroy();
    frmAccTrcPwdInter.scrollboxMain.scrollToEnd();
    frmAccTrcPwdInterPreShow();
    hbxFooterPwdNote.btnPopUpTermination.text = kony.i18n.getLocalizedString("Next");
    isMenuShown = false;
    DisableFadingEdges(frmAccTrcPwdInter);
};

function ehFrmAccTrcPwdInter_frmAccTrcPwdInter_postshow(eventobject, neworientation) {
    frmAccTrcPwdInter.scrollboxMain.scrollToEnd();
};

function ehFrmMBFTSchduleOldFt_btnWeekly_onClick() {
    frmMBFTSchdule.hbxEndingBtns.setVisibility(true)
    frmMBFTSchdule.lblEnding.setVisibility(true)
    frmMBFTSchdule.hbxNumOfTimes.setVisibility(false)
    frmMBFTSchdule.hbxFtEndOnDate.setVisibility(false)
    frmMBFTSchdule.lblInsturction.setVisibility(false)
};

function ehFrmMBFTSchduleOldFt_btnMonthly_onClick() {
    frmMBFTSchdule.hbxEndingBtns.setVisibility(true)
    frmMBFTSchdule.lblEnding.setVisibility(true)
    frmMBFTSchdule.hbxNumOfTimes.setVisibility(false)
    frmMBFTSchdule.hbxFtEndOnDate.setVisibility(false)
    frmMBFTSchdule.lblInsturction.setVisibility(false)
};

function ehFrmIBFTrnsrEditCnfmtn_button1014589649276_onClick(eventobject) {
    gblPOWcustNME = frmIBFTrnsrEditCnfmtn.lblFrmAccntNickName.text;
    gblPOWtransXN = "EditFutureTransfer"
    gblPOWchannel = "IB"
    postOnWall()
    requestfromform = "frmIBFTrnsrEditCnfmtn"
        /* 
fbShareFutureTransfersIB.call(this);

 */
};

function ehFrmIBFTrnsrView_btnReturnView_onClick(eventobject) {
    /*
frmIBMyActivities.show();
if(frmIBMyActivities.btncalender.focusSkin == btnIBTabBlue)
showCalendar(gsSelectedDate,frmIBMyActivities);
*/
    IBMyActivitiesShowCalendar();
};

function ehFrmIBTransferTemplate_btnMenuAboutMe_onClick(eventobject) {
    segAboutMeLoad();
    nameofform = kony.application.getCurrentForm();
    nameofform.btnMenuAboutMe.skin = "btnIBMenuAboutMe";
    nameofform.btnMenuAboutMe.hoverSkin = "btnIBMenuAboutMe";
    gblMenuSelection = 1;
};

function ehFrmMBFTViewCalendar_frmMBFTViewCalendar_preshow() {
    DisableFadingEdges(frmMBFTViewCalendar);
};

function ehFrmMBFTViewCalendar_frmMBFTViewCalendar_postshow() {};

function ehFrmTranfersToRecipents_frmTranfersToRecipents_postshow() {};

function ehFrmTransferConfirm_frmTransferConfirm_postshow() {};

function ehFrmMBFTEditCmpleteCalendar_button4455366701025366_onClick() {
    frmMBMyActivities.show();
    showCalendar(gsSelectedDate, frmMBMyActivities);
    MBMyActivitiesShowCalendar();
};

function ehFrmMBFTEditCmpleteCalendar_btnRight_onClick() {
    frmMBFTEditCmplete.hboxSharelist.setVisibility(true)
};

function ehFrmIBFTrnsrEditCnfmtn_frmIBFTrnsrEditCnfmtn_preshow(eventobject, neworientation) {
    varAvailBal = false;
    frmIBFTrnsrEditCnfmtn.txtBxOTPFT.text = "";
    frmIBFTrnsrEditCnfmtnPreShow();
};

function ehFrmIBFTrnsrEditCnfmtn_btnB4T_onClick(eventobject) {
    if (varAvailBal == false) {
        frmIBFTrnsrEditCnfmtn.lblFTAvailBal.setVisibility(true);
        frmIBFTrnsrEditCnfmtn.lblFTAvailBalVal.setVisibility(true);
        varAvailBal = true;
        //frmIBFTrnsrEditCnfmtn.lblFTHide.text = "Hide"
        frmIBFTrnsrEditCnfmtn.lblFTHide.text = kony.i18n.getLocalizedString("Hide");
    } else if (varAvailBal == true) {
        frmIBFTrnsrEditCnfmtn.lblFTAvailBal.setVisibility(false);
        frmIBFTrnsrEditCnfmtn.lblFTAvailBalVal.setVisibility(false);
        varAvailBal = false;
        //frmIBFTrnsrEditCnfmtn.lblFTHide.text = "Show"
        frmIBFTrnsrEditCnfmtn.lblFTHide.text = kony.i18n.getLocalizedString("keyUnhide");
    }
};

function ehFrmIBFTrnsrEditCnfmtn_button447476809947_onClick(eventobject) {
    frmIBFTrnsrView.hbxFTEditAmnt.setVisibility(true);
    frmIBFTrnsrView.hbxFTVeiwAmnt.setVisibility(false);
    frmIBFTrnsrView.lblFTEditHdr.setVisibility(true);
    frmIBFTrnsrView.btnFTEdit.setVisibility(true);
    frmIBFTrnsrView.hbxFTViewHdr.setVisibility(false);
    frmIBFTrnsrView.btnReturnView.setVisibility(false)
    frmIBFTrnsrView.hbxEditBtns.setVisibility(true);
    //frmIBFTrnsrView.lblToAccntName.text = gblAccntName_ORFT;
    frmIBFTrnsrView.hbxEditFT.setVisibility(false);
    frmIBFTrnsrView.show();
    frmIBFTrnsrEditCnfmtn.btnOTPReq.skin = btnIBREQotpFocus;
    frmIBFTrnsrEditCnfmtn.btnOTPReq.setEnabled(true);
    frmIBFTrnsrView.lblFeeVal.text = frmIBFTrnsrEditCnfmtn.lblFeeVal.text;
    frmIBFTrnsrView.btnFTEditFlow.setVisibility(false);
    frmIBFTrnsrView.btnFTDel.setVisibility(false);
};

function ehFrmIBFTrnsrEditCnfmtn_button445417506939912_onClick(eventobject) {
    frmIBFTrnsrView.lblViewStartOnDateVal.text = formatDateFT(gblFTViewStartOnDate);
    frmIBFTrnsrView.lblRepeatAsVal.text = gblFTViewRepeatAsVal
    frmIBFTrnsrView.lblRepeatAsValView.text = gblFTViewRepeatAsVal;
    var tmpEndDate = gblFTViewEndDate;
    if (gblFTViewEndDate == "-") {
        frmIBFTrnsrView.lblEndOnDateVal.text = "-";
    } else {
        if (tmpEndDate.indexOf("-", 0) != -1) {
            tmpEndDate = formatDateFT(gblFTViewEndDate);
        } else frmIBFTrnsrView.lblEndOnDateVal.text = tmpEndDate;
    }
    frmIBFTrnsrView.lblExcuteVal.text = gblFTViewExcuteVal
    frmIBFTrnsrView.lblRemainingVal.text = gblFTViewExcuteremaing + " )"
    gblIsEditAftr = gblIsEditAftrFrmService;
    gblIsEditOnDate = gblIsEditOnDateFrmService;
    gblScheduleRepeatFT = gblFTViewRepeatAsVal;
    gblScheduleEndFT = gblFTViewEndVal;
    gblEditFTSchdule = false;
    frmIBFTrnsrView.lblFeeVal.text = gblFTFeeView;
    if (gblViewRepeatAsLS == "Daily") {
        frmIBFTrnsrView.lblRepeatAsValView.text = kony.i18n.getLocalizedString("keyDaily");
    } else if (gblViewRepeatAsLS == "Weekly") {
        frmIBFTrnsrView.lblRepeatAsValView.text = kony.i18n.getLocalizedString("keyWeekly");
    } else if (gblViewRepeatAsLS == "Monthly") {
        frmIBFTrnsrView.lblRepeatAsValView.text = kony.i18n.getLocalizedString("keyMonthly");
    } else if (gblViewRepeatAsLS == "Yearly") {
        frmIBFTrnsrView.lblRepeatAsValView.text = kony.i18n.getLocalizedString("keyYearly");
    } else frmIBFTrnsrView.lblRepeatAsValView.text = kony.i18n.getLocalizedString("keyOnce");
    gblRepeatAsforLS = gblViewRepeatAsLS;
    frmIBFTrnsrView.lblToAccntNickName.text = gblToAccntNick;
    frmIBFTrnsrView.hbxFTEditAmnt.setVisibility(false);
    frmIBFTrnsrView.hbxFTVeiwAmnt.setVisibility(true);
    frmIBFTrnsrView.lblFTEditHdr.setVisibility(false);
    frmIBFTrnsrView.btnFTEdit.setVisibility(false);
    frmIBFTrnsrView.hbxFTViewHdr.setVisibility(true);
    //frmIBFTrnsrView.hbxbtnReturn.setVisibility(true);
    frmIBFTrnsrView.btnReturnView.setVisibility(true)
    frmIBFTrnsrView.hbxEditBtns.setVisibility(false);
    frmIBFTrnsrView.hbxFTAfterEditAvilBal.setVisibility(false);
    frmIBFTrnsrView.hbxEditFT.setVisibility(false);
    //New
    frmIBFTrnsrView.hbxTrnsfrDetails2.setVisibility(false);
    frmIBFTrnsrView.hbxTrnsfrDetails2ForView.setVisibility(true);
    frmIBFTrnsrView.btnFTEditFlow.setVisibility(true);
    frmIBFTrnsrView.btnFTDel.setVisibility(true);
    frmIBFTrnsrView.show();
};

function ehFrmIBFTrnsrEditCnfmtn_btnOTPReq_onClick(eventobject) {
    if (frmIBFTrnsrEditCnfmtn.btnOTPReq.text == kony.i18n.getLocalizedString("keyRequest")) {
        reqOTPFT();
        gblTokenSwitchFlag = false;
    } else if (frmIBFTrnsrEditCnfmtn.btnOTPReq.text == kony.i18n.getLocalizedString("keySwitchToSMS")) {
        frmIBFTrnsrEditCnfmtn.lblOTP.text = kony.i18n.getLocalizedString("keyOTP");
        frmIBFTrnsrEditCnfmtn.lblMsgRequset.text = kony.i18n.getLocalizedString("keyotpmsgreq");
        frmIBFTrnsrEditCnfmtn.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
        gblSwitchToken = true;
        gblTokenSwitchFlag = false;
        reqOTPFT();
    }
};

function ehFrmIBFTrnsrEditCnfmtn_btnReturnUpdated_onClick(eventobject) {
    frmIBFTrnsrView.hbxFTEditAmnt.setVisibility(false);
    frmIBFTrnsrView.hbxFTVeiwAmnt.setVisibility(true);
    frmIBFTrnsrView.lblFTEditHdr.setVisibility(false);
    frmIBFTrnsrView.btnFTEdit.setVisibility(false);
    frmIBFTrnsrView.hbxFTViewHdr.setVisibility(true);
    frmIBFTrnsrView.btnReturnView.setVisibility(true)
    frmIBFTrnsrView.hbxEditBtns.setVisibility(false);
    //frmIBFTrnsrView.txtFTEditAmountval.setFocus(false);
    frmIBFTrnsrView.hbxFTAfterEditAvilBal.setVisibility(false);
    if (gblActivitiesNvgn == "F") {
        invokeFutureInstructionService();
        /*
 frmIBMyActivities.show();
 if(frmIBMyActivities.btncalender.focusSkin == btnIBTabBlue)
 showCalendar(gsSelectedDate,frmIBMyActivities,1);
 */
        IBMyActivitiesReloadAndShowCalendar();
    } else if (gblActivitiesNvgn == "C") {
        /*
 frmIBMyActivities.show();
 if(frmIBMyActivities.btncalender.focusSkin == btnIBTabBlue)
 showCalendar(gsSelectedDate,frmIBMyActivities,1);*/
        IBMyActivitiesReloadAndShowCalendar();
    }
    /* 
frmIBMyActivities.show();
	
 */
};

function ehFrmIBTransferNowConfirmation_btnTranPreConfirm_onClick(eventobject) {
    onClickPreConfirmTrans();
};

function ehFrmIBTransferNowConfirmation_txtBxOTP_onKeyUp(eventobject, changedtext) {
    dontAllowNonNeumaric(frmIBTransferNowConfirmation.txtBxOTP);
};

function ehfrmIBTransferNowCompletion_lblHide_onClick(eventobject) {
    if (frmIBTransferNowCompletion.lblBal.isVisible) {
        frmIBTransferNowCompletion.lblBal.setVisibility(false);
        frmIBTransferNowCompletion.lblHide.text = kony.i18n.getLocalizedString("keyUnhide");
    } else {
        frmIBTransferNowCompletion.lblBal.setVisibility(true);
        frmIBTransferNowCompletion.lblHide.text = kony.i18n.getLocalizedString("keyHideIB");
    }
};

function ehFrmIBTransferNowCompletion_fbBtn_onClick(eventobject) {
    requestfromform = "frmIBTransferNowCompletion";
    gblPOWcustNME = gblCustomerName;
    if (gblPaynow) {
        gblPOWtransXN = kony.i18n.getLocalizedString("Transfer");
    } else {
        gblPOWtransXN = kony.i18n.getLocalizedString("Transfer_ScheduledTransfer");
    }
    gblPOWchannel = "IB";
    postOnWall();
};