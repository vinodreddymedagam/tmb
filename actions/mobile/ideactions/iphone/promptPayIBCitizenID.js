function onTextChangeToCitizenIDIB(txt) {
    //Max length of Citizen ID
    var maxlength = 17;
    frmIBTranferLP.txtXferMobileNumber.maxTextLength = maxlength;
    if (txt == null) return false;
    var numChars = txt.length;
    var temp = "";
    var i, txtLen = numChars;
    var currLen = numChars;
    if (gblPrevLen < currLen) {
        for (i = 0; i < numChars; ++i) {
            if (txt[i] != '-') {
                temp = temp + txt[i];
            } else {
                txtLen--;
            }
        }
        var iphenText = "";
        for (i = 0; i < txtLen; i++) {
            iphenText += temp[i];
            if (i == 0 || i == 4 || i == 9 || i == 11) {
                iphenText += '-';
            }
        }
        frmIBTranferLP.txtXferMobileNumber.text = iphenText;
    }
    gblPrevLen = currLen;
    var citizenID = frmIBTranferLP.txtXferMobileNumber.text;
    if (isNotBlank(citizenID)) {
        citizenID = removeHyphenIB(citizenID);
        if (!isNotBlank(frmIBTranferLP.lblMobileNumberTemp.text)) {
            frmIBTranferLP.lblMobileNumberTemp.text = "";
        }
        if (citizenID.length >= 13) {
            frmIBTransferNowConfirmation.lblXferToName.text = "";
            if (!kony.string.equalsIgnoreCase(frmIBTranferLP.lblMobileNumberTemp.text, frmIBTranferLP.txtXferMobileNumber.text)) {
                isOwnAccountP2P = false;
                //validate citizen number
                if (!checkCitizenID(citizenID)) {
                    showAlert(kony.i18n.getLocalizedString("MIB_P2PkeyCIisnotvalid"), kony.i18n.getLocalizedString("info"));
                    frmIBTranferLP.txtXferMobileNumber.setFocus(true);
                    return false;
                } else {
                    var enteredAmt = frmIBTranferLP.txbXferAmountRcvd.text;
                    if (isNotBlank(enteredAmt) && parseFloat(enteredAmt, 10) > 0) {
                        checkCallCheckOnUsPromptPayinqServiceIB();
                    }
                }
            } else {
                displayPrevValueForSameEnteredMobile();
            }
        }
    }
}

function onTextChangePromptPayValueIB() {
    var P2PData = removeHyphenIB(frmIBTranferLP.txtXferMobileNumber.text);
    if (P2PData.length > 0) {
        reCheckSelTransferMode(P2PData);
        if (gblSelTransferMode == 2) {
            onTextChangeToMobileNoP2PIB(frmIBTranferLP.txtXferMobileNumber.text);
            frmIBTranferLP.hbxNotifyRecipentP2P.setVisibility(true);
            frmIBTranferLP.hbxNotifyRecipent.setVisibility(false);
        } else if (gblSelTransferMode == 3) {
            onTextChangeToCitizenIDIB(frmIBTranferLP.txtXferMobileNumber.text);
            frmIBTranferLP.hbxNotifyRecipent.setVisibility(true);
            frmIBTranferLP.hbxNotifyRecipentP2P.setVisibility(false);
        }
        frmIBTranferLP.lineOne.setVisibility(true);
    }
}

function reCheckSelTransferMode(P2PData) {
    if (P2PData.length > 0) {
        var firstDigit = P2PData.substring(0, 1);
        if (firstDigit == "0") {
            gblSelTransferMode = 2;
        } else {
            gblSelTransferMode = 3;
        }
    }
}