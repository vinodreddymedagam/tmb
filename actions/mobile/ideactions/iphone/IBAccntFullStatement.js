 var totalGridData = [];
 var responseData = "";
 var bindataIB = "";
 var binlengthIB = "";
 var creditypeIB = "";
 var wcodeIB = "";
 var remainingcreditIB = "";
 var startDateIB = "";
 var endDateIB = "";
 var nopagesStmt = 0;
 var currentpageStmt = 1;
 var recordcountStmt = 0;
 var appendDATADG;
 var pdfStmntDate;
 var noOfStmtServiceCalls = 0;
 // In AccountStatement form 4 datagrid widgets are being used to design the UI as per the mock up screens
 /**
  * Sort Data based on the sorting criteria in the column in datagrid
  */
 function sortDateAscendCheque(array, columnid, key, columnidCheque, keyCheque) {
     return array.sort(function(a, b) {
         var x = sortDate(a[columnid][key]["text"]);
         var y = sortDate(b[columnid][key]["text"]);
         var d = a[columnidCheque][keyCheque]["text"];
         var e = b[columnidCheque][keyCheque]["text"];
         return ((x < y) ? -1 : ((x > y) ? 1 : ((d > e)) ? -1 : 1));
     });
 }

 function sortDateDescendCheque(array, columnid, key, columnidCheque, keyCheque) {
     return array.sort(function(a, b) {
         var x = sortDate(a[columnid][key]["text"]);
         var y = sortDate(b[columnid][key]["text"]);
         var d = a[columnidCheque][keyCheque]["text"];
         var e = b[columnidCheque][keyCheque]["text"];
         return ((x > y) ? -1 : ((x < y) ? 1 : ((d < e)) ? -1 : 1));
     });
 }

 function sortDate(str) {
     var mdy = str.split('/')
     return new Date(mdy[2], mdy[1] - 1, mdy[0]);
 }

 function sortDateDescend(array, columnid, key, seqid) {
     return array.sort(function(a, b) {
         var x = sortDate(a[columnid][key]["text"]);
         var y = sortDate(b[columnid][key]["text"]);
         var d = a[seqid];
         var e = b[seqid];
         return ((x > y) ? -1 : ((x < y) ? 1 : ((d < e)) ? -1 : 1));
     });
 }
 /**
  * Sort Data based on the sorting criteria in the column in datagrid
  */
 function sortDateAscend(array, columnid, key, seqid) {
     return array.sort(function(a, b) {
         var x = sortDate(a[columnid][key]["text"]);
         var y = sortDate(b[columnid][key]["text"]);
         var d = a[seqid];
         var e = b[seqid];
         return ((x < y) ? -1 : ((x > y) ? 1 : ((d > e)) ? -1 : 1));
     });
 }
 /**
  * Method to invoke Full statement for accounts
  */
 function viewFullStatementIB() {
     showLoadingScreenPopup();
     gblStmntSessionFlag = "2";
     noOfStmtServiceCalls = 0;
     totalGridData = [];
     disablePagenumbers();
     endDateIB = getTodaysDateStmt();
     startDateIB = dateFormatChange(new Date(new Date().getFullYear(), new Date().getMonth(), 1));
     frmIBAccntFullStatement.show();
 }

 function viewAccntFullStatementIB() {
     frmIBAccntFullStatement.lblname.text = frmIBAccntSummary.lblAccountNameHeader.text;
     frmIBAccntFullStatement.imgacnt.src = frmIBAccntSummary.imgAccountDetailsPic.src;
     if (gaccType == "CCA") {
         frmIBAccntFullStatement.lblamt.text = kony.i18n.getLocalizedString("keyCreditlblIB") + " " + frmIBAccntSummary.lblAccountBalanceHeader.text;
         frmIBAccntFullStatement.lblbill.text = kony.i18n.getLocalizedString("keySelectStmntCycle");
     } else {
         frmIBAccntFullStatement.lblamt.text = frmIBAccntSummary.lblAccountBalanceHeader.text;
     }
     getMonthCycleIB();
     currentpageStmt = 1;
     setSortBtnSkin();
     disablePagenumbers();
     appendDATADG = "NO";
     frmIBAccntFullStatement.dgcredit.removeAll();
     frmIBAccntFullStatement.dgfullstmt.removeAll();
     frmIBAccntFullStatement.dgloan.removeAll();
     frmIBAccntFullStatement.dgdeposit.removeAll();
     if (gaccType == "SDA" || gaccType == "DDA") {
         frmIBAccntFullStatement.hbxstmt.setVisibility(true);
         frmIBAccntFullStatement.dgfullstmt.setVisibility(true);
         frmIBAccntFullStatement.dgloan.setVisibility(false);
         frmIBAccntFullStatement.dgdeposit.setVisibility(false);
         frmIBAccntFullStatement.hbxtd.setVisibility(false);
         frmIBAccntFullStatement.hbxstmtcredit.setVisibility(false);
         frmIBAccntFullStatement.hbxslider.setVisibility(true);
         frmIBAccntFullStatement.hbxmonthcycle.setVisibility(true);
         frmIBAccntFullStatement.lblselect.text = kony.i18n.getLocalizedString("MyActivitiesIB_Duration");
         var headertext = frmIBAccntFullStatement.dgfullstmt.columnHeadersConfig;
         headertext[0].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("keyDate");
         headertext[1].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("keyIBDescription");
         headertext[2].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("MyActivitiesIB_Channel");
         headertext[3].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("MyActivitiesIB_Amount");
         headertext[4].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("Balance");
         startCASAstatementserviceIB(startDateIB, endDateIB);
     } else if (gaccType == "LOC") {
         var headertext = frmIBAccntFullStatement.dgloan.columnHeadersConfig;
         frmIBAccntFullStatement.hbxstmt.setVisibility(true);
         frmIBAccntFullStatement.dgfullstmt.setVisibility(false);
         frmIBAccntFullStatement.dgloan.setVisibility(true);
         frmIBAccntFullStatement.dgdeposit.setVisibility(false);
         frmIBAccntFullStatement.hbxtd.setVisibility(false);
         frmIBAccntFullStatement.hbxstmtcredit.setVisibility(false);
         frmIBAccntFullStatement.hbxslider.setVisibility(true);
         frmIBAccntFullStatement.hbxmonthcycle.setVisibility(true);
         frmIBAccntFullStatement.lblselect.text = kony.i18n.getLocalizedString("MyActivitiesIB_Duration");
         headertext[0].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("keyDate");
         headertext[1].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("keyHeaderPrinciplePaid");
         headertext[2].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("keyHeaderInterestPaid");
         headertext[3].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("keyMBPaymentAmount");
         headertext[4].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("keyHeaderOutstandingBalance");
         startLoanStatementserviceIB(startDateIB, endDateIB);
     } else if (gaccType == "CCA") {
         var headertext = frmIBAccntFullStatement.dgcredit.columnHeadersConfig;
         frmIBAccntFullStatement.hbxstmt.setVisibility(false);
         frmIBAccntFullStatement.dgfullstmt.setVisibility(false);
         frmIBAccntFullStatement.dgloan.setVisibility(false);
         frmIBAccntFullStatement.dgdeposit.setVisibility(false);
         frmIBAccntFullStatement.hbxstmtcredit.setVisibility(true);
         frmIBAccntFullStatement.hbxunbilled.setVisibility(true);
         frmIBAccntFullStatement.hbxbilled.setVisibility(false);
         frmIBAccntFullStatement.btnunbill.skin = "btntabrightfoc";
         frmIBAccntFullStatement.btnunbill.focusSkin = "btntabrightfoc";
         frmIBAccntFullStatement.btnbill.skin = "btntabright";
         frmIBAccntFullStatement.btnbill.focusSkin = "btntabright";
         headertext[0].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("keyIBTransactionDate");
         headertext[1].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("keyIBPostedDate");
         headertext[2].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("keyIBDescription");
         headertext[3].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("MyActivitiesIB_Amount");
         startCCstatementserviceIB("unbilled1", "0");
     } else if (gaccType == "CDA") {
         var headertext = frmIBAccntFullStatement.dgfullstmt.columnHeadersConfig;
         frmIBAccntFullStatement.hbxstmt.setVisibility(true);
         frmIBAccntFullStatement.dgfullstmt.setVisibility(true);
         frmIBAccntFullStatement.dgloan.setVisibility(false);
         frmIBAccntFullStatement.dgdeposit.setVisibility(false);
         frmIBAccntFullStatement.hbxtd.setVisibility(true);
         frmIBAccntFullStatement.hbxstmtcredit.setVisibility(false);
         frmIBAccntFullStatement.hbxslider.setVisibility(true);
         frmIBAccntFullStatement.hbxmonthcycle.setVisibility(true);
         frmIBAccntFullStatement.lblselect.text = kony.i18n.getLocalizedString("MyActivitiesIB_Duration");
         frmIBAccntFullStatement.btnstmt.skin = "btnIBTabType2Focus";
         frmIBAccntFullStatement.btnstmt.focusSkin = "btnIBTabType2Focus";
         frmIBAccntFullStatement.btndeposit.skin = "btnIBTabType2Normal";
         frmIBAccntFullStatement.btndeposit.focusSkin = "btnIBTabType2Normal";
         headertext[0].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("keyDate");
         headertext[1].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("keyIBDescription");
         headertext[2].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("MyActivitiesIB_Channel");
         headertext[3].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("MyActivitiesIB_Amount");
         headertext[4].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("Balance");
         startTDstatementserviceIB(startDateIB, endDateIB);
     }
 }
 /**
  * Method to invoke service for CCstatement
  */
 function startCASAstatementserviceIB(startdateIB, enddateIB) {
     totalGridData = [];
     startDateIB = startdateIB;
     endDateIB = enddateIB;
     noOfStmtServiceCalls++;
     var spName;
     if (gaccType == "SDA") spName = "com.fnis.xes.ST";
     if (gaccType == "DDA") spName = "com.fnis.xes.IM";
     var CASAinputParam = {};
     CASAinputParam["noOfServiceCall"] = noOfStmtServiceCalls;
     CASAinputParam["serviceName"] = "AccntTransStmt";
     CASAinputParam["sessionFlag"] = gblStmntSessionFlag;
     CASAinputParam["AcctIdentValue"] = gblAccountTable["custAcctRec"][gblIndex]["accId"]; //"0011001237";//
     CASAinputParam["FIIdent"] = gblAccountTable["custAcctRec"][gblIndex]["fiident"]; //"0011000100010000";//
     CASAinputParam["BinData"] = bindataIB;
     CASAinputParam["BinLength"] = binlengthIB;
     CASAinputParam["StartDt"] = startDateIB;
     CASAinputParam["EndDt"] = endDateIB;
     CASAinputParam["channelID"] = "IB";
     CASAinputParam["spName"] = spName;
     CASAinputParam["accName"] = frmIBAccntFullStatement.lblname.text;
     CASAinputParam["accType"] = gaccType;
     showLoadingScreen(); //showLoadingScreenPopup
     invokeServiceSecureAsync("getAccntTransStmt", CASAinputParam, startCASAstatementserviceIBCallback);
 }
 /**
  * Callback method for startCCstatementserviceCallback()
  */
 function startCASAstatementserviceIBCallback(status, callBackResponse) {
     if (status == 400) {
         if (callBackResponse["opstatus"] == 0) {
             gblStmntSessionFlag = callBackResponse["sesFlag"];
             dismissLoadingScreen(); //dismissLoadingScreenPopup();
             bindataIB = callBackResponse["BinData"];
             binlengthIB = callBackResponse["BinLength"];
             recordcountStmt = callBackResponse["IBRecsCount"];
             var tempData = [];
             var resp = callBackResponse;
             if (callBackResponse["StatusCode"] == 0) {
                 if (callBackResponse["StmtRecord"].length == 0) {
                     frmIBAccntFullStatement.dgfullstmt.removeAll();
                     disablePagenumbers();
                     alert(kony.i18n.getLocalizedString("NoRecsFound"));
                     return;
                 }
                 for (var i = 0; i < callBackResponse["StmtRecord"].length; i++) {
                     var chqdata = "";
                     if (callBackResponse["StmtRecord"][i]["ChkNum"] != undefined) chqdata = callBackResponse["StmtRecord"][i]["ChkNum"];
                     var segTable = {
                         "seqid": i,
                         "column6": {
                             "lbltransid": {
                                 "text": commaFormattedStmtIB(callBackResponse["StmtRecord"][i]["Balance"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht")
                             }
                         },
                         "column1": {
                             "lbltransid": {
                                 "text": formatDateStmtIB(callBackResponse["StmtRecord"][i]["PostedDt"])
                             }
                         },
                         "column2": {
                             "lbltransid": {
                                 "text": callBackResponse["StmtRecord"][i]["TrnDesc1"] + chqdata
                             }
                         },
                         "column5": {
                             "lbltransid": {
                                 "text": checkTranstypeIB(commaFormattedStmtIB(callBackResponse["StmtRecord"][i]["Amount"]), callBackResponse["StmtRecord"][i]["TrnType"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht")
                             }
                         },
                         "column4": {
                             "lbltransid": {
                                 "text": fieldemptyMB(callBackResponse["StmtRecord"][i]["TrnChannel"])
                             }
                         }
                     }
                     tempData.push(segTable);
                 }
                 appendData(tempData);
             } else {
                 alert(callBackResponse["errMsg"]);
                 return false;
             }
         } else {
             alert(callBackResponse["errMsg"]);
             return false;
         }
     } else {
         if (status == 300) {
             dismissLoadingScreen(); //dismissLoadingScreenPopup();
             alert("" + kony.i18n.getLocalizedString("ECGenericError"));
         }
     }
 }
 /**
  * Method to invoke service for CCstatement
  */
 function startCCstatementserviceIB(cctypeIB, wCodeIB) {
     totalGridData = [];
     wcodeIB = wCodeIB;
     var transcodeIB;
     if (cctypeIB == "unbilled1") {
         transcodeIB = "8190";
         creditypeIB = "unbilled1";
     } else if (cctypeIB == "unbilled2") {
         transcodeIB = "8292";
         creditypeIB = "unbilled2";
     } else if (cctypeIB == "billed") {
         transcodeIB = "8290";
         creditypeIB = "billed";
     }
     var todayDate = getTodaysDateStmt();
     var CCinputParam = {};
     CCinputParam["sessionFlag"] = gblStmntSessionFlag; //gblStmntSessionFlag;
     CCinputParam["CardId"] = gblAccountTable["custAcctRec"][gblIndex]["accId"]; //"000010114966941100070001";//
     CCinputParam["WaiverCode"] = wcodeIB; //"7";//
     CCinputParam["TranCode"] = transcodeIB; //"8290";//
     CCinputParam["PostedDt"] = todayDate; //"2013-03-15";//
     CCinputParam["BinData"] = bindataIB;
     CCinputParam["BinLength"] = binlengthIB;
     CCinputParam["channelID"] = "IB";
     CCinputParam["flagSPA"] = false;
     CCinputParam["accName"] = frmIBAccntFullStatement.lblname.text;
     CCinputParam["accType"] = gaccType;
     showLoadingScreenPopup();
     if (transcodeIB != "8190") {
         noOfStmtServiceCalls++;
         CCinputParam["noOfServiceCall"] = noOfStmtServiceCalls;
         CCinputParam["serviceName"] = "CCstatement";
         invokeServiceSecureAsync("getAccntTransStmt", CCinputParam, startCCstatementserviceIBCallback);
     } else {
         invokeServiceSecureAsync("CCstatement", CCinputParam, startCCstatementserviceIBCallback);
     }
 }
 /**
  * Callback method for startCCstatementserviceIBCallback()
  */
 function startCCstatementserviceIBCallback(status, callBackResponse) {
     var accId = gblAccountTable["custAcctRec"][gblIndex]["accId"];
     if (status == 400) {
         if (callBackResponse["opstatus"] == 0) {
             gblStmntSessionFlag = callBackResponse["sesFlag"];
             dismissLoadingScreenPopup();
             bindataIB = callBackResponse["BinData"];
             binlengthIB = callBackResponse["BinLength"];
             recordcountStmt = callBackResponse["IBRecsCount"];
             if (callBackResponse["StatusCode"] == 0) {
                 if (callBackResponse["StmtRecord"].length == 0) {
                     frmIBAccntFullStatement.dgcredit.removeAll();
                     frmIBAccntFullStatement.hbxunbilled.setVisibility(false);
                     frmIBAccntFullStatement.hbxbilled.setVisibility(false);
                     disablePagenumbers();
                     alert(kony.i18n.getLocalizedString("NoRecsFound"));
                     return;
                 }
                 if (creditypeIB == "unbilled1") {
                     remainingcreditIB = callBackResponse["StmtRecord"][0]["AvailCrLimit"];
                     gblStmntSessionFlag = "2";
                     startCCstatementserviceIB("unbilled2", "0");
                     return;
                 } else if (creditypeIB == "unbilled2") {
                     frmIBAccntFullStatement.hbxunbilled.setVisibility(true);
                     frmIBAccntFullStatement.lbllimit.text = kony.i18n.getLocalizedString("keyMBCreditlimit");
                     frmIBAccntFullStatement.lbloutbal.text = kony.i18n.getLocalizedString("keyIBOutstanding");
                     frmIBAccntFullStatement.lblcycle.text = kony.i18n.getLocalizedString("keyIBSpendcycle");
                     frmIBAccntFullStatement.lblpoints.text = kony.i18n.getLocalizedString("keyIBAvailable");
                     frmIBAccntFullStatement.lblexpire.text = kony.i18n.getLocalizedString("keyIBexpiring");
                     frmIBAccntFullStatement.lblexp.text = kony.i18n.getLocalizedString("keyIBPointexpiry");
                     //	frmIBAccntFullStatement.lbllimitval.text=commaFormattedStmt(callBackResponse["StmtRecord"][0]["AvailCrLimit"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                     frmIBAccntFullStatement.lbllimitval.text = commaFormattedStmt(remainingcreditIB) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                     frmIBAccntFullStatement.lbloutbalval.text = commaFormattedStmt(callBackResponse["StmtRecord"][0]["AvailCrLimit"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                     frmIBAccntFullStatement.lblcycleval.text = commaFormattedStmt(callBackResponse["StmtRecord"][0]["LedgerBal"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                     //frmIBAccntFullStatement.lblpointsval.text = commaFormattedPoints(callBackResponse["StmtRecord"][0]["BonusPointAvail"]) + " " + kony.i18n.getLocalizedString("keyIBPoints");
                     //frmIBAccntFullStatement.lblexpireval.text = commaFormattedPoints(callBackResponse["StmtRecord"][0]["BonusPointUsed"]) + " " + kony.i18n.getLocalizedString("keyIBPoints");
                     if (gblCC_RemainingPoint == "" || gblCC_RemainingPoint == null) gblCC_RemainingPoint = "0";
                     if (gblCC_PointExpRemaining == "" || gblCC_PointExpRemaining == null) gblCC_PointExpRemaining = "0";
                     frmIBAccntFullStatement.lblpointsval.text = commaFormattedPoints(gblCC_RemainingPoint) + " " + kony.i18n.getLocalizedString("keyIBPoints");
                     frmIBAccntFullStatement.lblexpireval.text = commaFormattedPoints(gblCC_PointExpRemaining) + " " + kony.i18n.getLocalizedString("keyIBPoints");
                     frmIBAccntFullStatement.lblexpval.text = formatDateStmt(callBackResponse["StmtRecord"][0]["StmtDt"]);
                     pdfStmntDate = formatDateStmt(callBackResponse["StmtRecord"][0]["StmtDt"]);
                 } else if (creditypeIB == "billed") {
                     frmIBAccntFullStatement.hbxbilled.setVisibility(true);
                     frmIBAccntFullStatement.lblfulpay.text = kony.i18n.getLocalizedString("keyIBFullPay");
                     frmIBAccntFullStatement.lblminpay.text = kony.i18n.getLocalizedString("keyIBMinimumPay");
                     frmIBAccntFullStatement.lbldue.text = kony.i18n.getLocalizedString("keyIBDueDate");
                     frmIBAccntFullStatement.lblpntsearned.text = kony.i18n.getLocalizedString("keyIBEarned");
                     frmIBAccntFullStatement.lbltotalpnts.text = kony.i18n.getLocalizedString("keyIBAvailable");
                     frmIBAccntFullStatement.lblfulpayval.text = commaFormattedStmt(callBackResponse["StmtRecord"][0]["LedgerBal"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                     frmIBAccntFullStatement.lblminpayval.text = commaFormattedStmt(callBackResponse["StmtRecord"][0]["LastPmtAmt"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                     frmIBAccntFullStatement.lbldueval.text = formatDateStmt(callBackResponse["StmtRecord"][0]["DueDt"]);
                     frmIBAccntFullStatement.lblpntsearnedval.text = commaFormattedPoints(callBackResponse["StmtRecord"][0]["BonusPoint"]) + " " + kony.i18n.getLocalizedString("keyIBPoints");
                     frmIBAccntFullStatement.lbltotalpntsval.text = commaFormattedPoints(callBackResponse["StmtRecord"][0]["BonusPointAvail"]) + " " + kony.i18n.getLocalizedString("keyIBPoints");
                     pdfStmntDate = formatDateStmt(callBackResponse["StmtRecord"][0]["StmtDt"]);
                 }
                 var tempData = [];
                 for (var i = 0; i < callBackResponse["StmtRecord"].length; i++) {
                     var segTable = {
                         "seqid": i,
                         "column1": {
                             "lbltransid": {
                                 "text": formatDateStmt(callBackResponse["StmtRecord"][i]["TranDt"])
                             }
                         },
                         "column2": {
                             "lbltransid": {
                                 "text": formatDateStmt(callBackResponse["StmtRecord"][i]["PostedDt"])
                             }
                         },
                         "column5": {
                             "lbltransid": {
                                 "text": commaFormattedStmt(callBackResponse["StmtRecord"][i]["TranAmt"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht")
                             }
                         },
                         "column4": {
                             "lbltransid": {
                                 "text": fieldemptyMB(callBackResponse["StmtRecord"][i]["StmtDesc"])
                             }
                         }
                     }
                     tempData.push(segTable);
                 }
                 appendData(tempData);
             } else {
                 alert(callBackResponse["errMsg"]);
                 return false;
             }
         } else {
             dismissLoadingScreenPopup();
             if (callBackResponse["errMsg"] != undefined) {
                 alert(" " + callBackResponse["errMsg"]);
             } else {
                 alert(kony.i18n.getLocalizedString("NoRecsFound"));
             }
             return false;
         }
     } else {
         if (status == 300) {
             dismissLoadingScreenPopup(); //dismissLoadingScreen(); 
             alert("" + kony.i18n.getLocalizedString("ECGenericError"));
         }
     }
 }
 /**
  * Method to invoke service for LoanStatement
  */
 function startLoanStatementserviceIB(startDateIB, endDateIB) {
     totalGridData = [];
     // 	gblLoanPrdID=gblAccountTable["custAcctRec"][0]["productID"];
     startDateIB = startDateIB;
     endDateIB = endDateIB;
     var startLoaninputParam = {};
     startLoaninputParam["sessionFlag"] = gblStmntSessionFlag;
     startLoaninputParam["AcctIdentValue"] = gblAccountTable["custAcctRec"][gblIndex]["accId"]; //"00015051725555";//
     startLoaninputParam["FIIdent"] = gblAccountTable["custAcctRec"][gblIndex]["fiident"]; //"0011000100010000";//
     startLoaninputParam["StartDt"] = startDateIB;
     startLoaninputParam["EndDt"] = endDateIB;
     startLoaninputParam["BinData"] = bindataIB;
     startLoaninputParam["BinLength"] = binlengthIB;
     startLoaninputParam["channelID"] = "IB";
     startLoaninputParam["accName"] = frmIBAccntFullStatement.lblname.text;
     startLoaninputParam["accType"] = gaccType;
     showLoadingScreen(); //showLoadingScreenPopup();
     invokeServiceSecureAsync("LoanStatement", startLoaninputParam, startLoanStatementserviceIBCallback);
 }
 /**
  * Callback method for startLoanStatementservice()
  */
 function startLoanStatementserviceIBCallback(status, callBackResponse) {
     if (status == 400) {
         if (callBackResponse["opstatus"] == 0) {
             gblStmntSessionFlag = callBackResponse["sesFlag"];
             dismissLoadingScreen();
             bindataIB = callBackResponse["BinData"];
             binlengthIB = callBackResponse["BinLength"];
             recordcountStmt = callBackResponse["IBRecsCount"];
             if (callBackResponse["StatusCode"] == 0) {
                 if (callBackResponse["StmtRecord"].length == 0) {
                     frmIBAccntFullStatement.dgloan.removeAll();
                     disablePagenumbers();
                     alert(kony.i18n.getLocalizedString("NoRecsFound"));
                     return;
                 }
                 var tempData = [];
                 for (var i = 0; i < callBackResponse["StmtRecord"].length; i++) {
                     var segTable = {
                         "seqid": i,
                         "column6": {
                             "lbltransid": {
                                 "text": commaFormattedStmt(callBackResponse["StmtRecord"][i]["Outbal"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht")
                             }
                         },
                         "column1": {
                             "lbltransid": {
                                 "text": formatDateStmt(callBackResponse["StmtRecord"][i]["TranDt"])
                             }
                         },
                         "column2": {
                             "lbltransid": {
                                 "text": commaFormattedStmt(callBackResponse["StmtRecord"][i]["Ppaid"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht")
                             }
                         },
                         "column5": {
                             "lbltransid": {
                                 "text": commaFormattedStmt(callBackResponse["StmtRecord"][i]["PaymentAmt"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht")
                             }
                         },
                         "column4": {
                             "lbltransid": {
                                 "text": commaFormattedStmt(callBackResponse["StmtRecord"][i]["Ipaid"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht")
                             }
                         }
                     }
                     tempData.push(segTable);
                 }
                 appendData(tempData);
             } else {
                 alert(callBackResponse["errMsg"]);
                 return false;
             }
         } else {
             alert(callBackResponse["errMsg"]);
             return false;
         }
     } else {
         if (status == 300) {
             dismissLoadingScreen(); //dismissLoadingScreenPopup();
             alert("" + kony.i18n.getLocalizedString("ECGenericError"));
         }
     }
 }
 /**
  * Method to invoke service for CCstatement
  */
 function startTDstatementserviceIB(startDateIB, endDateIB) {
     totalGridData = [];
     startDateIB = startDateIB;
     endDateIB = endDateIB;
     var TDinputParam = {};
     TDinputParam["sessionFlag"] = gblStmntSessionFlag;
     TDinputParam["AcctIdentValue"] = (gblAccountTable["custAcctRec"][gblIndex]["accId"]).substring(4);
     TDinputParam["FIIdent"] = gblAccountTable["custAcctRec"][gblIndex]["fiident"]; //"0011000100010000";//
     TDinputParam["BinData"] = bindataIB;
     TDinputParam["BinLength"] = binlengthIB;
     TDinputParam["StartDt"] = startDateIB;
     TDinputParam["EndDt"] = endDateIB;
     TDinputParam["channelID"] = "IB";
     TDinputParam["accName"] = frmIBAccntFullStatement.lblname.text;
     TDinputParam["accType"] = gaccType;
     showLoadingScreen();
     invokeServiceSecureAsync("TDStatement", TDinputParam, startTDstatementserviceIBCallback);
 }
 /**
  * Callback method for startCCstatementserviceCallback()
  */
 function startTDstatementserviceIBCallback(status, callBackResponse) {
     if (status == 400) {
         if (callBackResponse["opstatus"] == 0) {
             gblStmntSessionFlag = callBackResponse["sesFlag"];
             //activityLogServiceCall("047", "", "01", "", gblAccountTable["custAcctRec"][gblIndex]["accId"],frmIBAccntFullStatement.lblname.text, gaccType, "", "", "");
             dismissLoadingScreen(); //dismissLoadingScreenPopup();
             bindataIB = callBackResponse["BinData"];
             binlengthIB = callBackResponse["BinLength"];
             recordcountStmt = callBackResponse["IBRecsCount"];
             if (callBackResponse["StatusCode"] == 0) {
                 if (callBackResponse["StmtRecord"].length == 0) {
                     frmIBAccntFullStatement.dgfullstmt.removeAll();
                     disablePagenumbers();
                     alert(kony.i18n.getLocalizedString("NoRecsFound"));
                     return;
                 }
                 var tempData = [];
                 for (var i = 0; i < callBackResponse["StmtRecord"].length; i++) {
                     var chqdata = "";
                     if (callBackResponse["StmtRecord"][i]["ChkNum"] != undefined) chqdata = callBackResponse["StmtRecord"][i]["ChkNum"];
                     var segTable = {
                         "seqid": i,
                         "column6": {
                             "lbltransid": {
                                 "text": commaFormattedStmt(callBackResponse["StmtRecord"][i]["CurrentBalAmt"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht")
                             }
                         },
                         "column1": {
                             "lbltransid": {
                                 "text": formatDateStmt(callBackResponse["StmtRecord"][i]["TranDt"])
                             }
                         },
                         "column2": {
                             "lbltransid": {
                                 "text": callBackResponse["StmtRecord"][i]["TrnDesc1"] + ", " + chqdata
                             }
                         },
                         "column5": {
                             "lbltransid": {
                                 "text": TDAmountFormat(checkDebitorCredit(callBackResponse["StmtRecord"][i]["DebitAmt"], callBackResponse["StmtRecord"][i]["CreditAmt"])) + " " + kony.i18n.getLocalizedString("currencyThaiBaht")
                             }
                         },
                         "column4": {
                             "lbltransid": {
                                 "text": fieldemptyMB(callBackResponse["StmtRecord"][i]["TrnChannel"])
                             }
                         }
                     }
                     tempData.push(segTable);
                 }
                 appendData(tempData);
             } else {
                 alert(callBackResponse["errMsg"]);
                 return false;
             }
         } else {
             alert(callBackResponse["errMsg"]);
             return false;
         }
     } else {
         if (status == 300) {
             dismissLoadingScreen(); //dismissLoadingScreenPopup();
             alert("" + kony.i18n.getLocalizedString("ECGenericError"));
         }
     }
 }

 function TDAmountFormat(amount) {
     if (amount == "" || amount == null) return "-";
     else {
         var sign = amount.substring(0, 1);
         sign = sign + " ";
         var amount = amount.substring(1, amount.length);
         amount = amount.concat(".00")
         var delimiter = ","; // replace comma if desired
         amount = new String(amount);
         var a = amount.split('.', 2)
         var d = a[1];
         var i = parseInt(a[0]);
         if (isNaN(i)) {
             return '';
         }
         var minus = '';
         if (i < 0) {
             minus = '- ';
         }
         i = Math.abs(i);
         var n = new String(i);
         var a = [];
         while (n.length > 3) {
             var nn = n.substr(n.length - 3);
             a.unshift(nn);
             n = n.substr(0, n.length - 3);
         }
         if (n.length > 0) {
             a.unshift(n);
         }
         n = a.join(delimiter);
         if (d.length < 1) {
             amount = n;
         } else {
             amount = n + '.' + d;
         }
         amount = minus + amount;
         amount = sign.concat(amount);
         return amount;
     }
 }
 /**
  * Method to invoke service for TDDetailstatement
  */
 function startTDDetailserviceIB() {
     totalGridData = [];
     startDateIB = startDateIB;
     endDateIB = endDateIB;
     var TDinputParam = {};
     TDinputParam["acctIdentValue"] = (gblAccountTable["custAcctRec"][gblIndex]["accId"]).substring(4);
     TDinputParam["fIIdent"] = gblAccountTable["custAcctRec"][gblIndex]["fiident"];
     TDinputParam["rqUUId"] = "";
     showLoadingScreen(); //showLoadingScreenPopup();
     invokeServiceSecureAsync("tDDetailinq", TDinputParam, startTDDetailserviceIBCallback);
 }
 /**
  * Callback method for startTDDetailserviceIBCallback()
  */
 function startTDDetailserviceIBCallback(status, callBackResponse) {
     if (status == 400) {
         if (callBackResponse["opstatus"] == 0) {
             dismissLoadingScreen();
             gblStmntSessionFlag = callBackResponse["sesFlag"];
             if (callBackResponse["tdDetailsRec"].length == 0) {
                 frmIBAccntFullStatement.dgdeposit.removeAll();
                 disablePagenumbers();
                 alert(kony.i18n.getLocalizedString("NoRecsFound"));
                 return;
             }
             var tempData = [];
             for (var i = 0; i < callBackResponse["tdDetailsRec"].length; i++) {
                 var segTable = {
                     "seqid": i,
                     "column6": {
                         "lbltransid": {
                             "text": commaFormattedStmt(callBackResponse["tdDetailsRec"][i]["depositAmtVal"]) //confirm + " " + kony.i18n.getLocalizedString("currencyThaiBaht")
                         }
                     },
                     "column1": {
                         "lbltransid": {
                             "text": callBackResponse["tdDetailsRec"][i]["depositNo"]
                         }
                     },
                     "column2": {
                         "lbltransid": {
                             "text": formatDateStmt(callBackResponse["tdDetailsRec"][i]["depositDate"])
                         }
                     },
                     "column5": {
                         "lbltransid": {
                             "text": formatInterestRate(Number(callBackResponse["tdDetailsRec"][i]["interestRate"])) + " %"
                         }
                     },
                     "column4": {
                         "lbltransid": {
                             "text": formatDateStmt(callBackResponse["tdDetailsRec"][i]["maturityDate"])
                         }
                     }
                 }
                 tempData.push(segTable);
             }
             appendData(tempData);
         } else {
             alert(callBackResponse["errMsg"]);
             return false;
         }
     } else {
         if (status == 300) {
             dismissLoadingScreen(); //dismissLoadingScreenPopup();
             alert("" + kony.i18n.getLocalizedString("ECGenericError"));
         }
     }
 }

 function formatInterestRate(interestRate) {
     if (interestRate == "" || interestRate == null) return "-";
     else {
         var data = interestRate.toFixed(2);
         return data;
     }
 }

 function formatDateStmtIB(dateStr) {
     if (dateStr == "" || dateStr == null) return "-";
     else {
         dstr1 = dateStr.substring(0, 4);
         dstr2 = dateStr.substring(5, 7);
         dstr3 = dateStr.substring(8, 10);
         return dstr3 + "/" + dstr2 + "/" + dstr1;
     }
 }

 function checkTranstypeIB(data, type) {
     if (data == "" || data == null) return "-";
     else {
         if (data < 0) return data;
         else if (type == "Credit") {
             data = "+ " + data;
             return data;
         } else if (type == "Debit") {
             data = "- " + data;
             return data;
         }
     }
 }

 function commaFormattedStmtIB(amount) {
     if (amount == "" || amount == null) return "-";
     else {
         amount = amount.concat(".00")
         var delimiter = ",";
         amount = new String(amount);
         var a = amount.split('.', 2)
         var d = a[1];
         var i = parseInt(a[0]);
         if (isNaN(i)) {
             return '';
         }
         var minus = '';
         if (i < 0) {
             minus = '- ';
         }
         i = Math.abs(i);
         var n = new String(i);
         var a = [];
         while (n.length > 3) {
             var nn = n.substr(n.length - 3);
             a.unshift(nn);
             n = n.substr(0, n.length - 3);
         }
         if (n.length > 0) {
             a.unshift(n);
         }
         n = a.join(delimiter);
         if (d.length < 1) {
             amount = n;
         } else {
             amount = n + '.' + d;
         }
         amount = minus + amount;
         return amount;
     }
 }
 /**
  * Loading Page numbersIn Transaction
  */
 function loadPagenumbersInStmt() {
     frmIBAccntFullStatement.link1.text = "";
     frmIBAccntFullStatement.link2.text = "";
     frmIBAccntFullStatement.link3.text = "";
     frmIBAccntFullStatement.link4.text = "";
     frmIBAccntFullStatement.link5.text = "";
     var seglength = totalGridData.length;
     nopagesStmt = Math.ceil(seglength / recordcountStmt);
     invokeCommonIBLogger("page nos  " + nopagesStmt);
     if (nopagesStmt == 1) frmIBAccntFullStatement.link1.text = "";
     else {
         if (currentpageStmt <= nopagesStmt) {
             frmIBAccntFullStatement.link1.setVisibility(true);
             frmIBAccntFullStatement.link1.text = currentpageStmt;
             currentpageStmt = currentpageStmt + 1;
         }
         if (currentpageStmt <= nopagesStmt) {
             frmIBAccntFullStatement.link2.setVisibility(true);
             frmIBAccntFullStatement.link2.text = currentpageStmt;
             currentpageStmt = currentpageStmt + 1;
         }
         if (currentpageStmt <= nopagesStmt) {
             frmIBAccntFullStatement.link3.setVisibility(true);
             frmIBAccntFullStatement.link3.text = currentpageStmt;
             currentpageStmt = currentpageStmt + 1;
         }
         if (currentpageStmt <= nopagesStmt) {
             frmIBAccntFullStatement.link4.setVisibility(true);
             frmIBAccntFullStatement.link4.text = currentpageStmt;
             currentpageStmt = currentpageStmt + 1;
         }
         if (currentpageStmt <= nopagesStmt) {
             frmIBAccntFullStatement.link5.setVisibility(true);
             frmIBAccntFullStatement.link5.text = currentpageStmt;
         }
     }
 }
 /**
  * Loading data to datagrid based on the pagination InFuture Transaction
  */
 function loadsegdataInStmt(page) {
     if (page == 1) {
         frmIBAccntFullStatement.link1.skin = lnkNormal;
         frmIBAccntFullStatement.link2.skin = lnkIB18pxBlueUnderline;
         frmIBAccntFullStatement.link3.skin = lnkIB18pxBlueUnderline;
         frmIBAccntFullStatement.link4.skin = lnkIB18pxBlueUnderline;
         frmIBAccntFullStatement.link5.skin = lnkIB18pxBlueUnderline;
     }
     invokeCommonIBLogger("page " + page);
     currentpageStmt = page;
     if (parseInt(frmIBAccntFullStatement.link5.text) < nopagesStmt || (bindataIB != "" && bindataIB != null)) {
         frmIBAccntFullStatement.linknext.setVisibility(true);
     } else if ((parseInt(frmIBAccntFullStatement.link5.text) >= nopagesStmt) && (bindataIB == "" || bindataIB == null)) {
         frmIBAccntFullStatement.linknext.setVisibility(false);
     } else if ((parseInt(frmIBAccntFullStatement.link5.text) >= nopagesStmt) && (bindataIB != "" || bindataIB != null)) {
         frmIBAccntFullStatement.linknext.setVisibility(true);
     }
     if (parseInt(frmIBAccntFullStatement.link1.text) > 1) {
         frmIBAccntFullStatement.linkprev.setVisibility(true);
     } else {
         frmIBAccntFullStatement.linkprev.setVisibility(false);
     }
     var countdata = [];
     var dataindex = parseInt(page) * recordcountStmt - recordcountStmt;
     for (var i = 0; i < recordcountStmt; i++) {
         if (dataindex < totalGridData.length) countdata[i] = totalGridData[dataindex];
         dataindex++;
     }
     if (gaccType == "CCA") frmIBAccntFullStatement.dgcredit.setData(countdata);
     else if (gaccType == "TD") frmIBAccntFullStatement.dgdeposit.setData(countdata);
     else if (gaccType == "SDA" || gaccType == "DDA" || gaccType == "CDA") frmIBAccntFullStatement.dgfullstmt.setData(countdata);
     else if (gaccType == "LOC") frmIBAccntFullStatement.dgloan.setData(countdata);
 }

 function nextPageloadInAccntStmt(nextpage) {
     if (nextpage == "") nextpage = 5;
     currentpageStmt = parseInt(nextpage) + 1;
     var dataindex = parseInt(currentpageStmt) * recordcountStmt - recordcountStmt;
     var dgdatalength = totalGridData.length - dataindex;
     //if ((dgdatalength < recordcountStmt) && (bindataIB != "" || bindataIB != null)) {
     if ((currentpageStmt >= nopagesStmt) && (bindataIB != "" && bindataIB != null)) {
         //currentpageStmt = currentpageStmt-4;
         appendDATADG = "YES";
         callAccntFullStatementIB();
         return;
     }
     var pageshown = currentpageStmt;
     frmIBAccntFullStatement.link5.skin = lnkNormal;
     frmIBAccntFullStatement.link2.skin = lnkIB18pxBlueUnderline;
     frmIBAccntFullStatement.link3.skin = lnkIB18pxBlueUnderline;
     frmIBAccntFullStatement.link4.skin = lnkIB18pxBlueUnderline;
     frmIBAccntFullStatement.link1.skin = lnkIB18pxBlueUnderline;
     frmIBAccntFullStatement.link1.text = currentpageStmt - 4;
     frmIBAccntFullStatement.link2.text = currentpageStmt - 3;
     frmIBAccntFullStatement.link3.text = currentpageStmt - 2;
     frmIBAccntFullStatement.link4.text = currentpageStmt - 1;
     frmIBAccntFullStatement.link5.text = currentpageStmt;
     loadsegdataInStmt(pageshown);
 }
 /**
  *
  * load previous page data on Previous link click
  */
 function prevPageloadInAccntStmt(prevpage) {
     currentpageStmt = parseInt(prevpage) - 1;
     var pageshown = currentpageStmt;
     frmIBAccntFullStatement.link1.text = currentpageStmt;
     frmIBAccntFullStatement.link2.text = currentpageStmt + 1;
     frmIBAccntFullStatement.link3.text = currentpageStmt + 2;
     frmIBAccntFullStatement.link4.text = currentpageStmt + 3;
     frmIBAccntFullStatement.link5.text = currentpageStmt + 4;
     frmIBAccntFullStatement.link1.skin = lnkNormal;
     frmIBAccntFullStatement.link2.skin = lnkIB18pxBlueUnderline;
     frmIBAccntFullStatement.link3.skin = lnkIB18pxBlueUnderline;
     frmIBAccntFullStatement.link4.skin = lnkIB18pxBlueUnderline;
     frmIBAccntFullStatement.link5.skin = lnkIB18pxBlueUnderline;
     loadsegdataInStmt(pageshown);
 }
 /**
  * On submit button click
  */
 function onSubmitbtnClick() {
     var startdate = gblTxnHistDate1;
     startDateIB = dateFormatChange(startdate);
     endDateIB = dateFormatChange(gblTxnHistDate2);
     totalGridData = [];
     currentpageStmt = 1;
     setSortBtnSkin();
     disablePagenumbers();
     bindataIB = "";
     binlengthIB = "";
     noOfStmtServiceCalls = 0;
     callAccntFullStatementIB();
 }
 /**
  *
  * format date in the required format which is returned fron slider
  */
 function dateFormatChange(dateIB) {
     var d = new Date(dateIB);
     var curr_date = d.getDate();
     var curr_month = d.getMonth() + 1; //Months are zero based
     var curr_year = d.getFullYear();
     if (curr_month < 10) curr_month = "0" + curr_month;
     if (curr_date < 10) curr_date = "0" + curr_date;
     var finalDate = curr_year + "-" + curr_month + "-" + curr_date;
     return finalDate;
 }
 /**
  * Load page numbers and append data
  */
 function appendData(tempData) {
     var datalength = totalGridData.length;
     var tempDatalength = datalength + tempData.length;
     if (appendDATADG == "YES") {
         for (i = 0; i < tempData.length; i++) {
             totalGridData[datalength] = tempData[i];
             datalength++;
         }
         var seglength1 = totalGridData.length;
         nopagesStmt = Math.ceil(seglength1 / recordcountStmt);
         if (nopagesStmt == 4) {
             frmIBAccntFullStatement.link5.skin = lnkIB18pxBlueUnderline;
             frmIBAccntFullStatement.link2.skin = lnkIB18pxBlueUnderline;
             frmIBAccntFullStatement.link3.skin = lnkIB18pxBlueUnderline;
             frmIBAccntFullStatement.link4.skin = lnkNormal;
             frmIBAccntFullStatement.link1.skin = lnkIB18pxBlueUnderline;
             currentpageStmt = 4;
             frmIBAccntFullStatement.link3.setVisibility(true);
             frmIBAccntFullStatement.link4.setVisibility(true);
             frmIBAccntFullStatement.link3.text = currentpageStmt - 1;
             frmIBAccntFullStatement.link4.text = currentpageStmt;
         } else if (nopagesStmt == 5) {
             frmIBAccntFullStatement.link5.skin = lnkNormal;
             frmIBAccntFullStatement.link2.skin = lnkIB18pxBlueUnderline;
             frmIBAccntFullStatement.link3.skin = lnkIB18pxBlueUnderline;
             frmIBAccntFullStatement.link4.skin = lnkIB18pxBlueUnderline;
             frmIBAccntFullStatement.link1.skin = lnkIB18pxBlueUnderline;
             currentpageStmt = 5;
             frmIBAccntFullStatement.link3.setVisibility(true);
             frmIBAccntFullStatement.link4.setVisibility(true);
             frmIBAccntFullStatement.link5.setVisibility(true);
             frmIBAccntFullStatement.link3.text = currentpageStmt - 2;
             frmIBAccntFullStatement.link4.text = currentpageStmt - 1;
             frmIBAccntFullStatement.link5.text = currentpageStmt;
         } else if (nopagesStmt > 5) {
             frmIBAccntFullStatement.link5.skin = lnkNormal;
             frmIBAccntFullStatement.link2.skin = lnkIB18pxBlueUnderline;
             frmIBAccntFullStatement.link3.skin = lnkIB18pxBlueUnderline;
             frmIBAccntFullStatement.link4.skin = lnkIB18pxBlueUnderline;
             frmIBAccntFullStatement.link1.skin = lnkIB18pxBlueUnderline;
             frmIBAccntFullStatement.link1.text = currentpageStmt - 4;
             frmIBAccntFullStatement.link2.text = currentpageStmt - 3;
             frmIBAccntFullStatement.link3.text = currentpageStmt - 2;
             frmIBAccntFullStatement.link4.text = currentpageStmt - 1;
             frmIBAccntFullStatement.link5.text = currentpageStmt;
             frmIBAccntFullStatement.link3.setVisibility(true);
             frmIBAccntFullStatement.link4.setVisibility(true);
             frmIBAccntFullStatement.link5.setVisibility(true);
         }
     } else if (appendDATADG == "NO") {
         appendDATADG = "NO";
         totalGridData = tempData;
         currentpageStmt = 1;
         loadPagenumbersInStmt();
         currentpageStmt = 1;
     }
     /*if (sortOrderStmt == "descending")
     	tempData = sortDateDescend(totalGridData, "column1", "lbltransid" ,"seqid");
     else if (sortOrderStmt == "ascending")
     	tempData = sortDateAscend(totalGridData, "column1", "lbltransid" ,"seqid");*/
     tempData = totalGridData;
     if (bindataIB != "" || bindataIB != null) appendDATADG = "YES";
     if (bindataIB == "" || bindataIB == null) appendDATADG = "NO";
     loadsegdataInStmt(currentpageStmt);
 }
 /**
  * On deposit details button click
  */
 function onDepositDetailsClick() {
     gaccType = "TD";
     gblStmntSessionFlag = "2";
     //setSortBtnSkin();
     frmIBAccntFullStatement.dgfullstmt.setVisibility(false);
     frmIBAccntFullStatement.dgloan.setVisibility(false);
     frmIBAccntFullStatement.dgdeposit.setVisibility(true);
     frmIBAccntFullStatement.lblselect.text = "";
     frmIBAccntFullStatement.btndeposit.skin = "btnIBTabType2Focus";
     frmIBAccntFullStatement.btndeposit.focusSkin = "btnIBTabType2Focus";
     frmIBAccntFullStatement.btnstmt.skin = "btnIBTabType2Normal";
     frmIBAccntFullStatement.btnstmt.focusSkin = "btnIBTabType2Normal";
     frmIBAccntFullStatement.hbxslider.setVisibility(false);
     frmIBAccntFullStatement.hbxmonthcycle.setVisibility(false);
     var headertext = frmIBAccntFullStatement.dgdeposit.columnHeadersConfig;
     headertext[0].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("keyIBDepositNo");
     headertext[1].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("keyIBPostDate");
     headertext[2].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("keyHeaderMaturityDate");
     headertext[3].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("keyIBInterestRate");
     headertext[4].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("Balance");
     startTDDetailserviceIB();
 }
 /**
  * On FullStatement button click
  */
 function onFullStatementClick() {
     gaccType = "CDA";
     setSortBtnSkin();
     frmIBAccntFullStatement.dgfullstmt.setVisibility(true);
     frmIBAccntFullStatement.dgloan.setVisibility(false);
     frmIBAccntFullStatement.dgdeposit.setVisibility(false);
     frmIBAccntFullStatement.lblselect.text = kony.i18n.getLocalizedString("MyActivitiesIB_Duration");
     frmIBAccntFullStatement.hbxslider.setVisibility(true);
     frmIBAccntFullStatement.hbxmonthcycle.setVisibility(true);
     frmIBAccntFullStatement.btnstmt.skin = "btnIBTabType2Focus";
     frmIBAccntFullStatement.btnstmt.focusSkin = "btnIBTabType2Focus";
     frmIBAccntFullStatement.btndeposit.skin = "btnIBTabType2Normal";
     frmIBAccntFullStatement.btndeposit.focusSkin = "btnIBTabType2Normal";
     var headertext = frmIBAccntFullStatement.dgfullstmt.columnHeadersConfig;
     headertext[0].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("keyDate");
     headertext[1].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("keyIBDescription");
     headertext[2].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("MyActivitiesIB_Channel");
     headertext[3].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("MyActivitiesIB_Amount");
     headertext[4].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("Balance");
     totalGridData = [];
     startTDstatementserviceIB(startDateIB, endDateIB);
 }
 /**
  * On biled button click
  */
 function onBilledbtnClick() {
     setSortBtnSkin();
     frmIBAccntFullStatement.hbxunbilled.setVisibility(false);
     frmIBAccntFullStatement.hbxbilled.setVisibility(true);
     frmIBAccntFullStatement.btnunbill.skin = "btntabright";
     frmIBAccntFullStatement.btnunbill.focusSkin = "btntabright";
     frmIBAccntFullStatement.btnbill.skin = "btntabrightfoc";
     frmIBAccntFullStatement.btnbill.focusSkin = "btntabrightfoc";
     frmIBAccntFullStatement.btnm1.skin = "btnIBrndleftblueFullStatement";
     frmIBAccntFullStatement.btnm2.skin = "btnIBTabgreyFullStatement";
     frmIBAccntFullStatement.btnm3.skin = "btnIBTabgreyFullStatement";
     frmIBAccntFullStatement.btnm4.skin = "btnIBTabgreyFullStatement";
     frmIBAccntFullStatement.btnm5.skin = "btnIBTabgreyFullStatement";
     frmIBAccntFullStatement.btnm6.skin = "btnIBrndrightgreyFullStatement";
     totalGridData = [];
     startCCstatementserviceIB("billed", "1");
 }
 /**
  * On Unbiled button click
  */
 function onUnbilledbtnClick() {
     setSortBtnSkin();
     frmIBAccntFullStatement.hbxunbilled.setVisibility(true);
     frmIBAccntFullStatement.hbxbilled.setVisibility(false);
     frmIBAccntFullStatement.btnunbill.skin = "btntabrightfoc";
     frmIBAccntFullStatement.btnunbill.focusSkin = "btntabrightfoc";
     frmIBAccntFullStatement.btnbill.skin = "btntabright";
     frmIBAccntFullStatement.btnbill.focusSkin = "btntabright";
     totalGridData = [];
     startCCstatementserviceIB("unbilled1", "0");
 }
 /**
  *
  * Format points with comma
  */
 function commaFormattedPoints(points) {
     var minus = '';
     if (points == "" || points == null) return "-";
     var delimiter = ","; // replace comma if desired
     if (parseFloat(points) < 0) {
         minus = '-';
     }
     points = new String(points);
     var a = points.split('.', 2)
     var i = parseInt(a[0]);
     if (isNaN(i)) {
         return '';
     }
     i = Math.abs(i);
     var n = new String(i);
     var a = [];
     while (n.length > 3) {
         var nn = n.substr(n.length - 3);
         a.unshift(nn);
         n = n.substr(0, n.length - 3);
     }
     if (n.length > 0) {
         a.unshift(n);
     }
     n = a.join(delimiter);
     points = n;
     points = minus + points;
     return points;
 }
 /** data to be changed on locale switch
  **/
 function frmIBAccntFullStatementPreShow() {
     //CASA
     var headertext = frmIBAccntFullStatement.dgfullstmt.columnHeadersConfig;
     frmIBAccntFullStatement.dgfullstmt.columnHeadersConfig[0].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("keyDate");
     headertext[1].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("keyIBDescription");
     headertext[2].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("MyActivitiesIB_Channel");
     headertext[3].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("MyActivitiesIB_Amount");
     headertext[4].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("Balance");
     //LOAN
     var headertextLoan = frmIBAccntFullStatement.dgloan.columnHeadersConfig;
     headertextLoan[0].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("keyDate");
     headertextLoan[1].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("keyHeaderPrinciplePaid");
     headertextLoan[2].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("keyHeaderInterestPaid");
     headertextLoan[3].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("keyMBPaymentAmount");
     headertextLoan[4].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("keyHeaderOutstandingBalance");
     //CC
     var headertextCC = frmIBAccntFullStatement.dgcredit.columnHeadersConfig;
     headertextCC[0].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("keyIBTransactionDate");
     headertextCC[1].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("keyIBPostedDate");
     headertextCC[2].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("keyIBDescription");
     headertextCC[3].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("MyActivitiesIB_Amount");
     //TD
     var headertextTD = frmIBAccntFullStatement.dgdeposit.columnHeadersConfig;
     headertextTD[0].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("keyIBDepositNo");
     headertextTD[1].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("keyIBPostDate");
     headertextTD[2].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("keyHeaderMaturityDate");
     headertextTD[3].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("keyIBInterestRate");
     headertextTD[4].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("Balance");
     //		
     frmIBAccntFullStatement.lbllimit.text = kony.i18n.getLocalizedString("keyMBCreditlimit");
     frmIBAccntFullStatement.lbloutbal.text = kony.i18n.getLocalizedString("keyIBOutstanding");
     frmIBAccntFullStatement.lblcycle.text = kony.i18n.getLocalizedString("keyIBSpendcycle");
     frmIBAccntFullStatement.lblpoints.text = kony.i18n.getLocalizedString("keyIBAvailable");
     frmIBAccntFullStatement.lblexpire.text = kony.i18n.getLocalizedString("keyIBexpiring");
     frmIBAccntFullStatement.lblexp.text = kony.i18n.getLocalizedString("keyIBPointexpiry");
     frmIBAccntFullStatement.lblfulpay.text = kony.i18n.getLocalizedString("keyIBFullPay");
     frmIBAccntFullStatement.lblminpay.text = kony.i18n.getLocalizedString("keyIBMinimumPay");
     frmIBAccntFullStatement.lbldue.text = kony.i18n.getLocalizedString("keyIBDueDate");
     frmIBAccntFullStatement.lblpntsearned.text = kony.i18n.getLocalizedString("keyIBEarned");
     frmIBAccntFullStatement.lbltotalpnts.text = kony.i18n.getLocalizedString("keyIBAvailable");
     loadsegdataInStmt(currentpageStmt);
     gblLocale = false;
 }
 /**
  * set  SortBtn Skin as default
  */
 function setSortBtnSkin() {
     clearIBCCValues();
     frmIBAccntFullStatement.dgcredit.removeAll();
     //frmIBAccntFullStatement.dgfullstmt.removeAll();
     //frmIBAccntFullStatement.dgdeposit.removeAll();
     //frmIBAccntFullStatement.dgloan.removeAll();
     gblStmntSessionFlag = "2";
     bindataIB = "";
     binlengthIB = "";
     noOfStmtServiceCalls = 0;
     appendDATADG = "NO";
     disablePagenumbers();
     frmIBAccntFullStatement.linknext.setVisibility(false);
     frmIBAccntFullStatement.linkprev.setVisibility(false);
 }

 function setMyActivitiesMenu(formname) {
     if (kony.i18n.getCurrentLocale() == "th_TH") {
         formname.btnMenuMyActivities.skin = "btnIBMenuMyActivitiesFocusThai";
     } else {
         formname.btnMenuMyActivities.skin = "btnIBMenuMyActivitiesFocus";
     }
 }

 function clearIBCCValues() {
     frmIBAccntFullStatement.lbllimitval.text = "";
     frmIBAccntFullStatement.lbloutbalval.text = "";
     frmIBAccntFullStatement.lblcycleval.text = "";
     frmIBAccntFullStatement.lblpointsval.text = "";
     frmIBAccntFullStatement.lblexpireval.text = "";
     frmIBAccntFullStatement.lblexpval.text = "";
     frmIBAccntFullStatement.lblfulpayval.text = "";
     frmIBAccntFullStatement.lblminpayval.text = "";
     frmIBAccntFullStatement.lbldueval.text = "";
     frmIBAccntFullStatement.lblpntsearnedval.text = "";
     frmIBAccntFullStatement.lbltotalpntsval.text = "";
 }

 function syncIBFullStmtLocale() {
     var currentFormId = kony.application.getCurrentForm().id;
     if (currentFormId == "frmIBAccntFullStatement") {
         if (kony.i18n.getCurrentLocale() == "th_TH") {
             frmIBAccntFullStatement.btnMenuMyAccountSummary.skin = "btnIBMenuMyAccountSummaryFocusThai";
         } else {
             frmIBAccntFullStatement.btnMenuMyAccountSummary.skin = "btnIBMenuMyAccountSummaryFocus";
         }
         getMonthCycleIB();
         frmIBAccntFullStatementPreShow();
         frmIBAccntFullStatement.lblselect.text = kony.i18n.getLocalizedString("MyActivitiesIB_Duration");
         frmIBAccntFullStatement.lblbill.text = kony.i18n.getLocalizedString("keySelectStmntCycle");
         if (gaccType == "CCA") frmIBAccntFullStatement.lblamt.text = kony.i18n.getLocalizedString("keyCreditlblIB") + " " + frmIBAccntSummary.lblAccountBalanceHeader.text;
         frmIBAccntFullStatement.lblpointsval.text = frmIBAccntFullStatement.lblpointsval.text.split(" ")[0] + " " + kony.i18n.getLocalizedString("keyIBPoints");
         frmIBAccntFullStatement.lblexpireval.text = frmIBAccntFullStatement.lblexpireval.text.split(" ")[0] + " " + kony.i18n.getLocalizedString("keyIBPoints");
         frmIBAccntFullStatement.lblpntsearnedval.text = frmIBAccntFullStatement.lblpntsearnedval.text.split(" ")[0] + " " + kony.i18n.getLocalizedString("keyIBPoints");
         frmIBAccntFullStatement.lbltotalpntsval.text = frmIBAccntFullStatement.lbltotalpntsval.text.split(" ")[0] + " " + kony.i18n.getLocalizedString("keyIBPoints");
         var sbStr = gblAccountTable["custAcctRec"][gblIndex]["accId"];
         var length = sbStr.length;
         if (gblAccountTable["custAcctRec"][gblIndex]["accType"] == kony.i18n.getLocalizedString("Loan")) sbStr = sbStr.substring(7, 11);
         else sbStr = sbStr.substring(length - 4, length);
         var locale = kony.i18n.getCurrentLocale();
         var accNickName;
         if (locale == "en_US") {
             productName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"];
             accNickName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"] + sbStr
         } else {
             productName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"];
             accNickName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"] + sbStr
         }
         if (null != gblAccountTable["custAcctRec"][gblIndex]["acctNickName"]) {
             frmIBAccntFullStatement.lblname.text = gblAccountTable["custAcctRec"][gblIndex]["acctNickName"];
         } else {
             frmIBAccntFullStatement.lblname.text = accNickName;
         }
     } else if (currentFormId == "frmIBMyActivities") {
         gsFormId = kony.application.getCurrentForm();
         getMonthCycleIB();
         setMyActivitiesMenu(frmIBMyActivities);
         onLocaleChange();
         //For drop down list locale change
         setFilterTxnTypes();
         //For Month combo box locale change
         setMonthLocaleChange();
     } else if (currentFormId == "frmIBDateSlider") {
         gsFormId = kony.application.getCurrentForm();
         getMonthCycleIB();
         setMyActivitiesMenu(frmIBDateSlider);
         onLocaleChange();
         //For drop down list locale change
         setFilterTxnTypes();
         //For Month combo box locale change
         setMonthLocaleChange();
     } else if (currentFormId == "frmIBBillPaymentExecutedTxn") {
         showRefValues(frmIBBillPaymentExecutedTxn);
         setMyActivitiesMenu(frmIBBillPaymentExecutedTxn);
     } else if (currentFormId == "frmIBTopUpExecutedTxn") {
         showRefValues(frmIBTopUpExecutedTxn);
         setMyActivitiesMenu(frmIBTopUpExecutedTxn);
     } else if (currentFormId == "frmIBBeepAndBillExecutedTxn") {
         showRefValues(frmIBBeepAndBillExecutedTxn);
         setMyActivitiesMenu(frmIBBeepAndBillExecutedTxn);
     } else if (currentFormId == "frmIBExecutedTransaction") {
         if (gblSmartFlag) {
             frmIBExecutedTransaction.lblSplitXferDt.text = kony.i18n.getLocalizedString("keyOrderDAteFTMB");
         } else {
             frmIBExecutedTransaction.lblSplitXferDt.text = kony.i18n.getLocalizedString("keyXferTD");
         }
         frmIBExecutedTransaction.lblSplitTotAmt.text = kony.i18n.getLocalizedString("keyXferTotAmt");
         frmIBExecutedTransaction.lblSplitTotFee.text = kony.i18n.getLocalizedString("keyXferTotFee");
         if (frmIBExecutedTransaction.segSplitDet.data.length > 1) {
             var tempdata = [];
             for (var i = 0; i < frmIBExecutedTransaction.segSplitDet.data.length; i++) {
                 if (undefined != gblXferSplit[i]) {
                     tempdata.push({
                         "lblSplitTRN": kony.i18n.getLocalizedString("keyTransactionRefNo"),
                         "lblSplitAmt": kony.i18n.getLocalizedString("MyActivitiesIB_Amount"),
                         "lblSplitFee": kony.i18n.getLocalizedString("keyFee"),
                         "lblSplitTRNVal": frmIBExecutedTransaction.segSplitDet.data[i]["lblSplitTRNVal"],
                         "lblSplitAmtVal": frmIBExecutedTransaction.segSplitDet.data[i]["lblSplitAmtVal"],
                         "lblSplitFeeVal": frmIBExecutedTransaction.segSplitDet.data[i]["lblSplitFeeVal"],
                         "imgFail": {
                             "src": getXferImage(gblXferSplit[i]["txnStatus"])
                         }
                     })
                 }
             }
             frmIBExecutedTransaction.segSplitDet.setData(tempdata);
         }
         setMyActivitiesMenu(frmIBExecutedTransaction);
     } else if (currentFormId == "frmIBePaymentExecutedTxn") {
         showRefValues(frmIBePaymentExecutedTxn);
         setMyActivitiesMenu(frmIBePaymentExecutedTxn);
     } else if (currentFormId == "frmIBTOASavingsExecutedTxn") {
         setMyActivitiesMenu(frmIBTOASavingsExecutedTxn);
         if (kony.i18n.getCurrentLocale() == "en_US") {
             frmIBTOASavingsExecutedTxn.lblBranchVal.text = gblBranchNameEN;
             frmIBTOASavingsExecutedTxn.lblProdNameVal.text = gblProdNameEN;
         } else {
             frmIBTOASavingsExecutedTxn.lblBranchVal.text = gblBranchNameTH;
             frmIBTOASavingsExecutedTxn.lblProdNameVal.text = gblProdNameTH;
         }
     } else if (currentFormId == "frmIBTOASavingsCareExecutedTxn") {
         setMyActivitiesMenu(frmIBTOASavingsCareExecutedTxn);
         if (kony.i18n.getCurrentLocale() == "en_US") {
             frmIBTOASavingsCareExecutedTxn.lblOASCBranchVal.text = gblBranchNameEN;
         } else {
             frmIBTOASavingsCareExecutedTxn.lblOASCBranchVal.text = gblBranchNameTH;
         }
     } else if (currentFormId == "frmIBTOATDExecutedTxn") {
         setMyActivitiesMenu(frmIBTOATDExecutedTxn);
         if (kony.i18n.getCurrentLocale() == "en_US") {
             frmIBTOATDExecutedTxn.lblType.text = gblProdNameEN;
             frmIBTOATDExecutedTxn.lblBranchVal.text = gblBranchNameEN;
         } else {
             frmIBTOATDExecutedTxn.lblType.text = gblProdNameTH;
             frmIBTOATDExecutedTxn.lblBranchVal.text = gblBranchNameTH;
         }
     } else if (currentFormId == "frmIBTOADreamExecutedTxn") {
         setMyActivitiesMenu(frmIBTOADreamExecutedTxn);
         if (kony.i18n.getCurrentLocale() == "en_US") {
             frmIBTOADreamExecutedTxn.lblDSAckTitle.text = gblProdNameEN;
             frmIBTOADreamExecutedTxn.lblBranchVal.text = gblBranchNameEN;
         } else {
             frmIBTOADreamExecutedTxn.lblDSAckTitle.text = gblProdNameTH;
             frmIBTOADreamExecutedTxn.lblBranchVal.text = gblBranchNameTH;
         }
     } else if (currentFormId == "frmIBSTSExecutedTxn") {
         setMyActivitiesMenu(frmIBSTSExecutedTxn);
     } else if (currentFormId == "frmIBPostLoginDashboard") {
         setMonthLocaleChange();
     }
 }

 function getMonthCycleIB() {
     var montharray = [];
     var locale = kony.i18n.getCurrentLocale();
     montharray = [kony.i18n.getLocalizedString("keyCalendarJan"), kony.i18n.getLocalizedString("keyCalendarFeb"), kony.i18n.getLocalizedString("keyCalendarMar"),
         kony.i18n.getLocalizedString("keyCalendarApr"), kony.i18n.getLocalizedString("keyCalendarMay"), kony.i18n.getLocalizedString("keyCalendarJun"),
         kony.i18n.getLocalizedString("keyCalendarJul"), kony.i18n.getLocalizedString("keyCalendarAug"), kony.i18n.getLocalizedString("keyCalendarSep"),
         kony.i18n.getLocalizedString("keyCalendarOct"), kony.i18n.getLocalizedString("keyCalendarNov"), kony.i18n.getLocalizedString("keyCalendarDec")
     ];
     var currentForm = kony.application.getCurrentForm();
     var month = new Date();
     var day = month.getDate();
     var mm = month.getMonth();
     var yy = month.getFullYear();
     var montharrayfull = [];
     montharrayfull = [kony.i18n.getLocalizedString("keyCalendarJanuary"), kony.i18n.getLocalizedString("keyCalendarFebruary"), kony.i18n.getLocalizedString("keyCalendarMarch"),
         kony.i18n.getLocalizedString("keyCalendarApril"), kony.i18n.getLocalizedString("keyCalendarMay"), kony.i18n.getLocalizedString("keyCalendarJune"),
         kony.i18n.getLocalizedString("keyCalendarJuly"), kony.i18n.getLocalizedString("keyCalendarAugust"), kony.i18n.getLocalizedString("keyCalendarSeptember"),
         kony.i18n.getLocalizedString("keyCalendarOctober"), kony.i18n.getLocalizedString("keyCalendarNovember"), kony.i18n.getLocalizedString("keyCalendarDecember")
     ];
     var dateToday = day + " " + montharrayfull[mm] + " " + yy;
     currentForm.lbltoday.text = dateToday;
     currentForm.lblmonth7.text = montharray[mm];
     mm = mm - 1;
     if (mm == -1) {
         mm = 11;
         yy = yy - 1;
     }
     currentForm.lblmonth6.text = montharray[mm];
     mm = mm - 1;
     if (mm == -1) {
         mm = 11;
         yy = yy - 1;
     }
     currentForm.lblmonth5.text = montharray[mm];
     mm = mm - 1;
     if (mm == -1) {
         mm = 11;
         yy = yy - 1;
     }
     currentForm.lblmonth4.text = montharray[mm];
     mm = mm - 1;
     if (mm == -1) {
         mm = 11;
         yy = yy - 1;
     }
     currentForm.lblmonth3.text = montharray[mm];
     mm = mm - 1;
     if (mm == -1) {
         mm = 11;
         yy = yy - 1;
     }
     currentForm.lblmonth2.text = montharray[mm];
     mm = mm - 1;
     if (mm == -1) {
         mm = 11;
         yy = yy - 1;
     }
     currentForm.lblmonth1.text = montharray[mm];
 }

 function callAccntFullStatementIB() {
     if (gaccType == "SDA" || gaccType == "DDA") {
         startCASAstatementserviceIB(startDateIB, endDateIB);
     } else if (gaccType == "LOC") {
         startLoanStatementserviceIB(startDateIB, endDateIB);
     } else if (gaccType == "CCA") {
         startCCstatementserviceIB(creditypeIB, wcodeIB);
     } else if (gaccType == "CDA") {
         startTDstatementserviceIB(startDateIB, endDateIB);
     } else if (gaccType == "TD") {
         startTDDetailserviceIB();
     }
 }
 /** Disable page numbers
  **/
 function disablePagenumbers() {
     frmIBAccntFullStatement.link1.setVisibility(false);
     frmIBAccntFullStatement.link2.setVisibility(false);
     frmIBAccntFullStatement.link3.setVisibility(false);
     frmIBAccntFullStatement.link4.setVisibility(false);
     frmIBAccntFullStatement.link5.setVisibility(false);
     frmIBAccntFullStatement.linkprev.setVisibility(false);
     frmIBAccntFullStatement.linknext.setVisibility(false);
 }

 function savePDFAccountHistoryNew(fileType) {
     var inputParam = {};
     var suffix = "";
     var monthlyInstallment = "";
     var paymentDueDate = "";
     var accountNum = gblAccountTable["custAcctRec"][gblIndex]["accId"];
     inputParam["filetype"] = fileType;
     inputParam["accType"] = gaccType;
     var accountType = "";
     var d1 = dateFormatChange(new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()))
     var credittype = "";
     if (creditypeIB == "unbilled2" || creditypeIB == "unbilled1") {
         credittype = "Unbilled";
     } else {
         credittype = "Billed";
     }
     if (gaccType == "LOC") {
         accountNum = accountNum.substring(1, 11);
         if (gblAccountTable["custAcctRec"][gblIndex]["ICON_ID"] == "ICON-05") {
             if (gaccType == "LOC") {
                 suffix = frmIBAccntSummary.lblUsefulValue3.text;
             } else {
                 suffix = frmIBAccntSummary.lblUsefulValue4.text;
             }
             monthlyInstallment = frmIBAccntSummary.lblUsefulValue7.text + "" + kony.i18n.getLocalizedString("currencyThaiBaht");
             paymentDueDate = frmIBAccntSummary.lblUsefulValue6.text;
         } else {
             suffix = frmIBAccntSummary.lblUsefulValue3.text;
             monthlyInstallment = frmIBAccntSummary.lblUsefulValue6.text;
             paymentDueDate = frmIBAccntSummary.lblUsefulValue5.text;
         }
     }
     if (kony.i18n.getCurrentLocale() == "th_TH") custName = gblCustomerNameTh;
     else custName = gblCustomerName;
     var totalData = {
         "billed": wcodeIB,
         "accountId": formatAccountNumber(accountNum),
         "printedDate": d1.split("-", 3)[2] + "/" + d1.split("-", 3)[1] + "/" + d1.split("-", 3)[0],
         "creditType": credittype,
         "startDate": startDateIB.split("-", 3)[2] + "/" + startDateIB.split("-", 3)[1] + "/" + startDateIB.split("-", 3)[0],
         "endDate": endDateIB.split("-", 3)[2] + "/" + endDateIB.split("-", 3)[1] + "/" + endDateIB.split("-", 3)[0],
         "localeId": kony.i18n.getCurrentLocale(),
         "accountType": custName,
         "suffixNo": suffix,
         "monthlyInstallment": monthlyInstallment,
         "paymentDueOn": paymentDueDate,
         "remBal": commaFormatted(gblAccountTable["custAcctRec"][gblIndex]["availableBal"]) + "" + kony.i18n.getLocalizedString("currencyThaiBaht"),
         "cardHolder": frmIBAccntSummary.lblUsefulValue3.text,
         "creditCardNumber": frmIBAccntSummary.lblUsefulValue2.text,
         "remainingCreditlimit": frmIBAccntFullStatement.lbllimitval.text,
         "SpendingBillingCycle": frmIBAccntFullStatement.lblcycleval.text,
         "oustandingBal": frmIBAccntFullStatement.lbloutbalval.text,
         "availableRewardPoints": frmIBAccntFullStatement.lblpointsval.text,
         "ICON_ID": gblAccountTable["custAcctRec"][gblIndex]["ICON_ID"],
         "pointsExpiring": frmIBAccntFullStatement.lblexpireval.text,
         "pointsExpiryDate": frmIBAccntFullStatement.lblexpval.text,
         "fullPaymentAmnt": frmIBAccntFullStatement.lblfulpayval.text,
         "minPaymentAmnt": frmIBAccntFullStatement.lblminpayval.text,
         "paymentDueDate": frmIBAccntFullStatement.lbldueval.text,
         "totalBilledPointsEarned": frmIBAccntFullStatement.lblpntsearnedval.text,
         "totalBilledPoints": frmIBAccntFullStatement.lbltotalpntsval.text,
         "interestRate": frmIBAccntSummary.segMaturityDisplay.data != undefined ? frmIBAccntSummary.segMaturityDisplay.data[0].lblTransaction : "0.00%",
         "amountDue": "",
         "stmtDate": pdfStmntDate != undefined ? pdfStmntDate : "",
         "totalAmountDue": ""
     };
     inputParam["outputtemplatename"] = gaccType + "Statement_" + d1.split("-", 3)[2] + "" + d1.split("-", 3)[1] + "" + d1.split("-", 3)[0];
     inputParam["datajson"] = JSON.stringify(totalData, "");
     if (gblDeviceInfo.name == "thinclient") {
         inputParam["channelId"] = GLOBAL_IB_CHANNEL;
     } else {
         inputParam["channelId"] = GLOBAL_MB_CHANNEL;
     }
     inputParam["accName"] = frmIBAccntFullStatement.lblname.text;
     var url = "";
     invokeServiceSecureAsync("generatePdfImageForActStmts", inputParam, ibRenderFileCallbackfunction);
 }

 function savePDFAccountHistory(fileType) {
     savePDFAccountHistoryNew(fileType);
 }