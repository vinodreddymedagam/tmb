function getAccountDetailMessageSpa(segData) {
    var accDetailMsg = "";
    var locale = kony.i18n.getCurrentLocale();
    var tem = segData //frmIBMyReceipentsAddBankAccnt.segementTobeAddedAccnts.data;
    var count1 = 0;
    var count2 = 0;
    var oCount = [];
    var obn = "";
    var accString = kony.i18n.getLocalizedString("Receipent_Account");
    var bnName = ""
    var acctTemp = []; //CR13
    var tmpAcctId = "";
    for (var f = 0; f < globalSelectBankData.length; f++) {
        oCount.push(0);
        acctTemp.push("");
    }
    for (var i = 0; i < tem.length; i++) {
        if (spaChnage == "myacctsadd") {
            bnName = getBankName(tem[i].bankCD);
        } else {
            bnName = tem[i].lblBankName;
        }
        count2++;
        for (var g = 0; g < globalSelectBankData.length; g++) {
            if (bnName == globalSelectBankData[g][7] || bnName == globalSelectBankData[g][8]) {
                oCount[g] = kony.os.toNumber(oCount[g]);
                obn = globalSelectBankData[g][2]
                oCount[g]++;
                var bankNumber = "";
                if (spaChnage == "myacctsadd") {
                    bankNumber = tem[i].lblAccntNoValue
                } else {
                    bankNumber = tem[i].lblAccountNo.text;
                }
                tmpAcctId = removeHyphenIB(bankNumber);
                tmpAcctId = "x" + tmpAcctId.substring(tmpAcctId.length - 4);
                acctTemp[g] = acctTemp[g] + "," + tmpAcctId;
            }
        }
    }
    var tcount = count2;
    var accDetailMsgt = ""
    var naccDetailMsgt = ""
    if (tcount > 0) {
        for (var h = 0; h < oCount.length; h++) {
            if (oCount[h] > 1) {
                accDetailMsgt = accDetailMsgt + oCount[h] + ":" + globalSelectBankData[h][2] + ", "
            } else if (oCount[h] == 1) {
                accDetailMsgt = accDetailMsgt + globalSelectBankData[h][2] + ", ";
            }
            if (oCount[h] == 1 || oCount[h] > 1) {
                var str = acctTemp[h];
                naccDetailMsgt = naccDetailMsgt + globalSelectBankData[h][2] + "(" + str.substring(1) + ")";
            }
        }
    }
    if (accDetailMsgt.length > 1) {
        accDetailMsgt = accDetailMsgt.substring(0, accDetailMsgt.length - 2);
    }
    if (tcount > 0) {
        accDetailMsg = tcount + " " + accString + " (" + accDetailMsgt + ")";
    }
    return naccDetailMsgt;
}

function destroySavedBankData() {
    if (globalTobeAddedBankData != null) {
        for (var i = 0; i < globalTobeAddedBankData.length; i++) {
            globalTobeAddedBankData.pop();
        }
        globalTobeAddedBankData = [];
    }
    if (globalTobeAddedBankDataConfirmation != null) {
        for (var i = 0; i < globalTobeAddedBankDataConfirmation.length; i++) {
            globalTobeAddedBankDataConfirmation.pop();
        }
        globalTobeAddedBankDataConfirmation = [];
    }
}