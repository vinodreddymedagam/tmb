function OTPReqButton() {
    frmIBActivateIBankingStep2Confirm.btnRequest.setEnabled(true);
    frmIBActivateIBankingStep2Confirm.btnRequest.skin = "btnIB158";
    frmIBActivateIBankingStep2Confirm.btnRequest.focusSkin = "btnIB158active";
}

function maskingIB(phoneno) {
    if (phoneno != "") {
        return phoneno = "xxx-xxx-" + phoneno.slice(6);
    }
}

function maskingMB(phoneno) {
    if (phoneno != "" && phoneno != null) {
        return phoneno = "xxx-xxx-" + phoneno.substring(6, 10);
    }
}

function addHyphenIB(accno) {
    if (accno != "") {
        var hyphenText = "";
        if (accno.length > 10) {
            accno = accno.substring(accno.length - 10, accno.length)
        }
        for (i = 0; i < accno.length; i++) {
            hyphenText += accno[i];
            if (i == 2 || i == 3 || i == 8) {
                hyphenText += '-';
            }
        }
        return hyphenText;
    }
}

function ActvatnCodeValidatnIB(text) {
    if (text == null) return false;
    var txtLen = text.length;
    var charArray = ["|", "|", "o", "O", "0"];
    var isIPCharchek = kony.string.containsNoGivenChars(text, charArray);
    var pat1 = /[A-Z]/g;
    var pat2 = /[a-z]/g;
    var pat3 = /[0-9]/g;
    var pat4 = /[-!$%^&*()_+|~=`{}\[\]:\";\s'<>?,.@#\/]/g;
    var isUpperCasechk = pat1.test(text);
    var isLowerCseChk = pat2.test(text);
    var isNumCheck = pat3.test(text);
    var isSpecialCharCheck = pat4.test(text);
    //if (txtLen != 8 || isIPCharchek == false || isNumCheck == false || isUpperCasechk == false || isLowerCseChk == false || isSpecialCharCheck == true) {
    if (txtLen != 8) {
        //alert("Incorrect ActivationCode, please re-enter");
        return false;
    }
}
//function trassactionPwdValidatnIB(text) {
//	if (text == null) return false;
//	var txtLen = text.length;
//	var pat1 = /[A-Za-z]/g;
//	var pat2 = /[0-9]/g
//	var isAlpha = pat1.test(text);
//	var isNum = pat2.test(text);
//	if (txtLen < 8 || txtLen > 20 || isAlpha == false || isNum == false) {
//		//alert("Not a valid Transaction Password");
//		return false;
//	}
//}
function onEditAccNumIB(txt) {
    if (txt == null) return false;
    var txtLen = txt.length;
    var i;
    var iphenText = "";
    var noniphendata = "";
    for (i = 0; i < txtLen; i++)
        if (txt[i] != "-") noniphendata += txt[i];
    for (i = 0; i < noniphendata.length; i++) {
        iphenText += noniphendata[i];
        if (i == 2 || i == 3 || i == 8) iphenText += '-';
    }
    frmIBActivateIBankingStep1.txtAccountNumber.text = iphenText;
    //gblPrevLen = currLen;
}

function onEditPassIdIB() {
    if (frmIBActivateIBankingStep1.txtIDPass.text.match(/[^0-9-]/g)) {
        //frmIBActivateIBankingStep1.txtIDPass.maxTextLength = "20";
        var formatstring = "";
        var currstring = frmIBActivateIBankingStep1.txtIDPass.text;
        for (i = 0; i < currstring.length; i++)
            if (currstring[i] != "-") formatstring += currstring[i];
        frmIBActivateIBankingStep1.txtIDPass.text = formatstring.toUpperCase();
    } else {
        //frmIBActivateIBankingStep1.txtIDPass.maxTextLength = "17";
        var iddata = frmIBActivateIBankingStep1.txtIDPass.text
        var iphenText = "";
        var noniphendata = "";
        for (i = 0; i < iddata.length; i++)
            if (iddata[i] != "-") noniphendata += iddata[i];
        iddata = noniphendata;
        for (i = 0; i < iddata.length; i++) {
            iphenText += iddata[i];
            if (i == 0 || i == 4 || i == 9 || i == 11) {
                iphenText += '-';
            }
        }
        frmIBActivateIBankingStep1.txtIDPass.text = iphenText;
    }
}

function accDoubleAddValidtnIB(text) {
    if (text == null) return false;
    var temp = "" + text;
    var txtLen = temp.length;
    var weight = [];
    var digit_account = [];
    var accountNum = [];
    for (i = 0; i < txtLen; i++) {
        accountNum[i] = 0;
    }
    i = 0;
    while (text) {
        accountNum[txtLen - 1 - i] = text % 10;
        text = (text - (text % 10)) / 10;
        i++;
    }
    if (txtLen != 10) {
        //alert("INVALID ACCOUNT NUMBER");
        return false;
    } else {
        weight[0] = [2]
        weight[1] = [1]
        weight[2] = [2]
        weight[3] = [1]
        weight[4] = [2]
        weight[5] = [1]
        weight[6] = [2]
        weight[7] = [1]
        weight[8] = [2]
        var length = 1
        var digit_multiply_result = 0
        var summary_multiply_result = 0
        var mod_result = 0
        var subtract_result = 0
        var position;
        for (position = 0; position < txtLen - 1; position++) {
            digit_account[position] = accountNum[position]
            digit_multiply_result = digit_account[position] * weight[position];
            if (digit_multiply_result > 9 && digit_multiply_result < 100) {
                var temp1 = digit_multiply_result % 10;
                var temp2 = (digit_multiply_result - digit_multiply_result % 10) / 10;
                digit_multiply_result = temp1 + temp2;
            }
            summary_multiply_result = summary_multiply_result + digit_multiply_result;
        }
        mod_result = summary_multiply_result % 10;
        if (mod_result != "0") {
            subtract_result = 10 - mod_result;
        } else {
            subtract_result = mod_result;
        }
        if (subtract_result != accountNum[9]) {
            return false;
        }
    }
    return true;
}
//New validation
function checkCitizenIDIB(text) {
    return checkCitizenID(text);
}

function onEditMobileNumberIB(txt) {
    var curr_form = frmIBCMChgMobNoTxnLimit;
    if (isOpenAccountKYCRelated()) {
        curr_form = frmIBOpenAccountContactKYC;
    }
    if (txt == null) return false;
    var numChars = txt.length;
    if (kony.string.isNumeric(txt.charAt(numChars - 1))) {
        //OK
    } else {
        curr_form.txtChangeMobileNumber.text = txt.substring(0, numChars - 1);
        txt.substring(0, numChars - 1);
        numChars = numChars - 1;
    }
    var temp = "";
    var i, txtLen = numChars;
    var currLen = numChars;
    if (gblPrevLen < currLen) {
        for (i = 0; i < numChars; ++i) {
            if (txt[i] != '-') {
                temp = temp + txt[i];
            } else {
                txtLen--;
            }
        }
        var iphenText = "";
        for (i = 0; i < txtLen; i++) {
            iphenText += temp[i];
            if (i == 2 || i == 5) {
                iphenText += '-';
            }
        }
        if (iphenText.length > 12) {
            iphenText = iphenText.substring(0, 12);
        }
        curr_form.txtChangeMobileNumber.text = iphenText;
    }
    gblPrevLen = currLen;
}
//function onEditCitiZenIDIB(txt) {
//	if (txt == null) return false;
//	var noChars = txt.length;
//	var temp = "";
//	var i, txtLen = noChars;
//	var currLen = noChars;
//	if (gblPrevLen < currLen) {
//		for (i = 0; i < noChars; ++i) {
//			if (txt[i] != '-') {
//				temp = temp + txt[i];
//			} else {
//				txtLen--;
//			}
//		}
//		var iphenText = "";
//		for (i = 0; i < txtLen; i++) {
//			iphenText += temp[i];
//			if (i == 0 || i == 4 || i == 9 || i == 11) {
//				iphenText += '-';
//			}
//		}
//		frmIBActivateIBankingStep1.txtIDPass.text = iphenText;
//	}
//	gblPrevLen = currLen;
//}
function PasprtValidatnIB(text) {
    if (text == null) return false;
    var txtLen = text.length;
    if (txtLen > 25) {
        return false;
    }
    return true;
}

function emailValidatnIB(txt) {
    if (txt == null) return false;
    var txtLen = txt.length;
    var i, j;
    var atIndex, count = 0;
    for (i = 0; i < txtLen; i++) {
        if (txt[i] == '@') {
            count++;
            atIndex = i;
        }
    }
    if (atIndex < 1) return false;
    //
    if (count != 1) {
        //alert("INVALID EMAIL , PLEASE RE-ENTER");
        return false;
    } else {
        if ((atIndex + 13) <= txtLen) {
            temp = txt.substring(atIndex + 1, atIndex + 13);
            if (temp == "facebook.com") {
                //alert("INVALID EMAIL , PLEASE RE-ENTER");
                return false;
            }
        }
        count = 0;
        for (i = atIndex; i < txtLen; i++) {
            if (txt[i] == '.') {
                //
                count++;
                var temp = "";
                if ((i + 3) > txtLen) {
                    //alert("INVALID EMAIL , PLEASE RE-ENTER");
                    return false;
                } else {
                    temp = txt[i + 1] + txt[i + 2];
                    var flag = kony.string.isAsciiAlphaNumeric(temp);
                    var flag1 = kony.string.isAsciiAlpha(temp);
                    var flag2 = kony.string.isNumeric(temp);
                    if (!(flag || flag1 || flag2)) {
                        //alert("");
                        return false;
                    }
                }
            }
        }
        if (count == 0) {
            //alert("INVALID EMAIL , PLEASE RE-ENTER");
            return false;
        }
    }
    return true
}

function accValidatnIB(text) {
    if (text == null) return false;
    var txtLen = text.length;
    var isNum = kony.string.isNumeric(text);
    if (txtLen != 10 || isNum == false) {
        //alert("Entered AccountNumber is not valid");
        return false;
    }
    return true;
}

function ibActivationValidatn() {
    var IBactiCode = kony.string.trim(frmIBActivateIBankingStep1.txtActivationCode.text);
    var actCodeFlag = ActvatnCodeValidatnIB(IBactiCode);
    var invalidActiCode1 = kony.i18n.getLocalizedString("invalidAccNo"); // showing common message based on defect DEF2865
    var info1 = kony.i18n.getLocalizedString("info");
    var okk = kony.i18n.getLocalizedString("keyOK");
    if (actCodeFlag == false) {
        if (!gblSetPwd) {
            showAlertIB(invalidActiCode1, info1);
            return false;
        }
    }
    var IBaccntnum = frmIBActivateIBankingStep1.txtAccountNumber.text;
    var invalidAccNo1 = kony.i18n.getLocalizedString("invalidAccNo");
    var IBaccntnumLen = IBaccntnum.length;
    for (i = 0; i < IBaccntnumLen; i++) {
        if (IBaccntnum[i] == '-') {
            IBaccntnum = IBaccntnum.replace("-", "");
        }
    }
    var accNumFlag1 = accDoubleAddValidtnIB(IBaccntnum);
    var accNumFlag2 = accValidatnIB(IBaccntnum);
    var IBpassID = frmIBActivateIBankingStep1.txtIDPass.text;
    var invalidID = kony.i18n.getLocalizedString("keyIBInvalidPass");
    var IBpassIDLen = IBpassID.length;
    for (i = 0; i < IBpassIDLen; i++) {
        if (IBpassID[i] == '-') {
            IBpassID = IBpassID.replace("-", "");
        }
    }
    var citizezIDFlag = checkCitizenIDIB(IBpassID);
    var passFlag = PasprtValidatnIB(IBpassID);
    var isCitizen = false;
    var isPassport = false;
    if (kony.string.isNumeric(IBpassID) && IBpassID.length == 13) {
        isCitizen = true;
        isPassport = false;
    } else {
        isCitizen = false;
        isPassport = true;
    }
    if (gblSetPwd && !accNumFlag2) {
        // remove validate account check digit on client site, 
        showAlertIB(invalidAccNo1, info1);
        return false;
    } else if (!gblSetPwd && (accNumFlag1 == false || accNumFlag2 == false)) {
        showAlertIB(invalidAccNo1, info1);
        return false;
        //	alert("INVALID ACCOUNT NUMBER");
    } else if ((isCitizen && !citizezIDFlag) || (isPassport && !passFlag)) {
        showAlertIB(invalidID, info1);
        //	alert("INVALID ID/Pass Number");
        return false;
    } else {
        if (gblSetPwd == true) {
            ibActivationConfirm("12", IBpassID, isCitizen, isPassport); //Reset Password flow
        } else {
            ibActivationConfirm("11", IBpassID, isCitizen, isPassport); //For activation of IB
        }
        // Function calls crmprofileenq service for activation flow
        //iBActivationConfirm (11,IBactiCode,IBaccntnum,"IB",1,IBpassID);
        //ibActivationConfirm("", IBpassID);
        //frmIBActivateIBankingStep1Confirm.show();
        //frmIBActivateIBankingStep1Confirm.lblAccNoVal.text = frmIBActivateIBankingStep1.txtAccountNumber.text;
        //frmIBActivateIBankingStep1Confirm.lblIDNoVal.text = frmIBActivateIBankingStep1.txtIDPass.text;
    }
}

function ibUseridPasswordValidation() {
    IBuserID = frmIBCreateUserID.txtUserID.text;
    if (!gblSetPwd) {
        if (IBuserID.indexOf(" ") > 0) {
            alert(kony.i18n.getLocalizedString("keyUserIdMinRequirement"));
            return false;
        }
    }
    var IBpassword = frmIBCreateUserID.txtPassword.text;
    if (!gblSetPwd) {
        if (IBpassword.indexOf(" ") > 0) {
            alert(kony.i18n.getLocalizedString("keyPasswordMinRequirement"));
            return false;
        }
        if (IBuserID.length <= 7 && IBuserID.length >= 21) {
            alert(kony.i18n.getLocalizedString("keyUserIdMinRequirement"));
            return false;
        }
        if (!IBuserID.match(/[a-z]/ig)) {
            alert(kony.i18n.getLocalizedString("keyUserIdMinRequirement"));
            return false;
        }
        if (kony.string.containsChars(IBuserID, ["<"]) && kony.string.containsChars(IBuserID, [">"])) {
            alert(kony.i18n.getLocalizedString("keyUserIdMinRequirement"));
            return false;
        }
        if (frmIBCreateUserID.txtUserID.text == frmIBCreateUserID.txtPassword.text) {
            alert(kony.i18n.getLocalizedString("sameUserPass"));
        }
    }
    if (IBpassword.length > 7 && IBpassword.match(/[a-z]/gi) != null && IBpassword.match(/[0-9]/g) != null)
        if (!(kony.string.containsChars(IBpassword, ["<"]) && kony.string.containsChars(IBpassword, [">"])))
            if (frmIBCreateUserID.txtPassword.text == frmIBCreateUserID.txtConfirmPassword.text)
                if (gblSetPwd == true) {
                    gblPassword = frmIBCreateUserID.txtPassword.text;
                    resetIBPassword();
                } else IBcreateUser();
    else alert(kony.i18n.getLocalizedString("PassNoMatch"));
    else alert(kony.i18n.getLocalizedString("keyPasswordMinRequirement"));
    else alert(kony.i18n.getLocalizedString("keyPasswordMinRequirement"));
}