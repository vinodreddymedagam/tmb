







/*
************************************************************************
            Name    : onClickNext()
            Author  : Kumar Abhisek
            Date    : April 05, 2013
            Purpose : To validate form level validations of Transaction 
            			Password .
        Input params: none
       Output params: 
        ServiceCalls: nil
        Called From : onClick of next
************************************************************************
 */

function onClickNext() {
	var lblText = kony.i18n.getLocalizedString("transPasswordSub");
	var refNo = "";
	var mobNO = "";
	showOTPPopup(lblText, refNo, mobNO, callBackConfirm, 3);
}

function callBackConfirm(tranPassword) {
	//var transFlag = trassactionPwdValidatn(tranPassword);
	//if (transFlag != false) {
		//alert("Wrong password");
		//frmApplyInternetBankingConfirmation.show();
	if(tranPassword.length > 0){
		invokePwdVerifyApplyServ(tranPassword);
	} else {
		setTransPwdFailedError(kony.i18n.getLocalizedString("emptyMBTransPwd"));
	}
}
/*
************************************************************************
            Name    : ApplyInternetBankingConfirmationPreshow()
            Author  : Kumar Abhisek
            Date    : April 05, 2013
            Purpose : PreShow for Header Of form frmApplyInternetBankingConfirmation
        Input params: Apply services Function
       Output params: 
        ServiceCalls: nil
        Called From :  PreShow of frmApplyInternetBankingConfirmation
************************************************************************
 
 */
/* REMOVED FOR MENUES
function ApplyInternetBankingConfirmationPreshow( // ) {
    var lblText = kony.i18n.getLocalizedString("Complete");
    getHeader(0, lblText, 0, 0, 0);
    //frmApplyInternetBankingConfirmation.lblDescriptionActivationCodeSent.text=""

}
*/

function applyInternetBanking() {
	var lblText = kony.i18n.getLocalizedString("InternetBanking");
	//frmApplyInternetBankingMB.show();
	//getHeader(0, lblText, 0, 0, 0);
	frmApplyInternetBankingMB.lblHead.text = lblText;
	frmApplyInternetBankingMB.lblNumberInformation.text = kony.i18n.getLocalizedString("InternetBankingDesc");
	//frmApplyInternetBankingMB.btnInternetBankingNext.onClick = onClickNext;
	frmApplyInternetBankingConfirmation.lblDescriptionActivationCodeSent.text = kony.i18n.getLocalizedString(
		"activationCodeSentInternet");
}

function applyMobileBankingSpa() {
	//var lblText = "Apply Mobile Banking";
	//frmApplyInternetBankingMB.show();
//	//getHeader(0, lblText, 0, 0, 0);
//	frmApplyMBSPA.lblHead.text = lblText;
//	frmApplyMBSPA.lblNumberInformation.text = "We will send information to this number";
//	//frmApplyInternetBankingMB.btnInternetBankingNext.onClick = onClickNext;
//	frmApplyMBConfirmationSPA.lblDescriptionActivationCodeSent.text = "Mobile banking Activation Code sent"

//Removed as part of ENH_207_7
}

function applyNewDevice() {
		var lblText = kony.i18n.getLocalizedString("AddNewDeviceIB");
	//frmApplyInternetBankingMB.show();
	//getHeader(0, lblText, 0, 0, 0);
	frmApplyInternetBankingMB.lblHead.text = lblText;
	frmApplyInternetBankingMB.lblNumberInformation.text = kony.i18n.getLocalizedString("addDeviceDesc");
	frmApplyInternetBankingConfirmation.lblDescriptionActivationCodeSent.text = kony.i18n.getLocalizedString(
		"activationCodeSentNewDevice");
	//frmApplyInternetBankingMB.btnInternetBankingNext.onClick = onClickNext;
}

function applyNewDeviceSpa() {
	var lblText = "Request add New Device";
	//frmApplyInternetBankingMB.show();
	//getHeader(0, lblText, 0, 0, 0);
	frmApplyMBSPA.lblHead.text = lblText;
	frmApplyMBSPA.lblNumberInformation.text = "We will send information to this number";
	frmApplyMBConfirmationSPA.lblDescriptionActivationCodeSent.text = kony.i18n.getLocalizedString(
		"keySPAActivationMessage");
	//frmApplyInternetBankingMB.btnInternetBankingNext.onClick = onClickNext;
}


function resetIBankingPass() {
	var lblText = kony.i18n.getLocalizedString("keyForgotPasswordHeader");
	//frmApplyInternetBankingMB.show();
	// getHeader(0, lblText, 0, 0, 0);
	frmApplyInternetBankingMB.lblHead.text = lblText;
	frmApplyInternetBankingMB.lblNumberInformation.text = kony.i18n.getLocalizedString("unlockIbankingDesc");
	//frmApplyInternetBankingMB.btnInternetBankingNext.onClick = onClickNext;
	frmApplyInternetBankingConfirmation.lblDescriptionActivationCodeSent.text = kony.i18n.getLocalizedString(
		"activationCodeSentUnlockIBanking");
}

function resetMBankingPassSpa() {
	var lblText = kony.i18n.getLocalizedString("ResetMBPwd");
	//frmApplyInternetBankingMB.show();
	// getHeader(0, lblText, 0, 0, 0);
	frmApplyMBSPA.lblHead.text = lblText;
	frmApplyMBSPA.lblNumberInformation.text = kony.i18n.getLocalizedString("wewillsendinfo");
	//frmApplyInternetBankingMB.btnInternetBankingNext.onClick = onClickNext;
	frmApplyMBConfirmationSPA.lblDescriptionActivationCodeSent.text = "Unlock Mobile Banking Activation code sent"
}
//function applyServices( /*services*/ ) {
//	var services = 1;
//	if (services == 1) {
//		applyInternetBanking();
//	} else if (services == 2) {
//		applyNewDevice()
//	} else if (services == 3) {
//		resetIBankingPass();
//	}
//}

function invokePwdVerifyApplyServ(password) {
	showLoadingScreen();
	var inputParam = {};
	inputParam["retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
    inputParam["retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
    inputParam["password"] = password;
   		var locale = kony.i18n.getCurrentLocale();
		if (locale == "en_US") {
			inputParam["language"] = "EN";
		} else {
			inputParam["language"] = "TH";
		}
		inputParam["activityTypeID"] = "001";
		inputParam["deviceId"] = GBL_UNIQ_ID;
		if (gblApplyServices == 2) {
			inputParam["activityFlexValues1"] = "";	
			inputParam["activityFlexValues2"] = "";
			inputParam["activityFlexValues3"] = "";		
			inputParam["activityFlexValues4"] = "";		
			inputParam["activityFlexValues5"] = "";	
		}
	invokeServiceSecureAsync("RequestGenActCodeApplyExecute", inputParam, callBackPwdVerifyApplyServ)
}
/**
 * description
 * @returns {}
 */

function callBackPwdVerifyApplyServ(status, resulttable) {
	if (status == 400) {
		
		if (resulttable["opstatusVPX"] == 0) {
			//frmApplyInternetBankingConfirmation.show();
			popupTractPwd.dismiss();
			callBackApplyreqGenActivationCode(resulttable);
		} else if (resulttable["errCode"] == "VrfyTxPWDErr00001" || resulttable["errCode"] == "VrfyTxPWDErr00002") {
			setTransPwdFailedError(kony.i18n.getLocalizedString("invalidTxnPwd"));
		}else if (resulttable["errCode"] == "VrfyTxPWDErr00003" || resulttable["code"] == "10403") {
			showTranPwdLockedPopup();
		}
			//tRANSACTION LOCKED-update in crm profile 
/*
			var deviceInfo = kony.os.deviceInfo();
			var deviceHght = deviceInfo["deviceHeight"];
			if(deviceHght>480){
				popTransferConfirmOTPLock.containerHeight = 50;
				popTransferConfirmOTPLock.containerHeightReference = constants.HEIGHT_BY_DEVICE_REFERENCE;
			}else{
				popTransferConfirmOTPLock.containerHeight = 62;
				popTransferConfirmOTPLock.containerHeightReference = constants.HEIGHT_BY_DEVICE_REFERENCE;
			}
*/				//invokeCRMProfileUpdate("19");
		else {
			alert(" " + resulttable["errMsg"]);
			dismissLoadingScreen();
		}
	}
}


function getTMBTouch(){
	frmGetTMBTouch.show();
}