gblRedirect=false;
function campaginService(widgetName,formName,appChannel){
		gblRedirect =false;
		var inputParams = {
 			widgetName:widgetName,
 			formName:formName,
 			appChannel:appChannel,
 			prelogin:""
	};
//	showLoadingScreenPopup();
    invokeServiceSecureAsync("GetCampaign", inputParams, campaignServiceCallBack);
}


//need to add accountNo, balance, customerName and customerSurName
gblCmpAccountNo = "";
gblCmpBalance = "";
gblCustomerName = "";
gblCustomerSurName = "";

gblCampaignURLEN = "";
gblCampaignURLTH = "";
gblCampaignResp = "";
var gblCampaignResponse;
var gblMessageIdMB;
//begin MIB-1988
var gblHtmlStringEN = "";
var gblHtmlStringTH = "";
//end MIB-1988

function campaignServiceCallBack(status,callbackResponse){
	 try{
		
		if (status == 400) {
			if (callbackResponse["opstatus"] == 0 &&  callbackResponse["errCode"]== "0" ) {
				gblRedirect=true;
			//	dismissLoadingScreenPopup();
				var locale = kony.i18n.getCurrentLocale();
				var formNameStr = "";
				if(callbackResponse["formName"]!=null){
					if(callbackResponse["formName"]=="frmMyBillerAddComplete" || callbackResponse["formName"]=="frmMyTopUpAddComplete"){
						formNameStr = "frmMyTopUpComplete";
					} else if(callbackResponse["formName"]=="frmTopupPaymentComplete"){
						formNameStr = "frmBillPaymentComplete";
					} else if(callbackResponse["formName"]=="frmEditFutureTopupComplete"){
						formNameStr = "frmEditFutureBillPaymentComplete";
					} else if(callbackResponse["formName"]=="frmIBOpenNewSavingsAccComplete_AllFree"){
						formNameStr = "frmIBOpenNewSavingsAccComplete";
					} else if(callbackResponse["formName"]=="frmOpenAccountNSConfirmation_AllFree"){
						formNameStr = "frmOpenAccountNSConfirmation";
					}
					else {
						formNameStr = callbackResponse["formName"];
					} 
				}
				var formname = eval(formNameStr);
				 
				var widgetname = callbackResponse["widgetName"];
				var appChannel = callbackResponse["clickChannel"];
					if ( callbackResponse["campaigns"].length > 0 ) {		
						/*Enhanchment no: MIB-1875*/					
						gblCampaignResponse = callbackResponse["campaigns"][0];
						
							if(appChannel=='01'){
									//gblCampaignTotalData +=callbackResponse["campaigns"][0]["cmpCode"]+","+callbackResponse["formName"]+","+"1"+","+"0"+","+"0"+","+"0"+"$"
									if (locale == "en_US") {
									   formname[widgetname]["src"] = callbackResponse["campaigns"][0]["imgBannerInternetEN"];
									} else {
										formname[widgetname]["src"] = callbackResponse["campaigns"][0]["imgBannerInternetTH"];
									}
									gblCampaginForm = callbackResponse["formName"] + "-"+ callbackResponse["widgetName"]+"-"+callbackResponse["campaigns"][0]["bannerFlag"];
									gblCampaignResp = callbackResponse["formName"] + "~" + callbackResponse["campaigns"][0]["cmpType"] + "~" + callbackResponse["campaigns"][0]["cmpCode"] + "~" + callbackResponse["campaigns"][0]["cmpEndDate"];
									gblCampaignImgEN = callbackResponse["campaigns"][0]["imgBannerInternetEN"];
									gblCampaignImgTH = callbackResponse["campaigns"][0]["imgBannerInternetTH"];
									gblCampaignFlag = callbackResponse["campaigns"][0]["bannerFlag"]+"-"+callbackResponse["campaigns"][0]["bannerLinkFlagInternet"]+"-"+appChannel+"-"+callbackResponse["campaigns"][0]["cmpCode"];
									var internetLink = callbackResponse["campaigns"][0]["bannerLinkFlagInternet"];
									
									if(formname[widgetname].containerWeight == 0)
									{
										if(gblrefcontweight == "")
										{
											gblrefcontweight = 100;
										}
										formname[widgetname].containerWeight = gblrefcontweight;
										formname[widgetname].referenceWidth = gblrefcontwidth;
										formname[widgetname].referenceHeight = gblcontheight;
									}
									
									if(widgetname=='imgTwo'){
										formname["hbxAdv"]["isVisible"]=false;
										formname["hbxCommon"]["isVisible"]=true;
										formname["hbxCommon"]["onClick"]=getCampaignResult;
										var formHoxId = formNameStr+"_"+"hbxCommon";
										if(internetLink != undefined && internetLink != "00"){
											document.getElementById(formHoxId).style.cursor="pointer";
										} else {
											document.getElementById(formHoxId).style.cursor="default";
										}
									} else {
										formname["hbxAdv"]["isVisible"]=true;
										if(formNameStr=='frmIBOpenNewSavingsAccComplete'){
											formname["hbxCommon"]["isVisible"]=false;
										} 
										
										formname["hbxAdv"]["onClick"]=getCampaignResult;
										var formHoxId = formNameStr+"_"+"hbxAdv";
										if(internetLink != undefined && internetLink != "00"){
											document.getElementById(formHoxId).style.cursor="pointer";
										} else {
											document.getElementById(formHoxId).style.cursor="default";
										}
									}
									if(callbackResponse["campaigns"][0]["bannerFlag"]=='P'){
												//Setting the links
												if(gblCampaignResp.split("~")[1] != 'M'){
													campaignBannerIBCount("D","B");
												}
												gblCampaignDataEN=callbackResponse["campaigns"][0]["bannerLinkInternetEN"];
												gblCampaignDataTH=callbackResponse["campaigns"][0]["bannerLinkInternetTH"];
												
												//code for personalised banner
												var testBrowserCompo="";
												var vbxCompo=""
												vbxCompo=gblVbxCmp;
												testBrowserCompo=gblBrwCmpObject;
												var Finalstr="";
					                            
												var camphtmlStringEN = callbackResponse["campaigns"][0]["htmlContentEn"];
												var camphtmlStringTH = callbackResponse["campaigns"][0]["htmlContentTh"];
												//var camphtmlStringEN="<!doctype html><html><head><title>Untitled Document</title><style type=\"text/css\">.bannerBody { margin:0; padding:0px; font-size:14px; }#wrapperBanner { position:relative; display:block;	}.bannerTextbox .tbl { display:table; width:80%; height:100%; margin:0 0 0 3%; font-size:12px;}.bannerTextbox .tblRow { display:table-row; height:33.3%; width:100%; }.bannerTextbox .tblRow span { display:table-cell; width:31%; vertical-align:middle; color:#000; margin: 0 1%; }.bannerImg { width:100%; }.bannerTextbox { display:block;	position:absolute; left:0px; top:0px; width:100%; height:100%; text-align:left; font-size:80%; }.bannerTextbox .tblRow span:nth-child(1) { text-align:left; }.bannerTextbox .tblRow span:nth-child(2) { text-align:center; }.bannerTextbox .tblRow span:nth-child(3) { text-align:right; }</style></head><body class=\"bannerBody\"><a id=\"wrapperBanner\"><img src=\"https://media.tmbbank.com/cpm/img/441_banner_internet_bg_en.png\" class=\"bannerImg\" alt=\"\"><span class=\"bannerTextbox\">	<span class=\"tbl\">	<span class=\"tblRow\">		<span>Dear John Smith,</span>		<span></span>		<span></span>	</span>	<span class=\"tblRow\">		<span></span>		<span>Account no: 1234567890</span>		<span></span>	</span>	<span class=\"tblRow\">		<span></span>		<span></span>		<span>Balnace: 12,345.67</span>	</span>	</span></span></a></body></html>";
                            					//var camphtmlStringTH="<!doctype html><html><head><title>Untitled Document</title><style type=\"text/css\">.bannerBody { margin:0; padding:0px; font-size:14px; }#wrapperBanner { position:relative; display:block;	}.bannerTextbox .tbl { display:table; width:80%; height:100%; margin:0 0 0 3%; font-size:12px;}.bannerTextbox .tblRow { display:table-row; height:33.3%; width:100%; }.bannerTextbox .tblRow span { display:table-cell; width:31%; vertical-align:middle; color:#000; margin: 0 1%; }.bannerImg { width:100%; }.bannerTextbox { display:block;	position:absolute; left:0px; top:0px; width:100%; height:100%; text-align:left; font-size:80%; }.bannerTextbox .tblRow span:nth-child(1) { text-align:left; }.bannerTextbox .tblRow span:nth-child(2) { text-align:center; }.bannerTextbox .tblRow span:nth-child(3) { text-align:right; }</style></head><body class=\"bannerBody\"><a id=\"wrapperBanner\"><img src=\"https://media.tmbbank.com/cpm/img/441_banner_internet_bg_en.png\" class=\"bannerImg\" alt=\"\"><span class=\"bannerTextbox\">	<span class=\"tbl\">	<span class=\"tblRow\">		<span>Dear John Smith,</span>		<span></span>		<span></span>	</span>	<span class=\"tblRow\">		<span></span>		<span>Account no: 1234567890</span>		<span></span>	</span>	<span class=\"tblRow\">		<span></span>		<span></span>		<span>Balnace: 12,345.67</span>	</span>	</span></span></a></body></html>";
												
													//begin MIB-1988
													gblHtmlStringEN = camphtmlStringEN.replace("<a id=\"wrapperBanner\">","<a id=\"wrapperBanner\" onClick=\"getCampaignResult()\">");
													gblHtmlStringTH = camphtmlStringTH.replace("<a id=\"wrapperBanner\">","<a id=\"wrapperBanner\" onClick=\"getCampaignResult()\">");
													if (locale == "en_US") {
														testBrowserCompo["htmlString"]=gblHtmlStringEN;
													}else{
														testBrowserCompo["htmlString"]=gblHtmlStringTH;
													}
													/*
													if (locale == "en_US") 
													{
													camphtmlStringEN=camphtmlStringEN.replace("<a id=\"wrapperBanner\">","<a id=\"wrapperBanner\" onClick=\"getCampaignResult()\">");
													testBrowserCompo["htmlString"]=camphtmlStringEN;
													}
													else
													{
													camphtmlStringTH=camphtmlStringTH.replace("<a id=\"wrapperBanner\">","<a id=\"wrapperBanner\" onClick=\"getCampaignResult()\">");
													testBrowserCompo["htmlString"]=camphtmlStringTH;
													}
												*/
												//end MIB-1988
												
												gblrefcontweight = formname[widgetname].containerWeight;
												gblrefcontwidth = formname[widgetname].referenceWidth;
												gblcontheight = formname[widgetname].referenceHeight;
																	
												formname[widgetname].containerWeight=0;
												formname[widgetname].referenceWidth = 1;
					                            formname[widgetname].referenceHeight = 1;
												
												vbxCompo.remove(testBrowserCompo)
												formname.hbxAdv.remove(vbxCompo);
												vbxCompo.add(testBrowserCompo);
					                            formname.hbxAdv.add(vbxCompo);
												formname[vbxCompo].containerWeight=50;
					                            formname["hbxAdv"].setVisibility(true);												
									
											
									} else if(callbackResponse["campaigns"][0]["bannerFlag"]=='Y'){
											if(callbackResponse["campaigns"][0]["bannerLinkFlagInternet"]=='01' || callbackResponse["campaigns"][0]["bannerLinkFlagInternet"]=='02'){
													gblCampaignDataEN = callbackResponse["campaigns"][0]["bannerLinkInternetEN"];
													gblCampaignDataTH = callbackResponse["campaigns"][0]["bannerLinkInternetTH"];
											}
											try{
												//alert("Test");
												formname.gblVbxCmp.remove(gblBrwCmpObject);	
												formname.hbxAdv.remove(gblVbxCmp);												
											}catch(e){
												
											}
									} 
									
									
									if(gblCampaignResp.split("~")[1] != 'M'&& callbackResponse["campaigns"][0]["bannerFlag"]!='P'){
										campaignBannerIBCount("D","B");
									}
									//formname["hbxAdv"]["isVisible"]=true;
									//formname["hbxAdv"]["onClick"]=getCampaignResult;	
							
								} else {
									kony.print("***** Inside mobile code");
									//gblCampaignTotalData +=callbackResponse["campaigns"][0]["cmpCode"]+","+callbackResponse["formName"]+","+"1"+","+"0"+","+"0"+","+"0"+"$"
									gblCampaginForm = callbackResponse["formName"] + "-"+ callbackResponse["widgetName"]+"-"+callbackResponse["campaigns"][0]["bannerFlag"];
									gblCampaignResp = callbackResponse["formName"] + "~" + callbackResponse["campaigns"][0]["cmpType"] + "~" + callbackResponse["campaigns"][0]["cmpCode"] + "~" + callbackResponse["campaigns"][0]["cmpEndDate"];
									gblCampaignImgEN = callbackResponse["campaigns"][0]["imgBannerMobEN"];
									gblCampaignImgTH = callbackResponse["campaigns"][0]["imgBannerMobTH"];
									gblCampaignFlag = callbackResponse["campaigns"][0]["bannerFlag"]+"-"+callbackResponse["campaigns"][0]["bannerLinkFlagMob"]+"-"+appChannel+"-"+callbackResponse["campaigns"][0]["cmpCode"];
									
									//commentting below line to fix DEF1742 - banner are not displaying, printing gblrefcontweight value is causing the issue.
									//kony.print("Hi"+formname[widgetname].containerWeight);
									//kony.print("HI browser" + gblBrwCmpObject.containerHeight)
									//kony.print("gblrefcontweight :"+gblrefcontweight+"gblrefcontwidth :"+gblrefcontwidth+" gblcontheight:"+gblcontheight);
									if(formname[widgetname].containerWeight==0)
									{
										//alert("here"+callbackResponse["campaigns"][0]["bannerFlag"])
										if(gblrefcontweight=="")
										{
											gblrefcontweight=100;
										}
										formname[widgetname].containerWeight=gblrefcontweight;
										formname[widgetname].referenceWidth=gblrefcontwidth;
										formname[widgetname].referenceHeight=gblcontheight;
									}
									
									if(widgetname=='imgTwo'){
										formname["hbxAdv"]["isVisible"]=false;
										formname["hbxCommon"]["isVisible"]=true;
										formname["hbxCommon"]["onClick"]=getCampaignResult;
										//gblVbxCmp.remove(gblBrwCmpObject);
                                		//formname.hbxCommon.remove(gblVbxCmp);
									} else {									 
										formname["hbxAdv"]["isVisible"]=true;
										if(callbackResponse["formName"]=='frmOpenAccountNSConfirmation' || callbackResponse["formName"]=='frmMyTopUpComplete'  || callbackResponse["formName"]=='frmEditFutureBillPaymentComplete' || callbackResponse["formName"]=='frmBillPaymentComplete' || callbackResponse["formName"]=="frmMyBillerAddComplete" || callbackResponse["formName"]=="frmMyTopUpAddComplete"){
											formname["hbxCommon"]["isVisible"]=false;
										}
										//if cmp internal link from activation complete screen, then call all the services
										//which are calling on click of start button.
										//alert("form Name : "+callbackResponse["formName"]);
										if(callbackResponse["formName"] == 'frmMBActiComplete' && '01' == gblCampaignFlag.split("-")[1]){
											
											//alert("isCmpFlowFrmActivation : "+isCmpFlowFrmActivation);
											formname["hbxAdv"]["onClick"]=onClickOfStartBtnForMBSnippetForCmp;
										}else {
											formname["hbxAdv"]["onClick"]=getCampaignResult;
										}  
										//formname["hbxAdv"]["onClick"]=getCampaignResult;
										//gblVbxCmp.remove(gblBrwCmpObject)
                                		//formname.hbxAdv.remove(gblVbxCmp);
									}
			 						
			 						var refWidthAndHeight = getBannerRefWidthAndHeight();
			 						if("0-0" != refWidthAndHeight){
			 							formname[widgetname]["referenceWidth"] =  parseInt(refWidthAndHeight.split("-")[0]);
			 							formname[widgetname]["referenceHeight"] = parseInt(refWidthAndHeight.split("-")[1]);
			 						}			 						
									
									if (locale == "en_US") {
									    formname[widgetname]["src"] = callbackResponse["campaigns"][0]["imgBannerMobEN"];
									} else {
										formname[widgetname]["src"] = callbackResponse["campaigns"][0]["imgBannerMobTH"];
									}
									
									if(callbackResponse["campaigns"][0]["bannerFlag"]=='P'){
														
												if(gblCampaignResp.split("~")[1] != 'M'){
													campaignBannerCount("D","B");
												}
												if (callbackResponse["campaigns"][0]["bannerLinkFlagMob"]=="02")
												{
														gblCampaignDataEN=callbackResponse["campaigns"][0]["bannerExternalLinkMobEN"]
														gblCampaignDataTH=callbackResponse["campaigns"][0]["bannerExternalLinkMobTH"]
												}
												else
												{
														gblCampaignDataEN=callbackResponse["campaigns"][0]["bannerInternalLinkMobEN"]
														gblCampaignDataTH=callbackResponse["campaigns"][0]["bannerInternalLinkMobTH"]
												}
												// Code for personalised banner
												
												if(gblDeviceInfo["name"] == "iPhone")
												{
													gblBrwCmpObject.containerHeight= 22;
													gblBrwCmpObject.containerHeightReference=constants.CONTAINER_HEIGHT_BY_FORM_REFERENCE;
												}
												
												
												var Finalstr="";
					                            
												var camphtmlStringEN = callbackResponse["campaigns"][0]["htmlContentEn"];
												var camphtmlStringTH = callbackResponse["campaigns"][0]["htmlContentTh"];
												//var camphtmlStringEN="<!doctype html><html><head><title>Untitled Document</title><style type=\"text/css\">.bannerBody { margin:0; padding:0px; font-size:14px; }#wrapperBanner { position:relative; display:block;	}.bannerTextbox .tbl { display:table; width:80%; height:100%; margin:0 0 0 3%; font-size:12px;}.bannerTextbox .tblRow { display:table-row; height:33.3%; width:100%; }.bannerTextbox .tblRow span { display:table-cell; width:31%; vertical-align:middle; color:#000; margin: 0 1%; }.bannerImg { width:100%; }.bannerTextbox { display:block;	position:absolute; left:0px; top:0px; width:100%; height:100%; text-align:left; font-size:80%; }.bannerTextbox .tblRow span:nth-child(1) { text-align:left; }.bannerTextbox .tblRow span:nth-child(2) { text-align:center; }.bannerTextbox .tblRow span:nth-child(3) { text-align:right; }</style></head><body class=\"bannerBody\"><a id=\"wrapperBanner\"><img src=\"https://media.tmbbank.com/cpm/img/441_banner_internet_bg_en.png\" class=\"bannerImg\" alt=\"\"><span class=\"bannerTextbox\">	<span class=\"tbl\">	<span class=\"tblRow\">		<span>Dear John Smith,</span>		<span></span>		<span></span>	</span>	<span class=\"tblRow\">		<span></span>		<span>Account no: 1234567890</span>		<span></span>	</span>	<span class=\"tblRow\">		<span></span>		<span></span>		<span>Balnace: 12,345.67</span>	</span>	</span></span></a></body></html>";
                            					//var camphtmlStringTH="<!doctype html><html><head><title>Untitled Document</title><style type=\"text/css\">.bannerBody { margin:0; padding:0px; font-size:14px; }#wrapperBanner { position:relative; display:block;	}.bannerTextbox .tbl { display:table; width:80%; height:100%; margin:0 0 0 3%; font-size:12px;}.bannerTextbox .tblRow { display:table-row; height:33.3%; width:100%; }.bannerTextbox .tblRow span { display:table-cell; width:31%; vertical-align:middle; color:#000; margin: 0 1%; }.bannerImg { width:100%; }.bannerTextbox { display:block;	position:absolute; left:0px; top:0px; width:100%; height:100%; text-align:left; font-size:80%; }.bannerTextbox .tblRow span:nth-child(1) { text-align:left; }.bannerTextbox .tblRow span:nth-child(2) { text-align:center; }.bannerTextbox .tblRow span:nth-child(3) { text-align:right; }</style></head><body class=\"bannerBody\"><a id=\"wrapperBanner\"><img src=\"https://media.tmbbank.com/cpm/img/441_banner_internet_bg_en.png\" class=\"bannerImg\" alt=\"\"><span class=\"bannerTextbox\">	<span class=\"tbl\">	<span class=\"tblRow\">		<span>Dear John Smith,</span>		<span></span>		<span></span>	</span>	<span class=\"tblRow\">		<span></span>		<span>Account no: 1234567890</span>		<span></span>	</span>	<span class=\"tblRow\">		<span></span>		<span></span>		<span>Balnace: 12,345.67</span>	</span>	</span></span></a></body></html>";
												if (flowSpa) {
													//begin MIB-1988
													gblHtmlStringEN = camphtmlStringEN.replace("<a id=\"wrapperBanner\">","<a id=\"wrapperBanner\" onClick=\"getCampaignResult()\">");
													gblHtmlStringTH = camphtmlStringTH.replace("<a id=\"wrapperBanner\">","<a id=\"wrapperBanner\" onClick=\"getCampaignResult()\">");
													if (locale == "en_US"){
														gblBrwCmpObject["htmlString"]=gblHtmlStringEN;
													}else{
														gblBrwCmpObject["htmlString"]=gblHtmlStringTH;
													}
													/*
													if (locale == "en_US") 
													{
														camphtmlStringEN=camphtmlStringEN.replace("<a id=\"wrapperBanner\">","<a id=\"wrapperBanner\" onClick=\"getCampaignResult()\">");
														gblBrwCmpObject["htmlString"]=camphtmlStringEN;
													}
													else
													{
														camphtmlStringTH=camphtmlStringTH.replace("<a id=\"wrapperBanner\">","<a id=\"wrapperBanner\" onClick=\"getCampaignResult()\">");
														gblBrwCmpObject["htmlString"]=camphtmlStringTH;
													}
													*/
													//end MIB-1988
												}
												else
												{
													//begin MIB-1988
													if(gblDeviceInfo["name"] == "iPhone"){
														gblHtmlStringEN = camphtmlStringEN.replace("<a id=\"wrapperBanner\">","<a id=\"wrapperBanner\" href=\"\">");
														gblHtmlStringTH = camphtmlStringTH.replace("<a id=\"wrapperBanner\">","<a id=\"wrapperBanner\" href=\"\">");
														gblBrwCmpObject.clearHistory();
														if (locale == "en_US"){
															gblBrwCmpObject["htmlString"]=gblHtmlStringEN;
														}else{
															gblBrwCmpObject["htmlString"]=gblHtmlStringTH;
														}
														gblBrwCmpObject["handleRequest"]="";
														gblBrwCmpObject["handleRequest"]=setExternalLinks;
													}else{
														gblHtmlStringEN = camphtmlStringEN.replace("<a id=\"wrapperBanner\">","<a id=\"wrapperBanner\" href=\"http://tmbbank.com\">");
														gblHtmlStringTH = camphtmlStringTH.replace("<a id=\"wrapperBanner\">","<a id=\"wrapperBanner\" href=\"http://tmbbank.com\">");
														gblBrwCmpObject.clearHistory();
														if (locale == "en_US"){
															gblBrwCmpObject["htmlString"]=gblHtmlStringEN;
														}else{
															gblBrwCmpObject["htmlString"]=gblHtmlStringTH;
														}
														gblBrwCmpObject["handleRequest"]="";
														gblBrwCmpObject["handleRequest"]=getCampaignResult;
													}
													/*
													if (locale == "en_US") 
													{
														if(gblDeviceInfo["name"] == "iPhone")
														{
															camphtmlStringEN=camphtmlStringEN.replace("<a id=\"wrapperBanner\">","<a id=\"wrapperBanner\" href=\"\">");
															gblBrwCmpObject.clearHistory();
															gblBrwCmpObject["htmlString"]=camphtmlStringEN;
															gblBrwCmpObject["handleRequest"]="";
															gblBrwCmpObject["handleRequest"]=setExternalLinks;
														}
														else
														{
															camphtmlStringEN=camphtmlStringEN.replace("<a id=\"wrapperBanner\">","<a id=\"wrapperBanner\" href=\"http://tmbbank.com\">");
															gblBrwCmpObject.clearHistory();
															gblBrwCmpObject["htmlString"]=camphtmlStringEN;
															gblBrwCmpObject["handleRequest"]="";
															gblBrwCmpObject["handleRequest"]=getCampaignResult;
														}
														
													}
													else
													{
														if(gblDeviceInfo["name"] == "iPhone")
														{
															camphtmlStringTH=camphtmlStringTH.replace("<a id=\"wrapperBanner\">","<a id=\"wrapperBanner\" href=\"\">");
															gblBrwCmpObject.clearHistory();
															gblBrwCmpObject["htmlString"]=camphtmlStringTH;
															gblBrwCmpObject["handleRequest"]="";
															gblBrwCmpObject["handleRequest"]=setExternalLinks;
														}
														else
														{
															camphtmlStringTH=camphtmlStringTH.replace("<a id=\"wrapperBanner\">","<a id=\"wrapperBanner\" href=\"http://tmbbank.com\">");
															gblBrwCmpObject.clearHistory();
															gblBrwCmpObject["htmlString"]=camphtmlStringTH;
															gblBrwCmpObject["handleRequest"]="";
															gblBrwCmpObject["handleRequest"]=getCampaignResult;
														}
														
													}
													*/
													//end MIB-1988
													
												}
												
												gblrefcontweight=formname[widgetname].containerWeight;
												gblrefcontwidth=formname[widgetname].referenceWidth;
												gblcontheight=formname[widgetname].referenceHeight;
												
												formname[widgetname].containerWeight=0;
												formname[widgetname].referenceWidth = 1;
					                            formname[widgetname].referenceHeight = 1;
												if(widgetname=='imgTwo')
												{
													gblVbxCmp.add(gblBrwCmpObject);
						                            formname.hbxCommon.add(gblVbxCmp);
													formname["hbxCommon"].setVisibility(true);												
												}
												else
												{
													gblVbxCmp.add(gblBrwCmpObject);
						                            formname.hbxAdv.add(gblVbxCmp);
						                            formname["hbxAdv"].setVisibility(true);
												}
												kony.print("***** Done executing campaign");
											
									} else if(callbackResponse["campaigns"][0]["bannerFlag"]=='Y'){
											if(callbackResponse["campaigns"][0]["bannerLinkFlagMob"]=='01' ){
												gblCampaignDataEN = callbackResponse["campaigns"][0]["bannerInternalLinkMobEN"];
												gblCampaignDataTH = callbackResponse["campaigns"][0]["bannerInternalLinkMobTH"];
												gblCampaignURLEN = callbackResponse["campaigns"][0]["bannerExternalLinkMobEN"];
    											gblCampaignURLTH = callbackResponse["campaigns"][0]["bannerExternalLinkMobTH"];
											} else if( callbackResponse["campaigns"][0]["bannerLinkFlagMob"]=='02' ){
												gblCampaignDataEN = callbackResponse["campaigns"][0]["bannerExternalLinkMobEN"];
												gblCampaignDataTH = callbackResponse["campaigns"][0]["bannerExternalLinkMobTH"];
											}else if(callbackResponse["campaigns"][0]["bannerLinkFlagMob"]=='03' ){
												if(callbackResponse["campaigns"][0]["bannerInternalLinkMobEN"]!=undefined && callbackResponse["campaigns"][0]["bannerInternalLinkMobEN"]!=null && callbackResponse["campaigns"][0]["bannerInternalLinkMobEN"]!=""){
													gblCampaignDataEN = callbackResponse["campaigns"][0]["bannerInternalLinkMobEN"];
													gblCampaignDataTH = callbackResponse["campaigns"][0]["bannerInternalLinkMobTH"];
													gblCampaignURLEN = callbackResponse["campaigns"][0]["bannerExternalLinkMobEN"];
													gblCampaignURLTH = callbackResponse["campaigns"][0]["bannerExternalLinkMobTH"];
												}
												else {
													gblCampaignDataEN = callbackResponse["campaigns"][0]["bannerExternalLinkMobEN"];
													gblCampaignDataTH = callbackResponse["campaigns"][0]["bannerExternalLinkMobTH"];
												}
											}
											
											//begin MIB-3383
											//if(gblDeviceInfo["name"] == "iPhone")
                                            //  {
												try{
                                                 kony.print("Browser: New Code for Iphone BroswerCompoent Object history clear"); 
                                                 gblBrwCmpObject.clearHistory();
                                                 gblBrwCmpObject["handleRequest"]="";
                                                 gblBrwCmpObject["handleRequest"]="";
                                                 formname.gblVbxCmp.remove(gblBrwCmpObject);
                                                 formname.hbxAdv.remove(gblVbxCmp);
                                                 }catch(err){
                                                 }
                                            // }

											//end MIB-3383
									} 
									
									if(gblCampaignResp.split("~")[1] != 'M' && callbackResponse["campaigns"][0]["bannerFlag"]!='P'){
										campaignBannerCount("D","B");
									}
									
									adjustBannerMB(formname);
									
							}
					
					} else {
						//dismissLoadingScreenPopup();
						formname["hbxAdv"]["isVisible"]=false;
						if(widgetname=='imgTwo'&& (callbackResponse["formName"]=='frmOpenAccountNSConfirmation' || callbackResponse["formName"]=='frmMyTopUpComplete' || callbackResponse["formName"]=='frmEditFutureBillPaymentComplete' || callbackResponse["formName"]=='frmIBOpenNewSavingsAccComplete' ||  callbackResponse["formName"]=='frmBillPaymentComplete' || callbackResponse["formName"]=='frmTopupPaymentComplete' || callbackResponse["formName"]=='frmEditFutureTopupComplete')){
							formname["hbxCommon"]["isVisible"]=false;
						}
					}
				} else {
					var formValue = kony.application.getCurrentForm();
					formValue["hbxAdv"]["isVisible"]=false;
					if(kony.application.getCurrentForm().id =='frmOpenAccountNSConfirmation' || kony.application.getCurrentForm().id =='frmMyTopUpComplete' || kony.application.getCurrentForm().id=='frmEditFutureBillPaymentComplete' || kony.application.getCurrentForm().id=='frmIBOpenNewSavingsAccComplete' ||  kony.application.getCurrentForm().id=='frmBillPaymentComplete'){
							formValue["hbxCommon"]["isVisible"]=false;
					}
				}
			}
			
		}catch(e){
		//
		}
}

function setExternalLinks(){
	if(gblRedirect)
	{
		gblRedirect=false;
	}
	else
	{
		getCampaignResult();
	}
}

function getBannerRefWidthAndHeight(){
	
	var deviceName = gblDeviceInfo["name"];
	var screenwidth = gblDeviceInfo["deviceWidth"];
	if (flowSpa) {
		////  for SPA dimension	
           var refWidth = 300;
	       var refHeight = 120;	
		if (screenwidth == 414) {
			refWidth = 388;
			refHeight = 156;			
		} else if (screenwidth == 375) {
			refWidth = 353;
			refHeight = 142;
		} else if (screenwidth == 320) {
			refWidth = 300;
			refHeight = 120;
		} else if (screenwidth == 360) {
			refWidth = 300;
			refHeight = 120;
		} else if (screenwidth == 384) {
			refWidth = 360;
			refHeight = 144;
		}
	} else if(deviceName == "iPhone" || deviceName == "android" ){
	        var refWidth = 300;
	       var refHeight = 120;	
		///for  RC bannaer Dimensions
		if (deviceName == "iPhone") {
			var deviceModel = gblDeviceInfo["model"];
			// alert("Model : "+gblDeviceInfo["model"]);
			if (deviceModel == "iPhone 6 Plus") {
				// alert("this is from iphone6plus");
				refWidth = 388;
				refHeight = 155;
			} else if (deviceModel == "iPhone 6") {
				// alert("this is from iphone6");
				refWidth = 352;
				refHeight = 141;
			}
		} else if (deviceName == "android") {
			// alert("Model : "+gblDeviceInfo["model"]);
			if (screenwidth == 1080) {
				// alert("this is from nesux5"+screenwidth);
				refWidth = 1012;
				refHeight = 136;
			} else if (screenwidth == 768) {
				// alert("this is from Nexus"+screenwidth);
				refWidth = 720;
				refHeight = 144;
			}else if(screenwidth == 720){
			
			    refWidth =  675;
				refHeight = 144;
			}
		}
	}else {
	  var refWidth = 320;
	  var refHeight = 120;
	
	}
	return refWidth + "-" + refHeight;
} 
  
function ChangeCampaignLocale(){
	 	var locale = kony.i18n.getCurrentLocale();
	 	if(gblCampaginForm!=null && gblCampaginForm.trim()!="") {
	 		var formname = gblCampaginForm.split("-")[0]; //eval(gblCampaginForm.split("-")[0]);
			var widgetname = gblCampaginForm.split("-")[1];
		 	if(formname == "frmMyBillerAddComplete" || formname == "frmMyTopUpAddComplete"){
				formname = "frmMyTopUpComplete";
			} else if(formname == "frmTopupPaymentComplete"){
				formname = "frmBillPaymentComplete";
			} else if(formname == "frmEditFutureTopupComplete"){
				formname = "frmEditFutureBillPaymentComplete";
			} else if(formname == "frmIBOpenNewSavingsAccComplete_AllFree"){
				formname = "frmIBOpenNewSavingsAccComplete";
			} else if(formname == "frmOpenAccountNSConfirmation_AllFree"){
				formname = "frmOpenAccountNSConfirmation";
			}
			var formname_str = formname;
			kony.print("====formname_str = "+formname_str);
			formname = eval(formname);
			
			var refWidthAndHeight = getBannerRefWidthAndHeight();
			if("0-0" != refWidthAndHeight){
				formname[widgetname]["referenceWidth"] =  parseInt(refWidthAndHeight.split("-")[0]);
				formname[widgetname]["referenceHeight"] = parseInt(refWidthAndHeight.split("-")[1]);
			}
		 	//begin MIB-1988
			//gblCampaginForm assign from campaignServiceCallBack
			var bannerFlag = gblCampaginForm.split("-")[2];
			//for IB & MB
			kony.print("====>widgetname = "+widgetname);
			kony.print("====>bannerFlag = "+bannerFlag);
			kony.print("====>gblHtmlStringEN = "+gblHtmlStringEN);
			kony.print("====>gblHtmlStringTH = "+gblHtmlStringTH);
			kony.print("====>gblCampaignImgEN = "+gblCampaignImgEN);
			kony.print("====>gblCampaignImgTH = "+gblCampaignImgTH);
		 	if (bannerFlag == 'P') {
				try {
					//gblBrwCmpObject.clearHistory();
					gblRedirect = true;
					if (locale == "en_US") {
						gblBrwCmpObject["htmlString"] = gblHtmlStringEN;
				 	} else {
				 		gblBrwCmpObject["htmlString"] = gblHtmlStringTH;
				 	}
			 	}catch(e) {
			 		kony.print("====>e.message = "+e.message);
			 	}
			}else if (bannerFlag == 'Y') {
				if (locale == "en_US") {
			 		formname[widgetname]["src"] = gblCampaignImgEN;
			 	} else {
			 		formname[widgetname]["src"] = gblCampaignImgTH;
			 	}
			}
			/*
			if (locale == "en_US") {
		 		formname[widgetname]["src"] = gblCampaignImgEN;
		 	} else{
		 		formname[widgetname]["src"] = gblCampaignImgTH;
		 	}*/
			//end MIB-1988
		 	if(widgetname=='imgTwo' && (gblCampaginForm.split("-")[0]=='frmOpenAccountNSConfirmation' || gblCampaginForm.split("-")[0]=='frmMyTopUpComplete' || gblCampaginForm.split("-")[0]=='frmEditFutureBillPaymentComplete' || gblCampaginForm.split("-")[0]=='frmIBOpenNewSavingsAccComplete' || gblCampaginForm.split("-")[0]=='frmBillPaymentComplete')){
		 		formname["hbxCommon"]["onClick"]=getCampaignResult;
		 	} else {
		 		formname["hbxAdv"]["onClick"]=getCampaignResult;
		 	}
		 	
		 	adjustBannerMB(formname_str);
	 	}
}

/**
 *  Method setCampaignFlowForProdDetails() is added in set the isCmpFlow to 
 * true when product code is valid or not.
 */

function setCampaignFlowForProdDetails(){
	//var allAccProdCodes = gblOpenAcctNormalSavings + gblOpenAcctSavingsCare + gblOpenAcctTermDeposit+gblOpenAcctDreamSaving+gblForUserProdCodes;
	gblProdCode = "";
	if(undefined != gblCampaignDataEN && undefined != gblCampaignDataTH){
		if(gblCampaignDataEN != "" && gblCampaignDataEN.split("?").length > 1){
		//if(gblCampaignDataEN.startsWith("frmIBOpenActSelProd") || gblCampaignDataEN.startsWith("frmOpenActSelProd")){
			var tempCampaignDataEN = gblCampaignDataEN.split("?")[0];
			gblProdCode = gblCampaignDataEN.split("?")[1];
			//gblCampaignDataEN = tempCampaignDataEN;
			//if(gblProdCode != "" && allAccProdCodes.indexOf(gblProdCode) != -1){
			if("" != gblProdCode){
				isCmpFlow = true;
			}
		//}
		}
		if(gblCampaignDataTH != "" && gblCampaignDataTH.split("?").length > 1){
			//if(gblCampaignDataTH.startsWith("frmIBOpenActSelProd") || gblCampaignDataTH.startsWith("frmOpenActSelProd")){
				var tempCampaignDataTH = gblCampaignDataTH.split("?")[0];
				gblProdCode = gblCampaignDataTH.split("?")[1];
				//gblCampaignDataTH = tempCampaignDataTH;
				//if(gblProdCode != "" && allAccProdCodes.indexOf(gblProdCode) != -1){
				if("" != gblProdCode){
					isCmpFlow = true;
				}
			//}
		}
	}
}


function getCampaignResult(){
		if(kony.application.getCurrentForm().id == "frmMyAccntConfirmationAddAccount"){
			frmMyAccntConfirmationAddAccount.btnAddHeader.skin = "btnAddMyAcnt";
			frmMyAccntConfirmationAddAccount.btnAddHeader.focusSkin = "btnAddMyAcnt";
		}
 		var locale = kony.i18n.getCurrentLocale();
 		gblTourFlag = false;
 		if(gblCampaginForm !=null && gblCampaginForm.trim() != "") {
			var cmpType = gblCampaignResp.split("~")[1];
			if(cmpType != 'M'){
				if(gblCampaignFlag.split("-")[2] == "01"){
					campaignBannerIBCount("C","B");
				}else {
					campaignBannerCount("C","B");
				}
			}
			var bannerFlag = gblCampaginForm.split("-")[2];
			var linkFlag = 	gblCampaignFlag.split("-")[1];					
 			//gblCampaignTotalData +=callbackResponse["campaigns"][0]["cmpCode"]+","+callbackResponse["formName"]+","+"0"+","+"1"+","+"0"+","+"0"+"$" 
	 		//if((gblCampaginForm.split("-")[2]=='P' || gblCampaginForm.split("-")[2]=='Y') && gblCampaignFlag.split("-")[1]=='02'){
	 		if((bannerFlag == 'P' || bannerFlag == 'Y') && linkFlag == '02'){
	 			if (locale == "en_US") {
	 				if(gblCampaignDataEN.length > 0 ){
	 					kony.application.openURL(gblCampaignDataEN);
	 				}
		 			
			 	} else{
			 		if(gblCampaignDataTH.length > 0 ) { 
			 			kony.application.openURL(gblCampaignDataTH);
			 		}
			 	}
	 		} else if((bannerFlag == 'P' || bannerFlag == 'Y') && linkFlag == '01') {
	 			//all the campaign internal links for IB and MB are placed in below method 
	 			campaignInternalLinkCode();
	 		} else if((bannerFlag == 'Y' || bannerFlag == 'P') && linkFlag == '03' && gblCampaignFlag.split("-")[2]=='02'){
	 			//alert("Test point 1");
	 			//gblTourFlag=true;
	 			//showAppTourForm();
	 			// for mobile, banner flag is 03 then internal link is frmAppTour, then navigating to appTour
	 			if(gblCampaignDataEN == 'frmAppTour' || gblCampaignDataTH == 'frmAppTour'){
		 			gblTourFlag=true;
		 			showAppTourForm();
		 		}
	 		}
 		}
}



//////Adding this function for DEF16428

function getHotPromotionsResult() {
	var locale = kony.i18n.getCurrentLocale();
	var cmpType = gblCampaignResp.split("~")[1];
	if (cmpType != 'M') {
		var channel = gblCampaignResp.split("~")[4];
		if (channel == "01") {
			campaignBannerIBCount("C", gblCampaignResp.split("~")[5]);
		} else {
			campaignBannerCount("C", gblCampaignResp.split("~")[5]);
		}
	}
	if (gblCmpIntExt == "03") {
		//alert("gblCampaignDataENIL : "+gblCampaignDataENIL+" , gblCampaignDataTHIL : "+gblCampaignDataTHIL+" , gblCampaignDataENEL : "+gblCampaignDataENEL+" , gblCampaignDataTHEL : "+gblCampaignDataTHEL);
		gblCampaignDataEN = gblCampaignDataENIL;
		gblCampaignDataTH = gblCampaignDataTHIL;
		gblCampaignURLEN = gblCampaignDataENEL;
		gblCampaignURLTH = gblCampaignDataTHEL;
		if (gblCampaignDataEN == 'frmAppTour' || gblCampaignDataTH == 'frmAppTour') {
			gblTourFlag = true;
			showAppTourForm();
		}
	}
	if (gblCmpIntExt == "02") {
		if (locale == "en_US") {
			if (gblCampaignDataEN.length > 0) {
				kony.application.openURL(gblCampaignDataEN);
			}
		} else {
			if (gblCampaignDataTH.length > 0) {
				kony.application.openURL(gblCampaignDataTH);
			}
		}
	} else if (gblCmpIntExt == "01") {
		//all the campaign internal links for IB and MB are placed in below method
		campaignInternalLinkCode();
	} else if (gblCmpIntExt == "04") {
		var mobile_no = '';
		if (locale == "en_US") {
			mobile_no = gblCampaignDataENEL;
		}else{
			mobile_no = gblCampaignDataTHEL;
		}
		//dial mobile no
		try {
			kony.print("dial = "+mobile_no);
			//kony.phone.dial(mobile_no);
			if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad") {
				//var basicConf = {message:mobile_no, alertType:constants.ALERT_TYPE_CONFIRMATION, alertTitle:"", yesLabel:"Call", noLabel:"Cancel", alertHandler:callnumber}
				//var pspConf = {};
				//var infoAlert = kony.ui.Alert(basicConf,pspConf);
				IphoneCall.dial(mobile_no);
			}else if (gblDeviceInfo["name"] == "android"){
				kony.phone.dial(mobile_no);
			}else{
				//...
			}
		}catch(err){
			kony.print("error in dial = "+mobile_no+", error message = "+err);
		}
	}
}

/*
function callnumber(response) {
	if (response == true) {
		var locale = kony.i18n.getCurrentLocale();
		var mobile_no = '';
		if (locale == "en_US") {
			mobile_no = gblCampaignDataENEL;
		}else{
			mobile_no = gblCampaignDataTHEL;
		}
		kony.phone.dial(mobile_no);
	}
}
*/

var gblImgIncrement=0;
function campaignPreLoginService(widgetName,formName,appChannel){
 	var inputParams = {
 			widgetName:widgetName,
 			formName:formName,
 			appChannel:appChannel,
 			prelogin:"Y"
	};
	
    invokeServiceSecureAsync("GetCampaign", inputParams, campaignPreServiceCallBack);
}

function campaignPreServiceCallBack(status, callbackResponse) {
    if (status == 400) {
        if (callbackResponse["opstatus"] == 0 && callbackResponse["errCode"] == "0") {
            var locale = kony.i18n.getCurrentLocale();
            var formname = eval(callbackResponse["formName"]);
            var widgetname = callbackResponse["widgetName"];
            var appChannel = callbackResponse["clickChannel"];
            var imageData = [];
            imageDataEN = [];
            imageDataTH = [];
            bannerDataEN = [];
            bannerDataTH = [];
            var tempPreLoginCampData = [];
            var tempAllPreLoginCampData = [];
            gblCampaignPreLoginData = callbackResponse;
            if (callbackResponse["campaigns"].length > 0) {
                if (appChannel == '01') {
                    gblClickCount = callbackResponse["campaigns"].length;
                    for (var i = 0; i < callbackResponse["campaigns"].length; i++) {
                        if (locale == "en_US") {
                            imageData.push({
                                imgProm: {
                                    src: callbackResponse["campaigns"][i]["imgBannerInternetEN"]
                                }
                            });
                            formname["imgActivate"]["src"] = "aw_banner01.jpg";
                        } else {
                            imageData.push({
                                imgProm: {
                                    src: callbackResponse["campaigns"][i]["imgBannerInternetTH"]
                                }
                            });
                            formname["imgActivate"]["src"] = "aw_banner02.jpg";
                        }
                        imageDataEN.push({
                            imgProm: {
                                src: callbackResponse["campaigns"][i]["imgBannerInternetEN"]
                            }
                        });
                        imageDataTH.push({
                            imgProm: {
                                src: callbackResponse["campaigns"][i]["imgBannerInternetTH"]
                            }
                        });
                        if (callbackResponse["campaigns"][i]["bannerFlag"] == 'Y') //adding these conditions for fixing the defect DEF16447
                        {
                            if (callbackResponse["campaigns"][i]["bannerLinkFlagInternet"] == '01' || callbackResponse["campaigns"][i]["bannerLinkFlagInternet"] == '02') {
                                bannerDataEN.push(callbackResponse["campaigns"][i]["bannerLinkInternetEN"]);
                                bannerDataTH.push(callbackResponse["campaigns"][i]["bannerLinkInternetTH"]);
                            }
                            var internetLink = callbackResponse["campaigns"][i]["bannerLinkFlagInternet"];
                            if (locale == "en_US") {
                                if(undefined != internetLink && internetLink != "00"){
                                	tempPreLoginCampData = {
	                                    hbxCmpIBLogin: {
											skin: hbxCursor
										},
	                                    imgProm: {
	                                        src: callbackResponse["campaigns"][i]["imgBannerInternetEN"]
	                                    },
	                                    bannerLinkFlagInternet: callbackResponse["campaigns"][i]["bannerLinkFlagInternet"],
	                                    bannerFlag: callbackResponse["campaigns"][i]["bannerFlag"],
	                                    bannerLinkInternetTH: callbackResponse["campaigns"][i]["bannerLinkInternetTH"],
	                                    bannerLinkInternetEN: callbackResponse["campaigns"][i]["bannerLinkInternetEN"]
	                                };
                                } else {
                                	tempPreLoginCampData = {
	                                    hbxCmpIBLogin: {
											skin: "No Skin"
										},
	                                    imgProm: {
	                                        src: callbackResponse["campaigns"][i]["imgBannerInternetEN"]
	                                    },
	                                    bannerLinkFlagInternet: callbackResponse["campaigns"][i]["bannerLinkFlagInternet"],
	                                    bannerFlag: callbackResponse["campaigns"][i]["bannerFlag"],
	                                    bannerLinkInternetTH: callbackResponse["campaigns"][i]["bannerLinkInternetTH"],
	                                    bannerLinkInternetEN: callbackResponse["campaigns"][i]["bannerLinkInternetEN"]
	                                };
                                }
                                
                            } else {
                                if(undefined != internetLink && internetLink != "00"){
                                	tempPreLoginCampData = {
	                                    hbxCmpIBLogin: {
											skin: hbxCursor
										},
	                                    imgProm: {
	                                        src: callbackResponse["campaigns"][i]["imgBannerInternetTH"]
	                                    },
	                                    bannerLinkFlagInternet: callbackResponse["campaigns"][i]["bannerLinkFlagInternet"],
	                                    bannerFlag: callbackResponse["campaigns"][i]["bannerFlag"],
	                                    bannerLinkInternetTH: callbackResponse["campaigns"][i]["bannerLinkInternetTH"],
	                                    bannerLinkInternetEN: callbackResponse["campaigns"][i]["bannerLinkInternetEN"]
	                                };
                                } else {
                                	tempPreLoginCampData = {
	                                    hbxCmpIBLogin: {
											skin: "No Skin"
										},
	                                    imgProm: {
	                                        src: callbackResponse["campaigns"][i]["imgBannerInternetTH"]
	                                    },
	                                    bannerLinkFlagInternet: callbackResponse["campaigns"][i]["bannerLinkFlagInternet"],
	                                    bannerFlag: callbackResponse["campaigns"][i]["bannerFlag"],
	                                    bannerLinkInternetTH: callbackResponse["campaigns"][i]["bannerLinkInternetTH"],
	                                    bannerLinkInternetEN: callbackResponse["campaigns"][i]["bannerLinkInternetEN"]
	                                };
                                }
                                
                            }
                        }
                        tempAllPreLoginCampData.push(tempPreLoginCampData);
                    }
                    bannerFlag = callbackResponse["campaigns"][0]["bannerFlag"] + "-" + callbackResponse["campaigns"][0]["bannerLinkFlagInternet"];
                    frmIBPreLogin.segCampaignImage.setData(tempAllPreLoginCampData);
                    //formname[widgetname]["data"]=imageData;
                    setCampaignTimerIB();
                } else {
                    if (callbackResponse["formName"] == 'frmMBPreLoginAccessesPin') {
                        dismissLoadingScreen();
                    }
                    formname["hbxAdv"]["isVisible"] = true;
                    //deleted the mb prelogin banner size code, as prelogin do not have banner at present

                    if (locale == "en_US") {
                        formname[widgetname]["src"] = callbackResponse["campaigns"][0]["imgBannerMobEN"];
                    } else {
                        formname[widgetname]["src"] = callbackResponse["campaigns"][0]["imgBannerMobTH"];
                    }
                    imageDataEN.push(callbackResponse["campaigns"][0]["imgBannerMobEN"]);
                    imageDataTH.push(callbackResponse["campaigns"][0]["imgBannerMobTH"]);
                    if (callbackResponse["campaigns"][0]["bannerFlag"] == 'Y') {
                        if (callbackResponse["campaigns"][0]["bannerLinkFlagMob"] == '01') {
                            bannerDataEN.push(callbackResponse["campaigns"][0]["bannerInternalLinkMobEN"]);
                            bannerDataTH.push(callbackResponse["campaigns"][0]["bannerInternalLinkMobTH"]);
                            gblCampaignURLEN = callbackResponse["campaigns"][0]["bannerExternalLinkMobEN"];
                            gblCampaignURLTH = callbackResponse["campaigns"][0]["bannerExternalLinkMobTH"];
                        } else if (callbackResponse["campaigns"][0]["bannerLinkFlagMob"] == '02') {
                            bannerDataEN.push(callbackResponse["campaigns"][0]["bannerExternalLinkMobEN"]);
                            bannerDataTH.push(callbackResponse["campaigns"][0]["bannerExternalLinkMobTH"]);
                        } else if (callbackResponse["campaigns"][0]["bannerLinkFlagMob"] == '03') {
                            bannerDataEN.push(callbackResponse["campaigns"][0]["bannerInternalLinkMobEN"]);
                            bannerDataTH.push(callbackResponse["campaigns"][0]["bannerExternalLinkMobTH"]);
                            gblCampaignURLEN = callbackResponse["campaigns"][0]["bannerExternalLinkMobEN"];
                            gblCampaignURLTH = callbackResponse["campaigns"][0]["bannerExternalLinkMobTH"];
                        }
                    }
                    bannerFlag = callbackResponse["campaigns"][0]["bannerFlag"] + "-" + callbackResponse["campaigns"][0]["bannerLinkFlagMob"];
                    formname["hbxAdv"]["onClick"] = getCampaignMBPreResult;
                }
            } else {
                if (appChannel == '01') {
                    var tempData = [];
                    tempData.push({
                        imgProm: {
                            src: kony.i18n.getLocalizedString("keyCampaignDefault")
                        }
                    });
                    formname[widgetname]["data"] = tempData;
                    if (locale == "en_US") {
                        formname["imgActivate"]["src"] = "aw_banner01.jpg";
                    } else {
                        formname["imgActivate"]["src"] = "aw_banner02.jpg";
                    }
                } else {
                    if (callbackResponse["formName"] == 'frmMBPreLoginAccessesPin') {
                        dismissLoadingScreen();
                    }
                    formname["hbxAdv"]["isVisible"] = false;
                }
            }
        } else {
            if (kony.application.getCurrentForm().id == 'frmIBPreLogin') {
                frmIBPreLogin.segCampaignImage.data = [];
                var tempData = [];
                tempData.push({
                    imgProm: {
                        src: kony.i18n.getLocalizedString("keyCampaignDefault")
                    }
                });
                frmIBPreLogin.segCampaignImage.setData(tempData);
                var locale = kony.i18n.getCurrentLocale();
                if (locale == "en_US") {
                    frmIBPreLogin["imgActivate"]["src"] = "aw_banner01.jpg";
                } else {
                    frmIBPreLogin["imgActivate"]["src"] = "aw_banner02.jpg";
                }
                //formname[widgetname]["data"]=tempData;
            } else if (kony.application.getCurrentForm().id == 'frmSPALogin' || kony.application.getCurrentForm().id == 'frmMBPreLoginAccessesPin') {
                var formname = kony.application.getCurrentForm();
                formname["hbxAdv"]["isVisible"] = false;
                if (kony.application.getCurrentForm().id == 'frmMBPreLoginAccessesPin') {
                    dismissLoadingScreen();
                }
            }
        }
    }
}



function settingPreLoginCampapign() {
    var a = kony.i18n.getCurrentLocale();
    gblClickCount = gblCampaignPreLoginData["campaigns"].length;
    var tempPreLoginCampData = [];
    var tempAllPreLoginCampData = [];
    for (var i = 0; i < gblCampaignPreLoginData["campaigns"].length; i++) {
        var internetLink = gblCampaignPreLoginData["campaigns"][i]["bannerLinkFlagInternet"];
        if (gblCampaignPreLoginData["campaigns"][i]["bannerFlag"] == 'Y') //adding these conditions for fixing the defect DEF16447?
        {
            if (a == "en_US") {
                if(undefined != internetLink && internetLink != "00"){
                	tempPreLoginCampData = {
	                    hbxCmpIBLogin: {
							skin: hbxCursor
						},
	                    imgProm: {
	                        src: gblCampaignPreLoginData["campaigns"][i]["imgBannerInternetEN"]
	                    },
	                    bannerLinkFlagInternet: gblCampaignPreLoginData["campaigns"][i]["bannerLinkFlagInternet"],
	                    bannerFlag: gblCampaignPreLoginData["campaigns"][i]["bannerFlag"],
	                    bannerLinkInternetTH: gblCampaignPreLoginData["campaigns"][i]["bannerLinkInternetTH"],
	                    bannerLinkInternetEN: gblCampaignPreLoginData["campaigns"][i]["bannerLinkInternetEN"]
	                };
                } else {
                	tempPreLoginCampData = {
	                    hbxCmpIBLogin: {
							skin: "No Skin"
						},
	                    imgProm: {
	                        src: gblCampaignPreLoginData["campaigns"][i]["imgBannerInternetEN"]
	                    },
	                    bannerLinkFlagInternet: gblCampaignPreLoginData["campaigns"][i]["bannerLinkFlagInternet"],
	                    bannerFlag: gblCampaignPreLoginData["campaigns"][i]["bannerFlag"],
	                    bannerLinkInternetTH: gblCampaignPreLoginData["campaigns"][i]["bannerLinkInternetTH"],
	                    bannerLinkInternetEN: gblCampaignPreLoginData["campaigns"][i]["bannerLinkInternetEN"]
	                };
                }
            } else {
                if(undefined != internetLink && internetLink != "00"){
                	tempPreLoginCampData = {
	                    hbxCmpIBLogin: {
							skin: hbxCursor
						},
	                    imgProm: {
	                        src: gblCampaignPreLoginData["campaigns"][i]["imgBannerInternetTH"]
	                    },
	                    bannerLinkFlagInternet: gblCampaignPreLoginData["campaigns"][i]["bannerLinkFlagInternet"],
	                    bannerFlag: gblCampaignPreLoginData["campaigns"][i]["bannerFlag"],
	                    bannerLinkInternetTH: gblCampaignPreLoginData["campaigns"][i]["bannerLinkInternetTH"],
	                    bannerLinkInternetEN: gblCampaignPreLoginData["campaigns"][i]["bannerLinkInternetEN"]
	                };
                } else {
                	tempPreLoginCampData = {
	                    hbxCmpIBLogin: {
							skin: "No Skin"
						},
	                    imgProm: {
	                        src: gblCampaignPreLoginData["campaigns"][i]["imgBannerInternetTH"]
	                    },
	                    bannerLinkFlagInternet: gblCampaignPreLoginData["campaigns"][i]["bannerLinkFlagInternet"],
	                    bannerFlag: gblCampaignPreLoginData["campaigns"][i]["bannerFlag"],
	                    bannerLinkInternetTH: gblCampaignPreLoginData["campaigns"][i]["bannerLinkInternetTH"],
	                    bannerLinkInternetEN: gblCampaignPreLoginData["campaigns"][i]["bannerLinkInternetEN"]
	                };
                }
            }
        }
        tempAllPreLoginCampData.push(tempPreLoginCampData);
    }
    bannerFlag = gblCampaignPreLoginData["campaigns"][0]["bannerFlag"] + "-" + gblCampaignPreLoginData["campaigns"][0]["bannerLinkFlagInternet"];
    //alert("tempAllPreLoginCampData : "+tempAllPreLoginCampData);
    frmIBPreLogin.segCampaignImage.setData(tempAllPreLoginCampData);
}

function changeCampaignPreLocale() {
    var locale = kony.i18n.getCurrentLocale();
    if (imageDataEN.length > 0 || imageDataTH.length > 0) {
        frmIBPreLogin.segCampaignImage.removeAll();
        ////////////
        settingPreLoginCampapign();
        /////////////////////
        gblImgIncrement = 0;
        if (locale == "en_US") {
            //frmIBPreLogin.segCampaignImage.setData(imageDataEN);
            frmIBPreLogin["imgActivate"]["src"] = "aw_banner01.jpg";
        } else {
            //frmIBPreLogin.segCampaignImage.setData(imageDataTH);
            frmIBPreLogin["imgActivate"]["src"] = "aw_banner02.jpg";
        }
    } else {
        frmIBPreLogin.segCampaignImage.data = [];
        var tempData = [];
        tempData.push({
            imgProm: {
                src: kony.i18n.getLocalizedString("keyCampaignDefault")
            }
        });
        frmIBPreLogin.segCampaignImage.setData(tempData);
        if (locale == "en_US") {
            frmIBPreLogin["imgActivate"]["src"] = "aw_banner01.jpg";
        } else {
            frmIBPreLogin["imgActivate"]["src"] = "aw_banner02.jpg";
        }
    }
}

function changeCampaignMBPreLocale(formname,formHbxName){
 		var locale = kony.i18n.getCurrentLocale();
 		if(imageDataEN.length > 0 || imageDataTH.length > 0 ) {
 			if(locale == "en_US"){
 				 formname.src = imageDataEN[0];
	 		} else {
	 			 formname.src = imageDataTH[0];
	 		}
	 		formHbxName.onClick=getCampaignMBPreResult;
 		}else{
 			formHbxName.setVisibility(false);
 		}
 		
}

function getCampaignMBPreResult(){
 	  var locale = kony.i18n.getCurrentLocale();
    if (bannerDataEN.length > 0 || bannerDataTH.length > 0) {
	    if(bannerFlag.split("-")[0]=='Y' && bannerFlag.split("-")[1]=='01' ){
	    	var formname = "";
	    	if (locale == "en_US") {
		    		if(bannerDataEN[0].length > 0 ){
		    			formname = eval(bannerDataEN[0]);
		    			formname.show();
		    		}
	    	} else {
	    			if(bannerDataTH[0].length > 0 ){
	    				formname = eval(bannerDataTH[0]);
	    				formname.show();
	    			}
	    	}
	    	
	    	if(bannerDataEN[0]=='frmATMBranch' || bannerDataTH[0]=='frmATMBranch'){
	    		gblLatitude = "";
				gblLongitude = "";
				gblCurrenLocLat = "";
				gblCurrenLocLon = "";
				provinceID = "";
				districtID = "";
				gblProvinceData = [];
				gblDistrictData = [];
				frmATMBranch.btnCombProvince.text = kony.i18n.getLocalizedString("FindTMB_Province");
				frmATMBranch.btnCombDistrict.text =  kony.i18n.getLocalizedString("FindTMB_District");
				frmATMBranch.txtKeyword.text = "";
				onClickATM();
	    	} else if(bannerDataEN[0]=='frmExchangeRate'|| bannerDataTH[0]=='frmExchangeRate'){
	    		onClickExchangeRates();
	    	} else if(bannerDataEN[0]=='frmAppTour' ||  bannerDataTH[0]=='frmAppTour'){
	    		gblTourFlag=true;
	    		showAppTourForm();
	    	}else if(bannerDataEN[0]=='frmContactUsMB' || bannerDataTH[0]=='frmContactUsMB'){
	    		conatctUsForm();
	    	}
	    } else if( bannerFlag.split("-")[0]=='Y' && bannerFlag.split("-")[1]=='03'){
	    		gblTourFlag=true;
	    		showAppTourForm();
	    } else if(bannerFlag.split("-")[0]=='Y' && bannerFlag.split("-")[1]=='02'){
	    	 if (locale == "en_US") {
	    	 	if( bannerDataEN[0].length > 0 ){
	    	 		kony.application.openURL(bannerDataEN[0]);
	    	 	}
	        } else {
	        	if( bannerDataTH[0].length > 0 ){
	        		 kony.application.openURL(bannerDataTH[0]);
	        	}
	        }
	    }
    }
 }
 
function getCampaignPreResult() {
    var a = kony.i18n.getCurrentLocale();
    var tempBannerDataEN = frmIBPreLogin.segCampaignImage.selectedItems[0]["bannerLinkInternetEN"];
    var tempBannerDataTH = frmIBPreLogin.segCampaignImage.selectedItems[0]["bannerLinkInternetTH"];
    bannerFlag = frmIBPreLogin.segCampaignImage.selectedItems[0]["bannerFlag"] + "-" + frmIBPreLogin.segCampaignImage.selectedItems[0]["bannerLinkFlagInternet"];
    if (bannerDataEN.length > 0 || bannerDataTH.length > 0) {
        if (bannerFlag.split('-')[0] == 'Y' && bannerFlag.split('-')[1] == '03') {
            kony.application.openURL(kony.i18n.getLocalizedString('linkSiteTour'));
        } //////////////////added code for showing the internel forms when it is set to '01'
        else if (bannerFlag.split("-")[0] == 'Y' && bannerFlag.split("-")[1] == '01') {
            if (a == 'en_US') {
                //if(bannerDataEN[frmIBPreLogin.segCampaignImage.selectedIndex[1]].length > 0) {
                if (tempBannerDataEN == 'frmIBATMBranch') {
                    gblFindTMBViewTypeIB = "MapView";
                    gblLatitudeIB = "";
                    gblLongitudeIB = "";
                    provinceIDIB = "";
                    districtIDIB = "";
                    gblProvinceResultSet = [];
                    gblDistrictResultSet = [];
                    finTMBfooterLinks();
                    onClickFindATMIB();
                } else if (tempBannerDataEN == 'frmIBExchangeRates') {
                    exchangeRatefooterLinks();
                    onclickExchangeRatesIB();
                }
                //}
            } else {
                // if(bannerDataTH[frmIBPreLogin.segCampaignImage.selectedIndex[1]].length > 0) {
                if (tempBannerDataTH == 'frmIBATMBranch') {
                    gblFindTMBViewTypeIB = "MapView";
                    gblLatitudeIB = "";
                    gblLongitudeIB = "";
                    provinceIDIB = "";
                    districtIDIB = "";
                    gblProvinceResultSet = [];
                    gblDistrictResultSet = [];
                    finTMBfooterLinks();
                    onClickFindATMIB();
                } else if (tempBannerDataTH == 'frmIBExchangeRates') {
                    exchangeRatefooterLinks();
                    onclickExchangeRatesIB();
                }
                //}
            }
        } else if (bannerFlag.split("-")[0] == 'Y' && bannerFlag.split("-")[1] == '02') {
            if (a == 'en_US') {
                //if (bannerDataEN[frmIBPreLogin.segCampaignImage.selectedIndex[1]].length > 0) {
                //					kony.application.openURL(bannerDataEN[frmIBPreLogin.segCampaignImage.selectedIndex[1]]);
                //				}
                if (tempBannerDataEN != "") {
                    kony.application.openURL(tempBannerDataEN);
                }
            } else {
                //if (bannerDataTH[frmIBPreLogin.segCampaignImage.selectedIndex[1]].length > 0) {
                //					kony.application.openURL(bannerDataTH[frmIBPreLogin.segCampaignImage.selectedIndex[1]]);
                //				}
                if (tempBannerDataTH != "") {
                    kony.application.openURL(tempBannerDataTH);
                }
            }
        }
    }
}


 

function setCampaignTimerIB(){
  		try{
		  
		  kony.timer.schedule("campaignTimerIB", setTimeforCampaignImageIB, 5, true);
		 }catch(e){
		  
		 }
}
 
function setTimeforCampaignImageIB(){
	 try {
		  
		  var index = gblImgIncrement;
		  if(index == gblClickCount-1){
		   index = -1;
		   gblImgIncrement= -1;
		  }
		  gblImgIncrement = gblImgIncrement + 1;
		  index = index+1;
		  frmIBPreLogin.segCampaignImage.selectedIndex = [0,index];
	  }catch(e){
	 	 
	  //kony.timer.cancel("promoTimerIB");
	 }
} 
 
 
function campaignBannerIBCount(actionType, messageType) {
   	showLoadingScreenPopup();
	var userAgent = gblDeviceInfo["userAgent"]; //"Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.80 Safari/537.36";
	var osVersion = "Windows";
	if(userAgent.indexOf("Windows") != -1){
	  osVersion = "Windows";  
	} else if (userAgent.indexOf("Mac") != -1){
	  osVersion = "Mac";	  
	}
	if(gblCampaignResp.length > 0){
		var inputParams = {
            
            cmpCode: gblCampaignResp.split("~")[2],
            cmpEndDate: gblCampaignResp.split("~")[3],
            browser: gblDeviceInfo["category"] + " v"+gblDeviceInfo["version"],
            osVersion: osVersion,
            clickChannel: "IB",
            clickScreen: gblCampaignResp.split("~")[0],
            actionType : actionType,
            messageType : messageType            
        };
        invokeServiceSecureAsync("CollectCampaignMetric", inputParams, campaignCountServiceIBCallBack);
	}
}

function campaignCountServiceIBCallBack(status, callbackResponse) {
    dismissLoadingScreenPopup();
    if (status == 400) {
        if (callbackResponse["opstatus"] == 0) {
			/* gblCampaignResp = "";
        	gblCampaignFlag = "";
		    //gblCampaignData = "";
		    gblLastClicked = "";
		    gblCampaginForm = "";
		    gblCampaignImgEN = "";
		    gblCampaignImgTH = "";
		    gblCampaignDataEN = "";
		    gblCampaignDataTH = ""; */
        } 
    }
}

function campaignBannerCount(actionType, messageType) {
	var clickChannel = "MB";
	var deviceOs = "android"
	var deviceModel = "Nexus";
	if(flowSpa){
		clickChannel = "SPA";
		deviceOs = gblDeviceInfo["category"];
		deviceModel = "";
	} else {
		deviceOs = gblDeviceInfo["name"];
		deviceModel = gblDeviceInfo["model"];
	}
	// as per DEF1541, changed android to Android and iPhone to iOS
	if(deviceOs == "android"){
		deviceOs = "Android";
	} else if(deviceOs == "iPhone" || deviceOs == "iphone" || deviceOs == "iPad" || deviceOs == "iPod touch"){
		deviceOs = "iOS";
	}
	
	//alert("clickChannel : "+clickChannel);
    if (gblCampaignResp.length > 0) {
        var inputParams = {
            cmpCode: gblCampaignResp.split("~")[2],
            cmpEndDate: gblCampaignResp.split("~")[3],
            //deviceID: gblDeviceInfo["deviceid"],
			deviceID: GBL_UNIQ_ID,
            deviceModel: deviceModel,
            deviceOS: deviceOs,
            deviceOsVerion : gblDeviceInfo["version"],
            //browser: gblDeviceInfo["category"] + " v"+gblDeviceInfo["version"],
            clickChannel: clickChannel,
            clickScreen: gblCampaignResp.split("~")[0],
            actionType : actionType,
            messageType : messageType 
        };
        invokeServiceSecureAsync("CollectCampaignMetric", inputParams, campaignCountServiceCallBack);
    }
}


function campaignCountServiceCallBack(status, callbackResponse) {
    if (status == 400) {
        if (callbackResponse["opstatus"] == 0) {
        	/* gblCampaignFlag = "";
		    //gblCampaignData = "";
		    gblLastClicked = "";
		    gblCampaginForm = "";
		    gblCampaignImgEN = "";
		    gblCampaignImgTH = "";
		    gblCampaignFlag = "";
		    gblCampaignDataEN = "";
		    gblCampaignDataTH = "";
		    gblViewCount = 0;
		    gblClickCount = 0;
		    //gblCampaignTotalData = "";
		    campaignData = {};	*/	    
        } 
    } 
}


function campaignInternalLinkCode() {
	if (gblCampaignDataEN == 'frmIBTransferCustomWidgetLP' || gblCampaignDataTH == 'frmIBTransferCustomWidgetLP') {
		transferMenuClick()
	} else if (gblCampaignDataEN.indexOf('frmIBOpenActSelProd') == 0 || gblCampaignDataTH.indexOf('frmIBOpenActSelProd') == 0) {
		//adeed below method to set isCmpFlow true or false
		setCampaignFlowForProdDetails();
	
		//IBcheckTranPwdLocked();
		checkopenActCustStatus();
	} else if (gblCampaignDataEN == 'frmIBPromotionsDetails' || gblCampaignDataTH == 'frmIBPromotionsDetails') {
		//added this to implement MIB-967
		gblMyOffersDetails = true;
		onClickPromotionsIB();
		if(frmIBPromotions.hboxImage.isVisible){
			frmIBPromotions.hboxImage.setVisibility(false);
			frmIBPromotions.hboxPromotionDetails.setVisibility(true);
		}

	} else if (gblCampaignDataEN == 'frmPromotionDetails' || gblCampaignDataTH == 'frmPromotionDetails') {
		//added this to implement MIB-966
		gblMyOffersDetails = true;
		onClickPromotions();		
	} else if (gblCampaignDataEN.indexOf('frmOpenActSelProd') == 0 || gblCampaignDataTH.indexOf('frmOpenActSelProd') == 0) {
		//adeed below method to set isCmpFlow true or false
		setCampaignFlowForProdDetails();
		
		OnClickOpenNewAccount();
	} else if (gblCampaignDataEN == 'frmTransferLanding' || gblCampaignDataTH == 'frmTransferLanding') {
		//getTransferFromAccounts();
		transferFromMenu();
	} else if (gblCampaignDataEN == 'frmSSSApply' || gblCampaignDataTH == 'frmSSSApply') {
		getApplyS2SStatus();
	} else if (gblCampaignDataEN == 'frmIBSSSTnC' || gblCampaignDataTH == 'frmIBSSSTnC') {
		getApplyS2SStatusIB();
	} else if (gblCampaignDataEN == 'frmPromotion' || gblCampaignDataTH == 'frmPromotion') {
		gblMyOffersDetails = false;
		onClickPromotions();
	} else if (gblCampaignDataEN == 'frmIBPromotions' || gblCampaignDataTH == 'frmIBPromotions') {
		gblMyOffersDetails = false;
		onClickPromotionsIB();
	} else if (gblCampaignDataEN == 'frmInboxHome' || gblCampaignDataTH == 'frmInboxHome') {
		onClickMessages();
	} else if (gblCampaignDataEN == 'frmIBInboxHome' || gblCampaignDataTH == 'frmIBInboxHome') {
		//getInboxHistoryIB();
		frmIBInboxHome.show();
	} else if (gblCampaignDataEN == 'frmIBNotificationHome' || gblCampaignDataTH == 'frmIBNotificationHome') {
		//getNotificationHistoryIB();
		frmIBNotificationHome.show();
	} else if (gblCampaignDataEN == 'frmNotificationHome' || gblCampaignDataTH == 'frmNotificationHome') {
		onClickNotifications();
	} else if (gblCampaignDataEN == 'frmMBMyActivities' || gblCampaignDataTH == 'frmMBMyActivities') {
		showFrmMBMyActivities();
	} else if (gblCampaignDataEN == 'frmIBMyActivities' || gblCampaignDataTH == 'frmIBMyActivities') {
		onMenuMyActivitiesClick();
	} else if (gblCampaignDataEN == 'frmBBApplyNow' || gblCampaignDataTH == 'frmBBApplyNow') {
		checkUserStatusBBMB();
	} else if (gblCampaignDataEN == 'frmIBBeepAndBillList' || gblCampaignDataTH == 'frmIBBeepAndBillList') {
		checkUserStatusBBIB();
	} else if (gblCampaignDataEN == 'frmBillPayment' || gblCampaignDataTH == 'frmBillPayment') {
		showBillPaymentForm();
	} else if (gblCampaignDataEN == 'frmTopUp' || gblCampaignDataTH == 'frmTopUp') {
		showTopUpForm();
	} else if (gblCampaignDataEN == 'frmMyTopUpList' || gblCampaignDataTH == 'frmMyTopUpList') {
		onClickMyTopUpFlow();
	}
	//else if((gblCampaignDataEN=='frmMyTopUpList' && gblCampaginForm.split("-")[1]=='imgTwo') || (gblCampaignDataTH=='frmMyTopUpList' && gblCampaginForm.split("-")[1]=='imgTwo') ){
	//		 				onClickMyTopUpFlow();
	//		 			}
	else if (gblCampaignDataEN == 'frmContactUsMB' || gblCampaignDataTH == 'frmContactUsMB') {
		conatctUsForm();
	} else if (gblCampaignDataEN == 'frmAccountSummaryLanding' || gblCampaignDataTH == 'frmAccountSummaryLanding') {
		showAccountSummaryFromMenu();
	} else if (gblCampaignDataEN == 'frmIBMyBillersHome' || gblCampaignDataTH == 'frmIBMyBillersHome') {
		loadMyBillPage();
	} else if (gblCampaignDataEN == 'frmIBMyTopUpsHome' || gblCampaignDataTH == 'frmIBMyTopUpsHome') {
		gblTopupFromMenu = true;
		getMyTopUpSuggestListIB();
	} else if (gblCampaignDataEN == 'frmIBBillPaymentCW' || gblCampaignDataTH == 'frmIBBillPaymentCW') {
		callBillPaymentCustomerAccountServiceIB();
	} else if (gblCampaignDataEN == 'frmIBTopUpCW' || gblCampaignDataTH == 'frmIBTopUpCW') {
		gblPaynow = true;
		gblRetryCountRequestOTP = 0;
		gblEditTopUp = false;
		callTopUpPaymentCustomerAccountServiceIB();
	} else if (gblCampaignDataEN == 'frmIBChequeServiceStopChequeLanding' || gblCampaignDataTH == 'frmIBChequeServiceStopChequeLanding') {
		callcustomerAccountChequeService();
	} else if (gblCampaignDataEN == 'frmApplyMBviaIBStep1' || gblCampaignDataTH == 'frmApplyMBviaIBStep1') {
		//partyInqMobForApplyMBService(); // in latest campaign_info_v0.7.xls sheet above form not mentioned
	} else if (gblCampaignDataEN == 'frmApplyMBviaIBStepMobApp' || gblCampaignDataTH == 'frmApplyMBviaIBStepMobApp') {
		//apply MB
		partyInqMobForApplyMBService();
		frmApplyMBviaIBStep1.show();
		controlHeaderTextDisplay(0);
		gblMode = 0;
	} else if (gblCampaignDataEN == 'frmApplyMBviaIBStepUnlockMobApp' || gblCampaignDataTH == 'frmApplyMBviaIBStepUnlockMobApp') {
		//unlock MB or reset MB
		partyInqMobForApplyMBService();
		frmApplyMBviaIBStep1.show();
		controlHeaderTextDisplay(2);
		gblMode = 2;
	} else if (gblCampaignDataEN == 'frmIBAccntSummary' || gblCampaignDataTH == 'frmIBAccntSummary') {
		accountsummaryLangToggleIB();
	} else if (gblCampaignDataEN == 'frmMyProfile' || gblCampaignDataTH == 'frmMyProfile') {
		onClickMyProfileFlow();
	} else if (gblCampaignDataEN == 'frmMyAccountList' || gblCampaignDataTH == 'frmMyAccountList') {
		onClickMyAccountsFlow();
	} else if (gblCampaignDataEN == 'frmMyRecipients' || gblCampaignDataTH == 'frmMyRecipients') {
		onClickMyRecipientsFlow();
	} else if (gblCampaignDataEN == 'frmMyBillerList' || gblCampaignDataTH == 'frmMyBillerList') {
		onClickMyBillerFlow();
	} else if (gblCampaignDataEN == 'frmApplyInternetBankingMB' || gblCampaignDataTH == 'frmApplyInternetBankingMB') {
		gblApplyServices = 2;
		//ActionCode is 11 for ApplyIB
		if (checkMBUserStatus()) {
			applyServicesCommon("11", gblIBFlowStatus, gblMBFlowStatus, gblCustomerStatus);
		}
	} else if (gblCampaignDataEN == 'frmApplyInternetBankingMultiDevMB' || gblCampaignDataTH == 'frmApplyInternetBankingMultiDevMB') {
		//ActionCode is 23 for add device
		gblApplyServices = 3;
		if (checkMBUserStatus()) {
			applyServicesCommon("23", gblIBFlowStatus, gblMBFlowStatus, gblCustomerStatus);
		}
	} else if (gblCampaignDataEN == 'frmIBCMMyProfile' || gblCampaignDataTH == 'frmIBCMMyProfile') {
		IBviewprofileServiceCall();
	} else if (gblCampaignDataEN == 'frmIBMyAccnts' || gblCampaignDataTH == 'frmIBMyAccnts') {
		frmIBMyAccnts.hbxTMBImg.setVisibility(true);
		frmIBMyAccnts.hboxAddNewAccnt.setVisibility(false);
		//frmIBMyAccnts.imgArrow.setVisibility(false);
		frmIBMyAccnts.hbxViewAccnt.setVisibility(false);
		//frmIBMyAccnts.arrwImgSeg1.setVisibility(false);
		//frmIBMyAccnts.arrwImgSeg2.setVisibility(false);
		frmIBMyAccnts.cmbobxAccntType.setVisibility(false);
		frmIBMyAccnts.lineComboAccntype.setVisibility(false);
		frmIBMyAccnts.hbxEditAccntNN.setVisibility(false);
		showLoadingScreenPopup();
		frmIBMyAccnts.show();
	} else if (gblCampaignDataEN == 'frmIBMyReceipentsHome' || gblCampaignDataTH == 'frmIBMyReceipentsHome') {
		showLoadingScreenPopup();
		frmIBMyReceipentsHome.show();
	} else if (gblCampaignDataEN == 'frmIBATMBranch' || gblCampaignDataTH == 'frmIBATMBranch') {
		//getGeoLocationIB();
		gblFindTMBViewTypeIB = "MapView";
		gblLatitudeIB = "";
		gblLongitudeIB = "";
		provinceIDIB = "";
		districtIDIB = "";
		gblProvinceResultSet = [];
		gblDistrictResultSet = [];
		finTMBfooterLinks();
		onClickFindATMIB();
	} else if (gblCampaignDataEN == 'frmIBExchangeRates' || gblCampaignDataTH == 'frmIBExchangeRates') {
		onclickExchangeRatesIB();
	} else if (gblCampaignDataEN == 'frmAppTour' || gblCampaignDataTH == 'frmAppTour') {
		gblTourFlag = true;
		showAppTourForm();
	} else if (gblCampaignDataEN == 'frmMBTnC' || gblCampaignDataTH == 'frmMBTnC') {
		frmMBTnCPreShow();
	} else if (gblCampaignDataEN == 'frmIBActivationTandC' || gblCampaignDataTH == 'frmIBActivationTandC') {
		IBTnCText();
	} else if (gblCampaignDataEN == 'frmIBFirstTimeUserSelection' || gblCampaignDataTH == 'frmIBFirstTimeUserSelection') {
		frmIBFirstTimeUserSelection.show();
	} else if (gblCampaignDataEN == 'frmATMBranch' || gblCampaignDataTH == 'frmATMBranch') {
		gblLatitude = "";
		gblLongitude = "";
		gblCurrenLocLat = "";
		gblCurrenLocLon = "";
		provinceID = "";
		districtID = "";
		gblProvinceData = [];
		gblDistrictData = [];
		frmATMBranch.btnCombProvince.text = kony.i18n.getLocalizedString("FindTMB_Province");
		frmATMBranch.btnCombDistrict.text = kony.i18n.getLocalizedString("FindTMB_District");
		frmATMBranch.txtKeyword.text = "";
		onClickATM();
	} else if (gblCampaignDataEN == 'frmExchangeRate' || gblCampaignDataTH == 'frmExchangeRate') {
		onClickExchangeRates();
	} else if (gblCampaignDataEN == 'frmIBEStatementProdFeature' || gblCampaignDataEN == 'frmIBEStatementProdFeature') {
		if(isShowEStatementMenu()){
			//frmIBEStatementProdFeature.show();
			showLoadingScreenPopup();
			checkCustStatuseStmt();
		}else{
			alert(kony.i18n.getLocalizedString("keyUserDontHaveEStatement"));
		}		
	} else if (gblCampaignDataEN == 'frmIBAnyIDProdFeature' || gblCampaignDataTH == 'frmIBAnyIDProdFeature') {		
		if(gblCampaginForm.indexOf('frmIBFirstTimeActivationComplete') == 0){		
			bannerClickCheckIBLoginViaActivation();						
			var i = 0; 
			var targetObjStatus = ''; 
			var maxTrylimit = 30;
			var timeToRecheck = 2000;
			var checkLoginResult = function(){ 
				if (frmIBPostLoginDashboard.segMenuOptions != undefined){
					targetObjStatus = 'success';
				}	
				if (targetObjStatus == 'success') {
					bannerNavigatetoAnyIDBrief();					
				} else {
					i++;
					if (i<=maxTrylimit) {				
						timer1 =setTimeout(checkLoginResult,timeToRecheck);
					} else {
						// do no thing ,because can't find the target object in timeToRecheck*maxTrylimit/(60*1000) minute;
					}
				} 	
			}
			setTimeout(checkLoginResult,2000);						
		}else{
			bannerNavigatetoAnyIDBrief();
		}
	}else if (gblCampaignDataEN == 'frmIBInboxHomeDetails' || gblCampaignDataTH == 'frmIBInboxHomeDetails') {
		/*Enhanchment no: MIB-1875*/
		if(gblInboxMsgDetails === undefined){
			var inputParams = {};
			invokeServiceSecureAsync("inboxhistory", inputParams, getInboxMessagesCampaign);
		}else{
			navigateToInboxMessageDetail();
		}
	} else if (gblCampaignDataEN == 'frmInboxDetails' || gblCampaignDataTH == 'frmInboxDetails') {
		/*Enhanchment no: MIB-1875*/
		
		/*if(gblInboxMsgDetails === undefined){
			var inputParams = {};
			invokeServiceSecureAsync("inboxhistory", inputParams, getInboxMessagesCampaignMB);
		}else{
			navigateToInboxMessageDetailMB();
		}*/
		//Fixe MIB-3297 Call inboxhistory
		var inputParams = {};
		invokeServiceSecureAsync("inboxhistory", inputParams, getInboxMessagesCampaignMB);
		
	} else if (gblCampaignDataEN == 'frmIBMutualFundsSummary' || gblCampaignDataTH == 'frmIBMutualFundsSummary') {
		if(gblHaveMutualFundsFlg){
			callMutualFundsSummary();
		}else{
			alert(kony.i18n.getLocalizedString("keyUserDontHaveMutualFund"));
		}
	} else if (gblCampaignDataEN == 'frmIBBankAssuranceSummary' || gblCampaignDataTH == 'frmIBBankAssuranceSummary') {
		if(gblHaveBankAssuranceFlg){
			callBAPolicyListService();
		}else{
			alert(kony.i18n.getLocalizedString("keyUserDontHaveBankAssurance"));
		}
	} else if (gblCampaignDataEN == 'frmMBAnyIdRegTnC' || gblCampaignDataTH == 'frmMBAnyIdRegTnC') {
		 if (checkMBUserStatus()){
			//frmMBAnyIdRegTnC.show();
			getAnyIDBriefImage();
			onClickSetUserID();
	 	 }
	}else if(gblCampaignDataEN == 'frmMutualFundsSummaryLanding' || gblCampaignDataTH == 'frmMutualFundsSummaryLanding'){
    	if(gblHaveMutualFundsFlg){
			MBcallMutualFundsSummary();
		}else{
			alert(kony.i18n.getLocalizedString("keyUserDontHaveMutualFund"));
		}
	}else if(gblCampaignDataEN == 'frmMBBankAssuranceSummary' || gblCampaignDataTH == 'frmMBBankAssuranceSummary'){
		if(gblHaveBankAssuranceFlg){
			MBcallBAPolicyListService();
		}else{
			alert(kony.i18n.getLocalizedString("keyUserDontHaveBankAssurance"));
		}
	}else if(gblCampaignDataEN == 'frmMBForgotPin' || gblCampaignDataTH == 'frmMBForgotPin'){
		 gblApplyServices = 4;
         if (checkMBUserStatus()){
	 
				applyServicesCommon("12", gblIBFlowStatus, gblMBFlowStatus, gblCustomerStatus);
	 		}	
		
		//gblApplyServices = 4;
		//frmMBForgotPin.show();
		//frmApplyInternetBankingMBPreShow();
	}else if(gblCampaignDataEN == 'frmMBEStatementProdFeature' || gblCampaignDataTH == 'frmMBEStatementProdFeature'){
		if(isShowEStatementMenu()){
			if (checkMBUserStatus()){
				checkpointMBestmtBousinessHrs();
				//onClickEStatementLink();
			}
		}else{
			alert(kony.i18n.getLocalizedString("keyUserDontHaveEStatement"));
		}	
	}
}

function getInboxMessagesCampaign(status, resultset){
	if(status == 400){
		if(resultset["status"] == "0"){
			if(resultset["Results"].length > 0){
				gblInboxMsgDetails = resultset["Results"];
				navigateToInboxMessageDetail();
				
			} else {
				//if InboxMsg Not Found show pop up alert below
				alert(kony.i18n.getLocalizedString("keyCampaignInboxNotFound"));
			}
		} 
	}
	dismissLoadingScreenPopup();
}

function navigateToInboxMessageDetail(){
      var cmpCode = "";
      if (gblCampaignResponse !== undefined && gblCampaignResponse.length > 0) {
            cmpCode = gblCampaignResponse["cmpCode"];
      }else if (gblCampaignResp !== undefined && gblCampaignResp.length > 0){
            cmpCode = gblCampaignResp.split("~")[2];
      }
      
      if(cmpCode !== undefined && cmpCode != ""){
                  var campaignCode = cmpCode;
                  var inboxLen = gblInboxMsgDetails.length;
                  var inboxId = null;
                  for(i=0; i<inboxLen; i++){
                        if(gblInboxMsgDetails[i]["cmpCode"] == campaignCode){
                              inboxId = gblInboxMsgDetails[i]["id"];
                              break;
                        }
                  }
                  
                  if(inboxId != null){
                        getDetailsOfNotfnIN = inboxId;
                        getInboxDetailsIB();
                        frmIBInboxHome.show();
                  }else{
                        alert(kony.i18n.getLocalizedString("keyCampaignInboxNotFound"));
                  }
      }
}

function getInboxMessagesCampaignMB(status, resultset){
	if(status == 400){
		if(resultset["status"] == "0"){
			if(resultset["Results"].length > 0){
				gblInboxMsgDetails = resultset["Results"];
				navigateToInboxMessageDetailMB();
			} else {
				//if InboxMsg Not Found show pop up alert below
				alert(kony.i18n.getLocalizedString("keyCampaignInboxNotFound"));
			}
		}
	}
	dismissLoadingScreenPopup();
}

function navigateToInboxMessageDetailMB(){
	var cmpCode = "";
      if (gblCampaignResponse !== undefined && gblCampaignResponse.length > 0) {
            cmpCode = gblCampaignResponse["cmpCode"];
      }else if (gblCampaignResp !== undefined && gblCampaignResp.length > 0){
            cmpCode = gblCampaignResp.split("~")[2];
      }
	
	var inboxId = null;
	if(cmpCode !== undefined && cmpCode != ""){
			var campaignCode = cmpCode;
			if(gblInboxMsgDetails !== undefined && gblInboxMsgDetails.length > 0){
				for(i=0; i<gblInboxMsgDetails.length; i++){
					if(gblInboxMsgDetails[i]["cmpCode"] == campaignCode){
						inboxId = gblInboxMsgDetails[i]["id"];
						break;
					}
				}
			}			
			
			if(inboxId != null){
				gblMessageIdMB = inboxId;
				getInboxDetailsMBCampaign();
			}else{
				alert(kony.i18n.getLocalizedString("keyCampaignInboxNotFound"));
			}
		}
}

function getInboxDetailsMBCampaign(){
	showLoadingScreen();

	var inputParams = {
		messageId : gblMessageIdMB
	};
	invokeServiceSecureAsync("inboxdetails", inputParams, getInboxDetailsMBCB);
}

function  bannerClickCheckIBLoginViaActivation (){
	if(gblIsFromMigration == true){
			gblMYInboxCountFrom = false;
			unreadInboxMessagesTrackerLocalDBSyncIB();
			hbxFooterPrelogin.linkhotpromo.setVisibility(true);
			LoginflowPartyInqService();
		}else{
			IBLoginViaActivation();
	}
}

function bannerNavigatetoAnyIDBrief(){
		dynamicMenu_btnMenuConvenientServices_onClick_seq0();
		gblMenuSelection = 2; 
		selectedSubMenuIndex = 1;
		navigatetoAnyIDBrief();
}


function adjustBannerMB(formNameStr){	
	
	if(formNameStr == "frmMBAnyIdRegCompleted" || formNameStr == "frmMBEStatementComplete"){
		var deviceName = gblDeviceInfo["name"];
	    var deviceModel = gblDeviceInfo["model"];
		var screenwidth = gblDeviceInfo["deviceWidth"];
		var	deviceHeight = gblDeviceInfo["deviceHeight"];
		var locale = kony.i18n.getCurrentLocale();
		
		var bannerTmgin = (70/1920)*deviceHeight; 
		var bannerTmgin_min = 3;
		var bannerRmgin = 2 ;
		var bannerLmgin = 2 ; 
		var bannerBmgin = 1 ;
		
		if (flowSpa) {
			////  for SPA dimension	
	           bannerTmgin = 45 ;
			if (screenwidth == 414) {
				 bannerTmgin = 45  ;			
			} else if (screenwidth == 375) {
				 bannerTmgin = 45  ;
			} else if (screenwidth == 320) {
				 bannerTmgin = 45  ;
			} else if (screenwidth == 360) {
				 bannerTmgin = 45  ;
			} else if (screenwidth == 384) {
				 bannerTmgin = 45  ;
			}
		} else if(deviceName == "iPhone" || deviceName == "android" ){
			if (deviceName == "iPhone") {
				if (deviceModel == "iPhone 6 Plus") {
					 //alert("this is from iphone6plus");
					 bannerTmgin = 81  ;
					 bannerRmgin = 3  ;
				} else if (deviceModel == "iPhone 6") {
					 //alert("this is from iphone6");
					 bannerTmgin = 81  ;
				} else if (deviceModel == "5G iPhone") {
					// alert("this is from 5G iPhone");
					 bannerTmgin = 63  ;
				} else if (deviceModel == "4GS iPhone") {
					// alert("this is from 4GS iPhone");
					 bannerTmgin = 35  ;
					 bannerRmgin = 3  ;
					 bannerLmgin = 3 ;
				}else{
					 bannerTmgin = 63  ;
				}
			} else if (deviceName == "android") {  
				if (deviceModel == "Nexus 4") {
					 //alert("this is from Nexus 4");
					 bannerTmgin = 53  ;
				} else if (deviceModel == "GT-I9500") { // Samsung Galaxy S4
					 //alert("this is from Samsung Galaxy S4 GT-I9500");
					 bannerTmgin = 70  ;
					 bannerRmgin = 3 ;
					 bannerLmgin = 3 ;
				} else if (screenwidth == 1080) {
					 //alert("this is from nesux5 "+screenwidth);
					 bannerTmgin = 53  ;
				} else if (screenwidth == 768) {
					 //alert("this is from Nexus "+screenwidth);
					 bannerTmgin = 48  ;
				} else if(screenwidth == 720){
					//alert("this is from Nexus "+screenwidth);
				 	bannerTmgin = 48;  
				}
			}
			
			if(formNameStr == "frmMBAnyIdRegCompleted"){
				if(frmMBAnyIdRegCompleted.lblAnyIdSuccess1stLaunch.isVisible == true){
				 	bannerTmgin = bannerTmgin-34;
				 	if (locale == "en_US"){
						bannerTmgin = bannerTmgin - 2;
					}
				}
				if(frmMBAnyIdRegCompleted.lblAnyIdSuccess2ndLaunch.isVisible == true){
				 	bannerTmgin = bannerTmgin-11;
				 	if (locale == "en_US"){
						bannerTmgin = bannerTmgin - 2;
					}
				}  
				if(frmMBAnyIdRegCompleted.lblAnyIdFailure.isVisible == true){
				 	bannerTmgin = bannerTmgin-8;
				}  
				if(frmMBAnyIdRegCompleted.lblAnyIdSuccess.isVisible == true){
				 	if (locale == "en_US"){
						if (deviceModel == "Nexus 4") {
							bannerTmgin = bannerTmgin - 12; // Nexus 4 (-12)

						} else if (deviceModel == "GT-I9500") { 
							bannerTmgin = bannerTmgin - 12 ; // Samsung Galaxy S4
						
						} else if (deviceModel == "iPhone 6 Plus") {
							  bannerTmgin = bannerTmgin - 5 ; 
						
						} else if (deviceModel == "iPhone 6") {
							 bannerTmgin = bannerTmgin - 11 ; 
					
					 	} else if (deviceModel == "4GS iPhone") { 
							  bannerTmgin = bannerTmgin - 9 ;
							  bannerRmgin = 3  ;
				    		  bannerLmgin = 3  ; 
							  
						} else{
							bannerTmgin = bannerTmgin - 12 ; 
						}

					} else if (locale == "th_TH"){  
						if (deviceModel == "Nexus 4") {
							bannerTmgin = bannerTmgin - 2 ;  // Nexus 4 (-2)

						} else if (deviceModel == "GT-I9500") {  
							bannerTmgin = bannerTmgin  ; // Samsung Galaxy S4

						} else if (deviceModel == "iPhone 6 Plus") {
							  bannerTmgin = bannerTmgin + 5 ; 
						
						} else if (deviceModel == "iPhone 6") {
							 bannerTmgin = bannerTmgin + 1 ;
						
						} else if (deviceModel == "4GS iPhone") {
							  bannerTmgin = bannerTmgin + 5 ;
							  bannerRmgin = 3  ;
					 		  bannerLmgin = 3  ; 
							  
						} else{
							bannerTmgin = bannerTmgin - 2 ; 
						}
					}
				}  
				if(bannerTmgin < bannerTmgin_min){bannerTmgin = bannerTmgin_min;}
				
				frmMBAnyIdRegCompleted.hbxAdv.margin=[bannerLmgin,bannerTmgin,bannerRmgin,bannerBmgin];
			
			}else if(formNameStr == "frmMBEStatementComplete"){ 
				bannerTmgin = bannerTmgin - 9;
				if(frmMBEStatementComplete.imgEStatementComplete.isVisible == true){
				 	if (locale == "en_US"){
				 		if (deviceModel == "iPhone 6 Plus") {
							  bannerTmgin = bannerTmgin + 7; 
						
						} else if (deviceModel == "iPhone 6") {
							 bannerTmgin = bannerTmgin + 4 ;
						
						} else if (deviceModel == "4GS iPhone") {
							  bannerTmgin = bannerTmgin + 6 ; 
						
						} else {
							bannerTmgin = bannerTmgin + 3;
						
						}
					} else if (locale == "th_TH"){  
						if (deviceModel == "iPhone 6 Plus") {
							  bannerTmgin = bannerTmgin + 8; 
						
						} else if (deviceModel == "iPhone 6") {
							 bannerTmgin = bannerTmgin + 4 ;
						
						} else if (deviceModel == "4GS iPhone") {
							  bannerTmgin = bannerTmgin + 6 ; 
							  
						} else {
							bannerTmgin = bannerTmgin + 4 ;
						}
					}
				} 
				
				if(bannerTmgin < bannerTmgin_min){ bannerTmgin = bannerTmgin_min; } 
				frmMBEStatementComplete.hbxAdv.margin=[bannerLmgin,bannerTmgin,bannerRmgin,bannerBmgin];
			
			}
		}	
		
	}
}