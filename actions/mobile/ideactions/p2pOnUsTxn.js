gblDeviceContactList = [];
searchList = [];
isRefreshPressed = false;
searchEligibleContactList = [];
deviceContactRefreshServerValue = 1;
isExpanded = false;
isAvoidDuplicateError = false;

function onClickOnUsAccountNumber(){
		isMenuShown = false;
		ResetTransferHomePage();
		gblSelTransferMode = 1;
		displayAmountTextBox(true);
		displayOnUsEnterMobileNumber(false);
		displayOnUsEnterBankAccount(true);
		displayP2PITMXBank(false);
		gblSelTransferFromAcctNo = frmTransferLanding.segTransFrm.selectedItems[0].accountNum;
		//updateFromAccountsFromService();
		setOnClickSetCanlenderBtn(true);
}

function onClickOnUsMobileNumber(){
		isMenuShown = false;
		gblPrevLen = 0;
		gblTransferFromRecipient =false;
		ResetTransferHomePage();
		gblSelTransferMode = 2;
		frmTransferLanding.lineMyNote.setVisibility(false);
		frmTransferLanding.hbxTranLandNotifyRec.setVisibility(false);
		frmTransferLanding.imgOnUsMob.src = "tran_smartphone.png";
		frmTransferLanding.imgOnUsMob.margin = [0,1,0,0];
		displayP2PITMXBank(false);
		displayAmountTextBox(false);
		displayOnUsEnterBankAccount(false);
		gblSelTransferFromAcctNo = frmTransferLanding.segTransFrm.selectedItems[0].accountNum;
		//updateFromAccountsFromService();
		resetDefaultDisplayAfterTDP2P(false);
		displayOnUsEnterCitizenID(false);
		displayOnUsEnterMobileNumber(true);
		setOnClickSetCanlenderBtn(true);
		frmTransferLanding.txtOnUsMobileNo.setFocus(true);
}

function onClickOnUsCitizen(){
		isMenuShown = false;
		gblPrevLen = 0;
		gblTransferFromRecipient =false;
		ResetTransferHomePage();
		gblSelTransferMode = 3;
		frmTransferLanding.lineMyNote.setVisibility(false);
		frmTransferLanding.hbxTranLandNotifyRec.setVisibility(false);
		frmTransferLanding.imgOnUsMob.src = "citizen_icon_id.png";
		frmTransferLanding.imgOnUsMob.margin = [0,3,0,0];
		displayP2PITMXBank(false);
		displayAmountTextBox(false);
		displayOnUsEnterBankAccount(false);
		gblSelTransferFromAcctNo = frmTransferLanding.segTransFrm.selectedItems[0].accountNum;
		//updateFromAccountsFromService();
		resetDefaultDisplayAfterTDP2P(false);
		setOnClickSetCanlenderBtn(true);
		displayOnUsEnterMobileNumber(false);
		displayOnUsEnterCitizenID(true);
		frmTransferLanding.txtCitizenID.setFocus(true);
}

function setOnClickSetCanlenderBtn(enabled){
	if(enabled){
		if(gblSelTransferMode == 1){
			frmTransferLanding.hbxTransCalendar.setEnabled(true);
			frmTransferLanding.imgArrw.src = "navarrowblue.png";
			frmTransferLanding.lblSchedSel.skin = "lblBlue48px";
			frmTransferLanding.lblSchedSel2.skin = "lblBlue48px";
		}else{ 
			frmTransferLanding.hbxTransCalendar.setEnabled(false);
			frmTransferLanding.imgArrw.src = "empty.png";
			frmTransferLanding.lblSchedSel.skin = "lblGrey48px";
			frmTransferLanding.lblSchedSel2.skin = "lblGrey48px";
		}
	}else{
		frmTransferLanding.hbxTransCalendar.setEnabled(false);
		frmTransferLanding.imgArrw.src = "empty.png";
		frmTransferLanding.lblSchedSel.skin = "lblGrey48px";
		frmTransferLanding.lblSchedSel2.skin = "lblGrey48px";
	}
}



function displayOnUsEnterMobileNumber(isShow){
	frmTransferLanding.hbxMobileBank.setVisibility(isShow);
	frmTransferLanding.lineInputMobileNo.setVisibility(isShow);
	frmTransferLanding.txtOnUsMobileNo.setVisibility(isShow);
	frmTransferLanding.txtOnUsMobileNo.text = "";
	frmTransferLanding.hbxOnUsInputMobileNo.setVisibility(isShow);
}

function displayOnUsEnterCitizenID(isShow){
	frmTransferLanding.hbxMobileBank.setVisibility(isShow);
	frmTransferLanding.hbxCitizenID.setVisibility(isShow);
	frmTransferLanding.lineInputMobileNo.setVisibility(isShow);
	frmTransferLanding.txtCitizenID.text = "";
}

function displayOnUsEnterBankAccount(isShow){
	frmTransferLanding.hbxRecipientBankDetails.isVisible = isShow;
	frmTransferLanding.lineRecipientDetails.isVisible = isShow;
}

function displayP2PITMXBank(isShow){
	frmTransferLanding.hbxITMXBank.setVisibility(isShow);
	frmTransferLanding.lineITMXBank.setVisibility(isShow);
}

function displayAmountTextBox(isShow){
	frmTransferLanding.hbxTranLandAmt.setVisibility(isShow);
	frmTransferLanding.lineAmount.setVisibility(isShow);
}

function getOnlyDigitMobileNumber(mobileNo){
	var newMobileNo = mobileNo;
	if(isNotBlank(mobileNo)){
		newMobileNo = mobileNo.replace(/\s/g, '');
		newMobileNo = newMobileNo.replace("+660", "0");
		newMobileNo = newMobileNo.replace("+66(0)", "0");
		newMobileNo = newMobileNo.replace("+66", "0");
		newMobileNo = removeSquareBrackets(newMobileNo.trim());
		newMobileNo = removeHyphenIB(newMobileNo);
	}
	return newMobileNo;
}

function populateContactsInToList() {

	//#ifdef android
		populateContactsInToListAndroid();
	//#endif

	//#ifdef iphone
		populateContactsInToListIPhone();
	//#endif
}

function populateContactsInToListAndroid() {


	if(isAndroidM()){
		var permissionsArr=[gblPermissionsJSONObj.CONTACTS_GROUP];
		//Creates an object of class 'PermissionFFI'
		var RuntimePermissionsChkFFIObject = new MarshmallowPermissionChecks.RuntimePermissionsChkFFI();
		//Invokes method 'checkpermission' on the object
		RuntimePermissionsChkFFIObject.checkPermissions(permissionsArr,null,contactCheckPermissionCallback);
	}else{
		getContactPicker();
	}
		
}

function contactCheckPermissionCallback(result) {
	if(result == "1"){
		getContactPicker();
	}else{
		showAlertWithCallBack(kony.i18n.getLocalizedString("keyContactPermission"), kony.i18n.getLocalizedString("info"), callBackMobileNoFieldsP2P);
	}
}

function populateContactsInToListIPhone() {

	var deviceInfo = kony.os.deviceInfo();
	var osVer = deviceInfo["version"];
	kony.print("OS Version: "+osVer);
	
	if(parseInt(osVer) < 9) {
		kony.print("Inside Custom Contacts");
		populateCustomContactList();
	} else {
		kony.print("Inside Native Contacts");
		displayContacts();
	}
}

function populateCustomContactList() {
 	showLoadingScreen();
	frmTransferToRecipentsMobile.lblMsg.setVisibility(false);
	frmTransferToRecipentsMobile.lblMsg.text = kony.i18n.getLocalizedString("keybillernotfound");
	frmTransferToRecipentsMobile.txbXferSearch.text = "";
	frmTransferToRecipentsMobile.txbXferSearch.placeholder = kony.i18n.getLocalizedString("keySearch");
	focusDeviceContactTab();
	resetMIBRecipientTab();
	checkDeviceStoreForP2PEligiblecontacts(true);
}

function isCacheEligibleForRefresh() {
	var serverFlag = parseInt(deviceContactRefreshServerValue);
	var deviceFlag = kony.store.getItem("deviceValue"); 
	if(!isNotBlank(deviceFlag) || parseInt(deviceFlag) < serverFlag) {
		kony.store.setItem("deviceValue", serverFlag); // Update to server flag as cache is reloaded
		return true;
	} else {
		return false;
	}
}

function permissionCheckResults(result, resultFormat) {
	if(result == "1"){
		gblContactPermission = true;
	}else{
		gblContactPermission = false;
	}
}

function checkDeviceStoreForP2PEligiblecontacts(fromLanding){
		showLoadingScreen();
		if(isCacheEligibleForRefresh()) {
			kony.store.removeItem("deviceLocalStoreContacts");
		}
		var p2pEligibleContacts = null;
		if(gblContactPermission){
			p2pEligibleContacts = kony.store.getItem("deviceLocalStoreContacts");
		}

		if(p2pEligibleContacts!=null && p2pEligibleContacts.length>0){
			loadContactsFromDeviceStore(p2pEligibleContacts);
			displayCannotFindFriendInList(false);
			frmTransferToRecipentsMobile.hbxUpdateContactList.setVisibility(true);
		}else{
			
			gblDeviceContactList = [];
			showDeviceContacts(fromLanding);
		}
		//dismissLoadingScreen();
}

function showDeviceContacts(fromLanding){
	showLoadingScreen();
	gblDeviceContactList  = [];
	var fnTemp = "temp";
	var fn = null;
	var ln = null;
	var mn = null;
	var photo = null;
	var phnos = new Array();
	var contactCount = 0;
	var noResults = true;
	frmTransferToRecipentsMobile.segTransferToRecipients.removeAll();
	frmTransferToRecipentsMobile.segTransferToRecipients.setVisibility(false);
	frmTransferToRecipentsMobile.lblMsg.setVisibility(false);
	frmTransferToRecipentsMobile.lblWantUpdateMsg.setVisibility(false);
	 
	var contacts = kony.contact.find("*", true);
	if (contacts == null || contacts == undefined  || "" == contacts || 0 == contacts.length) {
		searchEligibleContactList = [];
		kony.store.removeItem("deviceLocalStoreContacts");
		//kony.store.setItem("noContacts", "No Contacts In Device");
		if(fromLanding){
			frmTransferToRecipentsMobile.txbXferSearch.text = "";
			resetDeviceContactTab();
			focusMIBRecipientTab();
			calGetRecipientsForOnUs();
		}else{
			dismissLoadingScreen();
			frmTransferToRecipentsMobile.lblMsg.setVisibility(true);
			frmTransferToRecipentsMobile.lblMsg.text = kony.i18n.getLocalizedString("MIB_P2PTRErr_NoToMobNotAllow");
			frmTransferToRecipentsMobile.segTransferToRecipients.setVisibility(false);
			displayCannotFindFriendInList(false);
			frmTransferToRecipentsMobile.hbxUpdateContactList.setVisibility(true);
			displayPopGeneralMsgRefreshDone();
		}
	}else {
		for (var i = 0; i < contacts.length; i++) {
			if(contacts[i]["phone"] == null || contacts[i]["phone"] == undefined){
				phnos.push("NULL");
			}
			else{
				phnos.push(contacts[i]["phone"]);
			}
		}
		for (var i = 0; i < contacts.length; i++) {
			//break;
			if (contacts[i]["firstname"] == null || contacts[i]["firstname"] == "") fn = "";
			else fn = contacts[i]["firstname"];
			if(contacts[i]["middleName"] == null || contacts[i]["middleName"] == "") mn = "";
			else mn = contacts[i]["middleName"];
			if (contacts[i]["lastname"] == null || contacts[i]["lastname"] == "") ln = "";
			else ln = contacts[i]["lastname"];
			    
			if(isNotBlank(mn))
				fn = fn + " " + mn + " " + ln;
			else
				fn = fn + " " + ln;
			
			var m = 0;
			//
			var phoneNumbers = [];
			while (true) {
				if(phnos[i] == "NULL"){
					break;
				}
				 	if(phnos[i][m] == null || phnos[i][m] == undefined){
				 		break;
				 	}
				 	var mobVal = getOnlyDigitMobileNumber(phnos[i][m]["number"]);
				 	if(isNotBlank(mobVal) && mobVal.length == 10  && recipientMobileVal(mobVal)){
							phoneNumbers.push({"mobile":mobVal});
							noResults = false;
				 	}
				m++;
			}
			phoneNumbers = removeDuplicateFromArray(phoneNumbers);
			
			if(fn != "" && phoneNumbers.length>0){
				var displayNumber = "";
				if(phoneNumbers.length == 1){
					displayNumber = onEditMobileNumberP2P(phoneNumbers[0]["mobile"]);
				}else{
					displayNumber = phoneNumbers.length + " " + kony.i18n.getLocalizedString("MIB_P2P_TotalMobileNum");
				}
				
				if (contacts[i]["photorawbytes"] == null || contacts[i]["photorawbytes"] == "") photo = "";
				else photo = contacts[i]["photorawbytes"];
				if (photo == null || photo == "") {
						photo = "avatar_dis.png";
				}
				else {
					photo = {rawBytes: photo};
				}
				
				if (((fn.indexOf("<") != -1) || (fn.indexOf("&lt;") != -1)) && ((fn.indexOf(">") != -1) || (fn.indexOf("&gt;") != -1))) {
					fn = fn.replace(/\</g, "")
					fn = fn.replace(/\>/g, "")
					fn = fn.replace(/\&lt;/g, "")
					fn = fn.replace(/\&gt;/g, "")
				}
				
				
				var temp = {
							lblName: fn,
							lblAccountNum: displayNumber,
							imgArrow: "empty.png",
							imgprofilepic: photo,
							imgfav:"empty.png",
							imgmob: "empty.png",
							hiddenmain: "main",
							phNumbers:phoneNumbers,
							serialNo:contactCount+""
							//template: hbrt1  not accepting kony.store.setItem API.
							}
				gblDeviceContactList.push(temp);
				contactCount++;
			}
			
		}
		
		if(gblDeviceContactList.length > 0){
			gblDeviceContactList.sort(dynamicSortOther("lblName"));
			
			var contactsToService = [];
			for(var i=0;i<gblDeviceContactList.length;i++){
				gblDeviceContactList[i].serialNo = i+"";
				if(gblDeviceContactList[i].phNumbers.length >1){
					for(var j=0;j<gblDeviceContactList[i].phNumbers.length;j++){
							contactsToService.push(gblDeviceContactList[i].phNumbers[j]["mobile"]);
					}
				}else{
					contactsToService.push(gblDeviceContactList[i].lblAccountNum);
				}
			}	
			//frmTransferToRecipentsMobile.show();
			displayCannotFindFriendInList(false);
			frmTransferToRecipentsMobile.hbxUpdateContactList.setVisibility(true);
			resetMIBRecipientTab();
			focusDeviceContactTab();
			var contactListToggle1 = true;

			checkEligibilityForP2P(contactsToService);
		}
		else{
			searchEligibleContactList = [];
		    kony.store.removeItem("deviceLocalStoreContacts");
		    //kony.store.setItem("noContacts", "No Contacts In Device");
			if(fromLanding){
				frmTransferToRecipentsMobile.txbXferSearch.text = "";
				resetDeviceContactTab();
				focusMIBRecipientTab();
				calGetRecipientsForOnUs();
			}else{
				dismissLoadingScreen();
				if(noResults){
					frmTransferToRecipentsMobile.lblMsg.setVisibility(true);
					frmTransferToRecipentsMobile.lblMsg.text = kony.i18n.getLocalizedString("MIB_P2PTRErr_NoToMobNotAllow");
					displayCannotFindFriendInList(false);
				}else{
					displayCannotFindFriendInList(true);
				}
				frmTransferToRecipentsMobile.segTransferToRecipients.setVisibility(false);
				frmTransferToRecipentsMobile.hbxUpdateContactList.setVisibility(true);
				displayPopGeneralMsgRefreshDone();
			}
			//dismissLoadingScreen();
		}
	}
}

function onClickDeviceContacts(){
	if(frmTransferToRecipentsMobile.vbxContact.skin == "vbxBGGrey2"){
		frmTransferToRecipentsMobile.txbXferSearch.text = "";
		focusDeviceContactTab();
		resetMIBRecipientTab();
		checkDeviceStoreForP2PEligiblecontacts(false);
	}
}

function onClickMibRecipients(){
	if(frmTransferToRecipentsMobile.vbxRecipient.skin == "vbxBGGrey2"){
		frmTransferToRecipentsMobile.txbXferSearch.text = "";
		resetDeviceContactTab();
		focusMIBRecipientTab();
		calGetRecipientsForOnUs();
		
	}
}

function calGetRecipientsForOnUs(){
		var inputParam = {}
		showLoadingScreen();
		frmTransferToRecipentsMobile.segTransferToRecipients.removeAll();
		frmTransferToRecipentsMobile.segTransferToRecipients.isVisible = false;
		frmTransferToRecipentsMobile.lblMsg.isVisible = false;
		gblTransferToRecipientData =[];
		frmTransferToRecipentsMobile.hbxUpdateContactList.setVisibility(false);
		frmTransferToRecipentsMobile.segTransferToRecipients.setVisibility(false);
		displayCannotFindFriendInList(false);
		invokeServiceSecureAsync("getMibRecipientsForOnUs", inputParam, calGetRecipientsForOnUsCallBack);
}

function calGetRecipientsForOnUsCallBack(status, resulttable){
	//success responce
	if (status == 400) {
		var mobVal = getOnlyDigitMobileNumber(gblPHONENUMBER);
		if (resulttable["RecepientsList"].length>0 || (isNotBlank(mobVal) && mobVal.length == 10  && recipientMobileVal(mobVal))) {
			var recipientList = resulttable["RecepientsList"];
			var recSegmentData = [];
			var favimg ="";
			var randomnum = Math.floor((Math.random()*10000)+1); 
			gblMyProfilepic = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/ImageRender?billerId=&crmId=Y&personalizedId=";
			var profilePicture = gblMyProfilepic + "&rr="+randomnum
			var contactsToService = [];
			var mobVal = getOnlyDigitMobileNumber(gblPHONENUMBER);
			if(isNotBlank(mobVal) && mobVal.length == 10  && recipientMobileVal(mobVal)){
				contactsToService.push(gblPHONENUMBER);
				recSegmentData.push({
						lblName: gblCustomerName,
						lblAccountNum: onEditMobileNumberP2P(gblPHONENUMBER),
						imgArrow: "empty.png",
						imgprofilepic: profilePicture,
						imgmob: "empty.png",
						imgfav:"empty.png",
						hiddenmain: "own",
						personalizedId: "",
						template: hbrt1
							});
			}
			for (var i = 0; i < recipientList.length; i++) {
				var rcImg = "";
				var profilePic = recipientList[i].personalizedPicId;
				var XferRcURL =gblMyProfilepic;
				if (recipientList[i].personalizedFavFlag != null &&
					recipientList[i].personalizedFavFlag=="Y") {
					favimg = "starblue.png";
				} else {
					favimg = "empty.png";
				}
				if (profilePic == null || profilePic == "") {
					rcImg = XferRcURL + recipientList[i].personalizedId +"&rr="+randomnum;
				} else {
					var check = kony.string.endsWith(profilePic + "", "nouserimg.jpg", true)
					if (check == true) {
						rcImg = XferRcURL + recipientList[i].personalizedId +"&rr="+randomnum;
					} else {
						var check = kony.string.startsWith(profilePic + "", "http", true)
						if (check == true) {
							rcImg = profilePic;
						} else {
							rcImg = XferRcURL + recipientList[i].personalizedId +"&rr="+randomnum;									
						}
					}
				}
				var mobileNumber = onEditMobileNumberP2P(recipientList[i].personalizedMobileNum.trim());
				var temp = {
						lblName: recipientList[i].personalizedName,
						lblAccountNum: mobileNumber,
						imgArrow: favimg,
						imgprofilepic: rcImg,
						imgfav:"empty.png",
						imgmob: "empty.png",
						hiddenmain: "main",
						personalizedId: recipientList[i].personalizedId,
						template: hbrt1
				}
				
				var mobVal = getOnlyDigitMobileNumber(recipientList[i].personalizedMobileNum.trim());
				if(isNotBlank(mobVal) && mobVal.length == 10  && recipientMobileVal(mobVal)){				
					contactsToService.push(recipientList[i].personalizedMobileNum.trim());
					recSegmentData.push(temp);
				}
			}
			gblTransferToRecipientData =[];
			gblTransferToRecipientData = recSegmentData;
			//frmTransferToRecipentsMobile.show();
			checkEligibilityForP2P(contactsToService);
		}else{
			dismissLoadingScreen();
			if(kony.application.getCurrentForm().id == "frmTransferToRecipentsMobile"){
				frmTransferToRecipentsMobile.lblMsg.isVisible = true;
				frmTransferToRecipentsMobile.lblMsg.text = kony.i18n.getLocalizedString("MIB_P2PTRErr_NoToMobMIB");
				frmTransferToRecipentsMobile.segTransferToRecipients.setVisibility(false);
			
			}else{
				if(ITMX_TRANSFER_ENABLE == "true") {
					showAlert(kony.i18n.getLocalizedString("MIB_P2PTRErr_NoToMobBoth"), kony.i18n.getLocalizedString("info"));	
				} else {
					showAlert(kony.i18n.getLocalizedString("MIB_P2PErrNoLinkAcct"), kony.i18n.getLocalizedString("info"));	
				}
			}
		}
	}
}

function resetDeviceContactTab(){
	frmTransferToRecipentsMobile.vbxContact.skin = "vbxBGGrey2";
	frmTransferToRecipentsMobile.vbxContact.focusSkin = "vbxBGGrey2";
	frmTransferToRecipentsMobile.lblDeviceContact.skin = "lblGrayMedium142";
	frmTransferToRecipentsMobile.imgArrowDevice.src = "arrowtoplightgrey.png";
}
function focusDeviceContactTab(){
	frmTransferToRecipentsMobile.vbxContact.skin = "vbxTabFocus";
	frmTransferToRecipentsMobile.vbxContact.focusSkin = "vbxTabFocus";
	frmTransferToRecipentsMobile.lblDeviceContact.skin = "lblWhite142";
	frmTransferToRecipentsMobile.imgArrowDevice.src = "arrowtoplightblue.png";
	setEnableRefreshBtn(true)
}
function resetMIBRecipientTab(){
	frmTransferToRecipentsMobile.vbxRecipient.skin = "vbxBGGrey2";
	frmTransferToRecipentsMobile.vbxRecipient.focusSkin = "vbxBGGrey2";
	frmTransferToRecipentsMobile.lblMiBRecipients.skin = "lblGrayMedium142";
	frmTransferToRecipentsMobile.imgArrowMiBRec.src = "arrowtoplightgrey.png";
}
function focusMIBRecipientTab(){
	frmTransferToRecipentsMobile.vbxRecipient.skin = "vbxTabFocus";
	frmTransferToRecipentsMobile.vbxRecipient.focusSkin = "vbxTabFocus";
	frmTransferToRecipentsMobile.lblMiBRecipients.skin = "lblWhite142";
	frmTransferToRecipentsMobile.imgArrowMiBRec.src = "arrowtoplightblue.png";
	setEnableRefreshBtn(false)
}

function returnfullData(index,data){
		var totalConatcts = [];
		var contactList = [];
		if(frmTransferToRecipentsMobile.txbXferSearch.text.trim().length > 0){
			contactList = searchList;
		}else{
			contactList = gblDeviceContactList;
		}
		if(contactList.length > 0){
		var temp={};
			for(var k=0;k < contactList.length;k++){
				
				if(contactList[k].serialNo == index+""){
					temp.template = hbrt5;
					temp.imgArrow = "navdownarrow.png";
					temp.imgHeaderLeft = "arrowtoplightblue.png";
					temp.lblName = contactList[k].lblName;
					temp.lblAccountNum = contactList[k].lblAccountNum;
					temp.imgprofilepic = contactList[k].imgprofilepic;
					if(contactList[k].imgfav == "tmb_recipient.png")
						temp.imgfav = "tmb_recipient_white.png";
					else 
						temp.imgfav = contactList[k].imgfav;
					temp.imgmob = contactList[k].imgmob;
					temp.hiddenmain = contactList[k].hiddenmain;
					temp.phNumbers = contactList[k].phNumbers;
					temp.serialNo = contactList[k].serialNo;
					totalConatcts.push(temp);
					for(var j=0;j < data.length;j++){
						totalConatcts.push(data[j]);
					}
				}else{
					totalConatcts.push(contactList[k]);
				}
			}
		}
		return totalConatcts;
}

function onRowClickMibRecipientSegment(){
	var selectData = frmTransferToRecipentsMobile.segTransferToRecipients.selectedItems[0];
	var selectedIndex = frmTransferToRecipentsMobile.segTransferToRecipients.selectedIndex;
	var filterResult = false;
	if(frmTransferToRecipentsMobile.txbXferSearch.text.trim().length > 0){
		filterResult = true;
	}
	frmTransferLanding.hbxOnUsNotifyRecipient.setVisibility(false);
	if(frmTransferToRecipentsMobile.vbxRecipient.skin == "vbxBGGrey2"){// if device contacts
		//device contacts
		if(selectData.template == hbrt1){
			var phoneNumbers = selectData.phNumbers;
			if(phoneNumbers.length > 1){// expand row if more than one phone number
					var phoneNumbersRows = [];
						for(var i=0;i < phoneNumbers.length;i++){
								var onUsLogo = "empty.png";
								if(phoneNumbers[i]["OnUsFlag"] == "Y"){
									onUsLogo = "tmb_recipient.png";
								}
								var childMobile = onEditMobileNumberP2P(phoneNumbers[i]["mobile"]);
								var tempPhRow = {
									img2: "mobileicotrf.png",
									lblMobile: childMobile,
									imgfav: onUsLogo,
									hiddenmain: "sub",
									template: hbxRecMobile,
									contactName: selectData.lblName,
									imgprofilepic:selectData.imgprofilepic,
									AcctIdentValue :phoneNumbers[i]["AcctIdentValue"],
									OnUsFlag: phoneNumbers[i]["OnUsFlag"],
									AcctTitle : phoneNumbers[i]["AcctTitle"],
									ITMXFlag: phoneNumbers[i]["ITMXFlag"]
									
								}
								phoneNumbersRows.push(tempPhRow);
						}
					frmTransferToRecipentsMobile.segTransferToRecipients.widgetDataMap = {
						lblName: "lblName",
						lblAccountNum: "lblAccountNum",
						imgArrow: "imgArrow",
						imgprofilepic: "imgprofilepic",
						imgmob: "imgmob",
						imgfb: "imgfb",
						hiddenmain: "hiddenmain",
						lbl3: "lbl3",
						lblMobile: "lblMobile",
						lbl5: "lbl5",
						lblBankName: "lblBankName",
						img2: "img2",
						img3: "img3",
						imgfav:"imgfav",
						imgHeaderLeft:"imgHeaderLeft"
						
					}
					frmTransferToRecipentsMobile.segTransferToRecipients.removeAll();
					frmTransferToRecipentsMobile.segTransferToRecipients.setData(returnfullData(selectData.serialNo,phoneNumbersRows));
					if(isExpanded){
						selectedIndex[1] -= phoneNumbersRows.length;
					}
					else{
						isExpanded = true;
					}
					frmTransferToRecipentsMobile.segTransferToRecipients.selectedIndex = selectedIndex;
				
			}else if (checkMobileEligibleForP2p(selectData)){// if click ony  one mobile number conatct  
				isOwnAccountP2P = false;
				frmTransferLanding.txtOnUsMobileNo.text = selectData.lblAccountNum;
				frmTransferLanding.lblRecipientName.text = selectData.lblName;
				frmTransferLanding.hbxITMXBank.isVisible = false;
				frmTransferLanding.lineITMXBank.isVisible = false;
				checkCallCheckOnUsPromptPayinqServiceMB(true);
			}
		}else if(selectData.template == hbrt5){// un expand row
					isExpanded = false;
					frmTransferToRecipentsMobile.segTransferToRecipients.removeAll();
					if(filterResult){
						frmTransferToRecipentsMobile.segTransferToRecipients.setData(searchList);
					}else{
						frmTransferToRecipentsMobile.segTransferToRecipients.setData(gblDeviceContactList);
					}
					frmTransferToRecipentsMobile.segTransferToRecipients.selectedIndex = selectedIndex;
		}else if(checkMobileEligibleForP2p(selectData)){// if click expanded mobile number
			isOwnAccountP2P = false;
			frmTransferLanding.txtOnUsMobileNo.text = selectData.lblMobile;
			frmTransferLanding.lblRecipientName.text = selectData.lblName;
			checkCallCheckOnUsPromptPayinqServiceMB(true);
		}
	}else if(checkMobileEligibleForP2p(selectData)){// if mib recipients on clicck
		frmTransferLanding.txtOnUsMobileNo.text = selectData.lblAccountNum;
		frmTransferLanding.lblRecipientName.text = selectData.lblName;
		if(selectData.hiddenmain == "own"){
			isOwnAccountP2P = true;
		}else{
			isOwnAccountP2P = false;
		}
		checkCallCheckOnUsPromptPayinqServiceMB(true);
	}
}

function checkCallCheckOnUsPromptPayinqServiceMB(fromMobileNo){
	var enteredAmt = frmTransferLanding.txtTranLandAmt.text;
	var mobileOrCitizenID = "";
	if(gblSelTransferMode == 2){
		mobileOrCitizenID = frmTransferLanding.txtOnUsMobileNo.text;
	} else if(gblSelTransferMode == 3){
		mobileOrCitizenID = frmTransferLanding.txtCitizenID.text;
	}
	if(isNotBlank(enteredAmt) && parseFloat(enteredAmt, 10) > 0 ){
		if(!kony.string.equalsIgnoreCase(frmTransferLanding.lblMobileNoTemp.text, mobileOrCitizenID)){
			displayP2PITMXBank(false);
			callCheckOnUsPromptPayinqServiceMB();
		}else{
			displayP2PITMXBank(true);
			var currentFormId = kony.application.getCurrentForm();
			if (currentFormId.id != "frmTransferLanding") {
				displayOwnerNotifyRecipientP2P();
				frmTransferLanding.show();
			}
		}
		if(fromMobileNo){
			clearNotifyRecipientfields();
		}
	}else{
		displayP2PITMXBank(false);
		if(kony.string.equalsIgnoreCase(frmTransferLanding.lblMobileNoTemp.text, mobileOrCitizenID)){
			displayP2PITMXBank(true);
		}
		frmTransferLanding.show();
		clearNotifyRecipientfields();
		displayAmountTextBox(true);
		setFocusAmountP2P();
	}
}

function setFocusAmountP2P(){
	var amount = frmTransferLanding.txtTranLandAmt.text;
	if(!isNotBlank(amount) || parseFloat(amount) < 0){
		frmTransferLanding.txtTranLandAmt.setFocus(true);
	}
}

function checkMobileEligibleForP2p(selectData){
	/*if(selectData.OnUsFlag == "Y"){
		gblPaynow = true;
		gblisTMB = "11";
		gblBANKREF = getBankNameCurrentLocaleTransfer(gblisTMB);
	}else{
		gblisTMB = "";
	}
	frmTransferLanding.show();
	frmTransferLanding.txtTranLandAmt.setFocus(true);*/
	return true;
}

function invokeServiceP2PITMX(fromMobileNo){
	showLoadingScreen();
	//var mobileNumber = removeHyphenIB(frmTransferLanding.txtOnUsMobileNo.text);
	frmTransferLanding.lineITMXBank.isVisible = false;
	frmTransferLanding.lineInputMobileNo.isVisible = true;
	//frmTransferLanding.lineInputMobileNo.skin = "lineBlue";
	checkCallCheckOnUsPromptPayinqServiceMB(fromMobileNo);
}

function frmTranfersToRecipentsMobilePreshow() {
	frmTransferToRecipentsMobile.lblHdrTxt.text = kony.i18n.getLocalizedString("Transfer_selectRecipient");
	frmTransferToRecipentsMobile.lblDeviceContact.text = kony.i18n.getLocalizedString("MIB_P2PDevice");
	frmTransferToRecipentsMobile.lblMiBRecipients.text =  kony.i18n.getLocalizedString("MIB_P2PRecipient");
	frmTransferToRecipentsMobile.txbXferSearch.placeholder = kony.i18n.getLocalizedString("keySearch");
	frmTransferToRecipentsMobile.lblWantUpdateMsg.text = kony.i18n.getLocalizedString("MIB_BPkeySearchNotFound");
	frmTransferToRecipentsMobile.button967430408331992.text = kony.i18n.getLocalizedString("MIB_BPkeyUpdateContact");
	frmTransferToRecipentsMobile.btnTranLandNext.text = kony.i18n.getLocalizedString("MIB_BPkeyUpdateContact");
}




function onEditMobileNumberP2P(txt) {
	if (txt == null) return false;
	var numChars = txt.length;
	var temp = "";
	var i, txtLen = numChars;
	var currLen = numChars;
	var iphenText = "";
	var startIdx = 0;
	if (startIdx < currLen) {
		for (i = 0; i < numChars; ++i) {
			if (txt[i] != '-') {
				temp = temp + txt[i];
			} else {
				txtLen--;
			}
		}
		for (i = 0; i < txtLen; i++) {
			iphenText += temp[i];
			if (i == 2 || i == 5) {
				iphenText += '-';
			}
		}
	}
	//gblPrevLen = currLen;
	return iphenText;
}

function displayHbxNotifyRecipient(isShow){
	frmTransferLanding.lineMyNote.setVisibility(isShow);
	if(!isShow){// Not show
		frmTransferLanding.hbxOnUsNotifyRecipient.setVisibility(isShow);
		frmTransferLanding.hbxTranLandNotifyRec.setVisibility(isShow);
	}else{ // show 
		if(gblSelTransferMode == 2){
			if(frmTransferLanding.hbxITMXBank.isVisible){
				frmTransferLanding.hbxOnUsNotifyRecipient.setVisibility(isShow);
				frmTransferLanding.hbxTranLandNotifyRec.setVisibility(!isShow);
			}else{
				frmTransferLanding.hbxOnUsNotifyRecipient.setVisibility(!isShow);
				frmTransferLanding.hbxTranLandNotifyRec.setVisibility(!isShow);
			}
		}else{
			frmTransferLanding.hbxOnUsNotifyRecipient.setVisibility(!isShow);
			if((gblTrasSMS == 1 && gblTransEmail == 1) || isOwnAccountP2P){
				frmTransferLanding.hbxTranLandNotifyRec.setVisibility(false);
			}else{
				frmTransferLanding.hbxTranLandNotifyRec.setVisibility(isShow);
			}
		}
	}
}

function onTextChangeToMobileNoP2P(txt){
	if (txt == null) return false;
	var numChars = txt.length;
	var temp = "";
	var i, txtLen = numChars;
	var currLen = numChars;
	if (gblPrevLen < currLen) {
		for (i = 0; i < numChars; ++i) {
			if (txt[i] != '-') {
				temp = temp + txt[i];
			} else {
				txtLen--;
			}
		}
		var iphenText = "";
		for (i = 0; i < txtLen; i++) {
			iphenText += temp[i];
			if (i == 2 || i == 5) {
				iphenText += '-';
			}
		}
		frmTransferLanding.txtOnUsMobileNo.text = iphenText;
	}
	gblPrevLen = currLen;
	displayP2PITMXBank(false);
	var enteredAmt = frmTransferLanding.txtTranLandAmt.text;
	if(frmTransferLanding.hbxRecievedBy.isVisible || (isNotBlank(enteredAmt) && parseFloat(enteredAmt, 10) > 0 )) {
		displayAmountTextBox(true);
	}else{
		displayAmountTextBox(false);
	}
	
	var mobileNumber = frmTransferLanding.txtOnUsMobileNo.text;
	if(isNotBlank(mobileNumber)){
		mobileNumber = removeHyphenIB(mobileNumber);
		if(!isNotBlank(frmTransferLanding.lblMobileNoTemp.text)){
			frmTransferLanding.lblMobileNoTemp.text = "";
		}
		if(mobileNumber.length == 10){
			frmTransferLanding.lblRecipientName.text = "";
			if(!kony.string.equalsIgnoreCase(frmTransferLanding.lblMobileNoTemp.text, frmTransferLanding.txtOnUsMobileNo.text)){
				isOwnAccountP2P = false;
				//validate mobile number
				if (!recipientMobileVal(mobileNumber)){
					showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_P2PkeyEnteredMobileNumberisnotvalid"), 
						kony.i18n.getLocalizedString("info"), callBackMobileNoFieldsP2P);
				}else{
					if(isNotBlank(enteredAmt) && parseFloat(enteredAmt, 10) > 0 ){
						invokeServiceP2PITMX(true);
					}else{
						//frmTransferLanding.lineInputMobileNo.skin = "lineBlue";
						displayAmountTextBox(true);
						
						//MIB-5814 - R75-MB Should display auto numeric key pad when edit mobile number 
						kony.timer.schedule("contactTimer", setFocusAmountP2P, 0.5, false);
						//setFocusAmountP2P();
					}
				}
			}else{
				displayP2PITMXBank(true);
				//frmTransferLanding.lineInputMobileNo.skin = "lineBlue";

				//MIB-5814 - R75-MB Should display auto numeric key pad when edit mobile number 
				kony.timer.schedule("contactTimer", setFocusAmountP2P, 0.5, false);
				//setFocusAmountP2P();
			}
		}else{
			//frmTransferLanding.lineInputMobileNo.skin = "linePopupBlack";
		}
	}
}

function onTextChangeToMobileNoSmS(txt){
	if (txt == null) return false;
	var numChars = txt.length;
	var temp = "";
	var i, txtLen = numChars;
	var currLen = numChars;
	if (gblPrevLen < currLen) {
		for (i = 0; i < numChars; ++i) {
			if (txt[i] != '-') {
				temp = temp + txt[i];
			} else {
				txtLen--;
			}
		}
		var iphenText = "";
		for (i = 0; i < txtLen; i++) {
			iphenText += temp[i];
			if (i == 2 || i == 5) {
				iphenText += '-';
			}
		}
		frmTransferLanding.txtTransLndSms.text = iphenText;
	}
	gblPrevLen = currLen;
}

function validateMobileNumberForSMSNotify(){
	var mobileNumber = removeHyphenIB(frmTransferLanding.txtTransLndSms.text);
	if (!isNotBlank(mobileNumber)) {
		showAlertWithCallBack(kony.i18n.getLocalizedString("keyEnterMobileNum"), 
			kony.i18n.getLocalizedString("info"), callBackTransLndSmsFields);
		return false;
	}
	if (!recipientMobileVal(mobileNumber)){
		showAlertWithCallBack(kony.i18n.getLocalizedString("keyEnteredMobileNumberisnotvalid"), 
			kony.i18n.getLocalizedString("info"), callBackTransLndSmsFields);
		return false;
	}
	return true;
}

function searchP2PContacts() {
	var searchText = frmTransferToRecipentsMobile.txbXferSearch.text;
	searchText = searchText.toLowerCase();
	var isDevice = false;
	var isMIB = false;
	if(frmTransferToRecipentsMobile.vbxRecipient.skin == "vbxBGGrey2") {
		var recipientsSegData = searchEligibleContactList;
		isDevice = true;
	} else {
		var recipientsSegData = gblTransferToRecipientData;
		isMIB = true;
	}
	if((isDevice && isNotBlank(recipientsSegData) && recipientsSegData.length > 0) 
		|| (isMIB && isNotBlank(recipientsSegData) && recipientsSegData.length > 0)){
		var searchOnDevice = false;
		if(isDevice) searchOnDevice = true;
			searchList = searchResultOfP2PContacts(recipientsSegData, searchText, searchOnDevice);
		
		if(frmTransferToRecipentsMobile.segTransferToRecipients.data != null && frmTransferToRecipentsMobile.segTransferToRecipients.data != undefined){
			if(frmTransferToRecipentsMobile.segTransferToRecipients.data.length != searchList.length){
				frmTransferToRecipentsMobile.segTransferToRecipients.setData(searchList);
			}
		}else{
			frmTransferToRecipentsMobile.segTransferToRecipients.setData(searchList);
		}
	}
}


function searchResultOfP2PContacts(recipientsSegData, searchText, searchOnDevice) {
	var searchList = [];
	var recipientsSegDataLength = recipientsSegData.length;
	var recipientName = "";
	var recipientNumber = "";
	if (recipientsSegDataLength > 0) { // Atleast one recipient should be avilable
		if (searchText.length >= 1) { //Atleast 3 characters should be entered in search box
			for (j = 0, i = 0; i < recipientsSegDataLength; i++) {
				
				recipientName = recipientsSegData[i].lblName;
				recipientNumber = removeHyphenIB(recipientsSegData[i].lblAccountNum);
				
				if (recipientName.toLowerCase().indexOf(searchText) > -1) {
					searchList[j] = recipientsSegData[i];
					j++;
				}else{
					if(searchOnDevice){
						var phoneNumbers = recipientsSegData[i].phNumbers;
						for(var k = 0; k < phoneNumbers.length; k++){
							recipientNumber = removeHyphenIB(phoneNumbers[k]["mobile"]);
						 	if(recipientNumber.indexOf(searchText) > -1){
								searchList[j] = recipientsSegData[i];
								j++;
								break;
						 	}
						}
					}else if (recipientNumber.indexOf(searchText) > -1) {
						searchList[j] = recipientsSegData[i];
						j++;
					}
				}
			}//For Loop End
			if(frmTransferToRecipentsMobile.vbxContact.skin == "vbxTabFocus"){
				displayCannotFindFriendInList(true);
				frmTransferToRecipentsMobile.hbxUpdateContactList.setVisibility(false);
			}else{
				displayCannotFindFriendInList(false);
				frmTransferToRecipentsMobile.hbxUpdateContactList.setVisibility(false);
			}
			if(searchList.length == 0) {
				frmTransferToRecipentsMobile.segTransferToRecipients.setVisibility(false);
				frmTransferToRecipentsMobile.lblMsg.text = kony.i18n.getLocalizedString("keybillernotfound");
				frmTransferToRecipentsMobile.lblMsg.setVisibility(true);
			} else {
				frmTransferToRecipentsMobile.segTransferToRecipients.setVisibility(true);
				frmTransferToRecipentsMobile.lblMsg.text = "";
				frmTransferToRecipentsMobile.lblMsg.setVisibility(false);
			}
		} else {//searchText condition End
			//If you Clear the Search Text then show All Transactions
			if(frmTransferToRecipentsMobile.vbxContact.skin == "vbxTabFocus") {
				searchList = searchEligibleContactList;
				frmTransferToRecipentsMobile.hbxUpdateContactList.setVisibility(true);
				displayCannotFindFriendInList(false);
			} else {
				searchList = gblTransferToRecipientData;
				frmTransferToRecipentsMobile.hbxUpdateContactList.setVisibility(false);
				displayCannotFindFriendInList(false);
			}
			if(searchList.length > 0){
				frmTransferToRecipentsMobile.segTransferToRecipients.setVisibility(true);
				frmTransferToRecipentsMobile.lblMsg.setVisibility(false);
			}
		}
	}//recipientsSegDataLength condition End
	return searchList;
}

function displayCannotFindFriendInList(isShow){
	frmTransferToRecipentsMobile.hbxNotFoundUpdatecontactList.setVisibility(isShow);
	frmTransferToRecipentsMobile.lblWantUpdateMsg.setVisibility(isShow);
}

function validateTransferFieldsP2P(){

	if(gblSelTransferMode == 2){
		if(!isNotBlank(frmTransferLanding.txtOnUsMobileNo.text)){
			showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_P2PTRErr_ToAcc"), 
				kony.i18n.getLocalizedString("info"), callBackMobileNoFieldsP2P);
			return false;
		}else{
			if (!recipientMobileVal(removeHyphenIB(frmTransferLanding.txtOnUsMobileNo.text))){
				showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_P2PkeyEnteredMobileNumberisnotvalid"), 
					kony.i18n.getLocalizedString("info"), callBackMobileNoFieldsP2P);
				return false;
			}
		}
	} else if(gblSelTransferMode == 3){
		var citizenID = removeHyphenIB(frmTransferLanding.txtCitizenID.text);
		if(!isNotBlank(citizenID)){
			showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_P2PTRErr_ToCI"), 
				kony.i18n.getLocalizedString("info"), callBackCitizenIDFieldsP2P);
			return false;
		}else{
			if (!checkCitizenID(citizenID)){
				showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_P2PkeyCIisnotvalid"), 
					kony.i18n.getLocalizedString("info"), callBackCitizenIDFieldsP2P);
				return false;
			}
		}
	}

	
	// Start #MIB-2274 : prevent double error on android device
	if (!isNotBlank(frmTransferLanding.txtTranLandAmt.text)) {
		showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_P2PkeyPleaseEnterAmount"), 
			kony.i18n.getLocalizedString("info"), callBackAmountFields);
		//frmTransferLanding.lineAmount.skin = linePopupBlack;
		return false;
	}
	
	var isCrtFormt;
 	enteredAmount = frmTransferLanding.txtTranLandAmt.text;
 	isCrtFormt = amountValidationMB(enteredAmount);
 	if(!isCrtFormt){
 		if (parseFloat(enteredAmount, 10) == 0) {
			showAlertWithCallBack(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), 
					kony.i18n.getLocalizedString("info"), callBackAmountFields);
			frmTransferLanding.txtTranLandAmt.text = "";
			//frmTransferLanding.lineAmount.skin = linePopupBlack;
			return false;
		}
 	}
	//frmTransferLanding.lineAmount.skin = lineBlue;
	
	enteredAmount = kony.string.replace(enteredAmount, ",", "");
	frmTransferLanding.txtTranLandAmt.text = numberWithCommas(fixedToTwoDecimal(enteredAmount));
    enteredAmount = fixedToTwoDecimal(enteredAmount);
	
	// End #MIB-2274
	//MIB-4884-Allow special characters for My Note and Note to recipient field
	/*
	if (isNotBlank(frmTransferLanding.txtTranLandMyNote.text)){
		if(!MyNoteValid(frmTransferLanding.txtTranLandMyNote.text)){		
			showAlert(kony.i18n.getLocalizedString("MIB_TRkeyMynoteInvalid"), 
				kony.i18n.getLocalizedString("info"));
			return false;
		}		
	}
	*/
	if (isNotBlank(frmTransferLanding.txtTranLandMyNote.text)){
		if(checkSpecialCharMyNote(frmTransferLanding.txtTranLandMyNote.text)){		
			showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_MyNoteInvalidSpecialChar"), 
					kony.i18n.getLocalizedString("info"), callBackMyNoteFields);	
			return false;
		}		
	}
	if (gblTrasSMS == 1) {
		if(!validateMobileNumberForSMSNotify()){
		 	return false;
		}else{
			if(frmTransferLanding.txtTranLandRecNote.text == ""){
				showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_P2PkeyPleaseEnterRcipientNote"), 
					kony.i18n.getLocalizedString("info"), callBackTranLandRecNoteFields);
				return false;
			}
			// End #MIB-2274
			//MIB-4884-Allow special characters for My Note and Note to recipient field
			/*
			if (isNotBlank(frmTransferLanding.txtTranLandRecNote.text)){
				if(!MyNoteValid(frmTransferLanding.txtTranLandRecNote.text)){		
					showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_TRkeyRecNoteInvalid"), 
						kony.i18n.getLocalizedString("info"), callBackTranLandRecNoteFields);
					return false;
				}		
			}
			*/
			if (isNotBlank(frmTransferLanding.txtTranLandRecNote.text)){
				if(checkSpecialCharMyNote(frmTransferLanding.txtTranLandRecNote.text)){		
					showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_MyNoteInvalidSpecialChar"), 
						kony.i18n.getLocalizedString("info"), callBackTranLandRecNoteFields);
					return false;
				}		
			}
		}
	}
	
	if(gblSelTransferMode == 3){
		if (gblTransEmail == 1) {
			var emailBlankChk = frmTransferLanding.txtTransLndSmsNEmail.text;
			var isValidEmail = validateEmail(frmTransferLanding.txtTransLndSmsNEmail.text);
			
			if (!isNotBlank(emailBlankChk)){
				showAlertWithCallBack(kony.i18n.getLocalizedString("transferEmailID"), 
					kony.i18n.getLocalizedString("info"), callBackTransLndSmsNEmailFields);
				return false;
			}
			if (isValidEmail == false) {
				showAlertWithCallBack(kony.i18n.getLocalizedString("keyPleaseEnterValidEMAILID"), 
					kony.i18n.getLocalizedString("info"), callBackTransLndSmsNEmailFields);
				return false;
			}
			
			if(frmTransferLanding.textRecNoteEmail.text == ""){
				showAlertWithCallBack(kony.i18n.getLocalizedString("keyPleaseEnterRcipientNote"), 
					kony.i18n.getLocalizedString("info"), callBankRecNoteEmailFields);
				return false;
			}
			// End #MIB-2274
			//MIB-4884-Allow special characters for My Note and Note to recipient field
			/*
			if (isNotBlank(frmTransferLanding.textRecNoteEmail.text)){
				if(!MyNoteValid(frmTransferLanding.textRecNoteEmail.text)){		
					showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_TRkeyRecNoteInvalid"), 
						kony.i18n.getLocalizedString("info"), callBankRecNoteEmailFields);
					return false;
				}		
			}
			*/
			if (isNotBlank(frmTransferLanding.textRecNoteEmail.text)){
				if(checkSpecialCharMyNote(frmTransferLanding.textRecNoteEmail.text)){		
					showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_MyNoteInvalidSpecialChar"), 
						kony.i18n.getLocalizedString("info"), callBankRecNoteEmailFields);
					return false;
				}		
			}
		}
	}
	return true;
}

function displayNotEligibleNumber(){
	var errorText = kony.i18n.getLocalizedString("MIB_P2PkeyErrMobtNoInvalid");
	errorText = errorText.replace("{mobile_no}", frmTransferLanding.txtOnUsMobileNo.text);
	frmTransferLanding.txtOnUsMobileNo.text = "";
	showAlertWithCallBack(errorText, kony.i18n.getLocalizedString("info"), callBackMobileNoFieldsP2P);
}

function verifyExceedAvalibleBalance(){
	var availableBal;
	var i = gbltranFromSelIndex[1];
	var fromData = frmTransferLanding.segTransFrm.data;
	
	availableBal = frmTransferLanding.segTransFrm.selectedItems[0].lblBalance;
	//for product code 225 and 226
    gblNofeeVar = fromData[i].prodCode;
	//availableBal = fromData[i].lblBalance;
 	availableBal = availableBal.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "")
	availableBal = kony.string.replace(availableBal, ",", "");
	availableBal = parseFloat(availableBal.trim());
	var findDot;
	var isCrtFormt;
	enteredAmount = frmTransferLanding.txtTranLandAmt.text;
	isCrtFormt = amountValidationMB(enteredAmount);

 	if(enteredAmount != null && enteredAmount != undefined){
 	   	enteredAmount = kony.string.replace(enteredAmount, ",", "");
 	}
	
	var fee = "";	
	if (gblTransSMART == 1) {
		var fees = frmTransferLanding.btnTransLndSmart.text;
		if(fees != null && fees != undefined){
		  	var data = fees.split(" ");
		  	fee = data[0];
		}
		
	} else if (gblTrasORFT == 1) {
		var fees = frmTransferLanding.btnTransLndORFT.text;
		if(fees != null && fees != undefined){
		  	var data = fees.split(" ");
		  	fee = data[0];
		}
		
	} else {
		fee = "0";
	}
	if(fee != null && fee != undefined){
	  fee = parseFloat(fee.trim());
	}
	
	
	var idexofdot;
				
	if(enteredAmount != null && enteredAmount != undefined){
	   idexofdot = enteredAmount.indexOf(".");
	}
	var deccimal = ""
	
	if(idexofdot != null && idexofdot != undefined){
	   if (idexofdot > 0)
		deccimal = enteredAmount.substr(idexofdot);
	}
	if (!isCrtFormt || deccimal.length > 3) {
		showAlertWithCallBack(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), 
			kony.i18n.getLocalizedString("info"), callBackAmountFields);
		return false;
	} else if (enteredAmount != null && enteredAmount != undefined && (availableBal < (parseFloat(enteredAmount) + fee)) && gblPaynow) {
		if(gblSelTransferMode == 2 || gblSelTransferMode == 3){
			showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_P2Pkeyenteredamountexceedsavailablebalance"), 
				kony.i18n.getLocalizedString("info"), callBackAmountFields);
			return false;
		}else{
			showAlertWithCallBack(kony.i18n.getLocalizedString("keyenteredamountexceedsavailablebalance"), 
				kony.i18n.getLocalizedString("info"), callBackAmountFields);
			return false;
		}
	}
	return true;
}

function resetDefaultDisplayAfterTDP2P(isShow){
	frmTransferLanding.hbxTranLandShec.setVisibility(isShow);
	frmTransferLanding.hbxRecievedBy.setVisibility(isShow);
	frmTransferLanding.lineTransferSchedule.setVisibility(isShow);
	frmTransferLanding.lineRecievedBy.setVisibility(isShow);
	frmTransferLanding.hbxTranLandMyNote.setVisibility(isShow);
	frmTransferLanding.lineMyNote.setVisibility(isShow);
}


function checkDeviceContactsForNickName(){
		showLoadingScreen();
		displayP2PITMXBank(false);
		var mobileNumber = frmTransferLanding.txtOnUsMobileNo.text;
		var phnos = new Array();
		var name ="";
		var photo ="";
		var fn = "";
		var mn = "";
		var ln = "";
		var contacts = kony.contact.find("*", true);
		if (contacts != null && contacts != undefined  ) {
		
			for (var i = 0; i < contacts.length; i++) {
				if(contacts[i]["phone"] == null || contacts[i]["phone"] == undefined){
					phnos.push("NULL");
				}
				else{
					phnos.push(contacts[i]["phone"]);
				}
			}
			for (var i = 0; i < contacts.length; i++) {
				//break;
				if (contacts[i]["firstname"] == null || contacts[i]["firstname"] == "" || contacts[i]["firstname"] == undefined) fn = "";
				else fn = contacts[i]["firstname"];
				if(contacts[i]["middleName"] == null || contacts[i]["middleName"] == "" || contacts[i]["middleName"] == undefined) mn = "";
				else mn = contacts[i]["middleName"];
				if(contacts[i]["lastname"] == null || contacts[i]["lastname"] == "" || contacts[i]["lastname"] == undefined) ln = "";
			    else ln = contacts[i]["lastname"];
			    
			    if(isNotBlank(mn))
					fn = fn + " " + mn + " " + ln;
				else
					fn = fn + " " + ln;
			    
				var m = 0;
				//
				var phoneNumbers = [];
				while (true) {
					if(phnos[i] == "NULL"){
						break;
					}
					 	if(phnos[i][m] == null || phnos[i][m] == undefined){
					 		break;
					 	}
					 	var mobVal = getOnlyDigitMobileNumber(phnos[i][m]["number"]);
					 	if(isNotBlank(mobVal))
							{
								if(removeHyphenIB(mobileNumber) == removeHyphenIB(mobVal)){
									name = fn;
									if (contacts[i]["photorawbytes"] == null || contacts[i]["photorawbytes"] == "") photo = "";
									else photo = contacts[i]["photorawbytes"];
									
									break;
								}
							}
					m++;
				}
				
				if(name != ""){
					break;					
				}
				
			}
		}
		
		if(name == ""){				
			if(removeHyphenIB(mobileNumber) == removeHyphenIB(gblPHONENUMBER)){
				name = gblCustomerName;
				frmTransferLanding.lblRecipientName.text = name;
				displayP2PITMXBank(true);
				displayHbxNotifyRecipient(false);
			}else{
				checkInMibRecipients();
			}
		}else{
			frmTransferLanding.lblRecipientName.text = name;
			displayP2PITMXBank(true);
			displayHbxNotifyRecipient(true);
		}
		
}

function checkInMibRecipients(){
		var inputParam = {}
		showLoadingScreen();
		invokeServiceSecureAsync("getMibRecipientsForOnUs", inputParam, checkInMibRecipientsCallBack);
}

function checkInMibRecipientsCallBack(status, resulttable){
	//success responce
	if (status == 400) {
		if (resulttable["opstatus"] == 0 || resulttable["RecepientsList"].length>0) {
			var	mobileNumber = frmTransferLanding.txtOnUsMobileNo.text;
			var recipientList = resulttable["RecepientsList"];
			var foundInRecipient = false;
			for (var i = 0; i < recipientList.length; i++) {
				if(removeHyphenIB(mobileNumber) == removeHyphenIB(recipientList[i].personalizedMobileNum.trim())){
					foundInRecipient = true;
					displayP2PITMXBank(true);
					displayHbxNotifyRecipient(true);
					frmTransferLanding.lblRecipientName.text = recipientList[i].personalizedName.trim();
					break;
				}
			}
			
			if(!foundInRecipient){
				displayP2PITMXBank(false);
				displayHbxNotifyRecipient(true);
				frmTransferLanding.show();
			}	
					
		}else{
			displayP2PITMXBank(false);
			displayHbxNotifyRecipient(true);
			frmTransferLanding.show();
		}
	}

}

function checkEligibilityForP2P(contactsToService){
		var inputParam = {}
		inputParam["numbersList"] = removeHyphenIB(contactsToService.toString());
		showLoadingScreen();
		invokeServiceSecureAsync("checkEligibilityOfP2P", inputParam, checkEligibilityForP2PCallBack);
}

function checkEligibilityForP2PCallBack(status, resulttable){
	//success responce
	if (status == 400) {
		if (resulttable["opstatus"] == 0 ){
			var anyIdDataSet = resulttable["CheckP2PResultDS"];
			if(anyIdDataSet.length > 0){
				if(frmTransferToRecipentsMobile.vbxRecipient.skin == "vbxTabFocus"){
					var tempData = []
					for(var i=0;i<anyIdDataSet.length;i++){
					
						gblTransferToRecipientData[i]["AcctIdentValue"] =anyIdDataSet[i]["AcctIdentValue"];
						gblTransferToRecipientData[i]["OnUsFlag"] = anyIdDataSet[i]["OnUsFlag"];
						gblTransferToRecipientData[i]["AcctTitle"] = anyIdDataSet[i]["AcctTitle"];
						gblTransferToRecipientData[i]["ITMXFlag"] = anyIdDataSet[i]["ITMXFlag"];
						gblTransferToRecipientData[i].imgfav = "empty.png";
						if(gblTransferToRecipientData[i]["OnUsFlag"] == "Y"){
							gblTransferToRecipientData[i].imgfav = "tmb_recipient.png";
							tempData.push(gblTransferToRecipientData[i]);
						}else if(ITMX_TRANSFER_ENABLE == "true"){
							tempData.push(gblTransferToRecipientData[i]);
						}
					}
					gblTransferToRecipientData = tempData;
					frmTransferToRecipentsMobile.segTransferToRecipients.removeAll();
					
					frmTranfersToRecipents.segTransferToRecipients.widgetDataMap = {
						lblName: "lblName",
						lblAccountNum: "lblAccountNum",
						imgArrow: "imgArrow",
						imgprofilepic: "imgprofilepic",
						imgmob: "imgmob",
						imgfb: "imgfb",
						hiddenmain: "hiddenmain",
						lbl3: "lbl3",
						lbl4: "lbl4",
						lbl5: "lbl5",
						lblBankName: "lblBankName",
						img2: "img2",
						img3: "img3",
						imgfav:"imgfav"
							
					};
					frmTransferToRecipentsMobile.segTransferToRecipients.setVisibility(true);
					if(gblTransferToRecipientData.length > 0){
						dismissLoadingScreen();
						frmTransferToRecipentsMobile.show();
						frmTransferToRecipentsMobile.lblMsg.text = "";
						frmTransferToRecipentsMobile.lblMsg.setVisibility(false);
						frmTransferToRecipentsMobile.segTransferToRecipients.setData(gblTransferToRecipientData);
					}else{
						dismissLoadingScreen();
						if(kony.application.getCurrentForm().id == "frmTransferToRecipentsMobile"){
							frmTransferToRecipentsMobile.lblMsg.isVisible = true;
							frmTransferToRecipentsMobile.lblMsg.text = kony.i18n.getLocalizedString("MIB_P2PErrNoLinkAcct");
							frmTransferToRecipentsMobile.segTransferToRecipients.setVisibility(false);
						
						}else{
							showAlert(kony.i18n.getLocalizedString("MIB_P2PErrNoLinkAcct"), kony.i18n.getLocalizedString("info"));	
						}
					}
				}else{
					var k=0;
					var updatedContacts = [];
					for(var i=0;i<anyIdDataSet.length;){
						if(gblDeviceContactList[k].phNumbers.length > 1){
							gblDeviceContactList[k].imgfav ="empty.png";
							var phoneNumbers = gblDeviceContactList[k].phNumbers;
							var updatedPhoneNumbers = [];
							for(var j=0;j<gblDeviceContactList[k].phNumbers.length;j++){
								gblDeviceContactList[k].phNumbers[j]["AcctIdentValue"] =anyIdDataSet[i]["AcctIdentValue"];
								gblDeviceContactList[k].phNumbers[j]["OnUsFlag"] = anyIdDataSet[i]["OnUsFlag"];
								gblDeviceContactList[k].phNumbers[j]["AcctTitle"] = anyIdDataSet[i]["AcctTitle"];
								gblDeviceContactList[k].phNumbers[j]["ITMXFlag"] = anyIdDataSet[i]["ITMXFlag"];
								if(anyIdDataSet[i]["OnUsFlag"] == "Y"){
									gblDeviceContactList[k].imgfav = "tmb_recipient.png";
									updatedPhoneNumbers.push(gblDeviceContactList[k].phNumbers[j]);
								}else if(ITMX_TRANSFER_ENABLE == "true"){
									updatedPhoneNumbers.push(gblDeviceContactList[k].phNumbers[j]);
								}
								i++;
							}
							gblDeviceContactList[k].phNumbers = updatedPhoneNumbers;
							var displayNumber = "";
							if(gblDeviceContactList[k].phNumbers.length == 0){
								
							}else if(gblDeviceContactList[k].phNumbers.length == 1){
								displayNumber = onEditMobileNumberP2P(gblDeviceContactList[k].phNumbers[0]["mobile"]);
								gblDeviceContactList[k].lblAccountNum = displayNumber;
								gblDeviceContactList[k]["OnUsFlag"] = "Y";
								updatedContacts.push(gblDeviceContactList[k]);
							}else{
								displayNumber = gblDeviceContactList[k].phNumbers.length + " " + kony.i18n.getLocalizedString("MIB_P2P_TotalMobileNum");
								gblDeviceContactList[k].lblAccountNum = displayNumber;
								updatedContacts.push(gblDeviceContactList[k]);
							}
						}else{
							gblDeviceContactList[k]["AcctIdentValue"] =anyIdDataSet[i]["AcctIdentValue"];
							gblDeviceContactList[k]["OnUsFlag"] = anyIdDataSet[i]["OnUsFlag"];
							gblDeviceContactList[k]["AcctTitle"] = anyIdDataSet[i]["AcctTitle"];
							gblDeviceContactList[k]["ITMXFlag"] = anyIdDataSet[i]["ITMXFlag"];
							gblDeviceContactList[k].imgfav ="empty.png";
							if(anyIdDataSet[i]["OnUsFlag"] == "Y"){
								gblDeviceContactList[k].imgfav = "tmb_recipient.png";
								updatedContacts.push(gblDeviceContactList[k]);
							}else if(ITMX_TRANSFER_ENABLE == "true"){
								updatedContacts.push(gblDeviceContactList[k]);
							}
							i++;
						}
						k++;
					}
					
					frmTransferToRecipentsMobile.segTransferToRecipients.removeAll();
					frmTranfersToRecipents.segTransferToRecipients.widgetDataMap = {
						lblName: "lblName",
						lblAccountNum: "lblAccountNum",
						imgArrow: "imgArrow",
						imgprofilepic: "imgprofilepic",
						imgmob: "imgmob",
						imgfb: "imgfb",
						hiddenmain: "hiddenmain",
						lbl3: "lbl3",
						lbl4: "lbl4",
						lbl5: "lbl5",
						lblBankName: "lblBankName",
						img2: "img2",
						img3: "img3",
						imgfav:"imgfav"
					}
					gblDeviceContactList = updatedContacts;
					
					if(gblDeviceContactList.length > 0){
						kony.store.setItem("deviceLocalStoreContacts", gblDeviceContactList);
						for(var i = 0; i < gblDeviceContactList.length; i++) {
				 			gblDeviceContactList[i]["template"] = hbrt1;
				 		}
				 		dismissLoadingScreen();
			 			frmTransferToRecipentsMobile.show();
						frmTransferToRecipentsMobile.lblMsg.text = "";
						frmTransferToRecipentsMobile.lblMsg.setVisibility(false);
						frmTransferToRecipentsMobile.segTransferToRecipients.setData(gblDeviceContactList);
						frmTransferToRecipentsMobile.segTransferToRecipients.setVisibility(true);
						
						searchEligibleContactList = gblDeviceContactList;
					}else{
						kony.store.removeItem("deviceLocalStoreContacts");
						
						if(kony.application.getCurrentForm().id == "frmTransferToRecipentsMobile"){
							dismissLoadingScreen();
							frmTransferToRecipentsMobile.lblMsg.isVisible = true;
							if(ITMX_TRANSFER_ENABLE == "true"){
								frmTransferToRecipentsMobile.lblMsg.text = kony.i18n.getLocalizedString("MIB_P2PTRErr_NoToMobNotAllow");
							} else {
								frmTransferToRecipentsMobile.lblMsg.text = kony.i18n.getLocalizedString("MIB_P2PTRErr_NoToMobNotAllow_TurnOff");
							}
							frmTransferToRecipentsMobile.segTransferToRecipients.setVisibility(false);
						
						}else{
							
							frmTransferToRecipentsMobile.txbXferSearch.text = "";
							resetDeviceContactTab();
							focusMIBRecipientTab();
							calGetRecipientsForOnUs();
							//showAlert(kony.i18n.getLocalizedString("MIB_P2PErrNoLinkAcct"), kony.i18n.getLocalizedString("info"));	
						}
						
					}
					displayPopGeneralMsgRefreshDone();
					
				}
			}
		}
		//dismissLoadingScreen();
	}
}

function displayPopGeneralMsgRefreshDone(){
	if(isRefreshPressed){
		popGeneralMsg.lblMsg.text =  kony.i18n.getLocalizedString("MIB_P2PRefreshDone");
		popGeneralMsg.btnClose.text = kony.i18n.getLocalizedString("keyOK");
		popGeneralMsg.btnClose.onClick = closePopGeneralMsg;
		popGeneralMsg.show();
		isRefreshPressed = false;
	}
}

function getPhraseToMobileNumber(){
	var locale = kony.i18n.getCurrentLocale(); 
		
	if(gblSelTransferMode == 2){
		if(locale == 'th_TH'){
			return kony.i18n.getLocalizedString("MIB_P2PTo") + "" + kony.i18n.getLocalizedString("MIB_P2PMob");
		}else{
			return kony.i18n.getLocalizedString("MIB_P2PTo") + " " + kony.i18n.getLocalizedString("MIB_P2PMob");
		}			
 	} else if(gblSelTransferMode == 3){
 		if(locale == 'th_TH'){
			return kony.i18n.getLocalizedString("MIB_P2PTo") + "" + kony.i18n.getLocalizedString("MIB_P2PCiti");
		}else{
			return kony.i18n.getLocalizedString("MIB_P2PTo") + " " + kony.i18n.getLocalizedString("MIB_P2PCiti");
		}	
 	}
	
}
function checkP2pToAccount(nickname){
		var locale = kony.i18n.getCurrentLocale();
		if(gblSelTransferMode == 2){
			if(nickname == kony.i18n.getLocalizedString("MIB_P2PMob")){
				return true;
			}else{
				if(locale == 'th_TH'){
					if(nickname == "Mobile Number"){
						return true;
					}else{
						return false;
					}
				}else{
					if(nickname == "เบอร์มือถือ"){
						return true;
					}else{
						return false;
					}
				}
			}
 		} else if(gblSelTransferMode == 3){
 			if(nickname == kony.i18n.getLocalizedString("MIB_P2PCiti")){
				return true;
			}else{
				if(locale == 'th_TH'){
					if(nickname == "Citizen ID"){
						return true;
					}else{
						return false;
					}
				}else{
					if(nickname == "บัตรประชาชน"){
						return true;
					}else{
						return false;
					}
				}
			}
 		}
 		else{
 			return false;
 		}
}

function checkLanguageP2P(nickname){
	alert("1." + nickname +" == " + kony.i18n.getLocalizedString("MIB_P2PMob"));
	if(nickname == kony.i18n.getLocalizedString("MIB_P2PMob")){
		kony.i18n.setCurrentLocaleAsync(locale, null, null, "");
		return true;
	}else{
		kony.i18n.setCurrentLocaleAsync(locale, null, null, "");
		return false;
	}
}

function goToNextafterCheckP2PEligible(){
	frmTransfersAck.lblTransNPbAckToAccountName.text = frmTransferConfirm.lblTransCnfmToAccountName.text;	
	
	if(gblSelTransferMode == 2){
		frmTransferConfirm.lblTransCnfmToNum.text = frmTransferLanding.txtOnUsMobileNo.text;
		frmTransferConfirm.imgTransCnfmTo.src = getBankLogoURL("toMobile");
 	} else if(gblSelTransferMode == 3){
 		frmTransferConfirm.lblTransCnfmToNum.text = frmTransferLanding.txtCitizenID.text;
 		frmTransferConfirm.imgTransCnfmTo.src = getBankLogoURL("toCitizen");
 	}
	
	//frmTransferConfirm.imgTransCnfmTo.src = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +"tmb" + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId=" + gblisTMB + "&modIdentifier=BANKICON";
	frmTransfersAck.imgTransNPbAckTo.src = frmTransferConfirm.imgTransCnfmTo.src;
	if(isNotBlank(frmTransferLanding.lblRecipientName.text)){
		frmTransferConfirm.lblTransCnfmToBankName.text = frmTransferLanding.lblRecipientName.text;
	}else{
		frmTransferConfirm.lblTransCnfmToBankName.text = getPhraseToMobileNumber();
		gblSelectedRecipentName = "";
	}
	
	var i = gbltranFromSelIndex[1];
	var fromData = frmTransferLanding.segTransFrm.data;
	frmTransferConfirm.hbxScheduleNote.setVisibility(false);
 	if (gblSMART_FREE_TRANS_CODES.indexOf(fromData[i].prodCode) >= 0) {
		ramainfeeValue = parseFloat(fromData[i].lblRemainFeeValue) < 0 ? 0 : fromData[i].lblRemainFeeValue ;
		frmTransferConfirm.hbxFreeTrans.setVisibility(true);
		frmTransfersAck.hbxFreeTrans.setVisibility(true);
		frmTransferConfirm.lblFreeTransValue.text=ramainfeeValue;
 	} else {
		frmTransferConfirm.hbxFreeTrans.setVisibility(false);
		frmTransfersAck.hbxFreeTrans.setVisibility(false);
 	}
 	frmTransferConfirm.hbxBalAfterTransfer.setVisibility(true);
	frmTransferConfirm.hbxTransCnfmBalBefVal.setVisibility(true);
	getFeeValueForITMX(frmTransferLanding.lblITMXFee.text);
	
	checkCrmProfileInq();	
}

function getFeeValueForITMX(data){
	 var lblFee = data.split(" ");
	 if(lblFee.length > 2){
	 	frmTransferConfirm.lblTransCnfmTotFeeVal.text = 
	 	"(" + commaFormatted(parseFloat(lblFee[1]).toFixed(2)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht") + ")";
	 }else{
	 	frmTransferConfirm.lblTransCnfmTotFeeVal.text = "(0.00 " + kony.i18n.getLocalizedString("currencyThaiBaht") + ")";
	 }
}

function callBackMobileNoFieldsP2P(){
	frmTransferLanding.txtOnUsMobileNo.setFocus(true);
}

function callBackAmountFields(){
	frmTransferLanding.txtTranLandAmt.setFocus(true);
}

function callBackMyNoteFields(){
	frmTransferLanding.txtTranLandMyNote.setFocus(true);
}

function callBackTransLndSmsFields(){
	frmTransferLanding.txtTransLndSms.setFocus(true);
}

function callBackTranLandRecNoteFields(){
	frmTransferLanding.txtTranLandRecNote.setFocus(true);
}

function callBackAccountNumberField(){
	frmTransferLanding.tbxAccountNumber.setFocus(true);
}

function callBackTransLndSmsNEmailFields(){
	frmTransferLanding.txtTransLndSmsNEmail.setFocus(true);
}

function callBankRecNoteEmailFields(){
	frmTransferLanding.textRecNoteEmail.setFocus(true);
}


function ehFrmTransferLandingBtnTranLandSmsOnClick() {
    if (gblTrasSMS == 0) {
    	frmTransferLanding.txtTranLandRecNote.text = "";
        frmTransferLanding.textRecNoteEmail.text = "";
        frmTransferLanding.txtTransLndSms.setVisibility(true);
        frmTransferLanding.txtTransLndSms.setFocus(true);
        frmTransferLanding.txtTransLndSmsNEmail.setVisibility(false);
        frmTransferLanding.txtTransLndSms.text =  onEditMobileNumberP2P(gblXferPhoneNo);
        frmTransferLanding.txtTransLndSms.placeholder = kony.i18n.getLocalizedString("keyEnterMobileNum");//keyIBPleaseEnterEmail
        frmTransferLanding.btnTranLandSms.src = "mobile_blue.png";
        frmTransferLanding.btnTranLandEmail.src = "email_grey.png";
        gblTrasSMS = gblTrasSMS + 1;
        gblTransEmail = 0;
        frmTransferLanding.hbxTranLandRecNote.setVisibility(true);
        frmTransferLanding.hbxRecNoteEmail.setVisibility(false);
        frmTransferLanding.lineMobileEmailTextBox.setVisibility(true);
        frmTransferLanding.lineNotifyRecipientButton.setVisibility(true);
        frmTransferLanding.lineRecipientNote.setVisibility(true);
        
       if(gblSelTransferMode == 2){
        	frmTransferLanding.btnOnUsMobileSmS.src = "tran_notify_mobile_only_selected.png";
        	frmTransferLanding.txtTransLndSms.text = frmTransferLanding.txtOnUsMobileNo.text;
        	if(isNotBlank(frmTransferLanding.txtOnUsMobileNo.text)){
        		frmTransferLanding.txtTranLandRecNote.setFocus(true);
        	}else{
        		frmTransferLanding.txtTransLndSms.setFocus(true);
        	}
       }
    } else {
        frmTransferLanding.txtTransLndSms.text = "";
        frmTransferLanding.txtTranLandRecNote.text = "";
        frmTransferLanding.textRecNoteEmail.text = "";
        frmTransferLanding.btnTranLandSms.src = "mobile_grey.png";
        frmTransferLanding.btnOnUsMobileSmS.src = "tran_notify_mobile_only.png";
        frmTransferLanding.txtTransLndSms.setVisibility(false);
        frmTransferLanding.lineMobileEmailTextBox.setVisibility(false);
        frmTransferLanding.lineNotifyRecipientButton.setVisibility(false);
        gblTrasSMS = 0;
        gblTransEmail = 0;
        frmTransferLanding.hbxTranLandRecNote.setVisibility(false);
        frmTransferLanding.hbxRecNoteEmail.setVisibility(false);
        frmTransferLanding.txtTranLandRecNote.setFocus(false);
    }
};

function ehFrmTransferLandingBtnTranLandEmailOnClick() {
    if (gblTransEmail == 0) {
   	  	frmTransferLanding.txtTranLandRecNote.text = "";
        frmTransferLanding.textRecNoteEmail.text = "";
        frmTransferLanding.hbxTranLandRecNote.setVisibility(false);
        frmTransferLanding.hbxRecNoteEmail.setVisibility(true);
       	frmTransferLanding.lineMobileEmailTextBox.setVisibility(true);
       	frmTransferLanding.lineNotifyRecipientButton.setVisibility(true);
        frmTransferLanding.lineRecipientNote.setVisibility(true);
        frmTransferLanding.txtTransLndSms.setVisibility(false);
        frmTransferLanding.txtTransLndSmsNEmail.setVisibility(true);
        frmTransferLanding.txtTransLndSmsNEmail.setFocus(true);
        frmTransferLanding.txtTransLndSmsNEmail.textInputMode = "TEXTBOX_INPUT_MODE_ANY"
        frmTransferLanding.txtTransLndSmsNEmail.text = gblXferEmail
        frmTransferLanding.txtTransLndSmsNEmail.placeholder = kony.i18n.getLocalizedString("keyIBPleaseEnterEmail");
        frmTransferLanding.btnTranLandEmail.src = "email_blue.png";
        frmTransferLanding.btnTranLandSms.src = "mobile_grey.png";
        frmTransferLanding.textRecNoteEmail.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_LEFT;
        gblTransEmail = gblTransEmail + 1;
        gblTrasSMS = 0;
    } else {
        frmTransferLanding.hbxTranLandRecNote.setVisibility(false);
        frmTransferLanding.hbxRecNoteEmail.setVisibility(false);
        frmTransferLanding.txtTransLndSmsNEmail.text = ""
        frmTransferLanding.txtTranLandRecNote.text = "";
        frmTransferLanding.textRecNoteEmail.text = "";
        frmTransferLanding.btnTranLandEmail.src = "email_grey.png";
        frmTransferLanding.txtTransLndSmsNEmail.setVisibility(false);
 		frmTransferLanding.lineMobileEmailTextBox.setVisibility(false);
 		frmTransferLanding.lineNotifyRecipientButton.setVisibility(false);
        gblTrasSMS = 0;
        gblTransEmail = 0;
    }
}

function formatAmountOnTextChangeFTEdit(){
	var enteredAmount = frmMBFTEdit.txtEditAmnt.text;
	if(isNotBlank(enteredAmount)) {
		enteredAmount = kony.string.replace(enteredAmount, ",", "");
		frmMBFTEdit.txtEditAmnt.text = commaFormattedTransfer(enteredAmount);
	}
}

function commaFormattedTransfer(amount) {
    var minus = '';
    
    if(amount == "" || amount == null || amount == undefined)
		return "-";
    if(amount.indexOf(".")==0){
		amount="0"+amount;
    }
	if(parseFloat(amount) < 0) {
		minus = '-';
	}    
    
    var delimiter = ","; // replace comma if desired
    amount = new String(amount);
    if(amount.indexOf(".") > -1){
        var a = amount.split('.', 2);
        var d = a[1];
        if(d == ""){
        	d = ".";
        }
        var i = parseInt(a[0], 10);
    } else {
        var d = "";
        var i = parseInt(amount, 10);
    }
    if (isNaN(i)) {
		return '';
    }

    i = Math.abs(i);
    var n = new String(i);
    var a = [];
    while (n.length > 3) {
        var nn = n.substr(n.length - 3);
        a.unshift(nn);
        n = n.substr(0, n.length - 3);
    }
    if (n.length > 0) {
        a.unshift(n);
    }
    n = a.join(delimiter);
    
    if (d.length < 1) {
        amount = n;
    } else {
    	if(d.indexOf(".") > -1 ){
    		amount = n + d;
    	}else{
    		amount = n + "." + d;
    	}
    }
    amount = minus + amount;
    return amount;
}

function removeDuplicateFromArray(array) {
    var seen = {};
    var out = [];
    var len = array.length;
    var j = 0;
    for(var i = 0; i < len; i++) {
         var item = array[i]["mobile"];
         if(seen[item] !== 1) {
               seen[item] = 1;
               out[j++]= {"mobile":item};
         }
    }
    return out;
}

function refreshContactsFromServer(){
		if(frmTransferToRecipentsMobile.vbxContact.skin == "vbxTabFocus"
			&& frmTransferToRecipentsMobile.btnRight.skin == "btnRefreshActive") {
			isRefreshPressed = true;
			frmTransferToRecipentsMobile.txbXferSearch.text = "";
			gblDeviceContactList  = [];
			searchEligibleContactList = [];
			kony.store.removeItem("deviceLocalStoreContacts");
			//kony.store.removeItem("noContacts");
			showDeviceContacts(false);
		}
}

function loadContactsFromDeviceStore(p2pEligibleContacts){
		frmTransferToRecipentsMobile.segTransferToRecipients.removeAll();
		frmTranfersToRecipents.segTransferToRecipients.widgetDataMap = {
			lblName: "lblName",
			lblAccountNum: "lblAccountNum",
			imgArrow: "imgArrow",
			imgprofilepic: "imgprofilepic",
			imgmob: "imgmob",
			imgfb: "imgfb",
			hiddenmain: "hiddenmain",
			lbl3: "lbl3",
			lbl4: "lbl4",
			lbl5: "lbl5",
			lblBankName: "lblBankName",
			img2: "img2",
			img3: "img3",
			imgfav:"imgfav"
		}
		
		 for(var i = 0; i < p2pEligibleContacts.length; i++) {
		 	p2pEligibleContacts[i]["template"] = hbrt1;
		 }
		frmTransferToRecipentsMobile.show();
		if(p2pEligibleContacts.length > 0){
			frmTransferToRecipentsMobile.lblMsg.text = "";
			frmTransferToRecipentsMobile.lblMsg.setVisibility(false);
			frmTransferToRecipentsMobile.segTransferToRecipients.setVisibility(true);
			for(var i = 0; i < p2pEligibleContacts.length; i++) {
				var displayNumber = p2pEligibleContacts[i].lblAccountNum.split(" ");
				if(displayNumber.length > 1){
					var newDisplayText = displayNumber[0] + " " + kony.i18n.getLocalizedString("MIB_P2P_TotalMobileNum");
					p2pEligibleContacts[i].lblAccountNum = newDisplayText;
				}
			}
			gblDeviceContactList = p2pEligibleContacts;
			frmTransferToRecipentsMobile.segTransferToRecipients.setData(p2pEligibleContacts);
			searchEligibleContactList = p2pEligibleContacts;
		 }else {
			frmTransferToRecipentsMobile.lblMsg.setVisibility(true);
			if(ITMX_TRANSFER_ENABLE == "true"){
				frmTransferToRecipentsMobile.lblMsg.text = kony.i18n.getLocalizedString("MIB_P2PTRErr_NoToMobNotAllow");
			} else {
				frmTransferToRecipentsMobile.lblMsg.text = kony.i18n.getLocalizedString("MIB_P2PTRErr_NoToMobNotAllow_TurnOff");
			}
			frmTransferToRecipentsMobile.segTransferToRecipients.setVisibility(false);
		}
		dismissLoadingScreen();
}



function setEnableRefreshBtn(isActive){
	if(isActive){
		frmTransferToRecipentsMobile.btnRight.skin = "btnRefreshActive";
		frmTransferToRecipentsMobile.btnRight.setEnabled(true);
	}else{
		frmTransferToRecipentsMobile.btnRight.skin = "btnRefreshInActive";
		frmTransferToRecipentsMobile.btnRight.setEnabled(false);
	}
}



function getITMXTransferFee(amount) {
	var prodCode = "";
 	if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "android" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPod touch") {
 		var i = gbltranFromSelIndex[1];
		var fromData = frmTransferLanding.segTransFrm.data;
		prodCode = fromData[i].prodCode;
    } else {
		prodCode = gblcwselectedData.prodCode;
    }
	if(gblITMX_FREE_TRANS_CODES.indexOf(prodCode) > - 1){
		return "0.00";
	}
   var limits = ITMX_TRANSFER_FEE_LIMITS;
   var allRangesFees = limits.split("~");
    for(var i=0;i<allRangesFees.length;i++){
          var rangeAndFee =  allRangesFees[i].split("#");
          var fee = rangeAndFee[1];
          var minMaxRange = rangeAndFee[0].split("-");
          var minRange;
          var maxRange;
          if(minMaxRange.length > 1){
          	minRange = parseFloat(minMaxRange[0]);
          	maxRange = parseFloat(minMaxRange[1]);
          	var enteredAmount = amount.replace(kony.i18n.getLocalizedString("currencyThaiBaht"),"").replace(/,/g, "");
   			enteredAmount = amount.trim();
   			var amountFloat = parseFloat(enteredAmount);
	        if(amountFloat > minRange && amountFloat <= maxRange ){
	          		return parseFloat(fee).toFixed(2);
	        }
          }else{
          	minRange = parseFloat(minMaxRange[0]);
           	if(amountFloat > minRange){
	          		return parseFloat(fee).toFixed(2);
	          }
          }
     }
    return parseFloat(0).toFixed(2);
}	

function callCheckOnUsPromptPayinqServiceMB(){
	var inputParam = {};
	var fromData = "";
 	var i = gbltranFromSelIndex[1];
	fromData = frmTransferLanding.segTransFrm.data;
	enteredAmount = frmTransferLanding.txtTranLandAmt.text;
	enteredAmount =  enteredAmount.replace(/,/g, "");
	var frmID = fromData[i].lblActNoval;
	var prodCode = fromData[i].prodCode;	
 	var fromAcctID = kony.string.replace(frmID,"-", "");
 	var toAcctID  = "";
 	var mobileOrCI = "";
 	if(gblSelTransferMode == 2){
		toAcctID  = removeHyphenIB(frmTransferLanding.txtOnUsMobileNo.text);
		mobileOrCI = "02";
 	} else if(gblSelTransferMode == 3){
 		toAcctID  = removeHyphenIB(frmTransferLanding.txtCitizenID.text);
 		mobileOrCI = "01";
 	}
 	var locale = kony.i18n.getCurrentLocale();
	frmTransferConfirm.lblHiddenToAccount.text = toAcctID;
	inputParam["fromAcctNo"] = fromAcctID;
	inputParam["toAcctNo"] = toAcctID;
	inputParam["toFIIdent"] = gblisTMB;	
	inputParam["transferAmt"] = enteredAmount;
	inputParam["prodCode"] = prodCode;
	inputParam["mobileOrCI"] = mobileOrCI;
	inputParam["locale"] = locale;
	showLoadingScreen();
	invokeServiceSecureAsync("checkOnUsPromptPayinq", inputParam, callBackCheckOnUsPromptPayinqServiceMB);
}


function callBackCheckOnUsPromptPayinqServiceMB(status, resulttable) {
	var ToAccountName  = "";
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			
			gblisTMB = resulttable["destBankCode"];
			if(!isNotBlank(gblisTMB)){
				showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
				dismissLoadingScreen();
				return false;
			}
			if(gblisTMB != gblTMBBankCD){
				ToAccountName = resulttable["toAcctName"];
			}else{
				ToAccountName = resulttable["toAccTitle"];
			}
			
			if(!isNotBlank(ToAccountName) && gblisTMB != gblTMBBankCD){
				if(gblSelTransferMode == 2){
					showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_P2PNullAcctName"), 
					kony.i18n.getLocalizedString("info"), callBackMobileNoFieldsP2P);
					frmTransferLanding.txtOnUsMobileNo.text = "";
				} else if(gblSelTransferMode == 3){
					showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_P2PnullAcctName2"), 
					kony.i18n.getLocalizedString("info"), callBackCitizenIDFieldsP2P);
					frmTransferLanding.txtCitizenID.text = "";
				}	
				dismissLoadingScreen();
				return false;
			}
			gblp2pAccountNumber = resulttable["toAcctNo"];
			var isOtherBank = frmTransferLanding.segTransFrm.selectedItems[0].isOtherBankAllowed;
			var isOtherTmb = frmTransferLanding.segTransFrm.selectedItems[0].isOtherTMBAllowed;
			var isAllowedSA = frmTransferLanding.segTransFrm.selectedItems[0].isAllowedSA;
			var isAllowedCA = frmTransferLanding.segTransFrm.selectedItems[0].isAllowedCA;
			var isAllowedTD = frmTransferLanding.segTransFrm.selectedItems[0].isAllowedTD;
			var fromAccNumber = frmTransferLanding.segTransFrm.selectedItems[0].accountNum;
			var prodCode = frmTransferLanding.segTransFrm.selectedItems[0].prodCode;
			if(isOtherBank != "Y" && gblisTMB != gblTMBBankCD){
				if(gblSelTransferMode == 2){
					showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_P2PErrFromToNotMatch"), 
					kony.i18n.getLocalizedString("info"), callBackMobileNoFieldsP2P);
					frmTransferLanding.txtOnUsMobileNo.text = "";
			 	} else if(gblSelTransferMode == 3){
			 		showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_P2PErrFromToNotMatch2"), 
					kony.i18n.getLocalizedString("info"), callBackCitizenIDFieldsP2P);
					frmTransferLanding.txtCitizenID.text = "";
			 	}
				
				displayP2PITMXBank(false);
				dismissLoadingScreen();
				return false;
			}
			
			if(isValidateFromToAccount(gblp2pAccountNumber) == false){
				if(gblSelTransferMode == 2){
					frmTransferLanding.txtOnUsMobileNo.text = "";
	                showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_P2PErrMsgMobileNoLinkFromAcct"), 
	                    kony.i18n.getLocalizedString("info"), callBackMobileNoFieldsP2P);
			 	} else if(gblSelTransferMode == 3){
			 		frmTransferLanding.txtCitizenID.text = "";
	                showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_P2PErrMsgCILinkFromAcct"), 
	                    kony.i18n.getLocalizedString("info"), callBackCitizenIDFieldsP2P);
			 	}
                dismissLoadingScreen();
				displayP2PITMXBank(false);
                return false;
                
            }
            isOwnAccountP2P = false;
            if(resulttable["isOwn"] == "Y"){
                isOwnAccountP2P = true;
            }
			frmTransferConfirm.lblHiddenToAccount.text = resulttable["toAcctNo"];
			frmTransferConfirm.lblTransCnfmTotFeeVal.text = "(" + resulttable["itmxFee"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht") + ")";
			if(gblSelTransferMode == 2){
				frmTransferLanding.lblMobileNoTemp.text = frmTransferLanding.txtOnUsMobileNo.text;
				displayOwnerNotifyRecipientP2P();
			} else if(gblSelTransferMode == 3){
				frmTransferLanding.lblMobileNoTemp.text = frmTransferLanding.txtCitizenID.text;
				frmTransferLanding.hbxOnUsNotifyRecipient.setVisibility(false);
				displayHbxNotifyRecipient(true);
			}
			gblPaynow = true;
			gblBANKREF = getBankNameCurrentLocaleTransfer(gblisTMB);
			gblSelectedRecipentName = ToAccountName;
			frmTransferLanding.lblTMXAccountName.text = ToAccountName;
			frmTransferConfirm.lblTransCnfmToAccountName.text = ToAccountName;
			frmTransferLanding.lblTMXBankName.text = getBankNameMB(gblisTMB);
			frmTransferLanding.imgITMXBankLogo.src = getBankLogoURL(gblisTMB);
			frmTransferConfirm.lblTransCnfmToBankName.text = kony.i18n.getLocalizedString("MIB_P2PMob");
			frmTransferLanding.lineInputMobileNo.setVisibility(true);
			frmTransferLanding.lineRecipientNote.setVisibility(true);
			//frmTransferLanding.lineInputMobileNo.skin = "lineBlue";
			displayP2PITMXBank(true);
			displayP2PITMXFee(true, enteredAmount);
			displayDebitReceivedNotify();
				
			if(kony.application.getCurrentForm().id != "frmTransferLanding"){
				frmTransferLanding.show();
			}
			dismissLoadingScreen();
		} else {
			dismissLoadingScreen();
			if (resulttable["promptPayFlag"] == "1"){
				var errCode = resulttable["errCode"];
				var errorText = "";
				if (!isNotBlank(errCode)){
					 showAlertWithCallBack(kony.i18n.getLocalizedString("ECGenericError"), 
					 kony.i18n.getLocalizedString("info"), callBackMobileNoFieldsP2P);					 
				}else if(errCode == 'XB240048'){
					if(gblSelTransferMode == 2){
						displayNotEligibleNumber();
					} else if(gblSelTransferMode == 3){ 
						displayNotEligibleCitizenID();
					}	
				}else if(errCode == 'XB240066'){
					if(gblSelTransferMode == 2){
						showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_P2PNullAcctName"), 
							kony.i18n.getLocalizedString("info"), callBackMobileNoFieldsP2P);
					} else if(gblSelTransferMode == 3){
						showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_P2PnullAcctName2"), 
							kony.i18n.getLocalizedString("info"), callBackCitizenIDFieldsP2P);
					}
				}else if(errCode == 'XB240088'){
					if(gblSelTransferMode == 2){
						errorText = kony.i18n.getLocalizedString("MIB_P2PAccInActive");
						showAlertWithCallBack(errorText, 
							kony.i18n.getLocalizedString("info"), callBackMobileNoFieldsP2P);
					} else if(gblSelTransferMode == 3){
						errorText = kony.i18n.getLocalizedString("MIB_P2PAccInActive");
						showAlertWithCallBack(errorText, 
							kony.i18n.getLocalizedString("info"), callBackCitizenIDFieldsP2P);
					}				
				}else if(errCode == 'XB240067'){
					if(gblSelTransferMode == 2){
						errorText = kony.i18n.getLocalizedString("MIB_P2PCutOffTime");
						showAlertWithCallBack(errorText, 
							kony.i18n.getLocalizedString("info"), callBackMobileNoFieldsP2P);
					} else if(gblSelTransferMode == 3){
						errorText = kony.i18n.getLocalizedString("MIB_P2PCutOffTime");
						showAlertWithCallBack(errorText, 
							kony.i18n.getLocalizedString("info"), callBackCitizenIDFieldsP2P);
					}				
				}else if(errCode == 'XB240072'){
					if(gblSelTransferMode == 2){
						errorText = kony.i18n.getLocalizedString("MIB_P2PCommunicationErr");
						showAlertWithCallBack(errorText, 
							kony.i18n.getLocalizedString("info"), callBackMobileNoFieldsP2P);
					} else if(gblSelTransferMode == 3){
						errorText = kony.i18n.getLocalizedString("MIB_P2PCommunicationErr");
						showAlertWithCallBack(errorText, 
							kony.i18n.getLocalizedString("info"), callBackCitizenIDFieldsP2P);
					}				
				}else if(errCode == 'X8899'){
					errorText = kony.i18n.getLocalizedString("MIB_P2PCloseBranchErr");
					showAlertWithCallBack(errorText, 
							kony.i18n.getLocalizedString("info"), callBackMobileNoFieldsP2P);			
				}else{					
					var errorText = kony.i18n.getLocalizedString("ECGenericError") + " (" + errCode +")";
					if(gblSelTransferMode == 2){	
						 showAlertWithCallBack(errorText, kony.i18n.getLocalizedString("info"), callBackMobileNoFieldsP2P);
					} else if(gblSelTransferMode == 3){
						 showAlertWithCallBack(errorText, kony.i18n.getLocalizedString("info"), callBackCitizenIDFieldsP2P);
					}					
				}
			}else{				
				if(gblSelTransferMode == 2){
					displayNotEligibleNumber();
				} else if(gblSelTransferMode == 3){ 
					displayNotEligibleCitizenID();
				}
			}
			frmTransferLanding.lblMobileNoTemp.text = "";
			frmTransferLanding.txtOnUsMobileNo.text = "";
			frmTransferLanding.txtCitizenID.text = "";
			displayP2PITMXBank(false);
			return false;
		}
		return true;
	}
	return false;
}

function callBackCheckOnUsPromptPayinqServiceMBOnNext(status, resulttable) {
	var statusGoTo = callBackCheckOnUsPromptPayinqServiceMB(status, resulttable);
	if(statusGoTo){
		goToNextafterCheckP2PEligible();
	}
}

function formatAmountOnTextChange(){
	var enteredAmount = frmTransferLanding.txtTranLandAmt.text;
	if(isNotBlank(enteredAmount)) {
		enteredAmount = kony.string.replace(enteredAmount, ",", "");
		if(isNotBlank(enteredAmount) && enteredAmount.length > 0 && parseFloat(enteredAmount, 10) == 0){
			
		}else{
			frmTransferLanding.txtTranLandAmt.text = commaFormattedTransfer(enteredAmount);
		}
		displayP2PITMXFee(false,enteredAmount);
	}else{
		displayP2PITMXFee(false,enteredAmount);
	}
}

function displayP2PITMXFee(isShow, amount){
	frmTransferLanding.lblITMXFee.setVisibility(isShow);
	if(isShow && isNotBlank(amount)){
		if(gblisTMB == gblTMBBankCD){
			frmTransferLanding.hbxFeeTransfer.setVisibility(true);
	    	frmTransferLanding.lblITMXFee.text = kony.i18n.getLocalizedString("keyFreeTransfer");
	    	frmTransferLanding.lblFeeTransfer.text = kony.i18n.getLocalizedString("MIB_P2PTRFeeTransfer");
	    	frmTransferLanding.lblITMXFee.skin = "lblGrey48px";
	    	frmTransferLanding.lblFeeTransfer.skin = "lblGrey36px";
	    }else{
	    	if(gblSelTransferMode == 1){
	    		frmTransferLanding.lblFeeTransfer.skin = "lblGrey36px";
	    	}else{
	    		var fee = getITMXTransferFee(amount);
		    	frmTransferLanding.lblFeeTransfer.text = kony.i18n.getLocalizedString("MIB_P2PTRFeeTransfer");
				if(parseFloat(fee) == 0 || (gblisTMB == gblTMBBankCD)){
					frmTransferLanding.lblITMXFee.text = kony.i18n.getLocalizedString("keyFreeTransfer");
					frmTransferLanding.lblFeeTransfer.skin = "lblGrey36px";
				}else{
					frmTransferLanding.lblITMXFee.text = fee;
					frmTransferLanding.lblFeeTransfer.skin = "lblGrey36px";
				}
	    	}
	    }
	}else{
		frmTransferLanding.lblITMXFee.text = "";
		frmTransferLanding.lblFeeTransfer.skin = "lblWhite36px";
	}
}



//FFI for iPhone Native Contacts Display
function contactsCallBack(response){
	if(response != null && response != undefined) {
		var contactDetails = JSON.parse(response);
		var phoneResponse = contactDetails.phoneNumber;
		var phoneNumber = phoneResponse.indexOf("digits=") > -1 ? phoneResponse.substring(phoneResponse.indexOf("digits=") + 7, phoneResponse.length - 1) : phoneResponse;
		
		validateMobileNumberP2P(phoneNumber);
		
	} else {
		frmTransferLanding.txtOnUsMobileNo.text = "";
	 	showAlertWithCallBack(kony.i18n.getLocalizedString("contactList_validMobNum"), kony.i18n.getLocalizedString("info"), callBackMobileNoFieldsP2P);
	}

}

//iPhone
function displayContacts(){
	//Creates an object of class 'ContactPick'
	var ContactPickObject = new iPhoneContacts.ContactPick();
	//Invokes method 'showContacts' on the object
	ContactPickObject.showContacts(contactsCallBack);
}

//Android
function getContactPicker() {
	//Creates an object of class 'ContactPicker'
	var ContactPickerObject = new androidContacts.ContactPicker();
	//Invokes method 'pickContact' on the object
	ContactPickerObject.pickContact(callbackContactPicker);
	
}

function callbackContactPicker(response){
	
	if(response != null && response != undefined) {
		
		var contactDetails = JSON.parse(response);
		
		var phoneNumber = contactDetails.phoneNumber;
		
		validateMobileNumberP2P(phoneNumber);
		
	} else {
		frmTransferLanding.txtOnUsMobileNo.text = "";
	 	showAlertWithCallBack(kony.i18n.getLocalizedString("contactList_validMobNum"), kony.i18n.getLocalizedString("info"), callBackMobileNoFieldsP2P);
	}
}

function validateMobileNumberP2P(phoneNumber) {
		
		phoneNumber = getOnlyDigitMobileNumber(phoneNumber);
					
		if(isNotBlank(phoneNumber) && phoneNumber.length == 10  && recipientMobileVal(phoneNumber)){
			frmTransferLanding.txtOnUsMobileNo.text = formatMobileNumber(phoneNumber);
			onTextChangeToMobileNoP2P(frmTransferLanding.txtOnUsMobileNo.text);
		} else {
			frmTransferLanding.txtOnUsMobileNo.text = "";
			showAlertWithCallBack(kony.i18n.getLocalizedString("contactList_validMobNum"), kony.i18n.getLocalizedString("info"), callBackMobileNoFieldsP2P);
		}

}