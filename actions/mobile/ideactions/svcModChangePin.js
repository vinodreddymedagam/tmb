function initOnlineChangePin() {
	showDismissLoadingMBIB(true);
    var inputParam = {};
    invokeServiceSecureAsync("InitOnlineATMPIN", inputParam, initOnlineChangePinCallBack);
}

function initOnlineChangePinCallBack(status, resulttable) {
    if (status == 400) {
    	showDismissLoadingMBIB(false);
    	if (resulttable["opstatus"] == 0) {
	        gblPk = resulttable["pubKey"];
	        gblRand = resulttable["serverRandom"];
	        gblAlgorithm = resulttable["hashAlgorithm"];

			callVerifyChangeExistingPin(encryptChangePinUsingE2EE(decryptPinDigit(glbPin)));
			
	    }else{
	    	gblPk="";
	    	gblRand="";
	    	gblAlgorithm="";
	    	showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
	    }
	}    
}

function initOnlineChangeNewPin() {
	showDismissLoadingMBIB(true);
    var inputParam = {};
    invokeServiceSecureAsync("InitOnlineATMPIN", inputParam, initOnlineChangeNewPinCallBack);
}

function initOnlineChangeNewPinCallBack(status, resulttable) {
    if (status == 400) {
    	showDismissLoadingMBIB(false);
    	if (resulttable["opstatus"] == 0) {
	        gblPk = resulttable["pubKey"];
	        gblRand = resulttable["serverRandom"];
	        gblAlgorithm = resulttable["hashAlgorithm"];
			
			callAssignChangePin(encryptChangePinUsingE2EE(decryptPinDigit(glbPin)));
			
	    }else{
	    	gblPk="";
	    	gblRand="";
	    	gblAlgorithm="";
	    	showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
	    }
	}    
}

function callVerifyChangeExistingPin(atmPinEncrypted){
	var inputParam = {};
	showLoadingScreen();
	inputParam["rPin"] = atmPinEncrypted;
	inputParam["cardRefId"] = gblCACardRefNumber;
	kony.print("atmPinEncrypted >>>>>>>"+atmPinEncrypted);
	invokeServiceSecureAsync("changeVerifyPinCompositeService", inputParam, callVerifyChangeExistingPinCallBack);
	
}

function callVerifyChangeExistingPinCallBack(status, resulttable) {
	dismissLoadingScreen();
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			//PIN Verify Success
			gblPk="";
    		gblRand="";
    		gblAlgorithm="";
			changePINBoxSkin("slFbox");
			frmMBChangePINEnterExistsPin.flxnewpinlink.setVisibility(false);
	 		frmMBChangePINEnterExistsPin.flxPinPad.setVisibility(true);
			//Existing Pin Verify Success, show Txn Password
			checkValidchangePin();
			
		} else {
			clearExistChangePIN();
			if(resulttable["errCode"] == "B243053") {
				remainAttempt = 3;
				if(kony.string.equalsIgnoreCase("B24INVALID PIN - 1", resulttable["errMsg"])) {
					remainAttempt = remainAttempt - 1;
				} else if(kony.string.equalsIgnoreCase("B24INVALID PIN - 2", resulttable["errMsg"])) {
					remainAttempt = remainAttempt - 2;
				}
				var errMsg = kony.i18n.getLocalizedString("PIN_Err_NotCorrect");
				errMsg = errMsg.replace("{x_time_wrong_pin}", remainAttempt+"");
				changePINBoxSkin("slFboxRed");
				frmMBChangePINEnterExistsPin.lblErrMsg.text = errMsg;
	 			frmMBChangePINEnterExistsPin.flxnewpinlink.setVisibility(true);
	 			frmMBChangePINEnterExistsPin.flxPinPad.setVisibility(false);
	 		} else if(resulttable["errCode"] == "B243062") {
				frmMBChangePinContactCenter.rchLockedMsg1.text = kony.i18n.getLocalizedString("PIN_Err_Locked3");
				frmMBChangePinContactCenter.lblLockedMsg2.text = kony.i18n.getLocalizedString("PIN_Locked_1558");
				frmMBChangePinContactCenter.btnCallCenterNum.text = kony.i18n.getLocalizedString("DBI03_Call1558");
	 			frmMBChangePinContactCenter.show();
	 		} else if(resulttable["errCode"] == "B243060") { // one time each day error
	 			showAlertWithCallBack(kony.i18n.getLocalizedString("PIN_Change_Repeat"), 
	 				kony.i18n.getLocalizedString("info"), onClickCloseChangePINLocked);
	 		}else {
				showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
	 		}
	 		gblPk = resulttable["pubKey"];
	       	gblRand = resulttable["serverRandom"];
	        gblAlgorithm = resulttable["hashAlgorithm"];	
			return false;
		}
	}
}

function changePINBoxSkin(skinVal){
	frmMBChangePINEnterExistsPin.flxpin1.skin = skinVal;
	frmMBChangePINEnterExistsPin.flxpin2.skin = skinVal;
	frmMBChangePINEnterExistsPin.flxpin3.skin = skinVal;
	frmMBChangePINEnterExistsPin.flxpin4.skin = skinVal;
}

function callAssignChangePin(encryptedPin){
	showDismissLoadingMBIB(true);
    var inputParam = {};
    inputParam["rPin"]=encryptedPin;
    inputParam["cardRefId"] = gblCACardRefNumber;
    inputParam["productTypeTH"] = gblSelectedCard["ProductNameThai"];
    inputParam["productTypeEN"] = gblSelectedCard["ProductNameEng"];
    invokeServiceSecureAsync("changeAssignPinCompositeService", inputParam, callAssignChangePinCallBack);
}

function callAssignChangePinCallBack(status,resulttable){
	if(status == 400){
		showDismissLoadingMBIB(false);
		if(resulttable["opstatus"]== 0){
			NavigateToCPCompleteScreen();
		}else {
			if(resulttable["errCode"] == "B243060") { // one time each day error
	 			showAlertWithCallBack(kony.i18n.getLocalizedString("PIN_Change_Repeat"), 
	 				kony.i18n.getLocalizedString("info"), onClickCloseChangePINLocked);
	 		}else {
				/*var errorMsgTop = resulttable["errMsg"];
	           	if(!isNotBlank(resulttable["errMsg"])){
	           		errorMsgTop = kony.i18n.getLocalizedString("ECGenOTPRtyErr00001");
	           	}
	            showAlertWithCallBack(errorMsgTop, kony.i18n.getLocalizedString("info"), gotoChangePinFailScreen);*/
	            gotoChangePinFailScreen();
	 		}
        }
	}	
}


//Transaction pop up open
function checkValidchangePin(){
	var lblText = kony.i18n.getLocalizedString("transPasswordSub");
	var refNo = "";
	var mobNO = "";
	showLoadingScreen();
	showOTPPopup(lblText, refNo, mobNO, onClickConfirmChangePINTransPop, 3);
}

function onClickConfirmChangePINTransPop(tranPassword) {
	if (isNotBlank(popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text)) {
		showLoadingScreen();
		popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("keyTransPwdMsg");
		popupTractPwd.lblPopupTract7.skin = lblPopupLabelTxt;
		popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = tbxPopupBlue;
		popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = tbxPopupBlue;
		popupTractPwd.hbxPopupTranscPwd.skin = tbxPopupBlue;
		callCheckChangePINService(tranPassword);	
	} else{
		setTransPwdFailedError(kony.i18n.getLocalizedString("emptyMBTransPwd"));
		return false;
	}
}

function callCheckChangePINService(tranPassword) {
	var inputParam = {};
    inputParam["password"] = tranPassword;
    inputParam["cardRefId"] = gblCACardRefNumber;
	invokeServiceSecureAsync("changePinVerifyPwdService", inputParam, callBackCheckChangePINService);
}

function callBackCheckChangePINService(status,resulttable){
	if (status == 400) {
        if (resulttable["opstatus"] == 0) {
			popupTractPwd.dismiss();
            dismissLoadingScreen();
            
            gotoEnterConfirmChangePIN();
            
        }else if (resulttable["errCode"] == "VrfyTxPWDErr00001" || resulttable["errCode"] == "VrfyTxPWDErr00002") {
			setTransPwdFailedError(kony.i18n.getLocalizedString("invalidTxnPwd"));
		}else if (resulttable["errCode"] == "VrfyTxPWDErr00003") {
			showTranPwdLockedPopup();
		}else if (resulttable["opstatus"] == 8005) {
			dismissLoadingScreen();
            if (resulttable["errCode"] == "VrfyOTPErr00001") {
                gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
                dismissLoadingScreen();
                showAlert("" + kony.i18n.getLocalizedString("invalidOTP"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00002") {
                dismissLoadingScreen();
                showAlert("" + kony.i18n.getLocalizedString("ECVrfyOTPErr"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00005") {
                dismissLoadingScreen();
                showAlert("" + kony.i18n.getLocalizedString("KeyTokenSerialNumError"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00006") {
            	dismissLoadingScreen();
                gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
                showAlert("" + resulttable["errMessage"], kony.i18n.getLocalizedString("info"));
                return false;
            }else if (resulttable["errCode"] == "VrfyOTPErr00004") {
                dismissLoadingScreen();
                if(isNotBlank(result["errMsg"])){
            	 	showAlert(resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
	            }else{
	            	showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
	            }
                return false;
            }else{
             	dismissLoadingScreen();
              	showAlert("" + kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                return false;
            }
        } else {
            dismissLoadingScreen();
			popupTractPwd.dismiss();
	           	var errorMsgTop = resulttable["errMsg"];
	           	if(!isNotBlank(resulttable["errMsg"])){
	           		errorMsgTop = kony.i18n.getLocalizedString("ECGenOTPRtyErr00001");
	           	}
	            showAlert(errorMsgTop, kony.i18n.getLocalizedString("info"));
        }
    } else {
    	dismissLoadingScreen();
		popupTractPwd.dismiss();
        if (status == 300) {
            showAlert(kony.i18n.getLocalizedString("Receipent_alert_Error"), kony.i18n.getLocalizedString("info"));
        }else{
        	showAlert("status : " + status + " errMsg: " +resulttable["errMsg"] , kony.i18n.getLocalizedString("info"));
        }
    }
}
