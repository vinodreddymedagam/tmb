gblPolicyNumber = "";
function moveToBillPaymentFromBAPolicyDetails() {
    showLoadingScreenPopup();
    
    gblPolicyNumber = frmIBBankAssuranceSummary.lblPolicyNoValue.text;
 	
 	var billPaymentValueDS = gblBAPolicyDetailsResult["billPaymentValueDS"];
    
    if(billPaymentValueDS.length > 0) {
    	compCodeFromSummary = billPaymentValueDS[1]["value_EN"];
    	gblReference1 = billPaymentValueDS[2]["value_EN"];
    	gblReference2 = billPaymentValueDS[3]["value_EN"];
    	gblBAPolicyAmount = billPaymentValueDS[4]["value_EN"];
    	
    	gblCompCode = compCodeFromSummary;
    	gblMyBillerTopUpBB = 0;
	    gblFromAccountSummary = true;
	    gblPayPremium = "Y";
	    clearIBBillPaymentLP();
	    masterBillerForCreditCardLoan();
    } else {
    	dismissLoadingScreenPopup();
    }

}

function moveToBillPaymentFromBAPolicyDetailsMB() {
 
  if(checkMBUserStatus()){
 	showLoadingScreen();
 	
 	gblPolicyNumber = frmMBBankAssuranceDetails.lblPolicyNoValue.text;
 	 
 	var billPaymentValueDS = gblBAPolicyDetailsData["billPaymentValueDS"];
    if(billPaymentValueDS.length > 0) {
    	compCodeFromSummary = billPaymentValueDS[1]["value_EN"];
    	gblReference1 = billPaymentValueDS[2]["value_EN"];
    	gblReference2 = billPaymentValueDS[3]["value_EN"];
    	gblBAPolicyAmount = billPaymentValueDS[4]["value_EN"];
    	
    	gblCompCode = compCodeFromSummary;
    	gblMyBillerTopUpBB = 0;
	    gblFromAccountSummary = true;
	    gblPayPremium = "Y";
		masterBillerForCreditCardLoanMB();
	}else{
		dismissLoadingScreen();
	}
  
    }
}