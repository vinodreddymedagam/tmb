







var tokenFlag;
var gblActMode;
/************************************************Invocation local methods**************************************/
/**
 * Function to control UI header display
 * @param {} mode
 * @returns {}
 */

function controlHeaderTextDisplay(mode) {
	switch (mode) {
	case 0:
		{
			//Apply MB text
			frmApplyMBviaIBStep1.label476047582115259.text = kony.i18n.getLocalizedString("MobileAppSpa");
			gblActMode = "21";
		}
		break;
	case 1:
		{
			//Add Device text
			frmApplyMBviaIBStep1.label476047582115259.text = kony.i18n.getLocalizedString("Add Device");
			gblActMode = "23";
		}
		break;
	case 2:
		{
			//Reset MB password text
			frmApplyMBviaIBStep1.label476047582115259.text = kony.i18n.getLocalizedString("ResetMBPwd");
			gblActMode = "22";
		}
		break;
	default:
		break;
	}
}
/**
 * Method to perform user validation at back end
 * @returns {}
 */

function validateUserApplyServicesIB(mode) {
 		switch (parseInt(mode.toString())) {
		case 21:
			//Apply MB
			startIBValidationService("21");
			//activityLogServiceCall("002", "", "00", "", gblUserName, gblPHONENUMBER, "","", "", "")	
			break;
		case 23:
			//Add Device
			startIBValidationService("23");
			//activityLogServiceCall("003", "", "00", "", gblUserName, gblPHONENUMBER, "","", "", "")	
			
 			break;
		case 22:
			//Reset MB password
			startIBValidationService("22");
						
			break;
		default:
			break;
		}
 }
/**
 * Method to validate the OTP in IB Apply services
 * @returns {}
 */

function validateOTPApplyServicesIB() {
 		startValidateOTPIBApplyServices();
 }

/**
 * Method to check if need token/OTP validation
 */

function validateSecurityForTransaction() {
	if (frmApplyMbviaIBConf.textbox247428873338513.text == null || frmApplyMbviaIBConf.textbox247428873338513.text == "") {
		if(gblTokenSwitchFlag == true)
			alert(kony.i18n.getLocalizedString("Receipent_tokenId"));
			//frmApplyMbviaIBConf.label476047582115288.text="Please enter valid OTP from Token"
		else
			alert(kony.i18n.getLocalizedString("Receipent_alert_correctOTP"));
		  // alert("Please enter valid OTP");
	} else {
				validateOTPApplyServicesIB();
 	}
}

/************************************************Service Invocation methods**************************************/
/**
 * Method to start the validation at back end when invoke apply services before proceeding
 * @returns {}
 */

function startIBValidationService(actCode) {
	var input = {};
	input["actionCode"] = actCode,
	input["citizenID"] = ""; // Not used
	showLoadingScreenPopup();
	invokeServiceSecureAsync("GetCustomerStatus", input, startIBValidationServiceAsyncCallback);
}
/**
 * Callback method for startIBValidationService()
 * @returns {}
 */

function startIBValidationServiceAsyncCallback(status, callBackResponse) {
	if (status == 400) {
		
		if (callBackResponse["opstatus"] == 0) {
			dismissLoadingScreenPopup();
			if (callBackResponse["eligibilityStatus"] == "0") {
				//Add form to navigate to
				frmApplyMbviaIBConf.label476047582115269.text="xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
				frmApplyMbviaIBConf.button449201830493387.hoverSkin=btnIBREQotp;
				frmApplyMbviaIBConf.show();
			} else {
				alert("Apply Services request rejected");
			}
		} else {
			dismissLoadingScreenPopup();
			
		}
	} else {
		if (status == 300) {
			dismissLoadingScreenPopup();
			
		}
	}
}
/**
 * Method to atrt the otp request while apply services in IB
 * @returns {}
 */

function onclickOfOTPRequest(){
			 frmApplyMbviaIBConf.lblOTPinCurr.text = "";
             frmApplyMbviaIBConf.lblPlsReEnter.text = ""; 
             frmApplyMbviaIBConf.hbxOTPincurrect.isVisible = false;
             frmApplyMbviaIBConf.hbox476047582127699.isVisible = true;
             frmApplyMbviaIBConf.hbxOTPsnt.isVisible = true;
             frmApplyMbviaIBConf.textbox247428873338513.setFocus(true);
		if(gblTokenSwitchFlag == true){
			 gblTokenSwitchFlag=false;
			 gblSwitchToken=true;
			 frmApplyMbviaIBConf.label476047582115288.text = kony.i18n.getLocalizedString("keyotpmsgreq");
			 frmApplyMbviaIBConf.label476047582115284.text = "OTP:";
			 frmApplyMbviaIBConf.button449201830493387.text = kony.i18n.getLocalizedString("keyRequest");
			 frmApplyMbviaIBConf.textbox247428873338513.setFocus(true);
		//	 frmApplyMbviaIBConf.textbox247428873338513.placeholder = kony.i18n.getLocalizedString("enterOTP");
			 startRequestOTPBackgndIBApplyServices(gblActMode);
		}else{
			 startRequestOTPIBApplyServices(gblActMode);
		}

}

function startRequestOTPIBApplyServices(mode) {
	
	var eventNotificationPolicy;
	var SMSSubject;
	var Channel
	var locale = kony.i18n.getCurrentLocale();
	switch (parseInt(mode.toString())) {
	case 21:
		//Apply MB
		{
			/*
			if (locale == "en_US") {
				eventNotificationPolicy = "MIB_OTPApply_SVC_EN";
				SMSSubject = "MIB_OTPApply_SVC_EN";
			} else {
				eventNotificationPolicy = "MIB_OTPApply_SVC_TH";
				SMSSubject = "MIB_OTPApply_SVC_TH";
			}*/
			Channel = "ApplyMBViaIB";
		}
		break;
	case 23:
		//Add Device
		{
			/*
			if (locale == "en_US") {
				eventNotificationPolicy = "MIB_OTPAddDeviceViaIB_EN";
				SMSSubject = "MIB_OTPAddDeviceViaIB_EN";
			} else {
				eventNotificationPolicy = "MIB_OTPAddDeviceViaIB_TH";
				SMSSubject = "MIB_OTPAddDeviceViaIB_TH";
			}
			*/
			Channel = "AddDeviceViaIB";
		}
		break;
	case 22:
		//Reset MB password
		{
			/*
			if (locale == "en_US") {
				eventNotificationPolicy = "MIB_OTPResetPWDViaIB_EN";
				SMSSubject = "MIB_OTPResetPWDViaIB_EN";
			} else {
				eventNotificationPolicy = "MIB_OTPResetPWDViaIB_TH";
				SMSSubject = "MIB_OTPResetPWDViaIB_TH";
			}*/
			Channel = "ResetPasswordViaIB";
		}
		break;
	default:
		break;
	}
	
	var inputParams = {
		retryCounterRequestOTP: gblRetryCountRequestOTP,
		Channel: Channel,
		locale: locale
	};
	
	invokeServiceSecureAsync("generateOTPWithUser", inputParams, startRequestOTPIBApplyServicesAsyncCallback);
}
/**
 * Callback method for startRequestOTPIBApplyServices()
 * @returns {}
 */

function startRequestOTPIBApplyServicesAsyncCallback(status, callBackResponse) {
	if (status == 400) {
		
		 if (callBackResponse["errCode"] == "GenOTPRtyErr00002") {
		 		frmApplyMbviaIBConf.button449201830493387.skin = btnIBREQotp; 
				showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00002"), kony.i18n.getLocalizedString("info"));
				return false;
			}
			if (callBackResponse["errCode"] == "GenOTPRtyErr00001") {
				showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
				return false;
			}
		if (callBackResponse["opstatus"] == 0) {
			dismissLoadingScreenPopup();
			if (callBackResponse["errCode"] == "GenOTPRtyErr00001") {
				showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (callBackResponse["errCode"] == "JavaErr00001") {
				showAlert(kony.i18n.getLocalizedString("ECJavaErr00001"), kony.i18n.getLocalizedString("info"));
				return false;
			}
			var reqOtpTimer = kony.os.toNumber(callBackResponse["requestOTPEnableTime"]);
			gblRetryCountRequestOTP = kony.os.toNumber(callBackResponse["retryCounterRequestOTP"]);
			gblOTPLENGTH = kony.os.toNumber(callBackResponse["otpLength"]);
			
			kony.timer.schedule("OTPTimer", OTPTimerCallBack, reqOtpTimer, false);
			frmApplyMbviaIBConf.hbox476047582127699.setVisibility(true);
			frmApplyMbviaIBConf.hbxOTPsnt.setVisibility(true);
			frmApplyMbviaIBConf.textbox247428873338513.setFocus(true);
			frmApplyMbviaIBConf.lblBankRef.text = kony.i18n.getLocalizedString("keybankrefno");
			var refVal="";
			for(var d=0;d<callBackResponse["Collection1"].length;d++){
				if(callBackResponse["Collection1"][d]["keyName"] == "pac"){
						refVal=callBackResponse["Collection1"][d]["ValueString"];
						break;
					}
				}
				frmApplyMbviaIBConf.button449201830493387.skin = btnIBREQotp;
            frmApplyMbviaIBConf.button449201830493387.focusSkin = btnIBREQotp;
            frmApplyMbviaIBConf.button449201830493387.hoverSkin=btnIBREQotp;
			frmApplyMbviaIBConf.label476047582127701.text =refVal// callBackResponse["Collection1"][8]["ValueString"];;
			frmApplyMbviaIBConf.label476047582115277.text = kony.i18n.getLocalizedString("keyotpmsg");
			frmApplyMbviaIBConf.label476047582115279.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
			
		} else {
			dismissLoadingScreenPopup();
			frmApplyMbviaIBConf.textbox247428873338513.setFocus(true);
			
		}
	} else {
		if (status == 300) {
			dismissLoadingScreenPopup();
			
		}
	}
}
/**
 * Method to validate the OTP generated for IB apply services
 * @returns {}
 */

function startValidateOTPIBApplyServices() {
 		var inputParam = {};
 		inputParam["retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
        inputParam["retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
        inputParam["retryCounterVerifyOTP"] = gblVerifyOTPCounter;
        inputParam["password"] = frmApplyMbviaIBConf.textbox247428873338513.text;
        showLoadingScreenPopup();
    if (gblTokenSwitchFlag == true) {
        //inputParam["password"] = frmApplyMbviaIBConf.textbox247428873338513.text;
        //showLoadingScreenPopup();
		//invokeServiceSecureAsync("verifyTokenEx", inputParam, startValidateOTPIBApplyServicesAsyncCallback)
 		inputParam["OTP_TOKEN_FLAG"] = "TOKEN";
    }else{
		//invokeServiceSecureAsync("verifyPasswordEx", inputParams, startValidateOTPIBApplyServicesAsyncCallback);
		inputParam["OTP_TOKEN_FLAG"] = "OTP";
  	}
			  	var activityTypeId = "";
			  	var flex1 = "";
			  	var flex2 = "";
			  	var flex3 = "";
			  	var flex4 = "";
			  	var flex5 = "";
  				switch (parseInt(gblActMode.toString())) {
				case 21:
					activityTypeId = "002";
					flex1 = gblUserName;
					flex2 = gblPHONENUMBER;
					//activityLogServiceCall("002", "", "01", "", gblUserName, gblPHONENUMBER, "", "", "", "")			
 					break;
				case 22:
					activityTypeId = "014";
					flex1 = "Active Customer";
					flex2 = "Reset-PWD Customer";
					flex3 = gblUserName;
					flex4 = gblPHONENUMBER;
				//	activityLogServiceCall("014", "", "01","","Active Customer", "Reset-PWD Customer", gblUserName, gblPHONENUMBER, "", "")	
 					break;
				case 23:
					activityTypeId = "003";
					flex1 = gblUserName;
					flex2 = gblPHONENUMBER;
					//activityLogServiceCall("003", "", "01", "", gblUserName, gblPHONENUMBER, "","", "", "")					
					break;
				default:
					break;
				}
		inputParam["activityTypeID"] = activityTypeId;
		inputParam["activityFlexValues1"] = flex1;	
		inputParam["activityFlexValues2"] = flex2;
		inputParam["activityFlexValues3"] = flex3;		
		inputParam["activityFlexValues4"] = flex4;		
		inputParam["activityFlexValues5"] = flex5;	
		var locale = kony.i18n.getCurrentLocale();
		if (locale == "en_US") {
			inputParam["language"] = "EN";
		} else {
			inputParam["language"] = "TH";
		}			
  	invokeServiceSecureAsync("RequestGenActCodeApplyExecute", inputParam, startValidateOTPIBApplyServicesAsyncCallback)
}
/**
 * Callback method for startValidateOTPIBApplyServices()
 * @returns {}
 */

function startValidateOTPIBApplyServicesAsyncCallback(status, callBackResponse) {
	if (status == 400) {
		frmApplyMbviaIBConf.textbox247428873338513.text="";
		
		if (callBackResponse["opstatusVPX"] == 0) {
			gblVerifyOTPCounter = "0";
			callBackstartApplyServicesReqGenActCodeIB(callBackResponse);
			
			//Add code to proceed here
		} else if (callBackResponse["opstatusVPX"] == 8005) {
			dismissLoadingScreenPopup();
			if (callBackResponse["errCode"] == "VrfyOTPErr00001") {
				//if (gblTokenSwitchFlag == true) 
//				 	alert(kony.i18n.getLocalizedString("ECVrfyOTPErr00005"));
//				else
					//alert("Helooo my msg" + kony.i18n.getLocalizedString("invalidOTP"));//commeted by swapna
                    frmApplyMbviaIBConf.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");//kony.i18n.getLocalizedString("invalidOTP"); //
                    frmApplyMbviaIBConf.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
                    frmApplyMbviaIBConf.hbxOTPincurrect.isVisible = true;
                    frmApplyMbviaIBConf.hbox476047582127699.isVisible = false;
                    frmApplyMbviaIBConf.hbxOTPsnt.isVisible = false;
                    frmApplyMbviaIBConf.textbox247428873338513.text = "";
                    frmApplyMbviaIBConf.textbox247428873338513.setFocus(true);
                    
				return false;
			} else if (callBackResponse["errCode"] == "VrfyOTPErr00002") {
					//startRcCrmUpdateProIB("04");
           
					handleOTPLockedIB(callBackResponse);
					return false;
			}else if (callBackResponse["errCode"] == "VrfyOTPErr00005") {
				//dismissLoadingScreenPopup();
				//alert("" + kony.i18n.getLocalizedString("KeyTokenSerialNumError"));
				//alert(kony.i18n.getLocalizedString("invalidOTP"));//commeted by swapna
				    frmApplyMbviaIBConf.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");//kony.i18n.getLocalizedString("invalidOTP"); //
                    frmApplyMbviaIBConf.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo"); 
                    frmApplyMbviaIBConf.hbxOTPincurrect.isVisible = true;
                    frmApplyMbviaIBConf.hbox476047582127699.isVisible = false;
                    frmApplyMbviaIBConf.hbxOTPsnt.isVisible = false;
                    frmApplyMbviaIBConf.textbox247428873338513.text = "";
                    frmApplyMbviaIBConf.textbox247428873338513.setFocus(true);
				return false;
			}else{
			        frmApplyMbviaIBConf.lblOTPinCurr.text = "";
                    frmApplyMbviaIBConf.lblPlsReEnter.text = ""; 
                    frmApplyMbviaIBConf.hbxOTPincurrect.isVisible = false;
	                frmApplyMbviaIBConf.hbox476047582127699.isVisible = true;
	                frmApplyMbviaIBConf.hbxOTPsnt.isVisible = true;
	     			dismissLoadingScreenPopup();
		    		frmApplyMbviaIBConf.textbox247428873338513.setFocus(true);
			    	alert("" + callBackResponse["errMsg"]);
			}
		} else {
				dismissLoadingScreenPopup();
				alert("" + callBackResponse["errMsg"]);
		}
	} else {
		if (status == 300) {
			dismissLoadingScreenPopup();
			
		}
	}
}

function OTPTimerCallBack() {
	//frmApplyMbviaIBConf.btnOTPReq.skin=btnIB158;
	//frmApplyMbviaIBConf.button449201830493387.onClick = startRequestOTPIBApplyServices;
 	frmApplyMbviaIBConf.button449201830493387.setEnabled(true);
	frmApplyMbviaIBConf.button449201830493387.skin = btnIBREQotpFocus;
	frmApplyMbviaIBConf.button449201830493387.hoverSkin=btnibreqotpfoc;
	try {
		kony.timer.cancel("OTPTimer");
	} catch (e) {
		
	}
}

//function applyResetPwdOTP() {
//	var inputParams = {};
//	inputParams["crmId"] = gblcrmId;
//	inputParams["rmId"] = "";
//	inputParams["ibUserId"] = "";
//	inputParams["activityTypeId"] = "";
//	invokeServiceSecureAsync("addResetPwdOTP", inputParams, applyResetPwdOTPCallBack);
//}

//function applyResetPwdOTPCallBack(status, result) {
//	if (status == 400) {
//		
//		if (result["opstatus"] == 0) {
//			dismissLoadingScreenPopup();
//			if (result["Results"][0]["Results"] == "ok") {
//				showCommonAlert("Success", null);
//				///@to Navigate to the Success form.
//			} else {
//				showCommonAlert("TMB Server is down, Please conatact TMB Helpdesk", null)
//			}
//		} else {
//			dismissLoadingScreenPopup();
//			showCommonAlert("" + result["errMsg"], null);
//		}
//	}
//}

//function srvLockAccnt() {
//	var inputParams = {};
//	invokeServiceSecureAsync("crmProfileUpdate", inputParams, srvLockAccntCallBack);
//}

//function srvLockAccntCallBack(status, resulttable) {
//	if (status == 400) {
//		
//		if (resulttable["opstatus"] == 0) {
//			
//		} else {
//			showCommonAlert("" + result["errMsg"], null);
//		}
//	}
//}
/**
 * Method to validate the OTP generated for IB apply services
 * @returns {}
 */

//function startCrmUpdateProIBApplyServices(iblock) {
//	var inputParams = {
//		serviceID: "crmProfileMod",
//		ibUserStatusId: iblock.toString()
//	};
//	showLoadingScreenPopup();
//	invokeServiceSecureAsync("crmProfileMod", inputParams, startCrmUpdateProIBApplyServicesAsyncCallback);
//}
/**
 * Callback method for startCrmUpdateProIBApplyServices()
 * @returns {}
 */

//function startCrmUpdateProIBApplyServicesAsyncCallback(status, callBackResponse) {
//	if (status == 400) {
//		if (callBackResponse["opstatus"] == 0) {
//	gblUserLockStatusIB=resulttable["IBUserStatusID"];
//			dismissLoadingScreenPopup();
//			
//		} else {
//			dismissLoadingScreenPopup();
//			showCommonAlert("Error " + callBackResponse["errMsg"], null);
//		}
//	} else {
//		if (status == 300) {
//			dismissLoadingScreenPopup();
//			
//		}
//	}
//}
/**
 * Method to atrt the otp request while apply services in IB
 * @returns {}
 */

function startRequestOTPBackgndIBApplyServices(mode) {
	
	var eventNotificationPolicy;
	var SMSSubject;
	var Channel
	var locale = kony.i18n.getCurrentLocale();
switch (parseInt(mode.toString())) {
	case 21:
		//Apply MB
		{
			/*
			if (locale == "en_US") {
				eventNotificationPolicy = "MIB_OTPApply_SVC_EN";
				SMSSubject = "MIB_OTPApply_SVC_EN";
			} else {
				eventNotificationPolicy = "MIB_OTPApply_SVC_TH";
				SMSSubject = "MIB_OTPApply_SVC_TH";
			}*/
			Channel = "ApplyMBViaIB";
		}
		break;
	case 23:
		//Add Device
		{
			/*
			if (locale == "en_US") {
				eventNotificationPolicy = "MIB_OTPAddDeviceViaIB_EN";
				SMSSubject = "MIB_OTPAddDeviceViaIB_EN";
			} else {
				eventNotificationPolicy = "MIB_OTPAddDeviceViaIB_TH";
				SMSSubject = "MIB_OTPAddDeviceViaIB_TH";
			}*/
			Channel = "AddDeviceViaIB";
		}
		break;
	case 22:
		//Reset MB password
		{
			/*
			if (locale == "en_US") {
				eventNotificationPolicy = "MIB_OTPResetPWDViaIB_EN";
				SMSSubject = "MIB_OTPResetPWDViaIB_EN";
			} else {
				eventNotificationPolicy = "MIB_OTPResetPWDViaIB_TH";
				SMSSubject = "MIB_OTPResetPWDViaIB_TH";
			}*/
			Channel = "ResetPasswordViaIB";
		}
		break;
		default:
		break;
	}
	
	var inputParams = {
		retryCounterRequestOTP: gblRetryCountRequestOTP,
		Channel: Channel,
		locale: locale
	};
	invokeServiceSecureAsync("generateOTPWithUser", inputParams, startRequestOTPBackgndIBApplyServicesAsyncCallback);
}
/**
 * Callback method for startRequestOTPBackgndIBApplyServices()
 * @returns {}
 */

function startRequestOTPBackgndIBApplyServicesAsyncCallback(status, callBackResponse) {
	if (status == 400) {
		
			if (callBackResponse["errCode"] == "GenOTPRtyErr00001") {
                    dismissLoadingScreenPopup();
                    showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                    return false;
           	}		
		if (callBackResponse["opstatus"] == 0) {
			if (callBackResponse["errCode"] == "GenOTPRtyErr00001") {
				return false;
			} else if (callBackResponse["errCode"] == "JavaErr00001") {
				return false;
			}
			
			var refVal="";
			for(var d=0;d<callBackResponse["Collection1"].length;d++){
				if(callBackResponse["Collection1"][d]["keyName"] == "pac"){
						refVal=callBackResponse["Collection1"][d]["ValueString"];
						break;
					}
				}
			frmApplyMbviaIBConf.hbox476047582127699.setVisibility(true);
			frmApplyMbviaIBConf.hbxOTPsnt.setVisibility(true);
			frmApplyMbviaIBConf.textbox247428873338513.setFocus(true);
			frmApplyMbviaIBConf.lblBankRef.text = kony.i18n.getLocalizedString("keybankrefno");
			frmApplyMbviaIBConf.label476047582127701.text = refVal;
			var reqOtpTimer = (kony.os.toNumber(callBackResponse["requestOTPEnableTime"]));
			gblRetryCountRequestOTP = kony.os.toNumber(callBackResponse["retryCounterRequestOTP"]);
			gblOTPLENGTH = kony.os.toNumber(callBackResponse["otpLength"]);
			
			kony.timer.schedule("OTPTimer", OTPTimerCallBack, reqOtpTimer, false);
			frmApplyMbviaIBConf.button449201830493387.skin = btnIBREQotp;
            frmApplyMbviaIBConf.button449201830493387.focusSkin = btnIBREQotp;
            frmApplyMbviaIBConf.button449201830493387.hoverSkin=btnIBREQotp;
            frmApplyMbviaIBConf.button449201830493387.setEnabled(false) ;
			frmApplyMbviaIBConf.label476047582115277.text = kony.i18n.getLocalizedString("keyotpmsg");
			frmApplyMbviaIBConf.label476047582115279.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
			
			
		} else {
			
		}
	} else {
		if (status == 300) {
			
		}
	}
}
/**
 * Method to invoke service that validates the Token
 * @returns {}
 */

/*function startIBApplyServicesTokenValidationService(tokentext) {
	var tokenText = tokentext;
	var inputParams = {
		token: tokenText
	};
	showLoadingScreenPopup();
	invokeServiceSecureAsync("verifyToken", inputParams, startRequestOTPBackgndIBApplyServicesAsyncCallback);
}*/

/**
 * callBackstartApplyServicesReqGenActCodeIB
 * @returns {}
 */

function callBackstartApplyServicesReqGenActCodeIB(resulttable) {
		frmApplyMBviaIBConfirmation.hbox866794482148687.setVisibility(false);
		//CR15.3 implemenation
		var applyMb = false;
		
		if (resulttable["opstatusReqGen"] == 0) {
			if (resulttable["eligibilityStatus"] == "0") {
				//Success
				var text;
 				switch (parseInt(gblActMode.toString())) {
				case 21:
					text = kony.i18n.getLocalizedString("activationCodeSentIB");
					//activityLogServiceCall("002", "", "01", "", gblUserName, gblPHONENUMBER, "", "", "", "")	
					applyMb = true;		
 					break;
				case 22:
					text = kony.i18n.getLocalizedString("activationCodeSentUnlockMBanking");
				//	activityLogServiceCall("014", "", "01","","Active Customer", "Reset-PWD Customer", gblUserName, gblPHONENUMBER, "", "")	
 					break;
				case 23:
					text = kony.i18n.getLocalizedString("activationCodeSentNewDevice");
					//activityLogServiceCall("003", "", "01", "", gblUserName, gblPHONENUMBER, "","", "", "")					
					break;
				default:
					break;
				}
				frmApplyMBviaIBConfirmation.show();
				dismissLoadingScreenPopup();
				frmApplyMBviaIBConfirmation.lblApplyServConf.text = text;
				if(applyMb == true){
					//frmApplyMBviaIBConfirmation.btnConfirm1.setVisibility(false);
					frmApplyMBviaIBConfirmation.hbox866794482148687.setVisibility(true);
				}
				
			} else {
				dismissLoadingScreenPopup();
				showCommonAlert(" " + "Request failed", null);
			}
		} else {
			dismissLoadingScreenPopup();
			showCommonAlert(" " + resulttable["errMsg"], null);
		}
	
}
/**
 * Mrthod to handle locale change for IB Apply services main screen
 * @returns {}
 */

function handleLocaleChangeIBApplyServiceMain() {
	controlHeaderTextDisplay(gblMode);
	frmApplyMBviaIBStep1.label476047582115267.text = kony.i18n.getLocalizedString("wewillsendinfo");
	frmApplyMBviaIBStep1.richtext475034688127909.text = kony.i18n.getLocalizedString("myKey");
	frmApplyMBviaIBStep1.btnConfirm1.text = kony.i18n.getLocalizedString("Next");
}
/**
 * Method to handle locale change for Ib Apply Services conf screen
 * @returns {}
 */

function handleLocaleChangeIBApplyServiceConf() {
	frmApplyMbviaIBConf.label476047582115259.text = kony.i18n.getLocalizedString("Confirmation");
	frmApplyMbviaIBConf.lblBankRef.text = kony.i18n.getLocalizedString("keybankrefno");
	frmApplyMbviaIBConf.label476047582115277.text = kony.i18n.getLocalizedString("keyotpmsg");
	frmApplyMbviaIBConf.label476047582115288.text = kony.i18n.getLocalizedString("keyotpmsgreq");
	frmApplyMbviaIBConf.button476047582115263.text = kony.i18n.getLocalizedString("keyCancelButton");
	frmApplyMbviaIBConf.button476047582115265.text = kony.i18n.getLocalizedString("keyConfirm");
//	frmApplyMbviaIBConf.textbox247428873338513.placeholder=	kony.i18n.getLocalizedString("enterOTP");
	handleOTPToken();
}

function handleOTPToken(){
	 var currForm = kony.application.getCurrentForm().id;
	

	if(gblSwitchToken == false && gblTokenSwitchFlag == false){
		checkApplyMBTokenFlag();
	} 
	if(gblTokenSwitchFlag == true && gblSwitchToken == false){
		frmApplyMbviaIBConf.button449201830493387.text = kony.i18n.getLocalizedString("Receipent_SwitchtoOTP");
		frmApplyMbviaIBConf.textbox247428873338513.setFocus(true);
		frmApplyMbviaIBConf.label476047582115284.text = kony.i18n.getLocalizedString("Receipent_Token");
		frmApplyMbviaIBConf.label476047582115277.text = kony.i18n.getLocalizedString("Receipent_Token");
		frmApplyMbviaIBConf.label476047582115288.text = kony.i18n.getLocalizedString("Receipent_tokenId");
	//	frmApplyMbviaIBConf.textbox247428873338513.placeholder=	kony.i18n.getLocalizedString("enterToken");		                 
	} else if(gblTokenSwitchFlag ==false && gblSwitchToken == true) {
		frmApplyMbviaIBConf.button449201830493387.text = kony.i18n.getLocalizedString("keyRequest");
		frmApplyMbviaIBConf.textbox247428873338513.setFocus(true);
		frmApplyMbviaIBConf.label476047582115277.text = kony.i18n.getLocalizedString("keyotpmsg");
		frmApplyMbviaIBConf.label476047582115288.text = kony.i18n.getLocalizedString("keyotpmsgreq");
	//	frmApplyMbviaIBConf.textbox247428873338513.placeholder=	kony.i18n.getLocalizedString("enterOTP");	
		startRequestOTPIBApplyServices(gblActMode);
				             
	}

}


function checkApplyMBTokenFlag() {
		var inputParam = [];
		inputParam["crmId"] = gblcrmId;
		showLoadingScreen();
		invokeServiceSecureAsync("tokenSwitching", inputParam, applyMBTokenFlagCallbackfunction);
}



function applyMBTokenFlagCallbackfunction(status,callbackResponse){
		var currForm = kony.application.getCurrentForm().id;
		
		if(status==400){
				if(callbackResponse["opstatus"] == 0){
					dismissLoadingScreen();
					if(callbackResponse["deviceFlag"].length==0){
						
						//return
					}
					if ( callbackResponse["deviceFlag"].length > 0 ){
							var tokenFlag = callbackResponse["deviceFlag"][0]["TOKEN_DEVICE_FLAG"];
							var mediaPreference = callbackResponse["deviceFlag"][0]["MEDIA_PREFERENCE"];
							var tokenStatus = callbackResponse["deviceFlag"][0]["TOKEN_STATUS_ID"];
							if ( tokenFlag=="Y" && (mediaPreference == "Token" || mediaPreference == "TOKEN") && tokenStatus=='02'){
							    frmApplyMbviaIBConf.button449201830493387.text = kony.i18n.getLocalizedString("Receipent_SwitchtoOTP");
							    frmApplyMbviaIBConf.textbox247428873338513.setFocus(true); 
							    frmApplyMbviaIBConf.label476047582115284.text = kony.i18n.getLocalizedString("Receipent_Token");
								frmApplyMbviaIBConf.label476047582115277.text = kony.i18n.getLocalizedString("Receipent_Token");
								frmApplyMbviaIBConf.label476047582115288.text = kony.i18n.getLocalizedString("Receipent_tokenId");
						//		frmApplyMbviaIBConf.textbox247428873338513.placeholder=	kony.i18n.getLocalizedString("enterToken");
								gblTokenSwitchFlag = true;
						} else {						
								gblTokenSwitchFlag = false;
								gblSwitchToken=true;
								frmApplyMbviaIBConf.button449201830493387.text = kony.i18n.getLocalizedString("keyRequest");
								frmApplyMbviaIBConf.label476047582115277.text = kony.i18n.getLocalizedString("keyotpmsg");
								frmApplyMbviaIBConf.label476047582115288.text = kony.i18n.getLocalizedString("keyotpmsgreq");
					//			frmApplyMbviaIBConf.textbox247428873338513.placeholder=	kony.i18n.getLocalizedString("enterOTP");
								startRequestOTPIBApplyServices(gblActMode);
						}
					}
				 else//deviceFlag length is 0.
					{
					 			frmApplyMbviaIBConf.button449201830493387.text = kony.i18n.getLocalizedString("keyRequest");
					 			frmApplyMbviaIBConf.textbox247428873338513.setFocus(true);
								frmApplyMbviaIBConf.label476047582115277.text = kony.i18n.getLocalizedString("keyotpmsg");
								frmApplyMbviaIBConf.label476047582115288.text = kony.i18n.getLocalizedString("keyotpmsgreq");
					//			frmApplyMbviaIBConf.textbox247428873338513.placeholder=	kony.i18n.getLocalizedString("enterOTP");
								gblTokenSwitchFlag = false;
								gblSwitchToken=true;
								startRequestOTPIBApplyServices(gblActMode);
								            
					 }
						
				}
		}
		
} 




function handleLocaleChangeApplyServicesConfirmation() {
	frmApplyMBviaIBConfirmation.label476047582115259.text = kony.i18n.getLocalizedString("Complete");
	var text;
	switch (parseInt(gblActMode.toString())) {
	case 21:
		text = kony.i18n.getLocalizedString("activationCodeSentIB");
		break;
	case 22:
		text = kony.i18n.getLocalizedString("activationCodeSentUnlockMBanking");
		break;
	case 23:
		text = kony.i18n.getLocalizedString("activationCodeSentNewDevice");
		break;
	default:
		break;
	}
	frmApplyMBviaIBConfirmation.lblApplyServConf.text = text;
	frmApplyMBviaIBConfirmation.btnConfirm1.text = kony.i18n.getLocalizedString("retToHome");
}
/**
 * Method to show the message in the conformation page
 * @returns {}
 */

function handleMessageConfPreShow() {
	var text;
	var applyMb = false;
	switch (parseInt(gblActMode.toString())) {
	case 21:
		applyMb = true;
		text = kony.i18n.getLocalizedString("activationCodeSentIB");
		break;
	case 22:
		text = kony.i18n.getLocalizedString("activationCodeSentUnlockMBanking");
		break;
	case 23:
		text = kony.i18n.getLocalizedString("activationCodeSentNewDevice");
		break;
	default:
		break;
	}
	frmApplyMBviaIBConfirmation.lblApplyServConf.text = text;
	if(applyMb == true){
		//frmApplyMBviaIBConfirmation.btnConfirm1.setVisibility(false);
		frmApplyMBviaIBConfirmation.hbox866794482148687.setVisibility(true);
	}else{
		frmApplyMBviaIBConfirmation.btnConfirm1.setVisibility(true);
		frmApplyMBviaIBConfirmation.hbox866794482148687.setVisibility(false);
	}
}
/**
 * Method to handle locale change for IB Apply services module
 * @returns {}
 */

function syncIBApplyServicesLocale() {
	var currentFormId = kony.application.getCurrentForm()
		.id;
	if (currentFormId == "frmApplyMBviaIBStep1") {
		handleLocaleChangeIBApplyServiceMain();
	} else if (currentFormId == "frmApplyMbviaIBConf") {
		handleLocaleChangeIBApplyServiceConf();
	} else if (currentFormId == "frmApplyMBviaIBConfirmation") {
		handleLocaleChangeApplyServicesConfirmation();
	}
	if (gblLoggedIn) {} // tells if the user has logged in or not
	//syncMenuLocale();
	if(currentFormId == "frmIBSSApplyCnfrmtn"){
		//frmIBSSApplyCnfrmtn.txtBxOTP.text = kony.i18n.getLocalizedString("enterOTP");
		frmIBSSApplyCnfrmtn.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
		frmIBSSApplyCnfrmtn.lblAcc.text = kony.i18n.getLocalizedString("keyIBAccountNo");
		frmIBSSApplyCnfrmtn.lblAcc2.text = kony.i18n.getLocalizedString("keyIBAccountNo");
	}
	if(currentFormId == "frmIBSSApplyCmplete"){
		frmIBSSApplyCmplete.lblAcc.text = kony.i18n.getLocalizedString("keyIBAccountNo");
		frmIBSSApplyCmplete.lblAcc2.text = kony.i18n.getLocalizedString("keyIBAccountNo");
		frmIBSSApplyCmplete.label446549564755399.text = kony.i18n.getLocalizedString("lblAplyDate");
	}
}
/*
 * Function not required as language of menu is changed dynamically 

function syncMenuLocale(){
	var currentFormId = kony.application.getCurrentForm();
	currentFormId.label502795003260673.text = kony.i18n.getLocalizedString('keyIBMenuTransfer')
	currentFormId.label502795003260713.text = kony.i18n.getLocalizedString('keyIBMenuMyActivities')
	currentFormId.label502795003260677.text = kony.i18n.getLocalizedString('keyIBMenuCardlessWithdrawal')
	currentFormId.label502795003260691.text = kony.i18n.getLocalizedString('keyIBMenuMyAccountSummary')
	currentFormId.label502795003260701.text = kony.i18n.getLocalizedString('keyIBMenuBillPayment')
	currentFormId.label502795003260705.text = kony.i18n.getLocalizedString('keyIBMenuTopUp')
	currentFormId.label502795003260681.text = kony.i18n.getLocalizedString('keyIBMenuAboutMe')
	currentFormId.label502795003260695.text = kony.i18n.getLocalizedString('keyIBMenuMyInbox')
	currentFormId.label502795003260709.text = kony.i18n.getLocalizedString('keyIBMenuConvenientServices')
}
*/


/*function checkConvinentServTokenFlag() {
		var inputParam = [];
		inputParam["crmId"] = gblcrmId;
		showLoadingScreen();
		invokeServiceSecureAsync("tokenSwitching", inputParam, ibConvinentServTokenFlagCallbackfunction);
}*/
//function ibConvinentServTokenFlagCallbackfunction(status,callbackResponse){
//		var currForm = kony.application.getCurrentForm().id;
//		
//		if(status==400){
//				if(callbackResponse["opstatus"] == 0){
//					dismissLoadingScreen();
//					if(callbackResponse["deviceFlag"].length==0){
//						
//						//return
//					}
//					if ( callbackResponse["deviceFlag"].length > 0 ){
//							var tokenFlag = callbackResponse["deviceFlag"][0]["TOKEN_DEVICE_FLAG"];
//							var mediaPreference = callbackResponse["deviceFlag"][0]["MEDIA_PREFERENCE"];
//							
//							if ( tokenFlag=="Y" && mediaPreference == "Token" ){
//									frmApplyMbviaIBConf.label476047582115288.text = kony.i18n.getLocalizedString("Receipent_tokenId");
//									frmApplyMbviaIBConf.label476047582115284.text = kony.i18n.getLocalizedString("Receipent_Token");
//									frmApplyMbviaIBConf.button449201830493387.text = kony.i18n.getLocalizedString("Receipent_SwitchtoOTP");
//									gblTokenSwitchFlag = true;
//							} else {
//									frmApplyMbviaIBConf.label476047582115288.text = kony.i18n.getLocalizedString("keyotpmsgreq");
//									frmApplyMbviaIBConf.label476047582115284.text = "OTP:";
//									frmApplyMbviaIBConf.button449201830493387.text = kony.i18n.getLocalizedString("keyRequest");
//				//					frmApplyMbviaIBConf.textbox247428873338513.placeholder = "Enter OTP";
//									startRequestOTPBackgndIBApplyServices(gblActMode);
//									gblTokenSwitchFlag = false;
//									
//							}
//						}
//					 else
//					{
//					 
//									frmApplyMbviaIBConf.label476047582115288.text = kony.i18n.getLocalizedString("keyotpmsgreq");
//									frmApplyMbviaIBConf.label476047582115284.text = "OTP:";
//									frmApplyMbviaIBConf.button449201830493387.text = kony.i18n.getLocalizedString("keyRequest");
//				//					frmApplyMbviaIBConf.textbox247428873338513.placeholder = "Enter OTP";
//									gblTokenSwitchFlag = false;
//									startRequestOTPBackgndIBApplyServices(gblActMode);
//					 }
//						
//				}
//		}
//		
//} 

function navigateToAppleStore(){

	var url = kony.i18n.getLocalizedString("applestorelink");
	kony.application.openURL(url);

}

function navigateToAppleStoreIphone(){

	var url = kony.i18n.getLocalizedString("applestorelink");
	location.href=url;

}

function navigateToGooglePlaystore(){

	var url = kony.i18n.getLocalizedString("googleplaystorelink");
	kony.application.openURL(url);

}
