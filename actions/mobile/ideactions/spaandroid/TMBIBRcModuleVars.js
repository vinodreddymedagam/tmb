//Variables for FB confirmation
var globalTobeAddedFBDataConfirmation = [];
//Variables for usage for data caching and search & other constants
var globalTobeAddedBankData = [];
var globalTobeAddedBankDataConfirmation = [];
var globalRcData = [];
var globalFBData = [];
var globalFBCacheData = [];
var globalFBConfData = [];
var globalSelectBankData = [];
var MAX_RECIPIENT_COUNT = 99;
var MAX_BANK_ACCNT_BULK_ADD = 2;
var MAX_BANK_ACCNT_PER_RC = 50;
var globalSeletedRcIndex = -1;
//General Variables & constants
var collectionData;
var formId;
var offset;
//var cacheoffset;
var tokenFlag = false;
var localIBLoginSuccessDate;
//var crmRcInqData = [];
var TMB_BANK_CODE = "11";
var TMB_BANK_CODE_ZERO_PAD = "011";
var TMB_BANK_FIXED_CODE = "0001";
var TMB_BANK_CODE_ADD = "0011";
var ZERO_PAD = "0000";
var securityValFlag = 0; //1 if token, 2 if OTP else if not set then 0
var BANK_LOGO_URL;
var RC_LOGO_URL;
var FACEBOOK_SERVLET_URL;
var gblFBId;
var gblInvokeFbListingSrv = false;
var FB_FRIENDS_SIZE_PER_CALL = 100;
var curFBFriendRetVal;
var isAddFb = false;
var bankShortNameList = [];
var confirmFlag = true;
var otherBankMyAccounts = [];