var gblsegdata = [];
var gblfiterdata = [];
var nopages = 0;
var currentpage = 1;
var gblsegfuturedata = [];
var nopagesfuture = 0;
var currentpagefuture = 1;
var recordcount = 10;
var gblXferSplit = [];
var sortingOrder = "descending";
var sortingOrderFI = "acsending";
var frmIBMyActivities_History_temp = [];
gblActivityIds = "";
/**
 * Loading Page numbersInFuture Transaction
 */
function loadPagenumbersInFutureTrans() {
    frmIBMyActivities.link1future.text = "";
    frmIBMyActivities.link2future.text = "";
    frmIBMyActivities.link3future.text = "";
    frmIBMyActivities.link4future.text = "";
    frmIBMyActivities.link5future.text = "";
    frmIBMyActivities.link1future.skin = lnkNormal;
    frmIBMyActivities.link2future.skin = lnkIB18pxBlueUnderline;
    frmIBMyActivities.link3future.skin = lnkIB18pxBlueUnderline;
    frmIBMyActivities.link4future.skin = lnkIB18pxBlueUnderline;
    frmIBMyActivities.link5future.skin = lnkIB18pxBlueUnderline;
    var seglength = gblsegfuturedata.length;
    nopagesfuture = Math.ceil(seglength / recordcount);
    invokeCommonIBLogger("page nos" + nopagesfuture);
    var pageshown = currentpagefuture;
    if (currentpagefuture <= nopagesfuture) frmIBMyActivities.link1future.text = currentpagefuture;
    currentpagefuture = currentpagefuture + 1;
    if (currentpagefuture <= nopagesfuture) frmIBMyActivities.link2future.text = currentpagefuture;
    currentpagefuture = currentpagefuture + 1;
    if (currentpagefuture <= nopagesfuture) frmIBMyActivities.link3future.text = currentpagefuture;
    currentpagefuture = currentpagefuture + 1;
    if (currentpagefuture <= nopagesfuture) frmIBMyActivities.link4future.text = currentpagefuture;
    currentpagefuture = currentpagefuture + 1;
    if (currentpagefuture <= nopagesfuture) frmIBMyActivities.link5future.text = currentpagefuture;
    loadsegdataInFutureTrans(pageshown);
}
/**
 * Loading data to datagrid based on the pagination InFuture Transaction
 */
function loadsegdataInFutureTrans(page) {
    currentpagefuture = page;
    if (parseInt(frmIBMyActivities.link5future.text) < nopagesfuture) {
        frmIBMyActivities.linknextfuture.setVisibility(true);
    } else {
        frmIBMyActivities.linknextfuture.setVisibility(false);
    }
    if (parseInt(frmIBMyActivities.link1future.text) > 1) {
        frmIBMyActivities.linkprevfuture.setVisibility(true);
    } else {
        frmIBMyActivities.linkprevfuture.setVisibility(false);
    }
    var countdata = [];
    var dataindex = parseInt(page) * recordcount - recordcount;
    for (var i = 0; i < recordcount; i++) {
        if (dataindex < gblsegfuturedata.length) countdata[i] = gblsegfuturedata[dataindex];
        dataindex++;
    }
    frmIBMyActivities.datagridFT.setData(countdata);
}

function nextPageloadInFutureTrans(nextpage) {
    currentpagefuture = parseInt(nextpage) + 1;
    var pageshown = currentpagefuture;
    frmIBMyActivities.link1future.text = currentpagefuture - 4;
    frmIBMyActivities.link2future.text = currentpagefuture - 3;
    frmIBMyActivities.link3future.text = currentpagefuture - 2;
    frmIBMyActivities.link4future.text = currentpagefuture - 1;
    frmIBMyActivities.link5future.text = currentpagefuture;
    frmIBMyActivities.link5future.skin = lnkNormal;
    frmIBMyActivities.link2future.skin = lnkIB18pxBlueUnderline;
    frmIBMyActivities.link3future.skin = lnkIB18pxBlueUnderline;
    frmIBMyActivities.link4future.skin = lnkIB18pxBlueUnderline;
    frmIBMyActivities.link1future.skin = lnkIB18pxBlueUnderline;
    loadsegdataInFutureTrans(pageshown);
}

function prevPageloadInFutureTrans(prevpage) {
    currentpagefuture = parseInt(prevpage) - 1;
    var pageshown = currentpagefuture;
    frmIBMyActivities.link1future.text = currentpagefuture;
    frmIBMyActivities.link2future.text = currentpagefuture + 1;
    frmIBMyActivities.link3future.text = currentpagefuture + 2;
    frmIBMyActivities.link4future.text = currentpagefuture + 3;
    frmIBMyActivities.link5future.text = currentpagefuture + 4;
    frmIBMyActivities.link1future.skin = lnkNormal;
    frmIBMyActivities.link2future.skin = lnkIB18pxBlueUnderline;
    frmIBMyActivities.link3future.skin = lnkIB18pxBlueUnderline;
    frmIBMyActivities.link4future.skin = lnkIB18pxBlueUnderline;
    frmIBMyActivities.link5future.skin = lnkIB18pxBlueUnderline;
    loadsegdataInFutureTrans(pageshown);
}
/**
 * Sort Data based on the sorting criteria in the column in datagrid
 */
function sortDate(str) {
    var mdy = str.split('/')
    return new Date(mdy[2], mdy[1] - 1, mdy[0]);
}

function rowclick() {
    var index3 = frmIBMyActivities.datagridhistory.selectedItem["column2"]["lbltransid"]["text"];
    invokeCommonIBLogger("selected index1" + index3);
    var index3 = frmIBMyActivities.datagridhistory.selectedItem["column4"]["lbltransid"]["text"];
    invokeCommonIBLogger("selected index1" + index3);
    var index3 = frmIBMyActivities.datagridhistory.selectedItem["column3"]["lbltransid"]["text"];
    invokeCommonIBLogger("selected index1" + index3);
    var index3 = frmIBMyActivities.datagridhistory.selectedItem["column7"]["lbltransid"]["text"];
    invokeCommonIBLogger("selected index1" + index3);
    var index3 = frmIBMyActivities.datagridhistory.selectedItem["column6"]["lbltransid"]["text"];
    invokeCommonIBLogger("selected index1" + index3);
    var index3 = frmIBMyActivities.datagridhistory.selectedItem["column4"]["lblno"]["text"];
    invokeCommonIBLogger("selected index1" + index3);
    var index = frmIBMyActivities.datagridhistory.selectedIndex;
    invokeCommonIBLogger("selected index" + index);
}
/**
 * loading column header text in DGHistory
 */
function columnHeaderInDGHistory() {
    var headertext = frmIBDateSlider.datagridhistory.columnHeadersConfig;
    headertext[0].columnheadertemplate.data.lbldate.text = kony.i18n.getLocalizedString("MyActivitiesIB_Date");
    headertext[1].columnheadertemplate.data.lbldate.text = kony.i18n.getLocalizedString("keyType");
    headertext[2].columnheadertemplate.data.lbldate.text = kony.i18n.getLocalizedString("keyMyActivityFrom");
    headertext[3].columnheadertemplate.data.lbldate.text = kony.i18n.getLocalizedString("MyActivitiesIB_To");
    headertext[4].columnheadertemplate.data.lbldate.text = kony.i18n.getLocalizedString("MyActivitiesIB_Amount");
    headertext[5].columnheadertemplate.data.lbldate.text = kony.i18n.getLocalizedString("MyActivitiesIB_Channel");
    //headertext[6].columnheadertemplate.data.lbldate.text = kony.i18n.getLocalizedString("MyActivitiesIB_Reason");
}
/**
 * loading column header text in DGFutureTrans
 */
function columnHeaderInDGFutureTrans() {
    var headertext = frmIBMyActivities.datagridFT.columnHeadersConfig;
    headertext[0].columnheadertemplate.data.lbldate.text = kony.i18n.getLocalizedString("MyActivitiesIB_Executing");
    headertext[1].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("MyActivitiesIB_Transaction");
    headertext[2].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("keyMyActivityFrom");
    headertext[3].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("MyActivitiesIB_To");
    headertext[4].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("MyActivitiesIB_Amount");
    headertext[5].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("MyActivitiesIB_Repeat");
    headertext[6].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("keyEndOn");
}
/**
 * Loading Page numbers
 */
function loadPagenumbers(Totalpages, currentPage1) {
    frmIBDateSlider.link1.text = "";
    frmIBDateSlider.link2.text = "";
    frmIBDateSlider.link3.text = "";
    frmIBDateSlider.link4.text = "";
    frmIBDateSlider.link5.text = "";
    //var seglength= gblfiterdata.length;
    nopages = Totalpages; //Math.ceil(seglength/recordcount);
    invokeCommonIBLogger("page nos" + nopages);
    if (nopages == 1) {
        frmIBDateSlider.link1.text = "";
        frmIBDateSlider.link2.text = "";
        frmIBDateSlider.link3.text = "";
        frmIBDateSlider.link4.text = "";
        frmIBDateSlider.link5.text = "";
        frmIBDateSlider.linknext.setVisibility(false);
        frmIBDateSlider.linkprev.setVisibility(false);
    } else {
        var pageshown = currentPage1;
        if (currentpage <= nopages) frmIBDateSlider.link1.text = currentpage;
        currentpage = currentpage + 1;
        if (currentpage <= nopages) frmIBDateSlider.link2.text = currentpage;
        currentpage = currentpage + 1;
        if (currentpage <= nopages) frmIBDateSlider.link3.text = currentpage;
        currentpage = currentpage + 1;
        if (currentpage <= nopages) frmIBDateSlider.link4.text = currentpage;
        currentpage = currentpage + 1;
        if (currentpage <= nopages) frmIBDateSlider.link5.text = currentpage;
        loadsegdata(pageshown);
    }
}
/**
 * Loading data to datagrid based on the pagination
 */
function loadsegdata(page) {
    invokeCommonIBLogger("page " + page);
    if (parseInt(frmIBDateSlider.link5.text) < nopages) {
        frmIBDateSlider.linknext.setVisibility(true);
    } else {
        frmIBDateSlider.linknext.setVisibility(false);
    }
    if (parseInt(frmIBDateSlider.link1.text) > 1) {
        frmIBDateSlider.linkprev.setVisibility(true);
    } else {
        frmIBDateSlider.linkprev.setVisibility(false);
    }
}

function dateFormatChangeTxn(dateIB) {
    var d = new Date(dateIB);
    var curr_date = d.getDate();
    var curr_month = d.getMonth() + 1; //Months are zero based
    var curr_year = d.getFullYear();
    if (curr_month < 10) curr_month = "0" + curr_month;
    if (curr_date < 10) curr_date = "0" + curr_date;
    var finalDate = curr_date + "/" + curr_month + "/" + curr_year;
    return finalDate;
}

function showImage(reason) {
    var image;
    if (reason == 'Success') {
        image = "icon_dot_green.png";
    } else if (reason == 'Fail') {
        image = "icon_dot_red.png";
    } else {
        image = "icon_dot_orange.png";
    }
    return image;
}

function clearFilters() {
    frmIBMyActivities.combotrans.selectedKey = "";
    frmIBMyActivities.combosub.selectedKey = "";
    frmIBMyActivities.combobank.selectedKey = "";
    frmIBMyActivities.vbxcombo2.setVisibility(false);
    frmIBMyActivities.vbxcombo3.setVisibility(false);
    gblSortBy = "txnDate";
    gblSortOrder = "DESC";
}

function getTransactionHistory(functn, filetype, templatename) {
    if (gblPreshow == "1") {
        gblPreshow = "0"; //set Dates
    } else if (gblPreshow == "0") {
        var billerCommCodeValue = "";
        var billerRefValue = "";
        var frmDt = dateFormatChangeTxn(gblTxnHistDate1);
        toDt = dateFormatChangeTxn(gblTxnHistDate2);
        if (frmIBDateSlider.combosub.selectedKey != null && frmIBDateSlider.combosub.selectedKey != "") {
            if (frmIBDateSlider.combotrans.selectedKey.split("~")[0] == "001") {
                billerCommCodeValue = "";
                billerRefValue = "";
            } else if (frmIBDateSlider.combotrans.selectedKey.split("~")[0] == "007" || frmIBDateSlider.combotrans.selectedKey.split("~")[0] == "002") {
                billerCommCodeValue = frmIBDateSlider.combosub.selectedKey.split("~")[0];
                billerRefValue = frmIBDateSlider.combosub.selectedKey.split("~")[1];
            } else if (frmIBDateSlider.combotrans.selectedKey.split("~")[0] == "005") {
                billerCommCodeValue = frmIBDateSlider.combosub.selectedKey.split("~")[1];
                billerRefValue = frmIBDateSlider.combosub.selectedKey.split("~")[2];
            } else {
                billerCommCodeValue = "";
                billerRefValue = "";
            }
        }
        showLoadingScreenPopup();
        var CRMFinActivityInquiry_inputparams = {};
        CRMFinActivityInquiry_inputparams["transactionType"] = (frmIBDateSlider.combotrans.selectedKey != null && frmIBDateSlider.combotrans.selectedKey != "") ? frmIBDateSlider.combotrans.selectedKey.split("~")[0] : "";
        CRMFinActivityInquiry_inputparams["activityTypeIds"] = (frmIBDateSlider.combotrans.selectedKey != null && frmIBDateSlider.combotrans.selectedKey != "") ? frmIBDateSlider.combotrans.selectedKey.split("~")[1] : "";
        CRMFinActivityInquiry_inputparams["transactionSubType"] = (frmIBDateSlider.combosub.selectedKey != null && frmIBDateSlider.combosub.selectedKey != "") ? true : false; //check on this
        if (LinkMyActivities == "1") CRMFinActivityInquiry_inputparams["accountNumber"] = gblAccountTable["custAcctRec"][gblIndex]["accId"];
        else if (LinkMyActivities == "0") CRMFinActivityInquiry_inputparams["accountNumber"] = ''; //accnt det screen
        CRMFinActivityInquiry_inputparams["toAccountId"] = (frmIBDateSlider.combosub.selectedKey != "" && frmIBDateSlider.combosub.selectedKey != null && frmIBDateSlider.combotrans.selectedKey.split("~")[0] == "001") ? frmIBDateSlider.combosub.selectedKey : "";
        CRMFinActivityInquiry_inputparams["billerCommCode"] = billerCommCodeValue;
        CRMFinActivityInquiry_inputparams["billerRef"] = billerRefValue;
        CRMFinActivityInquiry_inputparams["bankCode"] = (frmIBDateSlider.combobank.selectedKey != null && frmIBDateSlider.combobank.selectedKey != "") ? frmIBDateSlider.combobank.selectedKey : "";
        CRMFinActivityInquiry_inputparams["pageNo"] = gblCurrentPage;
        CRMFinActivityInquiry_inputparams["isPrevLink"] = gblIsPrevLink;
        CRMFinActivityInquiry_inputparams["isNextLink"] = gblIsNextLink;
        CRMFinActivityInquiry_inputparams["fromDate"] = (gblTxnHistDate1 != "" && gblTxnHistDate1 != undefined) ? frmDt : dateFormatChangeTxn(new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() - 30));
        CRMFinActivityInquiry_inputparams["sortBy"] = gblSortBy;
        CRMFinActivityInquiry_inputparams["sortOrder"] = gblSortOrder;
        CRMFinActivityInquiry_inputparams["toDate"] = (gblTxnHistDate2 != "" && gblTxnHistDate2 != undefined) ? toDt : dateFormatChangeTxn(new Date());
        //for pdf and print
        //functn, filetype,templatename
        if (functn != undefined && functn == 'forsave') {
            CRMFinActivityInquiry_inputparams["savefunctn"] = "forsave"; //hardcoded value
            if (filetype != undefined) {
                CRMFinActivityInquiry_inputparams["filetype"] = filetype; //pdf,png,csv
            }
            if (templatename != undefined) {
                CRMFinActivityInquiry_inputparams["templatename"] = templatename; //pdf xsl or csv xsl
            }
            var outputtemplatename = "Transaction History_" + new Date().getDate() + "" + ("0" + (new Date().getMonth() + 1)).slice(-2) + "" + new Date().getFullYear();
            CRMFinActivityInquiry_inputparams["outputtemplatename"] = outputtemplatename;
            CRMFinActivityInquiry_inputparams["localeId"] = kony.i18n.getCurrentLocale();
            invokeServiceSecureAsync("CRMFinActivityInquiry", CRMFinActivityInquiry_inputparams, ibRenderFileCallbackfunction);
        } else {
            invokeServiceSecureAsync("CRMFinActivityInquiry", CRMFinActivityInquiry_inputparams, getTransactionHistoryCallBack);
        }
    }
}

function getTransactionHistoryCallBack(status, resulttable) {
    //pdfTxnHist=resulttable;
    frmIBMyActivities_History_temp = [];
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (resulttable["SearchResults"].length > 0) {
                for (var i = 0; i < resulttable["SearchResults"].length; i++) {
                    var date = resulttable["SearchResults"][i]["dateTime"].split(" ")[0].substring(0, 6) + resulttable["SearchResults"]
                        [i]["dateTime"].split(" ")[0].substring(8, 10);
                    frmIBMyActivities_History_temp.push({
                        "column6": {
                            "lbltransid": {
                                "text": resulttable["SearchResults"][i]["channel"] != undefined ? resulttable["SearchResults"][i]["channel"] : " ",
                                "skin": "lblIB20pxOzoneXBlack"
                            }
                            //begin MIB-2793
                            ,
                            "lblAppId": {
                                "text": resulttable["SearchResults"][i]["appId"] != undefined ? resulttable["SearchResults"][i]["appId"] : " ",
                                "skin": "lblIB20pxOzoneXBlack",
                                "isVisible": false
                            }
                            //end MIB-2793
                        },
                        "column7": {
                            "lbltransid": {
                                "text": resulttable["SearchResults"][i]["updatedReason"] != undefined ? resulttable["SearchResults"][i]["updatedReason"] : " ",
                                "skin": "lblIB20pxOzoneXBlack"
                            }
                        },
                        "column1": {
                            "lbltime": {
                                "text": resulttable["SearchResults"][i]["dateTime"] != undefined ? resulttable["SearchResults"][i]["dateTime"].split(" ")[1] : " ",
                                "skin": "lblIB20pxOzoneXBlack"
                            },
                            "lbldate": {
                                "text": resulttable["SearchResults"][i]["dateTime"] != undefined ? date : " ",
                                "skin": "lblIB20pxOzoneXBlack"
                            },
                            "image2589548405258382": {
                                "src": showImage(resulttable["SearchResults"][i]["updatedReason"])
                            }
                        },
                        "column2": {
                            "lbltransid": {
                                "text": resulttable["SearchResults"][i]["transType"] != undefined ? resulttable["SearchResults"][i]["transType"] : " ",
                                "skin": "lblIB20pxOzoneXBlack"
                            }
                        },
                        "column3": {
                            "lbltransid": {
                                "text": resulttable["SearchResults"][i]["from"] != undefined ? resulttable["SearchResults"][i]["from"] : " ",
                                "skin": "lblIB20pxOzoneXBlack"
                            }
                        },
                        "column4": {
                            "lblno": {
                                "text": (resulttable["SearchResults"][i]["txnRefId"] != undefined && resulttable["SearchResults"][i]["activityId"] != undefined) ? resulttable["SearchResults"][i]["txnRefId"] + '`' + resulttable["SearchResults"]
                                    [i]["activityId"] + '`' + resulttable["SearchResults"][i]["openAcctType"] : " " + '`' + " " + '`' + " ",
                                "skin": "lblIB20pxOzoneXBlack",
                                "isVisible": false
                            },
                            "lblto": {
                                "text": resulttable["SearchResults"][i]["to"] != undefined ? resulttable["SearchResults"][i]["to"] : " ",
                                "skin": "lblIB20pxOzoneXBlack"
                            }
                        },
                        "column5": {
                            "lbltransid": {
                                "text": resulttable["SearchResults"][i]["amount"] != undefined ? resulttable["SearchResults"][i]["amount"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht") : " ",
                                "skin": "lblIB20pxOzoneXBlack"
                            }
                        }
                    })
                }
                gblCurrentPage = resulttable["pageNo"];
                Totalpages = resulttable["totalNoOfPages"];
                frmIBDateSlider.datagridhistory.data = frmIBMyActivities_History_temp;
                dismissLoadingScreenPopup();
                loadPagenumbers(Totalpages, gblCurrentPage);
            } else {
                dismissLoadingScreenPopup();
                alert(kony.i18n.getLocalizedString("NoRecsFound"));
                frmIBMyActivities.datagridhistory.removeAll();
                gblCurrentPage = 1;
                currentpage = kony.os.toNumber(gblCurrentPage);
                loadPagenumbers(1, 1);
            }
            //activityLogServiceCall('065', '', '01', '', '', '', '', '', '', '');
        } else {
            dismissLoadingScreenPopup();
        }
    }
    dismissLoadingScreenPopup();
    //activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1, activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId)
}

function getTransactionType() {
    if (gblPreshow == "0") {
        if (localeChange == "1") {
            var getTransactionTypes_inputparams = {};
            if (LinkMyActivities == "1") getTransactionTypes_inputparams["accountNumber"] = gblAccountTable["custAcctRec"][
                gblIndex
            ]["accId"];
            else if (LinkMyActivities == "0") getTransactionTypes_inputparams["accountNumber"] = ''; //accnt det screen
            getTransactionTypes_inputparams["transactionType"] = (storedTxnKey != null && storedTxnKey != "") ? storedTxnKey.split("~")[0] : "";
            getTransactionTypes_inputparams["activityTypeIds"] = (storedTxnKey != null && storedTxnKey != "") ? storedTxnKey.split("~")[1] : "";
            invokeServiceSecureAsync("getTransactionTypes", getTransactionTypes_inputparams, getTransactionTypesCallBack);
        } else {
            showLoadingScreenPopup();
            var getTransactionTypes_inputparams = {};
            if (LinkMyActivities == "1") getTransactionTypes_inputparams["accountNumber"] = gblAccountTable["custAcctRec"][
                gblIndex
            ]["accId"];
            else if (LinkMyActivities == "0") getTransactionTypes_inputparams["accountNumber"] = '';
            getTransactionTypes_inputparams["transactionType"] = (frmIBDateSlider.combotrans.selectedKey != null && frmIBDateSlider.combotrans.selectedKey != "") ? frmIBDateSlider.combotrans.selectedKey.split("~")[0] : "";
            getTransactionTypes_inputparams["activityTypeIds"] = (frmIBDateSlider.combotrans.selectedKey != null && frmIBDateSlider.combotrans.selectedKey != "") ? frmIBDateSlider.combotrans.selectedKey.split("~")[1] : "";
            invokeServiceSecureAsync("getTransactionTypes", getTransactionTypes_inputparams, getTransactionTypesCallBack);
        }
    } else if (gblPreshow == "1") {
        //set Dates
    }
}

function getTransactionTypesCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            var textretTxn;
            var textret;
            var frmIBMyActivities_Trans_temp = [];
            var frmIBMyActivities_SubTrans_temp = [];
            var frmIBMyActivities_BankList_temp = [];
            var Trans_temp = ["", kony.i18n.getLocalizedString("keyCalendarTxnType")];
            var SubTrans_temp = ["", kony.i18n.getLocalizedString("keyPleaseSelectToAccountBiller")];
            var BankList_temp = ["", kony.i18n.getLocalizedString("keylblBankName")];
            //if (frmIBDateSlider.combotrans.masterData == null) {
            if (resulttable["TransactionTypes"].length > 0) {
                frmIBMyActivities_Trans_temp.push(Trans_temp);
                for (var i = 0; i < resulttable["TransactionTypes"].length; i++) {
                    var locText = resulttable["TransactionTypes"][i]["transactionType"].split("#");
                    if (kony.i18n.getCurrentLocale() == "en_US") textretTxn = locText[0];
                    else textretTxn = locText[1];
                    //		gblTxnEN = locText[0];
                    //		gblTxnTH = locText[1];
                    frmIBMyActivities_Trans_temp.push([resulttable["TransactionTypes"][i]["transactionTypeCode"] + "~" + resulttable["TransactionTypes"][i]["activityTypeId"], textretTxn]);
                }
                var selKey = frmIBDateSlider.combotrans.selectedKey;
                frmIBDateSlider.combotrans.masterData = frmIBMyActivities_Trans_temp;
                frmIBDateSlider.combotrans.selectedKey = selKey;
                dismissLoadingScreenPopup();
                if (kony.string.equalsIgnoreCase(localeChange, "1")) {
                    frmIBDateSlider.combotrans.selectedKey = storedTxnKey;
                    if (storedSubTxnKey == "" && storedBankKey == "") {
                        localeChange = "0";
                    }
                } else {}
            }
            //}
            if (resulttable["SubTransactionTypes"].length > 0) {
                frmIBMyActivities_SubTrans_temp.push(SubTrans_temp);
                for (var i = 0; i < resulttable["SubTransactionTypes"].length; i++) {
                    var subtransactionType = resulttable["SubTransactionTypes"][i]["subTransactionData"].split("~");
                    var displayText = subtransactionType[0];
                    subtransactionType.reverse();
                    subtransactionType.pop();
                    subtransactionType.reverse();
                    var keyText = subtransactionType.join("~");
                    frmIBMyActivities_SubTrans_temp.push([keyText, displayText]);
                }
                if (frmIBDateSlider.vbxcombo2.isVisible) {} else {
                    frmIBDateSlider.vbxcombo2.setVisibility(true);
                }
                frmIBDateSlider.combosub.masterData = frmIBMyActivities_SubTrans_temp;
                dismissLoadingScreenPopup();
                if (kony.string.equalsIgnoreCase(localeChange, "1")) {
                    frmIBDateSlider.combosub.selectedKey = storedSubTxnKey;
                    if (storedBankKey == "") {
                        localeChange = "0";
                    }
                } else {}
            } else {
                if (frmIBDateSlider.vbxcombo2.isVisible) {
                    frmIBDateSlider.vbxcombo2.setVisibility(false);
                } else {}
                frmIBDateSlider.combosub.masterData = "";
                dismissLoadingScreenPopup();
            }
            if (resulttable["BankList"].length > 0) {
                frmIBMyActivities_BankList_temp.push(BankList_temp);
                for (var i = 0; i < resulttable["BankList"].length > 0; i++) {
                    if (kony.i18n.getCurrentLocale() == "en_US") textret = resulttable["BankList"][i]["bankNameEN"];
                    else textret = resulttable["BankList"][i]["bankNameTH"];
                    frmIBMyActivities_BankList_temp.push([resulttable["BankList"][i]["bankCode"], textret]);
                }
                if (frmIBMyActivities.vbxcombo3.isVisible) {} else {
                    frmIBDateSlider.vbxcombo3.setVisibility(true);
                }
                frmIBDateSlider.combobank.masterData = frmIBMyActivities_BankList_temp;
                dismissLoadingScreenPopup();
                if (kony.string.equalsIgnoreCase(localeChange, "1")) {
                    frmIBDateSlider.combobank.selectedKey = storedBankKey;
                    localeChange = "0";
                } else {}
            } else {
                if (frmIBDateSlider.vbxcombo3.isVisible) {
                    frmIBDateSlider.vbxcombo3.setVisibility(false);
                } else {}
                frmIBDateSlider.combobank.masterData = "";
                dismissLoadingScreenPopup();
            }
        } else {
            dismissLoadingScreenPopup();
        }
    }
    dismissLoadingScreenPopup();
}
/**
	function onSubmitbtnClick() {
	 
	 var startdate = frmIBAccntFullStatement.dateslider.selectedDate1;
	 
	 startDateIB = dateFormatChange(startdate);
	 
	 endDateIB = dateFormatChange(frmIBAccntFullStatement.dateslider.selectedDate2);
	 
	 //totalGridData = [];
	 //currentpageStmt = 1;
	}*/
function onNextPrevLinkClick(obj) {
    if (obj.id == "linkprev") {
        gblCurrentPage = parseInt(frmIBDateSlider.link1.text);
        currentpage = gblCurrentPage - 1;
        gblIsPrevLink = true;
        gblIsNextLink = false;
        frmIBDateSlider.link1.skin = lnkNormal;
        frmIBDateSlider.link2.skin = lnkIB18pxBlueUnderline;
        frmIBDateSlider.link3.skin = lnkIB18pxBlueUnderline;
        frmIBDateSlider.link4.skin = lnkIB18pxBlueUnderline;
        frmIBDateSlider.link5.skin = lnkIB18pxBlueUnderline;
    } else if (obj.id == "linknext") {
        gblCurrentPage = parseInt(frmIBDateSlider.link5.text);
        currentpage = gblCurrentPage - 3;
        gblIsPrevLink = false;
        gblIsNextLink = true;
        frmIBDateSlider.link1.skin = lnkIB18pxBlueUnderline;
        frmIBDateSlider.link2.skin = lnkIB18pxBlueUnderline;
        frmIBDateSlider.link3.skin = lnkIB18pxBlueUnderline;
        frmIBDateSlider.link4.skin = lnkIB18pxBlueUnderline;
        frmIBDateSlider.link5.skin = lnkNormal;
    } else if (obj.id == "link1" || obj.id == "link2" || obj.id == "link3" || obj.id == "link4" || obj.id == "link5") {
        gblIsPrevLink = false;
        gblIsNextLink = false;
        gblCurrentPage = obj.text;
        frmIBDateSlider.link1.skin = lnkIB18pxBlueUnderline;
        frmIBDateSlider.link2.skin = lnkIB18pxBlueUnderline;
        frmIBDateSlider.link3.skin = lnkIB18pxBlueUnderline;
        frmIBDateSlider.link4.skin = lnkIB18pxBlueUnderline;
        frmIBDateSlider.link5.skin = lnkIB18pxBlueUnderline;
        obj.skin = lnkNormal;
        currentpage = frmIBDateSlider.link1.text;
    }
    getTransactionHistory();
}

function clearData() {
    currentpage = 1;
    gblCurrentPage = 1;
    gblIsPrevLink = false;
    gblIsNextLink = false;
    frmIBDateSlider.link1.skin = lnkNormal;
    frmIBDateSlider.link2.skin = lnkIB18pxBlueUnderline;
    frmIBDateSlider.link3.skin = lnkIB18pxBlueUnderline;
    frmIBDateSlider.link4.skin = lnkIB18pxBlueUnderline;
    frmIBDateSlider.link5.skin = lnkIB18pxBlueUnderline;
}

function savePDFTransferIB(filetype) {
    var inputParam = {}
    inputParam["filename"] = "transfersTemplate"
    inputParam["filetype"] = filetype;
    inputParam["fromAcctNo"] = frmIBTransferNowCompletion.lblAccountNo.text
    inputParam["fromAcctName"] = frmIBTransferNowCompletion.lblName.text
    inputParam["toAcctNo"] = frmIBTransferNowCompletion.lblXferAccNo.text
    inputParam["toAcctName"] = frmIBTransferNowCompletion.lblXferToName.text
    inputParam["transferAmount"] = frmIBTransferNowCompletion.lblAmtVal.text
    inputParam["transferFee"] = frmIBTransferNowCompletion.lblFeeVal.text
    inputParam["transferDate"] = frmIBTransferNowCompletion.lblTransferVal.text
    inputParam["myNote"] = frmIBTransferNowCompletion.lblMNVal.text
    inputParam["noteToRecipient"] = frmIBTransferNowCompletion.lblNTRVal.text
    var notifyVia = "";
    if (gblTransEmail == 1) notifyVia = "Email";
    else if (gblTrasSMS == 1) notifyVia = "SMS";
    inputParam["notifyVia"] = notifyVia
    var txnRefNo = "";
    if (gblSplitStatusInfo.length > 0) {
        var j = 0;
        for (var i = 0; i < gblSplitStatusInfo.length; i++) {
            if (gblSplitStatusInfo[i] == 0) txnRefNo = txnRefNo + j + (i + 1) + gblTransferRefNo + ",";
            if ((i % 10) == 0) j++;
        }
    } else txnRefNo = "00" + gblTransferRefNo;
    inputParam["transactionRefNo"] = txnRefNo
    var url = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/TransfersPdfImage?filename=" + inputParam["filename"] + "&filetype=" + inputParam["filetype"] + "&fromAccountName=" + inputParam["fromAcctName"] + "&fromAccountNo=" + inputParam["fromAcctNo"] + "&myNote=" + inputParam["myNote"] + "&noteToRecipient=" + inputParam["noteToRecipient"] + "&notifyVia=" + inputParam["notifyVia"] + "&toAccountName=" + inputParam["toAcctName"] + "&toAccountNo=" + inputParam["toAcctNo"] + "&transactionRefNo=" + inputParam["transactionRefNo"] + "&amount=" + inputParam["transferAmount"] + "&transferDate=" + inputParam["transferDate"] + "&transferFee=" + inputParam["transferFee"];
    frmIBTransferGeneratePDF.browPdfTransfer.requestURLConfig = {
        URL: url,
        requestMethod: "constants.BROWSER_REQUEST_METHOD_GET"
    }
    frmIBTransferGeneratePDF.show();
    //invokeServiceSecureAsync("generateTransferPDF", inputParam, callbackGeneratePDFIB)
}

function onRowClick() {
    frmIBExecutedTransaction.show();
}

function btnhistoryOnClick() {
    gsFormId = frmIBDateSlider;
    clearDataOnLaunch();
    frmIBDateSlider.show();
    getTransactionType();
    getTransactionHistory();
    frmIBDateSlider.link1.skin = lnkNormal;
}

function btncalenderOnClick() {
    frmIBMyActivities.lblMyActivities.setVisibility(true);
    frmIBMyActivities.hbxCal.setVisibility(true);
    frmIBMyActivities.hbxhistory.setVisibility(false);
    frmIBMyActivities.hbxbottom.setVisibility(false);
    frmIBMyActivities.hbxfuture.setVisibility(false);
    frmIBMyActivities.hbox589488823281264.setVisibility(false);
    frmIBMyActivities.hbxbottomfuture.setVisibility(false);
    frmIBMyActivities.hbxCalendarContainer.setVisibility(true);
    frmIBMyActivities.vboxCalDayView.setVisibility(true);
    frmIBMyActivities.vbxArrow.setVisibility(false);
    frmIBMyActivities.lblactivity.setVisibility(false);
    frmIBMyActivities.hbxBtns.setVisibility(false);
    //setting the right margin so that the hboxStatusDesc takes less width
    //and this will be similar when vboxCalDayView is visible 
    frmIBMyActivities.hboxStatusDesc.margin = [0, 0, 0, 0];
    //frmIBMyActivities.vboxCalDayView.containerWeight = 0;
    frmIBMyActivities.vbxArrow.containerWeight = 2;
    frmIBMyActivities.btnfuture.skin = btnIBrndrightgrey;
    frmIBMyActivities.btnhistory.skin = btnIBrndleftgrey;
    frmIBMyActivities.btncalender.skin = btnIBTabBlue;
    frmIBMyActivities.btnfuture.focusSkin = btnIBrndrightgrey;
    frmIBMyActivities.btnhistory.focusSkin = btnIBrndleftgrey;
    frmIBMyActivities.btncalender.focusSkin = btnIBTabBlue;
    //gblPreshow = "1"; //to prevent calling Transaction History services
    frmIBMyActivities.show();
    if (window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8") langspecificmenuIE8();
    else langspecificmenu();
    frmIBMyActivities.segMenuOptions.removeAll();
    if (kony.i18n.getCurrentLocale() == "th_TH") {
        frmIBMyActivities.btnMenuMyActivities.skin = "btnIBMenuMyActivitiesFocusThai";
    } else {
        frmIBMyActivities.btnMenuMyActivities.skin = "btnIBMenuMyActivitiesFocus";
    }
    if (typeof gsSelectedDate != 'undefined') {
        reloadCalendar = 1;
        showCalendar(gsSelectedDate, frmIBMyActivities);
    } else {
        clearCalendarData();
        showCalendar("", frmIBMyActivities);
    }
}
/*
 * Function that makes service call to get future instructions
 */
function btnfutureOnClick() {
    if (gblCalledMasterBillerService == 0) {
        gblCalledFromFuture = 1;
        invokeMasterBillerInqService()
    } else {
        invokeFutureInstructionService();
    }
}
/*
 * Function that invokes masterBillerInquiry service
 * to populate active master billers into app cache
 */
function invokeMasterBillerInqService() {
    //This js variable is for transaction history. Setting it to null.
    frmIBMyActivities_History_temp = [];
    showLoadingScreenPopup();
    var inputParam = {};
    inputParam["BillerGroupType"] = "0";
    invokeServiceSecureAsync("masterBillerInquiryForEditBP", inputParam, callBackMasterBillerInqService)
}
/**
 * call back to handle master biller inq service responce.
 *  no return values
 */
function callBackMasterBillerInqService(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            //dismissLoadingScreenPopup();
            showLoadingScreenPopup();
            var inputParam = {
                IsActive: "1"
            };
            invokeServiceSecureAsync("customerBillInquiry", inputParam, callBackCustomerBillInqBPService)
        } else dismissLoadingScreenPopup();
    }
}

function callBackCustomerBillInqBPService(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            //dismissLoadingScreenPopup();
            invokeMasterBillerInqServiceForTopUp();
        } else dismissLoadingScreenPopup();
    }
}
/*
 * Function that invokes masterBillerInquiry service
 * to populate active top up into app cache
 */
function invokeMasterBillerInqServiceForTopUp() {
    showLoadingScreenPopup();
    var inputParam = {};
    inputParam["BillerGroupType"] = "1";
    invokeServiceSecureAsync("masterBillerInquiryForEditBP", inputParam, callBackMasterBillerInqTopUpService)
}
/**
 * call back to handle master biller inq service responce.
 *  no return values
 */
function callBackMasterBillerInqTopUpService(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            //dismissLoadingScreenPopup();
            invokeCustomerBillInqService();
        } else dismissLoadingScreenPopup();
    }
}
/*
 * Function that invokes customerBillInquiry service to get
 * all the master billers , bills, topups associated with the customer
 */
function invokeCustomerBillInqService() {
    showLoadingScreenPopup();
    var inputParam = {
        IsActive: "1"
    };
    invokeServiceSecureAsync("customerBillInquiry", inputParam, callBackCustomerBillInqService)
}
/**
 * call back to handle master biller inq service responce.
 *  no return values
 */
function callBackCustomerBillInqService(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            gblCalledMasterBillerService = 1;
            if (typeof(gblCalledFromFuture) != 'undefined' && gblCalledFromFuture == 1) {
                //Code when click on future transaction tab
                gblCalledFromFuture = 0;
                invokeFutureInstructionService();
            } else {
                //Code when clicking on any transaction
                IBMyActivitiesSegmentOnClick2();
            }
        } else dismissLoadingScreenPopup();
    }
}
/*
 * Function that invokes service
 */
function invokeFutureInstructionService() {
    showLoadingScreenPopup();
    var inputParam = {};
    //inputParam["fromAcctId"] = "";
    if (LinkMyActivities == "1") {
        var accId = gblAccountTable["custAcctRec"][gblIndex]["accId"];
        if (accId.length >= 10) {
            accId = accId.substring(accId.length - 10);
        }
        inputParam["fromAcctId"] = accId;
    } else if (LinkMyActivities == "0") inputParam["fromAcctId"] = "";
    invokeServiceSecureAsync("fetchTransactionInstructions", inputParam, callBackFutureInstructionService)
}
/**
 * call back to handle future instruction service responce.
 *  no return values
 */
function callBackFutureInstructionService(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            columnHeaderInDGFutureTrans();
            frmIBMyActivities.hbxhistory.setVisibility(false);
            frmIBMyActivities.hbxbottom.setVisibility(false);
            frmIBMyActivities.hbxfuture.setVisibility(true);
            frmIBMyActivities.hbxbottomfuture.setVisibility(true);
            frmIBMyActivities.hbxCalendarContainer.setVisibility(false);
            //frmIBMyActivities.vboxCalDayView.containerWeight = 0;
            frmIBMyActivities.vboxCalDayView.setVisibility(false);
            frmIBMyActivities.btnfuture.skin = btnIBrndrightblue;
            frmIBMyActivities.btnhistory.skin = btnIBrndleftgrey;
            frmIBMyActivities.btncalender.skin = btnIBTabgrey;
            frmIBMyActivities.btnfuture.focusSkin = btnIBrndrightblue;
            frmIBMyActivities.btnhistory.focusSkin = btnIBrndleftgrey;
            frmIBMyActivities.btncalender.focusSkin = btnIBTabgrey;
            frmIBMyActivities.datagridFT.removeAll();
            if (resulttable["StatusDesc"] != "Success") {
                dismissLoadingScreenPopup();
                alert(resulttable["AdditionalStatusDesc"]);
                gblsegfuturedata = [];
                loadPagenumbersInFutureTrans();
                return false;
            }
            recordcount = resulttable["recsPerPage"];
            setSegDataWithFutureIns(resulttable["PmtInqRs"]);
        } else {
            dismissLoadingScreenPopup();
        }
    }
}
/*
 * Function to set the received info to the data grid
 */
function setSegDataWithFutureIns(payInqRes) {
    if (payInqRes.length > 0) {
        var frmIBMyActivities_FutureIns_temp = [];
        for (var i = 0; i < payInqRes.length; i++) {
            if (payInqRes[i]["ChannelID"] == "SMEMB") {
                continue;
            }
            var tempData = {
                "seqid": i,
                "column1": {
                    "imgschedule": {
                        "src": "scheduled_blue.jpg"
                    },
                    "lbldateon": {
                        "text": payInqRes[i]["Duedt"] != undefined ? payInqRes[i]["Duedt"] : " ",
                        "skin": "lblIB20pxOzoneXBlack"
                    }
                },
                "column2": {
                    "lbltransid": {
                        "text": payInqRes[i]["PmtMethod"] != undefined ? payInqRes[i]["PmtMethod"] : " ",
                        "skin": "lblIB20pxOzoneXBlack"
                    }
                },
                "column3": {
                    "lbltransid": {
                        "text": payInqRes[i]["FromAccId"] != undefined ? payInqRes[i]["FromAccId"] : " ",
                        "skin": "lblIB20pxOzoneXBlack"
                    }
                },
                "column4": {
                    "lbltransid": {
                        "text": payInqRes[i]["ToAccId"] != undefined ? payInqRes[i]["ToAccId"] : " ",
                        "skin": "lblIB20pxOzoneXBlack"
                    }
                },
                "column5": {
                    "lbltransid": {
                        "text": payInqRes[i]["Amt"] != undefined ? payInqRes[i]["Amt"] + "" + kony.i18n.getLocalizedString("currencyThaiBaht") : " ",
                        "skin": "lblIB20pxOzoneXBlack"
                    }
                },
                "column6": {
                    "lblto": {
                        "text": payInqRes[i].hasOwnProperty('RepeatAs') ? (payInqRes[i]["RepeatAs"] == "Annually" ? "Yearly" : payInqRes[i]["RepeatAs"]) : "Once",
                        "skin": "lblIB20pxOzoneXBlack"
                    },
                    "lblno": {
                        "text": payInqRes[i].hasOwnProperty('scheRefNo') ? payInqRes[i]["scheRefNo"] : "",
                        "skin": "lblIB20pxOzoneXBlack",
                        "isVisible": false
                    }
                    //begin MIB-2796
                    ,
                    "lblChannelID": {
                        "text": payInqRes[i].hasOwnProperty('ChannelID') ? payInqRes[i]["ChannelID"] : "",
                        "skin": "lblIB20pxOzoneXBlack",
                        "isVisible": false
                    }
                    //end MIB-2796
                },
                "column7": {
                    "button588684466113119": {
                        "skin": "btnCalArrowSmall",
                        "isVisible": (payInqRes[i]["ChannelID"] == "SMEMB" ? false : true)
                    },
                    "lblendon": {
                        "text": payInqRes[i].hasOwnProperty('ExecutionTimes') ? payInqRes[i]["ExecutionTimes"] : (payInqRes[i].hasOwnProperty('EndDate') ? payInqRes[i]["EndDate"] : "-"),
                        "skin": "lblIB20pxOzoneXBlack"
                    }
                }
            }
            frmIBMyActivities_FutureIns_temp.push(tempData);
        }
        gblsegfuturedata = frmIBMyActivities_FutureIns_temp;
        //loadPagenumbersInFutureTrans();
        sortSegDataFT("column1", "lbldateon");
        dismissLoadingScreenPopup();
    } else {
        dismissLoadingScreenPopup();
        frmIBMyActivities.datagridFT.removeAll();
        alert(kony.i18n.getLocalizedString("NoRecsFound"));
        loadPagenumbersInFutureTrans();
    }
    //activityLogServiceCall('066', '', '01', '', '', '', '', '', '', '');
}

function formatAccountNumber(accountNo) {
    if (accountNo == null) return;
    var length = accountNo.length;
    if (length == 14) {
        accountNo = accountNo.substring(4);
    }
    length = accountNo.length;
    var fourthChar = accountNo.charAt(3);
    var middleFive = accountNo.substring(4, 9);
    var lastCharater = accountNo.substring(9, length); //accountNo.charAt(length - 1);
    var formatedValue = accountNo.substring(0, 3) + "-" + fourthChar + "-" + middleFive + "-" + lastCharater;
    return formatedValue;
}
/*
 * Function that flips  the sort order on click of sort button of future instructions page
 * for the date column
 */
function flipSortOrderForFT() {
    if (sortingOrderFI == "descending") {
        sortingOrderFI = "acsending";
    } else {
        sortingOrderFI = "descending";
    }
}

function sortSegDataFT(columnid, columnsortby) {
    if (sortingOrderFI == "acsending") {
        var sorteddata = sortDateDescend(gblsegfuturedata, columnid, columnsortby, "seqid");
        frmIBMyActivities.datagridFT.columnHeadersConfig[0].columnheadertemplate.data.btnsort.skin = "btnSortUpIB";
    } else if (sortingOrderFI == "descending") {
        var sorteddata = sortDateAscend(gblsegfuturedata, columnid, columnsortby, "seqid");
        frmIBMyActivities.datagridFT.columnHeadersConfig[0].columnheadertemplate.data.btnsort.skin = "btnsortby";
    }
    currentpagefuture = 1;
    loadPagenumbersInFutureTrans();
    //loadsegdataInFutureTrans(currentpagefuture);
}
gblNSTxnDetCallBackResult = "";

function getTransactionDetails(inputParamsFromCalnedar) {
    //begin MIB-2793
    /*appId = 02 is SMEMB or SMEIB will exit from function*/
    gblNSTxnDetCallBackResult = "";
    var selectedIndex = frmIBDateSlider.datagridhistory.selectedIndex;
    if (selectedIndex != null && selectedIndex >= 0) {
        var appId = frmIBDateSlider.datagridhistory.data[frmIBDateSlider.datagridhistory.selectedIndex]["column6"]["lblAppId"].text;
        if (appId != null && appId == '02') {
            return;
        }
    }
    //end MIB-2793
    showLoadingScreenPopup();
    var getTransactionDetails_inputparams = {};
    if (inputParamsFromCalnedar) //if any executed txn is clicked from Calendar
    {
        getTransactionDetails_inputparams["txnRefId"] = inputParamsFromCalnedar["txnRefId"];
        getTransactionDetails_inputparams["activityTypeIds"] = inputParamsFromCalnedar["activityTypeIds"];
        getTransactionDetails_inputparams["openAcctType"] = inputParamsFromCalnedar["openAcctType"];
    } else {
        getTransactionDetails_inputparams["txnRefId"] = frmIBDateSlider.datagridhistory.data[frmIBDateSlider.datagridhistory.selectedIndex]["column4"]["lblno"].text.split("`")[0];
        getTransactionDetails_inputparams["activityTypeIds"] = frmIBDateSlider.datagridhistory.data[frmIBDateSlider.datagridhistory.selectedIndex]["column4"]["lblno"].text.split("`")[1];
        getTransactionDetails_inputparams["openAcctType"] = frmIBDateSlider.datagridhistory.data[frmIBDateSlider.datagridhistory.selectedIndex]["column4"]["lblno"].text.split("`")[2] != undefined ? frmIBDateSlider.datagridhistory.data[frmIBDateSlider.datagridhistory.selectedIndex]["column4"]["lblno"].text.split("`")[2] : "";
    }
    gblActivityIds = getTransactionDetails_inputparams["activityTypeIds"];
    if (getTransactionDetails_inputparams["activityTypeIds"] == "023" || getTransactionDetails_inputparams["activityTypeIds"] == "024" || getTransactionDetails_inputparams["activityTypeIds"] == "025" || getTransactionDetails_inputparams["activityTypeIds"] == "026" || getTransactionDetails_inputparams["activityTypeIds"] == "604" || getTransactionDetails_inputparams["activityTypeIds"] == "605" || getTransactionDetails_inputparams["activityTypeIds"] == "606" || getTransactionDetails_inputparams["activityTypeIds"] == "607") {
        invokeServiceSecureAsync("getTransactionDetails", getTransactionDetails_inputparams, getTransferTxnDetCallBack);
    } else if (getTransactionDetails_inputparams["activityTypeIds"] == "027" || getTransactionDetails_inputparams["activityTypeIds"] == "028") {
        invokeServiceSecureAsync("getTransactionDetails", getTransactionDetails_inputparams, getBillPaymentTxnDetCallBack);
    } else if (getTransactionDetails_inputparams["activityTypeIds"] == "030" || getTransactionDetails_inputparams["activityTypeIds"] == "031") {
        invokeServiceSecureAsync("getTransactionDetails", getTransactionDetails_inputparams, getTopUpTxnDetCallBack);
    } else if (getTransactionDetails_inputparams["activityTypeIds"] == "034") {
        invokeServiceSecureAsync("getTransactionDetails", getTransactionDetails_inputparams, getBeepBillTxnDetCallBack);
    } else if (getTransactionDetails_inputparams["activityTypeIds"] == "029") {
        invokeServiceSecureAsync("getTransactionDetails", getTransactionDetails_inputparams, getePmntTxnDetCallBack);
    } else if (getTransactionDetails_inputparams["activityTypeIds"] == "063" && getTransactionDetails_inputparams["openAcctType"] == "NS") {
        invokeServiceSecureAsync("getTransactionDetails", getTransactionDetails_inputparams, getNSTxnDetCallBack);
    } else if (getTransactionDetails_inputparams["activityTypeIds"] == "063" && getTransactionDetails_inputparams["openAcctType"] == "SC") {
        invokeServiceSecureAsync("getTransactionDetails", getTransactionDetails_inputparams, getSCTxnDetCallBack);
    } else if (getTransactionDetails_inputparams["activityTypeIds"] == "063" && getTransactionDetails_inputparams["openAcctType"] == "TD") {
        invokeServiceSecureAsync("getTransactionDetails", getTransactionDetails_inputparams, getTDTxnDetCallBack);
    } else if (getTransactionDetails_inputparams["activityTypeIds"] == "063" && getTransactionDetails_inputparams["openAcctType"] == "DS") {
        invokeServiceSecureAsync("getTransactionDetails", getTransactionDetails_inputparams, getDSTxnDetCallBack);
    } else if (getTransactionDetails_inputparams["activityTypeIds"] == "038") {
        invokeServiceSecureAsync("getTransactionDetails", getTransactionDetails_inputparams, getS2STxnDetCallBack);
    } else {
        dismissLoadingScreenPopup();
        alert("Not Implemented");
    }
}

function getS2STxnDetCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (resulttable["ExecTransactionDetails"].length > 0) {
                frmIBSTSExecutedTxn.lblAmntLmtMax.text = commaFormatted(resulttable["fromMaxAmount"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                frmIBSTSExecutedTxn.lblAmntLmtMin.text = commaFormatted(resulttable["toMaxAmount"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                frmIBSTSExecutedTxn.lblAccType2.text = resulttable["ExecTransactionDetails"][0]["toAccName"] != undefined ? resulttable["ExecTransactionDetails"][0]["toAccName"] : " ";
                frmIBSTSExecutedTxn.lblAcctType.text = resulttable["ExecTransactionDetails"][0]["fromAcctName"] != undefined ? resulttable["ExecTransactionDetails"][0]["fromAcctName"] : " ";
                frmIBSTSExecutedTxn.lblBal2.text = resulttable["ExecTransactionDetails"][0]["toAcctBalance"] != undefined ? resulttable["ExecTransactionDetails"][0]["toAcctBalance"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht") : " ";
                frmIBSTSExecutedTxn.lblCustName.text = resulttable["ExecTransactionDetails"][0]["fromAcctNickName"] != undefined ? resulttable["ExecTransactionDetails"][0]["fromAcctNickName"] : " ";
                frmIBSTSExecutedTxn.lblAccNo.text = resulttable["ExecTransactionDetails"][0]["fromAcctId"] != undefined ? formatAccountNumber(resulttable["ExecTransactionDetails"][0]["fromAcctId"]) : " ";
                frmIBSTSExecutedTxn.lbLAccName2.text = resulttable["ExecTransactionDetails"][0]["toAccNickName"] != undefined ? resulttable["ExecTransactionDetails"][0]["toAccNickName"] : " ";
                frmIBSTSExecutedTxn.lblAfterBal.text = resulttable["ExecTransactionDetails"][0]["balAfterPymnt"] != undefined ? resulttable["ExecTransactionDetails"][0]["balAfterPymnt"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht") : " ";
                frmIBSTSExecutedTxn.lblAccNo2.text = resulttable["ExecTransactionDetails"][0]["toAccId"] != undefined ? formatAccountNumber(resulttable["ExecTransactionDetails"][0]["toAccId"]) : " ";
                frmIBSTSExecutedTxn.lblTransDate.text = resulttable["ExecTransactionDetails"][0]["transferDate"] != undefined ? resulttable["ExecTransactionDetails"][0]["transferDate"] : " ";
                if (resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"].length > 0) {
                    frmIBSTSExecutedTxn.lblTrnsRefNo.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["transRefId"] != undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["transRefId"] : " ";
                    frmIBSTSExecutedTxn.lblTrnsAmnt.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["amount"] != undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["amount"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht") : " ";
                    resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["txnStatus"] == "01" ? frmIBSTSExecutedTxn.imgComplete.src = "iconcomplete.png" : frmIBSTSExecutedTxn.imgComplete.src = "icon_notcomplete.png";
                } else {
                    frmIBSTSExecutedTxn.lblTrnsRefNo.text = "";
                }
                frmIBSTSExecutedTxn.imgFrom.src = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + resulttable["ICON_ID"] + "&modIdentifier=PRODICON";
                var personalizedId = resulttable["ExecTransactionDetails"][0]["PersonlizedID"] != undefined ? resulttable["ExecTransactionDetails"][0]["PersonlizedID"] : "";
                var toAccPic = resulttable["ExecTransactionDetails"][0]["toAccPic"] != undefined ? resulttable["ExecTransactionDetails"][0]["toAccPic"] : "";
                var ownflag = resulttable["ExecTransactionDetails"][0]["ownFlag"] != undefined ? resulttable["ExecTransactionDetails"][0]["ownFlag"] : "";
                frmIBSTSExecutedTxn.imgTo.src = getRecipientTxnCompletePic(ownflag, toAccPic, personalizedId);
                frmIBSTSExecutedTxn.show();
            }
        }
    }
    dismissLoadingScreenPopup();
}

function getXferImage(status) {
    var image;
    if (status == "01" || status == "03") {
        image = "completeico_sm.png";
    } else if (status == "02") {
        image = "failico_sm.png";
    }
    return image;
}

function getTransferTxnDetCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (resulttable["ExecTransactionDetails"].length > 0) {
                frmIBExecutedTransaction.lblAcctName.text = resulttable["ExecTransactionDetails"][0]["fromAcctName"] != undefined ? resulttable["ExecTransactionDetails"][0]["fromAcctName"] : " ";
                frmIBExecutedTransaction.lblAcctNickName.text = resulttable["ExecTransactionDetails"][0]["fromAcctNickName"] != undefined ? resulttable["ExecTransactionDetails"][0]["fromAcctNickName"] : " ";
                //frmIBExecutedTransaction.lblAccountNo.text = resulttable["ExecTransactionDetails"][0]["fromAcctId"] != undefined ?
                //formatAccountNumber(resulttable["ExecTransactionDetails"][0]["fromAcctId"]) : " ";
                //masking CR code
                frmIBExecutedTransaction.lblAccountNo.text = resulttable["ExecTransactionDetails"][0]["maskedFromAcctId"] != undefined ? PreencodeAccntNumbers(resulttable["ExecTransactionDetails"][0]["maskedFromAcctId"]) : " ";
                frmIBExecutedTransaction.lblBBPaymentValue.text = resulttable["ExecTransactionDetails"][0]["balAfterPymnt"] != undefined ? resulttable["ExecTransactionDetails"][0]["balAfterPymnt"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht") : " ";
                frmIBExecutedTransaction.lblXferToName.text = resulttable["ExecTransactionDetails"][0]["toAccName"] != undefined ? resulttable["ExecTransactionDetails"][0]["toAccName"] : " ";
                frmIBExecutedTransaction.lblXferToAccTyp.text = resulttable["ExecTransactionDetails"][0]["toAccNickName"] != undefined ? resulttable["ExecTransactionDetails"][0]["toAccNickName"] : " ";
                //frmIBExecutedTransaction.lblXferAccNo.text = resulttable["ExecTransactionDetails"][0]["toAccId"] != undefined ?
                //formatAccountNumber(resulttable["ExecTransactionDetails"][0]["toAccId"]) : " ";
                //masking CR code
                var mobileOrCI = resulttable["ExecTransactionDetails"][0]["toAccMobileOrCI"];
                //Any ID transfer from Mobile then show Mobile number
                if (isNotBlank(resulttable["ExecTransactionDetails"][0]["toAccMobile"])) {
                    if (isNotBlank(mobileOrCI) && mobileOrCI == "02") { //Mobile
                        gblSelTransferMode = 2;
                        frmIBExecutedTransaction.lblXferAccNo.text = maskMobileNumber(resulttable["ExecTransactionDetails"][0]["toAccMobile"]);
                    } else if (isNotBlank(mobileOrCI) && mobileOrCI == "01") { //Citizen
                        gblSelTransferMode = 3;
                        frmIBExecutedTransaction.lblXferAccNo.text = maskCitizenID(resulttable["ExecTransactionDetails"][0]["toAccMobile"]);
                    }
                } else {
                    gblSelTransferMode = 1;
                    frmIBExecutedTransaction.lblXferAccNo.text = resulttable["ExecTransactionDetails"][0]["maskedToAcctId"] != undefined ? encodeAccntNumbers(resulttable["ExecTransactionDetails"][0]["maskedToAcctId"]) : " ";
                }
                //MIB-4966 start
                //frmIBExecutedTransaction.lblBankValue.text = resulttable["ExecTransactionDetails"][0]["bankShortName"] != undefined ?
                //	resulttable["ExecTransactionDetails"][0]["bankShortName"] : " ";
                gblTransferTxnENCallBack = resulttable["ExecTransactionDetails"][0]["bankShortNameEN"];
                gblTransferTxnTHCallBack = resulttable["ExecTransactionDetails"][0]["bankShortNameTH"];
                if (kony.i18n.getCurrentLocale() == "en_US") {
                    frmIBExecutedTransaction.lblBankValue.text = resulttable["ExecTransactionDetails"][0]["bankShortName"] != undefined ? resulttable["ExecTransactionDetails"][0]["bankShortNameEN"] : " ";
                } else {
                    frmIBExecutedTransaction.lblBankValue.text = resulttable["ExecTransactionDetails"][0]["bankShortName"] != undefined ? resulttable["ExecTransactionDetails"][0]["bankShortNameTH"] : " ";
                }
                //MIB-4966 end					
                if (resulttable["ExecTransactionDetails"][0]["noteToRecipient"] != undefined) {
                    frmIBExecutedTransaction.hbxSplitNtr.setVisibility(true);
                    frmIBExecutedTransaction.lblSplitNTRVal.text = replaceHtmlTagChars(resulttable["ExecTransactionDetails"][0]["noteToRecipient"]);
                } else {
                    frmIBExecutedTransaction.hbxSplitNtr.setVisibility(false);
                    frmIBExecutedTransaction.lblSplitNTRVal.text = "";
                }
                if (resulttable["ExecTransactionDetails"][0]["smartFlag"] != undefined && resulttable["ExecTransactionDetails"][0]["smartFlag"] == "Y") {
                    gblSmartFlag = true;
                    if (isNotBlank(resulttable["ExecTransactionDetails"][0]["dueDate"])) {
                        frmIBExecutedTransaction.hbxSmartDate.setVisibility(true);
                        frmIBExecutedTransaction.lblSmartDateVal.text = resulttable["ExecTransactionDetails"][0]["dueDate"];
                        frmIBExecutedTransaction.hbxSmartDate.skin = "hboxLightGrey";
                        frmIBExecutedTransaction.hbxMyNote.skin = "";
                        frmIBExecutedTransaction.hbxNotifyRecipient.skin = "hboxLightGrey";
                        frmIBExecutedTransaction.hbxSplitNtr.skin = "";
                        frmIBExecutedTransaction.hbxRecipient.skin = "hboxLightGrey";
                    } else {
                        frmIBExecutedTransaction.hbxSmartDate.setVisibility(false);
                        frmIBExecutedTransaction.lblSmartDateVal.text = "";
                        frmIBExecutedTransaction.hbxMyNote.skin = "hboxLightGrey";
                        frmIBExecutedTransaction.hbxNotifyRecipient.skin = "";
                        frmIBExecutedTransaction.hbxSplitNtr.skin = "hboxLightGrey";
                        frmIBExecutedTransaction.hbxRecipient.skin = "";
                    }
                    frmIBExecutedTransaction.lblSplitXferDt.text = kony.i18n.getLocalizedString("keyOrderDAteFTMB");
                } else {
                    gblSmartFlag = false;
                    frmIBExecutedTransaction.hbxSmartDate.setVisibility(false);
                    frmIBExecutedTransaction.lblSmartDateVal.text = "";
                    frmIBExecutedTransaction.lblSplitXferDt.text = kony.i18n.getLocalizedString("keyXferTD");
                    frmIBExecutedTransaction.hbxMyNote.skin = "hboxLightGrey";
                    frmIBExecutedTransaction.hbxNotifyRecipient.skin = "";
                    frmIBExecutedTransaction.hbxSplitNtr.skin = "hboxLightGrey";
                    frmIBExecutedTransaction.hbxRecipient.skin = "";
                }
                frmIBExecutedTransaction.lblMyNoteVal.text = resulttable["ExecTransactionDetails"][0]["note"] != undefined ? replaceHtmlTagChars(resulttable["ExecTransactionDetails"][0]["note"]) : " ";
                if (resulttable["ExecTransactionDetails"][0]["recipientMobile"] == undefined) {
                    if (resulttable["ExecTransactionDetails"][0]["recipientEmail"] != undefined && resulttable["ExecTransactionDetails"][0]["recipientEmail"] != "null") {
                        frmIBExecutedTransaction.hbxNotifyRecipient.setVisibility(true);
                        frmIBExecutedTransaction.lblNotifyRecipientVal.text = "Email : " + resulttable["ExecTransactionDetails"][0]["recipientEmail"];
                    } else {
                        frmIBExecutedTransaction.lblNotifyRecipientVal.text = " ";
                        frmIBExecutedTransaction.hbxNotifyRecipient.setVisibility(false);
                    }
                } else {
                    frmIBExecutedTransaction.hbxNotifyRecipient.setVisibility(true);
                    var phoneNo = resulttable["ExecTransactionDetails"][0]["recipientMobile"];
                    if (phoneNo != "") {
                        frmIBExecutedTransaction.lblNotifyRecipientVal.text = kony.i18n.getLocalizedString("SMS") + " [xxx-xxx-" + removeHyphenIB(phoneNo.substring(6, 10)) + "]";
                    }
                }
                //
                gblXferSplit = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"];
                if (resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"].length > 1) {
                    var tempdata = [];
                    var successCount = 0;
                    var failureCount = 0;
                    var othersCount = 0;
                    for (var i = 0; i < resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"].length; i++) {
                        if (gblXferSplit[i]["txnStatus"] == "01") successCount++;
                        else if (gblXferSplit[i]["txnStatus"] == "02") failureCount++;
                        else othersCount++;
                        tempdata.push({
                            "lblSplitTRN": kony.i18n.getLocalizedString("keyTransactionRefNo"),
                            "lblSplitAmt": kony.i18n.getLocalizedString("MyActivitiesIB_Amount"),
                            "lblSplitFee": kony.i18n.getLocalizedString("keyFee"),
                            "lblSplitTRNVal": resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][i]["transRefId"] != undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][i]["transRefId"] : " ",
                            "lblSplitAmtVal": resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][i]["amount"] != undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][i]["amount"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht") : " ",
                            "lblSplitFeeVal": resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][i]["fee"] != undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][i]["fee"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht") : " ",
                            "imgFail": {
                                "src": resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][i]["txnStatus"] != undefined ? getXferImage(resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][i]["txnStatus"]) : ""
                            }
                        })
                    }
                    frmIBExecutedTransaction.lblSplitTotAmt.text = kony.i18n.getLocalizedString("keyXferTotAmt");
                    frmIBExecutedTransaction.lblSplitTotFee.text = kony.i18n.getLocalizedString("keyXferTotFee");
                    frmIBExecutedTransaction.lblSplitTotAmtVal.text = resulttable["ExecTransactionDetails"][0]["totalAmount"] != undefined ? resulttable["ExecTransactionDetails"][0]["totalAmount"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht") : " ";
                    frmIBExecutedTransaction.lblSplitTotFeeVal.text = resulttable["ExecTransactionDetails"][0]["totalFee"] != undefined ? resulttable["ExecTransactionDetails"][0]["totalFee"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht") : " ";
                    frmIBExecutedTransaction.segSplitDet.setData(tempdata);
                    frmIBExecutedTransaction.hbxRecipient.setVisibility(false);
                    frmIBExecutedTransaction.hbxSplitXfer.setVisibility(true);
                    if (failureCount == 0 && othersCount == 0) {
                        frmIBExecutedTransaction.hbxFail.setVisibility(false);
                        frmIBExecutedTransaction.hbxSuccess.setVisibility(true);
                    } else if (othersCount == 0 && successCount == 0) {
                        frmIBExecutedTransaction.hbxFail.setVisibility(true);
                        frmIBExecutedTransaction.hbxSuccess.setVisibility(false);
                    } else {
                        frmIBExecutedTransaction.hbxFail.setVisibility(true);
                        frmIBExecutedTransaction.hbxSuccess.setVisibility(false);
                    }
                } else {
                    frmIBExecutedTransaction.hbxSplitXfer.setVisibility(false);
                    frmIBExecutedTransaction.hbxRecipient.setVisibility(true);
                    frmIBExecutedTransaction.lblSplitTotAmt.text = kony.i18n.getLocalizedString("keyAmount");
                    frmIBExecutedTransaction.lblSplitTotFee.text = kony.i18n.getLocalizedString("keyXferFee");
                    if (resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"] != "") {
                        frmIBExecutedTransaction.lblTRNVal.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["transRefId"] != undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["transRefId"] : " ";
                        frmIBExecutedTransaction.lblSplitTotAmtVal.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["amount"] != undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["amount"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht") : " ",
                            frmIBExecutedTransaction.lblSplitTotFeeVal.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["fee"] != undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["fee"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht") : " ";
                        if (resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["txnStatus"] == "01") {
                            frmIBExecutedTransaction.hbxFail.setVisibility(false);
                            frmIBExecutedTransaction.hbxSuccess.setVisibility(true);
                        } else {
                            frmIBExecutedTransaction.hbxFail.setVisibility(true);
                            frmIBExecutedTransaction.hbxSuccess.setVisibility(false);
                        }
                        if (resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["txnStatus"] == "02") {
                            frmIBExecutedTransaction.hbxBalAfterXfer.setVisibility(false);
                        } else {
                            frmIBExecutedTransaction.hbxBalAfterXfer.setVisibility(true);
                        }
                    } else {
                        frmIBExecutedTransaction.lblTRNVal.text = "";
                        frmIBExecutedTransaction.lblSplitTotAmtVal.text = "";
                        frmIBExecutedTransaction.lblSplitTotFeeVal.text = "";
                    }
                }
                frmIBExecutedTransaction.lblSplitXferDtVal.text = resulttable["ExecTransactionDetails"][0]["transferDate"] != undefined ? resulttable["ExecTransactionDetails"][0]["transferDate"] : " ";
                frmIBExecutedTransaction.imgFromAccnt.src = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + resulttable["ICON_ID"] + "&modIdentifier=PRODICON";
                var personalizedId = resulttable["ExecTransactionDetails"][0]["PersonlizedID"] != undefined ? resulttable["ExecTransactionDetails"][0]["PersonlizedID"] : "";
                var toAccPic = resulttable["ExecTransactionDetails"][0]["toAccPic"] != undefined ? resulttable["ExecTransactionDetails"][0]["toAccPic"] : "";
                var ownflag = resulttable["ExecTransactionDetails"][0]["ownFlag"] != undefined ? resulttable["ExecTransactionDetails"][0]["ownFlag"] : "";
                if (isNotBlank(resulttable["ExecTransactionDetails"][0]["toAccMobile"])) {
                    if (isNotBlank(mobileOrCI) && mobileOrCI == "02") { //Mobile
                        frmIBExecutedTransaction.imgTo.src = getBankLogoURL("toMobile");
                    } else if (isNotBlank(mobileOrCI) && mobileOrCI == "01") { //Citizen
                        frmIBExecutedTransaction.imgTo.src = getBankLogoURL("toCitizen");
                    }
                    frmIBExecutedTransaction.hbxBankDet.setVisibility(false);
                    if (kony.string.equalsIgnoreCase(frmIBExecutedTransaction.lblXferToAccTyp.text, kony.i18n.getLocalizedString("MIB_P2PMob"))) {
                        frmIBExecutedTransaction.lblXferToAccTyp.setVisibility(false);
                    } else {
                        frmIBExecutedTransaction.lblXferToAccTyp.setVisibility(true);
                    }
                } else {
                    frmIBExecutedTransaction.imgTo.src = getRecipientTxnCompletePic(ownflag, toAccPic, personalizedId);
                    frmIBExecutedTransaction.lblXferToAccTyp.setVisibility(true);
                    frmIBExecutedTransaction.hbxBankDet.setVisibility(true);
                }
                dismissLoadingScreenPopup();
                frmIBExecutedTransaction.show();
            } else {
                dismissLoadingScreenPopup();
            }
        } else {
            dismissLoadingScreenPopup();
        }
    }
    dismissLoadingScreenPopup();
}

function getRecipientTxnCompletePic(ownflag, toAccPic, personalizedId) {
    var XferRcURL = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/ImageRender?billerId=&crmId=Y&personalizedId=";
    if (ownflag == 'Y') {
        rcImg = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=Y&personalizedId=&billerId=";
    } else if (personalizedId == null || personalizedId == "") {
        rcImg = "avatar.png";
    } else {
        //checking for Recipient images whether exists or not
        var randomnum = Math.floor((Math.random() * 10000) + 1);
        if (toAccPic == null || toAccPic == "") {
            rcImg = XferRcURL + personalizedId + "&rr=" + randomnum;
        } else {
            if (kony.string.endsWith(toAccPic, "nouserimg.jpg")) {
                rcImg = XferRcURL + personalizedId + "&rr=" + randomnum;
            } else {
                if (toAccPic.indexOf("fbcdn") >= 0) {
                    rcImg = toAccPic;
                } else {
                    rcImg = XferRcURL + personalizedId + "&rr=" + randomnum;
                }
            }
        }
    }
    return rcImg;
}

function getBillPaymentTxnDetCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (resulttable["ExecTransactionDetails"].length > 0) {
                frmIBBillPaymentExecutedTxn.lblAcctName.text = resulttable["ExecTransactionDetails"][0]["fromAcctNickName"] != undefined ? resulttable["ExecTransactionDetails"][0]["fromAcctNickName"] : " ";
                frmIBBillPaymentExecutedTxn.lblFromName.text = resulttable["ExecTransactionDetails"][0]["fromAcctName"] != undefined ? resulttable["ExecTransactionDetails"][0]["fromAcctName"] : " ";
                //frmIBBillPaymentExecutedTxn.lblAccountNo.text = resulttable["ExecTransactionDetails"][0]["fromAcctId"] != undefined ?
                //formatAccountNumber(resulttable["ExecTransactionDetails"][0]["fromAcctId"]) : " ";
                //masking CR code
                frmIBBillPaymentExecutedTxn.lblAccountNo.text = resulttable["ExecTransactionDetails"][0]["maskedFromAcctId"] != undefined ? formatAccountNumber(resulttable["ExecTransactionDetails"][0]["maskedFromAcctId"]) : " ";
                frmIBBillPaymentExecutedTxn.lblBBPaymentValue.text = resulttable["ExecTransactionDetails"][0]["balAfterPymnt"] != undefined ? resulttable["ExecTransactionDetails"][0]["balAfterPymnt"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht") : " ";
                if (resulttable["ExecTransactionDetails"][0]["billerShortName"] != undefined && resulttable["ExecTransactionDetails"][0]["billerCommCode"] != undefined) {
                    frmIBBillPaymentExecutedTxn.lblCompCode.text = resulttable["ExecTransactionDetails"][0]["billerShortName"] + "(" + resulttable["ExecTransactionDetails"][0]["billerCommCode"] + ")";
                } else {
                    if (resulttable["ExecTransactionDetails"][0]["billerShortName"] != undefined) frmIBBillPaymentExecutedTxn.lblCompCode.text = resulttable["ExecTransactionDetails"][0]["billerShortName"];
                    else if (resulttable["ExecTransactionDetails"][0]["billerCommCode"] != undefined) {
                        frmIBBillPaymentExecutedTxn.lblCompCode.text = "(" + resulttable["ExecTransactionDetails"][0]["billerCommCode"] + ")"
                    } else {
                        frmIBBillPaymentExecutedTxn.lblCompCode.text = "";
                    }
                }
                /*frmIBBillPaymentExecutedTxn.lblePayNickname.text = resulttable["ExecTransactionDetails"][0]["billerName"] !=
                	undefined ? resulttable["ExecTransactionDetails"][0]["billerName"] : " ";*/
                if (resulttable["ExecTransactionDetails"][0]["billerCustomerName"] != undefined) {
                    frmIBBillPaymentExecutedTxn.lblePayNickname.text = resulttable["ExecTransactionDetails"][0]["billerCustomerName"]
                } else {
                    frmIBBillPaymentExecutedTxn.lblePayNickname.text = frmIBBillPaymentExecutedTxn.lblCompCode.text
                }
                frmIBBillPaymentExecutedTxn.lblRef1Value.text = resulttable["ExecTransactionDetails"][0]["maskedBillerRef1"] != undefined ? resulttable["ExecTransactionDetails"][0]["maskedBillerRef1"] : " ";
                if (resulttable["ExecTransactionDetails"][0]["billerRef2"] != undefined) {
                    frmIBBillPaymentExecutedTxn.lblRef2Value.text = resulttable["ExecTransactionDetails"][0]["billerRef2"];
                    frmIBBillPaymentExecutedTxn.hbxRef2.setVisibility(true);
                } else {
                    frmIBBillPaymentExecutedTxn.lblRef2Value.text = "";
                    frmIBBillPaymentExecutedTxn.hbxRef2.setVisibility(false);
                }
                frmIBBillPaymentExecutedTxn.lblePayDate.text = resulttable["ExecTransactionDetails"][0]["transferDate"] != undefined ? resulttable["ExecTransactionDetails"][0]["transferDate"] : " ";
                frmIBBillPaymentExecutedTxn.lblMNVal.text = resulttable["ExecTransactionDetails"][0]["note"] != undefined ? resulttable["ExecTransactionDetails"][0]["note"] : " ";
                if (resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"].length > 0) {
                    frmIBBillPaymentExecutedTxn.lblTRNVal.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["transRefId"] != undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["transRefId"] : " ";
                    frmIBBillPaymentExecutedTxn.lblPaymentAmountValue.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"]
                        [0]["amount"] != undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["amount"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht") : " ";
                    frmIBBillPaymentExecutedTxn.lblFeeVal.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["fee"] != undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["fee"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht") : " ";
                    resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["txnStatus"] == "01" ? frmIBBillPaymentExecutedTxn.imgComplete.src = "iconcomplete.png" : frmIBBillPaymentExecutedTxn.imgComplete.src = "icon_notcomplete.png";
                    resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["txnStatus"] == "02" ? frmIBBillPaymentExecutedTxn.hbxBalanceAfterPayment.setVisibility(false) : frmIBBillPaymentExecutedTxn.hbxBalanceAfterPayment.setVisibility(true);
                } else {
                    frmIBBillPaymentExecutedTxn.lblTRNVal.text = "";
                    frmIBBillPaymentExecutedTxn.lblPaymentAmountValue.text = "";
                    frmIBBillPaymentExecutedTxn.lblFeeVal.text = "";
                }
                var imagesUrl = BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + resulttable["ExecTransactionDetails"][0]["billerCommCode"] + "&modIdentifier=MyBillers";
                frmIBBillPaymentExecutedTxn.imgTo.src = imagesUrl;
                frmIBBillPaymentExecutedTxn.imgFrom.src = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + resulttable["ICON_ID"] + "&modIdentifier=PRODICON";
                gblExecRef1ENVal = resulttable["ExecTransactionDetails"][0]["Ref1EN"] != undefined ? resulttable["ExecTransactionDetails"][0]["Ref1EN"] : "";
                gblExecRef2ENVal = resulttable["ExecTransactionDetails"][0]["Ref2EN"] != undefined ? resulttable["ExecTransactionDetails"][0]["Ref2EN"] : "";
                gblExecRef1THVal = resulttable["ExecTransactionDetails"][0]["Ref1TH"] != undefined ? resulttable["ExecTransactionDetails"][0]["Ref1TH"] : "";
                gblExecRef2THVal = resulttable["ExecTransactionDetails"][0]["Ref2TH"] != undefined ? resulttable["ExecTransactionDetails"][0]["Ref2TH"] : "";
                gblExecRef2Req = resulttable["ExecTransactionDetails"][0]["isRef2Req"] != undefined ? resulttable["ExecTransactionDetails"][0]["isRef2Req"] : "";
                showRefValues(frmIBBillPaymentExecutedTxn);
                frmIBBillPaymentExecutedTxn.lblCompCode.setVisibility(false);
                dismissLoadingScreenPopup();
                frmIBBillPaymentExecutedTxn.show();
            } else {
                dismissLoadingScreenPopup();
            }
        } else {
            dismissLoadingScreenPopup();
        }
    }
    dismissLoadingScreenPopup();
}

function getTopUpTxnDetCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (resulttable["ExecTransactionDetails"].length > 0) {
                frmIBTopUpExecutedTxn.lblAcctName.text = resulttable["ExecTransactionDetails"][0]["fromAcctNickName"] != undefined ? resulttable["ExecTransactionDetails"][0]["fromAcctNickName"] : " ";
                frmIBTopUpExecutedTxn.lblFromName.text = resulttable["ExecTransactionDetails"][0]["fromAcctName"] != undefined ? resulttable["ExecTransactionDetails"][0]["fromAcctName"] : " ";
                //frmIBTopUpExecutedTxn.lblAccountNo.text = resulttable["ExecTransactionDetails"][0]["fromAcctId"] != undefined ?
                //formatAccountNumber(resulttable["ExecTransactionDetails"][0]["fromAcctId"]) : " ";
                //masking CR code
                frmIBTopUpExecutedTxn.lblAccountNo.text = resulttable["ExecTransactionDetails"][0]["maskedFromAcctId"] != undefined ? formatAccountNumber(resulttable["ExecTransactionDetails"][0]["maskedFromAcctId"]) : " ";
                frmIBTopUpExecutedTxn.lblePayNickname.text = resulttable["ExecTransactionDetails"][0]["billerCustomerName"] != undefined ? resulttable["ExecTransactionDetails"][0]["billerCustomerName"] : " ";
                frmIBTopUpExecutedTxn.lblBBPaymentValue.text = resulttable["ExecTransactionDetails"][0]["balAfterPymnt"] != undefined ? resulttable["ExecTransactionDetails"][0]["balAfterPymnt"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht") : " ";
                if (resulttable["ExecTransactionDetails"][0]["billerShortName"] != undefined && resulttable["ExecTransactionDetails"][0]["billerCommCode"] != undefined) {
                    frmIBTopUpExecutedTxn.lblCompCode.text = resulttable["ExecTransactionDetails"][0]["billerShortName"] + "(" + resulttable["ExecTransactionDetails"][0]["billerCommCode"] + ")";
                } else {
                    if (resulttable["ExecTransactionDetails"][0]["billerShortName"] != undefined) frmIBTopUpExecutedTxn.lblCompCode.text = resulttable["ExecTransactionDetails"][0]["billerShortName"];
                    else if (resulttable["ExecTransactionDetails"][0]["billerCommCode"] != undefined) {
                        frmIBTopUpExecutedTxn.lblCompCode.text = "(" + resulttable["ExecTransactionDetails"][0]["billerCommCode"] + ")"
                    } else {
                        frmIBTopUpExecutedTxn.lblCompCode.text = "";
                    }
                }
                frmIBTopUpExecutedTxn.hbxToNickname.isVisible = false; // showing only 1 label for Nickname/Biller name compcode
                //frmIBTopUpExecutedTxn.lblRef1Value.text = resulttable["ExecTransactionDetails"][0]["billerRef1"] != undefined ?
                //					resulttable["ExecTransactionDetails"][0]["billerRef1"] : " ";
                // added below line to mask 16 digit number. cr PCI-DSS
                if (undefined != resulttable["ExecTransactionDetails"][0]["maskedBillerRef1"]) {
                    frmIBTopUpExecutedTxn.lblRef1Value.text = resulttable["ExecTransactionDetails"][0]["maskedBillerRef1"];
                } else {
                    frmIBTopUpExecutedTxn.lblRef1Value.text = " ";
                }
                frmIBTopUpExecutedTxn.lblePayDate.text = resulttable["ExecTransactionDetails"][0]["transferDate"] != undefined ? resulttable["ExecTransactionDetails"][0]["transferDate"] : " ";
                frmIBTopUpExecutedTxn.lblMNVal.text = resulttable["ExecTransactionDetails"][0]["note"] != undefined ? replaceHtmlTagChars(resulttable["ExecTransactionDetails"][0]["note"]) : " ";
                if (resulttable["ExecTransactionDetails"][0]["billerCommCode"] == "2151") {
                    frmIBTopUpExecutedTxn.hbxEasyPassCustName.setVisibility(true);
                    frmIBTopUpExecutedTxn.hbxCardBal.setVisibility(true);
                    frmIBTopUpExecutedTxn.hbxTopupRefId.setVisibility(true);
                    frmIBTopUpExecutedTxn.lblcustnameVal.text = resulttable["ExecTransactionDetails"]["0"]["billerCustomerName"] != undefined ? resulttable["ExecTransactionDetails"]["0"]["billerCustomerName"] : " ";
                    frmIBTopUpExecutedTxn.lblCardBalVal.text = resulttable["ExecTransactionDetails"]["0"]["billerBalance"] != undefined ? ProfileCommFormat(resulttable["ExecTransactionDetails"]["0"]["billerBalance"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht") : " ";
                    frmIBTopUpExecutedTxn.lblTopupRefIdVal.text = resulttable["ExecTransactionDetails"]["0"]["BillerMethod"] != undefined ? resulttable["ExecTransactionDetails"]["0"]["BillerMethod"] : " "; //parameter nt added
                } else {
                    frmIBTopUpExecutedTxn.hbxEasyPassCustName.setVisibility(false);
                    frmIBTopUpExecutedTxn.hbxCardBal.setVisibility(false);
                    frmIBTopUpExecutedTxn.hbxTopupRefId.setVisibility(false);
                }
                if (resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"].length > 0) {
                    frmIBTopUpExecutedTxn.lblTRNVal.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["transRefId"] != undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["transRefId"] : " ";
                    frmIBTopUpExecutedTxn.lblAmtVal.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["amount"] != undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["amount"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht") : " ";
                    frmIBTopUpExecutedTxn.lblFeeVal.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["fee"] != undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["fee"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht") : " ";
                    resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["txnStatus"] == "01" ? frmIBTopUpExecutedTxn.imgComplete.src = "iconcomplete.png" : frmIBTopUpExecutedTxn.imgComplete.src = "icon_notcomplete.png";
                    resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["txnStatus"] == "02" ? frmIBTopUpExecutedTxn.hbxBalanceAfterPayment.setVisibility(false) : frmIBTopUpExecutedTxn.hbxBalanceAfterPayment.setVisibility(true);
                } else {
                    frmIBTopUpExecutedTxn.lblTRNVal.text = "";
                    frmIBTopUpExecutedTxn.lblAmtVal.text = "";
                    frmIBTopUpExecutedTxn.lblFeeVal.text = "";
                }
                var imagesUrl = BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + resulttable["ExecTransactionDetails"][0]["billerCommCode"] + "&modIdentifier=MyBillers";
                frmIBTopUpExecutedTxn.imgTo.src = imagesUrl;
                var imageFromUrlIconId = getIconIdFromAccounts(resulttable["ExecTransactionDetails"][0]["fromAcctId"]);
                frmIBTopUpExecutedTxn.imgFrom.src = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + imageFromUrlIconId + "&modIdentifier=PRODICON";
                gblExecRef1ENVal = resulttable["ExecTransactionDetails"][0]["Ref1EN"] != undefined ? resulttable["ExecTransactionDetails"][0]["Ref1EN"] : "";
                gblExecRef2ENVal = resulttable["ExecTransactionDetails"][0]["Ref2EN"] != undefined ? resulttable["ExecTransactionDetails"][0]["Ref2EN"] : "";
                gblExecRef1THVal = resulttable["ExecTransactionDetails"][0]["Ref1TH"] != undefined ? resulttable["ExecTransactionDetails"][0]["Ref1TH"] : "";
                gblExecRef2THVal = resulttable["ExecTransactionDetails"][0]["Ref2TH"] != undefined ? resulttable["ExecTransactionDetails"][0]["Ref2TH"] : "";
                gblExecRef2Req = resulttable["ExecTransactionDetails"][0]["isRef2Req"] != undefined ? resulttable["ExecTransactionDetails"][0]["isRef2Req"] : "";
                showRefValues(frmIBTopUpExecutedTxn);
                dismissLoadingScreenPopup();
                frmIBTopUpExecutedTxn.show();
            } else {
                dismissLoadingScreenPopup();
            }
        } else {
            dismissLoadingScreenPopup();
        }
    }
    dismissLoadingScreenPopup();
}

function getBeepBillTxnDetCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (resulttable["ExecTransactionDetails"].length > 0) {
                frmIBBeepAndBillExecutedTxn.lblAcctName.text = resulttable["ExecTransactionDetails"][0]["fromAcctNickName"] != undefined ? resulttable["ExecTransactionDetails"][0]["fromAcctNickName"] : " ";
                frmIBBeepAndBillExecutedTxn.lblFromName.text = resulttable["ExecTransactionDetails"][0]["fromAcctName"] != undefined ? resulttable["ExecTransactionDetails"][0]["fromAcctName"] : " ";
                //frmIBBeepAndBillExecutedTxn.lblAccountNo.text = resulttable["ExecTransactionDetails"][0]["fromAcctId"] != undefined ?
                //formatAccountNumber(resulttable["ExecTransactionDetails"][0]["fromAcctId"]) : " ";
                //masking CR code
                frmIBBeepAndBillExecutedTxn.lblAccountNo.text = resulttable["ExecTransactionDetails"][0]["maskedFromAcctId"] != undefined ? formatAccountNumber(resulttable["ExecTransactionDetails"][0]["maskedFromAcctId"]) : " ";
                frmIBBeepAndBillExecutedTxn.lblePayNickname.text = resulttable["ExecTransactionDetails"][0]["billerName"] != undefined ? resulttable["ExecTransactionDetails"][0]["billerName"] : " ";
                frmIBBeepAndBillExecutedTxn.lblBBPaymentValue.text = resulttable["ExecTransactionDetails"][0]["balAfterPymnt"] != undefined ? resulttable["ExecTransactionDetails"][0]["balAfterPymnt"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht") : " ";
                if (resulttable["ExecTransactionDetails"][0]["billerShortName"] != undefined && resulttable["ExecTransactionDetails"][0]["billerCommCode"] != undefined) {
                    frmIBBeepAndBillExecutedTxn.lblCompCode.text = resulttable["ExecTransactionDetails"][0]["billerShortName"] + "(" + resulttable["ExecTransactionDetails"][0]["billerCommCode"] + ")";
                } else {
                    if (resulttable["ExecTransactionDetails"][0]["billerShortName"] != undefined) frmIBBeepAndBillExecutedTxn.lblCompCode.text = resulttable["ExecTransactionDetails"][0]["billerShortName"];
                    else if (resulttable["ExecTransactionDetails"][0]["billerCommCode"] != undefined) {
                        frmIBBeepAndBillExecutedTxn.lblCompCode.text = "(" + resulttable["ExecTransactionDetails"][0]["billerCommCode"] + ")"
                    } else {
                        frmIBBeepAndBillExecutedTxn.lblCompCode.text = "";
                    }
                }
                frmIBBeepAndBillExecutedTxn.lblRef1Value.text = resulttable["ExecTransactionDetails"][0]["billerRef1"] != undefined ? resulttable["ExecTransactionDetails"][0]["billerRef1"] : " ";
                if (resulttable["ExecTransactionDetails"][0]["billerRef2"] != undefined) {
                    frmIBBeepAndBillExecutedTxn.lblRef2Value.text = resulttable["ExecTransactionDetails"][0]["billerRef2"];
                    frmIBBeepAndBillExecutedTxn.hbxRef2.setVisibility(true);
                } else {
                    frmIBBeepAndBillExecutedTxn.lblRef2Value.text = "";
                    frmIBBeepAndBillExecutedTxn.hbxRef2.setVisibility(false);
                }
                frmIBBeepAndBillExecutedTxn.lblePayDate.text = resulttable["ExecTransactionDetails"][0]["transferDate"] != undefined ? resulttable["ExecTransactionDetails"][0]["transferDate"] : " ";
                frmIBBeepAndBillExecutedTxn.lblDueDtValue.text = resulttable["ExecTransactionDetails"][0]["dueDate"] != undefined ? resulttable["ExecTransactionDetails"][0]["dueDate"] : " ";
                frmIBBeepAndBillExecutedTxn.lblTxnIDVal.text = resulttable["ExecTransactionDetails"][0]["beepBillTransId"] != undefined ? resulttable["ExecTransactionDetails"][0]["beepBillTransId"] : " ";
                if (resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"].length > 0) {
                    frmIBBeepAndBillExecutedTxn.lblTRNVal.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["transRefId"] != undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["transRefId"] : " ";
                    frmIBBeepAndBillExecutedTxn.lblPaymentAmountValue.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"]
                        [0]["amount"] != undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["amount"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht") : " ";
                    frmIBBeepAndBillExecutedTxn.lblFeeVal.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["fee"] != undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["fee"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht") : " ";
                    resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["txnStatus"] == "01" ? frmIBBeepAndBillExecutedTxn.imgComplete.src = "iconcomplete.png" : frmIBBeepAndBillExecutedTxn.imgComplete.src = "icon_notcomplete.png";
                    resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["txnStatus"] == "02" ? frmIBBeepAndBillExecutedTxn.hbxBalanceafterPayment.setVisibility(false) : frmIBBeepAndBillExecutedTxn.hbxBalanceafterPayment.setVisibility(true);
                } else {
                    frmIBBeepAndBillExecutedTxn.lblTRNVal.text = "";
                    frmIBBeepAndBillExecutedTxn.lblPaymentAmountValue.text = "";
                    frmIBBeepAndBillExecutedTxn.lblFeeVal.text = "";
                }
                var imagesUrl = BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + resulttable["ExecTransactionDetails"][0]["billerCommCode"] + "&modIdentifier=MyBillers";
                frmIBBeepAndBillExecutedTxn.imgTo.src = imagesUrl;
                frmIBBeepAndBillExecutedTxn.imgFrom.src = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + resulttable["ICON_ID"] + "&modIdentifier=PRODICON";
                gblExecRef1ENVal = resulttable["ExecTransactionDetails"][0]["Ref1EN"] != undefined ? resulttable["ExecTransactionDetails"][0]["Ref1EN"] : "";
                gblExecRef2ENVal = resulttable["ExecTransactionDetails"][0]["Ref2EN"] != undefined ? resulttable["ExecTransactionDetails"][0]["Ref2EN"] : "";
                gblExecRef1THVal = resulttable["ExecTransactionDetails"][0]["Ref1TH"] != undefined ? resulttable["ExecTransactionDetails"][0]["Ref1TH"] : "";
                gblExecRef2THVal = resulttable["ExecTransactionDetails"][0]["Ref2TH"] != undefined ? resulttable["ExecTransactionDetails"][0]["Ref2TH"] : "";
                gblExecRef2Req = resulttable["ExecTransactionDetails"][0]["isRef2Req"] != undefined ? resulttable["ExecTransactionDetails"][0]["isRef2Req"] : "";
                showRefValues(frmIBBeepAndBillExecutedTxn);
                dismissLoadingScreenPopup();
                frmIBBeepAndBillExecutedTxn.show();
            } else {
                dismissLoadingScreenPopup();
            }
        } else {
            dismissLoadingScreenPopup();
        }
    }
    dismissLoadingScreenPopup();
}

function getePmntTxnDetCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (resulttable["ExecTransactionDetails"].length > 0) {
                frmIBePaymentExecutedTxn.lblAcctName.text = resulttable["ExecTransactionDetails"][0]["fromAcctNickName"] != undefined ? resulttable["ExecTransactionDetails"][0]["fromAcctNickName"] : " ";
                frmIBePaymentExecutedTxn.lblAccountName.text = resulttable["ExecTransactionDetails"][0]["fromAcctName"] != undefined ? resulttable["ExecTransactionDetails"][0]["fromAcctName"] : " ";
                //frmIBePaymentExecutedTxn.lblAccountNo.text = resulttable["ExecTransactionDetails"][0]["fromAcctId"] != undefined ?
                //formatAccountNumber(resulttable["ExecTransactionDetails"][0]["fromAcctId"]) : " ";
                //masking CR code
                frmIBePaymentExecutedTxn.lblAccountNo.text = resulttable["ExecTransactionDetails"][0]["maskedFromAcctId"] != undefined ? formatAccountNumber(resulttable["ExecTransactionDetails"][0]["maskedFromAcctId"]) : " ";
                //frmIBePaymentExecutedTxn.lblToBiller.text = resulttable["ExecTransactionDetails"][0]["billerShortName"]!=undefined ? resulttable["ExecTransactionDetails"][0]["billerShortName"] : " ";
                if (resulttable["ExecTransactionDetails"][0]["billerShortName"] != undefined && resulttable["ExecTransactionDetails"][0]["billerCommCode"] != undefined) {
                    frmIBePaymentExecutedTxn.lblePayNickname.text = resulttable["ExecTransactionDetails"][0]["billerShortName"] + "(" + resulttable["ExecTransactionDetails"][0]["billerCommCode"] + ")";
                } else {
                    if (resulttable["ExecTransactionDetails"][0]["billerShortName"] != undefined) frmIBePaymentExecutedTxn.lblePayNickname.text = resulttable["ExecTransactionDetails"][0]["billerShortName"];
                    else if (resulttable["ExecTransactionDetails"][0]["billerCommCode"] != undefined) {
                        frmIBePaymentExecutedTxn.lblePayNickname.text = "(" + resulttable["ExecTransactionDetails"][0]["billerCommCode"] + ")"
                    } else {
                        frmIBePaymentExecutedTxn.lblePayNickname.text = "";
                    }
                }
                if (resulttable["ExecTransactionDetails"][0]["flexValue1"] != undefined) {
                    frmIBePaymentExecutedTxn.lblFlexiParam1Value.text = resulttable["ExecTransactionDetails"][0]["flexValue1"];
                    frmIBePaymentExecutedTxn.hbxFlexi1.setVisibility(true);
                } else {
                    frmIBePaymentExecutedTxn.lblFlexiParam1Value.text = " ";
                    frmIBePaymentExecutedTxn.hbxFlexi1.setVisibility(false);
                }
                if (resulttable["ExecTransactionDetails"][0]["flexValue2"] != undefined) {
                    frmIBePaymentExecutedTxn.lblFlexiParam2Value.text = resulttable["ExecTransactionDetails"][0]["flexValue2"];
                    frmIBePaymentExecutedTxn.hbxFlexi2.setVisibility(true);
                } else {
                    frmIBePaymentExecutedTxn.lblFlexiParam2Value.text = " ";
                    frmIBePaymentExecutedTxn.hbxFlexi2.setVisibility(false);
                }
                if (resulttable["ExecTransactionDetails"][0]["flexValue3"] != undefined) {
                    frmIBePaymentExecutedTxn.lblFlexiParam3Value.text = resulttable["ExecTransactionDetails"][0]["flexValue3"];
                    frmIBePaymentExecutedTxn.hbxFlexi3.setVisibility(true);
                } else {
                    frmIBePaymentExecutedTxn.lblFlexiParam3Value.text = " ";
                    frmIBePaymentExecutedTxn.hbxFlexi3.setVisibility(false);
                }
                frmIBePaymentExecutedTxn.lblBBPaymentValue.text = resulttable["ExecTransactionDetails"][0]["balAfterPymnt"] != undefined ? resulttable["ExecTransactionDetails"][0]["balAfterPymnt"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht") : " ";
                //frmIBePaymentExecutedTxn.lblePayRef1Value.text = resulttable["ExecTransactionDetails"][0]["billerRef1"] !=
                //undefined ? resulttable["ExecTransactionDetails"][0]["billerRef1"] : " ";
                //masking CR code
                frmIBePaymentExecutedTxn.lblePayRef1Value.text = resulttable["ExecTransactionDetails"][0]["maskedBillerRef1"] != undefined ? resulttable["ExecTransactionDetails"][0]["maskedBillerRef1"] : " ";
                if (resulttable["ExecTransactionDetails"][0]["billerRef2"] != undefined) {
                    frmIBePaymentExecutedTxn.lblePayRef2Value.text = resulttable["ExecTransactionDetails"][0]["billerRef2"];
                    frmIBePaymentExecutedTxn.hbxRef2.setVisibility(true);
                } else {
                    frmIBePaymentExecutedTxn.lblePayRef2Value.text = "";
                    frmIBePaymentExecutedTxn.hbxRef2.setVisibility(false);
                }
                frmIBePaymentExecutedTxn.lblePayDate.text = resulttable["ExecTransactionDetails"][0]["transferDate"] != undefined ? resulttable["ExecTransactionDetails"][0]["transferDate"] : " ";
                if (resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"].length > 0) {
                    frmIBePaymentExecutedTxn.lblTRNVal.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["transRefId"] != undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["transRefId"] : " ";
                    frmIBePaymentExecutedTxn.lblAmtVal.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["amount"] != undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["amount"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht") : " ";
                    frmIBePaymentExecutedTxn.lblFeeVal.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["fee"] != undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["fee"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht") : " ";
                    resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["txnStatus"] == "01" ? frmIBePaymentExecutedTxn.imgComplete.src = "iconcomplete.png" : frmIBePaymentExecutedTxn.imgComplete.src = "icon_notcomplete.png";
                    resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["txnStatus"] == "02" ? frmIBePaymentExecutedTxn.hbxBalAfterPayment.setVisibility(false) : frmIBePaymentExecutedTxn.hbxBalAfterPayment.setVisibility(true);
                } else {
                    frmIBePaymentExecutedTxn.lblTRNVal.text = "";
                    frmIBePaymentExecutedTxn.lblAmtVal.text = "";
                    frmIBePaymentExecutedTxn.lblFeeVal.text = "";
                }
                var imagesUrl = BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + resulttable["ExecTransactionDetails"][0]["billerCommCode"] + "&modIdentifier=MyBillers";
                frmIBePaymentExecutedTxn.imgTo.src = imagesUrl;
                frmIBePaymentExecutedTxn.imgFrom.src = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + resulttable["ICON_ID"] + "&modIdentifier=PRODICON";
                gblExecRef1ENVal = resulttable["ExecTransactionDetails"][0]["Ref1EN"] != undefined ? resulttable["ExecTransactionDetails"][0]["Ref1EN"] : "";
                gblExecRef2ENVal = resulttable["ExecTransactionDetails"][0]["Ref2EN"] != undefined ? resulttable["ExecTransactionDetails"][0]["Ref2EN"] : "";
                gblExecRef1THVal = resulttable["ExecTransactionDetails"][0]["Ref1TH"] != undefined ? resulttable["ExecTransactionDetails"][0]["Ref1TH"] : "";
                gblExecRef2THVal = resulttable["ExecTransactionDetails"][0]["Ref2TH"] != undefined ? resulttable["ExecTransactionDetails"][0]["Ref2TH"] : "";
                gblExecRef2Req = resulttable["ExecTransactionDetails"][0]["isRef2Req"] != undefined ? resulttable["ExecTransactionDetails"][0]["isRef2Req"] : "";
                gblExecFlex1ENVal = resulttable["ExecTransactionDetails"][0]["Flex1EN"] != undefined ? resulttable["ExecTransactionDetails"][0]["Flex1EN"] : "";
                gblExecFlex1THVal = resulttable["ExecTransactionDetails"][0]["Flex1TH"] != undefined ? resulttable["ExecTransactionDetails"][0]["Flex1TH"] : "";
                gblExecFlex2ENVal = resulttable["ExecTransactionDetails"][0]["Flex2EN"] != undefined ? resulttable["ExecTransactionDetails"][0]["Flex2EN"] : "";
                gblExecFlex2THVal = resulttable["ExecTransactionDetails"][0]["Flex2TH"] != undefined ? resulttable["ExecTransactionDetails"][0]["Flex2TH"] : "";
                gblExecFlex3ENVal = resulttable["ExecTransactionDetails"][0]["Flex3EN"] != undefined ? resulttable["ExecTransactionDetails"][0]["Flex3EN"] : "";
                gblExecFlex3THVal = resulttable["ExecTransactionDetails"][0]["Flex3TH"] != undefined ? resulttable["ExecTransactionDetails"][0]["Flex3TH"] : "";
                gblExecRef2Req = resulttable["ExecTransactionDetails"][0]["isRef2Req"] != undefined ? resulttable["ExecTransactionDetails"][0]["isRef2Req"] : "";
                showRefValues(frmIBePaymentExecutedTxn);
                dismissLoadingScreenPopup();
                frmIBePaymentExecutedTxn.show();
            } else {
                dismissLoadingScreenPopup();
            }
        } else {
            dismissLoadingScreenPopup();
        }
    }
    dismissLoadingScreenPopup();
}

function setDatatoSavingscareLabels() {
    if (gblNSTxnDetCallBackResult != "") {
        if (gblNSTxnDetCallBackResult["ExecTransactionDetails"][0]["openPrdCode"] == "211") {
            frmIBTOASavingsExecutedTxn.lblCongrats.text = kony.i18n.getLocalizedString("keySavingCareGreet");
            if (kony.i18n.getCurrentLocale() == "en_US") {
                frmIBTOASavingsExecutedTxn.lblNSProdName.text = gblProdNameEN;
            } else {
                frmIBTOASavingsExecutedTxn.lblNSProdName.text = gblProdNameTH;
            }
        } else {
            frmIBTOASavingsExecutedTxn.lblCongrats.text = kony.i18n.getLocalizedString("keyAllFreeGreet");
            if (kony.i18n.getCurrentLocale() == "en_US") {
                frmIBTOASavingsExecutedTxn.lblNSProdName.text = gblProdNameEN;
            } else {
                frmIBTOASavingsExecutedTxn.lblNSProdName.text = gblProdNameTH;
            }
        }
    }
}
//Open account types complete screens
gblNSTxnDetCallBackResult = "";

function getNSTxnDetCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (resulttable["ExecTransactionDetails"].length > 0) {
                gblNSTxnDetCallBackResult = resulttable;
                frmIBTOASavingsExecutedTxn.imgNSProdName.src = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + resulttable["openICONID"] + "&modIdentifier=PRODICON";
                frmIBTOASavingsExecutedTxn.lblAccNSName.text = resulttable["ExecTransactionDetails"][0]["fromAcctNickName"] != undefined ? resulttable["ExecTransactionDetails"][0]["fromAcctNickName"] : " ";
                frmIBTOASavingsExecutedTxn.lblActNoval.text = resulttable["ExecTransactionDetails"][0]["fromAcctId"] != undefined ? formatAccountNumber(resulttable["ExecTransactionDetails"][0]["fromAcctId"]) : " ";
                frmIBTOASavingsExecutedTxn.lblIBOpenAccCnfmBalAmt.text = resulttable["ExecTransactionDetails"][0]["balAfterPymnt"] != undefined ? resulttable["ExecTransactionDetails"][0]["balAfterPymnt"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht") : " ";
                frmIBTOASavingsExecutedTxn.lblNickNameVal.text = resulttable["ExecTransactionDetails"][0]["toAccNickName"] != undefined ? resulttable["ExecTransactionDetails"][0]["toAccNickName"] : " ";
                frmIBTOASavingsExecutedTxn.lblAccuntNoVal.text = resulttable["ExecTransactionDetails"][0]["toAccId"] != undefined ? formatAccountNumber(resulttable["ExecTransactionDetails"][0]["toAccId"]) : " ";
                frmIBTOASavingsExecutedTxn.lblActName.text = resulttable["ExecTransactionDetails"][0]["toAccName"] != undefined ? resulttable["ExecTransactionDetails"][0]["toAccName"] : " ";
                gblProdNameEN = resulttable["openProductNameEN"];
                gblBranchNameEN = resulttable["ExecTransactionDetails"][0]["toAcctBranchNameEN"];
                gblProdNameTH = resulttable["openProductNameTH"];
                gblBranchNameTH = resulttable["ExecTransactionDetails"][0]["toAcctBranchNameTH"];
                if (resulttable["ExecTransactionDetails"][0]["openPrdCode"] != null && (resulttable["ExecTransactionDetails"][0]["openPrdCode"] == "225" || resulttable["ExecTransactionDetails"][0]["openPrdCode"] == "226")) {
                    frmIBTOASavingsExecutedTxn.hbxCardFee.setVisibility(true);
                    frmIBTOASavingsExecutedTxn.hbxCardType.setVisibility(true);
                    frmIBTOASavingsExecutedTxn.hbxAddress.setVisibility(true);
                    frmIBTOASavingsExecutedTxn.lblAddressHeader.setVisibility(true);
                    frmIBTOASavingsExecutedTxn.line474106367283368.setVisibility(true);
                    frmIBTOASavingsExecutedTxn.lblAddressHeader.text = appendColon(kony.i18n.getLocalizedString("keyAddressMailingCard"));
                    frmIBTOASavingsExecutedTxn.clblContactAdd2.text = resulttable["ExecTransactionDetails"][0]["flexValue3"];
                    frmIBTOASavingsExecutedTxn.clblContactAdd3.text = resulttable["ExecTransactionDetails"][0]["flexValue4"];
                    frmIBTOASavingsExecutedTxn.lblCardfeeValue.text = resulttable["ExecTransactionDetails"][0]["flexValue2"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                    frmIBTOASavingsExecutedTxn.lblCardTypeValue.text = resulttable["ExecTransactionDetails"][0]["flexValue1"];
                } else {
                    frmIBTOASavingsExecutedTxn.hbxCardFee.setVisibility(false);
                    frmIBTOASavingsExecutedTxn.hbxCardType.setVisibility(false);
                    frmIBTOASavingsExecutedTxn.hbxAddress.setVisibility(false);
                    frmIBTOASavingsExecutedTxn.lblAddressHeader.setVisibility(false);
                    frmIBTOASavingsExecutedTxn.line474106367283368.setVisibility(false);
                }
                if (kony.i18n.getCurrentLocale() == "en_US") {
                    frmIBTOASavingsExecutedTxn.lblBranchVal.text = gblBranchNameEN;
                    frmIBTOASavingsExecutedTxn.lblProdNameVal.text = gblProdNameEN;
                    frmIBTOASavingsExecutedTxn.lblNSProdName.text = gblProdNameEN;
                } else {
                    frmIBTOASavingsExecutedTxn.lblBranchVal.text = gblBranchNameTH;
                    frmIBTOASavingsExecutedTxn.lblProdNameVal.text = gblProdNameTH;
                    frmIBTOASavingsExecutedTxn.lblNSProdName.text = gblProdNameTH;
                }
                var platformChannel = "";
                platformChannel = gblDeviceInfo.name;
                if (platformChannel == "thinclient") {
                    if (resulttable["ExecTransactionDetails"] != null && resulttable["ExecTransactionDetails"] != undefined) {
                        if (resulttable["ExecTransactionDetails"][0] != null && resulttable["ExecTransactionDetails"][0] != undefined) {
                            if (resulttable["ExecTransactionDetails"][0]["openPrdCode"] == "211") {
                                frmIBTOASavingsExecutedTxn.lblCongrats.text = kony.i18n.getLocalizedString("keySavingCareGreet");
                            } else {
                                frmIBTOASavingsExecutedTxn.lblCongrats.text = kony.i18n.getLocalizedString("keyAllFreeGreet");
                            }
                        }
                    }
                }
                frmIBTOASavingsExecutedTxn.lblInterestRateVal.text = resulttable["ExecTransactionDetails"][0]["TDInterestRate"] != undefined ? resulttable["ExecTransactionDetails"][0]["TDInterestRate"] + "" + "%" : " ";
                frmIBTOASavingsExecutedTxn.lblOpeningDateVal.text = resulttable["ExecTransactionDetails"][0]["transferDate"] != undefined ? resulttable["ExecTransactionDetails"][0]["transferDate"] : " ";
                frmIBTOASavingsExecutedTxn.imgFrom.src = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + resulttable["ICON_ID"] + "&modIdentifier=PRODICON";
                if (resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"].length > 0) {
                    frmIBTOASavingsExecutedTxn.lbltransactionVal.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["transRefId"] != undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["transRefId"] : " ";
                    frmIBTOASavingsExecutedTxn.lblOpenAmtVal.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["amount"] != undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["amount"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht") : " ";
                    resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["txnStatus"] == "01" ? frmIBTOASavingsExecutedTxn.imgComplete.src = "iconcomplete.png" : frmIBTOASavingsExecutedTxn.imgComplete.src = "icon_notcomplete.png";
                    resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["txnStatus"] == "02" ? setVisiblityBalanceTransfer(frmIBTOASavingsExecutedTxn.lblBalanceBefrTransfr, frmIBTOASavingsExecutedTxn.lblIBOpenAccCnfmBalAmt, false) : setVisiblityBalanceTransfer(frmIBTOASavingsExecutedTxn.lblBalanceBefrTransfr, frmIBTOASavingsExecutedTxn.lblIBOpenAccCnfmBalAmt, true);
                } else {
                    frmIBTOASavingsExecutedTxn.lbltransactionVal.text = "";
                    frmIBTOASavingsExecutedTxn.lblOpenAmtVal.text = "";
                }
                dismissLoadingScreenPopup();
                frmIBTOASavingsExecutedTxn.show();
            }
        }
    }
    dismissLoadingScreenPopup();
}

function getSCTxnDetCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (resulttable["ExecTransactionDetails"].length > 0) {
                frmIBTOASavingsCareExecutedTxn.imgOASCTitle.src = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + resulttable["openICONID"] + "&modIdentifier=PRODICON";
                frmIBTOASavingsCareExecutedTxn.lblNFAccName.text = resulttable["ExecTransactionDetails"][0]["fromAcctNickName"] != undefined ? resulttable["ExecTransactionDetails"][0]["fromAcctNickName"] : " ";
                frmIBTOASavingsCareExecutedTxn.lblNFAccBal.text = resulttable["ExecTransactionDetails"][0]["fromAcctId"] != undefined ? formatAccountNumber(resulttable["ExecTransactionDetails"][0]["fromAcctId"]) : " ";
                frmIBTOASavingsCareExecutedTxn.lblIBOpenAccCnfmBalAmt.text = resulttable["ExecTransactionDetails"][0]["balAfterPymnt"] != undefined ? resulttable["ExecTransactionDetails"][0]["balAfterPymnt"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht") : " ";
                frmIBTOASavingsCareExecutedTxn.lblOASCNicNamVal.text = resulttable["ExecTransactionDetails"][0]["toAccNickName"] != undefined ? resulttable["ExecTransactionDetails"][0]["toAccNickName"] : " ";
                frmIBTOASavingsCareExecutedTxn.lblOASCActNumVal.text = resulttable["ExecTransactionDetails"][0]["toAccId"] != undefined ? formatAccountNumber(resulttable["ExecTransactionDetails"][0]["toAccId"]) : " ";
                frmIBTOASavingsCareExecutedTxn.lblOASCActNameVal.text = resulttable["ExecTransactionDetails"][0]["toAccName"] != undefined ? resulttable["ExecTransactionDetails"][0]["toAccName"] : " ";
                gblProdNameEN = resulttable["openProductNameEN"];
                gblBranchNameEN = resulttable["ExecTransactionDetails"][0]["toAcctBranchNameEN"];
                gblProdNameTH = resulttable["openProductNameTH"];
                gblBranchNameTH = resulttable["ExecTransactionDetails"][0]["toAcctBranchNameTH"];
                if (kony.i18n.getCurrentLocale() == "en_US") {
                    frmIBTOASavingsCareExecutedTxn.lblOASCBranchVal.text = gblBranchNameEN;
                    frmIBTOASavingsCareExecutedTxn.lblOASCTitle.text = gblProdNameEN;
                } else {
                    frmIBTOASavingsCareExecutedTxn.lblOASCBranchVal.text = gblBranchNameTH;
                    frmIBTOASavingsCareExecutedTxn.lblOASCTitle.text = gblProdNameTH;
                }
                careDetails = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"];
                if (careDetails.length == 1) {
                    frmIBTOASavingsCareExecutedTxn.lblBefNameValCnfrm1.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][0]["careBeneficiaryName"];
                    frmIBTOASavingsCareExecutedTxn.lblBefRsCnfrmVal1.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][0]["careBeneficiaryRelation"];
                    frmIBTOASavingsCareExecutedTxn.lblBefPerCnfrmVal1.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][0]["careBeneficiaryPct"] + "" + "%";
                } else if (careDetails.length == 2) {
                    frmIBTOASavingsCareExecutedTxn.lblBefNameValCnfrm1.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][0]["careBeneficiaryName"];
                    frmIBTOASavingsCareExecutedTxn.lblBefRsCnfrmVal1.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][0]["careBeneficiaryRelation"];
                    frmIBTOASavingsCareExecutedTxn.lblBefPerCnfrmVal1.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][0]["careBeneficiaryPct"] + "" + "%";
                    frmIBTOASavingsCareExecutedTxn.lblBefNameValCnfrm2.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][1]["careBeneficiaryName"];
                    frmIBTOASavingsCareExecutedTxn.lblBefRsCnfrmVal2.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][1]["careBeneficiaryRelation"];
                    frmIBTOASavingsCareExecutedTxn.lblBefPerCnfrmVal2.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][1]["careBeneficiaryPct"] + "" + "%";
                    frmIBTOASavingsCareExecutedTxn.hbxBefNameCnfrm2.setVisibility(true);
                    frmIBTOASavingsCareExecutedTxn.hbxBefRsCnfrm2.setVisibility(true);
                    frmIBTOASavingsCareExecutedTxn.hbxBefPerCnfrm2.setVisibility(true);
                } else if (careDetails.length == 3) {
                    frmIBTOASavingsCareExecutedTxn.lblBefNameValCnfrm1.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][0]["careBeneficiaryName"];
                    frmIBTOASavingsCareExecutedTxn.lblBefRsCnfrmVal1.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][0]["careBeneficiaryRelation"];
                    frmIBTOASavingsCareExecutedTxn.lblBefPerCnfrmVal1.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][0]["careBeneficiaryPct"] + "" + "%";
                    frmIBTOASavingsCareExecutedTxn.lblBefNameValCnfrm2.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][1]["careBeneficiaryName"];
                    frmIBTOASavingsCareExecutedTxn.lblBefRsCnfrmVal2.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][1]["careBeneficiaryRelation"];
                    frmIBTOASavingsCareExecutedTxn.lblBefPerCnfrmVal2.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][1]["careBeneficiaryPct"] + "" + "%";
                    frmIBTOASavingsCareExecutedTxn.lblBefNameValCnfrm3.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][2]["careBeneficiaryName"];
                    frmIBTOASavingsCareExecutedTxn.lblBefRsCnfrmVal3.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][2]["careBeneficiaryRelation"];
                    frmIBTOASavingsCareExecutedTxn.lblBefPerCnfrmVal3.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][2]["careBeneficiaryPct"] + "" + "%";
                    frmIBTOASavingsCareExecutedTxn.hbxBefNameCnfrm2.setVisibility(true);
                    frmIBTOASavingsCareExecutedTxn.hbxBefRsCnfrm2.setVisibility(true);
                    frmIBTOASavingsCareExecutedTxn.hbxBefPerCnfrm2.setVisibility(true);
                    frmIBTOASavingsCareExecutedTxn.hbxBefNameCnfrm3.setVisibility(true);
                    frmIBTOASavingsCareExecutedTxn.hbxBefRsCnfrm3.setVisibility(true);
                    frmIBTOASavingsCareExecutedTxn.hbxBefPerCnfrm3.setVisibility(true);
                } else if (careDetails.length == 4) {
                    frmIBTOASavingsCareExecutedTxn.lblBefNameValCnfrm1.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][0]["careBeneficiaryName"];
                    frmIBTOASavingsCareExecutedTxn.lblBefRsCnfrmVal1.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][0]["careBeneficiaryRelation"];
                    frmIBTOASavingsCareExecutedTxn.lblBefPerCnfrmVal1.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][0]["careBeneficiaryPct"] + "" + "%";
                    frmIBTOASavingsCareExecutedTxn.lblBefNameValCnfrm2.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][1]["careBeneficiaryName"];
                    frmIBTOASavingsCareExecutedTxn.lblBefRsCnfrmVal2.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][1]["careBeneficiaryRelation"];
                    frmIBTOASavingsCareExecutedTxn.lblBefPerCnfrmVal2.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][1]["careBeneficiaryPct"] + "" + "%";
                    frmIBTOASavingsCareExecutedTxn.lblBefNameValCnfrm3.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][2]["careBeneficiaryName"];
                    frmIBTOASavingsCareExecutedTxn.lblBefRsCnfrmVal3.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][2]["careBeneficiaryRelation"];
                    frmIBTOASavingsCareExecutedTxn.lblBefPerCnfrmVal3.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][2]["careBeneficiaryPct"] + "" + "%";
                    frmIBTOASavingsCareExecutedTxn.lblBefNameValCnfrm4.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][3]["careBeneficiaryName"];
                    frmIBTOASavingsCareExecutedTxn.lblBefRsCnfrmVal4.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][3]["careBeneficiaryRelation"];
                    frmIBTOASavingsCareExecutedTxn.lblBefPerCnfrmVal4.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][3]["careBeneficiaryPct"] + "" + "%";
                    frmIBTOASavingsCareExecutedTxn.hbxBefNameCnfrm2.setVisibility(true);
                    frmIBTOASavingsCareExecutedTxn.hbxBefRsCnfrm2.setVisibility(true);
                    frmIBTOASavingsCareExecutedTxn.hbxBefPerCnfrm2.setVisibility(true);
                    frmIBTOASavingsCareExecutedTxn.hbxBefNameCnfrm3.setVisibility(true);
                    frmIBTOASavingsCareExecutedTxn.hbxBefRsCnfrm3.setVisibility(true);
                    frmIBTOASavingsCareExecutedTxn.hbxBefPerCnfrm3.setVisibility(true);
                    frmIBTOASavingsCareExecutedTxn.hbxBefNameCnfrm4.setVisibility(true);
                    frmIBTOASavingsCareExecutedTxn.hbxBefRsCnfrm4.setVisibility(true);
                    frmIBTOASavingsCareExecutedTxn.hbxBefPerCnfrm4.setVisibility(true);
                } else if (careDetails.length == 5) {
                    frmIBTOASavingsCareExecutedTxn.lblBefNameValCnfrm1.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][0]["careBeneficiaryName"];
                    frmIBTOASavingsCareExecutedTxn.lblBefRsCnfrmVal1.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][0]["careBeneficiaryRelation"];
                    frmIBTOASavingsCareExecutedTxn.lblBefPerCnfrmVal1.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][0]["careBeneficiaryPct"] + "" + "%";
                    frmIBTOASavingsCareExecutedTxn.lblBefNameValCnfrm2.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][1]["careBeneficiaryName"];
                    frmIBTOASavingsCareExecutedTxn.lblBefRsCnfrmVal2.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][1]["careBeneficiaryRelation"];
                    frmIBTOASavingsCareExecutedTxn.lblBefPerCnfrmVal2.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][1]["careBeneficiaryPct"] + "" + "%";
                    frmIBTOASavingsCareExecutedTxn.lblBefNameValCnfrm3.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][2]["careBeneficiaryName"];
                    frmIBTOASavingsCareExecutedTxn.lblBefRsCnfrmVal3.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][2]["careBeneficiaryRelation"];
                    frmIBTOASavingsCareExecutedTxn.lblBefPerCnfrmVal3.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][2]["careBeneficiaryPct"] + "" + "%";
                    frmIBTOASavingsCareExecutedTxn.lblBefNameValCnfrm4.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][3]["careBeneficiaryName"];
                    frmIBTOASavingsCareExecutedTxn.lblBefRsCnfrmVal4.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][3]["careBeneficiaryRelation"];
                    frmIBTOASavingsCareExecutedTxn.lblBefPerCnfrmVal4.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][3]["careBeneficiaryPct"] + "" + "%";
                    frmIBTOASavingsCareExecutedTxn.lblBefNameValCnfrm5.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][4]["careBeneficiaryName"];
                    frmIBTOASavingsCareExecutedTxn.lblBefRsCnfrmVal5.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][4]["careBeneficiaryRelation"];
                    frmIBTOASavingsCareExecutedTxn.lblBefPerCnfrmVal5.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][4]["careBeneficiaryPct"] + "" + "%";
                    frmIBTOASavingsCareExecutedTxn.hbxBefNameCnfrm2.setVisibility(true);
                    frmIBTOASavingsCareExecutedTxn.hbxBefRsCnfrm2.setVisibility(true);
                    frmIBTOASavingsCareExecutedTxn.hbxBefPerCnfrm2.setVisibility(true);
                    frmIBTOASavingsCareExecutedTxn.hbxBefNameCnfrm3.setVisibility(true);
                    frmIBTOASavingsCareExecutedTxn.hbxBefRsCnfrm3.setVisibility(true);
                    frmIBTOASavingsCareExecutedTxn.hbxBefPerCnfrm3.setVisibility(true);
                    frmIBTOASavingsCareExecutedTxn.hbxBefNameCnfrm4.setVisibility(true);
                    frmIBTOASavingsCareExecutedTxn.hbxBefRsCnfrm4.setVisibility(true);
                    frmIBTOASavingsCareExecutedTxn.hbxBefPerCnfrm4.setVisibility(true);
                    frmIBTOASavingsCareExecutedTxn.hbxBefNameCnfrm5.setVisibility(true);
                    frmIBTOASavingsCareExecutedTxn.hbxBefRsCnfrm5.setVisibility(true);
                    frmIBTOASavingsCareExecutedTxn.hbxBefPerCnfrm5.setVisibility(true);
                }
                frmIBTOASavingsCareExecutedTxn.lblOASCIntRateVal.text = resulttable["ExecTransactionDetails"][0]["TDInterestRate"] != undefined ? resulttable["ExecTransactionDetails"][0]["TDInterestRate"] + "" + "%" : " ";
                frmIBTOASavingsCareExecutedTxn.lblOASCOpenDateVal.text = resulttable["ExecTransactionDetails"][0]["transferDate"] != undefined ? resulttable["ExecTransactionDetails"][0]["transferDate"] : " ";
                frmIBTOASavingsCareExecutedTxn.imgFrom.src = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + resulttable["ICON_ID"] + "&modIdentifier=PRODICON";
                if (resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"].length > 0) {
                    frmIBTOASavingsCareExecutedTxn.lblOASCTranRefNoVal.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["transRefId"] != undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["transRefId"] : " ";
                    frmIBTOASavingsCareExecutedTxn.lblOASCOpenActVal.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["amount"] != undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["amount"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht") : " ";
                    resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["txnStatus"] == "01" ? frmIBTOASavingsCareExecutedTxn.imgComplete.src = "iconcomplete.png" : frmIBTOASavingsCareExecutedTxn.imgComplete.src = "icon_notcomplete.png";
                    resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["txnStatus"] == "02" ? setVisiblityBalanceTransfer(frmIBTOASavingsCareExecutedTxn.lblBalanceBefrTransfr, frmIBTOASavingsCareExecutedTxn.lblIBOpenAccCnfmBalAmt, false) : setVisiblityBalanceTransfer(frmIBTOASavingsCareExecutedTxn.lblBalanceBefrTransfr, frmIBTOASavingsCareExecutedTxn.lblIBOpenAccCnfmBalAmt, true);
                } else {
                    frmIBTOASavingsCareExecutedTxn.lblOASCTranRefNoVal.text = "";
                    frmIBTOASavingsCareExecutedTxn.lblOASCOpenActVal.text = "";
                }
                dismissLoadingScreenPopup();
                frmIBTOASavingsCareExecutedTxn.show();
            }
        }
    }
    dismissLoadingScreenPopup();
}

function getTDTxnDetCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (resulttable["ExecTransactionDetails"].length > 0) {
                frmIBTOATDExecutedTxn.imgTDCnfmTitle.src = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + resulttable["openICONID"] + "&modIdentifier=PRODICON";
                frmIBTOATDExecutedTxn.lblOATDNickNameVal.text = resulttable["ExecTransactionDetails"][0]["toAccNickName"] != undefined ? resulttable["ExecTransactionDetails"][0]["toAccNickName"] : "";
                frmIBTOATDExecutedTxn.lblAccNoVal.text = resulttable["ExecTransactionDetails"][0]["toAccId"] != undefined ? formatAccountNumber(resulttable["ExecTransactionDetails"][0]["toAccId"]) : "";
                frmIBTOATDExecutedTxn.lblOpeningAmountValue.text = resulttable["ExecTransactionDetails"][0]["toAccName"] != undefined ? resulttable["ExecTransactionDetails"][0]["toAccName"] : "";
                frmIBTOATDExecutedTxn.lblAccNSName.text = resulttable["ExecTransactionDetails"][0]["fromAcctNickName"] != undefined ? resulttable["ExecTransactionDetails"][0]["fromAcctNickName"] : "";
                frmIBTOATDExecutedTxn.lblIBOpenAccCnfmBalAmt.text = resulttable["ExecTransactionDetails"][0]["balAfterPymnt"] != undefined ? resulttable["ExecTransactionDetails"][0]["balAfterPymnt"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht") : "";
                frmIBTOATDExecutedTxn.lblAccountBalance.text = resulttable["ExecTransactionDetails"][0]["fromAcctId"] != undefined ? formatAccountNumber(resulttable["ExecTransactionDetails"][0]["fromAcctId"]) : "";
                gblProdNameEN = resulttable["openProductNameEN"];
                gblBranchNameEN = resulttable["ExecTransactionDetails"][0]["toAcctBranchNameEN"];
                gblProdNameTH = resulttable["openProductNameTH"];
                gblBranchNameTH = resulttable["ExecTransactionDetails"][0]["toAcctBranchNameTH"];
                if (kony.i18n.getCurrentLocale() == "en_US") {
                    frmIBTOATDExecutedTxn.lblType.text = gblProdNameEN;
                    frmIBTOATDExecutedTxn.lblBranchVal.text = gblBranchNameEN;
                    frmIBTOATDExecutedTxn.lblTDCnfmTitle.text = gblProdNameEN;
                } else {
                    frmIBTOATDExecutedTxn.lblType.text = gblProdNameTH;
                    frmIBTOATDExecutedTxn.lblBranchVal.text = gblBranchNameTH;
                    frmIBTOATDExecutedTxn.lblTDCnfmTitle.text = gblProdNameTH;
                }
                frmIBTOATDExecutedTxn.lblTermval.text = resulttable["ExecTransactionDetails"][0]["TDTerm"] != undefined ? resulttable["ExecTransactionDetails"][0]["TDTerm"] : "";
                frmIBTOATDExecutedTxn.lblInterestVal.text = resulttable["ExecTransactionDetails"][0]["TDInterestRate"] != undefined ? resulttable["ExecTransactionDetails"][0]["TDInterestRate"] + "" + "%" : "";
                frmIBTOATDExecutedTxn.lblMaturityDateVal.text = resulttable["ExecTransactionDetails"][0]["TDMaturityDate"] != undefined ? resulttable["ExecTransactionDetails"][0]["TDMaturityDate"] : "";
                frmIBTOATDExecutedTxn.lblOpeningDateVal.text = resulttable["ExecTransactionDetails"][0]["transferDate"] != undefined ? resulttable["ExecTransactionDetails"][0]["transferDate"] : " ";
                frmIBTOATDExecutedTxn.imgFrom.src = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + resulttable["ICON_ID"] + "&modIdentifier=PRODICON";
                if (resulttable["ExecTransactionDetails"][0]["openPrdCode"] == '300' || resulttable["ExecTransactionDetails"][0]["openPrdCode"] == '301' || resulttable["ExecTransactionDetails"][0]["openPrdCode"] == '302') {
                    frmIBTOATDExecutedTxn.lblPayingToAccount.text = resulttable["ExecTransactionDetails"][0]["affliatedAcctId"] != undefined ? formatAccountNumber(resulttable["ExecTransactionDetails"][0]["affliatedAcctId"]) : "";
                    frmIBTOATDExecutedTxn.lblPayingToName.text = resulttable["ExecTransactionDetails"][0]["affliatedAcctNickName"] != undefined ? resulttable["ExecTransactionDetails"][0]["affliatedAcctNickName"] : "";
                    frmIBTOATDExecutedTxn.hbxInterest.setVisibility(false);
                } else {
                    frmIBTOATDExecutedTxn.imgAffliated.src = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + resulttable["AFFLIATED_ICON_ID"] + "&modIdentifier=PRODICON";
                    frmIBTOATDExecutedTxn.lblPayingToAccount.text = resulttable["ExecTransactionDetails"][0]["affliatedAcctId"] != undefined ? formatAccountNumber(resulttable["ExecTransactionDetails"][0]["affliatedAcctId"]) : "";
                    frmIBTOATDExecutedTxn.lblPayingToName.text = resulttable["ExecTransactionDetails"][0]["affliatedAcctNickName"] != undefined ? resulttable["ExecTransactionDetails"][0]["affliatedAcctNickName"] : "";
                    frmIBTOATDExecutedTxn.hbxInterest.setVisibility(true);
                }
                if (resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"].length > 0) {
                    frmIBTOATDExecutedTxn.lblTdTransRefVal.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["transRefId"] != undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["transRefId"] : " ";
                    resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["txnStatus"] == "01" ? frmIBTOATDExecutedTxn.imgComplete.src = "iconcomplete.png" : frmIBTOATDExecutedTxn.imgComplete.src = "icon_notcomplete.png";
                    resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["txnStatus"] == "02" ? setVisiblityBalanceTransfer(frmIBTOATDExecutedTxn.lblBalanceBefrTransfr, frmIBTOATDExecutedTxn.lblIBOpenAccCnfmBalAmt, false) : setVisiblityBalanceTransfer(frmIBTOATDExecutedTxn.lblBalanceBefrTransfr, frmIBTOATDExecutedTxn.lblIBOpenAccCnfmBalAmt, true);
                    frmIBTOATDExecutedTxn.lblOATDAmtVal.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["amount"] != undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["amount"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht") : " ";
                } else {
                    frmIBTOATDExecutedTxn.lblTdTransRefVal.text = "";
                    frmIBTOATDExecutedTxn.lblOATDAmtVal.text = "";
                }
                frmIBTOATDExecutedTxn.show();
                dismissLoadingScreenPopup();
            }
        }
    }
}

function getDSTxnDetCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (resulttable["ExecTransactionDetails"].length > 0) {
                frmIBTOADreamExecutedTxn.lblNickNameVal.text = resulttable["ExecTransactionDetails"][0]["toAccNickName"] != undefined ? resulttable["ExecTransactionDetails"][0]["toAccNickName"] : "";
                frmIBTOADreamExecutedTxn.lblOASCActNumVal.text = resulttable["ExecTransactionDetails"][0]["toAccId"] != undefined ? resulttable["ExecTransactionDetails"][0]["toAccId"] : "";
                frmIBTOADreamExecutedTxn.lblNFAccName.text = resulttable["ExecTransactionDetails"][0]["fromAcctNickName"] != undefined ? resulttable["ExecTransactionDetails"][0]["fromAcctNickName"] : "";
                frmIBTOADreamExecutedTxn.lblIBOpenAccCnfmBalAmt.text = resulttable["ExecTransactionDetails"][0]["balAfterPymnt"] != undefined ? resulttable["ExecTransactionDetails"][0]["balAfterPymnt"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht") : "";
                frmIBTOADreamExecutedTxn.lblNFAccBal.text = resulttable["ExecTransactionDetails"][0]["fromAcctId"] != undefined ? resulttable["ExecTransactionDetails"][0]["fromAcctId"] : "";
                gblProdNameEN = resulttable["openProductNameEN"];
                gblBranchNameEN = resulttable["ExecTransactionDetails"][0]["toAcctBranchNameEN"];
                gblProdNameTH = resulttable["openProductNameTH"];
                gblBranchNameTH = resulttable["ExecTransactionDetails"][0]["toAcctBranchNameTH"];
                if (kony.i18n.getCurrentLocale() == "en_US") {
                    frmIBTOADreamExecutedTxn.lblDSAckTitle.text = gblProdNameEN;
                    frmIBTOADreamExecutedTxn.lblBranchVal.text = gblBranchNameEN;
                } else {
                    frmIBTOADreamExecutedTxn.lblDSAckTitle.text = gblProdNameTH;
                    frmIBTOADreamExecutedTxn.lblBranchVal.text = gblBranchNameTH;
                }
                frmIBTOADreamExecutedTxn.lblInterestRateVal.text = resulttable["ExecTransactionDetails"][0]["TDInterestRate"] != undefined ? resulttable["ExecTransactionDetails"][0]["TDInterestRate"] : "";
                frmIBTOADreamExecutedTxn.lblOpeningDateVal.text = resulttable["ExecTransactionDetails"][0]["transferDate"] != undefined ? resulttable["ExecTransactionDetails"][0]["transferDate"] : " ";
                frmIBTOADreamExecutedTxn.lblTrgtAmt.text = resulttable["ExecTransactionDetails"][0]["DreamDesc"] != undefined ? resulttable["ExecTransactionDetails"][0]["DreamDesc"] : "";
                frmIBTOADreamExecutedTxn.lblOADreamDetailTarAmtVal.text = resulttable["ExecTransactionDetails"][0]["DreamTargetAmount"] != undefined ? resulttable["ExecTransactionDetails"][0]["DreamTargetAmount"] : " ";
                if (resulttable["ExecTransactionDetails"][0]["DreamTargetDescEn"] != undefined) {
                    if (resulttable["ExecTransactionDetails"][0]["DreamTargetDescEn"] == "My Vacation") {
                        dreamimage = "prod_vacation.png";
                    } else if (resulttable["ExecTransactionDetails"][0]["DreamTargetDescEn"] == "My Dream") {
                        dreamimage = "prod_dream.png";
                    } else if (resulttable["ExecTransactionDetails"][0]["DreamTargetDescEn"] == "My Car") {
                        dreamimage = "prod_car.png";
                    } else if (resulttable["ExecTransactionDetails"][0]["DreamTargetDescEn"] == "My Education") {
                        dreamimage = "prod_education.png";
                    } else if (resulttable["ExecTransactionDetails"][0]["DreamTargetDescEn"] == "My Own House") {
                        dreamimage = "prod_homeloan.png";
                    } else if (resulttable["ExecTransactionDetails"][0]["DreamTargetDescEn"] == "My Business") {
                        dreamimage = "prod_business.png";
                    } else if (resulttable["ExecTransactionDetails"][0]["DreamTargetDescEn"] == "My Saving") {
                        dreamimage = "prod_saving.png";
                    }
                    frmIBTOADreamExecutedTxn.imgOADreamDetail.src = dreamimage;
                }
                frmIBTOADreamExecutedTxn.imgDSAckTitle.src = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + resulttable["ICON_ID"] + "&modIdentifier=PRODICON";
                frmIBTOADreamExecutedTxn.imgFrom.src = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + resulttable["ICON_ID"] + "&modIdentifier=PRODICON";
                if (resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"].length > 0) {
                    frmIBTOADreamExecutedTxn.lbltransactionVal.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["transRefId"] != undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["transRefId"] : " ";
                    resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["txnStatus"] == "01" ? frmIBTOADreamExecutedTxn.imgComplete.src = "iconcomplete.png" : frmIBTOADreamExecutedTxn.imgComplete.src = "icon_notcomplete.png";
                    resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["txnStatus"] == "02" ? setVisiblityBalanceTransfer(frmIBTOADreamExecutedTxn.lblBalanceBefrTransfr, frmIBTOADreamExecutedTxn.lblIBOpenAccCnfmBalAmt, false) : setVisiblityBalanceTransfer(frmIBTOADreamExecutedTxn.lblBalanceBefrTransfr, frmIBTOADreamExecutedTxn.lblIBOpenAccCnfmBalAmt, true);
                    frmIBTOADreamExecutedTxn.lblOpenAmtVal.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["amount"] != undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["amount"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht") : " ";
                } else {
                    frmIBTOADreamExecutedTxn.lbltransactionVal.text = "";
                    frmIBTOADreamExecutedTxn.lblOpenAmtVal.text = "";
                }
                frmIBTOADreamExecutedTxn.show();
                dismissLoadingScreenPopup();
            }
        }
    }
}

function showRefValues(formName) {
    if (kony.i18n.getCurrentLocale() == "en_US") {
        formName.lblRef1.text = appendColon(gblExecRef1ENVal);
        if (gblExecRef2Req == "Y" && formName.id != "frmIBTopUpExecutedTxn") {
            formName.lblRef2.text = gblExecRef2ENVal;
            formName.hbxRef2.setVisibility(true);
        } else if (gblExecRef2Req == "N") {
            if (formName.id != "frmIBTopUpExecutedTxn") formName.hbxRef2.setVisibility(false);
        }
        if (formName.id == "frmIBePaymentExecutedTxn") {
            formName.lblFlexiParam1.text = gblExecFlex1ENVal;
            formName.lblFlexiParam2.text = gblExecFlex2ENVal;
            formName.lblFlexiParam3.text = gblExecFlex3ENVal;
        }
    } else if (kony.i18n.getCurrentLocale() == "th_TH") {
        formName.lblRef1.text = appendColon(gblExecRef1THVal);
        if (gblExecRef2Req == "Y" && formName.id != "frmIBTopUpExecutedTxn") {
            formName.lblRef2.text = gblExecRef2THVal;
            formName.hbxRef2.setVisibility(true);
        } else if (gblExecRef2Req == "N") {
            if (formName.id != "frmIBTopUpExecutedTxn") formName.hbxRef2.setVisibility(false);
        }
        if (formName.id == "frmIBePaymentExecutedTxn") {
            formName.lblFlexiParam1.text = gblExecFlex1THVal;
            formName.lblFlexiParam2.text = gblExecFlex2THVal;
            formName.lblFlexiParam3.text = gblExecFlex3THVal;
        }
    }
}

function onLocaleChange() {
    localeChange = "0";
    columnHeaderInDGHistory();
    columnHeaderInDGFutureTrans();
    if (LinkMyActivities == 0) {
        if (gsFormId.id == "frmIBPostLoginDashboard") gsFormId.lblMyactivitiesHdr.text = kony.i18n.getLocalizedString("MyActivitiesIB");
        else gsFormId.lblactivity.text = kony.i18n.getLocalizedString("MyActivitiesIB");
    } else {
        if (frmIBAccntSummary.lblAccountNameHeader.text != "") {
            gsFormId.lblactivity.text = kony.i18n.getLocalizedString("MyActivitiesIB") + "-" + frmIBAccntSummary.lblAccountNameHeader.text;
            gsFormId.lblMyActivities.text = kony.i18n.getLocalizedString("MyActivitiesIB") + "-" + frmIBAccntSummary.lblAccountNameHeader.text;
        } else {
            gsFormId.lblactivity.text = kony.i18n.getLocalizedString("MyActivitiesIB");
            gsFormId.lblMyActivities.text = kony.i18n.getLocalizedString("MyActivitiesIB");
        }
    }
    if (gsFormId.id != "frmIBPostLoginDashboard") {
        loadsegdataInFutureTrans(currentpagefuture);
        currentpage = gsFormId.link1.text;
        gsFormId.datagridhistory.data = frmIBMyActivities_History_temp;
        frmIBMyActivities_History_temp.length > 0 ? loadPagenumbers(Totalpages, gblCurrentPage) : loadPagenumbers(1, 1);
        gsFormId.combotrans.selectedKey != null ? storedTxnKey = gsFormId.combotrans.selectedKey : storedTxnKey = "";
        gsFormId.combosub.selectedKey != null ? storedSubTxnKey = gsFormId.combosub.selectedKey : storedSubTxnKey = "";
        gsFormId.combobank.selectedKey != null ? storedBankKey = gsFormId.combobank.selectedKey : storedBankKey = "";
        gsFormId.combotrans.masterData = null;
        if (gsFormId.id == "frmIBDateSlider") getTransactionType();
    }
}

function onReturnBtnClick() {
    IBMyActivitiesShowCalendar();
}

function onLinkMyActivitiesClick() {
    showLoadingScreenPopup();
    LinkMyActivities = 1;
    clearCalendarData();
    if (gblAccountTable["custAcctRec"]) {
        //var nickName = gblAccountTable["custAcctRec"][gblIndex]["acctNickName"] != undefined ? gblAccountTable["custAcctRec"][gblIndex]["acctNickName"] : "";
        if (frmIBAccntSummary.lblAccountNameHeader.text != "") {
            frmIBMyActivities.lblactivity.text = kony.i18n.getLocalizedString("MyActivitiesIB") + "-" + frmIBAccntSummary.lblAccountNameHeader.text;
            frmIBMyActivities.lblMyActivities.text = kony.i18n.getLocalizedString("MyActivitiesIB") + "-" + frmIBAccntSummary.lblAccountNameHeader.text;
            frmIBDateSlider.lblactivity.text = kony.i18n.getLocalizedString("MyActivitiesIB") + "-" + frmIBAccntSummary.lblAccountNameHeader.text;
            frmIBDateSlider.lblMyActivities.text = kony.i18n.getLocalizedString("MyActivitiesIB") + "-" + frmIBAccntSummary.lblAccountNameHeader.text;
        }
    }
    clearDataOnLaunch();
    frmIBMyActivities.show();
    if (window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8") langspecificmenuIE8();
    else langspecificmenu();
    frmIBMyActivities.segMenuOptions.removeAll();
    if (kony.i18n.getCurrentLocale() == "th_TH") {
        frmIBMyActivities.btnMenuMyActivities.skin = "btnIBMenuMyActivitiesFocusThai";
    } else {
        frmIBMyActivities.btnMenuMyActivities.skin = "btnIBMenuMyActivitiesFocus";
    }
    btncalenderOnClick();
}

function onMenuMyActivitiesClick() {
    LinkMyActivities = 0;
    clearCalendarData();
    frmIBMyActivities.lblactivity.text = kony.i18n.getLocalizedString("MyActivitiesIB");
    frmIBMyActivities.lblMyActivities.text = kony.i18n.getLocalizedString("MyActivitiesIB");
    frmIBDateSlider.lblactivity.text = kony.i18n.getLocalizedString("MyActivitiesIB");
    gsFormId = frmIBMyActivities;
    clearDataOnLaunch();
    frmIBMyActivities.show();
    if (window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8") langspecificmenuIE8();
    else langspecificmenu();
    frmIBMyActivities.segMenuOptions.removeAll();
    if (kony.i18n.getCurrentLocale() == "th_TH") {
        frmIBMyActivities.btnMenuMyActivities.skin = "btnIBMenuMyActivitiesFocusThai";
    } else {
        frmIBMyActivities.btnMenuMyActivities.skin = "btnIBMenuMyActivitiesFocus";
    }
    btncalenderOnClick();
}

function clearDataOnLaunch() {
    gblTxnHistDate1 = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() - 30);
    gblTxnHistDate2 = new Date();
    frmDt = dateFormatChangeTxn(new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() - 30))
    toDt = dateFormatChangeTxn(new Date());
    if (gsFormId.combotrans != null) gsFormId.combotrans.selectedKey = "";
    if (gsFormId.combosub != null) gsFormId.combosub.selectedKey = "";
    if (gsFormId.combobank != null) gsFormId.combobank.selectedKey = "";
    if (gsFormId.vbxcombo2 != null) gsFormId.vbxcombo2.setVisibility(false);
    if (gsFormId.vbxcombo3 != null) gsFormId.vbxcombo3.setVisibility(false);
    if (gsFormId.datagridhistory != null) gsFormId.datagridhistory.removeAll();
    gblCurrentPage = 1;
    gblIsPrevLink = false;
    gblIsNextLink = false;
    if (gsFormId.lblactivity != null) gsFormId.lblactivity.setVisibility(true);
    if (gsFormId.hbxBtns != null) gsFormId.hbxBtns.setVisibility(true);
    if (gsFormId.hbxhistory != null) gsFormId.hbxhistory.setVisibility(true);
    if (gsFormId.hbxfuture != null) gsFormId.hbxfuture.setVisibility(false);
    if (gsFormId.hbxbottom != null) gsFormId.hbxbottom.setVisibility(true);
    if (gsFormId.hbxbottomfuture != null) gsFormId.hbxbottomfuture.setVisibility(false);
    if (gsFormId.hbxCalendarContainer != null) gsFormId.hbxCalendarContainer.setVisibility(false);
    //gsFormId.vboxCalDayView.containerWeight = 0;
    if (gsFormId.vboxCalDayView != null) gsFormId.vboxCalDayView.setVisibility(false);
    if (gsFormId.btnfuture != null) gsFormId.btnfuture.skin = btnIBrndrightgrey;
    if (gsFormId.btnhistory != null) gsFormId.btnhistory.skin = btnIBrndleftblue;
    if (gsFormId.btncalender != null) gsFormId.btncalender.skin = btnIBTabgrey;
    if (gsFormId.btnfuture != null) gsFormId.btnfuture.focusSkin = btnIBrndrightgrey;
    if (gsFormId.btnhistory != null) gsFormId.btnhistory.focusSkin = btnIBrndleftblue;
    if (gsFormId.btncalender != null) gsFormId.btncalender.focusSkin = btnIBTabgrey;
    if (gsFormId.link1 != null) gsFormId.link1.skin = lnkNormal;
    if (gsFormId.link2 != null) gsFormId.link2.skin = lnkIB18pxBlueUnderline;
    if (gsFormId.link3 != null) gsFormId.link3.skin = lnkIB18pxBlueUnderline;
    if (gsFormId.link4 != null) gsFormId.link4.skin = lnkIB18pxBlueUnderline;
    if (gsFormId.link5 != null) gsFormId.link5.skin = lnkIB18pxBlueUnderline;
    gblSortBy = "txnDate";
    gblSortOrder = 'DESC';
    currentpage = 1;
    loadPagenumbers(1, 1);
}

function onSortColumn(columnid) {
    frmIBMyActivities.datagridhistory.columnHeadersConfig[0].columnheadertemplate.data.btnsort.skin = "btnsortby";
    frmIBMyActivities.datagridhistory.columnHeadersConfig[1].columnheadertemplate.data.btnsort.skin = "btnsortby";
    frmIBMyActivities.datagridhistory.columnHeadersConfig[2].columnheadertemplate.data.btnsort.skin = "btnsortby";
    frmIBMyActivities.datagridhistory.columnHeadersConfig[3].columnheadertemplate.data.btnsort.skin = "btnsortby";
    frmIBMyActivities.datagridhistory.columnHeadersConfig[4].columnheadertemplate.data.btnsort.skin = "btnsortby";
    frmIBMyActivities.datagridhistory.columnHeadersConfig[5].columnheadertemplate.data.btnsort.skin = "btnsortby";
    //frmIBMyActivities.datagridhistory.columnHeadersConfig[6].columnheadertemplate.data.btnsort.skin = "btnsortby";
    if (columnid == "column1") {
        if (gblSortBy == "txnDate" && gblSortOrder == 'DESC') {
            gblSortOrder = 'ASC';
            frmIBMyActivities.datagridhistory.columnHeadersConfig[0].columnheadertemplate.data.btnsort.skin = "btnSortUpIB";
        } else {
            gblSortBy = "txnDate";
            gblSortOrder = 'DESC';
            frmIBMyActivities.datagridhistory.columnHeadersConfig[0].columnheadertemplate.data.btnsort.skin = "btnsortby";
        }
    } else if (columnid == "column2") {
        if (gblSortBy == "transType" && gblSortOrder == 'ASC') {
            gblSortOrder = 'DESC';
            frmIBMyActivities.datagridhistory.columnHeadersConfig[1].columnheadertemplate.data.btnsort.skin = "btnsortby";
        } else {
            gblSortBy = "transType";
            gblSortOrder = 'ASC';
            frmIBMyActivities.datagridhistory.columnHeadersConfig[1].columnheadertemplate.data.btnsort.skin = "btnSortUpIB";
        }
    } else if (columnid == "column3") {
        if (gblSortBy == "frmAccName" && gblSortOrder == 'ASC') {
            gblSortOrder = 'DESC';
            frmIBMyActivities.datagridhistory.columnHeadersConfig[2].columnheadertemplate.data.btnsort.skin = "btnsortby";
        } else {
            gblSortBy = "frmAccName";
            gblSortOrder = 'ASC';
            frmIBMyActivities.datagridhistory.columnHeadersConfig[2].columnheadertemplate.data.btnsort.skin = "btnSortUpIB";
        }
    } else if (columnid == "column4") {
        if (gblSortBy == "toAccName" && gblSortOrder == 'ASC') {
            gblSortOrder = 'DESC';
            frmIBMyActivities.datagridhistory.columnHeadersConfig[3].columnheadertemplate.data.btnsort.skin = "btnsortby";
        } else {
            gblSortBy = "toAccName";
            gblSortOrder = 'ASC';
            frmIBMyActivities.datagridhistory.columnHeadersConfig[3].columnheadertemplate.data.btnsort.skin = "btnSortUpIB";
        }
    } else if (columnid == "column5") {
        if (gblSortBy == "amnt" && gblSortOrder == 'ASC') {
            gblSortOrder = 'DESC';
            frmIBMyActivities.datagridhistory.columnHeadersConfig[4].columnheadertemplate.data.btnsort.skin = "btnsortby";
        } else {
            gblSortBy = "amnt";
            gblSortOrder = 'ASC';
            frmIBMyActivities.datagridhistory.columnHeadersConfig[4].columnheadertemplate.data.btnsort.skin = "btnSortUpIB";
        }
    } else if (columnid == "column6") {
        if (gblSortBy == "channelDesc" && gblSortOrder == 'ASC') {
            gblSortOrder = 'DESC';
            frmIBMyActivities.datagridhistory.columnHeadersConfig[5].columnheadertemplate.data.btnsort.skin = "btnsortby";
        } else {
            gblSortBy = "channelDesc";
            gblSortOrder = 'ASC';
            frmIBMyActivities.datagridhistory.columnHeadersConfig[5].columnheadertemplate.data.btnsort.skin = "btnSortUpIB";
        }
    } else if (columnid == "column7") {
        if (gblSortBy == "reason" && gblSortOrder == 'ASC') {
            gblSortOrder = 'DESC';
            frmIBMyActivities.datagridhistory.columnHeadersConfig[6].columnheadertemplate.data.btnsort.skin = "btnsortby";
        } else {
            gblSortBy = "reason";
            gblSortOrder = 'ASC';
            frmIBMyActivities.datagridhistory.columnHeadersConfig[6].columnheadertemplate.data.btnsort.skin = "btnSortUpIB";
        }
    }
    clearData();
    getTransactionHistory();
}

function getFutureInstructionDetails() {
    //begin MIB-2796
    var channelID = frmIBMyActivities.datagridFT.data[frmIBMyActivities.datagridFT.selectedIndex]["column6"]["lblChannelID"].text;
    if (channelID == 'IB' || channelID == 'MB') {
        //end MIB-2796
        var schedID = frmIBMyActivities.datagridFT.data[frmIBMyActivities.datagridFT.selectedIndex]["column6"]["lblno"].text;
        gblActivitiesNvgn = "F";
        if (kony.string.startsWith(schedID, "SB", true)) {
            callPaymentInqServiceForEditBP(schedID);
        } else if (kony.string.startsWith(schedID, "SU", true)) {
            callPaymentInqServiceForEditTP(schedID);
        } else if (kony.string.startsWith(schedID, "ST", true)) {
            showLoadingScreenPopup();
            frmIBFTrnsrView.hbxFTEditAmnt.setVisibility(false);
            frmIBFTrnsrView.hbxFTVeiwAmnt.setVisibility(true);
            frmIBFTrnsrView.lblFTEditHdr.setVisibility(false);
            frmIBFTrnsrView.btnFTEdit.setVisibility(false);
            frmIBFTrnsrView.hbxFTViewHdr.setVisibility(true);
            frmIBFTrnsrView.btnReturnView.setVisibility(true)
            frmIBFTrnsrView.hbxEditBtns.setVisibility(false);
            //frmIBFTrnsrView.txtFTEditAmountval.setFocus(false);
            frmIBFTrnsrView.hbxFTAfterEditAvilBal.setVisibility(false);
            var inputparam = {};
            gblSchduleRefIDFT = frmIBMyActivities.datagridFT.data[frmIBMyActivities.datagridFT.selectedIndex]["column6"]["lblno"].text;
            gblVarEditFTIB();
        } else {
            alert("Not a valid schedule Ref Id");
        }
    }
}
/**************************************************************************************
		Module	: savePDFTransferIB
		Author  : Kony
		Date    : May 05, 2013
		Purpose : Method for calling generateTransferPDF
*****************************************************************************************/
//TEST SPLIT CASE
function savePDFExecTransferIB(filetype) {
    var inputParam = {};
    var TrasferJsondata = {};
    for (var i = 0; i < gblXferSplit.length; i++) {
        if (kony.string.endsWith(gblXferSplit[i]["fee"], kony.i18n.getLocalizedString("currencyThaiBaht"))) {
            gblXferSplit[i]["fee"] = gblXferSplit[i]["fee"]
        } else {
            gblXferSplit[i]["fee"] = gblXferSplit[i]["fee"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
        }
        if (kony.string.endsWith(gblXferSplit[i]["amount"], kony.i18n.getLocalizedString("currencyThaiBaht"))) {
            gblXferSplit[i]["amount"] = gblXferSplit[i]["amount"]
        } else {
            gblXferSplit[i]["amount"] = gblXferSplit[i]["amount"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
        }
    }
    TrasferJsondata = {
        "fromAcctNo": "xxx-x-" + frmIBExecutedTransaction.lblAccountNo.text.substring(6, 11) + "-x",
        "fromAcctName": frmIBExecutedTransaction.lblAcctName.text,
        "toAcctNo": frmIBExecutedTransaction.lblXferAccNo.text,
        "toAcctName": frmIBExecutedTransaction.lblXferToName.text,
        "bankName": frmIBExecutedTransaction.lblBankValue.text,
        "totalAmount": frmIBExecutedTransaction.lblSplitTotAmtVal.text,
        "totalFee": frmIBExecutedTransaction.lblSplitTotFeeVal.text,
        "transferOrderDate": frmIBExecutedTransaction.lblSplitXferDtVal.text.split(" ")[0],
        "splitResult": gblXferSplit,
        "myNote": frmIBExecutedTransaction.lblMyNoteVal.text,
        "notifyVia": frmIBExecutedTransaction.lblNotifyRecipientVal.text,
        "noteToRecipient": frmIBExecutedTransaction.lblSplitNTRVal.text,
        "localeId": kony.i18n.getCurrentLocale()
    }
    inputParam["outputtemplatename"] = "Transfers_Details_" + kony.os.date("DDMMYYYY");
    inputParam["filetype"] = filetype;
    inputParam["templatename"] = "ExecutedTransferComplete";
    inputParam["datajson"] = JSON.stringify(TrasferJsondata, "", "");
    var transRefId = frmIBExecutedTransaction.lblTRNVal.text;
    if (frmIBExecutedTransaction.hbxSplitXfer.isVisible == true) {
        transRefId = frmIBExecutedTransaction.segSplitDet.data[0].lblSplitTRNVal;
    } else {
        transRefId = frmIBExecutedTransaction.lblTRNVal.text;
    }
    //  if(kony.string.startsWith("N", transRefId.substring(0, 1))) {
    savePDFIB(filetype, gblActivityIds, transRefId);
    /*  } else {
			invokeServiceSecureAsync("generatePdfImage", inputParam, ibRenderFileCallbackfunction);   
	   }
    */
}

function savePDFExecTopUpPaymentIB(filetype) {
    var inputParam = {};
    var TopUpJsondata = {};
    TopUpJsondata = {
        "AccountNo": frmIBTopUpExecutedTxn.lblAccountNo.text,
        "AccountName": frmIBTopUpExecutedTxn.lblFromName.text,
        "BillerName": frmIBTopUpExecutedTxn.lblePayNickname.text,
        "Ref1Label": frmIBTopUpExecutedTxn.lblRef1.text,
        "Ref1Value": frmIBTopUpExecutedTxn.lblRef1Value.text,
        "Amount": frmIBTopUpExecutedTxn.lblAmtVal.text,
        "Fee": frmIBTopUpExecutedTxn.lblFeeVal.text,
        "PaymentOrderDate": frmIBTopUpExecutedTxn.lblePayDate.text,
        "MyNote": frmIBTopUpExecutedTxn.lblMNVal.text,
        "TransactionRefNo": frmIBTopUpExecutedTxn.lblTRNVal.text,
        "localeId": kony.i18n.getCurrentLocale()
    }
    inputParam["outputtemplatename"] = "Top-Up_Details_" + kony.os.date("DDMMYYYY");
    inputParam["filetype"] = filetype;
    inputParam["templatename"] = "ExecutedTopUpPaymentComplete";
    inputParam["datajson"] = JSON.stringify(TopUpJsondata, "", "");
    //  if(kony.string.startsWith("N", frmIBTopUpExecutedTxn.lblTRNVal.text.substring(0, 1))) {
    savePDFIB(filetype, gblActivityIds, frmIBTopUpExecutedTxn.lblTRNVal.text);
    /*} else {
    	invokeServiceSecureAsync("generatePdfImage", inputParam, ibRenderFileCallbackfunction);
    }*/
}

function savePDFExecBillPaymentIB(filetype) {
    var inputParam = {};
    var billPaymentJsondata = {};
    billPaymentJsondata = {
        "AccountNo": frmIBBillPaymentExecutedTxn.lblAccountNo.text,
        "AccountName": frmIBBillPaymentExecutedTxn.lblFromName.text,
        "BillerName": frmIBBillPaymentExecutedTxn.lblePayNickname.text,
        "Ref1Label": frmIBBillPaymentExecutedTxn.lblRef1.text,
        "Ref1Value": frmIBBillPaymentExecutedTxn.lblRef1Value.text,
        "Ref2Label": frmIBBillPaymentExecutedTxn.lblRef2.text,
        "Ref2Value": frmIBBillPaymentExecutedTxn.lblRef2Value.text,
        "Amount": frmIBBillPaymentExecutedTxn.lblPaymentAmountValue.text,
        "Fee": frmIBBillPaymentExecutedTxn.lblFeeVal.text,
        "PaymentOrderDate": frmIBBillPaymentExecutedTxn.lblePayDate.text,
        "MyNote": frmIBBillPaymentExecutedTxn.lblMNVal.text,
        "TransactionRefNo": frmIBBillPaymentExecutedTxn.lblTRNVal.text,
        "localeId": kony.i18n.getCurrentLocale()
    }
    inputParam["outputtemplatename"] = "Bill_Payment_Details_" + kony.os.date("DDMMYYYY");
    inputParam["filetype"] = filetype;
    inputParam["templatename"] = "ExecutedBillPaymentTemplate";
    inputParam["datajson"] = JSON.stringify(billPaymentJsondata, "", "");
    // if(kony.string.startsWith("N", frmIBBillPaymentExecutedTxn.lblTRNVal.text.substring(0, 1))) {
    savePDFIB(filetype, gblActivityIds, frmIBBillPaymentExecutedTxn.lblTRNVal.text);
    /*} else {
    	invokeServiceSecureAsync("generatePdfImage", inputParam, ibRenderFileCallbackfunction);
    }*/
}

function savePDFTxnHistory(filetype, templatename) {
    getTransactionHistory('forsave', filetype, templatename);
}

function savePDFExecePaymentIB(filetype) {
    var inputParam = {};
    var ePaymentJsondata = {};
    ePaymentJsondata = {
        "accountNumber": frmIBePaymentExecutedTxn.lblAccountNo.text,
        "accountName": frmIBePaymentExecutedTxn.lblAcctName.text,
        "billerName": frmIBePaymentExecutedTxn.lblePayNickname.text,
        "ref1LabelEN": gblExecRef1ENVal,
        "ref1LabelTH": gblExecRef1THVal,
        "ref1Value": frmIBePaymentExecutedTxn.lblePayRef1Value.text,
        "ref2LabelEN": gblExecRef2ENVal,
        "ref2LabelTH": gblExecRef2THVal,
        "ref2Value": frmIBePaymentExecutedTxn.lblePayRef2Value.text,
        "amount": frmIBePaymentExecutedTxn.lblAmtVal.text,
        "fee": frmIBePaymentExecutedTxn.lblFeeVal.text,
        "date": frmIBePaymentExecutedTxn.lblePayDate.text,
        "referenceNo": frmIBePaymentExecutedTxn.lblTRNVal.text,
        "localeId": kony.i18n.getCurrentLocale()
    }
    inputParam["outputtemplatename"] = "ePayment_" + kony.os.date("DDMMYYYY");
    inputParam["filetype"] = filetype;
    inputParam["templatename"] = "ePaymentComplete";
    inputParam["datajson"] = JSON.stringify(ePaymentJsondata, "", "");
    //if(kony.string.startsWith("N", frmIBePaymentExecutedTxn.lblTRNVal.text.substring(0, 1))) {
    savePDFIB(filetype, gblActivityIds, frmIBePaymentExecutedTxn.lblTRNVal.text);
    /*} else {
    	invokeServiceSecureAsync("generatePdfImage", inputParam, ibRenderFileCallbackfunction);
    }*/
}

function savePDFExecBeepBillIB(filetype) {
    var inputParam = {};
    var BeepBillJsondata = {};
    BeepBillJsondata = {
        "AccountNo": frmIBBeepAndBillExecutedTxn.lblAccountNo.text,
        "AccountName": frmIBBeepAndBillExecutedTxn.lblAcctName.text,
        "BillerName": frmIBBeepAndBillExecutedTxn.lblCompCode.text,
        "Ref1Label": frmIBBeepAndBillExecutedTxn.lblRef1.text,
        "Ref1Value": frmIBBeepAndBillExecutedTxn.lblRef1Value.text,
        "Ref2Label": frmIBBeepAndBillExecutedTxn.lblRef2.text,
        "Ref2Value": frmIBBeepAndBillExecutedTxn.lblRef2Value.text,
        "Amount": frmIBBeepAndBillExecutedTxn.lblPaymentAmountValue.text,
        "Fee": frmIBBeepAndBillExecutedTxn.lblFeeVal.text,
        "PaymentDate": frmIBBeepAndBillExecutedTxn.lblePayDate.text,
        "TransactionRefNo": frmIBBeepAndBillExecutedTxn.lblTRNVal.text,
        "localeId": kony.i18n.getCurrentLocale()
    }
    inputParam["outputtemplatename"] = "Beep&Bill_Details_" + kony.os.date("DDMMYYYY");
    inputParam["filetype"] = filetype;
    inputParam["templatename"] = "ExecuteBeepAndBillPaymentTemplate";
    inputParam["datajson"] = JSON.stringify(BeepBillJsondata, "", "");
    invokeServiceSecureAsync("generatePdfImage", inputParam, ibRenderFileCallbackfunction);
}

function savePDFExecSTSIB(filetype) {
    var inputParam = {};
    var STSJsondata = {};
    STSJsondata = {
        "fromAcctNo": frmIBSTSExecutedTxn.lblAccNo.text,
        "fromAcctName": frmIBSTSExecutedTxn.lblCustName.text,
        "toAcctNo": frmIBSTSExecutedTxn.lblAccNo2.text,
        "toAcctName": frmIBSTSExecutedTxn.lbLAccName2.text,
        "bankName": "",
        "amount": frmIBSTSExecutedTxn.lblTrnsAmnt.text,
        "fee": "",
        "transDate": frmIBSTSExecutedTxn.lblTransDate.text,
        "transRef": frmIBSTSExecutedTxn.lblTrnsRefNo.text,
        "localeId": kony.i18n.getCurrentLocale()
    }
    inputParam["outputtemplatename"] = "SendtoSave_Details_" + kony.os.date("DDMMYYYY");
    inputParam["filetype"] = filetype;
    inputParam["templatename"] = "SendToSaveDetails";
    inputParam["datajson"] = JSON.stringify(STSJsondata, "", "");
    invokeServiceSecureAsync("generatePdfImage", inputParam, ibRenderFileCallbackfunction);
}

function savePDFExecTOANSIB(filetype) {
    var inputParam = {};
    var TOANSJsondata = {};
    TOANSJsondata = {
        "toAccountNo": frmIBTOASavingsExecutedTxn.lblAccuntNoVal.text,
        "toAccountName": frmIBTOASavingsExecutedTxn.lblActName.text,
        "productName": frmIBTOASavingsExecutedTxn.lblProdNameVal.text,
        "branch": frmIBTOASavingsExecutedTxn.lblBranchVal.text,
        "fromAccountNo": frmIBTOASavingsExecutedTxn.lblActNoval.text,
        "fromAccountName": frmIBTOASavingsExecutedTxn.lblAccNSName.text,
        "Amount": frmIBTOASavingsExecutedTxn.lblOpenAmtVal.text,
        "openingDate": frmIBTOASavingsExecutedTxn.lblOpeningDateVal.text,
        "TransactionRefNo": frmIBTOASavingsExecutedTxn.lbltransactionVal.text,
        "localeId": kony.i18n.getCurrentLocale()
    }
    inputParam["outputtemplatename"] = "OpenAccount_Details_" + kony.os.date("DDMMYYYY");
    inputParam["filetype"] = filetype;
    inputParam["templatename"] = "OpenAccountNS";
    inputParam["datajson"] = JSON.stringify(TOANSJsondata, "", "");
    saveOpenAccountPDFIB(filetype, "063", frmIBTOASavingsExecutedTxn.lbltransactionVal.text, "NS");
    //invokeServiceSecureAsync("generatePdfImage", inputParam, ibRenderFileCallbackfunction);
}

function savePDFExecTOASCIB(filetype) {
    var inputParam = {};
    var TOANSJsondata = {};
    TOANSJsondata = {
        "toAccountNo": frmIBTOASavingsCareExecutedTxn.lblOASCActNumVal.text,
        "toAccountName": frmIBTOASavingsCareExecutedTxn.lblOASCActNameVal.text,
        "productName": gblProdNameEN,
        "branch": frmIBTOASavingsCareExecutedTxn.lblOASCBranchVal.text,
        "fromAccountNo": frmIBTOASavingsCareExecutedTxn.lblNFAccBal.text,
        "fromAccountName": frmIBTOASavingsCareExecutedTxn.lblNFAccName.text,
        "Amount": frmIBTOASavingsCareExecutedTxn.lblOASCOpenActVal.text,
        "openingDate": frmIBTOASavingsCareExecutedTxn.lblOASCOpenDateVal.text,
        "TransactionRefNo": frmIBTOASavingsCareExecutedTxn.lblOASCTranRefNoVal.text,
        "beneficiariesList": careDetails,
        "localeId": kony.i18n.getCurrentLocale()
    }
    inputParam["outputtemplatename"] = "OpenAccount_Details_" + kony.os.date("DDMMYYYY");
    inputParam["filetype"] = filetype;
    inputParam["templatename"] = "OpenAccountSC";
    inputParam["datajson"] = JSON.stringify(TOANSJsondata, "", "");
    invokeServiceSecureAsync("generatePdfImage", inputParam, ibRenderFileCallbackfunction);
}

function savePDFExecTOADSIB(filetype) {
    var inputParam = {};
    var TOANSJsondata = {};
    TOANSJsondata = {
        "toAccountNo": frmIBTOADreamExecutedTxn.lblOASCActNumVal.text,
        "toAccountName": frmIBTOADreamExecutedTxn.lblNickNameVal.text,
        "productName": frmIBTOADreamExecutedTxn.lblDSAckTitle.text,
        "branch": frmIBTOADreamExecutedTxn.lblBranchVal.text,
        "fromAccountNo": frmIBTOADreamExecutedTxn.lblNFAccBal.text,
        "fromAccountName": frmIBTOADreamExecutedTxn.lblNFAccName.text,
        "openingdate": frmIBTOADreamExecutedTxn.lblOpeningDateVal.text,
        "dreamdesc": "",
        "targetAmount": frmIBTOADreamExecutedTxn.lblOpenAmtVal.text,
        "term": "",
        "transferrecdate": "",
        "localeId": kony.i18n.getCurrentLocale()
    }
    inputParam["outputtemplatename"] = "OpenAccount_Details_" + kony.os.date("DDMMYYYY");
    inputParam["filetype"] = filetype;
    inputParam["templatename"] = "OpenAccountDS";
    inputParam["datajson"] = JSON.stringify(TOANSJsondata, "", "");
    invokeServiceSecureAsync("generatePdfImage", inputParam, ibRenderFileCallbackfunction);
}

function savePDFExecTOATDIB(filetype) {
    var inputParam = {};
    var TOANSJsondata = {};
    TOANSJsondata = {
        "toAccountNo": frmIBTOATDExecutedTxn.lblAccNoVal.text,
        "toAccountName": frmIBTOATDExecutedTxn.lblOATDNickNameVal.text,
        "productName": frmIBTOATDExecutedTxn.lblType.text,
        "branch": frmIBTOATDExecutedTxn.lblBranchVal.text,
        "fromAccountNo": frmIBTOATDExecutedTxn.lblAccountBalance.text,
        "fromAccountName": frmIBTOATDExecutedTxn.lblAccNSName.text,
        "affiliatedAccountNo": frmIBTOATDExecutedTxn.lblPayingToAccount.text,
        "affiliatedAccountName": frmIBTOATDExecutedTxn.lblPayingToName.text,
        "Amount": frmIBTOATDExecutedTxn.lblOATDAmtVal.text,
        "openingDate": frmIBTOATDExecutedTxn.lblOpeningDateVal.text,
        "term": frmIBTOATDExecutedTxn.lblTermval.text,
        "maturityDate": frmIBTOATDExecutedTxn.lblMaturityDateVal.text,
        "TransactionRefNo": frmIBTOATDExecutedTxn.lblTdTransRefVal.text,
        "localeId": kony.i18n.getCurrentLocale()
    }
    inputParam["outputtemplatename"] = "OpenAccount_Details_" + kony.os.date("DDMMYYYY");
    inputParam["filetype"] = filetype;
    inputParam["templatename"] = "OpenAccountTD";
    inputParam["datajson"] = JSON.stringify(TOANSJsondata, "", "");
    saveOpenAccountPDFIB(filetype, "063", frmIBTOATDExecutedTxn.lblTdTransRefVal.text, "TD");
    //invokeServiceSecureAsync("generatePdfImage", inputParam, ibRenderFileCallbackfunction);
}

function fbShareExecutedTxnIB() {
    //removed beep&bill and s2s forms for fb
    var currentForm = kony.application.getCurrentForm();
    gblPOWcustNME = gblCustomerName;
    if (kony.application.getCurrentForm() == frmIBExecutedTransaction) {
        gblPOWtransXN = "Transfer";
        gblPOWchannel = "Internet Banking";
    } else if (kony.application.getCurrentForm() == frmIBBillPaymentExecutedTxn) {
        gblPOWtransXN = "Bill Payment";
        gblPOWchannel = "Internet Banking";
    } else if (kony.application.getCurrentForm() == frmIBTopUpExecutedTxn) {
        gblPOWtransXN = "Top-Up";
        gblPOWchannel = "Internet Banking";
    } else if (kony.application.getCurrentForm() == frmIBePaymentExecutedTxn) {
        gblPOWtransXN = "Bill Payment";
        gblPOWchannel = "Internet Banking";
    } else if (kony.application.getCurrentForm() == frmIBTOASavingsExecutedTxn) {
        gblPOWtransXN = "Open Account";
        gblPOWchannel = "Internet Banking";
    } else if (kony.application.getCurrentForm() == frmIBTOASavingsCareExecutedTxn) {
        gblPOWtransXN = "Open Account";
        gblPOWchannel = "Internet Banking";
    } else if (kony.application.getCurrentForm() == frmIBTOATDExecutedTxn) {
        gblPOWtransXN = "Open Account";
        gblPOWchannel = "Internet Banking";
    } else if (kony.application.getCurrentForm() == frmIBTOADreamExecutedTxn) {
        gblPOWtransXN = "Open Account";
        gblPOWchannel = "Internet Banking";
    }
    requestfromform = currentForm;
    postOnWall();
}

function setVisiblityBalanceTransfer(widget1, widget2, bool) {
    widget1.setVisibility(bool);
    widget2.setVisibility(bool);
}