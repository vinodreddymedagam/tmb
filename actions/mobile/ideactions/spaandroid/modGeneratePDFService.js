function showAlertPopBlockerInfo(url, inputParam, callBackFunction) {
    var okk = kony.i18n.getLocalizedString("keyOK");
    //Defining basicConf parameter for alert
    var basicConf = {
        message: kony.i18n.getLocalizedString("keyPopUpBlockerErrMsg"),
        alertType: constants.ALERT_TYPE_INFO,
        alertTitle: kony.i18n.getLocalizedString("info"),
        yesLabel: okk,
        noLabel: "",
        alertHandler: handlePopBlockerIB
    };
    //Defining pspConf parameter for alert
    var pspConf = {};
    //Alert definition
    var infoAlert = kony.ui.Alert(basicConf, pspConf);

    function handlePopBlockerIB(response) {
        invokeServiceAsyncPopUpBlocker(url, inputParam, callBackFunction)
    }
    return;
}

function invokeServiceAsyncPopUpBlocker(url, inputParam, callBackFunction) {
    if (isMFEnabled) {
        var connHandle = callMF(inputParam["serviceID"], inputParam, callBackBasicFunctionPopUpBlocker);
    } else {
        var connHandle = kony.net.invokeServiceAsync(url, inputParam, callBackBasicFunctionPopUpBlocker, null, null, DEFAULT_SERVICE_TIMEOUT);
    }

    function callMF(serviceID, inputParam, callBackBasicFunction) {
        try {
            var headers = {};
            var serviceName = serviceOpMap[serviceID];
            bbServiceObj = mfClient.getIntegrationService(serviceName);
            bbServiceObj.invokeOperation(serviceID, headers, inputParam, function(result) {
                //success
                callBackBasicFunction(400, result);
            }, function(result) {
                //error
                kony.print("====>Operation failed");
                callBackBasicFunction(400, result);
            });
        } catch (e) {
            if (e.code == Errors.AUTH_FAILURE || e.code == Errors.INIT_FAILURE) {
                // initialization and authorization is not complete
                // sleep for try again
                kony.print("Trying to invoke service while SDK is not initialized yet!");
            }
        }
    }
    /**
     * function checks for valid session
     * @returns {}
     */
    function callBackBasicFunctionPopUpBlocker(status, resultTable) {
        if (status == 400) {
            if (resultTable["tknid"] != null) {
                tknid = resultTable["tknid"]; //////Added for CSFR Token ID/////////
            }
            var isSessionDestroyed = false;
            if (resultTable["opstatus"] == 6001) {
                isSessionDestroyed = true;
            } else if (resultTable["opstatus"] == 1011 || resultTable["opstatus"] == 1012 || resultTable["opstatus"] == 1000) { //wifi off
                //isSessionDestroyed = true;
                alert(kony.i18n.getLocalizedString("genErrorWifiOff"));
                // var deviceInf = kony.os.deviceInfo();
                if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPhone Simulator" || gblDeviceInfo["name"] == "android") {
                    dismissLoadingScreen();
                }
                return false;
            } else if (resultTable["opstatus"] == 1000) { //wifi off
                isSessionDestroyed = true;
            } else if (resultTable["opstatus"] == 0 && resultTable["errCode"] == "K898989") {
                dismissIBLoadingScreen();
                isSessionDestroyed = true;
            } else if (resultTable["opstatus"] == 0 && resultTable["errCode"] == "K898990") {
                dismissIBLoadingScreen();
                alert("You are not authorized to invoke this Service, please contact TMB HelpDesk");
            } else if (resultTable["opstatus"] == 0 && resultTable["errCode"] == "K899091") {
                dismissIBLoadingScreen();
                isSessionDestroyed = true;
                alert("CSRF Security threat found (May be service failure too!!!)");
            } else {
                //dismissIBLoadingScreen();
                //have to handle other conditons wifiOff//default error message,...
            }
            if (isSessionDestroyed) {
                //var deviceInfo = kony.os.deviceInfo();
                dismissIBLoadingScreen();
                if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPhone Simulator" || gblDeviceInfo["name"] == "android") {
                    var getEncrKeyFromDevice = kony.store.getItem("encrytedText");
                    if (getEncrKeyFromDevice != null) {
                        if (kony.application.getCurrentForm().id != "frmMBPreLoginAccessesPin") {
                            isSignedUser = false;
                            frmMBPreLoginAccessesPin.show();
                        } else {
                            //alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
                            dismissLoadingScreen();
                            return false;
                        }
                    } else {
                        frmMBanking.show();
                    }
                } else if (gblDeviceInfo["name"] == "thinclient" && gblDeviceInfo["type"] == "spa") {
                    //
                    //frmSPALogin.show();
                    //} else {
                    if (flowSpa) {
                        frmSPALogin.show();
                    } else {
                        frmIBPreLogin.show();
                    }
                }
            } else {
                if (resultTable["sPub"] != null && resultTable["sPub"] != undefined && resultTable["sPub"] != "") {
                    cSerPub = resultTable["sPub"];
                }
                callBackFunction(status, resultTable);
            }
        }
        return connHandle;
    }
}