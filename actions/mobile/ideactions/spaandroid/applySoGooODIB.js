gblPlan1 = "";
gblPlan2 = "";
gblPlan3 = "";
gblMaxApplySoGood = 5;

function applySoGooODLanding_preShow() {
    frmIBApplySoGooODPlanList.btnSelect.text = kony.i18n.getLocalizedString("keyselectall");
    frmIBApplySoGooODPlanList.lblCardNameLP.text = frmIBAccntSummary.lblAccountNameHeader.text;
    frmIBApplySoGooODPlanList.lblCardName.text = frmIBApplySoGooODPlanList.lblCardNameLP.text;
    frmIBApplySoGooODPlanList.lblCreditCardNickName.text = frmIBApplySoGooODPlanList.lblCardNameLP.text;
    if (gblTransactionPlanServiceData.length > 0) {
        frmIBApplySoGooODPlanList.hbxImage.setVisibility(true);
        frmIBApplySoGooODPlanList.hbxSelectedTrxnBox.setVisibility(true);
        frmIBApplySoGooODPlanList.segPlanData.setData(updateLocaleDataSeg(frmIBApplySoGooODPlanList.segPlanData.data));
    } else {
        frmIBApplySoGooODPlanList.hbxImage.setVisibility(true);
        frmIBApplySoGooODPlanList.hbxSelectedTrxnBox.setVisibility(false);
        frmIBApplySoGooODPlanList.lblAddSoGooOD.skin = lblIB20pxGrey;
    }
    if (frmIBApplySoGooODPlanList.hbxSelectSoGooODTxn.isVisible) {
        frmIBApplySoGooODPlanList.hbxImage.setVisibility(false);
        frmIBApplySoGooODPlanList.lblAddSoGooOD.skin = lblWhite140;
        loadRemainingSoGoooDTransactionsIB();
    } else if (frmIBApplySoGooODPlanList.hbxSelectSoGooODPlan.isVisible) {
        frmIBApplySoGooODPlanList.lblAddSoGooOD.skin = lblWhite140;
        frmIBApplySoGooODPlanList.hbxImage.setVisibility(false);
        var plan1 = gblPlan1 + " " + kony.i18n.getLocalizedString("keymonths");
        var plan2 = gblPlan2 + " " + kony.i18n.getLocalizedString("keymonths");
        var plan3 = gblPlan3 + " " + kony.i18n.getLocalizedString("keymonths");
        frmIBApplySoGooODPlanList.btnPlan1.text = plan1;
        frmIBApplySoGooODPlanList.btnPlan2.text = plan2;
        frmIBApplySoGooODPlanList.btnPlan3.text = plan3;
        frmIBApplySoGooODPlanList.lblMonthlyInstallment.text = kony.i18n.getLocalizedString("keySoGooODMonthlyInst") + ":";
        frmIBApplySoGooODPlanList.lblTotalInterest.text = kony.i18n.getLocalizedString("keySoGooODIntCharge") + ":";
        frmIBApplySoGooODPlanList.label3635821205527.text = kony.i18n.getLocalizedString("keySoGooODTotAmt");
    }
    var locale = kony.i18n.getCurrentLocale();
    if (locale == "en_US") {
        frmIBApplySoGooODPlanList.hbxAddSoGooOD.margin = [1, 57, 1, 60];
    } else {
        frmIBApplySoGooODPlanList.hbxAddSoGooOD.margin = [1, 56, 1, 60];
    }
    if (frmIBApplySoGooODPlanList.hbxSelectedTrxnBox.isVisible) {
        frmIBApplySoGooODPlanList.hbxAddSoGooOD.setVisibility(false);
    } else {
        frmIBApplySoGooODPlanList.hbxAddSoGooOD.setVisibility(true);
    }
    setFocusOnBtnAccountSum(kony.application.getCurrentForm());
}

function applySoGooODLanding_postShow() {
    addIBMenu();
}

function gotoApplySoGooODLandingPage() {
    //hideTransactions();
    frmIBApplySoGooODPlanList.hbxSelectSoGooODTxn.setVisibility(false);
    frmIBApplySoGooODPlanList.hbxSelectSoGooODPlan.setVisibility(false);
    frmIBApplySoGooODPlanList.hbxImage.setVisibility(true);
    enableAddButton("IB");
    disableNextButtonOfLandingIB();
    getSoGooODTransactionsIB();
    frmIBApplySoGooODPlanList.segPlanData.removeAll();
}

function enableNextButtonOfLandingIB() {
    frmIBApplySoGooODPlanList.btnNextPlanList.setEnabled(true);
    frmIBApplySoGooODPlanList.btnNextPlanList.skin = btnIB158;
    frmIBApplySoGooODPlanList.btnNextPlanList.focusSkin = btnIB158active;
    frmIBApplySoGooODPlanList.btnNextPlanList.hoverSkin = btnIB158active;
    frmIBApplySoGooODPlanList.hbxAddSoGooOD.setVisibility(false);
}

function disableNextButtonOfLandingIB() {
    frmIBApplySoGooODPlanList.btnNextPlanList.setEnabled(false);
    frmIBApplySoGooODPlanList.btnNextPlanList.skin = btnIB158disabled;
    frmIBApplySoGooODPlanList.btnNextPlanList.focusSkin = btnIB158disabled;
    frmIBApplySoGooODPlanList.btnNextPlanList.hoverSkin = NoSkin;
    frmIBApplySoGooODPlanList.hbxAddSoGooOD.setVisibility(true);
    frmIBApplySoGooODPlanList.lblAddSoGooOD.skin = lblIB20pxGrey;
}

function getSoGooODTransactionsIB() {
    frmIBApplySoGooODPlanList.lblNoSoGoooDTxns.setVisibility(false);
    frmIBApplySoGooODPlanList.hbxSoGooODSegment.setVisibility(true);
    frmIBApplySoGooODPlanList.hbxBackOk.margin = [0, 5, 0, 0];
    frmIBApplySoGooODPlanList.lblCardName.setVisibility(true);
    frmIBApplySoGooODPlanList.hbxBackOk.setVisibility(true);
    selectedTxnIds = [];
    gblCurrSelTxnIds = [];
    gblTransactionPlanServiceData = [];
    getSoGoodTxnsAndPlansService();
}

function loadSoGooODTxnsInSegmentIB(transactionData) {
    soGooODTxnsList = [];
    frmIBApplySoGooODPlanList.txtSoGooODSearch.text = "";
    disableAddButton("IB");
    disableOkButton();
    showTransactions();
    if (transactionData == null || transactionData.length == 0) {
        frmIBApplySoGooODPlanList.lblNoSoGoooDTxns.setVisibility(true);
        frmIBApplySoGooODPlanList.hbxSoGooODSegment.setVisibility(false);
        frmIBApplySoGooODPlanList.lblCardName.setVisibility(false);
        disableOkButton();
        frmIBApplySoGooODPlanList.hbxBackOk.margin = [0, 107, 0, 0];
        frmIBApplySoGooODPlanList.lblNoSoGoooDTxns.text = kony.i18n.getLocalizedString("keyNoTransactionFound");
    } else {
        for (var i = 0; i < transactionData.length; i++) {
            var tempRecord = {
                "transactionId": transactionData[i].transactionId,
                "lblTxnDate": kony.i18n.getLocalizedString("keyIBTransactionDate") + ": " + transactionData[i].txnDate,
                "lblDesc": kony.i18n.getLocalizedString("keyIBDescription") + ": " + transactionData[i].txnDesc,
                "lblTxnAmount": kony.i18n.getLocalizedString("keyAmount"),
                "lblAmountValue": commaFormatted(transactionData[i].txnAmount) + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
                "imgCheckBox": {
                    "src": "chkbox2.png"
                }
            };
            soGooODTxnsList.push(tempRecord);
        }
        frmIBApplySoGooODPlanList.lblNoSoGoooDTxns.setVisibility(false);
        frmIBApplySoGooODPlanList.hbxSoGooODSegment.setVisibility(true);
        frmIBApplySoGooODPlanList.lblCardName.setVisibility(true);
        frmIBApplySoGooODPlanList.hbxBackOk.margin = [0, 5, 0, 0];
        frmIBApplySoGooODPlanList.segSoGooODTxns.setData(soGooODTxnsList);
    }
}

function selectAll() {
    var txtSel = frmIBApplySoGooODPlanList.btnSelect.text;
    var txtSelectAll = kony.i18n.getLocalizedString("keyselectall");
    var txtDeSelectAll = kony.i18n.getLocalizedString("keydeselectall");
    var txtVal = "";
    var obj = frmIBApplySoGooODPlanList.segSoGooODTxns;
    if (obj.data.length > 0) {
        if (txtSel == txtSelectAll) {
            frmIBApplySoGooODPlanList.segSoGooODTxns.selectedRowIndices = null;
            txtVal = txtDeSelectAll;
            for (var i = 0; i < obj.data.length; i++) {
                obj.selectedRowIndex = [0, i];
            }
        } else {
            txtVal = txtSelectAll;
            frmIBApplySoGooODPlanList.segSoGooODTxns.selectedRowIndices = null;
        }
        frmIBApplySoGooODPlanList.btnSelect.text = txtVal;
    }
    var selectedItems = frmIBApplySoGooODPlanList.segSoGooODTxns.selectedItems;
    /* Can select unlimited transaction
    var check = selectSoGooODTransactionIB();
	
    if(!check){
    	if(txtSel == txtSelectAll){
    		frmIBApplySoGooODPlanList.segSoGooODTxns.selectedRowIndices = null;
    		for (var i = 0; i < gblMaxApplySoGood; i++) {
    			frmIBApplySoGooODPlanList.segSoGooODTxns.selectedRowIndex = [0, i];
    		}
    		enableOkButton();
    	}else{
    		frmIBApplySoGooODPlanList.segSoGooODTxns.selectedRowIndices = null;
    	}
    }
	
    alert("befor " + JSON.stringify(frmIBApplySoGooODPlanList.segSoGooODTxns.selectionBehaviorConfig));
    */
    if (selectedItems != null && selectedItems.length > 0) {
        enableOkButton();
    } else {
        disableOkButton();
    }
}

function showTransactions() {
    frmIBApplySoGooODPlanList.hbxImage.setVisibility(false);
    frmIBApplySoGooODPlanList.hbxSelectSoGooODTxn.setVisibility(true);
    //frmIBApplySoGooODPlanList.lblAddSoGooOD.setVisibility(false);
    frmIBApplySoGooODPlanList.lblAddSoGooOD.skin = lblWhite140;
    frmIBApplySoGooODPlanList.hbxSelectSoGooODPlan.setVisibility(false);
    //frmIBApplySoGooODPlanList.hbxFooter.margin=[0,98,0,0];
}

function hideTransactions() {
    frmIBApplySoGooODPlanList.hbxImage.setVisibility(true);
    frmIBApplySoGooODPlanList.hbxSelectSoGooODTxn.setVisibility(false);
    enableAddButton("IB");
    clearSelectedTxnOnBackBtn();
    var showLable = true;
    if (selectedTxnIds.length > 0) {
        showLable = false;
    }
    frmIBApplySoGooODPlanList.hbxAddSoGooOD.setVisibility(showLable);
    if (showLable == true) {
        frmIBApplySoGooODPlanList.lblAddSoGooOD.skin = lblIB20pxGrey;
    } else {
        frmIBApplySoGooODPlanList.lblAddSoGooOD.skin = lblWhite140;
    }
}

function searchSoGooODTransactionsIB() {
    var searchText = frmIBApplySoGooODPlanList.txtSoGooODSearch.text;
    searchText = searchText.toLowerCase();
    var soGooODTxnsSegData = soGooODTxnsList;
    var searchList = searchResult(soGooODTxnsSegData, searchText, null);
    if (frmIBApplySoGooODPlanList.segSoGooODTxns.data != null && frmIBApplySoGooODPlanList.segSoGooODTxns.data != undefined) {
        if (frmIBApplySoGooODPlanList.segSoGooODTxns.data.length != searchList.length) {
            frmIBApplySoGooODPlanList.segSoGooODTxns.setData(searchList);
        }
    } else {
        frmIBApplySoGooODPlanList.segSoGooODTxns.setData(searchList);
    }
    if (searchList.length <= 0) {
        gblCurrSelTxnIds = [];
        frmIBApplySoGooODPlanList.lblNoSoGoooDTxns.setVisibility(true);
        if (gblSoGooODTransactionServiceData.length == 0) {
            frmIBApplySoGooODPlanList.lblNoSoGoooDTxns.text = kony.i18n.getLocalizedString("keyNoTransactionFound");
        } else {
            frmIBApplySoGooODPlanList.lblNoSoGoooDTxns.text = kony.i18n.getLocalizedString("keybillernotfound");
        }
        frmIBApplySoGooODPlanList.hbxSoGooODSegment.setVisibility(false);
        frmIBApplySoGooODPlanList.hbxBackOk.margin = [0, 107, 0, 0];
        frmIBApplySoGooODPlanList.lblCardName.setVisibility(false);
    } else {
        frmIBApplySoGooODPlanList.lblNoSoGoooDTxns.setVisibility(false);
        frmIBApplySoGooODPlanList.hbxSoGooODSegment.setVisibility(true);
        frmIBApplySoGooODPlanList.hbxBackOk.margin = [0, 5, 0, 0];
        frmIBApplySoGooODPlanList.lblCardName.setVisibility(true);
    }
    frmIBApplySoGooODPlanList.btnSelect.text = kony.i18n.getLocalizedString("keyselectall");
    //Enable or Disable OK Button
    if (gblCurrSelTxnIds.length > 0) {
        enableOkButton();
    } else {
        disableOkButton();
    }
}

function selectSoGooODTransactionIB() {
    var selectedItems = frmIBApplySoGooODPlanList.segSoGooODTxns.selectedItems;
    var txtDeSelectAll = kony.i18n.getLocalizedString("keydeselectall");
    if (frmIBApplySoGooODPlanList.btnSelect.text == txtDeSelectAll) frmIBApplySoGooODPlanList.btnSelect.text = kony.i18n.getLocalizedString("keyselectall");
    /* Can select unlimited transaction
    var selectedItems = frmIBApplySoGooODPlanList.segSoGooODTxns.selectedItems;
	
    if(selectedItems != null) {
    	var totalSelLen = selectedItems.length + selectedTxnIds.length;
	
    	if(totalSelLen > gblMaxApplySoGood) {
    		var errorMsg = kony.i18n.getLocalizedString("keyValidMaxSoGoodSelection");
    		errorMsg = errorMsg.replace("[maxOfSoGoodTxn]", gblMaxApplySoGood);
    		alert(errorMsg);
    		
    		var selectedItem = frmIBApplySoGooODPlanList.segSoGooODTxns.selectedIndex[1];
    		var selectedData = frmIBApplySoGooODPlanList.segSoGooODTxns.data[selectedItem];
    		selectedData["imgCheckBox"] = {"src": "chkbox2.png"};
    		frmIBApplySoGooODPlanList.segSoGooODTxns.setDataAt(selectedData, selectedItem);
    		return false;
    	}
    }*/
    //Enable or Disable OK Button
    if (selectedItems != null && selectedItems.length > 0) {
        enableOkButton();
    } else {
        disableOkButton();
    }
}

function enableOkButton() {
    frmIBApplySoGooODPlanList.btnOk.setEnabled(true);
    frmIBApplySoGooODPlanList.btnOk.skin = btnIB158;
    frmIBApplySoGooODPlanList.btnOk.focusSkin = btnIB158active;
    frmIBApplySoGooODPlanList.btnOk.hoverSkin = btnIB158active;
}

function disableOkButton() {
    frmIBApplySoGooODPlanList.btnOk.setEnabled(false);
    frmIBApplySoGooODPlanList.btnOk.skin = btnIB158disabled;
    frmIBApplySoGooODPlanList.btnOk.focusSkin = btnIB158disabled;
    frmIBApplySoGooODPlanList.btnOk.hoverSkin = NoSkin;
}

function frmIBSoGooodProdBriefPreShow() {
    var currentLocale = kony.i18n.getCurrentLocale();
    frmIBSoGooodProdBrief.lblSoGooODHeader.text = kony.i18n.getLocalizedString('keyApplySoGooOD')
    frmIBSoGooodProdBrief.btnSoGooODCancel.text = kony.i18n.getLocalizedString('Back');
    frmIBSoGooodProdBrief.btnNext.text = kony.i18n.getLocalizedString('Next');
    //As per PO's change request
    var soGoooDTnCMsg = kony.i18n.getLocalizedString('keySoGooODPrdBrief');
    soGoooDTnCMsg = soGoooDTnCMsg.replace("{start_time}", soGooOD_StartTime);
    soGoooDTnCMsg = soGoooDTnCMsg.replace("{end_time}", soGooOD_EndTime);
    frmIBSoGooodProdBrief.lblTandC.text = soGoooDTnCMsg;
    frmIBSoGooodProdBrief.lblImgSoGooODHead.text = kony.i18n.getLocalizedString('keySoGooODPrdBriefHead');
    var apply_sogoood_head_image_name = "ApplySoGooodHead";
    var locale = "";
    if (currentLocale == "th_TH") {
        locale = "_TH";
    } else {
        locale = "_EN";
    }
    var apply_sogoood_tail_image_name = "ApplySoGooodTail" + locale;
    var randomnum = Math.floor((Math.random() * 10000) + 1);
    var apply_sogoood_head_image = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + apply_sogoood_head_image_name + "&modIdentifier=PRODUCTPACKAGEIMG&rr=" + randomnum;
    var apply_sogoood_tail_image = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + apply_sogoood_tail_image_name + "&modIdentifier=PRODUCTPACKAGEIMG&rr=" + randomnum;
    frmIBSoGooodProdBrief.imgSoGooODHead.src = apply_sogoood_head_image;
    frmIBSoGooodProdBrief.imgSoGooODHead.setVisibility(true);
    frmIBSoGooodProdBrief.imgSoGooODTail.src = apply_sogoood_tail_image;
    frmIBSoGooodProdBrief.imgSoGooODTail.setVisibility(true);
    setFocusOnBtnAccountSum(kony.application.getCurrentForm());
}

function loadInterestPlansIB() {
    frmIBApplySoGooODPlanList.hbxSelectPlans.setVisibility(true);
    var selectedItems = frmIBApplySoGooODPlanList.segSoGooODTxns.selectedItems;
    gblCurrSelTxnIds = [];
    var totalSelLen = selectedItems.length + selectedTxnIds.length;
    //Enable or Disable OK Button
    if (selectedItems != null) {
        /*if(totalSelLen > gblMaxApplySoGood) {
        	var errorMsg = kony.i18n.getLocalizedString("keyValidMaxSoGoodSelection");
        	errorMsg = errorMsg.replace("[maxOfSoGoodTxn]", gblMaxApplySoGood);
        	alert(errorMsg);
        	return false;
        } else {*/
        for (i = 0; i < selectedItems.length; i++) {
            var txnId = selectedItems[i]["transactionId"];
            if (selectedTxnIds.indexOf(txnId) < 0) {
                selectedTxnIds.push(txnId);
            }
            gblCurrSelTxnIds.push(txnId);
        }
        populateInterestPlansIB();
        //} 
    }
}

function populateInterestPlansIB() {
    var interestPlans = [];
    //repeatScheduleButtonSet
    frmIBApplySoGooODPlanList.lblCreditCardNickName.text = frmIBAccntSummary.lblAccountNameHeader.text;
    frmIBApplySoGooODPlanList.hbxSelectSoGooODPlan.setVisibility(true);
    frmIBApplySoGooODPlanList.hbxSelectSoGooODTxn.setVisibility(false);
    frmIBApplySoGooODPlanList.hbxImage.setVisibility(false);
    frmIBApplySoGooODPlanList.hbxFooter.setVisibility(true);
    var plan1 = gblPlan1 + " " + kony.i18n.getLocalizedString("keymonths");
    var plan2 = gblPlan2 + " " + kony.i18n.getLocalizedString("keymonths");
    var plan3 = gblPlan3 + " " + kony.i18n.getLocalizedString("keymonths");
    frmIBApplySoGooODPlanList.btnPlan1.text = plan1;
    frmIBApplySoGooODPlanList.btnPlan2.text = plan2;
    frmIBApplySoGooODPlanList.btnPlan3.text = plan3;
    frmIBApplySoGooODPlanList.btnPlan1.skin = "btnIBTab3LeftNrml";
    frmIBApplySoGooODPlanList.btnPlan2.skin = "btnIBTab3MidNrml";
    frmIBApplySoGooODPlanList.btnPlan3.skin = "btnIBTab3RightNrml";
    frmIBApplySoGooODPlanList.lblMonthlyInstallment.text = kony.i18n.getLocalizedString("keySoGooODMonthlyInst") + ":";
    frmIBApplySoGooODPlanList.lblTotalInterest.text = kony.i18n.getLocalizedString("keySoGooODIntCharge") + ":";
    frmIBApplySoGooODPlanList.label3635821205527.text = kony.i18n.getLocalizedString("keySoGooODTotAmt");
    frmIBApplySoGooODPlanList.lblMonthlyInstallmentVal.text = "";
    frmIBApplySoGooODPlanList.lblTotalInterestValue.text = "";
    frmIBApplySoGooODPlanList.lblTotalAmountValue.text = "";
    disableOKButtonOfPlanSelectIB();
}

function enableOKButtonOfPlanSelectIB() {
    frmIBApplySoGooODPlanList.btnNext.setEnabled(true);
    frmIBApplySoGooODPlanList.btnNext.skin = btnIB158;
    frmIBApplySoGooODPlanList.btnNext.focusSkin = btnIB158active;
    frmIBApplySoGooODPlanList.btnNext.hoverSkin = btnIB158active;
}

function disableOKButtonOfPlanSelectIB() {
    frmIBApplySoGooODPlanList.btnNext.setEnabled(false);
    frmIBApplySoGooODPlanList.btnNext.skin = btnIB158disabled;
    frmIBApplySoGooODPlanList.btnNext.focusSkin = btnIB158disabled;
    frmIBApplySoGooODPlanList.btnNext.hoverSkin = NoSkin;
}

function OnPlanSelectServiceIB(eventObject) {
    var buttonId = eventObject.id;
    var isToggle = "false";
    var planId = "";
    if (kony.string.equalsIgnoreCase(buttonId, "btnPlan1")) {
        if (frmIBApplySoGooODPlanList.btnPlan1.skin == "btnIBTab3LeftFocus") {
            frmIBApplySoGooODPlanList.btnPlan1.skin = "btnIBTab3LeftNrml";
            isToggle = "false";
        } else {
            frmIBApplySoGooODPlanList.btnPlan1.skin = "btnIBTab3LeftFocus";
            isToggle = "true";
        }
        frmIBApplySoGooODPlanList.btnPlan2.skin = "btnIBTab3MidNrml";
        frmIBApplySoGooODPlanList.btnPlan3.skin = "btnIBTab3RightNrml";
        planId = "1";
        plan = gblPlan1;
    } else if (kony.string.equalsIgnoreCase(buttonId, "btnPlan2")) {
        frmIBApplySoGooODPlanList.btnPlan1.skin = "btnIBTab3LeftNrml";
        if (frmIBApplySoGooODPlanList.btnPlan2.skin == "btnIBTab3MidFocus") {
            frmIBApplySoGooODPlanList.btnPlan2.skin = "btnIBTab3MidNrml";
            isToggle = "false";
        } else {
            frmIBApplySoGooODPlanList.btnPlan2.skin = "btnIBTab3MidFocus";
            isToggle = "true";
        }
        frmIBApplySoGooODPlanList.btnPlan3.skin = "btnIBTab3RightNrml";
        planId = "2";
        plan = gblPlan2;
    } else if (kony.string.equalsIgnoreCase(buttonId, "btnPlan3")) {
        frmIBApplySoGooODPlanList.btnPlan1.skin = "btnIBTab3LeftNrml";
        frmIBApplySoGooODPlanList.btnPlan2.skin = "btnIBTab3MidNrml";
        if (frmIBApplySoGooODPlanList.btnPlan3.skin == "btnIBTab3RightFocus") {
            frmIBApplySoGooODPlanList.btnPlan3.skin = "btnIBTab3RightNrml";
            isToggle = "false";
        } else {
            frmIBApplySoGooODPlanList.btnPlan3.skin = "btnIBTab3RightFocus";
            isToggle = "true";
        }
        planId = "3";
        plan = gblPlan3;
    }
    if (isToggle == "true") {
        disableOKButtonOfPlanSelectIB();
        calculateInterestForTxns(plan, "IB");
    } else {
        disableOKButtonOfPlanSelectIB();
        calculateInterestForTxns("untoggle", "IB");
    }
    //calculateInterestOnplanSelect(planId, plan, "IB");
}

function frmIBSoGooodTnCPreShow() {
    var currentLocale = kony.i18n.getCurrentLocale();
    frmIBSoGooodTnC.lblApplySoGooOD.text = kony.i18n.getLocalizedString('keyApplySoGooOD')
    frmIBSoGooodTnC.btnSoGooODAgree.text = kony.i18n.getLocalizedString('keyAgreeButton')
    frmIBSoGooodTnC.btnSoGooODCancel.text = kony.i18n.getLocalizedString('Back')
    var input_param = {};
    input_param["localeCd"] = currentLocale;
    input_param["moduleKey"] = 'TMBSoGooOD';
    showLoadingScreen();
    setFocusOnBtnAccountSum(kony.application.getCurrentForm());
    invokeServiceSecureAsync("readUTFFile", input_param, setSoGooodTnCIB);
}

function setSoGooodTnCIB(status, result) {
    if (status == 400) {
        dismissLoadingScreen();
        if (result["opstatus"] == 0) {
            frmIBSoGooodTnC.btnSoGooODAgree.text = kony.i18n.getLocalizedString('keyAgreeButton')
            frmIBSoGooodTnC.btnSoGooODCancel.text = kony.i18n.getLocalizedString('Back')
            frmIBSoGooodTnC.lblApplySoGooOD.text = kony.i18n.getLocalizedString('keyApplySoGooOD')
            frmIBSoGooodTnC.lblSoGooODTnCDesc.text = "";
            frmIBSoGooodTnC.lblSoGooODTnCDesc.text = result["fileContent"];
        }
    }
}

function showHideMoreDetailsOfTransactionIB() {
    var selectedItem = frmIBApplySoGooODPlanList.segPlanData.selectedIndex[1];
    var selectedData = frmIBApplySoGooODPlanList.segPlanData.data[selectedItem];
    if (selectedData.linkMoreHide == kony.i18n.getLocalizedString("More")) {
        selectedData = moreHidePlanRecord(selectedData, kony.i18n.getLocalizedString("Hide"), true);
    } else {
        selectedData = moreHidePlanRecord(selectedData, kony.i18n.getLocalizedString("More"), false);
    }
    frmIBApplySoGooODPlanList.segPlanData.setDataAt(selectedData, selectedItem);
}

function moreHidePlanRecord(selectedData, moreHide, visibleFlag) {
    selectedData["linkMoreHide"] = moreHide;
    selectedData["hbxInstallment"] = {
        "isVisible": visibleFlag
    };
    selectedData["hbxInterest"] = {
        "isVisible": visibleFlag
    };
    selectedData["hbxTotalAmount"] = {
        "isVisible": visibleFlag
    };
    selectedData["hbxTransactionDate"] = {
        "isVisible": visibleFlag
    };
    selectedData["hbxPostedDate"] = {
        "isVisible": visibleFlag
    };
    return selectedData;
}

function deleteSelectedSoGoooDTransactionIB() {
    var selectedItem = frmIBApplySoGooODPlanList.segPlanData.selectedIndex[1];
    var selectedData = frmIBApplySoGooODPlanList.segPlanData.data[selectedItem];
    var selTxnId = selectedData.transactionId;
    //Deleting selected transaction 
    if (selectedTxnIds.indexOf(selTxnId) > -1) selectedTxnIds.splice(selectedTxnIds.indexOf(selTxnId), 1);
    deleteTnxInGblTnxDataList(selTxnId, "IB");
    if (frmIBApplySoGooODPlanList.hbxSelectSoGooODTxn.isVisible) {
        loadRemainingSoGoooDTransactionsIB();
        frmIBApplySoGooODPlanList.hbxSelectSoGooODTxn.setVisibility(true);
    }
    if (frmIBApplySoGooODPlanList.hbxSelectSoGooODPlan.isVisible) {
        disableAddButton("IB");
    }
}

function backToRemainingSoGoooDTransactionsIB() {
    clearSelectedTxnOnBackBtn();
    loadRemainingSoGoooDTransactionsIB();
}

function loadRemainingSoGoooDTransactionsIB() {
    gblCurrSelTxnIds = [];
    disableOkButton();
    //var transactionData = '[{"stmtTranCode":"","SRCFlag":"3","txnAmount":"5000.00","soGoodFlag":"N","CCTrnSeqNum":"30174","txnDate":"12/06/2021","transactionId":"30174","KeyFile":"001511451822511100200200212","txnDesc":"TEST TRANSACTION 050","refNumber":"16300016576","postedDate":"12/06/2021","authentId":"090001"},{"stmtTranCode":"","SRCFlag":"3","txnAmount":"4800.00","soGoodFlag":"N","CCTrnSeqNum":"30172","txnDate":"12/06/2021","transactionId":"30172","KeyFile":"001511451822511100200200210","txnDesc":"TEST TRANSACTION 048","refNumber":"16300016574","postedDate":"12/06/2021","authentId":"090001"},{"stmtTranCode":"","SRCFlag":"3","txnAmount":"4700.00","soGoodFlag":"N","CCTrnSeqNum":"30171","txnDate":"12/06/2021","transactionId":"30171","KeyFile":"001511451822511100200200209","txnDesc":"TEST TRANSACTION 047","refNumber":"16300016573","postedDate":"12/06/2021","authentId":"090001"},{"stmtTranCode":"","SRCFlag":"3","txnAmount":"4600.00","soGoodFlag":"N","CCTrnSeqNum":"30170","txnDate":"12/06/2021","transactionId":"30170","KeyFile":"001511451822511100200200208","txnDesc":"TEST TRANSACTION 046","refNumber":"16300016572","postedDate":"12/06/2021","authentId":"090001"},{"stmtTranCode":"","SRCFlag":"3","txnAmount":"4500.00","soGoodFlag":"N","CCTrnSeqNum":"30169","txnDate":"12/06/2021","transactionId":"30169","KeyFile":"001511451822511100200200207","txnDesc":"TEST TRANSACTION 045","refNumber":"16300016571","postedDate":"12/06/2021","authentId":"090001"},{"stmtTranCode":"","SRCFlag":"3","txnAmount":"4400.00","soGoodFlag":"N","CCTrnSeqNum":"30168","txnDate":"12/06/2021","transactionId":"30168","KeyFile":"001511451822511100200200206","txnDesc":"TEST TRANSACTION 044","refNumber":"16300016570","postedDate":"12/06/2021","authentId":"090001"},{"stmtTranCode":"","SRCFlag":"3","txnAmount":"4300.00","soGoodFlag":"N","CCTrnSeqNum":"30167","txnDate":"12/06/2021","transactionId":"30167","KeyFile":"001511451822511100200200205","txnDesc":"TEST TRANSACTION 043","refNumber":"16300016569","postedDate":"12/06/2021","authentId":"090001"},{"stmtTranCode":"","SRCFlag":"3","txnAmount":"4200.00","soGoodFlag":"N","CCTrnSeqNum":"30166","txnDate":"12/06/2021","transactionId":"30166","KeyFile":"001511451822511100200200204","txnDesc":"TEST TRANSACTION 042","refNumber":"16300016568","postedDate":"12/06/2021","authentId":"090001"}]';	
    var transactionData = gblSoGooODTransactionServiceData;
    var remainingSoGoooDTxnData = [];
    for (var i = 0; i < transactionData.length; i++) {
        if (selectedTxnIds.indexOf(transactionData[i].transactionId) < 0) {
            remainingSoGoooDTxnData.push(transactionData[i]);
        }
    }
    loadSoGooODTxnsInSegmentIB(remainingSoGoooDTxnData);
}

function showSelectedPlanTxn() {
    frmIBApplySoGooODPlanList.hbxSelectSoGooODPlan.setVisibility(false);
    frmIBApplySoGooODPlanList.hbxSelectSoGooODTxn.setVisibility(false);
    frmIBApplySoGooODPlanList.hbxImage.setVisibility(true);
    frmIBApplySoGooODPlanList.hbxFooter.setVisibility(false);
    enableNextButtonOfLandingIB();
}

function onClickOfApplySogooodPlanIB() {
    var gblTotalTran = "1";
    if (gblTotalTran == "0") {
        dismissLoadingScreen();
        showAlert(callBackResponse["errMsg"], kony.i18n.getLocalizedString("info"));
        return false
    }
    frmIBSoGooodConf.lblHdrTxt.text = kony.i18n.getLocalizedString("keyConfirmation");
    frmIBSoGooodConf.lblCardName.text = frmIBApplySoGooODPlanList.lblCardNameLP.text;
    frmIBSoGooodConf.show();
}
//----------------------------------------------------------------------------- Comfirm Page
function frmMBSoGooodConfPreShowIB() {
    frmIBSoGooodConf.lblCardName.text = frmIBApplySoGooODPlanList.lblCardNameLP.text;
    frmIBSoGooodConf.lblHdrTxt.text = kony.i18n.getLocalizedString('keyConfirmation');
    frmIBSoGooodConf.btnIBCancel.text = kony.i18n.getLocalizedString('keyCancelButton');
    frmIBSoGooodConf.btnIBConfirm.text = kony.i18n.getLocalizedString('keyConfirm');
    loadSoGooODTransactionConfIB(gblTransactionPlanServiceData);
    setFocusOnBtnAccountSum(kony.application.getCurrentForm());
}

function loadSoGooODTransactionConfIB(transactionPlanData) {
    var segmentData = [];
    for (var i = 0; i < transactionPlanData.length; i++) {
        var tempRecord = getTransactionPlanRecordBasic(transactionPlanData[i]);
        tempRecord["linkMoreHide"] = kony.i18n.getLocalizedString("Hide");
        segmentData.push(tempRecord);
    }
    frmIBSoGooodConf.segApplyTnx.setData(segmentData);
}

function setIsVisibleAsFalse(segData) {
    for (var i = 0; i < segData.length; i++) {
        segData[i]["hbxInstallment"] = {
            "isVisible": false
        };
        segData[i]["hbxInterest"] = {
            "isVisible": false
        };
        segData[i]["hbxTotalAmount"] = {
            "isVisible": false
        };
        segData[i]["hbxTransactionDate"] = {
            "isVisible": false
        };
        segData[i]["hbxPostedDate"] = {
            "isVisible": false
        };
    }
    return segData;
}

function onClickMoreHideOnTnxConfIB() {
    var selectedItem = frmIBSoGooodConf.segApplyTnx.selectedIndex[1];
    var selectedData = frmIBSoGooodConf.segApplyTnx.data[selectedItem];
    if (selectedData.linkMoreHide == kony.i18n.getLocalizedString("More")) {
        selectedData = moreHidePlanRecord(selectedData, kony.i18n.getLocalizedString("Hide"), true);
    } else {
        selectedData = moreHidePlanRecord(selectedData, kony.i18n.getLocalizedString("More"), false);
    }
    frmIBSoGooodConf.segApplyTnx.setDataAt(selectedData, selectedItem);
}
//----------------------------------------------------------------------------- Complete Page
function onClickApplySoGooODConfirmIB() {
    showLoadingScreen();
    loadExcutedSoGooODTxnsInSegmentIB(gblTransactionPlanServiceData);
    frmIBApplySoGooODComplete.lblCardName.text = frmIBSoGooodConf.lblCardName.text;
    frmIBApplySoGooODComplete.show();
    dismissLoadingScreen();
}

function loadExcutedSoGooODTxnsInSegmentIB(excutedTransData) {
    var totalExcutedTnx = excutedTransData.length;
    var failedTnxcount = 0;
    var excutedSoGooODTxnsList = [];
    for (var i = 0; i < excutedTransData.length; i++) {
        var tempRecord;
        var failFlag = false;
        var srcImage = "correct.png";
        // fail status then display flase icon
        if (excutedTransData[i].status == '0') {
            failFlag = true;
            srcImage = "icon_false.png";
        }
        tempRecord = getTransactionPlanRecordBasic(excutedTransData[i]);
        tempRecord["imgStatus"] = srcImage;
        excutedSoGooODTxnsList.push(tempRecord);
        if (failFlag) {
            failedTnxcount++;
        }
    }
    var completedTnx = totalExcutedTnx - failedTnxcount;
    // display icon incomplete if there is fail transaction
    if (failedTnxcount > 0) {
        frmIBApplySoGooODComplete.imageStatus.src = "iconnotcomplete.png";
        frmIBApplySoGooODComplete.btnApplyMore.text = kony.i18n.getLocalizedString("btnTryAgain");
    } else {
        frmIBApplySoGooODComplete.imageStatus.src = "completeico.png";
    }
    if (getSuccessTransactionCount() > 0) {
        frmIBApplySoGooODComplete.hbxOptions.setVisibility(true);
    } else {
        frmIBApplySoGooODComplete.hbxOptions.setVisibility(false);
    }
    frmIBApplySoGooODComplete.segApplyTnx.setData(excutedSoGooODTxnsList);
    frmIBApplySoGooODComplete.segApplyTnx.setData(setIsVisibleAsFalse(frmIBApplySoGooODComplete.segApplyTnx.data));
}

function frmIBApplySoGooODComplete_preshow() {
    frmIBApplySoGooODComplete.lblCardName.text = frmIBSoGooodConf.lblCardName.text;
    frmIBApplySoGooODComplete.lblTransactionInfo.text = getApplySoGooODMessage();
    frmIBApplySoGooODComplete.btnRtn.text = kony.i18n.getLocalizedString("keyReturn");
    frmIBApplySoGooODComplete.lblComplete.text = kony.i18n.getLocalizedString("Complete");
    if (countTryAgain < maxTryAgain) {
        frmIBApplySoGooODComplete.btnApplyMore.setEnabled(true);
        frmIBApplySoGooODComplete.btnApplyMore.skin = btnIB158;
        frmIBApplySoGooODComplete.btnApplyMore.focusSkin = btnIB158active;
        frmIBApplySoGooODComplete.btnApplyMore.hoverSkin = btnIB158active;
    }
    if (getFailedTransactionCount() > 0 && countTryAgain < maxTryAgain) {
        frmIBApplySoGooODComplete.btnApplyMore.text = kony.i18n.getLocalizedString("btnTryAgain");
    } else {
        frmIBApplySoGooODComplete.btnApplyMore.text = kony.i18n.getLocalizedString("keyApplyMore");
        frmIBApplySoGooODComplete.btnApplyMore.setEnabled(true);
        frmIBApplySoGooODComplete.btnApplyMore.skin = btnIB158;
        frmIBApplySoGooODComplete.btnApplyMore.focusSkin = btnIB158active;
        frmIBApplySoGooODComplete.btnApplyMore.hoverSkin = btnIB158active;
    }
    frmIBApplySoGooODComplete.segApplyTnx.setData(updateLocaleDataSeg(frmIBApplySoGooODComplete.segApplyTnx.data));
    setFocusOnBtnAccountSum(kony.application.getCurrentForm());
}

function onClickOfExcutedSoGooODTnxDetailsMoreIB() {
    var selectedItem = frmIBApplySoGooODComplete.segApplyTnx.selectedIndex[1];
    var selectedData = frmIBApplySoGooODComplete.segApplyTnx.data[selectedItem];
    if (selectedData.linkMoreHide == kony.i18n.getLocalizedString("More")) {
        selectedData = moreHidePlanRecord(selectedData, kony.i18n.getLocalizedString("Hide"), true);
    } else {
        selectedData = moreHidePlanRecord(selectedData, kony.i18n.getLocalizedString("More"), false);
    }
    frmIBApplySoGooODComplete.segApplyTnx.setDataAt(selectedData, selectedItem);
}

function onClickApplySoGooODMoreIB() {
    if (frmIBApplySoGooODComplete.btnApplyMore.text == kony.i18n.getLocalizedString("keyApplyMore")) {
        frmIBSoGooodTnC.show();
    } else {
        countTryAgain++;
        callApplySogoodConfirmationService();
    }
}

function clearSelectedTxnOnBackBtn() {
    if (gblCurrSelTxnIds.length > 0) {
        for (i = 0; i < gblCurrSelTxnIds.length; i++) {
            if (selectedTxnIds.indexOf(gblCurrSelTxnIds[i]) > -1) {
                selectedTxnIds.splice(selectedTxnIds.indexOf(gblCurrSelTxnIds[i]), 1);
            }
        }
    }
}

function saveSooGooODCompleteAsPDFImageIB(filetype) {
    var inputParam = {};
    inputParam["filetype"] = filetype;
    inputParam["activityTypeId"] = "ApplySoGooOD";
    inputParam["futurePDF"] = "true";
    invokeServiceSecureAsync("generateImagePdf", inputParam, ibRenderFileCallbackfunction);
}

function postOnFBSoGooODCompleteIB() {
    requestfromform = "frmIBApplySoGooODComplete";
    gblPOWcustNME = gblCustomerName;
    gblPOWtransXN = "ApplySoGooOD";
    gblPOWchannel = "IB";
    postOnWall();
}

function setFocusOnBtnAccountSum(currentFormId) {
    if (undefined != currentFormId.btnMenuMyAccountSummary) {
        if (kony.i18n.getCurrentLocale() == "th_TH") {
            currentFormId.btnMenuMyAccountSummary.skin = "btnIBMenuMyAccountSummaryFocusThai";
        } else {
            currentFormId.btnMenuMyAccountSummary.skin = "btnIBMenuMyAccountSummaryFocus";
        }
    }
}