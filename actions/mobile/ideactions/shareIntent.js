var maskedAccntNum="";
var unmaskedAccntNum="";
var shareMessage="";
var settingVisibility=false;
var hbxadv=false;
var isAddToMyBillsDisplay = false;
var iphonemodule = "";

function hideHeaderFooter()
{
	try {
		kony.application.getCurrentForm().statusBarColor =  "FFFFFF00";
	} catch (e) {
		// todo: handle exception
	}
	var frmName=kony.application.getCurrentForm();
	if(gblDeviceInfo["name"]=="android")
	{
		//frmBlank.show();
		//frmName.show();
	}
	if(frmName.id=="frmTransfersAck" || frmName.id=="frmTransfersAckCalendar")
	{
		frmName.lblSucessScreenshot.setVisibility(true);
		unmaskedAccntNum=frmName.lblTransNPbAckFrmNum.text;
		maskedAccuntNum="xxx-x-" + frmName.lblTransNPbAckFrmNum.text.substring(6,11) + "-x";
		frmName.lblTransNPbAckFrmNum.text=maskedAccuntNum;
		frmName.hbxHeaderConfirmation.setVisibility(false);
		frmName.hbxConfirmCancel.setVisibility(false);
		frmName.hbxArrow.setVisibility(false);
		frmName.hbxFromToAcct2.setVisibility(false);
		if(frmName.segTransAckSplit.data != null && frmName.segTransAckSplit.data.length>=0)
		{
			if(frmName.lblAllSuccess.text=="0")
			{
				frmName.segTransAckSplit.setVisibility(false);
				frmName.lblHideMore.skin="lblGreyLightmask";
				frmName.imgMore.setVisibility(false);
			}
			else
			{
				frmName.segTransAckSplit.setVisibility(false);
				if(frmName.id=="frmTransfersAck")
				{
					frmTransfersAck.lblCnfmToRefTitle2.skin="lblGreyLightmask";
				}
				else
				{
					frmTransfersAckCalendar.lblCnfmToRefTitle1.skin="lblGreyLightmask";
				}
				frmName.lblHideMore.skin="lblGreyLightmask";
				frmName.imgMore.setVisibility(false);
			}
		}
		frmName.hbxAdv.setVisibility(false);
	}
	else if(frmName.id=="frmBillPaymentComplete")
	{
		unmaskedAccntNum=frmName.lblAccountNum.text;
		maskedAccuntNum="xxx-x-" + frmName.lblAccountNum.text.substring(6,11) + "-x";
		frmName.lblAccountNum.text=maskedAccuntNum;
		frmName.hbxHeaderConfirmation.setVisibility(false);
		//frmName.line968116430627578.setVisibility(false);
		frmName.lblSucessScreenshot.setVisibility(true);
		frmName.hbxArrow.setVisibility(false);
		frmName.hbxConfirmCancel.setVisibility(false);
		
		frmName.hbxOAFrmDetAck.setVisibility(false);
		if(frmName.hbxAddToMyBills.isVisible){
			isAddToMyBillsDisplay = true;
		}else{
			isAddToMyBillsDisplay = false;
		}
		frmName.hbxAddToMyBills.setVisibility(false);
		if(frmName.hbxAdv.isVisible)
		{
			hbxadv=true;
			frmName.hbxAdv.setVisibility(false);
			frmName.hbxCommon.setVisibility(false);
		}
		else
		{
			hbxadv=false;
			frmName.hbxAdv.setVisibility(false);
			frmName.hbxCommon.setVisibility(false);
		}
	}else if(frmName.id=="frmBillPaymentCompleteCalendar")
		{
		unmaskedAccntNum=frmName.lblAccountNum.text;
		maskedAccuntNum="xxx-x-" + frmName.lblAccountNum.text.substring(6,11) + "-x";
		frmName.lblAccountNum.text=maskedAccuntNum;
		frmName.hbxHeaderConfirmation.setVisibility(false);
		//frmName.line968116430627578.setVisibility(false);
		frmName.lblSucessScreenshot.setVisibility(true);
		frmName.hbxArrow.setVisibility(false);
		frmName.hbxbpconfcancel.setVisibility(false);
		
		frmName.hbxOAFrmDetAck.setVisibility(false);
		
		if(frmName.hbox50285458168.isVisible)
		{
			hbxadv=true;
			frmName.hbox50285458168.setVisibility(false);
		}
		else
		{
			hbxadv=false;
			frmName.hbox50285458168.setVisibility(false);
		}
	}
	
	//frmName.hbxImageAddRecipient.setVisibility(false);
	frmName.hbxShareOption.setVisibility(false);
}

function displayHeaderFooter()
{
	changeStatusBarColor();
	//alert(megaTransfer)
	var frmName=kony.application.getCurrentForm();
	if(frmName.id=="frmTransfersAck" || frmName.id=="frmTransfersAckCalendar")
	{
		frmName.lblSucessScreenshot.setVisibility(false);
		frmName.lblTransNPbAckFrmNum.text=unmaskedAccntNum;
		frmName.hbxHeaderConfirmation.setVisibility(true);
		frmName.hbxConfirmCancel.setVisibility(true);
		frmName.hbxArrow.setVisibility(true);
		frmName.hbxFromToAcct2.setVisibility(true);
		
		if(frmName.segTransAckSplit.data != null && frmName.segTransAckSplit.data.length>=0)
		{
			if(frmName.lblAllSuccess.text=="0")
			{
				frmName.segTransAckSplit.setVisibility(true);
				//frmName.hbxMoreHide.setVisibility(true);
				//frmName.lblCnfmToRefTitle2.skin="lblGreySmall114";
				frmName.lblHideMore.skin="lblbluemedium142";
				frmName.imgMore.setVisibility(true);
			}
			else
			{
				frmName.segTransAckSplit.setVisibility(true);
				if(frmName.id=="frmTransfersAck")
				{
					frmTransfersAck.lblCnfmToRefTitle2.skin="lblGreySmall114";
				}
				else
				{
					frmTransfersAckCalendar.lblCnfmToRefTitle1.skin="lblGreySmall114";	
				}
				frmName.lblHideMore.skin="lblbluemedium142";
				frmName.imgMore.setVisibility(true);
			}
		}
		frmName.hbxAdv.setVisibility(true);
	}
	else if(frmName.id=="frmBillPaymentComplete")
	{
		frmName.lblAccountNum.text=unmaskedAccntNum;
		frmName.hbxHeaderConfirmation.setVisibility(true);
		//frmName.line968116430627578.setVisibility(true);
		frmName.lblSucessScreenshot.setVisibility(false);
		frmName.hbxArrow.setVisibility(true);
		frmName.hbxConfirmCancel.setVisibility(true);
		frmName.hbxOAFrmDetAck.setVisibility(true);
		if(isAddToMyBillsDisplay){
			frmName.hbxAddToMyBills.setVisibility(true);
		}else{
			frmName.hbxAddToMyBills.setVisibility(false);
		}
		
		if(hbxadv)
		{
			frmName.hbxAdv.setVisibility(true);
			frmName.hbxCommon.setVisibility(false);
		}
		else
		{
			frmName.hbxCommon.setVisibility(true);
			frmName.hbxAdv.setVisibility(false);
		}
		hbxAdv=false;
	}else if(frmName.id=="frmBillPaymentCompleteCalendar")
	{
		frmName.lblAccountNum.text=unmaskedAccntNum;
		frmName.hbxHeaderConfirmation.setVisibility(true);
		//frmName.line968116430627578.setVisibility(true);
		frmName.lblSucessScreenshot.setVisibility(false);
		frmName.hbxArrow.setVisibility(true);
		frmName.hbxbpconfcancel.setVisibility(true);
		frmName.hbxOAFrmDetAck.setVisibility(true);
		if(hbxadv)
		{
			frmName.hbox50285458168.setVisibility(true);
		}
		else
		{
			frmName.hbox50285458168.setVisibility(false);
		}
		
	}
	frmName.hbxShareOption.setVisibility(true);
	settingVisibility=false;
}
/*****************************************************
function to handle share click in ios and Android
*****************************************************/

function shareToSocialMedia(url, module){
  if (gblDeviceInfo["name"] == "android") {
              //kony.print("if android share");
        androidshare(url, module);

    } else if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPad Simulator") {
        iosshare(url, module);

    }      
}

/*****************************************************
Sharing the data in iOS devices
*****************************************************/
function iosshare(url, module) {
	var emailsubject="";
	var module=module;
	if(kony.i18n.getCurrentLocale()=="en_US") 
		{
			if(module=="Transfer")
			{
				emailsubject=gblCustomerName+" "+kony.i18n.getLocalizedString("keyEmailTransferSlip");
			}
			else if(module=="BillPayment")
			{
				emailsubject=gblCustomerName+" "+kony.i18n.getLocalizedString("keyEmailBillPaySlip");
			}
			else if(module=="TopupPayment")
			{
				emailsubject=gblCustomerName+" "+kony.i18n.getLocalizedString("keyEmailTopUpSlip");
			}
			
		}
		else
		{
			if(module=="Transfer")
			{
				emailsubject=kony.i18n.getLocalizedString("keyEmailTransferSlip")+" "+gblCustomerNameTh;
			}
			else if(module=="BillPayment")
			{
				emailsubject=kony.i18n.getLocalizedString("keyEmailBillPaySlip")+" "+gblCustomerNameTh;
			}
			else if(module=="TopupPayment")
			{
				emailsubject=kony.i18n.getLocalizedString("keyEmailTopUpSlip")+" "+gblCustomerNameTh;
			}
	}
    try {
     	var socialmsg = url
        var emailmsg = url
        var smsmsg = url
       	var language= kony.i18n.getCurrentLocale();
        var sharecontent=[];
        sharecontent[0]=emailsubject;
        sharecontent[2]=socialmsg;
        sharecontent[1]=emailmsg;
        sharecontent[3]=language;
        
        //Creates an object of class 'sharescript'
        var sharescriptObject = new iossharemedia.sharescript();
        //Invokes method 'shareonios' on the object
        sharescriptObject.shareonios(
            /**Array*/
            sharecontent);
    } catch (err) {
        kony.print("the value of err.message is:" + err);
    }
}

/*****************************************************
Sharing the data in Android devices
*****************************************************/
function androidshare(url, module) {
    try {
        //alert("in anroid share"+url);
       	var module = module
        var emailsubject = "";
        if(kony.i18n.getCurrentLocale()=="en_US") 
		{
			if(module=="Transfer")
			{
				emailsubject=gblCustomerName+" "+kony.i18n.getLocalizedString("keyEmailTransferSlip");
			}
			else if(module=="BillPayment")
			{
				emailsubject=gblCustomerName+" "+kony.i18n.getLocalizedString("keyEmailBillPaySlip");
			}
			else if(module=="TopupPayment")
			{
				emailsubject=gblCustomerName+" "+kony.i18n.getLocalizedString("keyEmailTopUpSlip");
			}
			
		}
		else
		{
			if(module=="Transfer")
			{
				emailsubject=kony.i18n.getLocalizedString("keyEmailTransferSlip")+" "+gblCustomerNameTh;
			}
			else if(module=="BillPayment")
			{
				emailsubject=kony.i18n.getLocalizedString("keyEmailBillPaySlip")+" "+gblCustomerNameTh;
			}
			else if(module=="TopupPayment")
			{
				emailsubject=kony.i18n.getLocalizedString("keyEmailTopUpSlip")+" "+gblCustomerNameTh;
			}
		}
        var socialmsg = url
        var emailmsg = url
        var smsmsg = url
        
        var language=kony.i18n.getCurrentLocale();
        
        //Creates an object of class 'ShareOnAndroid'
        var ShareOnAndroidObject = new androidshareactivity.ShareOnAndroid(
        /**String*/
        emailsubject,
        /**String*/
        emailmsg,
        /**String*/
        socialmsg,
        /**String*/
        smsmsg,
        /**String*/
        language,
        /**String*/
        module);
         //kony.print("in android share 1");
		if(socialmsg=="line")
		{
			ShareOnAndroidObject.onJSClickActivityline();
		}
		else if(socialmsg=="facebook")
		{
			ShareOnAndroidObject.onJSClickActivityfbmsngr();
		}
		else if(socialmsg=="more")
		{
			ShareOnAndroidObject.onJSClickActivity();
		}
        else if(socialmsg=="screenshot")
        {
        	ShareOnAndroidObject.onJSClickActivityScreenShot();
        }
        // kony.print("in android share 21");
    } catch (err) {
        kony.print("the value of err.message is:" + err);
    }
}


function shareIntentCall(inputmessage,flow)
{
	if(gblDeviceInfo["name"]=="android")
	{
	}
	else
	{
		hideHeaderFooter();
	}
	shareMessage=inputmessage;
	module=flow;
	var message = "";
	if(inputmessage=="facebook")
	{
		message = kony.i18n.getLocalizedString("keyNavigateMsngr");
    }
    else
    {
    	message = kony.i18n.getLocalizedString("keyNavigateLine");
    }
    var title = "";
	var yeslbl = kony.i18n.getLocalizedString("keyOK");
	var nolbl = kony.i18n.getLocalizedString("keyCancelButton");
    var basicConf = {
                message: message,
                alertType: constants.ALERT_TYPE_CONFIRMATION,
                alertTitle: title,
                yesLabel: yeslbl,
                noLabel: nolbl,
                alertHandler: callbackShareFFI
            };
            //Defining pspConf parameter for alert
            var pspConf = {};
            //Alert definition
            var infoAlert = kony.ui.Alert(basicConf, pspConf);
}


function callbackShareFFI(response)
{
	if(gblDeviceInfo["name"]=="android")
	{
		hideHeaderFooter();
	}
	else
	{
	}
	hideHeaderFooter();
	if (response == true)
	{
     	shareToSocialMedia(shareMessage,module);
		try {
			kony.timer.cancel("displayTimer")
		} catch (e) {
		}
		kony.timer.schedule("displayTimer", displayHeaderFooter, 2, false);
	    }
    else
    {
        displayHeaderFooter();   
    }
	
}


function screenShotCall(message)
{
	hideHeaderFooter();
	try
	{
		kony.timer.cancel("takescreenshot")
	} catch (e)
	{
	}
	shareMessage=message;	
	kony.timer.schedule("takescreenshot", shareToSocialMediascreenShot, 2, false);
	
}

function shareToSocialMediascreenShot()
{
	shareToSocialMedia(shareMessage)
	displayHeaderFooter();
	//alert(kony.i18n.getLocalizedString("keySaveImageMessage"));
}


function shareIntentmoreCall(message, module)
{
	hideHeaderFooter();
	try
	{
		kony.timer.cancel("shareIntent")
	} catch (e)
	{
	}
	shareMessage=message;
	iphonemodule=module;	
	kony.timer.schedule("shareIntent", shareToSocialMediaIntent, 2, false);
	
}

function shareToSocialMediaIntent()
{
	shareToSocialMedia(shareMessage, iphonemodule)
	displayHeaderFooter();
}

function shareIntentmoreCallandroid(message, module)
{
	hideHeaderFooter();
	shareToSocialMedia(message,module)
	try
	{
		kony.timer.cancel("shareIntentdis")
	} catch (e)
	{
	}
	shareMessage=message;	
	kony.timer.schedule("shareIntentdis", shareToSocialMediaIntentand, 2, false);
	
}

function shareToSocialMediaIntentand()
{
	displayHeaderFooter();
	if(shareMessage=="screenshot")
	{
		    var message = kony.i18n.getLocalizedString("keySaveImageMessage");
			var title = "";
			var yeslbl = kony.i18n.getLocalizedString("keyOK");
			var nolbl = "";
		    var basicConf = {
                message: message,
                alertType: constants.ALERT_TYPE_INFO,
                alertTitle: title,
                yesLabel: yeslbl,
                noLabel: nolbl
            };
            //Defining pspConf parameter for alert
            var pspConf = {};
            //Alert definition
            var infoAlert = kony.ui.Alert(basicConf, pspConf);
	}
	
}

function onClickShareBillpayCalendar()
{
	
	if(frmBillPaymentCompleteCalendar.imgTransNBpAckComplete.src == "icon_notcomplete.png" || frmBillPaymentCompleteCalendar.imgTransNBpAckComplete.src == "mes2.png")
	{
		alert(kony.i18n.getLocalizedString("Err_ShareFailed"));
		return;
	}
	else
	{
		if(frmBillPaymentCompleteCalendar.hbxShareOption.isVisible){
				frmBillPaymentCompleteCalendar.hbxShareOption.isVisible = false;
				frmBillPaymentCompleteCalendar.imgHeaderMiddle.src = "arrowtop.png";
		}else{
				frmBillPaymentCompleteCalendar.hbxShareOption.isVisible = true;
				frmBillPaymentCompleteCalendar.imgHeaderMiddle.src = "empty.png";
		}
	}
}
