function verifyPinService() {
    glbPinArr[1] = glbPin;
    if (glbPinArr[0] != glbPinArr[1]) {
        showAlertWithCallBack(kony.i18n.getLocalizedString("keyAssignPinNotMatch"), kony.i18n.getLocalizedString("info"), clearATMPINMB);
    } else {
        assignPinService();
    }
}
var glbActivateDCStatus = false;

function frmMBActivateDebitCardCompletePreShow() {
    changeStatusBarColor();
    frmMBActivateDebitCardComplete.lblTxtHeader.text = kony.i18n.getLocalizedString("keyPinAssign");
    frmMBActivateDebitCardComplete.btnNext.text = kony.i18n.getLocalizedString("keybtnReturn");
    loadActivateDebitCardStatus();
    gblLocale = false;
}

function loadActivateDebitCardStatus() {
    var src = "completeico.png";
    var cardNo = frmMBAssignAtmPin.lblCardNickName.text;
    var messageHdr = kony.i18n.getLocalizedString('Complete');
    var message = kony.i18n.getLocalizedString('keyAssignPinCompleted').replace("[cardno]", cardNo);
    if (!glbActivateDCStatus) {
        src = "iconnotcomplete.png";
        messageHdr = kony.i18n.getLocalizedString('keyCalendarFailed');
        message = kony.i18n.getLocalizedString('keyAssignPinFailed').replace("[nl]", "\n");
    }
    frmMBActivateDebitCardComplete.imageStatus.src = src;
    frmMBActivateDebitCardComplete.lblTxtHeader.text = messageHdr;
    frmMBActivateDebitCardComplete.lblStatusDetails.text = message;
}

function assignPinService() {
    //service call shoud be done
    //Sample value
    encryptPinUsingE2EE(decryptPinDigit(glbPin));
    //			var RPIN = encryptPinUsingE2EE(glbPin);
    //			alert("RPIN: "+RPIN);
}

function debitcardAcivationConfirm() {
    if (flowSpa) {
        if (gblIBFlowStatus == "04") {
            alertUserStatusLocked();
        } else {
            var inputParams = {}
            spaChnage = "addbillertopup"
            gblOTPFlag = true;
            try {
                kony.timer.cancel("otpTimer")
            } catch (e) {}
            //input params for SPA OTP
            var locale = kony.i18n.getCurrentLocale();
            gblSpaChannel = "ActivateCard";
            onClickOTPRequestSpa();
        }
    } else {
        if (checkMBUserStatus()) {
            showLoadingScreen();
            MBCardActivateDepositAccountInquiryService();
        }
    }
}

function verifypwdForDebitCardActivation(tranPassword) {
    if (tranPassword == null || tranPassword == '') {
        setTransPwdFailedError(kony.i18n.getLocalizedString("emptyMBTransPwd"));
        return false;
    } else {
        showLoadingScreen();
        popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("keyTransPwdMsg");
        popupTractPwd.lblPopupTract7.skin = lblPopupLabelTxt;
        popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = tbxPopupBlue;
        popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = tbxPopupBlue;
        popupTractPwd.hbxPopupTranscPwd.skin = tbxPopupBlue;
        var inputParam = {};
        inputParam["password"] = tranPassword;
        inputParam["notificationAdd_appID"] = appConfig.appId;
        inputParam["verifyPwdMB_loginModuleId"] = "MB_TxPwd";
        inputParam["verifyPwdMB_retryCounterVerifyAccessPin"] = "0";
        inputParam["verifyPwdMB_retryCounterVerifyTransPwd"] = "0";
        invokeServiceSecureAsync("atmPINverifyOTPComposite", inputParam, callBackVerifyTxnPwdServiceForAssignPIN);
    }
}

function callBackVerifyTxnPwdServiceForAssignPIN(status, resulttable) {
    if (status == 400) {
        dismissLoadingScreen();
        if (resulttable["opstatus"] == 0) {
            //Do success
            popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("keyTransPwdMsg");
            popupTractPwd.lblPopupTract7.skin = lblPopupLabelTxt;
            popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = tbxPopupBlue;
            popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = tbxPopupBlue;
            popupTractPwd.hbxPopupTranscPwd.skin = tbxPopupBlue;
            popupTractPwd.dismiss();
            var cardDetails = frmAccountDetailsMB.segDebitCard.selectedItems[0];
            frmMBAssignAtmPin.lblCardNickName.text = cardDetails["lblCreditCardNo"];
            frmMBAssignAtmPin.imgAccountDetailsPic.src = cardDetails["imgCreditCard"];
            initializeAndNavigatetoPinAssign();
        } else if (resulttable["errCode"] == "VrfyTxPWDErr00001" || resulttable["errCode"] == "VrfyTxPWDErr00002") {
            setTransPwdFailedError(kony.i18n.getLocalizedString("invalidTxnPwd"));
        } else if (resulttable["errCode"] == "VrfyTxPWDErr00003") {
            showTranPwdLockedPopup();
        } else if (resulttable["opstatus"] == 8005) {
            dismissLoadingScreen();
            if (resulttable["errCode"] == "VrfyOTPErr00001") {
                gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
                dismissLoadingScreen();
                alert("" + kony.i18n.getLocalizedString("invalidOTP"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00002") {
                dismissLoadingScreen();
                alert("" + kony.i18n.getLocalizedString("ECVrfyOTPErr"));
                //startRcCrmUpdateProfilBPIB("04");
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00005") {
                dismissLoadingScreen();
                alert("" + kony.i18n.getLocalizedString("KeyTokenSerialNumError"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00006") {
                dismissLoadingScreen();
                gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
                alert("" + resulttable["errMessage"]);
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00004") {
                dismissLoadingScreen();
                alert("" + resulttable["errMsg"]);
                return false;
            } else {
                dismissLoadingScreen();
                alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
                return false;
            }
        } else {
            popupTractPwd.dismiss();
            alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
        }
    }
}

function MBCardActivateDepositAccountInquiryService() {
    var inputparam = {};
    inputparam["activateCard"] = "yes";
    inputparam["selectedCard"] = frmAccountDetailsMB.segDebitCard.selectedIndex[1] + "";
    inputparam["acctId"] = gblAccountTable["custAcctRec"][gblIndex]["accId"];
    var status = invokeServiceSecureAsync("depositAccountInquiry", inputparam, MBCardActivateDepositAccountInquiryCallBack);
}

function MBCardActivateDepositAccountInquiryCallBack(status, resulttable) {
    if (status == 400) {
        dismissLoadingScreen();
        if (resulttable["opstatus"] == 0) {
            initOnlineATMPIN();
        } else {
            showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
        }
    }
}

function showTxnPasswordPopupActivateDebitCard() {
    var lblText = kony.i18n.getLocalizedString("transPasswordSub");
    var refNo = "";
    var mobNO = "";
    showOTPPopup(lblText, refNo, mobNO, verifypwdForDebitCardActivation, 3);
}