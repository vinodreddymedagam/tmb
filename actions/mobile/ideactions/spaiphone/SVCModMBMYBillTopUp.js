myTopUpListRs = [];
myTopUpSuggestListRs = [];
myBillerTopupListMB = [];
currentCatSelected = 0;
myTopupListMB = [];
myTopupSuggestListMB = [];
mySelectBillerListMB = [];
mySelectBillerSuggestListMB = [];
isSearched = false;
isCatSelected = false;
myTopUpSelect = [];
completeMyTopUpSelectList = [];
myTopupSelectListMB = [];
gblTopUpSelectFormDataHolder = [];
gblBillerIdMB = "";
gblBillerMethod = "";
gblRef2FlagMB = "";
gblRef1LenMB = "";
gblRef2LenMB = "";
gblIsRef2RequiredMB = "";
gblTranxLockedForBiller = "";
isfirstCallToMaster = true;
searchFlagForLoadMore = false;
TempDataBillers = [];
//gblstepMinAmount = [];
/** To get CustmerBillInquiry Service for My TopUp
 * Input ReqUID,CRMID,RecSelect
 */
function getMyTopUpListMB() {
    frmMyTopUpList.segBillersList.removeAll();
    var inputParams = {
        IsActive: "1"
            //crmId: gblcrmId,
            //clientDate: getCurrentDate()
    };
    showLoadingScreen();
    invokeServiceSecureAsync("customerBillInquiry", inputParams, startMyTopUpListMBServiceAsyncCallback);
}

function startMyTopUpListMBServiceAsyncCallback(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == "0") {
            var responseData = callBackResponse["CustomerBillInqRs"];
            gblMyBillList = [];
            if (responseData.length > 0) {
                dismissLoadingScreen();
                gblMyBillList = responseData;
                //populateMyTopUpMB(callBackResponse["CustomerBillInqRs"]);
                if (kony.application.getCurrentForm().id == 'frmMyTopUpList' || kony.application.getPreviousForm().id == 'frmMyTopUpList') {
                    populateMyTopUpMB(callBackResponse["CustomerBillInqRs"]);
                } else if (kony.application.getCurrentForm().id == 'frmSelectBiller' || kony.application.getPreviousForm().id == 'frmSelectBiller') {
                    //populateMyTopUpMBForSelectBiller(callBackResponse["CustomerBillInqRs"]);
                    populateMyBillerMBForSelectBiller(callBackResponse["CustomerBillInqRs"]);
                }
            } else {
                dismissLoadingScreen();
                if (kony.application.getCurrentForm().id == 'frmSelectBiller' || kony.application.getPreviousForm().id == 'frmSelectBiller') {
                    frmSelectBiller.segMyBills.setVisibility(false);
                }
                myTopupListMB = [];
                frmMyTopUpList.lblNoBill.setVisibility(true);
                //alert(kony.i18n.getLocalizedString("keyaddbillerstolist"));
                //alert(kony.i18n.getLocalizedString("ECGenericError"));
            }
        } else {
            dismissLoadingScreen();
            if (kony.application.getCurrentForm().id == 'frmSelectBiller' || kony.application.getPreviousForm().id == 'frmSelectBiller') {}
            alert(kony.i18n.getLocalizedString("ECGenericError"));
        }
    } else {
        if (status == 300) {
            dismissLoadingScreen();
            if (kony.application.getCurrentForm().id == 'frmSelectBiller' || kony.application.getPreviousForm().id == 'frmSelectBiller') {}
            alert(kony.i18n.getLocalizedString("ECGenericError"));
        }
    }
}

function getCustomerBillersList() {
    var inputParams = {
        IsActive: "1"
    };
    showLoadingScreen();
    invokeServiceSecureAsync("customerBillInquiry", inputParams, callBackGetCustomerBillersList);
}

function callBackGetCustomerBillersList(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == "0") {
            var responseData = callBackResponse["CustomerBillInqRs"];
            if (responseData.length > 0) {
                gblMyBillList = responseData;
            } else {
                gblMyBillList = [];
            }
        } else {
            alert(kony.i18n.getLocalizedString("ECGenericError"));
        }
    } else {
        if (status == 300) {
            alert(kony.i18n.getLocalizedString("ECGenericError"));
        }
    }
    dismissLoadingScreen();
}
var gbllocaleMBBillerTopup = "";
//function getLocaleBillerMB() {
//	gbllocaleMBBillerTopup = "en_EN" //kony.i18n.getCurrentLocale();
//}
var gblCustomerBillList = [];

function populateMyTopUpMB(collectionData) {
    var billername = "BillerNameEN";
    var ref1label = "";
    var ref2label = "";
    search = "";
    search = frmMyTopUpList.txbSearch.text;
    if (search != null) {
        var searchtxt = search.toLowerCase();
    }
    var i = 0;
    var tmpSearchList = [];
    tmpSearchList.length = 0;
    var tmpRef2Value = "";
    var tmpRef2 = "";
    var tmpRef2label = "";
    var tmpIsRef2Req = "";
    var locale = kony.i18n.getCurrentLocale();
    myBillerTopupListMB = collectionData;
    if (kony.string.startsWith(locale, "en", true) == true) {
        billername = "BillerNameEN";
        ref1label = "LabelReferenceNumber1EN";
        ref2label = "LabelReferenceNumber2EN";
    } else if (kony.string.startsWith(locale, "th", true) == true) {
        billername = "BillerNameTH";
        ref1label = "LabelReferenceNumber1TH";
        ref2label = "LabelReferenceNumber2TH";
    }
    if (!isSearched) {
        myTopUpListRs.length = 0;
        for (var i = 0; i < collectionData.length; i++) {
            if (collectionData[i]["BillerGroupType"] == gblMyBillerTopUpBB) {
                if (gblMyBillerTopUpBB == gblGroupTypeBiller) {
                    tmpIsRef2Req = collectionData[i]["IsRequiredRefNumber2Add"];
                    if (tmpIsRef2Req == "Y" || tmpIsRef2Req == "y") {
                        tmpRef2label = collectionData[i][ref2label];
                        tmpRef2Value = collectionData[i]["ReferenceNumber2"];
                    } else {
                        tmpRef2label = "";
                        tmpRef2Value = "";
                    }
                } else {
                    tmpRef2label = "";
                    tmpRef2Value = "";
                }
                var imagesUrl = BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + collectionData[i]["BillerCompcode"] + "&modIdentifier=MyBillers";
                var billerNameEN = collectionData[i]["BillerNameEN"] + " (" + collectionData[i]["BillerCompcode"] + ")";
                var billerNameTH = collectionData[i]["BillerNameTH"] + " (" + collectionData[i]["BillerCompcode"] + ")";
                var tempRecord = {
                    "crmId": collectionData[i]["CRMID"],
                    "lblBillerNickname": {
                        "text": collectionData[i]["BillerNickName"]
                    },
                    "lblBillerName": {
                        "text": collectionData[i][billername] + " (" + collectionData[i]["BillerCompcode"] + ")"
                    },
                    "lblRef1Value": {
                        "text": collectionData[i]["ReferenceNumber1"]
                    },
                    "lblRef1ValueMasked": {
                        "text": maskCreditCard(collectionData[i]["ReferenceNumber1"])
                    },
                    "lblRef2Value": {
                        "text": tmpRef2Value
                    },
                    "imgBillerLogo": {
                        "src": imagesUrl
                    },
                    "imgBillersRtArrow": {
                        "src": "bg_arrow_right.png"
                    },
                    "lblRef1": {
                        "text": collectionData[i][ref1label]
                    },
                    "lblRef2": {
                        "text": tmpRef2label
                    },
                    "lblRef1TH": collectionData[i]["LabelReferenceNumber1TH"],
                    "lblRef2TH": collectionData[i]["LabelReferenceNumber2TH"],
                    "lblRef1EN": collectionData[i]["LabelReferenceNumber1EN"],
                    "lblRef2EN": collectionData[i]["LabelReferenceNumber2EN"],
                    "lblBillerNameEN": billerNameEN,
                    "lblBillerNameTH": billerNameTH,
                    "IsFullPayment": collectionData[i]["IsFullPayment"],
                    "BillerID": {
                        "text": collectionData[i]["BillerID"]
                    },
                    "BillerCompCode": {
                        "text": collectionData[i]["BillerCompcode"]
                    },
                    "BeepndBill": {
                        "text": collectionData[i]["BeepNBillFlag"]
                    },
                    "CustomerBillerID": {
                        "text": collectionData[i]["CustomerBillID"]
                    },
                    "BillerCategoryID": {
                        "text": collectionData[i]["BillerCategoryID"]
                    }
                }
                myTopUpListRs.push(tempRecord);
            }
        }
        myTopupListMB = myTopUpListRs;
        gblCustomerBillList = [];
        gblCustomerBillList = myTopUpListRs;
        if (myTopUpListRs.length > 0) {
            //frmMyTopUpList.segBillersList.removeAll();
            frmMyTopUpList.segBillersList.setData(myTopUpListRs);
        } else {
            if (gblMyBillerTopUpBB == gblGroupTypeBiller)
            //alert(kony.i18n.getLocalizedString("keyaddbillerstolist"));
                frmMyTopUpList.lblNoBill.setVisibility(true); //Added by Bhaskar
            else
            //alert(kony.i18n.getLocalizedString("noTopUpMB"));
            //
                frmMyTopUpList.lblNoBill.setVisibility(true); //Added by Bhaskar
        }
    } else if (isSearched) {
        for (var i = 0; i < collectionData.length; i++) {
            if (collectionData[i]["BillerNickName"]) {
                var lowerCaseName = collectionData[i]["BillerNickName"].toLowerCase();
                if (kony.string.startsWith(lowerCaseName, searchtxt, true)) {
                    if (collectionData[i]["BillerGroupType"] == gblMyBillerTopUpBB) {
                        if (gblMyBillerTopUpBB == gblGroupTypeBiller) {
                            tmpIsRef2Req = collectionData[i]["IsRequiredRefNumber2Add"];
                            if (tmpIsRef2Req == "Y" || tmpIsRef2Req == "y") {
                                tmpRef2label = collectionData[i][ref2label];
                                tmpRef2Value = collectionData[i]["ReferenceNumber2"];
                            }
                        }
                        var billerNameEN = collectionData[i]["BillerNameEN"] + " (" + collectionData[i]["BillerCompcode"] + ")";
                        var billerNameTH = collectionData[i]["BillerNameTH"] + " (" + collectionData[i]["BillerCompcode"] + ")";
                        var tempRecord = {
                            "crmId": collectionData[i]["CRMID"],
                            "lblBillerNickname": {
                                "text": collectionData[i]["BillerNickName"]
                            },
                            "lblBillerName": {
                                "text": collectionData[i][billername] + " (" + collectionData[i]["BillerCompcode"] + ")"
                            },
                            "lblRef1Value": {
                                "text": collectionData[i]["ReferenceNumber1"]
                            },
                            "lblRef1ValueMasked": {
                                "text": maskCreditCard(collectionData[i]["ReferenceNumber1"])
                            },
                            "lblRef2Value": {
                                "text": tmpRef2Value
                            },
                            "imgBillerLogo": {
                                "src": "bill_img01.png"
                            },
                            "imgBillersRtArrow": {
                                "src": "bg_arrow_right.png"
                            },
                            "lblRef1": {
                                "text": collectionData[i][ref1label]
                            },
                            "lblRef2": {
                                "text": tmpRef2label
                            },
                            "lblRef1TH": collectionData[i]["LabelReferenceNumber1TH"],
                            "lblRef2TH": collectionData[i]["LabelReferenceNumber2TH"],
                            "lblRef1EN": collectionData[i]["LabelReferenceNumber1EN"],
                            "lblRef2EN": collectionData[i]["LabelReferenceNumber2EN"],
                            "lblBillerNameEN": billerNameEN,
                            "lblBillerNameTH": billerNameTH,
                            "BillerID": {
                                "text": collectionData[i]["BillerID"]
                            },
                            "BillerCompCode": {
                                "text": collectionData[i]["BillerCompcode"]
                            },
                            "BeepndBill": {
                                "text": collectionData[i]["BeepNBillFlag"]
                            },
                            "CustomerBillerID": {
                                "text": collectionData[i]["CustomerBillID"]
                            },
                            "BillerCategoryID": {
                                "text": collectionData[i]["BillerCategoryID"]
                            }
                        }
                        tmpSearchList.push(tempRecord);
                    }
                }
            }
        }
        //new comm frmMyTopUpList.segBillersList.removeAll();
        frmMyTopUpList.segBillersList.setData(tmpSearchList);
    }
}
/**
 * to get Biller category for MyTopUp MB
 */
function startDisplayTopUpCategoryService() {
    var tmpBillerCatGroupType = "";
    if (gblMyBillerTopUpBB == 0) {
        tmpBillerCatGroupType = gblBillerCategoryGroupType;
    } else if (gblMyBillerTopUpBB == 1) {
        tmpBillerCatGroupType = gblTopupCategoryGroupType;
    }
    var inputParams = {
        BillerCategoryGroupType: tmpBillerCatGroupType,
        clientDate: getCurrentDate()
    };
    showLoadingScreen();
    invokeServiceSecureAsync("billerCategoryInquiry", inputParams, startDisplayMyTopUpBillerCatListServiceAsyncCallback);
}

function startDisplayMyTopUpBillerCatListServiceAsyncCallback(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == "0") {
            var collectionData = callBackResponse["BillerCategoryInqRs"];
            var masterData = [];
            masterData.push({
                "lblCategory": {
                    "text": kony.i18n.getLocalizedString("keyBillPaymentSelectCategory")
                },
                "BillerCategoryID": {
                    "text": "0"
                }
            });
            var tempData = "";
            var tmpCatDesc = "";
            var locale = kony.i18n.getCurrentLocale();
            if (kony.string.startsWith(locale, "en", true) == true) tmpCatDesc = "BillerCategoryDescEN";
            else if (kony.string.startsWith(locale, "th", true) == true) tmpCatDesc = "BillerCategoryDescTH";
            for (var i = 0; i < collectionData.length; i++) {
                invokeCommonIBLogger("BillerCategoryID" + collectionData[i]["BillerCategoryID"] + "BILLER NAME " + collectionData[i]
                    [tmpCatDesc]);
                tempData = {
                    "lblCategory": {
                        "text": collectionData[i][tmpCatDesc]
                    },
                    "BillerCategoryID": {
                        "text": collectionData[i]["BillerCategoryID"]
                    }
                }
                masterData.push(tempData);
            }
            popUpMyBillers.segPop.setData(masterData);
            popUpMyBillers.containerHeight = 80;
            //constants.HEIGHT_BY_DEVICE_REFERENCE not defined in IDE 6.0.2
            //popUpMyBillers.containerHeightReference = constants.HEIGHT_BY_DEVICE_REFERENCE;
            dismissLoadingScreen();
            popUpMyBillers.show();
        } else {
            var masterData = [];
            var tempData = {
                "lblCategory": {
                    "text": kony.i18n.getLocalizedString("keyBillPaymentSelectCategory")
                },
                "BillerCategoryID": {
                    "text": "0"
                }
            }
            masterData.push(tempData);
            popUpMyBillers.segPop.setData(masterData);
            dismissLoadingScreen();
            popUpMyBillers.show();
            alert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"));
        }
    } else {
        if (status == 300) {
            var masterData = [];
            var tempData = {
                "lblCategory": {
                    "text": kony.i18n.getLocalizedString("keyBillPaymentSelectCategory")
                },
                "BillerCategoryID": {
                    "text": "0"
                }
            }
            masterData.push(tempData);
            popUpMyBillers.segPop.setData(masterData);
            dismissLoadingScreen();
            popUpMyBillers.show();
            alert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"));
        }
    }
}
/**
 * Function for displaying billers/topups in master list to add to customer bills/topup list.
 *
 */
function setDataOnsegSelectTopUpFromService() {
    var billerGroupType = "";
    if (gblMyBillerTopUpBB == 0) {
        billerGroupType = "0";
    } else if (gblMyBillerTopUpBB == 1) {
        billerGroupType = "1";
    } else {
        billerGroupType = "0";
    }
    if (isSearched) {
        var inputParams = {
            IsActive: "1",
            BillerGroupType: billerGroupType,
            billerName: gblsearchtxt,
            flagBillerList: "MB"
        };
    } else {
        var inputParams = {
            IsActive: "1",
            BillerGroupType: billerGroupType,
            flagBillerList: "MB"
        };
    }
    showLoadingScreen();
    invokeServiceSecureAsync("masterBillerInquiry", inputParams, displayAllMasterBillerListServiceCallback);
}

function displayAllBillerCategoryServiceCallback(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == "0") {} else {}
    }
}

function displayAllMasterBillerListServiceCallback(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == "0") {
            var responseData = callBackResponse["MasterBillerInqRs"];
            if (responseData.length > 0) {
                gblstepMinAmount = [];
                populatefrmMyTopUpSelect(callBackResponse["MasterBillerInqRs"]);
                dismissLoadingScreen();
            } else {
                frmMyTopUpSelect.segSelectList.removeAll();
                frmMyTopUpSelect.hbxLink.setVisibility(false);
                dismissLoadingScreen();
                // alert("No Suggested Biller  found");
            }
        } else {
            frmMyTopUpSelect.segSelectList.removeAll();
            dismissLoadingScreen();
            //alert("No Suggested Biller found");
        }
    } else {
        if (status == 300) {
            frmMyTopUpSelect.segSelectList.removeAll();
            dismissLoadingScreen();
            //alert("No Suggested Biller found");
        }
    }
}

function populatefrmMyTopUpSelect(collectiondata) {
    myTopUpSelect.length = 0;
    var tmpRef2Len = 0;
    var billername = "";
    var billerCatName = "";
    var ref1label = "";
    var ref2label = "";
    var tmpRef2label = "";
    var masterData = [];
    var locale = kony.i18n.getCurrentLocale();
    var tmpIsRef2Req = "";
    if (kony.string.startsWith(locale, "en", true) == true) {
        billername = "BillerNameEN";
        billerCatName = "BillerCatgoryNameEN";
        ref1label = "LabelReferenceNumber1EN";
        ref2label = "LabelReferenceNumber2EN";
    } else if (kony.string.startsWith(locale, "th", true) == true) {
        billername = "BillerNameTH";
        billerCatName = "BillerCatgoryNameTH";
        ref1label = "LabelReferenceNumber1TH";
        ref2label = "LabelReferenceNumber2TH";
    }
    if (!isCatSelected) {
        for (var i = 0; i < collectiondata.length; i++) {
            /* minimum amount locha */
            var billerMethod = collectiondata[i]["BillerMethod"];
            if (billerMethod == "1" && collectiondata[i]["BillerGroupType"] == gblGroupTypeTopup && collectiondata[i]["BillerCompcode"] != "2605") {
                var min = collectiondata[i]["StepAmount"][0]["Amt"];
                tempMinAmount = {
                    "compcode": collectiondata[i]["BillerCompcode"],
                    "minAmount": min
                }
            } else {
                tempMinAmount = [];
            }
            if (collectiondata[i]["BillerGroupType"] == gblMyBillerTopUpBB || gblMyBillerTopUpBB == 2) {
                tmpIsRef2Req = collectiondata[i]["IsRequiredRefNumber2Add"];
                if (gblMyBillerTopUpBB == 0 && (tmpIsRef2Req == "Y" || tmpIsRef2Req == "y")) {
                    tmpRef2label = collectiondata[i][ref2label];
                    tmpRef2Value = collectiondata[i]["ReferenceNumber2"];
                    tmpRef2Len = collectiondata[i]["Ref2Len"];
                } else {
                    tmpRef2label = "";
                    tmpRef2Value = "";
                    tmpRef2Len = 0;
                }
                var imagesUrl = BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + collectiondata[i]["BillerCompcode"] + "&modIdentifier=MyBillers";
                var billerNameEN = collectiondata[i]["BillerNameEN"] + " (" + collectiondata[i]["BillerCompcode"] + ")";
                var billerNameTH = collectiondata[i]["BillerNameTH"] + " (" + collectiondata[i]["BillerCompcode"] + ")";
                //ENH_113	
                gblbillerStartTime = "";
                gblbillerEndTime = "";
                gblbillerFullPay = "";
                gblbillerAllowSetSched = "";
                gblbillerTotalPayAmtEn = "";
                gblbillerTotalPayAmtTh = "";
                gblbillerTotalIntEn = "";
                gblbillerTotalIntTh = "";
                gblbillerDisconnectAmtEn = "";
                gblbillerDisconnectAmtTh = "";
                gblbillerServiceType = "";
                gblbillerTransType = "";
                gblMeaFeeAmount = "";
                gblbillerAmountEn = "";
                gblbillerAmountTh = "";
                gblbillerMeterNoEn = "";
                gblbillerMeterNoTh = "";
                gblbillerCustNameEn = "";
                gblbillerCustNameTh = "";
                gblbillerCustAddressEn = "";
                gblbillerCustAddressTh = "";
                if (collectiondata[i]["BillerCompcode"] == "2533") {
                    if (collectiondata[i]["ValidChannel"] != undefined && collectiondata[i]["ValidChannel"].length > 0) {
                        for (var t = 0; t < collectiondata[i]["ValidChannel"].length; t++) {
                            if (collectiondata[i]["ValidChannel"][t]["ChannelCode"] == "01") {
                                gblMeaFeeAmount = collectiondata[i]["ValidChannel"][t]["FeeAmount"];
                            }
                        }
                    }
                    if (collectiondata[i]["BillerMiscData"] != undefined && collectiondata[i]["BillerMiscData"].length > 0) {
                        for (var j = 0; j < collectiondata[i]["BillerMiscData"].length; j++) {
                            if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BILLER.StartTime") {
                                gblbillerStartTime = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BILLER.EndTime") {
                                gblbillerEndTime = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BILLER.FullPayment") {
                                gblbillerFullPay = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BILLER.AllowSetSchedule") {
                                gblbillerAllowSetSched = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD1.LABEL.EN") {
                                gblbillerMeterNoEn = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD1.LABEL.TH") {
                                gblbillerMeterNoTh = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD2.LABEL.EN") {
                                gblbillerCustNameEn = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD2.LABEL.TH") {
                                gblbillerCustNameTh = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD3.LABEL.EN") {
                                gblbillerCustAddressEn = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD3.LABEL.TH") {
                                gblbillerCustAddressTh = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD4.LABEL.EN") {
                                gblbillerTotalPayAmtEn = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD4.LABEL.TH") {
                                gblbillerTotalPayAmtTh = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD5.LABEL.EN") {
                                gblbillerAmountEn = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD5.LABEL.TH") {
                                gblbillerAmountTh = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD6.LABEL.EN") {
                                gblbillerTotalIntEn = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD6.LABEL.TH") {
                                gblbillerTotalIntTh = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD7.LABEL.EN") {
                                gblbillerDisconnectAmtEn = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD7.LABEL.TH") {
                                gblbillerDisconnectAmtTh = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BILLER.ServiceType") {
                                gblbillerServiceType = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BILLER.TransactionType") {
                                gblbillerTransType = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                            }
                        }
                    }
                }
                //ENH113 end
                var tempRecord = {
                    "lblSuggestedBiller": {
                        "text": collectiondata[i][billername] + " (" + collectiondata[i]["BillerCompcode"] + ")"
                    },
                    "lblBillerNameEN": billerNameEN,
                    "lblBillerNameTH": billerNameTH,
                    "ToAccountKey": {
                        "text": collectiondata[i]["ToAccountKey"]
                    },
                    "lblBillerCategory": {
                        "text": collectiondata[i][billerCatName]
                    },
                    "btnAddBiller": {
                        "text": "  ",
                        "skin": "btnAdd"
                    },
                    "BillerID": {
                        "text": collectiondata[i]["BillerID"]
                    },
                    "BillerCompCode": {
                        "text": collectiondata[i]["BillerCompcode"]
                    },
                    "BarcodeOnly": collectiondata[i]["BarcodeOnly"],
                    "BillerGroupType": collectiondata[i]["BillerGroupType"],
                    "IsFullPayment": collectiondata[i]["IsFullPayment"],
                    "Ref1Label": {
                        "text": collectiondata[i][ref1label]
                    },
                    "Ref2Label": {
                        "text": tmpRef2label
                    },
                    "imgSuggestedBiller": {
                        "src": imagesUrl
                    },
                    "IsRequiredRefNumber2Add": {
                        "text": collectiondata[i]["IsRequiredRefNumber2Add"]
                    },
                    "IsRequiredRefNumber2Pay": {
                        "text": collectiondata[i]["IsRequiredRefNumber2Pay"]
                    },
                    "EffDt": {
                        "text": collectiondata[i]["EffDt"]
                    },
                    "BillerMethod": {
                        "text": collectiondata[i]["BillerMethod"]
                    },
                    "Ref1Len": {
                        "text": collectiondata[i]["Ref1Len"]
                    },
                    "Ref2Len": {
                        "text": tmpRef2Len
                    },
                    "BillerCategoryID": {
                        "text": collectiondata[i]["BillerCategoryID"]
                    },
                    "hdnLabelRefNum1EN": {
                        "text": collectiondata[i]["LabelReferenceNumber1EN"]
                    },
                    "hdnLabelRefNum1TH": {
                        "text": collectiondata[i]["LabelReferenceNumber1TH"]
                    },
                    "hdnLabelRefNum2EN": {
                        "text": collectiondata[i]["LabelReferenceNumber2EN"]
                    },
                    "hdnLabelRefNum2TH": {
                        "text": collectiondata[i]["LabelReferenceNumber2TH"]
                    },
                    "billerStartTime": gblbillerStartTime,
                    "billerEndTime": gblbillerEndTime,
                    "billerFullPay": gblbillerFullPay,
                    "billerAllowSetSched": gblbillerAllowSetSched,
                    "billerTotalPayAmtEn": gblbillerTotalPayAmtEn,
                    "billerTotalPayAmtTh": gblbillerTotalPayAmtTh,
                    "billerTotalIntEn": gblbillerTotalIntEn,
                    "billerTotalIntTh": gblbillerTotalIntTh,
                    "billerDisconnectAmtEn": gblbillerDisconnectAmtEn,
                    "billerDisconnectAmtTh": gblbillerDisconnectAmtTh,
                    "billerServiceType": gblbillerServiceType,
                    "billerTransType": gblbillerTransType,
                    "billerFeeAmount": gblMeaFeeAmount,
                    "billerAmountEn": gblbillerAmountEn,
                    "billerAmountTh": gblbillerAmountTh,
                    "billerMeterNoEn": gblbillerMeterNoEn,
                    "billerMeterNoTH": gblbillerMeterNoTh,
                    "billerCustNameEn": gblbillerCustNameEn,
                    "billerCustNameTh": gblbillerCustNameTh,
                    "billerCustAddressEn": gblbillerCustAddressEn,
                    "billerCustAddressTh": gblbillerCustAddressTh,
                    "billerBancassurance": collectiondata[i]["IsBillerBancassurance"],
                    "allowRef1AlphaNum": collectiondata[i]["AllowRef1AlphaNum"]
                }
                myTopUpSelect.push(tempRecord);
                if (tempMinAmount.length != 0) gblstepMinAmount.push(tempMinAmount);
            }
        }
        myTopupSelectListMB = myTopUpSelect;
        myTopupSuggestListMB = myTopUpSelect; ////// for issue 4468
        gblTopUpSelectFormDataHolder = myTopUpSelect;
        onMyTopUpSelectMoreDynamic(gblTopUpSelectFormDataHolder);
    } else {
        for (var i = 0; i < collectiondata.length; i++) {
            /* minimum amount locha */
            var billerMethod = collectiondata[i]["BillerMethod"];
            if (billerMethod == "1" && collectiondata[i]["BillerGroupType"] == gblGroupTypeTopup) {
                var min = collectiondata[i]["StepAmount"][0]["Amt"];
                tempMinAmount = {
                    "compcode": collectiondata[i]["BillerCompcode"],
                    "minAmount": min
                }
            } else {
                tempMinAmount = [];
            }
            if (collectiondata[i]["BillerGroupType"] == gblMyBillerTopUpBB) {
                tmpIsRef2Req = collectiondata[i]["IsRequiredRefNumber2Add"];
                if (gblMyBillerTopUpBB == 0 && (tmpIsRef2Req == "Y" || tmpIsRef2Req == "y")) {
                    tmpRef2label = collectiondata[i][ref2label];
                    tmpRef2Value = collectiondata[i]["ReferenceNumber2"];
                    tmpRef2Len = collectiondata[i]["Ref2Len"];
                } else {
                    tmpRef2label = "";
                    tmpRef2Value = "";
                    tmpRef2Len = collectiondata[i]["Ref2Len"];
                }
                //ENH_113	
                gblbillerStartTime = "";
                gblbillerEndTime = "";
                gblbillerFullPay = "";
                gblbillerAllowSetSched = "";
                gblbillerTotalPayAmtEn = "";
                gblbillerTotalPayAmtTh = "";
                gblbillerTotalIntEn = "";
                gblbillerTotalIntTh = "";
                gblbillerDisconnectAmtEn = "";
                gblbillerDisconnectAmtTh = "";
                gblbillerServiceType = "";
                gblbillerTransType = "";
                gblMeaFeeAmount = "";
                gblbillerAmountEn = "";
                gblbillerAmountTh = "";
                gblbillerMeterNoEn = "";
                gblbillerMeterNoTh = "";
                gblbillerCustNameEn = "";
                gblbillerCustNameTh = "";
                gblbillerCustAddressEn = "";
                gblbillerCustAddressTh = "";
                if (collectiondata[i]["BillerCompcode"] == "2533") {
                    if (collectiondata[i]["ValidChannel"] != undefined && collectiondata[i]["ValidChannel"].length > 0) {
                        for (var t = 0; t < collectiondata[i]["ValidChannel"].length; t++) {
                            if (collectiondata[i]["ValidChannel"][t]["ChannelCode"] == "01") {
                                gblMeaFeeAmount = collectiondata[i]["ValidChannel"][t]["FeeAmount"];
                            }
                        }
                    }
                    if (collectiondata[i]["BillerMiscData"] != undefined && collectiondata[i]["BillerMiscData"].length > 0) {
                        for (var j = 0; j < collectiondata[i]["BillerMiscData"].length; j++) {
                            if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BILLER.StartTime") {
                                gblbillerStartTime = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BILLER.EndTime") {
                                gblbillerEndTime = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BILLER.FullPayment") {
                                gblbillerFullPay = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BILLER.AllowSetSchedule") {
                                gblbillerAllowSetSched = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD1.LABEL.EN") {
                                gblbillerMeterNoEn = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD1.LABEL.TH") {
                                gblbillerMeterNoTh = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD2.LABEL.EN") {
                                gblbillerCustNameEn = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD2.LABEL.TH") {
                                gblbillerCustNameTh = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD3.LABEL.EN") {
                                gblbillerCustAddressEn = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD3.LABEL.TH") {
                                gblbillerCustAddressTh = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD4.LABEL.EN") {
                                gblbillerTotalPayAmtEn = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD4.LABEL.TH") {
                                gblbillerTotalPayAmtTh = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD5.LABEL.EN") {
                                gblbillerAmountEn = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD5.LABEL.TH") {
                                gblbillerAmountTh = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD6.LABEL.EN") {
                                gblbillerTotalIntEn = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD6.LABEL.TH") {
                                gblbillerTotalIntTh = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD7.LABEL.EN") {
                                gblbillerDisconnectAmtEn = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD7.LABEL.TH") {
                                gblbillerDisconnectAmtTh = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BILLER.ServiceType") {
                                gblbillerServiceType = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BILLER.TransactionType") {
                                gblbillerTransType = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                            }
                        }
                    }
                }
                //ENH113 end
                var imagesUrl = BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + collectiondata[i]["BillerCompcode"] + "&modIdentifier=MyBillers";
                if (collectiondata[i].BillerCategoryID.text == currentCatSelected) {
                    var tempRecord = {
                        "lblSuggestedBiller": collectiondata[i][billername] + " (" + collectiondata[i]["BillerCompcode"] + ")",
                        "btnAddBiller": {
                            "text": "  ",
                            "skin": "btnAdd"
                        },
                        "BillerID": {
                            "text": collectiondata[i]["BillerID"]
                        },
                        "BillerCompCode": {
                            "text": collectiondata[i]["BillerCompcode"]
                        },
                        "BarcodeOnly": collectiondata[i]["BarcodeOnly"],
                        "IsFullPayment": collectiondata[i]["IsFullPayment"],
                        "Ref1Label": {
                            "text": collectiondata[i][ref1label]
                        },
                        "Ref2Label": {
                            "text": tmpRef2label
                        },
                        "imgSuggestedBiller": {
                            "src": imagesUrl
                        },
                        "IsRequiredRefNumber2Add": {
                            "text": collectiondata[i]["IsRequiredRefNumber2Add"]
                        },
                        "IsRequiredRefNumber2Pay": {
                            "text": collectiondata[i]["IsRequiredRefNumber2Pay"]
                        },
                        "EffDt": {
                            "text": collectiondata[i]["EffDt"]
                        },
                        "BillerMethod": {
                            "text": collectiondata[i]["BillerMethod"]
                        },
                        "Ref1Len": {
                            "text": collectiondata[i]["Ref1Len"]
                        },
                        "Ref2Len": {
                            "text": tmpRef2Len
                        },
                        "BillerCategoryID": {
                            "text": collectiondata[i]["BillerCategoryID"]
                        },
                        "billerStartTime": gblbillerStartTime,
                        "billerEndTime": gblbillerEndTime,
                        "billerFullPay": gblbillerFullPay,
                        "billerAllowSetSched": gblbillerAllowSetSched,
                        "billerTotalPayAmtEn": gblbillerTotalPayAmtEn,
                        "billerTotalPayAmtTh": gblbillerTotalPayAmtTh,
                        "billerTotalIntEn": gblbillerTotalIntEn,
                        "billerTotalIntTh": gblbillerTotalIntTh,
                        "billerDisconnectAmtEn": gblbillerDisconnectAmtEn,
                        "billerDisconnectAmtTh": gblbillerDisconnectAmtTh,
                        "billerServiceType": gblbillerServiceType,
                        "billerTransType": gblbillerTransType,
                        "billerFeeAmount": gblMeaFeeAmount,
                        "billerAmountEn": gblbillerAmountEn,
                        "billerAmountTh": gblbillerAmountTh,
                        "billerMeterNoEn": gblbillerMeterNoEn,
                        "billerMeterNoTh": gblbillerMeterNoTh,
                        "billerCustNameEn": gblbillerCustNameEn,
                        "billerCustNameTh": gblbillerCustNameTh,
                        "billerCustAddressEn": gblbillerCustAddressEn,
                        "billerCustAddressTh": gblbillerCustAddressTh,
                        "billerBancassurance": collectiondata[i]["IsBillerBancassurance"],
                        "allowRef1AlphaNum": collectiondata[i]["AllowRef1AlphaNum"]
                    }
                    myTopUpSelect.push(tempRecord);
                    if (tempMinAmount != 0) gblstepMinAmount.push(tempMinAmount);
                }
            }
        }
        myTopupSelectListMB = myTopUpSelect;
        gblTopUpSelectFormDataHolder = myTopUpSelect;
        onMyTopUpSelectMoreDynamic(gblTopUpSelectFormDataHolder);
    }
}
/**
 * To get my TopUp Suusgested List
 */
function getMyTopUpSuggestListMB() {
    if (kony.application.getCurrentForm().id == 'frmSelectBiller' || kony.application.getPreviousForm().id == 'frmSelectBiller') {
        frmSelectBiller.segSuggestedBillers.removeAll();
        frmSelectBiller.segMyBills.removeAll();
    } else if (kony.application.getCurrentForm().id == 'frmMyTopUpList' || kony.application.getPreviousForm().id == 'frmMyTopUpList') {
        frmMyTopUpList.segSuggestedBillers.removeAll();
        frmMyTopUpList.segBillersList.removeAll();
    } else if (kony.application.getCurrentForm().id == 'frmMyTopUpSelect' || kony.application.getPreviousForm().id == 'frmMyTopUpSelect') {
        frmMyTopUpSelect.segSelectList.removeAll();
    }
    var billerGroupType = "";
    if (gblMyBillerTopUpBB == 0) {
        billerGroupType = "0";
    } else {
        billerGroupType = "1";
    }
    if (isSearched) {
        var inputParams = {
            IsActive: "1",
            BillerGroupType: billerGroupType,
            billerName: gblsearchtxt,
            flagBillerList: "MB"
        };
    } else {
        var inputParams = {
            IsActive: "1",
            BillerGroupType: billerGroupType,
            clientDate: getCurrentDate(),
            flagBillerList: "MB"
        };
    }
    if (!isSearched) {
        showLoadingScreen();
    }
    //showLoadingScreen();
    invokeServiceSecureAsync("masterBillerInquiry", inputParams, startMyTopUpSuggestListMBServiceAsyncCallback);
}

function startMyTopUpSuggestListMBServiceAsyncCallback(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == "0") {
            var responseData = callBackResponse["MasterBillerInqRs"];
            if (responseData.length > 0) {
                //	populateMyTopUpSuggestMB(callBackResponse["MasterBillerInqRs"]);
                if (kony.application.getCurrentForm().id == 'frmMyTopUpList' || kony.application.getCurrentForm().id == 'frmMyTopUpSelect' || kony.application.getPreviousForm().id == 'frmMyTopUpList' || kony.application.getPreviousForm().id == 'frmMyTopUpSelect') {
                    populateMyTopUpSuggestMB(callBackResponse["MasterBillerInqRs"]);
                } else if (kony.application.getCurrentForm().id == 'frmSelectBiller' || kony.application.getPreviousForm().id == 'frmSelectBiller') {
                    gblBillersFound = true;
                    frmSelectBiller.lblNoBillers.setVisibility(false);
                    populateMyBillerSuggestMBForSelectBiller(callBackResponse["MasterBillerInqRs"]);
                }
                if (isfirstCallToMaster) getMyTopUpListMB();
                isfirstCallToMaster = false;
                isSearched = false;
                if (kony.application.getCurrentForm().id == 'frmMyTopUpList' || kony.application.getPreviousForm().id == 'frmMyTopUpList') frmMyTopUpList.lblErrorMsg.setVisibility(false);
                if (kony.application.getCurrentForm().id == 'frmMyTopUpSelect' || kony.application.getPreviousForm().id == 'frmMyTopUpSelect') frmMyTopUpSelect.lblErrorMsg.setVisibility(false);
            } else {
                myTopupSuggestListMB = [];
                if (kony.application.getCurrentForm().id == 'frmMyTopUpList' || kony.application.getPreviousForm().id == 'frmMyTopUpList') {
                    frmMyTopUpList.lblSuggestedBillersText.setVisibility(false);
                    frmMyTopUpList.segSuggestedBillers.removeAll();
                } else if (kony.application.getCurrentForm().id == 'frmSelectBiller' || kony.application.getPreviousForm().id == 'frmSelectBiller') {
                    gblBillersFound = false;
                    if (gblBillerCatChangeFound != undefined && gblBillerCatChangeFound == false) {
                        frmSelectBiller.lblNoBillers.setVisibility(true);
                        //frmSelectBiller.hboxMoreSelectBiller.setVisibility(false);
                    }
                    frmSelectBiller.segSuggestedBillers.removeAll();
                } else if (kony.application.getCurrentForm().id == 'frmMyTopUpSelect' || kony.application.getPreviousForm().id == 'frmMyTopUpSelect') {
                    frmMyTopUpSelect.segSelectList.removeAll();
                    frmMyTopUpSelect.hbxLink.setVisibility(false);
                }
                dismissLoadingScreen();
                if (isSearched) {
                    //alert(kony.i18n.getLocalizedString("keybillernotfound"));
                    if (kony.application.getCurrentForm().id == 'frmMyTopUpList' || kony.application.getPreviousForm().id == 'frmMyTopUpList') {
                        if (frmMyTopUpList.segBillersList.data == null || frmMyTopUpList.segBillersList.data.length == 0) {
                            frmMyTopUpList.lblErrorMsg.setVisibility(true);
                        } else {
                            frmMyTopUpList.lblErrorMsg.setVisibility(false);
                        }
                    }
                    if (kony.application.getCurrentForm().id == 'frmMyTopUpSelect' || kony.application.getPreviousForm().id == 'frmMyTopUpSelect') {
                        frmMyTopUpSelect.lblErrorMsg.setVisibility(true);
                    }
                    isSearched = false;
                }
            }
        } else {
            if (kony.application.getCurrentForm().id == 'frmMyTopUpList' || kony.application.getPreviousForm().id == 'frmMyTopUpList') {
                frmMyTopUpList.segSuggestedBillers.removeAll();
            } else if (kony.application.getCurrentForm().id == 'frmSelectBiller' || kony.application.getPreviousForm().id == 'frmSelectBiller') {
                //frmSelectBiller.hboxMoreSelectBiller.setVisibility(false);
            } else if (kony.application.getCurrentForm().id == 'frmMyTopUpSelect' || kony.application.getPreviousForm().id == 'frmMyTopUpSelect') {
                frmMyTopUpSelect.segSelectList.removeAll();
                frmMyTopUpSelect.hbxLink.setVisibility(false);
                frmMyTopUpSelect.lblErrorMsg.setVisibility(false);
            }
            dismissLoadingScreen();
            alert(kony.i18n.getLocalizedString("ECGenericError"));
        }
    } else {
        if (status == 300) {
            if (kony.application.getCurrentForm().id == 'frmMyTopUpList' || kony.application.getPreviousForm().id == 'frmMyTopUpList') {
                frmMyTopUpList.segSuggestedBillers.removeAll();
            } else if (kony.application.getCurrentForm().id == 'frmSelectBiller') {
                //frmSelectBiller.hboxMoreSelectBiller.setVisibility(false);
            } else if (kony.application.getCurrentForm().id == 'frmMyTopUpSelect' || kony.application.getPreviousForm().id == 'frmMyTopUpSelect') {
                frmMyTopUpSelect.segSelectList.removeAll();
                frmMyTopUpSelect.hbxLink.setVisibility(false);
                frmMyTopUpSelect.lblErrorMsg.setVisibility(false);
            }
            dismissLoadingScreen();
            alert(kony.i18n.getLocalizedString("ECGenericError"));
        }
    }
}

function populateMyTopUpSuggestMB(collectiondata) {
    //var tmpRef2= null;
    var tmpRef2Len = 0;
    var billername = "";
    var sugBillCatName = "";
    var ref1label = "";
    var ref2label = "";
    var tmpRef2label = "";
    var j = 0;
    var masterData = [];
    var tmpCatFilteredData = [];
    var locale = kony.i18n.getCurrentLocale();
    var tmpIsRef2Req = "";
    var tempMinAmount = [];
    if (kony.string.startsWith(locale, "en", true) == true) {
        billername = "BillerNameEN";
        sugBillCatName = "BillerCatgoryNameEN";
        ref1label = "LabelReferenceNumber1EN";
        ref2label = "LabelReferenceNumber2EN";
    } else if (kony.string.startsWith(locale, "th", true) == true) {
        billername = "BillerNameTH";
        sugBillCatName = "BillerCatgoryNameTH";
        ref1label = "LabelReferenceNumber1TH";
        ref2label = "LabelReferenceNumber2TH";
    }
    myTopUpSuggestListRs.length = 0;
    gblstepMinAmount = [];
    if (!isSearched) TempDataBillers.length = 0;
    for (var i = 0; i < collectiondata.length; i++) {
        var billerMethod = collectiondata[i]["BillerMethod"];
        if (billerMethod == "1" && collectiondata[i]["BillerGroupType"] == "1" && collectiondata[i]["BillerCompcode"] != "2605") {
            var min = collectiondata[i]["StepAmount"][0]["Amt"];
            tempMinAmount = {
                "compcode": collectiondata[i]["BillerCompcode"],
                "minAmount": min
            }
        } else {
            tempMinAmount = [];
        }
        if (collectiondata[i]["BillerGroupType"] == gblMyBillerTopUpBB) {
            tmpIsRef2Req = collectiondata[i]["IsRequiredRefNumber2Add"];
            //if (gblMyBillerTopUpBB == 0 && (tmpIsRef2Req == "Y" || tmpIsRef2Req == "y")) {
            tmpRef2label = collectiondata[i][ref2label];
            tmpRef2Value = collectiondata[i]["ReferenceNumber2"];
            tmpRef2Len = collectiondata[i]["Ref2Len"];
            /*} else {
				
				tmpRef2label = "";
				tmpRef2Value = "";
				tmpRef2Len = 0;
			}*/
            var imagesUrl = BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + collectiondata[i]["BillerCompcode"] + "&modIdentifier=MyBillers";
            var billerNameEN = collectiondata[i]["BillerNameEN"] + " (" + collectiondata[i]["BillerCompcode"] + ")";
            var billerNameTH = collectiondata[i]["BillerNameTH"] + " (" + collectiondata[i]["BillerCompcode"] + ")";
            //ENH_113	
            gblbillerStartTime = "";
            gblbillerEndTime = "";
            gblbillerFullPay = "";
            gblbillerAllowSetSched = "";
            gblbillerTotalPayAmtEn = "";
            gblbillerTotalPayAmtTh = "";
            gblbillerTotalIntEn = "";
            gblbillerTotalIntTh = "";
            gblbillerDisconnectAmtEn = "";
            gblbillerDisconnectAmtTh = "";
            gblbillerServiceType = "";
            gblbillerTransType = "";
            gblMeaFeeAmount = "";
            gblbillerAmountEn = "";
            gblbillerAmountTh = "";
            gblbillerMeterNoEn = "";
            gblbillerMeterNoTh = "";
            gblbillerCustNameEn = "";
            gblbillerCustNameTh = "";
            gblbillerCustAddressEn = "";
            gblbillerCustAddressTh = "";
            if (collectiondata[i]["BillerCompcode"] == "2533") {
                if (collectiondata[i]["ValidChannel"] != undefined && collectiondata[i]["ValidChannel"].length > 0) {
                    for (var t = 0; t < collectiondata[i]["ValidChannel"].length; t++) {
                        if (collectiondata[i]["ValidChannel"][t]["ChannelCode"] == "01") {
                            gblMeaFeeAmount = collectiondata[i]["ValidChannel"][t]["FeeAmount"];
                        }
                    }
                }
                if (collectiondata[i]["BillerMiscData"] != undefined && collectiondata[i]["BillerMiscData"].length > 0) {
                    for (var j = 0; j < collectiondata[i]["BillerMiscData"].length; j++) {
                        if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BILLER.StartTime") {
                            gblbillerStartTime = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BILLER.EndTime") {
                            gblbillerEndTime = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BILLER.FullPayment") {
                            gblbillerFullPay = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BILLER.AllowSetSchedule") {
                            gblbillerAllowSetSched = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD1.LABEL.EN") {
                            gblbillerMeterNoEn = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD1.LABEL.TH") {
                            gblbillerMeterNoTh = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD2.LABEL.EN") {
                            gblbillerCustNameEn = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD2.LABEL.TH") {
                            gblbillerCustNameTh = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD3.LABEL.EN") {
                            gblbillerCustAddressEn = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD3.LABEL.TH") {
                            gblbillerCustAddressTh = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD4.LABEL.EN") {
                            gblbillerTotalPayAmtEn = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD4.LABEL.TH") {
                            gblbillerTotalPayAmtTh = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD5.LABEL.EN") {
                            gblbillerAmountEn = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD5.LABEL.TH") {
                            gblbillerAmountTh = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD6.LABEL.EN") {
                            gblbillerTotalIntEn = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD6.LABEL.TH") {
                            gblbillerTotalIntTh = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD7.LABEL.EN") {
                            gblbillerDisconnectAmtEn = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD7.LABEL.TH") {
                            gblbillerDisconnectAmtTh = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BILLER.ServiceType") {
                            gblbillerServiceType = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BILLER.TransactionType") {
                            gblbillerTransType = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                        }
                    }
                }
            }
            //ENH113 end
            var tempRecord = {
                "lblSuggestedBiller": {
                    "text": collectiondata[i][billername] + " (" + collectiondata[i]["BillerCompcode"] + ")"
                },
                "lblSuggBillCatName": {
                    "text": collectiondata[i][sugBillCatName]
                },
                "btnAddBiller": {
                    "text": "  ",
                    "skin": "btnAdd"
                },
                "BillerID": {
                    "text": collectiondata[i]["BillerID"]
                },
                "BillerCompCode": {
                    "text": collectiondata[i]["BillerCompcode"]
                },
                "BarcodeOnly": collectiondata[i]["BarcodeOnly"],
                "BillerGroupType": collectiondata[i]["BillerGroupType"],
                "IsFullPayment": collectiondata[i]["IsFullPayment"],
                "ToAccountKey": {
                    "text": collectiondata[i]["ToAccountKey"]
                },
                "Ref1Label": {
                    "text": collectiondata[i][ref1label]
                },
                "Ref2Label": {
                    "text": tmpRef2label
                },
                "imgSuggestedBiller": {
                    "src": imagesUrl
                },
                "IsRequiredRefNumber2Add": {
                    "text": collectiondata[i]["IsRequiredRefNumber2Add"]
                },
                "IsRequiredRefNumber2Pay": {
                    "text": collectiondata[i]["IsRequiredRefNumber2Pay"]
                },
                "EffDt": {
                    "text": collectiondata[i]["EffDt"]
                },
                "lblBillerNameEN": billerNameEN,
                "lblBillerNameTH": billerNameTH,
                "BillerMethod": {
                    "text": collectiondata[i]["BillerMethod"]
                },
                "Ref1Len": {
                    "text": collectiondata[i]["Ref1Len"]
                },
                "Ref2Len": {
                    "text": tmpRef2Len
                },
                "BillerCategoryID": {
                    "text": collectiondata[i]["BillerCategoryID"]
                },
                "hdnLabelRefNum1EN": {
                    "text": collectiondata[i]["LabelReferenceNumber1EN"]
                },
                "hdnLabelRefNum1TH": {
                    "text": collectiondata[i]["LabelReferenceNumber1TH"]
                },
                "hdnLabelRefNum2EN": {
                    "text": collectiondata[i]["LabelReferenceNumber2EN"]
                },
                "hdnLabelRefNum2TH": {
                    "text": collectiondata[i]["LabelReferenceNumber2TH"]
                },
                "StepAmount": collectiondata[i]["StepAmount"],
                "billerStartTime": gblbillerStartTime,
                "billerEndTime": gblbillerEndTime,
                "billerFullPay": gblbillerFullPay,
                "billerAllowSetSched": gblbillerAllowSetSched,
                "billerTotalPayAmtEn": gblbillerTotalPayAmtEn,
                "billerTotalPayAmtTh": gblbillerTotalPayAmtTh,
                "billerTotalIntEn": gblbillerTotalIntEn,
                "billerTotalIntTh": gblbillerTotalIntTh,
                "billerDisconnectAmtEn": gblbillerDisconnectAmtEn,
                "billerDisconnectAmtTh": gblbillerDisconnectAmtTh,
                "billerServiceType": gblbillerServiceType,
                "billerTransType": gblbillerTransType,
                "billerFeeAmount": gblMeaFeeAmount,
                "billerAmountEn": gblbillerAmountEn,
                "billerAmountTh": gblbillerAmountTh,
                "billerMeterNoEn": gblbillerMeterNoEn,
                "billerMeterNoTh": gblbillerMeterNoTh,
                "billerCustNameEn": gblbillerCustNameEn,
                "billerCustNameTh": gblbillerCustNameTh,
                "billerCustAddressEn": gblbillerCustAddressEn,
                "billerCustAddressTh": gblbillerCustAddressTh,
                "billerBancassurance": collectiondata[i]["IsBillerBancassurance"],
                "allowRef1AlphaNum": collectiondata[i]["AllowRef1AlphaNum"]
            }
            myTopUpSuggestListRs.push(tempRecord);
            if (!isSearched) TempDataBillers.push(tempRecord);
            if (collectiondata[i]["BillerGroupType"] == "1") {
                if (tempMinAmount.length != 0) gblstepMinAmount.push(tempMinAmount);
            }
        }
    }
    myTopupSuggestListMB = myTopUpSuggestListRs;
    gblTopUpSelectFormDataHolder = myTopUpSuggestListRs;
    sugBillerCatChangeMB();
}
/** To get CustmerBillInquiry Service for My Biller
 * Input ReqUID,CRMID,RecSelect
 */
//function getMyBillerListMB() {
//	var inputParams = {
//		crmId: gblcrmId,
//		clientDate: getCurrentDate()
//	};
//	invokeServiceSecureAsync("customerBillInquiry", inputParams, startMyBillerListMBServiceAsyncCallback);
//}
function populateMyBillerMBForSelectBiller(collectionData) {
    var billername = "";
    var ref1label = "";
    var ref2label = "";
    search = "";
    search = frmSelectBiller.tbxSearch.text;
    if (search != null) {
        var searchtxt = search.toLowerCase();
    }
    var i = 0;
    var tmpSearchList = [];
    tmpSearchList.length = 0;
    var locale = kony.i18n.getCurrentLocale();
    if (kony.string.startsWith(locale, "en", true) == true) {
        billername = "BillerNameEN";
        ref1label = "LabelReferenceNumber1EN";
        ref2label = "LabelReferenceNumber2EN";
    } else if (kony.string.startsWith(locale, "th", true) == true) {
        billername = "BillerNameTH";
        ref1label = "LabelReferenceNumber1TH";
        ref2label = "LabelReferenceNumber2TH";
    }
    if (!isSearched) {
        myTopUpListRs.length = 0;
        myBillerTopupListMB = collectionData;
        for (var i = 0; i < collectionData.length; i++) {
            if (collectionData[i]["BillerGroupType"] == gblMyBillerTopUpBB) {
                var imagesUrl = BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + collectionData[i]["BillerCompcode"] + "&modIdentifier=MyBillers";
                var billerName = collectionData[i][billername] + " (" + collectionData[i]["BillerCompcode"] + ")";
                var billerNameEN = collectionData[i]["BillerNameEN"] + " (" + collectionData[i]["BillerCompcode"] + ")";
                var billerNameTH = collectionData[i]["BillerNameTH"] + " (" + collectionData[i]["BillerCompcode"] + ")";
                var isRef2Req = collectionData[i]["IsRequiredRefNumber2Pay"];
                //ENH_113	
                gblbillerStartTime = "";
                gblbillerEndTime = "";
                gblbillerFullPay = "";
                gblbillerAllowSetSched = "";
                gblbillerTotalPayAmtEn = "";
                gblbillerTotalPayAmtTh = "";
                gblbillerTotalIntEn = "";
                gblbillerTotalIntTh = "";
                gblbillerDisconnectAmtEn = "";
                gblbillerDisconnectAmtTh = "";
                gblbillerServiceType = "";
                gblbillerTransType = "";
                gblMeaFeeAmount = "";
                gblbillerAmountEn = "";
                gblbillerAmountTh = "";
                gblbillerMeterNoEn = "";
                gblbillerMeterNoTh = "";
                gblbillerCustNameEn = "";
                gblbillerCustNameTh = "";
                gblbillerCustAddressEn = "";
                gblbillerCustAddressTh = "";
                gblbillerMeterNoEn = "";
                gblbillerMeterNoTh = "";
                gblbillerCustNameEn = "";
                gblbillerCustNameTh = "";
                gblbillerCustAddressEn = "";
                gblbillerCustAddressTh = "";
                if (collectionData[i]["BillerCompcode"] == "2533") {
                    if (collectionData[i]["ValidChannel"] != undefined && collectionData[i]["ValidChannel"].length > 0) {
                        for (var t = 0; t < collectionData[i]["ValidChannel"].length; t++) {
                            if (collectionData[i]["ValidChannel"][t]["ChannelCode"] == "01") {
                                gblMeaFeeAmount = collectionData[i]["ValidChannel"][t]["FeeAmount"];
                            }
                        }
                    }
                    if (collectionData[i]["BillerMiscData"] != undefined && collectionData[i]["BillerMiscData"].length > 0) {
                        for (var j = 0; j < collectionData[i]["BillerMiscData"].length; j++) {
                            if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "BILLER.StartTime") {
                                gblbillerStartTime = collectionData[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "BILLER.EndTime") {
                                gblbillerEndTime = collectionData[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "BILLER.FullPayment") {
                                gblbillerFullPay = collectionData[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "BILLER.AllowSetSchedule") {
                                gblbillerAllowSetSched = collectionData[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD1.LABEL.EN") {
                                gblbillerMeterNoEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD1.LABEL.TH") {
                                gblbillerMeterNoTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD2.LABEL.EN") {
                                gblbillerCustNameEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD2.LABEL.TH") {
                                gblbillerCustNameTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD3.LABEL.EN") {
                                gblbillerCustAddressEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD3.LABEL.TH") {
                                gblbillerCustAddressTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD4.LABEL.EN") {
                                gblbillerTotalPayAmtEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD4.LABEL.TH") {
                                gblbillerTotalPayAmtTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD5.LABEL.EN") {
                                gblbillerAmountEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD5.LABEL.TH") {
                                gblbillerAmountTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD6.LABEL.EN") {
                                gblbillerTotalIntEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD6.LABEL.TH") {
                                gblbillerTotalIntTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD7.LABEL.EN") {
                                gblbillerDisconnectAmtEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD7.LABEL.TH") {
                                gblbillerDisconnectAmtTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "BILLER.ServiceType") {
                                gblbillerServiceType = collectionData[i]["BillerMiscData"][j]["MiscText"];
                            }
                            if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "BILLER.TransactionType") {
                                gblbillerTransType = collectionData[i]["BillerMiscData"][j]["MiscText"];
                            }
                        }
                    }
                }
                //ENH113 end
                var tempRecord;
                if (isRef2Req == "Y") {
                    tempRecord = {
                        "crmId": collectionData[i]["CRMID"],
                        "lblBillerName": {
                            "text": billerName
                        },
                        "lblBillerNickname": {
                            "text": collectionData[i]["BillerNickName"]
                        },
                        "BillerCategoryID": {
                            "text": collectionData[i]["BillerCategoryID"]
                        },
                        "BillerCompCode": {
                            "text": collectionData[i]["BillerCompcode"]
                        },
                        "lblRef1Value": collectionData[i]["ReferenceNumber1"],
                        "lblRef1ValueMasked": maskCreditCard(collectionData[i]["ReferenceNumber1"]),
                        "lblRef2Value": isNotBlank(collectionData[i]["ReferenceNumber2"]) ? collectionData[i]["ReferenceNumber2"] : "",
                        "imgBillerLogo": {
                            "src": imagesUrl
                        },
                        "lblRef1": appendColon(collectionData[i][ref1label]),
                        "lblRef2": appendColon(collectionData[i][ref2label]),
                        "lblRef1TH": collectionData[i]["LabelReferenceNumber1TH"],
                        "lblRef2TH": collectionData[i]["LabelReferenceNumber2TH"],
                        "lblRef1EN": collectionData[i]["LabelReferenceNumber1EN"],
                        "lblRef2EN": collectionData[i]["LabelReferenceNumber2EN"],
                        "Ref2Len": collectionData[i]["Ref2Len"],
                        "Ref1Len": collectionData[i]["Ref1Len"],
                        "lblBillerNameEN": billerNameEN,
                        "lblBillerNameTH": billerNameTH,
                        "billerMethod": collectionData[i]["BillerMethod"],
                        "billerGroupType": collectionData[i]["BillerGroupType"],
                        "waiverCode": collectionData[i]["ReferenceNumber1"],
                        "ToAccountKey": collectionData[i]["ToAccountKey"],
                        "IsFullPayment": collectionData[i]["IsFullPayment"],
                        "CustomerBillID": collectionData[i]["CustomerBillID"],
                        "IsRequiredRefNumber2Add": collectionData[i]["IsRequiredRefNumber2Add"],
                        "IsRequiredRefNumber2Pay": collectionData[i]["IsRequiredRefNumber2Pay"],
                        "billerStartTime": gblbillerStartTime,
                        "billerEndTime": gblbillerEndTime,
                        "billerFullPay": gblbillerFullPay,
                        "billerAllowSetSched": gblbillerAllowSetSched,
                        "billerTotalPayAmtEn": gblbillerTotalPayAmtEn,
                        "billerTotalPayAmtTh": gblbillerTotalPayAmtTh,
                        "billerTotalIntEn": gblbillerTotalIntEn,
                        "billerTotalIntTh": gblbillerTotalIntTh,
                        "billerDisconnectAmtEn": gblbillerDisconnectAmtEn,
                        "billerDisconnectAmtTh": gblbillerDisconnectAmtTh,
                        "billerServiceType": gblbillerServiceType,
                        "billerTransType": gblbillerTransType,
                        "billerFeeAmount": gblMeaFeeAmount,
                        "billerAmountEn": gblbillerAmountEn,
                        "billerAmountTh": gblbillerAmountTh,
                        "billerMeterNoEn": gblbillerMeterNoEn,
                        "billerMeterNoTh": gblbillerMeterNoTh,
                        "billerCustNameEn": gblbillerCustNameEn,
                        "billerCustNameTh": gblbillerCustNameTh,
                        "billerCustAddressEn": gblbillerCustAddressEn,
                        "billerCustAddressTh": gblbillerCustAddressTh,
                        "billerBancassurance": collectionData[i]["IsBillerBancassurance"],
                        "allowRef1AlphaNum": collectionData[i]["AllowRef1AlphaNum"]
                    }
                } else {
                    tempRecord = {
                        "crmId": collectionData[i]["CRMID"],
                        "lblBillerName": {
                            "text": billerName
                        },
                        "lblBillerNickname": {
                            "text": collectionData[i]["BillerNickName"]
                        },
                        "BillerCategoryID": {
                            "text": collectionData[i]["BillerCategoryID"]
                        },
                        "BillerCompCode": {
                            "text": collectionData[i]["BillerCompcode"]
                        },
                        "lblRef1Value": collectionData[i]["ReferenceNumber1"],
                        "lblRef1ValueMasked": maskCreditCard(collectionData[i]["ReferenceNumber1"]),
                        "lblRef2Value": "",
                        "imgBillerLogo": {
                            "src": imagesUrl
                        },
                        "lblRef1": appendColon(collectionData[i][ref1label]),
                        "lblRef1TH": collectionData[i]["LabelReferenceNumber1TH"],
                        "lblRef1EN": collectionData[i]["LabelReferenceNumber1EN"],
                        "lblRef2": "",
                        "lblBillerNameEN": billerNameEN,
                        "lblBillerNameTH": billerNameTH,
                        "billerMethod": collectionData[i]["BillerMethod"],
                        "billerGroupType": collectionData[i]["BillerGroupType"],
                        "waiverCode": collectionData[i]["ReferenceNumber1"],
                        "ToAccountKey": collectionData[i]["ToAccountKey"],
                        "IsFullPayment": collectionData[i]["IsFullPayment"],
                        "CustomerBillID": collectionData[i]["CustomerBillID"],
                        "IsRequiredRefNumber2Add": collectionData[i]["IsRequiredRefNumber2Add"],
                        "IsRequiredRefNumber2Pay": collectionData[i]["IsRequiredRefNumber2Pay"],
                        "billerStartTime": gblbillerStartTime,
                        "billerEndTime": gblbillerEndTime,
                        "billerFullPay": gblbillerFullPay,
                        "billerAllowSetSched": gblbillerAllowSetSched,
                        "billerTotalPayAmtEn": gblbillerTotalPayAmtEn,
                        "billerTotalPayAmtTh": gblbillerTotalPayAmtTh,
                        "billerTotalIntEn": gblbillerTotalIntEn,
                        "billerTotalIntTh": gblbillerTotalIntTh,
                        "billerDisconnectAmtEn": gblbillerDisconnectAmtEn,
                        "billerDisconnectAmtTh": gblbillerDisconnectAmtTh,
                        "billerServiceType": gblbillerServiceType,
                        "billerTransType": gblbillerTransType,
                        "billerFeeAmount": gblMeaFeeAmount,
                        "billerAmountEn": gblbillerAmountEn,
                        "billerAmountTh": gblbillerAmountTh,
                        "billerMeterNoEn": gblbillerMeterNoEn,
                        "billerMeterNoTh": gblbillerMeterNoTh,
                        "billerCustNameEn": gblbillerCustNameEn,
                        "billerCustNameTh": gblbillerCustNameTh,
                        "billerCustAddressEn": gblbillerCustAddressEn,
                        "billerCustAddressTh": gblbillerCustAddressTh,
                        "billerBancassurance": collectionData[i]["IsBillerBancassurance"],
                        "allowRef1AlphaNum": collectionData[i]["AllowRef1AlphaNum"]
                    }
                }
                myTopUpListRs.push(tempRecord);
            }
        }
        mySelectBillerListMB = myTopUpListRs;
        gblCustomerBillList = [];
        gblCustomerBillList = myTopUpListRs;
        frmSelectBiller.segMyBills.setVisibility(true);
        frmSelectBiller.segMyBills.removeAll();
        frmSelectBiller.segMyBills.setData(myTopUpListRs);
        dismissLoadingScreen();
    } else if (isSearched) {
        for (var i = 0; i < collectionData.length; i++) {
            if (collectionData[i]["BillerNickName"]) {
                var lowerCaseName = collectionData[i]["BillerNickName"].toLowerCase();
                if (kony.string.startsWith(lowerCaseName, searchtxt, true)) {
                    if (collectionData[i]["BillerGroupType"] == gblMyBillerTopUpBB) {
                        var isRef2Req = collectionData[i]["IsRequiredRefNumber2Pay"];
                        var billerNameEN = collectionData[i]["BillerNameEN"] + " (" + collectionData[i]["BillerCompcode"] + ")";
                        var billerNameTH = collectionData[i]["BillerNameTH"] + " (" + collectionData[i]["BillerCompcode"] + ")";
                        var tempRecord;
                        if (isRef2Req == "Y") {
                            tempRecord = {
                                "crmId": collectionData[i]["CRMID"],
                                "lblBillerName": collectionData[i][billername] + " (" + collectionData[i]["BillerCompcode"] + ")",
                                "lblBillerNickname": {
                                    "text": collectionData[i]["BillerNickName"]
                                },
                                "BillerCategoryID": {
                                    "text": collectionData[i]["BillerCategoryID"]
                                },
                                "BillerCompCode": {
                                    "text": collectionData[i]["BillerCompcode"]
                                },
                                "lblRef1Value": collectionData[i]["ReferenceNumber1"],
                                "lblRef1ValueMasked": maskCreditCard(collectionData[i]["ReferenceNumber1"]),
                                "lblRef2Value": isNotBlank(collectionData[i]["ReferenceNumber2"]) ? collectionData[i]["ReferenceNumber2"] : "",
                                "imgBillerLogo": "bill_img01.png",
                                "lblRef1": appendColon(collectionData[i][ref1label]),
                                "lblRef2": appendColon(collectionData[i][ref2label]),
                                "lblRef1TH": collectionData[i]["LabelReferenceNumber1TH"],
                                "lblRef2TH": collectionData[i]["LabelReferenceNumber2TH"],
                                "lblRef1EN": collectionData[i]["LabelReferenceNumber1EN"],
                                "lblRef2EN": collectionData[i]["LabelReferenceNumber2EN"],
                                "lblBillerNameEN": billerNameEN,
                                "lblBillerNameTH": billerNameTH,
                                "billerMethod": collectionData[i]["BillerMethod"],
                                "billerGroupType": collectionData[i]["BillerGroupType"],
                                "waiverCode": collectionData[i]["ReferenceNumber1"],
                                "ToAccountKey": collectionData[i]["ToAccountKey"],
                                "IsFullPayment": collectionData[i]["IsFullPayment"],
                                "CustomerBillID": collectionData[i]["CustomerBillID"]
                            }
                        } else {
                            tempRecord = {
                                "crmId": collectionData[i]["CRMID"],
                                "lblBillerName": collectionData[i][billername] + " (" + collectionData[i]["BillerCompcode"] + ")",
                                "lblBillerNickname": {
                                    "text": collectionData[i]["BillerNickName"]
                                },
                                "BillerCategoryID": {
                                    "text": collectionData[i]["BillerCategoryID"]
                                },
                                "BillerCompCode": {
                                    "text": collectionData[i]["BillerCompcode"]
                                },
                                "lblRef1Value": collectionData[i]["ReferenceNumber1"],
                                "lblRef1ValueMasked": maskCreditCard(collectionData[i]["ReferenceNumber1"]),
                                //"lblRef2Value": collectionData[i]["ReferenceNumber2"],
                                "imgBillerLogo": "bill_img01.png",
                                "lblRef1": appendColon(collectionData[i][ref2label]),
                                "lblRef1TH": collectionData[i]["LabelReferenceNumber1TH"],
                                "lblRef1EN": collectionData[i]["LabelReferenceNumber1EN"],
                                "lblBillerNameEN": billerNameEN,
                                "lblBillerNameTH": billerNameTH,
                                //"lblRef2": collectionData[i][ref2label],
                                "billerMethod": collectionData[i]["BillerMethod"],
                                "billerGroupType": collectionData[i]["BillerGroupType"],
                                "waiverCode": collectionData[i]["ReferenceNumber1"],
                                "ToAccountKey": collectionData[i]["ToAccountKey"],
                                "IsFullPayment": collectionData[i]["IsFullPayment"],
                                "CustomerBillID": collectionData[i]["CustomerBillID"]
                            }
                        }
                    }
                    tmpSearchList.push(tempRecord);
                }
            }
        }
        //new comm frmSelectBiller.segMyBills.removeAll();
        frmSelectBiller.segMyBills.setVisibility(true);
        frmSelectBiller.segMyBills.removeAll();
        frmSelectBiller.segMyBills.setData(tmpSearchList);
        dismissLoadingScreen();
    }
}
var flagSearchMB = 0;
var flagCatMB = 0;
var currentDataMB = 0;
var suggestedCatDataMB = 0;
var catDataMB = 0;
var suggestedSearchDataMB = 0;
var searchDataMB = 0;

function searchSugBillerMB() {
    gblTopUpMore = 0;
    gblsearchtxt = "";
    if (kony.application.getCurrentForm().id == 'frmMyTopUpList') {
        gblsearchtxt = frmMyTopUpList.txbSearch.text;
        if (gblsearchtxt.length >= 3) {
            isSearched = true;
            searchFlagForLoadMore = true;
            if (myTopupListMB.length == 0) {
                frmMyTopUpList.lblNoBill.setVisibility(false);
            }
            getMyTopUpSuggestListMB();
            frmMyTopUpList.segSuggestedBillers.setVisibility(true);
        } else {
            if (myTopupListMB.length == 0) {
                frmMyTopUpList.lblNoBill.setVisibility(true);
                frmMyTopUpList.lblErrorMsg.setVisibility(false);
            }
            searchFlagForLoadMore = false;
            frmMyTopUpList.segSuggestedBillers.setVisibility(false);
        }
    } else if (kony.application.getCurrentForm().id == 'frmSelectBiller') {
        gblsearchtxt = frmSelectBiller.tbxSearch.text;
        if (gblsearchtxt.length >= 3) {
            isSearched = true;
            getMyTopUpSuggestListMB();
            //frmSelectBiller.segSuggestedBillers.setVisibility(true);
        } else {
            //frmSelectBiller.segSuggestedBillers.setVisibility(false);
        }
    } else if (kony.application.getCurrentForm().id == 'frmMyTopUpSelect') {
        gblsearchtxt = frmMyTopUpSelect.txbSearch.text;
        if (gblsearchtxt.length >= 3) {
            isSearched = true;
            getMyTopUpSuggestListMB();
        } else {
            gblTopUpSelectFormDataHolder = myTopUpSelect;
            myTopupSuggestListMB = myTopUpSelect;
            onMyTopUpSelectMoreDynamic(myTopUpSelect);
            sugBillerCatChangeMB();
        }
    }
}

function sugBillerCatChangeMB() {
    search = "";
    var lengthBillersSuggestList = 0;
    var tmpSugBillerData = [];
    var showCatSuggestList = [];
    var tmpCatSelected = currentCatSelected;
    if (kony.application.getCurrentForm().id == 'frmMyTopUpList' || kony.application.getPreviousForm().id == 'frmMyTopUpList') {
        tmpSugBillerData = myTopupSuggestListMB;
        lengthBillersSuggestList = myTopupSuggestListMB.length;
        search = frmMyTopUpList.txbSearch.text;
    } else if (kony.application.getCurrentForm().id == 'frmSelectBiller' || kony.application.getPreviousForm().id == 'frmSelectBiller') {
        tmpSugBillerData = mySelectBillerSuggestListMB;
        lengthBillersSuggestList = mySelectBillerSuggestListMB.length;
        search = frmSelectBiller.tbxSearch.text;
    } else if (kony.application.getCurrentForm().id == 'frmMyTopUpSelect' || kony.application.getPreviousForm().id == 'frmMyTopUpSelect') {
        search = frmMyTopUpSelect.txbSearch.text;
        tmpSugBillerData = myTopupSuggestListMB;
        lengthBillersSuggestList = myTopupSuggestListMB.length;
    }
    if (tmpCatSelected != 0) {
        for (j = 0, i = 0; i < lengthBillersSuggestList; i++) {
            if (tmpSugBillerData[i].BillerCategoryID.text == tmpCatSelected) {
                showCatSuggestList[j] = tmpSugBillerData[i];
                j++;
            }
        }
    } else {
        showCatSuggestList = tmpSugBillerData;
    }
    if (showCatSuggestList.length == 0 && search.length >= 3) {
        if (kony.application.getCurrentForm().id == 'frmMyTopUpList' || kony.application.getPreviousForm().id == 'frmMyTopUpList') {
            frmMyTopUpList.segSuggestedBillers.removeAll();
            frmMyTopUpList.lblErrorMsg.setVisibility(true);
        } else if (kony.application.getCurrentForm().id == 'frmSelectBiller' || kony.application.getPreviousForm().id == 'frmSelectBiller') {
            frmSelectBiller.segSuggestedBillers.removeAll();
        } else if (kony.application.getCurrentForm().id == 'frmMyTopUpSelect' || kony.application.getPreviousForm().id == 'frmMyTopUpSelect') {
            frmMyTopUpSelect.segSelectList.removeAll();
            frmMyTopUpSelect.lblErrorMsg.setVisibility(true);
        }
    } else {
        if (kony.application.getCurrentForm().id == 'frmMyTopUpList' || kony.application.getPreviousForm().id == 'frmMyTopUpList') {
            frmMyTopUpList.lblErrorMsg.setVisibility(false);
            gblTopUpSelectFormDataHolder = showCatSuggestList;
            onMyTopUpSelectMoreDynamic(showCatSuggestList);
        } else if (kony.application.getCurrentForm().id == 'frmSelectBiller' || kony.application.getPreviousForm().id == 'frmSelectBiller') {
            gblTopUpSelectFormDataHolder = showCatSuggestList;
            onMyTopUpSelectMoreDynamic(showCatSuggestList);
        } else if (kony.application.getCurrentForm().id == 'frmMyTopUpSelect' || kony.application.getPreviousForm().id == 'frmMyTopUpSelect') {
            frmMyTopUpSelect.lblErrorMsg.setVisibility(false);
            gblTopUpSelectFormDataHolder = showCatSuggestList;
            onMyTopUpSelectMoreDynamic(showCatSuggestList);
        }
    }
}
var searchAlertCounter = 0;

function billerCatChangeMB() {
    gblTopUpMore = 0;
    var showCatList = [];
    var searchCustomerData = [];
    var showSearchList = [];
    var isCatSelected = false;
    var tmpCatSelected = currentCatSelected;
    var j = 0;
    var tmp = "";
    var tmpBillerData = "";
    var tmpSugBillerData = "";
    var lengthBillersList = 0;
    flagCatMB = 1;
    var lengthBillersSuggestList = 0;
    search = "";
    if (kony.application.getCurrentForm().id == 'frmMyTopUpSelect') {
        return;
    }
    if (kony.application.getCurrentForm().id == 'frmMyTopUpList') {
        tmpBillerData = myTopupListMB;
        lengthBillersList = myTopupListMB.length;
        search = frmMyTopUpList.txbSearch.text;
        if (search.length >= 3) {
            frmMyTopUpList.segSuggestedBillers.setVisibility(true);
        } else {
            frmMyTopUpList.segSuggestedBillers.setVisibility(false);
        }
    } else if (kony.application.getCurrentForm().id == 'frmSelectBiller') {
        tmpBillerData = mySelectBillerListMB;
        lengthBillersList = mySelectBillerListMB.length;
        search = frmSelectBiller.tbxSearch.text;
    }
    searchCustomerData = tmpBillerData;
    if (search.length >= 3) {
        var searchtxt = search.toLowerCase();
        for (j = 0, i = 0; i < lengthBillersList; i++) {
            if (tmpBillerData[i]["lblBillerNickname"]) {
                lowerCaseName = tmpBillerData[i]["lblBillerNickname"]["text"].toLowerCase();
                var lowerCaseNameBiller = tmpBillerData[i]["lblBillerName"]["text"].toLowerCase();
                if (("" + lowerCaseName).indexOf("" + searchtxt) >= 0 || ("" + lowerCaseNameBiller).indexOf("" + searchtxt) >= 0) {
                    showSearchList[j] = tmpBillerData[i];
                    j++;
                }
            }
        }
        searchCustomerData = showSearchList;
    } else {
        frmMyTopUpList.lblMyBillers.setVisibility(false);
        frmMyTopUpList.lblSuggestedBillersText.setVisibility(false);
    }
    if (tmpCatSelected != 0) {
        var searchBillsLength = searchCustomerData.length;
        for (j = 0, i = 0; i < searchBillsLength; i++) {
            tmp = searchCustomerData[i].BillerCategoryID.text;
            if (kony.string.equalsIgnoreCase("" + tmp, "" + tmpCatSelected)) {
                showCatList[j] = searchCustomerData[i];
                j++;
            }
        }
        searchCustomerData = showCatList;
    }
    if (searchCustomerData.length == 0) {
        if (kony.application.getCurrentForm().id == 'frmMyTopUpList') {
            frmMyTopUpList.segBillersList.removeAll();
            frmMyTopUpList.lblMyBillers.setVisibility(false);
        } else if (kony.application.getCurrentForm().id == 'frmSelectBiller') {
            gblBillerCatChangeFound = false;
            if (gblBillersFound != undefined && gblBillersFound == false) {
                frmSelectBiller.lblNoBillers.setVisibility(true);
            }
            frmSelectBiller.segMyBills.removeAll();
            //Here you are means there are no My Bills
        }
        if (tmpCatSelected != 0) {
            frmMyTopUpList.lblErrorMsg.setVisibility(true);
        }
    } else {
        if (kony.application.getCurrentForm().id == 'frmMyTopUpList') {
            frmMyTopUpList.segBillersList.setData(searchCustomerData);
            if (searchFlagForLoadMore) {
                frmMyTopUpList.lblMyBillers.setVisibility(true);
            }
        } else if (kony.application.getCurrentForm().id == 'frmSelectBiller') {
            gblBillerCatChangeFound = true;
            frmSelectBiller.lblNoBillers.setVisibility(false);
            frmSelectBiller.segMyBills.removeAll();
            frmSelectBiller.segMyBills.setData(searchCustomerData);
        }
        frmMyTopUpList.lblErrorMsg.setVisibility(false);
    }
}
var billerIdMB;

function viewMybillTopUpdetail() {
    gblBillerRef1 = "";
    gblBillerRef2 = "";
    gblBillerName = "";
    gblBillerNickName = "";
    gblCustomerBillerIDMB = "";
    //if (kony.string.equalsIgnoreCase(frmMyTopUpList.segBillersList.selectedItems[0].BeepndBill.text, "Y")) {
    //		frmViewTopUpBiller.hbxLinkBeepandBill.setVisibility(true);
    //	} else {
    //		frmViewTopUpBiller.hbxLinkBeepandBill.setVisibility(false);
    //	}
    frmViewTopUpBiller.hbxLinkBeepandBill.setVisibility(false);
    gblCurRef1LblEN = frmMyTopUpList.segBillersList.selectedItems[0].lblRef1EN;
    gblCurRef1LblTH = frmMyTopUpList.segBillersList.selectedItems[0].lblRef1TH;
    gblCurRef2LblEN = frmMyTopUpList.segBillersList.selectedItems[0].lblRef2EN;
    gblCurRef2LblTH = frmMyTopUpList.segBillersList.selectedItems[0].lblRef2TH;
    gblBillerCompCodeEN = frmMyTopUpList.segBillersList.selectedItems[0].lblBillerNameEN;
    gblBillerCompCodeTH = frmMyTopUpList.segBillersList.selectedItems[0].lblBillerNameTH;
    gblBillerRef1 = frmMyTopUpList.segBillersList.selectedItems[0].lblRef1Value.text;
    gblBillerRef2 = frmMyTopUpList.segBillersList.selectedItems[0].lblRef2Value.text;
    gblBillerName = frmMyTopUpList.segBillersList.selectedItems[0].lblBillerName.text;
    gblBillerNickName = frmMyTopUpList.segBillersList.selectedItems[0].lblBillerNickname.text;
    gblCustomerBillerIDMB = frmMyTopUpList.segBillersList.selectedItems[0].CustomerBillerID.text;
    billerIdMB = frmMyTopUpList.segBillersList.selectedItems[0].BillerID.text;
    frmViewTopUpBiller.lblCnfrmBillerNameComp.text = frmMyTopUpList.segBillersList.selectedItems[0].lblBillerName.text;
    frmViewTopUpBiller.lblCnfrmNickName.text = frmMyTopUpList.segBillersList.selectedItems[0].lblBillerNickname.text;
    frmViewTopUpBiller.lblConfrmRef1.text = frmMyTopUpList.segBillersList.selectedItems[0].lblRef1.text;
    frmViewTopUpBiller.lblcnfrmRef1Value.text = frmMyTopUpList.segBillersList.selectedItems[0].lblRef1Value.text;
    //below line is changed for CR - PCI-DSS masked Credit card no
    frmViewTopUpBiller.lblcnfrmRef1ValueMasked.text = frmMyTopUpList.segBillersList.selectedItems[0].lblRef1ValueMasked.text;
    frmViewTopUpBiller.lblConfrmRef2.text = frmMyTopUpList.segBillersList.selectedItems[0].lblRef2.text;
    frmViewTopUpBiller.lblcnfrmRef2Value.text = frmMyTopUpList.segBillersList.selectedItems[0].lblRef2Value.text;
    frmViewTopUpBiller.imgTopUPBiller.src = frmMyTopUpList.segBillersList.selectedItems[0].imgBillerLogo.src;
    frmViewTopUpBiller.imgComplete.setVisibility(false);
    //gblTopupDeleteMB = frmMyTopUpList.segBillersList.selectedIndex[1];
    frmViewTopUpBiller.show();
}
//function checkDuplicateNicknameOnEditTopUpMB() {
//	//var TopUplist = frmMyTopUpList.segBillersList.data;
//	var nickname = frmMyTopUpEditScreens.txtEditName.text;
//	//alert("Nickname " + nickname);
//	var check = "";
//	var val = "";
//	for (var i = 0;
//		(((myBillerTopupListMB) != null) && i < myBillerTopupListMB.length); i++) {
//		val = myBillerTopupListMB[i]["BillerNickName"]; //TopUplist[i].lblCnfrmNickName;
//		
//		
//		if (nickname == val) {
//			//alert("Duplicate Nickname" + val);
//			return false;
//		}
//	}
//	return true;
//}
function callCustomerBillUpdateMB() {
    var inputparam = {};
    tmpBillerNickName = "";
    tmpBillerName = "";
    tmpBillerNewNickName = "";
    tmpBillerRef1 = "";
    tmpBillerRef2 = "";
    var activityTypeID = "";
    var errorCode = "";
    var activityStatus = "";
    var deviceNickName = "";
    if (gblMyBillerTopUpBB == 0) {
        inputparam["ReferenceNumber2"] = frmViewTopUpBiller.lblcnfrmRef2Value.text;
        tmpBillerRef2 = frmViewTopUpBiller.lblcnfrmRef2Value.text;
    }
    if (gblMyBillerTopUpBB == 0) {
        activityTypeID = "061";
        //activityFlexValues1 = "Edit My Bills MB";
    } else if (gblMyBillerTopUpBB == 1) {
        activityTypeID = "062";
        //activityFlexValues1 = "Edit My Top-Up MB";
    }
    tmpBillerNickName = frmViewTopUpBiller.lblCnfrmNickName.text;
    tmpBillerName = frmViewTopUpBiller.lblCnfrmBillerNameComp.text;
    tmpBillerNewNickName = frmMyTopUpEditScreens.txtEditName.text;
    tmpBillerRef1 = frmViewTopUpBiller.lblcnfrmRef1Value.text;
    var activityFlexValues1 = "Edit";
    var activityFlexValues2 = tmpBillerName;
    var activityFlexValues3 = tmpBillerNickName + "+" + tmpBillerNewNickName;
    var activityFlexValues4 = tmpBillerRef1;
    var activityFlexValues5 = tmpBillerRef2;
    var logLinkageId = "";
    inputparam["serviceID"] = "customerBillUpdate";
    //inputparam["crmId"] = gblcrmId;
    inputparam["BillerNickName"] = frmMyTopUpEditScreens.txtEditName.text;
    inputparam["BillerID"] = billerIdMB;
    inputparam["ReferenceNumber1"] = frmViewTopUpBiller.lblcnfrmRef1Value.text;
    inputparam["CustomerBillID"] = gblCustomerBillerIDMB;
    inputparam["clientDate"] = getCurrentDate();
    inputparam["flex1"] = activityFlexValues1;
    inputparam["flex2"] = activityFlexValues2;
    inputparam["flex3"] = activityFlexValues3;
    inputparam["flex4"] = activityFlexValues4;
    inputparam["flex5"] = activityFlexValues5;
    inputparam["typeID"] = activityTypeID;
    showLoadingScreen();
    invokeServiceSecureAsync("customerBillUpdate", inputparam, callBackCustomerBillUpdateMB)
}

function callBackCustomerBillUpdateMB(status, resulttable) {
    var activityTypeID = "";
    var errorCode = "";
    var activityStatus = "";
    var deviceNickName = "";
    var activityFlexValues1 = "Edit";
    var activityFlexValues2 = tmpBillerName;
    var activityFlexValues3 = tmpBillerNickName + "+" + tmpBillerNewNickName;
    var activityFlexValues4 = tmpBillerRef1;
    var activityFlexValues5 = tmpBillerRef2;
    var logLinkageId = "";
    if (gblMyBillerTopUpBB == 0) {
        activityTypeID = "061";
        //activityFlexValues1 = "Edit My Bills MB";
    } else if (gblMyBillerTopUpBB == 1) {
        activityTypeID = "062";
        //activityFlexValues1 = "Edit My Top-Up MB";
    }
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (resulttable["StatusCode"] != "0") {
                alert(resulttable["errMsg"]);
                activityStatus = "02";
            } else {
                activityStatus = "01";
                frmViewTopUpBiller.lblCnfrmNickName.text = frmMyTopUpEditScreens.txtEditName.text;
                frmViewTopUpBiller.imgComplete.setVisibility(true);
                getCustomerBillersList();
                frmViewTopUpBiller.show();
                TMBUtil.DestroyForm(frmMyTopUpEditScreens);
                //frmViewTopUpBiller.show();
            }
        } else {
            activityStatus = "02";
            alert(kony.i18n.getLocalizedString("ECGenericError"));
        }
        activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1, activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId)
    } else {
        if (status == 300) {
            alert(kony.i18n.getLocalizedString("ECGenericError"));
        }
    }
    dismissLoadingScreen();
}

function deleteOnTopupConfiramtion(callBackOnConfirm) {
    //popDelTopUp.label44836684219623.text = kony.i18n.getLocalizedString("deleteTopUpMsg"); //"Delete would remove this Biller.";
    popDelTopUp.label44836684219678.text = kony.i18n.getLocalizedString("deleteTopUpMsg2"); //"Are you sure you want to continue?";
    popDelTopUp.btnPopDeleteYEs.onClick = callBackOnConfirm;
    if (flowSpa) {} else if (gblDeviceInfo.name == "android") {} else {}
    popDelTopUp.btnPopDeleteCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
    popDelTopUp.btnPopDeleteYEs.text = kony.i18n.getLocalizedString("keyYes");
    popDelTopUp.show();
}

function deleteViewTopUpMB() {
    callCustomerBillDeleteMB();
}

function callCustomerBillDeleteMB() {
    var inputparam = {};
    inputparam["CustomerBillerID"] = gblCustomerBillerIDMB; //frmViewTopUpBiller.lblCnfrmNickName.text;
    inputparam["clientDate"] = getCurrentDate();
    var activityTypeID = "";
    var errorCode = "";
    var activityStatus = "";
    var deviceNickName = "";
    tmpBillerNickName = "";
    tmpBillerNickName = frmViewTopUpBiller.lblCnfrmNickName.text;
    var activityFlexValues1 = kony.i18n.getLocalizedString("Receipent_delete");
    var activityFlexValues2 = gblBillerName;
    var activityFlexValues3 = tmpBillerNickName;
    var activityFlexValues4 = gblBillerRef1;
    var activityFlexValues5 = gblBillerRef2;
    var logLinkageId = "";
    if (gblMyBillerTopUpBB == 0) {
        activityTypeID = "061";
        //activityFlexValues1 = "Delete My Bills MB";
    } else if (gblMyBillerTopUpBB == 1) {
        activityTypeID = "062";
        //activityFlexValues1 = "Delete My Top-Ups MB";
    }
    inputparam["flex1"] = activityFlexValues1;
    inputparam["flex2"] = activityFlexValues2;
    inputparam["flex3"] = activityFlexValues3;
    inputparam["flex4"] = activityFlexValues4;
    inputparam["flex5"] = activityFlexValues5;
    inputparam["typeID"] = activityTypeID;
    showLoadingScreen();
    invokeServiceSecureAsync("customerBillDelete", inputparam, callBackCustomerBillDeleteMB)
}

function callBackCustomerBillDeleteMB(status, resulttable) {
    var activityTypeID = "";
    var errorCode = "";
    var activityStatus = "";
    var deviceNickName = "";
    tmpBillerNickName = "";
    tmpBillerNickName = frmViewTopUpBiller.lblCnfrmNickName.text;
    var activityFlexValues1 = kony.i18n.getLocalizedString("Receipent_delete");
    var activityFlexValues2 = gblBillerName;
    var activityFlexValues3 = tmpBillerNickName;
    var activityFlexValues4 = gblBillerRef1;
    var activityFlexValues5 = gblBillerRef2;
    var logLinkageId = "";
    if (gblMyBillerTopUpBB == 0) {
        activityTypeID = "061";
        //activityFlexValues1 = "Delete My Bills MB";
    } else if (gblMyBillerTopUpBB == 1) {
        activityTypeID = "062";
        //activityFlexValues1 = "Delete My Top-Ups MB";
    }
    if (status == 400) {
        dismissLoadingScreen();
        if (resulttable["opstatus"] == 0) {
            if (resulttable["StatusCode"] != "0") {
                alert(resulttable["errMsg"]);
                popDelTopUp.dismiss();
                activityStatus = "02";
            } else {
                activityStatus = "01";
                popDelTopUp.dismiss();
                //if(myTopupListMB.length=1)
                //					myTopupListMB=[];
                frmMyTopUpList.show();
            }
        } else {
            activityStatus = "02";
            alert("Biller Delete Failed");
        }
        activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1, activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId)
    } else {
        if (status == 300) {
            alert("Biller Delete Failed");
        }
    }
    dismissLoadingScreen();
}

function categoryTopUpSelect(currForm) {
    //popUpMyBillers.show();
    var selected = popUpMyBillers.segPop.selectedIndex[1];
    currentCatSelected = 0;
    var data = popUpMyBillers.segPop.data;
    var selCategory = data[selected].lblCategory.text;
    currentCatSelected = data[selected].BillerCategoryID.text;
    if (currForm == "frmTopUp") {
        frmTopUp.btnTopUpAmountComboBox.text = selCategory;
    } else if (currForm == "frmMyTopUpList") {
        frmMyTopUpList.btnSelectCat.text = selCategory;
    } else if (currForm == "frmSelectBiller") {
        frmSelectBiller.btnSelectCat.text = selCategory;
    } else if (currForm == "frmMyTopUpSelect") {
        frmMyTopUpSelect.btnSelectCat.text = selCategory;
    } else if (currForm == "frmAddTopUpToMB") {
        frmAddTopUpToMB.btnRef2Dropdown.text = selCategory;
        currentCatSelected = null;
    }
    popUpMyBillers.dismiss();
}

function populateMyBillerSuggestMBForSelectBiller(collectiondata) {
    var billername = "";
    var ref1label = "";
    var ref2label = "";
    //BILLER_LOGO_URL="https://vit.tau2904.com/tmb/ImageRender";//hard code to check
    var locale = kony.i18n.getCurrentLocale();
    if (kony.string.startsWith(locale, "en", true) == true) {
        billername = "BillerNameEN";
        ref1label = "LabelReferenceNumber1EN";
        ref2label = "LabelReferenceNumber2EN";
    } else if (kony.string.startsWith(locale, "th", true) == true) {
        billername = "BillerNameTH";
        ref1label = "LabelReferenceNumber1TH";
        ref2label = "LabelReferenceNumber2TH";
    }
    if (!isSearched) TempDataBillers.length = 0;
    myTopUpSuggestListRs.length = 0;
    var masterData = [];
    var tempMinAmount = [];
    gblstepMinAmount = [];
    //	if(!isCatSelected){
    for (var i = 0; i < collectiondata.length; i++) {
        /* minimum amount locha */
        var billerMethod = collectiondata[i]["BillerMethod"];
        if (billerMethod == "1" && collectiondata[i]["BillerGroupType"] == "1" && collectiondata[i]["BillerCompcode"] != "2605") {
            var min = collectiondata[i]["StepAmount"][0]["Amt"];
            tempMinAmount = {
                "compcode": collectiondata[i]["BillerCompcode"],
                "minAmount": min
            }
        } else {
            tempMinAmount = [];
        }
        if (collectiondata[i]["BillerGroupType"] == gblMyBillerTopUpBB) {
            //
            var imagesUrl = BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + collectiondata[i]["BillerCompcode"] + "&modIdentifier=MyBillers";
            //
            var billerNameEN = collectiondata[i]["BillerNameEN"] + " (" + collectiondata[i]["BillerCompcode"] + ")";
            var billerNameTH = collectiondata[i]["BillerNameTH"] + " (" + collectiondata[i]["BillerCompcode"] + ")";
            //ENH113 
            gblbillerStartTime = "";
            gblbillerEndTime = "";
            gblbillerFullPay = "";
            gblbillerAllowSetSched = "";
            gblbillerTotalPayAmtEn = "";
            gblbillerTotalPayAmtTh = "";
            gblbillerTotalIntEn = "";
            gblbillerTotalIntTh = "";
            gblbillerDisconnectAmtEn = "";
            gblbillerDisconnectAmtTh = "";
            gblbillerServiceType = "";
            gblbillerTransType = "";
            gblMEABillerFee = "";
            gblbillerAmountEn = "";
            gblbillerAmountTh = "";
            gblbillerMeterNoEn = "";
            gblbillerMeterNoTh = "";
            gblbillerCustNameEn = "";
            gblbillerCustNameTh = "";
            gblbillerCustAddressEn = "";
            gblbillerCustAddressTh = "";
            gblbillerMeterNoEn = "";
            gblbillerMeterNoTh = "";
            gblbillerCustNameEn = "";
            gblbillerCustNameTh = "";
            gblbillerCustAddressEn = "";
            gblbillerCustAddressTh = "";
            if (collectiondata[i]["BillerCompcode"] == "2533") {
                if (collectiondata[i]["ValidChannel"] != undefined && collectiondata[i]["ValidChannel"].length > 0) {
                    for (var j = 0; j < collectiondata[i]["ValidChannel"].length; j++) {
                        if (collectiondata[i]["ValidChannel"][j]["ChannelCode"] == "02") {
                            gblMEABillerFee = collectiondata[i]["ValidChannel"][j]["FeeAmount"];
                            kony.print("gblMEABillerFee :" + gblMEABillerFee);
                        }
                    }
                }
                if (collectiondata[i]["BillerMiscData"] != undefined && collectiondata[i]["BillerMiscData"].length > 0) {
                    for (var j = 0; j < collectiondata[i]["BillerMiscData"].length; j++) {
                        if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BILLER.StartTime") {
                            gblbillerStartTime = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BILLER.EndTime") {
                            gblbillerEndTime = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BILLER.FullPayment") {
                            gblbillerFullPay = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BILLER.AllowSetSchedule") {
                            gblbillerAllowSetSched = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD1.LABEL.EN") {
                            gblbillerMeterNoEn = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD1.LABEL.TH") {
                            gblbillerMeterNoTh = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD2.LABEL.EN") {
                            gblbillerCustNameEn = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD2.LABEL.TH") {
                            gblbillerCustNameTh = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD3.LABEL.EN") {
                            gblbillerCustAddressEn = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD3.LABEL.TH") {
                            gblbillerCustAddressTh = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD4.LABEL.EN") {
                            gblbillerTotalPayAmtEn = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD4.LABEL.TH") {
                            gblbillerTotalPayAmtTh = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD5.LABEL.EN") {
                            gblbillerAmountEn = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD5.LABEL.TH") {
                            gblbillerAmountTh = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD6.LABEL.EN") {
                            gblbillerTotalIntEn = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD6.LABEL.TH") {
                            gblbillerTotalIntTh = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD7.LABEL.EN") {
                            gblbillerDisconnectAmtEn = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD7.LABEL.TH") {
                            gblbillerDisconnectAmtTh = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BILLER.ServiceType") {
                            gblbillerServiceType = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BILLER.TransactionType") {
                            gblbillerTransType = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                        }
                    }
                    kony.print("gblbillerStartTime: " + gblbillerStartTime + "gblbillerEndTime :" + gblbillerEndTime);
                }
            }
            //ENH113 end
            var tempRecord = {
                "lblSuggestedBiller": collectiondata[i][billername] + " (" + collectiondata[i]["BillerCompcode"] + ")",
                "btnAddBiller": {
                    "text": "  ",
                    "skin": "btnAdd"
                },
                "BarcodeOnly": collectiondata[i]["BarcodeOnly"],
                "BillerID": collectiondata[i]["BillerID"],
                "BillerCompCode": collectiondata[i]["BillerCompcode"],
                "Ref1Label": collectiondata[i][ref1label],
                "Ref2Label": collectiondata[i][ref2label],
                "imgSuggestedBiller": {
                    "src": imagesUrl
                },
                "IsRequiredRefNumber2Add": collectiondata[i]["IsRequiredRefNumber2Add"],
                "IsRequiredRefNumber2Pay": collectiondata[i]["IsRequiredRefNumber2Pay"],
                "EffDt": collectiondata[i]["EffDt"],
                "lblBillerNameEN": billerNameEN,
                "lblBillerNameTH": billerNameTH,
                "BillerMethod": collectiondata[i]["BillerMethod"],
                "BillerGroupType": collectiondata[i]["BillerGroupType"],
                "ToAccountKey": collectiondata[i]["ToAccountKey"],
                "IsFullPayment": collectiondata[i]["IsFullPayment"],
                "Ref1Len": {
                    "text": collectiondata[i]["Ref1Len"]
                },
                "Ref2Len": {
                    "text": collectiondata[i]["Ref2Len"]
                },
                "BillerCategoryID": {
                    "text": collectiondata[i]["BillerCategoryID"]
                },
                "hdnLabelRefNum1EN": {
                    "text": collectiondata[i]["LabelReferenceNumber1EN"]
                },
                "hdnLabelRefNum1TH": {
                    "text": collectiondata[i]["LabelReferenceNumber1TH"]
                },
                "hdnLabelRefNum2EN": {
                    "text": collectiondata[i]["LabelReferenceNumber2EN"]
                },
                "hdnLabelRefNum2TH": {
                    "text": collectiondata[i]["LabelReferenceNumber2TH"]
                },
                "StepAmount": collectiondata[i]["StepAmount"],
                "billerStartTime": gblbillerStartTime,
                "billerEndTime": gblbillerEndTime,
                "billerFullPay": gblbillerFullPay,
                "billerAllowSetSched": gblbillerAllowSetSched,
                "billerTotalPayAmtEn": gblbillerTotalPayAmtEn,
                "billerTotalPayAmtTh": gblbillerTotalPayAmtTh,
                "billerTotalIntEn": gblbillerTotalIntEn,
                "billerTotalIntTh": gblbillerTotalIntTh,
                "billerDisconnectAmtEn": gblbillerDisconnectAmtEn,
                "billerDisconnectAmtTh": gblbillerDisconnectAmtTh,
                "billerFeeAmount": gblMEABillerFee,
                "billerServiceType": gblbillerServiceType,
                "billerTransType": gblbillerTransType,
                "billerAmountEn": gblbillerAmountEn,
                "billerAmountTh": gblbillerAmountTh,
                "billerMeterNoEn": gblbillerMeterNoEn,
                "billerMeterNoTh": gblbillerMeterNoTh,
                "billerCustNameEn": gblbillerCustNameEn,
                "billerCustNameTh": gblbillerCustNameTh,
                "billerCustAddressEn": gblbillerCustAddressEn,
                "billerCustAddressTh": gblbillerCustAddressTh,
                "billerBancassurance": collectiondata[i]["IsBillerBancassurance"],
                "allowRef1AlphaNum": collectiondata[i]["AllowRef1AlphaNum"]
            }
            myTopUpSuggestListRs.push(tempRecord);
            if (!isSearched) TempDataBillers.push(tempRecord);
            if (collectiondata[i]["BillerGroupType"] == "1") {
                if (tempMinAmount.length != 0) gblstepMinAmount.push(tempMinAmount);
            }
        }
    }
    mySelectBillerSuggestListMB = myTopUpSuggestListRs;
    sugBillerCatChangeMB();
    gblTopUpSelectFormDataHolder = myTopUpSuggestListRs;
}

function checkMaxBillerCountOnConfirmMB() {
    var maxCount = kony.os.toNumber(GLOBAL_MAX_BILL_COUNT);
    var segData = frmAddTopUpBillerconfrmtn.segConfirmationList.data;
    var listData = gblCustomerBillList;
    var TotalBillerToBeAdded = segData.length + listData.length;
    if (listData.length == 0) {
        return true;
    } else if (TotalBillerToBeAdded <= maxCount) return true
    else if (TotalBillerToBeAdded > maxCount) return false;
}

function checkMaxBillerCountMB() {
    var BillerList = gblCustomerBillList;
    var maxCount = kony.os.toNumber(GLOBAL_MAX_BILL_COUNT);
    if (BillerList.length == 0) {
        return true;
    } else if (BillerList.length < maxCount) return true
    else if (BillerList.length >= maxCount) return false;
}

function onMyTopUpSelectMoreDynamic(gblTopUpSelectFormDataHolder) {
    var currentForm = kony.application.getCurrentForm();
    var previousForm = kony.application.getPreviousForm();
    var maxLength = kony.os.toNumber(GLOBAL_LOAD_MORE); //configurable parameter also initilaize gblTopUpMore to max length
    gblTopUpMore = gblTopUpMore + maxLength;
    var newData = [];
    var totalData = gblTopUpSelectFormDataHolder; //data from static population 
    var totalLength = totalData.length;
    if (totalLength == 0) {
        if (currentForm["id"] == "frmMyTopUpSelect" || previousForm["id"] == "frmMyTopUpSelect") {} else if (currentForm["id"] == "frmSelectBiller" || previousForm["id"] == "frmSelectBiller") {} else if (currentForm["id"] == "frmMyTopUpList" || previousForm["id"] == "frmMyTopUpList") {
            frmMyTopUpList.lblSuggestedBillersText.setVisibility(false);
        }
    } else {
        if (currentForm["id"] == "frmMyTopUpSelect" || previousForm["id"] == "frmMyTopUpSelect") {} else if (currentForm["id"] == "frmSelectBiller" || previousForm["id"] == "frmSelectBiller") {} else if (currentForm["id"] == "frmMyTopUpList" || previousForm["id"] == "frmMyTopUpList") {
            if (searchFlagForLoadMore) {
                frmMyTopUpList.lblSuggestedBillersText.setVisibility(true);
            } else {
                frmMyTopUpList.lblSuggestedBillersText.setVisibility(false);
            }
        }
    }
    if (totalLength > gblTopUpMore) {
        if (currentForm["id"] == "frmMyTopUpSelect" || previousForm["id"] == "frmMyTopUpSelect") {
            frmMyTopUpSelect.hbxLink.setVisibility(true);
        } else if (currentForm["id"] == "frmSelectBiller" || previousForm["id"] == "frmSelectBiller") {
            //frmSelectBiller.hboxMoreSelectBiller.setVisibility(true);
        } else if (currentForm["id"] == "frmMyTopUpList" || previousForm["id"] == "frmMyTopUpList") {
            if (searchFlagForLoadMore) {
                frmMyTopUpList.hbxMore.setVisibility(true);
            } else {
                frmMyTopUpList.hbxMore.setVisibility(false);
            }
        }
        var endPoint = gblTopUpMore
    } else {
        if (currentForm["id"] == "frmMyTopUpSelect" || previousForm["id"] == "frmMyTopUpSelect") {
            frmMyTopUpSelect.hbxLink.setVisibility(false);
        } else if (currentForm["id"] == "frmSelectBiller" || previousForm["id"] == "frmSelectBiller") {
            //frmSelectBiller.hboxMoreSelectBiller.setVisibility(false);
        } else if (currentForm["id"] == "frmMyTopUpList" || previousForm["id"] == "frmMyTopUpList") {
            frmMyTopUpList.hbxMore.setVisibility(false);
        }
        var endPoint = totalLength;
    }
    for (i = 0; i < endPoint; i++) {
        newData.push(gblTopUpSelectFormDataHolder[i])
    }
    if (currentForm["id"] == "frmMyTopUpSelect" || previousForm["id"] == "frmMyTopUpSelect") {
        frmMyTopUpSelect.segSelectList.data = newData;
        dismissLoadingScreen();
    } else if (currentForm["id"] == "frmMyTopUpList" || previousForm["id"] == "frmMyTopUpList") {
        frmMyTopUpList.segSuggestedBillers.data = newData;
        dismissLoadingScreen();
    } else if (currentForm["id"] == "frmSelectBiller" || previousForm["id"] == "frmSelectBiller") {
        //frmSelectBiller.segSuggestedBillers.data = newData;
        frmSelectBiller.segSuggestedBillers.data = gblTopUpSelectFormDataHolder;
        gblBillersFound = true;
        dismissLoadingScreen();
    }
}
// Function to check mbstatus id of user to block Add Biller
function startMBBillTopUpProfileInqService() {
    var inputParams = {
        //crmId: gblcrmId,
        rqUUId: "",
        channelName: "MB-INQ"
    };
    showIBLoadingScreen();
    invokeServiceSecureAsync("crmProfileInq", inputParams, startMBBillTopUpProfileInqServiceAsyncCallback);
}

function startMBBillTopUpProfileInqServiceAsyncCallback(status, callBackResponse) {
    var currForm = kony.application.getCurrentForm().id;
    if (status == 400) {
        if (callBackResponse["opstatus"] == 0) {
            gblTranxLockedForBiller = callBackResponse["mbFlowStatusIdRs"];
            gblIBFlowStatus = callBackResponse["ibUserStatusIdTr"];
            gblEmailIdBillerNotification = callBackResponse["emailAddr"];
        }
    } else {}
}