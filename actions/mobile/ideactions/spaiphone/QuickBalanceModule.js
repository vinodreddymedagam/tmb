// Below GLobal Variables which will be used only in this module hence declaring here
gblIsPullToRefreshCall = false;
gblQBCalled = true;
gblQuickBalanceDeviceId = "";

function changeLocalQuickBalanceService(deviceId) {
    gblQBCalled = true;
    dismissLoadingScreen();
    callBackQuickBalance(400, gblQuickBalanceResponse);
}
gotUserStatus = false;
isUserStatusActive = false;
//chkUserStatusCallBack
function chkUserStatusCallBack(status, resulttable) {
    try {
        //dismissLoadingScreen();
        kony.print("FPRINT USER STATUS IS" + status);
        if (status == 400) {
            if (resulttable["opstatus"] == 8005) {
                if (resulttable["errCode"] == 1002) {
                    kony.print("FPRINT SETTING isUserStatusActive TO FALSE");
                    isUserStatusActive = false;
                    kony.print("FPRINT USER STATUS IS INACTIVE");
                    showFPFlex(false);
                    onClickChangeTouchIcon();
                    kony.print("FPRINT USER STATUS errMsg=" + kony.i18n.getLocalizedString("VQB_MBStatus"));
                } else {
                    kony.print("FPRINT SETTING isUserStatusActive TO TRUE");
                    isUserStatusActive = true;
                    kony.print("FPRINT USER STATUS IS ACTIVE");
                    frmMBPreLoginAccessesPin.btnTouchnBack.skin = "btntouchiconstudio5";
                    frmMBPreLoginAccessesPin.btnTouchnBack.focusSkin = "btntouchiconfoc";
                    showFPFlex(true);
                    fpdefault();
                    closeQuickBalance();
                }
            } else {
                kony.print("FPRINT SETTING isUserStatusActive TO TRUE");
                isUserStatusActive = true;
                kony.print("FPRINT USER STATUS IS ACTIVE ELSE");
                frmMBPreLoginAccessesPin.btnTouchnBack.skin = "btntouchiconstudio5";
                frmMBPreLoginAccessesPin.btnTouchnBack.focusSkin = "btntouchiconfoc";
                showFPFlex(true);
                fpdefault();
                closeQuickBalance();
            }
            gotUserStatus = true;
            dismissLoadingScreen();
            /*else if (resulttable["opstatus"] == 1011) {
                //showAlertWithCallBack(kony.i18n.getLocalizedString("genErrorWifiOff"), kony.i18n.getLocalizedString("info"), closeQuickBalance);
				kony.print("FPRINT USER STATUS IS INACTIVE");
				kony.print("FPRINT USER STATUS errMsg="+kony.i18n.getLocalizedString("genErrorWifiOff"));
            } else {
                //showAlertWithCallBack(kony.i18n.getLocalizedString("VQB_ServiceDown"), kony.i18n.getLocalizedString("info"), closeQuickBalance);
				kony.print("FPRINT USER STATUS IS INACTIVE");
				kony.print("FPRINT USER STATUS errMsg="+kony.i18n.getLocalizedString("VQB_ServiceDown"));
            } */
        }
    } catch (e) {
        //showAlertWithCallBack(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"), closeQuickBalance);
        kony.print("FPRINT USER STATUS errMsg=" + kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
    }
}
// Quick balance service call back function
function callBackQuickBalance(status, resulttable) {
    try {
        dismissLoadingScreen();
        var locale_app = kony.i18n.getCurrentLocale();
        enableFormElements();
        var accountName = "";
        var availBalLabel = "";
        var availBalValue = ""
        var masterData = [];
        if (status == 400) {
            if (resulttable["opstatus"] == "0.0" || resulttable["opstatus"] == 0) {
                gblQuickBalanceResponse = resulttable;
                if (resulttable["setQuickBalance"] == "true" || resulttable["setQuickBalance"] == true) {
                    var collectionData = resulttable["custAcctRec"];
                    for (var i = 0; i < resulttable["custAcctRec"].length; i++) {
                        if ((resulttable["custAcctRec"][i]["acctNickName"]) == null || (resulttable["custAcctRec"][i]["acctNickName"]) == undefined) {
                            if (locale_app == "th_TH") accountName = resulttable["custAcctRec"][i]["defaultCurrentNickNameTH"];
                            else accountName = resulttable["custAcctRec"][i]["defaultCurrentNickNameEN"];
                        } else {
                            accountName = resulttable["custAcctRec"][i]["acctNickName"];
                        }
                        if ((resulttable["custAcctRec"][i]["accType"]) == "CCA") {
                            availBalLabel = kony.i18n.getLocalizedString("keyMBCreditlimit").replace(":", "");
                            availBalValue = resulttable["custAcctRec"][i]["availableCreditBalDisplay"] + kony.i18n.getLocalizedString("currencyThaiBaht");
                        } else {
                            availBalLabel = kony.i18n.getLocalizedString("Balance").replace(":", "");
                            availBalValue = resulttable["custAcctRec"][i]["availableBalDisplay"] + kony.i18n.getLocalizedString("currencyThaiBaht");
                        }
                        var tempRecord = {
                            "lblAccountNickName": accountName,
                            "lblAvailableBalanceValue": availBalValue,
                            "lblAvailBalText": availBalLabel,
                            "lblSeperator": "."
                        };
                        masterData.push(tempRecord);
                    }
                    frmMBPreLoginAccessesPin.segAccounts.setData(masterData);
                    if (!gblIsPullToRefreshCall) {
                        showQuickBalanceResults();
                    } else {
                        frmMBPreLoginAccessesPin.flexLoading.isVisible = false;
                        frmMBPreLoginAccessesPin.segAccounts.isVisible = true;
                    }
                    gblIsPullToRefreshCall = false;
                } else {
                    showQuickBalanceImage();
                }
            } else if (resulttable["opstatus"] == 8005) {
                frmMBPreLoginAccessesPin.flexLoading.isVisible = false;
                if (resulttable["errCode"] == 1002) {
                    //showFPFlex(false);
                    kony.print("FPRINT SETTING isUserStatusActive TO FALSE");
                    isUserStatusActive = false;
                    onClickChangeTouchIcon();
                    showAlertWithCallBack(kony.i18n.getLocalizedString("VQB_MBStatus"), kony.i18n.getLocalizedString("info"), closeQuickBalance);
                } else {
                    var errMsg = resulttable["errMsg"];
                    showAlertWithCallBack(errMsg, kony.i18n.getLocalizedString("info"), closeQuickBalance);
                }
            } else if (resulttable["opstatus"] == 1011) {
                frmMBPreLoginAccessesPin.flexLoading.isVisible = false;
                showAlertWithCallBack(kony.i18n.getLocalizedString("genErrorWifiOff"), kony.i18n.getLocalizedString("info"), closeQuickBalance);
            } else {
                frmMBPreLoginAccessesPin.flexLoading.isVisible = false;
                showAlertWithCallBack(kony.i18n.getLocalizedString("VQB_ServiceDown"), kony.i18n.getLocalizedString("info"), closeQuickBalance);
            }
        }
    } catch (e) {
        showAlertWithCallBack(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"), closeQuickBalance);
        frmMBPreLoginAccessesPin.flexLoading.isVisible = false;
    }
}

function showQuickBalanceResults() {
    frmMBPreLoginAccessesPin.flexQuickBalTextForAccoiunts.isVisible = true;
    frmMBPreLoginAccessesPin.segAccounts.isVisible = true;
    frmMBPreLoginAccessesPin.flexLoading.isVisible = false;
}

function disableFormElements() {
    frmMBPreLoginAccessesPin.flexQuickBalanceCancel.onTouchEnd = doNothing;
    frmMBPreLoginAccessesPin.btnMenu.onClick = doNothing;
    frmMBPreLoginAccessesPin.flexQuickBalButton.onTouchEnd = doNothing;
    var gestureConfig = {
        fingers: 1,
        swipeDirection: 4
    };
    // frmMBPreLoginAccessesPin.flexQuickBalanceCancel.addGestureRecognizer(2, gestureConfig, doNothing);
    //  frmMBPreLoginAccessesPin.btnQuickBalanceCancel.setEnabled(false);
    //  frmMBPreLoginAccessesPin.btnMenu.setEnabled(false);
}

function doNothing() {}

function enableFormElements() {
    frmMBPreLoginAccessesPin.flexQuickBalButton.onTouchEnd = showQuickBalance;
    frmMBPreLoginAccessesPin.flexQuickBalanceCancel.onTouchEnd = closeQuickBalance;
    frmMBPreLoginAccessesPin.btnMenu.onClick = handleMenuBtn;
    var gestureConfig = {
        fingers: 1,
        swipeDirection: 4
    };
    // frmMBPreLoginAccessesPin.flexQuickBalanceCancel.addGestureRecognizer(2, gestureConfig, closeQuickBalance);
    // frmMBPreLoginAccessesPin.btnQuickBalanceCancel.setEnabled(true);
    // frmMBPreLoginAccessesPin.btnMenu.setEnabled(true);
}

function invokeQuickBalanceService(deviceId) {
    gblQBCalled = true;
    var inputParam = {};
    inputParam["quickBalanceFlag"] = "true";
    dismissLoadingScreen();
    if (deviceId != "") {
        gblQuickBalanceDeviceId = deviceId;
    }
    var enCryptDeviceId = encryptData(gblQuickBalanceDeviceId);
    kony.print("enCryptDeviceId::::" + enCryptDeviceId)
        //inputParam["deviceId"] = gblQuickBalanceDeviceId; // Commented this as we are sending new ecrypted device id to the service
    inputParam["encryptDeviceId"] = enCryptDeviceId;
    invokeServiceSecureAsync("quickBalance", inputParam, callBackQuickBalance);
}

function chkUserStatus(deviceId) {
    gblQBCalled = true;
    var inputParam = {};
    inputParam["quickBalanceFlag"] = "true";
    inputParam["chkUserStatusOnly"] = "yes";
    dismissLoadingScreen();
    if (deviceId != "") {
        gblQuickBalanceDeviceId = deviceId;
    }
    var enCryptDeviceId = encryptData(gblQuickBalanceDeviceId);
    kony.print("enCryptDeviceId::::" + enCryptDeviceId)
        //inputParam["deviceId"] = gblQuickBalanceDeviceId; // Commented this as we are sending new ecrypted device id to the service
    inputParam["encryptDeviceId"] = enCryptDeviceId;
    invokeServiceSecureAsync("quickBalance", inputParam, chkUserStatusCallBack);
}

function showQuickBalanceImage() {
    kony.print("device details :" + kony.os.deviceInfo())
    var locale_app = kony.i18n.getCurrentLocale();
    if (locale_app == "th_TH") {
        frmMBPreLoginAccessesPin.imgQBStatic.src = "quick_balance_th.png"
    } else {
        frmMBPreLoginAccessesPin.imgQBStatic.src = "quick_balance.png"
    }
    frmMBPreLoginAccessesPin.flexQuickBalTextForAccoiunts.isVisible = false;
    frmMBPreLoginAccessesPin.imgHeaderLogo.isVisible = false;
    frmMBPreLoginAccessesPin.lblHeader1.isVisible = true;
    frmMBPreLoginAccessesPin.lblHeader2.isVisible = true;
    frmMBPreLoginAccessesPin.imgQBStatic.isVisible = true;
    var deviceWidth = parseInt(kony.os.deviceInfo()["deviceWidth"]);
    kony.print("deviceWidth:::::::" + deviceWidth)
    var deviceName = kony.os.deviceInfo()["name"];
    kony.print("deviceWidth:::::::" + deviceWidth)
    if (deviceWidth <= 768 && deviceName == "android") {
        kony.print(":::::: satisfying deviceWidth for android")
        frmMBPreLoginAccessesPin.btnLoginToQB.width = "300dp"
    }
    frmMBPreLoginAccessesPin.flexLoginToQuickBalance.isVisible = true
    frmMBPreLoginAccessesPin.flexLoading.isVisible = false;
}

function showQuickBalance() {
    kony.print("device details :" + kony.os.deviceInfo())
    dismissLoadingScreen();
    if (gblQBCalled) {
        //gblQBCalled = false;
        if (kony.net.isNetworkAvailable(constants.NETWORK_TYPE_ANY)) {
            disableFormElements();
            frmMBPreLoginAccessesPin.flexAccounts.isVisible = true;
            frmMBPreLoginAccessesPin.flexQuickBalButton.isVisible = false;
            frmMBPreLoginAccessesPin.flexLoading.isVisible = true;
            frmMBPreLoginAccessesPin.flexQuickBalanceCancel.isVisible = true;
            frmMBPreLoginAccessesPin.flexQuickBalTextForAccoiunts.isVisible = true;
            // invokeQuickBalanceService();
            GBL_FLOW_ID_CA = 11;
            frmMBPreLoginAccessesPin.flexKeyboard.animate(kony.ui.createAnimation({
                "100": {
                    "top": "86%",
                    "stepConfig": {
                        "timingFunction": kony.anim.EASE
                    }
                }
            }), {
                "delay": 0,
                "iterationCount": 1,
                "fillMode": kony.anim.FILL_MODE_FORWARDS,
                "duration": 0.25
            });
        } else {
            alert(kony.i18n.getLocalizedString("genErrorWifiOff"));
            gblQBCalled = true;
        }
    }
}

function hideQBImage() {
    frmMBPreLoginAccessesPin.imgHeaderLogo.isVisible = true;
    frmMBPreLoginAccessesPin.lblHeader1.isVisible = false;
    frmMBPreLoginAccessesPin.lblHeader2.isVisible = false;
    frmMBPreLoginAccessesPin.imgQBStatic.isVisible = false;
    frmMBPreLoginAccessesPin.flexLoginToQuickBalance.isVisible = false;
    frmMBPreLoginAccessesPin.flexQuickBalTextForAccoiunts.isVisible = false;
}

function hideQuickBalanceResults() {
    frmMBPreLoginAccessesPin.segAccounts.isVisible = false;
    frmMBPreLoginAccessesPin.flexQuickBalTextForAccoiunts.isVisible = false;
}

function closeQuickBalance() {
    gblQBCalled = true;
    gblQuickBalanceResponse = "";
    //if(frmMBPreLoginAccessesPin.flexKeyboard.isVisible){
    kony.print("FPRINT ENETERED1");
    frmMBPreLoginAccessesPin.flexKeyboard.animate(kony.ui.createAnimation({
        "100": {
            "top": "24%",
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            }
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.25
    });
    frmMBPreLoginAccessesPin.flexKeyboard.top = "24%";
    //  }  
    kony.print("FPRINT ENETERED3 top=" + frmMBPreLoginAccessesPin.flxFingerPrint.top);
    frmMBPreLoginAccessesPin.flexAccounts.isVisible = false;
    frmMBPreLoginAccessesPin.flexQuickBalButton.isVisible = true;
    frmMBPreLoginAccessesPin.flexQuickBalanceCancel.isVisible = false;
    frmMBPreLoginAccessesPin.flexLoading.isVisible = false;
    if (frmMBPreLoginAccessesPin.segAccounts.isVisible) {
        hideQuickBalanceResults();
    } else {
        hideQuickBalanceResults();
        hideQBImage();
    }
}

function showAnimation1() {
    var currForm = frmMBPreLoginAccessesPin;
    //  if(!isNullorEmpty(currForm.loadingBtnAnim1)){
    currForm.loadingBtnAnim1.animate(kony.ui.createAnimation({
        "0": {
            "stepConfig": {
                "timingFunction": kony.anim.LINEAR
            },
            "opacity": 0.0
        },
        "12": {
            "stepConfig": {
                "timingFunction": kony.anim.LINEAR
            },
            "opacity": 0.42
        },
        "25": {
            "stepConfig": {
                "timingFunction": kony.anim.LINEAR
            },
            "opacity": 0.85
        },
        "29": {
            "stepConfig": {
                "timingFunction": kony.anim.LINEAR
            },
            "opacity": 1.0
        },
        "38": {
            "stepConfig": {
                "timingFunction": kony.anim.LINEAR
            },
            "opacity": 0.85
        },
        "50": {
            "stepConfig": {
                "timingFunction": kony.anim.LINEAR
            },
            "opacity": 0.42
        },
        "54": {
            "stepConfig": {
                "timingFunction": kony.anim.LINEAR
            },
            "opacity": 0.14
        },
        "75": {
            "stepConfig": {
                "timingFunction": kony.anim.LINEAR
            },
            "opacity": 0.0
        },
        "100": {
            "stepConfig": {
                "timingFunction": kony.anim.LINEAR
            },
            "opacity": 0.0
        }
    }), {
        "delay": 0,
        "iterationCount": 0,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 1.0
    }, {
        "animationEnd": function() {}
    });
    //    }
}

function showAnimation2() {
    var currForm = frmMBPreLoginAccessesPin;
    //    if(!isNullorEmpty(currForm.loadingBtnAnim2)){
    currForm.loadingBtnAnim2.animate(kony.ui.createAnimation({
        "0": {
            "stepConfig": {
                "timingFunction": kony.anim.LINEAR
            },
            "opacity": 0.0
        },
        "33": {
            "stepConfig": {
                "timingFunction": kony.anim.LINEAR
            },
            "opacity": 0.14
        },
        "38": {
            "stepConfig": {
                "timingFunction": kony.anim.LINEAR
            },
            "opacity": 0.42
        },
        "50": {
            "stepConfig": {
                "timingFunction": kony.anim.LINEAR
            },
            "opacity": 0.85
        },
        "54": {
            "stepConfig": {
                "timingFunction": kony.anim.LINEAR
            },
            "opacity": 1.0
        },
        "62": {
            "stepConfig": {
                "timingFunction": kony.anim.LINEAR
            },
            "opacity": 0.85
        },
        "70": {
            "stepConfig": {
                "timingFunction": kony.anim.LINEAR
            },
            "opacity": 0.42
        },
        "75": {
            "stepConfig": {
                "timingFunction": kony.anim.LINEAR
            },
            "opacity": 0.14
        },
        "79": {
            "stepConfig": {
                "timingFunction": kony.anim.LINEAR
            },
            "opacity": 0.0
        },
        "100": {
            "stepConfig": {
                "timingFunction": kony.anim.LINEAR
            },
            "opacity": 0.0
        }
    }), {
        "delay": 0,
        "iterationCount": 0,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 1.0
    }, {
        "animationEnd": function() {}
    });
    //}  
}

function genRandomNum() {
    return Math.floor((Math.random() * 10000) + 1);
}

function preshowPreAccessScreen() {
    kony.print("FPRINT: preshowPreAccessScreen called");
    if (gblCallPrePost) {
        frmMBPreLoginAccessesPin.bounces = false;
        //frmMBPreLoginAccessesPin.scrollboxMain.bounces = false;
        afterLogoutMBInPreshowSnippetCode();
        //DisableFadingEdges(frmMBPreLoginAccessesPin);
    }
    preShowSetLoginCircle();
}

function preShowSetLoginCircle() {
    var deviceName = gblDeviceInfo["name"];
    var screenwidth = gblDeviceInfo["deviceWidth"];
    if (deviceName == "android") {
        if (screenwidth == 1080) {} else if (screenwidth == 768) {
            frmMBPreLoginAccessesPin.CopyFlexContainer0bddc389f3e0d42.height = "92%";
            frmMBPreLoginAccessesPin.CopyFlexContainer063256fd1562a46.height = "92%";
            frmMBPreLoginAccessesPin.CopyFlexContainer0e661061f71994f.height = "92%";
            frmMBPreLoginAccessesPin.CopyFlexContainer096ba943765d344.height = "92%";
            frmMBPreLoginAccessesPin.CopyFlexContainer0bddc389f3e0d42.width = "75%";
            frmMBPreLoginAccessesPin.CopyFlexContainer063256fd1562a46.width = "75%";
            frmMBPreLoginAccessesPin.CopyFlexContainer0e661061f71994f.width = "75%";
            frmMBPreLoginAccessesPin.CopyFlexContainer096ba943765d344.width = "75%";
            frmMBPreLoginAccessesPin.btnOne.height = "99%";
            frmMBPreLoginAccessesPin.btnTwo.height = "99%";
            frmMBPreLoginAccessesPin.btnThree.height = "99%";
            frmMBPreLoginAccessesPin.btnFour.height = "99%";
            frmMBPreLoginAccessesPin.btnFive.height = "99%";
            frmMBPreLoginAccessesPin.btnSix.height = "99%";
            frmMBPreLoginAccessesPin.btnSeven.height = "99%";
            frmMBPreLoginAccessesPin.btnEight.height = "99%";
            frmMBPreLoginAccessesPin.btnNine.height = "99%";
            frmMBPreLoginAccessesPin.btnForgotPin.height = "99%";
            frmMBPreLoginAccessesPin.btnZero.height = "99%";
            frmMBPreLoginAccessesPin.btnTouchnBack.height = "99%";
        } else if (screenwidth == 720) {
            frmMBPreLoginAccessesPin.CopyFlexContainer0bddc389f3e0d42.height = "92%";
            frmMBPreLoginAccessesPin.CopyFlexContainer063256fd1562a46.height = "92%";
            frmMBPreLoginAccessesPin.CopyFlexContainer0e661061f71994f.height = "92%";
            frmMBPreLoginAccessesPin.CopyFlexContainer096ba943765d344.height = "92%";
            frmMBPreLoginAccessesPin.btnOne.height = "99%";
            frmMBPreLoginAccessesPin.btnTwo.height = "99%";
            frmMBPreLoginAccessesPin.btnThree.height = "99%";
            frmMBPreLoginAccessesPin.btnFour.height = "99%";
            frmMBPreLoginAccessesPin.btnFive.height = "99%";
            frmMBPreLoginAccessesPin.btnSix.height = "99%";
            frmMBPreLoginAccessesPin.btnSeven.height = "99%";
            frmMBPreLoginAccessesPin.btnEight.height = "99%";
            frmMBPreLoginAccessesPin.btnNine.height = "99%";
            frmMBPreLoginAccessesPin.btnForgotPin.height = "99%";
            frmMBPreLoginAccessesPin.btnZero.height = "99%";
            frmMBPreLoginAccessesPin.btnTouchnBack.height = "99%";
        }
    }
}

function postshowPreAccessScreen() {
    if (gblCallPrePost) {
        //commonMBPostShow();
        //frmAccountSummaryLanding = null;
        //frmAccountSummaryLandingGlobals();
        TMBUtil.DestroyForm(frmAccountSummaryLanding);
        //moved from accsPwdValidatnLogin to here to prevent black screen after entering the access pin
        /* 
	MBcopyright_text_display.call(this);
	
	 */
        /* 
	campaignPreLoginService.call(this,"imgMBPreLogin", "frmAfterLogoutMB", "M");
	
	 */
        /* 
	MBcopyright_text_display.call(this);
	
	 */
        /* 
	 
	
	 */
        //setCallBacks.call(this);
        // Below code is for Quick Balance and to show loading indicator animation
        kony.print("device details :" + kony.os.deviceInfo())
            //setGestureToQuick();
            //setGestureToClose();
        if (frmMBPreLoginAccessesPin.flexAccounts.isVisible || frmMBPreLoginAccessesPin.flxFingerPrint.isVisible) {
            closeQuickBalance();
        }
    }
    showAnimation1();
    showAnimation2();
    assignGlobalForMenuPostshow();
    if (gblDeviceInfo["name"] == "android") {
        if (frmMBPreLoginAccessesPin.btnTouchnBack.skin == "btntouchiconstudio5" && frmMBPreLoginAccessesPin.flxFingerPrint.isVisible) {
            authUsingTouchID();
        }
    }
}

function onClickKeyBoardButton(eventobject) {
    onClickOfKeypad();
    getPin(eventobject);
    onClickChangeTouchIcon();
}

function onClickLoginToQB() {
    gblQuickBalanceFromLogin = true;
    closeQuickBalance();
}

function showQuickBalanceButton() {
    frmMBPreLoginAccessesPin.flexQuickBalButton.isVisible = false;
}

function onPullQuickBalanceSegment() {
    frmMBPreLoginAccessesPin.segAccounts.removeAll();
    disableFormElements();
    frmMBPreLoginAccessesPin.segAccounts.isVisible = false;
    gblIsPullToRefreshCall = true;
    frmMBPreLoginAccessesPin.flexLoading.isVisible = true;
    invokeQuickBalanceService("");
}

function changeSkinForSpecifyBtn(buttonId) {
    //var buttonId = eventObject.id;
    if (buttonId == "btnSpecify") {
        if (frmIBOpenNewSavingsAcc.btnSpecify.skin == "sknbtnGreyFont80") {
            frmIBOpenNewSavingsAcc.btnSpecify.skin == "sknBtnBlueFont80"
            frmIBOpenNewSavingsAcc.btnNotSpecify.skin == "sknbtnGreyFont80"
        }
    } else if (buttonId == "btnNotSpecify") {
        if (frmIBOpenNewSavingsAcc.btnNotSpecify.skin == "sknbtnGreyFont80") {
            frmIBOpenNewSavingsAcc.btnNotSpecify.skin == "sknBtnBlueFont80"
            frmIBOpenNewSavingsAcc.btnSpecify.skin == "sknbtnGreyFont80"
        }
    }
}