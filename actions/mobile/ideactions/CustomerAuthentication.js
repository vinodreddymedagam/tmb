







function activateAccount() {
	var actCodePatt = /[a-z0-9]{10}$/ig;
	var resultActCodePatt = actCodePatt.test(frmMBActivation.txtActivationCode.text);
	//	accNoPatt=/^[0-9]{3}-[0-9]-[0-9]{5}-[0-9]$/g;
	var accNoPatt1 = /^[0-9\d-]+$/g;
	var resultAccNoPatt = accNoPatt1.test(frmMBActivation.txtAccountNumber.text);
	//	IDPatt=/^[0-9]-[0-9]{4}-[0-9]{5}-[0-9]{2}-[0-9]$/g;
	var IDPatt1 = /^[0-9\d-]+$/g; ///[0-9-]{13}$/g;
	resultIDPatt = IDPatt1.test(frmMBActivation.txtIDPass.text);
	//	PassportPatt=/^[A-Z]{10}$/g;
	var PassportPatt = /^[A-Z\d-]+$/g;
	resultPassPatt = PassportPatt.test(frmMBActivation.txtIDPass.text);
	var resultIDPassPatt = resultIDPatt || resultPassPatt;
	if (!(resultActCodePatt || resultAccNoPatt || resultIDPassPatt))
		alert("Result: \nActivate Code:" + resultActCodePatt + "\nAccount Number:" + resultAccNoPatt +
			"\nID No. / Passport No.:" + resultIDPassPatt);
	else {
		if (gblACTCODEVALID && gblSTATUS)
			frmMBActiConfirm.show();
		else
			popActCodeInvalid.show();
	}
}

function navigateToTandC() {
	registerForTimeOut();
	if (isMenuShown == false) {
		//frmMBanking.hbox44538774217453.skin = "hbxOnfocus";
		/*if (isMenuRendered == true) {
		//frmMBanking.vbox47407564342877.remove(hboxMenuHeader,SegMainMenu);
          //frmMBanking.vbox47407564342877.removeAt(1);
          //frmMBanking.vbox47407564342877.removeAt(0);
		}*/
		frmMBTnCPreShow();
		//frmMBTnC.show();
	} else {
		isMenuShown = false;
		frmMBanking.scrollboxMain.scrollToEnd();
	}
}
/*
************************************************************************
            Name    : otpValidation
            Author  : Developer
            Date    : April, 2013
            Purpose : to check validation of OTP and verify it with server
        	Input params: entered OTP text by end user
        	Output params: validate entered OTP and then chek it on server
        	ServiceCalls: yes
        	Called From : on click of next button 
************************************************************************
 */

function otpValidation(text) {
	//kony.application.showLoadingScreen(frmLoading, "",constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
	//showLoadingScreen();
	showLoadingPopUpScreen();
	otpCodePattStr = "^[0-9]{" + gblOTPLENGTH + "}$";
	var otpCodePatt = new RegExp(otpCodePattStr, "g");
	resultOtpCodePatt = otpCodePatt.test(text);
	if (resultOtpCodePatt) {
		//	kony.application.showLoadingScreen(frmLoading, "",constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
		var inputParam = {};
		//inputParam["tokenUUID"] = gblTokenNum;
		inputParam["otp"] = text;
		inputParam["retryCounterVerifyOTP"] = gblVerifyOTPCounter;
		//inputParam["session"] = "session";
		invokeServiceSecureAsync("verifyOTP", inputParam, callBackOTPVerify)
	} else {
		//var invlidOTP = kony.i18n.getLocalizedString("invalidOTP");
		//var info1 = kony.i18n.getLocalizedString("info");
		//var okk = kony.i18n.getLocalizedString("keyOK");
		popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("invalidOTP");
		popupTractPwd.lblPopupTract2.text = "";
		popupTractPwd.lblPopupTract4.text = "";
		//Defining basicConf parameter for alert
		//var basicConf = {
		//	message: invlidOTP,
		//	alertType: constants.ALERT_TYPE_INFO,
		//	alertTitle: info1,
		//	yesLabel: okk,
		//	noLabel: "",
		//	alertHandler: handle2
		//};
		kony.application.dismissLoadingScreen();
		//Defining pspConf parameter for alert
		//var pspConf = {};
		//Alert definition
		//var infoAlert = kony.ui.Alert(basicConf, pspConf);
		//function handle2(response) {}
		//	alert("INVALID ACTIVATION CODE");
		return false;
	}
}

function callBackOTPVerify(status, resulttable) {
	if (status == 400) {
		
		//For activity logging
		var successFlag = false;
		if (resulttable["opstatus"] == 0) {
			gblVerifyOTPCounter = "0";
			getHeader(mobileMethod, kony.i18n.getLocalizedString("keySetPasswordLabel"), 0, 0, 0);
			//gblShowPinPwdSecs = kony.os.toNumber(resulttable["showPinPwdSecs"]);
			gblRetryCountRequestOTP = "0";
			//gblShowPwdNo = kony.os.toNumber(resulttable["showPinPwdCount"]);
			//popupTractPwd.dismiss(); added in preshow of the form for fixing defect #690
			chngUseridspa = false;
			chngPwdspa = false;
			successFlag = true;
			kony.application.dismissLoadingScreen();
			popupTractPwd.destroy();
			
			//Have to remove action code based handling as there is no difference now. Will show new transaction password page. ENH_207_1
			if(gblMBActivationVia == "2" || gblMBActivationVia == "1" || gblMBActivationVia == "3"){
				//IF OTP Success
					//MIB-532 - Always Show Email Address Field
					frmMBActiEmailDeviceName.lblEmailHead.setVisibility(true);
					frmMBActiEmailDeviceName.tbxEmail.setVisibility(true);
					frmMBActiEmailDeviceName.lblEmailMsg.setVisibility(true);
					frmMBActiEmailDeviceName.lineEmail.setVisibility(true);
					frmMBActiEmailDeviceName.lblHdrTxt.text=kony.i18n.getLocalizedString("keySetEmailDeviceName");
					frmMBActiEmailDeviceName.lblInfo.text=kony.i18n.getLocalizedString("keyPlzSetEmailDeviceName");
					if(isNotBlank(resulttable["crmEmailAddr"])){
						frmMBActiEmailDeviceName.tbxEmail.text = resulttable["crmEmailAddr"];
					} else {
						frmMBActiEmailDeviceName.tbxEmail.text = "";
					}	
			
				if(isNotBlank(resulttable["deviceNickname"])){
					frmMBActiEmailDeviceName.tbxDeviceName.text=resulttable["deviceNickname"];
				}else{
					frmMBActiEmailDeviceName.tbxDeviceName.text="";
				}
				frmMBActiEmailDeviceName.show();

			}else{
				if (gblActionCode == "23") {
				onClickNextPwdRules();
				}else{
				frmAccTrcPwdInter.show();
				}
			}	
			
		} else if (resulttable["opstatus"] == 8005) {
			/*if (resulttable["errCode"] == "VrfyOTPCtrErr00001" || resulttable["errCode"] == "VrfyOTPCtrErr00002" || resulttable["errCode"] == "VrfyOTPCtrErr00003"){
					if(resulttable["errCode"] == "VrfyOTPCtrErr00001"){
						gblVerifyOTPCounter = "1";
					}else if(resulttable["errCode"] == "VrfyOTPCtrErr00002"){
						gblVerifyOTPCounter = "2";
					}else if(resulttable["errCode"] == "VrfyOTPCtrErr00003"){
						gblVerifyOTPCounter = "3";
					}*/
			if (resulttable["errCode"] == "VrfyOTPErr00001") {
				gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
				//caseInCorrectOTP();
				if (flowSpa) {
					popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("wrongOTP");
					popupTractPwd.lblPopupTract2Spa.text = "";
					popupTractPwd.lblPopupTract4Spa.text = "";
				} else {
					popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("wrongOTP");
					popupTractPwd.lblPopupTract2.text = "";
					popupTractPwd.lblPopupTract4.text = "";
					popupTractPwd.txtOTP.text="";
				}
				kony.application.dismissLoadingScreen();
				return false;
			} else if (resulttable["errCode"] == "VrfyOTPErr00002") {
				gblVerifyOTPCounter = "0";
				gblRetryCountRequestOTP = "0";
				kony.application.dismissLoadingScreen();
				popupTractPwd.dismiss();
				showAlertWithCallBack(kony.i18n.getLocalizedString("ECVrfyOTPErr"), kony.i18n.getLocalizedString("info"),
					calBackLockAlert);
				return false;
			} else if (resulttable["errCode"] == "VrfyOTPErr00003") {
				kony.application.dismissLoadingScreen();
				showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00003"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (resulttable["errCode"] == "VrfyOTPErr00004") {
				kony.application.dismissLoadingScreen();
				showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00004"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (resulttable["errCode"] == "VrfyOTPErr00005") {
				kony.application.dismissLoadingScreen();
				showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00005"), kony.i18n.getLocalizedString("info"));
				return false;
			}else if (resulttable["errCode"] == "VrfyOTPErr00006") {
				kony.application.dismissLoadingScreen();
				showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00006"), kony.i18n.getLocalizedString("info"));
				return false;
			}else {
				kony.application.dismissLoadingScreen();
				showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
				return false;
			}
		} else if(resulttable["opstatus"] == "-1") {
			popupTractPwd.dismiss();
			//showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
			frmMBanking.show();
		} else {
			kony.application.dismissLoadingScreen();
			showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
			return false;
		}
		//Activity logging
	/*	if (successFlag) {
			if (gblActionCode == "21") {
				activityLogServiceCall("005", "", "01", "", "Success", "Success", "Success", "", "", "");
			} else if (gblActionCode == "23") {
				activityLogServiceCall("003", "", "01", "", "Success", "Success", "Success", "", "", "");
			} else if (gblActionCode == "22") {
				activityLogServiceCall("016", "", "01", "", "Success", "Success", "Success", "", "", "");
			}
		} else {
			if (gblActionCode == "21") {
				activityLogServiceCall("005", "", "02", "", "Fail", "Fail", "Fail", "", "", "");
			} else if (gblActionCode == "23") {
				activityLogServiceCall("003", "", "02", "", "Fail", "Fail", "Fail", "", "", "");
			} else if (gblActionCode == "22") {
				activityLogServiceCall("016", "", "02", "", "Fail", "Fail", "Fail", "", "", "");
			}
		}
		*/
		kony.application.dismissLoadingScreen();
	}
}

function calBackLockAlert(response) {
	if (response == true) {
		var getEncrKeyFromDevice = kony.store.getItem("encrytedText");
		if (getEncrKeyFromDevice != null) {
			frmMBPreLoginAccessesPin.show(); //frmAfterLogoutMB
		} else {
			if(gblMBActivationVia == "2")
				ActivationMBViaIBLogoutService();
			else
				frmMBanking.show();
		}
		
		/*if (gblActionCode == "22") {
			frmAfterLogoutMB.show();
		} else {
			frmMBanking.show();
		}*/
	}
}

/*
************************************************************************
            Name    : toCATnC
            Author  : Developer
            Date    : April, 2013
            Purpose : On click of Right side button of header of Terms & Condition Screen
        	Input params: na
        	Output params: open new option to user as PDF, PHOTO, EMAIL
        	ServiceCalls: nil
        	Called From : on click of right side button of header 
************************************************************************
 */

//function toCATnC() {
//	var btnskin = "btnShare";
//	var btnFocusSkin = "btnShareFoc";
//	if (frmMBTnC.hboxSaveCamEmail.isVisible) {
//		frmMBTnC.hboxSaveCamEmail.isVisible = false;
//		hbxHdrCommon.btnRight.skin = btnskin;
//		hbxArrow.imgHeaderMiddle.src = "arrowtop.png";
//		hbxArrow.imgHeaderRight.src = "empty.png";
//	} else {
//		frmMBTnC.hboxSaveCamEmail.isVisible = true;
//		hbxHdrCommon.btnRight.skin = btnFocusSkin;
//		hbxArrow.imgHeaderMiddle.src = "empty.png";
//		hbxArrow.imgHeaderRight.src = "arrowtop.png";
//	}
//}
// Mobile Banking Customer Authentication module; *PS == Preshow
/*
************************************************************************
            Name    : toCATnC
            Author  : Developer
            Date    : April, 2013
            Purpose : to load the common header
        	Input params: na
        	Output params: Loads header for a particular screen
        	ServiceCalls: nil
        	Called From : preshow of Terms & Condition Screen 
************************************************************************
 */

function toMBActiPS() {
	var lblText = kony.i18n.getLocalizedString("keyActivateMobileApp");
	getHeader(0, lblText, 0, 0, 0);
	hbxHdrCommon.lblHdrTxt.skin = "lblWhiteNormal";
	if(!flowSpa)frmMBActivation.label475124774164.containerWeight = 100;
}

function toMBActiConfirmPS() {
	var lblText = kony.i18n.getLocalizedString("keyActivateMobileApp");
	getHeader(0, lblText, 0, 0, 0);
	hbxHdrCommon.lblHdrTxt.skin = "lblWhiteNormal";
	if(!flowSpa)frmMBActiConfirm.label475124774164.containerWeight = 100;
}

/*
************************************************************************
            Name    : toMBsetPasswdPS
            Author  : Developer
            Date    : April, 2013
            Purpose : to load the common header and apply services
        	Input params: na
        	Output params: Loads header for Set PIN & Password screen
        	ServiceCalls: nil
        	Called From : preshow of Set PIN & Password screen
************************************************************************
 */

function toMBsetPasswdPS() {
	var lblText;
	if (gblSetPwd == false) {
		if (gblAddOrAuth == 0) {
			frmMBsetPasswd.lblHdrTxt.text = kony.i18n.getLocalizedString("keySetPINandPassword");
			frmMBsetPasswd.lblDeviceName.setVisibility(false);
			frmMBsetPasswd.txtDeviceName.setVisibility(false);
			//made visibility of lineAfetrTrans to true for DEF14895
			frmMBsetPasswd.lineAfetrTrans.setVisibility(true);
			frmMBsetPasswd.lineAfterDevice.setVisibility(false);
			frmMBsetPasswd.button474999996107.setVisibility(true);
			frmMBsetPasswd.button474999996108.setVisibility(true);
			frmMBsetPasswd.vbox44473234061.containerWeight = 90;
			frmMBsetPasswd.vbox47499999699.containerWeight = 90;
		} else {
			frmMBsetPasswd.lblHdrTxt.text = kony.i18n.getLocalizedString("keyConfirmPwd");
			frmMBsetPasswd.lblDeviceName.setVisibility(true);
			frmMBsetPasswd.txtDeviceName.setVisibility(true);
			frmMBsetPasswd.lineAfetrTrans.setVisibility(true);
			frmMBsetPasswd.lineAfterDevice.setVisibility(true);
			frmMBsetPasswd.button474999996107.setVisibility(false);
			frmMBsetPasswd.button474999996108.setVisibility(false);
			frmMBsetPasswd.vbox44473234061.containerWeight = 103;
			frmMBsetPasswd.vbox47499999699.containerWeight = 103;
		}
	} else {
		frmMBsetPasswd.lblHdrTxt.text = kony.i18n.getLocalizedString("resetPass");
		frmMBsetPasswd.lblDeviceName.setVisibility(false);
		frmMBsetPasswd.txtDeviceName.setVisibility(false);
		//made visibility of lineAfetrTrans to true for DEF14895
		frmMBsetPasswd.lineAfetrTrans.setVisibility(true);
		frmMBsetPasswd.lineAfterDevice.setVisibility(false);
		frmMBsetPasswd.button474999996107.setVisibility(true);
		frmMBsetPasswd.button474999996108.setVisibility(true);
		frmMBsetPasswd.vbox44473234061.containerWeight = 90;
		frmMBsetPasswd.vbox47499999699.containerWeight = 90;
	}
	//  getHeader(0, lblText, 0, 0, 0);
	hbxHdrCommon.lblHdrTxt.skin = "lblWhiteNormal";
}





//function toMBAccLockedPS() {
//	var lblText = kony.i18n.getLocalizedString("keyAccountLocked");
//	getHeader(0, lblText, 0, 0, 0)
//}

/*-----------------------------------------------------
        service calls for reset password
------------------------------------------------------*/

function resetPassword() {

	//TMBUtil.DestroyForm(frmAccountSummaryLanding);
	TMBUtil.DestroyForm(frmAccountSummaryLanding);
	pinChangePasswordService(); //we need to pass values "MB_TxPwd" + "MB_AcPwd"

}

function pinChangePasswordService() {
	showLoadingScreen();
	var password_inputparam = {};
	//password_inputparam["userId"] = "001100000000000000000001893847";
	password_inputparam["loginId"] = "MB_AcPwd";
	password_inputparam["newPassword"] = glbAccessPin; //map to transation password of form
	password_inputparam["segIdVal"] = "MIB"
	//password_inputparam["userStoreId"] = "DefaultStore"
	//password_inputparam["key"] = ""
	//password_inputparam["value"] = ""
	
	var status = invokeServiceSecureAsync("resetPassword", password_inputparam, pinChangePasswordCallBack);
}

function pinChangePasswordCallBack(status, resulttable) {
	
	if (status == 400) {
		
		if ((resulttable["errCode"] == "AccessPwdErr")|| (resulttable["errCode"] == "TxPwdError")) {
			kony.application.dismissLoadingScreen();
			showAlert(kony.i18n.getLocalizedString("invalidAccessPIN"), kony.i18n.getLocalizedString("info"));
			return false;
		}
		if (resulttable["opstatus"] == 0) {
			//alert("---Success---");
			if (resulttable["resetPasswordResponse"] != null) {
				
				
				
				passwordChangePasswordService();
			} else {
				dismissLoadingScreen();
				
				alert(" " + resulttable["faultstring"]);
			}
		} else {
			dismissLoadingScreen();
			
			alert(" " + resulttable["errMsg"]);
		}
	}
}

function passwordChangePasswordService() {
	var password_inputparam = {};
	//password_inputparam["userId"] = "001100000000000000000001893847";
	password_inputparam["loginId"] = "MB_TxPwd";
	password_inputparam["newPassword"] = glbTrasactionPwd; //map to transation password of form
	password_inputparam["segIdVal"] = "MIB"
	//password_inputparam["segmentIdVal"] = "MIB"
	//password_inputparam["userStoreId"] = "DefaultStore"
	//password_inputparam["key"] = ""
	//password_inputparam["value"] = ""
	
	var status = invokeServiceSecureAsync("resetPassword", password_inputparam, passwordChangePasswordCallBack);
}

function passwordChangePasswordCallBack(status, resulttable) {
	
	if (status == 400) {
		
		if (resulttable["errCode"] == "TxPwdError") {
			kony.application.dismissLoadingScreen();
			showAlert(kony.i18n.getLocalizedString("invalidTxnPwd"), kony.i18n.getLocalizedString("info"));
			return false;
		}
		if (resulttable["opstatus"] == 0) {
			//alert("---Success---");
			if (resulttable["resetPasswordResponse"] != null) {
				
				
				
				/*
				 * Change this LATER, IF CRMProfile WORKS
				 *
				 */
				//frmMBActiComplete.show();
				//dismissLoadingScreen();
				
				crmProfileUpdateKonyService();
			} else {
				dismissLoadingScreen();
				
				alert(" " + resulttable["faultstring"]);
			}
		} else {
			dismissLoadingScreen();
			
			alert(" " + resulttable["errMsg"]);
		}
	}
}

function crmProfileUpdateKonyService() {

	inputParam = {};
	inputParam["actionType"] = "67";
	invokeServiceSecureAsync("crmProfileModActivation", inputParam, crmProfUpdateStep2CallBack);
}

//function crmProfUpdateStep1CallBack(status, resulttable) {
//	
//	if (status == 400) {
//		
//		if (resulttable["opstatus"] == 0) {
//			// do whatever you want on success
//			var input = {};
//			//input["rqUUId"] = //preprocessor
//			//input["channelName"] = //preprocessor
//			input["ibUserId"] = resulttable["ibUserId"]
//			input["ebTxnLimitAmt"] = resulttable["ebTxnLimitAmt"]
//			input["ebMaxLimitAmtCurrent"] = resulttable["ebMaxLimitAmtCurrent"]
//			input["ebMaxLimitAmtHist"] = resulttable["ebMaxLimitAmtHist"]
//			input["ebMaxLimitAmtRequest"] = resulttable["ebMaxLimitAmtRequest"]
//			input["ebAccuUsgAmtDaily"] = resulttable["ebAccuUsgAmtDaily"]
//			input["firstActivationDate"] = resulttable["firstActivationDate"]
//			input["firstIbLoginDate"] = resulttable["firstIbLoginDate"]
//			input["firstMbLoginDate"] = resulttable["firstMbLoginDate"]
//			input["lastIbLoginSucessDate"] = resulttable["lastIbLoginSucessDate"]
//			input["lastIbLoginFailDate"] = resulttable["lastIbLoginFailDate"]
//			input["lastIbLogoutDate"] = resulttable["lastIbLogoutDate"]
//			input["lastMbLoginSucessDate"] = resulttable["lastMbLoginSucessDate"]
//			input["lastMbLoginFailDate"] = resulttable["lastMbLoginFailDate"]
//			input["lastMbLogoutDate"] = resulttable["lastMbLogoutDate"]
//			input["lastIbPasswordChangeDate"] = resulttable["lastIbPasswordChangeDate"]
//			input["lastMbAccessPasswordChangeDate"] = resulttable["lastMbAccessPasswordChangeDate"]
//			input["lastMbTxnPwdChangeDate"] = resulttable["lastMbTxnPwdChangeDate"]
//			input["ibApplyChannel"] = resulttable["ibApplyChannel"]
//			input["languageCd"] = resulttable["languageCd"]
//			input["emailAddr"] = resulttable["emailAddr"]
//			input["tokenDeviceFlag"] = resulttable["tokenDeviceFlag"]
//			input["facebookID"] = resulttable["facebookId"]
//			input["ibUserStatusId"] = resulttable["ibUserStatusId"]
//			input["mbUserStatusId"] = resulttable["mbUserStatusId"]
//			input["createdDt"] = resulttable["createdDt"]
//			input["p2pLinkAcctID"] = resulttable["p2pLinkedAcct"]
//			input["ebCustomerStatusID"] = resulttable["ebCustomerStatusId"]
//			input["actionType"] = "67"
//			
//			
//			invokeServiceSecureAsync("crmProfileMod", input, crmProfUpdateStep2CallBack);
//		} else {
//			dismissLoadingScreen();
//			
//			//alert(" " + resulttable["errMsg"]);
//		}
//	}
//}

function crmProfUpdateStep2CallBack(status, resulttable) {
	
	if (status == 400) {
		
		if (resulttable["opstatus"] == 0) {
			
			gblUserLockStatusIB=resulttable["IBUserStatusID"];
			
			
			
			gblSetPwd = false;
			dismissLoadingScreen();
			
			 if (gblActionCode == "22") {
				 var inputparam = {};
					inputparam["channelName"] = "Mobile Banking";
					inputparam["channelID"] = "02";
					inputparam["notificationType"] = "Email"; // always email
					inputparam["phoneNumber"] = gblPHONENUMBER;
					inputparam["mail"] = gblEmailId;
					inputparam["customerName"] = gblCustomerName;
					inputparam["localeCd"] = kony.i18n.getCurrentLocale();
					if(tnckeyword == undefined || tnckeyword == "") {
						inputparam["moduleKey"] = "TermsAndConditionsReactivation";
					} else {
						inputparam["moduleKey"] = tnckeyword;
					}
					invokeServiceSecureAsync("TCEMailService", inputparam, callBackNotificationAddService);
			
			 	reactivationComplete()
				
                 var inputparam = {}
            	 
             }
             else if(gblActionCode == "22") {
            
             	reactivationComplete();
             }
		}
		 else {
			reactivationComplete();
			
			dismissLoadingScreen();
			
			alert(" " + resulttable["errMsg"]);
		}
	}
}


function reactivationComplete(){

	frmMBActiComplete.image250285458166.src = "iconcomplete.png";
	frmMBActiComplete.label50285458167.text = kony.i18n.getLocalizedString("appReadytoUse");
	frmMBActiComplete.btnStart.setVisibility(true);
	
    frmMBActiComplete.show();
   
}



/*
*************************************************************************************
		Module	: passwordResetPasswordService
		Author  : Kony
		Purpose : Checking success of  resetPassword service and navigating to connect account screen or activation confirm screen based on flag
****************************************************************************************
*/

/*function resetPasswordCallBack(status, resulttable) {
	if (status == 400) {
		
		if (resulttable["opstatus"] == 0) {
			callBackCustAccForEcas() // Coming from activation flow
		} else {
			alert(" " + resulttable["errMsg"]);
			kony.application.dismissLoadingScreen();
			return false;
		}
	}
}*/

function sessionCheckAddResetPwdMB(uniqueId){
	showLoadingScreen();
	var inputparam = {};
	inputparam["deviceId"] = uniqueId;
	invokeServiceSecureAsync("duplicateSessionCheckMB", inputparam, callBackSessionCheckMB);
}

function callBackSessionCheckMB(status,resultTable){
	if (status == 400) {
		dismissLoadingScreen();
		if (resultTable["opstatus"] == 0) {
			if (resultTable["duplicateSession"] == "true") {
				showAlertDuplicateSession(kony.i18n.getLocalizedString("keyDuplicateSession"));
			}
			else{
				showAccuntSummaryScreen();
				unreadInboxMessagesMBLogin();
			}
		}
		else{
			alert(" " + resultTable["errmsg"]);
			kony.application.dismissLoadingScreen();
		}
	}
}
function showAlertDuplicateSession(message)
{
	 var alert_seq0_act1 = kony.ui.Alert({
	        "message":message,
	        "alertType": constants.ALERT_TYPE_INFO,
	        "alertTitle": "",
	        "yesLabel": "Ok",
	        "noLabel": "",
	        "alertIcon": "",
	        "alertHandler": callBackDuplicateSession
	    }, {});
}	
/**
 * description
 * @returns {}
 */
function callBackDuplicateSession() {
	isSignedUser = false;
	frmMBPreLoginAccessesPin.show();//frmAfterLogoutMB
}
