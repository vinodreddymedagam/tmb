function mySwipe(myWidget, gestureInfo) {
	// fixed for DEF572(UAT)
	if( gestureInfo["swipeDirection"] ==1 || gestureInfo["swipeDirection"] == 2){
		//alert(isMenuShown);
		if (isMenuShown == false) {
			
			
			if (gblIndex == -1) {
				
				var a = "";
				var hboxID = myWidget["id"];
				
				var indexString = hboxID.split("hboxSwipe");
				
				
				var index = indexString[1];
				
				glb_accountId = index
				
				var allwidgets = frmAccountSummaryLanding.vbox4751247744173.widgets();
				
				var hboxSwipe = "hboxSwipe" + index;
				for (var j = 0; j < allwidgets.length; j++) {
					
					var hboxSwpeName = "hbxBtnContainer" + index;
					if (allwidgets[j]["id"] == hboxSwpeName) {
						allwidgets[j].isVisible = true;
						//break;
					}
					if (allwidgets[j]["id"] == hboxSwipe) {
						allwidgets[j].isVisible = false;
						//break;
					}
					//code end
				}
				gblSwipeFlag = 1;
				gblIndex = index;
			} else {
				glb_accountId = ""
				
				var a = "";
				var hboxID = myWidget["id"];
				var indexString = hboxID.split("hboxSwipe");
				
				var index = indexString[1];
				glb_accountId = index
				
				var allwidgets = frmAccountSummaryLanding.vbox4751247744173.widgets();
				var hboxSwpeName = "hbxBtnContainer" + index;
				var hboxPrevSwp = "hbxBtnContainer" + gblIndex;
				var hboxSwipe = "hboxSwipe" + index;
				var hboxPrevSwipe = "hboxSwipe" + gblIndex;
				//alert(hboxSwpeName+"hboxswipe ");
				//alert(hboxPrevSwp+"previous swipe");
				if (index == gblIndex) {
					//alert("inside hell")
					if (gblSwipeFlag == 0) {
						for (var j = 0; j < allwidgets.length; j++) {
							
							if (allwidgets[j]["id"] == hboxSwpeName) {
								allwidgets[j].isVisible = true;
								//hboxSwipe.isVisible = false;
								//break;
							}
							if (allwidgets[j]["id"] == hboxSwipe) {
								allwidgets[j].isVisible = false;
								//hboxSwipe.isVisible = false;
								//break;
							}
							//code end
						}
						gblSwipeFlag = 1;
					} else if (gblSwipeFlag == 1) {
						for (var j = 0; j < allwidgets.length; j++) {
							if (allwidgets[j]["id"] == hboxSwpeName) {
								
								allwidgets[j].isVisible = false;
								//break;
							}
							if (allwidgets[j]["id"] == hboxSwipe) {
								allwidgets[j].isVisible = true;
								//break;
							}
							//code end//
						}
						gblSwipeFlag = 0;
					}
					if (gblSwipeFlag == 2) {
						for (var j = 0; j < allwidgets.length; j++) {
							if (allwidgets[j]["id"] == hboxPrevSwp) {
								allwidgets[j].isVisible = false;
								//break;
							}
							if (allwidgets[j]["id"] == hboxSwipe) {
								allwidgets[j].isVisible = true;
								//break;
							}
							//code end//
						}
						gblSwipeFlag = 0;
					}
				} else {
					for (var j = 0; j < allwidgets.length; j++) {
						if (allwidgets[j]["id"] == hboxSwpeName) {
							
							allwidgets[j].isVisible = true;
							//break;
						}
						if (allwidgets[j]["id"] == hboxSwipe) {
							
							allwidgets[j].isVisible = false;
							//break;
						}
						//code end
					}
					gblSwipeFlag = 1;
					for (var j = 0; j < allwidgets.length; j++) {
						if (allwidgets[j]["id"] == hboxPrevSwp) {
							
							
							allwidgets[j].isVisible = false;
							//break;
						}
						if (allwidgets[j]["id"] == hboxPrevSwipe) {
							allwidgets[j].isVisible = true;
						}
					}
				}
				gblIndex = index;
			}
		} else {
			
			isMenuShown = false;
			var currentFormID = kony.application.getCurrentForm();
			removeGestureForAccntSummary(currentFormID);
			currentFormID.scrollboxMain.scrollToEnd()
		}
	}
}
var setSwipe = {
	fingers: 1,
	swipedistance: 50,
	swipevelocity: 75
};

function createNoOfRowsDynamically(resulttable) {
	var locale = kony.i18n.getCurrentLocale();
	if (null != resulttable["forUse"] || null != resulttable["forSave"] || null != resulttable["forInvest"]) {
		
		var forUse = "0";
		var forSave = "0";
		var forInvest = "0";
		var ForTD = "0";
		if(isNotBlank(resulttable["forUse"]))
			forUse = parseInt(resulttable["forUse"]);
		
		if(isNotBlank(resulttable["forSave"])) 
		 	forSave = parseInt(resulttable["forSave"]);
		
		if(flowSpa) {
			if(isNotBlank(resulttable["forTD"]))
		 		ForTD = parseInt(resulttable["forTD"]);
		
		} else {
			if(isNotBlank(resulttable["forInvest"]))
		 		forInvest = parseInt(resulttable["forInvest"]);
		}
		
		
		var count_dynamic_label = frmAccountSummaryLanding.hbxBarGraph.widgets().length;
		for (i = 0; i < count_dynamic_label; i++)
			frmAccountSummaryLanding.hbxBarGraph.removeAt(0);
			
		if (forUse != 0) {
			createBarGraph(1, "lblBlueNew", forUse);
		}
		if (forSave != 0) {
			createBarGraph(2, "lblWhite", forSave);
		}
		if(!flowSpa) {
			if (forInvest != 0) {
				createBarGraph(3, "lblBarGreen", forInvest);
			}
		} else {
			if (ForTD != 0) {
				createBarGraph(3, "lblRed", ForTD);
			}
		}
		// MIB-1066
		if (forUse == 0 && forSave == 0 && ForTD == 0) {
			createBarGraph(1, "lblBarGrey", 100);
		}
		// MIB-1066
			
		if(isNotBlank(resulttable["forUse"]))
			frmAccountSummaryLanding.lblForUse.text = resulttable["forUse"] + kony.i18n.getLocalizedString("percent");
		else
			frmAccountSummaryLanding.lblForUse.text = "0.00" + kony.i18n.getLocalizedString("percent");
			
		if(isNotBlank(resulttable["forSave"])) 
			frmAccountSummaryLanding.lblForsave.text = resulttable["forSave"] + kony.i18n.getLocalizedString("percent");
		else
			frmAccountSummaryLanding.lblForsave.text = "0.00" + kony.i18n.getLocalizedString("percent");
			
			
		if(!flowSpa){
			frmAccountSummaryLanding.label4751247744357.text = kony.i18n.getLocalizedString("KeyForInvest");
			if(isNotBlank(resulttable["forInvest"]))
				frmAccountSummaryLanding.lblFunds.text = resulttable["forInvest"] + kony.i18n.getLocalizedString("percent");
			else
				frmAccountSummaryLanding.lblFunds.text = "0.00" + kony.i18n.getLocalizedString("percent");
		}else{
		frmAccountSummaryLanding.label4751247744357.text = kony.i18n.getLocalizedString("trmDeposit");
			if(isNotBlank(resulttable["forTD"])) {
				frmAccountSummaryLanding.lblFunds.text = resulttable["forTD"] + kony.i18n.getLocalizedString("percent");
			} else {
				frmAccountSummaryLanding.lblFunds.text = "0.00" + kony.i18n.getLocalizedString("percent");
			}
		}
		if(isNotBlank(resulttable["totalBal"]))
			frmAccountSummaryLanding.lblBalanceValue.text = resulttable["totalBal"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht")
		else
			frmAccountSummaryLanding.lblBalanceValue.text = "0.00 " + kony.i18n.getLocalizedString("currencyThaiBaht")
	} else {
		createBarGraph(1, "lblBarGrey", 100);
		frmAccountSummaryLanding.lblForUse.text = "0.00" + kony.i18n.getLocalizedString("percent");
		frmAccountSummaryLanding.lblForsave.text = "0.00" + kony.i18n.getLocalizedString("percent");
		if(!flowSpa){
			frmAccountSummaryLanding.label4751247744357.text = kony.i18n.getLocalizedString("KeyForInvest");
			frmAccountSummaryLanding.lblFunds.text = "0.00" + kony.i18n.getLocalizedString("percent");
		}else{
			frmAccountSummaryLanding.lblFunds.text = "0.00" + kony.i18n.getLocalizedString("percent");
			frmAccountSummaryLanding.label4751247744357.text = kony.i18n.getLocalizedString("trmDeposit");
		}  
		frmAccountSummaryLanding.lblBalanceValue.text = "0.00" + kony.i18n.getLocalizedString("currencyThaiBaht")
	}
	var crmIdIm= resulttable["crmId"];
	
  if(flowSpa)
  {
  	 	var randomnum = Math.floor((Math.random() * 10000) + 1);
  	 	//adding code for the SPA Image Load Optimisation for SPA. Commenting below lines for future requirements
  	 	//frmAccountSummaryLanding.imgProfile.referenceHeight=55;
  	 	//frmAccountSummaryLanding.imgProfile.referenceWidth=55;
  	 	//frmAccountSummaryLanding.imgProfile.skin=imgIB55x55;
  	 	frmAccountSummaryLanding.imgProfile.imageScaleMode=constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS;
  	 	
		gblMyProfilepic = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=Y&personalizedId=&billerId=&modIdentifier=MyProfile&dummy=" + randomnum;
    	if(gblMyProfilepic == null)
		{
		  frmAccountSummaryLanding.imgProfile.src="avatar1.png";
		}
		else
		{
		  frmAccountSummaryLanding.imgProfile.src = gblMyProfilepic;
		}
  }
  else
  {
  	  var randomnum = Math.floor((Math.random() * 10000) + 1);
  	  gblMyProfilepic = "";
	  gblMyProfilepic = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=Y&personalizedId=&billerId=&dummy=" +randomnum;
	  if(gblMyProfilepic == null)
	  {
	   frmAccountSummaryLanding.imgProfile.src="avatar1.png";
	  }
	 else
	  {
	   frmAccountSummaryLanding.imgProfile.src = gblMyProfilepic;
	  }
  }
	var productName;
	var imageName;
	var btnText;
	var containerWt;
	var visibility1st=false;
	var visibility2nd=false;
	var visibility3rd=false;
	var visibility4th=false;
	var visibility5th=false;
	var hbxPercentage;
	var hbxlayoutAlignment;
	var hbxwidgetAlignment;
	var hbxvisibilityRemainingFee;
	var lblRemainingFeeVal;
	var lblBaltext;
	var onClickBtn;
	var margin;
	var spaPadding;
	if (flowSpa) {
		spaPadding = [2, 1, 2, 1];
	} else {
		spaPadding = [0, 0, 0, 0];
	}
	//var gblDeviceInfo = kony.os.deviceInfo();
	//var gblPlatformName = kony.os.deviceInfo().name;
	if ((null != gblPlatformName) && (kony.string.equalsIgnoreCase("android", gblPlatformName))) {
		margin = [0, 2, 8, 2]
	} else if ((null != gblPlatformName) && (kony.string.equalsIgnoreCase("thinclient", gblPlatformName))) {
		margin = [0, 2, 8, 2]
	} else {
		margin = [0, 2, 0, 2]
	}
	var availableBalace;
	
	try {
		if(resulttable["custAcctRec"] != undefined){
		for (i = 0; i < resulttable["custAcctRec"].length; i++) {
			try {
				if (resulttable["custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_NOFEESAVING_TABLE") {
					hbxvisibilityRemainingFee = true;
					lblRemainingFeeVal = resulttable["custAcctRec"][i]["remainingFee"];
				} else {
					hbxvisibilityRemainingFee = false;
					lblRemainingFeeVal = "";
				}
				if (resulttable["custAcctRec"][i]["accType"] == kony.i18n.getLocalizedString("CreditCard"))
					availableBalace = commaFormatted(resulttable["custAcctRec"][i]["availableCreditBal"]) +" "+kony.i18n.getLocalizedString(
						"currencyThaiBaht")
				else
					availableBalace = resulttable["custAcctRec"][i]["availableBalDisplay"] +" "+ kony.i18n.getLocalizedString(
						"currencyThaiBaht")
				
					if (resulttable["custAcctRec"][i]["PayMyLoan"] == "Y") {			 		
					 visibility5th = true;
					lblBaltext = kony.i18n.getLocalizedString("OutstandingBalance");
					//onClickBtnLoan = onClickLoanAcctSummary;
					}else{
						visibility5th = false;
					}			
				  if (resulttable["custAcctRec"][i]["Transfer"] == "Y")
					 {
					visibility1st = true;			
					lblBaltext = kony.i18n.getLocalizedString("Balance");
					//onClickBtnTran = onclickgetTransferFromAccountsFromAccSmry;
					}else{
						visibility1st = false;
					}		
					
				  if (resulttable["custAcctRec"][i]["PayMyCreditCard"] == "Y") {
					lblBaltext = kony.i18n.getLocalizedString("AvailableCreditLimit");
					//onClickBtnCred = onClickCreditAcctSummary;
					visibility4th = true;
					}else{
						visibility4th=false;
					}
				  if (resulttable["custAcctRec"][i]["Paybill"] == "Y")
					{
					visibility2nd = true;
					lblBaltext = kony.i18n.getLocalizedString("Balance");
					//onClickBtnBillPay = onclickgetBillPaymentFromAccountsFromAccSmry;
				}else{
					visibility2nd = false;
				}
				 if (resulttable["custAcctRec"][i]["Topup"] == "Y")
					{
					visibility3rd = true;
					lblBaltext = kony.i18n.getLocalizedString("Balance");
					//onClickBtnTopup = onclickgetTopUpFromAccountsFromAccSmry;
				}else{
					visibility3rd = false;
				}
				if (locale == "th_TH") {
					productName = resulttable["custAcctRec"][i]["ProductNameThai"];
				} else if (locale == "en_US") {
					productName = resulttable["custAcctRec"][i]["ProductNameEng"];
				}
				var nickName;
				if ((resulttable["custAcctRec"][i]["acctNickName"]) == null || (resulttable["custAcctRec"][i]["acctNickName"]) == '') {
					var sbStr = resulttable["custAcctRec"][i]["accId"];
					var length = sbStr.length;
					if (resulttable["custAcctRec"][i]["accType"] == kony.i18n.getLocalizedString("Loan"))
						sbStr = sbStr.substring(7, 11);
					else
						sbStr = sbStr.substring(length - 4, length);
					nickName = productName + " " + sbStr;
				}
				else{
				   nickName=resulttable["custAcctRec"][i]["acctNickName"];
				}
				imageName="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+resulttable["custAcctRec"][i]["ICON_ID"]+"&modIdentifier=PRODICON";	
				var imgAccount = new kony.ui.Image2({
					"id": "imgAccount" + i,
					"isVisible": true,
					"src": imageName,
					"imageWhenFailed": "transparent.png",
					"imageWhileDownloading": "transparent.png"
				}, {
					"widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
					"margin": [0, 0, 0, 0],
					"padding": [0, 0, 0, 0],
					"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
					"referenceWidth": null,
					"referenceHeight": null,
					"marginInPixel": false,
					"paddingInPixel": false,
					"containerWeight": 14
				}, {});
				var lblAccountType = new kony.ui.Label({
					"id": "lblAccountType" + i,
					"isVisible": true,
					"text": nickName,
					"skin": "lblPopupBlack"
				}, {
					"widgetAlignment": constants.WIDGET_ALIGN_BOTTOM_CENTER,
					"vExpand": false,
					"hExpand": true,
					"margin": [0, 0, 0, 0],
					"padding": [0, 0, 0, 0],
					"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
					"marginInPixel": false,
					"paddingInPixel": false,
					"containerWeight": 29
				}, {});
				var lblRemaining = new kony.ui.Label({
					"id": "lblRemaining" + i,
					"isVisible": true,
					"text": kony.i18n.getLocalizedString("remFreeTran") + " ",
					"skin": "lblBlackSmall"
				}, {
					"widgetAlignment": constants.WIDGET_ALIGN_CENTER,
					"vExpand": false,
					"hExpand": true,
					"margin": [0, 0, 0, 0],
					"padding": [0, 0, 0, 0],
					"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
					"marginInPixel": false,
					"paddingInPixel": false,
					"containerWeight": 0
				}, {});
				var lblRemainingNo = new kony.ui.Label({
					"id": "lblRemainingNo" + i,
					"isVisible": true,
					"text": lblRemainingFeeVal,
					"skin": "lblBlueFont"
				}, {
					"widgetAlignment": constants.WIDGET_ALIGN_CENTER,
					"vExpand": false,
					"hExpand": true,
					"margin": [0, 0, 0, 0],
					"padding": [0, 0, 0, 0],
					"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
					"marginInPixel": false,
					"paddingInPixel": false,
					"containerWeight": 0
				}, {});
				var hboxRemaining = new kony.ui.Box({
					"id": "hboxRemaining" + i,
					"isVisible": hbxvisibilityRemainingFee,
					"position": constants.BOX_POSITION_AS_NORMAL,
					"orientation": constants.BOX_LAYOUT_HORIZONTAL
				}, {
					"containerWeight": 50,
					"percent": false,
					"layoutAlignment": constants.BOX_LAYOUT_ALIGN_FROM_LEFT,
					"widgetAlignment": constants.WIDGET_ALIGN_MIDDLE_LEFT,
					"margin": [0, 0, 0, 0],
					"padding": [0, 0, 0, 0],
					"vExpand": false,
					"hExpand": true,
					"marginInPixel": false,
					"paddingInPixel": false,
					"layoutType": constants.CONTAINER_LAYOUT_BOX
				}, {});
				hboxRemaining.add(lblRemaining, lblRemainingNo);
				var lblBalance = new kony.ui.Label({
					"id": "lblBalance" + i,
					"isVisible": true,
					"text": lblBaltext,
					"skin": "lblBlackSmallBold"
				}, {
					"widgetAlignment": constants.WIDGET_ALIGN_CENTER,
					"vExpand": false,
					"hExpand": true,
					"margin": [0, 0, 0, 2],
					"padding": [0, 0, 0, 0],
					"contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
					"marginInPixel": false,
					"paddingInPixel": false,
					"containerWeight": 30
				}, {});
				var lblBalValue = new kony.ui.Label({
					"id": "lblBalValue" + i,
					"isVisible": true,
					"text": availableBalace,
					"skin": "lblBlackLarge"
				}, {
					"widgetAlignment": constants.WIDGET_ALIGN_CENTER,
					"vExpand": false,
					"hExpand": true,
					"margin": [0, 0, 0, 0],
					"padding": [0, 0, 0, 0],
					"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
					"marginInPixel": false,
					"paddingInPixel": false,
					"containerWeight": 70
				}, {});
				var hboxBalance = new kony.ui.Box({
					"id": "hboxBalance" + i,
					"isVisible": true,
					"position": constants.BOX_POSITION_AS_NORMAL,
					"orientation": constants.BOX_LAYOUT_HORIZONTAL
				}, {
					"containerWeight": 30,
					"percent": true,
					"widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
					"margin": [0, 0, 0, 0],
					"padding": [0, 0, 0, 0],
					"vExpand": false,
					"hExpand": true,
					"marginInPixel": false,
					"paddingInPixel": false,
					"layoutType": constants.CONTAINER_LAYOUT_BOX
				}, {});
				hboxBalance.add(lblBalance, lblBalValue);
				var vboxAccontypeDetail = new kony.ui.Box({
					"id": "vboxAccontypeDetail" + i,
					"isVisible": true,
					"orientation": constants.BOX_LAYOUT_VERTICAL
				}, {
					"containerWeight": 81,
					"margin": [2, 0, 0, 0],
					"padding": [0, 0, 0, 0],
					"vExpand": false,
					"hExpand": true,
					"widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
					"marginInPixel": false,
					"paddingInPixel": false,
					"layoutType": constants.CONTAINER_LAYOUT_BOX
				}, {});
				vboxAccontypeDetail.add(lblAccountType, hboxRemaining, hboxBalance);
				var imgChevron = new kony.ui.Image2({
					"id": "imgChevron" + i,
					"isVisible": true,
					"src": "navarrow.png",
					"imageWhenFailed": null,
					"imageWhileDownloading": null
				}, {
					"widgetAlignment": constants.WIDGET_ALIGN_MIDDLE_CENTER,
					"margin": [2, 0, 2, 0],
					"padding": [0, 0, 0, 0],
					"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
					"referenceWidth": null,
					"referenceHeight": null,
					"marginInPixel": false,
					"paddingInPixel": false,
					"containerWeight": 5
				}, {});
				var hbxAccountDetail = new kony.ui.Box({
					"id": "hboxSwipe" + i,
					"isVisible": true,
					"position": constants.BOX_POSITION_AS_NORMAL,
					"orientation": constants.BOX_LAYOUT_HORIZONTAL,
					"skin": "hboxWhite",
					"onClick": showAccountDetails
				}, {
					"containerWeight": 11,
					"percent": true,
					"widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
					"margin": [0, 2, 0, 2],
					"padding": [0, 0, 1, 0],
					"vExpand": false,
					"hExpand": true,
					"marginInPixel": false,
					"paddingInPixel": false,
					"layoutType": constants.CONTAINER_LAYOUT_BOX
				}, {});
				hbxAccountDetail.add(imgAccount, vboxAccontypeDetail, imgChevron);
				var Transfer = new kony.ui.Button({
					"id": "Transfer" + i,
					"isVisible": visibility1st,
					"text": kony.i18n.getLocalizedString("Transfer"),
					"skin": "btnBlueSkin",
					"focusSkin": "btnBlueSkin",
					"onClick": onclickgetTransferFromAccountsFromAccSmry
				}, {
					"widgetAlignment": constants.WIDGET_ALIGN_CENTER,
					"vExpand": false,
					"hExpand": false, // need to change it to true 
					"margin": [8, 2, 4, 2],
					"padding": [0, 0, 0, 0],
					"contentAlignment": constants.CONTENT_ALIGN_CENTER,
					"displayText": true,
					"marginInPixel": false,
					"paddingInPixel": false,
					"containerWeight": 30
				}, {});
				var PayBill = new kony.ui.Button({
					"id": "PayBill" + i,
					"isVisible": visibility2nd,
					"text": kony.i18n.getLocalizedString("PayBill"),
					"skin": "btnBlueSkin",
					"focusSkin": "btnBlueSkin",
					"onClick": onclickgetBillPaymentFromAccountsFromAccSmry
				}, {
					"widgetAlignment": constants.WIDGET_ALIGN_CENTER,
					"vExpand": false,
					"hExpand": false, // need to change it to true 
					"margin": [0, 2, 4, 2],
					"padding": spaPadding,
					"contentAlignment": constants.CONTENT_ALIGN_CENTER,
					"displayText": true,
					"marginInPixel": false,
					"paddingInPixel": false,
					"containerWeight": 30
				}, {});
				var topUp = new kony.ui.Button({
					"id": "topUp" + i,
					"isVisible": visibility3rd,
					"text": kony.i18n.getLocalizedString("TopUp"),
					"skin": "btnBlueSkin",
					"focusSkin": "btnBlueSkin",
					"onClick": onclickgetTopUpFromAccountsFromAccSmry
				}, {
					"widgetAlignment": constants.WIDGET_ALIGN_CENTER,
					"vExpand": false,
					"hExpand": false, // need to change it to true 
					"margin": margin,
					"padding": [0, 0, 0, 0],
					"contentAlignment": constants.CONTENT_ALIGN_CENTER,
					"displayText": true,
					"marginInPixel": false,
					"paddingInPixel": false,
					"containerWeight": 30
				}, {});
				var payMyCredit = new kony.ui.Button({
					"id": "payMyCredit" + i,
					"isVisible": visibility4th,
					"text": kony.i18n.getLocalizedString("PayBill"),
					"skin": "btnBlueSkin",
					"focusSkin": "btnBlueSkin",
					"onClick": onClickCreditAcctSummary
				}, {
					"widgetAlignment": constants.WIDGET_ALIGN_CENTER,
					"vExpand": false,
					"hExpand": false, // need to change it to true 
					"margin": margin,
					"padding": [0, 0, 0, 0],
					"contentAlignment": constants.CONTENT_ALIGN_CENTER,
					"displayText": true,
					"marginInPixel": false,
					"paddingInPixel": false,
					"containerWeight": 30
				}, {});
				var payMyLoan = new kony.ui.Button({
					"id": "payMyLoan" + i,
					"isVisible": visibility5th,
					"text": kony.i18n.getLocalizedString("PayBill"),
					"skin": "btnBlueSkin",
					"focusSkin": "btnBlueSkin",
					"onClick": onClickLoanAcctSummary
				}, {
					"widgetAlignment": constants.WIDGET_ALIGN_CENTER,
					"vExpand": false,
					"hExpand": false, // need to change it to true 
					"margin": margin,
					"padding": [0, 0, 0, 0],
					"contentAlignment": constants.CONTENT_ALIGN_CENTER,
					"displayText": true,
					"marginInPixel": false,
					"paddingInPixel": false,
					"containerWeight": 30
				}, {});
				var hbxBtnContainer = new kony.ui.Box({
					"id": "hbxBtnContainer" + i,
					"isVisible": false,
					"position": constants.BOX_POSITION_AS_NORMAL,
					"orientation": constants.BOX_LAYOUT_HORIZONTAL
				}, {
					"containerWeight": 9,
					"percent": true,
					"layoutAlignment": constants.BOX_LAYOUT_ALIGN_FROM_CENTER,
					"widgetAlignment": constants.WIDGET_ALIGN_CENTER,
					"margin": [0, 0, 0, 0],
					"padding": [0, 6, 0, 6],
					"vExpand": false,
					"hExpand": true,
					"skin": "hboxLightGrey",
					"marginInPixel": false,
					"paddingInPixel": false,
					"layoutType": constants.CONTAINER_LAYOUT_BOX
				}, {});
				
			  if(Transfer.isVisible==true){ 
					Transfer.hExpand = true;
					Transfer.containerWeight = 60;
					Transfer.padding = [0, 0, 0, 0];
					Transfer.margin = [8, 2, 4, 2];
					hbxBtnContainer.add(Transfer);
					//hbxBtnContainer.padding =  [10, 0, 20, 0];		
				}
				if(PayBill.isVisible==true){
					hbxBtnContainer.add(PayBill);
					//hbxBtnContainer.padding =  [10, 0, 0, 0];	
				}
				if(topUp.isVisible==true){
					hbxBtnContainer.add(topUp);
					//hbxBtnContainer.padding =  [10, 0, 0, 0];	
				}
			   if(payMyCredit.isVisible==true){
					payMyCredit.hExpand = true;
					payMyCredit.containerWeight = 60;
					payMyCredit.padding = [0, 0, 0, 0];
					payMyCredit.margin = [8, 2, 4, 2];
					hbxBtnContainer.add(payMyCredit);
					//hbxBtnContainer.padding =  [10, 0, 0, 0];	
				}
			   if(payMyLoan.isVisible==true){
					payMyLoan.hExpand = true;
					payMyLoan.containerWeight = 60;
					payMyLoan.padding = [0, 0, 0, 0];
					payMyLoan.margin = [8, 2, 4, 2];
					payMyLoan.containerWeight=40;
					hbxBtnContainer.add(payMyLoan);
					//hbxBtnContainer.padding =  [10, 0, 0, 0];	
				}
				
				if(Transfer.isVisible==true && PayBill.isVisible==true && topUp.isVisible==true){ 
					Transfer.hExpand = false;
					Transfer.containerWeight = 30;
					Transfer.margin = [8, 2, 4, 2];
					Transfer.padding =  [0, 0, 0, 0];
					PayBill.margin = [0, 2, 4, 2];
					PayBill.padding = [0, 0, 0, 0];
					topUp.margin = margin;
					topUp.padding = [0, 0, 0, 0];
					//hbxBtnContainer.padding =  [7, 0, 0, 0];	
				}
				if(Transfer.isVisible==true && PayBill.isVisible==false && topUp.isVisible==false && payMyCredit.isVisible==false && payMyLoan.isVisible==false ){
				if (gblPlatformName == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblPlatformName == "iPhone Simulator" ) 
				 Transfer.margin=[25, 2, 0, 2];
				 Transfer.containerWeight = 60;
				}
				else if(Transfer.isVisible==false && PayBill.isVisible==false && topUp.isVisible==false && payMyCredit.isVisible==true && payMyLoan.isVisible==false ){
				if (gblPlatformName == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblPlatformName == "iPhone Simulator" ) 
				 payMyCredit.margin=[25, 2, 0, 2];
				 payMyCredit.containerWeight = 60;
				}
				else if(Transfer.isVisible==false && PayBill.isVisible==false && topUp.isVisible==false && payMyCredit.isVisible==false && payMyLoan.isVisible==true ){
				if (gblPlatformName == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblPlatformName == "iPhone Simulator" ) 
				payMyLoan.margin=[25, 2, 0, 2];
				payMyLoan.containerWeight = 60;
				}
				else if(Transfer.isVisible==true && PayBill.isVisible==false && topUp.isVisible==false && payMyCredit.isVisible==false && payMyLoan.isVisible==true ){
				payMyLoan.containerWeight = 50;
				Transfer.containerWeight = 50;
				
				if (gblPlatformName == "iPhone"  || gblDeviceInfo["name"] == "iPad"|| gblPlatformName == "iPhone Simulator" ) 
				payMyLoan.margin = [8, 2, 8, 2]
				else
				payMyLoan.margin=margin;
				//Transfer.margin=margin;
				//hbxBtnContainer.padding =  [7, 0, 0, 0];
				}
				
				var lineseparator = new kony.ui.Line({
					"id": "lineseparator" + i,
					"isVisible": true,
					"skin": "lnSegSeperator"
				}, {
					"thickness": 1,
					"margin": [0, 0, 0, 0],
					"marginInPixel": false,
					"paddingInPixel": false
				}, {});
				var setSwipe = {
					fingers: 1,
					swipedistance: 50,
					swipevelocity: 75
				};
				var setupTblTap = {
					fingers: 1,
					taps: 1
				};
				
				//hbxAccountDetail.setGestureRecognizer(1, setupTblTap, showAccountDetails);
				//hbxAccountDetail.setGestureRecognizer(1, setupDblTap, showAccountDetails);
				// due to SPA single tap platform issue we made it seperately.
				if ((null != gblPlatformName) && (kony.string.equalsIgnoreCase("thinclient", gblPlatformName))) {
					hbxAccountDetail.setGestureRecognizer(1, gblspasngleTap, showAccountDetails);
					hbxBtnContainer.setGestureRecognizer(2, setSwipe, mySwipe);
				} else { 
					hbxAccountDetail.setGestureRecognizer(1, setupTblTap, showAccountDetails);
				}
				hbxAccountDetail.setGestureRecognizer(2, setSwipe, mySwipe);
				hbxBtnContainer.setGestureRecognizer(2, setSwipe, mySwipe);
				frmAccountSummaryLanding.vbox4751247744173.add(hbxAccountDetail);
				// frmAccountSummaryLanding.add(hboxBalance);
				frmAccountSummaryLanding.vbox4751247744173.add(hbxBtnContainer);
				frmAccountSummaryLanding.vbox4751247744173.add(lineseparator);
			} catch(exception) {
				
			}
		}
		}
				
		//ENH_106_107 - Mutual Funds - MF Accounts Logic to add MF account in Customer Account Summary
		if(!flowSpa && resulttable["mfAccountFlag"] == "true") {

			if(!gblHaveMutualFundsFlg){ gblHaveMutualFundsFlg = true; }
			hbxvisibilityRemainingFee = false;
			lblRemainingFeeVal = "";

			availableBalace = resulttable["mfTotalAmount"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht");

			var nickName;
			nickName= kony.i18n.getLocalizedString("MF_Acc_Sumary_Title");

			imageName="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId=MF-Product&modIdentifier=MFLOGOS&rr="+randomnum;
			var imgAccount = new kony.ui.Image2({
				"id": "imgAccount" + i,
				"isVisible": true,
				"src": imageName,
				"imageWhenFailed": "transparent.png",
				"imageWhileDownloading": "transparent.png"
			}, {
				"widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
				"margin": [0, 0, 0, 0],
				"padding": [0, 0, 0, 0],
				"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
				"referenceWidth": null,
				"referenceHeight": null,
				"marginInPixel": false,
				"paddingInPixel": false,
				"containerWeight": 14
			}, {});
			var lblAccountType = new kony.ui.Label({
				"id": "lblAccountType" + i,
				"isVisible": true,
				"text": nickName,
				"skin": "lblPopupBlack"
			}, {
				"widgetAlignment": constants.WIDGET_ALIGN_BOTTOM_CENTER,
				"vExpand": false,
				"hExpand": true,
				"margin": [0, 0, 0, 0],
				"padding": [0, 0, 0, 0],
				"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
				"marginInPixel": false,
				"paddingInPixel": false,
				"containerWeight": 29
			}, {});
			var lblRemaining = new kony.ui.Label({
				"id": "lblRemaining" + i,
				"isVisible": true,
				"text": " ",
				"skin": "lblBlackSmall"
			}, {
				"widgetAlignment": constants.WIDGET_ALIGN_CENTER,
				"vExpand": false,
				"hExpand": true,
				"margin": [0, 0, 0, 0],
				"padding": [0, 0, 0, 0],
				"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
				"marginInPixel": false,
				"paddingInPixel": false,
				"containerWeight": 0
			}, {});
			var lblRemainingNo = new kony.ui.Label({
				"id": "lblRemainingNo" + i,
				"isVisible": true,
				"text": " ",
				"skin": "lblBlueFont"
			}, {
				"widgetAlignment": constants.WIDGET_ALIGN_CENTER,
				"vExpand": false,
				"hExpand": true,
				"margin": [0, 0, 0, 0],
				"padding": [0, 0, 0, 0],
				"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
				"marginInPixel": false,
				"paddingInPixel": false,
				"containerWeight": 0
			}, {});
			var hboxRemaining = new kony.ui.Box({
				"id": "hboxRemaining" + i,
				"isVisible": hbxvisibilityRemainingFee,
				"position": constants.BOX_POSITION_AS_NORMAL,
				"orientation": constants.BOX_LAYOUT_HORIZONTAL
			}, {
				"containerWeight": 50,
				"percent": false,
				"layoutAlignment": constants.BOX_LAYOUT_ALIGN_FROM_LEFT,
				"widgetAlignment": constants.WIDGET_ALIGN_MIDDLE_LEFT,
				"margin": [0, 0, 0, 0],
				"padding": [0, 0, 0, 0],
				"vExpand": false,
				"hExpand": true,
				"marginInPixel": false,
				"paddingInPixel": false,
				"layoutType": constants.CONTAINER_LAYOUT_BOX
			}, {});
			hboxRemaining.add(lblRemaining, lblRemainingNo);

			lblBaltext = kony.i18n.getLocalizedString("MF_lbl_Investment_value_h2");
			var lblBalance = new kony.ui.Label({
				"id": "lblBalance" + i,
				"isVisible": true,
				"text": lblBaltext,
				"skin": "lblBlackSmallBold"
			}, {
				"widgetAlignment": constants.WIDGET_ALIGN_CENTER,
				"vExpand": false,
				"hExpand": true,
				"margin": [0, 0, 0, 2],
				"padding": [0, 0, 0, 0],
				"contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
				"marginInPixel": false,
				"paddingInPixel": false,
				"containerWeight": 30
			}, {});
			var lblBalValue = new kony.ui.Label({
				"id": "lblBalValue" + i,
				"isVisible": true,
				"text": availableBalace,
				"skin": "lblBlackLarge"
			}, {
				"widgetAlignment": constants.WIDGET_ALIGN_CENTER,
				"vExpand": false,
				"hExpand": true,
				"margin": [0, 0, 0, 0],
				"padding": [0, 0, 0, 0],
				"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
				"marginInPixel": false,
				"paddingInPixel": false,
				"containerWeight": 70
			}, {});
			var hboxBalance = new kony.ui.Box({
				"id": "hboxBalance" + i,
				"isVisible": true,
				"position": constants.BOX_POSITION_AS_NORMAL,
				"orientation": constants.BOX_LAYOUT_HORIZONTAL
			}, {
				"containerWeight": 30,
				"percent": true,
				"widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
				"margin": [0, 0, 0, 0],
				"padding": [0, 0, 0, 0],
				"vExpand": false,
				"hExpand": true,
				"marginInPixel": false,
				"paddingInPixel": false,
				"layoutType": constants.CONTAINER_LAYOUT_BOX
			}, {});
			hboxBalance.add(lblBalance, lblBalValue);

			var vboxAccontypeDetail = new kony.ui.Box({
				"id": "vboxAccontypeDetail" + i,
				"isVisible": true,
				"orientation": constants.BOX_LAYOUT_VERTICAL
			}, {
				"containerWeight": 81,
				"margin": [2, 0, 0, 0],
				"padding": [0, 0, 0, 0],
				"vExpand": false,
				"hExpand": true,
				"widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
				"marginInPixel": false,
				"paddingInPixel": false,
				"layoutType": constants.CONTAINER_LAYOUT_BOX
			}, {});
			vboxAccontypeDetail.add(lblAccountType, hboxRemaining, hboxBalance);
			var imgChevron = new kony.ui.Image2({
				"id": "imgChevron" + i,
				"isVisible": true,
				"src": "navarrow.png",
				"imageWhenFailed": null,
				"imageWhileDownloading": null
			}, {
				"widgetAlignment": constants.WIDGET_ALIGN_MIDDLE_CENTER,
				"margin": [2, 0, 2, 0],
				"padding": [0, 0, 0, 0],
				"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
				"referenceWidth": null,
				"referenceHeight": null,
				"marginInPixel": false,
				"paddingInPixel": false,
				"containerWeight": 5
			}, {});
			var hbxAccountDetail = new kony.ui.Box({
				"id": "hboxSwipe" + i,
				"isVisible": true,
				"position": constants.BOX_POSITION_AS_NORMAL,
				"orientation": constants.BOX_LAYOUT_HORIZONTAL,
				"skin": "hboxWhite"
			}, {
				"containerWeight": 11,
				"percent": true,
				"widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
				"margin": [0, 2, 0, 2],
				"padding": [0, 0, 1, 0],
				"vExpand": false,
				"hExpand": true,
				"marginInPixel": false,
				"paddingInPixel": false,
				"layoutType": constants.CONTAINER_LAYOUT_BOX
			}, {});
			hbxAccountDetail.add(imgAccount, vboxAccontypeDetail, imgChevron);

			
			var lineseparator = new kony.ui.Line({
				"id": "lineseparator" + i,
				"isVisible": true,
				"skin": "lnSegSeperator"
			}, {
				"thickness": 1,
				"margin": [0, 0, 0, 0],
				"marginInPixel": false,
				"paddingInPixel": false
			}, {});
			
			var setupTblTap = {
					fingers: 1,
					taps: 1
			};
			hbxAccountDetail.setGestureRecognizer(1, setupTblTap, MBcallMutualFundsSummary);
			frmAccountSummaryLanding.vbox4751247744173.add(hbxAccountDetail);
			frmAccountSummaryLanding.vbox4751247744173.add(lineseparator);

		}

		i++;
		//ENH_108 - BA View Policy Details - Logic to add BA account in Customer Account Summary
		if(!flowSpa && resulttable["baAccountFlag"] == "true") {

			if(!gblHaveBankAssuranceFlg){ gblHaveBankAssuranceFlg = true; }
			hbxvisibilityRemainingFee = false;
			lblRemainingFeeVal = "";

			var totalSumInsured = resulttable["totalSumInsured"];
			if(totalSumInsured != "-") {
				totalSumInsured = commaFormatted(parseFloat(totalSumInsured).toFixed(2)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
			} 

			var nickName;
			nickName= kony.i18n.getLocalizedString("BA_Acc_Summary_Title");

			imageName="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId=BA_PROD_LOGO&modIdentifier=MFLOGOS&rr="+randomnum;
									
			var imgAccount = new kony.ui.Image2({
				"id": "imgAccount" + i,
				"isVisible": true,
				"src": imageName,
				"imageWhenFailed": "transparent.png",
				"imageWhileDownloading": "transparent.png"
			}, {
				"widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
				"margin": [0, 0, 0, 0],
				"padding": [0, 0, 0, 0],
				"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
				"referenceWidth": null,
				"referenceHeight": null,
				"marginInPixel": false,
				"paddingInPixel": false,
				"containerWeight": 14
			}, {});
			var lblAccountType = new kony.ui.Label({
				"id": "lblAccountType" + i,
				"isVisible": true,
				"text": nickName,
				"skin": "lblPopupBlack"
			}, {
				"widgetAlignment": constants.WIDGET_ALIGN_BOTTOM_CENTER,
				"vExpand": false,
				"hExpand": true,
				"margin": [0, 0, 0, 0],
				"padding": [0, 0, 0, 0],
				"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
				"marginInPixel": false,
				"paddingInPixel": false,
				"containerWeight": 29
			}, {});
			var lblRemaining = new kony.ui.Label({
				"id": "lblRemaining" + i,
				"isVisible": true,
				"text": " ",
				"skin": "lblBlackSmall"
			}, {
				"widgetAlignment": constants.WIDGET_ALIGN_CENTER,
				"vExpand": false,
				"hExpand": true,
				"margin": [0, 0, 0, 0],
				"padding": [0, 0, 0, 0],
				"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
				"marginInPixel": false,
				"paddingInPixel": false,
				"containerWeight": 0
			}, {});
			var lblRemainingNo = new kony.ui.Label({
				"id": "lblRemainingNo" + i,
				"isVisible": true,
				"text": " ",
				"skin": "lblBlueFont"
			}, {
				"widgetAlignment": constants.WIDGET_ALIGN_CENTER,
				"vExpand": false,
				"hExpand": true,
				"margin": [0, 0, 0, 0],
				"padding": [0, 0, 0, 0],
				"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
				"marginInPixel": false,
				"paddingInPixel": false,
				"containerWeight": 0
			}, {});
			var hboxRemaining = new kony.ui.Box({
				"id": "hboxRemaining" + i,
				"isVisible": hbxvisibilityRemainingFee,
				"position": constants.BOX_POSITION_AS_NORMAL,
				"orientation": constants.BOX_LAYOUT_HORIZONTAL
			}, {
				"containerWeight": 50,
				"percent": false,
				"layoutAlignment": constants.BOX_LAYOUT_ALIGN_FROM_LEFT,
				"widgetAlignment": constants.WIDGET_ALIGN_MIDDLE_LEFT,
				"margin": [0, 0, 0, 0],
				"padding": [0, 0, 0, 0],
				"vExpand": false,
				"hExpand": true,
				"marginInPixel": false,
				"paddingInPixel": false,
				"layoutType": constants.CONTAINER_LAYOUT_BOX
			}, {});
			hboxRemaining.add(lblRemaining, lblRemainingNo);

			lblBaltext = kony.i18n.getLocalizedString("BA_lbl_Total_Sum_Insured")+":";
			var lblBalance = new kony.ui.Label({
				"id": "lblBalance" + i,
				"isVisible": true,
				"text": lblBaltext,
				"skin": "lblBlackSmallBold"
			}, {
				"widgetAlignment": constants.WIDGET_ALIGN_CENTER,
				"vExpand": false,
				"hExpand": true,
				"margin": [0, 0, 0, 2],
				"padding": [0, 0, 0, 0],
				"contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
				"marginInPixel": false,
				"paddingInPixel": false,
				"containerWeight": 30
			}, {});
			var lblBalValue = new kony.ui.Label({
				"id": "lblBalValue" + i,
				"isVisible": true,
				"text": totalSumInsured,
				"skin": "lblBlackLarge"
			}, {
				"widgetAlignment": constants.WIDGET_ALIGN_CENTER,
				"vExpand": false,
				"hExpand": true,
				"margin": [0, 0, 0, 0],
				"padding": [0, 0, 0, 0],
				"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
				"marginInPixel": false,
				"paddingInPixel": false,
				"containerWeight": 70
			}, {});
			var hboxBalance = new kony.ui.Box({
				"id": "hboxBalance" + i,
				"isVisible": true,
				"position": constants.BOX_POSITION_AS_NORMAL,
				"orientation": constants.BOX_LAYOUT_HORIZONTAL
			}, {
				"containerWeight": 30,
				"percent": true,
				"widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
				"margin": [0, 0, 0, 0],
				"padding": [0, 0, 0, 0],
				"vExpand": false,
				"hExpand": true,
				"marginInPixel": false,
				"paddingInPixel": false,
				"layoutType": constants.CONTAINER_LAYOUT_BOX
			}, {});
			hboxBalance.add(lblBalance, lblBalValue);

			var vboxAccontypeDetail = new kony.ui.Box({
				"id": "vboxAccontypeDetail" + i,
				"isVisible": true,
				"orientation": constants.BOX_LAYOUT_VERTICAL
			}, {
				"containerWeight": 81,
				"margin": [2, 0, 0, 0],
				"padding": [0, 0, 0, 0],
				"vExpand": false,
				"hExpand": true,
				"widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
				"marginInPixel": false,
				"paddingInPixel": false,
				"layoutType": constants.CONTAINER_LAYOUT_BOX
			}, {});
			vboxAccontypeDetail.add(lblAccountType, hboxRemaining, hboxBalance);
			var imgChevron = new kony.ui.Image2({
				"id": "imgChevron" + i,
				"isVisible": true,
				"src": "navarrow.png",
				"imageWhenFailed": null,
				"imageWhileDownloading": null
			}, {
				"widgetAlignment": constants.WIDGET_ALIGN_MIDDLE_CENTER,
				"margin": [2, 0, 2, 0],
				"padding": [0, 0, 0, 0],
				"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
				"referenceWidth": null,
				"referenceHeight": null,
				"marginInPixel": false,
				"paddingInPixel": false,
				"containerWeight": 5
			}, {});
			var hbxAccountDetail = new kony.ui.Box({
				"id": "hboxSwipe" + i,
				"isVisible": true,
				"position": constants.BOX_POSITION_AS_NORMAL,
				"orientation": constants.BOX_LAYOUT_HORIZONTAL,
				"skin": "hboxWhite"
			}, {
				"containerWeight": 11,
				"percent": true,
				"widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
				"margin": [0, 2, 0, 2],
				"padding": [0, 0, 1, 0],
				"vExpand": false,
				"hExpand": true,
				"marginInPixel": false,
				"paddingInPixel": false,
				"layoutType": constants.CONTAINER_LAYOUT_BOX
			}, {});
			hbxAccountDetail.add(imgAccount, vboxAccontypeDetail, imgChevron);

			
			var lineseparator = new kony.ui.Line({
				"id": "lineseparator" + i,
				"isVisible": true,
				"skin": "lnSegSeperator"
			}, {
				"thickness": 1,
				"margin": [0, 0, 0, 0],
				"marginInPixel": false,
				"paddingInPixel": false
			}, {});
			
			var setupTblTap = {
					fingers: 1,
					taps: 1
			};
			hbxAccountDetail.setGestureRecognizer(1, setupTblTap, MBcallBAPolicyListService);
			frmAccountSummaryLanding.vbox4751247744173.add(hbxAccountDetail);
			frmAccountSummaryLanding.vbox4751247744173.add(lineseparator);

		}




		if(flowSpa)
		{
				var calledForm = kony.application.getCurrentForm();
				//createMenuDynamically(calledForm); commented due to DEF1195
		}
	} catch (e) {
		
	}
}

function createBarGraph(id, btnSkin, width) {
	
	var lblDepositeBal = new kony.ui.Label({
		"id": "lblDepositeBal" + id,
		"isVisible": true,
		"text": " ",
		"skin": btnSkin
	}, {
		"widgetAlignment": constants.WIDGET_ALIGN_CENTER,
		"vExpand": false,
		"hExpand": true,
		"margin": [0, 0, 0, 0],
		"padding": [0, 0, 0, 0],
		"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
		"marginInPixel": false,
		"paddingInPixel": false,
		"containerWeight": width
	}, {});
	frmAccountSummaryLanding.hbxBarGraph.add(lblDepositeBal);
}


function onSwipe(myWidget, gestureInfo) {
	if (isMenuShown == true) {
		
		var currentFormID = kony.application.getCurrentForm();
		removeGestureForAccntSummary(currentFormID);
		currentFormID.scrollboxMain.scrollToEnd();
		isMenuShown = false;
	}
}

function showAccuntSummaryScreen() {
	gblfromCalender =false;
	if(flowSpa)
	{
		//frmAccountSummaryLandingGlobals();
		TMBUtil.DestroyForm(frmAccountSummaryLanding);
		if(!flowSpa)
		{
			TMBUtil.DestroyForm(frmAccountDetailsMB);
		}
		gblAccountTable = "";
		GLOBAL_DEBITCARD_TABLE = "";
		gblSpaTokenServFalg=true;
		//frmAccountDetailsMB.segDebitCard.removeAll();
		loginFlow = true;
	}
	//callCustomerAccountService();
	showLoadingScreen();
	callPartyInquiryService()
	
}
/**
 * description
 * returns {}
 */

function showAccountSummLndnd(uniqueID) {
	showLoadingScreen();
		kony.print("FPRINT: gblSuccess="+gblSuccess);
	kony.print("FPRINT: uniqueID="+uniqueID);
	if(gblSuccess == true || gblSuccess == "true"){
	      var inputParam ={};
	  	  inputParam["deviceId"] = uniqueID;
	  	    kony.print("FPRINT: IN LoginTouchId");
      	  invokeServiceSecureAsync("LoginTouchId", inputParam, callBackMBLoginAuthTouch)
	} else {
	kony.print("FPRINT: IN loginAuth");
		loginAuth(uniqueID);
	}
}

function callBackMBLoginAuthTouch(status, resulttable){
    if (status == 400) {
        if(resulttable["opstatus"] == 0){
		  	if (resulttable["deviceStatus"] == "0") {
				showAlert(kony.i18n.getLocalizedString("ECKonyDeviceErr00002"), kony.i18n.getLocalizedString("info"));
				resetValues();
				kony.application.dismissLoadingScreen();
				return false;
			} else if (resulttable["deviceStatus"] == "2") {
				showAlert("Device is blocked", kony.i18n.getLocalizedString("info"));
				resetValues();
				kony.application.dismissLoadingScreen();
				return false;
			} else if (resulttable["deviceStatus"] == "3") {
				showAlert("Device is cancelled", kony.i18n.getLocalizedString("info"));
				resetValues();
				kony.application.dismissLoadingScreen();
				return false;
			} else if (resulttable["errCode"] != null && resulttable["errCode"] == "KONYERRTI001") {
				showAlert(kony.i18n.getLocalizedString("keyLoginTouchIDError"), kony.i18n.getLocalizedString("keyLoginTouchIDErrorTry"));
				resetValues();
				kony.application.dismissLoadingScreen();
				return false;
			}
				var initVecValue = resulttable["vecValue"];
		        var serverTime = resulttable["serverTime"];
		        var touchCode = resulttable["touchCode"];
		      
					var algo = "aes";
					var prptobj = {
						padding: "pkcs5",
						mode: "cbc",
						initializationvector: initVecValue
					};
					var encrKeyFromDevice = kony.store.getItem("encrytedText");
					var readEncryVal = encrKeyFromDevice["encKeyVal"]
					var decryptkey = kony.crypto.readKey(readEncryVal)
					var retencrKeyFromDevice = encrKeyFromDevice["enckey"]
					
					var myClearText = kony.crypto.decrypt(algo, decryptkey, retencrKeyFromDevice, prptobj);
					
				if(decryptkey == null || myClearText == null){
					glbInitVector = resulttable["vecValue"];
					gblServerTimeTouch = resulttable["serverTime"];
					gblTouchCodeTouch = resulttable["touchCode"];
					getKeyForLoginTouch()
				}else{
					loginTouchIdMobileBanking(initVecValue,touchCode,serverTime);
				}
		}  else{
		   resetValues();
		   kony.application.dismissLoadingScreen();
		   var errorMessage = resulttable["errMsg"];
			var middlewareErrorMessage = resulttable["errmsg"];
			if(errorMessage == null || errorMessage == undefined || errorMessage == "" ){
				if(middlewareErrorMessage != null && middlewareErrorMessage != undefined && middlewareErrorMessage != "" ){
				    errorMessage =  middlewareErrorMessage;
				}else{
					errorMessage = kony.i18n.getLocalizedString("ECGenOTPRtyErr00001");
				}
			}
	       alert(""+errorMessage);
	    }
      
      }
    
 } 

function invokeVerifyOTPKonyService(uniqueId)
{
	showLoadingScreen();
	var deviceGeneratedOTP = generateOTPWIthServerTime(GBL_TIMEFOR_LOGIN,uniqueId); //generateOTP();
	inputParam = {};
	inputParam["deviceId"] = uniqueId;
	inputParam["otp"] = deviceGeneratedOTP
	invokeServiceSecureAsync("verifyOTPKony", inputParam, callBackInvokeCrmProfileInq)	
}

function generateOTPWIthServerTime(time,uniqueID) {
	var secKeyVal = decrypt(glbInitVector);
	var hardwareID = uniqueID;
	var secKey = hardwareID + time + secKeyVal;
	var secKeyHash = kony.crypto.createHash("sha256", secKey);
	
	return secKeyHash
}

function callBackInvokeCrmProfileInq(status, resulttable) {
	if (status == 400) {
		
		if (resulttable["opstatus"] == 0) {
		
				showLoadingScreen();
				inputParam = {};
			inputParam["rqUUId"] = "";
			inputParam["LoginInd"] = "login";
			inputParam["TriggerEmail"]="yes";
			isFromLogin=false;
			inputParam["activationCompleteFlag"] = "true";
			inputParam["activityTypeId"] = "Login";
			invokeServiceSecureAsync("LoginProcessServiceExecute", inputParam, loginPProcessCallBack)
		} else if (resulttable["opstatus"] == 1) {
			showAlert(kony.i18n.getLocalizedString("accPwdNotValid"), kony.i18n.getLocalizedString("info"));
			kony.application.dismissLoadingScreen();
			return false;
		} else {
			alert(" " + resulttable["errMsg"]);
			kony.application.dismissLoadingScreen();
			return false;
		}
	}
}

function onClickForInnerBoxes() {
	
	
	if (isMenuShown == true) {
		isMenuShown = false;
		var currentFormID = kony.application.getCurrentForm();
		removeGestureForAccntSummary(currentFormID);
		currentFormID.scrollboxMain.scrollToEnd();
	}
}

function removeGestureForAccntSummary(calledForm) {
	try{
	
	calledForm.vboxRight.removeGestureRecognizer(swipeGesture);
	calledForm.vboxRight.removeGestureRecognizer(tapGesture);
	
	}catch(exception) {
				
	}
}

function settingGestureForAccntSummary(calledForm) {

	var setSwipe = {
		fingers: 1,
		swipedistance: 5,
		swipevelocity: 75
	};
	var setupTapGes = {
		fingers: 1,
		taps: 1
	};
	swipeGesture = calledForm.vboxRight.setGestureRecognizer(2, setSwipe, onSwipe);
	tapGesture = calledForm.vboxRight.setGestureRecognizer(1, setupTapGes, onClickForInnerBoxes);
}

function onclickgetBillPaymentFromAccountsFromAccSmry() {
 	if(checkMBUserStatus()){
		glb_accId = gblAccountTable["custAcctRec"][glb_accountId]["accId"];
		var jointActXfer  = gblAccountTable["custAcctRec"][glb_accountId].partyAcctRelDesc;
		
		if(jointActXfer == "PRIJNT" || jointActXfer == "SECJNT" || jointActXfer == "OTHJNT" || jointActXfer == "SECJAN"){
			alert(""+ kony.i18n.getLocalizedString('keyNotAllloedTransactions'));
			return;
		}
		var accountStatus = gblAccountTable["custAcctRec"][glb_accountId].acctStatusCode;
		
		if(accountStatus.indexOf("Active") == -1){
			alert(""+ kony.i18n.getLocalizedString('keyNotAllloedTransactions'));
			return;
		}
		
		var noCASAAct = noActiveActs();
	 	if(noCASAAct)
		{
			showAlertWithCallBack(kony.i18n.getLocalizedString("MB_StatusNotEligible"), kony.i18n.getLocalizedString("info"),onclickActivationCompleteStart);
			return false;
		}
		
		gblMyBillerTopUpBB = 0;
		GblBillTopFlag = true;
		gblBillPayShortCut = true;
		callMasterBillerService();
	}
}

function onclickgetTopUpFromAccountsFromAccSmry() {
 if(checkMBUserStatus()){
	glb_accId = gblAccountTable["custAcctRec"][glb_accountId]["accId"];
	var jointActXfer  = gblAccountTable["custAcctRec"][glb_accountId].partyAcctRelDesc;
	
	if(jointActXfer == "PRIJNT" || jointActXfer == "SECJNT" || jointActXfer == "OTHJNT" || jointActXfer == "SECJAN"){
		alert(""+ kony.i18n.getLocalizedString('keyNotAllloedTransactions'));
		return;
	}
	var accountStatus = gblAccountTable["custAcctRec"][glb_accountId].acctStatusCode;
	
	if(accountStatus.indexOf("Active") == -1){
		alert(""+ kony.i18n.getLocalizedString('keyNotAllloedTransactions'));
		return;
	}
		var noCASAAct = noActiveActs();
	 	if(noCASAAct)
		{
			showAlertWithCallBack(kony.i18n.getLocalizedString("MB_StatusNotEligible"), kony.i18n.getLocalizedString("info"),onclickActivationCompleteStart);
			return false;
		}
		
		gblMyBillerTopUpBB = 1;
		GblBillTopFlag = false;
		callMasterBillerService(); // For Top Up Redesign PBI
	}
}



//////Login Auth Composite service call starts here////

function loginAuth(uniqueID){
	inputParam = {};
	inputParam["deviceId"] = uniqueID;
	kony.print("FPRINT: uniqueID is deviceId= "+uniqueID);
	var pushRegID=kony.store.getItem("kpnssubcriptiontoken");
	if(pushRegID!=null && pushRegID!="" && pushRegID!=undefined && pushRegID!="undefined")
	{
		inputParam["subscriptionToken"]=pushRegID;
	}
	//inputParam["osDeviceId"] = kony.os.deviceInfo().deviceid;
	// modifying the code for kony device id.
	inputParam["osDeviceId"] = getDeviceID();
	//kony.print("FPRINT: getDeviceID= "+getDeviceID);
	channelId = "02";
	if (GLOBAL_MB_CHANNEL != null && GLOBAL_MB_CHANNEL != "") {
		channelId = GLOBAL_MB_CHANNEL;
	}
	inputParam["loginChannel"] = channelId;
	inputParam["nodeNum"] = "1";
	kony.print("FPRINT: gFromConnectAccount="+gFromConnectAccount);
	if (gFromConnectAccount == true) {
		inputParam["password"] = glbAccessPin
		kony.print("FPRINT: glbAccessPin="+glbAccessPin);
	} else {
		inputParam["password"] = gblNum;//frmAfterLogoutMB.tbxAccessPIN.text;
kony.print("FPRINT: gblNum="+gblNum);
        gblNum ="";
		//frmAfterLogoutMB.tbxAccessPIN.text = "";
	}
	inputParam["activityTypeId"] = "Login";
	gblLoginType = "Login";
	
	if(gblSuccess == true || gblSuccess == "true"){
	   inputParam["loginTypeTouch"] = "true";
	 }
	else{ 
	   inputParam["loginTypeTouch"] = "false";
	 }
	inputParam["deviceOS"] = getDeviceOS();
	//Hashing deviceId  
	//var secKeyVal = decrypt(glbInitVector);
	//alert("secKeyVal--->"+secKeyVal);
	invokeServiceSecureAsync("LoginCompositeService", inputParam, callBackMBLoginAuth)
}
function callBackMBLoginAuth(status, resulttable) {
kony.print("FPRINT: gblStartClickFromActivationMB="+gblStartClickFromActivationMB);
kony.print("FPRINT: status="+status);
kony.print("FPRINT: resulttable[opstatus]="+resulttable["opstatus"]); 
kony.print("FPRINT: resulttable[deviceStatus]="+resulttable["deviceStatus"]); 
kony.print("FPRINT: resulttable[errMsg]="+resulttable["errMsg"]); 
	if (status == 400) {
		
		if(gblStartClickFromActivationMB){
			gblStartClickFromActivationMB=false;
			kpns_log("callBackMBLoginAuth: call case gblStartClickFromActivationMB");
			registerAppWithPushNotificationsInfrastructure();
		} else {
			var ksid = kony.store.getItem("ksid");
			//var is_updated_kpns = kony.store.getItem("is_updated_kpns");
			// subscription token or code
			var pushRegID=kony.store.getItem("kpnssubcriptiontoken");
			if(ksid==null || ksid=="null" || ksid=="undefined"
				|| pushRegID == null || pushRegID=="null" || pushRegID=="undefined") {
				kpns_log("callBackMBLoginAuth: call case ksid is null or pushRegID is null");
				registerAppWithPushNotificationsInfrastructure();
			}
		}
		if (resulttable["opstatus"] == 1234 || resulttable["opstatus"] == "1234") {
		    kony.application.dismissLoadingScreen();
		    checkUpgrades(resulttable["updatestatus"], resulttable["updateurl"]);
		    resetValues();
		    return false;
		    
		} else if (resulttable["opstatus"] == 0) {
			gblShowAnyIDRegistration = resulttable["setyourID"];
			gblCustomerIDType=resulttable["customerIDType"];
			gblCustomerIDValue=resulttable["customerIDValue"];
			gbllastMbLoginSucessDateEng=resulttable["lastMbLoginSucessDateEng"];
			gbllastMbLoginSucessDateThai=resulttable["lastMbLoginSucessDateThai"];
			if (resulttable["deviceStatus"] == "0") {
				showAlert(kony.i18n.getLocalizedString("ECKonyDeviceErr00002"), kony.i18n.getLocalizedString("info"));
				resetValues();
				kony.application.dismissLoadingScreen();
				return false;
			} else if (resulttable["deviceStatus"] == "2") {
				showAlert("Device is blocked", kony.i18n.getLocalizedString("info"));
				resetValues();
				kony.application.dismissLoadingScreen();
				return false;
			} else if (resulttable["deviceStatus"] == "3") {
				showAlert("Device is cancelled", kony.i18n.getLocalizedString("info"));
				resetValues();
				kony.application.dismissLoadingScreen();
				return false;
			}
			else if (resulttable["errMsg"] == "Password is verified") {
				var time = resulttable["time"];
				GBL_TIMEFOR_LOGIN = resulttable["time"]
				registerForTimeOut();
				gblTouchStatus = resulttable["usesTouchId"];
				gblActivationFlow = true;  
				kony.print("FPRINT: Password is verified ");
				if(gblTouchStatus == undefined || gblTouchStatus == "null" ||  gblTouchStatus == null || gblTouchStatus == "" || gblTouchStatus == "N"){
					gblTouchStatus = "N";
					kony.store.setItem("usesTouchId", "N");
				} else if(gblTouchStatus == "Y")
				{
					kony.store.setItem("usesTouchId", "Y");
				}
				kony.print("FPRINT: gblTouchStatus="+gblTouchStatus);
				loginPProcessCallBack(resulttable);
				
			} else if (resulttable["errMsg"] == "Password Locked" || resulttable["errCode"] == "E10105") {
				 resetValues();
				 onClickChangeTouchIcon();
				 kony.print("FPRINT SETTING isUserStatusActive TO FALSE");
				 isUserStatusActive=false;
				 popupAccesesPinLocked.lblPopupConfText.text = kony.i18n.getLocalizedString("acclockmsg");
				 popupAccesesPinLocked.btnPopupConfCancel.text= kony.i18n.getLocalizedString("keyCancelButton");
				 popupAccesesPinLocked.btnCallTMB.text= kony.i18n.getLocalizedString("keyCall");
				 popupAccesesPinLocked.show();
				//alert(" " + kony.i18n.getLocalizedString("keyAccessPwdLocked"));
				kony.application.dismissLoadingScreen();
				return false;
			} else if (resulttable["errMsg"] == "User is already login to application") {
			    resetValues();
				kony.application.dismissLoadingScreen();
				showAlert(kony.i18n.getLocalizedString("ECUserLoggedIn"), kony.i18n.getLocalizedString("info"));
				frmMBPreLoginAccessesPin.show();		//As per existing behaviour from R67 app. QA expectation
				return false;
			}else {
                 resetValues();
				//showAlertRcMB(kony.i18n.getLocalizedString("keyWrongPwd"),kony.i18n.getLocalizedString("info"), "info");
				kony.application.dismissLoadingScreen();
				return false;
			}
		} else if (resulttable["errMsg"] == "User is already login to application") {
		    resetValues();
			kony.application.dismissLoadingScreen();
			showAlert(kony.i18n.getLocalizedString("ECUserLoggedIn"), kony.i18n.getLocalizedString("info"));
			frmMBPreLoginAccessesPin.show();		//As per existing behaviour from R67 app. QA expectation
			return false;
		} else {
			var errorMessage = resulttable["errMsg"];
			var middlewareErrorMessage = resulttable["errmsg"];
			if(errorMessage == null || errorMessage == undefined || errorMessage == "" ){
				if(middlewareErrorMessage != null && middlewareErrorMessage != undefined ){
				    errorMessage =  middlewareErrorMessage;
				}
			}		
			resetValues();
			kony.application.dismissLoadingScreen();
			alert(" " + errorMessage);
			return false;
		}
	}else{
            resetValues();	
	
	}
}
//////Login Auth Composite service call ends here////
