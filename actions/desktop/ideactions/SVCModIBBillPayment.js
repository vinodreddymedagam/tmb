billPaymentSuggestList = [];
billPaymentList = [];
gblMinAmount = "";
gblMaxAmount = "";
gblPayFull = "";
/*
 * Method for getting values from the service by name CustomerAccountService
 *
 */

function callBillPaymentCustomerAccountServiceIB() {
if(getCRMLockStatus())
	{
	curr_form=kony.application.getCurrentForm().id;
		dismissLoadingScreenPopup();
		popIBBPOTPLocked.show();
	}
	else{
	
	var inputParam = {}
	inputParam["billPayInd"] = "billPayInd";
	//billPaymentCustomerAccountCallBackIB(status,resulttable)
	showLoadingScreenPopup();
	invokeServiceSecureAsync("customerAccountInquiry", inputParam, billPaymentCustomerAccountCallBackIB);
	}
}

function billPaymentCustomerAccountCallBackIB(status, resulttable) {
	if (status == 400) //success responce
	{
		if (resulttable["opstatus"] == 0) {
			/** checking for Soap status below  */
			var StatusCode = resulttable["statusCode"];
			var fromData = []
			var j = 1
			gbltranFromSelIndex = [0, 0];
			if(resulttable.custAcctRec!=null)
			{gblNoOfFromAcs = resulttable.custAcctRec.length;
			var nonCASAAct = 0;
			for (var i = 0; i < resulttable.custAcctRec.length; i++) {
				//
				//fiident	
				var accountStatus = resulttable["custAcctRec"][i].acctStatus;
				if(accountStatus.indexOf("Active") == -1){
				nonCASAAct = nonCASAAct + 1;
				//showAlertWithCallBack(kony.i18n.getLocalizedString("MB_StatusNotEligible"), kony.i18n.getLocalizedString("info"),onclickActivationCompleteStart);
					//return false;
				}
				if(accountStatus.indexOf("Active") >=0){
				if(resulttable.custAcctRec[i].personalisedAcctStatusCode == "02" ) {
						//alert("deleted account found");
						continue;
					}
				var icon = "";
				
					icon="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+resulttable.custAcctRec[i]["ICON_ID"]+"&modIdentifier=PRODICON";
				if (j == 1) {
					var temp = createSegmentRecordIB(resulttable.custAcctRec[i],icon);
				} else if (j == 2) {
					var temp = createSegmentRecordIB(resulttable.custAcctRec[i],icon);
				} else if (j == 3) {
					var temp = createSegmentRecordIB(resulttable.custAcctRec[i],icon);
					j = 0;
				}
				j++;
				var jointActXfer = resulttable.custAcctRec[i].partyAcctRelDesc;
				if(jointActXfer == "PRIJNT" || jointActXfer == "SECJNT" || jointActXfer == "OTHJNT" || jointActXfer == "SECJAN") {
				}
				 else {
					kony.table.insert(fromData, temp[0])
				}
				}
				//
			} }//for
			//frmIBBillPaymentCW.customIBBPFromAccount.data = fromData;
			if(nonCASAAct==resulttable.custAcctRec.length)
			{
				
				showAlertWithCallBack(kony.i18n.getLocalizedString("MB_StatusNotEligible"), kony.i18n.getLocalizedString("info"),onclickActivationCompleteStart);
				return false;
			}
			if(fromData.length == 0){
				//alert(kony.i18n.getLocalizedString("MB_CommonError_NoSA"));
				showAlertWithCallBack(kony.i18n.getLocalizedString("MB_CommonError_NoSA"), kony.i18n.getLocalizedString("info"),onclickActivationCompleteStart);
				dismissLoadingScreenPopup();
				return;
			}else{
				frmIBBillPaymentCW.customIBBPFromAccount.data = fromData;
			}
			//frmIBBillPaymentCW.customIBBPFromAccount.data = fromData;
			dismissLoadingScreenPopup();
			frmIBBillPaymentCW.show();
			paymentOverpaidCheck();
			
			if(window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8")
				langspecificmenuIE8();
			else
				langspecificmenu();
				
			if(kony.i18n.getCurrentLocale() != "th_TH")
				frmIBBillPaymentCW.btnMenuBillPayment.skin = "btnIBMenuBillPaymentFocus";
			else 
				frmIBBillPaymentCW.btnMenuBillPayment.skin = "btnIBMenuBillPaymentFocusThai";
			frmIBBillPaymentCW.segMenuOptions.removeAll();	
			frmIBBillPaymentCW.customIBBPFromAccount.onSelect = customWidgetSelectEventBillPmntIB;
		} 
		else if (resulttable["opstatus"] == 1) {
		showAlertWithCallBack(kony.i18n.getLocalizedString("MB_CommonError_NoSA"), kony.i18n.getLocalizedString("info"),onclickActivationCompleteStart);
		}
		else {
			dismissLoadingScreenPopup();
			alert(" " + resulttable["errMsg"]);
		}
	} //status 400
}
/*
 *  on click of Cancel of IB Bill Payment Confirmation
 */

function cancelIBBillPaymentConfm() {
	//frmIBBillPaymentLP.destroy();
	//frmIBBillPaymentCompletenow.destroy();
	//Below should not be destoryed
	//frmIBBillPaymentConfirm.destroy();
	gblFromAccountSummary	= false;
	gblFromAccountSummaryBillerAdded = false;
	gblPaynow=true;
	callBillPaymentCustomerAccountServiceIB();
	//var	nameofforma = kony.application.getCurrentForm();
}
/*
 * Method to get the Amount from the OnlinePaymentInquiry Service
 *
 *
 */

function BillPaymentOnlinePaymentInqServiceCallBackIB(status, result) {

    penalty = "0.00";
    fullAmt = "0.00";
    minAmt = "0.00";
    gblRef1 = "";
    gblRef2 = "";
    gblRef3 = "";
    gblRef4 = "";
    BankRefId = "";
    TranId = "";
    gblMinAmount = "";
    gblMaxAmount = "";
    //ENH_113_1 - MEA show warn message for I004 & I005 
	var warnStatusCode = "";
    var warnMsg = "";
    
    if (status == 400) //success responce
    {
        frmIBBillPaymentLP.hbxHideBtn.setVisibility(false);
        if (result["opstatus"] == 0) {
            if (result["StatusCode"] == 0) {

                for (var i = 0; i < result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"].length; i++) {

                    if (result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscName"] == "PenaltyAmt")
                        penalty = result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscText"];
                    if (result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscName"] == "FullAmt")
                        fullAmt = result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscText"];
                    if (result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscName"] == "BilledAmt")
                        minAmt = result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscText"];
					if (result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscName"] == "StatusCode") {
                        warnStatusCode = result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscText"];
					}
					
                    //CR71 Implementation	
                    if (kony.string.equalsIgnoreCase(result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscName"], "Minimum")) {
                        gblMinAmount = result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscText"];
                    }

                    if (kony.string.equalsIgnoreCase(result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscName"], "Maximum")) {
                        gblMaxAmount = result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscText"];
                    }
                }
                BankRefId = result["OnlinePmtInqRs"][0]["BankRefId"];
                TranId = result["OnlinePmtInqRs"][0]["TrnId"];
                gblRef1 = result["OnlinePmtInqRs"][0]["Ref1"];
                gblRef2 = result["OnlinePmtInqRs"][0]["Ref2"];
                gblRef3 = result["OnlinePmtInqRs"][0]["Ref3"];
                gblRef4 = result["OnlinePmtInqRs"][0]["Ref4"];
                
                warnMsg = result["OnlinePmtInqRs"][0]["warnMsg"];
                
                frmIBBillPaymentLP.lblFullPayment.setVisibility(true);
                frmIBBillPaymentLP.txtBillAmt.setVisibility(false);
                frmIBBillPaymentLP.lblTHB.setVisibility(false);
                gblFullPayment = true;

                if (penalty == "0.00" || penalty == undefined) {
                    gblPenalty = false;

                } else {
                    gblPenalty = true;

                }


                // need clarification
                if (gblPenalty) {
                    frmIBBillPaymentLP.lblPenaltyVal.text = penalty + kony.i18n.getLocalizedString("currencyThaiBaht");
                    frmIBBillPaymentLP.lblAmountValue.text = fullAmt + kony.i18n.getLocalizedString("currencyThaiBaht");
                    frmIBBillPaymentLP.lblFullPayment.text = (parseInt(fullAmt) + parseInt(penalty)) + " " +kony.i18n.getLocalizedString("currencyThaiBaht");

                    frmIBBillPaymentLP.hbxPenalty.setVisibility(true);
                    frmIBBillPaymentLP.txtBillAmt.text = ""; // parseInt(frmIBBillPaymentLP["segBPBillsList"]["selectedItems"][0]["fullAmount"]) + parseInt("100") + "\u0E3F";
                } else {
                    frmIBBillPaymentLP.lblFullPayment.text = numberWithCommas(removeCommos(fullAmt)) + kony.i18n.getLocalizedString("currencyThaiBaht");

                    frmIBBillPaymentLP.hbxPenalty.setVisibility(false);
                    frmIBBillPaymentLP.txtBillAmt.text = "" //frmIBBillPaymentLP["segBPBillsList"]["selectedItems"][0]["fullAmount"] + "\u0E3F";
                }
                
                //Don't Remove this code - Temporarily Commenting Code for IB because OnlinePaymentInq is always giving statusCode as 0001, because we are not sending Amt to Service
                //Thai Life Warning Message PBI - 
               /* if (gblCompCode == "0947" || gblCompCode == "0016") {
                	if(warnStatusCode == "0001") {
                		alert(newLineTextIB(kony.i18n.getLocalizedString("keyThaiLifeWarning")));
                	}
                   }
                 */
                    
                //ENH_113
                if (gblCompCode == "2533") { //MEA biller
                    if (TranId != "" && gblRef4 == "Y") {
                    	if(warnStatusCode == "I004" || warnStatusCode == "I005") {
                    		alert(warnMsg);
                    	}
                    	frmIBBillPaymentLP.lblBPAmount.containerWeight = 35;
                        gblSegBillerData["MeterNo"] = result["OnlinePmtInqRs"][0]["Ref1"];
                        gblSegBillerData["CustName"] = result["OnlinePmtInqRs"][0]["BankRefId"];
                        gblSegBillerData["CustAddress"] = result["OnlinePmtInqRs"][0]["CustReference"];
                        var locale = kony.i18n.getCurrentLocale();
                        if (kony.string.startsWith(locale, "en", true) == true) {
                            frmIBBillPaymentLP.lblBPAmount.text = gblSegBillerData["billerTotalPayAmtEn"] + ":";
                            frmIBBillPaymentLP.lblAmtLabel.text = gblSegBillerData["billerAmountEn"] + ":";
                            frmIBBillPaymentLP.lblAmtInterest.text = gblSegBillerData["billerTotalIntEn"] + ":";
                            frmIBBillPaymentLP.lblAmtDisconnected.text = gblSegBillerData["billerDisconnectAmtEn"] + ":";
                        } else {
                            frmIBBillPaymentLP.lblBPAmount.text = gblSegBillerData["billerTotalPayAmtTh"] + ":";
                            frmIBBillPaymentLP.lblAmtLabel.text = gblSegBillerData["billerAmountTh"] + ":";
                            frmIBBillPaymentLP.lblAmtInterest.text = gblSegBillerData["billerTotalIntTh"] + ":";
                            frmIBBillPaymentLP.lblAmtDisconnected.text = gblSegBillerData["billerDisconnectAmtTh"] + ":";
                        }
                        allowSchedule = gblSegBillerData["billerAllowSetSched"]
                        if (allowSchedule == "N") {
                            frmIBBillPaymentLP.btnBPPayOn.setEnabled(false);
                        } else {
                            frmIBBillPaymentLP.btnBPPayOn.setEnabled(true);
                        }
                        frmIBBillPaymentLP.hbxAmount.hoverSkin = "hbxProper";
                        frmIBBillPaymentLP.lnkHide.text = kony.i18n.getLocalizedString("show");
                        frmIBBillPaymentLP.hbxHideBtn.setVisibility(true);
                        frmIBBillPaymentLP.hbxAmountDetailsMEA.setVisibility(false);
                        frmIBBillPaymentLP.lblFullPayment.text = autoFormatAmount(fullAmt);
                        frmIBBillPaymentLP.lblAmtLabel.text = kony.i18n.getLocalizedString("amount");
                        if (gblRef2 == undefined || gblRef2 == null || gblRef2 == "") {
                            gblRef2 = "0.00";
                        }
                        if (gblRef3 == undefined || gblRef3 == null || gblRef3 == "") {
                            gblRef3 = "0.00";
                        }
                        var newAmount = fullAmt - gblRef2 - gblRef3;
                        frmIBBillPaymentLP.lblAmtValue.text = autoFormatAmount(newAmount.toString());
                        frmIBBillPaymentLP.lblAmtInterestValue.text = autoFormatAmount(gblRef2);
                        frmIBBillPaymentLP.lblAmtDisconnectedValue.text = autoFormatAmount(gblRef3);
                        gblOnlinePmtType = result["OnlinePmtInqRs"][0]["OnlinePmtType"];
                        //set values in all label recieved from service for split amount
                    } else {
                        dismissLoadingScreenPopup();
                        //clearIBBillPaymentLP();
                        //alert(kony.i18n.getLocalizedString("keyMEARef2orRef4Invalid"));
                        //clearIBBillPaymentLP();
                    }

                } else {
                    frmIBBillPaymentLP.hbxHideBtn.setVisibility(false);
                }
                //ENH_113

                if (gblPayFull == "Y") {
                    gblFullPayment = true;
                    frmIBBillPaymentLP.lblFullPayment.setVisibility(true);
                    frmIBBillPaymentLP.hbxAmtVal.setVisibility(false);
                    frmIBBillPaymentLP.btnAmtFull.setVisibility(false);
                    frmIBBillPaymentLP.btnAmtMinimum.setVisibility(false);
                    frmIBBillPaymentLP.btnAmtSpecified.setVisibility(false);
                    frmIBBillPaymentLP.hbxMinMax.setVisibility(false);
                }
                dismissLoadingScreenPopup();
                
                if (gblCompCode == "2533" && gblRef4 == "N") {
                    frmIBBillPaymentLP.show();
                    clearIBBillPaymentLPOnRef1Ref2Done();
                    frmIBBillPaymentLP.hbxImage.setVisibility(true);
                    dismissLoadingScreenPopup();
                    alert(kony.i18n.getLocalizedString("keyMEARef2orRef4Invalid"));
                }
            } else if (gblCompCode == "2533") {
                var addMEABiller = false;
                var statusCode = ["PGI001", "PGI002", "PGI003", "PGI004", "PGI005", "PGI006", "PGI007", "PGI008", "PGI009", "PGI010"];
                var currForm = kony.application.getCurrentForm();
                if (currForm.id == "frmIBMyBillersHome") {
                    for (i = 0; i < statusCode.length; i++) {
                        if (result["XPServerStatCode"] == statusCode[i]) {
                            addMEABiller = true;
                        }
                    }
                } else {
                    addMEABiller = false;
                }
                if (addMEABiller == true) {
                    frmIBBillPaymentLP.show();
                    clearIBBillPaymentLPOnRef1Ref2Done();
                    frmIBBillPaymentLP.hbxImage.setVisibility(true);
                    dismissLoadingScreenPopup();
                    var msg = result["additionalDS"][2]["StatusDesc"];
                    if (result["XPServerStatCode"] == "PGI003" || result["XPServerStatCode"] == "PGI006") {
                        alert(" " + result["additionalDS"][2]["StatusDesc"]);
                    } else {
                        alert(" " + result["errMsg"]);
                    }

                } else {
                    clearIBBillPaymentLPOnRef1Ref2Done();
                    dismissLoadingScreenPopup();
                    if (result["XPServerStatCode"] == "PGI003" || result["XPServerStatCode"] == "PGI006") {
                        alert(" " + result["additionalDS"][2]["StatusDesc"]);
                    } else {
                        alert(" " + result["errMsg"]);
                    }
                }
            } else {
                clearIBBillPaymentLPOnRef1Ref2Done();
                dismissLoadingScreenPopup();
                alert(" " + result["errMsg"]);

            }

        } else {
            alert(" " + result["errMsg"]);
            dismissLoadingScreenPopup();
        }
    }
}
/*
 * Method to get the FeeAmount from the BillPaymentInquiry Service
 *
 *
 */

function BillPaymentInquiryModuleIB() {
	inputParam = {};
	var tranCode;
	if (frmIBBillPaymentLP.lblAcctType.text == kony.i18n.getLocalizedString("DDA")) {
		tranCode = "88" + "10";
	} else {
		tranCode = "88" + "20";
	}
	var invoice;
	if (gblreccuringDisableAdd == "N") {
		invoice = "";
	} else {
		invoice = frmIBBillPaymentLP.lblRefVal2.text;
	}
	var fromAcctNo = removeHyphenIB(frmIBBillPaymentLP.lblFromAccNo.text);
	var fromAccType;
	if (fromAcctNo.length == 14) {
		fromAccType = "SDA";
	} else {
		fourthDigit = fromAcctNo.charAt(3);
		if (fourthDigit == "2" || fourthDigit == "7" || fourthDigit == "9") {
			fromAccType = "SDA";
			fromAcctNo = "0000" + fromAcctNo;
		} else {
			fromAccType = "DDA";
		}
	}
	inputParam["tranCode"] = tranCode;
	inputParam["fromAcctIdentValue"] = fromAcctNo; //removeHyphenIB(frmIBBillPaymentLP.lblFromAccNo.text);
	inputParam["fromAcctTypeValue"] = frmIBBillPaymentLP.lblAcctType.text;
	inputParam["transferAmount"] = billAmountBillPay;
	inputParam["pmtRefIdent"] = frmIBBillPaymentLP.lblBPRef1Val.text;
	inputParam["invoiceNumber"] = invoice;
	inputParam["postedDate"] = changeDateFormatForService(frmIBBillPaymentLP.lblBPBillPayDate.text);
	inputParam["ePayCode"] = "";
	inputParam["compCode"] = gblCompCode;
	inputParam["waiveCode"] = "I";
	inputParam["fIIdent"] = frmIBBillPaymentLP.lblDummy.text;
	invokeServiceSecureAsync("billPaymentInquiry", inputParam, BillPaymentInquiryServiceCallBackIB);
}

function BillPaymentInquiryServiceCallBackIB(status, result) {
	
	
	
	if (status == 400) //success responce
	{
		
		
		
		if (result["opstatus"] == 0) {
			var FeeAmnt = "0";
			var StatusCode = result["StatusCode"];
			/** checking for Soap status below  */
			
			if (StatusCode != 0) {
				dismissLoadingScreenPopup();
				alert(" " + result["errMsg"]);
				return false;
			} else {
				for (var i = 0; i < result["BillPmtInqRs"].length; i++) {
					if (result["BillPmtInqRs"][i]["WaiveFlag"] == "Y") {
						FeeAmnt = "0.00";
					} else {
						FeeAmnt = result["BillPmtInqRs"][i]["FeeAmnt"];
					}
				}
			}
			frmIBBillPaymentConfirm.lblFeeVal.text = parseFloat(FeeAmnt).toFixed(2) +" "+ kony.i18n.getLocalizedString("currencyThaiBaht");
			checkBillPaymentCrmProfileInqIB();
		} else {
			dismissLoadingScreenPopup();
			alert(" " + result["errMsg"]);
		}
	}
}

function BillPaymentcreditcardDetailsInqServiceCallBackIB(status, result) {
	
	fullAmt = "0.00";
	minAmt = "0.00";
	
	if (status == 400) //success responce
	{
		var accountlist = gblAccountTable;
		
		if (result["opstatus"] == 0) {
			if (result["StatusCode"] == 0) {
				var Amt = result["fullPmtAmt"];
				fullAmt = result["fullPmtAmt"];
				if(parseInt(fullAmt)<0)alert(kony.i18n.getLocalizedString("keyPaymentOverpaid"));
				minAmt = result["minPmtAmt"]
				if(fullAmt=="")
				fullAmt="0.00";
				if(minAmt=="")
				minAmt="0.00";			
				gblPenalty = false;
	           
	            frmIBBillPaymentLP.lblBPAmount.text = kony.i18n.getLocalizedString("keyAmount");
			    //Penalty Hide
			    frmIBBillPaymentLP.hbxPenalty.setVisibility(false);
			
			    	
				   	frmIBBillPaymentLP.btnAmtFull.setEnabled(true);
				    frmIBBillPaymentLP.btnAmtFull.setVisibility(true);
				    frmIBBillPaymentLP.btnAmtMinimum.setEnabled(true);
				    frmIBBillPaymentLP.btnAmtMinimum.setVisibility(true);
				    frmIBBillPaymentLP.btnAmtSpecified.setEnabled(true);
				    frmIBBillPaymentLP.btnAmtSpecified.setVisibility(true);
				    frmIBBillPaymentLP.hbxMinMax.setVisibility(true);
				    frmIBBillPaymentLP.hbxAmtVal.isVisible = true;
				    frmIBBillPaymentLP.txtBillAmt.text = numberWithCommas(removeCommos(fullAmt));
				    frmIBBillPaymentLP.txtBillAmt.setVisibility(false);
				    frmIBBillPaymentLP.lblTHB.setVisibility(false);
				    frmIBBillPaymentLP.lblFullPayment.setVisibility(true);
				    eh_frmIBBillPaymentLP_btnAmtFull_onClick();
			
	            //frmIBBillPaymentLP.lblFullPayment.text = Amt;
	            dismissLoadingScreenPopup();
			}
			else{
					dismissLoadingScreenPopup();
					alert(" " + result["errMsg"]);
			}
		} else {
			dismissLoadingScreenPopup();
			alert(" " + result["errMsg"]);
		}
	}
}

function BillPaymentdoLoanAcctInqServiceCallBackIB(status, result) {
    showLoadingScreenPopup();
    fullAmt="0.00"
    
    
    if (status == 400) //success responce
    {
        
        
        
        if (result["opstatus"] == 0) {
            
            var balAmount;
            fullAmt=result["regPmtCurAmt"];
            
            frmIBBillPaymentLP.lblFullPayment.text = numberWithCommas(removeCommos(fullAmt)) + kony.i18n.getLocalizedString("currencyThaiBaht");
            frmIBBillPaymentLP.lblFullPayment.setVisibility(true);
            
            frmIBBillPaymentLP.txtBillAmt.text = fullAmt;
            frmIBBillPaymentLP.txtBillAmt.setVisibility(false);
            frmIBBillPaymentLP.lblTHB.setVisibility(false);
            dismissLoadingScreenPopup();
        } else {

            alert(" " + result["errMsg"]);
            dismissLoadingScreenPopup();
        }
    }
}

function BillPaymentdepositAccountInquiryServiceCallBackIB(status, result) {
	showLoadingScreenPopup();
	
	
	if (status == 400) //success responce
	{
		
		
		
		if (result["opstatus"] == 0) {
			//var StatusCode = result["statusCode"];
			/** checking for Soap status below  */
			
			var BalType;
			var Amt;
			var CurCode;
			//if(StatusCode!= 0){
			//					 alert("----DepositAccountInquiry StatusDesc:"+StatusDesc)
			//					return false;
			//				 }
			//else
			//				{
			for (var i = 0; i < result["AcctBal"].length; i++) {
				BalType = result["AcctBal"][i]["BalType"];
				
				if (BalType == "Principle") {
					Amt = result["AcctBal"][i]["Amt"];
					CurCode = result["AcctBal"][i]["CurCode"];
				}
			}
			
			frmIBBillPaymentLP.txtBillAmt.text = numberWithCommas(removeCommos(Amt));
			dismissLoadingScreenPopup();
		} else {
			alert(" " + result["errMsg"]);
			dismissLoadingScreenPopup();
		}
	}
}

function getTodaysDate() {
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth() + 1; //January is 0!
	var yyyy = today.getFullYear();
	if (dd < 10) dd = '0' + dd
	if (mm < 10) mm = '0' + mm
	today = yyyy + "-" + mm + "-" + dd
	return today;
}

function getCurrentDate() {
	var today = new Date();
	return today.toISOString();
}

/**
 * Method to start the service that request OTP

 */

function startBillPaymentOTPRequestService() {
    frmIBBillPaymentConfirm.txtotp.text = "";
	var locale = kony.i18n.getCurrentLocale();
	var eventNotificationPolicy;
	var SMSSubject;
	var Channel;
	Channel = "BillPayment";
	
    frmIBBillPaymentConfirm.lblOTPinCurr.text = " ";//kony.i18n.getLocalizedString("invalidOTP"); //
    frmIBBillPaymentConfirm.lblPlsReEnter.text = " "; 
    frmIBBillPaymentConfirm.hbxOTPincurrect.isVisible = false;
    frmIBBillPaymentConfirm.hbox476047582127699.isVisible = true;
    frmIBBillPaymentConfirm.hbxOTPsnt.isVisible = true;
    //frmIBBillPaymentConfirm.txtotp.setFocus(true); 
	var inputParams = {
		retryCounterRequestOTP: gblRetryCountRequestOTP,
		Channel: Channel,
		billerCategoryID: gblBillerCategoryID,
		locale: locale
	};
	showLoadingScreenPopup();
	try {
		// invokeServiceSecureAsync("generateOTP", inputParams, startOTPRequestServiceBPAsyncCallback);
		invokeServiceSecureAsync("generateOTPWithUser", inputParams, startOTPRequestServiceBPAsyncCallback);
	} catch (e) {
		// todo: handle exception
		invokeCommonIBLogger("Exception in invoking service");
	}
}
/**
 * Callback method for startOTPRequestService
 * @param {} callBackResponse
 */

function startOTPRequestServiceBPAsyncCallback(status, callBackResponse) {
	if (status == 400) {
		if (callBackResponse["errCode"] == "GenOTPRtyErr00001") {
		dismissLoadingScreenPopup();
		showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"),
			kony.i18n.getLocalizedString("info"));
		return false;
	   }
	   if (callBackResponse["errCode"] == "GenOTPRtyErr00002") {
	   	dismissLoadingScreenPopup();
		showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00002"), kony.i18n.getLocalizedString("info"));
		return false;
	   }
		if (callBackResponse["opstatus"] == 0) {
			
			
			if (callBackResponse["errCode"] == "GenOTPRtyErr00001") {
				dismissLoadingScreenPopup();
				showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"),
					kony.i18n.getLocalizedString("info"));
				return false;
			} else if (callBackResponse["errCode"] == "JavaErr00001") {
				dismissLoadingScreenPopup();
				showAlert(kony.i18n.getLocalizedString("ECJavaErr00001"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if(callBackResponse["errCode"] == "ECGenOTPRtyErr00002"){
				dismissLoadingScreenPopup();
				showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00002"), kony.i18n.getLocalizedString("info"));
				frmIBBillPaymentConfirm.btnOTPReq.onClick = "";
				frmIBBillPaymentConfirm.btnOTPReq.skin = btnIBREQotp;
				frmIBBillPaymentConfirm.btnOTPReq.focusSkin = btnIBREQotp;
				frmIBBillPaymentConfirm.btnOTPReq.hoverSkin = btnIBREQotp;
				return false;
			}
			gblOTPReqLimit = kony.os.toNumber(callBackResponse["otpRequestLimit"]);
			var reqOtpTimer = kony.os.toNumber(callBackResponse["requestOTPEnableTime"]);
			gblRetryCountRequestOTP = kony.os.toNumber(callBackResponse["retryCounterRequestOTP"]);
			gblOTPLENGTH = kony.os.toNumber(callBackResponse["otpLength"]);
			
			/*try {
				kony.timer.cancel("OTPTimer");
			} catch (e) {
				// todo: handle exception
			}*/
			var refVal="";
   			for(var d=0;callBackResponse["Collection1"].length;d++){
    			if(callBackResponse["Collection1"][d]["keyName"] == "pac"){
      				refVal=callBackResponse["Collection1"][d]["ValueString"];
      			break;
    		}
   		}
			kony.timer.schedule("OtpTimer", OTPTimercallbackBP, reqOtpTimer, false);
			frmIBBillPaymentConfirm.hbox476047582127699.setVisibility(true);
			frmIBBillPaymentConfirm.hbxOTPsnt.setVisibility(true);
			//frmIBBillPaymentConfirm.txtotp.setFocus(true); 
			frmIBBillPaymentConfirm.btnOTPReq.skin = btnIBREQotp;
			frmIBBillPaymentConfirm.btnOTPReq.focusSkin = btnIBREQotp;
			frmIBBillPaymentConfirm.btnOTPReq.hoverSkin = btnIBREQotp;
			frmIBBillPaymentConfirm.btnOTPReq.onClick = "";
			frmIBBillPaymentConfirm.lblBankRef.setVisibility(true);
			frmIBBillPaymentConfirm.label476047582127701.setVisibility(true);
			frmIBBillPaymentConfirm.label476047582115277.setVisibility(true);
			frmIBBillPaymentConfirm.label476047582115279.setVisibility(true);
			frmIBBillPaymentConfirm.label476047582127701.text = refVal;//callBackResponse["bankRefNo"];
			frmIBBillPaymentConfirm.label476047582115277.text = kony.i18n.getLocalizedString("keyotpmsg");
			frmIBBillPaymentConfirm.label476047582115279.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
			dismissLoadingScreenPopup();
			frmIBBillPaymentConfirm.txtotp.setFocus(true);
		} else {
			dismissLoadingScreenPopup();
			//alert(" " + kony.i18n.getLocalizedString("Receipent_alert_Error") + ":" + callBackResponse["errMsg"]);
			if (callBackResponse["errCode"] == "GenOTPRtyErr00001") {
				dismissLoadingScreenPopup();
				showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"),
					kony.i18n.getLocalizedString("info"));
				return false;
			} else if (callBackResponse["errCode"] == "JavaErr00001") {
				dismissLoadingScreenPopup();
				showAlert(kony.i18n.getLocalizedString("ECJavaErr00001"), kony.i18n.getLocalizedString("info"));
				return false;
			}else {
				dismissLoadingScreenPopup();
				alert(""+callBackResponse["errMsg"]);				
			}
		}
	} else {
		if (status == 300) {
			dismissLoadingScreenPopup();
			
		}
	}
}
/**
 * Cancelling the otpTimer
 */

function OTPTimercallbackBP() {

	if(gblRetryCountRequestOTP >= gblOTPReqLimit){
		frmIBBillPaymentConfirm.btnOTPReq.skin = btnIBREQotp;
		frmIBBillPaymentConfirm.btnOTPReq.focusSkin = btnIBREQotp;
		frmIBBillPaymentConfirm.btnOTPReq.onClick = "";
		frmIBBillPaymentConfirm.btnOTPReq.hoverSkin = btnIBREQotp;
	}else {
		frmIBBillPaymentConfirm.btnOTPReq.skin = btnIBREQotpFocus;
		frmIBBillPaymentConfirm.btnOTPReq.hoverSkin = btnIBREQotpFocus;
		frmIBBillPaymentConfirm.btnOTPReq.focusSkin = btnIBREQotpFocus;
		frmIBBillPaymentConfirm.btnOTPReq.onClick = startBillPaymentOTPRequestService;
	}
	
	try {
		kony.timer.cancel("OtpTimer");
	} catch (e) {
		
	}
}

function verifyOTPBillPayment() {
		//startOTPValidationBP();
      confirmBillPaymentServiceCall();    
}


/*
 * Composite Service call on submit of OTP/Token
 * 
*/
function confirmBillPaymentServiceCall() {
    var inputParam = {};
    showLoadingScreenPopup();
    inputParam["flowspa"] = false;
    inputParam["gblFromAccountSummary"] = gblFromAccountSummary;
    if (gblTokenSwitchFlag == true) {
        var otpToken = frmIBBillPaymentConfirm.tbxToken.text;
        if (otpToken != null) {
            otpToken = otpToken.trim();
        } else {
            dismissLoadingScreenPopup();
            alert(kony.i18n.getLocalizedString("Receipent_tokenId"));
            
            return false;
        }
        if (otpToken == "") {
            dismissLoadingScreenPopup();
            alert(kony.i18n.getLocalizedString("Receipent_tokenId"));
            return false;
        }
        inputParam["verifyToken_loginModuleId"] = "IB_HWTKN";
        inputParam["verifyToken_userStoreId"] = "DefaultStore";
        inputParam["verifyToken_retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
        inputParam["verifyToken_retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
        inputParam["password"] = otpToken; //frmIBBillPaymentConfirm.tbxToken.text;
        
        inputParam["verifyToken_segmentId"] = "segmentId";
        inputParam["TokenSwitchFlag"] = true;
        inputParam["verifyToken_segmentIdVal"] = "MIB";
        //inputParam["verifyToken_channel"] = "InterNet Banking";
        //inputParam["verifyToken_appID"] = appConfig.appId;
        //inputParam["verifyToken_channelName"] = "IB";
    } else {
        var otpText = frmIBBillPaymentConfirm.txtotp.text;
        if (otpText != null) {
            otpText = otpText.trim();
        } else {
			dismissLoadingScreenPopup();
            alert(kony.i18n.getLocalizedString("Receipent_alert_correctOTP"));
            return false;
        }
        if (otpText == "") {
            dismissLoadingScreenPopup();
            alert(kony.i18n.getLocalizedString("Receipent_alert_correctOTP"));
            return false;
        }
        inputParam["verifyPwd_retryCounterVerifyOTP"] = gblRetryCountRequestOTP;
        inputParam["password"] = frmIBBillPaymentConfirm.txtotp.text;
        //inputParam["verifyPwd_appID"] = appConfig.appId;
        //inputParam["verifyPwd_channel"] = "IB";
        
    } //else close
	//MIB-4884-Allow special characters for My Note and Note to recipient field
	var myNote = frmIBBillPaymentConfirm.lblMyNoteVal.text;
	
    var billerMethod = gblBillerMethod;
    var transferFee = frmIBBillPaymentConfirm.lblFeeVal.text;
    var transferAmount = parseFloat(removeCommos(frmIBBillPaymentConfirm.lblAmtVal.text));
    var invoice;
    if (gblreccuringDisablePay == "Y") {
        invoice = frmIBBillPaymentConfirm.lblBPRef2Val.text;
    } else {
        invoice = ""; //workaround
    }
    //billerMethod=0;
    inputParam["billerMethod"] = billerMethod;
    inputParam["isPayNow"] = gblPaynow;
    inputParam["compCode"] = gblCompCode;
    inputParam["toAccountKey"] = gblToAccountKey;
    
    inputParam["isPayPremium"] = gblPayPremium;
    inputParam["policyNumber"] = gblPolicyNumber;
    
    if (gblPaynow) {
    inputParam["NotificationAdd_source"] = "billpaymentnow";
        if (billerMethod == 0) {
            inputParam["billpaymentAdd_fromAcctNo"] = frmIBBillPaymentConfirm.lblFromAccountNo.text;
            inputParam["billpaymentAdd_toAcctNo"] = gblToAccountKey;
            inputParam["billpaymentAdd_frmFiident"] = frmIBBillPaymentLP.lblDummy.text; 
            inputParam["billpaymentAdd_transferAmt"] = transferAmount;
            inputParam["billpaymentAdd_billPmtFee"] = parseFloat(transferFee);
            inputParam["billpaymentAdd_pmtRefIdent"] = frmIBBillPaymentConfirm.lblBPRef1Val.text;;
            inputParam["billpaymentAdd_invoiceNum"] = invoice;
            inputParam["billpaymentAdd_EPAYCode"] = "";
            inputParam["billpaymentAdd_CompCode"] = gblCompCode;
            inputParam["billpaymentAdd_billPay"] = "billPay";
            inputParam["UsageLimit"] = "0";
        }
        if (billerMethod == 1) {
            var bankrefID = "";
            var ref1 = "";
            var ref2 = "";
            var ref3 = "";
            var ref4 = "";
            if (gblCompCode == "2151" || gblCompCode == "0131" || gblCompCode == "2135" || gblCompCode == "0328" || gblCompCode == "0254" || gblCompCode == "0185") {
                bankrefID = BankRefId;
            } else if (gblCompCode == "0014" || gblCompCode == "0249" || gblCompCode == "2270") {
                bankrefID = TranId;
            } else {
                bankrefID = "";
            }
            if (gblCompCode == "2151" || gblCompCode == "0131" || gblCompCode == "2135" || gblCompCode == "0328" || gblCompCode == "0254" || gblCompCode == "0185") {
                ref1 = gblRef1;
                ref2 = gblRef2;
                ref3 = gblRef3;
                ref4 = gblRef4;
            } else {
                ref1 = "";
                ref2 = "";
                ref3 = "";
                ref4 = "";
            }
            inputParam["onlinepaymentAdd_TrnId"] = TranId;
            inputParam["onlinepaymentAdd_BankId"] = "011";
            inputParam["onlinepaymentAdd_BranchId"] = "0001";
            inputParam["onlinepaymentAdd_BankRefId"] = bankrefID;
            inputParam["onlinepaymentAdd_Amt"] = transferAmount;
            inputParam["onlinepaymentAdd_Ref1"] = ref1; //frmIBBillPaymentLP.lblBPRef1Val.text;
            inputParam["onlinepaymentAdd_Ref2"] = ref2; //frmIBBillPaymentLP.lblRefVal2.text;
            inputParam["onlinepaymentAdd_Ref3"] = ref3;
            inputParam["onlinepaymentAdd_Ref4"] = ref4;
            inputParam["onlinepaymentAdd_MobileNumber"] = frmIBBillPaymentLP.lblBPRef1Val.text;
            inputParam["onlinepaymentAdd_fromAcctNo"] = frmIBBillPaymentConfirm.lblFromAccountNo.text;
            inputParam["onlinepaymentAdd_toAcctNo"] = gblToAccountKey;
            inputParam["onlinepaymentAdd_frmFiident"] = frmIBBillPaymentLP.lblDummy.text;
            inputParam["onlinepaymentAdd_transferAmt"] = transferAmount;
            inputParam["onlinepaymentAdd_channelName"] = "IB";
            inputParam["onlinepaymentAdd_billPmtFee"] = parseFloat(transferFee).toFixed(2); //.substring(0, transferFee.indexOf(" "));
            inputParam["onlinepaymentAdd_pmtRefIdent"] = frmIBBillPaymentConfirm.lblBPRef1Val.text;
            
          if(gblCompCode == "0835" )
		{
			invoice = "01"+TranId;
		} else if(gblCompCode == "2136" || gblCompCode == "0472" ){
			invoice = TranId;
		} else if(gblCompCode == "0185" || gblCompCode == "0254"|| gblCompCode == "0328"|| gblCompCode == "2135"|| gblCompCode == "0131" ){
			invoice = gblRef2;
		} else if(gblCompCode == "2533"){
			invoice = TranId+gblSegBillerData["billerServiceType"]+gblOnlinePmtType+gblSegBillerData["billerTransType"];
			inputParam["onlinepaymentAdd_Ref3"]=parseFloat(transferFee);
			inputParam["onlinepaymentAdd_BankRefId"] = frmIBBillPaymentConfirm.lblTRNVal.text;
		}else {
			if (gblreccuringDisablePay == "Y") {
                invoice = frmIBBillPaymentLP.lblRefVal2.text;//no ref 2 for topup?
            } else {
            	invoice = ""; 
            }
		}
            inputParam["onlinepaymentAdd_invoiceNum"] = invoice;
            inputParam["onlinepaymentAdd_EPAYCode"] =  "";//"EPYS";
            inputParam["onlinepaymentAdd_compCode"] = gblCompCode;
            inputParam["onlinepaymentAdd_InterRegionFee"] = "0.00";
            inputParam["onlinepaymentAdd_billPay"] = "billPay";
        }
        if (billerMethod == 2 || billerMethod == 3 || billerMethod == 4) {
            if (gblFullPayment) {
                transferAmt = frmIBBillPaymentLP.lblFullPayment.text;
                if (kony.string.containsChars(transferAmt, ["."])) {
                    transferAmt = transferAmt;
                } else {
                    transferAmt = transferAmt + ".00";
                }
            } else {
                transferAmt = frmIBBillPaymentLP.txtBillAmt.text;
                if (kony.string.containsChars(transferAmt, ["."])) {
                    transferAmt = transferAmt;
                } else {
                    transferAmt = transferAmt + ".00";
                }
            }
			var fee=parseFloat(removeCommos(frmIBBillPaymentConfirm.lblFeeVal.text))
			inputParam["transferAdd_xferFee"] = fee;
            inputParam["transferAdd_fromAcctNo"] = frmIBBillPaymentConfirm.lblFromAccountNo.text;
            inputParam["transferAdd_fromAcctTypeValue"] = frmIBBillPaymentConfirm.lblAcctType.text;
			var toAcctNo = removeHyphenIB(frmIBBillPaymentLP.lblBPRef1Val.text);
            inputParam["transferAdd_toAcctNo"] = toAcctNo;
            inputParam["transferAdd_transferAmt"] = parseFloat(removeCommos(transferAmt)).toFixed(2);
            inputParam["transferAdd_orgin"] = "billpay";
            inputParam["transferAdd_billerMethod"] = billerMethod;
            inputParam["transferAdd_ref2"] = frmIBBillPaymentConfirm.lblBPRef2Val.text;
            inputParam["transferAdd_CompCode"] = gblCompCode;
        }
    } else {
       inputParam["NotificationAdd_source"] = "FutureBillPaymentAndTopUp";
        var amount = parseFloat(removeCommos(frmIBBillPaymentConfirm.lblAmtVal.text)).toFixed(2);
		var dueDate = frmIBBillPaymentConfirm.lblStartOnValue.text.trim();
		var tranId = frmIBBillPaymentConfirm.lblTRNVal.text;
        var pmtMethod = "BILL_PAYMENT"
        if(gblreccuringDisablePay == "Y"){
        	 inputParam["doPmtAdd_pmtRefNo2"] = frmIBBillPaymentConfirm.lblBPRef2Val.text;
        }
        if (billerMethod == 1) {
            pmtMethod = "ONLINE_PAYMENT";
            inputParam["doPmtAdd_PmtMiscType"] = "MOBILE";
            inputParam["doPmtAdd_MiscText"] = frmIBBillPaymentConfirm.lblBPRef1Val.text;
        } else if (billerMethod == 2 || billerMethod == 3 || billerMethod == 4) {
            pmtMethod = "INTERNAL_PAYMENT"
        }
        if (timesRepeat == "1") {
            inputParam["doPmtAdd_NumInsts"] = frmIBBillPaymentConfirm.lblExecuteValue.text;
        } else if (timesRepeat == "2") {
            inputParam["doPmtAdd_FinalDueDt"] = changeDateFormatForService(frmIBBillPaymentConfirm.lblEnDONValue.text);
        } else if (timesRepeat == "0") {
            //inputParam["NumInsts"] = "99";
        }
        
        inputParam["doPmtAdd_addBillerNickname"] = frmIBBillPaymentLP.txtScheduleNickname.text;  
        inputParam["doPmtAdd_rqUID"] = "";
        inputParam["doPmtAdd_toBankId"] = "011";
        inputParam["doPmtAdd_amt"] = amount;
        inputParam["doPmtAdd_billerMethod"] = billerMethod;
        inputParam["doPmtAdd_fromAcct"] = frmIBBillPaymentConfirm.lblFromAccountNo.text; //removeHyphenIB(frmIBBillPaymentConfirm.lblFromAccountNo.text);
        inputParam["doPmtAdd_fromAcctType"] = frmIBBillPaymentConfirm.lblAcctType.text;
    	inputParam["doPmtAdd_toAcct"] = gblToAccountKey;
    	inputParam["doPmtAdd_custPayeeId"] = gblBillerID;
        inputParam["doPmtAdd_fromAcctNickName"] = frmIBBillPaymentConfirm.lblFromAccName.text;
		//inputParam["doPmtAdd_toNickName"] = frmIBBillPaymentConfirm.lblBPBillerName.text;
        inputParam["doPmtAdd_toNickName"] = frmIBBillPaymentConfirm.lblBPBillerName.text; //Added for CR14
        inputParam["doPmtAdd_dueDate"] = changeDateFormatForService(dueDate);
        inputParam["doPmtAdd_pmtRefNo1"] = frmIBBillPaymentConfirm.lblBPRef1Val.text;
        
        inputParam["doPmtAdd_channelId"] = "IB";
        inputParam["doPmtAdd_curCode"] = "THB";
        inputParam["doPmtAdd_memo"] = myNote;
        inputParam["doPmtAdd_pmtMethod"] = pmtMethod;
        inputParam["doPmtAdd_extTrnRefId"] = "SB" + kony.string.sub(tranId, 2, tranId.length);
        
        var frequency = frmIBBillPaymentConfirm.lblRepeatValue.text;
        inputParam["doPmtAdd_freq"] = frequency;
        if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Daily"))) {
            inputParam["doPmtAdd_freq"] = "Daily";
            inputParam["NotificationAdd_recurring"] = "keyDaily";
        }
        if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Weekly"))) {
            inputParam["doPmtAdd_freq"] = "Weekly";
            inputParam["NotificationAdd_recurring"] = "keyWeekly";
        }
        if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Monthly"))) {
            inputParam["doPmtAdd_freq"] = "Monthly";
            inputParam["NotificationAdd_recurring"] = "keyMonthly";
        }
        if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Yearly")) || kony.string.equalsIgnoreCase(frequency, "Yearly")) {
            inputParam["doPmtAdd_freq"] = "Annually";
            inputParam["NotificationAdd_recurring"] = "keyYearly";
        }
        if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyOnce")) || kony.string.equalsIgnoreCase(frequency, "Once")) {
        	inputParam["doPmtAdd_freq"] = "once";
        	inputParam["NotificationAdd_recurring"] = "keyOnce";
        }	
        inputParam["doPmtAdd_desc"] = "";
    }
    inputParam["NotificationAdd_channelId"] = "Internet Banking";
    inputParam["NotificationAdd_customerName"] = gblCustomerName;;
    inputParam["NotificationAdd_fromAcctNick"] = frmIBBillPaymentConfirm.lblFromAccName.text;
    inputParam["NotificationAdd_fromAcctName"] = frmIBBillPaymentConfirm.lblCustomerName.text;
    inputParam["NotificationAdd_billerNick"] = frmIBBillPaymentConfirm.lblBPBillerName.text;
    inputParam["NotificationAdd_billerName"] = gblBillerCompCodeEN;
    inputParam["NotificationAdd_billerNameTH"] = gblBillerCompCodeTH;
 	
 	inputParam["NotificationAdd_ref1EN"] = gblRef1LblEN+" : " + frmIBBillPaymentConfirm.lblBPRef1Val.text;
    inputParam["NotificationAdd_ref1TH"] = gblRef1LblTH+" : " + frmIBBillPaymentConfirm.lblBPRef1Val.text;

    if (frmIBBillPaymentConfirm.hbxRef2.isVisible) {
        ref2 = frmIBBillPaymentConfirm.lblBPRef2Val.text;
        inputParam["NotificationAdd_ref2EN"] = gblRef2LblEN+" : " + ref2;
        inputParam["NotificationAdd_ref2TH"] = gblRef2LblTH+" : " + ref2;
    } else {
        inputParam["NotificationAdd_ref2EN"] = "";
        inputParam["NotificationAdd_ref2TH"] = "";
    }
    if(gblPaynow){
	inputParam["NotificationAdd_source"]="billpaymentnow";
	}
	else{
	inputParam["NotificationAdd_source"]="FutureBillPaymentAndTopUp";
	
	inputParam["NotificationAdd_startDt"] = frmIBBillPaymentConfirm.lblStartOnValue.text;
	//inputParam["NotificationAdd_initiationDt"] = frmIBBillPaymentConfirm.lblDateVal.text; 
	inputParam["NotificationAdd_initiationDt"] = frmIBBillPaymentCompletenow.lblePayDate.text;
	inputParam["NotificationAdd_todayTime"] = "23:59:59 "; 
	
	
	//inputParam["NotificationAdd_recurring"] = frmIBBillPaymentConfirm.lblRepeatValue.text 
	inputParam["NotificationAdd_endDt"] = frmIBBillPaymentConfirm.lblEnDONValue.text;
	inputParam["NotificationAdd_mynote"] = myNote;
	
			if(frmIBBillPaymentConfirm.lblEnDONValue.text == "-") {
				inputParam["NotificationAdd_PaymentSchedule"] = frmIBBillPaymentConfirm.lblStartOnValue.text + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + frmIBBillPaymentConfirm.lblRepeatValue.text;
			}else{
				inputParam["NotificationAdd_PaymentSchedule"] = frmIBBillPaymentConfirm.lblStartOnValue.text + " " + kony.i18n.getLocalizedString("keyTo") + " " +  frmIBBillPaymentConfirm.lblEnDONValue.text + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + frmIBBillPaymentConfirm.lblRepeatValue.text + " " + kony.i18n.getLocalizedString("keyFor") + " " + frmIBBillPaymentConfirm.lblExecuteValue.text +" "+kony.i18n.getLocalizedString("keyTimesIB");
				inputParam["NotificationAdd_endDt"] = inputParam["NotificationAdd_endDt"] + " - " + frmIBBillPaymentConfirm.lblExecuteValue.text +" "+kony.i18n.getLocalizedString("keyTimesIB");
			}
	}
	inputParam["NotificationAdd_amount"] = frmIBBillPaymentConfirm.lblAmtVal.text;
    inputParam["NotificationAdd_fee"] = frmIBBillPaymentConfirm.lblFeeVal.text;
    inputParam["NotificationAdd_refID"] = frmIBBillPaymentConfirm.lblTRNVal.text;
    inputParam["NotificationAdd_memo"] = myNote;
    inputParam["NotificationAdd_deliveryMethod"] = "Email";
    inputParam["NotificationAdd_noSendInd"] = "0";
    inputParam["NotificationAdd_notificationType"] = "Email";
    inputParam["NotificationAdd_Locale"] = kony.i18n.getCurrentLocale();
    inputParam["NotificationAdd_notificationSubject"] = "billpay";
    inputParam["NotificationAdd_notificationContent"] = "billPaymentDone";
    inputParam["NotificationAdd_appID"] = appConfig.appId;
	
	var availableBal = parseFloat(removeCommos(frmIBBillPaymentConfirm.lblBalance.text));
	inputParam["finActivityLog_availableBal"] = availableBal;
	inputParam["finActivityLog_finTxnRefId"] = frmIBBillPaymentConfirm.lblTRNVal.text;
	inputParam["finActivityLog_channelId"] = "01";
	var billerName = frmIBBillPaymentConfirm.lblBPBillerName.text;
	billerName = billerName.substring(0, billerName.lastIndexOf("("));
 	inputParam["finActivityLog_billerRef1"] = frmIBBillPaymentConfirm.lblBPRef1Val.text;
 	inputParam["finActivityLog_fromAcctName"] = frmIBBillPaymentConfirm.lblCustomerName.text;
 	inputParam["finActivityLog_fromAcctNickname"] = frmIBBillPaymentConfirm.lblFromAccName.text;
 	inputParam["finActivityLog_toAcctName"] =  billerName;//
 	inputParam["finActivityLog_toAcctNickname"] = frmIBBillPaymentConfirm.lblBPBillerName.text;
	inputParam["finActivityLog_billerRef2"]= frmIBBillPaymentConfirm.lblBPRef2Val.text;
    //inputParam["activityLog_appID"] = appConfig.appId;
    //inputParam["activityLog_channel"] = "IB";
		var activityFlexValues5 = "";
		if (gblreccuringDisablePay == "Y") {
	        activityFlexValues5 = frmIBBillPaymentConfirm.lblBPRef2Val.text;
	    } else {
	        activityFlexValues5 = ""; 
	    }
		var activityFlexValues1 = billerName;
		var activityFlexValues2 = removeHyphenIB(frmIBBillPaymentConfirm.lblFromAccountNo.text);
		var activityFlexValues3 = frmIBBillPaymentConfirm.lblBPRef1Val.text;
		var amt = parseFloat(removeCommos(frmIBBillPaymentConfirm.lblAmtVal.text));
		var fee = parseFloat(removeCommos(frmIBBillPaymentConfirm.lblFeeVal.text));
		var activityFlexValues4 = amt.toFixed(2)+ "+" + fee.toFixed(2);
	inputParam["activityLog_channelId"] = "01";
	inputParam["activityLog_activityFlexValues1"] = activityFlexValues1 ;
	inputParam["activityLog_activityFlexValues2"] = activityFlexValues2;
	inputParam["activityLog_activityFlexValues3"] = activityFlexValues3;
	inputParam["activityLog_activityFlexValues4"] = activityFlexValues4;
	inputParam["activityLog_activityFlexValues5"] = activityFlexValues5;
    inputParam["serviceID"] = "ConfirmBillPaymentService";
    frmIBBillPaymentConfirm.txtotp.text = "";
    inputParam["finActivityLog_billerCustomerName"] =  frmIBBillPaymentConfirm.lblBPBillerName.text;//
    
    invokeServiceSecureAsync("ConfirmBillPaymentService", inputParam, callBackCompositeBillPay);
}


function callBackCompositeBillPay(status, resulttable) {
    
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            //gblVerifyOTPCounter = "0";
            //callBillPaymentServiceOnConfirmClickIB()
            dismissLoadingScreenPopup();
            
            gblRetryCountRequestOTP = 0;
            frmIBBillPaymentCompletenow.lblBBPaymentValue.text = commaFormatted(resulttable["availBal"])+ kony.i18n.getLocalizedString("currencyThaiBaht");
			if (gblPaynow){
			  frmIBBillPaymentCompletenow.lblePayDate.text = resulttable["paymentServerDate"]; 
			  populateBillPaymentCompleteScreen(resulttable);
			}else{
			 gblCurrentBillPayTime = resulttable["currentTime"];
             populateBillPaymentCompleteScreen(resulttable);
		   }
        } else if (resulttable["opstatus"] == 8005) {
      		  frmIBBillPaymentConfirm.txtotp.text = "";
      		  frmIBBillPaymentConfirm.tbxToken.text = "";
      		  dismissLoadingScreenPopup();
            if (resulttable["errCode"] == "VrfyOTPErr00001") {
                gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
                dismissLoadingScreenPopup();
                //alert("" + kony.i18n.getLocalizedString("invalidOTP"));//commented by swapna
                    frmIBBillPaymentConfirm.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");//kony.i18n.getLocalizedString("invalidOTP"); //
                    frmIBBillPaymentConfirm.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
                    frmIBBillPaymentConfirm.hbxOTPincurrect.isVisible = true;
                    frmIBBillPaymentConfirm.hbox476047582127699.isVisible = false;
                    frmIBBillPaymentConfirm.hbxOTPsnt.isVisible = false;
                    frmIBBillPaymentConfirm.txtotp.text = "";
                    if(gblTokenSwitchFlag == true && gblSwitchToken == false){
                       frmIBBillPaymentConfirm.tbxToken.setFocus(true);
                     }else{
                       frmIBBillPaymentConfirm.txtotp.setFocus(true);
                     }
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00002") {
                dismissLoadingScreenPopup();
                //alert("" + kony.i18n.getLocalizedString("ECVrfyOTPErr"));
                //startRcCrmUpdateProfilBPIB("04");
				handleOTPLockedIB(resulttable);
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00005") {
                dismissLoadingScreenPopup();
                //alert("" + kony.i18n.getLocalizedString("KeyTokenSerialNumError")); //commented by swapna
                //alert("" + kony.i18n.getLocalizedString("invalidOTP"));
                    frmIBBillPaymentConfirm.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");//kony.i18n.getLocalizedString("invalidOTP"); //
                    frmIBBillPaymentConfirm.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
                    frmIBBillPaymentConfirm.hbxOTPincurrect.isVisible = true;
                    frmIBBillPaymentConfirm.hbox476047582127699.isVisible = false;
                    frmIBBillPaymentConfirm.hbxOTPsnt.isVisible = false;
                    frmIBBillPaymentConfirm.txtotp.text = "";
                    if(gblTokenSwitchFlag == true && gblSwitchToken == false){
                      frmIBBillPaymentConfirm.tbxToken.setFocus(true);
                    }else{
                      frmIBBillPaymentConfirm.txtotp.setFocus(true);
                    }
                    
                    
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00006") {
            	dismissLoadingScreenPopup();
                gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
                alert("" + resulttable["errMsg"]);
                return false;
            }else if (resulttable["errCode"] == "GenOTPRtyErr00001") {
                    dismissLoadingScreenPopup();
                    showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                    return false;
           	}else if (resulttable["errCode"] == "VrfyOTPErr00004") {
                dismissLoadingScreenPopup();
                alert("" + resulttable["errMsg"]);
                return false;
            }else{
                    frmIBBillPaymentConfirm.lblOTPinCurr.text = " ";//kony.i18n.getLocalizedString("invalidOTP"); //
                    frmIBBillPaymentConfirm.lblPlsReEnter.text = " "; 
                    frmIBBillPaymentConfirm.hbxOTPincurrect.isVisible = false;
                    frmIBBillPaymentConfirm.hbox476047582127699.isVisible = true;
                    frmIBBillPaymentConfirm.hbxOTPsnt.isVisible = true;
                    frmIBBillPaymentConfirm.txtotp.setFocus(true); 
                    frmIBBillPaymentConfirm.tbxToken.setFocus(true);
             	dismissLoadingScreenPopup();
              	alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
                return false;
            }
        } else {
            dismissLoadingScreenPopup();
            if(resulttable["opstatus"] == 1 && resulttable["errCode"] == "ERRDUPTR0001"){
            	showAlertWithHandler(kony.i18n.getLocalizedString("keyErrDuplicatemt"), kony.i18n.getLocalizedString("info"), billpaymentExecutionErrCallBackIB);
           		return;
            }
            alert("" + resulttable["errMsg"]);
            if(resulttable["isServiceFailed"] == "true"){
            callBillPaymentCustomerAccountServiceIB();
            }
        }
    } else {
        if (status == 300) {
            dismissLoadingScreenPopup();
            alert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"));
        }
    }
}


function billpaymentExecutionErrCallBackIB(){
	callBillPaymentCustomerAccountServiceIB();
}

var flagSearchBP;
var flagCatBP;

function createSegmentRecordIB(record,icon) {
	var accVal = record.accId;
	var accValLength = accVal.length;
	var remFee;
	var remFeeValue;
	var accountName = record["acctNickName"];
	var accountNickNameEN = record["acctNickNameEN"]
	var accountNickNameTH = record["acctNickNameTH"]
	if (accountName == "" || accountName == undefined) {
		var acctnum = accVal;
		var acctnumNew = acctnum.substring(acctnum.length -4, acctnum.length);
		var accNewNameEN = record["productNmeEN"];
		var	accNewNameTH = record["productNmeTH"];
			
			if(accNewNameEN.length>15) {
				accNewNameEN = accNewNameEN.substring(0, 15);
			}
			
			if(accNewNameTH.length>15) {
				accNewNameTH = accNewNameTH.substring(0, 15);
			}
				
		if(kony.i18n.getCurrentLocale()=="en_US"){
			accountName = accNewNameEN + " " + acctnumNew;
		}
		else {
			accountName = accNewNameTH + " " + acctnumNew;
		}
			accountNickNameEN = accNewNameEN + " " + acctnumNew;
			accountNickNameTH = accNewNameTH + " " + acctnumNew;
	}
	accVal = accVal.substring((accValLength - 10), (accValLength - 7)) + "-" + accVal.substring((accValLength - 7), (
		accValLength - 6)) + "-" + accVal.substring((accValLength - 6), (accValLength - 1)) + "-" + accVal.substring((
		accValLength - 1), accValLength);
		if(gblSMART_FREE_TRANS_CODES.indexOf(record.productID) >= 0 ){
		remFeeValue=record.remainingFee;
		remFee=kony.i18n.getLocalizedString("keylblNoOfFreeTrans");
		}
		else{
		remFeeValue="";
		remFee=""
		}
		var productName;
		if(kony.i18n.getCurrentLocale()=="en_US"){
			productName = record.productNmeEN 
		}
		else {
		productName = record.productNmeTH 
		}
	var temp = [{
		lblProduct: kony.i18n.getLocalizedString("Product"),
		lblProductVal: productName,
		lblACno: kony.i18n.getLocalizedString("AccountNo"),
		img1: icon,
		lblCustName: accountName,
		lblAccountNickNameEN: accountNickNameEN,
		lblAccountNickNameTH: accountNickNameTH,
		lblBalance: commaFormatted(record.availableBal)+ " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
		lblActNoval: accVal,
		lblRemFee:remFee ,
		lblRemFeeval:remFeeValue,
		//lblActNoval:addHyphenIB(record.accId),
		lblDummy: record.fiident,
		lblSliderAccN1: kony.i18n.getLocalizedString("Product") + ".:",
		lblSliderAccN2: record.accType,
		remainingFee: record.remainingFee,
		prodCode: record.productID,
		AccountName : record.accountName,
		lblProductValEN: record.productNmeEN, 
		lblProductValTH: record.productNmeTH 
    }]
	return temp;
}

function onBillPaymentNextIB() {
   var currForm = kony.application.getCurrentForm();
	

	if(gblSwitchToken == false && gblTokenSwitchFlag == false){
		checkBillPaymentTokenFlag();
	} 
	if(gblTokenSwitchFlag == true && gblSwitchToken == false){
	
			frmIBBillPaymentConfirm.hbxTokenEntry.setVisibility(true);
			frmIBBillPaymentConfirm.hbxOTPEntry.setVisibility(false);
			frmIBBillPaymentConfirm.label476047582115288.text=kony.i18n.getLocalizedString("emptyToken");
		
	} else if(gblTokenSwitchFlag ==false && gblSwitchToken == true) {
		    frmIBBillPaymentConfirm.hbxTokenEntry.setVisibility(false);
		    frmIBBillPaymentConfirm.hbxOTPEntry.setVisibility(true);
		    frmIBBillPaymentConfirm.label476047582115288.text=kony.i18n.getLocalizedString("keyotpmsgreq");
	}

}
function checkBillPaymentTokenFlag() {
		var inputParam = [];
		inputParam["crmId"] = gblcrmId;
		showLoadingScreen();
		invokeServiceSecureAsync("tokenSwitching", inputParam, ibBillPaymentTokenFlagCallbackfunction);
}
function ibBillPaymentTokenFlagCallbackfunction(status,callbackResponse){
		var currForm = kony.application.getCurrentForm().id;
		
		if(status==400){
				if(callbackResponse["opstatus"] == 0){
					dismissLoadingScreen();
					if(callbackResponse["deviceFlag"].length==0){
						
						//return
					}
					if ( callbackResponse["deviceFlag"].length > 0 ){
							var tokenFlag = callbackResponse["deviceFlag"][0]["TOKEN_DEVICE_FLAG"];
							var mediaPreference = callbackResponse["deviceFlag"][0]["MEDIA_PREFERENCE"];
							var tokenStatus = callbackResponse["deviceFlag"][0]["TOKEN_STATUS_ID"];
							
							if ( tokenFlag=="Y" && ( mediaPreference == "Token" || mediaPreference == "TOKEN") && tokenStatus=='02'){
										frmIBBillPaymentConfirm.hbxTokenEntry.setVisibility(true);
										frmIBBillPaymentConfirm.hbxOTPEntry.setVisibility(false);
										frmIBBillPaymentConfirm.label476047582115288.text=kony.i18n.getLocalizedString("emptyToken");
								gblTokenSwitchFlag = true;
							} else {
									    frmIBBillPaymentConfirm.hbxTokenEntry.setVisibility(false);
									    frmIBBillPaymentConfirm.hbxOTPEntry.setVisibility(true);
									    frmIBBillPaymentConfirm.label476047582115288.text=kony.i18n.getLocalizedString("keyotpmsgreq");
								gblTokenSwitchFlag = false;
							}
						}
					 else
					{
					 
						    frmIBBillPaymentConfirm.hbxTokenEntry.setVisibility(false);
						    frmIBBillPaymentConfirm.hbxOTPEntry.setVisibility(true);
						    frmIBBillPaymentConfirm.label476047582115288.text=kony.i18n.getLocalizedString("keyotpmsgreq");
					 }
						
				}
		}
		
} 


function onClickHide(){
	var currForm = kony.application.getCurrentForm();
	if (currForm.hbxAmountDetailsMEA.isVisible){
		currForm.hbxAmountDetailsMEA.setVisibility(false);
		currForm.lnkHide.text=kony.i18n.getLocalizedString("show")
	}else{
		currForm.hbxAmountDetailsMEA.setVisibility(true);
		currForm.lnkHide.text=kony.i18n.getLocalizedString("Hide")
	}
}
