function onTextChangeToCitizenIDP2P(txt){
	if (txt == null) return false;
	var numChars = txt.length;
	var temp = "";
	var i, txtLen = numChars;
	var currLen = numChars;
	if (gblPrevLen < currLen) {
		for (i = 0; i < numChars; ++i) {
			if (txt[i] != '-') {
				temp = temp + txt[i];
			} else {
				txtLen--;
			}
		}
		var iphenText = "";
		for (i = 0; i < txtLen; i++) {
			iphenText += temp[i];
			if (i == 0 || i == 4 || i == 9 || i == 11) {
				iphenText += '-';
			}
		}
		frmTransferLanding.txtCitizenID.text = iphenText;
	}
	gblPrevLen = currLen;
	
	displayP2PITMXBank(false);
	var enteredAmt = frmTransferLanding.txtTranLandAmt.text;
	if(frmTransferLanding.hbxRecievedBy.isVisible || (isNotBlank(enteredAmt) && parseFloat(enteredAmt, 10) > 0 )) {
		displayAmountTextBox(true);
	}else{
		displayAmountTextBox(false);
	}
	
	var citizenID = frmTransferLanding.txtCitizenID.text;
	if(isNotBlank(citizenID)){
		citizenID = removeHyphenIB(citizenID);
		
		if(!isNotBlank(frmTransferLanding.lblMobileNoTemp.text)){
			frmTransferLanding.lblMobileNoTemp.text = "";
		}
		if(citizenID.length == 13){
			frmTransferLanding.lblRecipientName.text = "";
			if(!kony.string.equalsIgnoreCase(frmTransferLanding.lblMobileNoTemp.text, frmTransferLanding.txtCitizenID.text)){
				isOwnAccountP2P = false;
				//validate mobile number
				if (!checkCitizenID(citizenID)){
					showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_P2PkeyCIisnotvalid"), 
						kony.i18n.getLocalizedString("info"), callBackCitizenIDFieldsP2P);
				}else{
					if(isNotBlank(enteredAmt) && parseFloat(enteredAmt, 10) > 0 ){
						invokeServiceP2PITMX(true);
					}else{
						displayAmountTextBox(true);
						setFocusAmountP2P();
					}
				}
			}else{
				displayP2PITMXBank(true);
				setFocusAmountP2P();
			}
		}else{
			clearNotifyRecipientfields();
		}
	}
}

function onDoneValidateCITransfer(citizenID) {
	
 	citizenID = removeHyphenIB(citizenID);
	if(!isNotBlank(citizenID)){
		showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_P2PTRErr_ToCI"), 
			kony.i18n.getLocalizedString("info"), callBackCitizenIDFieldsP2P);
		return false;
	}else{
		if (!checkCitizenID(citizenID)){
			showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_P2PkeyCIisnotvalid"), 
				kony.i18n.getLocalizedString("info"), callBackCitizenIDFieldsP2P);
			return false;
		}
	}
	
	return true;
}

function callBackCitizenIDFieldsP2P(){
	frmTransferLanding.txtCitizenID.text = "";
	frmTransferLanding.txtCitizenID.setFocus(true);
}

function displayNotEligibleCitizenID(){
	var errorText = "";
	if(ITMX_TRANSFER_ENABLE == "true"){ 
		errorText = kony.i18n.getLocalizedString("MIB_P2PkeyErrCINotRegisTurnOn");
	} else {
		errorText = kony.i18n.getLocalizedString("MIB_P2PkeyErrCINotRegisTurnOff");
	}
	errorText = errorText.replace("{citizenID}", frmTransferLanding.txtCitizenID.text);
	showAlertWithCallBack(errorText, kony.i18n.getLocalizedString("info"), callBackCitizenIDFieldsP2P);
}

function onBeginEditCitizenID(){
	var citiID = removeHyphenIB(frmTransferLanding.txtCitizenID.text);
	var maxTextLength = 13;
	if(isNotBlank(citiID)){
		if(citiID.length < maxTextLength){
			clearNotifyRecipientfields();
		}
	}
}