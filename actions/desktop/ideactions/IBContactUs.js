function contactUsOptionsIB(){
	var selectIndex = frmIBContactUs.cmbCnt.selectedKey;
	selectedCNTIndex = selectIndex;
	if ( selectIndex == 5) {
			frmIBContactUs.hbxCNTMsg.setVisibility(true);
			frmIBContactUs.hbxRadio.setVisibility(true);
	} else {
			frmIBContactUs.hbxCNTMsg.setVisibility(false);
			frmIBContactUs.hbxRadio.setVisibility(false);	
	}
	frmIBContactUs.tbxMsg.text="";
	frmIBContactUs.btnMobilePhone.skin="btnRadio";
	var isIE8 = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
	if(!isIE8){
		frmIBContactUs.btnMobilePhone.focusSkin="btnRadioFoc";
		frmIBContactUs.btnemail.focusSkin="btnRadioFoc"; 
	}
	frmIBContactUs.btnemail.skin="btnRadioFoc";
}

var contactMode ="";
var cmbValue = "";
var subject = "";
var sourceType = "";
var message = "";
var locale="";
var cSource = "contactUsEmail";
function contactUsEmainNotificationIB(){
	if ( frmIBContactUs.cmbCnt.selectedKey == 1 ){
			alert(kony.i18n.getLocalizedString("cntoption"));
 			return false;	
	}
	else if( frmIBContactUs.tbxMsg.text == null || frmIBContactUs.tbxMsg.text.trim() == "" ){
			alert(kony.i18n.getLocalizedString("cntText"));
			return false;
	}
	
	var key = frmIBContactUs.cmbCnt.selectedKey;
	
	if ( key == 2 ) {
		subject = kony.i18n.getLocalizedString("Cnt_IB");
		sourceType = "IB";	
	} else if ( key == 3 ){
		subject = kony.i18n.getLocalizedString("Cnt_DA");
		sourceType = "deposit";
	} else if ( key == 4 ) {
		subject = kony.i18n.getLocalizedString("Cnt_LP");
		sourceType = "lending"
	} else if( key == 5) {
		subject = kony.i18n.getLocalizedString("Cnt_Compliants");
		sourceType = "complaint"
	}
		if(frmIBContactUs.btnMobilePhone.skin=="btnRadioFoc"){
				contactMode = "Mobile Phone";
		} else if(frmIBContactUs.btnemail.skin=="btnRadioFoc"){
				contactMode = "EMAIL"
		}
	
	cmbValue = frmIBContactUs.cmbCnt.selectedKeyValue;
	
	
	message = frmIBContactUs.tbxMsg.text;
	locale=kony.i18n.getCurrentLocale();
	var inputParams = {
		notificationType:"Email",
		notificationSubject:subject,
		recipientNickName:gblCustomerName,
		//source:cSource,
		Locale:locale,
		//channel:"IB-INQ",
		//channelName:"IB-INQ",
		message:message,
		type:"User",
		custNAME:gblCustomerName
	};
	showLoadingScreenPopup();
    invokeServiceSecureAsync("ContactUsNotification", inputParams, sendIBContactUsNotificationCallBack);
   }
//function crmProfileEmailCallBackIB(status, callBackResponse){
//	
//	
//
//	if (status == 400) {
//		if (callBackResponse["opstatus"] == 0) {
//				
//				
//				gblEmailId=callBackResponse["emailAddr"];
//		}
//	}
//
//
//}
function sendIBContactUsNotificationCallBack(status,callbackResponse){
	if(status == 400){
			if (callbackResponse["opstatus"] == 0) {
				dismissLoadingScreenPopup(); 
				frmIBContactUs.hbxCntImage.setVisibility(true);
				frmIBContactUs.hbxCntCmplt.setVisibility(false);
				frmIBContactUs.hbxFeedComplt.setVisibility(false);
				
				frmIBContactUs.hbox98089341265246.setVisibility(true);
			    frmIBContactUs.hbox98089341292096.setVisibility(false);
			    frmIBContactUs.hbxContactIB.setVisibility(false);
			    frmIBContactUs.hbox506459299512370.setVisibility(false);
			    frmIBContactUs.label506459299571357.setVisibility(false);
				frmIBContactUs.label98089341265252.setVisibility(true);
				var emailId = callbackResponse["emailId"];
				if(cmbValue[1] == kony.i18n.getLocalizedString("Cnt_Compliants")){
						if(contactMode.trim() == "Mobile Phone"){
							if(gblPHONENUMBER != null && gblPHONENUMBER.length >= 10){
							frmIBContactUs.label98089341265256.text =kony.i18n.getLocalizedString("keyThankyou")+" "+kony.i18n.getLocalizedString("keyTMBCompleteData")+" "+ "xxx-xxx-" + gblPHONENUMBER.substring(6, 10) +" "+kony.i18n.getLocalizedString("keyTMBComplete");	
							}else{
							frmIBContactUs.label98089341265256.text =kony.i18n.getLocalizedString("keyThankyou")+" "+kony.i18n.getLocalizedString("keyTMBCompleteData")+" "+ gblPHONENUMBER +" "+kony.i18n.getLocalizedString("keyTMBComplete");
							}
						} else {
							frmIBContactUs.label98089341265256.text = kony.i18n.getLocalizedString("keyThankyou")+" "+kony.i18n.getLocalizedString("keyTMBCompleteData")+" "+ emailId +" "+kony.i18n.getLocalizedString("keyTMBComplete");
						}
					
				} else {
					if(kony.i18n.getCurrentLocale()!="th_TH")
						frmIBContactUs.label98089341265256.text = kony.i18n.getLocalizedString("keyThankyou")+" "+kony.i18n.getLocalizedString("keyTMBCompleteData")+" "+emailId + " "+kony.i18n.getLocalizedString("keyShortly");
					else
						frmIBContactUs.label98089341265256.text = kony.i18n.getLocalizedString("keyThankYouOther")+" "+emailId;
			   }
			   contactUsTMBEmailNotificationIB(sourceType,message,gblCustomerName,gblPHONENUMBER,locale,contactMode,subject,cSource);
				
			} else {
				dismissLoadingScreenPopup(); 
			}
	   }

}
	function contactUsTMBEmailNotificationIB(sourceType,message,gblCustomerName,gblPHONENUMBER,locale,contactMode,subject,cSource) {
			
				var inputParams = {
					notificationType:"Email",
					notificationSubject:subject,
					//source:cSource,
					Locale:"th_TH",
					//channel:"IB-INQ",
					//channelName:"IB-INQ",
					message:message,
					sourcetype:sourceType,
					contactType:contactMode,
					custNAME:gblCustomerName,
					type:"Admin",
					phoneNumber:gblPHONENUMBER
				};
				 invokeServiceSecureAsync("ContactUsNotification", inputParams, sendIBContactUsAdminNotificationCallBack);
	}
	function sendIBContactUsAdminNotificationCallBack(status,callbackResponse){
			
		}
		function contactFAQUrlIB(){
				var locale = kony.i18n.getCurrentLocale();
				
				var url = "";
				//if(locale == "en_US") {
//					//url ="https://www.tmbbank.com/howto/en/e-banking/faq.html";
//					url ="https://www.tmbbank.com/en/howto/e-banking/faq.html"
//				} else {
//					url ="https://www.tmbbank.com/howto/e-banking/faq.html";
//				}
				url = kony.i18n.getLocalizedString("contact_Faq");
				
				kony.application.openURL(url);			   		 
			}
	function contactUsDisplayFormIB(){
	
		if(gblLoggedIn== true || gblLoggedIn == "true") {
			contactUsDisplayIB();
			frmIBContactUs.line473367234297752.isVisible = false;
		} else {
			var locale = kony.i18n.getCurrentLocale();
				var url = "";
				//if(locale == "en_US") {
//					url ="https://www.tmbbank.com/en/contact/";
//				} else {
//					url ="https://www.tmbbank.com/contact/";
//				}
				url = kony.i18n.getLocalizedString("contact_Url");
				kony.application.openURL(url);
		}
		frmIBContactUs.hbox506459299512370.isVisible = true;
		frmIBContactUs.hbox98089341292088.isVisible = false;
	}
	function contactUsDisplayIB(){
		showLoadingScreenPopup();
		frmIBContactUs.hbxContactIB.setVisibility(true);
		frmIBContactUs.hbxCntCmplt.setVisibility(false);
		frmIBContactUs.hbxFeedComplt.setVisibility(false);
		frmIBContactUs.hbxCNTMsg.setVisibility(true);
		frmIBContactUs.cmbCnt.selectedKey=1;
		frmIBContactUs.tbxMsg.text="";
		frmIBContactUs.hbxRadio.setVisibility(true);
		frmIBContactUs.hbox476108006294054.isVisible = false;
		frmIBContactUs.hbxFB1.isVisible = false;
		frmIBContactUs.hbxFB2.isVisible = false;
		frmIBContactUs.hbxFB3.isVisible = false;
		frmIBContactUs.label477746511291445.isVisible = false;
		frmIBContactUs.tbxMessage.isVisible = false;
		frmIBContactUs.hbxCntImage.isVisible = true;
		frmIBContactUs.hbox98089341265246.setVisibility(false);
	    frmIBContactUs.hbox98089341292096.setVisibility(false);
		frmIBContactUs.btnSend.setVisibility(false);
		frmIBContactUs.line473367234297752.isVisible = false;
		frmIBContactUs.btnContactUs.skin = "btnTab2LeftFocus";
		frmIBContactUs.btnFeedBack.skin = "btnTab2RightNormal";
		frmIBContactUs.btnMobilePhone.skin="btnRadio";
		var isIE8 = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
		if(!isIE8){
			frmIBContactUs.btnMobilePhone.focusSkin="btnRadioFoc";
			frmIBContactUs.btnemail.focusSkin="btnRadioFoc";
		}
		frmIBContactUs.btnemail.skin="btnRadioFoc";
		frmIBContactUs.label506459299571357.text = kony.i18n.getLocalizedString("keyContactUs");
		var temp=[];
						temp.push(["1",kony.i18n.getLocalizedString("cnt_topic")]);
						temp.push(["2",kony.i18n.getLocalizedString("Cnt_IB")]);
						temp.push(["3",kony.i18n.getLocalizedString("Cnt_DA")]);
						temp.push(["4",kony.i18n.getLocalizedString("Cnt_LP")]);
						temp.push(["5",kony.i18n.getLocalizedString("Cnt_Compliants")]);
						frmIBContactUs.cmbCnt.masterData=temp;	
		dismissLoadingScreenPopup();						
		frmIBContactUs.show();				
	}
	function syncIBContactUs(){
			try {
				if (kony.application.getCurrentForm().id == "frmIBContactUs") 
					{
						var temp=[];
						temp.push(["1",kony.i18n.getLocalizedString("cnt_topic")]);
						temp.push(["2",kony.i18n.getLocalizedString("Cnt_IB")]);
						temp.push(["3",kony.i18n.getLocalizedString("Cnt_DA")]);
						temp.push(["4",kony.i18n.getLocalizedString("Cnt_LP")]);
						temp.push(["5",kony.i18n.getLocalizedString("Cnt_Compliants")]);
						frmIBContactUs.cmbCnt.masterData=temp;
						frmIBContactUs.cmbCnt.selectedKey = selectedCNTIndex;
						 
					//	frmIBContactUs.tbxMsg.placeholder = kony.i18n.getLocalizedString("cnt_textMessage");
					//	frmIBContactUs.tbxMessage.placeholder = kony.i18n.getLocalizedString("cnt_textMessage");
					}
	
			} catch (e) {
				// todo: handle exception
			}
	}
	function contactUsImageChange(btnname){
		var isIE8 = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
		if(btnname=='phone'){
			frmIBContactUs.btnMobilePhone.skin="btnRadioFoc";
			if(!isIE8){
				frmIBContactUs.btnMobilePhone.focusSkin="btnRadioFoc";
				frmIBContactUs.btnemail.focusSkin="btnRadio";
			}	
			frmIBContactUs.btnemail.skin="btnRadio";
		} else {
			frmIBContactUs.btnMobilePhone.skin="btnRadio";
			if(!isIE8){
				frmIBContactUs.btnMobilePhone.focusSkin="btnRadio";
				frmIBContactUs.btnemail.focusSkin="btnRadioFoc";
			}
			frmIBContactUs.btnemail.skin="btnRadioFoc";
		}
	
	}
