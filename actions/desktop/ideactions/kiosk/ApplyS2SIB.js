var s2s_ib_activityTypeID;
var s2s_ib_errorCode;
var s2s_ib_activityStatus;
var s2s_ib_deviceNickName;
var s2s_ib_activityFlexValues1;
var s2s_ib_activityFlexValues2;
var s2s_ib_activityFlexValues3;
var s2s_ib_activityFlexValues4;
var s2s_ib_activityFlexValues5;
var s2s_ib_logLinkageId;
var selectedData;
var ibLinkdAcct;
var ibNoFixedAcct;
var ibLiqInqCollectionData;
var eligibleS2SAcctIBInfo;
var noFixedAcctIBInfo;
var retailLiqTranInqRsltIB;
var tokenValueForS2S;
var tokenFlagForS2S;
var collectionDataExecuteIB;
var exeIBFromAcctName;
var exeIBFromAcctType;
var exeIBToAcctName;
var exeIBToAcctType;
var transRefNumForS2SIB;
var retailLiqTrnsAddIB;
//var exeToAccountProdId;
/** This method is to get the status of S2S. Whether the customer already applied for S2S or not. **/
function getApplyS2SStatusIB() {
    showLoadingScreenPopup();
    gbls2sEditFlag = "false";
    gblConfOrComp = false;
    var inputParams = {}
    invokeServiceSecureAsync("RetailLiquidityInquiry", inputParams, startRetailLiqInqForIBAsyncCallback);
}

function startRetailLiqInqForIBAsyncCallback(status, collectionData) {
    if (status == 400) {
        if (collectionData["opstatus"] == 0) {
            // checking already applied or not 
            //As of now date is using have to check in vit/previt env with sample req
            if (collectionData["statusCode"] == "0") {
                if (collectionData["originalDtRegister"] != 0) {
                    gblSSApply = "false";
                    gblExeS2S = "false";
                    gbls2sEditFlag = "true";
                    // Edit or Delete flow: fetching linked account and nofixed account details for edit flow
                    ibLinkdAcct = collectionData["acctIdentValue"];
                    ibNoFixedAcct = collectionData["partyId"];
                    ibLiqInqCollectionData = collectionData;
                    dismissLoadingScreenPopup();
                    getCustomerAccountForIBInfo();
                } else {
                    if (collectionData["serverStatusCode"] == 15) {
                        frmIBSSApply.hbxTMBImg.setVisibility(false);
                        frmIBSSApply.hbxSSApply.skin = "hbxIBSizelimiterRightGrey";
                        frmIBSSApply.vbxSSRight.skin = "vbxRightColumnGrey";
                        frmIBSSApply.customFromAccountIB.setVisibility(true);
                        frmIBSSApply.imgXferArrowFrom.setVisibility(true);
                        gblSSApply = "true";
                        gbls2sEditFlag = "false";
                        gblExeS2S = "false";
                        dismissLoadingScreenPopup();
                        checkBusinessHoursForS2SIB();
                    }
                }
            } else {
                frmIBSSApply.hbxTMBImg.setVisibility(false);
                frmIBSSApply.hbxSSApply.skin = "hbxIBSizelimiterRightGrey";
                frmIBSSApply.vbxSSRight.skin = "vbxRightColumnGrey";
                frmIBSSApply.imgXferArrowFrom.setVisibility(true);
                frmIBSSApply.customFromAccountIB.setVisibility(true);
                gblSSApply = "true";
                gbls2sEditFlag = "false";
                gblExeS2S = "false";
                gblSSServieHours = "applyServiceHours";
                dismissLoadingScreenPopup();
                checkBusinessHoursForS2SIB();
            }
        } else {
            dismissLoadingScreenPopup();
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}
/** This method is used to check the service hours **/
function checkBusinessHoursForS2SIB() {
    showLoadingScreenPopup();
    var inputParams = {
        myProfileFlag: "false"
    };
    invokeServiceSecureAsync("checkS2SBusinessHours", inputParams, startApplyS2SIBServiceAsyncCallback);
}

function startApplyS2SIBServiceAsyncCallback(status, collectionData) {
    if (status == 400) {
        if (collectionData["opstatus"] == 0) {
            dismissLoadingScreenPopup();
            //collectionData["s2sBusinessHrsFlag"] = "true";
            if (collectionData["s2sBusinessHrsFlag"] == "true") {
                // call if current time falls in between provided business hours
                gblSSServieHours = "applyServiceHours";
                getIBStatus();
            } else {
                dismissLoadingScreenPopup();
                showAlert(kony.i18n.getLocalizedString("S2S_ApplyBusinessHours"), kony.i18n.getLocalizedString("info"));
            }
        } else {
            dismissLoadingScreenPopup();
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}
/** This method is used to get the ib statuses **/
function getIBStatus() {
    showLoadingScreenPopup();
    var inputParams = {
        crmId: ""
    };
    invokeServiceSecureAsync("crmProfileInq", inputParams, startcrmProfileIBInqServiceAsyncCallback);
}
/**************************************************************************************
		Module	: startcrmProfileIBInqServiceAsyncCallback
		Author  : Kony
		Date    : 
		Purpose : This method is used to get the ib and mb status values by inquiring the crmprofile
****************************************************************************************/
function startcrmProfileIBInqServiceAsyncCallback(status, collectionData) {
    if (status == 400) {
        if (collectionData["opstatus"] == 0) {
            var statusCode = collectionData["statusCode"];
            var severity = collectionData["severity"];
            var statusDesc = collectionData["statusDesc"];
            showLoadingScreenPopup();
            if (statusCode != 0) {
                dismissLoadingScreenPopup();;
                showAlert(collectionData["addStatusDesc"], kony.i18n.getLocalizedString("info"));
                return false;
            } else {
                var ibStat = collectionData["ibUserStatusIdTr"];
                // tokenValueForS2S = collectionData["tokenFlag"];
                gblEmailId = collectionData["emailAddr"];
                if (ibStat != null) {
                    var inputParams = {
                        ibStatus: ibStat
                    };
                    invokeServiceSecureAsync("checkIBMBStatus", inputParams, checkIBStatus);
                }
            }
        } else {
            dismissLoadingScreenPopup();
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}

function checkIBStatus(status, collectionData) {
    if (status == 400) {
        if (collectionData["opstatus"] == 0) {
            dismissLoadingScreenPopup();
            // this will be made as true when we click on notification link. true = we are trying to execute s2s.
            if (gblExeS2S == true) {
                if (collectionData["ibStatusFlag"] == "true") {
                    showAlert(kony.i18n.getLocalizedString("keyTranxPwdLocked"), kony.i18n.getLocalizedString("info"));
                    return false;
                } else {
                    invokeRetailLiquidityTransferInqForIB();
                }
            } else {
                if (collectionData["ibStatusFlag"] == "true") {
                    showAlert(kony.i18n.getLocalizedString("keyTranxPwdLocked"), kony.i18n.getLocalizedString("info"));
                    return false;
                } else {
                    getCustomerAccountForIBInfo();
                }
            }
        } else {
            dismissLoadingScreenPopup();
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}
/** This method is used to get the account information for the respective crmid given **/
function getCustomerAccountForIBInfo() {
    showLoadingScreenPopup();
    if (gblExeS2S == true) {
        var inputParams = {
            crmId: "",
            locale: kony.i18n.getCurrentLocale(),
            s2sExeFlag: gblExeS2S,
            linkedAccount: ibLinkdAcct,
            noFixedAccount: ibNoFixedAcct
        };
    } else if (gbls2sEditFlag == "true") {
        var inputParams = {
            crmId: "",
            locale: kony.i18n.getCurrentLocale(),
            s2sEditFlag: gbls2sEditFlag,
            linkedAccount: ibLinkdAcct,
            noFixedAccount: ibNoFixedAcct,
            s2sExeFlag: gblExeS2S
        };
    } else {
        var inputParams = {
            crmId: "",
            locale: kony.i18n.getCurrentLocale(),
            s2sEditFlag: gbls2sEditFlag,
            s2sExeFlag: gblExeS2S
        };
    }
    invokeServiceSecureAsync("s2sCustomerAccInq", inputParams, startCustAccountInqIBServiceAsyncCallback);
}

function startCustAccountInqIBServiceAsyncCallback(status, collectionData) {
    if (status == 400) {
        if (collectionData["opstatus"] == 0) {
            var statusCode = collectionData["statusCode"];
            var severity = collectionData["severity"];
            var statusDesc = collectionData["statusDesc"];
            gblCurrentDateS2S = "";
            if (statusCode != 0) {
                dismissLoadingScreenPopup();
                showAlert(collectionData["acctStatusDesc"], kony.i18n.getLocalizedString("info"));
            } else {
                dismissLoadingScreenPopup();
                gblCurrentDateS2S = collectionData["currentDate"];
                if (gbls2sEditFlag == "false" && gblExeS2S == "false") {
                    eligibleS2SAcctIBInfo = collectionData["eligibleS2SAcctInfo"];
                    noFixedAcctIBInfo = collectionData["noFixedAcctInfo"];
                    if (noFixedAcctIBInfo.length > 0) {
                        //for(var i=0; i < noFixedAcctIBInfo.length; i++) {
                        if (noFixedAcctIBInfo[0]["isNoFixedExist"] == "true") {
                            gblNFOpnStats = noFixedAcctIBInfo[0]["isNoFixedExist"].trim();
                            gblNFActiStats = noFixedAcctIBInfo[0]["isNoFixedActive"].trim();
                            gblNFHidden = noFixedAcctIBInfo[0]["isNoFixedHidden"].trim();
                        } else if (noFixedAcctIBInfo[0]["isNoFixedExist"] == "false") {
                            gblNFOpnStats = noFixedAcctIBInfo[0]["isNoFixedExist"].trim();
                            gblNFActiStats = false;
                            gblNFHidden = false;
                        }
                    }
                    if (eligibleS2SAcctIBInfo.length != 0) {
                        //Resetting the form
                        frmIBSSSTnC.hbxSSSIntroHdr.setVisibility(true);
                        frmIBSSSTnC.hbxTnCHdr.setVisibility(false);
                        frmIBSSSTnC.lblSSSIntroData.setVisibility(true);
                        //frmIBSSSTnC.lblSSSTnC.setVisibility(true);
                        frmIBSSSTnC.hbxApplyNow.setVisibility(true);
                        frmIBSSSTnC.hbxBtnsCnclAgree.setVisibility(false);
                        frmIBSSSTnC.hboxTncContent.setVisibility(false);
                        //--
                        frmIBSSSTnC.show();
                    } else {
                        alert("you dont have any link account which is eligible");
                    }
                } else if (gbls2sEditFlag == "true") {
                    gblSSLinkedStatus = collectionData["linkedAcctStatus"];
                    gblNFHidden = collectionData["NoFixedAcctStatus"];
                    ibLinkdAcct = ibLinkdAcct.substring(ibLinkdAcct.length - 10, ibLinkdAcct.length);
                    ibNoFixedAcct = ibNoFixedAcct.substring(ibNoFixedAcct.length - 10, ibNoFixedAcct.length);
                    //New DEF13096  //Resetting all the values as per tmb.properties file
                    gblSSTrnsLmtMin = collectionData["toMinAmount"];
                    gblSSTrnMinofMax = collectionData["toMaxAmount"];
                    gblSSTrnMaxofMin = collectionData["fromMinAmount"];
                    gblSSTrnsLmtMax = collectionData["fromMaxAmount"];
                    //--
                    //frmIBSSSDetails.lbltoNoFix.text = kony.i18n.getLocalizedString("S2S_FromExeedsMsg");
                    //frmIBSSSDetails.lbltransFrm.text = kony.i18n.getLocalizedString("S2S_ToBelowMsg");
                    var imageName = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + collectionData["ICON_IDLINK"] + "&modIdentifier=PRODICON";
                    frmIBSSSDetails.image247502979411375.src = imageName;
                    //frmIBSSSDetails.image2448366816169765.src = 
                    frmIBSSSDetails.lblAmntLmtMax.text = commaFormatted(ibLiqInqCollectionData["maxAmt"])
                    frmIBSSSDetails.lblAmntLmtMin.text = commaFormatted(ibLiqInqCollectionData["minAmt"])
                    frmIBSSSDetails.lblAccNo.text = formatAccountNo(ibLinkdAcct);
                    frmIBSSSDetails.lblAccNo2.text = formatAccountNo(ibNoFixedAcct);
                    frmIBSSSDetails.lblBalance.text = commaFormatted(collectionData["lblFromAccBal"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                    //frmIBSSSDetails.lblCurrencyLogo1.text = kony.i18n.getLocalizedString("ThaiBhat");
                    frmIBSSSDetails.lblCustName.text = collectionData["lblFromAccName"];
                    frmIBSSSDetails.lblAccType.text = collectionData["lblFromAccType"];
                    frmIBSSSDetails.lblBal2.text = commaFormatted(collectionData["lblToAccBal"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                    //frmIBSSSDetails.lblCurrencyLogo2.text = kony.i18n.getLocalizedString("ThaiBhat");
                    frmIBSSSDetails.lblCustName2.text = collectionData["lblToAccName"];
                    frmIBSSSDetails.lblAccType2.text = collectionData["lblToAccType"];
                    //if() both Accounts {No fixed and Linked account} are in active state not in Hidden State
                    frmIBSSSDetails.show();
                } else if (gblExeS2S == true) {
                    collectionDataExecuteIB = collectionData;
                    exeIBFromAcctName = collectionData["lblExeFromAccName"];
                    exeIBFromAcctType = collectionData["lblExeFromAccType"];
                    exeIBToAcctName = collectionData["lblExeToAccName"];
                    //exeIBToAcctType = collectionData["lblExeToAccType"];
                    exeIBToAcctType = collectionData["lblFromAccType"];
                    var exeToAccountProdId = collectionData["lblExeToAccProdID"];
                    var imageNameLink = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + collectionData["ICON_IDLINKED"] + "&modIdentifier=PRODICON";
                    var imageName = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + collectionData["ICON_IDNOFIXED"] + "&modIdentifier=PRODICON";
                    frmIBSSSExcuteTrnsfr.image247502979411375.src = imageNameLink;
                    frmIBSSSExcuteTrnsfr.image2448366816169765.src = imageName;
                    //getMinMaxAmountsForIB();
                    // to get reference id
                    finActivityLogForIB();
                }
            }
        } else {
            dismissLoadingScreenPopup();
            alert("No Account found");
        }
    }
}
/** in preshow of frm ib TnC **/
function s2sVerticalCoverFlow() {
    var count = 0;
    var fromData = [];
    var imageName = "";
    var locale = kony.i18n.getCurrentLocale();
    for (var i = 0; i < eligibleS2SAcctIBInfo.length; i++) {
        if (locale == "en_US") {
            productName = eligibleS2SAcctIBInfo[i]["productNameEN"]
            if (eligibleS2SAcctIBInfo[i]["personalizedAcctNickNameEN"] == null || eligibleS2SAcctIBInfo[i]["personalizedAcctNickNameEN"] == '') custName = eligibleS2SAcctIBInfo[i]["personalizedAcctNickName"]
            else custName = eligibleS2SAcctIBInfo[i]["personalizedAcctNickNameEN"]
        } else {
            productName = eligibleS2SAcctIBInfo[i]["productNameTH"]
            if (eligibleS2SAcctIBInfo[i]["personalizedAcctNickNameTH"] == null || eligibleS2SAcctIBInfo[i]["personalizedAcctNickNameTH"] == '') custName = eligibleS2SAcctIBInfo[i]["personalizedAcctNickName"]
            else custName = eligibleS2SAcctIBInfo[i]["personalizedAcctNickNameTH"]
        }
        imageName = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + eligibleS2SAcctIBInfo[i]["ICON_ID"] + "&modIdentifier=PRODICON";
        /*Account number formating*/
        var acctID = eligibleS2SAcctIBInfo[i]["acctIdentValue"];
        if (acctID.length == 14) {
            var frmId = "";
            for (var k = 4; k < 14; k++) {
                frmId = frmId + acctID[k];
            }
            acctID = frmId;
        }
        //end of account formating
        var availableBal = +" " + kony.i18n.getLocalizedString("ThaiBhat");
        if (count == 0) {
            fromData.push({
                lblProduct: kony.i18n.getLocalizedString("Product"),
                lblProductVal: productName,
                lblACno: kony.i18n.getLocalizedString("keyIBAccountNo"),
                img1: imageName,
                lblCustName: custName,
                lblBalance: commaFormatted(eligibleS2SAcctIBInfo[i]["availableAmt"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
                lblActNoval: addHyphenIB(acctID),
                lblSliderAccN1: "",
                lblSliderAccN2: eligibleS2SAcctIBInfo[i]["acctTypeValue"],
                lblDummy: "",
                lblRemFee: "",
                lblRemFeeval: "",
                prodCode: eligibleS2SAcctIBInfo[i]["productIdent"],
                accountWOF: addHyphenIB(eligibleS2SAcctIBInfo[i]["acctIdentValue"]),
                lblCustNameTH: eligibleS2SAcctIBInfo[i]["productNameTH"],
                lblCustNameEN: eligibleS2SAcctIBInfo[i]["productNameEN"],
                hiddenBranchEN: eligibleS2SAcctIBInfo[i]["personalizedAcctNickNameEN"],
                hiddenBranchTH: eligibleS2SAcctIBInfo[i]["personalizedAcctNickNameTH"]
            })
            count = count + 1
        } else if (count == 1) {
            fromData.push({
                lblProduct: kony.i18n.getLocalizedString("Product"),
                lblProductVal: productName,
                lblACno: kony.i18n.getLocalizedString("keyIBAccountNo"),
                img1: imageName,
                lblCustName: custName,
                lblBalance: commaFormatted(eligibleS2SAcctIBInfo[i]["availableAmt"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
                lblActNoval: addHyphenIB(acctID),
                lblSliderAccN1: "",
                lblSliderAccN2: eligibleS2SAcctIBInfo[i]["acctTypeValue"],
                lblDummy: "",
                lblRemFee: "",
                lblRemFeeval: "",
                prodCode: eligibleS2SAcctIBInfo[i]["productIdent"],
                accountWOF: addHyphenIB(eligibleS2SAcctIBInfo[i]["acctIdentValue"]),
                lblCustNameTH: eligibleS2SAcctIBInfo[i]["productNameTH"],
                lblCustNameEN: eligibleS2SAcctIBInfo[i]["productNameEN"],
                hiddenBranchEN: eligibleS2SAcctIBInfo[i]["personalizedAcctNickNameEN"],
                hiddenBranchTH: eligibleS2SAcctIBInfo[i]["personalizedAcctNickNameTH"]
            })
            count = count + 1
        } else if (count == 2) {
            fromData.push({
                lblProduct: kony.i18n.getLocalizedString("Product"),
                lblProductVal: productName,
                lblACno: kony.i18n.getLocalizedString("keyIBAccountNo"),
                img1: imageName,
                lblCustName: custName,
                lblBalance: commaFormatted(eligibleS2SAcctIBInfo[i]["availableAmt"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
                lblActNoval: addHyphenIB(acctID),
                lblSliderAccN1: "",
                lblSliderAccN2: eligibleS2SAcctIBInfo[i]["acctTypeValue"],
                lblDummy: "",
                lblRemFee: "",
                lblRemFeeval: "",
                prodCode: eligibleS2SAcctIBInfo[i]["productIdent"],
                accountWOF: addHyphenIB(eligibleS2SAcctIBInfo[i]["acctIdentValue"]),
                lblCustNameTH: eligibleS2SAcctIBInfo[i]["productNameTH"],
                lblCustNameEN: eligibleS2SAcctIBInfo[i]["productNameEN"],
                hiddenBranchEN: eligibleS2SAcctIBInfo[i]["personalizedAcctNickNameEN"],
                hiddenBranchTH: eligibleS2SAcctIBInfo[i]["personalizedAcctNickNameTH"]
            })
            count = 0
        }
    }
    frmIBSSApply.customFromAccountIB.data = fromData;
    frmIBSSApply.customFromAccountIB.onSelect = customWidgetSelectEventSend2Save;
    //frmIBSSApply.show();
    dismissLoadingScreenPopup();
}

function customWidgetSelectEventSend2Save() {
    var selectedItem = frmIBSSApply.customFromAccountIB.selectedItem;
    selectedData = frmIBSSApply.customFromAccountIB.data[gblCWSelectedItem];
    frmIBSSApply.lblLinkedAcctNickName.text = selectedData.lblCustName;
    frmIBSSApply.lblLinkedAcctTitle.text = selectedData.lblProductVal;
    frmIBSSApply.lblAcctNoValue.text = selectedData.lblActNoval;
    frmIBSSApply.lblLinkdBal.text = selectedData.lblBalance;
    frmIBSSApply.imgLinkedAcct.src = selectedData.img1;
    frmIBSSApply.hboxLinkedAcct.setVisibility(true);
    frmIBSSApply.txtBalMax.setFocus(true);
}
/** populate No Fixed Account details on frmIBSSApply page **/
function s2sNoFixedAcctPopulateIB() {
    var locale = kony.i18n.getCurrentLocale();
    if (gblNFOpnStats == "true" && gblNFActiStats == "true" && gblNFHidden == "true") {
        frmIBSSApply.imgNoFixedAcct.src = "icon_pig35.png";
        if (locale == "en_US") {
            if (noFixedAcctIBInfo[0]["personalizedAcctNickNameEN"] == null || noFixedAcctIBInfo[0]["personalizedAcctNickNameEN"] == '') frmIBSSApply.lblCustName2.text = noFixedAcctIBInfo[0]["personalizedAcctNickName"];
            else frmIBSSApply.lblCustName2.text = noFixedAcctIBInfo[0]["personalizedAcctNickNameEN"];
            if (noFixedAcctIBInfo[0]["productNameEN"] == null || noFixedAcctIBInfo[0]["productNameEN"] == '') frmIBSSApply.lblAccType2.text = noFixedAcctIBInfo[0]["productName"]
            else frmIBSSApply.lblAccType2.text = noFixedAcctIBInfo[0]["productNameEN"]
        } else {
            if (noFixedAcctIBInfo[0]["personalizedAcctNickNameTH"] == null || noFixedAcctIBInfo[0]["personalizedAcctNickNameTH"] == '') frmIBSSApply.lblCustName2.text = noFixedAcctIBInfo[0]["personalizedAcctNickName"];
            else frmIBSSApply.lblCustName2.text = noFixedAcctIBInfo[0]["personalizedAcctNickNameTH"];
            if (noFixedAcctIBInfo[0]["productNameTH"] == null || noFixedAcctIBInfo[0]["productNameTH"] == '') frmIBSSApply.lblAccType2.text = noFixedAcctIBInfo[0]["productName"]
            else frmIBSSApply.lblAccType2.text = noFixedAcctIBInfo[0]["productNameTH"]
        }
        frmIBSSApply.lblBal2.text = commaFormatted(noFixedAcctIBInfo[0]["availableAmt"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
        //frmIBSSApply.lblAcctNo.text = kony.i18n.getLocalizedString("AccountNoLabel") + ": ";
        frmIBSSApply.lblAccNo2.text = addHyphenIB(noFixedAcctIBInfo[0]["acctIdentValue"]);
    }
}
/** this method is used to get the max and min amounts which are configurable **/
function getMinMaxAmountsForIB() {
    showLoadingScreenPopup();
    var inputParams = {};
    invokeServiceSecureAsync("getMinMaxAmountVals", inputParams, startMinMaxAmtsIBAsyncCallback);
}
/** this method is used to set the min and max amounts to global variables **/
function startMinMaxAmtsIBAsyncCallback(status, collectionData) {
    if (status == 400) {
        if (collectionData["opstatus"] == 0) {
            dismissLoadingScreenPopup();
            gblSSTrnsLmtMax = collectionData["fromMaxAmount"];
            gblSSTrnMaxofMin = collectionData["fromMinAmount"];
            gblSSTrnMinofMax = collectionData["toMaxAmount"];
            gblSSTrnsLmtMin = collectionData["toMinAmount"];
            frmIBSSApply.txtBalMax.text = commaFormatted(collectionData["fromMaxDefaultAmount"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
            frmIBSSApply.txtBalMin.text = commaFormatted(collectionData["toMinDefaultAmount"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
            /*
			
			if (gblSSServieHours == "S2SEdit") {
				
				frmIBSSApply.txtBalMax.text = ibLiqInqCollectionData["maxAmt"];
				frmIBSSApply.txtBalMin.text = ibLiqInqCollectionData["minAmt"];
				frmIBSSApply.vbxSSRight.setVisibility(false);
				frmIBSSApply.hboxLinkedAcct.setVisibility(true);
				frmIBSSApply.hbxCancelSave.setVisibility(true);
				frmIBSSApply.hbxNext.setVisibility(false);
				}
			else if(gbls2sEditFlag == "confirmationEdit"){
			    
			    frmIBSSApply.txtBalMax.text=frmIBSSApplyCnfrmtn.lblAmntLmtMax.text;
            	frmIBSSApply.txtBalMin.text=frmIBSSApplyCnfrmtn.lblAmntLmtMin.text;	
			}else {
				frmIBSSApply.txtBalMax.text = commaFormatted(gblSSTrnsLmtMax);
				frmIBSSApply.txtBalMin.text = commaFormatted(gblSSTrnMinofMax);
			}*/
        } else {
            s2s_ib_errorCode = collectionData["opstatus"];
            s2s_ib_activityStatus = "02";
            dismissLoadingScreenPopup();
            activityLogServiceCall(s2s_ib_activityTypeID, s2s_ib_errorCode, s2s_ib_activityStatus, s2s_ib_deviceNickName, s2s_ib_activityFlexValues1, s2s_ib_activityFlexValues2, s2s_ib_activityFlexValues3, s2s_ib_activityFlexValues4, s2s_ib_activityFlexValues5, s2s_ib_logLinkageId);
            alert("You Can't apply for Send To Save");
        }
    }
}
/** populate segmentslider information on confirm page **/
function s2sSegmentSliderMappingForIBConfirm() {
    var selectedData = frmIBSSApply.customFromAccountIB.data[gblCWSelectedItem];
    frmIBSSApplyCnfrmtn.lblAccType.text = selectedData.lblProductVal;
    frmIBSSApplyCnfrmtn.lblCustName.text = selectedData.lblCustName;
    frmIBSSApplyCnfrmtn.lblAcc.text = kony.i18n.getLocalizedString("keyIBAccountNo");
    frmIBSSApplyCnfrmtn.lblAccNo.text = selectedData.lblActNoval;
    frmIBSSApplyCnfrmtn.lblBalance.text = selectedData.lblBalance;
    frmIBSSApplyCnfrmtn.imgFrom.src = selectedData.img1;
}
/** populate NoFixedAcct information on confirm page **/
function s2sNoFixedAcctMappingForIBConfirm() {
    frmIBSSApplyCnfrmtn.imgNoFix.src = frmIBSSApply.imgNoFixedAcct.src;
    frmIBSSApplyCnfrmtn.lblCustName2.text = frmIBSSApply.lblCustName2.text;
    frmIBSSApplyCnfrmtn.lblBal2.text = frmIBSSApply.lblBal2.text;
    //frmIBSSApplyCnfrmtn.lblAcc2.text = frmIBSSApply.lblAcctNo.text;
    frmIBSSApplyCnfrmtn.lblAccNo2.text = frmIBSSApply.lblAccNo2.text;
    frmIBSSApplyCnfrmtn.lblAccType2.text = frmIBSSApply.lblAccType2.text;
}

function saveInputinSessionIB() {
    var inputParam = [];
    showLoadingScreenPopup();
    invokeServiceSecureAsync("tokenSwitching", inputParam, saveInputTokeSwithchInSessionIBCallBack);
}

function saveInputTokeSwithchInSessionIBCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            var txtBalMaxVal = frmIBSSApply.txtBalMax.text;
            if (txtBalMaxVal.indexOf(",") != -1) txtBalMaxVal = replaceCommon(txtBalMaxVal, ",", "");
            txtBalMaxVal = txtBalMaxVal.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
            var txtBalMinVal = frmIBSSApply.txtBalMin.text;
            if (txtBalMinVal.indexOf(",") != -1) txtBalMinVal = replaceCommon(txtBalMinVal, ",", "");
            txtBalMinVal = txtBalMinVal.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
            inputParam = {};
            inputParam["linkedAcctValue"] = removeHyphenIB(frmIBSSApply.lblAcctNoValue.text);
            inputParam["partyId"] = removeHyphenIB(frmIBSSApply.lblAccNo2.text);
            inputParam["maxAmt"] = txtBalMaxVal; //removeCommas(txtBalMaxVal);
            inputParam["minAmt"] = txtBalMinVal; //removeCommas(txtBalMinVal);
            inputParam["s2sType"] = "ApplyS2S";
            invokeServiceSecureAsync("sendToSaveApplyValidationService", inputParam, saveInputinSessioncallBackIB);
        }
    }
}

function saveInputinSessioncallBackIB(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
            frmIBSSApplyCnfrmtn.show();
            dismissLoadingScreenPopup();
        } else {
            dismissLoadingScreenPopup();
        }
    }
}

function saveInputinSessionEditIB() {
    var inputParam = [];
    showLoadingScreenPopup();
    invokeServiceSecureAsync("tokenSwitching", inputParam, saveInputinSessionEditIBTokenSwithchCallBack);
}

function saveInputinSessionEditIBTokenSwithchCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            var txtBalMaxVal = frmIBSSApply.txtBalMax.text;
            if (txtBalMaxVal.indexOf(",") != -1) txtBalMaxVal = replaceCommon(txtBalMaxVal, ",", "");
            txtBalMaxVal = txtBalMaxVal.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
            var txtBalMinVal = frmIBSSApply.txtBalMin.text;
            if (txtBalMinVal.indexOf(",") != -1) txtBalMinVal = replaceCommon(txtBalMinVal, ",", "");
            txtBalMinVal = txtBalMinVal.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
            inputParam = {};
            inputParam["linkedAcctValue"] = removeHyphenIB(frmIBSSApply.lblAcctNoValue.text);
            inputParam["partyId"] = removeHyphenIB(frmIBSSApply.lblAccNo2.text);
            inputParam["maxAmt"] = txtBalMaxVal; //removeCommas(txtBalMaxVal);
            inputParam["minAmt"] = txtBalMinVal; //removeCommas(txtBalMinVal);
            inputParam["s2sType"] = "EditS2S";
            invokeServiceSecureAsync("sendToSaveApplyValidationService", inputParam, saveInputinSessioncallBackIB);
        }
    }
}
/** populate NoFixedAcct information on confirm page **/
function s2sLinkedAcctForIBEditConfirmation() {
    frmIBSSApplyCnfrmtn.imgFrom.src = frmIBSSApply.imgLinkedAcct.src;
    frmIBSSApplyCnfrmtn.lblCustName.text = frmIBSSApply.lblLinkedAcctNickName.text;
    frmIBSSApplyCnfrmtn.lblBalance.text = frmIBSSApply.lblLinkdBal.text;
    frmIBSSApplyCnfrmtn.lblAcc.text = frmIBSSApply.lblAcctNo1.text;
    frmIBSSApplyCnfrmtn.lblAccNo.text = frmIBSSApply.lblAcctNoValue.text;
    frmIBSSApplyCnfrmtn.lblAccType.text = frmIBSSApply.lblLinkedAcctTitle.text;
}
/** request for otp **/
function requestButtonForOTPIBForS2S() {
    var curFrm = kony.application.getCurrentForm().id;
    if (curFrm == "frmIBSSSExcuteTrnsfr") {
        frmIBSSSExcuteTrnsfr.lblOTPinCurr.text = " "; //kony.i18n.getLocalizedString("invalidOTP"); //
        frmIBSSSExcuteTrnsfr.lblPlsReEnter.text = " ";
        frmIBSSSExcuteTrnsfr.hbxOTPincurrect.isVisible = false;
        frmIBSSSExcuteTrnsfr.hbox476047582127699.isVisible = true;
        frmIBSSSExcuteTrnsfr.hbxOTPsnt.isVisible = true;
        frmIBSSSExcuteTrnsfr.txtBxOTP.setFocus(true);
    } else if (curFrm == "frmIBSSApplyCnfrmtn") {
        frmIBSSApplyCnfrmtn.lblOTPinCurr.text = " "; //kony.i18n.getLocalizedString("invalidOTP"); //
        frmIBSSApplyCnfrmtn.lblPlsReEnter.text = " ";
        frmIBSSApplyCnfrmtn.hbxOTPincurrect.isVisible = false;
        frmIBSSApplyCnfrmtn.hboxBankRef.isVisible = true;
        frmIBSSApplyCnfrmtn.hbxOTPsnt.isVisible = true;
        frmIBSSApplyCnfrmtn.txtBxOTP.setFocus(true);
    }
    //checking for otp flag
    //if(tokenFlagForS2S == false) {
    frmIBSSApplyCnfrmtn.lblkeyOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
    frmIBSSApplyCnfrmtn.lblOTPKey.text = kony.i18n.getLocalizedString("keyOTP");
    frmIBSSApplyCnfrmtn.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
    frmIBSSApplyCnfrmtn.txtBxOTP.text = "";
    tokenFlagForS2S = true;
    startReqOTPApplyServicesS2SIB();
    //}
}

function startReqOTPApplyServicesS2SIB() {
    showLoadingScreenPopup();
    var inputParams = {};
    var locale = kony.i18n.getCurrentLocale();
    //inputParams["Product_Code"]= "";
    /*
    if(kony.application.getCurrentForm().id == 'frmIBSSSExcuteTrnsfr'){
    	// Execute
    	inputParams["Channel"]= "IB_S2S_EXECUTE";
    	if (kony.i18n.getCurrentLocale() == "en_US") {
    		inputParams["eventNotificationPolicyId"] = "MIB_iORFT_EN";
    		inputParams["SMS_Subject"] = "MIB_iORFT_EN";
    	} else {
    		inputParams["eventNotificationPolicyId"] = "MIB_iORFT_TH";
    		inputParams["SMS_Subject"] = "MIB_iORFT_TH";
    	}
    } else {
    	// Apply
    	inputParams["Channel"]= "IB_S2S_APPLY";
    	if (kony.i18n.getCurrentLocale() == "en_US") {
    		inputParams["eventNotificationPolicyId"] = "MIB_OTPApply_SVC_EN";
    		inputParams["SMS_Subject"] = "MIB_OTPApply_SVC_EN";
    	} else {
    		inputParams["eventNotificationPolicyId"] = "MIB_OTPApply_SVC_TH";
    		inputParams["SMS_Subject"] = "MIB_OTPApply_SVC_TH";
    	}
    }
	
    */
    //New Changes
    var Channel = "";
    var eventNotificationPolicyId = "";
    var SMS_Subject = "";
    var Amount = frmIBSSSExcuteTrnsfr.lblTrnsAmnt.text;
    if (Amount.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht")) != -1) {
        Amount = Amount.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim();
    }
    if (kony.application.getCurrentForm().id == "frmIBSSSExcuteTrnsfr") {
        // Execute
        Channel = "ExecuteTransfer";
        /*
        if (locale == "en_US") {
        	eventNotificationPolicyId  = "MIB_iORFT_EN";
        	SMS_Subject = "MIB_iORFT_EN";
        } else {
        	eventNotificationPolicyId = "MIB_iORFT_TH";
        	SMS_Subject = "MIB_iORFT_TH";
        }*/
    } else {
        // Apply
        //Channel= "IB_S2S_APPLY";
        //CR 13 change
        if (gbls2sEditFlag == "true") {
            Channel = "EditS2S";
            /*
		    if (locale == "en_US") {
		    	eventNotificationPolicyId = "MIB_OTPEdit_SVC_EN";
			    SMS_Subject = "MIB_OTPEdit_SVC_EN";
	     	} else {
			   eventNotificationPolicyId = "MIB_OTPEdit_SVC_TH";
			   SMS_Subject = "MIB_OTPEdit_SVC_TH";
		   }
		   */
        } else {
            Channel = "ApplyS2S"
                /*
		   if (locale == "en_US") {
		    	eventNotificationPolicyId = "MIB_OTPApply_SVC_EN";
			    SMS_Subject = "MIB_OTPApply_SVC_EN";
	     	} else {
			   eventNotificationPolicyId = "MIB_OTPApply_SVC_TH";
			   SMS_Subject = "MIB_OTPApply_SVC_TH";
		   }*/
        }
        //CR 13 change end
    }
    //0809307535 , gblPHONENUMBER
    var inputParams = {
        retryCounterRequestOTP: gblRetryCountRequestOTP,
        Channel: Channel,
        locale: locale
    };
    invokeServiceSecureAsync("generateOTPWithUser", inputParams, callBackGenOTPForS2S);
}

function callBackGenOTPForS2S(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["errCode"] == "GenOTPRtyErr00001") {
            dismissLoadingScreenPopup();
            showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
            return false;
        }
        if (callBackResponse["errCode"] == "GenOTPRtyErr00002") {
            dismissLoadingScreenPopup();
            showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00002"), kony.i18n.getLocalizedString("info"));
            return false;
        }
        if (callBackResponse["opstatus"] == 0) {
            dismissLoadingScreenPopup();
            if (callBackResponse["errCode"] == "GenOTPRtyErr00001") {
                s2s_ib_activityStatus = "02";
                s2s_ib_errorCode = callBackResponse["errCode"];
                activityLogServiceCall(s2s_ib_activityTypeID, s2s_ib_errorCode, s2s_ib_activityStatus, s2s_ib_deviceNickName, s2s_ib_activityFlexValues1, s2s_ib_activityFlexValues2, s2s_ib_activityFlexValues3, s2s_ib_activityFlexValues4, s2s_ib_activityFlexValues5, s2s_ib_logLinkageId);
                showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (callBackResponse["errCode"] == "JavaErr00001") {
                s2s_ib_activityStatus = "02";
                s2s_ib_errorCode = callBackResponse["errCode"];
                activityLogServiceCall(s2s_ib_activityTypeID, s2s_ib_errorCode, s2s_ib_activityStatus, s2s_ib_deviceNickName, s2s_ib_activityFlexValues1, s2s_ib_activityFlexValues2, s2s_ib_activityFlexValues3, s2s_ib_activityFlexValues4, s2s_ib_activityFlexValues5, s2s_ib_logLinkageId);
                showAlertIB(kony.i18n.getLocalizedString("ECJavaErr00001"), kony.i18n.getLocalizedString("info"));
                return false;
            }
            gblOTPReqLimit = kony.os.toNumber(callBackResponse["otpRequestLimit"]);
            var reqOtpTimer = kony.os.toNumber(callBackResponse["requestOTPEnableTime"]);
            gblRetryCountRequestOTP = kony.os.toNumber(callBackResponse["retryCounterRequestOTP"]);
            gblOTPLENGTH = kony.os.toNumber(callBackResponse["otpLength"]);
            if (gblExeS2S == true) {
                frmIBSSSExcuteTrnsfr.hboxBankRef.setVisibility(true);
                frmIBSSSExcuteTrnsfr.hbxOTPsnt.setVisibility(true);
                frmIBSSSExcuteTrnsfr.txtBxOTP.setFocus(true);
                kony.timer.schedule("OTPTimerReq", OTPTimerCallBackS2S, reqOtpTimer, false);
                frmIBSSSExcuteTrnsfr.lblbankRef.text = kony.i18n.getLocalizedString("keyIBbankrefno");
                var refVal = "";
                for (var d = 0; d < callBackResponse["Collection1"].length; d++) {
                    if (callBackResponse["Collection1"][d]["keyName"] == "pac") {
                        refVal = callBackResponse["Collection1"][d]["ValueString"];
                        break;
                    }
                }
                frmIBSSSExcuteTrnsfr.lblBankRefNo.text = refVal //callBackResponse["Collection1"][8]["ValueString"];
                frmIBSSSExcuteTrnsfr.lblMobNo.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
                frmIBSSSExcuteTrnsfr.btnOTPReq.skin = btnIBREQotp;
                frmIBSSSExcuteTrnsfr.btnOTPReq.setEnabled(false);
            } else {
                frmIBSSApplyCnfrmtn.hboxBankRef.setVisibility(true);
                frmIBSSApplyCnfrmtn.hbxOTPsnt.setVisibility(true);
                frmIBSSApplyCnfrmtn.txtBxOTP.setFocus(true);
                kony.timer.schedule("OTPTimerReq", OTPTimerCallBackS2S, reqOtpTimer, false);
                frmIBSSApplyCnfrmtn.lblbankRef.text = kony.i18n.getLocalizedString("keyIBbankrefno");
                var refVal = "";
                for (var d = 0; d < callBackResponse["Collection1"].length; d++) {
                    if (callBackResponse["Collection1"][d]["keyName"] == "pac") {
                        refVal = callBackResponse["Collection1"][d]["ValueString"];
                        break;
                    }
                }
                frmIBSSApplyCnfrmtn.lblBankRefNo.text = refVal //callBackResponse["Collection1"][8]["ValueString"];
                frmIBSSApplyCnfrmtn.lblMobNo.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
                frmIBSSApplyCnfrmtn.btnOTPReq.skin = btnIBREQotp;
                frmIBSSApplyCnfrmtn.btnOTPReq.setEnabled(false);
            }
            dismissLoadingScreenPopup();
            frmIBSSApplyCnfrmtn.txtBxOTP.setFocus(true);
        } else {
            dismissLoadingScreenPopup();
        }
    }
}

function OTPTimerCallBackS2S() {
    if (gblRetryCountRequestOTP > gblOTPReqLimit) {
        if (gblExeS2S == true) {
            frmIBSSSExcuteTrnsfr.btnOTPReq.skin = btnIBREQotp;
            frmIBSSSExcuteTrnsfr.btnOTPReq.setEnabled(false);
        } else {
            frmIBSSApplyCnfrmtn.btnOTPReq.skin = btnIBREQotp;
            frmIBSSApplyCnfrmtn.btnOTPReq.setEnabled(false);
        }
    } else {
        if (gblExeS2S == true) {
            frmIBSSSExcuteTrnsfr.btnOTPReq.skin = btnIBREQotpFocus;
            frmIBSSSExcuteTrnsfr.btnOTPReq.setEnabled(true);
        } else {
            frmIBSSApplyCnfrmtn.btnOTPReq.skin = btnIBREQotpFocus;
            frmIBSSApplyCnfrmtn.btnOTPReq.setEnabled(true);
        }
    }
    /*
    	if(gblExeS2S == true) {
    		frmIBSSSExcuteTrnsfr.btnOTPReq.skin = btnIBREQotpFocus;
    		frmIBSSSExcuteTrnsfr.btnOTPReq.setEnabled(true);
    	} else {
    		frmIBSSApplyCnfrmtn.btnOTPReq.skin = btnIBREQotpFocus;
    		frmIBSSApplyCnfrmtn.btnOTPReq.setEnabled(true);
    	}
    */
    try {
        kony.timer.cancel("OTPTimerReq")
    } catch (e) {}
}
/** This function is used to check the business hours for S2S when click on confirm **/
function checkBusinessHoursBeforeIBConfirm() {
    var inputParams = {
        myProfileFlag: "false"
    };
    showLoadingScreenPopup();
    invokeServiceSecureAsync("checkS2SBusinessHoursBeforeConfirm", inputParams, startCheckBusinessHoursForIBAsyncCallback);
}

function startCheckBusinessHoursForIBAsyncCallback(status, collectionData) {
    if (status == 400) {
        if (collectionData["opstatus"] == 0) {
            if (collectionData["s2sBusinessHrsFlag"] == "true") {
                //checkBusinessHoursBeforeIBConfirm = true;
                if (checkBusinessHoursBeforeIBConfirm) {
                    //gblSSServieHours = "applyServiceHours";
                    //showOTPPopup(kony.i18n.getLocalizedString("TransactionPass")+":", "", "", onClickSSConfrmOTP, 3);
                    if (gblExeS2S == true) {
                        s2s_ib_activityStatus = "00"
                        callApplyS2SActivityLogForS2SExecutionInitialIB(s2s_ib_activityStatus);
                        if (gblTokenSwitchFlag == false) onClickConfirmS2SIB(frmIBSSSExcuteTrnsfr.txtBxOTP.text);
                        else onClickConfirmS2SIB(frmIBSSSExcuteTrnsfr.tbxToken.text);
                    } else {
                        if (gblTokenSwitchFlag == false) onClickConfirmS2SIB(frmIBSSApplyCnfrmtn.txtBxOTP.text);
                        else onClickConfirmS2SIB(frmIBSSApplyCnfrmtn.tbxToken.text);
                    }
                }
            } else {
                dismissLoadingScreenPopup();
                s2s_ib_activityStatus = "01";
                activityLogServiceCall(s2s_ib_activityTypeID, s2s_ib_errorCode, s2s_ib_activityStatus, s2s_ib_deviceNickName, s2s_ib_activityFlexValues1, s2s_ib_activityFlexValues2, s2s_ib_activityFlexValues3, s2s_ib_activityFlexValues4, s2s_ib_activityFlexValues5, s2s_ib_logLinkageId);
                alert("You Can't apply for Send To Save");
            }
        } else {
            dismissLoadingScreenPopup();
            s2s_ib_activityStatus = "02";
            s2s_ib_errorCode = collectionData["opstatus"];
            activityLogServiceCall(s2s_ib_activityTypeID, s2s_ib_errorCode, s2s_ib_activityStatus, s2s_ib_deviceNickName, s2s_ib_activityFlexValues1, s2s_ib_activityFlexValues2, s2s_ib_activityFlexValues3, s2s_ib_activityFlexValues4, s2s_ib_activityFlexValues5, s2s_ib_logLinkageId);
            alert("You Can't apply for Send To Save");
        }
    }
}

function onClickConfirmS2SIB(otp) {
    if (gblExeS2S == true) {
        if (gblTokenSwitchFlag == false) {
            if (frmIBSSSExcuteTrnsfr.txtBxOTP.text == "") {
                //alert("Please Enter OTP");
                dismissLoadingScreenPopup();
                alert(kony.i18n.getLocalizedString("Receipent_alert_correctOTP"));
                return false;
            } else {
                gblVerifyOTP = gblVerifyOTP + 1;
                checkVerifyOTPIBForS2SExecute(otp);
            }
        } else {
            if (frmIBSSSExcuteTrnsfr.tbxToken.text == "") {
                //alert("Please Enter token");
                dismissLoadingScreenPopup();
                alert(kony.i18n.getLocalizedString("Receipent_tokenId"));
                return false;
            } else {
                gblVerifyOTP = gblVerifyOTP + 1;
                checkVerifyOTPIBForS2SExecute(otp);
            }
        }
    } else {
        if (gblTokenSwitchFlag == false) {
            if (frmIBSSApplyCnfrmtn.txtBxOTP.text == "") {
                dismissLoadingScreenPopup();
                alert(kony.i18n.getLocalizedString("Receipent_alert_correctOTP"));
                return false;
            } else {
                gblVerifyOTP = gblVerifyOTP + 1;
                if (gbls2sEditFlag == "true") checkVerifyOTPIBForS2SEdit(otp);
                else checkVerifyOTPIBForS2S(otp);
            }
        } else {
            if (frmIBSSApplyCnfrmtn.tbxToken.text == "") {
                dismissLoadingScreenPopup();
                alert(kony.i18n.getLocalizedString("Receipent_tokenId"));
                return false;
            } else {
                gblVerifyOTP = gblVerifyOTP + 1;
                if (gbls2sEditFlag == "true") checkVerifyOTPIBForS2SEdit(otp);
                else checkVerifyOTPIBForS2S(otp);
            }
        }
    }
}

function checkVerifyOTPIBForS2SEdit(otp) {
    var inputParam = {};
    setRetailLiquidityModInputParam(inputParam);
    inputParam["appID"] = appConfig.appId;
    //activitylogging
    inputParam["activationActivityLog_channelId"] = "01";
    inputParam["activationActivityLog_activityTypeID"] = s2s_ib_activityTypeID;
    inputParam["activationActivityLog_activityFlexValues1"] = s2s_ib_activityFlexValues1;
    inputParam["activationActivityLog_activityFlexValues2"] = s2s_ib_activityFlexValues2;
    inputParam["activationActivityLog_activityFlexValues3"] = s2s_ib_activityFlexValues3;
    inputParam["activationActivityLog_activityFlexValues4"] = s2s_ib_activityFlexValues4;
    inputParam["activationActivityLog_activityFlexValues5"] = s2s_ib_activityFlexValues5;
    if (gblTokenSwitchFlag == false) {
        inputParam["flowspa"] = false;
        inputParam["gblTokenSwitchFlag"] = false;
        inputParam["verifyPasswordEx_retryCounterVerifyOTP"] = gblVerifyOTP;
        inputParam["verifyPasswordEx_password"] = otp;
        inputParam["verifyPasswordEx_userId"] = gblUserName;
        frmIBSSApplyCnfrmtn.txtBxOTP.text = "";
        invokeServiceSecureAsync("SendToSaveEditConfirm", inputParam, callBackEditComposite)
            //
    } else {
        var inputParam = {};
        setRetailLiquidityModInputParam(inputParam);
        inputParam["appID"] = appConfig.appId;
        //activitylogging
        inputParam["activationActivityLog_channelId"] = "01";
        inputParam["activationActivityLog_activityTypeID"] = s2s_ib_activityTypeID;
        inputParam["activationActivityLog_activityFlexValues1"] = s2s_ib_activityFlexValues1;
        inputParam["activationActivityLog_activityFlexValues2"] = s2s_ib_activityFlexValues2;
        inputParam["activationActivityLog_activityFlexValues3"] = s2s_ib_activityFlexValues3;
        inputParam["activationActivityLog_activityFlexValues4"] = s2s_ib_activityFlexValues4;
        inputParam["activationActivityLog_activityFlexValues5"] = s2s_ib_activityFlexValues5;
        inputParam["gblTokenSwitchFlag"] = true;
        inputParam["flowspa"] = false;
        inputParam["verifyTokenEx_loginModuleId"] = "IB_HWTKN";
        inputParam["verifyTokenEx_userStoreId"] = "DefaultStore";
        inputParam["verifyTokenEx_retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
        inputParam["verifyTokenEx_retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
        inputParam["verifyTokenEx_userId"] = gblUserName;
        inputParam["verifyTokenEx_password"] = frmIBSSApplyCnfrmtn.tbxToken.text;
        inputParam["verifyTokenEx_sessionVal"] = "";
        inputParam["verifyTokenEx_segmentId"] = "segmentId";
        inputParam["verifyTokenEx_segmentIdVal"] = "MIB";
        inputParam["verifyTokenEx_channel"] = "InterNet Banking";
        frmIBSSApplyCnfrmtn.tbxToken.text = "";
        //invokeServiceSecureAsync("verifyTokenEx", inputParam, callBackVerifyOTPIBS2S)
        invokeServiceSecureAsync("SendToSaveEditConfirm", inputParam, callBackEditComposite)
    }
}

function callBackEditComposite(status, result) {
    if (result["opstatus"] == "1") {
        dismissLoadingScreenPopup();
        alert("Transaction Can Not Be Preocessed");
        return false;
    }
    if (result["opstatusVPX"] == "0") {
        gblVerifyOTPCounter = "0";
        if (result["opstatus"] == "0") {
            dismissLoadingScreenPopup();
            frmIBSSSDetails.lblAmntLmtMax.text = commaFormatted(result["MaxAmount"]);
            frmIBSSSDetails.lblAmntLmtMin.text = commaFormatted(result["MinAmount"]);
            gblSSApply = true;
            frmIBSSSDetails.show();
        } else {
            dismissLoadingScreenPopup();
            alert(result["errMsg"]);
            return false;
        }
    } else {
        frmIBFTrnsrEditCnfmtn.txtBxOTPFT.text = "";
        if (result["opstatusVPX"] == "8005") {
            if (result["errCode"] == "VrfyOTPErr00001") {
                gblVerifyOTPCounter = result["retryCounterVerifyOTP"];
                dismissLoadingScreenPopup();
                alert("" + kony.i18n.getLocalizedString("invalidOTP"));
                return false;
            } else if (result["errCode"] == "VrfyOTPErr00002") {
                dismissLoadingScreenPopup();
                handleOTPLockedIB(result);
                return false;
            } else if (result["errCode"] == "VrfyOTPErr00005") {
                dismissLoadingScreenPopup();
                alert("" + kony.i18n.getLocalizedString("invalidOTP"));
                return false;
            } else if (result["errCode"] == "VrfyOTPErr00006") {
                dismissLoadingScreenPopup();
                gblVerifyOTPCounter = result["retryCounterVerifyOTP"];
                alert("" + result["errMsg"]);
                return false;
            } else if (result["errCode"] == "GenOTPRtyErr00001") {
                dismissLoadingScreenPopup();
                showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                return false;
            } else {
                dismissLoadingScreenPopup();
                alert(" " + result["errMsg"]);
                return false;
            }
        }
    }
}

function checkVerifyOTPIBForS2S(otp) {
    var inputParam = {};
    setS2SLiquidityAddService(inputParam);
    setNotificationAdd(inputParam);
    inputParam["activationActivityLog_channelId"] = "01";
    inputParam["activationActivityLog_activityTypeID"] = s2s_ib_activityTypeID;
    inputParam["activationActivityLog_activityFlexValues1"] = s2s_ib_activityFlexValues1;
    inputParam["activationActivityLog_activityFlexValues2"] = s2s_ib_activityFlexValues2;
    inputParam["activationActivityLog_activityFlexValues3"] = s2s_ib_activityFlexValues3;
    inputParam["activationActivityLog_activityFlexValues4"] = s2s_ib_activityFlexValues4;
    inputParam["activationActivityLog_activityFlexValues5"] = s2s_ib_activityFlexValues5;
    inputParam["appID"] = appConfig.appId;
    if (gblTokenSwitchFlag == false) {
        inputParam["flowspa"] = false;
        inputParam["gblTokenSwitchFlag"] = false;
        inputParam["verifyPasswordEx_retryCounterVerifyOTP"] = gblVerifyOTP;
        inputParam["verifyPasswordEx_password"] = otp;
        inputParam["verifyPasswordEx_userId"] = gblUserName;
        if (gblExeS2S == true) {
            frmIBSSSExcuteTrnsfr.txtBxOTP.text = "";
        } else {
            frmIBSSApplyCnfrmtn.txtBxOTP.text = "";
        }
        invokeServiceSecureAsync("SendToSaveApplyConfirm", inputParam, callBackVerifyOTPIBS2SApply)
    } else {
        var inputParam = {};
        setS2SLiquidityAddService(inputParam);
        setNotificationAdd(inputParam);
        inputParam["flowspa"] = false;
        inputParam["activationActivityLog_channelId"] = "01";
        inputParam["activationActivityLog_activityTypeID"] = s2s_ib_activityTypeID;
        inputParam["activationActivityLog_activityFlexValues1"] = s2s_ib_activityFlexValues1;
        inputParam["activationActivityLog_activityFlexValues2"] = s2s_ib_activityFlexValues2;
        inputParam["activationActivityLog_activityFlexValues3"] = s2s_ib_activityFlexValues3;
        inputParam["activationActivityLog_activityFlexValues4"] = s2s_ib_activityFlexValues4;
        inputParam["activationActivityLog_activityFlexValues5"] = s2s_ib_activityFlexValues5;
        inputParam["appID"] = appConfig.appId;
        inputParam["gblTokenSwitchFlag"] = true;
        inputParam["verifyTokenEx_loginModuleId"] = "IB_HWTKN";
        inputParam["verifyTokenEx_userStoreId"] = "DefaultStore";
        inputParam["verifyTokenEx_retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
        inputParam["verifyTokenEx_retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
        inputParam["verifyTokenEx_userId"] = gblUserName;
        inputParam["verifyTokenEx_password"] = frmIBSSApplyCnfrmtn.tbxToken.text;
        inputParam["verifyTokenEx_sessionVal"] = "";
        inputParam["verifyTokenEx_segmentId"] = "segmentId";
        inputParam["verifyTokenEx_segmentIdVal"] = "MIB";
        inputParam["verifyTokenEx_channel"] = "InterNet Banking";
        frmIBSSApplyCnfrmtn.tbxToken.text = "";
        invokeServiceSecureAsync("SendToSaveApplyConfirm", inputParam, callBackVerifyOTPIBS2SApply)
    }
}

function callBackVerifyOTPIBS2SApply(status, result) {
    if (result["opstatus"] == "1") {
        dismissLoadingScreenPopup();
        alert("Transaction Can Not Be Preocessed");
        return false;
    }
    if (result["opstatusVPX"] == "0") {
        gblVerifyOTPCounter = "0";
        if (result["opstatus"] == "0") {
            dismissLoadingScreenPopup();
            frmIBSSApplyCmplete.label444732313770162.text = result["currentDate"];
            populateApplys2sCompleteScreen();
        } else {
            dismissLoadingScreenPopup();
            alert(result["errMsg"]);
            return false;
        }
    } else {
        if (result["opstatusVPX"] == "8005") {
            if (result["errCode"] == "VrfyOTPErr00001") {
                gblVerifyOTPCounter = result["retryCounterVerifyOTP"];
                dismissLoadingScreenPopup();
                alert("" + kony.i18n.getLocalizedString("invalidOTP"));
                return false;
            } else if (result["errCode"] == "VrfyOTPErr00002") {
                dismissLoadingScreenPopup();
                handleOTPLockedIB(result);
                return false;
            } else if (result["errCode"] == "VrfyOTPErr00005") {
                dismissLoadingScreenPopup();
                alert("" + kony.i18n.getLocalizedString("invalidOTP"));
                return false;
            } else if (result["errCode"] == "VrfyOTPErr00006") {
                dismissLoadingScreenPopup();
                gblVerifyOTPCounter = result["retryCounterVerifyOTP"];
                alert("" + result["errMsg"]);
                return false;
            } else if (result["errCode"] == "GenOTPRtyErr00001") {
                dismissLoadingScreenPopup();
                showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                return false;
            } else {
                dismissLoadingScreenPopup();
                alert(" " + result["errMsg"]);
                return false;
            }
        }
    }
}

function setS2SLiquidityAddService(inputParam) {
    var txtBalMaxVal = frmIBSSApplyCnfrmtn.lblAmntLmtMax.text;
    if (txtBalMaxVal.indexOf(",") != -1) txtBalMaxVal = replaceCommon(txtBalMaxVal, ",", "");
    txtBalMaxVal = txtBalMaxVal.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    var txtBalMinVal = frmIBSSApplyCnfrmtn.lblAmntLmtMin.text;
    if (txtBalMinVal.indexOf(",") != -1) txtBalMinVal = replaceCommon(txtBalMinVal, ",", "");
    txtBalMinVal = txtBalMinVal.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    var linkedAcctValue = frmIBSSApplyCnfrmtn.lblAccNo.text;
    linkedAcctValue = linkedAcctValue.replace(/-/g, "");
    inputParam["retailLiquidityAdd_mobileNumber"] = gblPHONENUMBER;
    inputParam["retailLiquidityAdd_acctIdentValue"] = linkedAcctValue;
    inputParam["retailLiquidityAdd_partyId"] = noFixedAcctIBInfo[0]["acctIdentValue"];
    inputParam["retailLiquidityAdd_maxAmt"] = removeCommas(txtBalMaxVal);
    inputParam["retailLiquidityAdd_minAmt"] = removeCommas(txtBalMinVal);
}

function setNotificationAdd(inputParam) {
    var deliveryMethod = "Email";
    var deliveryMethod = "Email";
    var maxAmount = (frmIBSSApplyCnfrmtn.lblAmntLmtMax.text).replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    var minAmount = (frmIBSSApplyCnfrmtn.lblAmntLmtMin.text).replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    var locale = kony.i18n.getCurrentLocale();
    inputParam["Locale"] = kony.i18n.getCurrentLocale(); //"en_US";
    //inputParam["emailId"]= gblEmailId;
    inputParam["NotificationAdd_customerName"] = gblCustomerName;
    inputParam["NotificationAdd_notificationType"] = deliveryMethod;
    inputParam["NotificationAdd_source"] = "sendEmailForS2S"
    inputParam["NotificationAdd_fromAcctNick"] = frmIBSSApplyCnfrmtn.lblCustName.text;
    inputParam["NotificationAdd_fromAccNo"] = "xxx-x-" + (frmIBSSApplyCnfrmtn.lblAccNo.text).substring(6, 11) + "-x";
    inputParam["NotificationAdd_toAccNick"] = frmIBSSApplyCnfrmtn.lblCustName2.text;
    inputParam["NotificationAdd_toAccNo"] = "xxx-x-" + (frmIBSSApplyCnfrmtn.lblAccNo2.text).substring(6, 11) + "-x";
    inputParam["NotificationAdd_amntTrnsFromNF"] = commaFormatted(removeCommas(maxAmount))
    inputParam["NotificationAdd_amntTrnsToNF"] = commaFormatted(removeCommas(minAmount))
}

function populateApplys2sCompleteScreen() {
    frmIBSSApplyCmplete.lblCustName.text = frmIBSSApplyCnfrmtn.lblCustName.text;
    frmIBSSApplyCmplete.lblBalance.text = frmIBSSApplyCnfrmtn.lblBalance.text;
    frmIBSSApplyCmplete.lblAcc.text = frmIBSSApplyCnfrmtn.lblAcc.text;
    frmIBSSApplyCmplete.lblAccNo.text = frmIBSSApplyCnfrmtn.lblAccNo.text;
    frmIBSSApplyCmplete.lblAccType.text = frmIBSSApplyCnfrmtn.lblAccType.text;
    frmIBSSApplyCmplete.lbLAccName2.text = frmIBSSApplyCnfrmtn.lblCustName2.text;
    frmIBSSApplyCmplete.lblBal2.text = frmIBSSApplyCnfrmtn.lblBal2.text;
    //frmIBSSApplyCmplete.lblAcc2.text = frmIBSSApplyCnfrmtn.lblAcc2.text;
    frmIBSSApplyCmplete.lblAccNo2.text = frmIBSSApplyCnfrmtn.lblAccNo2.text;
    frmIBSSApplyCmplete.lblAccType2.text = frmIBSSApplyCnfrmtn.lblAccType2.text;
    frmIBSSApplyCmplete.lblAmntLmtMax.text = frmIBSSApplyCnfrmtn.lblAmntLmtMax.text;
    frmIBSSApplyCmplete.lblAmntLmtMin.text = frmIBSSApplyCnfrmtn.lblAmntLmtMin.text;
    frmIBSSApplyCmplete.image247502979411375.src = frmIBSSApplyCnfrmtn.imgFrom.src;
    frmIBSSApplyCmplete.image2448366816169765.src = frmIBSSApplyCnfrmtn.imgNoFix.src;
    frmIBSSApplyCmplete.hbxBPSave.isVisible = true;
    frmIBSSApplyCmplete.show();
}
/** TODO: Navigates to dashboard after completion of S2S apply **/
function onClickReturnS2SForIB() {
    onclickActivationCompleteStart();
}
/** On click on confirm during deletion of apply s2s **/
function ssDeleteConfrmForIB() {
    checkBusinessHoursForIBS2SEditDel();
}
/** to cancel delete request of s2s */
function ssDeleteCancelForIB() {
    popupDeleAccnt.dismiss();
}

function showDeletepopForIBS2S(imgIcon, confText, callBackOnConfirm, callBackCancel) {
    popupDeleAccnt.image244747680938084.src = imgIcon;
    popupDeleAccnt.lblConfoMsg.text = kony.i18n.getLocalizedString("S2S_delSureMsgIB");
    popupDeleAccnt.button44747680938122.onClick = callBackOnConfirm;
    popupDeleAccnt.button44914510567703.onClick = callBackCancel;
    popupDeleAccnt.button44914510567703.text = kony.i18n.getLocalizedString("keyCancelButton");
    popupDeleAccnt.button44747680938122.text = kony.i18n.getLocalizedString("keyYes");
    popupDeleAccnt.show();
}

function checkBusinessHoursForIBS2SEditDel() {
    //Loading Screen function
    var inputParams = {
        myProfileFlag: "false"
    };
    invokeServiceSecureAsync("checkS2SBusinessHours", inputParams, checkBusinessHoursForIBS2SEditDelCB);
}

function checkBusinessHoursForIBS2SEditDelCB(status, collectionData) {
    if (status == 400) {
        if (collectionData["opstatus"] == 0) {
            //collectionData["s2sBusinessHrsFlag"] = "true"
            if (collectionData["s2sBusinessHrsFlag"] == "true") {
                if (gblSSServieHours == "deleteS2S") {
                    gblSSServieHours = "dummyXYZ"; //assigning this value to avoid any conflict 
                    retailLiquidityDeleteForIB(); // this is for delete S2S flow, onClick of delete icon, we r giving value of gblSSApply = "delete"
                } else if (gblSSServieHours == "modifyS2S") {
                    gblSSServieHours = "dummyABX"; //assigning this value to avoid any conflict 
                    showOTPPopup(kony.i18n.getLocalizedString("TransactionPass") + ":", "", "", onClickSSConfrm, 3);
                } else if (gblSSServieHours == "S2SEdit") {
                    //frmIBSSApply.lblFrmMsg1.text = kony.i18n.getLocalizedString("S2S_FromExeedsMsg");
                    //frmIBSSApply.lblToMsg1.text = kony.i18n.getLocalizedString("S2S_ToBelowMsg");												
                    frmIBSSApply.txtBalMax.text = frmIBSSSDetails.lblAmntLmtMax.text + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                    frmIBSSApply.txtBalMin.text = frmIBSSSDetails.lblAmntLmtMin.text + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                    frmIBSSApply.imgLinkedAcct.src = frmIBSSSDetails.image247502979411375.src;
                    frmIBSSApply.lblLinkedAcctNickName.text = frmIBSSSDetails.lblCustName.text;
                    frmIBSSApply.lblLinkdBal.text = frmIBSSSDetails.lblBalance.text
                    frmIBSSApply.lblAcctNoValue.text = frmIBSSSDetails.lblAccNo.text;
                    frmIBSSApply.lblLinkedAcctTitle.text = frmIBSSSDetails.lblAccType.text;
                    frmIBSSApply.imgNoFixedAcct.src = frmIBSSSDetails.image2448366816169765.src;
                    frmIBSSApply.lblBal2.text = frmIBSSSDetails.lblBal2.text
                    frmIBSSApply.lblCustName2.text = frmIBSSSDetails.lblCustName2.text;
                    frmIBSSApply.lblAccNo2.text = frmIBSSSDetails.lblAccNo2.text;
                    frmIBSSApply.lblAccType2.text = frmIBSSSDetails.lblAccType2.text;
                    frmIBSSApply.vbxSSRight.setVisibility(true);
                    frmIBSSApply.hboxLinkedAcct.setVisibility(true);
                    frmIBSSApply.hbxCancelSave.setVisibility(true);
                    frmIBSSApply.hbxNext.setVisibility(false);
                    //gblSSServieHours = "S2SAuthenticated";
                    gblSSServieHours = "S2SEdit";
                    frmIBSSApply.imgXferArrowFrom.setVisibility(false);
                    frmIBSSApply.hbxSSApply.skin = "hbxIBSizeLimiter";
                    frmIBSSApply.hbxTMBImg.setVisibility(true);
                    frmIBSSApply.vbxSSRight.skin = "vbxRightColumn";
                    frmIBSSApply.customFromAccountIB.setVisibility(false);
                    frmIBSSApply.show();
                } else if (gblSSServieHours == "s2sExecuteTransfer") {
                    gblSSServieHours = "dummyS2SExecuteTransfer";
                    showOTPPopup(kony.i18n.getLocalizedString("TransactionPass") + ":", "", "", onClickSSConfrm, 3);
                }
            } else {
                s2s_ib_activityStatus = "01";
                activityLogServiceCall(s2s_ib_activityTypeID, s2s_ib_errorCode, s2s_ib_activityStatus, s2s_ib_deviceNickName, s2s_ib_activityFlexValues1, s2s_ib_activityFlexValues2, s2s_ib_activityFlexValues3, s2s_ib_activityFlexValues4, s2s_ib_activityFlexValues5, s2s_ib_logLinkageId);
                showAlert("Operation can be executed successfully only within service hours", "info");
                return false;
            }
        } else {
            s2s_ib_activityStatus = "02";
            activityLogServiceCall(s2s_ib_activityTypeID, s2s_ib_errorCode, s2s_ib_activityStatus, s2s_ib_deviceNickName, s2s_ib_activityFlexValues1, s2s_ib_activityFlexValues2, s2s_ib_activityFlexValues3, s2s_ib_activityFlexValues4, s2s_ib_activityFlexValues5, s2s_ib_logLinkageId);
            showAlert("Operation can be executed successfully only within service hours", "info");
        }
    }
}
/*
 * function to delete S2S service
 */
function retailLiquidityDeleteForIB() {
    showLoadingScreenPopup();
    var patryId = removeUnwatedSymbols(frmIBSSSDetails.lblAccNo2.text);
    inputParam = {};
    inputParam["PartyId"] = patryId; //Main-account or No fixed Account
    inputParam["activityFlexValues1"] = gblPHONENUMBER;
    inputParam["activityFlexValues2"] = ibLinkdAcct;
    inputParam["activityFlexValues3"] = ibNoFixedAcct;
    inputParam["activityFlexValues4"] = ibLiqInqCollectionData["maxAmt"];
    inputParam["activityFlexValues5"] = ibLiqInqCollectionData["minAmt"];
    invokeServiceSecureAsync("RetailLiquidityDelete", inputParam, retailLiquidityDeleteCBForIB)
}

function retailLiquidityDeleteCBForIB(status, resultTbl) {
    popupDeleAccnt.dismiss();
    if (status == 400) {
        if (resultTbl["opstatus"] == 0) {
            dismissLoadingScreenPopup();
            if (resultTbl["statusCode"] == 0) {
                //TMBUtil.DestroyForm(frmIBSSApply);
                eligibleS2SAcctIBInfo = "";
                s2s_ib_activityStatus = "01";
                //activityLogServiceCall(s2s_ib_activityTypeID, s2s_ib_errorCode, s2s_ib_activityStatus, s2s_ib_deviceNickName, s2s_ib_activityFlexValues1, s2s_ib_activityFlexValues2, s2s_ib_activityFlexValues3, s2s_ib_activityFlexValues4, s2s_ib_activityFlexValues5, s2s_ib_logLinkageId);
                frmIBSSApply.lblAcctNoValue.text = "";
                frmIBSSApply.lblLinkedAcctNickName.text = "";
                frmIBSSApply.lblLinkdBal.text = "";
                frmIBSSApply.lblLinkedAcctTitle.text = "";
                frmIBSSApply.lblAcctNo1.text = "";
                frmIBSSApply.hbxCancelSave.setVisibility(false);
                frmIBSSApply.hbxNext.setVisibility(true);
                frmIBSSApply.hboxLinkedAcct.setVisibility(false);
                //to show dashboard screen
                onclickActivationCompleteStart();
                //popupDeleAccnt.dismiss();
            }
        }
    } else {
        dismissLoadingScreenPopup();
        s2s_ib_activityStatus = "02";
        //activityLogServiceCall(s2s_ib_activityTypeID, s2s_ib_errorCode, s2s_ib_activityStatus, s2s_ib_deviceNickName, s2s_ib_activityFlexValues1, s2s_ib_activityFlexValues2, s2s_ib_activityFlexValues3, s2s_ib_activityFlexValues4, s2s_ib_activityFlexValues5, s2s_ib_logLinkageId);
        alert(resultTbl["errMsg"])
        return false;
    }
}
/** This method is used to get tokenValueForS2S **/
function getTokenStatus() {
    var inputParams = {
        crmId: ""
    };
    invokeServiceSecureAsync("crmProfileInqForS2S", inputParams, callBackForTokenStatus);
}

function callBackForTokenStatus(status, collectionData) {
    if (status == 400) //success responce
    {
        if (collectionData["opstatus"] == 0) {
            if (collectionData["statusCode"] != 0) {
                kony.application.dismissLoadingScreen();
                s2s_ib_activityStatus = "02";
                s2s_ib_errorCode = collectionData["addStatusCode"];
                activityLogServiceCall(s2s_ib_activityTypeID, s2s_ib_errorCode, s2s_ib_activityStatus, s2s_ib_deviceNickName, s2s_ib_activityFlexValues1, s2s_ib_activityFlexValues2, s2s_ib_activityFlexValues3, s2s_ib_activityFlexValues4, s2s_ib_activityFlexValues5, s2s_ib_logLinkageId);
                showAlert(collectionData["addStatusDesc"], kony.i18n.getLocalizedString("info"));
                return false;
            } else {
                tokenValueForS2S = collectionData["tokenFlag"];
            }
        } else {
            s2s_ib_activityStatus = "02";
            s2s_ib_errorCode = collectionData["addStatusCode"];
            activityLogServiceCall(s2s_ib_activityTypeID, s2s_ib_errorCode, s2s_ib_activityStatus, s2s_ib_deviceNickName, s2s_ib_activityFlexValues1, s2s_ib_activityFlexValues2, s2s_ib_activityFlexValues3, s2s_ib_activityFlexValues4, s2s_ib_activityFlexValues5, s2s_ib_logLinkageId);
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}
/*
 * function to update or edit Max and Min limit to use S2S service
 */
function setRetailLiquidityModInputParam(inputParam) {
    var txtBalMaxVal = frmIBSSApplyCnfrmtn.lblAmntLmtMax.text;
    if (txtBalMaxVal.indexOf(",") != -1) txtBalMaxVal = replaceCommon(txtBalMaxVal, ",", "");
    txtBalMaxVal = txtBalMaxVal.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    var txtBalMinVal = frmIBSSApplyCnfrmtn.lblAmntLmtMin.text;
    if (txtBalMinVal.indexOf(",") != -1) txtBalMinVal = replaceCommon(txtBalMinVal, ",", "");
    txtBalMinVal = txtBalMinVal.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    inputParam["retailLiquidityMod_acctIdentValue"] = ibLinkdAcct;
    inputParam["retailLiquidityMod_partyId"] = ibNoFixedAcct;
    inputParam["retailLiquidityMod_maxAmt"] = txtBalMaxVal; //removeCommas(txtBalMaxVal);	
    inputParam["retailLiquidityMod_minAmt"] = txtBalMinVal; //removeCommas(txtBalMinVal);	
}

function onClickCallBackFunctionIB() {
    addMyAccntFT();
}
// if gblExeS2S == true then execution will be started. call getIBStatus().
/*
 * Function to execute S2S trasfer amount, [screen: SSSExecute]
 */
function invokeRetailLiquidityTransferInqForIB() {
    showLoadingScreenPopup();
    invokeServiceSecureAsync("RetailLiquidityTransferInquiry", {}, callBackRetailLiqTransInqForIB)
}

function callBackRetailLiqTransInqForIB(status, resultTbl) {
    if (status == 400) //success responce
    {
        if (resultTbl["opstatus"] == 0) {
            var StatusCode = resultTbl["StatusCode"];
            if (StatusCode != "0") {
                dismissLoadingScreenPopup();
                alert(" " + resultTbl["errMsg"]);
                return false;
            } else {
                retailLiqTranInqRsltIB = resultTbl;
                // to fetch account nickname and account type
                ibLinkdAcct = resultTbl["FromAcctNo"];
                ibNoFixedAcct = resultTbl["ToAcctNo"];
                getCustomerAccountForIBInfo();
            }
        } else {
            dismissLoadingScreenPopup();
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}

function finActivityLogForIB() {
    var inputParam = {}
    inputParam["transRefType"] = "SS";
    invokeServiceSecureAsync("generateTransferRefNo", inputParam, callBackgetS2STransRefNoIB)
}

function callBackgetS2STransRefNoIB(status, collectionData) {
    if (status == 400) {
        if (collectionData["opstatus"] == 0) {
            if (collectionData["errCode"] == "") {
                transRefNumForS2SIB = collectionData["transRefNum"];
                s2sExeFormMappingForIB(transRefNumForS2SIB);
            }
        } else {
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        }
    }
}

function s2sExeFormMappingForIB(transRefNumForS2SIB) {
    var resultTbl = retailLiqTranInqRsltIB;
    //Date format 2013-07-30 to 30/07/13	
    var date = resultTbl["TransferDate"];
    var dateSplit = date.split("-");
    var formattedDate = dateSplit[2] + "/" + dateSplit[1] + "/" + dateSplit[0].substring(dateSplit[0].length - 2, dateSplit[0].length);
    frmIBSSSExcuteTrnsfr.lblCustName.text = exeIBFromAcctName;
    frmIBSSSExcuteTrnsfr.lblAcctType.text = exeIBFromAcctType;
    frmIBSSSExcuteTrnsfr.lblAccNo.text = formatAccountNo(resultTbl["FromAcctNo"]);
    frmIBSSSExcuteTrnsfr.lblBeforeBal.text = commaFormatted(resultTbl["FromAmt"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
    frmIBSSSExcuteTrnsfr.lblCustName2.text = exeIBToAcctName;
    frmIBSSSExcuteTrnsfr.lblBal2.text = commaFormatted(resultTbl["ToAmt"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
    frmIBSSSExcuteTrnsfr.lblAccNo2.text = formatAccountNo(resultTbl["ToAcctNo"]);
    frmIBSSSExcuteTrnsfr.lblAccType2.text = exeIBToAcctType;
    frmIBSSSExcuteTrnsfr.lblAmntLmtMax.text = commaFormatted(resultTbl["MaxCurAmount"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
    frmIBSSSExcuteTrnsfr.lblAmntLmtMin.text = commaFormatted(resultTbl["MinCurAmt"]) + kony.i18n.getLocalizedString("currencyThaiBaht");
    frmIBSSSExcuteTrnsfr.lblTrnsAmnt.text = commaFormatted(resultTbl["TranAmt"]) + kony.i18n.getLocalizedString("currencyThaiBaht");
    //frmIBSSSExcuteTrnsfr.lblTrnDtMsg.text = kony.i18n.getLocalizedString("S2S_TransactionDt");
    frmIBSSSExcuteTrnsfr.lblTransDate.text = gblCurrentDateS2S;
    frmIBSSSExcuteTrnsfr.lblTrnsRefNo.text = transRefNumForS2SIB + "00"; //resultTbl["TrnId"];
    dismissLoadingScreenPopup();
    frmIBSSSExcuteTrnsfr.show();
}
/*function financialActivityLogForS2SIB(CRMFinancialActivityLogAddRequest){
	inputParam = {};
	inputParam["crmId"] = CRMFinancialActivityLogAddRequest.crmId;
	inputParam["finTxnRefId"] = CRMFinancialActivityLogAddRequest.finTxnRefId;
	inputParam["finTxnDate"] = CRMFinancialActivityLogAddRequest.finTxnDate;
	inputParam["activityTypeId"] = CRMFinancialActivityLogAddRequest.activityTypeId;
	inputParam["txnCd"] = CRMFinancialActivityLogAddRequest.txnCd;
	inputParam["tellerId"] = CRMFinancialActivityLogAddRequest.tellerId;
	inputParam["txnDescription"] = CRMFinancialActivityLogAddRequest.txnDescription;
	inputParam["finLinkageId"] = CRMFinancialActivityLogAddRequest.finLinkageId;
	inputParam["channelId"] = CRMFinancialActivityLogAddRequest.channelId;
	inputParam["fromAcctId"] = CRMFinancialActivityLogAddRequest.fromAcctId;
	inputParam["toAcctId"] = CRMFinancialActivityLogAddRequest.toAcctId;
	inputParam["toBankAcctCd"] = CRMFinancialActivityLogAddRequest.toBankAcctCd;
	inputParam["finTxnAmount"] = CRMFinancialActivityLogAddRequest.finTxnAmount;
	inputParam["finTxnFee"] = CRMFinancialActivityLogAddRequest.finTxnFee;
	inputParam["finTxnBalance"] = CRMFinancialActivityLogAddRequest.finTxnBalance;
	inputParam["finTxnMemo"] = CRMFinancialActivityLogAddRequest.finTxnMemo;
	inputParam["billerRef1"] = CRMFinancialActivityLogAddRequest.billerRef1;
	inputParam["billerRef2"] = CRMFinancialActivityLogAddRequest.billerRef2;
	inputParam["noteToRecipient"] = CRMFinancialActivityLogAddRequest.noteToRecipient;
	inputParam["recipientMobile"] = CRMFinancialActivityLogAddRequest.recipientMobile;
	inputParam["recipientEmail"] = CRMFinancialActivityLogAddRequest.recipientEmail;
	inputParam["finTxnStatus"] = CRMFinancialActivityLogAddRequest.finTxnStatus;
	inputParam["smartFlag"] = CRMFinancialActivityLogAddRequest.smartFlag;
	inputParam["clearingStatus"] = CRMFinancialActivityLogAddRequest.clearingStatus;
	inputParam["finSchduleRefId"] = CRMFinancialActivityLogAddRequest.finSchduleRefId;
	inputParam["billerCommCode"] = CRMFinancialActivityLogAddRequest.billerCommCode;
	inputParam["fromAcctName"] = CRMFinancialActivityLogAddRequest.fromAcctName;
	inputParam["fromAcctNickname"] = CRMFinancialActivityLogAddRequest.fromAcctNickname;
	inputParam["toAcctName"] = CRMFinancialActivityLogAddRequest.toAcctName;
	inputParam["toAcctNickname"] = CRMFinancialActivityLogAddRequest.toAcctNickname;
	inputParam["billerCustomerName"] = CRMFinancialActivityLogAddRequest.billerCustomerName;
	inputParam["billerBalance"] = CRMFinancialActivityLogAddRequest.billerBalance;
	inputParam["activityRefId"] = CRMFinancialActivityLogAddRequest.activityRefId;
	inputParam["eventId"] = CRMFinancialActivityLogAddRequest.eventId;
	inputParam["errorCd"] = CRMFinancialActivityLogAddRequest.errorCd;
	inputParam["txnType"] = CRMFinancialActivityLogAddRequest.txnType;
	
	
	inputParam["tdInterestAmount"] = CRMFinancialActivityLogAddRequest.tdInterestAmount;
	inputParam["tdTaxAmount"] = CRMFinancialActivityLogAddRequest.tdTaxAmount;
	inputParam["tdPenaltyAmount"] = CRMFinancialActivityLogAddRequest.tdPenaltyAmount;
	inputParam["tdNetAmount"] = CRMFinancialActivityLogAddRequest.tdNetAmount;
	inputParam["tdMaturityDate"] = CRMFinancialActivityLogAddRequest.tdMaturityDate;
	inputParam["remainingFreeTxn"] = CRMFinancialActivityLogAddRequest.remainingFreeTxn;
	inputParam["toAccountBalance"] = CRMFinancialActivityLogAddRequest.toAccountBalance;
	inputParam["sendToSavePoint"] = CRMFinancialActivityLogAddRequest.sendToSavePoint;
	inputParam["openTdTerm"] = CRMFinancialActivityLogAddRequest.openTdTerm;
	inputParam["openTdInterestRate"] = CRMFinancialActivityLogAddRequest.openTdInterestRate;
	inputParam["openTdMaturityDate"] = CRMFinancialActivityLogAddRequest.openTdMaturityDate;
	inputParam["affiliatedAcctNickname"] = CRMFinancialActivityLogAddRequest.affiliatedAcctNickname;
	inputParam["affiliatedAcctId"] = CRMFinancialActivityLogAddRequest.affiliatedAcctId;
	inputParam["beneficialFirstname"] = CRMFinancialActivityLogAddRequest.beneficialFirstname;
	inputParam["beneficialLastname"] = CRMFinancialActivityLogAddRequest.beneficialLastname;
	inputParam["relationship"] = CRMFinancialActivityLogAddRequest.relationship;
	inputParam["percentage"] = CRMFinancialActivityLogAddRequest.percentage;
	inputParam["finFlexValues1"] = CRMFinancialActivityLogAddRequest.finFlexValues1;
	inputParam["finFlexValues2"] = CRMFinancialActivityLogAddRequest.finFlexValues2;
	inputParam["finFlexValues3"] = CRMFinancialActivityLogAddRequest.finFlexValues3;
	inputParam["finFlexValues4"] = CRMFinancialActivityLogAddRequest.finFlexValues4;
	inputParam["finFlexValues5"] = CRMFinancialActivityLogAddRequest.finFlexValues5;
	inputParam["dueDate"] = CRMFinancialActivityLogAddRequest.dueDate;
	inputParam["beepAndBillTxnId"] = CRMFinancialActivityLogAddRequest.beepAndBillTxnId;
	
	invokeServiceSecureAsync("financialActivityLog", inputParam, callBackFinActivityLogForS2SIB)
}*/
/*function callBackFinActivityLogForS2SIB(status, resulttable) {
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			sendNotificationForIBExecute();
			frmIBSSSExcuteTrnsfrCmplete.show();
			TMBUtil.DestroyForm(frmIBSSSExcuteTrnsfr);
		} else {
            showAlert(kony.i18n.getLocalizedString("ECGenericError"),kony.i18n.getLocalizedString("info"));
        }
	}
}*/
function callApplyS2SIBActivityLog() {
    s2s_ib_activityTypeID = "035";
    s2s_ib_errorCode = "";
    s2s_ib_activityStatus = "00";
    s2s_ib_deviceNickName = "";
    s2s_ib_activityFlexValues1 = gblPHONENUMBER;
    var txtBalMax = frmIBSSApply.txtBalMax.text
    txtBalMax = txtBalMax.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    var txtBalMin = frmIBSSApply.txtBalMin.text
    txtBalMin = txtBalMin.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    var selectedItem = frmIBSSApply.customFromAccountIB.selectedItem;
    var selectedData = frmIBSSApply.customFromAccountIB.data[gblCWSelectedItem];
    var linkedAcct = selectedData.lblActNoval;
    linkedAcct = linkedAcct.replace(/-/g, "");
    s2s_ib_activityFlexValues2 = linkedAcct;
    s2s_ib_activityFlexValues3 = (frmIBSSApply.lblAccNo2.text).replace(/-/g, "");
    s2s_ib_activityFlexValues4 = removeCommas(txtBalMax);
    s2s_ib_activityFlexValues5 = removeCommas(txtBalMin);
    s2s_ib_logLinkageId = "";
    activityLogServiceCall(s2s_ib_activityTypeID, s2s_ib_errorCode, s2s_ib_activityStatus, s2s_ib_deviceNickName, s2s_ib_activityFlexValues1, s2s_ib_activityFlexValues2, s2s_ib_activityFlexValues3, s2s_ib_activityFlexValues4, s2s_ib_activityFlexValues5, s2s_ib_logLinkageId);
}

function callApplyS2SIBActivityLogForEdit() {
    s2s_ib_activityTypeID = "037";
    s2s_ib_errorCode = "";
    s2s_ib_activityStatus = "00";
    s2s_ib_deviceNickName = "";
    s2s_ib_activityFlexValues1 = gblPHONENUMBER;
    var txtBalMax = frmIBSSApplyCnfrmtn.lblAmntLmtMax.text
    txtBalMax = txtBalMax.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    var txtBalMin = frmIBSSApplyCnfrmtn.lblAmntLmtMin.text;
    txtBalMin = txtBalMin.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    //var selectedItem = frmIBSSApply.customFromAccountIB.selectedItem;
    //var selectedData = frmIBSSApply.customFromAccountIB.data[selectedItem];
    //var linkedAcct = selectedData.lblActNoval;
    var linkedAcct = frmIBSSApplyCnfrmtn.lblAccNo.text
    linkedAcct = linkedAcct.replace(/-/g, "");
    s2s_ib_activityFlexValues2 = linkedAcct;
    s2s_ib_activityFlexValues3 = (frmIBSSApplyCnfrmtn.lblAccNo2.text).replace(/-/g, "");
    s2s_ib_activityFlexValues4 = removeCommas(txtBalMax);
    s2s_ib_activityFlexValues5 = removeCommas(txtBalMin);
    s2s_ib_logLinkageId = "";
    //alert(s2s_ib_activityFlexValues2+s2s_ib_activityFlexValues3+s2s_ib_activityFlexValues4+s2s_ib_activityFlexValues5)
    activityLogServiceCall(s2s_ib_activityTypeID, s2s_ib_errorCode, s2s_ib_activityStatus, s2s_ib_deviceNickName, s2s_ib_activityFlexValues1, s2s_ib_activityFlexValues2, s2s_ib_activityFlexValues3, s2s_ib_activityFlexValues4, s2s_ib_activityFlexValues5, s2s_ib_logLinkageId);
}

function onClickGeneratePDFImageForS2SApplyComp(filetype) {
    var inputParam = {};
    var scheduleDetails = "";
    var pdfImagedata = {};
    fileType = filetype;
    inputParam["outputtemplatename"] = "SendToSaveIB_Details_" + kony.os.date("DDMMYYYY");
    var pdfImagedata = {
        "localeId": kony.i18n.getCurrentLocale(),
        "fromAcctNo": frmIBSSApplyCmplete.lblAccNo.text,
        "fromAcctName": frmIBSSApplyCmplete.lblCustName.text,
        "toAcctNo": frmIBSSApplyCmplete.lblAccNo2.text,
        "toAcctName": frmIBSSApplyCmplete.lbLAccName2.text,
        "transferToNoFix": frmIBSSApplyCmplete.lblAmntLmtMax.text,
        "transferFromNoFix": frmIBSSApplyCmplete.lblAmntLmtMin.text
    }
    inputParam["filetype"] = fileType;
    inputParam["templatename"] = "ApplyS2S";
    inputParam["datajson"] = JSON.stringify(pdfImagedata, "", "");
    invokeServiceSecureAsync("generatePdfImage", inputParam, ibRenderFileCallbackfunction);
    // var url = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext +"/GenericPdfImageServlet";
    //kony.net.invokeServiceAsync(url, inputParam, ibRenderFileCallbackfunction);
}

function onClickGeneratePDFImageForS2SExecution(filetype) {
    var inputParam = {};
    var scheduleDetails = "";
    var pdfImagedata = {};
    fileType = filetype;
    //frmIBSSSExcuteTrnsfrCmplete.lblTransDate.text
    var pdfImagedata = {
        "localeId": kony.i18n.getCurrentLocale(),
        "fromAcctNo": frmIBSSSExcuteTrnsfrCmplete.lblAccNo.text,
        "fromAcctName": frmIBSSSExcuteTrnsfrCmplete.lblCustName.text,
        "toAcctNo": frmIBSSSExcuteTrnsfrCmplete.lblAccNo2.text,
        "toAcctName": frmIBSSSExcuteTrnsfrCmplete.lbLAccName2.text,
        "bankName": kony.i18n.getLocalizedString("keylblTMBBank"),
        "amount": frmIBSSSExcuteTrnsfrCmplete.lblTrnsAmnt.text,
        "transDate": gblCurrentDateS2S,
        "transRef": frmIBSSSExcuteTrnsfrCmplete.lblTrnsRefNo.text
    };
    inputParam["filetype"] = fileType;
    inputParam["templatename"] = "SendToSaveDetails";
    inputParam["datajson"] = JSON.stringify(pdfImagedata, "", "");
    if (flowSpa) {
        inputParam["outputtemplatename"] = "SendToSaveExecuteCompleteIB_Details_" + kony.os.date("DDMMYYYY");
    } else {
        inputParam["outputtemplatename"] = "SendToSaveExecuteCompleteIB_Details_" + kony.os.date("DDMMYYYY");
    }
    invokeServiceSecureAsync("generatePdfImage", inputParam, ibRenderFileCallbackfunction);
    // var url = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext +"/GenericPdfImageServlet";
    //kony.net.invokeServiceAsync(url, inputParam, ibRenderFileCallbackfunction);
}

function shareMyFBS2SIB() {
    gblPOWcustNME = "lknk"
    gblPOWtransXN = "sf"
    gblPOWchannel = "IB"
    requestfromform = "frmIBSSSExcuteTrnsfrCmplete"
    postOnWall();
}

function shareMyFBS2SApplyIB() {
    gblPOWcustNME = "lknk"
    gblPOWtransXN = "sf"
    gblPOWchannel = "IB"
    requestfromform = "frmIBSSApplyCmplete"
    postOnWall();
}
/*function sendNotificationForIBExecute(){
    var maxAmount=(frmIBSSSExcuteTrnsfr.lblAmntLmtMax.text).replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    var minAmount=(frmIBSSSExcuteTrnsfr.lblAmntLmtMin.text).replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    
    var deliveryMethod = "Email";
	var inputParam ={}
	var locale = kony.i18n.getCurrentLocale();	
	inputParam["channelName"] ="IB-INQ";
	inputParam["Locale"]= kony.i18n.getCurrentLocale();//"en_US";
	inputParam["notificationSubject"] ="";
	inputParam["notificationContent"] ="";
	inputParam["emailId"]= gblEmailId;
	inputParam["customerName"] =gblCustomerName;
	inputParam["notificationType"] =deliveryMethod;
     inputParam["source"] = "sendEmailForS2SExecution";
	inputParam["lblAmntMax"] = removeCommas(maxAmount);
	inputParam["lblAmntMin"] = removeCommas(minAmount);
	inputParam["lblFromAccName"] = frmIBSSSExcuteTrnsfr.lblCustName.text;
	inputParam["lblFromAccNo"] = (frmIBSSSExcuteTrnsfr.lblAccNo.text).substring(6,11);
	inputParam["lblToAccName"] = frmIBSSSExcuteTrnsfr.lblCustName2.text;
	inputParam["lblToAccNo"] = (frmIBSSSExcuteTrnsfr.lblAccNo2.text).substring(6,11);
	inputParam["lblTransAmnt"] = frmIBSSSExcuteTrnsfr.lblTrnsAmnt.text; 
		inputParam["lblTransFee"] = " ";
		inputParam["lblDate"] = " ";
		inputParam["lblRefNo"] = frmIBSSSExcuteTrnsfr.lblTrnsRefNo.text;
	invokeServiceSecureAsync("NotificationAdd", inputParam, callBackS2SNotificationAddServiceIBExecute)	
}

function callBackS2SNotificationAddServiceIBExecute(status, result){
 if (status == 400) {
        if (result["opstatus"] == 0) {
            var StatusCode = result["StatusCode"];
            var Severity = result["Severity"];
            var StatusDesc = result["StatusDesc"];
            if (StatusCode == 0) {
            	
             }
          }
        } 
        
    }*/
function validAmntLmtIB(maxAmount, minAmount) {
    //	var maxAmount = frmSSService.txtBalMax.text;
    //	var minAmount = frmSSService.txtBalMin.text;
    if (maxAmount.indexOf(",") != -1) maxAmount = replaceCommon(maxAmount, ",", "");
    if (minAmount.indexOf(",") != -1) minAmount = replaceCommon(minAmount, ",", "");
    var decimalNo = /^[0-9]+(|.\d*[0-9])+$/g; // regex to test decimal no
    var decimalNo1 = /^[0-9]+(|.\d*[0-9])+$/g;
    var maxNum = decimalNo.test(maxAmount);
    var minNum = decimalNo1.test(minAmount);
    if (!maxNum) {
        showAlert(kony.i18n.getLocalizedString("keyValidMaxAmt"), kony.i18n.getLocalizedString("info"));
        return;
    }
    if (!minNum) {
        showAlert(kony.i18n.getLocalizedString("keyValidMinAmt"), kony.i18n.getLocalizedString("info"));
        return;
    }
    var maxAmnt = parseFloat(maxAmount);
    var minAmnt = parseFloat(minAmount);
    var maxAmntFlag = false;
    var minAmntFlag = false;
    var SSTrnsLmtMin = gblSSTrnsLmtMin; //toMinAmount
    if (SSTrnsLmtMin.indexOf(",") != -1) SSTrnsLmtMin = replaceCommon(SSTrnsLmtMin, ",", "");
    SSTrnsLmtMin = parseFloat(SSTrnsLmtMin.toString());
    var SSTrnMaxofMin = gblSSTrnMaxofMin; //fromMinAmount
    if (SSTrnMaxofMin.indexOf(",") != -1) SSTrnMaxofMin = replaceCommon(SSTrnMaxofMin, ",", "");
    SSTrnMaxofMin = parseFloat(SSTrnMaxofMin.toString());
    var SSTrnMinofMax = gblSSTrnMinofMax; //toMaxAmount
    if (SSTrnMinofMax.indexOf(",") != -1) SSTrnMinofMax = replaceCommon(SSTrnMinofMax, ",", "");
    SSTrnMinofMax = parseFloat(SSTrnMinofMax.toString());
    var SSTrnsLmtMax = gblSSTrnsLmtMax; //fromMaxAmount
    if (SSTrnsLmtMax.indexOf(",") != -1) SSTrnsLmtMax = replaceCommon(SSTrnsLmtMax, ",", "");
    SSTrnsLmtMax = parseFloat(SSTrnsLmtMax.toString());
    /*	
    	if( maxAmnt != null && maxAmnt >= SSTrnMinofMax && maxAmnt <= SSTrnsLmtMax){
    		maxAmntFlag = true;
    	}else{
    		showAlert("Maximum amount should be between "+SSTrnMinofMax + " to "+ SSTrnsLmtMax, info1);
    		return;
    	} 
    	
    	if( minAmnt != null && minAmnt >= SSTrnsLmtMin && minAmnt <= SSTrnMaxofMin){
    		minAmntFlag = true;
    	}else{
    		showAlert("Minimum amount should between "+ SSTrnsLmtMin +" to "+ SSTrnMaxofMin, info1);
    		return;
    	}	
    */
    if (maxAmnt != null && (maxAmnt >= SSTrnMaxofMin) && (maxAmnt <= SSTrnsLmtMax)) {
        maxAmntFlag = true;
    } else {
        showAlert(kony.i18n.getLocalizedString("keyValidMaxAmt"), kony.i18n.getLocalizedString("info"));
        return;
    }
    if (minAmnt != null && (minAmnt >= SSTrnsLmtMin) && (minAmnt <= SSTrnMinofMax)) {
        minAmntFlag = true;
    } else {
        showAlert(kony.i18n.getLocalizedString("keyValidMinAmt"), kony.i18n.getLocalizedString("info"));
        return;
    }
    if (maxAmnt <= minAmnt) {
        minAmntFlag = false;
        showAlert("Minimun amount should be less than Maximum Amount", kony.i18n.getLocalizedString("info"));
    }
    if (minAmntFlag && maxAmntFlag) {
        return true; // App level validation is passed, update the values in bank's DB
    }
}

function loadTermsNConditionForS2SIB() {
    showLoadingScreenPopup();
    var input_param = {};
    var locale = kony.i18n.getCurrentLocale();
    if (locale == "en_US") {
        input_param["localeCd"] = "en_US";
    } else {
        input_param["localeCd"] = "th_TH";
    }
    input_param["moduleKey"] = "SendtoSave";
    invokeServiceSecureAsync("readUTFFile", input_param, loadTNCS2SCallBackIB);
}

function loadTNCS2SCallBackIB(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
            //frmIBSSSTnC.lblSSSTnC.setVisibility(true);
            frmIBSSSTnC.lblSSSTnC.text = result["fileContent"];
            dismissLoadingScreenPopup()
        }
    }
    dismissLoadingScreenPopup()
}

function loadTermsNConditionForS2SIBforPrint() {
    var tncTitle = kony.i18n.getLocalizedString("keyTermsNConditions");
    var filecontent = frmIBSSSTnC.lblSSSTnC.text;
    try {
        showPopup(tncTitle, filecontent);
    } catch (Error) {
        alert("Please disable popupblker")
    }
}

function onClickpreConfirm() {
    if (gblSwitchToken == false && gblTokenSwitchFlag == false) {
        checkTokenFlagForS2S();
    }
    if (gblTokenSwitchFlag && gblSwitchToken == false) {
        frmIBSSApplyCnfrmtn.hbxToken.setVisibility(true);
        frmIBSSApplyCnfrmtn.hbox50639980413174.setVisibility(true);
        frmIBSSApplyCnfrmtn.keyEnterToken.setVisibility(true);
        frmIBSSApplyCnfrmtn.hbxOTP.setVisibility(false);
        frmIBSSApplyCnfrmtn.hbxCancelConf.setVisibility(false);
        frmIBSSApplyCnfrmtn.hbxDecisionOTP.setVisibility(true);
    } else if (gblTokenSwitchFlag == false && gblSwitchToken == true) {
        frmIBSSApplyCnfrmtn.hbxToken.setVisibility(false);
        frmIBSSApplyCnfrmtn.hbxOTP.setVisibility(true);
        startReqOTPApplyServicesS2SIB();
        gblTokenSwitchFlag = false;
        gblSwitchToken = true;
        frmIBSSApplyCnfrmtn.hbxCancelConf.setVisibility(false);
        frmIBSSApplyCnfrmtn.hbxDecisionOTP.setVisibility(true);
    }
}

function checkTokenFlagForS2S() {
    var inputParam = [];
    showLoadingScreenPopup();
    invokeServiceSecureAsync("tokenSwitching", inputParam, ibTokenFlagCallbackfunctionS2S);
}

function ibTokenFlagCallbackfunctionS2S(status, callbackResponse) {
    if (status == 400) {
        if (callbackResponse["opstatus"] == 0) {
            dismissLoadingScreenPopup();
            if (callbackResponse["deviceFlag"].length == 0) {
                //return
            }
            if (callbackResponse["deviceFlag"].length > 0) {
                var tokenFlag = callbackResponse["deviceFlag"][0]["TOKEN_DEVICE_FLAG"];
                var mediaPreference = callbackResponse["deviceFlag"][0]["MEDIA_PREFERENCE"];
                if (tokenFlag == "Y" && mediaPreference == "Token") {
                    frmIBSSApplyCnfrmtn.hbxToken.setVisibility(true);
                    frmIBSSApplyCnfrmtn.keyEnterToken.setVisibility(true);
                    frmIBSSApplyCnfrmtn.hbox50639980413174.setVisibility(true);
                    frmIBSSApplyCnfrmtn.hbxOTP.setVisibility(false);
                    frmIBSSApplyCnfrmtn.hbxCancelConf.setVisibility(false);
                    frmIBSSApplyCnfrmtn.hbxDecisionOTP.setVisibility(true);
                    gblTokenSwitchFlag = true;
                } else {
                    frmIBSSApplyCnfrmtn.hbxToken.setVisibility(false);
                    frmIBSSApplyCnfrmtn.hbxOTP.setVisibility(true);
                    startReqOTPApplyServicesS2SIB();
                    gblTokenSwitchFlag = false;
                    gblSwitchToken = true;
                    frmIBSSApplyCnfrmtn.hbxCancelConf.setVisibility(false);
                    frmIBSSApplyCnfrmtn.hbxDecisionOTP.setVisibility(true);
                    gblTokenSwitchFlag = false;
                }
            } else {
                frmIBSSApplyCnfrmtn.hbxToken.setVisibility(false);
                frmIBSSApplyCnfrmtn.hbxOTP.setVisibility(true);
                startReqOTPApplyServicesS2SIB();
                gblTokenSwitchFlag = false;
                gblSwitchToken = true;
                frmIBSSApplyCnfrmtn.hbxCancelConf.setVisibility(false);
                frmIBSSApplyCnfrmtn.hbxDecisionOTP.setVisibility(true);
                gblTokenSwitchFlag = false;
            }
        } else {
            dismissLoadingScreenPopup();
        }
    }
}

function onClickExecutePreConfirm() {
    inputParam = {};
    var Amount = frmIBSSSExcuteTrnsfr.lblTrnsAmnt.text;
    if (Amount.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht")) != -1) {
        Amount = Amount.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim();
    }
    inputParam["amount"] = Amount;
    inputParam["accountName"] = frmIBSSSExcuteTrnsfr.lblCustName2.text;
    inputParam["accountNum"] = "xxx-x-" + frmIBSSSExcuteTrnsfr.lblAccNo2.text.substring(6, 11) + "-x";
    inputParam["bankName"] = "TMB";
    inputParam["module"] = "execute";
    invokeServiceSecureAsync("ExecuteTransferValidationService", inputParam, sendToSaveSaveInSessionCallBack);
}

function sendToSaveSaveInSessionCallBack(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
            onClickExecutePreConfirmNext();
        }
    }
}

function onClickExecutePreConfirmNext() {
    gblExeS2S = true
    if (gblSwitchToken == false && gblTokenSwitchFlag == false) {
        checkS2SExecuteTokenFlag();
    }
    if (gblTokenSwitchFlag == true && gblSwitchToken == false) {
        frmIBSSSExcuteTrnsfr.hbxTokenEntry.setVisibility(true);
        frmIBSSSExcuteTrnsfr.hbxSSSTrnsfrOTP.setVisibility(false);
        frmIBSSSExcuteTrnsfr.hbxTrnsfrCCBtns.setVisibility(false);
        frmIBSSSExcuteTrnsfr.hbxSSSTrnsfrOTPBtns.setVisibility(true);
    } else if (gblTokenSwitchFlag == false && gblSwitchToken == true) {
        frmIBSSSExcuteTrnsfr.hbxTokenEntry.setVisibility(false);
        frmIBSSSExcuteTrnsfr.hbxSSSTrnsfrOTP.setVisibility(true);
        frmIBSSSExcuteTrnsfr.hbxTrnsfrCCBtns.setVisibility(false);
        frmIBSSSExcuteTrnsfr.hbxSSSTrnsfrOTPBtns.setVisibility(true);
        startReqOTPApplyServicesS2SIB();
    }
}

function checkS2SExecuteTokenFlag() {
    var inputParam = [];
    inputParam["crmId"] = gblcrmId;
    showLoadingScreen();
    invokeServiceSecureAsync("tokenSwitching", inputParam, ibS2SexecuteTokenFlagCallbackfunction);
}

function ibS2SexecuteTokenFlagCallbackfunction(status, callbackResponse) {
    if (status == 400) {
        if (callbackResponse["opstatus"] == 0) {
            dismissLoadingScreen();
            if (callbackResponse["deviceFlag"].length == 0) {
                //return
            }
            if (callbackResponse["deviceFlag"].length > 0) {
                var tokenFlag = callbackResponse["deviceFlag"][0]["TOKEN_DEVICE_FLAG"];
                var mediaPreference = callbackResponse["deviceFlag"][0]["MEDIA_PREFERENCE"];
                if (tokenFlag == "Y" && mediaPreference == "Token") {
                    frmIBSSSExcuteTrnsfr.hbxTokenEntry.setVisibility(true);
                    frmIBSSSExcuteTrnsfr.hbxSSSTrnsfrOTP.setVisibility(false);
                    gblTokenSwitchFlag = true;
                    frmIBSSSExcuteTrnsfr.hbxTrnsfrCCBtns.setVisibility(false);
                    frmIBSSSExcuteTrnsfr.hbxSSSTrnsfrOTPBtns.setVisibility(true);
                } else {
                    frmIBSSSExcuteTrnsfr.hbxTokenEntry.setVisibility(false);
                    frmIBSSSExcuteTrnsfr.hbxSSSTrnsfrOTP.setVisibility(true);
                    frmIBSSSExcuteTrnsfr.hbxTrnsfrCCBtns.setVisibility(false);
                    frmIBSSSExcuteTrnsfr.hbxSSSTrnsfrOTPBtns.setVisibility(true);
                    startReqOTPApplyServicesS2SIB();
                    gblTokenSwitchFlag = false;
                }
            } else {
                frmIBSSSExcuteTrnsfr.hbxTokenEntry.setVisibility(false);
                frmIBSSSExcuteTrnsfr.hbxSSSTrnsfrOTP.setVisibility(true);
                frmIBSSSExcuteTrnsfr.hbxTrnsfrCCBtns.setVisibility(false);
                frmIBSSSExcuteTrnsfr.hbxSSSTrnsfrOTPBtns.setVisibility(true);
                startReqOTPApplyServicesS2SIB();
            }
        }
    }
}

function callApplyS2SActivityLogForS2SExecutionInitialIB(s2s_ib_activityStatus) {
    s2s_ib_activityTypeID = "038";
    s2s_ib_errorCode = "";
    s2s_ib_activityStatus = s2s_ib_activityStatus;
    s2s_ib_deviceNickName = "";
    s2s_ib_activityFlexValues1 = gblPHONENUMBER;
    s2s_ib_activityFlexValues2 = removeUnwatedSymbols(frmIBSSSExcuteTrnsfr.lblAccNo.text);
    s2s_ib_activityFlexValues3 = removeUnwatedSymbols(frmIBSSSExcuteTrnsfr.lblAccNo2.text);
    s2s_ib_activityFlexValues4 = removeCommas(frmIBSSSExcuteTrnsfr.lblAmntLmtMax.text);
    //s2s_ib_activityFlexValues5 = removeCommas(frmIBSSSExcuteTrnsfr.lblAmntLmtMin.text);		
    s2s_ib_logLinkageId = "";
    activityLogServiceCall(s2s_ib_activityTypeID, s2s_ib_errorCode, s2s_ib_activityStatus, s2s_ib_deviceNickName, s2s_ib_activityFlexValues1, s2s_ib_activityFlexValues2, s2s_ib_activityFlexValues3, s2s_ib_activityFlexValues4, s2s_ib_activityFlexValues5, s2s_ib_logLinkageId);
}
//Function for Locale Change of cover flow
function frmS2SApplyLocaleChange() {
    getCustomerAccountForIBInfo_LC()
}

function getCustomerAccountForIBInfo_LC() {
    showLoadingScreenPopup();
    if (gbls2sEditFlag == "true") {
        var inputParams = {
            crmId: "",
            locale: kony.i18n.getCurrentLocale(),
            s2sEditFlag: gbls2sEditFlag,
            linkedAccount: ibLinkdAcct,
            noFixedAccount: ibNoFixedAcct,
            s2sExeFlag: gblExeS2S
        };
    } else {
        var inputParams = {
            crmId: "",
            locale: kony.i18n.getCurrentLocale(),
            s2sEditFlag: gbls2sEditFlag,
            s2sExeFlag: gblExeS2S
        };
    }
    invokeServiceSecureAsync("s2sCustomerAccInq", inputParams, startCustAccountInqIBServiceAsyncCallback_LC);
}

function startCustAccountInqIBServiceAsyncCallback_LC(status, collectionData) {
    if (status == 400) {
        if (collectionData["opstatus"] == 0) {
            var statusCode = collectionData["statusCode"];
            if (statusCode != 0) {
                dismissLoadingScreenPopup();
                showAlert(collectionData["acctStatusDesc"], kony.i18n.getLocalizedString("info"));
            } else {
                dismissLoadingScreenPopup();
                if (gbls2sEditFlag == "true") {
                    frmIBSSApply.lblAccType2.text = collectionData["lblToAccName"];
                    frmIBSSApply.lblCustName2.text = collectionData["lblToAccType"];
                    frmIBSSApply.lblLinkedAcctNickName.text = collectionData["lblFromAccName"];
                    frmIBSSApply.lblLinkedAcctTitle.text = collectionData["lblFromAccType"];
                } else {
                    eligibleS2SAcctIBInfo = collectionData["eligibleS2SAcctInfo"];
                    noFixedAcctIBInfo = collectionData["noFixedAcctInfo"];
                    s2sVerticalCoverFlow();
                    s2sNoFixedAcctPopulateIB();
                    getMinMaxAmountsForIB()
                    if (frmIBSSApply.hboxLinkedAcct.isVisible == true) {
                        //frmIBSSApply.lblLinkedAcctNickName.text = selectedData.lblCustName;
                        //frmIBSSApply.lblLinkedAcctTitle.text = selectedData.lblProductVal;
                        var locale = kony.i18n.getCurrentLocale();
                        if (locale == "en_US") {
                            if (selectedData.hiddenBranchEN == null || selectedData.hiddenBranchEN == '') frmIBSSApply.lblLinkedAcctNickName.text = selectedData.lblCustName;
                            else frmIBSSApply.lblLinkedAcctNickName.text = selectedData.hiddenBranchEN;
                            frmIBSSApply.lblLinkedAcctTitle.text = selectedData.lblCustNameEN;
                        } else {
                            if (selectedData.hiddenBranchTH == null || selectedData.hiddenBranchTH == '') frmIBSSApply.lblLinkedAcctNickName.text = selectedData.lblCustName;
                            else frmIBSSApply.lblLinkedAcctNickName.text = selectedData.hiddenBranchTH;
                            frmIBSSApply.lblLinkedAcctTitle.text = selectedData.lblCustNameTH;
                        }
                    }
                    frmIBSSApply.lblAccType2.text = noFixedAcctIBInfo[0]["productName"]
                }
                frmIBSSApply.show();
            }
        }
    }
}

function getCustomerAccountForIBInfo_LCforEdit() {
    showLoadingScreenPopup();
    var inputParams = {
        crmId: "",
        locale: kony.i18n.getCurrentLocale(),
        s2sEditFlag: gbls2sEditFlag,
        linkedAccount: ibLinkdAcct,
        noFixedAccount: ibNoFixedAcct,
        s2sExeFlag: gblExeS2S
    };
    invokeServiceSecureAsync("s2sCustomerAccInq", inputParams, startCustAccountInqIBServiceAsyncCallback_LCForEdit);
}

function startCustAccountInqIBServiceAsyncCallback_LCForEdit(status, collectionData) {
    if (status == 400) {
        if (collectionData["opstatus"] == 0) {
            var statusCode = collectionData["statusCode"];
            if (statusCode != 0) {
                dismissLoadingScreenPopup();
                showAlert(collectionData["acctStatusDesc"], kony.i18n.getLocalizedString("info"));
            } else {
                dismissLoadingScreenPopup();
                frmIBSSSDetails.lblCustName.text = collectionData["lblFromAccName"];
                frmIBSSSDetails.lblAccType.text = collectionData["lblFromAccType"];
                frmIBSSSDetails.lblCustName2.text = collectionData["lblToAccName"];
                frmIBSSSDetails.lblAccType2.text = collectionData["lblToAccType"];
                frmIBSSSDetails.show();
            }
        }
    }
}

function changeLocaleApplyConfirm() {
    var locale = kony.i18n.getCurrentLocale();
    if (gbls2sEditFlag == "true") {
        if (locale == "en_US") {
            if (collectionData["lblFromAccNameEN"] == null || collectionData["lblFromAccNameEN"] == '') frmIBSSApplyCnfrmtn.lblCustName.text = collectionData["lblFromAccName"]
            else frmIBSSApplyCnfrmtn.lblCustName.text = collectionData["lblFromAccNameEN"];
            if (collectionData["lblFromAccTypeEN"] == null || collectionData["lblFromAccTypeEN"] == '') frmIBSSApplyCnfrmtn.lblAccType.text = collectionData["lblFromAccType"];
            else frmIBSSApplyCnfrmtn.lblAccType.text = collectionData["lblFromAccTypeEN"];
            if (collectionData["lblToAccNameEN"] == null || collectionData["lblToAccNameEN"] == '') frmIBSSApplyCnfrmtn.lblCustName2.text = collectionData["lblToAccName"];
            else frmIBSSApplyCnfrmtn.lblCustName2.text = collectionData["lblToAccNameEN"];
            if (collectionData["lblToAccTypeEN"] == null || collectionData["lblToAccTypeEN"] == '') frmIBSSApplyCnfrmtn.lblAccType2.text = collectionData["lblToAccType"];
            else frmIBSSApplyCnfrmtn.lblAccType2.text = collectionData["lblToAccTypeEN"];
        } else {
            if (collectionData["lblFromAccNameTH"] == null || collectionData["lblFromAccNameTH"] == '') frmIBSSApplyCnfrmtn.lblCustName.text = collectionData["lblFromAccName"]
            else frmIBSSApplyCnfrmtn.lblCustName.text = collectionData["lblFromAccNameTH"];
            if (collectionData["lblFromAccTypeTH"] == null || collectionData["lblFromAccTypeTH"] == '') frmIBSSApplyCnfrmtn.lblAccType.text = collectionData["lblFromAccType"];
            else frmIBSSApplyCnfrmtn.lblAccType.text = collectionData["lblFromAccTypeTH"];
            if (collectionData["lblToAccNameTH"] == null || collectionData["lblToAccNameTH"] == '') frmIBSSApplyCnfrmtn.lblCustName2.text = collectionData["lblToAccName"];
            else frmIBSSApplyCnfrmtn.lblCustName2.text = collectionData["lblToAccNameTH"];
            if (collectionData["lblToAccTypeTH"] == null || collectionData["lblToAccTypeTH"] == '') frmIBSSApplyCnfrmtn.lblAccType2.text = collectionData["lblToAccType"];
            else frmIBSSApplyCnfrmtn.lblAccType2.text = collectionData["lblToAccTypeTH"];
        }
    } else {
        if (locale == "en_US") {
            if (selectedData.hiddenBranchEN == null || selectedData.hiddenBranchEN == '') frmIBSSApplyCnfrmtn.lblCustName.text = selectedData.lblCustName;
            else frmIBSSApplyCnfrmtn.lblCustName.text = selectedData.hiddenBranchEN;
            frmIBSSApplyCnfrmtn.lblAccType.text = selectedData.lblCustNameEN;
            if (noFixedAcctIBInfo[0]["personalizedAcctNickNameEN"] == null || noFixedAcctIBInfo[0]["personalizedAcctNickNameEN"] == '') frmIBSSApplyCnfrmtn.lblCustName2.text = noFixedAcctIBInfo[0]["personalizedAcctNickName"];
            else frmIBSSApplyCnfrmtn.lblCustName2.text = noFixedAcctIBInfo[0]["personalizedAcctNickNameEN"];
            frmIBSSApplyCnfrmtn.lblAccType2.text = noFixedAcctIBInfo[0]["productNameEN"];
        } else {
            if (selectedData.hiddenBranchTH == null || selectedData.hiddenBranchTH == '') frmIBSSApplyCnfrmtn.lblCustName.text = selectedData.lblCustName
            else frmIBSSApplyCnfrmtn.lblCustName.text = selectedData.hiddenBranchTH;
            frmIBSSApplyCnfrmtn.lblAccType.text = selectedData.lblCustNameTH;
            if (noFixedAcctIBInfo[0]["personalizedAcctNickNameTH"] == null || noFixedAcctIBInfo[0]["personalizedAcctNickNameTH"] == '') frmIBSSApplyCnfrmtn.lblCustName2.text = noFixedAcctIBInfo[0]["personalizedAcctNickName"];
            else frmIBSSApplyCnfrmtn.lblCustName2.text = noFixedAcctIBInfo[0]["personalizedAcctNickNameTH"];
            frmIBSSApplyCnfrmtn.lblAccType2.text = noFixedAcctIBInfo[0]["productNameTH"];
        }
    }
}

function changeLocaleApplyComplete() {
    var locale = kony.i18n.getCurrentLocale();
    if (locale == "en_US") {
        if (selectedData.hiddenBranchEN == null || selectedData.hiddenBranchEN == '') frmIBSSApplyCmplete.lblCustName.text = selectedData.lblCustName;
        else frmIBSSApplyCmplete.lblCustName.text = selectedData.hiddenBranchEN;
        frmIBSSApplyCmplete.lblAccType.text = selectedData.lblCustNameEN;
        if (noFixedAcctIBInfo[0]["personalizedAcctNickNameEN"] == null || noFixedAcctIBInfo[0]["personalizedAcctNickNameEN"] == '') frmIBSSApplyCmplete.lbLAccName2.text = noFixedAcctIBInfo[0]["personalizedAcctNickName"];
        else frmIBSSApplyCmplete.lbLAccName2.text = noFixedAcctIBInfo[0]["personalizedAcctNickNameEN"];
        frmIBSSApplyCmplete.lblAccType2.text = noFixedAcctIBInfo[0]["productNameEN"];
    } else {
        if (selectedData.hiddenBranchTH == null || selectedData.hiddenBranchTH == '') frmIBSSApplyCmplete.lblCustName.text = selectedData.lblCustName;
        else frmIBSSApplyCmplete.lblCustName.text = selectedData.hiddenBranchTH;
        frmIBSSApplyCmplete.lblAccType.text = selectedData.lblCustNameTH;
        if (noFixedAcctIBInfo[0]["personalizedAcctNickNameTH"] == null || noFixedAcctIBInfo[0]["personalizedAcctNickNameTH"] == '') frmIBSSApplyCmplete.lbLAccName2.text = noFixedAcctIBInfo[0]["personalizedAcctNickName"];
        else frmIBSSApplyCmplete.lbLAccName2.text = noFixedAcctIBInfo[0]["personalizedAcctNickNameTH"];
        frmIBSSApplyCmplete.lblAccType2.text = noFixedAcctIBInfo[0]["productNameTH"];
    }
}

function changeLocaleforExecuteDetailIB() {
    if (collectionDataExecuteIB != null || collectionDataExecuteIB != '') var locale = kony.i18n.getCurrentLocale();
    if (locale == "en_US") {
        if (collectionDataExecuteIB["lblExeFromAccNameEN"] == null || collectionDataExecuteIB["lblExeFromAccNameEN"] == '') frmIBSSSExcuteTrnsfr.lblCustName.text = collectionDataExecuteIB["lblExeFromAccName"];
        else frmIBSSSExcuteTrnsfr.lblCustName.text = collectionDataExecuteIB["lblExeFromAccNameEN"];
        if (collectionDataExecuteIB["lblExeFromAccTypeEN"] == null || collectionDataExecuteIB["lblExeFromAccTypeEN"] == '') frmIBSSSExcuteTrnsfr.lblAcctType.text = collectionDataExecuteIB["lblExeFromAccType"];
        else frmIBSSSExcuteTrnsfr.lblAcctType.text = collectionDataExecuteIB["lblExeFromAccTypeEN"];
        if (collectionDataExecuteIB["lblExeToAccNameEN"] == null || collectionDataExecuteIB["lblExeToAccNameEN"] == '') frmIBSSSExcuteTrnsfr.lblCustName2.text = collectionDataExecuteIB["lblExeToAccName"];
        else frmIBSSSExcuteTrnsfr.lblCustName2.text = collectionDataExecuteIB["lblExeToAccNameEN"]
        if (collectionDataExecuteIB["lblFromAccTypeEN"] == null || collectionDataExecuteIB["lblFromAccTypeEN"] == '') frmSSSExecute.lblSendToAccType.text = collectionDataExecuteIB["lblFromAccType"]
        else frmSSSExecute.lblSendToAccType.text = collectionDataExecuteIB["lblFromAccTypeEN"];
    } else {
        if (collectionDataExecuteIB["lblExeFromAccNameTH"] == null || collectionDataExecuteIB["lblExeFromAccNameTH"] == '') frmIBSSSExcuteTrnsfr.lblCustName.text = collectionDataExecuteIB["lblExeFromAccName"];
        else frmIBSSSExcuteTrnsfr.lblCustName.text = collectionDataExecuteIB["lblExeFromAccNameTH"];
        if (collectionDataExecuteIB["lblExeFromAccTypeTH"] == null || collectionDataExecuteIB["lblExeFromAccTypeTH"] == '') frmIBSSSExcuteTrnsfr.lblAcctType.text = collectionDataExecuteIB["lblExeFromAccType"];
        else frmIBSSSExcuteTrnsfr.lblAcctType.text = collectionDataExecuteIB["lblExeFromAccTypeTH"];
        if (collectionDataExecuteIB["lblExeToAccNameTH"] == null || collectionDataExecuteIB["lblExeToAccNameTH"] == '') frmIBSSSExcuteTrnsfr.lblCustName2.text = collectionDataExecuteIB["lblExeToAccName"];
        else frmIBSSSExcuteTrnsfr.lblCustName2.text = collectionDataExecuteIB["lblExeToAccNameTH"]
        if (collectionDataExecuteIB["lblFromAccTypeTH"] == null || collectionDataExecuteIB["lblFromAccTypeTH"] == '') frmIBSSSExcuteTrnsfr.lblAccType2.text = collectionDataExecuteIB["lblFromAccType"]
        else frmIBSSSExcuteTrnsfr.lblAccType2.text = collectionDataExecuteIB["lblFromAccTypeTH"];
    }
}

function changeLocaleforExecuteCompleteIB() {
    syncIBSendToSave();
    if (collectionDataExecuteIB != null || collectionDataExecuteIB != '') var locale = kony.i18n.getCurrentLocale();
    if (locale == "en_US") {
        if (collectionDataExecuteIB["lblExeFromAccNameEN"] == null || collectionDataExecuteIB["lblExeFromAccNameEN"] == '') frmIBSSSExcuteTrnsfrCmplete.lblCustName.text = collectionDataExecuteIB["lblExeFromAccName"];
        else frmIBSSSExcuteTrnsfrCmplete.lblCustName.text = collectionDataExecuteIB["lblExeFromAccNameEN"];
        if (collectionDataExecuteIB["lblExeFromAccTypeEN"] == null || collectionDataExecuteIB["lblExeFromAccTypeEN"] == '') frmIBSSSExcuteTrnsfrCmplete.lblAcctType.text = collectionDataExecuteIB["lblExeFromAccType"];
        else frmIBSSSExcuteTrnsfrCmplete.lblAcctType.text = collectionDataExecuteIB["lblExeFromAccTypeEN"];
        if (collectionDataExecuteIB["lblExeToAccNameEN"] == null || collectionDataExecuteIB["lblExeToAccNameEN"] == '') frmIBSSSExcuteTrnsfrCmplete.lbLAccName2.text = collectionDataExecuteIB["lblExeToAccName"];
        else frmIBSSSExcuteTrnsfrCmplete.lbLAccName2.text = collectionDataExecuteIB["lblExeToAccNameEN"]
        if (collectionDataExecuteIB["lblFromAccTypeEN"] == null || collectionDataExecuteIB["lblFromAccTypeEN"] == '') frmIBSSSExcuteTrnsfrCmplete.lblAccType2.text = collectionDataExecuteIB["lblFromAccType"]
        else frmIBSSSExcuteTrnsfrCmplete.lblAccType2.text = collectionDataExecuteIB["lblFromAccTypeEN"];
    } else {
        if (collectionDataExecuteIB["lblExeFromAccNameTH"] == null || collectionDataExecuteIB["lblExeFromAccNameTH"] == '') frmIBSSSExcuteTrnsfrCmplete.lblCustName.text = collectionDataExecuteIB["lblExeFromAccName"];
        else frmIBSSSExcuteTrnsfrCmplete.lblCustName.text = collectionDataExecuteIB["lblExeFromAccNameTH"];
        if (collectionDataExecuteIB["lblExeFromAccTypeTH"] == null || collectionDataExecuteIB["lblExeFromAccTypeTH"] == '') frmIBSSSExcuteTrnsfrCmplete.lblAcctType.text = collectionDataExecuteIB["lblExeFromAccType"];
        else frmIBSSSExcuteTrnsfrCmplete.lblAcctType.text = collectionDataExecuteIB["lblExeFromAccTypeTH"];
        if (collectionDataExecuteIB["lblExeToAccNameTH"] == null || collectionDataExecuteIB["lblExeToAccNameTH"] == '') frmIBSSSExcuteTrnsfrCmplete.lbLAccName2.text = collectionDataExecuteIB["lblExeToAccName"];
        else frmIBSSSExcuteTrnsfrCmplete.lbLAccName2.text = collectionDataExecuteIB["lblExeToAccNameTH"]
        if (collectionDataExecuteIB["lblFromAccTypeTH"] == null || collectionDataExecuteIB["lblFromAccTypeTH"] == '') frmIBSSSExcuteTrnsfrCmplete.lblAccType2.text = collectionDataExecuteIB["lblFromAccType"]
        else frmIBSSSExcuteTrnsfrCmplete.lblAccType2.text = collectionDataExecuteIB["lblFromAccTypeTH"];
    }
}

function syncIBSendToSave() {
    if (kony.application.getCurrentForm().id == "frmIBSSSExcuteTrnsfrCmplete") {
        if (frmIBSSSExcuteTrnsfrCmplete.lblBeforeBalMsg.isVisible) frmIBSSSExcuteTrnsfrCmplete.lblHide.text = kony.i18n.getLocalizedString("keyUnhide");
        else frmIBSSSExcuteTrnsfrCmplete.lblHide.text = kony.i18n.getLocalizedString("Hide");
        if (frmIBSSSExcuteTrnsfrCmplete.lblAfterBal.isVisible) frmIBSSSExcuteTrnsfrCmplete.lblHide1.text = kony.i18n.getLocalizedString("keyUnhide");
        else frmIBSSSExcuteTrnsfrCmplete.lblHide1.text = kony.i18n.getLocalizedString("Hide");
        if (frmIBSSSExcuteTrnsfrCmplete.lblBal2.isVisible) frmIBSSSExcuteTrnsfrCmplete.lblHide3.text = kony.i18n.getLocalizedString("keyUnhide");
        else frmIBSSSExcuteTrnsfrCmplete.lblHide3.text = kony.i18n.getLocalizedString("Hide");
    }
}