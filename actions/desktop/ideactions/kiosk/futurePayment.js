/*
*************************************************************************************
		Module	: doFuturePayment
		Author  : Kony
		Date    : June 06, 2013
		Purpose : This function is to make future payment.
****************************************************************************************
*/
//function doFuturePayment() {
//	var inputParam = {}
//	//inputParam["crmId"]="001100000000000000000009925939";
//	var frmAcct = frmBillPaymentConfirmationFuture.lblAccountNum.text;
//	var fromAcctID;
//	var fromAcctType;
//	for (var i = 0; i < frmAcct.length; i++) {
//		if (frmAcct[i] != "-") {
//			if (fromAcctID == null) {
//				fromAcctID = frmAcct[i];
//			} else {
//				fromAcctID = fromAcctID + frmAcct[i];
//			}
//		}
//	}
//	if (fromAcctID.length == 14) {
//		fromAcctType = "SDA";
//	} else {
//		fourthDigit = fromAcctID.charAt(6);
//		if (fourthDigit == "2" || fourthDigit == "7" || fourthDigit == "9") {
//			fromAcctType = "SDA";
//		} else {
//			fromAcctType = "DDA";
//		}
//	}
//	inputParam["rqUID"] = rquid;
//	inputParam["CRMID"] = gblcrmId;
//	inputParam["amt"] = getAmount();
//	inputParam["fromAcct"] = fromAcctID;
//	inputParam["fromAcctType"] = fromAcctType;
//	inputParam["toAcct"] = frmBillPaymentConfirmationFuture.lblRef1Value.text;
//	inputParam["toAccTType"] = "";
//	inputParam["dueDate"] = changeDateFormatForService(frmBillPaymentConfirmationFuture.lblStartOnValue.text);
//	inputParam["pmtRefNo1"] = frmBillPaymentConfirmationFuture.lblRef1Value.text;;
//	inputParam["custPayeeId"] = "";
//	inputParam["transCode"] = "";
//	inputParam["memo"] = frmBillPaymentConfirmationFuture.lblMyNoteValue.text;
//	inputParam["pmtMethod"] = "BILL_PAYMENT";
//	inputParam["extTrnRefId"] = frmBillPaymentConfirmationFuture.lblTxnNumValue.text;
//	inputParam["NumInsts"] = frmBillPaymentConfirmationFuture.lblExecuteValue.text;
//	inputParam["FinalDueDt"] = "";
//	var frequency = frmBillPaymentConfirmationFuture.lblEveryMonth.text;
//	inputParam["freq"] = frequency;
//	if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Weekly"))) {
//		inputParam["dayOfWeek"] = "";
//		inputParam["weekOfMonth"] = "";
//	}
//	if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Monthly"))) {
//		inputParam["dayOfMonth"] = "";
//		inputParam["monthOfYear"] = "";
//	}
//	if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Yearly"))) {
//		inputParam["dayOfYear"] = "";
//	}
//	inputParam["desc"] = "";
//	invokeServiceSecureAsync("doPmtAdd", inputParam, callBackDoFuturePayment);
//}
//function callBackDoFuturePayment(status, resulttable) {
//	
//	
//	if (status == 400) //success responce
//	{
//		
//		
//		
//		if (resulttable["opstatus"] == 0) {}
//	}
//}
/**
 * description
 * @returns {}
 */
//function getAmount() {
//	var amount = frmBillPaymentConfirmationFuture.lblAmountValue.text;
//	var fee = frmBillPaymentConfirmationFuture.lblPaymentFeeValue.text;
//	return parseInt(amount) + parseInt(fee);
//}