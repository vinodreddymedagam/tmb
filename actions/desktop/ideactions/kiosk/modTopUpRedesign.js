gblCategoryFormClosed = false;

function callTopUpFromMainMenu() {
    if (checkMBUserStatus()) {
        var noCASAAct = noActiveActs();
        if (noCASAAct) {
            showAlertWithCallBack(kony.i18n.getLocalizedString("MB_StatusNotEligible"), kony.i18n.getLocalizedString("info"), onclickActivationCompleteStart);
            return false;
        }
        gblMyBillerTopUpBB = 1; // top up or billpayment differenciation
        GblBillTopFlag = false; //  top up or billpayment differenciation
        glb_accId = 0;
        callMasterBillerService();
    }
}

function gotoTopUpSelectBillerForm() {
    showLoadingScreen();
    displaySelectBillerMainMenu();
    gblSelectBillerCategoryID = "0";
    gblMyBillerTopUpBB = 1; // top up or billpayment differenciation
    GblBillTopFlag = false; //  top up or billpayment differenciation
    isSearched = false;
    gblCategoryFormClosed = false;
    frmSelectBiller.tbxSearch.text = "";
    frmSelectBiller.tbxSearch.placeholder = kony.i18n.getLocalizedString("keySearch");
    frmSelectBiller.lblCategories.text = kony.i18n.getLocalizedString("MIB_BPCateTitle");
    frmSelectBiller.show();
    //getMyBillTopUpSuggestBillerListMB();
}

function myTopUpSegmentRowSelct() {
    var caterogeryFlag = frmSelectBiller["segMyBills"]["selectedItems"][0]["caterogeryFlag"];
    if (caterogeryFlag == "header") {} else if (caterogeryFlag == "mybills") {
        myTopUpsRowSelected();
    } else if (caterogeryFlag == "suggested") {
        myTopUpsSuggestedRowSelected();
    }
}

function displaySelectBillerMainMenu() {
    frmSelectBiller.btnHdrMenu.skin = "btnMenuIcon";
    frmSelectBiller.btnHdrMenu.focusSkin = "btnMenuIcon";
    frmSelectBiller.btnHdrMenu.onClick = handleMenuBtn;
}

function displaySelectBillerHrdBack() {
    //frmSelectBiller.btnHdrMenu.setVisibility(true);
    frmSelectBiller.btnHdrMenu.skin = "btnSelectBack";
    frmSelectBiller.btnHdrMenu.focusSkin = "btnSelectBack";
    frmSelectBiller.btnHdrMenu.onClick = selectBillerMainBack;
}

function frmSelectTopUpPostShow() {
    frmSelectBiller.scrollboxMain.scrollToEnd();
}

function selectBillerMainBack() {
    frmSelectBillerLanding.show();
}

function getTopUpBillerNickNameMB() {
    var topUpNickname = "";
    var ref1 = frmTopUp.lblRef1Value.text;
    topUpNickname = getBillerNickNameFromMyBills(gblMyBillList, gblCompCode, ref1, "")
    if (!isNotBlank(topUpNickname)) {
        topUpNickname = gblBillerCompCodeTH;
        if ("en_US" == kony.i18n.getCurrentLocale()) {
            topUpNickname = gblBillerCompCodeEN;
        }
    }
    frmTopUp.lblTopUpName.text = topUpNickname;
    showNicknameForScheduleTopUpNotInMyBills();
}

function showNicknameForScheduleTopUpNotInMyBills() {
    //alert("gblPaynow------->" + gblPaynow + "gblBillerPresentInMyBills-------->" + gblBillerPresentInMyBills)
    if (gblPaynow || gblBillerPresentInMyBills) {
        frmTopUp.hbxTopUpNickName.setVisibility(false);
        frmTopUp.txtNickName.text = "";
        frmTopUp.hbxTopUpNickNameInfo.setVisibility(false);
        frmTopUp.lineTopUpNickName.setVisibility(false);
        frmTopUp.hbox47826097229903.setVisibility(false);
    } else {
        frmTopUp.hbxTopUpNickName.setVisibility(true);
        frmTopUp.hbxTopUpNickNameInfo.setVisibility(true);
        frmTopUp.lineTopUpNickName.setVisibility(true);
        frmTopUp.hbox47826097229903.setVisibility(true);
    }
}

function getIconIdFromAccounts(accountNoIcon) {
    var iconId = "";
    for (var i = 0; i < gblAccountTable["custAcctRec"].length; i++) {
        if (!(gblAccountTable["custAcctRec"][i]["accId"].indexOf(accountNoIcon) < 0)) {
            iconId = gblAccountTable["custAcctRec"][i]["ICON_ID"];
        }
    }
    return iconId;
}
//Auto focus to different widgets based on conditions.
function autoFocusAfterSelectTopUp() {
    if (kony.application.getPreviousForm().id != "frmBillPaymentConfirmationFuture") {
        if (kony.application.getPreviousForm().id != "frmSchedule") {
            if (isNotBlank(frmTopUp.lblRef1Value.text)) {
                if (frmTopUp.hbxExclBillerOne.isVisible && !isNotBlank(frmTopUp.tbxExcludingBillerOne.text)) {
                    frmTopUp.tbxExcludingBillerOne.setFocus(true);
                }
            } else {
                frmTopUp.lblRef1Value.setFocus(true);
            }
        } else {
            if (frmTopUp.hbxTopUpNickName.isVisible) {
                frmTopUp.txtNickName.setFocus(true);
            }
        }
    }
}