var gblIDID = null;
//easy for debug about KPNS
function kpns_log(log) {
    kony.print("KPNS: " + log);
}

function registerPushCallBacks() {
    kpns_log("registerPushCallBacks");
    //resetGlobalsForKPNS();
    gPushRegId = "";
    //var config	= {	
    //					onsuccessfulregistration : onSuccessfulRegisration,
    //					onfailureregistration : onFailureRegistration,
    //					onlinenotification : onlineNotification,
    //					offlinenotification : offlineNotification,
    //					onsuccessfulderegistration : onSuccessfulDeregistration,
    //					onfailurederegistration : onFailureDeregistration 
    //				   };
    kony.push.setCallbacks({
        onsuccessfulregistration: onSuccessfulRegisration,
        onfailureregistration: onFailureRegistration,
        onlinenotification: onlineNotification,
        offlinenotification: offlineNotification,
        onsuccessfulderegistration: onSuccessfulDeregistration,
        onfailurederegistration: onFailureDeregistration
    });
    //	registerAppWithPushNotificationsInfrastructure();
}

function registerAppWithPushNotificationsInfrastructure() {
    kpns_log("registerAppWithPushNotificationsInfrastructure: gblDeviceInfo=" + JSON.stringify(gblDeviceInfo));
    /*	showAccuntSummaryScreen();
    	popAllowOrNot.dismiss(); */
    var config = [];
    // Register with GCM for Android
    if (kony.string.startsWith(gblDeviceInfo.name, "android", true)) {
        kpns_log("case android");
        config["senderid"] = "526171020976";
    }
    // Register with APNS for iPhone
    else if (kony.string.startsWith(gblDeviceInfo.name, "iPhone", true) || kony.string.startsWith(gblDeviceInfo.name, "iPad", true) || kony.string.startsWith(gblDeviceInfo.name, "iPod", true) || kony.string.startsWith(gblDeviceInfo.name, "iPhone Simulator", true)) {
        /*
         * 	0 - Specifies the Notification type as Badge.
         *	1 - Specifies the Notification type as Sound.
         *	2 - Specifies the Notification type as Alert.
         */
        kpns_log("case ios");
        config = [0, 1, 2];
    }
    // Register with BIS for BB
    else if (kony.string.startsWith(gblDeviceInfo.name, "blackberry", true)) {
        /*
         * appid - Specifies the unique application ID.
         * port - Specifies the port number on the device where the Push Notifications are received.
         * bpasurl - Specifies an HTTP version of the PPG base URL (Push Proxy Gateway). You receive the BPAS URL only after you register the application for Push Notification with RIM.
         * appicon - Specifies the icon to be displayed when there are no messages in the queue (typically the application logo).
         * starappicon - Specifies the icon to be displayed when there are unread messages in the queue (typically the application logo with a star on the top-right corner of the image (Image dimensions 54x54)).
         * statusbaricon - Specifies the icon to be displayed in the status bar (top area on the home screen of the device (Image dimensions 17x17)).
         */
        kpns_log("case bb");
        config["appid"] = "2784-6B71119D100275arco32679181akksk0540";
        config["port"] = "32111";
        config["bpasurl"] = "http://cpXXX.pushapi.eval.blackberry.com";
        config["appicon"] = "appicon.png";
        config["starappicon"] = "starappicon.png";
        config["statusbaricon"] = "statusbaricon.png";
    }
    kony.push.register(config);
    //registerAppWithKPNS(); 
}

function onSuccessfulRegisration(regId) {
    kpns_log("onSuccessfulRegisration: regId =" + regId);
    var basicConf = {
        message: "Registration Successful: RegisrationID: " + regId,
        alertType: constants.ALERT_TYPE_INFO,
        alertTitle: "",
        yesLabel: "OK",
        noLabel: "",
        alertHandler: null
    }
    var pspConf = {}
        //var infoAlert = kony.ui.Alert(basicConf,pspConf);
    gPushRegId = regId;
    kony.store.setItem("kpnssubcriptiontoken", gPushRegId);
    registerAppWithKPNS();
}

function onFailureRegistration(msg) {
    kpns_log("onFailureRegistration: msg=" + JSON.stringify(msg));
    var basicConf = {
        message: "Registration Failure: Registration Failure Message: " + msg,
        alertType: constants.ALERT_TYPE_INFO,
        alertTitle: "",
        yesLabel: "OK",
        noLabel: "",
        alertHandler: null
    }
    var pspConf = {}
        //var infoAlert = kony.ui.Alert(basicConf,pspConf);
    gPushRegId = "";
}
/*
 * If the device receives a message when the application is running, callback function 'onlineNotification' is executed by the underlying platform
 * 'payload' is an Object that contains a set of key-value pairs provided by the respective Push Notification Server
 */
function onlineNotification(payload) {
    kpns_log("onlineNotification: payload=" + JSON.stringify(payload));
    /* this called is supress until TMB confirm back to handle online notification
    handleAndroid(payload)
    
   // alert("Message Received : "+payload["content"]);
	if (kony.string.startsWith(gblDeviceInfo.name, "iPhone", true)){
		var basicConf = {message:"Online MSG: " +payload["alert"], alertType:constants.ALERT_TYPE_INFO, alertTitle:"", yesLabel:"OK", noLabel:"", alertHandler:null}
	}else{
		var basicConf = {message:"Online MSG: " +payload["content"], alertType:constants.ALERT_TYPE_INFO, alertTitle:"", yesLabel:"OK", noLabel:"", alertHandler:null}
	}
	
	var pspConf = {}
	var infoAlert = kony.ui.Alert(basicConf,pspConf); 
    */
}

function markNotificationReadMBCB(status, result) {
    if (status == 400) {
        if (result.status == "0") {
            //alert("SUCCESS");
        } else if (result.status == "1") {
            //alert("FAILURE");
        }
    }
}
/*
 * If the device receives a message when the application is not running, this callback is executed by the underlying platform.
 * 'payload' is an Object that contains a set of key-value pairs provided by the respective Push Notification Server}
 */
function offlineNotification(payload) {
    kpns_log("offlineNotification, payload = " + JSON.stringify(payload));
    gblNotificationFor = payload.notificationsource; // need to extract it from matadata of payload
    //gblPayLoadKpns = payload;	//this object we need pass to Beep and Bill method
    //On Successful login we have to check, which flow it has to call based on value of 'gblNotificationFor' variable
    //if(isSignedUser == true){
    //gblPushNotificationFlag = false;
    //}else{
    gblPushNotificationFlag = true;
    //}
    //var basicConf = {message:"OFF MSG: " +getStringfyObject(payload), alertType:constants.ALERT_TYPE_INFO, alertTitle:"", yesLabel:"OK", noLabel:"", alertHandler:null}
    if (kony.string.startsWith(gblDeviceInfo.name, "iPhone", true)) {
        var basicConf = {
            message: "" + payload["alert"],
            alertType: constants.ALERT_TYPE_INFO,
            alertTitle: "",
            yesLabel: "OK",
            noLabel: "",
            alertHandler: null
        }
    } else {
        var basicConf = {
            message: "" + payload["content"],
            alertType: constants.ALERT_TYPE_INFO,
            alertTitle: "",
            yesLabel: "OK",
            noLabel: "",
            alertHandler: null
        }
    }
    var pspConf = {}
    var infoAlert = kony.ui.Alert(basicConf, pspConf);
}

function onSuccessfulDeregistration() {
    kpns_log("onSuccessfulDeregistration");
    var basicConf = {
        message: "Deregistration Successful",
        alertType: constants.ALERT_TYPE_INFO,
        alertTitle: "",
        yesLabel: "OK",
        noLabel: "",
        alertHandler: null
    }
    var pspConf = {}
        //var infoAlert = kony.ui.Alert(basicConf,pspConf);
}

function onFailureDeregistration(payload) {
    kpns_log("onFailureDeregistration: payload=" + JSON.stringify(payload));
    var basicConf = {
        message: "Deregistration Failure" + payload,
        alertType: constants.ALERT_TYPE_INFO,
        alertTitle: "",
        yesLabel: "OK",
        noLabel: "",
        alertHandler: null
    }
    var pspConf = {}
        //var infoAlert = kony.ui.Alert(basicConf,pspConf);
}

function registerAppWithKPNS() {
    kpns_log("registerAppWithKPNS: gPushRegId = " + gPushRegId);
    if (gPushRegId == null || gPushRegId == "") {
        var basicConf = {
            message: "Registration ID is not available. Please register with native servers first.",
            alertType: constants.ALERT_TYPE_INFO,
            alertTitle: "",
            yesLabel: "OK",
            noLabel: "",
            alertHandler: null
        }
        var pspConf = {}
            //	var infoAlert = kony.ui.Alert(basicConf,pspConf);
        return;
    }
    //AppName		: "com.kone.TMB"
    var inputParams = {
        serviceID: "SubscribeKPNS",
        AppName: kony.i18n.getLocalizedString("bundleIdentifier").trim(),
        SID: gPushRegId,
        DeviceOS: "android",
        isGCM: "true",
        UFID: gblcrmId,
        channel: "MB"
    }
    if (!kony.string.startsWith(gblDeviceInfo.name, "iPhone", true)) {
        inputParams["DeviceID"] = getDeviceID();
    } else {
        inputParams["DeviceID"] = getDeviceID();
    }
    //for update device mgst master
    inputParams["device_uid"] = GBL_UNIQ_ID;
    kpns_log("registerAppWithKPNS: inputParams=" + JSON.stringify(inputParams));
    //kony.net.invokeServiceAsync(url, input, registerAppWithKPNS_CallBack, null);
    //appmiddlewareinvokerasync(inputParams, registerAppWithKPNS_CallBack);
    invokeServiceSecureAsyncKPNS("SubscribeKPNS", inputParams, registerAppWithKPNS_CallBack);
}

function registerAppWithKPNS_CallBack(status, response) {
    kpns_log("registerAppWithKPNS_CallBack: status = " + status + ", response = " + JSON.stringify(response));
    if (status == 400) {
        if (response["tknid"] != null) {
            tknid = response["tknid"]; //////Added for CSFR Token ID/////////
        }
        var basicConf = {
            message: "Registration with KPNS is successful" + JSON.stringify(response),
            alertType: constants.ALERT_TYPE_INFO,
            alertTitle: "",
            yesLabel: "OK",
            noLabel: "",
            alertHandler: null
        }
        var pspConf = {};
        //	var infoAlert = kony.ui.Alert(basicConf,pspConf);
        if (response["opstatus"] == 0) {
            if (response["subscriptionResponse"][0]["statusCode"] == 200 && response["subscriptionResponse"][0]["ksid"] != -1) {
                // store the ksid in local storage 
                kpns_log("registerAppWithKPNS_CallBack ksid " + response["subscriptionResponse"][0]["ksid"]);
                kony.store.setItem("ksid", response["subscriptionResponse"][0]["ksid"]);
                // this flag for fix bug when user already have ksid on devices
                kony.store.setItem("is_updated_kpns", "1");
            }
        }
    }
}

function getStringfyObject(payload) {
    var mystr = "unknown can not stringgify";
    try {
        payload["Subject"] = payload["Subject"].replace(/[^a-z0-9s]/gi, ' ');
        payload["Amount"] = unescape(payload["Amount"]).replace("+", "");
        mystr = JSON.stringify(payload);
    } catch (err) {}
    return mystr;
}

function invokeServiceSecureAsyncKPNS(serviceID, inputParam, callBackFunction) {
    if (appConfig.secureServerPort != null) {
        //var url = "http://" + appConfig.serverIp + ":8080" + "/" + appConfig.middlewareContext + "/MWServlet";
        var url = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/MWServlet";
    } else {
        //var url = "http://" + appConfig.serverIp + ":8080" + "/" + appConfig.middlewareContext + "/MWServlet";
        var url = "http://" + appConfig.serverIp + "/" + appConfig.middlewareContext + "/MWServlet";
    }
    //	alert("URL "+url);
    var sessionIdKey = "cacheid";
    inputParam.appID = appConfig.appId;
    inputParam.appver = appConfig.appVersion;
    inputParam["serviceID"] = serviceID;
    /*var gblDeviceInfo;
    if (gblDeviceInfo.length == 0) {
    	gblDeviceInfo = kony.os.deviceInfo();
    }
    if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "android") {
    	inputParam["channel"] = "rc";
    } else {
    	inputParam["channel"] = "wap";
    }
    inputParam["platform"] = gblDeviceInfo.name;*/
    inputParam[sessionIdKey] = sessionID;
    if (globalhttpheaders) {
        if (inputParam["httpheaders"]) {
            inputParam.httpheaders = mergeHeaders(inputParam.httpheaders, globalhttpheaders);
        } else {
            inputParam["httpheaders"] = globalhttpheaders;
        }
    }
    inputParam["tknid"] = tknid; //////Added for CSFR Token ID/////////
    var DEFAULT_SERVICE_TIMEOUT = 60000;
    //var connHandle = kony.net.invokeServiceAsync(url, inputParam, callBackFunction, null,null, DEFAULT_SERVICE_TIMEOUT);
    if (isMFEnabled) {
        var connHandle = callMFKpns(serviceID, inputParam, callBackFunction);
    } else {
        var connHandle = kony.net.invokeServiceAsync(url, inputParam, callBackFunction);
    }

    function callMFKpns(serviceID, inputParam, callBackFunction) {
        try {
            var headers = {};
            headers["Content-Type"] = "text/xml";
            var serviceName = serviceOpMap[serviceID];
            //alert("isMFsdkinit  ---  "+isMFsdkinit);
            if (isMFsdkinit) {
                bbServiceObj = mfClient.getIntegrationService(serviceName);
                bbServiceObj.invokeOperation(serviceID, headers, inputParam, function(result) {
                    //success
                    callBackFunction(400, result);
                }, function(result) {
                    //error
                    kony.print("====>Operation failed");
                    //alert(kony.i18n.getLocalizedString("genErrorWifiOff"));
                    if (result["opstatus"] == 1013 || result["opstatus"] == 1011) {
                        //isMFsdkinit=false;
                        alert(kony.i18n.getLocalizedString("genErrorWifiOff"));
                        kony.application.dismissLoadingScreen();
                    } else {
                        callBackFunction(400, result);
                    }
                });
            } else {
                kony.print("====>SDK intailization  failed");
                isMFsdkinit = false;
                alert(kony.i18n.getLocalizedString("genErrorWifiOff"));
                kony.application.dismissLoadingScreen();
            }
        } catch (e) {
            if (e.code == Errors.AUTH_FAILURE || e.code == Errors.INIT_FAILURE) {
                // initialization and authorization is not complete
                // sleep for try again
                isMFsdkinit = false;
                alert(kony.i18n.getLocalizedString("genErrorWifiOff"));
                dismissLoadingScreen();
                kony.print("Trying to invoke service while SDK is not initialized yet!");
            }
        }
    }
    return connHandle;
}
/*
 * Functions will be called on click of popUP's 'Allow' or 'Don't Allow' btns to register the app with APNS/GCM and KPNS console
 */
function pushNotificationAllow(upgradeSkip) {
    showLoadingScreen();
    popAllowOrNot.dismiss();
    gblNotificationFor = gblPayLoadKpns.notificationsource;
    if (gblPayLoadKpns.notificationID != null || gblPayLoadKpns.notificationID != "") {
        var inputParams = {
            messageId: gblPayLoadKpns.notificationID
        };
        if (upgradeSkip != null && upgradeSkip != undefined && upgradeSkip != "") {
            inputParams["upgradeSkip"] = upgradeSkip;
            clearFatcaGlobal();
        }
        invokeServiceSecureAsync("notificationmarkread", inputParams, markNotificationReadMBCB)
    }
    var basicConf = {
        message: "ON MSG: " + getStringfyObject(gblPayLoadKpns),
        alertType: constants.ALERT_TYPE_INFO,
        alertTitle: "",
        yesLabel: "OK",
        noLabel: "",
        alertHandler: null
    }
    var pspConf = {}
        //	var infoAlert = kony.ui.Alert(basicConf,pspConf);
    if (gblNotificationFor == "bb" || gblNotificationFor == "BB") {
        //go to Beep and Bill flow:
        gblNotificationFor = "randomCXZ";
        gblPushBBflow = true;
        gblPushNotfnDetailsFrom = "pushnotification";
        customerPaymentStatusInquiryForBBPushMsg(gblPayLoadKpns, "false");
    } else if (gblNotificationFor == "s2s" || gblNotificationFor == "S2S") {
        //go to Send To Save Execution Flow
        gblSSExcuteCnfrm = "randomXC"; // to control S2S execution flow 
        gblExeS2S = "true";
        gblPushNotfnDetailsFrom = "pushnotification";
        gblNotificationFor = "randomXC";
        getIBMBStatus();
        //	alert("function getIBMBStatus called for S2S execution ");
    } else {
        //Navigate to Notification Details Page
        gblPushNotfnDetailsFrom = "pushnotification"; // can be either of "pushnotification" OR null
        getNotificationDetailsMB();
    }
}

function pushNotificationDontAllow() {
    gblPushNotificationFlag = false;
    popAllowOrNot.dismiss();
    return false;
}
/*
 * Generalized function to show a popup, showing a message, decision btns and on click functions of btns
 */
/*function showPopupAllow(imgIcon, msgText, callBackOnConfirm, callBackCancel, btnAllowText, btnDontAllowText) {
	
	popAllowOrNot.lblTitle = "TMB Bank:";
	popAllowOrNot.imgIcon.src = imgIcon;	
	popAllowOrNot.lblMsgText.text = msgText;
	popAllowOrNot.btnAllow.onClick = callBackOnConfirm;
	popAllowOrNot.btnDontAllow.onClick = callBackCancel;
	popAllowOrNot.btnDontAllow.text = btnDontAllowText;// kony.i18n.getLocalizedString("keyCancelButton");
	popAllowOrNot.btnAllow.text = btnAllowText;//kony.i18n.getLocalizedString("keyConfirm");
	popAllowOrNot.show();
//	return true;
}*/
/*function pushNotificationAllowLg(){
	gblNotificationFor = gblPayLoadKpns.notificationsource;
	popAllowOrNot.dismiss();
	return false;
}
function pushNotificationDontAllowLg(){
	gblPushNotificationFlag = false;
	popAllowOrNot.dismiss();
	return false;
}*/
/*function handleIphone(payload) {

    
    gblPayLoadKpns = payload;
    var pushSub;
    
    if (payload["Subject"] == null || payload["content"] == null) {
        pushSub = "You got a Notification, would you like to move further";
    } else {
        pushSub = "" + payload["Subject"].replace(/[^a-z0-9s]/gi, ' ') + " " + payload["content"];
    }
    

    if (isSignedUser == true) {
        gblPushNotificationFlag = false;
        showPopupAllow("empty.png", pushSub, pushNotificationAllow, pushNotificationDontAllow, "Launch", "Close");
    } else {
        gblPushNotificationFlag = true;
        showPopupAllow("empty.png", pushSub, pushNotificationAllowLg, pushNotificationDontAllowLg, "Launch", "Close");
    }
    
    


}*/
//function handleAndroid(payload) {
//
//    
//
//    //	gblNotificationFor = payload.notificationsource;// need to extract it from matadata of payload
//    payload["Amount"] = unescape(payload["Amount"]).replace("+", "");
//    payload["Subject"] = payload["Subject"].replace(/[^a-z0-9s]/gi, ' ');
//    
//    gblPayLoadKpns = payload;
//
//    var pushSub;
//    
//    if (payload["Subject"] == null || payload["content"] == null) {
//        pushSub = "You got a Notification, would you like to move further";
//    } else {
//        pushSub = "" + payload["Subject"].replace(/[^a-z0-9s]/gi, ' ') + " " + payload["content"];
//    }
//    
//
//    if (isSignedUser == true) {
//        gblPushNotificationFlag = false;
//        showPopupAllow("empty.png", pushSub, pushNotificationAllow, pushNotificationDontAllow, "Launch", "Close");
//    } else {
//        gblPushNotificationFlag = true;
//        showPopupAllow("empty.png", pushSub, pushNotificationAllowLg, pushNotificationDontAllowLg, "Launch", "Close");
//    }
//    
//    
//
//
//}