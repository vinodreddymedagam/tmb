//var gAcctID = "";
var gActNoAndNameMapping = {};
var gActNoAndNameMappingTH = {};
var gCollection = {};
var gFinalCollection = {};
gblCalendarCollection = {}; //global variable, initializing this on click of My Activities from Menu, My Activities link from My Account Details page and Calendar tab click
gStatusImages = {};	//global variable, initializing this on click of My Activities from Menu or My Activities link from My Account Details page and Calendar tab click
var gWidgetsOnCalGrid = "";
var gPrevSelMY = "";
//var gActList = "";	//setting this on click of My Activities from Menu
//var gActListObj = [];
gNavPrevMonth = 0;
gNoOfNavPrevMonth = 0;
var gsLatestTxnShowed = 0;	//It is required to prevent showing latest txns if already shown on click of prev/next buttons on IB Month view 
var gsLatestTxnDate = "";
gMyActivitiesSegScrollEventTriggered = 0; //scroll onReachingBegining & onReachingEnd are getting triggered multiple times when we scroll to begining/end
gPrevCalSelIndex = 0;
gAccountDetailsFromLink = ""; //will be updated when user navigates to Calendar from Account Summary page
var gStartDay, gMonth, gYear, gEndDay;
var gToDay;
var gToDayYYYYMMDD;
var gStartDate, gEndDate;


//For MB List view
if(typeof(gListTab) == 'undefined')
{
	gListTab = 0; //to track whether List View tab is clicked
}

if(kony.os.deviceInfo().name != "iPhone" && kony.os.deviceInfo().name != "iPad" && kony.os.deviceInfo().name != "android" && kony.os.deviceInfo().name != "iPod touch" && 
	window != 'undefined' && window.navigator != 'undefined' && (window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8")) {
	Array.prototype.sortByProp = function(p) {
    	return this.sort(function(a, b) {
        	return (a[p] > b[p]) ? 1 : (a[p] < b[p]) ? -1 : 0;
    	});
	}
}
else {
	Object.defineProperty(Array.prototype,'sortByProp',{ enumerable:false,value:function(p) {
	return this.sort(function(a , b) {
	return (a[p] > b[p]) ? 1 : (a[p] < b[p]) ? -1 : 0;
	})
	}});
}

function objectSize(the_object) {
    /* function to validate the existence of each key in the object to get the number of valid keys. */
    var object_size = 0;
    for (key in the_object) {
        if (the_object.hasOwnProperty(key)) {
            object_size++;
        }
    }
    return object_size;
}

function initializeCalendarVariables() {
	
	if(GLOBAL_TODAY_DATE == null || GLOBAL_TODAY_DATE == "") {
		var d = new Date();
		GLOBAL_TODAY_DATE = (d.getMonth()+1) + "/" + d.getDate() + "/" + d.getFullYear();
		
	}
	
	gWidgetsOnCalGrid = "";
	gblKeyDates = [];
	gToDay = new Date(GLOBAL_TODAY_DATE);
	gStartDay = 1; //for getCalendarData the startdate is always 1st of that month
	var gToDayDate = gToDay.getDate();
	gMonth = gToDay.getMonth();
	gYear = gToDay.getFullYear();
	//gMonth = gToDay.getMonth() + 1;	//as in javascript month starts with 00 
	gEndDay = (32 - new Date(gYear, gMonth, 32).getDate()); //for getCalendarData the enddate can be 28/29/30/31 of that month
	gMonth++; //as in javascript month starts with 00 
	gStartDay = Number(gStartDay);
	gEndDay = Number(gEndDay);
	gMonth = Number(gMonth);
	if (gStartDay < 10) gStartDay = "0" + gStartDay;
	if (gEndDay < 10) gEndDay = "0" + gEndDay;
	if (gMonth < 10) gMonth = "0" + gMonth;
	if (gToDayDate < 10) gToDayDate = "0" + gToDayDate;
	gToDayYYYYMMDD = new Date(gYear, (gMonth - 1), gToDayDate);
	//For MB Day view
	gStartDate = new Date(gToDay.getFullYear(), (gToDay.getMonth() - 6), gToDay.getDate());
	gEndDate = new Date(gToDay.getFullYear(), (gToDay.getMonth() + 6), gToDay.getDate());
	
	
	
}

function clearCalendarData() {
	
	delete gAcctID, gActNoAndNameMapping, gActNoAndNameMappingTH, gCollection, gFinalCollection, gWidgetsOnCalGrid, gPrevSelMY, gsLatestTxnDate, gStartDay, gMonth, gYear, gEndDay, gToDay, gToDayYYYYMMDD;
	gblCalendarCollection = {}; //global variable, initializing this on click of My Activities from Menu, My Activities link from My Account Details page and Calendar tab click
	gStatusImages = {};	//global variable, initializing this on click of My Activities from Menu or My Activities link from My Account Details page and Calendar tab click
	gActList = "";
//	gActListObj = [];
	gsSelectedDate = null;
	gMyActivitiesSegScrollEventTriggered = 0;
	gPrevCalSelIndex = 0;
	gAccountDetailsFromLink = 0;
	gsLatestTxnShowed = 0;
	gNoOfNavPrevMonth = 0;
	gNavPrevMonth = 0;
	reloadCalendar = 1;
	var gAcctID = "";
	gActNoAndNameMapping = {};
	gActNoAndNameMappingTH = {};
	gCollection = {};
	gFinalCollection = {};
	gWidgetsOnCalGrid = "";
	gPrevSelMY = "";
	gsLatestTxnDate = "";
	gListGoToBeginning = 0;
	if(typeof(frmIBMyActivities) != 'undefined') {
		frmIBMyActivities.segCalendar.data = [];
	}
	if(typeof(frmIBPostLoginDashboard) != 'undefined') {
		frmIBPostLoginDashboard.segCalendar.data = [];
	}
	if(typeof(frmMBMyActivities) != 'undefined') {
		frmMBMyActivities.segCalendar.data = [];
	}
	initializeCalendarVariables();
	
}

/**
 * initial function call. This will be called onClick of a day from Calendar grid
 * set gDeleteThisKeyMY = 1, when any future transaction is updated through Calendar
 */
function IBMyActivitiesCalendarStartUp(gsSelectedDate, formId) {
    
    gsFormId = formId;
    // //Uncaught TypeError: Converting circular structure to JSON konyuiBaseClasses.js:134
    
    
        
    if (gsFormId.id == "frmIBMyActivities" || gsFormId.id == "frmIBPostLoginDashboard") {
        showLoadingScreenPopup();
    } else if (gsFormId.id == "frmMBMyActivities") {
        showLoadingScreen();
        //kony.application.showLoadingScreen(frmLoading, "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, false, true, null );    	
    }
    var selDate = gsSelectedDate.split("/");
    gMonth = selDate[0];
    var lMonth = (gMonth-1);
    gDate = selDate[1];
    gYear = selDate[2];
    gEndDay = (32 - new Date(gYear, lMonth, 32).getDate()); //for getCalendarData the enddate can be 28/29/30/31 of that month
    //if (gEndDay < 10) gEndDay = "0" + gEndDay;
    //if (gMonth < 10) gMonth = "0" + gMonth;
    //for updating status images
	if(gWidgetsOnCalGrid == "")
		IBMyActivitiesGetAllWidgetsOnCalGrid();
	
    if (gPrevSelMY == "") {
        //
		IBMyActivitiesSetDefaultStatusImages2();
        gPrevSelMY = gMonth + "" + gYear;
    } else {
        //
		if (reloadCalendar == 1) 
		{
			IBMyActivitiesSetDefaultStatusImages();
		}
        gPrevSelMY = gMonth + "" + gYear;
    }
    if (typeof(LinkMyActivities) != 'undefined' && typeof(gblAccountTable) != 'undefined') {
        
        if (LinkMyActivities == 1) //initiate it to 0 when click on My Activities Menu
        {
            
            if (gNoOfNavPrevMonth <= 6) {
				
                gNavPrevMonth = 1;
            } else {
				
                gNavPrevMonth = 0;
            }
			
        } else {
            
        }
    }
    //initializing
    gCollection = {};
    gFinalCollection = {};
	gsLatestTxnDate = "";
				
    //Get the data for a month if it's not there already
    if ((!gblCalendarCollection[gMonth + "" + gYear]) || (typeof(gblCalendarCollection[gMonth + "" + gYear]) == 'undefined')) {
        
        reloadCalendar = 1;
        //gblCalendarCollection[gMonth + "" + gYear] = new Array();
        if ( gActList == "" ) {
        	
			if( LinkMyActivities == 0 ) {
				
				IBMyActivitiesServiceCalendarInquiry();
			}
			else {
				
				IBMyActivitiesLinkMyActivitiesOnClick();
			}
        }		
		else {
			
			IBMyActivitiesServiceCalendarInquiry();
        }
    } else {
        //Get the data for current month eventhough it's already available. This is to get the updated data for today's transaction.
        //if(gMonth == new Date().getMonth())
        
        
        if (gDeleteThisKeyMY == 1) {
            
            reloadCalendar = 1;
            for (var keyMY1 in gblCalendarCollection) {
                if (!(gblCalendarCollection[keyMY1] < gblCalendarCollection[gMonth + "" + gYear])) {
                    
                    delete gblCalendarCollection[keyMY1];
                }
            }
            gDeleteThisKeyMY = 0;
			IBMyActivitiesServiceCalendarInquiry();
        } else {
            
            IBMyActivitiesShowDayTxns();
        }
    }
    
}
/**
var gblCalendarCollection = 	{"092013":[
									{"12345":[
												{"2013-01-16":[
																{"fromAcctNickname":"Rick","txnDate":"2013-01-16"},{"fromAcctNickname":"John","txnDate":"2013-01-16"}
															  ],
												"2013-01-11":[														
																{"fromAcctNickname":"ravi","txnDate":"2013-01-11"},{"fromAcctNickname":"Phani","txnDate":"2013-01-11"}													
															 ]
												} 														   
										    ],
									"67890":[
												{"2013-01-18":[
																{"fromAcctNickname":"Rick2","txnDate":"2013-01-16"},{"fromAcctNickname":"John2","txnDate":"2013-01-16"}
															  ],
												"2013-01-11":[														
																{"fromAcctNickname":"ravi2","txnDate":"2013-01-11"},{"fromAcctNickname":"Phani2","txnDate":"2013-01-11"}												
														     ]
												} 														   
										  ]
									}
								]						
						};
						

for(var keyMY in gblCalendarCollection)
{	
	for(var keyActNo in gblCalendarCollection[keyMY][0])
	{						
		for(var keyDate in gblCalendarCollection[keyMY][0][keyActNo][0])
		{
			for(var i=0; i<gblCalendarCollection[keyMY][0][keyActNo][0][keyDate].length; i++)
			{
				document.write("<br\>"+gblCalendarCollection[keyMY][0][keyActNo][0][keyDate][i]["fromAcctNickname"]);
			}
		}
	}
}

//fields in the final collection
txnType (to choose a template)
txnAction (to choose a template)
txnChannel (to choose a template)
txnTime (for sorting txns)
txnStatus (for image representation)
txnAmount
txnToAccountNickname
txnMyNote
txnBankName
txnBranchName
txnRecipientName
txnRecipientMobileNo
txnRecipientFbId
txnBillerNickname
TxnDescTemplate


*/

function IBMyActivitiesLinkMyActivitiesOnClick()
{
	if(gblAccountTable["custAcctRec"][gblIndex]["accId"] != undefined)
	{
		gActList = gblAccountTable["custAcctRec"][gblIndex]["accId"];
		
		//AccountNo and AccountNickName mapping
		var accId = gblAccountTable["custAcctRec"][gblIndex]["accId"];
        if ((typeof(gblAccountTable["custAcctRec"][gblIndex]["acctNickName"]) == 'undefined') || (gblAccountTable["custAcctRec"][gblIndex]["acctNickName"] == null)) {
        	
        	
			gActNoAndNameMappingTH[accId] = gblAccountTable["custAcctRec"][gblIndex].ProductNameThai + " " + accId.slice(accId.length - 4);
			gActNoAndNameMapping[accId] = gblAccountTable["custAcctRec"][gblIndex].ProductNameEng + " " + accId.slice(accId.length - 4);
        } else {
        	
            gActNoAndNameMappingTH[accId] = gblAccountTable["custAcctRec"][gblIndex]["acctNickName"];
            gActNoAndNameMapping[accId] = gblAccountTable["custAcctRec"][gblIndex]["acctNickName"];
        }
		
		gAccountDetailsFromLink = {};
		gAccountDetailsFromLink["accId"] = gblAccountTable["custAcctRec"][gblIndex]["accId"];
		gAccountDetailsFromLink["fiident"] = gblAccountTable["custAcctRec"][gblIndex]["fiident"];
		gAccountDetailsFromLink["accType"] = gblAccountTable["custAcctRec"][gblIndex]["accType"];
		IBMyActivitiesServiceCalendarInquiry();
	}
	else
	{
		alert("accId not received from My Activities Link of My Account Details page");
	}
}

/**************************************************************************************
		Module	: callBackTDAccountCalendar
		Author  : Kony
		Date    : Jan 17, 2014
		Purpose : Fetches  TD Account details from tDDetailinq service
*****************************************************************************************/

/*function callBackTDAccountCalendar(status, resulttable) {
 	if (status == 400) {
 		if (resulttable["opstatus"] == 0) {
			var key = "";
			var value = ""; 		
 			var tdData = [];
			var tdXferData =[];
			var temp = []
			temp[0] = 0;
			temp[1] = "Please select Maturity Date";
			kony.table.insert(tdXferData, temp);
   			for (var i = 0; i < resulttable.tdDetailsRec.length; i++) {
				var dumTemp = {
					maturityDate:resulttable.tdDetailsRec[i].maturityDate,depositAmtVal:resulttable.tdDetailsRec[i].depositAmtVal,
					depositNo:resulttable.tdDetailsRec[i].depositNo
				}
				kony.table.insert(tdData, dumTemp);
			}
 			 tdData .sort(function(a, b){
		         var dateA=a.maturityDate;
		         dateA=dateformatIB(dateA);
		         var dateB=b.maturityDate;
		         dateB=dateformatIB(dateB);
 		         return kony.os.compareDates(dateA, dateB, "dd/mm/yyyy") //default return value (no sorting)
		      })
		    
 			for (var i = 0; i < tdData.length; i++) {
				var dummy = [];
				key = i;
				value = "Date:  " + dateformatIB(tdData[i].maturityDate) + "    Amount:   " + tdData[i].depositAmtVal + " " 
						+kony.i18n.getLocalizedString("currencyThaiBaht") ;
 				//gblTDDepositNo[i] = tdData[i].depositNo;
				//gblXferTDAmt[i] = tdData[i].depositAmtVal;
				dummy[0] = key + 1;
				dummy[1] = value;
				kony.table.insert(tdXferData, dummy);
				
			}
			
			
			
		} else {			
			alert(resulttable["errMsg"]);
		}
	}
}*/

/**
 * CalendarInquiry Service
 */
function IBMyActivitiesServiceCalendarInquiry() {
	
    var nextMonth = Number(gMonth) + 1;
    if (nextMonth < 10) nextMonth = "0" + nextMonth;
	
	var startDate = gStartDay + "." + gMonth + "." + gYear;
	var endDate = gEndDay + "." + gMonth + "." + gYear;
	
    if (gMonth == gsMinMonth && gYear == gsMinYear) {
        startDate = gToDay.getDate() + "." + gMonth + "." + gYear;
        
    }  else if (gMonth == gsMaxMonth && gYear == gsMaxYear) {
        endDate = gToDay.getDate() + "." + gMonth + "." + gYear;
        
    }
    var inputParam = {};
    inputParam["startDate"] = startDate;
    inputParam["endDate"] = endDate;
    inputParam["bankCd"] = gblTMBBankCD;
	if( LinkMyActivities == 1 )
	inputParam["accountDetailsFromLink"] = JSON.stringify(gAccountDetailsFromLink);
	else
	inputParam["accountDetailsFromLink"] = "";
	if(gActList == "")
		inputParam["reloadFlag"] = "1";
	else
		inputParam["reloadFlag"] = "0";
    invokeServiceSecureAsync("CalendarInquiry", inputParam, IBMyActivitiesServiceCalendarInquiryCallBack);
    
}

function IBMyActivitiesServiceCalendarInquiryCallBack(status, resulttable) {
	
    if (gsFormId.id == "frmIBMyActivities" || gsFormId.id == "frmIBPostLoginDashboard") {
        showLoadingScreenPopup();
    } else if (gsFormId.id == "frmMBMyActivities") {
        showLoadingScreen();
    }
    //    
    if (status == 400) {
        //
        //        
        //when previt or xpress are down - dummy data - start
        /*if (objectSize(resulttable) == 0 || resulttable["opstatus"] != 0) {
			
			resulttable = {
				"0361899016": [{
					"dueDate": "2013-09-13",
					"pinCode": "0",
					"txnType": "001",
					"activityTypeID": "023",
					"createDate": "1470-07-30",
					"finTxnRefID": "54",
					"txnDescription": "Transfer via\\tM-Banking 50,000.00 ?\\nTo\\t\\nMy Note\\t",
					"createBy": "UNKNOWN",
					"eventID": "13000000001674",
					"finTxnAmount": "50000",
					"txnDate": "2013-09-11",
					"fromAcctID": "0361899016",
					"crmID": "001100000000000000000000033075",
					"toAcctNickname": "RC03-05-SCB-ORFT",
					"txnStatus": "01",
					"channelID": "02"
				}, {
					"dueDate": "2013-09-13",
					"pinCode": "0",
					"txnType": "001",
					"activityTypeID": "025",
					"createDate": "1470-07-30",
					"finTxnRefID": "56",
					"txnDescription": "Transfer via\\tM-Banking 1.00 ?\\nTo\\t\\nMy Note\\t",
					"createBy": "UNKNOWN",
					"eventID": "13000000001676",
					"finTxnAmount": "1",
					"txnDate": "2013-09-11",
					"fromAcctID": "0361899016",
					"crmID": "001100000000000000000000033075",
					"toAcctNickname": "RC03-05-SCB-ORFT",
					"txnStatus": "01",
					"channelID": "02"
				}],
				"00000012019675": [{
					"dueDate": "2013-09-15",
					"pinCode": "0",
					"txnType": "1",
					"activityTypeID": "001",
					"createDate": "2013-08-10",
					"fromAcctNickname": "fromAcctNickname",
					"finTxnRefID": "52",
					"createBy": "UNKNOWN",
					"eventID": "1",
					"finSchduleRefID": "2",
					"finTxnAmount": "100",
					"txnDate": "2013-09-14",
					"fromAcctID": "00000012019675",
					"crmID": "001100000000000000000000033075",
					"tellerID": "tell id4",
					"finTxnMemo": "memo1",
					"txnStatus": "00",
					"toAcctNickname": "toAcctNickname",
					"channelID": "01"
				}, {
					"dueDate": "2013-09-15",
					"pinCode": "0",
					"txnType": "001",
					"activityTypeID": "025",
					"createDate": "1470-07-31",
					"fromAcctNickname": "nareshd",
					"finTxnRefID": "NT1300000000195300",
					"txnDescription": "Transfer via\\tI-Banking 121.00 ?\\nTo\\tnareshd\\nMy Note\\tee",
					"createBy": "UNKNOWN",
					"eventID": "13000000001953",
					"finSchduleRefID": "2",
					"finTxnAmount": "121",
					"txnDate": "2013-09-14",
					"fromAcctID": "00000012019675",
					"crmID": "001100000000000000000000033075",
					"tellerID": "4",
					"finTxnMemo": "ee",
					"txnStatus": "01",
					"toAcctNickname": "raina",
					"channelID": "01"
				}]
			};
		}*/
        //when previt or xpress are down - dummy data - end	
		
        
        if ((objectSize(resulttable) > 0) && (!resulttable.errmsg)) //checking this condition as getCalendarData is not returning opstatus
        {
            if (gsFormId.id == "frmMBMyActivities") {
            	
            }
            
            if(resulttable["todayDate"] != null || resulttable["todayDate"] != undefined) {
				GLOBAL_TODAY_DATE = resulttable["todayDate"];
				
			}
            
            //Populating customer Account inquiry calendar service output
			if( LinkMyActivities == 0 && typeof(resulttable["custAcctRec"]) != 'undefined' ) {
				if (resulttable["custAcctRec"].length > 0) 
				{
					gActList = "";
					for (var i = 0; i < resulttable["custAcctRec"].length; i++) {
						var accId = resulttable["custAcctRec"][i]["accId"];
						if (i == 0) gActList += accId;
						else gActList += "," + accId;
						
						
						gActNoAndNameMappingTH[accId] = resulttable["custAcctRec"][i].acctNameThai;
						gActNoAndNameMapping[accId] = resulttable["custAcctRec"][i].acctNameEng;
					}
					for (var x in gActNoAndNameMapping) {
						
					}
				}
			}
            
            //getCalendarData - sort by dates
            for (var keyActNo in resulttable) //key calendarcollection
            {
				if(keyActNo != "custAcctRec") {
					if (typeof(resulttable[keyActNo]) == "object" && resulttable[keyActNo].length > 0) {
						//
						resulttable[keyActNo].sortByProp('txnDate');
						resulttable[keyActNo].sortByProp('txnTime');
					}
				}
			}
            //getCalendarData - preparing the collection
            for (var keyActNo in resulttable) //key calendarcollection
            {
				if(keyActNo != "custAcctRec") {
					if (typeof(resulttable[keyActNo]) == "object" && resulttable[keyActNo].length > 0) {
						//	                    
						for (var i = 0; i < resulttable[keyActNo].length; i++) //looping calendarcollection
						{
							//			
							var keyDate = resulttable[keyActNo][i]["txnDate"];
							if ((!gCollection[keyDate]) || (typeof(gCollection[keyDate]) == 'undefined')) {
								gCollection[keyDate] = new Array();
							} else {
								//		
							}
							if ((!gCollection[keyDate][0]) || (typeof(gCollection[keyDate][0]) == 'undefined')) {
								gCollection[keyDate][0] = new Object();
							} else {
								//		
							}
							if ((!gCollection[keyDate][0][keyActNo]) || (typeof(gCollection[keyDate][0][keyActNo]) == 'undefined')) { //check if its already exists
								//		
								gCollection[keyDate][0][keyActNo] = new Array();
							} else {
								//		
							}
							gCollection[keyDate][0][keyActNo].push({
								"txnType": resulttable[keyActNo][i]["txnType"],
								"eventID": resulttable[keyActNo][i]["eventID"],
								"channelID": resulttable[keyActNo][i]["channelID"],
								"txnDate": resulttable[keyActNo][i]["txnDate"],
								"txnStatus": resulttable[keyActNo][i]["txnStatus"],
								"finTxnAmount": resulttable[keyActNo][i]["finTxnAmount"],
								"toAcctNickname": resulttable[keyActNo][i]["toAcctNickname"],
								"finTxnMemo": resulttable[keyActNo][i]["finTxnMemo"],
								"txnBankName": resulttable[keyActNo][i]["txnBankName"],
								"txnBranchName": resulttable[keyActNo][i]["txnBranchName"],
								"txnRecipientName": resulttable[keyActNo][i]["txnRecipientName"],
								"txnRecipientMobileNo": resulttable[keyActNo][i]["txnRecipientMobileNo"],
								"txnRecipientFbId": resulttable[keyActNo][i]["txnRecipientFbId"],
								"txnBillerNickname": resulttable[keyActNo][i]["txnBillerNickname"],
								"finTxnRefID": resulttable[keyActNo][i]["finTxnRefID"],
								"finSchduleRefID": resulttable[keyActNo][i]["finSchduleRefID"],
								"pinCode": resulttable[keyActNo][i]["pinCode"],
								"TxnDescTemplate": resulttable[keyActNo][i]["txnDescription"],
								"activityTypeID": resulttable[keyActNo][i]["activityTypeID"],
								"createBy": resulttable[keyActNo][i]["createBy"],
								"createDate": resulttable[keyActNo][i]["createDate"],
								"dueDate": resulttable[keyActNo][i]["dueDate"],
								"fromAcctID": resulttable[keyActNo][i]["fromAcctID"],
								"txnTime": resulttable[keyActNo][i]["txnTime"],
								"finFlexValues": resulttable[keyActNo][i]["finFlexValues"]
							});
						}
					}
				}
			}
        } else {
            
        }
        mySorting();
    }
    
}

/**
 * Sorting an object with respect to it's values
 */
function mySorting() {
	
    //old - start
    var maxSpeed = {
        car: 300,
        bike: 60,
        motorbike: 200,
        airplane: 1000,
        helicopter: 400,
        rocket: 8 * 60 * 60
    };
    var sortable = [];
    for (var vehicle in maxSpeed) {
        sortable.push([vehicle, maxSpeed[vehicle]]);
    }
    //document.write(sortable);
    sortable.sort(function(a, b) {
        return a[1] - b[1]
    });
    //document.write("<br/>" + sortable);
    //old - end
	var noOfTxns = 0;
	var lDate = gsSelectedDate.split("/");	
    //new - start
    //sorting the final array records with all transactions on date basis
    
	//gCollection -> gFinalCollection -> gblCalendarCollection
	
	var txnDates = [];
	for (var keyDate in gCollection) 
	{
        //        
        txnDates.push(keyDate);
    }
	txnDates.sort(function(date1, date2) {
		if (date1 > date2) return 1;
		if (date1 < date2) return -1;
		return 0;
	});
	
	//
	for (var k = 0; k < txnDates.length; k++) {
		//
	}
	
	var keyDate;
	for (var k = 0; k < txnDates.length; k++) {
		keyDate = txnDates[k];
		//		
		for (var i = 0; i < gCollection[keyDate].length; i++) {
			for (var keyActNo in gCollection[keyDate][i]) {
				//
				//
				//
				gCollection[keyDate][i][keyActNo].sortByProp('txnTime');
				
				for (var j = 0; j < gCollection[keyDate][i][keyActNo].length; j++) {
					//
					
					//
                    if ((!gFinalCollection[keyDate]) || (typeof(gFinalCollection[keyDate]) == 'undefined')) {
                        gFinalCollection[keyDate] = new Array();
                    } else {
                        //
                    }
                    if ((!gFinalCollection[keyDate][0]) || (typeof(gFinalCollection[keyDate][0]) == 'undefined')) {
                        gFinalCollection[keyDate][0] = new Object();
                    } else {
                        //
                    }
                    if ((!gFinalCollection[keyDate][0][keyActNo]) || (typeof(gFinalCollection[keyDate][0][keyActNo]) == 'undefined')) {
                        gFinalCollection[keyDate][0][keyActNo] = new Array();
                    } else {
                        //
                    }
					noOfTxns++;
                    gFinalCollection[keyDate][i][keyActNo].push({
                        "txnType": gCollection[keyDate][i][keyActNo][j]["txnType"],
                        "eventID": gCollection[keyDate][i][keyActNo][j]["eventID"],
                        "channelID": gCollection[keyDate][i][keyActNo][j]["channelID"],
                        "txnDate": gCollection[keyDate][i][keyActNo][j]["txnDate"],
                        "keyActNo": gCollection[keyDate][i][keyActNo][j]["keyActNo"],
                        "txnStatus": gCollection[keyDate][i][keyActNo][j]["txnStatus"],
                        "finTxnAmount": gCollection[keyDate][i][keyActNo][j]["finTxnAmount"],
                        "toAcctNickname": gCollection[keyDate][i][keyActNo][j]["toAcctNickname"],
                        "finTxnMemo": gCollection[keyDate][i][keyActNo][j]["finTxnMemo"],
                        "txnBankName": gCollection[keyDate][i][keyActNo][j]["txnBankName"],
                        "txnBranchName": gCollection[keyDate][i][keyActNo][j]["txnBranchName"],
                        "txnRecipientName": gCollection[keyDate][i][keyActNo][j]["txnRecipientName"],
                        "txnRecipientMobileNo": gCollection[keyDate][i][keyActNo][j]["txnRecipientMobileNo"],
                        "txnRecipientFbId": gCollection[keyDate][i][keyActNo][j]["txnRecipientFbId"],
                        "txnBillerNickname": gCollection[keyDate][i][keyActNo][j]["txnBillerNickname"],
                        "finTxnRefID": gCollection[keyDate][i][keyActNo][j]["finTxnRefID"],
                        "finSchduleRefID": gCollection[keyDate][i][keyActNo][j]["finSchduleRefID"],
                        "pinCode": gCollection[keyDate][i][keyActNo][j]["pinCode"],
                        "TxnDescTemplate": gCollection[keyDate][i][keyActNo][j]["TxnDescTemplate"],
                        "activityTypeID": gCollection[keyDate][i][keyActNo][j]["activityTypeID"],
                        "createBy": gCollection[keyDate][i][keyActNo][j]["createBy"],
                        "createDate": gCollection[keyDate][i][keyActNo][j]["createDate"],
                        "dueDate": gCollection[keyDate][i][keyActNo][j]["dueDate"],
                        "fromAcctID": gCollection[keyDate][i][keyActNo][j]["fromAcctID"],
                        "txnTime": gCollection[keyDate][i][keyActNo][j]["txnTime"],
                        "finFlexValues": gCollection[keyDate][i][keyActNo][j]["finFlexValues"]
                    });
                }
            }
        }
    }
    
    //push all the sorted collection into gblCalendarCollection
    if ((!gblCalendarCollection[gMonth + "" + gYear]) || (typeof(gblCalendarCollection[gMonth + "" + gYear]) == 'undefined')) {
        gblCalendarCollection[gMonth + "" + gYear] = new Array();
    }
    gblCalendarCollection[gMonth + "" + gYear].push(gFinalCollection);
	
	//update status images on Calendar grid - start
	var selDate = gsSelectedDate.split("/"); //mm-dd-yyyy
	var keyMY = selDate[0] + "" + selDate[2];
	//
	gStatusImages[keyMY] = new Object();
	for (var keyDate in gblCalendarCollection[keyMY][0]) {
		var keyDateArray = keyDate.split("-");
		var keyDay = keyDateArray[2];
		if (!gStatusImages[keyMY][keyDay]) {
			gStatusImages[keyMY][keyDay] = {
				scheduled: 0,
				due: 0,
				completed: 0,
				failed: 0,
				pending: 0
			};
		}
		for (var keyActNo in gblCalendarCollection[keyMY][0][keyDate][0]) {
			for (var i = 0; i < gblCalendarCollection[keyMY][0][keyDate][0][keyActNo].length; i++) {
				//
				if ((gblCalendarCollection[keyMY][0][keyDate][0][keyActNo][i]["finTxnRefID"] && gblCalendarCollection[keyMY][0][keyDate][0][keyActNo][i]["finTxnRefID"] != "" && gblCalendarCollection[keyMY][0][keyDate][0][keyActNo][i]["finTxnRefID"] != null) || (gblCalendarCollection[keyMY][0][keyDate][0][keyActNo][i]["channelID"] != '01' && gblCalendarCollection[keyMY][0][keyDate][0][keyActNo][i]["channelID"] != '02' && gblCalendarCollection[keyMY][0][keyDate][0][keyActNo][i]["channelID"] != '10' && gblCalendarCollection[keyMY][0][keyDate][0][keyActNo][i]["channelID"] != '11' && gblCalendarCollection[keyMY][0][keyDate][0][keyActNo][i]["channelID"] != 'Internet Bank' && gblCalendarCollection[keyMY][0][keyDate][0][keyActNo][i]["channelID"] != 'Mobile Banking')) {
					//
					if (gblCalendarCollection[keyMY][0][keyDate][0][keyActNo][i]["txnStatus"]) {
						if (gblCalendarCollection[keyMY][0][keyDate][0][keyActNo][i]["txnStatus"] == "01") {
							gStatusImages[keyMY][keyDay]["completed"]++;
						} else if (gblCalendarCollection[keyMY][0][keyDate][0][keyActNo][i]["txnStatus"] == "02") {
							gStatusImages[keyMY][keyDay]["failed"]++;
						} else if (gblCalendarCollection[keyMY][0][keyDate][0][keyActNo][i]["txnStatus"] == "03") {
							if (gblCalendarCollection[keyMY][0][keyDate][0][keyActNo][i]["txnType"] == "007") {
								gStatusImages[keyMY][keyDay]["failed"]++;
							} else {
								gStatusImages[keyMY][keyDay]["pending"]++;
							}
						}
					} else {
						//
						gStatusImages[keyMY][keyDay]["pending"]++;
					}
				} else {
					//
					if (gblCalendarCollection[keyMY][0][keyDate][0][keyActNo][i]["txnType"] == "007") {
						gStatusImages[keyMY][keyDay]["due"]++;
					} else {
						gStatusImages[keyMY][keyDay]["scheduled"]++;
					}
				}
			}
		}
	}
	//update status images on Calendar grid - end
	
    if (LinkMyActivities == 1 && gNavPrevMonth == 1 && gsLatestTxnShowed == 0) {
        //If current month have any transactions
		if (noOfTxns > 0) 
		{
			for (var y = 0; y < txnDates.length; y++) {
				
				var ltDate = txnDates[y].split("-");
				var lKeyDate = new Date(ltDate[0], (ltDate[1]-1), ltDate[2]);
				
				if(lKeyDate <= gToDayYYYYMMDD)
				{
					gsLatestTxnDate =  lKeyDate.getFullYear()+"-"+(lKeyDate.getMonth()+1)+"-"+lKeyDate.getDate();  //this must be <= today
				}
			}
				
		}	
		if (noOfTxns > 0 && gsLatestTxnDate != "") 
		{
			var ltDate = gsLatestTxnDate.split("-");
			var keyDate = getCalDateMDY(ltDate[1], ltDate[2], ltDate[0]);
			gNavPrevMonth = 0; //otherwise it will be infinite loop
			gsLatestTxnShowed = 1;
			/*
			if (gsFormId.id == "frmIBMyActivities" && gsFormId.vboxCalDayView.containerWeight != 30) {
				gsFormId.vbxArrow.setVisibility(false);
				gsFormId.vbxArrow.containerWeight = 2;
				gsFormId.vboxCalDayView.containerWeight = 30;
				//clear the right margin for hboxStatusDesc which we set on click on Calendar tab 
				gsFormId.hbxBtns.containerWeight = 30;
				gsFormId.hboxStatusDesc.margin = [0, 0, 0, 0];
				gsFormId.lblactivity.setVisibility(false);
				gsFormId.hbxBtns.setVisibility(false);
				gsFormId.lblMyActivities.setVisibility(true);
				gsFormId.hbxCal.setVisibility(true);
				gsFormId.vboxCalDayView.setVisibility(true);
			}*/
			showCalendar(keyDate, gsFormId);
        }			
		else 
		{
			//If current month doesn't have any transactions, then navigate to previous month
            if (gNoOfNavPrevMonth < 6) {
                
                gNoOfNavPrevMonth++; //not updating gMonth & gYear here as we are updating them at navigateToPreviousMonth()
                
                navigateToPreviousMonth();
            } 
			else 
			{
                
                gNavPrevMonth = 0; //otherwise it will be infinite loop
                gsLatestTxnShowed = 1;
                /*
                if (gsFormId.id == "frmIBMyActivities" && gsFormId.vboxCalDayView.containerWeight != 30) {
                    gsFormId.vbxArrow.setVisibility(false);
                    gsFormId.vbxArrow.containerWeight = 2;
                    gsFormId.vboxCalDayView.containerWeight = 30;
                    //clear the right margin for hboxStatusDesc which we set on click on Calendar tab 
                    gsFormId.hbxBtns.containerWeight = 30;
                    gsFormId.hboxStatusDesc.margin = [0, 0, 0, 0];
                    gsFormId.lblactivity.setVisibility(false);
                    gsFormId.hbxBtns.setVisibility(false);
                    gsFormId.lblMyActivities.setVisibility(true);
                    gsFormId.hbxCal.setVisibility(true);
                    gsFormId.vboxCalDayView.setVisibility(true);
                }*/
                showCalendar("", gsFormId);
            }
        }
    } else {
    	/*
        //to traverse through gFinalCollection and prepare the final array records with all transactions
        
        for (var keyDate in gFinalCollection) {
            
            for (var i = 0; i < gFinalCollection[keyDate].length; i++) {
                for (var keyActNo in gFinalCollection[keyDate][i]) {
                    
                    
                    for (var j = 0; j < gFinalCollection[keyDate][i][keyActNo].length; j++) {
                        
                    }
                }
            }
        }
        
        //traverse through superCollection and show txns
        for (var keyMY in gblCalendarCollection) {
            for (var keyDate in gblCalendarCollection[keyMY][0]) {
                for (var keyActNo in gblCalendarCollection[keyMY][0][keyDate][0]) {
                    for (var i = 0; i < gblCalendarCollection[keyMY][0][keyDate][0][keyActNo].length; i++) {
                        
                    }
                }
            }
        }
		*/
		
        //then show transactions for today by default for MB (or) for selected day for MIB
        if ((gsFormId.id == "frmMBMyActivities") || (gsFormId.id == "frmIBMyActivities") || gsFormId.id == "frmIBPostLoginDashboard") {
            
            IBMyActivitiesShowDayTxns();
        }
    }
    
}
/**
 * Show transactions list
 */
function IBMyActivitiesShowDayTxns() {
	
	if (reloadCalendar == 1) 
	{
		IBMyActivitiesUpdateStatusImages();
    }
	IBMyActivitiesCreateSegRowsForTxns();   
}
/**
 * onClick event on Calendar tab in MyActivities form.
 * Use btncalenderOnClick() from IBMyActivities.js instead of the below function
 */
function frmIBMyActivities_btncalender_onClick() {
    frmIBMyActivities.hbox58868446699428.setVisibility(true);
    frmIBMyActivities.lblactivity.setVisibility(true);
    frmIBMyActivities.hbxhistory.setVisibility(false);
    frmIBMyActivities.hbxfuture.setVisibility(false);
    frmIBMyActivities.hbxbottom.setVisibility(false);
    frmIBMyActivities.hbxbottomfuture.setVisibility(false);
    frmIBMyActivities.hbox589488823281264.setVisibility(false);
    frmIBMyActivities.hbxCalendarContainer.setVisibility(true);
    frmIBMyActivities.vboxCalDayView.setVisibility(false);
    //setting the right margin so that the hboxStatusDesc takes less width
    //and this will be similar when vboxCalDayView is visible 
    frmIBMyActivities.hboxStatusDesc.margin = [0, 0, 40, 0];
    //frmIBMyActivities.vboxCalDayView.containerWeight = 0;
    frmIBMyActivities.btnfuture.skin = btnIBrndleftgrey;
    frmIBMyActivities.btnhistory.skin = btnIBrndleftgrey;
    frmIBMyActivities.btncalender.skin = btnIBrndrightblue;
    showCalendar("", frmIBMyActivities);
};
var initializationVectorKey;

//function getInitVectorKey() {
//    initializationVectorKey = Math.floor(Math.random() * 5);
//}

//function encryptActivities(gFinalCollection) {
//    try {
//        var algo = "aes";
//        var encryptDecryptKey = kony.crypto.newKey("random", 128, {
//            passphrasetext: null,
//            subalgo: "aes",
//            passphrasehashalgo: ""
//        });
//        var inputstr = gFinalCollection;
//        var prptobj = {
//            padding: "pkcs5",
//            mode: "cbc",
//            initializationvector: initializationVectorKey
//        };
//        myEncryptedTextRaw = kony.crypto.encrypt(algo, encryptDecryptKey, inputstr, prptobj);
//        saveEncrykey = kony.crypto.saveKey("encrptkey", encryptDecryptKey)
//        var list = {
//            enckey: myEncryptedTextRaw,
//            encKeyVal: saveEncrykey
//        }
//        kony.store.setItem("encrptedText", list);
//        //
//    } catch (err) {
//        alert("Error in callbackEncryptAes : " + err);
//    }
//}

//function decryptActivities(initializationVectorKey) {
//    try {
//        var algo = "aes";
//        var prptobj = {
//            padding: "pkcs5",
//            mode: "cbc",
//            initializationvector: initializationVectorKey
//        };
//        var encrKeyFromDevice = kony.store.getItem("encrptedText");
//        var readEncryVal = encrKeyFromDevice["encKeyVal"]
//        decryptkey = kony.crypto.readKey(readEncryVal)
//        //
//        var retencrKeyFromDevice = encrKeyFromDevice["enckey"]
//        //
//        myClearText = kony.crypto.decrypt(algo, decryptkey, retencrKeyFromDevice, prptobj);
//        //
//        return myClearText
//    } catch (err) {
//        alert("Error in callbackDecryptAes : " + err);
//    }
//}
/*
**************************************************************************************************************************************
  	Name    : showCalendarDayView
  	Author  : Phanidhar Anumula 
  	Date    : Sep 02 2013
  	Purpose : This function is to disply the selected date on the 3r vbox from the date selected from 2nd vbox at frmIBMyActivities.kl
**************************************************************************************************************************************
--*/
//function showCalendarDayView(src) {
//    frmIBMyActivities.vboxCalDayView.containerWeight = 30;
//    //clear the right margin for hboxStatusDesc which we set on click on Calendar tab 
//    frmIBMyActivities.hboxStatusDesc.margin = [0, 0, 0, 0];
//    frmIBMyActivities.vboxCalDayView.setVisibility(true);
//    //IBMyActivitiesDeleteExistingTxnsList();
//    //IBMyActivitiesCreateRowsForTxns();
//}
/*
******************************************************************************************************************
  	Name    : IBMyActivitiesDeleteExistingTxnsList
  	Author  : Phanidhar Anumula 
  	Date    : Sep 12 2013
  	Purpose : To clean up the 3rd vbox on IB: 001100000000000000000000033075
			
*******************************************************************************************************************
--*/
//function IBMyActivitiesDeleteExistingTxnsList() {
//    if (gsFormId.vboxTxnsDynamic) {
//        
//        //gsFormId.remove(vboxTxnsDynamic);
//        gsFormId.hboxTxnsMain.removeAt(0) 
//    }
//    //uncomment
//    /*
//    vboxTxnsDynamic = new kony.ui.Box({
//        "id": "vboxTxnsDynamic",
//        "isVisible": true,
//        "orientation": constants.BOX_LAYOUT_VERTICAL
//    }, {
//        "containerWeight": 100,
//        "margin": [0, 0, 0, 0],
//        "padding": [0, 0, 0, 0],
//        "vExpand": false,
//        "hExpand": true,
//        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
//        "marginInPixel": false,
//        "paddingInPixel": false,
//        "layoutType": constants.CONTAINER_LAYOUT_BOX
//    }, {});
//    gsFormId.hboxTxnsMain.addAt(vboxTxnsDynamic, 0);*/
//}
/*
******************************************************************************************************************
  	Name    : calendarEventExecutedTxnMB
  	Author  : Rishika Kushwaha 
  	Date    : Nov 7,2013
  	Purpose : This module is to add click event for MB executed transactions
*******************************************************************************************************************
--*/
function calendarEventExecutedTxnMB2( txnId, actvtyId, openAcctType) {    
    var getTransactionDetails_inputparams = {};
    getTransactionDetails_inputparams["txnRefId"] = txnId;
    getTransactionDetails_inputparams["activityTypeIds"] = actvtyId;
    getTransactionDetails_inputparams["openAcctType"] = openAcctType;
    gblTDTransfer = false;
    accountReturned = "";
    gblActivityIds = getTransactionDetails_inputparams["activityTypeIds"];
    
    if (getTransactionDetails_inputparams["activityTypeIds"] == "023" || getTransactionDetails_inputparams["activityTypeIds"] == "024" || getTransactionDetails_inputparams["activityTypeIds"] == "025" || getTransactionDetails_inputparams["activityTypeIds"] == "026" || getTransactionDetails_inputparams["activityTypeIds"] == "604" || getTransactionDetails_inputparams["activityTypeIds"] == "605" || getTransactionDetails_inputparams["activityTypeIds"] == "606" || getTransactionDetails_inputparams["activityTypeIds"] == "607") {
        invokeServiceSecureAsync("getTransactionDetails", getTransactionDetails_inputparams, getTransferTxnDetCallBackMB);
    } else if (getTransactionDetails_inputparams["activityTypeIds"] == "027" || getTransactionDetails_inputparams["activityTypeIds"] == "028") {
        invokeServiceSecureAsync("getTransactionDetails", getTransactionDetails_inputparams, getBillPaymentTxnDetCallBackMB);
    } else if (getTransactionDetails_inputparams["activityTypeIds"] == "030" || getTransactionDetails_inputparams["activityTypeIds"] == "031") {
        invokeServiceSecureAsync("getTransactionDetails", getTransactionDetails_inputparams, getTopUpTxnDetCallBackMB);
    } else if (getTransactionDetails_inputparams["activityTypeIds"] == "034") {
        invokeServiceSecureAsync("getTransactionDetails", getTransactionDetails_inputparams, getBeepBillTxnDetCallBackMB);
    } else if (getTransactionDetails_inputparams["activityTypeIds"] == "029") {
        invokeServiceSecureAsync("getTransactionDetails", getTransactionDetails_inputparams, getePmntTxnDetCallBackMB);
    } else if (getTransactionDetails_inputparams["activityTypeIds"] == "063" && getTransactionDetails_inputparams["openAcctType"] == "NS") {
        invokeServiceSecureAsync("getTransactionDetails", getTransactionDetails_inputparams, getNSTxnDetCallBackMB);
    } else if (getTransactionDetails_inputparams["activityTypeIds"] == "063" && getTransactionDetails_inputparams["openAcctType"] == "SC") {
        invokeServiceSecureAsync("getTransactionDetails", getTransactionDetails_inputparams, getSCTxnDetCallBackMB);
    } else if (getTransactionDetails_inputparams["activityTypeIds"] == "063" && getTransactionDetails_inputparams["openAcctType"] == "TD") {
        invokeServiceSecureAsync("getTransactionDetails", getTransactionDetails_inputparams, getTDTxnDetCallBackMB);
        //alert("Not Implemented");
    } else if (getTransactionDetails_inputparams["activityTypeIds"] == "063" && getTransactionDetails_inputparams["openAcctType"] == "DS") {
        invokeServiceSecureAsync("getTransactionDetails", getTransactionDetails_inputparams, getDSTxnDetCallBackMB);
        //alert("Not Implemented");
    } else if (getTransactionDetails_inputparams["activityTypeIds"] == "038") {
        invokeServiceSecureAsync("getTransactionDetails", getTransactionDetails_inputparams, getS2STxnDetCallBackMB);
        //alert("Not Implemented");     
    } else {
    	
        kony.application.dismissLoadingScreen();
        alert("Yet to be implemented for this activity type");
    }
}

var pattST = /^ST/i;
var pattSB = /^SB/i;
var pattSU = /^SU/i;

/*
******************************************************************************************************************
  	Name    : calendarEventScheduledTxnIB
  	Author  : Phanidhar Anumula 
  	Date    : Sep 16 2013, Oct 07 2013
  	Purpose : This module is to add click event for transactions on IB
*******************************************************************************************************************
--*/
function calendarEventScheduledTxnIB2( refId ) {
	var schRefId = refId;
    
    //To redirect to Calendar page from future txns pages
    gblActivitiesNvgn = "C";
    if (schRefId) {
        if (pattST.test(schRefId)) {
            //calling future transfers screen
            frmIBFTrnsrView.hbxFTEditAmnt.setVisibility(false);
            frmIBFTrnsrView.hbxFTVeiwAmnt.setVisibility(true);
            frmIBFTrnsrView.lblFTEditHdr.setVisibility(false);
            frmIBFTrnsrView.btnFTEdit.setVisibility(false);
            frmIBFTrnsrView.hbxFTViewHdr.setVisibility(true);
            frmIBFTrnsrView.btnReturnView.setVisibility(true)
            frmIBFTrnsrView.hbxEditBtns.setVisibility(false);
            //frmIBFTrnsrView.txtFTEditAmountval.setFocus(false);
            frmIBFTrnsrView.hbxFTAfterEditAvilBal.setVisibility(false);
            gblSchduleRefIDFT = schRefId;
            gblVarEditFTIB();
        }
        else if (pattSB.test(schRefId)) {
            //calling future billers screen
          	callPaymentInqServiceForEditBP(schRefId);
        } else if (pattSU.test(schRefId)) {
            //calling future topups screen
            callPaymentInqServiceForEditTP(schRefId);
        }
    }
}

/*
******************************************************************************************************************
  	Name    : calendarEventScheduledTxnMB
  	Author  : Phanidhar Anumula 
  	Date    : Sep 16 2013, Oct 07 2013 
  	Purpose : This module is to add click event for transactions
*******************************************************************************************************************
--*/
function calendarEventScheduledTxnMB2( refId ) {
	var schRefId = refId;
    
    if (schRefId) {
        if (pattST.test(schRefId)) {
            //calling future transfers screen            
            gblSchduleRefIDFT = schRefId;
            gblVarEditFTMB();
        } else if (pattSB.test(schRefId)) {
            //calling future billers screen
            getMasterBillerDataForMB(schRefId);
        } else if (pattSU.test(schRefId)) {
            //calling future topups screen
            getMasterBillerDataForMBTU(schRefId);
        }
    }
}

function IBMyActivitiesSegmentOnClick(eventObject) {
    
	if (gsFormId.id == "frmIBMyActivities" || gsFormId.id == "frmIBPostLoginDashboard") {
		if(gblCalledMasterBillerService == 0)
			invokeMasterBillerInqService();
		else {
			IBMyActivitiesSegmentOnClick2();
		}
	}
	else {
		IBMyActivitiesSegmentOnClick2();
	}
}

function IBMyActivitiesSegmentOnClick2() {	
	var lHidden = "";
	var actiNiceName = "";
	if (gListTab) 
    {
		lHidden = gsFormId.segCalendarListView.selectedItems[0].lblHidden;
		actiNiceName = gsFormId.segCalendarListView.selectedItems[0].segLblToValue;
	}
	else
	{
		lHidden = gsFormId.segCalendar.selectedItems[0].lblHidden;
		actiNiceName = gsFormId.segCalendar.selectedItems[0].segLblToValue;
	}	
	 
     if(actiNiceName != null && actiNiceName != undefined){
     gblActivityNiceName = actiNiceName.trim();
     }
     kony.print("SegMent OnClik : acctName "+gblActivityNiceName);
	
    
    //Can not view SME transactions from MIB - removed alert also
 	if(lHidden == "sme") {
		return;
	}
    
	if(lHidden != "")
	{		
		
		var result = lHidden.split("|||");				
		
		if( result[0] == "bnb" )
		{
			
			var calEvenObj = new Object();
			calEvenObj.finFlexValues = result[1];
			calEvenObj.finTxnRefID = result[2];
			
			if (gsFormId.id == "frmIBMyActivities" || gsFormId.id == "frmIBPostLoginDashboard") {
				customerPaymentStatusInquiryForBBIBCalendar(calEvenObj);
			}
			else if (gsFormId.id == "frmMBMyActivities") {
				customerPaymentStatusInquiryForBBMBCalendar(calEvenObj);
			}
		}
		else if ( result[0] == "executed" )
		{
			
			
			
			if (gsFormId.id == "frmIBMyActivities" || gsFormId.id == "frmIBPostLoginDashboard") {
				var getTransactionDeatils_inputparams = {};
				getTransactionDeatils_inputparams["txnRefId"] = result[1];
				getTransactionDeatils_inputparams["activityTypeIds"] = result[2];
				
				if(result[2] == "063") {
					showLoadingScreenPopup();
					var inputParam = {};
					inputParam["finTxnRefID"] = result[1];
					gTxnRefId = result[1];
					invokeServiceSecureAsync("GetOpenProductCode", inputParam, getOpenProdCodeCallBack);
				}
				else {
					getTransactionDeatils_inputparams["openAcctType"] = "";
					getTransactionDetails(getTransactionDeatils_inputparams);
				}
			}
			else if (gsFormId.id == "frmMBMyActivities") {
				if(result[2] == "063") {
					var inputParam = {};
					inputParam["finTxnRefID"] = result[1];
					gTxnRefId = result[1];
					invokeServiceSecureAsync("GetOpenProductCode", inputParam, getOpenProdCodeCallBack);
				}
				else {
					calendarEventExecutedTxnMB2( result[1], result[2], "" );
				}
			}
		}
		else if ( result[0] == "scheduled" )
		{
			
			
			if (gsFormId.id == "frmIBMyActivities" || gsFormId.id == "frmIBPostLoginDashboard") {
				calendarEventScheduledTxnIB2( result[1] );
			}
			else if (gsFormId.id == "frmMBMyActivities") {
				calendarEventScheduledTxnMB2( result[1] );
			}
		}
	}
	else
	{		
		return;
	}	
}

/*
******************************************************************************************************************
  	Name    : getOpenProdCodeCallBack
  	Author  : Yugal Sharma 
  	Date    : Feb 13 2014
  	Purpose : Service call back of GetOpenProductCode service. Returns OpenProdType for getTransactionDetails service.
			
*******************************************************************************************************************
--*/
function getOpenProdCodeCallBack(status, resulttable) {
	if(status == 400) {
		if(resulttable["opstatus"] == 0 && resulttable["errCode"] == 0) {
			if (gsFormId.id == "frmMBMyActivities") {
				
			}
			if(gsFormId.id == "frmIBMyActivities" || gsFormId.id == "frmIBPostLoginDashboard") {
				var getTransactionDetails_inputparams = {};
				getTransactionDetails_inputparams["txnRefId"] = gTxnRefId;
				getTransactionDetails_inputparams["activityTypeIds"] = "063";
				getTransactionDetails_inputparams["openAcctType"] = resulttable["openProductCode"];
				delete gTxnRefId;
				getTransactionDetails(getTransactionDetails_inputparams);
			}
			else if(gsFormId.id == "frmMBMyActivities") {
				var refId = gTxnRefId;
				delete gTxnRefId;
				
				
				calendarEventExecutedTxnMB2(refId, "063", resulttable["openProductCode"]);
			}
		}
		else {
			
		}
	}
}

function IBMyActivitiesSegmentOnReachingBegining() {
	
	if(gListTab)
    {
	    
	    gListGoToBeginning = 1;
	    navigateToPreviousMonth();
    }
}

function IBMyActivitiesSegmentOnReachingEnd() {
	
	if(gListTab)
	{
	    
	    navigateToNextMonth();
    }    
} 

/*
******************************************************************************************************************
  	Name    : IBMyActivitiesCreateSegRowsForTxns
  	Author  : Phanidhar Anumula 
  	Date    : Dec 11 2013, Sep 23 2013
  	Purpose : This module is to create rows for all the transactions 
  			  When user clicks on a particula day in Month view (or)
  			  By navigating to a day in Day view (or)
  			  When calendar is reloaded in List view
*******************************************************************************************************************
--*/
function IBMyActivitiesCreateSegRowsForTxns(explicit) {
    
    
    if (gsFormId.id == "frmIBMyActivities" || gsFormId.id == "frmIBPostLoginDashboard") {
        showLoadingScreenPopup();
    } else if (gsFormId.id == "frmMBMyActivities") {
        showLoadingScreen();
        //kony.application.showLoadingScreen(frmLoading, "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, false, true, null );    	
    }
    //
    //to prevent executing IBMyActivitiesCreateSegRowsForTxns() when gsFormId.cmbTxnType.selectedKey = "000";
    if ((gblCalendarCollection[gMonth + "" + gYear]) || (typeof(gblCalendarCollection[gMonth + "" + gYear]) != 'undefined')) {
        var widgetIndex = 0;
        var selDate = gsSelectedDate.split("/");
        var keyMY = selDate[0] + "" + selDate[2];
        
        var keyDate = selDate[2] + "-" + selDate[0] + "-" + selDate[1];
        var keySelDate = keyDate;
        
        var keyDates = [];
        keyDates.push(keyDate);
        
        var sortedTxns = {};
        var segListSize = 0;
        var cmbTxnRef = gsFormId.cmbTxnType;
        //var deviceInfo = kony.os.deviceInfo();
		if(gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPod touch" || gblDeviceInfo["name"] == "iPhone Simulator")
        {	if(gListTab) {
        		cmbTxnRef = gsFormId.cmbTxnType1;
        	}
        	else {
        		cmbTxnRef = gsFormId.cmbTxnType;
       		}
       	}	
        if (((gsFormId.id == "frmMBMyActivities") || (gsFormId.id == "frmIBMyActivities")) && (cmbTxnRef.selectedKey != null && cmbTxnRef.selectedKey != '000')) {
            //if filter by transaction type required
            
            
            reloadCalendar = 1;
            if (gblCalendarCollection[keyMY] && gblCalendarCollection[keyMY][0]) {
                keyDates = [];
                for (var keyDate2 in gblCalendarCollection[keyMY][0]) {
						keyDates.push(keyDate2);
                }
				
				gblKeyDates = [];
				gblKeyDates = keyDates;
            }
            if ((!sortedTxns[keyMY]) || (typeof(sortedTxns[keyMY]) == 'undefined')) {
                
                sortedTxns[keyMY] = new Array();
            }
            if ((!sortedTxns[keyMY][0]) || (typeof(sortedTxns[keyMY][0]) == 'undefined')) {
                sortedTxns[keyMY][0] = new Object();
            }
			var lKeyDates = [];
            for (var d = 0; d < keyDates.length; d++) {
                keyDate = keyDates[d];
                //
                if (gblCalendarCollection[keyMY] && gblCalendarCollection[keyMY][0][keyDate]) {
                    if ((!sortedTxns[keyMY][0][keyDate]) || (typeof(sortedTxns[keyMY][0][keyDate]) == 'undefined')) {
                        //				
                        sortedTxns[keyMY][0][keyDate] = new Array();
                    }
                    if ((!sortedTxns[keyMY][0][keyDate][0]) || (typeof(sortedTxns[keyMY][0][keyDate][0]) == 'undefined')) {
                        sortedTxns[keyMY][0][keyDate][0] = new Object();
                    }
                    for (var keyActNo in gblCalendarCollection[keyMY][0][keyDate][0]) {
                        var j = 0;
                        for (var i = 0; i < gblCalendarCollection[keyMY][0][keyDate][0][keyActNo].length; i++) {
                            if (cmbTxnRef.selectedKey == gblCalendarCollection[keyMY][0][keyDate][0][keyActNo][i]["txnType"]) {
                                //						
                                if ((!sortedTxns[keyMY][0][keyDate][0][keyActNo]) || (typeof(sortedTxns[keyMY][0][keyDate][0][keyActNo]) == 'undefined')) {
                                    //
                                    sortedTxns[keyMY][0][keyDate][0][keyActNo] = new Array();
                                }
                                if ((!sortedTxns[keyMY][0][keyDate][0][keyActNo][j]) || (typeof(sortedTxns[keyMY][0][keyDate][0][keyActNo][j]) == 'undefined')) {
                                    sortedTxns[keyMY][0][keyDate][0][keyActNo][j] = new Object();
                                }
                                sortedTxns[keyMY][0][keyDate][0][keyActNo][j]["txnType"] = gblCalendarCollection[keyMY][0][keyDate][0][keyActNo][i]["txnType"];
                                sortedTxns[keyMY][0][keyDate][0][keyActNo][j]["eventID"] = gblCalendarCollection[keyMY][0][keyDate][0][keyActNo][i]["eventID"];
                                sortedTxns[keyMY][0][keyDate][0][keyActNo][j]["channelID"] = gblCalendarCollection[keyMY][0][keyDate][0][keyActNo][i]["channelID"];
                                sortedTxns[keyMY][0][keyDate][0][keyActNo][j]["txnDate"] = gblCalendarCollection[keyMY][0][keyDate][0][keyActNo][i]["txnDate"];
                                sortedTxns[keyMY][0][keyDate][0][keyActNo][j]["txnStatus"] = gblCalendarCollection[keyMY][0][keyDate][0][keyActNo][i]["txnStatus"];
                                sortedTxns[keyMY][0][keyDate][0][keyActNo][j]["finTxnAmount"] = gblCalendarCollection[keyMY][0][keyDate][0][keyActNo][i]["finTxnAmount"];
                                sortedTxns[keyMY][0][keyDate][0][keyActNo][j]["toAcctNickname"] = gblCalendarCollection[keyMY][0][keyDate][0][keyActNo][i]["toAcctNickname"];
                                sortedTxns[keyMY][0][keyDate][0][keyActNo][j]["finTxnMemo"] = gblCalendarCollection[keyMY][0][keyDate][0][keyActNo][i]["finTxnMemo"];
                                sortedTxns[keyMY][0][keyDate][0][keyActNo][j]["txnBankName"] = gblCalendarCollection[keyMY][0][keyDate][0][keyActNo][i]["txnBankName"];
                                sortedTxns[keyMY][0][keyDate][0][keyActNo][j]["txnBranchName"] = gblCalendarCollection[keyMY][0][keyDate][0][keyActNo][i]["txnBranchName"];
                                sortedTxns[keyMY][0][keyDate][0][keyActNo][j]["txnRecipientName"] = gblCalendarCollection[keyMY][0][keyDate][0][keyActNo][i]["txnRecipientName"];
                                sortedTxns[keyMY][0][keyDate][0][keyActNo][j]["txnRecipientMobileNo"] = gblCalendarCollection[keyMY][0][keyDate][0][keyActNo][i]["txnRecipientMobileNo"];
                                sortedTxns[keyMY][0][keyDate][0][keyActNo][j]["txnRecipientFbId"] = gblCalendarCollection[keyMY][0][keyDate][0][keyActNo][i]["txnRecipientFbId"];
                                sortedTxns[keyMY][0][keyDate][0][keyActNo][j]["txnBillerNickname"] = gblCalendarCollection[keyMY][0][keyDate][0][keyActNo][i]["txnBillerNickname"];
                                sortedTxns[keyMY][0][keyDate][0][keyActNo][j]["finTxnRefID"] = gblCalendarCollection[keyMY][0][keyDate][0][keyActNo][i]["finTxnRefID"];
                                sortedTxns[keyMY][0][keyDate][0][keyActNo][j]["finSchduleRefID"] = gblCalendarCollection[keyMY][0][keyDate][0][keyActNo][i]["finSchduleRefID"];
                                sortedTxns[keyMY][0][keyDate][0][keyActNo][j]["pinCode"] = gblCalendarCollection[keyMY][0][keyDate][0][keyActNo][i]["pinCode"];
                                sortedTxns[keyMY][0][keyDate][0][keyActNo][j]["TxnDescTemplate"] = gblCalendarCollection[keyMY][0][keyDate][0][keyActNo][i]["TxnDescTemplate"];
                                sortedTxns[keyMY][0][keyDate][0][keyActNo][j]["activityTypeID"] = gblCalendarCollection[keyMY][0][keyDate][0][keyActNo][i]["activityTypeID"];
                                sortedTxns[keyMY][0][keyDate][0][keyActNo][j]["createBy"] = gblCalendarCollection[keyMY][0][keyDate][0][keyActNo][i]["createBy"];
                                sortedTxns[keyMY][0][keyDate][0][keyActNo][j]["createDate"] = gblCalendarCollection[keyMY][0][keyDate][0][keyActNo][i]["createDate"];
                                sortedTxns[keyMY][0][keyDate][0][keyActNo][j]["dueDate"] = gblCalendarCollection[keyMY][0][keyDate][0][keyActNo][i]["dueDate"];
                                sortedTxns[keyMY][0][keyDate][0][keyActNo][j]["fromAcctID"] = gblCalendarCollection[keyMY][0][keyDate][0][keyActNo][i]["fromAcctID"];
                                sortedTxns[keyMY][0][keyDate][0][keyActNo][j]["txnTime"] = gblCalendarCollection[keyMY][0][keyDate][0][keyActNo][i]["txnTime"];
                                sortedTxns[keyMY][0][keyDate][0][keyActNo][j]["finFlexValues"] = gblCalendarCollection[keyMY][0][keyDate][0][keyActNo][i]["finFlexValues"];
                                j++;
								
								if(lKeyDates.length > 0) {
									if(lKeyDates[lKeyDates.length-1] != keyDate)
									lKeyDates.push(keyDate);
								}
								else {
									lKeyDates.push(keyDate);
								}
                            }
                        }
                    }
                }
            }
			keyDates = [];
			keyDates = lKeyDates;
        } else {
			
            sortedTxns = gblCalendarCollection;
			if (gsFormId.id == "frmMBMyActivities" && reloadCalendar == 1) {
				if (sortedTxns[keyMY] && sortedTxns[keyMY][0]) {
					
					keyDates = [];
					for (var keyDate2 in sortedTxns[keyMY][0]) {
						keyDates.push(keyDate2);
					}
				
					gblKeyDates = [];
					gblKeyDates = keyDates;
				}
			}
			else if(gListTab && gblKeyDates != null) {
				reloadCalendar = 1;
				keyDates = [];
				keyDates = gblKeyDates;
			}
        }
        
        /*for (var d = 0; d < (keyDates.length); d++) {
	        
	    }*/
        
        var noOfTxns = 0;
        var noOfTodaysTxns = 0;
        gsFormId.segCalendar.widgetDataMap = {
            "hbox1": "hbox1",
            "hbox2": "hbox2",
            "lblDateHeader": "lblDateHeader",
            "hbox0": "hbox0",
            "hboxCalendarFourRowsTemplate": "hboxCalendarFourRowsTemplate",
            "segLblMyNote": "segLblMyNote",
            "segLblToValue": "segLblToValue",
            "segImgArrow": "segImgArrow",
            "vbox0": "vbox0",
            "segLblMyNoteValue": "segLblMyNoteValue",
            "segCalendarFourRowsTemplate": "segCalendarFourRowsTemplate",
            "segLblTxnType": "segLblTxnType",
            "segLblTime": "segLblTime",
            "segLblTxnTypeValue": "segLblTxnTypeValue",
            "segLblTo": "segLblTo",
            "segImgStatus": "segImgStatus",
            "lblHidden": "lblHidden",
            "lblAcctNName": "lblAcctNName"
        };
        var month = new Array();
        month["01"] = kony.i18n.getLocalizedString("keyCalendarJan");
        month["02"] = kony.i18n.getLocalizedString("keyCalendarFeb");
        month["03"] = kony.i18n.getLocalizedString("keyCalendarMar");
        month["04"] = kony.i18n.getLocalizedString("keyCalendarApr");
        month["05"] = kony.i18n.getLocalizedString("keyCalendarMay");
        month["06"] = kony.i18n.getLocalizedString("keyCalendarJun");
        month["07"] = kony.i18n.getLocalizedString("keyCalendarJul");
        month["08"] = kony.i18n.getLocalizedString("keyCalendarAug");
        month["09"] = kony.i18n.getLocalizedString("keyCalendarSep");
        month["10"] = kony.i18n.getLocalizedString("keyCalendarOct");
        month["11"] = kony.i18n.getLocalizedString("keyCalendarNov");
        month["12"] = kony.i18n.getLocalizedString("keyCalendarDec");
        /*
		[ //segData
			[ //segRow	-> each date
				{ //segRowSecHdr},	-> each date
				[ //segRowData	-> no. of txns of each date for all accounts
					{ //segRowDataAcctNName }	-> act no. which is having txns for that date
					{ //segRowDataItem }	-> all txns under that act for that date
				]
			]
		] */
        
        gsFormId.segCalendar.data = [];
        if (gsFormId.id == "frmMBMyActivities" && reloadCalendar == 1) {
            gsFormId.segCalendarListView.data = [];
        }
        gsFormId.lblNoActivities.setVisibility(false);
        var segData = new Array();
        var segDataListView = new Array();
        var sectionIndex = 0;
        var setIndexForThisDate = 0;
        
  
        if (sortedTxns[keyMY]) {
            
            for (var d = 0; d < keyDates.length; d++) {
                keyDate = keyDates[d];
                
                //To identify the indexes for section header & row to show the transactions of gsSelectedDate on top on List View
                if (keyDate == keySelDate) {
                    //
                    setIndexForThisDate = 1;
                } else {
                    //
                    setIndexForThisDate = 0;
                }
                //
                //
                if (sortedTxns[keyMY][0][keyDate]) {
                    //
                    var segRowListView = new Array();
                    var segRowDataListView = new Array();
                    var rowIndex = 0;
                    
                    var selDate = keyDate.split("-");
                    var lMonth = selDate[1];
                    //if (lMonth < 10) lMonth = "0" + lMonth;
                    var sDate = selDate[2] + " " + month[lMonth] + " " + selDate[0];
                    var segRowSecHdr = new Object();
                    //segRowSecHdr["lblDateHeader"] = keyDate;
                    segRowSecHdr["lblDateHeader"] = sDate;
                    segRowListView.push(segRowSecHdr);
                    segListSize += 11;
                    sectionIndex++;
                    
                    var lActList = gActList.split(",");
                    var keyActNo = "";
                    //for (var keyActNo in sortedTxns[keyMY][0][keyDate][0]) {
                    for (var a = 0; a < lActList.length; a++) {
                        keyActNo = lActList[a];
                        //
                        if (sortedTxns[keyMY][0][keyDate][0][keyActNo]) {
                            noOfTxns++;
                            //first row with acct nickname									
                            var segRowDataAcctNName = new Object();
                            //
                            if (kony.i18n.getCurrentLocale() == "en_US") {
                                if ((!gActNoAndNameMapping[keyActNo]) || (typeof(gActNoAndNameMapping[keyActNo]) == 'undefined')) segRowDataAcctNName["lblAcctNName"] = keyActNo;
                                else segRowDataAcctNName["lblAcctNName"] = gActNoAndNameMapping[keyActNo];
                            } else {
                                if ((!gActNoAndNameMappingTH[keyActNo]) || (typeof(gActNoAndNameMappingTH[keyActNo]) == 'undefined')) segRowDataAcctNName["lblAcctNName"] = keyActNo;
                                else segRowDataAcctNName["lblAcctNName"] = gActNoAndNameMappingTH[keyActNo];
                            }
                            //segRowDataAcctNName["lblAcctNName"] = keyActNo;
                            segRowDataAcctNName["template"] = hboxCalendarAcctNNameTemplate;
                            segRowDataAcctNName["metainfo"] = {
                                clickable: false
                            };
                            if (gsFormId.id == "frmMBMyActivities" && reloadCalendar == 1) {
                                segRowDataListView.push(segRowDataAcctNName);
                                rowIndex++;
                            }
                            if (keyDate == keySelDate) {
                                segData.push(segRowDataAcctNName);
                            }
                            segListSize += 11;
                            
                            for (var i = 0; i < sortedTxns[keyMY][0][keyDate][0][keyActNo].length; i++) {
                                
                                //
                                var segRowDataItem = new Object();
                                //segRowDataItem["template"] = hboxCalendarFourRowsTemplate;
                                var statusImg = ""; //changae this with default status image
                                //executed txns from calendar db and statement
                                if (sortedTxns[keyMY][0][keyDate][0][keyActNo][i]["finTxnRefID"] && sortedTxns[keyMY][0][keyDate][0][keyActNo][i]["finTxnRefID"] != "" && sortedTxns[keyMY][0][keyDate][0][keyActNo][i]["finTxnRefID"] != null || sortedTxns[keyMY][0][keyDate][0][keyActNo][i]["channelID"] != '01' && sortedTxns[keyMY][0][keyDate][0][keyActNo][i]["channelID"] != '02' && sortedTxns[keyMY][0][keyDate][0][keyActNo][i]["channelID"] != '10' && sortedTxns[keyMY][0][keyDate][0][keyActNo][i]["channelID"] != '11' && sortedTxns[keyMY][0][keyDate][0][keyActNo][i]["channelID"] != 'Internet Bank' && sortedTxns[keyMY][0][keyDate][0][keyActNo][i]["channelID"] != 'Mobile Banking') {
                                    //
                                    if (sortedTxns[keyMY][0][keyDate][0][keyActNo][i]["txnStatus"]) {
                                        if (sortedTxns[keyMY][0][keyDate][0][keyActNo][i]["txnStatus"] == "01") {
                                            if (gsFormId.id == "frmIBMyActivities") {
                                                statusImg = "cal_big_c.png";
                                            } else if (gsFormId.id == "frmMBMyActivities" || gsFormId.id == "frmIBPostLoginDashboard") {
                                                statusImg = "cal_status_completed.png";
                                            }
                                        } else if (sortedTxns[keyMY][0][keyDate][0][keyActNo][i]["txnStatus"] == "02") {
                                            if (gsFormId.id == "frmIBMyActivities") {
                                                statusImg = "cal_big_f.png";
                                            } else if (gsFormId.id == "frmMBMyActivities" || gsFormId.id == "frmIBPostLoginDashboard") {
                                                statusImg = "cal_status_failed.png";
                                            }
                                        } else if (sortedTxns[keyMY][0][keyDate][0][keyActNo][i]["txnStatus"] == "03") {
                                            if (sortedTxns[keyMY][0][keyDate][0][keyActNo][i]["txnType"] == "007") {
                                                if (gsFormId.id == "frmIBMyActivities") {
                                                    statusImg = "cal_big_f.png";
                                                } else if (gsFormId.id == "frmMBMyActivities" || gsFormId.id == "frmIBPostLoginDashboard") {
                                                    statusImg = "cal_status_failed.png";
                                                }
                                            } else {
                                                if (gsFormId.id == "frmIBMyActivities") {
                                                    statusImg = "cal_big_p.png";
                                                } else if (gsFormId.id == "frmMBMyActivities" || gsFormId.id == "frmIBPostLoginDashboard") {
                                                    statusImg = "cal_status_pending.png";
                                                }
                                            }
                                        }
                                    } else {
                                        //
                                        if (gsFormId.id == "frmIBMyActivities") {
                                            statusImg = "cal_big_p.png";
                                        } else if (gsFormId.id == "frmMBMyActivities" || gsFormId.id == "frmIBPostLoginDashboard") {
                                            statusImg = "cal_status_pending.png";
                                        }
                                    }
                                } else {
                                    //
                                    if (sortedTxns[keyMY][0][keyDate][0][keyActNo][i]["txnType"] == "007") {
                                        statusImg = "cal_ico_due.png";
                                    } else {
                                        statusImg = "cal_ico_scheduled.png";
                                    }
                                }
                                segRowDataItem["segImgStatus"] = statusImg;
                                //enable this when txnTime is available from Calendar db
                                //if (EnableDisableTimeFlag == true)
                                //{
                                var lblTxnTime = sortedTxns[keyMY][0][keyDate][0][keyActNo][i]["txnTime"];
                                var newTime = convertTime(lblTxnTime);
                                segRowDataItem["segLblTime"] = newTime;
                                //}
                                var tmpl = String(sortedTxns[keyMY][0][keyDate][0][keyActNo][i]["TxnDescTemplate"]);
                                // 
                                //tmpl = "Transfer via I-Banking\t20,000THB\rTo\tJohn Smith\rMy Note\ttest transfer";
                                //var result = tmpl.match(/(\\n|\\r)/g);
                                var patt1 = /\\n/g;
                                var patt2 = /\\r/g; //if there is \r in db, instead of \n
                                var noOfRows = [];
                                //var segObj = {};
                                if (tmpl.search(patt1) > 0) //expecting \n to be atleast after 5 chars which is the min size of first row? 
                                {
                                    noOfRows = tmpl.split(patt1);
                                } else {
                                    noOfRows[0] = tmpl;
                                }
                                //
                                //
                                var cols1, cols2, cols3;
                                if (noOfRows[0]) {
                                    cols1 = noOfRows[0].split("\\t");
                                    if (cols1.length == 1) {
                                        //
                                        segRowDataItem["segLblTxnTypeValue"] = tmpl;
                                        segRowDataItem["template"] = hboxCalendarOneRowTemplate;
                                    } else //it has 2 cols
                                    {
                                        //
                                        segRowDataItem["segLblTxnType"] = cols1[0];
                                        segRowDataItem["segLblTxnTypeValue"] = cols1[1];
                                    }
                                }
                                if (noOfRows[1]) {
                                    cols2 = noOfRows[1].split("\\t");
                                    // 
                                    segRowDataItem["segLblTo"] = cols2[0];
                                    segRowDataItem["segLblToValue"] = cols2[1];
                                }
                                if (noOfRows[2]) {
                                    cols3 = noOfRows[2].split("\\t");
                                    //
                                    segRowDataItem["segLblMyNote"] = cols3[0];
                                    segRowDataItem["segLblMyNoteValue"] = replaceHtmlTagChars(cols3[1]);
                                }
                                //Add click event only when the txn is not from statement
                                
                                if (sortedTxns[keyMY][0][keyDate][0][keyActNo][i]["finTxnRefID"] != "true") {
                                    
                                    if (gsFormId.id == "frmIBMyActivities" || gsFormId.id == "frmIBPostLoginDashboard") {
                                        
                                        segRowDataItem["template"] = hboxCalendarFourRowsTemplate;
                                    } else if (gsFormId.id == "frmMBMyActivities") {
                                        
                                        segRowDataItem["segImgArrow"] = "cal_list_arrow_big.png";
                                        segRowDataItem["template"] = hboxCalendarFourRowsTemplate;
                                    }
                                    //add a hidden label to hboxTxns. It's value can be either pincCode or FinTxnRefId or scheduleTxnRefId
                                    //set the text for the above hidden label with the required value, it could be either pinCode or finTxnRefID or finSchduleRefID. This value will be handled in the onClick of that hboxTxns.
                                    if (sortedTxns[keyMY][0][keyDate][0][keyActNo][i]["txnType"] == "007") //Beep and Bill
                                    {
                                        segRowDataItem["lblHidden"] = "bnb|||" + sortedTxns[keyMY][0][keyDate][0][keyActNo][i]["finFlexValues"] + "|||" + sortedTxns[keyMY][0][keyDate][0][keyActNo][i]["finTxnRefID"];
										segRowDataItem["metainfo"] = {
											clickable: true
                                        };
                                    } else if (sortedTxns[keyMY][0][keyDate][0][keyActNo][i]["finTxnRefID"]) {
                                        
                                        segRowDataItem["lblHidden"] = "executed|||" + sortedTxns[keyMY][0][keyDate][0][keyActNo][i]["finTxnRefID"] + "|||" + sortedTxns[keyMY][0][keyDate][0][keyActNo][i]["activityTypeID"];
										segRowDataItem["metainfo"] = {
											clickable: true
                                        };
                                    } else {
                                        
                                        if (sortedTxns[keyMY][0][keyDate][0][keyActNo][i]["finSchduleRefID"] != "") {
                                            segRowDataItem["lblHidden"] = "scheduled|||" + sortedTxns[keyMY][0][keyDate][0][keyActNo][i]["finSchduleRefID"];
											segRowDataItem["metainfo"] = {
												clickable: true
                                            };
                                        } else {
                                            
                                            segRowDataItem["metainfo"] = {
                                                clickable: false
                                            };
                                            if (gsFormId.id == "frmIBMyActivities" || gsFormId.id == "frmIBPostLoginDashboard") {
                                                
                                                segRowDataItem["segImgArrow"] = "";
                                                segRowDataItem["template"] = hboxCalendarFourRowsNoArrowTemplate;
                                            } else if (gsFormId.id == "frmMBMyActivities") {
                                                
                                                segRowDataItem["segImgArrow"] = "";
                                                segRowDataItem["template"] = hboxCalendarFourRowsTemplate;
                                            }
                                        }
                                    }
                                    
                                    if(sortedTxns[keyMY][0][keyDate][0][keyActNo][i]["channelID"] == "11" || sortedTxns[keyMY][0][keyDate][0][keyActNo][i]["channelID"] == "10") {
										segRowDataItem["lblHidden"] = "sme";
										segRowDataItem["segImgArrow"] = "";
										if (gsFormId.id == "frmIBMyActivities" || gsFormId.id == "frmIBPostLoginDashboard") {
                                        	segRowDataItem["template"] = hboxCalendarFourRowsNoArrowTemplate;
                                        }
										segRowDataItem["metainfo"] = {
											clickable: false
										};
									} 
									
                                } else {
                                    
                                    segRowDataItem["metainfo"] = {
                                        clickable: false
                                    };
                                    if (gsFormId.id == "frmIBMyActivities" || gsFormId.id == "frmIBPostLoginDashboard") {
                                        
                                        segRowDataItem["template"] = hboxCalendarFourRowsNoArrowTemplate;
                                    } else if (gsFormId.id == "frmMBMyActivities") {
                                        
                                        segRowDataItem["template"] = hboxCalendarFourRowsTemplate;
                                    }
                                }
                                
                                if (sortedTxns[keyMY][0][keyDate][0][keyActNo][i]["txnType"] == "007") //Beep and Bill
                                {
                                    var lDDate = sortedTxns[keyMY][0][keyDate][0][keyActNo][i]["dueDate"].split("-");
                                    var lDueDate = new Date(lDDate[0], (lDDate[1] - 1), lDDate[2]);
                                    
                                    if (lDueDate < gToDayYYYYMMDD) {
                                        
                                        segRowDataItem["metainfo"] = {
                                            clickable: false
                                        };
                                    }
									
                                }
                                //var lblTxnDateListView = sortedTxns[keyMY][0][keyDate][0][keyActNo][i]["txnDate"];
                                widgetIndex++;
                                if (gsFormId.id == "frmMBMyActivities" && reloadCalendar == 1) {
                                    segRowDataListView.push(segRowDataItem);
                                    
                                    rowIndex++;
                                }
                                if (keyDate == keySelDate) {
                                    segData.push(segRowDataItem);
                                    noOfTodaysTxns++;
                                }
                                if (gsFormId.id == "frmIBPostLoginDashboard" && widgetIndex == 2) {
                                    break; //since we should show the first 2 txns only in dashboard
                                }
                                segListSize += 22;
                            } //for (var i = 0; i < sortedTxns[keyMY][0][keyDate][0][keyActNo].length; i++)					
                        } //if (sortedTxns[keyMY][0][keyDate][0][keyActNo])
						if (gsFormId.id == "frmIBPostLoginDashboard" && widgetIndex == 2) {
							break;
						}
                    } //for (var keyActNo in sortedTxns[keyMY][0][keyDate][0])
                    if (gsFormId.id == "frmMBMyActivities" && reloadCalendar == 1) {
						
                        segRowListView.push(segRowDataListView);
                        segDataListView.push(segRowListView);
                    }
                } //if(sortedTxns[keyMY][0][keyDate])
				if (gsFormId.id == "frmIBPostLoginDashboard" && widgetIndex == 2) {
					break;
				}
            } //for (var d=0;d<keyDates.length;d++)			
        } //if (sortedTxns[keyMY])
        //prevent reloading list view transactions with every call of IBMyActivitiesCreateSegRowsForTxns()
        if (gsFormId.id == "frmMBMyActivities" && reloadCalendar == 1) {
            if (noOfTxns == 0) {
                
                gsFormId.lblNoActivities.setVisibility(false);
                var segData2 = new Array();
                var segRow2 = new Array();
                var segRowSecHdr2 = new Object();
                
                var selDate = gsSelectedDate.split("/");
                lMonth = Number(selDate[0]);
                
                if (lMonth < 10) lMonth = "0" + lMonth;
                
                
                var sDate = month[lMonth] + " " + selDate[2];
                segRowSecHdr2["lblDateHeader"] = sDate;
                segRow2.push(segRowSecHdr2);
                var segRowData2 = new Array();
                addExtraRowToListView(segRowData2, 0);
                
                segRow2.push(segRowData2);
                segData2.push(segRow2);
                gsFormId.segCalendarListView.separatorThickness = 0;
                gsFormId.segCalendarListView.setData(segData2);
                frmMBMyActivities.segCalendarListView.selectedIndex = [0, 0];
            } else {
                addExtraRowToListView(segDataListView, segListSize);
                
                gsFormId.segCalendarListView.separatorThickness = 1;
                gsFormId.segCalendarListView.setData(segDataListView);
                
                if(gListGoToBeginning && gListTab) {
    				var selSectionIndex = (gblKeyDates.length-1);
    				//gsFormId.segCalendarListView.selectedIndex = [selSectionIndex, 0];
    				gListGoToBeginning = 0;
                }
                else {
                	//gsFormId.segCalendarListView.selectedIndex = [0, 0];
                }
                
            }
        }
        
        //we should reload month/day view transactions with every call of IBMyActivitiesCreateSegRowsForTxns()
        if (noOfTodaysTxns == 0 && gListTab == 0) {
            
            gsFormId.lblNoActivities.setVisibility(true);
        } else {
            
            gsFormId.segCalendar.setData(segData);
        }
        
        
        gMyActivitiesSegScrollEventTriggered = 0;
        reloadCalendar = 0;
        if (gsFormId.id == "frmIBMyActivities" || (gsFormId.id == "frmIBPostLoginDashboard" && explicit != "excplisit")) {
            dismissLoadingScreenPopup();
            frmIBAccntSummary.imgProfile.setVisibility(true);
			frmIBPostLoginDashboard.imgProfile.setVisibility(true);
        } else if (gsFormId.id == "frmMBMyActivities") {
            
            kony.application.dismissLoadingScreen();
        }
    } else {
        
        if (gsFormId.id == "frmIBMyActivities" || (gsFormId.id == "frmIBPostLoginDashboard" && explicit != "excplisit")) {
            dismissLoadingScreenPopup();
            frmIBAccntSummary.imgProfile.setVisibility(true);
			frmIBPostLoginDashboard.imgProfile.setVisibility(true);
        } else if (gsFormId.id == "frmMBMyActivities") {
            
            //kony.application.dismissLoadingScreen();
        }
    }
}
/*
******************************************************************************************************************
  	Name    : IBMyActivitiesUpdateStatusImages
  	Author  : Phanidhar Anumula 
  	Date    : Sep 30 2013
  	Purpose : This module is to update status images on Calendar grid
*******************************************************************************************************************
--*/
function IBMyActivitiesUpdateStatusImages() {
    
    var selDate = gsSelectedDate.split("/"); //mm-dd-yyyy
    var keyMY = selDate[0] + "" + selDate[2];
    
    IBMyActivitiesSetDefaultStatusImages();
    
	//updating status images
    for (var keyDay in gStatusImages[keyMY]) {
        
        var keyDayTemp = new Number(keyDay); //to remove the prepended 0 if exists
        keyDayTemp = keyDayTemp.toString();
        //Find out where 1st day of the month is existing on the calendar grid
        //
        var d = new Date(selDate[2], (selDate[0] - 1), 1);
        //
        var firstDayOfTheMonth = d.getDay();
        //
        keyDayTemp = keyDayTemp - 1 + firstDayOfTheMonth;
        //		
        if (gsFormId.id == "frmIBMyActivities" || gsFormId.id == "frmIBPostLoginDashboard") {
            var imgTxnTimeId = gsFormId.id + "_imgTxnTime" + keyDayTemp;
            //
            var imgTxnTime = document.getElementById(imgTxnTimeId);
            //
			var patt = /Microsoft\s*Internet\s*Explorer/gi;
			if(!patt.test(navigator.appName))
			{
				imgTxnTime.style.removeProperty('width');
				imgTxnTime.style.removeProperty('height');
			}            
            var imgTxnTimeSpanId = gsFormId.id + "_imgTxnTime" + keyDayTemp + "_span";
            var imgTxnTimeSpan = document.getElementById(imgTxnTimeSpanId);
            if(!patt.test(navigator.appName))
			{
				imgTxnTimeSpan.style.display = "inline";
            }
			var imgTxnStatusId = gsFormId.id + "_imgTxnStatus" + keyDayTemp;
            //
            var imgTxnStatus = document.getElementById(imgTxnStatusId);
            //
            if(!patt.test(navigator.appName))
			{
				imgTxnStatus.style.removeProperty('width');
				imgTxnStatus.style.removeProperty('height');
            }
			var imgTxnStatusSpanId = gsFormId.id + "_imgTxnStatus" + keyDayTemp + "_span";
            var imgTxnStatusSpan = document.getElementById(imgTxnStatusSpanId);
            if(!patt.test(navigator.appName))
			{
				imgTxnStatusSpan.style.display = "inline";
            }
			//
            if (gStatusImages[keyMY][keyDay]["scheduled"] > 0 && gStatusImages[keyMY][keyDay]["due"] > 0) {
				//
                imgTxnTime.src = "desktopweb/images/cal_s_d.png";
            } else if (gStatusImages[keyMY][keyDay]["scheduled"] > 0) {
				//
                imgTxnTime.src = "desktopweb/images/cal_s.png";
            } else if (gStatusImages[keyMY][keyDay]["due"] > 0) {
                //
                imgTxnTime.src = "desktopweb/images/cal_d.png";
            }
            if (gStatusImages[keyMY][keyDay]["completed"] > 0 && gStatusImages[keyMY][keyDay]["failed"] > 0 && gStatusImages[keyMY][keyDay]["pending"] > 0) {
				//
                imgTxnStatus.src = "desktopweb/images/cal_c_f_p.png";
            } else if (gStatusImages[keyMY][keyDay]["completed"] > 0 && gStatusImages[keyMY][keyDay]["failed"] > 0) {
				//
                imgTxnStatus.src = "desktopweb/images/cal_c_f.png";
            } else if (gStatusImages[keyMY][keyDay]["completed"] > 0 && gStatusImages[keyMY][keyDay]["pending"] > 0) {
				//
                imgTxnStatus.src = "desktopweb/images/cal_c_p.png";
            } else if (gStatusImages[keyMY][keyDay]["failed"] > 0 && gStatusImages[keyMY][keyDay]["pending"] > 0) {
				//
                imgTxnStatus.src = "desktopweb/images/cal_f_p.png";
            } else if (gStatusImages[keyMY][keyDay]["completed"] > 0) {
				//
                imgTxnStatus.src = "desktopweb/images/cal_c.png";
            } else if (gStatusImages[keyMY][keyDay]["failed"] > 0) {
				//
                imgTxnStatus.src = "desktopweb/images/cal_f.png";
            } else if (gStatusImages[keyMY][keyDay]["pending"] > 0) {
				//
                imgTxnStatus.src = "desktopweb/images/cal_p.png";
            }
			//
			//
		}
		
		if (gsFormId.id == "frmMBMyActivities") {
			var imgTxnTimeId = "imgTxnTime" + keyDayTemp;
			//
			var imgTxnTime = gWidgetsOnCalGrid[imgTxnTimeId];
			//
			//imgTxnTime.style.removeProperty('width');
			//imgTxnTime.style.removeProperty('height');
			imgTxnTime.containerweight = 100;
			//
			
			//var imgTxnTimeSpanId = gsFormId.id + "_imgTxnTime" + keyDayTemp + "_span";
			//var imgTxnTimeSpan = document.getElementById(imgTxnTimeSpanId);        
			//imgTxnTimeSpan.style.display = "inline";
			
			var imgTxnStatusId = "imgTxnStatus" + keyDayTemp;
			//
			var imgTxnStatus = gWidgetsOnCalGrid[imgTxnStatusId];
			//
			//imgTxnStatus.style.removeProperty('width');
			//imgTxnStatus.style.removeProperty('height');
			imgTxnStatus.containerweight = 100;
			//
			
			//var imgTxnStatusSpanId = gsFormId.id + "_imgTxnStatus" + keyDayTemp + "_span";
			//var imgTxnStatusSpan = document.getElementById(imgTxnStatusSpanId);        
			//imgTxnStatusSpan.style.display = "inline";
			
			if(isIphone6)
			{
				if (gStatusImages[keyMY][keyDay]["scheduled"] > 0 && gStatusImages[keyMY][keyDay]["due"] > 0) {
                //
                imgTxnTime.src = "cal_s_d@3x.png";
				} else if (gStatusImages[keyMY][keyDay]["scheduled"] > 0) {
					//
					imgTxnTime.src = "cal_s@3x.png";
				} else if (gStatusImages[keyMY][keyDay]["due"] > 0) {
					//
					imgTxnTime.src = "cal_d@3x.png";
				}
				if (gStatusImages[keyMY][keyDay]["completed"] > 0 && gStatusImages[keyMY][keyDay]["failed"] > 0 && gStatusImages[keyMY][keyDay]["pending"] > 0) {
					//
					imgTxnStatus.src = "cal_c_f_p@3x.png";
				} else if (gStatusImages[keyMY][keyDay]["completed"] > 0 && gStatusImages[keyMY][keyDay]["failed"] > 0) {
					//
					imgTxnStatus.src = "cal_c_f@3x.png";
				} else if (gStatusImages[keyMY][keyDay]["completed"] > 0 && gStatusImages[keyMY][keyDay]["pending"] > 0) {
					//
					imgTxnStatus.src = "cal_c_p@3x.png";
				} else if (gStatusImages[keyMY][keyDay]["failed"] > 0 && gStatusImages[keyMY][keyDay]["pending"] > 0) {
					//
					imgTxnStatus.src = "cal_f_p@3x.png";
				} else if (gStatusImages[keyMY][keyDay]["completed"] > 0) {
					//
					imgTxnStatus.src = "cal_c@3x.png";
				} else if (gStatusImages[keyMY][keyDay]["failed"] > 0) {
					//
					imgTxnStatus.src = "cal_f@3x.png";
				} else if (gStatusImages[keyMY][keyDay]["pending"] > 0) {
				   // 
					imgTxnStatus.src = "cal_p@3x.png";
				}
				
			}
			else
			{
				if (gStatusImages[keyMY][keyDay]["scheduled"] > 0 && gStatusImages[keyMY][keyDay]["due"] > 0) {
                //
                imgTxnTime.src = "cal_s_d.png";
				} else if (gStatusImages[keyMY][keyDay]["scheduled"] > 0) {
					//
					imgTxnTime.src = "cal_s.png";
				} else if (gStatusImages[keyMY][keyDay]["due"] > 0) {
					//
					imgTxnTime.src = "cal_d.png";
				}
				if (gStatusImages[keyMY][keyDay]["completed"] > 0 && gStatusImages[keyMY][keyDay]["failed"] > 0 && gStatusImages[keyMY][keyDay]["pending"] > 0) {
					//
					imgTxnStatus.src = "cal_c_f_p.png";
				} else if (gStatusImages[keyMY][keyDay]["completed"] > 0 && gStatusImages[keyMY][keyDay]["failed"] > 0) {
					//
					imgTxnStatus.src = "cal_c_f.png";
				} else if (gStatusImages[keyMY][keyDay]["completed"] > 0 && gStatusImages[keyMY][keyDay]["pending"] > 0) {
					//
					imgTxnStatus.src = "cal_c_p.png";
				} else if (gStatusImages[keyMY][keyDay]["failed"] > 0 && gStatusImages[keyMY][keyDay]["pending"] > 0) {
					//
					imgTxnStatus.src = "cal_f_p.png";
				} else if (gStatusImages[keyMY][keyDay]["completed"] > 0) {
					//
					imgTxnStatus.src = "cal_c.png";
				} else if (gStatusImages[keyMY][keyDay]["failed"] > 0) {
					//
					imgTxnStatus.src = "cal_f.png";
				} else if (gStatusImages[keyMY][keyDay]["pending"] > 0) {
				   // 
					imgTxnStatus.src = "cal_p.png";
				}
			}
            
			//
			//
		}        
    }
	
}

function IBMyActivitiesSetDefaultStatusImages()
{
	
	if (gStatusImages[gPrevSelMY]) {
        //
        //
		var month = gPrevSelMY.charAt(0)+""+gPrevSelMY.charAt(1);
		var year = gPrevSelMY.charAt(2)+""+gPrevSelMY.charAt(3)+""+gPrevSelMY.charAt(4)+""+gPrevSelMY.charAt(5);
        for (var keyDay in gStatusImages[gPrevSelMY]) {
            var keyDayTemp = new Number(keyDay); //to remove the prepended 0 if exists
            keyDayTemp = keyDayTemp.toString();
            //Find out where 1st day of the month is existing on the calendar grid
            //
            var d = new Date(year, (month - 1), 1);
            //
            var firstDayOfTheMonth = d.getDay();
            //
            keyDayTemp = keyDayTemp - 1 + firstDayOfTheMonth;
            //		
            
			if (gsFormId.id == "frmIBMyActivities" || gsFormId.id == "frmIBPostLoginDashboard" || gsFormId.id == "frmIBDateSlider") {
				var imgTxnTimeId = gsFormId.id + "_imgTxnTime" + keyDayTemp;
				//
				var imgTxnTime = document.getElementById(imgTxnTimeId);
				//
				//imgTxnTime.src = "desktopweb/images/empty.png";
				imgTxnTime.src = "desktopweb/images/cal_empty_top.png";
				var imgTxnStatusId = gsFormId.id + "_imgTxnStatus" + keyDayTemp;
				//
				var imgTxnStatus = document.getElementById(imgTxnStatusId);
				//
				//imgTxnStatus.src = "desktopweb/images/empty.png";
				imgTxnStatus.src = "desktopweb/images/cal_empty_bottom.png";
			}
			
			if (gsFormId.id == "frmMBMyActivities") {
				
				var imgTxnTimeId = "imgTxnTime" + keyDayTemp;
				//
				var imgTxnTime = gWidgetsOnCalGrid[imgTxnTimeId];
				//
				if(isIphone6)
				{
					imgTxnTime.src = "cal_ico_top_empty@3x.png";
				}
				else
				{
					imgTxnTime.src = "cal_ico_top_empty.png";
				}
				var imgTxnStatusId = "imgTxnStatus" + keyDayTemp;
				//
				var imgTxnStatus = gWidgetsOnCalGrid[imgTxnStatusId];
				//
				if(isIphone6)
				{
					imgTxnStatus.src = "cal_ico_btm_empty@3x.png";
				}
				else
				{
					imgTxnStatus.src = "cal_ico_btm_empty.png";
				}
			}
			
        }
    }
    
}

function IBMyActivitiesSetDefaultStatusImages2() {
    
    for (var i=0; i<42; i++) {
		var keyDayTemp = new Number(i);
		keyDayTemp = keyDayTemp.toString();
		
		if (gsFormId.id == "frmIBMyActivities" || gsFormId.id == "frmIBPostLoginDashboard" ||gsFormId.id == "frmIBDateSlider") {
			var imgTxnTimeId = gsFormId.id + "_imgTxnTime" + keyDayTemp;
			var imgTxnTime = document.getElementById(imgTxnTimeId);
			imgTxnTime.src = "desktopweb/images/cal_empty_top.png";
			var imgTxnStatusId = gsFormId.id + "_imgTxnStatus" + keyDayTemp;
			var imgTxnStatus = document.getElementById(imgTxnStatusId);
            imgTxnStatus.src = "desktopweb/images/cal_empty_bottom.png";
		}
		else if (gsFormId.id == "frmMBMyActivities") {
			var imgTxnTimeId = "imgTxnTime" + keyDayTemp;
            var imgTxnTime = gWidgetsOnCalGrid[imgTxnTimeId];
            if(isIphone6)
			{
				imgTxnTime.src = "cal_ico_top_empty@3x.png";
			}
			else
			{
				imgTxnTime.src = "cal_ico_top_empty.png";
			}
            var imgTxnStatusId = "imgTxnStatus" + keyDayTemp;
            var imgTxnStatus = gWidgetsOnCalGrid[imgTxnStatusId];
            if(isIphone6)
			{
					imgTxnStatus.src = "cal_ico_btm_empty@3x.png";
			}
			else
			{
						imgTxnStatus.src = "cal_ico_btm_empty.png";
			}
		}
	}
	
}

/*
******************************************************************************************************************
  	Name    : IBMyActivitiesGetAllWidgetsOnCalGrid
  	Author  : Phanidhar Anumula 
  	Date    : Oct 29 2013
  	Purpose : This module is to get all widget id's available on calendar grid, to set status images
			  ex: gWidgetsOnCalGrid["frmIBMyActivities_imgTxnTime0"] = frmIBMyActivities.frmIBMyActivities_imgTxnTime0 i.e string = object
*******************************************************************************************************************
--*/
function IBMyActivitiesGetAllWidgetsOnCalGrid()
{
	
		
	gWidgetsOnCalGrid ={imgTxnTime0 : gsFormId.imgTxnTime0, imgTxnTime1 : gsFormId.imgTxnTime1, imgTxnTime2 : gsFormId.imgTxnTime2, imgTxnTime3 : gsFormId.imgTxnTime3, imgTxnTime4 : gsFormId.imgTxnTime4, imgTxnTime5 : gsFormId.imgTxnTime5, imgTxnTime6 : gsFormId.imgTxnTime6, imgTxnTime7 : gsFormId.imgTxnTime7, imgTxnTime8 : gsFormId.imgTxnTime8, imgTxnTime9 : gsFormId.imgTxnTime9, imgTxnTime10 : gsFormId.imgTxnTime10, imgTxnTime11 : gsFormId.imgTxnTime11, imgTxnTime12 : gsFormId.imgTxnTime12, imgTxnTime13 : gsFormId.imgTxnTime13, imgTxnTime14 : gsFormId.imgTxnTime14, imgTxnTime15 : gsFormId.imgTxnTime15, imgTxnTime16 : gsFormId.imgTxnTime16, imgTxnTime17 : gsFormId.imgTxnTime17, imgTxnTime18 : gsFormId.imgTxnTime18, imgTxnTime19 : gsFormId.imgTxnTime19, imgTxnTime20 : gsFormId.imgTxnTime20, imgTxnTime21 : gsFormId.imgTxnTime21, imgTxnTime22 : gsFormId.imgTxnTime22, imgTxnTime23 : gsFormId.imgTxnTime23, imgTxnTime24 : gsFormId.imgTxnTime24, imgTxnTime25 : gsFormId.imgTxnTime25, imgTxnTime26 : gsFormId.imgTxnTime26, imgTxnTime27 : gsFormId.imgTxnTime27, imgTxnTime28 : gsFormId.imgTxnTime28, imgTxnTime29 : gsFormId.imgTxnTime29, imgTxnTime30 : gsFormId.imgTxnTime30, imgTxnTime31 : gsFormId.imgTxnTime31, imgTxnTime32 : gsFormId.imgTxnTime32, imgTxnTime33 : gsFormId.imgTxnTime33, imgTxnTime34 : gsFormId.imgTxnTime34, imgTxnTime35 : gsFormId.imgTxnTime35, imgTxnTime36 : gsFormId.imgTxnTime36, imgTxnTime37 : gsFormId.imgTxnTime37, imgTxnTime38 : gsFormId.imgTxnTime38, imgTxnTime39 : gsFormId.imgTxnTime39, imgTxnTime40 : gsFormId.imgTxnTime40, imgTxnTime41 : gsFormId.imgTxnTime41, imgTxnStatus0 : gsFormId.imgTxnStatus0, imgTxnStatus1 : gsFormId.imgTxnStatus1, imgTxnStatus2 : gsFormId.imgTxnStatus2, imgTxnStatus3 : gsFormId.imgTxnStatus3, imgTxnStatus4 : gsFormId.imgTxnStatus4, imgTxnStatus5 : gsFormId.imgTxnStatus5, imgTxnStatus6 : gsFormId.imgTxnStatus6, imgTxnStatus7 : gsFormId.imgTxnStatus7, imgTxnStatus8 : gsFormId.imgTxnStatus8, imgTxnStatus9 : gsFormId.imgTxnStatus9, imgTxnStatus10 : gsFormId.imgTxnStatus10, imgTxnStatus11 : gsFormId.imgTxnStatus11, imgTxnStatus12 : gsFormId.imgTxnStatus12, imgTxnStatus13 : gsFormId.imgTxnStatus13, imgTxnStatus14 : gsFormId.imgTxnStatus14, imgTxnStatus15 : gsFormId.imgTxnStatus15, imgTxnStatus16 : gsFormId.imgTxnStatus16, imgTxnStatus17 : gsFormId.imgTxnStatus17, imgTxnStatus18 : gsFormId.imgTxnStatus18, imgTxnStatus19 : gsFormId.imgTxnStatus19, imgTxnStatus20 : gsFormId.imgTxnStatus20, imgTxnStatus21 : gsFormId.imgTxnStatus21, imgTxnStatus22 : gsFormId.imgTxnStatus22, imgTxnStatus23 : gsFormId.imgTxnStatus23, imgTxnStatus24 : gsFormId.imgTxnStatus24, imgTxnStatus25 : gsFormId.imgTxnStatus25, imgTxnStatus26 : gsFormId.imgTxnStatus26, imgTxnStatus27 : gsFormId.imgTxnStatus27, imgTxnStatus28 : gsFormId.imgTxnStatus28, imgTxnStatus29 : gsFormId.imgTxnStatus29, imgTxnStatus30 : gsFormId.imgTxnStatus30, imgTxnStatus31 : gsFormId.imgTxnStatus31, imgTxnStatus32 : gsFormId.imgTxnStatus32, imgTxnStatus33 : gsFormId.imgTxnStatus33, imgTxnStatus34 : gsFormId.imgTxnStatus34, imgTxnStatus35 : gsFormId.imgTxnStatus35, imgTxnStatus36 : gsFormId.imgTxnStatus36, imgTxnStatus37 : gsFormId.imgTxnStatus37, imgTxnStatus38 : gsFormId.imgTxnStatus38, imgTxnStatus39 : gsFormId.imgTxnStatus39, imgTxnStatus40 : gsFormId.imgTxnStatus40, imgTxnStatus41 : gsFormId.imgTxnStatus41};	
	
	//
}

/*
**************************************************************************************************************************************
  	Name    : frmMBMyActivities_init_segDayView()
  	Author  : Phanidhar Anumula 
  	Date    : Aug 27 2013
  	Purpose : To display Segment in Calendar MB Day View Page
  	Trigger : Click on My Activities tab
**************************************************************************************************************************************
--*/
var calSelIndex = [0, 0];
var calLength = -1;

var monthNumber=new Array();

function frmMBMyActivities_init_segDayView() {	
	
	//
	var lStartDate = new Date(gStartDate.getFullYear(), gStartDate.getMonth(), gStartDate.getDate());
	//
	//
	calLength = -1;
	monthNumber[kony.i18n.getLocalizedString("keyCalendarJanuary")]=0;
	monthNumber[kony.i18n.getLocalizedString("keyCalendarFebruary")]=1;
	monthNumber[kony.i18n.getLocalizedString("keyCalendarMarch")]=2;
	monthNumber[kony.i18n.getLocalizedString("keyCalendarApril")]=3;
	monthNumber[kony.i18n.getLocalizedString("keyCalendarMayFull")]=4;
	monthNumber[kony.i18n.getLocalizedString("keyCalendarJune")]=5;
	monthNumber[kony.i18n.getLocalizedString("keyCalendarJuly")]=6;
	monthNumber[kony.i18n.getLocalizedString("keyCalendarAugust")]=7;
	monthNumber[kony.i18n.getLocalizedString("keyCalendarSeptember")]=8;
	monthNumber[kony.i18n.getLocalizedString("keyCalendarOctober")]=9;
	monthNumber[kony.i18n.getLocalizedString("keyCalendarNovember")]=10;
	monthNumber[kony.i18n.getLocalizedString("keyCalendarDecember")]=11;
	
	var weekday=new Array(7);
	weekday[0]=kony.i18n.getLocalizedString("keyCalendarSunday");
	weekday[1]=kony.i18n.getLocalizedString("keyCalendarMonday");
	weekday[2]=kony.i18n.getLocalizedString("keyCalendarTuesday");
	weekday[3]=kony.i18n.getLocalizedString("keyCalendarWednesday");
	weekday[4]=kony.i18n.getLocalizedString("keyCalendarThursday");
	weekday[5]=kony.i18n.getLocalizedString("keyCalendarFriday");
	weekday[6]=kony.i18n.getLocalizedString("keyCalendarSaturday");
	
	var month=new Array();
	month[0]=kony.i18n.getLocalizedString("keyCalendarJanuary");
	month[1]=kony.i18n.getLocalizedString("keyCalendarFebruary");
	month[2]=kony.i18n.getLocalizedString("keyCalendarMarch");
	month[3]=kony.i18n.getLocalizedString("keyCalendarApril");
	month[4]=kony.i18n.getLocalizedString("keyCalendarMayFull");
	month[5]=kony.i18n.getLocalizedString("keyCalendarJune");
	month[6]=kony.i18n.getLocalizedString("keyCalendarJuly");
	month[7]=kony.i18n.getLocalizedString("keyCalendarAugust");
	month[8]=kony.i18n.getLocalizedString("keyCalendarSeptember");
	month[9]=kony.i18n.getLocalizedString("keyCalendarOctober");
	month[10]=kony.i18n.getLocalizedString("keyCalendarNovember");
	month[11]=kony.i18n.getLocalizedString("keyCalendarDecember");
	//
	//
	//

	//to set the corousel effect for android
	//#ifdef android
 		gsFormId.segDayView.viewConfig = {
        	coverflowConfig : {
            	projectionAngle : 60,
                rowItemRotationAngle : 45,
                spaceBetweenRowItems : 1,
                rowItemWidth : 60,
                isCircular : true
            }
        }   
	//#endif

	//
	var segData = [];
	var lDay, lDate, lMnY, x, y, z; 
	var sliderTemplate = hboxSegSliderCalendar2;
	//
	
	while(lStartDate <= gEndDate)
	{
		//
		x = lStartDate.getDay();		
		lDay = weekday[x];
		y = lStartDate.getDate();
		lDate = y.toString();
		z = lStartDate.getMonth(); 
		z = month[z]+" "+lStartDate.getFullYear();
		lMnY = z;
		if(sliderTemplate.id == "hboxSegSliderCalendar2")
		{
			sliderTemplate = hboxSegSliderCalendar;
		}
		else
		{
			sliderTemplate = hboxSegSliderCalendar2;
		}	
		var temp = [{
			lblDay: lDay,
			lblDate: lDate,
			lblMnY: lMnY,
			template: sliderTemplate
		}];
		kony.table.insert(segData, temp[0]);		
		lStartDate.setDate(lStartDate.getDate() + 1);
		//
		calLength++;
	}
	
	gsFormId.segDayView.data = [];
	gsFormId.segDayView.data = segData;
	//gsFormId.segDayView.containerWeight = 100;
	
	
	
	
		
}

/*
**************************************************************************************************************************************
  	Name    : frmMBMyActivities_onClick_segDayView
  	Author  : Phanidhar Anumula 
  	Date    : Aug 27 2013
  	Purpose : To set index of the Segment in Calendar MB Day View Page
  	Trigger : Click on Day view tab 
**************************************************************************************************************************************
--*/
function frmMBMyActivities_onClick_segDayView() {
				
	var oneDay = 24*60*60*1000;	// hours*minutes*seconds*milliseconds
	var selDate = gsSelectedDate.split("/");
    var lSelDate = new Date(selDate[2], (selDate[0]-1), selDate[1]);
    // 
    //
	var diffDays = Math.abs((gStartDate.getTime() - lSelDate.getTime())/(oneDay));
	
	gsFormId.segDayView.selectedIndex=[0,diffDays];	
	
	gPrevCalSelIndex = gsFormId.segDayView.selectedIndex[1];
	
	gsFormId.leftCal.setEnabled(true);
	gsFormId.rightCal.setEnabled(true);
	
}

function frmMBMyActivities_onClick_leftCal() {
	
	calSelIndex = gsFormId.segDayView.selectedIndex;
	
	//when segment length < 0
	if (calSelIndex[1] < 0) {
		return false;
	}
	else if (calSelIndex[1] > 0) {
		gsFormId.segDayView.selectedIndex = [0, (calSelIndex[1] - 1)];
		
		gsFormId.leftCal.setEnabled(true);
		calSelIndex = gsFormId.segDayView.selectedIndex;
		
	}
	else if (calSelIndex[1] == 0) {
		//gsFormId.segDayView.selectedIndex = [0, calLength];
		
		gsFormId.leftCal.setEnabled(false);
	}
	gsFormId.rightCal.setEnabled(true);
	//calSelIndex = gsFormId.segDayView.selectedIndex;
	
	//show prev day txns
	
	gPrevCalSelIndex = gsFormId.segDayView.selectedIndex[1];
	
	var indexOfSelectedIndex = gsFormId.segDayView.selectedItems[0];
	var selDay = indexOfSelectedIndex.lblDate;
	//
	var tmp1 = (indexOfSelectedIndex.lblMnY)
	var tmp = tmp1.split(/\s+/);
	var selMonth = monthNumber[tmp[0]] + 1;
	//
	var selYear = tmp[1];
	//
	gsSelectedDate = getCalDateMDY(selMonth, selDay, selYear);
	
	showCalendar(gsSelectedDate, gsFormId);
}

function frmMBMyActivities_onClick_rightCal() {
	
	calSelIndex = gsFormId.segDayView.selectedIndex;
		
	//when segment length < 0
	if (calSelIndex[1] > calLength) {
		return false;
	}
	
	else if (calSelIndex[1] < calLength) {
		gsFormId.segDayView.selectedIndex = [0, (calSelIndex[1] + 1)];
		
		gsFormId.rightCal.setEnabled(true);
		calSelIndex = gsFormId.segDayView.selectedIndex;
		
	}
	else if (calSelIndex[1] == calLength) {
		//gsFormId.segDayView.selectedIndex = [0, 0];
		
		gsFormId.rightCal.setEnabled(false);
	}
	gsFormId.leftCal.setEnabled(true);
	//calSelIndex = gsFormId.segDayView.selectedIndex;
	//show next day txns
	
	gPrevCalSelIndex = gsFormId.segDayView.selectedIndex[1];
	
	var indexOfSelectedIndex = gsFormId.segDayView.selectedItems[0];
	var selDay = indexOfSelectedIndex.lblDate;
	//
	var tmp1 = (indexOfSelectedIndex.lblMnY)
	var tmp = tmp1.split(/\s+/);
	var selMonth = monthNumber[tmp[0]] + 1;
	//
	var selYear = tmp[1];
	//
	gsSelectedDate = getCalDateMDY(selMonth, selDay, selYear);
	
	showCalendar(gsSelectedDate, gsFormId);
}

function frmMBMyActivities_segDayView_swipe() {
	
	if(gblMBCalView == 2)
	{
		
		
		calSelIndex = gsFormId.segDayView.selectedIndex;
		
		
		if (gPrevCalSelIndex > 300 && calSelIndex[1] < 50) {
			
			gsFormId.segDayView.selectedIndex = [0, calLength];
			gsFormId.leftCal.setEnabled(true);
			gsFormId.rightCal.setEnabled(false);		
		}
		else if (gPrevCalSelIndex < 50 && calSelIndex[1] > 300) {
			
			gsFormId.segDayView.selectedIndex = [0, 0];
			gsFormId.leftCal.setEnabled(false);
			gsFormId.rightCal.setEnabled(true);
		}
			
		//show next day txns
		
		gPrevCalSelIndex = gsFormId.segDayView.selectedIndex[1];
		var indexOfSelectedIndex = gsFormId.segDayView.selectedItems[0];
		var selDay = indexOfSelectedIndex.lblDate;
		//
		var tmp1 = (indexOfSelectedIndex.lblMnY)
		var tmp = tmp1.split(/\s+/);
		var selMonth = monthNumber[tmp[0]] + 1;
		//
		var selYear = tmp[1];
		//
		gsSelectedDate = getCalDateMDY(selMonth, selDay, selYear);
		
		showCalendar(gsSelectedDate, gsFormId);
	}
	else
	{
		
	}
	
}

function MBMyActivitiesShowCalendar()
{
	
	
	gDeleteThisKeyMY = 0; //to not to reload calendar
	//Code changes for IOS9	
	if(gblMBCalView == 1) {
		calendarSetVisibilityMonthViewMB();
	}
	else if(gblMBCalView == 2) {
		calendarSetVisibilityDayViewMB();
	}
	else if(gblMBCalView == 3) {
		calendarSetVisibilityListViewMB();
	}
	frmMBMyActivities.show();
}

function MBMyActivitiesReloadAndShowCalendar()
{
	
	
	gDeleteThisKeyMY = 1; //to reload calendar
	//Code Changes for IOS9	
	if(gblMBCalView == 1) {
		calendarSetVisibilityMonthViewMB();
	}
	else if(gblMBCalView == 2) {
		calendarSetVisibilityDayViewMB();
	}
	else if(gblMBCalView == 3) {
		calendarSetVisibilityListViewMB();
	}
	frmMBMyActivities.show();
}

function IBMyActivitiesShowCalendar() {
	var prevForm = kony.application.getPreviousForm().id;
	
	if(prevForm == "frmIBPostLoginDashboard")
		frmIBPostLoginDashboard.show();
	else if(prevForm == "frmIBDateSlider"){
		gDeleteThisKeyMY = 0; //to not to reload calendar
		gblDateReset = "1";
		//gblPreshow = "1";
		reloadCalendar = 1;
		onLocaleChange();
		frmIBDateSlider.show();
	}else{
		gDeleteThisKeyMY = 0; //to not to reload calendar
		gblDateReset = "1";
		//gblPreshow = "1";
		reloadCalendar = 1;
		//onLocaleChange(); as it was done for transaction history. Now it has own function
		frmIBMyActivities.show();
		showCalendar(gsSelectedDate, frmIBMyActivities);
	}
}

function IBMyActivitiesReloadAndShowCalendar()
{
	gDeleteThisKeyMY = 1; //to reload calendar
	gblDateReset = "1";
	//gblPreshow = "1";
	reloadCalendar = 1;
	onLocaleChange();
	if(gsFormId.id != "frmIBPostLoginDashboard") {
		frmIBMyActivities.show();
		showCalendar(gsSelectedDate,frmIBMyActivities);
	}
	else
		frmIBPostLoginDashboard.show();
}

//function masterBillerInquiryForCalAsyncCallback(status, collectionData) {
//    
//    if (status == 400) {
//        if (collectionData["opstatus"] == 0) {
//            var masterBillerInqRs = collectionData["MasterBillerInqRs"];
//            for (var i = 0; i < masterBillerInqRs.length; i++) {
//                if (masterBillerInqRs[i]["BillerShortName"] != null) {
//                    var billerShortName = masterBillerInqRs["BillerShortName"];
//                }
//            }
//        } else {
//           // alert("........");
//        }
//    } else {
//        if (status == 300) {
//           // alert("........");
//        }
//    }
//    
//}
// End of Flow for Executed Transactions in Calendar
// Start of Beep and bill flow in Calendar
//function executeBB() {
//    // TODO: need to place in invisible lable of hbox for each activity
//    var pinCode = "abc"; //form.lable.text
//    customerPaymentStatusInquiryForBB(pinCode);
//}
// End of Beep and bill flow in Calendar

function convertTime(lblTxnTime) {
	if(lblTxnTime == undefined || lblTxnTime == null)
		return "12:00 AM";
	
	var lblTxnTimeSplit = lblTxnTime.split(":");
	
	
	if(lblTxnTimeSplit[0][0] == '0')
		var num = parseInt(lblTxnTimeSplit[0][1]);
	else
		var num = parseInt(lblTxnTimeSplit[0]);
	
	if(num > 12) {
		num = num%12;
		if(num < 10)
			lblTxnTimeSplit[0] = "0" + num.toString();
		else
			lblTxnTimeSplit[0] = num.toString();
		var ampm = "PM";
	}
	
	else if(num == 12) {
		lblTxnTimeSplit[0] = '12'
		var ampm = "PM";
	}
	
	else if(num == 0) {
		lblTxnTimeSplit[0] = '12';
		var ampm = "AM";
	}
	
	else {
		var ampm = "AM";
	}

	return (lblTxnTimeSplit[0] + ":" + lblTxnTimeSplit[1] + " " + ampm);
}


function frmMBMyActivities_preshow() {
	
    isMenuShown = false;
	isSignedUser=true;
	frmMBMyActivities.scrollboxMain.scrollToEnd();
	
	if((typeof(frmMBMyActivities.segCalendarListView) == 'undefined') || (frmMBMyActivities.segCalendarListView == null))
	{
	    
        IBMyActivitiesCreateSegCalendarListView();
    }
    
	if(gblMBCalView == 1) 
	{
		calendarSetVisibilityMonthViewMB();		
	}
	else if(gblMBCalView == 2)
	{
		calendarSetVisibilityDayViewMB();
	}
	else
	{
		calendarSetVisibilityListViewMB()
	}
	
	if(typeof gsSelectedDate != 'undefined')
	{	
		showCalendar(gsSelectedDate, frmMBMyActivities);
	}
	else
	{
		showCalendar("", frmMBMyActivities);
	}
	
	frmMBMyActivities_init_segDayView();
	
}

/*
**************************************************************************************************************************************
  	Name    : calendar_form_preshow()
  	Author  : Phanidhar Anumula 
  	Date    : Mar 21 2014
  	Purpose : To prevent executing form preShow when user is returning back to Calendar page from any transactions complete screens
  			  And to prevent highlighting a previous selected sectionheader on MB List View	
  	Trigger : User navigated to complete screens and trying to returning back to calendar page
**************************************************************************************************************************************
--*/
function calendar_form_preshow() {
	
	
	if((typeof(gDeleteThisKeyMY) == 'undefined') || ((typeof(gDeleteThisKeyMY) != 'undefined') && (gDeleteThisKeyMY == 1)))
	{
		
		reloadCalendar = 1;
		frmMBMyActivities_preshow();
	}
	else if((typeof(gsSelectedDate) == 'undefined') || ((typeof(gsSelectedDate) != 'undefined') && (gsSelectedDate == null)))
	{
		
		reloadCalendar = 1;
		frmMBMyActivities_preshow();
	}
	else
	{		
		
		reloadCalendar = 0;
		return true;
	}
	    
}
