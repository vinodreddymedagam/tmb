







/**
 * description
 * @returns {}
 */

//function showConfPopup(imgIcon, confText, callBackOnConfirm) {
//	popupConfirmation.imgPopConfirmIcon.src = imgIcon;
//	popupConfirmation.lblPopupConfText.text = confText;
//	popupConfirmation.btnpopConfConfirm.onClick = callBackOnConfirm;
//	popupConfirmation.show();
//}
/**
 * description
 * @returns {}
 */

//function onConfirmCall() {
//	alert("ConfCalled");
//}
/*
 * description
 * @returns {}
 */

//function showLogoutPopup() {
//	popUpLogout.containerWeight = 94;
//	popUpLogout.show();
//}
/**
 * typeFlag 1 means OTP , 2 means access pin and 3 means transaction pwd
 * @returns {}
 */

function showOTPPopup(lblText, refNo, mobNO, callBackConfirm, typeFlag) {
	//Please DO NOT KEEP THIS FUNCTION OUTSIDE. This is inner function assigned to popupTractPwd.btnPopupTractConf.onClick
	popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = tbxPopupBlue;
	function onConfClick() {
		//popupTractPwd.dismiss();
		var enteredText = "";
		if (typeFlag == 1) {
			//enteredText = popupTractPwd.tbxPopupTractPwdtxt.text;
			enteredText = popupTractPwd.txtOTP.text;
			if (popupTractPwd.hbxIncorrectOTP.isVisible == true) {
				enteredText = popupTractPwd.txtIncorrectOTP.text;
			}
		} else if (typeFlag == 2) {
			enteredText = popupTractPwd.tbxPopupTractPwdtxtAccPin.text;
		} else if (typeFlag == 3) {
			enteredText = popupTractPwd.tbxPopupTractPwdtxtTranscPwd
				.text;
		}
		callBackConfirm(enteredText);
	}
	popupTractPwd.containerWeight = 94;
	popupTractPwd.btnPopupTractConf.onClick = onConfClick;
	popupTractPwd.btnPopupTractConf.text = kony.i18n.getLocalizedString(
		"keyConfirm");
	// popupTractPwd.tbxPopupTractPwdtxt.text = "";
	popupTractPwd.txtOTP.text = "";
	//popupTractPwd.lblPopTractPwdtxt.text = lblText;
	popupTractPwd.lblOTP.text = lblText;
	popupTractPwd.hbxIncorrectOTP.isVisible = false;
	if (typeFlag == 1) {
		popupTractPwd.lblPopupTract1.isVisible = true;
		if (flowSpa) {
			popupTractPwd.hbxPopupTractlblHoldSpa.isVisible = true;
		} else {
			popupTractPwd.hbxPopupTractlblHold.isVisible = true;
		}
		popupTractPwd.lblPopupTract5.text = kony.i18n.getLocalizedString("keyotpmsgreq");
		popupTractPwd.lblPopupTract5.isVisible = true;
		if (flowSpa) {
			popupTractPwd.lblPopupTract4Spa.text = mobNO;
		} else {
			popupTractPwd.lblPopupTract4.text = mobNO;
		}
		popupTractPwd.btnPopUpTractCancel.skin = btnLightBlue;
		popupTractPwd.btnPopUpTractCancel.focusSkin = btnLightBlue;
		popupTractPwd.btnPopUpTractCancel.setEnabled(true);
		popupTractPwd.btnPopUpTractCancel.onClick = onClickAccessPinCancel;
		popupTractPwd.btnPopUpTractCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
		if (kony.i18n.getCurrentLocale() == "th_TH") {
			//popupTractPwd.lblPopTractPwdtxt.containerWeight = 25;
			popupTractPwd.lblOTP.containerWeight = 12;
		} else {
			//popupTractPwd.lblPopTractPwdtxt.containerWeight = 12;
			popupTractPwd.lblOTP.containerWeight = 12;
		}
		//popupTractPwd.lblPopupTract1.text = kony.i18n.getLocalizedString("keybankrefno")+gblRefNum; Commented as added in onClickOTPRequest
		//popupTractPwd.hbxPoupOTP.isVisible = true;
		popupTractPwd.hbxOTP.isVisible = true;
		popupTractPwd.hbxPoupAccesspin.isVisible = false;
		popupTractPwd.hbxPopupTranscPwd.isVisible = false;
		//popupTractPwd.lblPopTractPwdtxt.text = lblText;
		popupTractPwd.lblOTP.text = lblText;
		//gblOTPFlag = true;
		//onClickOTPRequest();
		//popupTractPwd.tbxPopupTractPwdtxt.text = "";
		popupTractPwd.txtOTP.textInputMode = constants.TEXTBOX_INPUT_MODE_NUMERIC;
		
		popupTractPwd.txtOTP.keyBoardStyle = constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD;
		
		if (flowSpa) {
			popupTractPwd.txtOTP.textInputMode = constants.TEXTBOX_INPUT_MODE_ANY;
		}
		if(gblMBActivationVia == "2"){
			// activation via ib login
			popupTractPwd.btnOtpRequest.onClick = activationViaIBUserOTP;
			popupTractPwd.btnOtpRequest.setEnabled(false);
			popupTractPwd.btnOtpRequest.skin=btnDisabledGray;
			popupTractPwd.btnOtpRequest.focusSkin=btnDisabledGray;
			popupTractPwd.btnPopupTractConf.onClick=verifyOTPActivationIBUserOTP;
		}else{
			popupTractPwd.btnOtpRequest.onClick = onClickActiRequestOtp;
		}
	} else if (typeFlag == 2) {
		popupTractPwd.line502709349207.isVisible = false;
		popupTractPwd.lblPopupTract1.isVisible = false;
		if (flowSpa) {
			popupTractPwd.hbxPopupTractlblHoldSpa.isVisible = false;
		} else {
			popupTractPwd.hbxPopupTractlblHold.isVisible = false;
		}
		popupTractPwd.lblPopupTract7.isVisible = false;
		popupTractPwd.lblPopupTract5.isVisible = false;
		popupTractPwd.btnPopUpTractCancel.text = kony.i18n.getLocalizedString(
			"keyCancelButton");
		popupTractPwd.btnPopUpTractCancel.skin = btnLightBlue;
		popupTractPwd.btnPopUpTractCancel.focusSkin = btnLightBlue;
		//popupTractPwd.lblPopTractPwdtxt.containerWeight = 28;
		popupTractPwd.lblOTP.containerWeight = 28;
		//popupTractPwd.hbxPoupOTP.isVisible = false;
		popupTractPwd.hbxOTP.isVisible = false;
		popupTractPwd.tbxPopupTractPwdtxtAccPin.text = "";
		popupTractPwd.hbxPoupAccesspin.isVisible = true;
		popupTractPwd.hbxPopupTranscPwd.isVisible = false;
		popupTractPwd.lblPopupPwdAccPin.text = kony.i18n.getLocalizedString("AccessPin");
		popupTractPwd.lblPopupPwdAccPin.skin = lblPopupLabelTxt;
		popupTractPwd.btnPopUpTractCancel.onClick =
			onClickAccessPinCancel;
		gblOTPFlag = true;
		popupTractPwd.tbxPopupTractPwdtxtAccPin.textInputMode =
			constants.TEXTBOX_INPUT_MODE_NUMERIC;
		popupTractPwd.tbxPopupTractPwdtxtAccPin.keyBoardStyle =
			constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD;
		if ((null != gblDeviceInfo
			.name) && (kony.string.equalsIgnoreCase(
			"android", gblDeviceInfo
			.name))) {
			popupTractPwd.tbxPopupTractPwdtxtAccPin.keyBoardStyle =
				constants.TEXTBOX_NUMERIC_PASSWORD;
		}
	} else if (typeFlag == 3) {
		popupTractPwd.line502709349207.isVisible = false;
		popupTractPwd.lblPopupTract1.isVisible = false;
		if (flowSpa) {
			popupTractPwd.hbxPopupTractlblHoldSpa.isVisible = false;
		} else {
			popupTractPwd.hbxPopupTractlblHold.isVisible = false;
		}
		popupTractPwd.lblPopupTract5.isVisible = false;
		popupTractPwd.lblPopupTract7.isVisible = true;
		popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("keyTransPwdMsg");
		popupTractPwd.lblPopupTract7.skin = lblPopupLabelTxt;
		popupTractPwd.btnPopUpTractCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
		popupTractPwd.btnPopUpTractCancel.skin = btnLightBlue;
		popupTractPwd.btnPopUpTractCancel.focusSkin = btnLightBlue;
		//popupTractPwd.lblPopTractPwdtxt.containerWeight = 53;
		popupTractPwd.lblOTP.containerWeight = 53;
		//popupTractPwd.hbxPoupOTP.isVisible = false;
		popupTractPwd.hbxOTP.isVisible = false;
		popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text = "";
		popupTractPwd.hbxPoupAccesspin.isVisible = false;
		popupTractPwd.hbxPopupTranscPwd.isVisible = true;
		//popupTractPwd.lblPopupTranscPwd.text = lblText;
		popupTractPwd.btnPopUpTractCancel.onClick =
			onClickAccessPinCancel;
		gblOTPFlag = true;
		//popupTractPwd.tbxPopupTractPwdtxt.textInputMode = constants.TEXTBOX_INPUT_MODE_ANY;
		//commenting the lines as it was causing issues in change mobile after any transaction pdw pop up
		//popupTractPwd.txtOTP.textInputMode = constants.TEXTBOX_INPUT_MODE_ANY;
		//popupTractPwd.txtOTP.maxTextLength = gblOTPLENGTH;
	}
	dismissLoadingScreen();
	popupTractPwd.show();
}

/**
 * 
 *  lblText
 *  callBackConfirmSSL
 *  typeFlag - Used for module differentation in SLL callback
 *  returns {} 
 */
function showOTPSSLPopup(callBackConfirmSSL) {
	//Please DO NOT KEEP THIS FUNCTION OUTSIDE. This is inner function assigned to popupTractPwd.btnPopupTractConf.onClick
	function onConfClick() {
		showLoadingScreen();
		var enteredText = "";
		enteredText = popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text;
		//if (deviceInfo["name"] == "iPhone" || deviceInfo["name"] == "iPhone Simulator") {
		//#ifdef iphone
			VerifyTransSSLValidation();
		//}else if (deviceInfo["name"] == "android"){
		//#else
			//#ifdef android
				sslVerifyTransAndroid();
			//#endif
		//#endif
		//callBackConfirm(enteredText);
	}
	function callBackFromSSLImpl() {
		dismissLoadingScreen();
		callBackConfirmSSL(popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text);
	}
	function sslVerifyTransAndroid(){
		if(TmbTrusteerFFIObject == null)
			TmbTrusteerFFIObject = new trust.TmbTrusteerFFI();
		 var url = appConfig.serverIp;
			TmbTrusteerFFIObject.sslValidationAndroid(callbackVerifyTransSSL,url);
	}

	function callbackVerifyTransSSL(sslResult) {
	    if(sslResult == 0){
	    	
	    	//callBackFromSSLImpl();
		}else{
			/*
			dismissLoadingScreen();
			showAlertForUniqueGenFail(kony.i18n.getLocalizedString("keySSLValidationFailureMsg"));*/
			//alert(kony.i18n.getLocalizedString("keySSLValidationFailureMsg"));
		}
	    callBackFromSSLImpl();
	}
	function VerifyTransSSLValidation()
	{
		
		var sslObj = {};
		sslObj["SSLValidation"] = verifyTransSSLCallBack;
		sslObj["message"] = "yes";
		sslObj["url"] = appConfig.serverIp;
		
		
		ffinamespace.getsslValidation(sslObj);
	}
	function verifyTransSSLCallBack(sslresult){
		if(sslresult == 0 || sslresult == "0" || sslresult == 99 || sslresult == "99"){
			
			//callBackFromSSLImpl();
		}else{
			/*
			dismissLoadingScreen();
			showAlertForUniqueGenFail(kony.i18n.getLocalizedString("keySSLValidationFailureMsg"));*/
			//alert(kony.i18n.getLocalizedString("keySSLValidationFailureMsg"));
		}
		callBackFromSSLImpl();
	}
	
	popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = txtFocusBG;
	popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = txtFocusBG;
	popupTractPwd.containerWeight = 94;
	popupTractPwd.btnPopupTractConf.onClick = onConfClick;
	popupTractPwd.btnPopupTractConf.text = kony.i18n.getLocalizedString(
		"keyConfirm");
	// popupTractPwd.tbxPopupTractPwdtxt.text = "";
	popupTractPwd.txtOTP.text = "";
	//popupTractPwd.lblPopTractPwdtxt.text = lblText;
	popupTractPwd.lblOTP.text = kony.i18n.getLocalizedString("transPasswordSub");
	popupTractPwd.hbxIncorrectOTP.isVisible = false;
	popupTractPwd.line502709349207.isVisible = false;
	popupTractPwd.lblPopupTract1.isVisible = false;
	popupTractPwd.hbxPopupTractlblHold.isVisible = false;
	popupTractPwd.lblPopupTract5.isVisible = false;
	popupTractPwd.lblPopupTract7.isVisible = true;
	popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("keyTransPwdMsg");
	popupTractPwd.lblPopupTract7.skin = lblPopupLabelTxt;
	popupTractPwd.btnPopUpTractCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
	popupTractPwd.btnPopUpTractCancel.skin = btnLightBlue;
	popupTractPwd.btnPopUpTractCancel.focusSkin = btnLightBlue;
	//popupTractPwd.lblPopTractPwdtxt.containerWeight = 53;
	popupTractPwd.lblOTP.containerWeight = 53;
	//popupTractPwd.hbxPoupOTP.isVisible = false;
	popupTractPwd.hbxOTP.isVisible = false;
	popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text = "";
	popupTractPwd.hbxPoupAccesspin.isVisible = false;
	popupTractPwd.hbxPopupTranscPwd.isVisible = true;
	//popupTractPwd.lblPopupTranscPwd.text = kony.i18n.getLocalizedString("transPasswordSub");
	popupTractPwd.btnPopUpTractCancel.onClick = onClickAccessPinCancel;
	gblOTPFlag = true;
	//popupTractPwd.tbxPopupTractPwdtxt.textInputMode = constants.TEXTBOX_INPUT_MODE_ANY;
	//commenting the lines of code to fix the text box issue on MB
	//popupTractPwd.txtOTP.textInputMode = constants.TEXTBOX_INPUT_MODE_ANY;
	//popupTractPwd.txtOTP.maxTextLength = gblOTPLENGTH;
	dismissLoadingScreen();
	popupTractPwd.show();
}

//function onSaveChngAcessPinClick() {
//	popupTractPwd.dismiss();
//	//callBackConfirm(popupTractPwd.tbxPopupTractPwdtxt.text);
//}

//function onCancelChngAcessPinClick() {
//	popupTractPwd.dismiss();
//}
/**
 * description
 * Cancelling the otpTimer
 * @returns {}
 */

function otpTimerCallBack() {
	popupTractPwd.btnOtpRequest.setEnabled(true);
	popupTractPwd.btnOtpRequest.skin=btnLightBlue;
	popupTractPwd.btnOtpRequest.focusSkin=btnLightBlue;
	popupTractPwd.btnOtpRequest.onClick = onClickActiRequestOtp;
	gblOTPFlag = true;
	try {
		kony.timer.cancel("otpTimer")
	} catch (e) {
		
	}
}
/*
*************************************************************************************
		Module	: onClickOTPRequest
		Author  : Kony
		Purpose : Defining onclick for next button in activation confirmation page
****************************************************************************************
*/

function onClickActiConfirm() {
	showLoadingScreen();
	
	gblOTPFlag = true;
/*	gblOTPFlag = false;
	if(gblLastClicked == null) {
		gblLastClicked = getPresentTime();
		gblOTPFlag = true;
	} else if(getPresentTime() - gblLastClicked > 10000){
		gblLastClicked = getPresentTime();
		gblOTPFlag = true;
	} else {
		gblOTPFlag = false;
		dismissLoadingScreen();
	}
	
	*/
	gblOnClickReq = false;
	//Commenting this for DEF11700
	/*
	try {
		kony.timer.cancel("otpTimer")
	} catch (e) {
		
	}
	*/
	if (flowSpa) {
		spaChnage = "activation";
		successFlag=false;
		//onClickOTPRequestSpa();
	onClickOTPSpaAactivate();
	}
	else
	{
		//showLoadingScreen();
		requestOTPActivate();
	}
}

function requestOTPActivate(){
//Added for ENH_207_6 to send uniqueid in Activation Code verification OTP call - function onClickOTPRequest()

	GBL_FLOW_ID_CA=10;
	//#ifdef iphone
		TrusteerDeviceId();
	//#else
		//#ifdef android
			getUniqueID();
		//#endif
	//#endif 
}
/*
*************************************************************************************
		Module	: onClickActiRequestOtp
		Author  : Kony
		Purpose : Defining onclick for Request button for OTP pop up
****************************************************************************************
*/

function onClickActiRequestOtp() {
	gblOTPFlag = true;
	gblOnClickReq = true;
	onClickOTPRequest();
}
/*
*************************************************************************************
		Module	: onClickOTPRequest
		Author  : Kony
		Purpose : Invoking request OTP kony service
****************************************************************************************
*/

function onClickOTPRequest(uniqueid) {
	
	if (gblOTPFlag) {
		//popupTractPwd.btnPopUpTractCancel.skin = btnDisabledGray;
		//popupTractPwd.btnPopUpTractCancel.setEnabled(false);
		//Disabled for ENH_207 and below code implemented
		popupTractPwd.btnOtpRequest.setEnabled(false);
		popupTractPwd.btnOtpRequest.skin=btnDisabledGray;
		popupTractPwd.btnOtpRequest.focusSkin=btnDisabledGray;
		var inputParam = {};
		//inputParam["tokenUUID"] = gblTokenNum
		//inputParam["sessionId"] = "session";
		/*	inputParam["policyId"] = "";
		
		var locale=kony.i18n.getCurrentLocale();
		// For Activation Flow
		if(gblActionCode == "21"){
			if(locale=="en_US"){
				
				inputParam["eventNotificationPolicyId"] = "MIB_OTPActivate_EN";
				inputParam["SMS_Subject"] = "MIB_OTPActivateMB_EN";
			}
			else{
				inputParam["eventNotificationPolicyId"] = "MIB_OTPActivate_TH";
				inputParam["SMS_Subject"] = "MIB_OTPActivateMB_TH";
			} 
		}// For Add device Flow
		else if(gblActionCode == "23"){
			if(locale=="en_US"){
				
				inputParam["eventNotificationPolicyId"] = "MIB_OTPAddDevice_EN";
				inputParam["SMS_Subject"] = "MIB_OTPAddDevice_EN";
			}
			else{
				inputParam["eventNotificationPolicyId"] = "MIB_OTPAddDevice_TH";
				inputParam["SMS_Subject"] = "MIB_OTPAddDevice_TH";
			} 
		}// For Reset Password Flow
		else if(gblActionCode == "22"){
			if(locale=="en_US"){
				
				inputParam["eventNotificationPolicyId"] = "MIB_OTPResetPWD_EN";
				inputParam["SMS_Subject"] = "MIB_OTPResetPWDMB_EN";
			}
			else{
				inputParam["eventNotificationPolicyId"] = "MIB_OTPResetPWD_TH";
				inputParam["SMS_Subject"] = "MIBOTPResetPWDMB_TH";
			} 
		}
		
		
		
		inputParam["retryCounterRequestOTP"] = gblRetryCountRequestOTP;  
	    inputParam["storeId"] = "";
	    inputParam["Batch_No"] = "";
	    inputParam["Bank_Ref"] = "";
	    inputParam["Product_Code"] = "";
	    inputParam["Recipient_Name"] = "";
	    inputParam["MobileNumber"] = gblPHONENUMBER;
	    inputParam["Channel"] = "";
	    inputParam["inputSeed"]="";
		invokeServiceSecureAsync("requestOTP",inputParam,CallbackonClickOTPRequest);*/
		var locale = kony.i18n.getCurrentLocale();
		var spaChannel
		// For Activation Flow
		
		if (flowSpa) //if else condition for SPA
		{
			if (gblActionCode == "11") {
				if (locale == "en_US") {
					inputParam["eventNotificationId"] =
						"MIB_OTPActivate_EN";
					inputParam["smsSubject"] =
						"MIB_OTPActivateIB_EN";
					spaChannel = "IB-ACTIVATION";
				} else {
					inputParam["eventNotificationId"] =
						"MIB_OTPActivate_TH";
					inputParam["smsSubject"] =
						"MIB_OTPActivateIB_TH";
					spaChannel = "IB-ACTIVATION";
				}
			} else if (gblActionCode == "12") {
				if (locale == "en_US") {
					inputParam["eventNotificationId"] =
						"MIB_OTPResetPWD_EN";
					inputParam["smsSubject"] =
						"MIB_OTPResetPWDIB_EN";
					spaChannel = "IB-ACTIVATION";
				} else {
					inputParam["eventNotificationId"] =
						"MIB_OTPResetPWD_TH";
					inputParam["smsSubject"] =
						"MIB_OTPResetPWDIB_TH";
					spaChannel = "IB-ACTIVATION";
				}
			} else if (spaChnage == "chguseridib") {
				inputParam["eventNotificationId"] =
					"MIB_ChangeUSERID_" + kony.i18n.getCurrentLocale();
				inputParam["smsSubject"] = "MIB_ChangeUSERID_" +
					kony.i18n.getCurrentLocale();
				spaChannel = "IB-CHANGEUSERID";
			} else if (spaChnage == "chgpwdidib") {
				inputParam["eventNotificationId"] =
					"MIB_ChangeIBPWD_" + kony.i18n.getCurrentLocale();
				inputParam["smsSubject"] = "MIB_ChangeIBPWD_" +
					kony.i18n.getCurrentLocale();
				spaChannel = "IB-CHANGEPWD";
			}
		} else {
			inputParam["deviceId"]=uniqueid;	// Added for device name in MB activation
			if (true) {//no action codes enh217
				if (locale == "en_US") {
					inputParam["eventNotificationId"] =	"MIB_OTPActivate_EN";
					inputParam["smsSubject"] = "MIB_OTPActivateMB_EN";
				} else {
					inputParam["eventNotificationId"] =	"MIB_OTPActivate_TH";
					inputParam["smsSubject"] = "MIB_OTPActivateMB_TH";
				}
			} // For Add device Flow
			else if (gblActionCode == "23") {
				if (locale == "en_US") {
					inputParam["eventNotificationId"] =	"MIB_OTPAddDevice_EN";
					inputParam["smsSubject"] = "MIB_OTPAddDevice_EN";
				} else {
					inputParam["eventNotificationId"] = "MIB_OTPAddDevice_TH";
					inputParam["smsSubject"] = "MIB_OTPAddDevice_TH";
				}
			} // For Reset Password Flow
			else if (gblActionCode == "22") {
				if (locale == "en_US") {
					inputParam["eventNotificationId"] =	"MIB_OTPResetPWDMB_EN";
					inputParam["smsSubject"] = "MIB_OTPResetPWDMB_EN";
				} else {
					inputParam["eventNotificationId"] =	"MIB_OTPResetPWDMB_TH";
					inputParam["smsSubject"] = "MIBOTPResetPWDMB_TH";
				}
			}
		}
		inputParam["bankRef"] = "";
		inputParam["batchNo"] = "";
		if (flowSpa) //for SPA
		{
			
			inputParam["Channel"] = spaChannel;
		} else {
			inputParam["channel"] = "";
			if(gblMobileNewChange=="Y")
			{
				inputParam["Channel"] = "ChangeMobileNumberNew";
			}
		}
		// inputParam["eventNotificationId"] = "";
		inputParam["inputSeedString"] = "";
		//inputParam["mobileNo"] = gblPHONENUMBER; //"0865832721" Mobile num VIT offshore; mib xpress inq already has it
		inputParam["param"] = "";
		inputParam["entries"] = "";
		inputParam["key"] = "";
		inputParam["value"] = "";
		inputParam["policyId"] = "";
		inputParam["productCode"] = "";
		inputParam["receipientName"] = "";
		inputParam["session"] = "";
		//inputParam["smsSubject"] = ""; 
		inputParam["storeId"] = "";
		inputParam["tokenUUID"] = "";
		inputParam["retryCounterRequestOTP"] = gblRetryCountRequestOTP;
		invokeServiceSecureAsync("RequestOTPKony", inputParam,
			CallbackonClickOTPRequest);
	}
}

function CallbackonClickOTPRequest(status, resulttable) {
	if (status == 400) {
		
		
		if (resulttable["opstatus"] == 0) {
		 //DEF11700 Change start
		   if (resulttable["errCode"] == "GenOTPRtyErr00002"){
		    return false;
		  }   
		  	try {
		       kony.timer.cancel("otpTimer")
	         } catch (e) {
		       
	        }
	     //DEF11700 Change End   
			gblRetryCountRequestOTP = resulttable[
				"retryCounterRequestOTP"];
			var reqOtpTimer = kony.os.toNumber(resulttable[
				"requestOTPEnableTime"]);
			gblOTPLENGTH = kony.os.toNumber(resulttable["otpLength"]);
			
			kony.timer.schedule("otpTimer", otpTimerCallBack,
				reqOtpTimer, false);
				
				
			if (gblOnClickReq == false) {
				showOTPPopup(kony.i18n.getLocalizedString(
						"keyOTP"), "ABCD", "xxx-xxx-" +
					gblPHONENUMBER.substring(6, 10),
					otpValidation, 1)
			}
			if (flowSpa) {
				if (spaChnage) {
					showOTPPopup(kony.i18n.getLocalizedString(
							"keyOTP"), "ABCD",
						"xxx-xxx-" +
						gblPHONENUMBER.substring(6, 10),
						otpValidationspa, 1)
				}
			}
			popupTractPwd.lblPopupTract1.text = kony.i18n.getLocalizedString(
				"keybankrefno") + resulttable[
				"pac"];
				
			if (flowSpa) {
				popupTractPwd.lblPopupTract2Spa.text = kony.i18n
					.getLocalizedString("keyotpmsg");
				popupTractPwd.lblPopupTract4Spa.text =
					"xxx-xxx-" + gblPHONENUMBER.substring(6,
						10);
				popupTractPwd.lblPopupTract7.text = "";
			} else {
				popupTractPwd.lblPopupTract2.text = kony.i18n.getLocalizedString(
					"keyotpmsg");
				popupTractPwd.lblPopupTract7.text = "";
				popupTractPwd.txtOTP.text="";
				popupTractPwd.lblPopupTract4.text = "xxx-xxx-" +
					gblPHONENUMBER.substring(6, 10);
				otpConfirmEnable();
			}
			gblOTPFlag = false;
		} else {
			if (resulttable["errCode"] == "GenOTPRtyErr00002") {
				kony.application.dismissLoadingScreen();
				showAlert(kony.i18n.getLocalizedString(
						"ECGenOTPRtyErr00002"), kony.i18n
					.getLocalizedString(
						"info"));
				return false;
			} else if (resulttable["errCode"] == "GenOTPRtyErr00001") {
				kony.application.dismissLoadingScreen();
				if(resulttable["mobileAllZero"] != undefined && resulttable["mobileAllZero"] == "true" && resulttable["mobileAllZero"] != null){
					showAlert(kony.i18n.getLocalizedString("keyMobileAllZero"),kony.i18n.getLocalizedString("info"));
				}else{
                	showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                 }
				gblOTPFlag = false;
				//return false;
			} else {
				kony.application.dismissLoadingScreen();
				showAlert(kony.i18n.getLocalizedString(
					"ECGenericError"), kony.i18n.getLocalizedString(
					"info"));
				gblOTPFlag = false;
				//return false;
			}
			//popupTractPwd.lblPopupTract1.text = kony.i18n.getLocalizedString("keybankrefno") + resulttable["pac"];
			gblRetryCountRequestOTP = resulttable[
				"retryCounterRequestOTP"];
			var reqOtpTimer = kony.os.toNumber(resulttable[
				"requestOTPEnableTime"]);
			gblOTPLENGTH = kony.os.toNumber(resulttable["otpLength"]);
			kony.timer.schedule("otpTimer", otpTimerCallBack,
				reqOtpTimer, false);
			
		}
		kony.application.dismissLoadingScreen();
	}
}
/*
 * This function enables / disables the confirm button on UI
 * as per the OTP being entered in OTP text field
 */

function otpConfirmEnable() {
	var otpText = popupTractPwd.txtOTP.text;
	if (otpText.trim()
		.length > 0) {
		popupTractPwd.btnPopupTractConf.skin = btnBlueSkin;
		popupTractPwd.btnPopupTractConf.setEnabled(true);
	} else {
		popupTractPwd.btnPopupTractConf.skin = btnDisabledGray;
		popupTractPwd.btnPopupTractConf.focusSkin = btnDisabledGray;
		popupTractPwd.btnPopupTractConf.setEnabled(false);
	}
}
/**
 * description
 * @returns {}
 */

function onClickAccessPinCancel() {
	popupTractPwd.btnPopupTractConf.skin = btnBlueSkin;
	popupTractPwd.btnPopupTractConf.focusSkin = btnBlueSkin;
	popupTractPwd.btnPopupTractConf.setEnabled(true);
	if(gblMBActivationVia == "2"){
		try{
			kony.timer.cancel("activateViaIBTimer");
		}catch (e) {
		}	
	}
	if(kony.application.getCurrentForm().id == "frmCardActivationDetails"){
		//frmMBCardList.show();
		if(frmCardActivationDetails.flexCardDetails.isVisible){
			//frmCardActivationDetails.txtCCNumbetInputThree.text ="";
		}
		
		if(gblCardType == "C"){
			isPopupCancel = true;
		    showCVVDetails();
		}else if(gblCardType == "R"){
			frmCardActivationDetails.txtCCNumbetInputThree.text ="";
			frmCardActivationDetailsInit();
			showCitizenId();
		}else{
			frmCardActivationDetails.txtCCNumbetInputThree.text="";
			gblCardTrans = "T";
			activationReadUTFFile();
            showCardDetailsFlex();
            frmMBNewTncCreditCardClickBtnNext();
		}
	} else if(kony.application.getCurrentForm().id == "frmMBChangePINEnterExistsPin"){
		clearExistChangePIN();
	}
	popupTractPwd.dismiss();	
}
/**
 * description
 * @returns {}
 */

//function transPopupCallBack(text) {
//	alert(text);
//}

//function toShowMBsetPasswd() {
//	frmMBsetPasswd.show()
//}
/**
 * description
 * @returns {}
 */

//function showTerminationPopup() {
//	popUpTermination.containerWeight = 94;
//	popUpTermination.show();
//}
/**
 * description
 * @returns {}
 */

function onClickNextPwdRules() {
	var prevForm = kony.application.getPreviousForm();
	if (prevForm["id"] == "frmMyProfile") {
		if (gblChangePWDFlag == 0) {
			frmCMChgAccessPin.tbxCurAccPin.text = "";
			frmCMChgAccessPin.tbxCurAccPinUnMask.text = "";
			frmCMChgAccessPin.txtAccessPwd.text = "";
			frmCMChgAccessPin.txtAccessPwdUnMask.text = "";
			frmCMChgAccessPin.show();
		} else if (gblChangePWDFlag == 1) {
			frmCMChgTransPwd.tbxTranscCrntPwd.text = "";
			frmCMChgTransPwd.tbxTranscCrntPwdTemp.text = "";
			frmCMChgTransPwd.txtTemp.text = "";
			frmCMChgTransPwd.txtTransPass.text = "";
			frmCMChgTransPwd.show();
		}
	} else {
		
		showAddDevOrActivation();
		//invokeDeviceInquiry();
		//frmMBsetPasswd.show();
		//gblAddOrAuth = 0;
		//frmMBsetPasswd.lblDeviceName.isVisible = false;
		//frmMBsetPasswd.txtDeviceName.isVisible = false;
	}
}

function showOTPPopupForOTPValidation(lblText, refNo, mobNO, callBackConfirm) {
	//Please DO NOT KEEP THIS FUNCTION OUTSIDE. This is inner function assigned to popupTractPwd.btnPopupTractConf.onClick
	/*function onConfClick() {
		//popupTractPwd.dismiss();
		var enteredText = "";
		if (typeFlag == 1) {
			//enteredText = popupTractPwd.tbxPopupTractPwdtxt.text;
			enteredText = popupTractPwd.txtOTP.text;
			if (popupTractPwd.hbxIncorrectOTP.isVisible == true) {
				enteredText = popupTractPwd.txtIncorrectOTP.text;
			}
		} 
		callBackConfirm(enteredText);
	}*/
	function onConfClickOTP() {
		showLoadingScreen();
		var enteredText = "";
		enteredText = popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text;
		//if (deviceInfo["name"] == "iPhone" || deviceInfo["name"] == "iPhone Simulator") {
		//#ifdef iphone
			VerifyTransSSLValidationOTP();
		//}else if (deviceInfo["name"] == "android"){
		//#else
			//#ifdef android
				sslVerifyTransAndroidOTP();
			//#endif
		//#endif
		//callBackConfirm(enteredText);
	}
	function callBackFromSSLImplOTP() {
		dismissLoadingScreen();
		callBackConfirm(popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text);
	}
	function sslVerifyTransAndroidOTP(){
		if(TmbTrusteerFFIObject == null)
			TmbTrusteerFFIObject = new trust.TmbTrusteerFFI();
		  var url = appConfig.serverIp;
			TmbTrusteerFFIObject.sslValidationAndroid(callbackVerifyTransSSLOTP,url);
	}

	function callbackVerifyTransSSLOTP(sslResult) {
	    if(sslResult == 0){
	    	
	    	//callBackFromSSLImpl();
		}else{
			/*
			dismissLoadingScreen();
			showAlertForUniqueGenFail(kony.i18n.getLocalizedString("keySSLValidationFailureMsg"));*/
			//alert(kony.i18n.getLocalizedString("keySSLValidationFailureMsg"));
		}
	    callBackFromSSLImplOTP();
	}
	function VerifyTransSSLValidationOTP()
	{
		
		var sslObj = {};
		sslObj["SSLValidation"] = verifyTransSSLCallBackOTP;
		sslObj["message"] = "yes";
		sslObj["url"] = appConfig.serverIp;
		
		
		ffinamespace.getsslValidation(sslObj);
	}
	function verifyTransSSLCallBackOTP(sslresult){
		if(sslresult == 0 || sslresult == "0" || sslresult == 99 || sslresult == "99" ){
			
			//callBackFromSSLImpl();
		}else{
			/*
			dismissLoadingScreen();
			showAlertForUniqueGenFail(kony.i18n.getLocalizedString("keySSLValidationFailureMsg"));*/
			//alert(kony.i18n.getLocalizedString("keySSLValidationFailureMsg"));
		}
		callBackFromSSLImplOTP();
	}
	popupTractPwd.containerWeight = 94;
	popupTractPwd.btnPopupTractConf.skin = btnBlueSkin;
	popupTractPwd.btnPopupTractConf.setEnabled(true);
	popupTractPwd.btnPopupTractConf.onClick = onConfClickOTP;
	popupTractPwd.btnPopupTractConf.text = kony.i18n.getLocalizedString(
		"keyConfirm");
	// popupTractPwd.tbxPopupTractPwdtxt.text = "";
	popupTractPwd.txtOTP.text = "";
	//popupTractPwd.lblPopTractPwdtxt.text = lblText;
	popupTractPwd.lblOTP.text = lblText;
	popupTractPwd.hbxIncorrectOTP.isVisible = false;
	//if (typeFlag == 1) {
		popupTractPwd.lblPopupTract1.isVisible = true;
		if (flowSpa) {
			popupTractPwd.hbxPopupTractlblHoldSpa.isVisible = true;
		} else {
			popupTractPwd.hbxPopupTractlblHold.isVisible = true;
		}
		popupTractPwd.lblPopupTract5.text = kony.i18n.getLocalizedString("keyotpmsgreq");
		popupTractPwd.lblPopupTract5.isVisible = true;
		if (flowSpa) {
			popupTractPwd.lblPopupTract4Spa.text = mobNO;
		} else {
			popupTractPwd.lblPopupTract4.text = mobNO;
		}
		//popupTractPwd.btnPopUpTractCancel.skin = btnDisabledGray;
//		popupTractPwd.btnPopUpTractCancel.focusSkin = btnDisabledGray;
//		popupTractPwd.btnPopUpTractCancel.setEnabled(false);
		//popupTractPwd.btnPopUpTractCancel.onClick = onClickOTPRequest;
		//popupTractPwd.btnPopUpTractCancel.text = kony.i18n.getLocalizedString(
//			"keyRequest");
		popupTractPwd.btnPopUpTractCancel.skin = btnLightBlue;
		popupTractPwd.btnPopUpTractCancel.focusSkin = btnLightBlue;
        popupTractPwd.btnPopUpTractCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
        popupTractPwd.btnPopUpTractCancel.onClick = onClickOTPCancel;
		if (kony.i18n.getCurrentLocale() == "th_TH") {
			//popupTractPwd.lblPopTractPwdtxt.containerWeight = 25;
			popupTractPwd.lblOTP.containerWeight = 12;
		} else {
			//popupTractPwd.lblPopTractPwdtxt.containerWeight = 12; btnOtpRequest
			popupTractPwd.lblOTP.containerWeight = 12;
		}
		//popupTractPwd.lblPopupTract1.text = kony.i18n.getLocalizedString("keybankrefno")+gblRefNum; Commented as added in onClickOTPRequest
		//popupTractPwd.hbxPoupOTP.isVisible = true;
		popupTractPwd.hbxOTP.isVisible = true;
		popupTractPwd.btnOtpRequest.skin=btnDisabledGray;
		popupTractPwd.btnOtpRequest.focusSkin=btnDisabledGray;
		popupTractPwd.btnOtpRequest.setEnabled(false);
		popupTractPwd.hbxPoupAccesspin.isVisible = false;
		popupTractPwd.hbxPopupTranscPwd.isVisible = false;
		//popupTractPwd.lblPopTractPwdtxt.text = lblText;
		popupTractPwd.lblOTP.text = lblText;
		//gblOTPFlag = true;
		//onClickOTPRequest();
		//popupTractPwd.tbxPopupTractPwdtxt.text = "";
		popupTractPwd.txtOTP.textInputMode = constants.TEXTBOX_INPUT_MODE_NUMERIC;
		
		popupTractPwd.txtOTP.keyBoardStyle = constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD;
		
		if (flowSpa) {
			popupTractPwd.txtOTP.textInputMode = constants.TEXTBOX_INPUT_MODE_ANY;
		}
	//} 
	dismissLoadingScreen();
	popupTractPwd.show();
}
/**
 * description
 * @returns {}
 */

function onClickOTPCancel(){
	popupTractPwd.btnPopupTractConf.skin = btnBlueSkin;
	popupTractPwd.btnPopupTractConf.focusSkin = btnBlueSkin;
	popupTractPwd.btnPopupTractConf.setEnabled(true);
	popupTractPwd.dismiss();
	/*
	if(gblOpenAccountFlow){
      	frmCheckContactInfo.show();	
	}else{
		gblOpenAccountFlow = false;
		onClickMyProfileFlow();
	}
	**/
}


function activationViaIBUserOTP(){
	showLoadingScreen();
	var inputParams={};
	inputParams["Channel"] = "ActivateMB";
    inputParams["locale"] = kony.i18n.getCurrentLocale();
	invokeServiceSecureAsync("generateOTPWithUser", inputParams, callBackactivationViaIBUserOTP);
}

function callBackactivationViaIBUserOTP(status, resulttable){
	if(status == 400){
		dismissLoadingScreen();
		if (resulttable["errCode"] == "GenOTPRtyErr00002") {
				showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00002"), kony.i18n.getLocalizedString("info"));
				disableActivateMBViaIBPopup();
				return false;
		}else if (resulttable["errCode"] == "JavaErr00001") {
				showAlert(kony.i18n.getLocalizedString("ECJavaErr00001"), kony.i18n.getLocalizedString("info"));
				return false;
		}else if (resulttable["errCode"] == "GenOTPRtyErr00001") {
				if(resulttable["mobileAllZero"] != undefined && resulttable["mobileAllZero"] == "true"){
					showAlert(kony.i18n.getLocalizedString("keyMobileAllZero"),kony.i18n.getLocalizedString("info"));
				}else{
                	showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                }	
                return false;
        }else if(resulttable["code"]== "10403"){
      			showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr"), kony.i18n.getLocalizedString("info"));
      			return false;
      	}else if (resulttable["opstatus"] == 0) {
				//Put success handler here
				
				var refVal="";
				for(var d=0;resulttable["Collection1"].length;d++){
					if(resulttable["Collection1"][d]["keyName"] == "pac"){
						refVal=resulttable["Collection1"][d]["ValueString"];
						break;
					}
				}
				gblOTPLENGTH = kony.os.toNumber(resulttable["otpLength"]);
				
				popupTractPwd.lblPopupTract1.text = kony.i18n.getLocalizedString("keybankrefno") + refVal;
				popupTractPwd.lblPopupTract2.text = kony.i18n.getLocalizedString("keyotpmsg");
				popupTractPwd.lblPopupTract7.text = "";
				popupTractPwd.txtOTP.text="";
				popupTractPwd.lblPopupTract4.text = glbMobileNo;
				var reqOtpTimer = kony.os.toNumber(resulttable["requestOTPEnableTime"]);
				try {kony.timer.cancel("activateViaIBTimer")} catch (e) {}
				kony.timer.schedule("activateViaIBTimer", activateViaIBTimerCallBack,reqOtpTimer, false);
				showOTPPopup(kony.i18n.getLocalizedString("keyOTP"), "ABCD",glbMobileNo,otpValidation, 1)
				otpConfirmEnable();
		}
	}	
}

function activateViaIBTimerCallBack() {
	popupTractPwd.btnOtpRequest.setEnabled(true);
	popupTractPwd.btnOtpRequest.skin = btnLightBlue;
	popupTractPwd.btnOtpRequest.focusSkin=btnLightBlue;
	gblOTPFlag = true;
	try {
		kony.timer.cancel("activateViaIBTimer");
	} catch (e) {
		
	}
}

function disableActivateMBViaIBPopup(){
	popupTractPwd.btnOtpRequest.setEnabled(false);
	popupTractPwd.btnOtpRequest.skin=btnDisabledGray;
	popupTractPwd.btnOtpRequest.focusSkin=btnDisabledGray;
	kony.timer.cancel("activateViaIBTimer");
}

function verifyOTPActivationIBUserOTP(){
	var otpText= popupTractPwd.txtOTP.text;
	showLoadingPopUpScreen();
	otpCodePattStr = "^[0-9]{" + gblOTPLENGTH + "}$";
	var otpCodePatt = new RegExp(otpCodePattStr, "g");
	resultOtpCodePatt = otpCodePatt.test(otpText);
	if (resultOtpCodePatt) {
		var inputParam = {};
		inputParam["verifyPasswordEx_password"] = otpText;
		invokeServiceSecureAsync("verifyOTPActivateMBUsingIB", inputParam, callBackOTPVerify)
	}else {
		popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("invalidOTP");
		popupTractPwd.lblPopupTract2.text = "";
		popupTractPwd.lblPopupTract4.text = "";
		kony.application.dismissLoadingScreen();
		return false;
	}
}


// Added by Vijay for OTP and transaction pwd flipflop - 27-01
function showOTPSSLPopupTransPwd(callBackConfirmSSL) {
	//Please DO NOT KEEP THIS FUNCTION OUTSIDE. This is inner function assigned to popupTractPwd.btnPopupTractConf.onClick
	function onConfClick() {
		showLoadingScreen();
		var enteredText = "";
		enteredText = popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text;
		//if (deviceInfo["name"] == "iPhone" || deviceInfo["name"] == "iPhone Simulator") {
		//#ifdef iphone
			VerifyTransSSLValidation();
		//}else if (deviceInfo["name"] == "android"){
		//#else
			//#ifdef android
				sslVerifyTransAndroid();
			//#endif
		//#endif
		//callBackConfirm(enteredText);
	}
	function callBackFromSSLImpl() {
		dismissLoadingScreen();
		callBackConfirmSSL(popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text);
	}
	function sslVerifyTransAndroid(){
		if(TmbTrusteerFFIObject == null)
			TmbTrusteerFFIObject = new trust.TmbTrusteerFFI();
		 var url = appConfig.serverIp;
			TmbTrusteerFFIObject.sslValidationAndroid(callbackVerifyTransSSL,url);
	}

	function callbackVerifyTransSSL(sslResult) {
	    if(sslResult == 0){
	    	
	    	//callBackFromSSLImpl();
		}else{
			/*
			dismissLoadingScreen();
			showAlertForUniqueGenFail(kony.i18n.getLocalizedString("keySSLValidationFailureMsg"));*/
			//alert(kony.i18n.getLocalizedString("keySSLValidationFailureMsg"));
		}
	    callBackFromSSLImpl();
	}
	function VerifyTransSSLValidation()
	{
		
		var sslObj = {};
		sslObj["SSLValidation"] = verifyTransSSLCallBack;
		sslObj["message"] = "yes";
		sslObj["url"] = appConfig.serverIp;
		
		
		ffinamespace.getsslValidation(sslObj);
	}
	function verifyTransSSLCallBack(sslresult){
		if(sslresult == 0 || sslresult == "0" || sslresult == 99 || sslresult == "99"){
			
			//callBackFromSSLImpl();
		}else{
			/*
			dismissLoadingScreen();
			showAlertForUniqueGenFail(kony.i18n.getLocalizedString("keySSLValidationFailureMsg"));*/
			//alert(kony.i18n.getLocalizedString("keySSLValidationFailureMsg"));
		}
		callBackFromSSLImpl();
	}
	popupTractPwd.containerWeight = 94;
	popupTractPwd.btnPopupTractConf.onClick = onConfClick;
	popupTractPwd.btnPopupTractConf.text = kony.i18n.getLocalizedString(
		"keyConfirm");
	// popupTractPwd.tbxPopupTractPwdtxt.text = "";
	popupTractPwd.txtOTP.text = "";
	//popupTractPwd.lblPopTractPwdtxt.text = lblText;
	popupTractPwd.lblOTP.text = kony.i18n.getLocalizedString("transPasswordSub");
	popupTractPwd.hbxIncorrectOTP.isVisible = false;
	popupTractPwd.line502709349207.isVisible = false;
	popupTractPwd.lblPopupTract1.isVisible = false;
	popupTractPwd.hbxPopupTractlblHold.isVisible = false;
	popupTractPwd.lblPopupTract5.isVisible = false;
	popupTractPwd.lblPopupTract7.isVisible = true;
	popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("keyTransPwdMsg");
	popupTractPwd.lblPopupTract7.skin = lblPopupLabelTxt;
	popupTractPwd.btnPopUpTractCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
	popupTractPwd.btnPopUpTractCancel.skin = btnLightBlue;
	popupTractPwd.btnPopUpTractCancel.focusSkin = btnLightBlue;
	//popupTractPwd.lblPopTractPwdtxt.containerWeight = 53;
	popupTractPwd.lblOTP.containerWeight = 53;
	//popupTractPwd.hbxPoupOTP.isVisible = false;
	popupTractPwd.hbxOTP.isVisible = false;
	popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text = "";
	popupTractPwd.hbxPoupAccesspin.isVisible = false;
	popupTractPwd.hbxPopupTranscPwd.isVisible = true;
	//popupTractPwd.lblPopupTranscPwd.text = kony.i18n.getLocalizedString("transPasswordSub");
	popupTractPwd.btnPopUpTractCancel.onClick = onClickAccessPinCancel;
	gblOTPFlag = true;
	//popupTractPwd.tbxPopupTractPwdtxt.textInputMode = constants.TEXTBOX_INPUT_MODE_ANY;
	//commenting the lines of code to fix the text box issue on MB
	//popupTractPwd.txtOTP.textInputMode = constants.TEXTBOX_INPUT_MODE_ANY;
	//popupTractPwd.txtOTP.maxTextLength = gblOTPLENGTH;
	dismissLoadingScreen();
	popupTractPwd.show();
}

