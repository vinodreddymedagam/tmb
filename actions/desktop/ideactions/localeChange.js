





languageFlip=false;

function changeLocale(eventObj) {
	
	try{
		if(frmMBPreLoginAccessesPin.flexKeyboard.isVisible) languageFlip=true;
		
		LocaleController.switchLocale();
		var locale = kony.i18n.getCurrentLocale();
		var curformID = kony.application.getCurrentForm();
		var previousFormId = kony.application.getPreviousForm();
		if (kony.string.startsWith(locale, "en", true) == true) {
			if (eventObj["id"] == "btnThai") {
				//this snippet is added to change the button skin to highlighted skin
				if(curformID.id=="frmMenu")
				{
					curformID.btnEng.skin = btnThaiMenuLight;
				}
				else
				{
					curformID.btnEng.skin = btnThaiMenu;
				}
				curformID.btnEng.zIndex=1;
				curformID.btnThai.skin = btnEngMenu;
				curformID.btnThai.zIndex=2;
				//end of snippet
	 			//if(isSignedUser==false){
//					frmSPALogin.btnLoginText.containerWeight =18;
//				}
				kony.i18n.setCurrentLocaleAsync("th_TH", onSuccessLocaleChange, onFailureLocaleChange, "");
				if (previousFormId.id == "frmAccountSummaryLanding") {
					if(flowSpa)
					{
						//frmSPABlank.show();
						//destroyMenuSpa();
						//frmAccountSummaryLanding = null;
						//frmAccountSummaryLandingGlobals();						
						//TMBUtil.DestroyForm(frmAccountSummaryLanding);
						//gblAccountTable = "";
						//callPartyInquiryService();
						//callCustomerAccountService();
					}
					else
					{
						//frmAccountSummaryLanding = null;
//						frmAccountSummaryLandingGlobals();						
						//TMBUtil.DestroyForm(frmAccountSummaryLanding);
						gblFromSwitchLanguage = true;
						removeGestureForAccntSummary(previousFormId);
						gblAccountTable = "";
						callCustomerAccountService();
					}
				}
				if (previousFormId.id == "frmAccountStatementMB") {
						var input_params={};
						invokeServiceSecureAsync("GetServerDateTime", input_params, clBackChangeMonth)
				}
			}
		} else {
			if (eventObj["id"] == "btnEng") {
				//this snippet is added to change the button skin to highlighted skin
				curformID.btnEng.skin = btnEngMenu;
				curformID.btnEng.zIndex = 2;
				if(curformID.id=="frmMenu")
				{
					curformID.btnThai.skin = btnThaiMenuLight;
				}
				else
				{
					curformID.btnThai.skin = btnThaiMenu;
				}
				
				curformID.btnThai.zIndex = 1;
				//if(isSignedUser==false){
//					frmSPALogin.btnLoginText.containerWeight =11;
//				}
				//end of snippet	 
				
				/* if(flowSpa)
				{
					list = {
									appLocale: "en_US"
								}
					kony.store.removeItem("curAppLocale");
					kony.store.setItem("curAppLocale", list);
				} */
				kony.i18n.setCurrentLocaleAsync("en_US", onSuccessLocaleChange, onFailureLocaleChange, "");
				if (previousFormId.id == "frmAccountSummaryLanding") {
					if(flowSpa)
					{
						//frmSPABlank.show();
						//destroyMenuSpa();
						//frmAccountSummaryLanding = null;
						//frmAccountSummaryLandingGlobals();						
						//TMBUtil.DestroyForm(frmAccountSummaryLanding);
						//gblAccountTable = "";
						//callPartyInquiryService();
						//callCustomerAccountService();
					}
					else
					{
						//frmAccountSummaryLanding = null;
//						frmAccountSummaryLandingGlobals();						
						//TMBUtil.DestroyForm(frmAccountSummaryLanding);
						gblFromSwitchLanguage = true;
						removeGestureForAccntSummary(previousFormId);
						gblAccountTable = "";
						callCustomerAccountService();
					}
				}
				
				if (previousFormId.id == "frmAccountStatementMB") {
						var input_params={};
						invokeServiceSecureAsync("GetServerDateTime", input_params, clBackChangeMonth)
				}
			}
		}
	}catch(e){
		
	}
}

function onSuccessLocaleChange() {
    var currentFormId = kony.application.getCurrentForm().id;
    var previousFormId = kony.application.getPreviousForm().id;
	if (isSignedUser == true){
			//updateLocaleCrmProfile();
	}
	if((previousFormId == "frmMyTopUpList") && (gblMyBillerTopUpBB !=2)){
		//added below line for SPA defect
		frmMyTopUpList.lblErrorMsg.setVisibility(false);
		getMyTopUpListMB();
		getMyTopUpSuggestListMB();
	}
	if((previousFormId == "frmBBApplyNow") && (gblMyBillerTopUpBB ==2)){
			frmBBApplyNow.show();
	}
	if((previousFormId == "frmBBMyBeepAndBill") && (gblMyBillerTopUpBB ==2)){
			selectBillerToViewBB();
	}
	if(previousFormId == "frmBBExecuteConfirmAndComplete"){
			frmBBExecuteConfirmAndComplete.show();
	}
	
	if((previousFormId == "frmMyTopUpList") && (gblMyBillerTopUpBB ==2)){
			onBBListLocaleChange();
	}
	if((previousFormId == "frmAddTopUpToMB") && (gblMyBillerTopUpBB ==2)){
			onBBApplyLocaleChange();
	}
	
	if((previousFormId == "frmBBConfirmAndComplete") && (gblMyBillerTopUpBB ==2)){
		onBBConfirmCompleteLocaleChange();
	}
	
	if(previousFormId == "frmATMBranch"){
		synfrmATMBranch();
	}
	if(previousFormId == "frmATMBranchList"){
		onClickSearchBtn();
	}
	if(previousFormId == "frmATMBranchesDetails"){
		synfrmATMBranchesDetails();
	}
	if((previousFormId == "frmMyTopUpSelect") && (gblMyBillerTopUpBB !=2))
		setDataOnsegSelectTopUpFromService();
		
	if((previousFormId == "frmMBMyActivities")) {
		//For drop down list locale change
	 	setFilterTxnTypes();
	 	//For Month combo box locale change
		setMonthLocaleChange();
	}
	if(previousFormId=="frmMyRecipients"){
		startReceipentListingServiceMB();
	}	
	if(previousFormId=="frmApplyMBConfirmationSPA"){
		frmApplyMBConfirmationSPAPreShow();
	}
	if (previousFormId == "frmMyRecipientDetail") {
        frmMyRecipientsAccountListing() 
    }
    
    if (previousFormId == "frmeditMyProfile") {
        syncLocaleforMBProfile(); 
    }else if (previousFormId == "frmMBSavingsCareAddNickName") {
       	frmMBSavingsCareAddNickNameMenuPostshow();
    }
    if(previousFormId == "frmCardActivationComplete"){
    	frmCardActivationCompletePostShow();
    }	
    
	var formLocaleList = {
		"frmMBanking": frmMBankingPreShow,
		"frmMBankingSpa": frmMBankingPreShow,
		"frmMBTnC": frmMBTnCPreShow,
		"frmAccountDetailsMB": frmAccountDetailsMBPreShow,
		"frmAccountSummaryLanding": frmAccountSummaryLandingPreShow,
		"frmMBActiComplete": frmMBActiCompletePreShow,
		"frmMyProfile": frmMyProfilePreShow,
		"frmAccTrcPwdInter": frmAccTrcPwdInterPreShow,
		"frmMBPreLoginAccessesPin": frmMBPreLoginAccessesPinPreShow,
		"frmMBForgotPin": frmMBForgotPinPreShow,
		"frmApplyInternetBankingConfirmation": frmApplyInternetBankingConfirmationPreShow,
		"frmApplyInternetBankingMB": frmApplyInternetBankingMBPreShow,
		"frmChangeMobNoTransLimitMB": PreShowChngeMobNuTransLimit,
		"frmCMChgAccessPin": frmCMChgAccessPinPreShow,
		"frmCMChgTransPwd": frmCMChgTransPwdPreShow,
		"frmMBAccLocked": frmMBAccLockedPreShow,
		"frmMBActiConfirm": frmMBActiConfirmPreShow,
		"frmMBActivation": frmMBActivationPreShow,
		"frmMBsetPasswd": frmMBsetPasswdPreShow,
		"frmMBSetAccPinTxnPwd": frmMBSetAccPinPwdPreShow,
		"frmMBEnterATMPin": frmMBEnterATMPinPreShow,
		"frmConnectAccMB": frmConnectAccMBPreShow,
		"frmMyAccntConfirmationAddAccount": frmMyAccntConfirmationAddAccountPreShow,
		"frmMyAccntAddAccount": frmMyAccntAddAccountPreShow,
		"frmMyAccountEdit": frmMyAccountEditPreShow,
		"frmMyAccountView": frmMyAccountViewMenuPreshow,
		"frmMyAccountList": frmMyAccountListPreShow,
		"frmMyRecipientAddAcc": frmMyRecipientAddAccPreShow,
		"frmMyRecipientAddAccComplete": frmMyRecipientAddAccCompletePreShow,
		"frmMyRecipientAddAccConf": frmMyRecipientAddAccConfPreShow,
		"frmMyRecipientAddProfile": frmMyRecipientAddProfilePreShow,
		"frmMyRecipientAddProfileComp": frmMyRecipientAddProfileCompPreShow,
		"frmMyRecipientDetail": frmMyRecipientDetailPreShow,
		"frmMyRecipientEditAccComplete": frmMyRecipientEditAccCompletePreShow,
		"frmMyRecipientEditAccount": frmMyRecipientEditAccountPreShow,
		"frmMyRecipientEditProfile": frmMyRecipientEditProfilePreShow,
		"frmMyRecipientEditProfileComp": frmMyRecipientEditProfileCompPreShow,
		"frmMyRecipients": frmMyRecipientsPreShow,
		"frmMyRecipientSelectContacts": frmMyRecipientSelectContactsPreShow,
		"frmMyRecipientSelectContactsComp": frmMyRecipientSelectContactsCompPreShow,
		"frmMyRecipientSelectContactsConf": frmMyRecipientSelectContactsConfPreShow,
		"frmMyRecipientSelectFacebook": frmMyRecipientSelectFacebookPreShow,
		"frmMyRecipientSelectFBComp": frmMyRecipientSelectFBCompPreShow,
		"frmMyRecipientSelectFBConf": frmMyRecipientSelectFBConfPreShow,
		"frmMyRecipientSelectFbID": frmMyRecipientSelectFbIDPreShow,
		"frmMyRecipientSelectMobile": frmMyRecipientSelectMobilePreShow,
		"frmMyRecipientViewAccount": frmMyRecipientViewAccountPreShow,
		"frmTransferConfirm": frmTransferConfirmPreShow,
		"frmTransferLanding": preShowfrmTransferLandingMB,
		"frmTransfersAck": preShowfrmTransferAckMB,
		"frmTranfersToRecipents": preShowfrmTranfersToRecipentsMB,
		"frmBillPayment": resethzRoundAbtBillPayment,
		"frmBillPaymentComplete": frmBillPaymentCompletePreShow,
		"frmeditMyProfile" : frmeditMyProfilePreShow,
		"frmBillPaymentConfirmationFuture": frmBillPaymentConfirmationFuturePreShow,
		"frmTopUp": resethzRoundAbtTopUp,
		"frmMyTopUpList": frmMyTopUpListPreShow,
		"frmViewTopUpBiller": frmViewTopUpBillerPreShow,
		"frmMyTopUpEditScreens": frmMyTopUpEditScreensPreshow,
		"frmAddTopUpBiller": frmAddTopUpBillerconfrmtnPreshow,
		"frmMyTopUpSelect": frmMyTopUpSelectPreshow,
		"frmAddTopUpToMB": frmAddTopUpToMBPreShow,
		"frmAddTopUpBillerconfrmtn": frmAddTopUpBillerconfrmtnPreshow,
		"frmMyTopUpComplete": frmMyTopUpCompletePreshow,
		"frmBBApplyNow": frmBBApplyNowPreshow,
		"frmBBList": frmBBListPreshow,
		"frmBBConfirmAndComplete": frmBBConfirmAndCompletePreshow,
		"frmBBMyBeepAndBill": frmBBMyBeepAndBillPreshow,
		"frmSelectBiller": eh_frmSelectBiller_frmSelectBiller_preshow,
		"frmSelectBillerLanding": frmSelectBillerLandingPreShow,
		"frmSSSApply": frmSSSApplyPreShow,
		"frmSSTnC": frmSSTnCPreShow,
		"frmSSService": frmSSServicePreShow,
		"frmSSConfirmation": frmSSConfirmationPreShow,
		"frmBBPaymentApply": frmBBPaymentApplyPreshow,
		"frmBBSelectBill": frmBBSelectBillPreshow,
		"frmSSSExecute": frmSSSExecutePreShow,
		"frmSSSExecuteCalendar": frmSSSExecuteCalendarPreShow,
		"frmSSServiceED": frmSSServiceEDPreShow,
		"frmAccountStatementMB": frmAccountStatementMBPreShow,
		"frmOpnActSelAct" : frmOpnActSelActPreShow,
	    "frmOpenProdDetnTnC" : frmOpenProdDetnTnCPreShow,
		"frmOpenActTDConfirm" : frmOpenActTDConfirmPreShow,
		"frmOpenActTDAck" : frmOpenActTDAckPreShow,
		"frmOpenActSelProd" : frmOpenActSelProdMenuPreshow,
		"frmOpenActSavingCareCnfNAck" : frmOpenActSavingCareCnfNAckPreShow,
		"frmOpenActDSConfirm" : frmOpenActDSConfirmPreShow,
		"frmOpenActDSAck" : frmOpenActDSAckPreShow,
		"frmOpenAcDreamSaving" : frmOpenAcDreamSavingPreShow,
		"frmOpenAccTermDeposit" : frmOpenAccTermDepositPreShow,
		"frmOpenAccountSavingCareMB" : frmOpenAccountSavingCareMBPreShow,
		"frmOpenAccountNSConfirmation" : frmOpenAccountNSConfirmationPreShow,
		"frmDreamSavingMB":frmDreamSavingMaintainPreShow,
		"frmDreamSavingEdit":frmDreamSavingEditPreshow,
		"frmDreamCalculator":frmDreamCalculatorPreShow,
		"frmSPALogin": frmSPALoginPreShow,
		"frmNotificationHome": frmNotificationHomePreShow,
		"frmNotificationDetails": frmNotificationDetailsPreShow,
		"frmInboxHome": frmInboxHomePreShow,
		"frmInboxDetails": frmInboxDetailsPreShow,
		"frmExchangeRate": frmExchangeRatePreShow,
		"frmATMBranch": frmATMBranchPreShow,
		"frmATMBranchList": frmATMBranchListPreShow,
		"frmATMBranchesDetails": frmATMBranchesDetailsPreShow,
		"frmPromotion": frmPromotionPreShow,
		"frmPromotionDetails": frmPromotionDetailsPreShow,
		"frmMBMyActivities": frmMBMyActivitiesPreShow,
		"frmAppTour": frmAppTourPreShow,
		"frmOpenAccTermDepositToSpa" :frmOpenAccTermDepositToSpaPreShow,
		"frmContactUsMB": frmContactUsMBPreShow,
		"frmBillPaymentCompleteCalendar": frmBillPaymentCompleteCalendarPreShow,
		"frmOpenAccountNSConfirmationCalendar": frmOpenAccountNSConfirmationCalendarPreShow,
		"frmOpenActDSAckCalendar": frmOpenActDSAckCalendarPreShow,
		"frmOpenActSavingCareCnfNAckCalendar": frmOpenActSavingCareCnfNAckCalendarPreShow,
		"frmOpenActTDAckCalendar": frmOpenActTDAckCalendarPreShow,
		"frmTransfersAckCalendar": frmTransfersAckCalendarPreShow,
		"frmMBFTView":frmMBFTViewPreShow,
		"frmMBFTEdit":frmMBFTEditPreShow,
		"frmMBFTEditCnfrmtn":frmMBFTEditCnfrmtnPreShow,
		"frmMBFTEditCmplete":frmMBFTEditCmpletePreShow,
		"frmMBFtSchedule":frmMBFtSchedulePreShow,
		"frmBillPaymentView":frmBillPaymentViewPreShow,
		"frmBillPaymentEdit":frmBillPaymentEditPreShow,
		"frmEditFutureBillPaymentConfirm":frmEditFutureBillPaymentConfirmPreShow,
		"frmEditFutureBillPaymentComplete":frmEditFutureBillPaymentCompletePreShow,
		"frmScheduleBillPayEditFuture":frmScheduleBillPayEditFuturePreShow,
		"frmApplyMBSPA" : frmApplyMBSPAPreShow,
		"frmCMChgPwdSPA":frmCMChgPwdSPAPreShow,
		"frmBBExecuteConfirmAndComplete":populateOnExecuteBBPage,
		"frmMyProfileReqHistory":frmMyProfileReqHistoryPreShow,
		"frmContactUsCompleteScreenMB":frmContactUsCompleteScreenMBPreShow,
		"frmMBSetuseridSPA":frmMBSetuseridSPAPreShow,
		"frmSpaTokenactivationstartup":frmSpaTokenactivationstartupPreShow,
		"frmSpaTokenConfirmation":frmSpaTokenConfirmationPreshow,
		"frmFeedbackComplete":frmFeedbackCompletePreShow,
		"frmFATCAQuestionnaire1": frmFATCAQuestionnaire1PreShow,
		"frmFATCATnC": frmFATCATnCPreShow,
		"frmOpenProdViewAddress": frmOpenProdViewAddressPreShow,
		"frmOpenProdEditAddress": frmOpenProdEditAddressPreShow,
		"frmOpenProdEditAddressConfirm": frmOpenProdEditAddressConfirmPreShow,
		"frmTouchIdSettings":preShowTouchIdSetting,
		"frmTouchIdIntermediateLogin": preShowfrmTouchIdIntermediateLogin, 
		"frmGetTMBTouch": frmGetTMBTouchPreshow,
		"frmMBSoGooodProdBrief": frmMBSoGooodProdBriefPreShow,
        "frmMBSoGooodTnC": frmMBSoGooodTnCPreShow,
        "frmMBSoGooodConf": frmMBSoGooodConfPreShow,
        "frmApplySoGooodLanding":frmApplySoGooodLandingPreshow,
        "frmMBSoGooodPlanSelect":frmMBSoGooodPlanSelectPreshow,
        "frmMBSoGooodPlanList":frmMBSoGooodPlanListPreshow,
        "frmMBSoGooODTranasactions":frmMBSoGooODTranasactionsPreshow,
        "frmApplySoGooODComplete":frmApplySoGooODCompletePreshow,
        "frmMBPointRedemptionProdFeature":frmMBPointRedemptionProdFeaturePreShow,
        "frmMBPointRedemptionTnC":frmMBPointRedemptionTnCPreShow,
        "frmMBPointRedemptionLanding":frmMBPointRedemptionLandingPreShow,
        "frmMBPointRedemptionRewardsList":frmMBPointRedemptionRewardsListPreShow,
        "frmMBPointRedemptionConfirmation":frmMBPointRedemptionConfirmationPreShow,
        "frmMBPointRedemptionComplete":frmMBPointRedemptionCompletePreShow,
        "frmMBActivateDebitCardComplete":frmMBActivateDebitCardCompletePreShow,
        "frmMBAssignAtmPin":frmMBAssignAtmPinpreShow,
        "frmMBEStatementProdFeature":frmMBEStatementProdFeaturePreShow,
        "frmMBEStatementTnC":frmMBEStatementTnCPreShow,
        "frmMBEStatementLanding":frmMBEStatementLandingPreShow,
        "frmMBEStatementConfirmation":frmMBEStatementConfirmationPreShow,
        "frmMBEStatementComplete":frmMBEStatementCompletePreShow,
        "frmMutualFundsSummaryLanding":frmMutualFundsSummaryLandingPreShow,
        "frmMBMutualFundDetails":frmMBMutualFundsSummaryPreShow,
        "frmMBBankAssuranceSummary":frmMBBankAssuranceSummaryPreShow,
        "frmMBBankAssuranceDetails":frmMBBankAssuranceDetailsPreShow,
        "frmMFFullStatementMB":frmMBMFAcctFullStatementPreShow,
        "frmCheckContactInfo":frmCheckContactInfoPreShow,
        "frmOccupationInfo":frmCheckOccupationInfoPreShow,
        "frmeditContactInfo":frmeditConactInfoPreShow,
        "frmMBAnyIdRegTnC":frmMBAnyIdRegTnCPreShow,
        "frmMBAnyIDSelectActs":frmMBAnyIDSelectActsPreShow,
        "frmMBActivateAnyId":frmMBActivateAnyIdPreShowLocale,
        "frmMBAnyIdRegCompleted":frmMBAnyIdCompletePreShowLocale,
        "frmMBAnyIDRegAcceptTnC":frmMBAnyIDRegAcceptTnCLocale,
        "frmMBSavingsCareProdBrief":frmMBSavingsCareProdBriefPreShow,
		"frmMBSavingCareTnC":frmMBSavingCareTnCMenuPreshow,
        "frmMBSavingsCareContactInfo":frmMBSavingsCareContactInfoPreShow,
        "frmMBSavingsCareOccupationInfo":frmMBSavingsCareOccupationInfoPreShow,
        "frmMBSavingsCareAddBal":frmMBSavingsCareAddBalPreShow,
        "frmMBQuickBalanceBrief":frmMBQuickBalanceBriefPreShow,
        "frmMBSavingsCareAddNickName":preshowfrmMBSavingsCareAddNickName,
        "frmMBSavingsCareConfirmation":frmMBSavingsCareConfirmationMenuPreshow,
        "frmMBSavingsCareComplete":frmMBSavingsCareCompleteMenuPreshow,
        "frmMBQuickBalanceSetting":frmMBQuickBalanceSettingPreShow,
        "frmPreTransferMB":preShowfrmPreTransferMB,
        "frmMenu":reMenuLocaleChange,
        "frmPreMenu":preLoginMenuLocaleChange,
        //block cc & dc
        "frmMBNewTnC":frmMBNewTnCMenuPreshow,
        "frmMBBlockCardSuccess":frmMBBlockCardSuccessMenuPreshow,
        "frmMBReIssueDBProduct":frmMBReIssueDBProductMenuPreshow,
        "frmMBBlockCardRecommendation":frmMBBlockCardRecommendationMenuPreshow,
        "frmMBBlockCCReason":frmMBBlockCCReasonMenuPreshow,
        "frmMBBlockCCChangeAddress":frmMBBlockCCChangeAddressMenuPreshow,
        "frmMBBlockCardCCDBConfirm":frmMBBlockCardCCDBConfirmMenuPreshow,
        "frmMBBlockDebitCardConfirm":frmMBBlockDebitCardConfirmMenuPreshow,
        //
        "frmMBCashAdvanceCardInfo":frmMBCashAdvanceCardInfoLocalePreshow,
        "frmMBCashAdvAcctSelect":frmMBCashAdvAcctSelectLocalePreshow,
        "frmMBCashAdvanceTnC":frmMBCashAdvanceTnCLocalePreshow,
        "frmCashAdvancePlanDetails":frmCashAdvancePlanDetailsLocalePreshow,
        "frmCAPaymentPlanConfirmation":frmCAPaymentPlanConfirmationLocalePreshow,
        "frmCAPaymentPlanComplete":frmCAPaymentPlanCompleteLocalePreshow,
		"frmMBRequestNewPin":frmMBRequestNewPinLocalePreshow,
        "frmMBManageCard":frmMBManageCardLocalePreshow,
        "frmMBRequestPinSuccess":frmMBRequestPinSuccessLocalePreshow,
        "frmMBRequestPinFailure":frmMBRequestPinFailureLocalePreshow,
        "frmMBAssignAtmPinNew":frmMBAssignAtmPinNewVRpreShow,
        "frmCardActivationDetails":frmCardActivationDetailsPreShow,
        "frmCardActivationComplete":frmCardActivationCompletePreshow,
        "frmMBChangePINEnterExistsPin":frmMBChangePINEnterExistsPinLocalePreshow,
        "frmMBChangePINConfirm":frmMBChangePINConfirmLocalePreshow,
        "frmMBChangePinSuccess":frmMBChangePinSuccessLocalePreshow,
        "frmMBChangePinFailure":frmMBChangePinFailureLocalePreshow,
        "frmFPSetting":callTouchIDSettingsAndroid,
        "frmMBCardList":frmCardListLocalChange,
        "frmMBListDebitCard":frmDebitCardListLocalChange,
        "frmMBCardProductLink":preshowMBCardProductLink,
        "frmMBCardMoreDetail":preshowMBCardProductURL,
        "frmMBApplySoGooodComplete": preshowfrmMBApplySoGooodComplete,
        "frmMBApplysogooodConfirm":preshowfrmMBApplysogooodConfirm,
        "frmMBListCreditCard": frmCreditCardListLocalChange
        
	}
	if (kony.application.getCurrentForm().id == "frmMenu") {
		reMenuLocaleChange();
	}
	if (kony.application.getCurrentForm().id == "frmPreMenu") {
		preLoginMenuLocaleChange();
	}
	formLocaleList[previousFormId].call();
}

function onFailureLocaleChange() {
	alert(kony.i18n.getLocalizedString("keyPOWFail"));
}

function resethzRoundAbtBillPayment()
{
	/*
	if(flowSpa)
	{
		gblMyBillerTopUpBB = 0
		callBillPaymentCustomerAccountService();
	}
	else
	{
	*/
		frmBillPaymentPreShow();
	//}
}

function resethzRoundAbtTopUp()
{
	if(flowSpa)
	{
		gblMyBillerTopUpBB = 1
		callBillPaymentCustomerAccountService();
	}
	else
	{
		frmTopUpPreShow();
	}

}

function callTouchIDSettingsAndroid(){
//#ifdef android
			frmFPSetting.show();
//#endif
		

} 

