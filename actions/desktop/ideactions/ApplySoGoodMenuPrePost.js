//Starting frmApplySoGooODComplete.kl
function frmApplySoGooODCompleteMenuPreshow(){
	if (gblCallPrePost) 
	{
		frmApplySoGooODComplete.scrollboxMain.scrollToEnd();
    	frmApplySoGooODCompletePreshow.call(this);
	}
}
function frmApplySoGooODCompleteMenuPostshow(){
    if (gblCallPrePost) {
        // Add the new code in the post show please add here

    }
    assignGlobalForMenuPostshow();
}

//Starting frmApplySoGooodLanding.kl

function frmApplySoGooodLandingMenuPreshow(){
	if (gblCallPrePost)
	{
	    frmApplySoGooodLanding.scrollboxMain.scrollToEnd();
	    isMenuShown = false;
	    isSignedUser = true;
	    frmApplySoGooodLanding.btnNext.setEnabled(false);
	    DisableFadingEdges.call(this, frmApplySoGooodLanding);
	    frmApplySoGooodLandingPreshow.call(this);
	}
}
function frmApplySoGooodLandingMenuPostshow(){
    if (gblCallPrePost) {
        // Add the new code in the post show please add here

    }
    assignGlobalForMenuPostshow();
}


//Starting frmMBSoGooodConf.kl

function frmMBSoGooodConfMenuPreshow(){
	if (gblCallPrePost)
	{
	    isMenuShown = false;
	    frmMBSoGooodConf.scrollboxMain.scrollToEnd();
	    DisableFadingEdges.call(this, frmMBSoGooodConf);
	    frmMBSoGooodConfPreShow.call(this);
	}
}
function frmMBSoGooodConfMenuPostshow(){
    if (gblCallPrePost) {
        // Add the new code in the post show please add here

    }
    assignGlobalForMenuPostshow();
}

//Starting frmMBSoGooodPlanList.kl

function frmMBSoGooodPlanListMenuPreshow(){
	if (gblCallPrePost)
	{
	 	isMenuShown = false;
	    isSignedUser = true;
	    frmMBSoGooodPlanList.scrollboxMain.scrollToEnd();
	    DisableFadingEdges.call(this, frmMBSoGooodPlanList);
	    frmMBSoGooodPlanListPreshow.call(this);
	}
}
function frmMBSoGooodPlanListMenuPostshow(){
    if (gblCallPrePost) {
        // Add the new code in the post show please add here

    }
    assignGlobalForMenuPostshow();
}

//Starting frmMBSoGooodPlanSelect.kl

function frmMBSoGooodPlanSelectMenuPreshow(){
	frmMBSoGooodPlanSelectPreshow();
	/*if (gblCallPrePost)
	{
		//frmMBSoGooodPlanSelect.scrollboxMain.scrollToEnd();
	    isMenuShown = false;
	    isSignedUser = true;
	    //frmMBSoGooodPlanSelect.btnNext.setEnabled(false);
	    //frmMBSoGooodPlanSelect.btnNext.skin = btnDisabledGray;
	    //frmMBSoGooodPlanSelect.btnNext.focusSkin = btnDisabledGray;
	    //DisableFadingEdges.call(this, frmMBSoGooodPlanSelect);
	    frmMBSoGooodPlanSelectPreshow.call(this);
	}*/
}

function frmMBApplysogooodConfirmMenuPreshow(){
	if (gblCallPrePost)
	{
		
	    isMenuShown = false;
	    isSignedUser = true;
	   
	    frmMBApplysogooodConfirmPreShow.call(this);
	}
}

function frmMBSoGooodPlanSelectMenuPostshow(){
    if (gblCallPrePost) {
        // Add the new code in the post show please add here

    }
    assignGlobalForMenuPostshow();
}

//Starting frmMBSoGooodProdBrief.kl

function frmMBSoGooodProdBriefMenuPreshow(){

	    var deviceInfo = kony.os.deviceInfo().name;
	    var deviceInfo1 = kony.os.deviceInfo();
	    var deviceHght = deviceInfo1["deviceHeight"];
	    isMenuShown = false;
		gblSoGooodMap = {};
		gblSoGooodTotalMap = {};
		tmpGblSoGooodMap = {};
		tmpGblSoGooodTotalMap = {};
	    frmMBSoGooodProdBriefPreShow.call(this);	

	
}
function frmMBSoGooodProdBriefMenuPostshow(){

    if (gblCallPrePost) {
        // Add the new code in the post show please add here
	    isMenuRendered = false;
	    isMenuShown = false;
	    //frmMBSoGooodProdBrief.scrollboxMain.scrollToEnd();

    }
    assignGlobalForMenuPostshow();

}


//Starting frmMBSoGooodTnC.kl

function frmMBSoGooodTnCMenuPreshow(){
	if (gblCallPrePost)
	{
		isMenuShown = false;
	    isSignedUser = true;
	    //frmMBSoGooodTnC.scrollboxMain.scrollToEnd();
	    frmMBSoGooodTnC.flxSaveCamEmail.isVisible = false;
	    //frmMBSoGooodTnC.imgHeaderMiddle.src = "arrowtop.png"
	    //frmMBSoGooodTnC.imgHeaderRight.src = "empty.png"
	    frmMBSoGooodTnC.btnRight.skin = "btnRightShare";
	    //DisableFadingEdges.call(this, frmMBSoGooodTnC);
	    frmMBSoGooodTnCPreShow.call(this);	

	}
}

//Starting frmMBSoGooODTranasactions.kl

function frmMBSoGooODTranasactionsMenuPreshow(){
	frmMBSoGooODTranasactionsPreshow();
	/*if (gblCallPrePost)
	{
		frmMBSoGooODTranasactionsPreShow.call(this);
	}*/
}
function frmMBSoGooODTranasactionsMenuPostshow(){

    if (gblCallPrePost) {
        // Add the new code in the post show please add here

    }
    assignGlobalForMenuPostshow();
}

