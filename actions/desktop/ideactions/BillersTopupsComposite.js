







moduleName = "";
spaFlag = ""

    function validateOTPtextBillTopUpJavaService(otpPwd, module, spaflag) {
        var inputParam = {};
        var Pwd = otpPwd;
        spaFlag = spaflag;
        moduleName = module;
        //Common Inputs
        inputParam["module"] = moduleName;
        inputParam["password"] = otpPwd
        // TOKEN
        inputParam["TokenSwitchFlag"] = gblTokenSwitchFlag;
        inputParam["verifyToken_loginModuleId"] = "IB_HWTKN";
        inputParam["verifyToken_userStoreId"] = "DefaultStore";
        inputParam["verifyToken_retryCounterVerifyAccessPin"] = "0";
        inputParam["verifyToken_retryCounterVerifyTransPwd"] = "0";
        inputParam["verifyToken_userId"] = gblUserName;
        
        //inputParam["verifyToken_password"] = Pwd;
        inputParam["verifyToken_sessionVal"] = "";
        inputParam["verifyToken_segmentId"] = "segmentId";
        inputParam["verifyToken_segmentIdVal"] = "MIB";
        inputParam["verifyToken_channel"] = "Internet Banking";
        
        //OTP
        //inputParam["verifyPwd_TokenSwitchFlag"] = gblTokenSwitchFlag;
        inputParam["verifyPwd_retryCounterVerifyOTP"] = "0",
        inputParam["verifyPwd_userId"] = gblUserName,
        inputParam["verifyPwd_userStoreId"] = "DefaultStore",
        inputParam["verifyPwd_loginModuleId"] = "IBSMSOTP",
        //inputParam["verifyPwd_password"] = Pwd,
        inputParam["verifyPwd_segmentId"] = "MIB"
        //Mobile Pwd 
        inputParam["verifyPwdMB_loginModuleId"] = "MB_TxPwd";
        inputParam["verifyPwdMB_retryCounterVerifyAccessPin"] = "0";
        inputParam["verifyPwdMB_retryCounterVerifyTransPwd"] = "0";
        //inputParam["verifyPwdMB_password"] = Pwd;

        //Add Billers
        tmpBillerName = [];
        tmpBillerNickName = [];
        tmpBillerRef1 = [];
        tmpBillerRef2 = [];
        tmpBillerCompCode = [];
        tmpBillerRef1Lbl = [];
        tmpBillerRef2Lbl = [];
        var BatchLength = 0;
        billerCounterValue = 0;
        var tmpAddBillData = "";
        if (moduleName == "AddBillersIB")
            tmpAddBillData = frmIBMyBillersHome.segBillersConfirm.data;
        else if (moduleName == "AddTopupsIB")
            tmpAddBillData = frmIBMyTopUpsHome.segBillersConfirm.data;
        else if (moduleName == "AddBillersMB" || moduleName == "AddTopupsMB") {
            if (frmAddTopUpBillerconfrmtn.segConfirmationList.data != null)
                tmpAddBillData = frmAddTopUpBillerconfrmtn.segConfirmationList.data;
        }
        if (tmpAddBillData != null)
            BatchLength = tmpAddBillData.length;
        var billerDetails = "";
        for (i = 0; i < tmpAddBillData.length; i++) {



            if (moduleName == "AddBillersIB") {
                billerDetails = billerDetails + tmpAddBillData[i]["lblConfirmBillerName"] + "##" + tmpAddBillData[i]["BillerId"] + "##" + tmpAddBillData[i]["BillerCompCode"] + "##" + tmpAddBillData[i]["lblConfirmBillerRef1Value"] + "##" + tmpAddBillData[i]["lblConfirmBillerRef2Value"] + "##" + tmpAddBillData[i]["lblConfirmBillerNameCompCode"] + "##" + tmpAddBillData[i]["lblConfirmBillerRef1"] + "##" + tmpAddBillData[i]["lblConfirmBillerRef2"] + "##" + tmpAddBillData[i]["Ref1LblEN"] + "##" + tmpAddBillData[i]["Ref1LblTH"] + "##" + tmpAddBillData[i]["Ref2LblEN"] + "##" + tmpAddBillData[i]["Ref2LblTH"] + "##" + tmpAddBillData[i]["BillerNameEN"] + "##" + tmpAddBillData[i]["BillerNameTH"] + "~";
                
            } else if (moduleName == "AddBillersMB") {
		            if(tmpAddBillData[i]["lblcnfrmRef2Value"]==undefined)tmpAddBillData[i]["lblcnfrmRef2Value"]="";
					if(tmpAddBillData[i]["lblConfrmRef2"]==undefined)tmpAddBillData[i]["lblConfrmRef2"]="";
					if(tmpAddBillData[i]["Ref2LblEN"]==undefined)tmpAddBillData[i]["Ref2LblEN"]="";
					if(tmpAddBillData[i]["Ref2LblTH"]==undefined)tmpAddBillData[i]["Ref2LblTH"]="";
		                billerDetails = billerDetails + tmpAddBillData[i]["lblCnfrmNickName"] + "##" + tmpAddBillData[i]["BillerId"] + "##" + tmpAddBillData[i]["BillerCompCode"] + "##" + tmpAddBillData[i]["lblcnfrmRef1Value"] + "##" + tmpAddBillData[i]["lblcnfrmRef2Value"] + "##" + tmpAddBillData[i]["lblNameComp"] + "##" + tmpAddBillData[i]["lblConfrmRef1"] + "##" + tmpAddBillData[i]["lblConfrmRef2"] + "##" + tmpAddBillData[i]["Ref1LblEN"] + "##" + tmpAddBillData[i]["Ref1LblTH"] + "##" + tmpAddBillData[i]["Ref2LblEN"] + "##" + tmpAddBillData[i]["Ref2LblTH"] + "##" + tmpAddBillData[i]["BillerNameEN"] + "##" + tmpAddBillData[i]["BillerNameTH"] + "~";
		                
            } else if (moduleName == "AddTopupsIB") {
                billerDetails = billerDetails + tmpAddBillData[i]["lblConfirmBillerName"] + "##" + tmpAddBillData[i]["BillerId"] + "##" + tmpAddBillData[i]["BillerCompCode"] + "##" + tmpAddBillData[i]["lblConfirmBillerRef1Value"] + "##" + "" + "##" + tmpAddBillData[i]["lblConfirmBillerNameCompCode"] + "##" + tmpAddBillData[i]["lblConfirmBillerRef1"] + "##" + "" + "##" + tmpAddBillData[i]["Ref1LblEN"] + "##" + tmpAddBillData[i]["Ref1LblTH"] + "##" + "" + "##" + "" + "##" + tmpAddBillData[i]["BillerNameEN"] + "##" + tmpAddBillData[i]["BillerNameTH"] + "~";
                
            } else {
                billerDetails = billerDetails + tmpAddBillData[i]["lblCnfrmNickName"] + "##" + tmpAddBillData[i]["BillerId"] + "##" + tmpAddBillData[i]["BillerCompCode"] + "##" + tmpAddBillData[i]["lblcnfrmRef1Value"] + "##" + "" + "##" + tmpAddBillData[i]["lblNameComp"] + "##" + tmpAddBillData[i]["lblConfrmRef1"] + "##" + "" + "##" + tmpAddBillData[i]["Ref1LblEN"] + "##" + tmpAddBillData[i]["Ref1LblTH"] + "##" + "" + "##" + "" + "##" + tmpAddBillData[i]["BillerNameEN"] + "##" + tmpAddBillData[i]["BillerNameTH"] + "~";
                
            }

        }
        inputParam["billAdd_billerAddDetails"] = billerDetails;

        //Notification ADD
        var platformChannel = "";
        platformChannel = gblDeviceInfo
            .name;

        if (platformChannel == "thinclient") {
            //inputParam["channel"] = GLOBAL_IB_CHANNEL;
            inputParam["notificationAdd_channel"] = "IB";
            inputParam["notificationAdd_channelId"] = "Internet Banking";
            //inputParam["notificationAdd_channelName"] = "Internet Banking";
        } else {
            //inputParam["channel"] = GLOBAL_MB_CHANNEL;
            inputParam["notificationAdd_channel"] = "MB";
            inputParam["notificationAdd_channelId"] = "Mobile Banking";
            // inputParam["notificationAdd_channelName"] = "Mobile Banking";
        }

        inputParam["notificationAdd_deliveryMethod"] = "Email";
        inputParam["notificationAdd_noSendInd"] = "0";

        inputParam["notificationAdd_notificationType"] = "Email";
        inputParam["notificationAdd_Locale"] = kony.i18n.getCurrentLocale();


        inputParam["notificationAdd_customerName"] = gblCustomerName;
        inputParam["notificationAdd_appID"] = appConfig.appId;

        if (gblBillerTopupType == 1 || gblBillerTopupType == 3) //for Biller
            inputParam["notificationAdd_source"] = "addBiller";
        else if (gblBillerTopupType == 2 || gblBillerTopupType == 4) // for Topup
            inputParam["notificationAdd_source"] = "addTopup";


        //ACTIVITY LOG
        //var platformChannel = gblDeviceInfo.name;
        if (moduleName == "AddBillersIB" || moduleName == "AddBillersMB")
            inputParam["activityLog_activityTypeID"] = "061";
        else if (moduleName == "AddTopupsIB" || moduleName == "AddTopupsMB")
            inputParam["activityLog_activityTypeID"] = "062";

        //For failures the error code received from ECAS/XPRESS/Other systems. For Success case it should be "0000".
        inputParam["activityLog_errorCd"] = "";
        //This status willl be "01" for success and "02" for failure of the actual event/instance
        //inputParam["activityStatus"] = activityStatus;
        //inputParam["deviceNickName"] = deviceNickName;
        inputParam["activityLog_activityFlexValues1"] = "Add";
        //inputParam["activityFlexValues2"] = activityFlexValues2;
        //inputParam["activityFlexValues3"] = activityFlexValues3;
        //inputParam["activityFlexValues4"] = activityFlexValues4;
        //inputParam["activityFlexValues5"] = activityFlexValues5;
        if (platformChannel == "thinclient")
            inputParam["activityLog_channelId"] = GLOBAL_IB_CHANNEL;
        else
            inputParam["activityLog_channelId"] = GLOBAL_MB_CHANNEL;
        inputParam["activityLog_logLinkageId"] = "";


        if (moduleName == "AddTopupsMB" || moduleName == "AddBillersMB")
            showLoadingScreen();
        else
            showLoadingScreenPopup();


        invokeServiceSecureAsync("ConfirmBillAddService", inputParam, validateOTPtextBillTopUpJavaServiceCallback);

    }




    function validateOTPtextBillTopUpJavaServiceCallback(status, callBackResponse) {
        if (status == 400) {
            if (callBackResponse["opstatus"] == 0) {
            gblBillerID = callBackResponse["custPayID"];
                if (moduleName == "AddBillersIB") {
                
                    dismissLoadingScreenPopup();
                    frmIBMyBillersHome.segBillersConfirm.removeAll();
                    frmIBMyBillersHome.btnAddBiller.setEnabled(true); //Enabling add more since the segment is flushed
                    frmIBMyBillersHome.btnConfirmBillerAdd.setEnabled(true); //Enabling add more since the segment is flushed	
                    frmIBMyBillersHome.segSuggestedBillersList.setEnabled(true); //Enabling add more since the segment is flushed
                    frmIBMyBillersHome.imgTMBLogo.setVisibility(false);
                    frmIBMyBillersHome.hbxOtpBox.setVisibility(false)
                    frmIBMyBillersHome.hbxBillersAddContainer.setVisibility(false);
                    frmIBMyBillersHome.hbxBillersConfirmContainer.setVisibility(false);
                    frmIBMyBillersHome.hbxBillersEditContainer.setVisibility(false);
                    frmIBMyBillersHome.hbxBillersViewContainer.setVisibility(false);
                    frmIBMyBillersHome.hbxCanConfBtnContainer.setVisibility(false);
                    frmIBMyBillersHome.hbxBillersCompleteContainer.setVisibility(true);
                    frmIBMyBillersHome.imgArrowAddBiller.setVisibility(true);
                    frmIBMyBillersHome.imgArrowSegBiller.setVisibility(false);
                    //to disable OTP related widgets
                    frmIBMyBillersHome.lblBankRef.setVisibility(false);
                    frmIBMyBillersHome.lblBankRefValue.setVisibility(false);
                    frmIBMyBillersHome.lblOTPMsg.setVisibility(false);
                    frmIBMyBillersHome.lblOTPMobileNo.setVisibility(false);
                    frmIBMyBillersHome.txtotp.text = "";
                    frmIBMyBillersHome.tbxToken.text = "";
                    frmIBMyBillersHome.btnOTPReq.onClick = startBillTopUpOTPRequestService;
                    frmIBMyBillersHome.btnOTPReq.skin = btnIBREQotpFocus;
                    frmIBMyBillersHome.btnOTPReq.focusSkin = btnIBREQotpFocus;
                    campaginService("imgMyBiller","frmIBMyBillersHome","I");
                    postShowForBillers();
                } else if (moduleName == "AddTopupsIB") {
                
                    dismissLoadingScreenPopup();
                    //Added for Masking Fleet Card
                    mapIBTopUpCompleteSegData();
                    frmIBMyTopUpsHome.segBillersConfirm.removeAll();
                    frmIBMyTopUpsHome.btnAddBiller.setEnabled(true); //Enabling add more since the segment is flushed
                    frmIBMyTopUpsHome.btnConfirmBillerAdd.setEnabled(true); //Enabling add more since the segment is flushed
                    frmIBMyTopUpsHome.segSuggestedBillersList.setEnabled(true); //Enabling add more since the segment is flushed
                    frmIBMyTopUpsHome.imgTMBLogo.setVisibility(false);
                    frmIBMyTopUpsHome.hbxOtpBox.setVisibility(false)
                    frmIBMyTopUpsHome.hbxBillersAddContainer.setVisibility(false);
                    frmIBMyTopUpsHome.hbxBillersConfirmContainer.setVisibility(false);
                    frmIBMyTopUpsHome.hbxBillersEditContainer.setVisibility(false);
                    frmIBMyTopUpsHome.hbxBillersViewContainer.setVisibility(false);
                    frmIBMyTopUpsHome.hbxCanConfBtnContainer.setVisibility(false);
                    frmIBMyTopUpsHome.hbxBillersCompleteContainer.setVisibility(true);
                    frmIBMyTopUpsHome.imgArrowAddBiller.setVisibility(true);
                    frmIBMyTopUpsHome.imgArrowSegBiller.setVisibility(false);
                    //to disable OTP related widgets
                    frmIBMyTopUpsHome.lblBankRef.setVisibility(false);
                    frmIBMyTopUpsHome.lblBankRefValue.setVisibility(false);
                    frmIBMyTopUpsHome.lblOTPMsg.setVisibility(false);
                    frmIBMyTopUpsHome.lblOTPMobileNo.setVisibility(false);
                    frmIBMyTopUpsHome.txtotp.text = "";
                    frmIBMyTopUpsHome.tbxToken.text = "";
                    frmIBMyTopUpsHome.btnOTPReq.onClick = startBillTopUpOTPRequestService;
                    frmIBMyTopUpsHome.btnOTPReq.skin = btnIBREQotpFocus;
                    frmIBMyTopUpsHome.btnOTPReq.focusSkin = btnIBREQotpFocus;
                    campaginService("imgTopup","frmIBMyTopUpsHome","I");
                    postShowForTopups();
                } else if (moduleName == "AddTopupsMB" || moduleName == "AddBillersMB") {
                	
                	
                	if (flowSpa) {
                        otplocked = false;
                        gblVerifyOTPCounter = "0";
                        getHeader(mobileMethod, kony.i18n.getLocalizedString("keySetPasswordLabel"), 0, 0, 0);
                        gblShowPinPwdSecs = kony.os.toNumber(callBackResponse["showPinPwdSecs"]);
                        gblRetryCountRequestOTP = "0";
                        gblShowPwdNo = kony.os.toNumber(callBackResponse["showPinPwdCount"]);
                        popOtpSpa.dismiss();
                        kony.application.dismissLoadingScreen();


                    } else {
                        popupTractPwd.dismiss();
                        dismissLoadingScreen();

                    }
                    
                    frmMyTopUpList.button1010778103103263.setEnabled(true);
                    enableAddButtonTopUpBillerConfirm();
                    frmMyTopUpList.segSuggestedBillers.setEnabled(true);
                    gblBlockBillerTopUpAdd = false;
                    var completeSegData = frmAddTopUpBillerconfrmtn.segConfirmationList.data;
                    frmMyTopUpComplete.segComplete.setData(completeSegData);
                    frmMyTopUpComplete.show();
                    //TMBUtil.DestroyForm(frmAddTopUpBillerconfrmtn);
                    frmAddTopUpBillerconfrmtn.segConfirmationList.data = [];
                    TMBUtil.DestroyForm(frmAddTopUpToMB);
                    //addAnotherValidValue = addAnotherValidValue + 1;
                    //counterValue = counterValue + 1;
                    //
                    gblFlagConfirmDataAddedMB = 0;
              }
            } else if ((callBackResponse["opstatus"] == 8005)) {
               var currFrm = kony.application.getCurrentForm().id;
                if ((moduleName == "AddBillersIB") || (moduleName == "AddTopupsIB")) {
                	if (moduleName == "AddBillersIB")
                    	 frmIBMyBillersHome.txtotp.text = "";
                    else
                        frmIBMyTopUpsHome.txtotp.text = "";
                     
                    dismissLoadingScreenPopup();
                    if (callBackResponse["errCode"] == "VrfyOTPErr00001") {
                        gblVerifyOTPCounter = "0";
                        dismissLoadingScreenPopup();
                       // alert("in iffffff OTP incorrec");
                        //alert("" + kony.i18n.getLocalizedString("invalidOTP"));//commentd by swapna
                      if(currFrm == "frmIBMyBillersHome"){
	                       frmIBMyBillersHome.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");//"OTP is not correct.";//kony.i18n.getLocalizedString("invalidOTP"); //
	                       frmIBMyBillersHome.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");//"Please try again."; 
	                       frmIBMyBillersHome.hbxOTPincurrect.isVisible = true;
	                       frmIBMyBillersHome.hbxBankDetails.isVisible = false;
	                       frmIBMyBillersHome.hbxOTPsnt.isVisible = false;
	                       frmIBMyBillersHome.txtotp.text = "";
	                       if (frmIBMyBillersHome.hbxOTPEntry.isVisible){
	                       		frmIBMyBillersHome.txtotp.setFocus(true);
	                       }else{
	                       		frmIBMyBillersHome.tbxToken.text = "";
	                       		frmIBMyBillersHome.tbxToken.setFocus(true);
	                       	}
                      }else if(currFrm == "frmIBMyTopUpsHome"){
	                       frmIBMyTopUpsHome.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");//kony.i18n.getLocalizedString("invalidOTP"); //
	                       frmIBMyTopUpsHome.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo"); 
	                       frmIBMyTopUpsHome.hbxOTPincurrect.isVisible = true;
	                       frmIBMyTopUpsHome.hbxBankDetails.isVisible = false;
	                       frmIBMyTopUpsHome.hbxOTPsnt.isVisible = false; 
	                       frmIBMyTopUpsHome.txtotp.text = "";
	                       if (frmIBMyTopUpsHome.hbxOTPEntry.isVisible){
	                       		frmIBMyTopUpsHome.txtotp.setFocus(true); 
	                       	}else{
	                       		frmIBMyTopUpsHome.tbxToken.text = "";
	                       		frmIBMyTopUpsHome.tbxToken.setFocus(true);
	                       	}                     
                       }
                        return false;
                    } else if (callBackResponse["errCode"] == "VrfyOTPErr00002") {
                        dismissLoadingScreenPopup();
                        // alert("" + kony.i18n.getLocalizedString("ECVrfyOTPErr"));
                        // startRcCrmUpdateProIB("04");
                        handleOTPLockedIB(callBackResponse);
                        gblOTPLockedForBiller = "04"
                        return false;
                    } else if (callBackResponse["errCode"] == "VrfyOTPErr00005") {
                        dismissLoadingScreenPopup();
                        //alert("" + kony.i18n.getLocalizedString("KeyTokenSerialNumError"));
                        alert("" + kony.i18n.getLocalizedString("invalidOTP"));
                        return false;
                    } else if (callBackResponse["errCode"] == "VrfyOTPErr00006") {
                        //gblVerifyOTPCounter = callBackResponse["retryCounterVerifyOTP"];
                        alert("" + callBackResponse["errMsg"]);
                        return false;
                    } else if (callBackResponse["errCode"] == "GenOTPRtyErr00001") {
                        dismissLoadingScreenPopup();
                        showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                        return false;
                   	}else{
                   	 //alert("in elssseee OTP Correct");
                      if(currFrm == "frmIBMyBillersHome"){
	                       frmIBMyBillersHome.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");//kony.i18n.getLocalizedString("invalidOTP"); //
	                       frmIBMyBillersHome.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
	                       frmIBMyBillersHome.hbxOTPincurrect.isVisible = true;
	                       frmIBMyBillersHome.hbxBankDetails.isVisible = false;
	                       frmIBMyBillersHome.hbxOTPsnt.isVisible = false;
	                       frmIBMyBillersHome.txtotp.text = "";
	                       frmIBMyBillersHome.txtotp.setFocus(true);
                      }else{
	                       frmIBMyTopUpsHome.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");
	                       frmIBMyTopUpsHome.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
	                       frmIBMyTopUpsHome.hbxOTPincurrect.isVisible = true;
	                       frmIBMyTopUpsHome.hbxBankDetails.isVisible = false;
	                       frmIBMyTopUpsHome.hbxOTPsnt.isVisible = false;  
	                       frmIBMyTopUpsHome.txtotp.text = "";
	                       frmIBMyTopUpsHome.txtotp.setFocus(true);                     
                       }               	
                      // dismissLoadingScreenPopup();
                   	}
                } else if ((moduleName == "AddBillersMB" || moduleName == "AddTopupsMB") && spaFlag == true) {


					popOtpSpa.txtOTP.text="";
                    if (callBackResponse["errCode"] == "VrfyOTPErr00001") {
                        gblVerifyOTPCounter = "0";
                              
                        popOtpSpa.lblPopupTract2Spa.text = kony.i18n.getLocalizedString("wrongOTP");
                        popOtpSpa.lblPopupTract4Spa.text = "";

                        kony.application.dismissLoadingScreen();
                        return false;
                    } else if (callBackResponse["errCode"] == "VrfyOTPErr00002") {
                        gblVerifyOTPCounter = "0";
                        otplocked = true;
                        kony.application.dismissLoadingScreen();
                        popOtpSpa.dismiss();
                        popTransferConfirmOTPLock.show();
                        // calling crmprofileMod to update the user status
                        //updteuserSpa();

                        return false;
                    } else if (callBackResponse["errCode"] == "VrfyOTPErr00003") {
                        kony.application.dismissLoadingScreen();
                        showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00003"), kony.i18n.getLocalizedString("info"));
                        return false;
                    } else if (callBackResponse["errCode"] == "VrfyOTPErr00004") {
                        kony.application.dismissLoadingScreen();
                        showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00004"), kony.i18n.getLocalizedString("info"));
                        return false;
                    } else if (callBackResponse["errCode"] == "VrfyOTPErr00005") {
                        kony.application.dismissLoadingScreen();
                        showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00005"), kony.i18n.getLocalizedString("info"));
                        return false;


                    }
					 else if (callBackResponse["errCode"] == "VrfyOTPErr00006") {
                        //gblVerifyOTPCounter = callBackResponse["retryCounterVerifyOTP"];
                        gblVerifyOTPCounter ="0";
                        alert("" + callBackResponse["errMsg"]);
                        return false;
                   	 }else if (callBackResponse["errCode"] == "GenOTPRtyErr00001") {
                        dismissLoadingScreenPopup();
                        showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                        return false;
           	        }
             } else if (moduleName == "AddTopupsMB" || moduleName == "AddBillersMB") {
                    gblRtyCtrVrfyTxPin = "0";//callBackResponse["retryCounterVerifyTransPwd"];
                    gblRtyCtrVrfyAxPin = "0";//callBackResponse["retryCounterVerifyAccessPin"];
                    // popupTractPwd.dismiss();
                    //  
                    dismissLoadingScreen();
                    //showAlert("" + callBackResponse["errMsg"]);
                    // return false;
                    if (callBackResponse["errCode"] == "VrfyTxPWDErr00003") {
                    	showTranPwdLockedPopup();
					}else {
						setTransPwdFailedError(kony.i18n.getLocalizedString("invalidTxnPwd"));
					}
                }
            } else {
                if (glbChannelFlag == 1 || glbChannelFlag == 2) {
                    dismissLoadingScreenPopup();
                } else if (glbChannelFlag == 3 || glbChannelFlag == 4) {
                    dismissLoadingScreen();
                }
                if (callBackResponse["errCode"] == "MAXBILLERS") {
                	alert(kony.i18n.getLocalizedString("MIB_BPErrMaxBiller"));
                } else {
	                alert(kony.i18n.getLocalizedString("ECGenericError"));
                }

            }

        } else if (status == 300) {
            if (glbChannelFlag == 1 || glbChannelFlag == 2) {
                dismissLoadingScreenPopup();
            } else if (glbChannelFlag == 3 || glbChannelFlag == 4) {
                dismissLoadingScreen();
            }
            alert(kony.i18n.getLocalizedString("ECGenericError"));
        }
    }
    
function mapIBTopUpCompleteSegData() {
	  frmIBMyTopUpsHome.segAddBillerComplete.widgetDataMap = {
	  "lblBillerNameComplete": "lblConfirmBillerName",
	  "imgAddBillerComplete": "imgConfirmBillerLogo",
	  "lblBillerNameCompCodeComplete": "lblConfirmBillerNameCompCode",
	  "lblRef1Complete": "lblConfirmBillerRef1",
	  "lblRef1CompleteValue": "lblConfirmBillerRef1Value"
	  //"btnPayBillComplete": "btnPayBillComplete"
	 };
	 var temp = frmIBMyTopUpsHome.segBillersConfirm.data;
	 for(i = 0 ;i<temp.length;i++){
	  var fleetCard = temp[i]["lblConfirmBillerRef1Value"];
	  fleetCard = maskCreditCard(fleetCard);
	  temp[i]["lblConfirmBillerRef1Value"] = fleetCard;
	 }
	 frmIBMyTopUpsHome.segAddBillerComplete.setData(temp);
 }     
