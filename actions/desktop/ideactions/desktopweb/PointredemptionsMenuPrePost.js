function frmMBPointRedemptionCompleteMenuPreshow() {
    if (gblCallPrePost) {
        isMenuShown = false;
        isSignedUser = true;
        frmMBPointRedemptionComplete.scrollboxMain.scrollToEnd();
        DisableFadingEdges.call(this, frmMBPointRedemptionComplete);
        frmMBPointRedemptionCompletePreShow.call(this);
    }
}

function frmMBPointRedemptionCompleteMenuPostshow() {
    if (gblCallPrePost) {
        // Add the new code in the post show please add here
    }
    assignGlobalForMenuPostshow();
}

function frmMBPointRedemptionConfirmationMenuPreshow() {
    if (gblCallPrePost) {
        isMenuShown = false;
        isSignedUser = true;
        frmMBPointRedemptionConfirmation.scrollboxMain.scrollToEnd();
        DisableFadingEdges.call(this, frmMBPointRedemptionConfirmation);
        frmMBPointRedemptionConfirmationPreShow.call(this);
    }
}

function frmMBPointRedemptionConfirmationMenuPostshow() {
    if (gblCallPrePost) {
        // Add the new code in the post show please add here
    }
    assignGlobalForMenuPostshow();
}

function frmMBPointRedemptionLandingMenuPreshow() {
    if (gblCallPrePost) {
        isMenuShown = false;
        isSignedUser = true;
        frmMBPointRedemptionLanding.scrollboxMain.scrollToEnd();
        DisableFadingEdges.call(this, frmMBPointRedemptionLanding);
        frmMBPointRedemptionLandingPreShow.call(this);
    }
}

function frmMBPointRedemptionLandingMenuPostshow() {
    if (gblCallPrePost) {
        // Add the new code in the post show please add here
    }
    assignGlobalForMenuPostshow();
}

function frmMBPointRedemptionProdFeatureMenuPreshow() {
    if (gblCallPrePost) {
        var deviceInfo = kony.os.deviceInfo().name;
        var deviceInfo1 = kony.os.deviceInfo();
        var deviceHght = deviceInfo1["deviceHeight"];
        isMenuShown = false;
        frmMBPointRedemptionProdFeature.scrollboxMain.scrollToEnd();
        frmMBPointRedemptionProdFeature.imgHeaderMiddle.src = "arrowtop.png"
        frmMBPointRedemptionProdFeature.imgHeaderRight.src = "empty.png"
        DisableFadingEdges.call(this, frmMBPointRedemptionProdFeature);
        frmMBPointRedemptionProdFeaturePreShow.call(this);
    }
}

function frmMBPointRedemptionProdFeatureMenuPostshow() {
    if (gblCallPrePost) {
        isMenuRendered = false;
        isMenuShown = false;
        frmMBPointRedemptionProdFeature.scrollboxMain.scrollToEnd();
    }
    assignGlobalForMenuPostshow();
}

function frmMBPointRedemptionRewardsListMenuPreshow() {
    if (gblCallPrePost) {
        frmMBPointRedemptionRewardsListPreShow.call(this);
    }
}

function frmMBPointRedemptionRewardsListMenuPostshow() {
    if (gblCallPrePost) {
        // Add the new code in the post show please add here
    }
    assignGlobalForMenuPostshow();
}

function frmMBPointRedemptionTnCMenuPreshow() {
    if (gblCallPrePost) {
        frmMBPointRedemptionTnC.scrollboxMain.scrollToEnd();
        isMenuShown = false;
        isSignedUser = true;
        DisableFadingEdges.call(this, frmMBPointRedemptionTnC);
        frmMBPointRedemptionTnCPreShow.call(this);
    }
}

function frmMBPointRedemptionTnCMenuPostshow() {
    if (gblCallPrePost) {
        // Add the new code in the post show please add here
    }
    assignGlobalForMenuPostshow();
}

function frmMBPointRedemptionToAccountsMenuPreshow() {
    if (gblCallPrePost) {
        frmMBPointRedemptionToAccountsPreShow.call(this);
    }
}

function frmMBPointRedemptionToAccountsMenuPostshow() {
    if (gblCallPrePost) {
        // Add the new code in the post show please add here
    }
    assignGlobalForMenuPostshow();
}