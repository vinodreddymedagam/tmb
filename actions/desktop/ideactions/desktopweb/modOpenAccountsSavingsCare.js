savingsCareProdFeature_image_EN = "";
savingsCareProdFeature_image_TH = "";
flow = "";
accDetail = "";
accEdit = "";
gblSelectedRel = "";
openMoreSave = false;
gblFromNickNameBack = true;

function frmMBSavingsCareProdBriefPreShow() {
    changeStatusBarColor();
    kony.print("Inside frmMBSavingsCareProdBriefPreShow()----->");
    frmMBSavingsCareProdBrief.lblHdrTxt.text = kony.i18n.getLocalizedString("keyOpenAcc"); //kony.i18n.getLocalizedString("MIB_P2PPrdBri"); -- Add i18 Key here.
    frmMBSavingsCareProdBrief.btnback.text = kony.i18n.getLocalizedString("Back");
    frmMBSavingsCareProdBrief.btnnext.text = kony.i18n.getLocalizedString("Next");
    //frmMBSavingsCareProdBrief.flxHeader.zIndex="3";
    //frmMBSavingsCareProdBrief.flexscrollBody.top="7.5%";
    //frmMBSavingsCareProdBrief.flexscrollBody.width="100%";
    frmMBSavingsCareProdBrief.flexscrollBody.height = "82%"
    if (getDeviceType() == "iPhone5") {
        frmMBSavingsCareProdBrief.imgTransNBpAckComplete.height = "101%";
    }
    getSavingCareBriefImage();
}

function getSavingCareBriefImage() {
    kony.print("Inside getSavingCareBriefImage()----->");
    var savingsCareProdFeature_image_name_EN = "SavingsCareIntroEN";
    var savingsCareProdFeature_image_name_TH = "SavingsCareIntroTH";
    if (savingsCareProdFeature_image_EN == "") savingsCareProdFeature_image_EN = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + savingsCareProdFeature_image_name_EN + "&modIdentifier=PRODUCTPACKAGEIMG";
    if (savingsCareProdFeature_image_TH == "") savingsCareProdFeature_image_TH = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + savingsCareProdFeature_image_name_TH + "&modIdentifier=PRODUCTPACKAGEIMG";
    if (kony.i18n.getCurrentLocale() == "th_TH") frmMBSavingsCareProdBrief.imgTransNBpAckComplete.src = savingsCareProdFeature_image_TH;
    else frmMBSavingsCareProdBrief.imgTransNBpAckComplete.src = savingsCareProdFeature_image_EN;
}

function showSavingCareProductBrief() {
    kony.print("Inside showSavingCareProductBrief()----->");
    //getSavingCareBriefImage();
    frmMBSavingsCareProdBrief.show();
}

function frmMBSavingsCareTnCLocale() {
    if (flow == "myAccount") {
        frmMBAnyIDRegAcceptTnC.lblHdrTxt.text = kony.i18n.getLocalizedString("keyMyAccountIB");
    } else {
        frmMBAnyIDRegAcceptTnC.lblHdrTxt.text = kony.i18n.getLocalizedString("keyOpenAcc");
    }
}

function onMyAccountEdit() {
    kony.print("GOWRI in onMyAccountEdit gblSavingsCareFlow=" + gblSavingsCareFlow);
    kony.print("GOWRI in onMyAccountEdit gblOpeningMethod=" + gblOpeningMethod);
    if (gblSavingsCareFlow == "MyAccountFlow") {
        gblOpenSavingsCareNickName = frmMyAccountView.lblNickName.text;
        //if(gblBeneficiaryData.length==0 && gblOpeningMethod=="BRN"){
        if (gblOpeningMethod == "BRN" && gblNeverSetBeneficiary == "Y") {
            popupConfrmDelete.lblheader.text = kony.i18n.getLocalizedString("edit_benefi");
            popupConfrmDelete.lblPopupConfText.text = kony.i18n.getLocalizedString("keyeditBeneficSure");
            //"Are you sure you want to edit your beneficiaries? The new beneficiaries will overwrite any previous beneficiaries that were submitted at a TMB branch." //kony.i18n.getLocalizedString("deleteTopUpMsg2");
            popupConfrmDelete.btnpopConfDelete.text = kony.i18n.getLocalizedString("keyYes");
            popupConfrmDelete.btnPopupConfCancel.text = kony.i18n.getLocalizedString("keyNo");
            popupConfrmDelete.btnpopConfDelete.onClick = onpopupConfrmDeleteYES;
            popupConfrmDelete.btnPopupConfCancel.onClick = onpopupConfrmDeleteNO;
            popupConfrmDelete.show();
        } else {
            loadSavingsCareTermsNConditions();
        }
    } else {
        notSavingsCareMyAccountEdit();
    }
}

function onpopupConfrmDeleteYES() {
    popupConfrmDelete.dismiss();
    loadSavingsCareTermsNConditions();
}

function onpopupConfrmDeleteNO() {
    popupConfrmDelete.dismiss();
}

function onMyAccountDetailsEdit() {
    kony.print("GOWRI in onMyAccountDetailsEdit gblSavingsCareFlow=" + gblSavingsCareFlow);
    kony.print("GOWRI in onMyAccountDetailsEdit gblOpeningMethod=" + gblOpeningMethod);
    if (gblSavingsCareFlow == "AccountDetailsFlow") {
        gblOpenSavingsCareNickName = frmAccountDetailsMB.lblAccountNameHeader.text;
        kony.print("GOWRI gblNeverSetBeneficiary=" + gblNeverSetBeneficiary);
        if (gblOpeningMethod == "BRN" && gblNeverSetBeneficiary == "Y") {
            popupConfrmDelete.lblheader.text = kony.i18n.getLocalizedString("edit_benefi");
            popupConfrmDelete.lblPopupConfText.text = kony.i18n.getLocalizedString("keyeditBeneficSure");
            //"Are you sure you want to edit your beneficiaries? The new beneficiaries will overwrite any previous beneficiaries that were submitted at a TMB branch." //kony.i18n.getLocalizedString("deleteTopUpMsg2");
            popupConfrmDelete.btnpopConfDelete.text = kony.i18n.getLocalizedString("keyYes");
            popupConfrmDelete.btnPopupConfCancel.text = kony.i18n.getLocalizedString("keyNo");
            popupConfrmDelete.btnpopConfDelete.onClick = onpopupConfrmDeleteYES;
            popupConfrmDelete.btnPopupConfCancel.onClick = onpopupConfrmDeleteNO;
            popupConfrmDelete.show();
        } else {
            loadSavingsCareTermsNConditions();
        }
    }
}
// for Terms & COnditions
function loadSavingsCareTermsNConditionsEdit() {
    kony.print("GOWRI IN loadSavingsCareTermsNConditions gblSavingsCareFlow=" + gblSavingsCareFlow);
    onMyAccountEdit();
}
// for Terms & COnditions
function loadSavingsCareTermsNConditions() {
    kony.print("GOWRII ENETERED in loadSavingsCareTermsNConditions");
    var input_param = {};
    var locale = kony.i18n.getCurrentLocale();
    if (locale == "en_US") {
        input_param["localeCd"] = "en_US";
    } else {
        input_param["localeCd"] = "th_TH";
    }
    //module key again is from tmb_tnc file
    var prodDescTnC = "TMBSavingCare";
    //prodDescTnC = prodDescTnC.replace(/\s+/g, '');
    input_param["moduleKey"] = prodDescTnC;
    showLoadingScreen();
    invokeServiceSecureAsync("readUTFFile", input_param, loadSavingsCareTNCBackMB);
}

function notSavingsCareMyAccountEdit() {
    kony.print("IN notSavingsCareMyAccountEdit gblSavingsCareFlow=" + gblSavingsCareFlow);
    gblAcctNicName = frmMyAccountView.lblNickName.text;
    kony.print(" gblAcctNicName is " + gblAcctNicName);
    gblAccNo = frmMyAccountView.lblAccntNoValue.text;
    kony.print(" gblAccNo is " + gblAccNo);
    if (checkMBUserStatus()) {
        frmMyAccountEdit.txtEditAccntNickName.text = frmMyAccountView.lblNickName.text;
        onClickEditMyaccountMB();
        frmMyAccountEdit.show();
    }
}

function loadSavingsCareTNCBackMB(status, result) {
    kony.print("GOWRI SUCESS result=" + JSON.stringify(result));
    if (status == 400) {
        if (result["opstatus"] == 0) {
            kony.print("GOWRI SUCESS 1");
            frmMBSavingCareTnC.btnRight.setVisibility(true);
            //if(flow=="myAccount"){
            //			frmMBSavingCareTnC.lblHdrTxt.text = kony.i18n.getLocalizedString("keyMyAccountIB");
            //			}else{
            //				frmMBSavingCareTnC.lblHdrTxt.text = kony.i18n.getLocalizedString("keyOpenAcc");
            //			}
            //			frmMBSavingCareTnC.lblOpenActDescSubTitle.text = kony.i18n.getLocalizedString("keyTermsNConditions");
            //frmMBSavingCareTnC.lblHdrTxt.text = kony.i18n.getLocalizedString("keyOpenAcc");
            //frmMBSavingCareTnC.richTextProdDetails.setVisibility(false);
            //frmMBSavingCareTnC.hboxSaveCamEmail.setVisibility(false);
            //frmMBSavingCareTnC.richtext47425439023817.setVisibility(true);
            frmMBSavingCareTnC.btnnext.text = kony.i18n.getLocalizedString("keyAgreeButton");
            frmMBSavingCareTnC.btnback.text = kony.i18n.getLocalizedString("Back");
            frmMBSavingCareTnC.richtextTnC.text = result["fileContent"]; // current 
            frmMBSavingCareTnC.lblOpenActDescSubTitle.setFocus(true);
            dismissLoadingScreen();
            frmMBSavingCareTnC.show();
        } else {
            kony.print("GOWRI SUCESS 2");
            dismissLoadingScreen();
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}

function storingOldBenificiaries() {
    gblArrayBenificiaryOld = [];
    if (gblBeneficiaryData.length > 0) {
        var currLocale = kony.i18n.getCurrentLocale();
        for (var i = 0; i < gblBeneficiaryData.length; i++) {
            var benRec = {};
            var fullName = gblBeneficiaryData[i]["fullName"];
            var nameArr = fullName.split(" ");
            var len = nameArr.length;
            var FM_len = len - 1;
            var firstName = "";
            var lastName = "";
            if (len >= 0) {
                firstName = nameArr[0];
            }
            for (var j = 1; j < len; j++) {
                lastName += " " + nameArr[j];
            }
            if (firstName == undefined) {
                firstName = "";
            }
            if (lastName == undefined) {
                lastName = "";
            }
            benRec["firstName"] = firstName.trim();
            benRec["lastName"] = lastName.trim();
            if (currLocale == "en_US") {
                benRec["relation"] = gblBeneficiaryData[i]["relationEN"];
                benRec["relationCode"] = gblBeneficiaryData[i]["relationCode"];
            } else {
                benRec["relation"] = gblBeneficiaryData[i]["relationTH"];
                benRec["relationCode"] = gblBeneficiaryData[i]["relationCode"];
            }
            benRec["percentage"] = gblBeneficiaryData[i]["percentValue"];
            kony.print("firstName=" + benRec["firstName"] + "lastName=" + benRec["lastName"] + " relation=" + benRec["relation"] + " percentage=" + benRec["percentage"])
                // benRec{"firstName":kiran,""}
                // arrayBenificiary.push(benRec);
            gblArrayBenificiaryOld.push(benRec);
        }
    }
}

function onClickNextSavingsCareTnC() {
    gblCCDBCardFlow = "";
    arrayBenificiary = [];
    showLoadingScreen();
    kony.print("gblBeneficiaryData  " + JSON.stringify(gblBeneficiaryData));
    //kony.print("gblRelationDescAndCodeTH  " +JSON.stringify(gblRelationDescAndCodeTH));
    //kony.print("gblRelationDescAndCodeEN  " +JSON.stringify(gblRelationDescAndCodeEN));
    if (gblSavingsCareFlow == "MyAccountFlow" || gblSavingsCareFlow == "AccountDetailsFlow") {
        gblSelProduct = "TMBSavingcare";
        gblTest = false;
        if (gblBeneficiaryData.length > 0) {
            var currLocale = kony.i18n.getCurrentLocale();
            for (var i = 0; i < gblBeneficiaryData.length; i++) {
                var benRec = {};
                var fullName = gblBeneficiaryData[i]["fullName"];
                var nameArr = fullName.split(" ");
                var len = nameArr.length;
                var firstName = "";
                var lastName = "";
                if (len >= 0) {
                    firstName = nameArr[0];
                }
                for (var j = 1; j < len; j++) {
                    lastName += " " + nameArr[j];
                }
                if (firstName == undefined) {
                    firstName = "";
                }
                if (lastName == undefined) {
                    lastName = "";
                }
                benRec["firstName"] = firstName.trim();
                benRec["lastName"] = lastName.trim();
                if (currLocale == "en_US") {
                    benRec["relation"] = gblBeneficiaryData[i]["relationEN"];
                    benRec["relationCode"] = gblBeneficiaryData[i]["relationCode"];
                } else {
                    benRec["relation"] = gblBeneficiaryData[i]["relationTH"];
                    benRec["relationCode"] = gblBeneficiaryData[i]["relationCode"];
                }
                benRec["percentage"] = gblBeneficiaryData[i]["percentValue"];
                kony.print("firstName=" + benRec["firstName"] + "lastName=" + benRec["lastName"] + " relation=" + benRec["relation"] + " percentage=" + benRec["percentage"])
                    // benRec{"firstName":kiran,""}
                arrayBenificiary.push(benRec);
            }
        }
        kony.print("GOWRI in onClickNextSavingsCareTnC gblAccountDetailsNickName=" + gblAccountDetailsNickName);
        //gblOpenSavingsCareNickName=gblAccountDetailsNickName;
        //frmMBSavingsCareAddNickName.txtNickName.text=gblAccountDetailsNickName;
        dismissLoadingScreen();
        getRelationshipList(); //Getting the relationships list from DB and storing them in global variables.
        frmMBSavingsCareAddNickName.show();
    } else {
        gblSavingsCareFlow = "openAccount";
        getRelationshipList(); //Getting the relationships list from DB and storing them in global variables.
        invokePartyInqService();
    }
    /*if(gblSavingsCareFlow=="MyAccountFlow"){
		  onClickAddAccountNext();
	    }else if(gblSavingsCareFlow=="AccountDetailsFlow"){
			flow="";
		  //invokePartyInqService();
	    }*/
}

function frmMBSavingsCareContactInfoPreShow() {
    //frmMBSavingsCareContactInfo.scrollboxMain.scrollToEnd();
    gblOpenActSavingCareEditCont = false;
    frmMBSavingsCareContactInfo.lblHdrTxt.text = kony.i18n.getLocalizedString("Kyc_ttl");
    frmMBSavingsCareContactInfo.lblContactinfo.text = kony.i18n.getLocalizedString("Kyc_lbl_contactInfo");
    frmMBSavingsCareContactInfo.lblName.text = kony.i18n.getLocalizedString("Kyc_lbl_thname");
    frmMBSavingsCareContactInfo.lblName2.text = kony.i18n.getLocalizedString("Kyc_lbl_enname");
    frmMBSavingsCareContactInfo.lblRegisterAddr.text = kony.i18n.getLocalizedString("Kyc_lbl_registeraddr");
    frmMBSavingsCareContactInfo.lblContactAddr.text = kony.i18n.getLocalizedString("Kyc_lbl_contactaddr");
    frmMBSavingsCareContactInfo.lblMobileNum.text = kony.i18n.getLocalizedString("Kyc_lbl_mobileno");
    frmMBSavingsCareContactInfo.lblEmailAddr.text = kony.i18n.getLocalizedString("Kyc_lbl_email");
    frmMBSavingsCareContactInfo.richtextfooterNote.text = kony.i18n.getLocalizedString("Kyc_lbl_contactdescNew");
    frmMBSavingsCareContactInfo.lblMobile.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
    frmMBSavingsCareContactInfo.btnnext.text = kony.i18n.getLocalizedString("keyConfirm");
    frmMBSavingsCareContactInfo.btnback.text = kony.i18n.getLocalizedString("Back");
    dismissLoadingScreen();
}

function invokePartyInqService() {
    //frmMBSavingsCareContactInfo.show();
    kony.print("1111111111111");
    var input_param = {};
    input_param["viewFlag"] = "OpenAccount";
    kony.print("2222222222");
    showLoadingScreen();
    kony.print("3333333333");
    if (gblCCDBCardFlow == "DEBIT_CARD_REISSUE") {
        invokeServiceSecureAsync("MyProfileViewCompositeService", input_param, callBackDebitCardContactAddress);
    } else {
        invokeServiceSecureAsync("MyProfileViewCompositeService", input_param, callBackOpenAccountSavingsCareContacts);
    }
}

function callBackOpenAccountSavingsCareContacts(status, resulttable) {
    if (status == 400) //success response
    {
        //dismissLoadingScreenPopup();
        if (resulttable["opstatus"] == 0) {
            kony.print("inside callBackOpenAccountSavingsCareContacts------->44444444");
            gblOfficeAddrOpenAc = "";
            gblPartyInqOARes = "";
            gblPartyInqOARes = resulttable;
            if (resulttable["emailAddr"] != null) {
                gblEmailAddr = resulttable["emailAddr"];
                gblEmailAddrOld = gblEmailAddr;
                frmMBSavingsCareContactInfo.lblEmailAddr1.text = gblEmailAddr;
            }
            kony.print("55555555555555");
            for (var i = 0; i < resulttable["ContactNums"].length; i++) {
                var PhnType = resulttable["ContactNums"][i]["PhnType"];
                if (PhnType != null && PhnType != "" && resulttable["ContactNums"][i]["PhnNum"] != undefined) {
                    if (PhnType == "Mobile") {
                        if ((resulttable["ContactNums"][i]["PhnNum"] != null) && (resulttable["ContactNums"][i]["PhnNum"] != "" && resulttable["ContactNums"][i]["PhnNum"] != undefined)) {
                            gblPHONENUMBER = resulttable["ContactNums"][i]["PhnNum"];
                            gblPHONENUMBEROld = gblPHONENUMBER;
                            GblMobileAL = HidePhnNum(gblPHONENUMBER);
                            frmMBSavingsCareContactInfo.lblMobile.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
                        }
                    }
                }
            }
            kony.print("6666666666666666");
            if (resulttable["Persondata"] != null || resulttable["Persondata"] != undefined) {
                for (var i = 0; i < resulttable["Persondata"].length; i++) {
                    var addressType = resulttable["Persondata"][i]["AddrType"];
                    if (resulttable["Persondata"][i]["addr3"] != null || resulttable["Persondata"][i]["addr3"] != "" || resulttable["Persondata"][i]["addr3"] != undefined) {
                        var adr3 = resulttable["Persondata"][i]["addr3"];
                        var reg = / {1,}/;
                        var tempArr = [];
                        tempArr = adr3.split(reg);
                        var subDistBan = kony.i18n.getLocalizedString("gblsubDtPrefixThaiB");
                        var subDistNotBan = kony.i18n.getLocalizedString("gblsubDtPrefixThai") + ".";
                        var distBan = kony.i18n.getLocalizedString("gblDistPrefixThaiB");
                        var distNotBan = kony.i18n.getLocalizedString("gblDistPrefixThai") + ".";
                        if (addressType == "Primary") {
                            if (tempArr[0] != null && tempArr[1] != null && tempArr[0] != "" && tempArr[1] != "" && tempArr[0] != undefined && tempArr[1] != undefined) {
                                if (resulttable["Persondata"][i]["City"] != null && resulttable["Persondata"][i]["City"] != "" && resulttable["Persondata"][i]["City"] != undefined) {
                                    if (tempArr[0].indexOf(subDistBan, 0) >= 0) gblsubdistrictValue = tempArr[0].substring(4);
                                    else if (tempArr[0].indexOf(subDistNotBan, 0) >= 0) gblsubdistrictValue = tempArr[0].substring(2);
                                    else gblsubdistrictValue = "";
                                    if (tempArr[1].indexOf(distBan, 0) >= 0) gbldistrictValue = tempArr[1].substring(3);
                                    else if (tempArr[1].indexOf(distNotBan, 0) >= 0) gbldistrictValue = tempArr[1].substring(2);
                                    else gbldistrictValue = "";
                                }
                            } else {
                                gblsubdistrictValue = "";
                                gbldistrictValue = "";
                            }
                            if (tempArr[0] == undefined || tempArr[0] == "" || tempArr[0] == null || tempArr[0] == "undefined") gblViewsubdistrictValue = "";
                            else gblViewsubdistrictValue = tempArr[0];
                            if (tempArr[1] == undefined || tempArr[1] == "" || tempArr[1] == null || tempArr[1] == "undefined") gblViewdistrictValue = "";
                            else gblViewdistrictValue = tempArr[1];
                            if (resulttable["Persondata"][i]["City"] != null && resulttable["Persondata"][i]["City"] != "" && resulttable["Persondata"][i]["City"] != undefined) gblStateValue = resulttable["Persondata"][i]["City"];
                            else gblStateValue = "";
                            if (resulttable["Persondata"][i]["PostalCode"] != null && resulttable["Persondata"][i]["PostalCode"] != "" && resulttable["Persondata"][i]["PostalCode"] != undefined) gblzipcodeValue = resulttable["Persondata"][i]["PostalCode"];
                            else gblzipcodeValue = resulttable["Persondata"][i]["PostalCode"];
                            if (resulttable["Persondata"][i]["CountryCodeValue"] != null && resulttable["Persondata"][i]["CountryCodeValue"] != "" && resulttable["Persondata"][i]["CountryCodeValue"] != undefined) gblcountryCode = resulttable["Persondata"][i]["CountryCodeValue"];
                            else gblcountryCode = "";
                            if (resulttable["Persondata"][i]["CountryCodeValue"] != kony.i18n.getLocalizedString("Thailand")) {
                                gblnotcountry = true;
                            } else {
                                gblnotcountry = false;
                            }
                            gblAddress1Value = resulttable["Persondata"][i]["addr1"];
                            gblAddress2Value = resulttable["Persondata"][i]["addr2"];
                            frmMBSavingsCareContactInfo.lblContactAddr1.text = gblAddress1Value + " " + gblAddress2Value + " " + gblViewsubdistrictValue + " " + gblViewdistrictValue + " " + gblStateValue + " " + gblzipcodeValue + " " + gblcountryCode;
                            kony.print("7777777777777777");
                        } else if (addressType == "Registered") {
                            //alert("registered address..."); 
                            if (resulttable["Persondata"][i]["AddrType"] != null && resulttable["Persondata"][i]["AddrType"] != "" && resulttable["Persondata"][i]["AddrType"] != undefined) {
                                if (resulttable["Persondata"][i]["addr3"] != null || resulttable["Persondata"][i]["addr3"] != "" || resulttable["Persondata"][i]["addr3"] != undefined) {
                                    var adr3 = resulttable["Persondata"][i]["addr3"];
                                    //alert(adr3);
                                    var reg = / {1,}/;
                                    var tempArr = [];
                                    tempArr = adr3.split(reg);
                                    //if (resulttable["Persondata"][i]["City"] != null || resulttable["Persondata"][i]["City"] != "") 
                                    {
                                        if (tempArr[0] != null && tempArr[1] != null && tempArr[0] != "" && tempArr[1] != "" && tempArr[0] != undefined && tempArr[1] != undefined) {
                                            if (resulttable["Persondata"][i]["City"] != "" && resulttable["Persondata"][i]["City"] != null && resulttable["Persondata"][i]["City"] != undefined && resulttable["Persondata"][i]["City"] == kony.i18n.getLocalizedString("BangkokThaiValueProfile")) {
                                                gblregsubdistrictValue = tempArr[0] //.substring(3);
                                                gblregdistrictValue = tempArr[1] //.substring(3);
                                            } else {
                                                gblregsubdistrictValue = tempArr[0] //.substring(2);
                                                gblregdistrictValue = tempArr[1] //.substring(2);
                                            }
                                        }
                                    }
                                    gblregAddress1Value = resulttable["Persondata"][i]["addr1"];
                                    gblregAddress2Value = resulttable["Persondata"][i]["addr2"];
                                    if (resulttable["Persondata"][i]["City"] != null || resulttable["Persondata"][i]["City"] != "" || resulttable["Persondata"][i]["City"] != undefined) {
                                        gblregStateValue = resulttable["Persondata"][i]["City"];
                                    } else {
                                        gblregStateValue = "";
                                    }
                                    //gblregStateValue = resulttable["Persondata"][i]["City"];
                                    if (resulttable["Persondata"][i]["PostalCode"] != "" && resulttable["Persondata"][i]["PostalCode"] != null && resulttable["Persondata"][i]["PostalCode"] != undefined) gblregzipcodeValue = resulttable["Persondata"][i]["PostalCode"];
                                    else gblregzipcodeValue = "";
                                    if (resulttable["Persondata"][i]["CountryCodeValue"] != "" && resulttable["Persondata"][i]["CountryCodeValue"] != null && resulttable["Persondata"][i]["CountryCodeValue"] != undefined) gblregcountryCodeIB = resulttable["Persondata"][i]["CountryCodeValue"];
                                    else gblregcountryCodeIB = "";
                                    frmMBSavingsCareContactInfo.lblRegAddr.text = gblregAddress1Value + " " + gblregAddress2Value + " " + gblregsubdistrictValue + " " + gblregdistrictValue + " " + " " + gblregStateValue + " " + " " + gblregzipcodeValue + " " + " " + gblregcountryCode;
                                }
                            }
                            kony.print("888888888888888");
                        } else if (addressType == "Office") {
                            if (resulttable["Persondata"][i]["AddrType"] != null && resulttable["Persondata"][i]["AddrType"] != "" && resulttable["Persondata"][i]["AddrType"] != undefined) {
                                if (resulttable["Persondata"][i]["addr3"] != null || resulttable["Persondata"][i]["addr3"] != "" || resulttable["Persondata"][i]["addr3"] != undefined) {
                                    var adr3 = resulttable["Persondata"][i]["addr3"];
                                    var reg = / {1,}/;
                                    var tempArr = [];
                                    tempArr = adr3.split(reg);
                                    var gblOffsubdistrictValue = "";
                                    var gblOffdistrictValue = "";
                                    var gblOffAddress1Value = "";
                                    var gblOffAddress2Value = "";
                                    var gblOffStateValue = "";
                                    var gblOffzipcodeValue = "";
                                    var gblOffcountryCode = "";
                                    //if (resulttable["Persondata"][i]["City"] != null || resulttable["Persondata"][i]["City"] != "") 
                                    {
                                        if (tempArr[0] != null && tempArr[1] != null && tempArr[0] != "" && tempArr[1] != "" && tempArr[0] != undefined && tempArr[1] != undefined) {
                                            if (resulttable["Persondata"][i]["City"] != "" && resulttable["Persondata"][i]["City"] != null && resulttable["Persondata"][i]["City"] != undefined && resulttable["Persondata"][i]["City"] == kony.i18n.getLocalizedString("BangkokThaiValueProfile")) {
                                                gblOffsubdistrictValue = tempArr[0] //.substring(3);
                                                gblOffdistrictValue = tempArr[1] //.substring(3);
                                            } else {
                                                gblOffsubdistrictValue = tempArr[0] //.substring(2);
                                                gblOffdistrictValue = tempArr[1] //.substring(2);
                                            }
                                        }
                                    }
                                    gblOffAddress1Value = resulttable["Persondata"][i]["addr1"];
                                    gblOffAddress2Value = resulttable["Persondata"][i]["addr2"];
                                    if (resulttable["Persondata"][i]["City"] != null || resulttable["Persondata"][i]["City"] != "" || resulttable["Persondata"][i]["City"] != undefined) {
                                        gblOffStateValue = resulttable["Persondata"][i]["City"];
                                    } else {
                                        gblOffStateValue = "";
                                    }
                                    //gblregStateValue = resulttable["Persondata"][i]["City"];
                                    if (resulttable["Persondata"][i]["PostalCode"] != "" && resulttable["Persondata"][i]["PostalCode"] != null && resulttable["Persondata"][i]["PostalCode"] != undefined) gblOffzipcodeValue = resulttable["Persondata"][i]["PostalCode"];
                                    else gblOffzipcodeValue = "";
                                    if (resulttable["Persondata"][i]["CountryCodeValue"] != "" && resulttable["Persondata"][i]["CountryCodeValue"] != null && resulttable["Persondata"][i]["CountryCodeValue"] != undefined) gblOffcountryCode = resulttable["Persondata"][i]["CountryCodeValue"];
                                    else gblOffcountryCode = "";
                                    gblOfficeAddrOpenAc = gblOffAddress1Value + " " + gblOffAddress2Value + " " + gblOffsubdistrictValue + " " + gblOffdistrictValue + " " + " " + gblOffStateValue + " " + " " + gblOffzipcodeValue + " " + " " + gblOffcountryCode;
                                }
                            }
                        }
                    }
                    kony.print("999999999999999999");
                    if (gblPartyInqOARes["customerNameTH"] != null && gblPartyInqOARes["customerNameTH"] != "" && gblPartyInqOARes["customerNameTH"] != undefined) {
                        frmMBSavingsCareContactInfo.lblName1.text = gblPartyInqOARes["customerNameTH"];
                    } else {
                        frmMBSavingsCareContactInfo.lblName1.text = "-";
                    }
                    if (gblPartyInqOARes["customerName"] != null && gblPartyInqOARes["customerName"] != "" && gblPartyInqOARes["customerName"] != undefined) {
                        frmMBSavingsCareContactInfo.lblengname.text = gblPartyInqOARes["customerName"];
                    } else {
                        frmMBSavingsCareContactInfo.lblengname.text = "-";
                    }
                    if (gblPartyInqOARes["emailAddr"] != null && gblPartyInqOARes["emailAddr"] != "" && gblPartyInqOARes["emailAddr"] != undefined) {
                        frmMBSavingsCareContactInfo.lblEmailAddr1.text = gblPartyInqOARes["emailAddr"];
                    } else {
                        frmMBSavingsCareContactInfo.lblEmailAddr1.text = "-";
                    }
                    kony.print("s2sBusinessHrsFlag in callBackOpenAccountContacts" + gblPartyInqOARes["s2sBusinessHrsFlag"]);
                    if (gblPartyInqOARes["s2sBusinessHrsFlag"] != null && gblPartyInqOARes["s2sBusinessHrsFlag"] != "" && gblPartyInqOARes["s2sBusinessHrsFlag"] != undefined) {
                        gblOpenActBusinessHrs = gblPartyInqOARes["s2sBusinessHrsFlag"];
                    }
                    if (gblPartyInqOARes["s2sStartTime"] != null && gblPartyInqOARes["s2sStartTime"] != "" && gblPartyInqOARes["s2sStartTime"] != undefined) {
                        s2sStartTime = gblPartyInqOARes["s2sStartTime"];
                    }
                    if (gblPartyInqOARes["s2sEndTime"] != null && gblPartyInqOARes["s2sEndTime"] != "" && gblPartyInqOARes["s2sEndTime"] != undefined) {
                        s2sEndTime = gblPartyInqOARes["s2sEndTime"];
                    }
                    kony.print("1010101010101010101");
                    //dismissLoadingScreen();
                    frmMBSavingsCareContactInfo.show();
                }
            }
        }
    }
}

function OnclickShowSavingsCareOccupationInfo() {
    //alert(gblPartyInqOARes["OccupationEN"]);
    kony.print("Inside OnclickShowSavingsCareOccupationInfo");
    if (gblPartyInqOARes["OccupationEN"] != null && gblPartyInqOARes["OccupationEN"] != "" && gblPartyInqOARes["OccupationEN"] != undefined) {
        frmMBSavingsCareOccupationInfo.lblName1.text = gblPartyInqOARes["OccupationEN"];
    } else {
        frmMBSavingsCareOccupationInfo.lblName1.text = "-";
    }
    if (gblPartyInqOARes["CompanyName"] != null && gblPartyInqOARes["CompanyName"] != "" && gblPartyInqOARes["CompanyName"] != undefined) {
        frmMBSavingsCareOccupationInfo.officename.text = gblPartyInqOARes["CompanyName"];
    } else {
        frmMBSavingsCareOccupationInfo.officename.text = "-";
    }
    if (gblOfficeAddrOpenAc != null && gblOfficeAddrOpenAc != "" && gblOfficeAddrOpenAc != undefined) {
        frmMBSavingsCareOccupationInfo.lblRegAddr.text = gblOfficeAddrOpenAc;
    } else {
        frmMBSavingsCareOccupationInfo.lblRegAddr.text = "-";
    }
    if (gblPartyInqOARes["SourceOfIncomeEN"] != null && gblPartyInqOARes["SourceOfIncomeEN"] != "" && gblPartyInqOARes["SourceOfIncomeEN"] != undefined) {
        frmMBSavingsCareOccupationInfo.incomesource.text = gblPartyInqOARes["SourceOfIncomeEN"];
    } else {
        frmMBSavingsCareOccupationInfo.incomesource.text = "-";
    }
    if (gblPartyInqOARes["CountryOfIncomeEN"] != null && gblPartyInqOARes["CountryOfIncomeEN"] != "" && gblPartyInqOARes["CountryOfIncomeEN"] != undefined) {
        frmMBSavingsCareOccupationInfo.countryincome.text = gblPartyInqOARes["CountryOfIncomeEN"];
    } else {
        frmMBSavingsCareOccupationInfo.countryincome.text = "-";
    }
    kony.print("Before frmMBSavingsCareOccupationInfo show()");
    frmMBSavingsCareOccupationInfo.show();
}

function frmMBSavingsCareOccupationInfoPreShow() {
    changeStatusBarColor();
    kony.print("Inside frmMBSavingsCareOccupationInfoPreShow()");
    frmMBSavingsCareOccupationInfo.lblHdrTxt.text = kony.i18n.getLocalizedString("Kyc_ttl");
    frmMBSavingsCareOccupationInfo.lblOccupationinfo.text = kony.i18n.getLocalizedString("Kyc_lbl_occupationInfo");
    frmMBSavingsCareOccupationInfo.lblOccupationName.text = kony.i18n.getLocalizedString("Kyc_lbl_occupation");
    frmMBSavingsCareOccupationInfo.lblOfficeName.text = kony.i18n.getLocalizedString("Kyc_lbl_officename");
    frmMBSavingsCareOccupationInfo.lblOfficeAddr.text = kony.i18n.getLocalizedString("Kyc_lbl_officeaddress");
    frmMBSavingsCareOccupationInfo.lblincomesource.text = kony.i18n.getLocalizedString("Kyc_lbl_sourceofIncome");
    frmMBSavingsCareOccupationInfo.lblcountryincome.text = kony.i18n.getLocalizedString("Kyc_lbl_countryofIncome");
    frmMBSavingsCareOccupationInfo.btnconfirm.text = kony.i18n.getLocalizedString("keyConfirm");
    frmMBSavingsCareOccupationInfo.btnback.text = kony.i18n.getLocalizedString("Back");
    frmMBSavingsCareOccupationInfo.richtextfooterNote.text = kony.i18n.getLocalizedString("Kyc_lbl_occupdescNew");
    if (locale == "en_US") {
        if (gblPartyInqOARes["OccupationEN"] != null && gblPartyInqOARes["OccupationEN"] != "" && gblPartyInqOARes["OccupationEN"] != undefined) {
            frmMBSavingsCareOccupationInfo.lblName1.text = gblPartyInqOARes["OccupationEN"];
        } else {
            frmMBSavingsCareOccupationInfo.lblName1.text = "-";
        }
        if (gblPartyInqOARes["CompanyName"] != null && gblPartyInqOARes["CompanyName"] != "" && gblPartyInqOARes["CompanyName"] != undefined) {
            frmMBSavingsCareOccupationInfo.officename.text = gblPartyInqOARes["CompanyName"];
        } else {
            frmMBSavingsCareOccupationInfo.officename.text = "-";
        }
        if (gblOfficeAddrOpenAc != null && gblOfficeAddrOpenAc != "" && gblOfficeAddrOpenAc != undefined) {
            frmMBSavingsCareOccupationInfo.lblRegAddr.text = gblOfficeAddrOpenAc;
        } else {
            frmMBSavingsCareOccupationInfo.lblRegAddr.text = "-";
        }
        if (gblPartyInqOARes["SourceOfIncomeEN"] != null && gblPartyInqOARes["SourceOfIncomeEN"] != "" && gblPartyInqOARes["SourceOfIncomeEN"] != undefined) {
            frmMBSavingsCareOccupationInfo.incomesource.text = gblPartyInqOARes["SourceOfIncomeEN"];
        } else {
            frmMBSavingsCareOccupationInfo.incomesource.text = "-";
        }
        if (gblPartyInqOARes["CountryOfIncomeEN"] != null && gblPartyInqOARes["CountryOfIncomeEN"] != "" && gblPartyInqOARes["CountryOfIncomeEN"] != undefined) {
            frmMBSavingsCareOccupationInfo.countryincome.text = gblPartyInqOARes["CountryOfIncomeEN"];
        } else {
            frmMBSavingsCareOccupationInfo.countryincome.text = "-";
        }
    } else {
        if (gblPartyInqOARes["OccupationTH"] != null && gblPartyInqOARes["OccupationTH"] != "" && gblPartyInqOARes["OccupationTH"] != undefined) {
            frmMBSavingsCareOccupationInfo.lblName1.text = gblPartyInqOARes["OccupationTH"];
        } else {
            frmMBSavingsCareOccupationInfo.lblName1.text = "-";
        }
        if (gblPartyInqOARes["CompanyName"] != null && gblPartyInqOARes["CompanyName"] != "" && gblPartyInqOARes["CompanyName"] != undefined) {
            frmMBSavingsCareOccupationInfo.officename.text = gblPartyInqOARes["CompanyName"];
        } else {
            frmMBSavingsCareOccupationInfo.officename.text = "-";
        }
        if (gblOfficeAddrOpenAc != null && gblOfficeAddrOpenAc != "" && gblOfficeAddrOpenAc != undefined) {
            frmMBSavingsCareOccupationInfo.lblRegAddr.text = gblOfficeAddrOpenAc;
        } else {
            frmMBSavingsCareOccupationInfo.lblRegAddr.text = "-";
        }
        if (gblPartyInqOARes["SourceOfIncomeTH"] != null && gblPartyInqOARes["SourceOfIncomeTH"] != "" && gblPartyInqOARes["SourceOfIncomeTH"] != undefined) {
            frmMBSavingsCareOccupationInfo.incomesource.text = gblPartyInqOARes["SourceOfIncomeTH"];
        } else {
            frmMBSavingsCareOccupationInfo.incomesource.text = "-";
        }
        if (gblPartyInqOARes["CountryOfIncomeTH"] != null && gblPartyInqOARes["CountryOfIncomeTH"] != "" && gblPartyInqOARes["CountryOfIncomeTH"] != undefined) {
            frmMBSavingsCareOccupationInfo.countryincome.text = gblPartyInqOARes["CountryOfIncomeTH"];
        } else {
            frmMBSavingsCareOccupationInfo.countryincome.text = "-";
        }
    }
    kony.print("End frmMBSavingsCareOccupationInfoPreShow()");
}

function showSavingsCareAddAccount() {
    setSavingCareAddAccountData();
}

function setSavingCareAddAccountData() {
    var ifAccounts = true;
    dsSelActSelIndex = [0, 0];
    dsSelActLength = 0;
    if (flowSpa) {
        ifAccounts = setDataforOpenFromActs(frmMBSavingsCareAddBal.segNSSlider);
        if (ifAccounts == false) {
            return false;
        }
    } else {
        ifAccounts = setDataforOpenFromActs(frmMBSavingsCareAddBal.segNSSlider);
        if (ifAccounts == false) {
            return false;
        }
        frmMBSavingsCareAddBal.segNSSlider.selectedIndex = dsSelActSelIndex;
        dsSelActLength = frmMBSavingsCareAddBal.segNSSlider.data.length - 1;
    }
    if (gblMinOpenAmt != null && gblMinOpenAmt != "") {
        frmMBSavingsCareAddBal.txtAmountVal.text = fixedToTwoDecimal(gblMinOpenAmt);
    } else {
        kony.print("Logging -- Minimum amount is empty check it");
    }
    frmMBSavingsCareAddBal.show();
}

function onClickAddAccountNext() {
    frmMBSavingsCareAddNickName.show();
}

function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}

function swipeBeneficiariesNickname() {
    var count = 0;
    var setSwipe = {
        fingers: 1,
        swipedistance: 50,
        swipevelocity: 75
    };
    frmMBSavingsCareAddNickName.flxBenefetiariesInner.addGestureRecognizer(2, setSwipe, mySwipeCallbackNickname);
    frmMBSavingsCareAddNickName.flexBenefnner2.addGestureRecognizer(2, setSwipe, mySwipeCallbackBenif2);
    //frmMBSavingsCareAddNickName.flexBenefnner3.addGestureRecognizer(2, setSwipe, mySwipeCallbackNickname);
    //frmMBSavingsCareAddNickName.flexBenefnner4.addGestureRecognizer(2, setSwipe, mySwipeCallbackNickname);
    //frmMBSavingsCareAddNickName.flexBenefnner5.addGestureRecognizer(2, setSwipe, mySwipeCallbackNickname);
}

function mySwipeCallbackNickname() {
    frmMBSavingsCareAddNickName.flxBenefitiariesList1.forceLayout();
    if (frmMBSavingsCareAddNickName.flxBenefetiariesInner.left == "0.0%") {
        frmMBSavingsCareAddNickName.flxBenefetiariesInner.left = "-30%";
    } else {
        frmMBSavingsCareAddNickName.flxBenefetiariesInner.left = "0.0%";
    }
}

function mySwipeCallbackBenif2() {
    frmMBSavingsCareAddNickName.flxBenefitiariesList2.forceLayout();
    if (frmMBSavingsCareAddNickName.flexBenefnner2.left == "0.0%") {
        frmMBSavingsCareAddNickName.flexBenefnner2.left = "-30%";
    } else {
        frmMBSavingsCareAddNickName.flexBenefnner2.left = "0.0%";
    }
}

function onClickEdit() {
    //alert("Show Edit");
    frmMBSavingsCareEditBeneficiary.show();
}

function onClickDelete() {
    alert("Are you sure you want to delete");
}

function onBackCallsShowAddBenef() {
    frmMBSavingsCareAddNickName.vboxEditDelete.setVisibility(false);
    frmMBSavingsCareAddNickName.show();
}

function preshowfrmSCConfirmationold() {
    var specify = true;
    frmMBSavingsCareConfirmation.lblHdrTxt.text = kony.i18n.getLocalizedString("keyConfirmation");
    frmMBSavingsCareConfirmation.lblsubHeader.text = removeColonFromEnd(kony.i18n.getLocalizedString("savingCareActInfo"));
    frmMBSavingsCareConfirmation.btnBack.text = kony.i18n.getLocalizedString("Back");
    frmMBSavingsCareConfirmation.btnNext.text = kony.i18n.getLocalizedString("keyConfirm");
    if (flow == "myAccount") {
        frmMBSavingsCareConfirmation.flxAccuntName.setVisibility(false);
        frmMBSavingsCareConfirmation.imgHeader.setVisibility(false);
        frmMBSavingsCareConfirmation.flxBranch.setVisibility(false);
        frmMBSavingsCareConfirmation.flxDeductAmt.setVisibility(false);
        frmMBSavingsCareConfirmation.flxOpeningAmt.setVisibility(false);
        frmMBSavingsCareConfirmation.flxIntRate.setVisibility(false);
        frmMBSavingsCareConfirmation.flxOpeningDate.setVisibility(false);
        frmMBSavingsCareConfirmation.flxRefNo.setVisibility(false);
    } else {
        frmMBSavingsCareConfirmation.lblnickname.text = removeColonFromEnd(kony.i18n.getLocalizedString("keyNickname"));
        frmMBSavingsCareConfirmation.lblAccountname.text = removeColonFromEnd(kony.i18n.getLocalizedString("AccountName"));
        frmMBSavingsCareConfirmation.lblbranch.text = removeColonFromEnd(kony.i18n.getLocalizedString("Branch"));
        frmMBSavingsCareConfirmation.lblOpeningAmt.text = removeColonFromEnd(kony.i18n.getLocalizedString("keyLblOpnAmount"));
        frmMBSavingsCareConfirmation.lblDeductAmt.text = removeColonFromEnd(kony.i18n.getLocalizedString("deduct_amt"));
        frmMBSavingsCareConfirmation.lblIntRate.text = removeColonFromEnd(kony.i18n.getLocalizedString("Interest_rate_Annual"));
        frmMBSavingsCareConfirmation.lblOpeningDate.text = removeColonFromEnd(kony.i18n.getLocalizedString("keyLblOpeningDate"));
        frmMBSavingsCareConfirmation.lblRefNo.text = removeColonFromEnd(kony.i18n.getLocalizedString("keyTransactionRefNumber"));
    }
    if (specify == true) {
        frmMBSavingsCareConfirmation.flexContainer761510865397604.setVisibility(true);
        frmMBSavingsCareConfirmation.segBeneficiaryList.setVisibility(true);
        frmMBSavingsCareConfirmation.flexContainer761510865399016.setVisibility(true);
        frmMBSavingsCareConfirmation.richtextnonSpecifyDis.setVisibility(false);
        frmMBSavingsCareConfirmation.lblname.text = removeColonFromEnd(kony.i18n.getLocalizedString("name"));
        frmMBSavingsCareConfirmation.lblrelation.text = removeColonFromEnd(kony.i18n.getLocalizedString("keyRelation"));
        frmMBSavingsCareConfirmation.lblbenefit.text = removeColonFromEnd(kony.i18n.getLocalizedString("benefitKey"));
        setSegdata();
    } else {
        frmMBSavingsCareConfirmation.richtextnonSpecifyDis.setVisibility(true);
        frmMBSavingsCareConfirmation.lblBeneficiary.text = kony.i18n.getLocalizedString("keyBenificiaries") + " : " + kony.i18n.getLocalizedString("keynotSpecify");
        frmMBSavingsCareConfirmation.flexContainer761510865397604.setVisibility(false);
        frmMBSavingsCareConfirmation.segBeneficiaryList.setVisibility(false);
        frmMBSavingsCareConfirmation.flexContainer761510865399016.setVisibility(false);
    }
    //frmMBSavingsCareConfirmation.label52440721546425.text=removeColonFromEnd(kony.i18n.getLocalizedString("name"));
    //frmMBSavingsCareConfirmation.label52440721547521.text=removeColonFromEnd(kony.i18n.getLocalizedString("keyOpenRelation"));
}

function setSegdataold() {
    var fName = "";
    var lName = "";
    var relationShip = "";
    var percentage = "";
    var data = [];
    frmMBSavingsCareConfirmation.segBeneficiaryList.widgetDataMap = {
        "lblnameVal": "lblname",
        "lblrelationVal": "lblrelation",
        "lblBenefitVal": "lblbenefit",
        "lblline": "lineval"
    };
    for (var i = 0; i <= arrayBenificiary.length; i++) {
        fName = arrayBenificiary[i]["firstName"] + " " + arrayBenificiary[i]["lastName"];
        relationShip = arrayBenificiary[i]["relation"];
        percentage = arrayBenificiary[i]["percentage"];
        var benifDataTemp = {
            "lblname": fName,
            "lblrelation": relationShip,
            "lblbenefit": percentage,
            "lineval": "."
        }
        data.push(benifDataTemp);
    }
    frmMBSavingsCareConfirmation.lblBeneficiary.text = kony.i18n.getLocalizedString("keyBenificiaries") + " " + arrayBenificiary.length;
    /*
    var data=[{"lblname": "John Deo",
    			"lblrelation":"Father",
    			"lblbenefit": "20 %",
    			"lineval":"."},
    			{"lblname": "Jason Deo",
    			"lblrelation":"Mother",
    			"lblbenefit": "20 %",
    			"lineval":"."},
    			{"lblname": "Jennifer Deo",
    			"lblrelation":"Spouse",
    			"lblbenefit": "20 %",
    			"lineval":"."},
    			{"lblname": "Jeff Deo",
    			"lblrelation":"Child",
    			"lblbenefit": "20 %",
    			"lineval":"."},
    			{"lblname": "Jack Deo",
    			"lblrelation":"Child",
    			"lblbenefit": "20 %",
    			"lineval":""}];
    	**/
    frmMBSavingsCareConfirmation.segBeneficiaryList.setData(data);
}

function onClickEmailTnCMBSavingsCare() {
    popupConfrmDelete.lblheader.text = kony.i18n.getLocalizedString("TncInfoHeader");
    popupConfrmDelete.btnpopConfDelete.text = kony.i18n.getLocalizedString("keyOK");
    popupConfrmDelete.btnPopupConfCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
    popupConfrmDelete.btnpopConfDelete.onClick = onClickPopUpOk;
    //popupConfrmDelete.btnPopupConfCancel.text=popupConfrmDelete.dismiss();
    popupConfrmDelete.lblPopupConfText.text = kony.i18n.getLocalizedString("keytermOpenAcnt");
    popupConfrmDelete.show();
}

function onClickPopUpOk() {
    popupConfrmDelete.dismiss();
    if (flow == "myAccount") {
        emailTnCMBSavingsCare();
    } else {
        passingProdDescTnCEmailMB();
        onClickEmailTnCMBOpenAccount(prodDescTnC, "Mobile Banking", "02")
    }
}

function backTnCForm() {
    if (gblSavingsCareFlow == "MyAccountFlow") {
        frmMyAccountView.show();
    } else if (gblSavingsCareFlow == "AccountDetailsFlow") {
        frmAccountDetailsMB.show();
    } else {
        frmMBSavingsCareProdBrief.show();
    }
}
//call notification add service for email with the tnc keyword
function emailTnCMBSavingsCare() {
    //alert(kony.i18n.getLocalizedString("keyTermsAndConditionsPopUp"));
    popupConfrmDelete.dismiss();
    showLoadingScreen()
    var inputparam = {};
    inputparam["channelName"] = "Mobile Banking";
    inputparam["channelID"] = "02";
    inputparam["notificationType"] = "Email"; // always email
    inputparam["phoneNumber"] = gblPHONENUMBER;
    inputparam["mail"] = gblEmailId;
    inputparam["customerName"] = gblCustomerName;
    inputparam["localeCd"] = kony.i18n.getCurrentLocale();
    //inputparam["moduleKey"] = "TMBSavingCare";
    inputparam["moduleKey"] = "OpenAccount";
    inputparam["fileNameOpen"] = "TMBSavingCare";
    if (gblSavingsCareFlow == "MyAccountFlow") {
        inputparam["productName"] = frmMyAccountList.segTMBAccntDetails.selectedItems[0].hiddenProductNameEng
        inputparam["productNameTH"] = frmMyAccountList.segTMBAccntDetails.selectedItems[0].hiddenProductNameThai
        inputparam["prodCode"] = frmMyAccountList.segTMBAccntDetails.selectedItems[0].productID
    } else {
        inputparam["productName"] = gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"]
        inputparam["productNameTH"] = gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"]
        inputparam["prodCode"] = gblAccountTable["custAcctRec"][gblIndex]["productID"]
    }
    //inputparam["productName"] = frmOpenActSelProd.segOpenActSelProd.selectedItems[0].hiddenProductNameEng;
    //inputparam["productNameTH"] = frmOpenActSelProd.segOpenActSelProd.selectedItems[0].hiddenProductNameThai;
    //inputparam["prodCode"] = frmOpenActSelProd.segOpenActSelProd.selectedItems[0].productID;
    invokeServiceSecureAsync("TCEMailService", inputparam, callBackEmailTnCSavingCare);
}

function callBackEmailTnCSavingCare(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
            var StatusCode = result["StatusCode"];
            var Severity = result["Severity"];
            var StatusDesc = result["StatusDesc"];
            if (StatusCode == 0) {
                kony.print("Successfully email sent");
                //popupConfrmDelete.dismiss();
                //showAlertWithtwoBtn(kony.i18n.getLocalizedString("keytermOpenAcnt"), kony.i18n.getLocalizedString("info"));
                dismissLoadingScreen();
            } else {
                dismissLoadingScreen()
                return false;
            }
        } else {
            dismissLoadingScreen()
        }
    }
}

function showAlertSC(keyMsg, KeyTitle) {
    var okk = kony.i18n.getLocalizedString("keyOK");
    var cancel = ""
        //Defining basicConf parameter for alert
    var basicConf = {
        message: keyMsg,
        alertType: constants.ALERT_TYPE_INFO,
        alertTitle: KeyTitle,
        yesLabel: okk,
        noLabel: cancel,
        alertHandler: handle2
    };
    //Defining pspConf parameter for alert
    var pspConf = {};
    //Alert definition
    var infoAlert = kony.ui.Alert(basicConf, pspConf);

    function handle2(response) {
        //if(response==)
        frmMBSavingsCareAddBeneficiary.txtfirstName.setFocus(true);
    }
    return;
}

function preshowfrmRelationship() {
    var data = [{
        "lblrelation": kony.i18n.getLocalizedString("keyFather"),
        "lblline": ".",
        "imgtick": "",
        "flxRow": {
            skin: flexWhiteBG
        }
    }, {
        "lblrelation": kony.i18n.getLocalizedString("keyMother"),
        "lblline": ".",
        "imgtick": "",
        "flxRow": {
            skin: flexWhiteBG
        }
    }, {
        "lblrelation": kony.i18n.getLocalizedString("keySpouseReg"),
        "lblline": ".",
        "imgtick": "",
        "flxRow": {
            skin: flexWhiteBG
        }
    }, {
        "lblrelation": kony.i18n.getLocalizedString("keySpouseNL"),
        "lblline": ".",
        "imgtick": "",
        "flxRow": {
            skin: flexWhiteBG
        }
    }, {
        "lblrelation": kony.i18n.getLocalizedString("keyChild"),
        "lblline": ".",
        "imgtick": "",
        "flxRow": {
            skin: flexWhiteBG
        }
    }, {
        "lblrelation": kony.i18n.getLocalizedString("keySCRelative"),
        "lblline": ".",
        "imgtick": "",
        "flxRow": {
            skin: flexWhiteBG
        }
    }, {
        "lblrelation": kony.i18n.getLocalizedString("keyOtherRelation"),
        "lblline": ".",
        "imgtick": "",
        "flxRow": {
            skin: flexWhiteBG
        }
    }];
    frmMBSavingsCareRelationshipBeneficiary.segRelationList.setData(data);
    //frmMBSavingsCareRelationshipBeneficiary.img1.setVisibility(false);
    //      frmMBSavingsCareRelationshipBeneficiary.img2.setVisibility(false);
    //      frmMBSavingsCareRelationshipBeneficiary.img3.setVisibility(false);
    //      frmMBSavingsCareRelationshipBeneficiary.img4.setVisibility(false);
    //      frmMBSavingsCareRelationshipBeneficiary.img5.setVisibility(false);
    //      frmMBSavingsCareRelationshipBeneficiary.img6.setVisibility(false);
    //      frmMBSavingsCareRelationshipBeneficiary.img7.setVisibility(false);
    //      frmMBSavingsCareRelationshipBeneficiary.flxFather.skin="flexWhiteBG";
    //      frmMBSavingsCareRelationshipBeneficiary.flxMother.skin="flexWhiteBG";
    //      frmMBSavingsCareRelationshipBeneficiary.flxSpouseReg.skin="flexWhiteBG";
    //      frmMBSavingsCareRelationshipBeneficiary.flxSpousenoList.skin="flexWhiteBG";
    //      frmMBSavingsCareRelationshipBeneficiary.flxChild.skin="flexWhiteBG";
    //      frmMBSavingsCareRelationshipBeneficiary.flxRelative.skin="flexWhiteBG";
    //      frmMBSavingsCareRelationshipBeneficiary.flxOther.skin="flexWhiteBG";
    //      frmMBSavingsCareRelationshipBeneficiary.lblfather.text=kony.i18n.getLocalizedString("keyFather");
    //      frmMBSavingsCareRelationshipBeneficiary.lblmother.text=kony.i18n.getLocalizedString("keyMother");
    //      frmMBSavingsCareRelationshipBeneficiary.lblspousereg.text=kony.i18n.getLocalizedString("keySpouseReg");
    //      frmMBSavingsCareRelationshipBeneficiary.lblspouseNL.text=kony.i18n.getLocalizedString("keySpouseNL");
    //      frmMBSavingsCareRelationshipBeneficiary.lblchild.text=kony.i18n.getLocalizedString("keyChild");
    //      frmMBSavingsCareRelationshipBeneficiary.lblrelative.text=kony.i18n.getLocalizedString("keySCRelative");
    //      frmMBSavingsCareRelationshipBeneficiary.lblother.text=kony.i18n.getLocalizedString("keyOtherRelation");
    frmMBSavingsCareRelationshipBeneficiary.Label09f769a45499649.text = kony.i18n.getLocalizedString("keyOpenRelation");
}

function backRelationship() {
    frmMBSavingsCareAddBeneficiary.show();
}

function showDeletepopSC() {
    popupConfrmDelete.lblheader.text = kony.i18n.getLocalizedString("delete_benefi");
    popupConfrmDelete.lblPopupConfText.text = kony.i18n.getLocalizedString("deleteBeniftxt");
    popupConfrmDelete.btnpopConfDelete.text = kony.i18n.getLocalizedString("keyDeletebenefit");
    popupConfrmDelete.btnPopupConfCancel.text = kony.i18n.getLocalizedString("keyCancelbenefit");
    popupConfrmDelete.btnpopConfDelete.onClick = deleteBenificiarybyPopUp;
    popupConfrmDelete.show();
}

function deleteBenificiarybyPopUp() {
    arrayBenificiary.splice(editIndex, 1);
    popupConfrmDelete.dismiss();
    showNickNameForm();
}

function onclicksegRelationRow() {
    var selectedIndex = frmMBSavingsCareRelationshipBeneficiary.segRelationList.selectedIndex[1];
    var selectedData = frmMBSavingsCareRelationshipBeneficiary.segRelationList.data[selectedIndex];
    kony.print("row value : " + JSON.stringify(selectedData));
    kony.print("selected value " + selectedData["lblrelation"]);
    kony.print("selected value " + selectedData["imgtick"]);
    gblSelectedRel = selectedData["lblrelation"];
    kony.print("gblSelectedRel value " + gblSelectedRel);
    if (selectedData["imgtick"] == "") {
        selectedData.flxRow.skin = "flexTransBG";
        selectedData["imgtick"] = "tick_icon.png";
        kony.print("selected value " + selectedData["imgtick"]);
        kony.print("selected value " + selectedData["flxRow"]);
    }
    if (accEdit == "true") {
        frmMBSavingsCareEditBeneficiary.lblrelation.text = gblSelectedRel;
        frmMBSavingsCareEditBeneficiary.show();
    } else {
        frmMBSavingsCareAddBeneficiary.lblrelation.text = gblSelectedRel;
        frmMBSavingsCareAddBeneficiary.show();
    }
}

function validateBeneficiary() {
    var fname = frmMBSavingsCareAddBeneficiary.txtfirstName.text.trim();
    var lname = frmMBSavingsCareAddBeneficiary.txtlastName.text.trim();
    var duplicateName = false;
    for (var i = 0; i < arrayBenificiary.length; i++) {
        var tempBenefic = arrayBenificiary[i];
        kony.print("GOWRI fname.toLowerCase()=" + fname.toLowerCase());
        kony.print("GOWRI tempBenefic[firstName].toLowerCase()=" + tempBenefic["firstName"].toLowerCase());
        kony.print("GOWRI lname.toLowerCase()=" + lname.toLowerCase());
        kony.print("GOWRI tempBenefic[lastName].toLowerCase()=" + tempBenefic["lastName"].toLowerCase());
        if (fname.toLowerCase() == tempBenefic["firstName"].toLowerCase() && lname.toLowerCase() == tempBenefic["lastName"].toLowerCase()) {
            duplicateName = true;
            break;
        }
    }
    if (fname == "" || fname == null) {
        showAlertWithCallBack(kony.i18n.getLocalizedString("first_name"), kony.i18n.getLocalizedString("info"), function() {
            frmMBSavingsCareAddBeneficiary.txtfirstName.setFocus(true);
        });
    } else if (!firstNameAlpNumValidation(fname)) {
        showAlertWithCallBack(kony.i18n.getLocalizedString("keyBenFirstNameAlphaNum"), kony.i18n.getLocalizedString("info"), function() {
            frmMBSavingsCareAddBeneficiary.txtfirstName.setFocus(true);
        });
    } else if (lname == "" || lname == null) {
        showAlertWithCallBack(kony.i18n.getLocalizedString("last_name"), kony.i18n.getLocalizedString("info"), function() {
            frmMBSavingsCareAddBeneficiary.txtlastName.setFocus(true);
        });
    } else if (!lastNameAlpNumValidation(lname)) {
        showAlertWithCallBack(kony.i18n.getLocalizedString("keyBenSecondNameAlphNum"), kony.i18n.getLocalizedString("info"), function() {
            frmMBSavingsCareAddBeneficiary.txtfirstName.setFocus(true);
        });
    } else if (duplicateName == true) {
        showAlertWithCallBack(kony.i18n.getLocalizedString("duplicate_beneficiary"), kony.i18n.getLocalizedString("info"), function() {
            frmMBSavingsCareAddBeneficiary.txtfirstName.setFocus(true);
        });
    } else if (gblSelectedRel == "" || gblSelectedRel == null) {
        showAlertWithCallBack(kony.i18n.getLocalizedString("select_relation"), kony.i18n.getLocalizedString("info"), function() {
            frmMBSavingsCareRelationshipBeneficiary.show();
        });
    } else {
        gblSelectedRel = "";
        addBeneficiaryDetails();
    }
}

function validateBeneficiaryEdit() {
    var fname = frmMBSavingsCareEditBeneficiary.txtfirstName.text;
    var lname = frmMBSavingsCareEditBeneficiary.txtlastName.text;
    gblSelectedRel = frmMBSavingsCareEditBeneficiary.lblrelation.text;
    var duplicateName = false;
    kony.print("editIndex  " + editIndex);
    kony.print("arrayBenificiary.length  " + arrayBenificiary.length);
    for (var i = 0; i < arrayBenificiary.length; i++) {
        var tempBenefic = arrayBenificiary[i];
        if (editIndex == i) {
            kony.print("if case");
        } else if (fname.toLowerCase() == tempBenefic["firstName"].toLowerCase() && lname.toLowerCase() == tempBenefic["lastName"].toLowerCase()) {
            kony.print("else case")
            duplicateName = true;
            break;
        }
    }
    if (fname == "" || fname == null) {
        showAlertWithCallBack(kony.i18n.getLocalizedString("first_name"), kony.i18n.getLocalizedString("info"), function() {
            frmMBSavingsCareEditBeneficiary.txtfirstName.setFocus(true);
        });
    } else if (!firstNameAlpNumValidation(fname)) {
        showAlertWithCallBack(kony.i18n.getLocalizedString("keyBenFirstNameAlphaNum"), kony.i18n.getLocalizedString("info"), function() {
            frmMBSavingsCareEditBeneficiary.txtfirstName.setFocus(true);
        });
    } else if (lname == "" || lname == null) {
        showAlertWithCallBack(kony.i18n.getLocalizedString("last_name"), kony.i18n.getLocalizedString("info"), function() {
            frmMBSavingsCareEditBeneficiary.txtlastName.setFocus(true);
        });
    } else if (!lastNameAlpNumValidation(lname)) {
        showAlertWithCallBack(kony.i18n.getLocalizedString("keyBenSecondNameAlphNum"), kony.i18n.getLocalizedString("info"), function() {
            frmMBSavingsCareEditBeneficiary.txtfirstName.setFocus(true);
        });
    } else if (duplicateName == true) {
        showAlertWithCallBack(kony.i18n.getLocalizedString("duplicate_beneficiary"), kony.i18n.getLocalizedString("info"), function() {
            frmMBSavingsCareEditBeneficiary.txtfirstName.setFocus(true);
        });
    } else if (gblSelectedRel == "" || gblSelectedRel == null) {
        showAlertWithCallBack(kony.i18n.getLocalizedString("select_relation"), kony.i18n.getLocalizedString("info"), function() {
            frmMBSavingsCareRelationshipBeneficiary.show();
        });
    } else {
        gblSelectedRel = "";
        saveBeneficiaries();
    }
}

function onclickfirstNameEdit() {
    frmMBSavingsCareEditBeneficiary.txtlastName.setFocus(true);
}

function onclicklastNameEdit() {
    onclickfrmMBSavingsCareAddBeneficiaryRelationLabel();
    //frmMBSavingsCareRelationshipBeneficiary.show();
}

function onclickfirstName() {
    frmMBSavingsCareAddBeneficiary.txtlastName.setFocus(true);
}

function onclicklastName() {
    onclickfrmMBSavingsCareAddBeneficiaryRelationLabel();
    //frmMBSavingsCareRelationshipBeneficiary.show();
}

function preshowfrmMBSavingsCareAddNickName() {
    changeStatusBarColor();
    if (gblFromNickNameBack) {
        if (gblSavingsCareFlow == "MyAccountFlow" || gblSavingsCareFlow == "AccountDetailsFlow" || flow == "myAccount") {
            kony.print("GOWRI in NICKNAME PRE SHOW FOR SC");
            frmMBSavingsCareAddNickName.Label0db8ff91d2a8c44.text = kony.i18n.getLocalizedString("keyMyAccountIB");
            //frmMBSavingsCareAddNickName.lblOccupationinfo.text=kony.i18n.getLocalizedString("keySavingCareAct");
            frmMBSavingsCareAddNickName.imgHeader.setVisibility(false);
            frmMBSavingsCareAddNickName.flxLine1.setVisibility(false);
            frmMBSavingsCareAddNickName.flxAmount.setVisibility(false);
            frmMBSavingsCareAddNickName.txtNickName.placeholder = kony.i18n.getLocalizedString("add_actNickname") //"My Saving Care";
            frmMBSavingsCareAddNickName.txtNickName.placeholderSkin = "txtBlueNormal200";
            frmMBSavingsCareAddNickName.btnBack.text = kony.i18n.getLocalizedString("keyCancelButton");
            frmMBSavingsCareAddNickName.btnNext.text = kony.i18n.getLocalizedString("Next");
            frmMBSavingsCareAddNickName.Label0db8ff91d2a8c44.centerY = "50%";
        } else {
            setlanguageflipforNickName();
        }
    }
}

function onclickfrmMBSavingsCareEditBeneficiaryRelationLabel() {
    //accEdit="true";
    getRelationshipList();
    //frmMBSavingsCareRelationshipBeneficiary.show();
}

function addOtherRel(relationCodeAndDesc) {
    var temprowData = {};
    if (gblSelectedRel != "" && gblSelectedRel == relationCodeAndDesc["99"]) {
        //var relation = frmMBSavingsCareEditBeneficiary.lblrelation.text;
        temprowData = {
            "lblrelation": relationCodeAndDesc["99"],
            "lblline": ".",
            "imgtick": "tick_icon.png",
            "flxRow": {
                skin: flexGreyBG
            }
        };
    } else {
        temprowData = {
            "lblrelation": relationCodeAndDesc["99"],
            "lblline": ".",
            "imgtick": "",
            "flxRow": {
                skin: flexWhiteBG
            }
        };
    }
    return temprowData;
}

function onclickfrmMBSavingsCareAddBeneficiaryRelationLabel() {
    //accEdit="false";
    //getRelationshipList();
    //frmMBSavingsCareRelationshipBeneficiary.show();
    if (accEdit == "true") {
        gblSelectedRel = frmMBSavingsCareEditBeneficiary.lblrelation.text;
    }
    var tempData = [];
    kony.print("gblRelationCodeAndDescEN in onclickfrmMBSavingsCareAddBeneficiaryRelationLabel)()" + JSON.stringify(gblRelationCodeAndDescEN))
    if (getCurrentLocale() == "en_US") {
        var temprowData = {};
        for (var key in gblRelationCodeAndDescEN) {
            temprowData = {};
            if (key != "99") {
                if (gblSelectedRel != "" && gblSelectedRel == gblRelationCodeAndDescEN[key]) {
                    //var relation = frmMBSavingsCareEditBeneficiary.lblrelation.text;
                    temprowData = {
                        "lblrelation": gblRelationCodeAndDescEN[key],
                        "lblline": ".",
                        "imgtick": "tick_icon.png",
                        "flxRow": {
                            skin: flexGreyBG
                        }
                    };
                } else {
                    temprowData = {
                        "lblrelation": gblRelationCodeAndDescEN[key],
                        "lblline": ".",
                        "imgtick": "",
                        "flxRow": {
                            skin: flexWhiteBG
                        }
                    };
                }
                tempData.push(temprowData)
            } //end of if
        } //end of for
        temprowData = addOtherRel(gblRelationCodeAndDescEN);
        tempData.push(temprowData);
    } else {
        var temprowData = {};
        for (var key in gblRelationCodeAndDescTH) {
            temprowData = {};
            if (key != "99") {
                if (gblSelectedRel != "" && gblSelectedRel == gblRelationCodeAndDescTH[key]) {
                    //var relation = frmMBSavingsCareEditBeneficiary.lblrelation.text;
                    var temprowData = {
                        "lblrelation": gblRelationCodeAndDescTH[key],
                        "lblline": ".",
                        "imgtick": "tick_icon.png",
                        "flxRow": {
                            skin: flexGreyBG
                        }
                    };
                } else {
                    temprowData = {
                        "lblrelation": gblRelationCodeAndDescTH[key],
                        "lblline": ".",
                        "imgtick": "",
                        "flxRow": {
                            skin: flexWhiteBG
                        }
                    };
                }
                tempData.push(temprowData)
            } //end of if            
        } //end of for
        temprowData = addOtherRel(gblRelationCodeAndDescTH);
        tempData.push(temprowData);
    }
    frmMBSavingsCareRelationshipBeneficiary.segRelationList.setData(tempData);
    frmMBSavingsCareRelationshipBeneficiary.Label09f769a45499649.text = kony.i18n.getLocalizedString("keyOpenRelation");
    frmMBSavingsCareRelationshipBeneficiary.show();
    dismissLoadingScreen();
}

function getRelationshipList() {
    showLoadingScreen()
    var inputparam = {};
    inputparam["channelId"] = "";
    invokeServiceSecureAsync("getBeneficiaryRelationships", inputparam, callBackRelationshipService);
}

function callBackRelationshipService(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
            var relationListEN = result["relationshipsEN"][0];
            var relationListTH = result["relationshipsTH"][0];
            gblRelationCodeAndDescEN = result["relationshipsEN"][0];
            gblRelationCodeAndDescTH = result["relationshipsTH"][0];
            gblRelationDescAndCodeEN = {};
            gblRelationDescAndCodeTH = {};
            //if(getCurrentLocale()=="en_US"){
            for (var key in relationListEN) {
                /*var temprowData={"lblrelation":relationListEN[key], 
                				 "lblline": ".",
                				 "imgtick":"",
                				 "flxRow":{skin:flexWhiteBG}
                				 };
                				 gblrelationshipsEN[relationListEN[key]]=key;
						
                	tempData.push(temprowData)	*/
                gblRelationDescAndCodeEN[relationListEN[key]] = key;
                gblRelationDescAndCodeTH[relationListTH[key]] = key;
            }
            //}else{
            //for(var key in relationListTH){
            /*var temprowData={"lblrelation":relationListTH[key], 
            				 "lblline": ".",
            				 "imgtick":"",
            				 "flxRow":{skin:flexWhiteBG}
            				 };
            	gblrelationshipsTH[relationListTH[key]]=key;
            	tempData.push(temprowData);	*/
            //gblRelationDescAndCodeTH[relationListTH[key]]=key;		
            //}
            //}	
            //frmMBSavingsCareRelationshipBeneficiary.segRelationList.setData(tempData);
            //frmMBSavingsCareRelationshipBeneficiary.Label09f769a45499649.text=kony.i18n.getLocalizedString("keyOpenRelation");
            //frmMBSavingsCareRelationshipBeneficiary.show();
            //dismissLoadingScreen();
            //} else {
            //				dismissLoadingScreen()
            //				return false;
            //			}
        } else {
            //dismissLoadingScreen()
        }
    }
}

function setlanguageflipforNickName() {
    //lblCustName
    var fromAccountNickName = frmMBSavingsCareAddBal["segNSSlider"]["selectedItems"][0].lblActNoval;
    fromAccountNickName = "xxx-x-" + fromAccountNickName.substring(6, 11) + "-x";
    //kony.print("fromAccountNickName" + fromAccountNickName);
    frmMBSavingsCareAddNickName.Label0db8ff91d2a8c44.text = kony.i18n.getLocalizedString("keyOpenAcc");
    frmMBSavingsCareAddNickName.Label0db8ff91d2a8c44.centerY = "32%";
    frmMBSavingsCareAddNickName.lblAmountDesc.text = kony.i18n.getLocalizedString("deduct_amt") + " " + fromAccountNickName;
    frmMBSavingsCareAddNickName.txtNickName.placeholder = kony.i18n.getLocalizedString("add_actNickname") //"My Saving Care";
    frmMBSavingsCareAddNickName.txtNickName.placeholderSkin = "txtBlueNormal200";
    //frmMBSavingsCareAddNickName.lblavailPercent.text=kony.i18n.getLocalizedString("");
    //frmMBSavingsCareAddNickName.lblBenifitHeading.text=kony.i18n.getLocalizedString("");
    frmMBSavingsCareAddNickName.lblBenifTop.text = kony.i18n.getLocalizedString("keyBenificiaries");
    // MIB-4989
    //var nickNameAddBal = removeColonFromEnd(kony.i18n.getLocalizedString("billerNicknameMB"));
    var nickNameAddBal = removeColonFromEnd(kony.i18n.getLocalizedString("Nickname"));
    frmMBSavingsCareAddNickName.lblNickNameDesc.text = nickNameAddBal;
    //frmMBSavingsCareAddNickName.lblOccupationinfo.text=kony.i18n.getLocalizedString("keySavingCareAct");
    frmMBSavingsCareAddNickName.btnBack.text = kony.i18n.getLocalizedString("Back");
    frmMBSavingsCareAddNickName.btnNext.text = kony.i18n.getLocalizedString("Next");
}

function setlanguageflipforAddBal() {
    frmMBSavingsCareAddBal.lblAmount.text = kony.i18n.getLocalizedString("");
    frmMBSavingsCareAddBal.lblAmountDesc.text = kony.i18n.getLocalizedString("");
    frmMBSavingsCareAddBal.label475124774164.text = kony.i18n.getLocalizedString("keyOpenAcc");
    frmMBSavingsCareAddBal.btnOATDConfCancel.text = kony.i18n.getLocalizedString("");
    frmMBSavingsCareAddBal.btnPopUpTermination.text = kony.i18n.getLocalizedString("");
}

function deviceBack() {
    kony.print("Notihing happen");
}