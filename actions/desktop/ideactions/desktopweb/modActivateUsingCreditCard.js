function onClickCreditCardImg() {
    frmMBActiAtmIdMobile.hbxATM.skin = "hbxATMcard";
    frmMBActiAtmIdMobile.lblAtmNumber.setVisibility(false);
    frmMBActiAtmIdMobile.tbxAtmNumber.setVisibility(false);
    frmMBActiAtmIdMobile.lineAtm.setVisibility(false);
    frmMBActiAtmIdMobile.lblAtmNumberConfirm.setVisibility(false);
    frmMBActiAtmIdMobile.tbxAtmNumber.text = "";
    frmMBActiAtmIdMobile.txtCreditCardNum.text = "";
    frmMBActiAtmIdMobile.hbxCC.skin = "hbxCreditCardfocus";
    frmMBActiAtmIdMobile.lblCreditCardNum.setVisibility(true);
    frmMBActiAtmIdMobile.txtCreditCardNum.setVisibility(true);
    frmMBActiAtmIdMobile.lineCreditCard.setVisibility(true);
    frmMBActiAtmIdMobile.lblCreditCardNumConfirm.setVisibility(false);
    frmMBActiAtmIdMobile.imgNoteCreditCard.src = "arrowtoplightblue.png";
    frmMBActiAtmIdMobile.imgNoteDebitCard.src = "empty.png";
    frmMBActiAtmIdMobile.lblCardDesc.text = kony.i18n.getLocalizedString("Act_CreditCard_Option");
}

function onClickATMCardImg() {
    frmMBActiAtmIdMobile.hbxCC.skin = "hbxCreditCard";
    frmMBActiAtmIdMobile.lblCreditCardNum.setVisibility(false);
    frmMBActiAtmIdMobile.txtCreditCardNum.setVisibility(false);
    frmMBActiAtmIdMobile.lineCreditCard.setVisibility(false);
    frmMBActiAtmIdMobile.lblCreditCardNumConfirm.setVisibility(false);
    frmMBActiAtmIdMobile.tbxAtmNumber.text = "";
    frmMBActiAtmIdMobile.txtCreditCardNum.text = "";
    frmMBActiAtmIdMobile.lblAtmNumber.setVisibility(true);
    frmMBActiAtmIdMobile.tbxAtmNumber.setVisibility(true);
    frmMBActiAtmIdMobile.lineAtm.setVisibility(true);
    frmMBActiAtmIdMobile.lblAtmNumberConfirm.setVisibility(false);
    frmMBActiAtmIdMobile.imgNoteCreditCard.src = "empty.png";
    frmMBActiAtmIdMobile.imgNoteDebitCard.src = "arrowtoplightblue.png";
    frmMBActiAtmIdMobile.lblCardDesc.text = kony.i18n.getLocalizedString("Act_ATM_Option");
    frmMBActiAtmIdMobile.hbxATM.skin = "hbxATMcardfocus";
}

function validateCVV(atmPinEncrypted) {
    var inputParam = {};
    inputParam["cvv"] = encryptDPUsingE2EE(atmPinEncrypted);
    showLoadingScreen();
    invokeServiceSecureAsync("verifyCVVE2E", inputParam, validateCVVCallBack);
}

function validateCVVCallBack(status, resulttable) {
    if (status == 400) {
        dismissLoadingScreen();
        if (resulttable["opstatus"] == 0) {
            //PIN Verify Success
            gblDPPk = "";
            gblDPRandNumber = "";
            atmCitizenIdVisibilityForConfirmation(true);
        } else if (resulttable["opstatus"] == "-1") {
            //showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            gblDPPk = "";
            gblDPRandNumber = "";
            frmMBanking.show();
        } else {
            clearCVVMB(); // method to clear the virtual keyboard
            if (resulttable["errCode"] == "B243083") {
                showAlert(kony.i18n.getLocalizedString("MB_ATErr_CVV"), kony.i18n.getLocalizedString("info"));
            } else if (resulttable["errCode"] == "B243082") {
                showAlertWithCallBack(kony.i18n.getLocalizedString("MB_ATErr_IncorrectCVV"), kony.i18n.getLocalizedString("info"), onClickPreLoginBtn);
            } else {
                showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
            }
            /*gblPk = resulttable["pubKey"];
	       	gblRand = resulttable["serverRandom"];
	        gblAlgorithm = resulttable["hashAlgorithm"];*/
            return false;
        }
    }
}