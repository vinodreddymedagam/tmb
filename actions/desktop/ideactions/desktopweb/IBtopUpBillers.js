var flagSearch = 0;
var flagCat = 0;
var currentData = 0;
var suggestedCatData = 0;
var catData = 0;
var suggestedSearchData = 0;
var searchData = 0;

function IBTopUpcurrentSystemDate() {
    if (!gblEditTopUp) {
        var today = new Date(GLOBAL_TODAY_DATE);
        var dd = today.getDate();
        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        today = dd + '/' + mm + '/' + yyyy;
        frmIBTopUpLandingPage.lblPayBillOnValue.text = today
        frmIBTopUpLandingPage.lblrepeats.text = "";
        frmIBTopUpLandingPage.lblrepeats.setVisibility(false);
        frmIBTopUpLandingPage.lblDatesFuture.text = "";
        frmIBTopUpLandingPage.lblDatesFuture.setVisibility(false);
        frmIBTopUpLandingPage.lblPayBillOnValue.setVisibility(true);
        gblPaynow = true;
    }
}

function clearfrmIBTopUpLandingPage() {
    if (gblEditTopUp) {
        gblEditTopUp = false;
    } else {
        gblPaynow = true;
        //two global vars to keep track of scheduled selection
        gblScheduleTopUpRepeatSelection = "0";
        gblScheduleTopUpEndingSelection = "0";
        frmIBTopUpLandingPage.lblNickTopUp.text = "";
        frmIBTopUpLandingPage.lblCompCode.text = "";
        frmIBTopUpLandingPage.ref1value.text = "";
        //below line is added for CR - PCI-DSS masked Credit card no
        frmIBTopUpLandingPage.ref1valueMasked.text = "";
        frmIBTopUpLandingPage.txtAmount.text = "";
        frmIBTopUpLandingPage.txtAmount.placeholder = "0.00";
        frmIBTopUpLandingPage.lblTHB.text = " " + kony.i18n.getLocalizedString('currencyThaiBaht');
        frmIBTopUpLandingPage.imgArrowBiller.setVisibility(false);
        //frmIBTopUpLandingPage.lblLimitValue.text = "";
        frmIBTopUpLandingPage.textarea210136690395040.text = "";
        frmIBTopUpLandingPage.hbxRef1TopUpScreen.setVisibility(false);
        frmIBTopUpLandingPage.imgTopUp.isVisible = false;
        frmIBTopUpLandingPage.hbxBiller.setVisibility(false);
        frmIBTopUpLandingPage.hbxImage.setVisibility(true);
        frmIBTopUpLandingPage.hbxAmount.setVisibility(false);
        frmIBTopUpLandingPage.lblrepeats.text = "";
        frmIBTopUpLandingPage.lblrepeats.setVisibility(false);
        frmIBTopUpLandingPage.lblDatesFuture.text = "";
        frmIBTopUpLandingPage.lblDatesFuture.setVisibility(false);
        frmIBTopUpLandingPage.lblPayBillOnValue.setVisibility(true);
        frmIBTopUpLandingPage.hbxAmntExclBillerMethodOne.setVisibility(true);
        //Resetting calendar when we come to this page not from Editing.
        gblrepeat = 0;
        gblClicked = "once";
        frmIBTopUpLandingPage.lblXferEnding.setVisibility(false);
        frmIBTopUpLandingPage.lblXferEnding.setVisibility(false);
        frmIBTopUpLandingPage.hbxrec.setVisibility(false);
        frmIBTopUpLandingPage.lblENDtext.setVisibility(false);
        frmIBTopUpLandingPage.hbxEndCalendar.setVisibility(false);
        frmIBTopUpLandingPage.lineAfter.setVisibility(false);
        frmIBTopUpLandingPage.hbxTimes.setVisibility(false);
        frmIBTopUpLandingPage.txtTimes.text = "";
        var today = new Date();
        var tDate = today.getDate();
        var tMonth = today.getMonth() + 1;
        var tYear = today.getFullYear();
        frmIBTopUpLandingPage.CalendarXferDate.dateComponents = [tDate, tMonth, tYear];
        frmIBTopUpLandingPage.calendarXferUntil.dateComponents = [tDate, tMonth, tYear];
        frmIBTopUpLandingPage.btnDaily.skin = btnIBTab4LeftNrml;
        frmIBTopUpLandingPage.btnWeekly.skin = btnIBTab4MidNrml;
        frmIBTopUpLandingPage.btnMonthly.skin = btnIBTab4MidNrml;
        frmIBTopUpLandingPage.btnYearly.skin = btnIbTab4RightNrml;
        frmIBTopUpLandingPage.btnNever.skin = btnIBTab3LeftFocus;
        frmIBTopUpLandingPage.btnAfter.skin = btnIBTab3MidNrml;
        frmIBTopUpLandingPage.btnOnDate.skin = btnIBTab3RightNrml;
        //var currentDate = new Date(new Date().getTime() + 24 * 60 * 60 * 1000); -- Can select today date as per MIB-2558
        var currentDate = new Date();
        var day = currentDate.getDate()
        var month = currentDate.getMonth() + 1
        var year = currentDate.getFullYear()
        frmIBTopUpLandingPage.CalendarXferDate.validStartDate = [day, month, year];
        frmIBTopUpLandingPage.calendarXferUntil.validStartDate = [day, month, year];
    }
}
/*function formatAMPM(date) {
	var hours = date.getHours();
	var minutes = date.getMinutes();
	var ampm = hours >= 12 ? 'PM' : 'AM';
	hours = hours % 12;
	hours = hours ? hours : 12; // the hour '0' should be '12'
	minutes = minutes < 10 ? '0' + minutes : minutes;
	var strTime = hours + ':' + minutes + ' ' + ampm;
	return strTime;
}*/
function IBTopUpBiller() {
    //alert("ok");
    if (kony.application.getCurrentForm().id == "frmIBTopUpCW") {
        alert(kony.i18n.getLocalizedString("keyIBTransferFromSelect"));
        return false;
    }
    if (frmIBTopUpLandingPage.hbxBiller.isVisible == false) {
        frmIBTopUpLandingPage.segBiller.data = [];
        frmIBTopUpLandingPage.txtSearchBox.text = "";
        frmIBTopUpLandingPage.segSuggestedBiller.data = [];
        frmIBTopUpLandingPage.hbxBiller.setVisibility(true);
        frmIBTopUpLandingPage.hbxImage.setVisibility(false);
        frmIBTopUpLandingPage.hbxFutureTopup.setVisibility(false);
        frmIBTopUpLandingPage.hbxwaterWheel.setVisibility(false);
        //frmIBTopUpLandingPage.imgArrowBiller.setVisibility(true);
        frmIBTopUpLandingPage.imgArrowCal.setVisibility(false);
        gblMyBillerTopUpBB = 1;
        showLoadingScreenPopup();
        getSelectTopUpCategoryService();
        frmIBTopUpLandingPage.txtAmount.setEnabled(true);
    } else {
        frmIBTopUpLandingPage.hbxBiller.setVisibility(false);
        frmIBTopUpLandingPage.hbxImage.setVisibility(true);
        frmIBTopUpLandingPage.imgArrowBiller.setVisibility(false);
    }
}

function IBFutureTopUp() {
    // gblClicked = "Once";
    if (frmIBTopUpLandingPage.lblNickTopUp.text == null || frmIBTopUpLandingPage.lblNickTopUp.text == "" || frmIBTopUpLandingPage.lblNickTopUp.text == " ") {
        alert(kony.i18n.getLocalizedString("Valid_SelBiller"));
        return;
    }
    getTopUpBillerNickName();
    if (frmIBTopUpLandingPage.hbxFutureTopup.isVisible == false) {
        frmIBTopUpLandingPage.hbxFutureTopup.setVisibility(true);
        frmIBTopUpLandingPage.hbxImage.setVisibility(false);
        frmIBTopUpLandingPage.hbxBiller.setVisibility(false);
        frmIBTopUpLandingPage.hbxwaterWheel.setVisibility(false);
        frmIBTopUpLandingPage.imgArrowCal.setVisibility(false);
        frmIBTopUpLandingPage.imgArrowBiller.setVisibility(false);
        populateTopUpScheduleIB();
    } else {
        frmIBTopUpLandingPage.hbxFutureTopup.setVisibility(false);
        frmIBTopUpLandingPage.hbxImage.setVisibility(true);
    }
}

function IBTopUpCoverWheel() {
    if (frmIBTopUpLandingPage.hbxwaterWheel.isVisible == false) {
        frmIBTopUpLandingPage.hbxwaterWheel.setVisibility(true);
        frmIBTopUpLandingPage.hbxImage.setVisibility(false);
        frmIBTopUpLandingPage.hbxBiller.setVisibility(false);
        frmIBTopUpLandingPage.hbxFutureTopup.setVisibility(false);
        frmIBTopUpLandingPage.imgArrowWheel.setVisibility(true);
    } else {
        frmIBTopUpLandingPage.hbxwaterWheel.setVisibility(false);
        frmIBTopUpLandingPage.imgArrowWheel.setVisibility(false);
        frmIBTopUpLandingPage.hbxImage.setVisibility(true);
    }
}
/*
*************************************************************************************
		Module	: TopUpBillerSearch
		Author  : Kony
		Date    : June 06, 2013
		Purpose : This is used for function search in TopUp Billers
****************************************************************************************
*/
//function IBsearchTopUpBiller(eventObject, searchType) {
//	//
//	//
//	var textLength;
//	//if searchType is 1 then it is for textbox search else dictionary view 
//	
//	if (searchType == "1") {
//		textLength = 2;
//	} else {
//		textLength = 0;
//	}
//	
//	var sProduct = eventObject.text;
//	if (sProduct.length <= textLength) {
//		frmIBTopUpLandingPage.lblName.setVisibility(false);
//		frmIBTopUpLandingPage.segBiller.data = gblTransferToRecipientCache;
//		gblTransferToRecipientData = gblTransferToRecipientCache;
//	} else {
//		final_Product = [];
//		var check = "";
//		for (var i = 0;
//			((gblTransferToRecipientCache) != null) && i < gblTransferToRecipientCache.length; i++) {
//			val = gblTransferToRecipientCache[i].lblName;
//			
//			check = kony.string.startsWith(val, sProduct, true);
//			
//			if (check == true) {
//				
//				final_Product.push({
//					lblName: gblTransferToRecipientCache[i].lblName,
//					lblAccountNum: gblTransferToRecipientCache[i].lblAccountNum,
//					imgArrow: "navarrow3.png",
//					imgprofilepic: "avatar_dis.png",
//					mobileNo: gblTransferToRecipientCache[i].mobileNo,
//					imgmob: gblTransferToRecipientCache[i].imgmob,
//					facebookNo: gblTransferToRecipientCache[i].facebookNo,
//					imgfb: gblTransferToRecipientCache[i].imgfb,
//					hiddenmain: "main",
//					template: hbrt1
//				});
//			}
//			
//			
//		}
//		frmIBTopUpLandingPage.segBiller.data = final_Product;
//		gblTransferToRecipientData = final_Product;
//		if (final_Product.length == 0) {
//			frmIBTopUpLandingPage.lblName.setVisibility(true);
//		} else {
//			frmIBTopUpLandingPage.lblName.setVisibility(false);
//		}
//	}
//}
function segBillerRowClickIB() {
    /*
"imgBillerArrow": "navarrow2.png",
		"fullAmount": "87000",
		"billerGroupType": "0",
		"lblBillerName": "Electricity",
		"lblRef1": "Ref1",
		"feeAmount": "0",
		"lblRef2": "Ref2",
		"lblRef2Value": "211-33243-4",
		"waiverCode": "true",
		"billerMethod": "3",
		"imgBillerpic": "bill_img02.png",
		"lblRef1Value": "524-457-789",
		"Amount1": "520",
		"Amount2": "120"
*/
    var indexOfSelectedRow = frmIBTopUpLandingPage.segBiller.selectedIndex[1];
    var indexOfSelectedIndex = frmIBTopUpLandingPage.segBiller.selectedItems[0];
    //frmIBTopUpLandingPage.hboxSelectBiller.isVisible= false
    frmIBTopUpLandingPage.hbxRef1TopUpScreen.isVisible = true;
    var dataList = frmIBTopUpLandingPage.segBiller.data[indexOfSelectedRow];
    frmIBTopUpLandingPage.imgTopUp.src = indexOfSelectedIndex.imgBillerpic;
    frmIBTopUpLandingPage.lblNickTopUp.text = indexOfSelectedIndex.lblBillerName
    frmIBTopUpLandingPage.lblCompCode.text = indexOfSelectedIndex.BillerID + "(" + indexOfSelectedIndex.BillerCompCode + ")";
    frmIBTopUpLandingPage.ref1.text = indexOfSelectedIndex.lblRef1;
    frmIBTopUpLandingPage.ref1value.text = indexOfSelectedIndex.lblRef1Value;
    //below line is added for CR - PCI-DSS masked Credit card no
    frmIBTopUpLandingPage.ref1valueMasked.text = maskCreditCard(indexOfSelectedIndex.lblRef1Value);
}

function onChooseScheduleDateTopUpIB() {
    date1 = frmIBTopUpLandingPage.CalendarXferDate.formattedDate;
    var today = currentSystemDate();
    if ((parseDate(date1) - parseDate(today)) == 0) {
        clearTopUpIBRepeatOptions();
        clearTopUpIBEndingOptions();
        return;
    }
}

function clearTopUpIBRepeatOptions() {
    gblClicked = "once";
    times = 0;
    frmIBTopUpLandingPage.btnDaily.skin = "btnIBTab4LeftNrml";
    frmIBTopUpLandingPage.btnWeekly.skin = "btnIBTab4MidNrml";
    frmIBTopUpLandingPage.btnMonthly.skin = "btnIBTab4MidNrml";
    frmIBTopUpLandingPage.btnYearly.skin = "btnIbTab4RightNrml";
    frmIBTopUpLandingPage.lblXferEnding.setVisibility(false);
    frmIBTopUpLandingPage.hbxrec.setVisibility(false);
}

function clearTopUpIBEndingOptions() {
    frmIBTopUpLandingPage.hbxEndCalendar.setVisibility(false);
    //frmIBTopUpLandingPage.txtTimes.setVisibility(false);
    //frmIBTopUpLandingPage.lblIncludes.setVisibility(false);
    frmIBTopUpLandingPage.btnNever.skin = "btnIBTab3LeftNrml";
    frmIBTopUpLandingPage.btnAfter.skin = "btnIBTab3MidNrml";
    frmIBTopUpLandingPage.btnOnDate.skin = "btnIBTab3RightNrml";
    frmIBTopUpLandingPage.hbxTimes.setVisibility(false);
    frmIBTopUpLandingPage.lblENDtext.setVisibility(false);
    frmIBTopUpLandingPage.lineAfter.setVisibility(false);
    frmIBTopUpLandingPage.txtTimes.text = "";
    frmIBTopUpLandingPage.calendarXferUntil.clear();
    gblrepeat = 0;
}

function shouldNotAllowRecurringForTodayDateTopUp() {
    var selectedDate = frmIBTopUpLandingPage.CalendarXferDate.formattedDate;
    var today = currentSystemDate();
    if (!isNotBlank(selectedDate)) {
        alert(kony.i18n.getLocalizedString("KeySelStartDate"));
        return true;
    } else if ((parseDate(selectedDate) - parseDate(today)) == 0) {
        alert(kony.i18n.getLocalizedString("Schedule_TodayErr"));
        return true;
    } else {
        return false;
    }
}

function IBOnClickDailyFutureTopUp() {
    if (shouldNotAllowRecurringForTodayDateTopUp()) {
        return;
    }
    if (frmIBTopUpLandingPage.btnDaily.skin == "btnIBTab4LeftFocus") {
        frmIBTopUpLandingPage.btnDaily.skin = "btnIBTab4LeftNrml";
        gblClicked = "once";
        clearTopUpIBEndingOptions();
        frmIBTopUpLandingPage.lblXferEnding.setVisibility(false);
        frmIBTopUpLandingPage.hbxrec.setVisibility(false);
        return;
    }
    if (gblreccuringDisableAdd == "N" && gblreccuringDisablePay == "Y") {
        frmIBTopUpLandingPage.btnDaily.skin = "btnIBTab4LeftNrml";
        alert(kony.i18n.getLocalizedString("Valid_RecurringDisabled"));
        //gblClicked = kony.i18n.getLocalizedString("keyOnce");
        gblClicked = "once";
        return;
    } else {
        frmIBTopUpLandingPage.btnDaily.skin = "btnIBTab4LeftFocus";
        gblClicked = "Daily";
        //gblClicked = kony.i18n.getLocalizedString("keyDaily");
    }
    frmIBTopUpLandingPage.btnWeekly.skin = "btnIBTab4MidNrml";
    frmIBTopUpLandingPage.btnMonthly.skin = "btnIBTab4MidNrml";
    frmIBTopUpLandingPage.btnYearly.skin = "btnIbTab4RightNrml";
    frmIBTopUpLandingPage.lblXferEnding.setVisibility(true);
    frmIBTopUpLandingPage.hbxrec.setVisibility(true);
    frmIBTopUpLandingPage.btnNever.onClick();
    gblrepeat = 3;
}

function IBOnClickWeeklyFutureTopUp() {
    if (shouldNotAllowRecurringForTodayDateTopUp()) {
        return;
    }
    if (frmIBTopUpLandingPage.btnWeekly.skin == "btnIBTab4MidFocus") {
        frmIBTopUpLandingPage.btnWeekly.skin = "btnIBTab4MidNrml";
        gblClicked = "once";
        clearTopUpIBEndingOptions();
        frmIBTopUpLandingPage.lblXferEnding.setVisibility(false);
        frmIBTopUpLandingPage.hbxrec.setVisibility(false);
        return;
    }
    if (gblreccuringDisableAdd == "N" && gblreccuringDisablePay == "Y") {
        frmIBTopUpLandingPage.btnWeekly.skin = "btnIBTab4MidNrml";
        alert(kony.i18n.getLocalizedString("Valid_RecurringDisabled"));
        //gblClicked = kony.i18n.getLocalizedString("keyOnce");
        gblClicked = "once";
        return;
    } else {
        frmIBTopUpLandingPage.btnWeekly.skin = "btnIBTab4MidFocus";
        //gblClicked = kony.i18n.getLocalizedString("keyWeekly");
        gblClicked = "Weekly";
    }
    frmIBTopUpLandingPage.btnDaily.skin = "btnIBTab4LeftNrml";
    frmIBTopUpLandingPage.btnMonthly.skin = "btnIBTab4MidNrml";
    frmIBTopUpLandingPage.btnYearly.skin = "btnIbTab4RightNrml";
    frmIBTopUpLandingPage.lblXferEnding.setVisibility(true);
    frmIBTopUpLandingPage.hbxrec.setVisibility(true);
    frmIBTopUpLandingPage.btnNever.onClick();
    gblrepeat = 3;
}

function IBOnClickMonthlyFutureTopUp() {
    if (shouldNotAllowRecurringForTodayDateTopUp()) {
        return;
    }
    if (frmIBTopUpLandingPage.btnMonthly.skin == "btnIBTab4MidFocus") {
        frmIBTopUpLandingPage.btnMonthly.skin = "btnIBTab4MidNrml";
        gblClicked = "once";
        clearTopUpIBEndingOptions();
        frmIBTopUpLandingPage.lblXferEnding.setVisibility(false);
        frmIBTopUpLandingPage.hbxrec.setVisibility(false);
        return;
    }
    if (gblreccuringDisableAdd == "N" && gblreccuringDisablePay == "Y") {
        frmIBTopUpLandingPage.btnMonthly.skin = "btnIBTab4MidNrml";
        alert(kony.i18n.getLocalizedString("Valid_RecurringDisabled"));
        //gblClicked = kony.i18n.getLocalizedString("keyOnce");
        gblClicked = "once";
        return;
    } else {
        frmIBTopUpLandingPage.btnMonthly.skin = "btnIBTab4MidFocus";
        //gblClicked = kony.i18n.getLocalizedString("keyMonthly");
        gblClicked = "Monthly";
    }
    frmIBTopUpLandingPage.btnDaily.skin = "btnIBTab4LeftNrml";
    frmIBTopUpLandingPage.btnWeekly.skin = "btnIBTab4MidNrml";
    frmIBTopUpLandingPage.btnYearly.skin = "btnIbTab4RightNrml";
    frmIBTopUpLandingPage.lblXferEnding.setVisibility(true);
    frmIBTopUpLandingPage.hbxrec.setVisibility(true);
    frmIBTopUpLandingPage.btnNever.onClick();
    gblrepeat = 3;
}

function IBOnClickYearlyFutureTopUp() {
    if (shouldNotAllowRecurringForTodayDateTopUp()) {
        return;
    }
    if (frmIBTopUpLandingPage.btnYearly.skin == "btnIBTab4RightFocus") {
        frmIBTopUpLandingPage.btnYearly.skin = "btnIbTab4RightNrml";
        gblClicked = "once";
        clearTopUpIBEndingOptions();
        frmIBTopUpLandingPage.lblXferEnding.setVisibility(false);
        frmIBTopUpLandingPage.hbxrec.setVisibility(false);
        return;
    }
    if (gblreccuringDisableAdd == "N" && gblreccuringDisablePay == "Y") {
        frmIBTopUpLandingPage.btnYearly.skin = "btnIbTab4RightNrml";
        alert(kony.i18n.getLocalizedString("Valid_RecurringDisabled"));
        //gblClicked = kony.i18n.getLocalizedString("keyOnce");
        gblClicked = "once";
        return;
    } else {
        frmIBTopUpLandingPage.btnYearly.skin = "btnIBTab4RightFocus";
        //gblClicked = kony.i18n.getLocalizedString("keyYearly");
        gblClicked = "Yearly";
    }
    frmIBTopUpLandingPage.btnDaily.skin = "btnIBTab4LeftNrml";
    frmIBTopUpLandingPage.btnWeekly.skin = "btnIBTab4MidNrml";
    frmIBTopUpLandingPage.btnMonthly.skin = "btnIBTab4MidNrml";
    frmIBTopUpLandingPage.lblXferEnding.setVisibility(true);
    frmIBTopUpLandingPage.hbxrec.setVisibility(true);
    frmIBTopUpLandingPage.btnNever.onClick();
    gblrepeat = 3;
}

function IBOnClickNeverFutureTopUp() {
    frmIBTopUpLandingPage.hbxEndCalendar.setVisibility(false);
    //frmIBTopUpLandingPage.txtTimes.setVisibility(false);
    //frmIBTopUpLandingPage.lblIncludes.setVisibility(false);
    frmIBTopUpLandingPage.btnNever.skin = "btnIBTab3LeftFocus";
    frmIBTopUpLandingPage.btnNever.focusSkin = "btnIBTab3LeftFocus";
    frmIBTopUpLandingPage.btnAfter.skin = "btnIBTab3MidNrml";
    frmIBTopUpLandingPage.btnOnDate.skin = "btnIBTab3RightNrml";
    frmIBTopUpLandingPage.hbxTimes.setVisibility(false);
    frmIBTopUpLandingPage.lblENDtext.setVisibility(false);
    frmIBTopUpLandingPage.lineAfter.setVisibility(false);
    gblrepeat = 3;
}

function IBOnClickAfterFutureTopUp() {
    frmIBTopUpLandingPage.hbxEndCalendar.setVisibility(false);
    //frmIBTopUpLandingPage.lblIncludes.setVisibility(true);
    frmIBTopUpLandingPage.lblENDtext.text = kony.i18n.getLocalizedString("keyEndAfter");
    //frmIBTopUpLandingPage.lblIncludes.text = kony.i18n.getLocalizedString("keyInclThisTime"); //added i18 keys
    frmIBTopUpLandingPage.btnNever.skin = "btnIBTab3LeftNrml";
    frmIBTopUpLandingPage.btnAfter.focusSkin = "btnIBTab3MidFocus";
    frmIBTopUpLandingPage.btnAfter.skin = "btnIBTab3MidFocus";
    frmIBTopUpLandingPage.btnOnDate.skin = "btnIBTab3RightNrml";
    frmIBTopUpLandingPage.lblENDtext.setVisibility(true);
    //frmIBTopUpLandingPage.lineAfter.setVisibility(true);
    frmIBTopUpLandingPage.hbxTimes.setVisibility(true);
    gblrepeat = 1;
}

function IBOnClickOnDateFutureTopUp() {
    frmIBTopUpLandingPage.hbxEndCalendar.setVisibility(true);
    frmIBTopUpLandingPage.lblENDtext.text = kony.i18n.getLocalizedString("keyEndDate");
    //frmIBTopUpLandingPage.lblIncludes.setVisibility(false);
    frmIBTopUpLandingPage.btnNever.skin = "btnIBTab3LeftNrml";
    frmIBTopUpLandingPage.btnAfter.skin = "btnIBTab3MidNrml";
    frmIBTopUpLandingPage.btnOnDate.skin = "btnIBTab3RightFocus";
    frmIBTopUpLandingPage.btnOnDate.focusSkin = "btnIBTab3RightFocus";
    frmIBTopUpLandingPage.hbxTimes.setVisibility(false);
    frmIBTopUpLandingPage.lblENDtext.setVisibility(true);
    //frmIBTopUpLandingPage.lineAfter.setVisibility(true);
    gblrepeat = 2;
}

function IBOnClickSaveFutureTopUp() {
    if (!gblEditTopUp) {
        gblPaynow = false;
        //var timesOrDate = "0";
        gblTimes = "-1";
        var times = "-1";
    }
    var firstDate = frmIBTopUpLandingPage.CalendarXferDate.formattedDate;
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    today = dd + '/' + mm + '/' + yyyy;
    if (firstDate == "" || firstDate == null) {
        alert(kony.i18n.getLocalizedString("Valid_SelectvalidFutureDate"));
        gblPaynow = true;
        return;
    } else if ((parseDate(firstDate) - parseDate(today)) < 0) {
        alert(kony.i18n.getLocalizedString("Valid_SelectvalidFutureDate"));
        gblPaynow = true;
        return;
    } else if ((parseDate(firstDate) - parseDate(today)) == 0) {
        //New Top Up Requirement to let user choose today date and save.
        gblPaynow = true;
        gblScheduleTopUpRepeatSelection = "0";
        gblScheduleTopUpEndingSelection = "0";
        frmIBTopUpLandingPage.lblPayBillOnValue.setVisibility(true);
        frmIBTopUpLandingPage.lblPayBillOnValue.text = today;
        frmIBTopUpLandingPage.lblDatesFuture.setVisibility(false);
        frmIBTopUpLandingPage.lblDatesFuture.text = "";
        frmIBTopUpLandingPage.lblrepeats.setVisibility(false);
        frmIBTopUpLandingPage.lblrepeats.text = "";
        frmIBTopUpLandingPage.hbxFutureTopup.setVisibility(false);
        frmIBTopUpLandingPage.hbxImage.setVisibility(true);
        hideShowScheduleNicknameTopUpIB(!(gblPaynow || gblBillerPresentInMyBills));
        return;
    } else {
        frmIBTopUpLandingPage.lblPayBillOnValue.text = firstDate;
        var secondDate = "";
        if (frmIBTopUpLandingPage.hbxEndCalendar.isVisible) {
            //timesOrDate = "1";
            gblScheduleTopUpEndingSelection = "3";
            frmIBTopUpLandingPage.lblDatesFuture.setVisibility(true);
            frmIBTopUpLandingPage.lblPayBillOnValue.setVisibility(false);
            frmIBTopUpLandingPage.lblrepeats.setVisibility(true);
            secondDate = frmIBTopUpLandingPage.calendarXferUntil.formattedDate;
            if (secondDate == "" || secondDate == null) {
                alert(kony.i18n.getLocalizedString("Valid_SelectEndDate"));
                if (!gblEditTopUp) gblPaynow = true;
                return;
            }
            if ((parseDate(secondDate) - parseDate(firstDate)) <= 0) {
                alert(kony.i18n.getLocalizedString("keySelectValidEndDateFT"));
                return;
            }
            times = numberOfDaysIB(firstDate, secondDate);
            times = Number(times);
            if (times > 99) {
                alert(kony.i18n.getLocalizedString("Valid_ErrorRecLessthan99"));
                gblPaynow = true;
                return;
            }
            if (isNaN(times) || times < 1) {
                alert(kony.i18n.getLocalizedString("Valid_EntervalidRecValue"));
                gblPaynow = true;
                return;
            }
        } else if (frmIBTopUpLandingPage.hbxTimes.isVisible) {
            //timesOrDate = "0";
            gblScheduleTopUpEndingSelection = "2";
            frmIBTopUpLandingPage.lblDatesFuture.setVisibility(true);
            frmIBTopUpLandingPage.lblPayBillOnValue.setVisibility(false);
            frmIBTopUpLandingPage.lblrepeats.setVisibility(true);
            times = frmIBTopUpLandingPage.txtTimes.text;
            times = Number(times);
            if (isNaN(times) || times < 1) {
                alert(kony.i18n.getLocalizedString("Valid_EntervalidRecValue"));
                gblPaynow = true;
                return;
            }
            if (times > 99) {
                alert(kony.i18n.getLocalizedString("Valid_ErrorRecLessthan99"));
                gblPaynow = true;
                return;
            }
            var startDate = parseDate(firstDate);
            //frmIBTopUpLandingPage.lblPayBillOnValue.setVisibility(false);
            secondDate = endDateCalculatorBillIB(startDate, times);
        } else if (gblrepeat == 3) {
            gblScheduleTopUpEndingSelection = "1";
            frmIBTopUpLandingPage.lblDatesFuture.setVisibility(true);
            frmIBTopUpLandingPage.lblPayBillOnValue.setVisibility(false);
            frmIBTopUpLandingPage.lblrepeats.setVisibility(true);
            gblPaynow = false;
            frmIBTopUpLandingPage.lblDatesFuture.text = firstDate;
            gblScheduleTopUpRepeatSelection = gblClicked;
            if (gblClicked == "Daily") {
                frmIBTopUpLandingPage.lblrepeats.text = kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyDaily");
            } else if (gblClicked == "Weekly") {
                frmIBTopUpLandingPage.lblrepeats.text = kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyWeekly");
            } else if (gblClicked == "Monthly") {
                frmIBTopUpLandingPage.lblrepeats.text = kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyMonthly");
            } else if (gblClicked == "Yearly") {
                frmIBTopUpLandingPage.lblrepeats.text = kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyYearly");
            } else {
                frmIBTopUpLandingPage.lblrepeats.text = kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyOnce");
            }
            frmIBTopUpLandingPage.hbxFutureTopup.setVisibility(false);
            frmIBTopUpLandingPage.hbxImage.setVisibility(true);
            onClickOKScheduleTopUpIB();
            return;
        } else {
            gblScheduleTopUpRepeatSelection = "0";
            gblScheduleTopUpEndingSelection = "0";
            frmIBTopUpLandingPage.lblDatesFuture.setVisibility(true);
            frmIBTopUpLandingPage.lblPayBillOnValue.setVisibility(false);
            frmIBTopUpLandingPage.lblrepeats.setVisibility(true);
            secondDate = firstDate;
        }
        labelTransfertop = firstDate + " " + kony.i18n.getLocalizedString("keyTo") + " " + secondDate;
        //	labelTransferBottomWeek = kony.i18n.getLocalizedString("keyRepeatWeekly") + " " + times + " " + kony.i18n.getLocalizedString("keyTimesMB");
        //	labelTransferBottomMonth = kony.i18n.getLocalizedString("keyRepeatMonthly") + " " + times + " " + kony.i18n.getLocalizedString("keyTimesMB");
        //	labelTransferBottomYear = kony.i18n.getLocalizedString("keyRepeatYearly") + " " + times + " " + kony.i18n.getLocalizedString("keyTimesMB");
        //    labelTransferBottomDaily = kony.i18n.getLocalizedString("keyRepeatDaily") + " " + times + " " + kony.i18n.getLocalizedString("keyTimesMB");
        frmIBTopUpLandingPage.lblDatesFuture.text = labelTransfertop;
        gblPaynow = false;
        gblScheduleTopUpRepeatSelection = gblClicked;
        if (gblClicked == "Daily") {
            frmIBTopUpLandingPage.lblrepeats.text = kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyDaily") + " " + times + " " + kony.i18n.getLocalizedString("keyTimesMB");
        } else if (gblClicked == "Weekly") {
            frmIBTopUpLandingPage.lblrepeats.text = kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyWeekly") + " " + times + " " + kony.i18n.getLocalizedString("keyTimesMB");
        } else if (gblClicked == "Monthly") {
            frmIBTopUpLandingPage.lblrepeats.text = kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyMonthly") + " " + times + " " + kony.i18n.getLocalizedString("keyTimesMB");
        } else if (gblClicked == "Yearly") {
            frmIBTopUpLandingPage.lblrepeats.text = kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyYearly") + " " + times + " " + kony.i18n.getLocalizedString("keyTimesMB");
        } else {
            frmIBTopUpLandingPage.lblrepeats.text = kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyOnce");
        }
    }
    gblTimes = times;
    frmIBTopUpLandingPage.hbxFutureTopup.setVisibility(false);
    frmIBTopUpLandingPage.hbxImage.setVisibility(true);
    onClickOKScheduleTopUpIB();
}

function numberOfDaysIB(date1, date2) {
    var startDate = parseDate(date1);
    var endDate = parseDate(date2);
    if (kony.string.equalsIgnoreCase(gblClicked, "Daily")) {
        gblNumberOfDays = daydiff(startDate, endDate);
    } else if (kony.string.equalsIgnoreCase(gblClicked, "Weekly")) {
        gblNumberOfDays = weekdiff(startDate, endDate);
    } else if (kony.string.equalsIgnoreCase(gblClicked, "Monthly")) {
        gblNumberOfDays = monthdiff(startDate, endDate);
    } else if (kony.string.equalsIgnoreCase(gblClicked, "Yearly")) {
        gblNumberOfDays = yeardiff(startDate, endDate);
    }
    return gblNumberOfDays;
}

function onClickOKScheduleTopUpIB() {
    getTopUpBillerNickName();
}

function hideShowScheduleNicknameTopUpIB(showFlag) {
    frmIBTopUpLandingPage.hbxAddBillsBeforeSchedule.isVisible = showFlag;
    frmIBTopUpLandingPage.hbxScheduleNickName.isVisible = showFlag;
    frmIBTopUpLandingPage.lineAddBillsBeforeSchedule.isVisible = showFlag;
    if (!showFlag) {
        frmIBTopUpLandingPage.txtScheduleNickname.text = "";
    }
}

function populateTopUpScheduleIB() {
    var scheduleOrNowDate = "";
    if (gblPaynow) {
        scheduleOrNowDate = frmIBTopUpLandingPage.lblPayBillOnValue.text.trim();
        scheduleOrNowDate = scheduleOrNowDate.split("/", 3);
        var day = scheduleOrNowDate[0];
        var month = scheduleOrNowDate[1];
        var year = scheduleOrNowDate[2];
        frmIBTopUpLandingPage.CalendarXferDate.dateComponents = [day, month, year];
        clearTopUpIBEndingOptions();
        clearTopUpIBRepeatOptions();
    } else {
        clearTopUpIBEndingOptions();
        clearTopUpIBRepeatOptions();
        scheduleOrNowDate = frmIBTopUpLandingPage.lblDatesFuture.text.trim();
        var scheduleRepeatOption = frmIBTopUpLandingPage.lblrepeats.text.trim();
        scheduleOrNowDate = scheduleOrNowDate.split(" ");
        scheduleRepeatOption = scheduleRepeatOption.split(" ");
        var startOnDate = scheduleOrNowDate[0].split("/", 3);
        var day = startOnDate[0];
        var month = startOnDate[1];
        var year = startOnDate[2];
        frmIBTopUpLandingPage.CalendarXferDate.dateComponents = [day, month, year];
        // Doing focus on relevant options
        switch (gblScheduleTopUpRepeatSelection) {
            case "Daily":
                frmIBTopUpLandingPage.btnDaily.onClick();
                break;
            case "Weekly":
                frmIBTopUpLandingPage.btnWeekly.onClick();
                break;
            case "Monthly":
                frmIBTopUpLandingPage.btnMonthly.onClick();
                break;
            case "Yearly":
                frmIBTopUpLandingPage.btnYearly.onClick();
                break;
        }
        switch (gblScheduleTopUpEndingSelection) {
            case "1":
                frmIBTopUpLandingPage.btnNever.onClick();
                break;
            case "2":
                frmIBTopUpLandingPage.btnAfter.onClick();
                break;
            case "3":
                frmIBTopUpLandingPage.btnOnDate.onClick();
                break;
        }
        if (gblScheduleTopUpEndingSelection == "2") {
            frmIBTopUpLandingPage.txtTimes.text = scheduleRepeatOption[2];
        } else if (gblScheduleTopUpEndingSelection == "3") {
            var endOnDate = scheduleOrNowDate[2].split("/", 3);;
            day = endOnDate[0];
            month = endOnDate[1];
            year = endOnDate[2];
            frmIBTopUpLandingPage.calendarXferUntil.dateComponents = [day, month, year];
        }
    }
}