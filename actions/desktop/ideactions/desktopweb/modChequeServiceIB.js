function validateStopChequeService() {
    var chequeNo = frmIBChequeServiceStopChequeLanding.txtChequeNo.text;
    var fromAccount = frmIBChequeServiceStopChequeLanding.lblFromAccount.text;
    var regrex = /^[0-9]+$/
    if (chequeNo.length == 0) {
        showAlert(kony.i18n.getLocalizedString("keyEnterCheque"), kony.i18n.getLocalizedString("info"));
        frmIBChequeServiceStopChequeLanding.txtChequeNo.skin = "txtErrorBG";
        return false;
    } else if (!regrex.test(chequeNo)) {
        showAlert(kony.i18n.getLocalizedString("keyEnterCheque"), kony.i18n.getLocalizedString("info"));
        frmIBChequeServiceStopChequeLanding.txtChequeNo.skin = "txtErrorBG";
        //alert("Please enter valid cheque No ")
        return false;
    } else if (chequeNo.length < 6) {
        showAlert(kony.i18n.getLocalizedString("keyIncorrectCheque"), kony.i18n.getLocalizedString("info"));
        frmIBChequeServiceStopChequeLanding.txtChequeNo.skin = "txtErrorBG";
        //alert("Please enter valid cheque No.")
        return false;
    } else if (fromAccount = "") {
        showAlert(kony.i18n.getLocalizedString("keySelectFromAcc"), kony.i18n.getLocalizedString("info"));
        //alert("Please select valid from account.")
        return false;
    } else return true
}

function btnOnclickNextStopCheque() {
    if (gblOnClickCoverflow == "STOPCHEQUE") {
        if (validateStopChequeService() != true) return false
        else {
            frmIBChequeServiceStopChequeAck.lblCustName.text = glb_selectedData.lblCustName;
            frmIBChequeServiceStopChequeAck.lblBalance.text = glb_selectedData.lblBalance;
            frmIBChequeServiceStopChequeAck.lblAccNo.text = frmIBChequeServiceStopChequeLanding.lblAccNo.text;
            frmIBChequeServiceStopChequeAck.lblAccType2.text = glb_selectedData.lblProductVal;
            frmIBChequeServiceStopChequeAck.lblChequeVal.text = frmIBChequeServiceStopChequeLanding.txtChequeNo.text;
            frmIBChequeServiceStopChequeAck.image247502979411375.src = glb_selectedData.img1;
            // OTPSession-Change for top up here
            frmIBChequeServiceStopChequeLanding.txtChequeNo.skin = "txtIB20pxBlack";
            saveStopChequeServiceParamsInSession();
        }
    } else {
        showAlert(kony.i18n.getLocalizedString("keySelectFromAcc"), kony.i18n.getLocalizedString("info"));
        //alert("Please Select an account");
    }
}

function btnOnClickStopCheque() {
    //gblSwitchToken=true;
    gblStopChequeOTPFlag = "true";
    if (gblSwitchToken == false && gblTokenSwitchFlag == false) {
        IBCScheckTokenFlag();
    }
    if (gblTokenSwitchFlag == true && gblSwitchToken == false) {
        frmIBChequeServiceConfirmation.hbxToken.setVisibility(true);
        frmIBChequeServiceConfirmation.hbox47423104458243.setVisibility(false);
        showingToken();
    } else if (gblTokenSwitchFlag == false && gblSwitchToken == true) {
        frmIBChequeServiceConfirmation.hbxToken.setVisibility(false);
        frmIBChequeServiceConfirmation.hbox47423104458243.setVisibility(true);
        callOTPServiceSCS();
        /*
        showLoadingScreenPopup();
        frmIBChequeServiceConfirmation.show();
        dismissLoadingScreenPopup();
        */
    } else if (gblTokenSwitchFlag == true && gblSwitchToken == true) {
        frmIBChequeServiceConfirmation.hbxToken.setVisibility(false);
        frmIBChequeServiceConfirmation.hbox47423104458243.setVisibility(true);
        callOTPServiceSCS();
    }
    //  callOTPServiceSCS();
    /*if(gblStopChequeOTPFlag!=null && gblStopChequeOTPFlag=="true")
	{
		TMBUtil.DestroyForm(frmIBChequeServiceConfirmation);
		frmIBChequeServiceConfirmation.lblCustName.text=frmIBChequeServiceStopChequeAck.lblCustName.text;
		frmIBChequeServiceConfirmation.lblBalance.text=frmIBChequeServiceStopChequeAck.lblBalance.text;
		frmIBChequeServiceConfirmation.lblAccNo.text=frmIBChequeServiceStopChequeAck.lblAccNo.text;
		frmIBChequeServiceConfirmation.lblAccType2.text=frmIBChequeServiceStopChequeAck.lblAccType2.text
		frmIBChequeServiceConfirmation.lblAccNoVal.text=frmIBChequeServiceStopChequeAck.lblChequeVal.text;
		gblStopChequeOTPFlag="false";
		frmIBChequeServiceConfirmation.show();
	}*/
}

function showingToken() {
    frmIBChequeServiceConfirmation.lblCustName.text = frmIBChequeServiceStopChequeAck.lblCustName.text;
    frmIBChequeServiceConfirmation.lblBalance.text = frmIBChequeServiceStopChequeAck.lblBalance.text;
    frmIBChequeServiceConfirmation.lblAccNo.text = frmIBChequeServiceStopChequeAck.lblAccNo.text;
    frmIBChequeServiceConfirmation.lblAccType2.text = frmIBChequeServiceStopChequeAck.lblAccType2.text
    frmIBChequeServiceConfirmation.lblAccNoVal.text = frmIBChequeServiceStopChequeAck.lblChequeVal.text;
    frmIBChequeServiceConfirmation.hbxOTPBankRef.isVisible = false;
    frmIBChequeServiceConfirmation.hbxOTPsnt.isVisible = false;
    frmIBChequeServiceConfirmation.label476047582115288.text = kony.i18n.getLocalizedString("keyPleaseEnterToken");
    frmIBChequeServiceConfirmation.label476047582115288.isVisible = true;
    gblStopChequeOTPFlag = "false";
    dismissLoadingScreenPopup();
    frmIBChequeServiceConfirmation.show();
    frmIBChequeServiceConfirmation.tbxToken.setFocus(true);
}

function IBCScheckTokenFlag() {
    var inputParam = [];
    inputParam["crmId"] = gblcrmId;
    showLoadingScreen();
    invokeServiceSecureAsync("tokenSwitching", inputParam, IBCSibTokenFlagCallbackfunction);
}

function IBCSibTokenFlagCallbackfunction(status, callbackResponse) {
    if (status == 400) {
        if (callbackResponse["opstatus"] == 0) {
            dismissLoadingScreen();
            if (callbackResponse["deviceFlag"].length == 0) {
                //return
                frmIBChequeServiceConfirmation.hbxToken.isVisible = false;
                frmIBChequeServiceConfirmation.hbox47423104458243.isVisible = true;
                callOTPServiceSCS();
            }
            if (callbackResponse["deviceFlag"].length > 0) {
                var tokenFlag = callbackResponse["deviceFlag"][0]["TOKEN_DEVICE_FLAG"];
                var mediaPreference = callbackResponse["deviceFlag"][0]["MEDIA_PREFERENCE"];
                if (tokenFlag == "Y" && mediaPreference == "Token") {
                    gblTokenSwitchFlag = true;
                    frmIBChequeServiceConfirmation.hbxToken.isVisible = true;
                    frmIBChequeServiceConfirmation.hbox47423104458243.isVisible = false;
                    showingToken()
                } else {
                    frmIBChequeServiceConfirmation.hbxToken.isVisible = false;
                    frmIBChequeServiceConfirmation.hbox47423104458243.isVisible = true;
                    gblTokenSwitchFlag = false;
                    callOTPServiceSCS();
                }
            }
        } else {
            dismissLoadingScreen();
        }
    }
}

function customWidgetSelectEventforChequeService() {
    //var selectedItem = frmIBChequeServiceStopChequeLanding.customFromAccountIB.selectedItem;
    if (frmIBChequeServiceStopChequeLanding.customFromAccountIB.data != null && frmIBChequeServiceStopChequeLanding.customFromAccountIB.data.length > 0) {
        var tLen = frmIBChequeServiceStopChequeLanding.customFromAccountIB.data.length;
        glb_selectedData = frmIBChequeServiceStopChequeLanding.customFromAccountIB.data[gblCWSelectedItem % tLen];
        frmIBChequeServiceStopChequeLanding.lblFromAccount.text = glb_selectedData.lblCustName;
        frmIBChequeServiceStopChequeLanding.lblAccNo.text = glb_selectedData.lblActNoval;
        frmIBChequeServiceStopChequeLanding.imageAcc.src = glb_selectedData.img1;
        gblOnClickCoverflow = "STOPCHEQUE";
    } else {
        frmIBChequeServiceStopChequeLanding.lblFromAccount.text = "";
        frmIBChequeServiceStopChequeLanding.lblAccNo.text = "";
        frmIBChequeServiceStopChequeLanding.imageAcc.src = "";
    }
}

function onClickConfirmStopChequeIB() {
    if (frmIBChequeServiceConfirmation.txtotp.text == "" && gblTokenSwitchFlag == false) {
        showAlert(kony.i18n.getLocalizedString("Receipent_alert_correctOTP"), kony.i18n.getLocalizedString("info"));
        //alert("Please Enter otp");
        return false;
    } else if (frmIBChequeServiceConfirmation.tbxToken.text == "" && gblTokenSwitchFlag == true) {
        alert(kony.i18n.getLocalizedString("Receipent_tokenId"));
        return false;
    } else {
        validateOTPforChequeService();
    }
}

function showStopChequeServiceComplete() {
    /*   frmIBChequeServiceConfirmation.hbxOtpBox.isVisible = false;
       frmIBChequeServiceConfirmation.hboxStatus.isVisible = false;
       frmIBChequeServiceConfirmation.hbxBtn.isVisible = false;
       frmIBChequeServiceConfirmation.lblSuccess.isVisible = true;
       frmIBChequeServiceConfirmation.hbxSuccess.setVisibility(true);
       frmIBChequeServiceConfirmation.btnGoto.isVisible = true;*/
}

function onClickCancelBtnFromOtp() {
    GLOBAL_CHEQUESERVICE_TABLE = "";
    // TMBUtil.DestroyForm(frmIBChequeServiceStopChequeLanding);
    callcustomerAccountChequeService();
}