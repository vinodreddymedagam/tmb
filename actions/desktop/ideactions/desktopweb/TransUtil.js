function setTransPwdFailedError(errMsg) {
    dismissLoadingScreen();
    popupTractPwd.lblPopupTract7.text = errMsg;
    popupTractPwd.lblPopupTract7.skin = lblPopUpErr;
    popupTractPwd.lblPopupTract7.setVisibility(true);
    popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = txtErrorBG;
    popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = txtErrorBG;
    popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text = "";
}

function showTranPwdLockedPopup() {
    dismissLoadingScreen();
    gblRtyCtrVrfyTxPin = "0";
    gblUserLockStatusMB = "03";
    lockUser("03");
    popupTractPwd.dismiss();
    popTransferConfirmOTPLock.show();
}

function setChangeTransPwdError() {
    frmCMChgTransPwd.tbxTranscCrntPwd.skin = txtNormalBG;
    frmCMChgTransPwd.tbxTranscCrntPwdTemp.skin = txtNormalBG;
    frmCMChgTransPwd.txtTransPass.skin = txtNormalBG;
    frmCMChgTransPwd.txtTemp.skin = txtNormalBG;
}

function setTransNewPwdErrorBGnAlert(errMsg, level) {
    frmCMChgTransPwd.txtTransPass.skin = txtErrorBG;
    frmCMChgTransPwd.txtTemp.skin = txtErrorBG;
    showAlert(errMsg, level);
}

function validateChangeTxnPwd() {
    setChangeTransPwdError();
    if (frmCMChgTransPwd.tbxTranscCrntPwdTemp.isVisible) frmCMChgTransPwd.tbxTranscCrntPwd.text = frmCMChgTransPwd.tbxTranscCrntPwdTemp.text;
    if (frmCMChgTransPwd.txtTemp.isVisible) frmCMChgTransPwd.txtTransPass.text = frmCMChgTransPwd.txtTemp.text;
    var info1 = kony.i18n.getLocalizedString("info");
    var curPin = frmCMChgTransPwd.tbxTranscCrntPwd.text;
    if (curPin == null || curPin == '') {
        gblShowPwd++;
        var minCurrPWD = kony.i18n.getLocalizedString("transPwdMsg");
        frmCMChgTransPwd.tbxTranscCrntPwd.skin = txtErrorBG;
        frmCMChgTransPwd.tbxTranscCrntPwdTemp.skin = txtErrorBG;
        showAlert(minCurrPWD, info1);
        return false;
    }
    if (frmCMChgTransPwd.txtTransPass.isVisible || frmCMChgTransPwd.txtTemp.isVisible) {
        var newPin = frmCMChgTransPwd.txtTransPass.text;
        if (newPin == null || newPin == '' || newPin.length < 8 || newPin.length > 20) {
            setTransNewPwdErrorBGnAlert(kony.i18n.getLocalizedString("minNewPwd"), info1);
            return false;
        }
        if (isNotAlphaNumeric(newPin)) {
            gblShowPwd++;
            setTransNewPwdErrorBGnAlert(kony.i18n.getLocalizedString("invalidTransPwd"), info1);
            return false;
        }
        if (newPin == curPin) {
            gblShowPwd++;
            setTransNewPwdErrorBGnAlert(kony.i18n.getLocalizedString("keySameTransPwd"), info1);
            return false;
        }
    }
    temp = kony.i18n.getLocalizedString("AccesPIN");
    showOTPPopup(kony.i18n.getLocalizedString("AccessPin"), "", "", onClickConfirmAccessPIN, 2);
    if (frmCMChgTransPwd.txtTemp.isVisible) {
        cancelTimer();
    }
}

function isNotAlphaNumeric(identifier) {
    var pat1 = /[A-Za-z]/g
    var pat2 = /[0-9]/g
    var isAlpha = pat1.test(identifier);
    var isNum = pat2.test(identifier);
    if (isAlpha == false || isNum == false) {
        return true;
    }
    return false;
}