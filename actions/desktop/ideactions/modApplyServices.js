








function applyServicesCommon(actionCode, IB_FlowStatus, MB_FlowStatus, custStatus) {
	showLoadingScreen();
	gblRetryCountRequestOTP = "0";
	inputParam = {};
	inputParam["actionCode"] = actionCode;
	inputParam["citizenID"] = ""; // Not used
	
	invokeServiceSecureAsync("GetCustomerStatus", inputParam, callBackApplyServicesCommon)
}

function callBackApplyServicesCommon(status, resulttable) {
	if (status == 400) {
		var dismissLoadingScreenFlag = false;
		if (resulttable["opstatus"] == 0) {
			if (resulttable["eligibilityStatus"] == "0") {
				if(flowSpa)
				{
					partyInqMobForApplyMBServiceSpa();
				}
				if (gblApplyServices == 2) {
					if(flowSpa)
					{
					applyMobileBankingSpa();
					}
					else
					{
					applyInternetBanking();
					}
				} else if (gblApplyServices == 3) {
					if(flowSpa)
					{
					applyNewDeviceSpa();
					}
					else
					{
					applyNewDevice();
					}
				} else if (gblApplyServices == 4) {
					if(flowSpa)
					{
					resetMBankingPassSpa();
					}
					else
					{
					resetIBankingPass();
					}
				}
				if (gblApplyServices == 5) {
					// For IB and SPA
				} else {
					
					if(flowSpa)
					{
					dismissLoadingScreenFlag = true; 
					frmApplyMBSPA.scrollboxMain.scrollToEnd();
				    frmApplyMBSPA.show();
					}
					else
					{
					var ifCASAActs = checkTermDepositCount();
					if(ifCASAActs == 0){
						dismissLoadingScreen();
				        showAlert(kony.i18n.getLocalizedString("MB_CommonError_NoSA"), kony.i18n.getLocalizedString("info"),onClickOfAccountDetailsBack);
						return false;
					}else{
					frmApplyInternetBankingMB.scrollboxMain.scrollToEnd();
					frmApplyInternetBankingMB.show();
					}
					}
					
				}
			} else {
				var errorMsg = "";
				if (gblApplyServices == 2) {
					if(flowSpa)
					{
					errorMsg = "Apply MB request rejected";
					}
					else
					{
					errorMsg = kony.i18n.getLocalizedString("keyApplyIBRejectMB");
					}
					
				} else if (gblApplyServices == 3) {
					errorMsg = kony.i18n.getLocalizedString("keyAddDeviceRejectedMB");
				} else if (gblApplyServices == 4) {
					if(flowSpa)
					{
					errorMsg="Unlock Mobile Banking Rejected"
					}
					else
					{
					errorMsg = kony.i18n.getLocalizedString("keyResetIBRejectedMB");
					}
				} else if (gblApplyServices == 5) {
					errorMsg = "Apply MB request rejected";
				}
				showAlertforApplyServ(errorMsg);
			}
		} else {
			alert(" " + resulttable["errmsg"]);
		}
		if( !dismissLoadingScreenFlag){
			kony.application.dismissLoadingScreen();
		}
	}
}
/**
 * description
 * 
 * @returns {}
 */

function onClicSegkApplyServices() {
	var applyServiceSelected = frmApplyServicesMB.segServiceList.selectedIndex[1];
	
	if (applyServiceSelected == 2) {
		gblApplyServices = 2;
		// ActionCode is 11 for ApplyIB
		applyServicesCommon("11", gblIBFlowStatus, gblMBFlowStatus, gblCustomerStatus);
	} else if (applyServiceSelected == 3) {
		// ActionCode is 23 for add device
		gblApplyServices = 3;
		applyServicesCommon("23", gblIBFlowStatus, gblMBFlowStatus, gblCustomerStatus);
	} else if (applyServiceSelected == 4) {
		// ActionCode is 12 for reset
		gblApplyServices = 4;
		applyServicesCommon("12", gblIBFlowStatus, gblMBFlowStatus, gblCustomerStatus);
	}
}
 
function showAlertforApplyServ(errMsg) {
	var alert_seq0_act1 = kony.ui.Alert({
		"message": errMsg,
		"alertType": constants.ALERT_TYPE_ERROR,
		"alertTitle": "Error",
		"yesLabel": "Ok",
		"noLabel": "",
		"alertIcon": "",
		"alertHandler": null
	}, {});
}
 
function applyServicesreqGenActCode() {
	var actionCode = "";
	if (gblApplyServices == 2) {
		if(flowSpa)
		{
		actionCode = "21"
		}
		else
		{
		actionCode = "11";
		}
	} else if (gblApplyServices == 3) {
		actionCode = "23";
	} else if (gblApplyServices == 4) {
		if(flowSpa)
		{
		actionCode = "22";
		}
		else
		{
		actionCode = "12";
		}
	} else if (gblApplyServices == 5) {
		actionCode = "21";
	}
	inputParam = {};
	inputParam["actionCode"] = actionCode;
	var locale = kony.i18n.getCurrentLocale();
	if (locale == "en_US") {
		inputParam["language"] = "EN";
	} else {
		inputParam["language"] = "TH";
	}
	if (flowSpa) {
		channelId = "01";
	} else {
		channelId = "02";
	}
	if (GLOBAL_MB_CHANNEL != null && GLOBAL_MB_CHANNEL != "") {
		if (flowSpa) {
			channelId = GLOBAL_IB_CHANNEL;
		} else {
			channelId = GLOBAL_MB_CHANNEL;
		}
	}
		inputParam["channelId"] = channelId;
	inputParam["mobileNumber"] = gblPHONENUMBER
	invokeServiceSecureAsync("ReqGenActivationCode", inputParam, callBackApplyreqGenActivationCode)
}

 
function callBackApplyreqGenActivationCode(resulttable) {
		
		var flexval;
		if (resulttable["opstatusReqGen"] == 0) {
			if (resulttable["eligibilityStatus"] == "0") {
				if (gblApplyServices == 5) {
					// for IB and SPA
				} else {
					if(flowSpa)
					{
						frmApplyMBConfirmationSPA.scrollboxMain.scrollToEnd();
						frmApplyMBConfirmationSPA.show();
					} else {
						frmApplyInternetBankingConfirmation.scrollboxMain.scrollToEnd();
						frmApplyInternetBankingConfirmation.show();
					}
				}
			} else {
				alert("You are not eligible for activation. Please contact Customer Care.");
			}
		} else {
			alert(" " + resulttable["errMsg"]);
		}
		kony.application.dismissLoadingScreen();
}



function modApplyServiceSPAdoRequest(pwd) {
    var inputParam = {};
 		inputParam["retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
        inputParam["retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
        inputParam["retryCounterVerifyOTP"] = gblVerifyOTPCounter;
        inputParam["password"] = pwd;
        showLoadingScreen();
    if (gblTokenSwitchFlag == true) {
  		inputParam["OTP_TOKEN_FLAG"] = "TOKEN";
    }else{
 		inputParam["OTP_TOKEN_FLAG"] = "OTP";
  	}
  	var activityTypeId = "";
			  	var flex1 = "";
			  	var flex2 = "";
			  	var flex3 = "";
			  	var flex4 = "";
			  	var flex5 = "";
  				switch (parseInt(gblApplyServices.toString())) {
				case 2:
					activityTypeId = "002";
					flex1 = gblUserName;
					flex2 = gblPHONENUMBER;
  					break;
				case 4:
					activityTypeId = "014";
					flex1 = "Active Customer";
					flex2 = "Reset-PWD Customer";
					flex3 = gblUserName;
					flex4 = gblPHONENUMBER;
  					break;
				case 3:
					activityTypeId = "003";
					flex1 = gblUserName;
					flex2 = gblPHONENUMBER;
 					break;
				case 5:
					activityTypeId = "002";
					flex1 = gblUserName;
					flex2 = gblPHONENUMBER;
  					break;	
				default:
					break;
				}
		inputParam["activityTypeID"] = activityTypeId;
		inputParam["activityFlexValues1"] = flex1;	
		inputParam["activityFlexValues2"] = flex2;
		inputParam["activityFlexValues3"] = flex3;		
		inputParam["activityFlexValues4"] = flex4;		
		inputParam["activityFlexValues5"] = flex5;	
		var locale = kony.i18n.getCurrentLocale();
		if (locale == "en_US") {
			inputParam["language"] = "EN";
		} else {
			inputParam["language"] = "TH";
		}	
		invokeServiceSecureAsync("RequestGenActCodeApplyExecute", inputParam, callBackReqGenSPAApplyServices)		
}

function callBackReqGenSPAApplyServices(status, resulttable) {
	popOtpSpa.txttokenspa.text = "";
    if (status == 400) {
        
        if (resulttable["opstatusVPX"] == 0) {
            otplocked = false;
            gblVerifyOTPCounter = "0";
            gblRetryCountRequestOTP = "0";
            getHeader(mobileMethod, kony.i18n.getLocalizedString("keySetPasswordLabel"), 0, 0, 0);
            gblShowPinPwdSecs = kony.os.toNumber(resulttable["showPinPwdSecs"]);
            gblRetryCountRequestOTP = "0";
            gblShowPwdNo = kony.os.toNumber(resulttable["showPinPwdCount"]);
            popOtpSpa.dismiss();
			callBackApplyreqGenActivationCode(resulttable);
            kony.application.dismissLoadingScreen();
        } else if (resulttable["opstatusVPX"] == 8005) {
            if (gblVerifyOTPCounter >= 3) {
                modifyUserUpdate("Disabled", "3");
                alertUserStatusLocked()
            } else {
                popOtpSpa.lblTokenMsg.text = kony.i18n.getLocalizedString("wrongOTP");
				kony.application.dismissLoadingScreen();
            }
            if (resulttable["errCode"] == "VrfyOTPErr00001") {
                gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];

                popOtpSpa.lblPopupTract2Spa.text = kony.i18n.getLocalizedString("wrongOTP");
                popOtpSpa.lblPopupTract4Spa.text = "";

                kony.application.dismissLoadingScreen();
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00002") {
                gblVerifyOTPCounter = "0";
                otplocked = true;
                kony.application.dismissLoadingScreen();
                popOtpSpa.dismiss();
                popTransferConfirmOTPLock.show();
                // calling crmprofileMod to update the user status
                updteuserSpa();

                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00003") {
                kony.application.dismissLoadingScreen();
                showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00003"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00004") {
                kony.application.dismissLoadingScreen();
                showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00004"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00005") {
               kony.application.dismissLoadingScreen();
               showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00005"), kony.i18n.getLocalizedString("info"));
               return false;
            } else {
                kony.application.dismissLoadingScreen();
                showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
                return false;
            }
        } else {
            kony.application.dismissLoadingScreen();
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
        kony.application.dismissLoadingScreen();
    }
}
