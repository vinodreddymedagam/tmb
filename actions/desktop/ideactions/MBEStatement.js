function checkpointMBestmtBousinessHrs(){
	var input_param = {};
    invokeServiceSecureAsync("pointRedeemBousinessHrs", input_param, checkeStmtBusinessHrsCallBack);
}

function checkeStmtBusinessHrsCallBack(status,result){
       if(status == 400){
              if(result["opstatus"] == 0){
              gbleStmtProducts = result;
                     var serviceHrsFlag = result["pointRedeemActBusinessHrsFlag"];
                     if(serviceHrsFlag=="true"){
                     GLOBAL_E_STATEMENT_PRODUCTS=result["E_STATEMENT_PRODUCTS"];
                     GLOBAL_E_STATEMENT_IMAGES=result["E_STATEMENT_IMAGES"];
                           frmMBEStatementProdFeature.show();
                     }else{
                 		var startTime = result["pointRedeemStartTime"];
                 		pntRedeem_StartTime=result["pointRedeemStartTime"];
                 		var endTime = result["pointRedeemEndTime"];
                 		pntRedeem_EndTime=result["pointRedeemEndTime"];
                 		var messageUnavailable = kony.i18n.getLocalizedString("keySoGooODServiceUnavailable");
                 		messageUnavailable = messageUnavailable.replace("{start_time}", startTime);
                 		messageUnavailable = messageUnavailable.replace("{end_time}", endTime);
                 		isMenuShown=false;
                      	showAlertWithCallBack(messageUnavailable, kony.i18n.getLocalizedString("info"),onClickOfAccountDetailsBack);
                        return false;
                     }                    
              }else{
                    	dismissLoadingScreenBasedOnChannel(channel);
						if (resulttable["errMsg"] != null || resulttable["errMsg"] != "") {
								showAlert("Sorry, System found error : " + resulttable["errMsg"], kony.i18n.getLocalizedString("info") );
								return false;
						}else{
								showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
								return false;
						}
              }
       }
}


function frmMBEStatementProdFeaturePreShow(){
changeStatusBarColor();
	var currentLocale = kony.i18n.getCurrentLocale();
	frmMBEStatementProdFeature.lblHdrTxt.text = kony.i18n.getLocalizedString("keyEStmt");
	frmMBEStatementProdFeature.lblEStatementProdFeature.text = kony.i18n.getLocalizedString("keyEStmtProdFeature");
	frmMBEStatementProdFeature.lblEStatementSubscribeFor.text = kony.i18n.getLocalizedString("keyEStmtSubScribeFor");
	frmMBEStatementProdFeature.btnCancel.text = kony.i18n.getLocalizedString('Back');
	frmMBEStatementProdFeature.btnNext.text = kony.i18n.getLocalizedString('Next');
	
	var eStatementProdFeature_image_name="EStatementBriefEN";
	if(currentLocale == "th_TH"){
		eStatementProdFeature_image_name = "EStatementBriefTH";
	}
	var eStatementProdFeature_image="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+eStatementProdFeature_image_name+"&modIdentifier=PRODUCTPACKAGEIMG";
		
	frmMBEStatementProdFeature.imgEStatementProdFeature.src = eStatementProdFeature_image;
	
	var dataSegment=[];
	var prodImg;
	//list of products
	var updatedVal =GLOBAL_E_STATEMENT_PRODUCTS.split(",");
	//images
	var updatedImgs =GLOBAL_E_STATEMENT_IMAGES.split(",");
	for(i=0;i<updatedVal.length;i++){
			prodImg = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+updatedImgs[i]+""+"&modIdentifier=PRODICON";
			var tempSegmentRecord;
			tempSegmentRecord = {"imgProdType":prodImg,"lblProdName":kony.i18n.getLocalizedString(updatedVal[i])};
			dataSegment.push(tempSegmentRecord);
	}
	frmMBEStatementProdFeature.segEStatementProdAllow.setData(dataSegment);
}

function onClickBackEStatement(){
	onCancelOfTouchFrm();
}

function frmMBEStatementTnCPreShow() {
	changeStatusBarColor();
	var currentLocale = kony.i18n.getCurrentLocale();
	frmMBEStatementTnC.lblHdrTxt.text = kony.i18n.getLocalizedString('keyEStmt');
		 
	frmMBEStatementTnC.btnPointCancel.text = kony.i18n.getLocalizedString('Back')
	frmMBEStatementTnC.btnAgree.text = kony.i18n.getLocalizedString('keyAgreeButton')
	
	var input_param = {};
	input_param["localeCd"]=currentLocale;
	input_param["moduleKey"]='TMBEStatement';
	showLoadingScreen();
    invokeServiceSecureAsync("readUTFFile", input_param, setEStatementTnC);	
}

function setEStatementTnC(status,result){
	if(status==400){
		dismissLoadingScreen();
		if(result["opstatus"]==0){
			frmMBEStatementTnC.richTandC.text="";
			frmMBEStatementTnC.richTandC.text=result["fileContent"];
		}else{
			showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
			return false;
		}
	}
}

function onClickEmailTnCMBEStatement() {
	
	showLoadingScreen()
	var inputparam = {};
	inputparam["channelName"] = "Mobile Banking";
	inputparam["channelID"] = "02";
	inputparam["notificationType"] = "Email"; // always email
	inputparam["phoneNumber"] = gblPHONENUMBER;
	inputparam["mail"] = gblEmailId;
	inputparam["customerName"] = gblCustomerName;
	inputparam["localeCd"] = kony.i18n.getCurrentLocale();
    inputparam["moduleKey"] = "TMBEStatement";
	invokeServiceSecureAsync("TCEMailService", inputparam, callBackEmailTnCEStatementMB);
	
}

function callBackEmailTnCEStatementMB(status, result) {
	if (status == 400) {
		if (result["opstatus"] == 0) {
			var StatusCode = result["StatusCode"];
			var Severity = result["Severity"];
			var StatusDesc = result["StatusDesc"];
			if (StatusCode == 0) {
				showAlert(kony.i18n.getLocalizedString("keytermOpenAcnt"), kony.i18n.getLocalizedString("info"));
				dismissLoadingScreen();
			} else {
				dismissLoadingScreen()
				return false;
			}
		} else {
			dismissLoadingScreen()
		}
	}
}
function onClickNextTnCEStatement(){
	var input_param = {};
	invokeServiceSecureAsync("eStmtDetailsCompositeJavaService", input_param, callBackeStmt);	
}
function callBackeStmt(status,result){
	if(status==400){
		dismissLoadingScreen();
		if(result["opstatus"]==0)
		{
			if(result["EStmtAnswerDS"]!=null)
			{
				gblstmtAns=result["EStmtAnswerDS"];
				selectEStmt_CC=gblstmtAns[1]["EStmt_CC"]+",0";
				selectEStmt_RDC=gblstmtAns[2]["EStmt_RDC"]+",0";
				selectEStmt_C2G=gblstmtAns[3]["EStmt_C2G"]+",0";
			}else{
				showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
				return false;
			}
			//Setting Emails to blank by default so that if the address is not coming in service, we can show the correct message.
			gblEStmtRegisterAddr="";
			gblEStmtOfficeAddr="";
			gblEStmtContactAddr="";
			
			if(result["RegfullAddr"]!=null)
			{
				gblEStmtRegisterAddr = result["RegfullAddr"];
			}
			if(result["officefullAddr"]!=null)
			{
				gblEStmtOfficeAddr = result["officefullAddr"];
			}
			if(result["primaryfullAddr"]!=null)
			{
				gblEStmtContactAddr = result["primaryfullAddr"];
			}
			if(result["emailId"]!=null)
			{
				gblEmailAddr = result["emailId"];
			}
			frmMBEStatementLanding.txtEStatementEmail.text="";
			frmMBEStatementLanding.btnNext.setEnabled(false);
			frmMBEStatementLanding.btnNext.skin="btnDisabledGray";
	   		frmMBEStatementLanding.btnNext.focusSkin="btnDisabledGray";
			frmMBEStatementLanding.show();
		}
		else
		{
			showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
			return false;
		}
	}
}
function frmMBEStatementLandingPreShow(){
changeStatusBarColor();
	frmMBEStatementLanding.lblHdrTxt.text = kony.i18n.getLocalizedString("keyEStmt");
	frmMBEStatementLanding.lblEStatementConfirmEmailChooseOption.text = kony.i18n.getLocalizedString("keyEStmtConfirmEmail");
	frmMBEStatementLanding.txtEStatementEmail.placeholder = gblEmailAddr;
	frmMBEStatementLanding.lblEStatementEmailNote.text = kony.i18n.getLocalizedString("keyEStmtNote");
	frmMBEStatementLanding.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
	frmMBEStatementLanding.btnNext.text = kony.i18n.getLocalizedString("Next");

	var dataSegment=[];
	var prodImg;
	var images;
	updatedVal={};
	updatedImgs={};
	emailSkins={};
	paperSkins={};
	var countEStatProds=0;
	if(gblAccountTable!=null)
	{
		glbAllowCreditCard = gblAccountTable["haveCreditCard"] == "yes" && gbleStmtProducts["haveCreditCardProd"]!==undefined && gbleStmtProducts["haveCreditCardImg"]!==undefined;
		glbAllowReadyCash = gblAccountTable["haveReadyCash"] == "yes" && gbleStmtProducts["haveReadyCashProd"]!==undefined && gbleStmtProducts["haveReadyCashImg"]!==undefined;
		glbAllowCash2Go = gblAccountTable["haveCash2Go"] == "yes" && gbleStmtProducts["haveCash2GoProd"]!==undefined && gbleStmtProducts["haveCash2GoImg"]!==undefined;
			
		if(glbAllowCreditCard) 
		{
			updatedVal[countEStatProds] = gbleStmtProducts["haveCreditCardProd"];
			updatedImgs[countEStatProds] = gbleStmtProducts["haveCreditCardImg"];
			//if(!gblFromEStatementEdit)selectEStmt_CC=gblstmtAns[1]["EStmt_CC"]+",0";
			if(selectEStmt_CC.split(",")[0]=="Y")
			{
				emailSkins[countEStatProds] = btnEmailRgtFocus;
				paperSkins[countEStatProds] = btnSmsLeft;
			}
			else
			{
				emailSkins[countEStatProds]  = btnEmailSMS;
				paperSkins[countEStatProds] = btnEmailFocus;
			}
			countEStatProds++;
		}
		if(glbAllowReadyCash) 
		{
			updatedVal[countEStatProds] = gbleStmtProducts["haveReadyCashProd"];
			updatedImgs[countEStatProds] = gbleStmtProducts["haveReadyCashImg"];
			//if(!gblFromEStatementEdit)selectEStmt_RDC=gblstmtAns[2]["EStmt_RDC"]+",0";
			if(selectEStmt_RDC.split(",")[0]=="Y")
			{
				emailSkins[countEStatProds]  = btnEmailRgtFocus;
				paperSkins[countEStatProds] = btnSmsLeft;
			}
			else
			{
				emailSkins[countEStatProds]  = btnEmailSMS;
				paperSkins[countEStatProds] = btnEmailFocus;
			}
			countEStatProds++;
		}
		if(glbAllowCash2Go)
		{
			updatedVal[countEStatProds] = gbleStmtProducts["haveCash2GoProd"];
			updatedImgs[countEStatProds] = gbleStmtProducts["haveCash2GoImg"];
			//if(!gblFromEStatementEdit)selectEStmt_C2G=gblstmtAns[3]["EStmt_C2G"]+",0";
			if(selectEStmt_C2G.split(",")[0]=="Y")
			{
				emailSkins[countEStatProds]  = btnEmailRgtFocus;
				paperSkins[countEStatProds] = btnSmsLeft;
			}
			else
			{
				emailSkins[countEStatProds] = btnEmailSMS;
				paperSkins[countEStatProds] = btnEmailFocus;
			}
			countEStatProds++;
		}
		gblFromEStatementEdit=false;
	}
	for(i=0;i<countEStatProds;i++){
			prodImg = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+updatedImgs[i]+""+"&modIdentifier=PRODICON";
			var tempSegmentRecord;
			var selectEStmt_AnyProduct="";
			if(updatedVal[i] == "keyTMBCreditCard"){
				selectEStmt_AnyProduct=selectEStmt_CC;
			}else if(updatedVal[i] == "keyReadyCash"){
				selectEStmt_AnyProduct=selectEStmt_RDC;
			}else if(updatedVal[i] == "keyCash2Go"){
				selectEStmt_AnyProduct=selectEStmt_C2G;
			}
			if(selectEStmt_AnyProduct.split(",")[0] == "N" && selectEStmt_AnyProduct.split(",")[1] != "0"){
				var addressSelectedflag = selectEStmt_AnyProduct.split(",")[1];
				var address = "";
				var addressNote = "";
				var registerAddressButtonSkin =btnRegisterAddrGrey;
				var officeAddressButtonSkin =btnOfficeAddrGrey;
				var contactAddressButtonSkin =btnContactAddrGrey;
				if(addressSelectedflag == "1"){
					address = gblEStmtRegisterAddr;
					addressNote = kony.i18n.getLocalizedString("keyEStmtNoteForRegisteredAndOffice");
					registerAddressButtonSkin = btnRegisterAddr;
					addressTypeFlag="1";
				}else if(addressSelectedflag == "2"){
					address = gblEStmtOfficeAddr;
					addressNote = kony.i18n.getLocalizedString("keyEStmtNoteForRegisteredAndOffice");
					officeAddressButtonSkin = btnOfficeAddr;
					addressTypeFlag="2"
				}else{
					address = gblEStmtContactAddr;
					addressNote = kony.i18n.getLocalizedString("keyEStmtNoteForContact");
					contactAddressButtonSkin = btnContactAddr;
					addressTypeFlag="3";
				}
				tempSegmentRecord = {
				"imgProdType":prodImg,
				"lblProdName":kony.i18n.getLocalizedString(updatedVal[i]),
				"btnEmailOption":{text:kony.i18n.getLocalizedString("email"),skin:emailSkins[i]},
				"btnPaperOption":{text:kony.i18n.getLocalizedString("keyEStmtPaper"),skin:paperSkins[i]},
				"hbxAddressButtons":{isVisible:true},
				"btnRegisterAddr":{text:kony.i18n.getLocalizedString("keyEStmtRegAddr"), skin:registerAddressButtonSkin},
				"btnOfficeAddr":{text:kony.i18n.getLocalizedString("keyEStmtOfficeAddr"), skin:officeAddressButtonSkin},
				"btnContactAddr":{text:kony.i18n.getLocalizedString("keyEStmtContactAddr"), skin:contactAddressButtonSkin},
				"lblAddress":{text:address, isVisible:true},
				"lblAddressNote":{text:addressNote, isVisible:true},
				"lblNoAddressSelected":{isVisible:false},
				"addressTypeFlag":addressTypeFlag,
				"productType":updatedVal[i]
				}
			}else{
				tempSegmentRecord = {
				"imgProdType":prodImg,
				"lblProdName":kony.i18n.getLocalizedString(updatedVal[i]),
				"btnEmailOption":{text:kony.i18n.getLocalizedString("email"),skin:emailSkins[i]},
				"btnPaperOption":{text:kony.i18n.getLocalizedString("keyEStmtPaper"),skin:paperSkins[i]},
				"hbxAddressButtons":{isVisible:false},
				"btnRegisterAddr":{text:kony.i18n.getLocalizedString("keyEStmtRegAddr"), skin:registerAddressButtonSkin},
				"btnOfficeAddr":{text:kony.i18n.getLocalizedString("keyEStmtOfficeAddr"), skin:officeAddressButtonSkin},
				"btnContactAddr":{text:kony.i18n.getLocalizedString("keyEStmtContactAddr"), skin:contactAddressButtonSkin},
				"lblAddress":{isVisible:false},
				"lblAddressNote":{isVisible:false},
				"lblNoAddressSelected":{isVisible:false},
				"addressTypeFlag":"0",
				"productType":updatedVal[i]
				}
			}	
			dataSegment.push(tempSegmentRecord);
				
	}
	frmMBEStatementLanding.segEStatementProdAllow.setData(dataSegment);
}

function onClickEStmtEmailPaperOption(widgetName){
	var updatedSegmentRecord;
	var selectedSegmentRecord;
	var selectedIndex = frmMBEStatementLanding.segEStatementProdAllow.selectedIndex[1];
	var EmailSkin = "";
	var PaperSkin = "";
	var selectEStmt_AnyProduct="";
	var showAddressBar =false;  
	if(widgetName == "btnPaperOption"){
		EmailSkin = btnEmailSMS;
		PaperSkin = btnEmailFocus;
		showAddressBar = true;
	}else{
		EmailSkin = btnEmailRgtFocus;
		PaperSkin = btnSmsLeft;
	}
	selectedSegmentRecord = frmMBEStatementLanding.segEStatementProdAllow.data[selectedIndex];
	updatedSegmentRecord = {
		"imgProdType":selectedSegmentRecord["imgProdType"],
		"lblProdName":selectedSegmentRecord["lblProdName"],
		"btnEmailOption":{text:kony.i18n.getLocalizedString("email"),skin:EmailSkin},
		"btnPaperOption":{text:kony.i18n.getLocalizedString("keyEStmtPaper"),skin:PaperSkin},
		"hbxAddressButtons":{isVisible:showAddressBar},
		"btnRegisterAddr":{text:kony.i18n.getLocalizedString("keyEStmtRegAddr"), skin:btnRegisterAddrGrey},
		"btnOfficeAddr":{text:kony.i18n.getLocalizedString("keyEStmtOfficeAddr"), skin:btnOfficeAddrGrey},
		"btnContactAddr":{text:kony.i18n.getLocalizedString("keyEStmtContactAddr"), skin:btnContactAddrGrey},
		"lblAddress":{isVisible:false},
		"lblAddressNote":{isVisible:false},
		"lblNoAddressSelected":{text:kony.i18n.getLocalizedString("keyEStmtDefault"),isVisible:showAddressBar},
		"addressTypeFlag":"0",
		"productType":selectedSegmentRecord["productType"]
	}
	if(showAddressBar){
		selectEStmt_AnyProduct="N,0";
	}else{
		selectEStmt_AnyProduct="Y,0";
	}	
	if(selectedSegmentRecord["productType"] == "keyTMBCreditCard"){
		selectEStmt_CC=selectEStmt_AnyProduct;
	}else if(selectedSegmentRecord["productType"] == "keyReadyCash"){
		selectEStmt_RDC=selectEStmt_AnyProduct;
	}else if(selectedSegmentRecord["productType"] == "keyCash2Go"){
		selectEStmt_C2G=selectEStmt_AnyProduct;
	}
	frmMBEStatementLanding.segEStatementProdAllow.setDataAt(updatedSegmentRecord, selectedIndex);
	checkNextButtonEnableEStatementLanding("");
}

function onClickEStmtChooseAddressButton(widgetName){
	var updatedSegmentRecord;
	var selectedSegmentRecord;
	var selectedIndex = frmMBEStatementLanding.segEStatementProdAllow.selectedIndex[1];
	selectedSegmentRecord = frmMBEStatementLanding.segEStatementProdAllow.data[selectedIndex];
	var address = kony.i18n.getLocalizedString("keyEStmtNoAddress");
	var addressNote = "";
	var selectEStmt_AnyProduct="";
	var registerAddressButtonSkin =btnRegisterAddrGrey;
	var officeAddressButtonSkin =btnOfficeAddrGrey;
	var contactAddressButtonSkin =btnContactAddrGrey;
	var addressTypeFlag ="0"; 
	if(widgetName == "btnRegisterAddr"){
		if(gblEStmtRegisterAddr!=""){
			address = gblEStmtRegisterAddr;
			addressNote = kony.i18n.getLocalizedString("keyEStmtNoteForRegisteredAndOffice");
			addressTypeFlag="1";
		}
		registerAddressButtonSkin = btnRegisterAddr;
	}else if(widgetName == "btnOfficeAddr"){
		if(gblEStmtOfficeAddr!=""){
			address = gblEStmtOfficeAddr;
			addressNote = kony.i18n.getLocalizedString("keyEStmtNoteForRegisteredAndOffice");
			addressTypeFlag="2"
		}
		officeAddressButtonSkin = btnOfficeAddr;
	}else{
		if(gblEStmtContactAddr!=""){
			address = gblEStmtContactAddr;
			addressNote = kony.i18n.getLocalizedString("keyEStmtNoteForContact");
			addressTypeFlag="3";
		}
		contactAddressButtonSkin = btnContactAddr;	
	}
	
	selectEStmt_AnyProduct="N,"+addressTypeFlag;
	if(selectedSegmentRecord["productType"] == "keyTMBCreditCard"){
		selectEStmt_CC=selectEStmt_AnyProduct;
	}else if(selectedSegmentRecord["productType"] == "keyReadyCash"){
		selectEStmt_RDC=selectEStmt_AnyProduct;
	}else if(selectedSegmentRecord["productType"] == "keyCash2Go"){
		selectEStmt_C2G=selectEStmt_AnyProduct;
	}
	updatedSegmentRecord = {
		"imgProdType":selectedSegmentRecord["imgProdType"],
		"lblProdName":selectedSegmentRecord["lblProdName"],
		"btnEmailOption":{text:kony.i18n.getLocalizedString("email"),skin:btnEmailSMS},
		"btnPaperOption":{text:kony.i18n.getLocalizedString("keyEStmtPaper"),skin:btnEmailFocus},
		"hbxAddressButtons":{isVisible:true},
		"btnRegisterAddr":{text:kony.i18n.getLocalizedString("keyEStmtRegAddr"), skin:registerAddressButtonSkin},
		"btnOfficeAddr":{text:kony.i18n.getLocalizedString("keyEStmtOfficeAddr"), skin:officeAddressButtonSkin},
		"btnContactAddr":{text:kony.i18n.getLocalizedString("keyEStmtContactAddr"), skin:contactAddressButtonSkin},
		"lblAddress":{text:address, isVisible:true},
		"lblAddressNote":{text:addressNote, isVisible:true},
		"lblNoAddressSelected":{isVisible:false},
		"addressTypeFlag":addressTypeFlag,
		"productType":selectedSegmentRecord["productType"]
	}
	frmMBEStatementLanding.segEStatementProdAllow.setDataAt(updatedSegmentRecord, selectedIndex);
	checkNextButtonEnableEStatementLanding("");
}

function checkNextButtonEnableEStatementLanding(widget_id){
	gblEStatementEMail="";
	var enableButton=true;
	if(frmMBEStatementLanding.txtEStatementEmail.text != ""){
		gblEStatementEMail = frmMBEStatementLanding.txtEStatementEmail.text;
	}else{
		gblEStatementEMail = frmMBEStatementLanding.txtEStatementEmail.placeholder;
	}
	var emailNotMatch = (frmMBEStatementLanding.txtEStatementEmail.text != frmMBEStatementLanding.txtEStatementEmail.placeholder);
	if(frmMBEStatementLanding.txtEStatementEmail.text == ""){
	 	if(checkOptionsWereChanged() == "1"){
	 		if(!checkEStmtAllPaper()){
	 			if(!emailValidatn(gblEStatementEMail)){
	 				enableButton=false;
	 			}else{
	 				enableButton=true;
	 			}
	 		}else{
	 			enableButton=true;
	 		}
		}else{
			enableButton=false;
		}
	}else{
		if (emailValidatn(gblEStatementEMail) && emailNotMatch) {
			if(checkOptionsWereChanged("fromEmail") == "1")
				enableButton=true;
			else
				enableButton=false;	
	    }else if(!emailValidatn(gblEStatementEMail)){
	    	if(widget_id == "")
	    		showAlertWithCallBack(kony.i18n.getLocalizedString("invalidEmail"), kony.i18n.getLocalizedString("info"),putFocusEStatementTextbox);
	    	enableButton=false;
	    }else {
	    	enableButton=false;
    	}
    }
    
    if(enableButton){
    	frmMBEStatementLanding.btnNext.setEnabled(true);
       	frmMBEStatementLanding.btnNext.skin="btnBlueSkin";
	   	frmMBEStatementLanding.btnNext.focusSkin="btnBlueSkin";
    }else{
    	frmMBEStatementLanding.btnNext.setEnabled(false);
       	frmMBEStatementLanding.btnNext.skin="btnDisabledGray";
	   	frmMBEStatementLanding.btnNext.focusSkin="btnDisabledGray";
    }
}

function checkOptionsWereChanged(Email){
	var changeInOptionFlag = 0;
	if(gblstmtAns[1]["EStmt_CC"] != selectEStmt_CC.split(",")[0] || selectEStmt_CC.split(",")[1]!="0"){
		changeInOptionFlag++;
		if(selectEStmt_CC.split(",")[0] == "N" && selectEStmt_CC.split(",")[1] == "0")return "2";
	}
	if(gblstmtAns[2]["EStmt_RDC"] != selectEStmt_RDC.split(",")[0] || selectEStmt_RDC.split(",")[1]!="0"){
		changeInOptionFlag++;
		if(selectEStmt_RDC.split(",")[0] == "N" && selectEStmt_RDC.split(",")[1] == "0")return "2";
	}
	if(gblstmtAns[3]["EStmt_C2G"] != selectEStmt_C2G.split(",")[0] || selectEStmt_C2G.split(",")[1]!="0"){
		changeInOptionFlag++;
		if(selectEStmt_C2G.split(",")[0] == "N" && selectEStmt_C2G.split(",")[1] == "0")return "2";
	}
	if(!checkEStmtAllPaper() && Email=="fromEmail"){
		changeInOptionFlag++;
	}
	//To check if anything was changed on the screen
	if(changeInOptionFlag >0)return "1";
	return "3";
}

function checkEStmtAllPaper(){
	var passCondition=0;
	if(glbAllowCreditCard && selectEStmt_CC.split(",")[0] == "N"){
		passCondition++;
	}else if(!glbAllowCreditCard){
		passCondition++;
	}
	if(glbAllowReadyCash && selectEStmt_RDC.split(",")[0] == "N"){
		passCondition++;
	}else if(!glbAllowReadyCash){
		passCondition++;
	}
	if(glbAllowCash2Go && selectEStmt_C2G.split(",")[0] == "N"){
		passCondition++;
	}else if(!glbAllowCash2Go){
		passCondition++;
	}
	if(passCondition == 3)return true;	
	return false;			
}

function checkEmailFormatEstatement(){
	if(frmMBEStatementLanding.txtEStatementEmail.text != ""){
		if (emailValidatn(frmMBEStatementLanding.txtEStatementEmail.text) == false) {
	        //frmMBEStatementLanding.txtEStatementEmail.skin = txtErrorBG;
	        //frmMBEStatementLanding.txtEStatementEmail.focusSkin = txtErrorBG;
	        showAlertWithCallBack(kony.i18n.getLocalizedString("invalidEmail"), kony.i18n.getLocalizedString("info"),putFocusEStatementTextbox);
	    }
	}    
}

function putFocusEStatementTextbox(){
	frmMBEStatementLanding.txtEStatementEmail.setFocus(true);
}


function onClickEStatementLandingNext(){
	showLoadingScreen();
	var inputParam = {};
	if(frmMBEStatementLanding.txtEStatementEmail.text != "" && !checkEStmtAllPaper()){
		inputParam["eMailEStatement"] = frmMBEStatementLanding.txtEStatementEmail.text;
	}else{
		inputParam["eMailEStatement"] = frmMBEStatementLanding.txtEStatementEmail.placeholder;
	}
	inputParam["EStmtCC"] = selectEStmt_CC;
	inputParam["EStmtRDC"] = selectEStmt_RDC;
	inputParam["EStmtC2G"] = selectEStmt_C2G;

    invokeServiceSecureAsync("saveEStatementSession", inputParam, saveEStatementInSessionCallbackfunctionMB);
}

function saveEStatementInSessionCallbackfunctionMB(status, resulttable){
	if (status == 400) {
		dismissLoadingScreen();
		if (resulttable["opstatus"] == "0") {
			//Show the Confirm Form
			frmMBEStatementConfirmation.show();
		}
		else{
			showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
			return false;
		}
	}	
}

function frmMBEStatementConfirmationPreShow(){
	changeStatusBarColor();
	// Do validation like Email is correct and/or changed or not, also Options were changed.
	if(frmMBEStatementLanding.txtEStatementEmail.text != "" && !checkEStmtAllPaper()){
		gblEStatementEMail = frmMBEStatementLanding.txtEStatementEmail.text;
	}else{
		gblEStatementEMail = frmMBEStatementLanding.txtEStatementEmail.placeholder;
	}
    var segLength = frmMBEStatementLanding.segEStatementProdAllow.data.length;
	var segData = frmMBEStatementLanding.segEStatementProdAllow.data;
	var sendByValue ="";
	var addressLabelVisible=false;
	var addressType="";
	var dataSegment=[];
	for(i=0;i<segLength;i++){
		if(segData[i]["btnEmailOption"]["skin"] == btnEmailSMS){
			sendByValue = kony.i18n.getLocalizedString("keyEStmtPaper");
			if(segData[i]["lblAddress"]["isVisible"]){
				if(segData[i]["addressTypeFlag"] == "1"){
					addressType="keyEStmtRegAddr";
				}else if(segData[i]["addressTypeFlag"] == "2"){
					addressType="keyEStmtOfficeAddr";
				}else{
					addressType="keyEStmtContactAddr";
				}
				addressLabelVisible=true;
			}else{
				addressLabelVisible=false;
			}	
		}else{
			sendByValue = kony.i18n.getLocalizedString("email");
			addressLabelVisible=false;
		}
		var tempSegRecord ={
			"imgProdType":segData[i]["imgProdType"],
			"lblProdName":kony.i18n.getLocalizedString(updatedVal[i]),
			"lblSendBy":{text:kony.i18n.getLocalizedString("keyEStmtSendBy")},
			"lblSendByValue":{text:sendByValue},
			"lblAddress":{text:kony.i18n.getLocalizedString(addressType),isVisible:addressLabelVisible},
			"lblAddressValue":{text:segData[i]["lblAddress"]["text"],isVisible:addressLabelVisible}
		}
		dataSegment.push(tempSegRecord); 
	}
	frmMBEStatementConfirmation.segEStatementProdAllow.setData(dataSegment);
	if(checkEStmtAllPaper()){
		frmMBEStatementConfirmation.lblEmailAddress.isVisible=false;
	}else{
		frmMBEStatementConfirmation.lblEmailAddress.isVisible=true;
	}
	frmMBEStatementConfirmation.lblHdrTxt.text=kony.i18n.getLocalizedString("keyConfirmation");
	frmMBEStatementConfirmation.lblEStatementConfirmEmailChooseOption.text = kony.i18n.getLocalizedString("keyEStmtConfirm");
	frmMBEStatementConfirmation.lblEmailAddress.text=kony.i18n.getLocalizedString("keyEmail")+gblEStatementEMail;
	frmMBEStatementConfirmation.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
	frmMBEStatementConfirmation.btnNext.text = kony.i18n.getLocalizedString("keyConfirm"); 
}

function onClickEStatementConfirmationNext(){
	if(frmMBEStatementLanding.txtEStatementEmail.text != "" && !checkEStmtAllPaper()){
		showTransactionPasswordPopupEStatement();
	}else{
		eStatementTxnPasswordConfirmation("*");
	}	
}

function showTransactionPasswordPopupEStatement(){
    var lblText = kony.i18n.getLocalizedString("transPasswordSub");
	var refNo = "";
	var mobNO = "";
	showLoadingScreen();
	showOTPPopup(lblText, refNo, mobNO, eStatementTxnPasswordConfirmation, 3);
}

function eStatementTxnPasswordConfirmation(tranPassword){
	if (tranPassword == null || tranPassword == '') {
		setTransPwdFailedError(kony.i18n.getLocalizedString("emptyMBTransPwd"));
		return false;
	}
	else
	{
		showLoadingScreen();
		popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("keyTransPwdMsg");
		popupTractPwd.lblPopupTract7.skin = lblPopupLabelTxt;
		popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = tbxPopupBlue;
		popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = tbxPopupBlue;
		popupTractPwd.hbxPopupTranscPwd.skin = tbxPopupBlue;
    	var inputParam = {};
        inputParam["password"] = tranPassword;
        inputParam["notificationAdd_appID"] = appConfig.appId;
        inputParam["verifyPwdMB_loginModuleId"] = "MB_TxPwd";
        inputParam["verifyPwdMB_retryCounterVerifyAccessPin"] = "0";
        inputParam["verifyPwdMB_retryCounterVerifyTransPwd"] = "0";
        inputParam["eMailEStatement"] = gblEStatementEMail;
        inputParam["EStmtCC"] = selectEStmt_CC;		//(Y)Email || (N)Paper , AddressType
        inputParam["EStmtRDC"] = selectEStmt_RDC;
        inputParam["EStmtC2G"] = selectEStmt_C2G;
        
        invokeServiceSecureAsync("eStmtModifyCompositeJavaService", inputParam, callBackeStmtModifyCompJavaServiceMB);
	}
}

function callBackeStmtModifyCompJavaServiceMB(status,resulttable){
	if(status == 400){
		dismissLoadingScreen();
		if(resulttable["opstatus"] == 0){
			popupTractPwd.dismiss();
			if(resulttable["pointRedeemActBusinessHrsFlag"] == "false") 
			{
				var startTime = resulttable["pointRedeemStartTime"];
         		var endTime = resulttable["pointRedeemEndTime"];
         		var messageUnavailable = kony.i18n.getLocalizedString("keySoGooODServiceUnavailable");
         		messageUnavailable = messageUnavailable.replace("{start_time}", startTime);
         		messageUnavailable = messageUnavailable.replace("{end_time}", endTime);
               	showAlertWithCallBack(messageUnavailable, kony.i18n.getLocalizedString("info"),onClickOfAccountDetailsBack);
               	return false;
			}
			else
			{
				frmMBEStatementComplete.imgEStatementComplete.isVisible=true;
				frmMBEStatementComplete.imgEStatementInComplete.isVisible=false;
				frmMBEStatementComplete.show();
			}
		}else if (resulttable["errCode"] == "VrfyTxPWDErr00001" || resulttable["errCode"] == "VrfyTxPWDErr00002") {
			setTransPwdFailedError(kony.i18n.getLocalizedString("invalidTxnPwd"));
		}else if (resulttable["errCode"] == "VrfyTxPWDErr00003") {
			showTranPwdLockedPopup();
		}else if (resulttable["opstatus"] == 8005) {
			dismissLoadingScreen();
            if (resulttable["errCode"] == "VrfyOTPErr00001") {
                gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
                dismissLoadingScreen();
                alert("" + kony.i18n.getLocalizedString("invalidOTP"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00002") {
                dismissLoadingScreen();
                alert("" + kony.i18n.getLocalizedString("ECVrfyOTPErr"));
                //startRcCrmUpdateProfilBPIB("04");
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00005") {
                dismissLoadingScreen();
                alert("" + kony.i18n.getLocalizedString("KeyTokenSerialNumError"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00006") {
            	 dismissLoadingScreen();
                gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
                alert("" + resulttable["errMessage"]);
                return false;
            }else if (resulttable["errCode"] == "VrfyOTPErr00004") {
                dismissLoadingScreen();
                alert("" + resulttable["errMsg"]);
                return false;
            }else{
             	dismissLoadingScreen();
              	alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
                return false;
            }
        }
		else{
			popupTractPwd.dismiss();
			frmMBEStatementComplete.imgEStatementComplete.isVisible=false;
			frmMBEStatementComplete.imgEStatementInComplete.isVisible=true;
			frmMBEStatementComplete.show();
		}
	}
}

function frmMBEStatementCompletePreShow(){
	changeStatusBarColor();
	frmMBEStatementComplete.lblHdrTxt.text=kony.i18n.getLocalizedString("Complete");
	if(frmMBEStatementComplete.imgEStatementComplete.isVisible){
		frmMBEStatementComplete.lblCompleteMessage.text = kony.i18n.getLocalizedString("keyEStmtSuccess");
	}else{
		frmMBEStatementComplete.lblCompleteMessage.text = kony.i18n.getLocalizedString("keyEStmtFail");
	}	
	frmMBEStatementComplete.btnEStatementReturn.text = kony.i18n.getLocalizedString("keyReturn");
	adjustBannerMB("frmMBEStatementComplete");
}
function onClickEstatementProducts(){
	//frmMBEStatementTnC.richTandC.top = ".";
	TMBUtil.DestroyForm(frmMBEStatementTnC);
	frmMBEStatementTnC.show();
}