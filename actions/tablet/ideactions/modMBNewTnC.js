//const for term condition flow
var TNC_FLOW_TMB_BLOCK_CREDIT_CARD = "TMB_BLOCK_CREDIT_CARD";
var TNC_FLOW_TMB_NEW_DEBIT_CARD = "TMB_NEW_DEBIT_CARD";

//service & service call back

function callServiceReadUTFFile(isReloadRichtextTnC) {
    var input_param = {};
    var locale = kony.i18n.getCurrentLocale();
    if (locale == "en_US") {
        input_param["localeCd"] = "en_US";
    } else {
        input_param["localeCd"] = "th_TH";
    }
    var moduleKey = "";
    if (gblMBNewTncFlow == TNC_FLOW_TMB_BLOCK_CREDIT_CARD) {
        moduleKey = "TMBBlockCreditCard";
    } else if (gblMBNewTncFlow == TNC_FLOW_TMB_NEW_DEBIT_CARD) {
        moduleKey = "TMBRequestNewDebitCard";
    }
    input_param["moduleKey"] = moduleKey;
    showLoadingScreen();
    if (typeof isReloadRichtextTnC !== 'undefined' && isReloadRichtextTnC == true) {
        invokeServiceSecureAsync("readUTFFile", input_param, frmMBNewTncReloadCB);
    } else {
        invokeServiceSecureAsync("readUTFFile", input_param, callServiceReadUTFFileCB);
    }
}

function frmMBNewTncReloadCB(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
            frmMBNewTnC.btnRight.setVisibility(true);
            frmMBNewTnC.btnnext.text = kony.i18n.getLocalizedString("keyAgreeButton");
            frmMBNewTnC.btnback.text = kony.i18n.getLocalizedString("Back");
            frmMBNewTnC.richtextTnC.text = result["fileContent"];
            frmMBNewTnC.lblDescSubTitle.setFocus(true);
            dismissLoadingScreen();
        } else {
            dismissLoadingScreen();
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}

function callServiceReadUTFFileCB(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
            frmMBNewTnC.btnRight.setVisibility(true);
            frmMBNewTnC.btnnext.text = kony.i18n.getLocalizedString("keyAgreeButton");
            frmMBNewTnC.btnback.text = kony.i18n.getLocalizedString("Back");
            frmMBNewTnC.richtextTnC.text = result["fileContent"];
            frmMBNewTnC.lblDescSubTitle.setFocus(true);
            dismissLoadingScreen();
            frmMBNewTnC.show();
        } else {
            dismissLoadingScreen();
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}

function callServiceTCEMailService(moduleKey) {
    //call notification add service for email with the tnc keyword
    showLoadingScreen()
    var inputparam = {};
    inputparam["channelName"] = "Mobile Banking";
    inputparam["channelID"] = "02";
    inputparam["notificationType"] = "Email"; // always email
    inputparam["phoneNumber"] = gblPHONENUMBER;
    inputparam["mail"] = gblEmailId;
    inputparam["customerName"] = gblCustomerName;
    inputparam["localeCd"] = kony.i18n.getCurrentLocale();
    inputparam["moduleKey"] = moduleKey;
    //inputparam["fileNameOpen"] = fileNameOpen;
    invokeServiceSecureAsync("TCEMailService", inputparam, callServiceTCEMailServiceCB);
}

function callServiceTCEMailServiceCB(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
            var StatusCode = result["StatusCode"];
            var Severity = result["Severity"];
            var StatusDesc = result["StatusDesc"];
            if (StatusCode == "0") {
                showAlert(kony.i18n.getLocalizedString("keytermOpenAcnt"), kony.i18n.getLocalizedString("info"));
                dismissLoadingScreen();
            } else {
                dismissLoadingScreen();
                return false;
            }
        } else {
            dismissLoadingScreen();
        }
    }
}

// event

function onClickBtnNextFrmMBNewTnC() {
    if (gblMBNewTncFlow == TNC_FLOW_TMB_BLOCK_CREDIT_CARD) {
        frmMBNewTncBlockCCOnClickBtnNext();
    } else if (gblMBNewTncFlow == TNC_FLOW_TMB_NEW_DEBIT_CARD) {
        frmMBNewTncNewDCOnClickBtnNext();
    } else if (gblMBNewTncFlow == TNC_FLOW_TMB_CREDIT_CARD_ACTIVATION) {
        frmMBNewTncCreditCardClickBtnNext();
    } else if (gblMBNewTncFlow == TNC_FLOW_TMB_DEBIT_CARD_ACTIVATION) {
        frmMBNewTncDebitCardClickBtnNext();
    } else if (gblMBNewTncFlow == TNC_FLOW_TMB_READY_CASH_ACTIVATION) {
        frmMBNewTncReadyCashClickBtnNext();
    }
}

function onClickBtnBackFrmMBNewTnC() {
    if (gblMBNewTncFlow == TNC_FLOW_TMB_BLOCK_CREDIT_CARD) {
        frmMBNewTncBlockCCOnClickBtnBack();
    } else if (gblMBNewTncFlow == TNC_FLOW_TMB_NEW_DEBIT_CARD) {
        frmMBNewTncNewDCOnClickBtnBack();
    } else if (gblMBNewTncFlow == TNC_FLOW_TMB_CREDIT_CARD_ACTIVATION) {
        btnCancelEvent();
    } else if (gblMBNewTncFlow == TNC_FLOW_TMB_DEBIT_CARD_ACTIVATION) {
        btnCancelEvent();
    } else if (gblMBNewTncFlow == TNC_FLOW_TMB_READY_CASH_ACTIVATION) {
        btnCancelEvent();
    }
}

function onClickBtnEmailFrmMBNewTnC() {
    popupConfrmDelete.lblheader.text = kony.i18n.getLocalizedString("TncInfoHeader");
    popupConfrmDelete.btnpopConfDelete.text = kony.i18n.getLocalizedString("keyOK");
    popupConfrmDelete.btnPopupConfCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
    popupConfrmDelete.btnpopConfDelete.onClick = onClickPopUpOkBtnEmailFrmMBNewTnC;
    popupConfrmDelete.lblPopupConfText.text = kony.i18n.getLocalizedString("keytermOpenAcnt");
    popupConfrmDelete.show();
}

function onClickPopUpOkBtnEmailFrmMBNewTnC() {
    popupConfrmDelete.dismiss();
    if (gblMBNewTncFlow == TNC_FLOW_TMB_BLOCK_CREDIT_CARD) {
        callServiceTCEMailService("TMBBlockCreditCard");
    } else if (gblMBNewTncFlow == TNC_FLOW_TMB_NEW_DEBIT_CARD) {
        callServiceTCEMailService("TMBRequestNewDebitCard");
    } else if (gblMBNewTncFlow == TNC_FLOW_TMB_CREDIT_CARD_ACTIVATION) {
        callServiceTCEMailService("TMBCreditCardActivation");
    } else if (gblMBNewTncFlow == TNC_FLOW_TMB_DEBIT_CARD_ACTIVATION) {
        callServiceTCEMailService("TMBDebitCardActivation");
    } else if (gblMBNewTncFlow == TNC_FLOW_TMB_READY_CASH_ACTIVATION) {
        callServiceTCEMailService("TMBReadyCashActivation");
    }
}

function onClickBtnPdfFrmMBNewTnC() {
    if (gblMBNewTncFlow == TNC_FLOW_TMB_BLOCK_CREDIT_CARD) {
        onClickDownloadTnC("pdf", "TMBBlockCreditCard");
    } else if (gblMBNewTncFlow == TNC_FLOW_TMB_NEW_DEBIT_CARD) {
        onClickDownloadTnC("pdf", "TMBRequestNewDebitCard");
    } else if (gblMBNewTncFlow == TNC_FLOW_TMB_CREDIT_CARD_ACTIVATION) {
    	onClickDownloadTnC("pdf", "TMBCreditCardActivation");
    } else if (gblMBNewTncFlow == TNC_FLOW_TMB_DEBIT_CARD_ACTIVATION) {
    	onClickDownloadTnC("pdf", "TMBDebitCardActivation");
    } else if (gblMBNewTncFlow == TNC_FLOW_TMB_READY_CASH_ACTIVATION) {
    	onClickDownloadTnC("pdf", "TMBReadyCashActivation");
    }
}