







var s = [0, 0];
var n = 0;


function setRowTemplate(currForm) {
	currForm.segSlider.containerWeight = 100;
	currForm.segSlider.widgetDataMap = {
		lblACno: "lblACno",
		lblAcntType: "lblAcntType",
		img1: "img1",
		lblCustName: "lblCustName",
		lblBalance: "lblBalance",
		lblActNoval: "lblActNoval",
		lblDummy: "lblDummy",
		lblSliderAccN1: "lblSliderAccN1",
		lblSliderAccN2: "lblSliderAccN2"
	}
	currForm.segSlider.data = [{
		lblACno: kony.i18n.getLocalizedString("AccountNo"),
		lblAcntType: "TMB Savings Account1",
		img1: "piggyico.png",
		lblCustName: "Joseph Bing",
		lblBalance: "10,000,000.00 ?",
		lblActNoval: "001-2-34567-8",
		lblSliderAccN1: kony.i18n.getLocalizedString("Product"),
		lblSliderAccN2: " TMB Saving Account",
		lblDummy: "-",
		template: hbxSliderTemp1
	}, {
		lblACno: kony.i18n.getLocalizedString("AccountNo"),
		lblAcntType: "TMB Savings Account1",
		img1: "piggyico.png",
		lblCustName: "Joseph Bing",
		lblBalance: "10,000,000.00 ?",
		lblActNoval: "001-2-34567-8",
		lblSliderAccN1: kony.i18n.getLocalizedString("Product"),
		lblSliderAccN2: " TMB Saving Account",
		lblDummy: "-",
		template: hbxSliderTemp3
	}, {
		lblACno: kony.i18n.getLocalizedString("AccountNo"),
		lblAcntType: "TMB Savings Account1",
		img1: "piggyico.png",
		lblCustName: "Joseph Bing",
		lblBalance: "10,000,000.00 ?",
		lblActNoval: "001-2-34567-8",
		lblSliderAccN1: kony.i18n.getLocalizedString("Product"),
		lblSliderAccN2: " TMB Saving Account",
		lblDummy: "-",
		template: hbxSliderTemp2
	}, {
		lblACno: kony.i18n.getLocalizedString("AccountNo"),
		lblAcntType: "TMB Savings Account1",
		img1: "piggyico.png",
		lblCustName: "Joseph Bing",
		lblBalance: "10,000,000.00 ?",
		lblActNoval: "001-2-34567-8",
		lblSliderAccN1: kony.i18n.getLocalizedString("Product"),
		lblSliderAccN2: " TMB Saving Account",
		lblDummy: "-",
		template: hbxSliderTemp3
	}, {
		lblACno: kony.i18n.getLocalizedString("AccountNo"),
		lblAcntType: "TMB Savings Account1",
		img1: "piggyico.png",
		lblCustName: "Joseph Bing",
		lblBalance: "10,000,000.00 ?",
		lblActNoval: "001-2-34567-8",
		lblSliderAccN1: kony.i18n.getLocalizedString("Product"),
		lblSliderAccN2: " TMB Saving Account",
		lblDummy: "-",
		template: hbxSliderTemp1
	}, {
		lblACno: kony.i18n.getLocalizedString("AccountNo"),
		lblAcntType: "TMB Savings Account1",
		img1: "piggyico.png",
		lblCustName: "Joseph Bing",
		lblBalance: "10,000,000.00 ?",
		lblActNoval: "001-2-34567-8",
		lblSliderAccN1: kony.i18n.getLocalizedString("Product"),
		lblSliderAccN2: " TMB Saving Account",
		lblDummy: "-",
		template: hbxSliderTemp2
	}, {
		lblACno: kony.i18n.getLocalizedString("AccountNo"),
		lblAcntType: "TMB Savings Account1",
		img1: "piggyico.png",
		lblCustName: "Joseph Bing",
		lblBalance: "10,000,000.00 ?",
		lblActNoval: "001-2-34567-8",
		lblSliderAccN1: kony.i18n.getLocalizedString("Product"),
		lblSliderAccN2: " TMB Saving Account",
		lblDummy: "-",
		template: hbxSliderTemp3
	}]
	currForm.segSlider.selectedIndex = s;
	n = currForm.segSlider.data.length - 1;
}

function onClickLeftArrow1(currForm) {
	var currForm = kony.application.getCurrentForm();
	s = currForm.segSlider.selectedIndex;
	if (n < 1) {
		return false;
	}
	if (s[1] == 0) {
		currForm.segSlider.selectedIndex = [0, n];
	} else {
		currForm.segSlider.selectedIndex = [0, (s[1] - 1)];
	}
	s = currForm.segSlider.selectedIndex;
}

function onClickRightArrow1(currForm) {
	var currForm = kony.application.getCurrentForm()
	s = currForm.segSlider.selectedIndex;
	if (n < 1) {
		return false;
	}
	if (s[1] == n) {
		currForm.segSlider.selectedIndex = [0, 0];
	} else {
		currForm.segSlider.selectedIndex = [0, (s[1] + 1)];
	}
	s = currForm.segSlider.selectedIndex;
}

function onClickConnectSeg() {
	var currForm = kony.application.getCurrentForm()
	s = currForm.segSlider.selectedIndex;
}

function setRowTemplateForS2S() {
	
	//frmSSService.segSlider.containerWeight = 100;
	var s2SFromAcctCount = 0;
	var s2SFromAcctlist = [];
	 s = [0, 0];
     n = 0;
	if (flowSpa) {
		gblnormSelIndex=0
        gblnormSelIndex = gbltranFromSelIndex[1];
    }
	if(flowSpa)
	{
		frmSSService.segSliderSpa.widgetDataMap = {
			lblACno: "lblACno",
			lblAcntType: "lblAcntType",
			img1: "img1",
			lblCustName: "lblCustName",
			lblBalance: "lblBalance",
			lblActNoval: "lblActNoval",
			lblDummy: "lblDummy",
			lblSliderAccN1: "lblSliderAccN1",
			lblSliderAccN2: "lblSliderAccN2"
		}
	
	}
	else
	{
		frmSSService.segSlider.widgetDataMap = {
			lblACno: "lblACno",
			lblAcntType: "lblAcntType",
			img1: "img1",
			lblCustName: "lblCustName",
			lblBalance: "lblBalance",
			lblActNoval: "lblActNoval",
			lblDummy: "lblDummy",
			lblSliderAccN1: "lblSliderAccN1",
			lblSliderAccN2: "lblSliderAccN2"
			
		}
	}
	var icon="";
	
	for (var i = 0; i < eligibleS2SAcctInfo.length; i++) {
	
		//Code for displaying image
		
	     icon="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+eligibleS2SAcctInfo[i]["ICON_ID"]+"&modIdentifier=PRODICON";
		var locale = kony.i18n.getCurrentLocale();
	    var productName;
	    var custName;
		if(locale == "en_US"){
		productName=eligibleS2SAcctInfo[i]["productNameEN"]
		if(eligibleS2SAcctInfo[i]["personalizedAcctNickNameEN"]==null || eligibleS2SAcctInfo[i]["personalizedAcctNickNameEN"]=='')
		custName=eligibleS2SAcctInfo[i]["personalizedAcctNickName"]
		else
		custName=eligibleS2SAcctInfo[i]["personalizedAcctNickNameEN"]
		}
		else{
		productName=eligibleS2SAcctInfo[i]["productNameTH"]
		if(eligibleS2SAcctInfo[i]["personalizedAcctNickNameTH"]==null ||  eligibleS2SAcctInfo[i]["personalizedAcctNickNameTH"]=='')
		custName=eligibleS2SAcctInfo[i]["personalizedAcctNickName"]
		else
		custName=eligibleS2SAcctInfo[i]["personalizedAcctNickNameTH"]
		
		}		
		
		
		
		
		
	
		var availableBal = eligibleS2SAcctInfo[i]["availableAmt"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
	
		if (s2SFromAcctCount == 0) {
			s2SFromAcctlist.push({
				img1: icon,
				lblCustName: custName,
				lblBalance: commaFormatted(availableBal),
				lblACno: kony.i18n.getLocalizedString("AccountNo")+" ",
				lblActNoval: formatAccountNo(eligibleS2SAcctInfo[i]["acctIdentValue"]),
				lblAcntType: "",
				lblSliderAccN2:productName ,
				lblDummy: "",
				lblSliderAccN1: kony.i18n.getLocalizedString("Product")+" ",
				tdFlag: "",
				fromFIIdent: "",
				remainingFee: "",
				prodCode: "",
				lblRemainFeeValue: "",
				lblRemainFee: "",
				template: hbxSliderTemp1,
				hiddenProductNameEN:eligibleS2SAcctInfo[i]["productNameEN"],
				hiddenProductNameTH:eligibleS2SAcctInfo[i]["productNameTH"],
				hiddenPersonalizedAcctNickNameEN:eligibleS2SAcctInfo[i]["personalizedAcctNickNameEN"],
				hiddenPersonalizedAcctNickNameTH:eligibleS2SAcctInfo[i]["personalizedAcctNickNameTH"]
			})
			//
			//
			s2SFromAcctCount = s2SFromAcctCount + 1;
			//
		} else if (s2SFromAcctCount == 1) {
			s2SFromAcctlist.push({
				img1: icon,
				lblCustName: custName,
				lblBalance: commaFormatted(availableBal),
				lblACno: kony.i18n.getLocalizedString("AccountNo")+" ",
				lblActNoval: formatAccountNo(eligibleS2SAcctInfo[i]["acctIdentValue"]),
				lblAcntType: "",
				lblSliderAccN2: productName,
				lblDummy: "",
				lblSliderAccN1: kony.i18n.getLocalizedString("Product")+" ",
				tdFlag: "",
				fromFIIdent: "",
				remainingFee: "",
				prodCode: "",
				lblRemainFeeValue: "",
				lblRemainFee: "",
				template: hbxSliderTemp2,
				hiddenProductNameEN:eligibleS2SAcctInfo[i]["productNameEN"],
				hiddenProductNameTH:eligibleS2SAcctInfo[i]["productNameTH"],
				hiddenPersonalizedAcctNickNameEN:eligibleS2SAcctInfo[i]["personalizedAcctNickNameEN"],
				hiddenPersonalizedAcctNickNameTH:eligibleS2SAcctInfo[i]["personalizedAcctNickNameTH"]
			})
			//
			//
			s2SFromAcctCount = s2SFromAcctCount + 1;
			//
		} else if (s2SFromAcctCount == 2) {
			s2SFromAcctlist.push({
				img1: icon,
				lblCustName: custName,
				lblBalance: commaFormatted(availableBal),
				lblACno: kony.i18n.getLocalizedString("AccountNo")+" ",
				lblActNoval: formatAccountNo(eligibleS2SAcctInfo[i]["acctIdentValue"]),
				lblAcntType: "",
				lblSliderAccN2: productName,
				lblDummy: "",
				lblSliderAccN1: kony.i18n.getLocalizedString("Product")+" ",
				tdFlag: "",
				fromFIIdent: "",
				remainingFee: "",
				prodCode: "",
				lblRemainFeeValue: "",
				lblRemainFee: "",
				template: hbxSliderTemp3,
				hiddenProductNameEN:eligibleS2SAcctInfo[i]["productNameEN"],
				hiddenProductNameTH:eligibleS2SAcctInfo[i]["productNameTH"],
				hiddenPersonalizedAcctNickNameEN:eligibleS2SAcctInfo[i]["personalizedAcctNickNameEN"],
				hiddenPersonalizedAcctNickNameTH:eligibleS2SAcctInfo[i]["personalizedAcctNickNameTH"]
			})
			//
			//
			s2SFromAcctCount = s2SFromAcctCount + 1;
			//
		} else if (s2SFromAcctCount == 3) {
			s2SFromAcctlist.push({
				img1: icon,
				lblCustName: custName,
				lblBalance: commaFormatted(availableBal),
				lblACno: kony.i18n.getLocalizedString("AccountNo")+" ",
				lblActNoval: formatAccountNo(eligibleS2SAcctInfo[i]["acctIdentValue"]),
				lblAcntType: "",
				lblSliderAccN2: productName,
				lblDummy: "",
				lblSliderAccN1: kony.i18n.getLocalizedString("Product")+" ",
				tdFlag: "",
				fromFIIdent: "",
				remainingFee: "",
				prodCode: "",
				lblRemainFeeValue: "",
				lblRemainFee: "",
				template: hbxSliderTemp4,
				hiddenProductNameEN:eligibleS2SAcctInfo[i]["productNameEN"],
				hiddenProductNameTH:eligibleS2SAcctInfo[i]["productNameTH"],
				hiddenPersonalizedAcctNickNameEN:eligibleS2SAcctInfo[i]["personalizedAcctNickNameEN"],
				hiddenPersonalizedAcctNickNameTH:eligibleS2SAcctInfo[i]["personalizedAcctNickNameTH"]
			})
			//
			//
			s2SFromAcctCount = s2SFromAcctCount + 1;
			//
		} else if (s2SFromAcctCount == 4) {
			s2SFromAcctlist.push({
				img1: icon,
				lblCustName: custName,
				lblBalance: commaFormatted(availableBal),
				lblACno: kony.i18n.getLocalizedString("AccountNo")+" ",
				lblActNoval: formatAccountNo(eligibleS2SAcctInfo[i]["acctIdentValue"]),
				lblAcntType: "",
				lblSliderAccN2: productName,
				lblDummy: "",
				lblSliderAccN1: kony.i18n.getLocalizedString("Product")+" ",
				tdFlag: "",
				fromFIIdent: "",
				remainingFee: "",
				prodCode: "",
				lblRemainFeeValue: "",
				lblRemainFee: "",
				template: hbxSliderTemp5,
				hiddenProductNameEN:eligibleS2SAcctInfo[i]["productNameEN"],
				hiddenProductNameTH:eligibleS2SAcctInfo[i]["productNameTH"],
				hiddenPersonalizedAcctNickNameEN:eligibleS2SAcctInfo[i]["personalizedAcctNickNameEN"],
				hiddenPersonalizedAcctNickNameTH:eligibleS2SAcctInfo[i]["personalizedAcctNickNameTH"]
			})
			//
			//
			s2SFromAcctCount = 0;
			//
		}
	}
	if(flowSpa)
	{
		frmSSService.segSliderSpa.data=s2SFromAcctlist;
	}
	else
	{
	    if (s2SFromAcctlist.length == 1) {             
        //#ifdef android
                        frmSSService.segSlider.viewConfig = {
                                    coverflowConfig : {
                                                      projectionAngle : 60,
                                                      rowItemRotationAngle : 45,
                                                      spaceBetweenRowItems : 0,
                                                      rowItemWidth : 80,
                                                       isCircular : false
                                                      }
                                                }   
      //#endif                       
                  
      frmSSService.btnLtArrow.setVisibility(false);
      frmSSService.btnRtArrow.setVisibility(false);
  } else if (s2SFromAcctlist.length > 1) {            
      //#ifdef android
                        frmSSService.segSlider.viewConfig = {
                                    coverflowConfig : {
                                                      projectionAngle : 60,
                                                      rowItemRotationAngle : 45,
                                                      spaceBetweenRowItems : 0,
                                                      rowItemWidth : 80,
                                                      isCircular : true
                                                      }
                                          }   
	  //#endif
       frmSSService.btnLtArrow.setVisibility(true);
      frmSSService.btnRtArrow.setVisibility(true);
            

}
	    
		frmSSService.segSlider.setData(s2SFromAcctlist);
		frmSSService.segSlider.selectedIndex = s;
		
		n = frmSSService.segSlider.data.length - 1;
	}	
	
}

function deleteUserFromECASCallBack(status, resulttable) {
	
	if (status == 400) {
		
		
		
		
		if (resulttable["opstatus"] == 0) {
			
			
			kony.application.dismissLoadingScreen();
			//showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
			return false;
		}
		kony.application.dismissLoadingScreen();
	}
}
