function onDoneAddRecipientName(){
	popupAddToMyRecipient.scrollRecipient.setVisibility(false);
	if(!isNotBlank(popupAddToMyRecipient.tbxRecipientName.text) || 
		popupAddToMyRecipient.tbxRecipientName.text.length < 1){
		popupAddToMyRecipient.lineRecipientName.skin  = linePopupBlack;
	}else{
		popupAddToMyRecipient.tbxRecipientName.skin =  txtBlueTextNormal;
		popupAddToMyRecipient.lineRecipientName.skin  = lineBlue;
		popupAddToMyRecipient.tbxAccountNickName.setFocus(true);
	}
}

function onDoneAddRecipientAcntNickName(){
	if(!isNotBlank(popupAddToMyRecipient.tbxAccountNickName.text)
		|| popupAddToMyRecipient.tbxAccountNickName.text.length < 3){
		popupAddToMyRecipient.lineAccountNickName.skin  = linePopupBlack;
	}else{
		popupAddToMyRecipient.lineAccountNickName.skin  = lineBlue;
		popupAddToMyRecipient.tbxAccountNickName.skin =  txtBlueTextNormal;
		if(popupAddToMyRecipient.hbxAccountName.isVisible){
			popupAddToMyRecipient.tbxAccountName.setFocus(true);
		}
	}
}

function onDoneAddRecipientAcntName(){
	if(!isNotBlank(popupAddToMyRecipient.tbxAccountName.text) ||
		popupAddToMyRecipient.tbxAccountName.text.length < 1){
		popupAddToMyRecipient.lineAccountName.skin  = linePopupBlack;
	}else{
		popupAddToMyRecipient.lineAccountName.skin  = lineBlue;
		popupAddToMyRecipient.tbxAccountName.skin =  txtBlueTextNormal;
	}
}

function onClickOfAddRecipientListDropDown(){
	if(popupAddToMyRecipient.scrollRecipient.isVisible){
		popupAddToMyRecipient.scrollRecipient.setVisibility(false);
	}else{
		popupAddToMyRecipient.scrollRecipient.setVisibility(true);
	}
}

function onClickAddToMyRecipient(){
	popupAddToMyRecipient.tbxAccountName.text = "";
	popupAddToMyRecipient.tbxAccountNickName.text = "";
	popupAddToMyRecipient.tbxRecipientName.text = "";
	popupAddToMyRecipient.lblSelRecipientName.text = "";
	popupAddToMyRecipient.imgRecipient.src ="";
	popupAddToMyRecipient.tbxRecipientName.isVisible = true;
	popupAddToMyRecipient.hbxSelectedRecipientValues.isVisible = false;
	popupAddToMyRecipient.lineRecipientName.skin  = linePopupBlack;
	popupAddToMyRecipient.lineAccountNickName.skin  = linePopupBlack;
	popupAddToMyRecipient.lineAccountName.skin  = linePopupBlack;
	popupAddToMyRecipient.segRecipientList.removeAll();
	getReceipentListingToAddMB();
	if(getORFTFlag(gblisTMB) == "Y"){
		popupAddToMyRecipient.hbxAccountName.setVisibility(false);
		popupAddToMyRecipient.lineAccountName.setVisibility(false);
	}else{
		popupAddToMyRecipient.hbxAccountName.setVisibility(true);
		popupAddToMyRecipient.lineAccountName.setVisibility(true);
	}
	popupAddToMyRecipient.show();
}

function onRowSelectRecipientToAdd(){
	var selectedData = popupAddToMyRecipient.segRecipientList.selectedItems[0];
	popupAddToMyRecipient.imgRecipient.src = selectedData["imgRecipientProfile"];
	popupAddToMyRecipient.lblSelRecipientName.text = selectedData["lblRecipientNameExisting"];
	
	popupAddToMyRecipient.tbxRecipientName.setVisibility(false);
	popupAddToMyRecipient.hbxSelectedRecipientValues.setVisibility(true);
	popupAddToMyRecipient.lineRecipientName.skin = lineBlue;
	popupAddToMyRecipient.scrollRecipient.setVisibility(false);
}

function onClickSelRecipientHBox(){
	popupAddToMyRecipient.hbxSelectedRecipientValues.setVisibility(false);
	popupAddToMyRecipient.lineRecipientName.skin = linePopupBlack;
	popupAddToMyRecipient.tbxRecipientName.setVisibility(true);
	popupAddToMyRecipient.tbxRecipientName.text = "";
	popupAddToMyRecipient.tbxRecipientName.setFocus(true);
}

function getReceipentListingToAddMB() {
	var inputParams = {};
    showLoadingPopUpScreen();
	invokeServiceSecureAsync("receipentListingService", inputParams, getReceipentListingToAddCallbackMB);
}


function getReceipentListingToAddCallbackMB(status, callBackResponse) {
	if (status == 400) {
		AddAccountList = [];
		AccountList = [];
		gblEditContactList = false;
		if (callBackResponse["opstatus"] == "0") {
			gblMAXRecipientCount = callBackResponse["recipientMaxCount"];
			gblMAXAccountCount = callBackResponse["recipientAccountMaxCount"];
			MAX_BANK_ACCNT_BULK_ADD = callBackResponse["recipientBulkAccntAddCount"];
			MAX_RECIPIENT_BULK_COUNT=callBackResponse["recipientBulkMaxCount"]
			if(callBackResponse["crmId"] != null && callBackResponse["crmId"] != undefined){
				gblcrmId = callBackResponse["crmId"];
			}
			if(callBackResponse["Results"].length > 0) {
				popupAddToMyRecipient.btnRecipientList.setVisibility(true);
				populateRecipientsListMBToAdd(callBackResponse["Results"]);
			}else {
				myRecipientsRs.length = 0;
				popupAddToMyRecipient.segRecipientList.removeAll();
				popupAddToMyRecipient.btnRecipientList.setVisibility(false);
				gblMixedCallsTrack = "done";
			}
		}else {
			gblMixedCallsTrack = "done";
		}
		
		if(gblMixedCallsTrack == ""){
			gblMixedCallsTrack = "done";
		}else{
			dismissLoadingScreen();
		}
	}else {
		if (status == 300) {
			alert(kony.i18n.getLocalizedString("ECGenericError"));
		}
	}
	dismissLoadingScreen();
}


function populateRecipientsListMBToAdd(collectionData) {
	myRecipientsRs.length = 0;
	var pic = null;
	
	var randomnum = Math.floor((Math.random()*10000)+1); 
	gblRcImageUrlConstant = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/ImageRender?billerId=&crmId=Y";
	
	if(gblisTMB == gblTMBBankCD && !(gblTransEmail == 1 && gblTrasSMS == 1)){
		// if other tmb account dont show own recipient
		gblMAXRecipientCount = gblMAXRecipientCount -1;
	}else{	
		myRecipientsRs.push(	{
				"recipientID":"own",
				"lblRecipientNameExisting": gblCustomerName,
				"imgRecipientProfile": gblMyProfilepic
			});
	}
	popupAddToMyRecipient.tbxRecipientName.setEnabled(true);
	popupAddToMyRecipient.hbxSelectedRecipientValues.setEnabled(true);
	
	if(gblisTMB == gblTMBBankCD && (gblTransEmail == 1 && gblTrasSMS == 1)){//if own tmb dont show recipients
		popupAddToMyRecipient.tbxRecipientName.setEnabled(false);
		popupAddToMyRecipient.hbxSelectedRecipientValues.setEnabled(false);
	}else if(collectionData.length > 0){
		for (var i = 0; i < collectionData.length; i++) {
			pic = gblRcImageUrlConstant + "&personalizedId=" + collectionData[i]["personalizedId"]+"&rr="+randomnum;
            if (collectionData[i]["personalizedPictureId"] != null && collectionData[i]["personalizedPictureId"].length > 0) {
                if (collectionData[i]["personalizedPictureId"].substring(0, 4) == "http") {
                    pic = collectionData[i]["personalizedPictureId"];
                } else {
                    pic = gblRcImageUrlConstant + "&personalizedId=" + collectionData[i]["personalizedId"]+"&rr="+randomnum;
                }
            }
            var recName = "";
            if(isNotBlank(collectionData[i]["personalizedName"])){
           		 recName = collectionData[i]["personalizedName"];
            }
			var tempRecord = {
				"recipientID": collectionData[i]["personalizedId"],
				"lblRecipientNameExisting": recName,
				"imgRecipientProfile": pic
			}
			myRecipientsRs.push(tempRecord);
		}
	}
	popupAddToMyRecipient.segRecipientList.removeAll();
	popupAddToMyRecipient.segRecipientList.setData(myRecipientsRs);
}


function callAddToRecipientService(){
	if(verifyInputAddToMyRecipient()){
		var inputParams ={};
		var acctName ="";
		var addType ="";
		var personalizedId =  "";
		if(popupAddToMyRecipient.tbxRecipientName.isVisible){
				popupAddToMyRecipient.tbxRecipientName.text;
				addType = "new";
				inputParams["recNickname"] = popupAddToMyRecipient.tbxRecipientName.text;
				
		}else{
				popupAddToMyRecipient.lblSelRecipientName.text;
				addType = "existing";
				var selectedRecipient = popupAddToMyRecipient.segRecipientList.selectedItems[0];
			inputParams["personalizedId"] = selectedRecipient["recipientID"];
			personalizedId = selectedRecipient["recipientID"];
			inputParams["recNickname"] = selectedRecipient["lblRecipientNameExisting"];
				
		}
		if(popupAddToMyRecipient.hbxAccountName.isVisible){
			acctName = popupAddToMyRecipient.tbxAccountName.text;
		}else{
			acctName = frmTransfersAck.lblTransNPbAckToAccountName.text;
		}
		
		if(acctName == null || acctName == undefined || acctName == ""){
			acctName = "Not returned from GSB";
		}
		var toAccId = removeHyphenIB(frmTransferConfirm.lblHiddenToAccount.text)
		var totalData = [];
		var bankData = [ "", personalizedId, gblisTMB, toAccId, popupAddToMyRecipient.tbxAccountNickName.text, "Added",acctName];
		totalData.push(bankData);
		inputParams["personalizedAccList"] = totalData.toString()
		inputParams["addType"] = addType;
		inputParams["bankCode"] = gblisTMB;
		inputParams["accId"] = toAccId;
		inputParams["accNickName"] = popupAddToMyRecipient.tbxAccountNickName.text;
		inputParams["acctName"] = acctName;
		if(frmTransferLanding.txtTransLndSmsNEmail.isVisible == true){
			inputParams["emailId"] = frmTransferLanding.txtTransLndSmsNEmail.text;
		}else if(frmTransferLanding.txtTransLndSms.isVisible == true){
			inputParams["phoneNumber"] = frmTransferLanding.txtTransLndSms.text;
		}
		
		
	    showLoadingScreen();
		invokeServiceSecureAsync("addMyRecipientAfterTransferComplete", inputParams, callAddToRecipientServiceCallBack);
	}
//
}

function callAddToRecipientServiceCallBack(status, callBackResponse){
	if (status == 400) {
			dismissLoadingScreen();
			if (callBackResponse["opstatus"] == "0") {
				popupAddToMyRecipient.dismiss();
				frmTransfersAck.hbxImageAddRecipient.setVisibility(false);
				showAlertRcMB(kony.i18n.getLocalizedString("TRAdd_Complete"), kony.i18n.getLocalizedString("info"), "info");
			}else if(callBackResponse["opstatus"] == "2"){//duplicate account nickname
				showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_correctnickname"), kony.i18n.getLocalizedString("info"), "info");
			}else if(callBackResponse["opstatus"] == "3"){//max accounts reached
				showAlertRcMB(kony.i18n.getLocalizedString("TR_MaxAccount"), kony.i18n.getLocalizedString("info"), "info");
			}else{
 				showCommonAlert(callBackResponse["errMsg"], callBackResponse["XPServerStatCode"]);
			}
	}
}

function verifyInputAddToMyRecipient(){
	if(!popupAddToMyRecipient.hbxSelectedRecipientValues.isVisible){
		var recipientName = popupAddToMyRecipient.tbxRecipientName.text;
		if(!isNotBlank(recipientName) || recipientName.trim() == kony.i18n.getLocalizedString("TREnter_PL_Name")){
			TextBoxErrorSkin(popupAddToMyRecipient.tbxRecipientName, txtErrorBG);
			showAlertRcMB(kony.i18n.getLocalizedString("TRErr_RecipientBlank"), kony.i18n.getLocalizedString("info"), "info");
			return false;
		}
		/*if(recipientName.length < 3){
			TextBoxErrorSkin(popupAddToMyRecipient.tbxRecipientName, txtErrorBG);
			showAlertRcMB(kony.i18n.getLocalizedString("RecipientNameShort"), kony.i18n.getLocalizedString("info"), "info");
			return false;
		}*/
		if(checkDuplicateRecipientNamesForAdd(recipientName)){
			TextBoxErrorSkin(popupAddToMyRecipient.tbxRecipientName, txtErrorBG);
			showAlertRcMB(kony.i18n.getLocalizedString("TRErr_ReceipentName"), kony.i18n.getLocalizedString("info"), "info");
			return false;
		}
		if (myRecipientsRs.length > gblMAXRecipientCount) {
			alert99LimitReached();
			return false;
		}
		
	}
	
	var accountNickName = popupAddToMyRecipient.tbxAccountNickName.text;
	if(!isNotBlank(accountNickName) || accountNickName.trim() == kony.i18n.getLocalizedString("TREnter_PL_AccNickName")){
		TextBoxErrorSkin(popupAddToMyRecipient.tbxAccountNickName, txtErrorBG);
		showAlertRcMB(kony.i18n.getLocalizedString("TRErr_AccNickname"), kony.i18n.getLocalizedString("info"), "info");
		return false;
	}
	if(accountNickName.length < 3){
		TextBoxErrorSkin(popupAddToMyRecipient.tbxAccountNickName, txtErrorBG);
		showAlertRcMB(kony.i18n.getLocalizedString("keyInvalidNickName"), kony.i18n.getLocalizedString("info"), "info");
		return false;
	}
	var charsArr = ["="];
	if(ThaiOrEnglish(accountNickName)){
		if(kony.string.containsChars(accountNickName, charsArr)){
			TextBoxErrorSkin(popupAddToMyRecipient.tbxAccountNickName, txtErrorBG);
			showAlertRcMB(kony.i18n.getLocalizedString("keyInvalidNickName"), kony.i18n.getLocalizedString("info"), "info");
			return false;
		}
	}else{
		TextBoxErrorSkin(popupAddToMyRecipient.tbxAccountNickName, txtErrorBG);
		showAlertRcMB(kony.i18n.getLocalizedString("keyInvalidNickName"), kony.i18n.getLocalizedString("info"), "info")
		return false;	
		
	}
	
	if(popupAddToMyRecipient.hbxAccountName.isVisible){
		var accountName = popupAddToMyRecipient.tbxAccountName.text;
		if(!isNotBlank(accountName) || accountName.trim() ==  kony.i18n.getLocalizedString("TREnter_PL_AccName")){
			TextBoxErrorSkin(popupAddToMyRecipient.tbxAccountName, txtErrorBG);
			showAlertRcMB(kony.i18n.getLocalizedString("TRErr_AccName"), kony.i18n.getLocalizedString("info"), "info");
			return false;
		}
	}
	return true;
}

function checkDuplicateRecipientNamesForAdd(name) {
	if (myRecipientsRs != null & myRecipientsRs.length != 0) {
		for (var i = 0; i < myRecipientsRs.length; i++) {
			if(isNotBlank(myRecipientsRs[i]["lblRecipientNameExisting"])){
				if (kony.string.equals(name.trim(), myRecipientsRs[i]["lblRecipientNameExisting"].trim())){
					return true;
				}
			}
		}
	}
	return false;
}

function searchMyRecipients() {
	var searchText = popupAddToMyRecipient.tbxRecipientName.text;
	searchText = searchText.toLowerCase();
	var recipientsSegData = myRecipientsRs;
	
	var  searchList = searchResultOfMyRecipients(recipientsSegData, searchText);
	
	if(popupAddToMyRecipient.segRecipientList.data != null && popupAddToMyRecipient.segRecipientList.data != undefined){
		if(popupAddToMyRecipient.segRecipientList.data.length != searchList.length){
			popupAddToMyRecipient.segRecipientList.setData(searchList);
		}
	}else{
		popupAddToMyRecipient.segRecipientList.setData(searchList);
	}
	
	if(searchText.length >= 1 && searchList.length > 0) {
		popupAddToMyRecipient.scrollRecipient.setVisibility(true);
	} else {
		popupAddToMyRecipient.scrollRecipient.setVisibility(false);
	}
}

function searchResultOfMyRecipients(recipientsSegData, searchText) {
	var searchList = [];
	var recipientsSegDataLength = recipientsSegData.length;
	var recipientName = "";
	
	if (recipientsSegDataLength > 0) { // Atleast one recipient should be avilable

		if (searchText.length >= 1) { //Atleast 1 characters should be entered in search box

			for (j = 0, i = 0; i < recipientsSegDataLength; i++) {
				
				recipientName = recipientsSegData[i].lblRecipientNameExisting;
				if (recipientName.toLowerCase().indexOf(searchText) > -1) {
					searchList[j] = recipientsSegData[i];
					j++;
				}
			}//For Loop End

		} else {//searchText condition End
			//If you Clear the Search Text then show All Transactions
			searchList = myRecipientsRs;
			
		}
	}//recipientsSegDataLength condition End
	
	return searchList;
}