/*frmInboxHome*/
function frmInboxHomeMenuPreshow() {
    if (gblCallPrePost) {
        frmInboxHome.scrollboxMain.scrollToEnd();
        gblIndex = -1;
        isMenuShown = false;
        isSignedUser = true;
        frmInboxHomePreShow.call(this);
        DisableFadingEdges.call(this, frmInboxHome);
    }
}

function frmInboxHomeMenuPostshow() {
    if (gblCallPrePost) {
        if (gblInboxHistoryModified) {
            getInboxHistoryMB.call(this);
        } else {
            if (InboxHomePageState) {
                populateInboxNormalState();
            } else {
                populateInboxDeleteState();
            }
        }
    }
    assignGlobalForMenuPostshow();
}
/*frmInboxDetails*/
function frmInboxDetailsMenuPreshow() {
    if (gblCallPrePost) {
        frmInboxDetails.scrollboxMain.scrollToEnd();
        gblIndex = -1;
        isMenuShown = false;
        isSignedUser = true;
        frmInboxDetailsPreShow.call(this);
        DisableFadingEdges.call(this, frmInboxDetails);
    }
}

function frmInboxDetailsMenuPostshow() {
    if (gblCallPrePost) {
        //...
    }
    assignGlobalForMenuPostshow();
}
/*frmNotificationHome*/
function frmNotificationHomeMenuPreshow() {
    if (gblCallPrePost) {
        frmNotificationHome.scrollboxMain.scrollToEnd();
        gblIndex = -1;
        isMenuShown = false;
        isSignedUser = true;
        frmNotificationHomePreShow.call(this);
        DisableFadingEdges.call(this, frmNotificationHome);
    }
}

function frmNotificationHomeMenuPostshow() {
    if (gblCallPrePost) {
        if (gblNotificationHistoryModified) {
            getNotificationHistoryMB.call(this);
        }
    }
    assignGlobalForMenuPostshow();
}
/*frmNotificationDetails*/
function frmNotificationDetailsMenuPreshow() {
    if (gblCallPrePost) {
        frmNotificationDetails.hbox4733076529053.setVisibility(true);
        //frmNotificationDetails.hbox473361467784111.setVisibility(false);
        frmNotificationDetails.scrollboxMain.scrollToEnd();
        gblIndex = -1;
        isMenuShown = false;
        isSignedUser = true;
        frmNotificationDetailsPreShow.call(this);
        DisableFadingEdges.call(this, frmNotificationDetails);
    }
}

function frmNotificationDetailsMenuPostshow() {
    if (gblCallPrePost) {
        frmNotificationDetails.scrollboxMain.scrollToEnd();
    }
    assignGlobalForMenuPostshow();
}
/*frmPromotion*/
function frmPromotionMenuPreshow() {
    if (gblCallPrePost) {
        frmPromotion.scrollboxMain.scrollToEnd();
        DisableFadingEdges.call(this, frmPromotion);
    }
}

function frmPromotionMenuPostshow() {
    if (gblCallPrePost) {
        frmPromotion.scrollboxMain.scrollToEnd();
        gblIndex = -1;
        isMenuShown = false;
    }
    assignGlobalForMenuPostshow();
}
/*frmPromotionDetails*/
function frmPromotionDetailsMenuPreshow() {
    if (gblCallPrePost) {
        frmPromotionDetails.scrollboxMain.scrollToEnd();
        DisableFadingEdges.call(this, frmPromotionDetails);
    }
}

function frmPromotionDetailsMenuPostshow() {
    if (gblCallPrePost) {
        setPromotionsTimer.call(this);
    }
    assignGlobalForMenuPostshow();
}