//function setDataOnSegMyBillerList() {
//	frmMyTopUpList.segBillersList.widgetDataMap = {
//		imgBillerLogo: "imgTopUPBiller",
//		lblBillerNickname: "lblCnfrmNickName",
//		lblBillerName: "lblNameComp",
//		lblRef1: "lblConfrmRef1",
//		lblRef1Value: "lblcnfrmRef1Value",
//		lblRef2: "lblConfrmRef2",
//		lblRef2Value: "lblcnfrmRef2Value"
//	};
//	segData = frmMyTopUpComplete.segComplete.data;
//	frmMyTopUpList.segBillersList.addAll(segData);
//	segData = [];
//}
//function setDataOnsegSelectBiller() {
//	var newData = [];
//	var maxLength = 15; //configurable parameter
//	segSelectData = [{
//		imgSelectBiller: {
//			src: "img_bill_3.png"
//		},
//		lblSelectBiller: "True Internate(1234)",
//		btnAddBiller: {
//			skin: "btnAdd"
//		}
//    }, {
//		imgSelectBiller: {
//			src: "img_bill_2.png"
//		},
//		lblSelectBiller: "MEA(1234)",
//		btnAddBiller: {
//			skin: "btnAdd"
//		}
//    }, {
//		imgSelectBiller: {
//			src: "img_bill_3.png"
//		},
//		lblSelectBiller: "True Internate(1234)",
//		btnAddBiller: {
//			skin: "btnAdd"
//		}
//    }, {
//		imgSelectBiller: {
//			src: "img_bill_3.png"
//		},
//		lblSelectBiller: "True Internate(1234)",
//		btnAddBiller: {
//			skin: "btnAdd"
//		}
//    }, {
//		imgSelectBiller: {
//			src: "img_bill_3.png"
//		},
//		lblSelectBiller: "True Internate(1234)",
//		btnAddBiller: {
//			skin: "btnAdd"
//		}
//    }, {
//		imgSelectBiller: {
//			src: "img_bill_3.png"
//		},
//		lblSelectBiller: "True Internate(1234)",
//		btnAddBiller: {
//			skin: "btnAdd"
//		}
//    }];
//	if (segSelectData.length <= maxLength) {
//		frmMyTopUpSelect.hbxLink.isVisible = false;
//		frmMyTopUpSelect.segSelectList.data = segSelectData;
//	} else {
//		frmMyTopUpSelect.hbxLink.isVisible = true;
//		for (i = 0; i < maxLength; i++) {
//			newData.push(segSelectData[i])
//			//alert("data "+[segSelectData[i]])
//		}
//		frmMyTopUpSelect.segSelectList.data = newData;
//	}
//}
//function selctedMyBiller() {
//	var indexOfSelectedRow = frmMyTopUpSelect.segSelectList.selectedIndex[1];
//	var indexOfSelectedIndex = frmMyTopUpSelect.segSelectList.selectedItems[0];
//	var dataList = frmMyTopUpSelect.segSelectList.data[indexOfSelectedRow];
//	frmAddTopUpToMB.imgAddedBiller.src = indexOfSelectedIndex.imgSelectBiller.src;
//	frmAddTopUpToMB.lblAddbillerName = indexOfSelectedIndex.lblSelectBiller;
//	frmAddTopUpToMB.show();
//	TMBUtil.DestroyForm(frmMyTopUpSelect);
//}
//function checkDuplicateNicknameOnAddBiller() {
//	var BillerList = frmMyTopUpList.segBillersList.data;
//	var ConfirmationList = frmAddTopUpBillerconfrmtn.segConfirmationList.data;
//	var nickname = frmAddTopUpToMB.txbNickName.text;
//	//alert("Nickname " + nickname);
//	var check = "";
//	var val = "";
//	for (var i = 0;
//		(((BillerList) != null) && i < BillerList.length); i++) {
//		val = BillerList[i].lblCnfrmNickName;
//		
//		
//		if (nickname == val) {
//			//alert("Duplicate Nickname" + val);
//			return false;
//		}
//	}
//	
//	for (var j = 0;
//		(((ConfirmationList) != null) && j < ConfirmationList.length); j++) {
//		val = ConfirmationList[j].lblCnfrmNickName;
//		
//		
//		if (nickname == val) {
//			//alert("Duplicate Nickname" + val);
//			return false;
//		}
//	}
//	return true;
//}
function addBillerToMBNext() {
    var billerMethodFlagMB = null;
    //var billerTopupType = 3; // 3 for MB biller and 4 for MB topup
    //verifyBillerMethod(billerTopupType);
    var nickname = frmAddTopUpToMB.txbNickName.text;
    var ref1Val = frmAddTopUpToMB.txtRef1.text;
    var ref2Val = frmAddTopUpToMB.txtRef2.text;
    var billerName = frmAddTopUpToMB.lblAddbillerName.text;
    callBillerValidationService(gblCompCode, nickname, ref1Val, ref2Val, billerName, channelFlagBillerMB);
}

function loadBillerConfirmSegmentDataMB() {
    var ref2Val = "";
    var ref2 = "";
    var ref1 = "";
    var locale = kony.i18n.getCurrentLocale();
    if (gblIsRef2RequiredMB != "Y") {
        ref2Val = "";
        ref2 = "";
    } else {
        ref2Val = frmAddTopUpToMB.txtRef2.text;
        if (locale == "en_US") {
            ref2 = gblCurRef2LblEN;
        } else {
            ref2 = gblCurRef2LblTH;
        }
    }
    if (locale == "en_US") {
        ref1 = gblCurRef1LblEN
    } else {
        ref1 = gblCurRef1LblTH
    }
    var dataSeg = [];
    //below line is added for CR - PCI-DSS masked Credit card no
    var maskedRef1 = maskCreditCard(frmAddTopUpToMB.txtRef1.text);
    if (gblIsRef2RequiredMB != "Y") {
        dataSeg = [{
            lblCnfrmNickName: frmAddTopUpToMB.txbNickName.text,
            imgTopUPBiller: frmAddTopUpToMB.imgAddedBiller.src,
            lblNameComp: frmAddTopUpToMB.lblAddbillerName.text,
            lblConfrmRef1: appendColon(ref1),
            lblcnfrmRef1Value: frmAddTopUpToMB.txtRef1.text,
            lblcnfrmRef1ValueMasked: maskedRef1,
            btnDlt: {
                skin: "btnDelete"
            },
            BillerCompCode: gblCompCode,
            BillerId: gblBillerIdMB,
            BillerNameEN: gblBillerCompCodeEN,
            BillerNameTH: gblBillerCompCodeTH,
            Ref1LblEN: gblCurRef1LblEN,
            Ref1LblTH: gblCurRef1LblTH
        }];
    } else {
        dataSeg = [{
            lblCnfrmNickName: frmAddTopUpToMB.txbNickName.text,
            imgTopUPBiller: frmAddTopUpToMB.imgAddedBiller.src,
            lblNameComp: frmAddTopUpToMB.lblAddbillerName.text,
            lblConfrmRef1: appendColon(ref1),
            lblcnfrmRef1Value: frmAddTopUpToMB.txtRef1.text,
            lblcnfrmRef1ValueMasked: maskedRef1,
            lblConfrmRef2: appendColon(ref2),
            lblcnfrmRef2Value: ref2Val,
            btnDlt: {
                skin: "btnDelete"
            },
            BillerCompCode: gblCompCode,
            BillerId: gblBillerIdMB,
            BillerNameEN: gblBillerCompCodeEN,
            BillerNameTH: gblBillerCompCodeTH,
            Ref1LblEN: gblCurRef1LblEN,
            Ref1LblTH: gblCurRef1LblTH,
            Ref2LblEN: gblCurRef2LblEN,
            Ref2LblTH: gblCurRef2LblTH
        }];
    }
    frmAddTopUpBillerconfrmtn.segConfirmationList.addAll(dataSeg);
    gblFlagConfirmDataAddedMB = 1;
    var billerCount = "";
    billerCount = frmAddTopUpBillerconfrmtn.segConfirmationList.data;
    var maxBulkAddCount = kony.os.toNumber(GLOBAL_MAX_BILL_ADD);
    if (billerCount.length >= maxBulkAddCount) {
        frmMyTopUpList.button1010778103103263.setEnabled(false);
        disableAddButtonTopUpBillerConfirm();
        frmMyTopUpList.segSuggestedBillers.setEnabled(false);
        gblBlockBillerTopUpAdd = true;
    }
    frmAddTopUpBillerconfrmtn.show();
    //added to avoid crash appearing sometimes in iPHONE
    frmAddTopUpToMB.txbNickName = "";
    frmAddTopUpToMB.txtRef1 = "";
    frmAddTopUpToMB.txtRef2 = "";
    // TMBUtil.DestroyForm(frmAddTopUpToMB);
}
//function myBBPostshow() {
//	segData = frmMyTopUpList.segBillersList.data;
//	if (!segData || segData.length <= 0) {
//		//frmAddTopUpBiller.show();
//		alert("Please add a Beep and Biller");
//	}
//}
function myTopUpAddButton() {
    if (gblMyBillerTopUpBB == 0) {
        if (segData == 50 && segSugData == 50) {
            alert("User has already added 50 billers");
        } else {
            frmAddTopUpBiller.hbxRef2.isVisible = true;
            frmAddTopUpToMB.show();
        }
    } else if (gblMyBillerTopUpBB == 1) {
        if (segData == 50 && segSugData == 50) {
            alert("User has already added 50 TopUps");
        } else {
            frmAddTopUpBiller.hbxRef2.isVisible = false;
            frmAddTopUpToMB.show();
        }
    } else if (gblMyBillerTopUpBB == 2) {
        if (segData == 50 && segSugData == 50) {
            alert("User has already added 50 Beep & Bills");
        } else {
            frmAddTopUpBiller.hbxRef2.isVisible = true;
            frmAddTopUpToMB.show();
        }
    }
}