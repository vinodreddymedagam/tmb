function onClickInboxnew(eventobject, x, y) {
    if ((inboxclickedClicked == 0)) {
        var trans100 = kony.ui.makeAffineTransform();
        trans100.rotate(180);
        frmMenu["imgarinbox"].animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans100
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        }, {
            "animationEnd": butonMovedinbox
        });
    } else {
        var trans100 = kony.ui.makeAffineTransform();
        trans100.rotate(360);
        frmMenu["imgarinbox"].animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans100
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        }, {
            "animationEnd": buttonUnmovedinbox
        });
    }
}

function buttonUnmovedinbox() {
    inboxclickedClicked = 0;
    frmMenu.flexseginbox.setVisibility(false);
}

function butonMovedinbox() {
    inboxclickedClicked = 1;
    //Reset the code for settings
    if (settingsClicked == 1) {
        settingsClicked = 0
        var trans100 = kony.ui.makeAffineTransform();
        trans100.rotate(360);
        frmMenu["imgarsettings"].animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans100
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        }, {
            "animationEnd": buttonUnmovedsettinsg
        });
    }
    //
    //Reset the code for more
    if (moreClicked == 1) {
        moreClicked = 0;
        var trans100 = kony.ui.makeAffineTransform();
        trans100.rotate(360);
        frmMenu["imgarmore"].animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans100
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        }, {
            "animationEnd": buttonUnmovedmore
        });
    }
    //
    frmMenu.flexseginbox.setVisibility(true);
    frmMenu.flexsegmore.setVisibility(false);
    frmMenu.flexsegsettings.setVisibility(false);
}
//settings events
function onClickSettingsnew(eventobject, x, y) {
    if ((settingsClicked == 0)) {
        var trans100 = kony.ui.makeAffineTransform();
        trans100.rotate(180);
        frmMenu["imgarsettings"].animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans100
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        }, {
            "animationEnd": butonMovedsettings
        });
    } else {
        var trans100 = kony.ui.makeAffineTransform();
        trans100.rotate(360);
        frmMenu["imgarsettings"].animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans100
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        }, {
            "animationEnd": buttonUnmovedsettinsg
        });
    }
}

function buttonUnmovedsettinsg() {
    settingsClicked = 0;
    frmMenu.flexsegsettings.setVisibility(false);
}

function butonMovedsettings() {
    settingsClicked = 1;
    //re seeting the code inboxsection
    if (inboxclickedClicked == 1) {
        inboxclickedClicked = 0;
        var trans100 = kony.ui.makeAffineTransform();
        trans100.rotate(360);
        frmMenu["imgarinbox"].animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans100
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        }, {
            "animationEnd": buttonUnmovedinbox
        });
    }
    //
    if (moreClicked == 1) {
        moreClicked = 0;
        var trans100 = kony.ui.makeAffineTransform();
        trans100.rotate(360);
        frmMenu["imgarmore"].animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans100
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        }, {
            "animationEnd": buttonUnmovedmore
        });
    }
    //
    frmMenu.flexsegsettings.setVisibility(true);
    frmMenu.flexseginbox.setVisibility(false);
    frmMenu.flexsegmore.setVisibility(false);
}
//more events
function onClickMorenew(eventobject, x, y) {
    if ((moreClicked == 0)) {
        var trans100 = kony.ui.makeAffineTransform();
        trans100.rotate(180);
        frmMenu["imgarmore"].animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans100
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        }, {
            "animationEnd": butonMovedmore
        });
    } else {
        var trans100 = kony.ui.makeAffineTransform();
        trans100.rotate(360);
        frmMenu["imgarmore"].animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans100
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        }, {
            "animationEnd": buttonUnmovedmore
        });
    }
}

function buttonUnmovedmore() {
    moreClicked = 0;
    frmMenu.flexsegmore.setVisibility(false);
}

function butonMovedmore() {
    moreClicked = 1;
    //re seeting the code inboxsection
    if (inboxclickedClicked == 1) {
        inboxclickedClicked = 0;
        var trans100 = kony.ui.makeAffineTransform();
        trans100.rotate(360);
        frmMenu["imgarinbox"].animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans100
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        }, {
            "animationEnd": buttonUnmovedinbox
        });
    }
    //Resetting the settings code
    if (settingsClicked == 1) {
        settingsClicked = 0;
        var trans100 = kony.ui.makeAffineTransform();
        trans100.rotate(360);
        frmMenu["imgarsettings"].animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans100
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        }, {
            "animationEnd": buttonUnmovedsettinsg
        });
    }
    //
    frmMenu.flexsegmore.setVisibility(true);
    frmMenu.flexseginbox.setVisibility(false);
    frmMenu.flexsegsettings.setVisibility(false);
}

function moveSegIbox() {
    function scaleWidgetCode() {}
    frmMenu["flexseginbox"].animate(kony.ui.createAnimation({
        "100": {
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            },
            "height": "100%"
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.25
    }, {
        "animationEnd": scaleWidgetCode
    });
}
//Reset the PostLogin Menu Evenets
function resetMenuEvents() {
    if (inboxclickedClicked == 1) {
        inboxclickedClicked = 0;
        var trans100 = kony.ui.makeAffineTransform();
        trans100.rotate(360);
        frmMenu["imgarinbox"].animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans100
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        }, {
            "animationEnd": buttonUnmovedinbox
        });
    }
    if (settingsClicked == 1) {
        settingsClicked = 0;
        var trans100 = kony.ui.makeAffineTransform();
        trans100.rotate(360);
        frmMenu["imgarsettings"].animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans100
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        }, {
            "animationEnd": buttonUnmovedsettinsg
        });
    }
    //
    //Reset the code for more
    if (moreClicked == 1) {
        moreClicked = 0;
        var trans100 = kony.ui.makeAffineTransform();
        trans100.rotate(360);
        frmMenu["imgarmore"].animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans100
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        }, {
            "animationEnd": buttonUnmovedmore
        });
    }
}