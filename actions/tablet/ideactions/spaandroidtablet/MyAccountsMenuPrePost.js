/*frmMyAccntAddAccount*/
function frmMyAccntAddAccountMenuPreshow() {
    if (gblCallPrePost) {
        frmMyAccntAddAccount.txtBxAccnt1.skin = txtNormalBG;
        frmMyAccntAddAccount.txtBxAccnt2.skin = txtNormalBG;
        frmMyAccntAddAccount.txtBxAccnt3.skin = txtNormalBG;
        frmMyAccntAddAccount.txtBxAccnt4.skin = txtNormalBG;
        frmMyAccntAddAccount.tbxNickname.skin = txtNormalBG;
        frmMyAccntAddAccount.tbxother.skin = txtNormalBG;
        frmMyAccntAddAccount.hbxOther.setVisibility(false);
        frmMyAccntAddAccount.scrollboxMain.scrollToEnd();
        // Setting header
        frmMyAccntAddAccount.hbxAccntNovalue.setVisibility(true);
        frmMyAccntAddAccount.lblAccntNo.setVisibility(true);
        // clearing data after landing to form
        cleardataAddAccnt();
        //Initializing global variable used to keep track of max accnt. len of that bank
        MAX_ACC_LEN = 0;
        // Is selected bank ORFT bank
        IS_ORFT = "Y";
        if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad") {
            frmMyAccntAddAccount.tbxother.keyBoardStyle = constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD;
            frmMyAccntAddAccount.txtBxAccnt1.keyBoardStyle = constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD;
            frmMyAccntAddAccount.txtBxAccnt2.keyBoardStyle = constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD;
            frmMyAccntAddAccount.txtBxAccnt3.keyBoardStyle = constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD;
            frmMyAccntAddAccount.txtBxAccnt4.keyBoardStyle = constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD;
            frmMyAccntAddAccount.textbx4.keyBoardStyle = constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD;
            frmMyAccntAddAccount.textbx1.keyBoardStyle = constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD;
            frmMyAccntAddAccount.textbx2.keyBoardStyle = constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD;
            frmMyAccntAddAccount.textbx3.keyBoardStyle = constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD;
        }
        isMenuRendered = false;
        isMenuShown = false;
        frmMyAccntAddAccountPreShow.call(this);
        DisableFadingEdges.call(this, frmMyAccntAddAccount);
    }
}

function frmMyAccntAddAccountMenuPostshow() {
    if (gblCallPrePost) {
        frmMyAccntAddAccount.scrollboxMain.scrollToEnd();
        var isSuffix = frmMyAccntAddAccount.vboxsuffix.isVisible;
        frmMyAccntAddAccount.vboxsuffix.setVisibility(false)
            //alert("isSuffix : " + isSuffix);
        if (isSuffix == true) {
            frmMyAccntAddAccount.vboxsuffix.containerWeight = 18;
            frmMyAccntAddAccount.vbxAccountNumber.containerWeight = 82;
        } else {
            frmMyAccntAddAccount.vbxAccountNumber.containerWeight = 100;
        }
    }
    assignGlobalForMenuPostshow();
}
/*frmMyAccntConfirmationAddAccount*/
function frmMyAccntConfirmationAddAccountMenuPreshow() {
    if (gblCallPrePost) {
        frmMyAccntConfirmationAddAccount.scrollboxMain.scrollToEnd();
        isMenuRendered = false;
        isMenuShown = false;
        if (flowSpa) {
            frmMyAccntConfirmationAddAccount.hbox473361467792157.isVisible = true;
            frmMyAccntConfirmationAddAccount.hbox473361467792205.isVisible = false;
        } else {
            frmMyAccntConfirmationAddAccount.hbxcnfrmftr.isVisible = true;
            frmMyAccntConfirmationAddAccount.hbxftraddmore.isVisible = false;
        }
        frmMyAccntConfirmationAddAccount.btnAddHeader.isVisible = true;
        frmMyAccntConfirmationAddAccount.imgcomplete.isVisible = false;
        frmMyAccntConfirmationAddAccount.lblHdrTxt.text = kony.i18n.getLocalizedString("keylblConfirmation");
        var setupTblTap = {
            fingers: 1,
            swipedistance: 50,
            swipevelocity: 75
        }
        var tapGesture = frmMyAccntConfirmationAddAccount.vboxRight.setGestureRecognizer(2, setupTblTap, swipeEventMyAccount);
        if (flowSpa) {
            swipeEnable = true;
            var setupTblTap = {
                fingers: 1,
                swipedistance: 50,
                swipevelocity: 75
            }
            swipeGesture = frmMyAccntConfirmationAddAccount.sboxRight.setGestureRecognizer(2, setupTblTap, swipeEventMyAccount);
            //swipeGesture = frmMyAccntConfirmationAddAccount.vbox4751247744173.setGestureRecognizer(2, setupTblTap, swipeEventMyAccount);
            swipeGesture = frmMyAccntConfirmationAddAccount.scrollboxLeft.setGestureRecognizer(2, setupTblTap, swipeEventMyAccount);
            //swipeGesture = frmMyAccntConfirmationAddAccount.hboxLeft1.setGestureRecognizer(2, setupTblTap, swipeEventMyAccount);
        }
        frmMyAccntConfirmationAddAccount.scrollboxMain.scrollToEnd();
        loadvalues.call(this);
        frmMyAccntConfirmationAddAccountPreShow.call(this);
        DisableFadingEdges.call(this, frmMyAccntConfirmationAddAccount);
    }
}

function frmMyAccntConfirmationAddAccountMenuPostshow() {
    if (gblCallPrePost) {
        frmMyAccntConfirmationAddAccount.scrollboxMain.scrollToEnd();
        /* campaginService.call(this,"image2448367682213126", "frmMyAccntConfirmationAddAccount", "M"); */
    }
    assignGlobalForMenuPostshow();
}
/*frmMyAccountEdit*/
function frmMyAccountEditMenuPreshow() {
    if (gblCallPrePost) {
        frmMyAccountEdit.txtEditAccntNickName.skin = txtNormalBG;
        isMenuRendered = false;
        isMenuShown = false;
        frmMyAccountEdit.scrollboxMain.scrollToEnd();
        frmMyAccountEditPreShow.call(this);
        DisableFadingEdges.call(this, frmMyAccountEdit);
    }
}

function frmMyAccountEditMenuPostshow() {
    if (gblCallPrePost) {
        frmMyAccountEdit.scrollboxMain.scrollToEnd();
    }
    assignGlobalForMenuPostshow();
}
/*frmMyAccountList*/
function frmMyAccountListMenuPreshow() {
    if (gblCallPrePost) {
        gblPreviousForm = "frmMyAccountList";
        frmMyAccountList.scrollboxMain.scrollToEnd();
        clearSegDataAccntList.call(this);
        gblAddAccntVar.call(this);
        isMenuRendered = false;
        isMenuShown = false;
        frmMyAccountListPreShow.call(this);
        DisableFadingEdges.call(this, frmMyAccountList);
    }
}

function frmMyAccountListMenuPostshow() {
    if (gblCallPrePost) {
        frmMyAccountEdit.scrollboxMain.scrollToEnd();
    }
    assignGlobalForMenuPostshow();
}
/*frmMyAccountView*/
function frmMyAccountViewMenuPreshow() {
    changeStatusBarColor();
    if (gblCallPrePost) {
        isMenuRendered = false;
        isMenuShown = false;
        frmMyAccountView.scrollboxMain.scrollToEnd();
        if (gblSavingsCareFlow == "MyAccountFlow") {
            kony.print("MyAccountFlow");
            handleSavingCareMBDetails(frmMyAccountView);
        }
        frmMyAccountViewPreShow.call(this);
        /* 
		DisableFadingEdges.call(this,frmMyAccountView);
		
		 */
    }
}

function frmMyAccountViewMenuPostshow() {
    if (gblCallPrePost) {}
    assignGlobalForMenuPostshow();
}