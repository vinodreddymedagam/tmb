/**
 * Success callback on locale change in IB
 */
//function onSuccessLocaleChangeIB() {
//	var currentFormId = kony.application.getCurrentForm()
//		.id;
//	var formLocaleList = {
//		"frmIBMyReceipentsHome": syncIBRcLocale,
//		"frmIBMyReceipentsAccounts": syncIBRcLocale,
//		"frmIBMyReceipentsAddBankAccnt": syncIBRcLocale,
//		"frmIBMyReceipentsAddContactFB": syncIBRcLocale,
//		"frmIBMyReceipentsAddContactManually": syncIBRcLocale,
//		"frmIBMyReceipentsAddContactManuallyConf": syncIBRcLocale,
//		"frmIBMyReceipentsEditAccountConf": syncIBRcLocale,
//		"frmIBMyReceipentsFBContactConf": syncIBRcLocale,
//		"frmIBTranferLP": handleLocaleChangeIBTranferLP,
//		"frmIBTransferNowConfirmation": handleLocaleChangeIBTransferNowConfirmation,
//		"frmIBTransferNowCompletion": handleLocaleChangeIBTransferNowCompletion,
//		"frmIBCMMyProfile": IBCMMyProfilePreShow,
//		"frmApplyMBviaIBStep1": handleLocaleChangeIBApplyServiceMain,
//		"frmApplyMbviaIBConf": handleLocaleChangeIBApplyServiceConf
//	}
//	formLocaleList[currentFormId].call();
//}
/**
 * Failure callback on locale change in IB
 * @returns {}
 */
//function onFailureLocaleChangeIB() {
//	alert("Unable to change onFailureLocaleChange");
//}
/**
 * Method to sync IB locale
 * @returns {}
 */
function syncIBLocale() {
    syncIBBillPayment();
    syncIBMyActivities();
    syncIBBeepAndBill();
    syncIBEditFT();
    syncContactUs();
    syncIBChequeServices();
    syncIBDreamSavingMaintainance();
    syncIBAccountDetailsUpdate();
    syncIBProfileLocale();
    syncIBTransferLocale();
    syncIBRcLocale();
    syncIBApplyServicesLocale();
    syncIBTopupLocale();
    syncIBFullStmtLocale();
    syncIBNotfnInboxLocale();
    syncIBOpenAcctLocale();
    syncIBFindTMB();
    syncIBHotPromotions();
    ChangeCampaignLocale();
    syncIBContactUs();
    syncIBMyAccounts();
    changeCampaignPreLocale();
    syncMyBillers();
    syncIBMyAccountSummary();
    syncIBFATCALocale();
    syncIBEditBP();
    syncIBSoGooODLocale();
    syncIBCardActivate();
    syncIBPointRedemption();
    syncIBEStatement();
    syncIBMutualFund();
    syncIBBankAssurance();
    syncIBOTPAndToken();
    syncIBOpenAccountKYC();
    syncIBAnyIDRegistrationFlow();
    syncIBTransferLP();
    syncIBTransferNowConfirmation();
    syncIBOpenNewDreamAccConfirmation();
    syncIBOpenNewDreamAccComplete();
    syncIBEditMyAccountsFlow();
    syncIBExecutedTransaction();
}

function syncIBExecutedTransaction() {
    dismissLoadingScreen();
    if (kony.i18n.getCurrentLocale() == "en_US") {
        frmIBExecutedTransaction.lblBankValue.text = gblTransferTxnENCallBack;
    } else {
        frmIBExecutedTransaction.lblBankValue.text = gblTransferTxnTHCallBack;
    }
}

function syncIBTransferNowConfirmation() {
    var currentFormId = kony.application.getCurrentForm();
    if (currentFormId.id == "frmIBTransferNowConfirmation") {
        currentFormId.segMenuOptions.removeAll();
        if (kony.i18n.getCurrentLocale() == "th_TH") {
            frmIBTransferNowConfirmation.btnMenuTransfer.skin = 'btnIBMenuTransferFocusThai';
            frmIBTransferNowConfirmation.btnMenuMyAccountSummary.skin = 'btnIBMenuMyAccountSummaryThai';
        } else {
            frmIBTransferNowConfirmation.btnMenuTransfer.skin = 'btnIBMenuTransferFocus';
            frmIBTransferNowConfirmation.btnMenuMyAccountSummary.skin = 'btnIBMenuMyAccountSummary';
        }
    }
}

function syncIBTransferLP() {
    var currentFormId = kony.application.getCurrentForm();
    if (currentFormId.id == "frmIBTranferLP") {
        setSelTabBankAccountOrMobile();
        currentFormId.segMenuOptions.removeAll();
        if (currentFormId.hbxP2PFee.isVisible) {
            var fees = currentFormId.lblP2PFeeVal.text.trim();
            var fee = fees.split(" ");
            if (fee.length > 2) {
                currentFormId.lblP2PFeeVal.text = kony.i18n.getLocalizedString("TREnter_Fee") + " " + fee[1] + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
            } else {
                currentFormId.lblP2PFeeVal.text = kony.i18n.getLocalizedString("keyFreeTransfer");
            }
        }
        if (kony.i18n.getCurrentLocale() == "th_TH") {
            currentFormId.btnMenuMyAccountSummary.skin = 'btnIBMenuMyAccountSummaryThai'
            currentFormId.btnMenuTransfer.skin = "btnIBMenuTransferFocusThai";
        } else {
            currentFormId.btnMenuMyAccountSummary.skin = 'btnIBMenuMyAccountSummary'
            currentFormId.btnMenuTransfer.skin = "btnIBMenuTransferFocus";
        }
        if (gblSelTransferMode == 2 || gblSelTransferMode == 3) {
            currentFormId.txtXferMobileNumber.placeholder = kony.i18n.getLocalizedString("IB_P2PToSection");
            checkDisplayNotifyP2PMOorCI();
            if (isNotBlank(gblisTMB)) {
                currentFormId.lblBankName.text = getBankNameIB(gblisTMB);
            }
            if (!currentFormId.hbxNoteToRecipent.isVisible) {
                currentFormId.lineThree.setVisibility(false);
            } else {
                currentFormId.lineThree.setVisibility(true);
            }
        }
        currentFormId.txtTransLndSmsNEmail.placeholder = kony.i18n.getLocalizedString("TransferMobileNo");
        currentFormId.tbxEmail.placeholder = kony.i18n.getLocalizedString("transferEmailID");
    }
}

function syncMyBillers() {
    var currentFormId = kony.application.getCurrentForm().id;
    if (currentFormId == "frmIBMyBillersHome") {
        frmIBMyBillersHome.lblOTPMsg.text = kony.i18n.getLocalizedString('keyotpmsg');
        frmIBMyBillersHome.lblOTPReqMsg.text = kony.i18n.getLocalizedString('keyotpmsgreq');
    }
}

function syncContactUs() {
    frmIBContactUs.label506459299571357.text = kony.i18n.getLocalizedString("keyContactUs");
    frmIBContactUs.lblFeel.text = kony.i18n.getLocalizedString("feebackText");
    var tab = [];
    tab[0] = [1, kony.i18n.getLocalizedString('cnt_topic')];
    tab[1] = [2, kony.i18n.getLocalizedString('Cnt_IB')];
    tab[2] = [3, kony.i18n.getLocalizedString('Cnt_DA')];
    tab[3] = [4, kony.i18n.getLocalizedString('Cnt_LP')];
    tab[4] = [5, kony.i18n.getLocalizedString('Cnt_Compliants')];
    frmIBContactUs.cmbCnt.masterData = tab;
    if (kony.application.getCurrentForm().id == "frmIBContactUs") footerPagesMenu();
}

function syncIBMyAccountSummary() {
    if (kony.application.getCurrentForm().id == "frmIBAccntSummary") {
        hbox5036590321206468.lblRemainFree.containerWeight = 65;
        if (kony.i18n.getCurrentLocale() == "th_TH") {
            hbox5036590321206468.lblRemainFree.containerWeight = 20;
        }
    }
}

function syncIBBeepAndBill() {
    if (kony.application.getCurrentForm().id == "frmIBBeepAndBillApplyCW") {
        if (kony.i18n.getCurrentLocale() == "en_US") {
            frmIBBeepAndBillApplyCW.lblAddBiller.containerWeight = 19;
            frmIBBeepAndBillApplyCW.lblAddBillerCompCode.containerWeight = 54;
        } else {
            frmIBBeepAndBillApplyCW.lblAddBiller.containerWeight = 26;
            frmIBBeepAndBillApplyCW.lblAddBillerCompCode.containerWeight = 46;
        }
    }
    if (kony.application.getCurrentForm().id == "frmIBBeepAndBillApply") {
        if (kony.i18n.getCurrentLocale() == "en_US") {
            frmIBBeepAndBillApply.lblAddBiller.containerWeight = 19;
            frmIBBeepAndBillApply.lblAddBillerCompCode.containerWeight = 54;
        } else {
            frmIBBeepAndBillApply.lblAddBiller.containerWeight = 23;
            frmIBBeepAndBillApply.lblAddBillerCompCode.containerWeight = 47;
        }
    }
    if (kony.application.getCurrentForm().id == "frmIBBeepAndBillConfAndComp") {
        if (gblTokenSwitchFlagBB == true) frmIBBeepAndBillConfAndComp.lblMsgRequset.text = kony.i18n.getLocalizedString("keyPleaseEnterToken");
        else frmIBBeepAndBillConfAndComp.lblMsgRequset.text = kony.i18n.getLocalizedString("keyotpmsgreq");
    }
    if (kony.application.getCurrentForm().id == "frmIBBeepAndBillExecuteConfComp") {
        if (gblTokenSwitchFlagBB == true) frmIBBeepAndBillExecuteConfComp.lblMsgRequset.text = kony.i18n.getLocalizedString("keyPleaseEnterToken");
        else frmIBBeepAndBillExecuteConfComp.lblMsgRequset.text = kony.i18n.getLocalizedString("keyotpmsgreq");
    }
    //if (kony.application.getCurrentForm().id == "frmIBBillPaymentConfirm") {
    //	if (kony.i18n.getCurrentLocale() == "en_US") {
    //        frmIBBillPaymentConfirm.label476047582115284.containerWeight=11;
    //		frmIBBillPaymentConfirm.txtotp.containerWeight=48;
    //    } else {
    //        frmIBBillPaymentConfirm.label476047582115284.containerWeight=22;
    //		frmIBBillPaymentConfirm.txtotp.containerWeight=37;
    //    }
    //}
}

function syncIBBillPayment() {
    if (kony.i18n.getCurrentLocale() == "en_US") {
        frmIBBillPaymentCW.lblBPRef1.text = appendColon(gblRef1LblEN);
        frmIBBillPaymentCW.lblBPRef2.text = appendColon(gblRef2LblEN);
    } else {
        frmIBBillPaymentCW.lblBPRef1.text = appendColon(gblRef1LblTH);
        frmIBBillPaymentCW.lblBPRef2.text = appendColon(gblRef2LblTH);
    }
    if (kony.application.getCurrentForm().id == "frmIBBillPaymentCompletenow") {
        if (frmIBBillPaymentCompletenow.lblBBPaymentValue.isVisible) {
            frmIBBillPaymentCompletenow.lblHide.text = kony.i18n.getLocalizedString("Hide");
        } else {
            frmIBBillPaymentCompletenow.lblHide.text = kony.i18n.getLocalizedString("keyUnhide");
        }
    }
    if (gblCompCode != undefined && gblCompCode != "" && gblCompCode == "2533") {
        var frmName = kony.application.getCurrentForm();
        var frmId = kony.application.getCurrentForm().id;
        if (frmId == "frmIBBillPaymentLP" || frmId == "frmIBBillPaymentConfirm" || frmId == "frmIBBillPaymentCompletenow") {
            if (frmId == "frmIBBillPaymentConfirm" || frmId == "frmIBBillPaymentCompletenow") {
                showMEACustDetails(frmName);
            }
            if (frmName.hbxAmountDetailsMEA.isVisible) {
                frmName.lnkHide.text = kony.i18n.getLocalizedString("Hide")
            } else {
                frmName.lnkHide.text = kony.i18n.getLocalizedString("show")
            }
            if (kony.i18n.getCurrentLocale() == "en_US") {
                if (frmId == "frmIBBillPaymentLP") {
                    frmIBBillPaymentLP.lblBPAmount.text = gblSegBillerData["billerTotalPayAmtEn"] + ":";
                } else if (frmId == "frmIBBillPaymentConfirm") {
                    frmIBBillPaymentConfirm.lblAmt.text = gblSegBillerData["billerTotalPayAmtEn"] + ":";
                } else if (frmId == "frmIBBillPaymentCompletenow") {
                    frmIBBillPaymentCompletenow.label589607987325263.text = gblSegBillerData["billerTotalPayAmtEn"] + ":";
                }
                frmName.lblAmtLabel.text = gblSegBillerData["billerAmountEn"] + ":";
                frmName.lblAmtInterest.text = gblSegBillerData["billerTotalIntEn"] + ":";
                frmName.lblAmtDisconnected.text = gblSegBillerData["billerDisconnectAmtEn"] + ":";
            } else {
                if (frmId == "frmIBBillPaymentLP") {
                    frmIBBillPaymentLP.lblBPAmount.text = gblSegBillerData["billerTotalPayAmtTh"] + ":";
                } else if (frmId == "frmIBBillPaymentConfirm") {
                    frmIBBillPaymentConfirm.lblAmt.text = gblSegBillerData["billerTotalPayAmtTh"] + ":";
                } else if (frmId == "frmIBBillPaymentCompletenow") {
                    frmIBBillPaymentCompletenow.label589607987325263.text = gblSegBillerData["billerTotalPayAmtTh"] + ":";
                }
                frmName.lblAmtLabel.text = gblSegBillerData["billerAmountTh"] + ":";
                frmName.lblAmtInterest.text = gblSegBillerData["billerTotalIntTh"] + ":";
                frmName.lblAmtDisconnected.text = gblSegBillerData["billerDisconnectAmtTh"] + ":";
            }
        }
    }
}

function syncIBMyActivities() {
    if (kony.application.getCurrentForm().id == "frmIBMyActivities") {
        if (kony.i18n.getCurrentLocale() == "th_TH") {
            var isIE8 = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
            if (isIE8) {
                frmIBMyActivities.vbox588717954792719.containerWeight = 14;
            }
        }
    }
}

function syncIBFATCALocale() {
    if (kony.application.getCurrentForm().id == "frmIBFATCATandC") {
        changeFATCATnCLocaleIB();
    }
    if (kony.application.getCurrentForm().id == "frmIBFATCAQuestionnaire") {
        changeFATCAFormLocaleIB();
        changeFATCAQuestionLocaleIB();
    }
}

function syncIBEditBP() {
    if (kony.application.getCurrentForm().id == "frmIBBillPaymentView") {
        if (gblOnLoadRepeatAsIB == "Yearly") {
            frmIBBillPaymentView.lblBPRepeatAsVal.text = kony.i18n.getLocalizedString("keyYearly");;
        } else if (gblOnLoadRepeatAsIB == "Monthly") {
            frmIBBillPaymentView.lblBPRepeatAsVal.text = kony.i18n.getLocalizedString("keyMonthly");;
        } else if (gblOnLoadRepeatAsIB == "Weekly") {
            frmIBBillPaymentView.lblBPRepeatAsVal.text = kony.i18n.getLocalizedString("keyWeekly");;
        } else if (gblOnLoadRepeatAsIB == "Daily") {
            frmIBBillPaymentView.lblBPRepeatAsVal.text = kony.i18n.getLocalizedString("Transfer_Daily");;
        }
        if (gblEndingFreqOnLoadIB == "OnDate") {
            frmIBBillPaymentView.lblBPExcuteValTimes.text = kony.i18n.getLocalizedString("keyTimesIB");
        } else if (gblEndingFreqOnLoadIB == "After") {
            frmIBBillPaymentView.lblBPExcuteValTimes.text = kony.i18n.getLocalizedString("keyTimesIB");
        } else if (gblEndingFreqOnLoadIB == "Never") {
            frmIBBillPaymentView.lblBPExcuteValTimes.text = "-";
        } else if (gblOnLoadRepeatAsIB == "Once") {
            frmIBBillPaymentView.lblBPExcuteValTimes.text = kony.i18n.getLocalizedString("keyTimesMB");
        }
    }
    if (kony.application.getCurrentForm().id == "frmIBEditFutureBillPaymentPrecnf") {
        if (gblOnLoadRepeatAsIB == "Yearly") {
            frmIBEditFutureBillPaymentPrecnf.lblBPRepeatAsVal.text = kony.i18n.getLocalizedString("keyYearly");;
        } else if (gblOnLoadRepeatAsIB == "Monthly") {
            frmIBEditFutureBillPaymentPrecnf.lblBPRepeatAsVal.text = kony.i18n.getLocalizedString("keyMonthly");;
        } else if (gblOnLoadRepeatAsIB == "Weekly") {
            frmIBEditFutureBillPaymentPrecnf.lblBPRepeatAsVal.text = kony.i18n.getLocalizedString("keyWeekly");;
        } else if (gblOnLoadRepeatAsIB == "Daily") {
            frmIBEditFutureBillPaymentPrecnf.lblBPRepeatAsVal.text = kony.i18n.getLocalizedString("Transfer_Daily");;
        }
        if (gblEndingFreqOnLoadIB == "OnDate") {
            frmIBEditFutureBillPaymentPrecnf.lblBPExcuteValTimes.text = kony.i18n.getLocalizedString("keyTimesIB");
        } else if (gblEndingFreqOnLoadIB == "After") {
            frmIBEditFutureBillPaymentPrecnf.lblBPExcuteValTimes.text = kony.i18n.getLocalizedString("keyTimesIB");
        } else if (gblEndingFreqOnLoadIB == "Never") {
            frmIBEditFutureBillPaymentPrecnf.lblBPExcuteValTimes.text = "-";
        } else if (gblOnLoadRepeatAsIB == "Once") {
            frmIBEditFutureBillPaymentPrecnf.lblBPExcuteValTimes.text = kony.i18n.getLocalizedString("keyTimesMB");
        }
    }
}

function syncIBSoGooODLocale() {
    if (kony.application.getCurrentForm().id == "frmIBSoGooodTnC") {
        frmIBSoGooodTnCPreShow();
    } else if (kony.application.getCurrentForm().id == "frmIBSoGooodProdBrief") {
        frmIBSoGooodProdBriefPreShow();
    } else if (kony.application.getCurrentForm().id == "frmIBApplySoGooODComplete") {
        frmIBApplySoGooODComplete_preshow();
    } else if (kony.application.getCurrentForm().id == "frmIBSoGooodConf") {
        frmMBSoGooodConfPreShowIB();
    } else if (kony.application.getCurrentForm().id == "frmIBApplySoGooODPlanList") {
        applySoGooODLanding_preShow();
    }
}

function syncIBMutualFund() {
    if (kony.application.getCurrentForm().id == "frmIBMutualFundsSummary") {
        frmIBMutualFundsSummaryPreShow();
    } else if (kony.application.getCurrentForm().id == "frmIBMFAcctFullStatement") {
        frmIBMFAcctFullStatementPreShow();
    } else if (kony.application.getCurrentForm().id == "frmIBMFAcctOrderToBProceed") {
        frmIBMFAcctOrderToBProceedPreShow();
    }
}

function syncIBBankAssurance() {
    if (kony.application.getCurrentForm().id == "frmIBBankAssuranceSummary") {
        frmIBBankAssuranceSummaryPreShow();
    }
}

function syncIBOpenAccountKYC() {
    if (kony.application.getCurrentForm().id == "frmIBOpenAccountOccupationKYC") {
        populateDataKYCOccupationInfo();
    } else if (kony.application.getCurrentForm().id == "frmIBOpenAccountContactKYC") {
        languageToggleForOpenAccountContactKYC();
    }
}

function syncIBAnyIDRegistrationFlow() {
    if (kony.application.getCurrentForm().id == "frmIBAnyIDRegistration") {
        languageToggleForAnyIDRegistrationIB();
    } else if (kony.application.getCurrentForm().id == "frmIBAnyIDProdFeature") {
        languageToggleForAnyIDProdFeatureIB();
    }
}

function syncIBOpenNewDreamAccConfirmation() {
    var currentFormId = kony.application.getCurrentForm();
    if (currentFormId.id == "frmIBOpenNewDreamAccConfirmation") {
        var locale = kony.i18n.getCurrentLocale();
        if (locale == "th_TH") {
            frmIBOpenNewDreamAccConfirmation.lblDreamOpenActNameVal.text = gblPartyInqOARes["FullName"];
        } else if (locale == "en_US") {
            frmIBOpenNewDreamAccConfirmation.lblDreamOpenActNameVal.text = gblPartyInqOARes["PrefName"];
        }
    }
}

function syncIBOpenNewDreamAccComplete() {
    var currentFormId = kony.application.getCurrentForm();
    if (currentFormId.id == "frmIBOpenNewDreamAccComplete") {
        var locale = kony.i18n.getCurrentLocale();
        if (locale == "th_TH") {
            frmIBOpenNewDreamAccComplete.lblDreamOpenAcctNamComVal.text = gblPartyInqOARes["FullName"];
        } else if (locale == "en_US") {
            frmIBOpenNewDreamAccComplete.lblDreamOpenAcctNamComVal.text = gblPartyInqOARes["PrefName"];
        }
    }
}

function syncIBEditMyAccountsFlow() {
    var currentFormId = kony.application.getCurrentForm();
    var previousForm = kony.application.getPreviousForm();
    if (currentFormId.id == "frmIBEditMyAccounts") {
        frmIBEditMyAccounts.lblViewAccntNum.text = kony.i18n.getLocalizedString("keyAccountNoIB");
        frmIBEditMyAccounts.lblViewAccntName.text = kony.i18n.getLocalizedString("AccountName");
        if (frmIBMyAccnts.segTMBAccntsList.selectedItems != null && undefined != frmIBMyAccnts.segTMBAccntsList.selectedItems[0].productID && gblAccountTable["SAVING_CARE_PRODUCT_CODES"].indexOf(frmIBMyAccnts.segTMBAccntsList.selectedItems[0].productID) >= 0) {
            changeMasterdatainRelationIBEditMyAccounts();
            frmIBEditMyAccounts.lblViewAccntNum.text = kony.i18n.getLocalizedString("keyAccountNoIB");
            frmIBEditMyAccounts.lblViewAccntName.text = kony.i18n.getLocalizedString("AccountName");
            if (gblBeneficiaryData != null && gblBeneficiaryData.length > 0) {
                handleEditSavingCareIBMyAccount(true);
            } else {
                handleEditSavingCareIBMyAccount(false);
            }
        }
    }
    if (currentFormId.id == "frmIBEditMyAccountsConfirmComplete") {
        if (kony.i18n.getCurrentLocale() == "en_US") {
            frmIBEditMyAccountsConfirmComplete.lblProductName.text = frmIBMyAccnts.segTMBAccntsList.selectedItems[0].hiddenProductNameEng;
        } else {
            frmIBEditMyAccountsConfirmComplete.lblProductName.text = frmIBMyAccnts.segTMBAccntsList.selectedItems[0].hiddenProductNameThai;
        }
        if (frmIBEditMyAccounts.btnSpecified.skin == "btnIBTab4LeftFocus") {
            frmIBEditMyAccountsConfirmComplete.lblBeneficiarySpecify.text = appendColon(kony.i18n.getLocalizedString("keyBeneficiaries")) + " " + kony.i18n.getLocalizedString("keyBeneficiariesSpecify");
            frmIBEditMyAccountsConfirmComplete.lblSegBeneficiaryName.text = kony.i18n.getLocalizedString("keyBeneficiaryName");
            frmIBEditMyAccountsConfirmComplete.lblSegBeneficiaryRelation.text = kony.i18n.getLocalizedString("keyBeneficiaryRelationship");
            frmIBEditMyAccountsConfirmComplete.lblSegBeneficiaryBenefit.text = kony.i18n.getLocalizedString("keyBeneficiaryBenefit");
        } else {
            frmIBEditMyAccountsConfirmComplete.lblBeneficiarySpecify.text = appendColon(kony.i18n.getLocalizedString("keyBeneficiaries")) + " " + kony.i18n.getLocalizedString("keyBeneficiariesNotSpecify");
        }
        setEditBenificiariesConfirmCompleteSeg();
    }
}