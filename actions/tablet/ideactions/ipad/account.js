function accountNickNameMaxLength() {
    //frmEditAccnt.txtEditAccntNickName.maxTextLength=20;
    var actCodePatt = /[A-Za-z0-9]{20}$/ig;
    //resultActCodePatt = actCodePatt.test(frmMyAccountEdit.txtEditAccntNickName.text);
}
/**************************
						   ADD Account
					**************************/
function banklist(lblflag) {
    if (frmMyAccntAddAccount.vbxAccountNumber.isVisible == true) frmMyAccntAddAccount.vbxAccountNumber.containerWeight = 100;
    if (lblflag == "Bank") {
        clearAccNoAndNickName();
        // Setting MAX_ACC_LEN value in call back of srvGetBankDetail
        srvGetBankDetail(popBankList.segBanklist.selectedItems[0].bankCode);
        gblBankCD = popBankList.segBanklist.selectedItems[0].bankCode;
        frmMyAccntAddAccount.lblAccntNo.text = kony.i18n.getLocalizedString("AccountNumber");
        var bankName = popBankList.segBanklist.selectedItems[0].lblBanklist;
        //added the js code	
        var len = bankName.length;
        var cutlen = len - 37;
        if (len > 37) {
            var x = bankName.substr(len - cutlen);
        }
        var bankName = bankName.replace(x, "..");
        popBankList.segBanklist.selectedItems[0].lblBanklist = bankName;
        if (bankName == kony.i18n.getLocalizedString("keyTMBBankListVal") || bankName == kony.i18n.getLocalizedString("keyTMBBank")) {
            frmMyAccntAddAccount.btnBanklist.text = bankName;
            frmMyAccntAddAccount.hbxbtnacntList.setVisibility(true);
            frmMyAccntAddAccount.lblAccntNo.setVisibility(true);
            frmMyAccntAddAccount.hbxAccntNovalue.setVisibility(true);
            frmMyAccntAddAccount.lblAccountSuffix.setVisibility(false);
            frmMyAccntAddAccount.hbxCreditAccoutNoValue.setVisibility(false);
            frmMyAccntAddAccount.vboxsuffix.setVisibility(false);
            frmMyAccntAddAccount.hbxOther.setVisibility(false);
            frmMyAccntAddAccount.hbxAccountName.setVisibility(false);
            frmMyAccntAddAccount.line363582120412684.setVisibility(false);
        } else {
            //alert(popBankList.segBanklist.selectedItems[0].lblBanklist);
            frmMyAccntAddAccount.btnBanklist.text = bankName;
            frmMyAccntAddAccount.lblAccntNo.setVisibility(true);
            frmMyAccntAddAccount.hbxbtnacntList.setVisibility(false);
            frmMyAccntAddAccount.hbxAccntNovalue.setVisibility(false);
            frmMyAccntAddAccount.lblAccountSuffix.setVisibility(false);
            frmMyAccntAddAccount.hbxCreditAccoutNoValue.setVisibility(false);
            frmMyAccntAddAccount.vboxsuffix.setVisibility(false);
            frmMyAccntAddAccount.hbxOther.setVisibility(true);
            if (popBankList.segBanklist.selectedItems[0].orftFlag == "Y") {
                frmMyAccntAddAccount.hbxAccountName.setVisibility(false);
                frmMyAccntAddAccount.line363582120412684.setVisibility(false);
            } else {
                frmMyAccntAddAccount.hbxAccountName.setVisibility(true);
                frmMyAccntAddAccount.line363582120412684.setVisibility(true);
            }
        }
    } else if (lblflag == "Account") {
        clearAccNoAndNickName();
        frmMyAccntAddAccount.lblAccntNo.text = kony.i18n.getLocalizedString("AccountNumber");
        if (popBankList.segBanklist.selectedIndex[1] == 0) {
            frmMyAccntAddAccount.btnacntlist.text = kony.i18n.getLocalizedString("keylblDeposit");
            //frmMyAccntAddAccount.btnacntlist.text= popBankList.segBanklist.selectedItems[0].lblBanklist;
            frmMyAccntAddAccount.lblAccntNo.setVisibility(true);
            frmMyAccntAddAccount.hbxAccntNovalue.setVisibility(true);
            frmMyAccntAddAccount.lblAccountSuffix.setVisibility(false);
            frmMyAccntAddAccount.hbxCreditAccoutNoValue.setVisibility(false);
            frmMyAccntAddAccount.vboxsuffix.setVisibility(false);
            frmMyAccntAddAccount.hbxOther.setVisibility(false);
        } else if (popBankList.segBanklist.selectedIndex[1] == 2) {
            frmMyAccntAddAccount.btnacntlist.text = kony.i18n.getLocalizedString("keylblLoan");
            //frmMyAccntAddAccount.btnacntlist.text= popBankList.segBanklist.selectedItems[0].lblBanklist;
            frmMyAccntAddAccount.lblAccntNo.setVisibility(true);
            frmMyAccntAddAccount.hbxAccntNovalue.setVisibility(true);
            frmMyAccntAddAccount.lblAccountSuffix.setVisibility(true);
            frmMyAccntAddAccount.hbxCreditAccoutNoValue.setVisibility(false);
            frmMyAccntAddAccount.vboxsuffix.setVisibility(true);
            frmMyAccntAddAccount.vbxAccountNumber.containerWeight = 82;
            frmMyAccntAddAccount.vboxsuffix.containerWeight = 18;
            frmMyAccntAddAccount.hbxOther.setVisibility(false);
        } else if (popBankList.segBanklist.selectedIndex[1] == 1) {
            frmMyAccntAddAccount.btnacntlist.text = kony.i18n.getLocalizedString("keyCreditCardIB");
            //frmMyAccntAddAccount.btnacntlist.text= popBankList.segBanklist.selectedItems[0].lblBanklist;
            frmMyAccntAddAccount.lblAccntNo.text = kony.i18n.getLocalizedString("keylblCardNo");
            frmMyAccntAddAccount.lblAccntNo.setVisibility(true);
            frmMyAccntAddAccount.hbxAccntNovalue.setVisibility(false);
            frmMyAccntAddAccount.lblAccountSuffix.setVisibility(false);
            frmMyAccntAddAccount.hbxCreditAccoutNoValue.setVisibility(true);
            frmMyAccntAddAccount.vboxsuffix.setVisibility(false);
            frmMyAccntAddAccount.hbxOther.setVisibility(false);
        } else {
            /*
            
            frmMyAccntAddAccount.btnacntlist.text = popBankList.segBanklist.selectedItems[0].lblBanklist;
            frmMyAccntAddAccount.tbxother.maxTextLength = 10; // Setting max accnt length as 14
            frmMyAccntAddAccount.lblAccntNo.setVisibility(true);
            frmMyAccntAddAccount.hbxbtnacntList.setVisibility(true);
            frmMyAccntAddAccount.hbxAccntNovalue.setVisibility(false);
            frmMyAccntAddAccount.lblAccountSuffix.setVisibility(false);
            frmMyAccntAddAccount.hbxCreditAccoutNoValue.setVisibility(false);
            frmMyAccntAddAccount.vboxsuffix.setVisibility(false);
            frmMyAccntAddAccount.hbxOther.setVisibility(true);
            */
            //frmMyAccntAddAccount.btnacntlist.text = kony.i18n.getLocalizedString("keylblDeposit");
            frmMyAccntAddAccount.btnacntlist.text = popBankList.segBanklist.selectedItems[0].lblBanklist;
            frmMyAccntAddAccount.lblAccntNo.setVisibility(true);
            frmMyAccntAddAccount.hbxAccntNovalue.setVisibility(true);
            frmMyAccntAddAccount.lblAccountSuffix.setVisibility(false);
            frmMyAccntAddAccount.hbxCreditAccoutNoValue.setVisibility(false);
            frmMyAccntAddAccount.vboxsuffix.setVisibility(false);
            frmMyAccntAddAccount.hbxOther.setVisibility(false);
        }
    }
    /*else if (lblflag == "SelectBiller") {

        if (popBankList.segBanklist.selectedIndex[1] == 0) {
            frmSelectBiller.btnSelectCategory.text = "Select Category";
        }
        if (popBankList.segBanklist.selectedIndex[1] == 1) {
            //frmMyAccntAddAccount.btnacntlist.text=kony.i18n.getLocalizedString("keylblLoan");
            frmSelectBiller.btnSelectCategory.text = "Electricity";
        }
        if (popBankList.segBanklist.selectedIndex[1] == 2) {
            frmSelectBiller.btnSelectCategory.text = "Tv Recharge";
        }
        if (popBankList.segBanklist.selectedIndex[1] == 3) {
            frmSelectBiller.btnSelectCategory.text = "Broadband";
        }
        if (popBankList.segBanklist.selectedIndex[1] == 4) {
            frmSelectBiller.btnSelectCategory.text = "Water bill";
        }
        if (popBankList.segBanklist.selectedIndex[1] == 5) {
            frmSelectBiller.btnSelectCategory.text = "Internet";
        }
    } else if (lblflag == "SelectTopUp") {

        if (popBankList.segBanklist.selectedIndex[1] == 0) {
            frmSelectBiller.btnSelectCategory.text = "TopUpSelect Category";
        }
        if (popBankList.segBanklist.selectedIndex[1] == 1) {
            //frmMyAccntAddAccount.btnacntlist.text=kony.i18n.getLocalizedString("keylblLoan");
            frmSelectBiller.btnSelectCategory.text = "TopUpElectricity";
        }
        if (popBankList.segBanklist.selectedIndex[1] == 2) {
            frmSelectBiller.btnSelectCategory.text = "TopUpTv Recharge";
        }
        if (popBankList.segBanklist.selectedIndex[1] == 3) {
            frmSelectBiller.btnSelectCategory.text = "TopUpBroadband";
        }
        if (popBankList.segBanklist.selectedIndex[1] == 4) {
            frmSelectBiller.btnSelectCategory.text = "TopUpWater bill";
        }
        if (popBankList.segBanklist.selectedIndex[1] == 5) {
            frmSelectBiller.btnSelectCategory.text = "TopUpInternet";
        }
    } */
    else if (lblflag == "TopUpType1") {
        if (popBankList.segBanklist.selectedIndex[1] == 0) {
            frmTopUp.btnTopUpAmountComboBox.text = "100";
        }
        if (popBankList.segBanklist.selectedIndex[1] == 1) {
            //frmMyAccntAddAccount.btnacntlist.text=kony.i18n.getLocalizedString("keylblLoan");
            frmTopUp.btnTopUpAmountComboBox.text = "200";
        }
        if (popBankList.segBanklist.selectedIndex[1] == 2) {
            frmTopUp.btnTopUpAmountComboBox.text = "300";
        }
        if (popBankList.segBanklist.selectedIndex[1] == 3) {
            frmTopUp.btnTopUpAmountComboBox.text = "400";
        }
    } else if (lblflag == "TopUpType2") {
        if (popBankList.segBanklist.selectedIndex[1] == 0) {
            frmTopUp.btnTopUpAmountComboBox.text = "111";
        }
        if (popBankList.segBanklist.selectedIndex[1] == 1) {
            //frmMyAccntAddAccount.btnacntlist.text=kony.i18n.getLocalizedString("keylblLoan");
            frmTopUp.btnTopUpAmountComboBox.text = "222";
        }
        if (popBankList.segBanklist.selectedIndex[1] == 2) {
            frmTopUp.btnTopUpAmountComboBox.text = "333";
        }
        if (popBankList.segBanklist.selectedIndex[1] == 3) {
            frmTopUp.btnTopUpAmountComboBox.text = "444";
        }
    } else if (lblflag == "TopUpType3") {
        if (popBankList.segBanklist.selectedIndex[1] == 0) {
            frmTopUp.btnTopUpAmountComboBox.text = "101";
        }
        if (popBankList.segBanklist.selectedIndex[1] == 1) {
            //frmMyAccntAddAccount.btnacntlist.text=kony.i18n.getLocalizedString("keylblLoan");
            frmTopUp.btnTopUpAmountComboBox.text = "201";
        }
        if (popBankList.segBanklist.selectedIndex[1] == 2) {
            frmTopUp.btnTopUpAmountComboBox.text = "301";
        }
        if (popBankList.segBanklist.selectedIndex[1] == 3) {
            frmTopUp.btnTopUpAmountComboBox.text = "401";
        }
    } else if (lblflag == "TopUpType4") {
        if (popBankList.segBanklist.selectedIndex[1] == 0) {
            frmTopUp.btnTopUpAmountComboBox.text = "123";
        }
        if (popBankList.segBanklist.selectedIndex[1] == 1) {
            //frmMyAccntAddAccount.btnacntlist.text=kony.i18n.getLocalizedString("keylblLoan");
            frmTopUp.btnTopUpAmountComboBox.text = "234";
        }
        if (popBankList.segBanklist.selectedIndex[1] == 2) {
            frmTopUp.btnTopUpAmountComboBox.text = "345";
        }
    } else if (lblflag == "TopUpType5") {
        if (popBankList.segBanklist.selectedIndex[1] == 0) {
            frmTopUp.btnTopUpAmountComboBox.text = "110";
        }
        if (popBankList.segBanklist.selectedIndex[1] == 1) {
            //frmMyAccntAddAccount.btnacntlist.text=kony.i18n.getLocalizedString("keylblLoan");
            frmTopUp.btnTopUpAmountComboBox.text = "220";
        }
        if (popBankList.segBanklist.selectedIndex[1] == 2) {
            frmTopUp.btnTopUpAmountComboBox.text = "330";
        }
        if (popBankList.segBanklist.selectedIndex[1] == 3) {
            frmTopUp.btnTopUpAmountComboBox.text = "440";
        }
    } else if (lblflag == "TopUpType7") {
        if (popBankList.segBanklist.selectedIndex[1] == 0) {
            frmTopUp.btnTopUpAmountComboBox.text = "1111";
        }
        if (popBankList.segBanklist.selectedIndex[1] == 1) {
            //frmMyAccntAddAccount.btnacntlist.text=kony.i18n.getLocalizedString("keylblLoan");
            frmTopUp.btnTopUpAmountComboBox.text = "2222";
        }
        if (popBankList.segBanklist.selectedIndex[1] == 2) {
            frmTopUp.btnTopUpAmountComboBox.text = "3333";
        }
        if (popBankList.segBanklist.selectedIndex[1] == 3) {
            frmTopUp.btnTopUpAmountComboBox.text = "4444";
        }
    }
}

function depositAccntTextbox() {
    if (frmMyAccntAddAccount.txtBxAccnt1.text.length == 3) {
        frmMyAccntAddAccount.txtBxAccnt2.setFocus(true);
    }
    if (frmMyAccntAddAccount.txtBxAccnt1.text.length > 3) {
        frmMyAccntAddAccount.txtBxAccnt1.text = frmMyAccntAddAccount.txtBxAccnt1.text.substring(0, 3);
    }
}

function testfocusAccnt_two() {
    if (frmMyAccntAddAccount.txtBxAccnt2.text.length == 1) {
        frmMyAccntAddAccount.txtBxAccnt3.setFocus(true);
    }
    if (frmMyAccntAddAccount.txtBxAccnt2.text.length == 0) {
        frmMyAccntAddAccount.txtBxAccnt1.setFocus(true);
    }
    if (frmMyAccntAddAccount.txtBxAccnt2.text.length > 1) {
        frmMyAccntAddAccount.txtBxAccnt2.text = frmMyAccntAddAccount.txtBxAccnt2.text.substring(0, 1);
    }
}

function testfocusAccnt_three() {
    if (frmMyAccntAddAccount.txtBxAccnt3.text.length == 5) {
        frmMyAccntAddAccount.txtBxAccnt4.setFocus(true);
    }
    if (frmMyAccntAddAccount.txtBxAccnt3.text.length == 0) {
        frmMyAccntAddAccount.txtBxAccnt2.setFocus(true);
    }
    if (frmMyAccntAddAccount.txtBxAccnt3.text.length > 5) {
        frmMyAccntAddAccount.txtBxAccnt3.text = frmMyAccntAddAccount.txtBxAccnt3.text.substring(0, 5);
    }
}

function testfocusAccnt_four() {
    if (frmMyAccntAddAccount.txtBxAccnt4.text.length == 1) {
        frmMyAccntAddAccount.tbxNickname.setFocus(true);
    }
    if (frmMyAccntAddAccount.txtBxAccnt4.text.length == 0) {
        frmMyAccntAddAccount.txtBxAccnt3.setFocus(true);
    }
    if (frmMyAccntAddAccount.txtBxAccnt3.text.length > 1) {
        frmMyAccntAddAccount.txtBxAccnt4.text = frmMyAccntAddAccount.txtBxAccnt4.text.substring(0, 1);
    }
}

function testfocusAccnt_suffix() {
    if (frmMyAccntAddAccount.txtBxAccnt4.text.length == 1) {
        frmMyAccntAddAccount.tbxSuffix.setFocus(true);
    }
    if (frmMyAccntAddAccount.txtBxAccnt4.text.length > 1) {
        frmMyAccntAddAccount.txtBxAccnt4.text = frmMyAccntAddAccount.txtBxAccnt4.text.substring(0, 1);
    }
}

function testsuffix() {
    if (frmMyAccntAddAccount.tbxSuffix.text.length == 3) {
        frmMyAccntAddAccount.tbxNickname.setFocus(true);
    }
    if (frmMyAccntAddAccount.tbxSuffix.text.length > 3) {
        frmMyAccntAddAccount.tbxSuffix.text = frmMyAccntAddAccount.tbxSuffix.text.substring(0, 3);
    }
}

function fcsTxt() {
    if (frmMyAccntAddAccount.textbx1.text.length == 4) frmMyAccntAddAccount.textbx2.setFocus(true);
}

function fcsTxt1() {
    if (frmMyAccntAddAccount.textbx2.text.length == 4) {
        frmMyAccntAddAccount.textbx3.setFocus(true);
    }
    if (frmMyAccntAddAccount.textbx2.text.length == 0) {
        frmMyAccntAddAccount.textbx1.setFocus(true);
    }
}

function fcsTxt2() {
    if (frmMyAccntAddAccount.textbx3.text.length == 4) {
        frmMyAccntAddAccount.textbx4.setFocus(true);
    }
    if (frmMyAccntAddAccount.textbx3.text.length == 0) {
        frmMyAccntAddAccount.textbx2.setFocus(true);
    }
}

function fcsTxt4() {
    if (frmMyAccntAddAccount.textbx4.text.length == 4) {
        frmMyAccntAddAccount.tbxNickname.setFocus(true);
    }
    if (frmMyAccntAddAccount.textbx4.text.length == 0) {
        frmMyAccntAddAccount.textbx3.setFocus(true);
    }
}

function addAccount(lblflag) {
    gblSuffix = false;
    var newAccount = {};
    var bank = frmMyAccntAddAccount.btnBanklist.text;
    bank = (bank == kony.i18n.getLocalizedString("keyTMBBank")) ? true : false;
    //
    //Other bank
    var bankLogoURL = "";
    var bankLogo = "";
    if (bank) {
        bankLogoURL = "";
        bankLogo = "";
    } else {
        bankLogoURL = "https://" + appConfig.serverIp + "/" + appConfig.middlewareContext + "/Bank-logo/bank-logo-";
        bankLogo = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + gblBankCD + "&modIdentifier=BANKICON";
    }
    if (lblflag == "Bank" && bank == false) {
        var accntNo;
        if (MAX_ACC_LEN == 10 && MAX_ACC_LEN_LIST.indexOf(",") < 0) {
            accntNo = frmMyAccntAddAccount.txtBxAccnt1.text + "-" + frmMyAccntAddAccount.txtBxAccnt2.text + "-" + frmMyAccntAddAccount.txtBxAccnt3.text + "-" + frmMyAccntAddAccount.txtBxAccnt4.text;
        } else {
            accntNo = frmMyAccntAddAccount.tbxother.text;
        }
        newAccount = {
            "lblAccountNickname": kony.i18n.getLocalizedString("keylblNickname") + ":",
            "lblaccountNickNamevalue": frmMyAccntAddAccount.tbxNickname.text,
            "lblAccntNo": kony.i18n.getLocalizedString("AccountNumber") + ":",
            "lblAccntNoValue": accntNo,
            "lblbankname": kony.i18n.getLocalizedString("AccountName"),
            "lblbanknamevalue": gblAccntName,
            "btnDelete": " ",
            "isTMB": bank,
            "bankCD": gblBankCD,
            "bankLogo": bankLogo
        };
    } else if (lblflag == "Account" && popBankList.segBanklist.selectedIndex[1] == 2) {
        gblSuffix = true;
        var searchAccNo = "0" + frmMyAccntAddAccount.txtBxAccnt1.text + frmMyAccntAddAccount.txtBxAccnt2.text + frmMyAccntAddAccount.txtBxAccnt3.text + frmMyAccntAddAccount.txtBxAccnt4.text + frmMyAccntAddAccount.tbxSuffix.text
        for (var k = 0; k < gblAccntData.length; k++) {
            if (searchAccNo == gblAccntData[k].accntNo) {
                bankLogo = gblAccntData[k].productImage;
                break;
            }
        }
        newAccount = {
            "lblAccountNickname": kony.i18n.getLocalizedString("keylblNickname") + ":",
            "lblaccountNickNamevalue": frmMyAccntAddAccount.tbxNickname.text,
            "lblAccntNo": kony.i18n.getLocalizedString("AccountNo") + ":",
            "lblAccntNoValue": frmMyAccntAddAccount.txtBxAccnt1.text + "-" + frmMyAccntAddAccount.txtBxAccnt2.text + "-" + frmMyAccntAddAccount.txtBxAccnt3.text + "-" + frmMyAccntAddAccount.txtBxAccnt4.text,
            "lblbankname": kony.i18n.getLocalizedString("AccountName"),
            "lblbanknamevalue": gblAccntName,
            "lblAccntSuffix": kony.i18n.getLocalizedString("keylblAccountSuffix") + ":",
            "lblAccntsuffixValue": frmMyAccntAddAccount.tbxSuffix.text,
            "btnDelete": " ",
            "isTMB": bank,
            "bankCD": gblBankCD,
            "bankLogo": bankLogo
        };
    } else if (lblflag == "Account" && popBankList.segBanklist.selectedIndex[1] == 0) {
        var searchAccNo = "0000" + frmMyAccntAddAccount.txtBxAccnt1.text + frmMyAccntAddAccount.txtBxAccnt2.text + frmMyAccntAddAccount.txtBxAccnt3.text + frmMyAccntAddAccount.txtBxAccnt4.text;
        var tmpAccntNo = "";
        for (var k = 0; k < gblAccntData.length; k++) {
            tmpAccntNo = gblAccntData[k].accntNo;
            if (tmpAccntNo.length == 10) {
                tmpAccntNo = "0000" + tmpAccntNo;
            }
            if (searchAccNo == tmpAccntNo) {
                bankLogo = gblAccntData[k].productImage;
                break;
            }
        }
        newAccount = {
            "lblAccountNickname": kony.i18n.getLocalizedString("keylblNickname") + ":",
            "lblaccountNickNamevalue": frmMyAccntAddAccount.tbxNickname.text,
            "lblAccntNo": kony.i18n.getLocalizedString("AccountNo") + ":",
            "lblAccntNoValue": frmMyAccntAddAccount.txtBxAccnt1.text + "-" + frmMyAccntAddAccount.txtBxAccnt2.text + "-" + frmMyAccntAddAccount.txtBxAccnt3.text + "-" + frmMyAccntAddAccount.txtBxAccnt4.text,
            "btnDelete": " ",
            "lblbankname": kony.i18n.getLocalizedString("AccountName"),
            "lblbanknamevalue": gblAccntName,
            "isTMB": bank,
            "bankCD": gblBankCD,
            "bankLogo": bankLogo
        };
    } else if (lblflag == "Account" && popBankList.segBanklist.selectedIndex[1] == 1) {
        bankLogo = "prod_creditcard.png";
        var searchAccNo = frmMyAccntAddAccount.textbx1.text + frmMyAccntAddAccount.textbx2.text + frmMyAccntAddAccount.textbx3.text + frmMyAccntAddAccount.textbx4.text;
        var len = searchAccNo.length;
        var index = -10;
        if (len == 16) index = -16;
        for (var k = 0; k < gblAccntData.length; k++) {
            if (gblAccntData[k].accntNo.substr(index) == searchAccNo) {
                bankLogo = gblAccntData[k].productImage;
                break;
            }
        }
        newAccount = {
            "lblAccountNickname": kony.i18n.getLocalizedString("keylblNickname") + ":",
            "lblaccountNickNamevalue": frmMyAccntAddAccount.tbxNickname.text,
            "lblAccntNo": kony.i18n.getLocalizedString("keyCreditCardNo") + ":",
            "lblAccntNoValue": frmMyAccntAddAccount.textbx1.text + "-" + frmMyAccntAddAccount.textbx2.text + "-" + frmMyAccntAddAccount.textbx3.text + "-" + frmMyAccntAddAccount.textbx4.text,
            "btnDelete": " ",
            "lblbankname": kony.i18n.getLocalizedString("keyCardHolderName"),
            "lblbanknamevalue": gblAccntName,
            "isTMB": bank,
            "bankCD": gblBankCD,
            "bankLogo": bankLogo
        };
    } else if (lblflag == "Account" && popBankList.segBanklist.selectedIndex[1] > 2) {
        var searchAccNo = "0000" + frmMyAccntAddAccount.txtBxAccnt1.text + frmMyAccntAddAccount.txtBxAccnt2.text + frmMyAccntAddAccount.txtBxAccnt3.text + frmMyAccntAddAccount.txtBxAccnt4.text;
        for (var k = 0; k < gblAccntData.length; k++) {
            if (searchAccNo == gblAccntData[k].accntNo) {
                bankLogo = gblAccntData[k].productImage;
                break;
            }
        }
        newAccount = {
            "lblAccountNickname": kony.i18n.getLocalizedString("keylblNickname") + ":",
            "lblaccountNickNamevalue": frmMyAccntAddAccount.tbxNickname.text,
            "lblAccntNo": kony.i18n.getLocalizedString("AccountNo") + ":",
            "lblAccntNoValue": frmMyAccntAddAccount.txtBxAccnt1.text + "-" + frmMyAccntAddAccount.txtBxAccnt2.text + "-" + frmMyAccntAddAccount.txtBxAccnt3.text + "-" + frmMyAccntAddAccount.txtBxAccnt4.text,
            "btnDelete": " ",
            "lblbankname": kony.i18n.getLocalizedString("AccountName"),
            "lblbanknamevalue": gblAccntName,
            "isTMB": bank,
            "bankCD": gblBankCD,
            "bankLogo": bankLogo
        };
    } else {}
    //
    gblMyAccntAddTmpData.push(newAccount);
    //frmMyAccntConfirmationAddAccountPreShow();
    frmMyAccntConfirmationAddAccount.segBankAccnt.setData(gblMyAccntAddTmpData);
}

function hideDelete() {
    var newAccountList = [];
    var segData = frmMyAccntConfirmationAddAccount.segBankAccnt.data;
    for (var i = 0; i < segData.length; i++) {
        if (gblSuffix) {
            var newAccount = {
                "lblAccountNickname": kony.i18n.getLocalizedString("keylblNickname") + ":",
                "lblaccountNickNamevalue": segData[i]["lblaccountNickNamevalue"],
                "lblAccntNo": kony.i18n.getLocalizedString("AccountNo"),
                "lblAccntNoValue": segData[i]["lblAccntNoValue"],
                "btnDelete": " ",
                "lblbankname": kony.i18n.getLocalizedString("AccountName"),
                "lblbanknamevalue": segData[i]["lblbanknamevalue"],
                "lblAccntSuffix": kony.i18n.getLocalizedString("keylblAccountSuffix") + ":",
                "lblAccntsuffixValue": frmMyAccntAddAccount.tbxSuffix.text,
                "isTMB": segData[i]["isTMB"],
                "bankCD": segData[i]["bankCD"],
                "bankLogo": segData[i]["bankLogo"],
                template: hbxBankAccntComp
            };
        } else {
            var newAccount = {
                "lblAccountNickname": kony.i18n.getLocalizedString("keylblNickname") + ":",
                "lblaccountNickNamevalue": segData[i]["lblaccountNickNamevalue"],
                "lblAccntNo": kony.i18n.getLocalizedString("AccountNo"),
                "lblAccntNoValue": segData[i]["lblAccntNoValue"],
                "btnDelete": " ",
                "lblbankname": kony.i18n.getLocalizedString("AccountName"),
                "lblbanknamevalue": segData[i]["lblbanknamevalue"],
                "isTMB": segData[i]["isTMB"],
                "bankCD": segData[i]["bankCD"],
                "bankLogo": segData[i]["bankLogo"],
                template: hbxBankAccntComp
            };
        }
        newAccountList.push(newAccount);
        //
        var newAddedAccData = {
            "nickName": segData[i]["lblaccountNickNamevalue"],
            "accntNo": removeHyphenSPA(segData[i]["lblAccntNoValue"]),
            "bankCD": segData[i]["bankCD"],
            "accntName": segData[i]["lblbanknamevalue"],
            "accntStatus": "NA"
        };
        gblAccntData.push(newAddedAccData);
    }
    frmMyAccntConfirmationAddAccount.segBankAccnt.removeAll();
    frmMyAccntConfirmationAddAccount.segBankAccnt.setData(newAccountList);
}

function cleardataAddAccnt() {
    frmMyAccntAddAccount.lblAccntNo.setVisibility(true);
    frmMyAccntAddAccount.lblAccntNo.text = kony.i18n.getLocalizedString("AccountNumber");
    frmMyAccntAddAccount.hbxAccntNovalue.setVisibility(true);
    frmMyAccntAddAccount.lblAccountSuffix.setVisibility(false);
    frmMyAccntAddAccount.hbxCreditAccoutNoValue.setVisibility(false);
    frmMyAccntAddAccount.hbxAccountName.setVisibility(false);
    frmMyAccntAddAccount.line363582120412684.setVisibility(false);
    frmMyAccntAddAccount.vboxsuffix.setVisibility(false);
    if (flowSpa) {
        frmMyAccntAddAccount.hbxAccntNovalue.setVisibility(true);
        //frmMyAccntAddAccount.hbxOther.setVisibility(true); 
    } else {
        frmMyAccntAddAccount.hbxAccntNovalue.setVisibility(true);
        //frmMyAccntAddAccount.hbxOther.setVisibility(false);
    }
    frmMyAccntAddAccount.hbxbtnacntList.setVisibility(false);
    frmMyAccntAddAccount.btnBanklist.text = kony.i18n.getLocalizedString("keylblBankName");
    frmMyAccntAddAccount.btnacntlist.text = kony.i18n.getLocalizedString("keybtnPleaseSelect")
    frmMyAccntAddAccount.tbxSuffix.text = "";
    frmMyAccntAddAccount.tbxNickname.text = "";
    frmMyAccntAddAccount.textbx1.text = "";
    frmMyAccntAddAccount.textbx2.text = "";
    frmMyAccntAddAccount.textbx3.text = "";
    frmMyAccntAddAccount.textbx4.text = "";
    frmMyAccntAddAccount.txtBxAccnt1.text = "";
    frmMyAccntAddAccount.txtBxAccnt2.text = "";
    frmMyAccntAddAccount.txtBxAccnt3.text = "";
    frmMyAccntAddAccount.txtBxAccnt4.text = "";
    frmMyAccntAddAccount.tbxother.text = "";
    frmMyAccntAddAccount.tbxAccountName.text = "";
}

function clearAccNoAndNickName() {
    frmMyAccntAddAccount.tbxSuffix.text = "";
    frmMyAccntAddAccount.tbxNickname.text = "";
    frmMyAccntAddAccount.textbx1.text = "";
    frmMyAccntAddAccount.textbx2.text = "";
    frmMyAccntAddAccount.textbx3.text = "";
    frmMyAccntAddAccount.textbx4.text = "";
    frmMyAccntAddAccount.txtBxAccnt1.text = "";
    frmMyAccntAddAccount.txtBxAccnt2.text = "";
    frmMyAccntAddAccount.txtBxAccnt3.text = "";
    frmMyAccntAddAccount.txtBxAccnt4.text = "";
    frmMyAccntAddAccount.tbxother.text = "";
    frmMyAccntAddAccount.tbxAccountName.text = "";
    frmMyAccntAddAccount.btnacntlist.text = kony.i18n.getLocalizedString("keybtnPleaseSelect");
}
/**************************
						Confirm ADD Account
					**************************/
function onreturnAccnt() {
    frmMyAccntConfirmationAddAccount.hbxAdv.setVisibility(false);
    hbxReturnFtr.setVisibility(false);
    frmMyAccntConfirmationAddAccount.imgcomplete.setVisibility(false);
    hbxftrconfirm.setVisibility(true);
    getHeader(0, kony.i18n.getLocalizedString("keylblConfirmation"), btnAdd, btnAdd, cleardataAddAccnt);
}

function loadvalues() {
    frmMyAccntConfirmationAddAccount.hbxAdv.isVisible = false;;
    hbxReturn.isVisible = false;
    frmMyAccntConfirmationAddAccount.imgcomplete.isVisible = false;;
    hbxftrconfirm.isVisible = true;
    //frmMyAccntConfirmationAddAccount.segBankAccnt.btnDelete.setVisibility(true);
}

function dummy() {}

function onpopconfirmaccnt() {
    var pass = popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text;
    if (pass == null || pass == '') {
        setTransPwdFailedError(kony.i18n.getLocalizedString("emptyMBTransPwd"));
        return false;
    }
    /*if (trassactionPwdValidatn(pass) == false) {
    	//alert("Invalid Transation Password");
    	showAlert(kony.i18n.getLocalizedString("invalidTxnPwd"), kony.i18n.getLocalizedString("info"));
    	return;
    }
    //frmMyAccntConfirmationAddAccount.segBankAccnt.btnDelete.setVisibility(false);
    //Uncommetn this*/
    else if (pass.length > 0) {
        srvVerifyTxnPassword();
    }
    //Comment this
    //srvAddCompleteProcess();
}

function addCompleteScreen() {
    gblDelteIcn = true;
    hideDelete();
    swipeEnable = true; // Enable swaping
    hbxfooter.isVisible = false;
    frmMyAccntConfirmationAddAccount.lblHdrTxt.text = kony.i18n.getLocalizedString("keylblComplete");
    frmMyAccntConfirmationAddAccount.btnAddHeader.isVisible = false;
    frmMyAccntConfirmationAddAccount.hbxAdv.isVisible = true;
    if (flowSpa) {
        frmMyAccntConfirmationAddAccount.hbox473361467792157.isVisible = false;
        frmMyAccntConfirmationAddAccount.hbox473361467792205.isVisible = true;
    } else {
        frmMyAccntConfirmationAddAccount.hbxcnfrmftr.isVisible = false;
        frmMyAccntConfirmationAddAccount.hbxftraddmore.isVisible = true;
    }
    frmMyAccntConfirmationAddAccount.imgcomplete.isVisible = true;
    campaginService("image2448367682213126", "frmMyAccntConfirmationAddAccount", "M");
}

function showMyAccntList() {
    frmMyAccountList.show();
}

function showfrmviewCancel() {
    frmMyAccountView.show();
}

function ftrCancelConfimation() {
    frmMyAccntConfirmationAddAccount.btnAddHeader.skin = "btnAddMyAcnt";
    frmMyAccntConfirmationAddAccount.btnAddHeader.focusSkin = "btnAddMyAcnt";
    frmMyAccountList.show();
}

function ftrConfirm() {
    popAccntConfirmation.show();
}
////////////*************POP DELETE *********************************///
function showDeletepop(imgIcon, confText, callBackOnConfirm, callBackCancel) {
    //popupConfirmation.imgPopConfirmIcon.src = imgIcon;
    //popupConfirmation.containerHeight = 40;
    //popupConfirmation.containerHeightReference = constants.HEIGHT_BY_DEVICE_REFERENCE;
    if (gblsegID == "segTMBAccntDetails") {
        popupConfirmation.lblPopupConfText.text = kony.i18n.getLocalizedString("keylblDelAcc");
        //"Are you sure to delete this TMB account ?"
    } else if (gblsegID == "segOtherBankAccntDetails") {
        popupConfirmation.lblPopupConfText.text = kony.i18n.getLocalizedString("keylblDelAcc");
    } else {
        popupConfirmation.lblPopupConfText.text = confText;
    }
    popupConfirmation.btnpopConfConfirm.onClick = callBackOnConfirm;
    popupConfirmation.btnPopupConfCancel.onClick = callBackCancel;
    popupConfirmation.btnPopupConfCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
    popupConfirmation.btnpopConfConfirm.text = kony.i18n.getLocalizedString("keyYes");
    popupConfirmation.show();
}
//function ConfirmBtn() {
//	popupConfirmation.dismiss();
//	myAccountDelService();
//}
function YesBtn() {
    popupConfirmation.dismiss();
    myAccountEditServiceNew("delete");
}

function CancelBtn() {
    popupConfirmation.dismiss();
}

function ConfirmBtnCache() {
    popupConfirmation.dismiss();
    DelAcntCache();
}

function DelAcntCache() {
    var row = frmMyAccntConfirmationAddAccount.segBankAccnt.selectedIndex[1];
    // is this record is TMB bank accnt
    var isTMB = frmMyAccntConfirmationAddAccount.segBankAccnt.selectedItems[0].isTMB;
    // Delete that row
    gblMyAccntAddTmpData.splice(row, 1);
    // Decrease total added accnt in cache count
    TOTAL_AC_ADD--;
    // if non TMB recored, decrease non TMB added in cache count
    if (!isTMB) NON_TMB_ADD--;
    if (TOTAL_AC_ADD < MAX_ACC_ADD) {
        frmMyAccntConfirmationAddAccount.btnAddHeader.skin = "btnAddMyAcnt";
        frmMyAccntConfirmationAddAccount.btnAddHeader.onClick = addMore;
        frmMyAccntConfirmationAddAccount.btnAddHeader.setVisibility(true);
    }
    // If cache become empty navigate to accnt list
    if (gblMyAccntAddTmpData.length == 0 || TOTAL_AC_ADD == 0) {
        frmMyAccountList.show();
    }
    frmMyAccntConfirmationAddAccount.segBankAccnt.setData(gblMyAccntAddTmpData);
}