function setDataOnSegBillersListBB() {
    var currForm = kony.application.getCurrentForm();
    var segData = [{
        imgBillerLogo: {
            src: "bill_img01.png"
        },
        lblBillerNickname: "MyElectricity1",
        lblBillerCompCode: "MEA(1234)",
        lblRef1: "Ref1:",
        lblRef1Value: "212-22434-1",
        lblRef2: "Ref2:",
        lblRef2Value: "212-22434-1",
        hdnAccType: "My Normal Savings",
        hdnBalacne: "20,0000B",
        hdnAccountNum: "1-234-2342-1",
        hdnBillerNameEN: "",
        imgBillersRtArrow: {
            src: "bg_arrow_right.png"
        }
    }, {
        imgBillerLogo: {
            src: "bill_img02.png"
        },
        lblBillerNickname: "MyElectricity2",
        lblBillerCompCode: "MEA(1235)",
        lblRef1: "Ref1:",
        lblRef1Value: "212-22434-2",
        lblRef2: "Ref2:",
        lblRef2Value: "212-22434-2",
        hdnAccType: "My TMB Savings",
        hdnBalacne: "10,0000B",
        hdnAccountNum: "9-234-2342-1",
        hdnBillerNameEN: "Pavan",
        imgBillersRtArrow: {
            src: "bg_arrow_right.png"
        }
    }, {
        imgBillerLogo: {
            src: "bill_sug_02.png"
        },
        lblBillerNickname: "MyElectricity3",
        lblBillerCompCode: "MEA(1236)",
        lblRef1: "Ref1:",
        lblRef1Value: "212-22434-3",
        lblRef2: "Ref2:",
        lblRef2Value: "212-22434-3",
        hdnAccType: "TMB Savings Account",
        hdnBalacne: "30,0000B",
        hdnAccountNum: "4-377-2742-9",
        hdnBillerNameEN: "Madhu",
        imgBillersRtArrow: {
            src: "bg_arrow_right.png"
        }
    }];
    currForm.segBillersList.data = segData;
}

function setDataOnSegSugBillersListBB() {
    var currForm = kony.application.getCurrentForm();
    segSugData = [{
        imgSugBillerLogo: {
            src: "bill_sug_01.png"
        },
        lblSugBillerCompCode: "True Interest(1234)",
        btnSugBillerAdd: {
            text: "Add",
            skn: "btnIBaddsmall"
        }
    }, {
        imgSugBillerLogo: {
            src: "bill_sug_01.png"
        },
        lblSugBillerCompCode: "True1 Interest(1234)",
        btnSugBillerAdd: {
            text: "Add",
            skn: "btnIBaddsmall"
        }
    }, {
        imgSugBillerLogo: {
            src: "bill_sug_01.png"
        },
        lblSugBillerCompCode: "MyElectricity3(1234)",
        btnSugBillerAdd: {
            text: "Add",
            skn: "btnIBaddsmall"
        }
    }, {
        imgSugBillerLogo: {
            src: "bill_sug_01.png"
        },
        lblSugBillerCompCode: "True2 Interest(1234)",
        btnSugBillerAdd: {
            text: "Add",
            skn: "btnIBaddsmall"
        }
    }];
    currForm.segSuggestedBillersList.data = segSugData;
}