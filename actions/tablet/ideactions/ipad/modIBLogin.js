lastIBLoginDatefromCRM = "";

function createBarGraphIB(width, btnSkin) {
    var lblUse = new kony.ui.Label({
        "id": "lblUse" + btnSkin + width,
        "isVisible": true,
        "text": " ",
        "skin": btnSkin
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": width
    }, {});
    frmIBPostLoginDashboard.hbxBarGraph.add(lblUse);
    frmIBAccntSummary.hbxBarGraph.add(lblUse);
}

function btnIBLoginClickCheck() {
    if (frmIBPreLogin.txtUserId.text != "") {
        if (frmIBPreLogin.txtPassword.text != "") {
            if (frmIBPreLogin.hboxCaptchaText.isVisible) {
                if (frmIBPreLogin.txtCaptchaText.text != "") {
                    captchaValidation.call(this, frmIBPreLogin.txtCaptchaText.text);
                } else {
                    var alert_seq8_act0 = kony.ui.Alert({
                        "message": kony.i18n.getLocalizedString("keyCaptchaRequired"),
                        "alertType": constants.ALERT_TYPE_ERROR,
                        "alertTitle": "Alert",
                        "yesLabel": "Yes",
                        "noLabel": "",
                        "alertIcon": "",
                        "alertHandler": null
                    }, {});
                    frmIBPreLogin.txtCaptchaText.setFocus(true);
                }
            } else {
                IBLoginService.call(this);
            }
        } else {
            var alert_seq4_act0 = kony.ui.Alert({
                "message": kony.i18n.getLocalizedString("keyPasswordRequired"),
                "alertType": constants.ALERT_TYPE_ERROR,
                "alertTitle": "Alert",
                "yesLabel": "Yes",
                "noLabel": "",
                "alertIcon": "",
                "alertHandler": null
            }, {});
            frmIBPreLogin.txtPassword.setFocus(true);
        }
    } else {
        var alert_seq2_act0 = kony.ui.Alert({
            "message": kony.i18n.getLocalizedString("keyLoginRequired"),
            "alertType": constants.ALERT_TYPE_ERROR,
            "alertTitle": "Alert",
            "yesLabel": "Yes",
            "noLabel": "",
            "alertIcon": "",
            "alertHandler": null
        }, {});
        frmIBPreLogin.txtUserId.setFocus(true);
    }
}

function IBLoginService() {
    //kony.os.startSecureTransaction();
    LoginFormName = kony.application.getCurrentForm();
    showLoadingScreenPopup();
    IBuserID = LoginFormName.txtUserId.text
        //if (IBuserID.length > 7 && IBuserID.length < 21 && IBuserID.match(/[a-z]/ig)){
    var inputParam = {};
    inputParam["loginId"] = kony.string.trim(LoginFormName.txtUserId.text);
    inputParam["userid"] = kony.string.trim(LoginFormName.txtUserId.text);
    inputParam["password"] = kony.string.trim(LoginFormName.txtPassword.text);
    if (LoginFormName.hboxCaptchaText.isVisible) inputParam["capchaID"] = LoginFormName.txtCaptchaText.text;
    gblUserName = LoginFormName.txtUserId.text;
    gblLoginType = "Login";
    invokeServiceSecureAsync("IBVerifyLoginEligibility", inputParam, callBackIBLoginService)
}
var gblMYInboxCountFrom = false;

function callBackIBLoginService(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            //ENH_149 : IB Reactivation flow
            if (resulttable["reactivationFlow"] == "true") {
                dismissLoadingScreenPopup();
                gblSetPwd = true;
                gblUserName = LoginFormName.txtUserId.text;
                IBTnCText();
                return;
            } else if (resulttable["reactivationFlow"] == "false") {
                dismissLoadingScreenPopup();
                LoginFormName.txtPassword.text = "";
                LoginFormName.hboxCaptcha.setVisibility(false);
                LoginFormName.hboxCaptchaText.setVisibility(false);
                LoginFormName.hboxLine2.setVisibility(false);
                LoginFormName.txtCaptchaText.text = "";
                if (resulttable["errCode"] == "VrfyOTPErr00002") {
                    //You have reached max number of invalid attempts. Your account is locked. Please re-apply.
                    //showAlertIB(kony.i18n.getLocalizedString("ECVrfyActLockErr00002"), kony.i18n.getLocalizedString("info"));
                    //make captcha related stuff invisibles
                    popupIBTempPwd.lblText.text = kony.i18n.getLocalizedString("ECVrfyActLockErr00002");
                    popupIBTempPwd.show();
                    return false;
                } else if (resulttable["errCode"] == "JavaErr00001") {
                    //Error occured. Please contact TMB helpdesk.
                    showAlertIB(kony.i18n.getLocalizedString("ECJavaErr00001"), kony.i18n.getLocalizedString("info"));
                    return false;
                } else if (resulttable["errCode"] == "VrfyTknErr00003") {
                    //Activation Code expired for Reset Password / Unlock Otp
                    //showAlert(kony.i18n.getLocalizedString("ECVrfyActExpErr00003"), kony.i18n.getLocalizedString("info"));
                    popupIBTempPwd.lblText.text = kony.i18n.getLocalizedString("ECVrfyActExpErr00003");
                    popupIBTempPwd.show();
                    return false;
                } else {
                    //System Error
                    showAlertIB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
                    return false;
                }
            }
            gblSetPwd = false;
            if (resulttable["statusCode"] == "statusDesc") {
                var errMessage = kony.i18n.getLocalizedString("crmerrMsg");
                alert(errMessage)
            } else if (resulttable["statusCode"] == "errMsg") {
                var errMessage = kony.i18n.getLocalizedString("errMsg");
                alert(errMessage)
            } else {
                // code to get account summary 
                LoginFormName.txtUserId.text = "";
                gblLangPrefIB = true;
                // To show the count in Menu for inbox notification
                //gblMYInboxCount = 5;
                gblMYInboxCountFrom = false;
                //unreadInboxMessagesTrackerLocalDBSyncIB()
                gblCalledMasterBillerService = 0; // global variable being used in my activites to show details
                //frmIBPostLoginDashboard.btnMenuMyInbox.text = gblMYInboxCount
                if (resulttable.mobNo != "") var glbMobileNo = resulttable.mobNo;
                else glbMobileNo = "";
                if (resulttable["lastIBLoginSuccessDate"] != null) {
                    lastIBLoginDatefromCRM = resulttable["lastIBLoginSuccessDate"];
                }
                //resulttable["forceChangePasswordFlag"] = "true" // this should be commented
                //ENH_126: Removed Migration flow completely
                if (resulttable["userIDcaptchaCode"] == "1") {
                    frmIBCMChngUserID.hboxCaptcha.setVisibility(true);
                    frmIBCMChngUserID.hboxCaptchaText.setVisibility(true);
                    frmIBCMChngUserID.imgcaptcha.base64 = resulttable["captchaImage"];
                    frmIBCMChngUserID.txtCaptchaText.text = "";
                } else if (resulttable["userIDcaptchaCode"] == "0") {
                    frmIBCMChngUserID.hboxCaptcha.setVisibility(false);
                    frmIBCMChngUserID.hboxCaptchaText.setVisibility(false);
                    frmIBCMChngUserID.txtCaptchaText.text = "";
                }
                //activityLogServiceCall("009", "", "01", "", gblUserName, kony.os.deviceInfo().userAgent, "", "", "", "");
                //gblLoggedIn = true;	
                hbxFooterPrelogin.linkhotpromo.setVisibility(true);
                if (gblIsNewOffersExists) {
                    hbxFooterPrelogin.linkHotPromoImg.isVisible = true;
                } else {
                    hbxFooterPrelogin.linkHotPromoImg.isVisible = false;
                }
                isFromLogin = true;
                loadCRMProfileDataToPage(resulttable);
                loginCompositeExec();
                //commenting the 1st line as it causes null issues 2 nd line commented on request of TMB
                //localStorage.clear();
                //loadResourceBundleIB();
            }
        } else {
            if (resulttable["opstatus"] == 1) {
                showAlertIB(kony.i18n.getLocalizedString("keyNetworkError"), kony.i18n.getLocalizedString("info"));
                //loginFailUpdate();
                dismissLoadingScreenPopup();
                return false;
            } else if (resulttable["opstatus"] == 8005) {
                if (resulttable["captchaCode"] == "0") {
                    LoginFormName.hboxCaptcha.setVisibility(false);
                    LoginFormName.hboxCaptchaText.setVisibility(false);
                    LoginFormName.hboxLine2.setVisibility(false);
                } else {
                    //dismissLoadingScreenPopup();
                    if (resulttable["captchaCode"] == "1") {
                        LoginFormName.hboxCaptcha.setVisibility(true);
                        LoginFormName.hboxCaptchaText.setVisibility(true);
                        LoginFormName.hboxLine2.setVisibility(true);
                        LoginFormName.imgcaptcha.base64 = resulttable["captchaImage"];
                        LoginFormName.txtCaptchaText.text = "";
                        LoginFormName.txtPassword.text = "";
                    } else {
                        LoginFormName.hboxCaptcha.setVisibility(false);
                        LoginFormName.hboxCaptchaText.setVisibility(false);
                        LoginFormName.hboxLine2.setVisibility(false);
                        LoginFormName.txtCaptchaText.text = "";
                    }
                }
                if (resulttable["faultdetail"]) {
                    var faultDetail = "";
                    if (resulttable["faultdetail"].match(/K000000300005/gi) != null) {
                        faultDetail = "K000000300005";
                        showAlertIB(kony.i18n.getLocalizedString("keyIBLoginError002"), kony.i18n.getLocalizedString("info"));
                        //alert("User is already logged in or Browser was force closed, Please try after sometime ");
                        dismissLoadingScreenPopup();
                    } else if (resulttable["faultdetail"].match(/K000000300007/gi) != null) {
                        faultDetail = "K000000300007";
                        showAlertIB(kony.i18n.getLocalizedString("ECUserLoggedIn"), kony.i18n.getLocalizedString("info"));
                        //alert("User is already logged in or Browser was force closed, Please try after sometime ");
                        dismissLoadingScreenPopup();
                    } else if (resulttable["faultdetail"].match(/K000000300001/gi) != null) {
                        faultDetail = "K000000300001";
                        showAlertIB(kony.i18n.getLocalizedString("keyIBLoginError001"), kony.i18n.getLocalizedString("info"));
                        //alert("User profile is not setup");
                        dismissLoadingScreenPopup();
                    } else if (resulttable["faultdetail"].match(/E10001/gi) != null) {
                        faultDetail = "E10001";
                        showAlertIB(kony.i18n.getLocalizedString("keyIBLoginError001"), kony.i18n.getLocalizedString("info"));
                        //alert("Account does not exist"); to fix DEF3565 
                        dismissLoadingScreenPopup();
                    } else if (resulttable["faultdetail"].match(/E10403/gi) != null) {
                        faultDetail = "E10403";
                        showAlertIB(kony.i18n.getLocalizedString("keyIBLoginError003"), kony.i18n.getLocalizedString("info"));
                        //alert("Account blocked due to max failed attempts");
                        //BlockNotificationCrmProfileInq();
                        dismissLoadingScreenPopup();
                    } else if (resulttable["faultdetail"].match(/E10020/gi) != null) {
                        faultDetail = "E10020";
                        showAlertIB(kony.i18n.getLocalizedString("keyIBLoginError001"), kony.i18n.getLocalizedString("info"));
                        //alert("Either User ID or password is incorrect"); // wrong password scenario
                        if (resulttable["captchaCode"] == "1") {
                            LoginFormName.hboxCaptcha.setVisibility(true);
                            LoginFormName.hboxCaptchaText.setVisibility(true);
                            LoginFormName.hboxLine2.setVisibility(true);
                            LoginFormName.imgcaptcha.base64 = resulttable["captchaImage"];
                            LoginFormName.txtCaptchaText.text = "";
                            LoginFormName.txtPassword.text = "";
                        } else {
                            LoginFormName.hboxCaptcha.setVisibility(false);
                            LoginFormName.hboxCaptchaText.setVisibility(false);
                            LoginFormName.hboxLine2.setVisibility(false);
                        }
                        //IBmodifyfailLoginattempt();
                        dismissLoadingScreenPopup();
                    } else if (resulttable["faultdetail"].match(/K000000300009/gi) != null) {
                        faultDetail = "K000000300009";
                        showAlertIB(kony.i18n.getLocalizedString("keyIBLoginError009"), kony.i18n.getLocalizedString("info"));
                        dismissLoadingScreenPopup();
                        //alert("Logging not allowed")
                    } else if (resulttable["faultdetail"].match(/K000000300010/gi) != null) {
                        faultDetail = "K000000300010";
                        showAlertIB(kony.i18n.getLocalizedString("keyIBLoginError010"), kony.i18n.getLocalizedString("info"));
                        dismissLoadingScreenPopup();
                        //alert("Logging not allowed")
                    } else if (resulttable["faultdetail"].match(/K000000300011/gi) != null) {
                        faultDetail = "K000000300011";
                        showAlertIB(kony.i18n.getLocalizedString("keyIBLoginError011"), kony.i18n.getLocalizedString("info"));
                        dismissLoadingScreenPopup();
                        //alert("Logging not allowed")
                    } else if (resulttable["faultdetail"].match(/E10105/gi) != null) {
                        faultDetail = "E10105";
                        showAlertIB(kony.i18n.getLocalizedString("keyIBLoginError004"), kony.i18n.getLocalizedString("info"));
                        dismissLoadingScreenPopup();
                    } else if (resulttable["faultdetail"].match(/K000000300012/gi) != null) {
                        faultDetail = "K000000300012";
                        showAlertIB(kony.i18n.getLocalizedString("keyIBLoginError012"), kony.i18n.getLocalizedString("info"));
                        dismissLoadingScreenPopup();
                    } else {
                        if (resulttable["faultdetail"] != undefined) alert("Error : Wrong User ID or Password has been entered, or you are already logged in, please try again in 10 minutes");
                        else alert(kony.i18n.getLocalizedString("keyUndefinedError"));
                        dismissLoadingScreenPopup();
                        return false;
                    }
                    //activityLogServiceCall("009", "", "02", "", gblUserName, kony.os.deviceInfo()
                    //		.userAgent, faultDetail, "", "", "");
                } else if (resulttable["errCode"] == "VrfyOTPCtrErr00001") {
                    //You have entered incorrect ActivationCode
                    showAlertIB(kony.i18n.getLocalizedString("ECVrfyACTErr00001"), kony.i18n.getLocalizedString("info"));
                    dismissLoadingScreenPopup();
                    return false;
                } else {
                    dismissLoadingScreenPopup();
                    if (resulttable["captchaCode"] == "1") {
                        alert(kony.i18n.getLocalizedString("msgIBCaptchaWrong"));
                        LoginFormName.hboxCaptcha.setVisibility(true);
                        LoginFormName.hboxCaptchaText.setVisibility(true);
                        LoginFormName.hboxLine2.setVisibility(true);
                        LoginFormName.imgcaptcha.base64 = resulttable["captchaImage"];
                        LoginFormName.txtCaptchaText.text = "";
                        LoginFormName.txtPassword.text = "";
                    } else {
                        LoginFormName.hboxCaptcha.setVisibility(false);
                        LoginFormName.hboxCaptchaText.setVisibility(false);
                        LoginFormName.hboxLine2.setVisibility(false);
                        LoginFormName.txtCaptchaText.text = "";
                    }
                }
            } else if (resulttable["opstatus"] == 99999) {
                if (resulttable["errmsg"] != undefined) {
                    alert(resulttable["errmsg"]);
                    window.location.reload(true);
                }
            } else {
                alert(kony.i18n.getLocalizedString("ECGenericError"));
            }
            dismissLoadingScreenPopup();
        }
    }
    //LoginFormName.txtUserId.text = "";
    LoginFormName.txtPassword.text = "";
}

function LoginflowPartyInqService() {
    var inputparam = {};
    if (isFromLogin) inputparam["TriggerEmail"] = "yes";
    isFromLogin = false;
    invokeServiceSecureAsync("partyInquiry", inputparam, LoginFlowPartyInqCallBack);
}

function LoginFlowPartyInqCallBack(status, resulttable) {
    if (status == 400) {
        var isError = showCommonAlertIB(resulttable)
        if (isError) return false;
        if (resulttable["opstatus"] == 0) {
            gblCustomerName = resulttable["customerName"];
            gblCustomerNameTh = resulttable["customerNameTH"];
            gblIsFromMigration = false;
            //gblEmailId = resulttable["emailId"];
            if (kony.i18n.getCurrentLocale() == "th_TH") hbxIBPostLogin.lblName.text = gblCustomerNameTh;
            else hbxIBPostLogin.lblName.text = gblCustomerName;
            var len = hbxIBPostLogin.lblName.text.length;
            if (hbxIBPostLogin.lblName.text.length < 24) {} else {
                hbxIBPostLogin.label502795003206049.containerWeight = 14 - (len - 24);
                //hbxIBPostLogin.vbox866794452740809.containerWeight=21+(len-24);
            }
            //When the object is UNDEFINED, the respective attribute is not accessigned. Hence adding this check.
            if (resulttable["ContactNums"] != undefined) {
                for (var i = 0; i < resulttable["ContactNums"].length; i++) {
                    if (resulttable["ContactNums"][i]["PhnType"] == 'Mobile') gblPHONENUMBER = resulttable["ContactNums"][i]["PhnNum"];
                }
            }
            // Calling function to show dashboard screen only if login happens from the main LoginPage. - for fixing DEF13640
            //if(LoginFormName.id == "frmIBPreLogin")
            //else{
            //setHeaderPrePostLogin();
            //	addIBMenu();
            //	dismissLoadingScreenPopup();
            //}
            IBcrmProfileInq();
        } else {
            //invokeIBCRMProfileUpdateLogout();
            alert(kony.i18n.getLocalizedString("ECGenericError"));
        }
    }
}

function IBcrmProfileInq() {
    inputParam = {};
    invokeServiceSecureAsync("crmProfileInq", inputParam, IBcrmProfileInqCallback)
}

function callActivationComplete() {
    //FATCA CR code starts
    if (kony.i18n.getCurrentLocale() == "th_TH") {
        hbxIBPreLogin.btnEng.skin = "btnEngLangOff";
        hbxIBPreLogin.btnThai.skin = "btnThaiLangOn";
    } else {
        hbxIBPreLogin.btnEng.skin = "btnEngLangOn";
        hbxIBPreLogin.btnThai.skin = "btnThaiLangOff";
    }
    gblLoggedIn = true;
    if (gblFATCAUpdateFlag == "0") {
        gblFATCAFlow = "login";
        showFACTAInfoIB();
    } else if (gblFATCAUpdateFlag == "8" || gblFATCAUpdateFlag == "9" || gblFATCAUpdateFlag == "P" || gblFATCAUpdateFlag == "R" || gblFATCAUpdateFlag == "X") {
        gblFATCAFlow = "login";
        setFATCAIBGoToBranchInfoMessage("N");
    } else {
        onclickActivationCompleteStart("");
    }
    //FATCA CR code ends
}

function IBcrmProfileInqCallback(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            gblFBCode = resulttable["facebookId"];
            /* Uncommented code to handle language preference in case forcepasswordchangeflag == false as fix for DEF15762*/
            if (resulttable["languageCd"] != null) {
                gblLangPrefMB = resulttable["languageCd"];
                var langPrefVariable = "";
                if (gblLangPrefMB == "EN") {
                    langPrefVariable = "en_US";
                } else {
                    langPrefVariable = "th_TH";
                }
                kony.i18n.setCurrentLocaleAsync(langPrefVariable, callActivationComplete, callActivationComplete, "");
            } else {
                onclickActivationCompleteStart();
            }
            //var IBLastLogin = resulttable["lastIbLoginSucessDateFrmt"];
            var IBLastLogin = lastIBLoginDatefromCRM;
            if (IBLastLogin != null && IBLastLogin != "") {
                hbxIBPostLogin.lblLastLoginTime.setVisibility(true);
                hbxIBPostLogin.lblLastLoginTimeVal.setVisibility(true);
                hbxIBPostLogin.lblLastLoginTimeVal.text = IBLastLogin;
            } else {
                hbxIBPostLogin.lblLastLoginTime.setVisibility(false);
                hbxIBPostLogin.lblLastLoginTimeVal.setVisibility(false);
            }
            gblXferLnkdAccnts = resulttable["p2pLinkedAcct"];
            gblEmailId = resulttable["emailId"];
            gblcrmId = resulttable["crmProfileId"];
            var randomnum = Math.floor((Math.random() * 10000) + 1);
            //gblMyProfilepic = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=Y&personalizedId=&billerId=";
            gblMyProfilepic = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=Y&personalizedId=&billerId=&dummy=" + randomnum;
            frmIBPostLoginDashboard.imgProfile.imageScaleMode = constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO;
            frmIBAccntSummary.imgProfile.imageScaleMode = constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO;
            frmIBPostLoginDashboard.imgProfile.src = gblMyProfilepic;
            frmIBAccntSummary.imgProfile.src = gblMyProfilepic;
            hbxIBPostLogin.imgProfilePic.src = gblMyProfilepic
            gblUserLockStatusIB = resulttable["ibUserStatusIdTr"];
            gblMBFlowStatus = resulttable["mbFlowStatusIdRs"];
            menuOptionEnableTokenActivation();
        }
    }
    //dismissLoadingScreenPopup();
}
/*function BlockNotificationCrmProfileInqCallBack(status, resulttable) {
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			gblEmailId = resulttable["emailAddr"];
			sendNotificationIB();
			
		} else {
			alert(kony.i18n.getLocalizedString("ECGenericError"));
		}
	}
}*/
/*function BlockNotificationCrmProfileModCallBack(status, resulttable) {
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			if (resulttable["errCode"] == "JavaErr00001") {
				showAlertIB(kony.i18n.getLocalizedString("ECJavaErr00001"), kony.i18n.getLocalizedString("info"));
				return false;
			} else {
				//activityLogServiceCall("005","0000000000000","01");		
				
			}
		} else {
			showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
			return false;
		}
	}
}*/
/*function sendNotificationIB() {
 	inputParams = {};
 	inputParams["deliveryMethod"] = "Email";
	inputParams["notificationType"] = "Email";
	inputParams["noSendInd"] = "0";
	//inputParams["emailId"] = gblEmailId;
	inputParams["notificationSubject"] = "User Account Locked";
	inputParams["notificationContent"] = "Your Account has been Locked. Please contact TMB customer care.";
 	invokeServiceSecureAsync("NotificationAdd", inputParams, sendNotificationCallBackIB);
}*/
/*function sendNotificationCallBackIB(status, callBackResponse) {
	if (status == 400) {
		if (callBackResponse["opstatus"] == 0) {
 		}
	} else {
 
	}
}*/
/*function IBmodifyfailLoginattemptCallBack(status, resulttable) {
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			
			inputParam = {};
			inputParam["ibUserId"] = gblUserName;
			inputParam["crmId"] = resulttable["crmProfileId"]
			inputParam["actionType"] = "27";
 			invokeServiceSecureAsync("crmProfileModWithUserId", inputParam, BlockNotificationCrmProfileModCallBack)
		} else {
			alert(kony.i18n.getLocalizedString("ECGenericError"));
		}
	}
}*/
/*function postLoginCLang()
{
		
		if (gblLangPrefMB != null && gblLangPrefIB == true	) {
							if (gblLangPrefMB == "EN") {
								langchange("en_US");
								hbxIBPreLogin.btnEng.skin = "btnEngLangOn";
								hbxIBPreLogin.btnThai.skin = "btnThaiLangOff";
								hbxIBPostLogin.btnEng.skin = "btnEngLangOn";
								hbxIBPostLogin.btnThai.skin = "btnThaiLangOff";
							} else if (gblLangPrefMB == "TH") {
								langchange("th_TH");
								hbxIBPreLogin.btnEng.skin = "btnEngLangOff";
								hbxIBPreLogin.btnThai.skin = "btnThaiLangOn";
								hbxIBPostLogin.btnEng.skin = "btnEngLangOff";
								hbxIBPostLogin.btnThai.skin = "btnThaiLangOn";
							}
					}		
		
	if(gblLangPrefIB == true){	
	frmIBAccntSummary.imgProfile.setVisibility(false);
	frmIBPostLoginDashboard.imgProfile.setVisibility(false);
	}
	gblLangPrefIB = false;
}*/
function loginCompositeExec() {
    showLoadingScreenPopup();
    inputParam = {};
    inputParam["rqUUId"] = "";
    inputParam["LoginInd"] = "login";
    inputParam["TriggerEmail"] = "yes";
    inputParam["activationCompleteFlag"] = gblLoginType;
    invokeServiceSecureAsync("LoginProcessServiceExecuteIB", inputParam, loginPProcessIBCallBack)
}

function loginPProcessIBCallBack(status, resultTable) {
    if (status == 400) {
        if (resultTable["opstatus"] == 0) {
            //MIB-1207
            //Code to check if ANY ID is enabled or not
            gblShowAnyIDRegistration = "false";
            if (undefined != resultTable["setyourID"]) {
                gblShowAnyIDRegistration = resultTable["setyourID"];
            }
            // take the showAnnouncementPage field from response set to gbl variable "displayAnnoucementtoUser"
            displayAnnoucementtoUser = false;
            if (undefined != resultTable["registerWithAnyId"] && null != resultTable["registerWithAnyId"]) {
                //registerWithAnyId is N means user not register with Any Id with TMB
                if ("N" == resultTable["registerWithAnyId"]) {
                    displayAnnoucementtoUser = true;
                }
            }
            //below code is added to implement MIB-945
            gblIsNewOffersExists = false;
            if (undefined != resultTable["unreadMyOffers"] && null != resultTable["unreadMyOffers"]) {
                //unreadMyOffers is Y means user has new offers
                //alert("unreadMyOffers : "+resultTable["unreadMyOffers"]);
                if ("Y" == resultTable["unreadMyOffers"]) {
                    gblIsNewOffersExists = true;
                }
            }
            //process getinboxunreadcount response
            if (resultTable["status"] == "1") {
                gblMYInboxCount = "0";
            } else if (resultTable["status"] == "0") {
                gblMYInboxCount = resultTable["unread"];
            } else {
                gblMYInboxCount = "0";
            }
            //process getInboxMessagesUnreadCount response
            if (resultTable["statusMessagesUnread"] == "1") {
                gblMyInboxMSGTotalCount = "0";
            } else if (resultTable["statusMessagesUnread"] == "0") {
                gblMyInboxMSGTotalCount = resultTable["messagesUnread"];
            } else {
                gblMyInboxMSGTotalCount = "0";
            }
            gblMyInboxTotalCount = parseInt(gblMYInboxCount) + parseInt(gblMyInboxMSGTotalCount);
            if (gblMYInboxCountFrom == false) {
                /* removing as per ENH_028 - IB Menu on Home Screen
                frmIBPostLoginDashboard.btnMenuMyInbox.text = gblMyInboxTotalCount
                */
            } else {
                var currForm = kony.application.getCurrentForm();
                currForm.btnMenuMyInbox.text = (parseInt(gblMyInboxTotalCount) == 0 ? "" : gblMyInboxTotalCount);
                gblMenuSelection = 3;
                segMyInboxLoad();
            }
            gblMYInboxCountFrom = false;
            //Party inq response processing
            if (resultTable["opstatusPartyInq"] != null && resultTable["opstatusPartyInq"] == 0 && resultTable["StatusCode_partyInq"] != null && resultTable["StatusCode_partyInq"] == 0) {
                gblCustomerName = resultTable["customerName"];
                gblCustomerNameTh = resultTable["customerNameTH"];
                gblIsFromMigration = false;
                if (kony.i18n.getCurrentLocale() == "th_TH") hbxIBPostLogin.lblName.text = gblCustomerNameTh;
                else hbxIBPostLogin.lblName.text = gblCustomerName;
                var len = hbxIBPostLogin.lblName.text.length;
                if (hbxIBPostLogin.lblName.text.length < 24) {} else {
                    hbxIBPostLogin.label502795003206049.containerWeight = 14 - (len - 24);
                }
                gblPHONENUMBER = resultTable["PhnNum"];
                //FATCA CR code starts
                if (gblLoginType == "Login") {
                    gblFATCASkipCounter = parseInt(resultTable["FATCASkipCounter"]);
                    gblFATCAUpdateFlag = resultTable["FatcaFlag"];
                } else {
                    gblFATCAUpdateFlag = "N";
                }
                //FATCA CR code ends
                //Any ID Enhancement Begins
                gblCustomerIDType = resultTable["customerIDType"];
                gblCustomerIDValue = resultTable["customerIDValue"];
                //Any ID Enhancement Ends
                kony.i18n.setCurrentLocaleAsync(langPrefVariable, callActivationComplete, callActivationComplete, "");
            } else {
                dismissLoadingScreenPopup();
                alert(kony.i18n.getLocalizedString("ECGenericError"));
                return;
            }
            // menuOptionEnableTokenActivation response processing
            var tokenMenuFlag = false
            var tokenEnable = resultTable["menuEnableStatus"];
            if (tokenEnable == "true") {
                gblTokenActivationFlag = true;
            }
        } else {
            dismissLoadingScreenPopup();
            alert(kony.i18n.getLocalizedString("ECGenericError"));
            return;
        }
    }
}

function loadCRMProfileDataToPage(resultTable) {
    gblFBCode = resultTable["faceBookId"];
    if (resultTable["languageCd"] != null) {
        gblLangPrefMB = resultTable["languageCd"];
        langPrefVariable = "";
        if (gblLangPrefMB == "EN") {
            langPrefVariable = "en_US";
        } else {
            langPrefVariable = "th_TH";
        }
        // kony.i18n.setCurrentLocaleAsync(langPrefVariable, callActivationComplete, callActivationComplete, "");
    }
    /*else{			
    							onclickActivationCompleteStart();
    					}*/
    //var IBLastLogin = resulttable["lastIbLoginSucessDateFrmt"];
    var IBLastLogin = lastIBLoginDatefromCRM;
    if (IBLastLogin != null && IBLastLogin != "") {
        hbxIBPostLogin.lblLastLoginTime.setVisibility(true);
        hbxIBPostLogin.lblLastLoginTimeVal.setVisibility(true);
        hbxIBPostLogin.lblLastLoginTimeVal.text = IBLastLogin;
    } else {
        hbxIBPostLogin.lblLastLoginTime.setVisibility(false);
        hbxIBPostLogin.lblLastLoginTimeVal.setVisibility(false);
    }
    gblXferLnkdAccnts = resultTable["linkedAcct"];
    gblEmailId = resultTable["emailId"];
    var randomnum = Math.floor((Math.random() * 10000) + 1);
    gblMyProfilepic = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=Y&personalizedId=&billerId=&dummy=" + randomnum;
    frmIBPostLoginDashboard.imgProfile.imageScaleMode = constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO;
    frmIBAccntSummary.imgProfile.imageScaleMode = constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO;
    frmIBPostLoginDashboard.imgProfile.src = gblMyProfilepic;
    frmIBAccntSummary.imgProfile.src = gblMyProfilepic;
    hbxIBPostLogin.imgProfilePic.src = gblMyProfilepic
    gblUserLockStatusIB = resultTable["ibFlowStatus"];
    gblMBFlowStatus = resultTable["mbFlowStatus"];
}

function refreshCaptchaImage() {
    showLoadingScreenPopup();
    LoginFormName = kony.application.getCurrentForm();
    var inputParam = {};
    invokeServiceSecureAsync("getCaptchaOnRefresh", inputParam, getCaptchaOnRefreshCallback)
}

function getCaptchaOnRefreshCallback(status, resulttable) {
    dismissLoadingScreenPopup();
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            LoginFormName.imgcaptcha.base64 = resulttable["captchaImage"];
            LoginFormName.txtCaptchaText.text = "";
        }
    }
}