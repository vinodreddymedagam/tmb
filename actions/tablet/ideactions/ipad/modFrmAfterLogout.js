function onLogoutPopUpConfrm() {
    //Changes as per ENH_223
    // moving the code gblFlagMenu = "" from above function to here as SPA the touch events are effected
    if (flowSpa) {
        assignPostLoginFormSetUndefined();
    }
    gblFlagMenu = "";
    isSignedUser = false;
    var currentForm = kony.application.getCurrentForm();
    showLoadingScreen();
    frmMenu.flexdynamicContainer.removeAll();
    //popUpLogout.destroy();
    //changes for 30771 only for android
    invokeLogoutService();
}
/**
 * Function to invoke the logout service call
 *  no return values
 */
function invokeLogoutService() {
    //kony.application.showLoadingScreen(frmLoading, "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
    showLoadingScreen();
    inputParam = {};
    var channelId = "";
    if (flowSpa) {
        channelId = "01";
    } else {
        channelId = "02";
    }
    if (GLOBAL_MB_CHANNEL != null && GLOBAL_MB_CHANNEL != "") {
        if (flowSpa) {
            channelId = GLOBAL_IB_CHANNEL;
        } else {
            channelId = GLOBAL_MB_CHANNEL;
        }
    }
    if (flowSpa) {
        inputParam["deviceId"] = "";
    } else {
        inputParam["deviceId"] = GBL_UNIQ_ID;
    }
    inputParam["channelId"] = channelId;
    inputParam["timeOut"] = GBL_Time_Out;
    var locale = kony.i18n.getCurrentLocale();
    if (locale == "en_US") {
        inputParam["languageCd"] = "EN";
    } else {
        inputParam["languageCd"] = "TH";
    }
    invokeServiceSecureAsync("logOutTMB", inputParam, callBackLogOutService)
}
/*****************************************************************
 *	Name    : getDeviceID
 *	Author  : Kony Solutions
 *	Purpose : To get the device ID
 ******************************************************************/
/*function getDeviceID() {
	var hardwareID = ""
	var deviceInfo = kony.os.deviceInfo();
	if (deviceInfo["name"] == "android") {
		hardwareID = deviceInfo["deviceid"];
		
	}
	if (deviceInfo["name"] == "iPhone") {
		hardwareID = deviceInfo["deviceid"];
		
	}
	return hardwareID
}*/
/**
 * call back to handle logout service responce.Usagetime is returned from logout service.
 *  no return values
 */
function callBackLogOutService(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            var usageTime = resulttable["usageTime"];
        }
        if (flowSpa) {
            isMenuShown = false;
            TMBUtil.DestroyForm(frmAccountSummaryLanding);
            frmSPALogin = null;
            frmSPALoginGlobals();
            //When IDLE TIME OUT of the application && When the application is running in the BACKGROUND. 
            //We are a WHITE STRIP in the login page and hence adding the below code.
            //Increasing and setting it back to original. Playing around with the VBOX LEFT && VBOX RIGHT widths (containerWeight)
            if (GBL_Time_Out == "true") {
                frmSPALogin.vboxLeft.containerWeight = frmSPALogin.vboxLeft.containerWeight - 10;
                frmSPALogin.vboxRight.containerWeight = frmSPALogin.vboxRight.containerWeight + 10;
            }
            frmSPALogin.show();
            if (GBL_Time_Out == "true") {
                frmSPALogin.vboxLeft.containerWeight = frmSPALogin.vboxLeft.containerWeight + 10;
                frmSPALogin.vboxRight.containerWeight = frmSPALogin.vboxRight.containerWeight - 10;
            }
        } else {
            //When IDLE TIME OUT of the application && When the application is running in the BACKGROUND. 
            //We are a WHITE STRIP in the login page and hence adding the below code.
            //Increasing and setting it back to original. Playing around with the VBOX LEFT && VBOX RIGHT widths (containerWeight)
            if (GBL_Time_Out == "true") {
                //frmMBPreLoginAccessesPin.vboxLeft.containerWeight = frmMBPreLoginAccessesPin.vboxLeft.containerWeight - 10;
                //frmMBPreLoginAccessesPin.vboxRight.containerWeight = frmMBPreLoginAccessesPin.vboxRight.containerWeight + 10;
            }
            gblTouchShow = false;
            if (GBL_Time_Out == "true") {
                //frmMBPreLoginAccessesPin.vboxLeft.containerWeight = frmMBPreLoginAccessesPin.vboxLeft.containerWeight + 10;
                //frmMBPreLoginAccessesPin.vboxRight.containerWeight = frmMBPreLoginAccessesPin.vboxRight.containerWeight - 10;
            }
        }
        clearGlobalVariables();
        spaFormGlobalsCall();
        cleanPreorPostForms();
        gblFromLogout = true;
        frmMBPreLoginAccessesPin.show();
        kony.application.dismissLoadingScreen();
    }
}