/*frmMBEStatementComplete*/
function frmMBEStatementCompleteMenuPreshow() {
    if (gblCallPrePost) {
        isMenuShown = false;
        isSignedUser = true;
        frmMBEStatementComplete.scrollboxMain.scrollToEnd();
        DisableFadingEdges.call(this, frmMBEStatementComplete);
        frmMBEStatementCompletePreShow.call(this);
    }
}

function frmMBEStatementCompleteMenuPostshow() {
    if (gblCallPrePost) {
        campaginService.call(this, 'img1', 'frmMBEStatementComplete', 'M');
        if (flowSpa) {
            isMenuRendered = false;
            isMenuShown = false;
            frmMBEStatementComplete.scrollboxMain.scrollToEnd();
        }
    }
    assignGlobalForMenuPostshow();
}
/*frmMBEStatementConfirmation*/
function frmMBEStatementConfirmationMenuPreshow() {
    if (gblCallPrePost) {
        var deviceInfo = kony.os.deviceInfo().name;
        var deviceInfo1 = kony.os.deviceInfo();
        var deviceHght = deviceInfo1["deviceHeight"];
        isMenuShown = false;
        frmMBEStatementConfirmation.scrollboxMain.scrollToEnd();
        frmMBEStatementConfirmation.imgHeaderMiddle.src = "arrowtop.png"
        frmMBEStatementConfirmation.imgHeaderRight.src = "empty.png"
        DisableFadingEdges.call(this, frmMBEStatementConfirmation);
        frmMBEStatementConfirmationPreShow.call(this);
    }
}

function frmMBEStatementConfirmationMenuPostshow() {
    if (gblCallPrePost) {
        isMenuRendered = false;
        isMenuShown = false;
        frmMBEStatementConfirmation.scrollboxMain.scrollToEnd();
    }
    assignGlobalForMenuPostshow();
}
/*frmMBEStatementLanding*/
function frmMBEStatementLandingMenuPreshow() {
    if (gblCallPrePost) {
        var deviceInfo = kony.os.deviceInfo().name;
        var deviceInfo1 = kony.os.deviceInfo();
        var deviceHght = deviceInfo1["deviceHeight"];
        isMenuShown = false;
        frmMBEStatementLanding.scrollboxMain.scrollToEnd();
        frmMBEStatementLanding.imgHeaderMiddle.src = "arrowtop.png"
        frmMBEStatementLanding.imgHeaderRight.src = "empty.png"
        DisableFadingEdges.call(this, frmMBEStatementLanding);
        frmMBEStatementLandingPreShow.call(this);
    }
}

function frmMBEStatementLandingMenuPostshow() {
    if (gblCallPrePost) {
        isMenuRendered = false;
        isMenuShown = false;
        frmMBEStatementLanding.scrollboxMain.scrollToEnd();
        if (gblEmailAddr.length < 4) {
            frmMBEStatementLanding.txtEStatementEmail.setFocus(true);
        }
    }
    assignGlobalForMenuPostshow();
}
/*frmMBEStatementProdFeature*/
function frmMBEStatementProdFeatureMenuPreshow() {
    if (gblCallPrePost) {
        var deviceInfo = kony.os.deviceInfo().name;
        var deviceInfo1 = kony.os.deviceInfo();
        var deviceHght = deviceInfo1["deviceHeight"];
        isMenuShown = false;
        frmMBEStatementProdFeature.scrollboxMain.scrollToEnd();
        frmMBEStatementProdFeature.imgHeaderMiddle.src = "arrowtop.png"
        frmMBEStatementProdFeature.imgHeaderRight.src = "empty.png"
        DisableFadingEdges.call(this, frmMBEStatementProdFeature);
        frmMBEStatementProdFeaturePreShow.call(this);
    }
}

function frmMBEStatementProdFeatureMenuPostshow() {
    if (gblCallPrePost) {
        isMenuRendered = false;
        isMenuShown = false;
        frmMBEStatementProdFeature.scrollboxMain.scrollToEnd();
    }
    assignGlobalForMenuPostshow();
}
/*frmMBEStatementTnC*/
function frmMBEStatementTnCMenuPreshow() {
    if (gblCallPrePost) {
        frmMBEStatementTnC.scrollboxMain.scrollToEnd();
        isMenuShown = false;
        isSignedUser = true;
        DisableFadingEdges.call(this, frmMBEStatementTnC);
        frmMBEStatementTnCPreShow.call(this);
    }
}

function frmMBEStatementTnCMenuPostshow() {
    if (gblCallPrePost) {
        frmMBEStatementTnC.scrollboxMain.scrollToEnd();
    }
    assignGlobalForMenuPostshow();
}