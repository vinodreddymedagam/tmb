function onClickPointRedemptionFromAccountDetailsIB(curr_form) {
    var sbStr = gblAccountTable["custAcctRec"][gblIndex]["accId"];
    var length = sbStr.length;
    sbStr = sbStr.substring(length - 4, length);
    var accNickName;
    if (isNotBlank(gblAccountTable["custAcctRec"][gblIndex]["acctNickName"])) {
        accNickName = gblAccountTable["custAcctRec"][gblIndex]["acctNickName"];
    } else {
        if (kony.i18n.getCurrentLocale() == "en_US") {
            accNickName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"] + " " + sbStr;
        } else {
            accNickName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"] + " " + sbStr;
        }
    }
    curr_form.lblCardNickName.text = accNickName;
    curr_form.lblCardNumberValue.text = gblAccountTable["custAcctRec"][gblIndex]["accountNoFomatted"];
    curr_form.imgCredicard.src = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + gblAccountTable["custAcctRec"][gblIndex]["ICON_ID"] + "&modIdentifier=PRODICON";
    curr_form.lblAvailablePointsValue.text = commaFormattedPoints(gblAccountTable["custAcctRec"][gblIndex]["bonusPointAvail"]);
    curr_form.lblExpireDateValue.text = commaFormattedPoints(gblAccountTable["custAcctRec"][gblIndex]["bonusPointUsed"]);
    curr_form.lblExpireDate.text = kony.i18n.getLocalizedString("keyPointExpiringOn") + " " + reformatDate(gblAccountTable["custAcctRec"][gblIndex]["stmtDate"]) + ":";
}

function onClickCancelPointRedemptionIB() {
    frmIBAccntSummary.show();
    frmIBAccntSummary.segAccountDetails.selectedIndex = [0, gblIndex];
    showIBAccountDetails();
    OTPcheckOnAccountSummary();
    // TMBUtil.DestroyForm(frmIBPointRedemptionConfirmation);		no need to delete as it also cause issue in IE8
    // TMBUtil.DestroyForm(frmIBPointRedemptionComplete);
}

function frmIBPointRedemptionProdFeaturePreShow() {
    var pntRedeemTnCMsg = kony.i18n.getLocalizedString('keyPointRedmptionProdFeature');
    pntRedeemTnCMsg = pntRedeemTnCMsg.replace("{start_time}", pntRedeem_StartTime);
    pntRedeemTnCMsg = pntRedeemTnCMsg.replace("{end_time}", pntRedeem_EndTime);
    frmIBPointRedemptionProdFeature.lblPointRedemptionProdFeature.text = pntRedeemTnCMsg;
    //frmIBPointRedemptionProdFeature.lblPointRedemptionProdFeature.text = kony.i18n.getLocalizedString('keyPointRedmptionProdFeature');
    var currentLocale = kony.i18n.getCurrentLocale();
    var pointRedemptionProdFeature_image_name = "PointRedemptionProdFeatureEN";
    if (currentLocale == "th_TH") {
        pointRedemptionProdFeature_image_name = "PointRedemptionProdFeatureTH";
    }
    var pointRedemptionProdFeature_image = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + pointRedemptionProdFeature_image_name + "&modIdentifier=PRODUCTPACKAGEIMG";
    frmIBPointRedemptionProdFeature.imgPointRedemptionProdFeature.src = pointRedemptionProdFeature_image;
    frmIBPointRedemptionProdFeature.imgPointRedemptionProdFeature.setVisibility(true);
}

function frmIBPointRedemptionTnCPreShow() {
    var currentLocale = kony.i18n.getCurrentLocale();
    var locale = kony.i18n.getCurrentLocale();
    var input_param = {};
    if (locale == "en_US") {
        input_param["localeCd"] = "en_US";
    } else {
        input_param["localeCd"] = "th_TH";
    }
    input_param["moduleKey"] = 'TMBPointRedemption';
    showLoadingScreenPopup();
    invokeServiceSecureAsync("readUTFFile", input_param, setIBPointRedemptionTnC);
}

function setIBPointRedemptionTnC(status, result) {
    if (status == 400) {
        dismissLoadingScreenPopup();
        if (result["opstatus"] == 0) {
            frmIBPointRedemptionTnC.lblPointRedemtionDesc.text = result["fileContent"];
        }
    }
}

function loadTermsNConditionPointRedemtionIB() {
    var input_param = {};
    var locale = kony.i18n.getCurrentLocale();
    if (locale == "en_US") {
        input_param["localeCd"] = "en_US";
    } else {
        input_param["localeCd"] = "th_TH";
    }
    input_param["moduleKey"] = 'TMBPointRedemption';
    invokeServiceSecureAsync("readUTFFile", input_param, loadTNCPointRedemtionIBCallBack);
}

function loadTNCPointRedemtionIBCallBack(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
            var data = result["fileContent"];
            var tncTitle = kony.i18n.getLocalizedString("keyTermsNConditions");
            if (tncTitle == "Terms & Conditions") {
                tncTitle = "TermsandConditions";
            }
            showPopup(tncTitle, data);
        } else {
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}

function onClickEmailTnCMBPointRedemptionIB() {
    showLoadingScreenPopup()
    var inputparam = {};
    inputparam["channelName"] = "Internet Banking";
    inputparam["channelID"] = "01";
    inputparam["notificationType"] = "Email"; // always email
    inputparam["phoneNumber"] = gblPHONENUMBER;
    inputparam["mail"] = gblEmailId;
    inputparam["customerName"] = gblCustomerName;
    inputparam["localeCd"] = kony.i18n.getCurrentLocale();
    inputparam["moduleKey"] = "PointRedemption";
    //if (kony.i18n.getCurrentLocale() == "en_US") {
    inputparam["productName"] = "";
    //} else {
    inputparam["productNameTH"] = "";
    //}
    invokeServiceSecureAsync("TCEMailService", inputparam, callBackEmailTnCPointRedemptionIB);
}

function callBackEmailTnCPointRedemptionIB(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
            var StatusCode = result["StatusCode"];
            var Severity = result["Severity"];
            var StatusDesc = result["StatusDesc"];
            if (StatusCode == 0) {
                showAlert(kony.i18n.getLocalizedString("keytermOpenAcnt"), kony.i18n.getLocalizedString("info"));
                dismissLoadingScreenPopup();
            } else {
                dismissLoadingScreenPopup();
                return false;
            }
        } else {
            dismissLoadingScreenPopup()
        }
    }
}

function IBonCLickNextPointsTnC() {
    //Language setting on first time landing on this page
    frmIBPointRedemptionLanding.lblRewards.text = kony.i18n.getLocalizedString("keyChooseYourReward") + ":";
    frmIBPointRedemptionLanding.lblMemberNumber.text = kony.i18n.getLocalizedString("keyRewardsMemberNo");
    frmIBPointRedemptionLanding.lblLastName.text = kony.i18n.getLocalizedString("keyLastName");
    frmIBPointRedemptionLanding.lblPointsBeingRedeemed.text = kony.i18n.getLocalizedString("keyPointsbeingredeemed");
    frmIBPointRedemptionLanding.lblRemainingPoints.text = kony.i18n.getLocalizedString("keyRemainingPoints");
    frmIBPointRedemptionLanding.txtLastName.placeholder = kony.i18n.getLocalizedString("keyLastNameFirstFiveChars");
    frmIBPointRedemptionLanding.lblCashbackTo.text = kony.i18n.getLocalizedString("keyCashbackToAccount");
    //End Language setting
    //Resetting rewards name and common box
    frmIBPointRedemptionLanding.hbxRewardsName.isVisible = false;
    frmIBPointRedemptionLanding.hbxCommonRewardsEntry.isVisible = false;
    frmIBPointRedemptionLanding.txtPointsBeingRedeemed.text = "";
    frmIBPointRedemptionLanding.txtRemainingPoints.text = "";
    frmIBPointRedemptionLanding.txtAmntCashbackRopMile.text = "";
    frmIBPointRedemptionLanding.hbxAmntCashbackRopMile.isVisible = false;
    frmIBPointRedemptionLanding.btnNext.skin = "btnIB158disabled";
    frmIBPointRedemptionLanding.btnNext.hoverSkin = "btnIB158disabled";
    frmIBPointRedemptionLanding.btnNext.setEnabled(false);
    //gblrewardsData=[];
    //End rewards name and common box reset
    //Resetting cashback box
    frmIBPointRedemptionLanding.hbxCashBack.isVisible = false;
    frmIBPointRedemptionLanding.lblTranLandToName.text = "";
    frmIBPointRedemptionLanding.lblTranLandToNum.text = "";
    frmIBPointRedemptionLanding.imgCashbackTo.src = "avatar.png";
    //End cashback reset
    //Resetting Air asia , thai airways box
    frmIBPointRedemptionLanding.txtMemberNumber.text = "";
    frmIBPointRedemptionLanding.txtLastName.text = "";
    frmIBPointRedemptionLanding.hbxMemberNoLastName.isVisible = false;
    //End Air asia , thai airways reset
    frmIBPointRedemptionLanding.show();
}

function frmIBPointRedemptionLandingPreShow() {
    onClickPointRedemptionFromAccountDetailsIB(frmIBPointRedemptionLanding);
}

function IBloadPointRedemptionOptions() {
    var inputParam = {};
    if (isNotBlankObject(gblrewardsData)) {
        IBreloadRewardsList();
    } else {
        showLoadingScreen();
        invokeServiceSecureAsync("getCardRewards", inputParam, IBloadPointRedemptionOptionsCallBack);
    }
}

function IBloadPointRedemptionOptionsCallBack(status, resulttable) {
    if (status == 400) {
        dismissLoadingScreen();
        if (resulttable["opstatus"] == "0") {
            if (resulttable["rewardsDataSet"].length > 0) {
                gblrewardsData = [];
                gblrewardsData = resulttable;
                IBreloadRewardsList();
            }
        } else {
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        }
    }
}

function IBreloadRewardsList() {
    if (gblrewardsData["rewardsDataSet"].length > 0) {
        rewardsDataSegment = [];
        for (i = 0; i < gblrewardsData["rewardsDataSet"].length; i++) {
            var pointRedemptionRewards_image_name = gblrewardsData["rewardsDataSet"][i]["logoImageName"];
            var pointRedemptionRewards_image = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + pointRedemptionRewards_image_name + "&modIdentifier=PRODUCTPACKAGEIMG";
            var rewardsName = "";
            if (kony.i18n.getCurrentLocale() == "th_TH") {
                rewardsName = gblrewardsData["rewardsDataSet"][i]["rewardNameTh"];
            } else {
                rewardsName = gblrewardsData["rewardsDataSet"][i]["rewardNameEn"];
            }
            var rewardsRate = commaFormattedPoints(gblrewardsData["rewardsDataSet"][i]["rewardPoint"]) + " " + kony.i18n.getLocalizedString("keyMBPoints") + " = " + commaFormattedPoints(gblrewardsData["rewardsDataSet"][i]["rewardUnit"]) + " " + kony.i18n.getLocalizedString(gblrewardsData["rewardsDataSet"][i]["tphraseRewardUnitKey"]);
            var tempRewardRecord = {
                "imgRewardsLogo": pointRedemptionRewards_image,
                "lblIBRewardsName": rewardsName,
                "lblIBRewardsRate": rewardsRate,
                "hiddenRewardCode": gblrewardsData["rewardsDataSet"][i]["rewardCode"]
            }
            rewardsDataSegment.push(tempRewardRecord);
        }
        frmIBPointRedemptionLanding.segRewardsList.setData(rewardsDataSegment);
        frmIBPointRedemptionLanding.hbxIBRewardsList.isVisible = true;
        frmIBPointRedemptionLanding.hbxAccountList.isVisible = false;
        frmIBPointRedemptionLanding.hbxwaterWheel.isVisible = false;
    }
}

function onClickRewardsAnywhereHideRightPanel() {
    frmIBPointRedemptionLanding.hbxIBRewardsList.isVisible = false;
    frmIBPointRedemptionLanding.hbxAccountList.isVisible = false;
    frmIBPointRedemptionLanding.hbxwaterWheel.isVisible = true;
}

function IBonSelectingReward() {
    gblSelectedReward = frmIBPointRedemptionLanding.segRewardsList.selectedIndex[1];
    frmIBPointRedemptionLanding.lblRewardsName.text = frmIBPointRedemptionLanding.segRewardsList.data[gblSelectedReward]["lblIBRewardsName"];
    frmIBPointRedemptionLanding.lblRewardsRate.text = frmIBPointRedemptionLanding.segRewardsList.data[gblSelectedReward]["lblIBRewardsRate"];
    frmIBPointRedemptionLanding.imgRewardSelected.src = frmIBPointRedemptionLanding.segRewardsList.data[gblSelectedReward]["imgRewardsLogo"];
    frmIBPointRedemptionConfirmation.lblRewardsName.text = frmIBPointRedemptionLanding.segRewardsList.data[gblSelectedReward]["lblIBRewardsName"];
    frmIBPointRedemptionConfirmation.lblRewardsRate.text = frmIBPointRedemptionLanding.segRewardsList.data[gblSelectedReward]["lblIBRewardsRate"];
    frmIBPointRedemptionConfirmation.imgRewardSelected.src = frmIBPointRedemptionLanding.segRewardsList.data[gblSelectedReward]["imgRewardsLogo"];
    frmIBPointRedemptionComplete.lblRewardsName.text = frmIBPointRedemptionLanding.segRewardsList.data[gblSelectedReward]["lblIBRewardsName"];
    frmIBPointRedemptionComplete.lblRewardsRate.text = frmIBPointRedemptionLanding.segRewardsList.data[gblSelectedReward]["lblIBRewardsRate"];
    frmIBPointRedemptionComplete.imgRewardSelected.src = frmIBPointRedemptionLanding.segRewardsList.data[gblSelectedReward]["imgRewardsLogo"];
    frmIBPointRedemptionLanding.hbxRewardsName.isVisible = true;
    frmIBPointRedemptionLanding.hbxIBRewardsList.isVisible = false;
    frmIBPointRedemptionLanding.hbxwaterWheel.isVisible = true;
    frmIBPointRedemptionLanding.hbxAccountList.isVisible = false;
    frmIBPointRedemptionLanding.hbxCommonRewardsEntry.isVisible = true;
    frmIBPointRedemptionLanding.hbxAmntCashbackRopMile.isVisible = true;
    frmIBPointRedemptionLanding.txtAmntCashbackRopMile.setEnabled(false);
    frmIBPointRedemptionLanding.txtRemainingPoints.setEnabled(false);
    frmIBPointRedemptionLanding.txtLastName.text = "";
    frmIBPointRedemptionLanding.txtMemberNumber.text = "";
    frmIBPointRedemptionLanding.txtPointsBeingRedeemed.text = "";
    frmIBPointRedemptionLanding.txtRemainingPoints.text = "";
    frmIBPointRedemptionLanding.txtAmntCashbackRopMile.text = "";
    frmIBPointRedemptionLanding.lblTranLandToName.text = "";
    frmIBPointRedemptionLanding.lblTranLandToNum.text = "";
    frmIBPointRedemptionLanding.imgCashbackTo.src = "avatar.png";
    frmIBPointRedemptionLanding.btnNext.skin = "btnIB158disabled";
    frmIBPointRedemptionLanding.btnNext.setEnabled(false);
    var rewardType = IBidentifyRewardType();
    if (rewardType == "Cashback") {
        //Cashback Related Begin
        frmIBPointRedemptionLanding.hbxCashBack.isVisible = true;
        frmIBPointRedemptionLanding.hbxMemberNoLastName.isVisible = false;
        frmIBPointRedemptionLanding.lblAmntCashbackRopMile.text = kony.i18n.getLocalizedString("keyTotalCashbackAmount");
        //Cashback Related End
    } else if (rewardType == "AirAsia") {
        //AirAsia Related Begin
        frmIBPointRedemptionLanding.hbxMemberNoLastName.isVisible = true;
        frmIBPointRedemptionLanding.hbxCashBack.isVisible = false;
        frmIBPointRedemptionLanding.txtMemberNumber.maxTextLength = 10;
        frmIBPointRedemptionLanding.lblAmntCashbackRopMile.text = kony.i18n.getLocalizedString("keyReceivedBigPoint");
        //AirAsia Related End
    } else if (rewardType == "ROP") {
        //Thai Airways Related Begin
        frmIBPointRedemptionLanding.hbxMemberNoLastName.isVisible = true;
        frmIBPointRedemptionLanding.hbxCashBack.isVisible = false;
        frmIBPointRedemptionLanding.txtMemberNumber.maxTextLength = 7;
        frmIBPointRedemptionLanding.lblAmntCashbackRopMile.text = kony.i18n.getLocalizedString("keyReceivedROPMile");
        //Thai Airways Related End
    } else if (rewardType == "OtherMile") {
        //Other Airways Related Begin
        frmIBPointRedemptionLanding.hbxMemberNoLastName.isVisible = true;
        frmIBPointRedemptionLanding.hbxCashBack.isVisible = false;
        frmIBPointRedemptionLanding.txtMemberNumber.maxTextLength = 10;
        frmIBPointRedemptionLanding.lblAmntCashbackRopMile.text = kony.i18n.getLocalizedString("keyReceivedMile");
        //Other Airways Related End
    } else {
        alert("Sorry, the selected Reward can not be redemmed online, please contact nearest TMB Branch");
        return false;
    }
}

function IBidentifyRewardType() {
    var rewardType = gblrewardsData["rewardsDataSet"][gblSelectedReward]["rewardType"];
    var rewardName = gblrewardsData["rewardsDataSet"][gblSelectedReward]["rewardNameEn"];
    if (rewardType == "01") {
        if (rewardName.match(/Air Asia/gi) || rewardName.match(/Big Point/gi)) {
            return "AirAsia";
        }
        if (rewardName.match(/ROYAL ORCHID/gi) || rewardName.match(/ROP/gi)) {
            return "ROP";
        }
        return "OtherMile";
    } else if (rewardType == "00") {
        return "Cashback";
    }
    return "NotRecognisedRewardType";
}

function sortToAccountForCashback(arr, currentForm) {
    arrCreditCard = [];
    arrSaving = [];
    for (var i = 0; i < arr.length; i++) {
        if (arr[i]["accountType"] == kony.i18n.getLocalizedString("CreditCard")) {
            arrCreditCard.push(arr[i]);
        } else {
            arrSaving.push(arr[i]);
        }
    }
    arrCreditCard = selectionSortAlgorithm(arrCreditCard);
    arrSaving = selectionSortAlgorithm(arrSaving);
    arrCreditCard = arrCreditCard.concat(arrSaving);
    return putCurrentCardToFirst(arrCreditCard, currentForm);
}

function selectionSortAlgorithm(arr) {
    var minIdx, temp,
        len = arr.length;
    for (var i = 0; i < len; i++) {
        minIdx = i;
        for (var j = i + 1; j < len; j++) {
            if (arr[j]["lblAccountName"] < arr[minIdx]["lblAccountName"]) {
                minIdx = j;
            }
        }
        temp = arr[i];
        arr[i] = arr[minIdx];
        arr[minIdx] = temp;
    }
    return arr;
}

function putCurrentCardToFirst(arr, currentForm) {
    var currentCard, currentCardIndex;
    for (var i = 0; i < arr.length; i++) {
        if (currentForm.lblCardNickName.text == arr[i]["lblAccountName"]) {
            currentCard = arr[i];
            currentCardIndex = i;
        }
    }
    arr.splice(currentCardIndex, 1);
    for (var i = arr.length; i > 0; i--) {
        arr[i] = arr[i - 1];
    }
    arr[0] = currentCard;
    return arr;
}

function onClickToAccountForCashbackIB() {
    gblAccountsToCashback = [];
    for (var i = 0; i < gblAccountTable["custAcctRec"].length; i++) {
        var accountName;
        var accountNo;
        var accountNameEN;
        var accountNameTH;
        var accountNoTenDigit;
        var accountNoUnformatted;
        var isTMBAccount = gblAccountTable["custAcctRec"][i]["receiveCashBackEligible"];
        if (parseInt(isTMBAccount)) {
            if ((gblAccountTable["custAcctRec"][i]["acctNickName"]) == null || (gblAccountTable["custAcctRec"][i]["acctNickName"]) == '') {
                var sbStr = gblAccountTable["custAcctRec"][i]["accId"];
                var length = sbStr.length;
                sbStr = sbStr.substring(length - 4, length);
                if (kony.i18n.getCurrentLocale() == "th_TH") accountName = gblAccountTable["custAcctRec"][i]["ProductNameThai"] + " " + sbStr;
                else accountName = gblAccountTable["custAcctRec"][i]["ProductNameEng"] + " " + sbStr;
                accountNameEN = gblAccountTable["custAcctRec"][i]["ProductNameEng"] + " " + sbStr;
                accountNameTH = gblAccountTable["custAcctRec"][i]["ProductNameThai"] + " " + sbStr;
            } else {
                accountName = gblAccountTable["custAcctRec"][i]["acctNickName"];
                accountNameEN = gblAccountTable["custAcctRec"][i]["acctNickName"];
                accountNameTH = gblAccountTable["custAcctRec"][i]["acctNickName"];
            }
            if (gblAccountTable["custAcctRec"][i]["accType"] == kony.i18n.getLocalizedString("CreditCard")) {
                accountNo = gblAccountTable["custAcctRec"][i]["accountNoFomatted"];
            } else {
                accountNoUnformatted = gblAccountTable["custAcctRec"][i]["accId"];
                accountNoTenDigit = accountNoUnformatted.substring(accountNoUnformatted.length - 10, accountNoUnformatted.length);
                accountNo = formatAccountNo(accountNoTenDigit);
            }
            imageName = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + gblAccountTable["custAcctRec"][i]["ICON_ID"] + "&modIdentifier=PRODICON";
            var tempToCashbackAccount = {
                "imgAccountType": imageName,
                "lblAccountName": accountName,
                "lblAccountNumber": accountNo,
                "lblBankName": "TMB",
                "lblAccountNameEN": accountNameEN,
                "lblAccountNameTH": accountNameTH,
                "hiddenAccountId": gblAccountTable["custAcctRec"][i]["accId"],
                "accountType": gblAccountTable["custAcctRec"][i]["accType"]
            }
            gblAccountsToCashback.push(tempToCashbackAccount);
        }
    }
    if (isNotBlankObject) {
        gblAccountsToCashback = sortToAccountForCashback(gblAccountsToCashback, frmIBPointRedemptionLanding);
        frmIBPointRedemptionLanding.segAccountList.setData(gblAccountsToCashback);
        frmIBPointRedemptionLanding.lblMesg.isVisible = false;
        frmIBPointRedemptionLanding.hbxContainerAccount.isVisible = true;
    } else {
        frmIBPointRedemptionLanding.lblMesg.isVisible = true;
        frmIBPointRedemptionLanding.hbxContainerAccount.isVisible = false
    }
    frmIBPointRedemptionLanding.txbSearchAccount.text = "";
    frmIBPointRedemptionLanding.hbxwaterWheel.setVisibility(false);
    frmIBPointRedemptionLanding.hbxIBRewardsList.setVisibility(false);
    frmIBPointRedemptionLanding.hbxAccountList.setVisibility(true);
}

function searchToAccountsRewardsIB() {
    var searchText = frmIBPointRedemptionLanding.txbSearchAccount.text;
    searchText = searchText.toLowerCase();
    var searchList = searchToAccountsRewardsLogic(searchText, false);
    frmIBPointRedemptionLanding.segAccountList.setData(searchList);
    if (searchList.length <= 0) {
        frmIBPointRedemptionLanding.lblMesg.isVisible = true;
        frmIBPointRedemptionLanding.hbxContainerAccount.isVisible = false
    } else {
        frmIBPointRedemptionLanding.lblMesg.isVisible = false;
        frmIBPointRedemptionLanding.hbxContainerAccount.isVisible = true;
    }
}

function onSelectToAccountForCashbackIB() {
    gblSelectedToAccount = frmIBPointRedemptionLanding.segAccountList.data[frmIBPointRedemptionLanding.segAccountList.selectedIndex[1]];
    frmIBPointRedemptionLanding.lblTranLandToName.text = gblSelectedToAccount["lblAccountName"];
    frmIBPointRedemptionLanding.lblTranLandToNum.text = gblSelectedToAccount["lblAccountNumber"];
    frmIBPointRedemptionLanding.imgCashbackTo.src = gblSelectedToAccount["imgAccountType"];
    frmIBPointRedemptionConfirmation.lblTranLandToName.text = gblSelectedToAccount["lblAccountName"];
    frmIBPointRedemptionConfirmation.lblTranLandToNum.text = gblSelectedToAccount["lblAccountNumber"];
    frmIBPointRedemptionConfirmation.imgCashbackTo.src = gblSelectedToAccount["imgAccountType"];
    frmIBPointRedemptionComplete.lblTranLandToName.text = gblSelectedToAccount["lblAccountName"];
    frmIBPointRedemptionComplete.lblTranLandToNum.text = gblSelectedToAccount["lblAccountNumber"];
    frmIBPointRedemptionComplete.imgCashbackTo.src = gblSelectedToAccount["imgAccountType"];
    frmIBPointRedemptionLanding.hbxwaterWheel.setVisibility(true);
    frmIBPointRedemptionLanding.hbxAccountList.setVisibility(false);
    checkNextEnableFromRewardsLandingIB();
}

function checkNextEnableFromRewardsLandingIB() {
    var all_conditions = 0;
    var passed_conditions = 0;
    if (frmIBPointRedemptionLanding.hbxMemberNoLastName.isVisible) {
        var lastName = frmIBPointRedemptionLanding.txtLastName.text;
        all_conditions += (lastName.length > 0 && lastName.match(/^[a-zA-Z]+$/) != undefined) ? 1 : 0;
        passed_conditions += 1;
        var memberNumber = frmIBPointRedemptionLanding.txtMemberNumber.text;
        if (IBidentifyRewardType() == "AirAsia") {
            all_conditions += (memberNumber.length > 0 && memberNumber.match(/[0-9]{10}/) != undefined && memberNumber.length == 10) ? 1 : 0;
            passed_conditions += 1;
        } else if (IBidentifyRewardType() == "ROP") {
            all_conditions += (memberNumber.length == 7 && memberNumber.match(/\b[a-zA-Z]{2}[0-9]{5}\b/) != undefined) ? 1 : 0;
            passed_conditions += 1;
        } else {
            all_conditions += (memberNumber.length > 0) ? 1 : 0;
            passed_conditions += 1;
        }
    }
    if (frmIBPointRedemptionLanding.hbxCashBack.isVisible) {
        all_conditions += (frmIBPointRedemptionLanding.lblTranLandToName.text != "") ? 1 : 0;
        passed_conditions += 1;
        all_conditions += (frmIBPointRedemptionLanding.lblTranLandToNum.text != "") ? 1 : 0;
        passed_conditions += 1;
    }
    all_conditions += (frmIBPointRedemptionLanding.txtPointsBeingRedeemed.text != "") ? 1 : 0;
    passed_conditions += 1;
    all_conditions += (frmIBPointRedemptionLanding.txtRemainingPoints.text != "") ? 1 : 0;
    passed_conditions += 1;
    all_conditions += (frmIBPointRedemptionLanding.txtAmntCashbackRopMile.text != "") ? 1 : 0;
    passed_conditions += 1;
    //	alert("## all_conditions : "+all_conditions);
    //	alert("## passed_conditions : "+passed_conditions);
    if (all_conditions == passed_conditions) {
        frmIBPointRedemptionLanding.btnNext.skin = "btnIB158";
        frmIBPointRedemptionLanding.btnNext.hoverSkin = "btnIB158active";
        frmIBPointRedemptionLanding.btnNext.setEnabled(true);
    } else {
        frmIBPointRedemptionLanding.btnNext.skin = "btnIB158disabled";
        frmIBPointRedemptionLanding.btnNext.hoverSkin = "btnIB158disabled";
        frmIBPointRedemptionLanding.btnNext.setEnabled(false);
    }
}

function calculateRemainingPointsIB() {
    var minimumPoints = Number(gblrewardsData["rewardsDataSet"][gblSelectedReward]["rewardPoint"]);
    var rewardsMinimum = Number(gblrewardsData["rewardsDataSet"][gblSelectedReward]["rewardUnit"]);
    var total_points = frmIBPointRedemptionLanding.lblAvailablePointsValue.text;
    total_points = total_points.replace(/[a-z, ]/gi, "");
    total_points = Number(total_points);
    var points_being_redemmed = Number(frmIBPointRedemptionLanding.txtPointsBeingRedeemed.text);
    if (total_points >= points_being_redemmed) {
        var remaining_points = total_points - points_being_redemmed;
        if (remaining_points == 0) frmIBPointRedemptionLanding.txtRemainingPoints.text = "0";
        else frmIBPointRedemptionLanding.txtRemainingPoints.text = commaFormattedPoints(remaining_points);
        if (points_being_redemmed > 0 && points_being_redemmed % minimumPoints == 0) {
            if (IBidentifyRewardType() == "Cashback") {
                frmIBPointRedemptionLanding.txtAmntCashbackRopMile.text = commaFormattedPoints(points_being_redemmed / minimumPoints * rewardsMinimum) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
            } else {
                frmIBPointRedemptionLanding.txtAmntCashbackRopMile.text = commaFormattedPoints(points_being_redemmed / minimumPoints * rewardsMinimum);
            }
        } else {
            frmIBPointRedemptionLanding.txtRemainingPoints.text = ""
            frmIBPointRedemptionLanding.txtAmntCashbackRopMile.text = "";
            frmIBPointRedemptionLanding.btnNext.skin = "btnIB158disabled";
            frmIBPointRedemptionLanding.btnNext.hoverSkin = "btnIB158disabled";
            frmIBPointRedemptionLanding.btnNext.setEnabled(false);
        }
    } else {
        frmIBPointRedemptionLanding.txtRemainingPoints.text = "";
        frmIBPointRedemptionLanding.txtAmntCashbackRopMile.text = "";
        frmIBPointRedemptionLanding.btnNext.skin = "btnIB158disabled";
        frmIBPointRedemptionLanding.btnNext.hoverSkin = "btnIB158disabled";
        frmIBPointRedemptionLanding.btnNext.setEnabled(false);
    }
}

function checkRedeemPointsMoreAvailablePointsIB() {
    var total_points = frmIBPointRedemptionLanding.lblAvailablePointsValue.text;
    total_points = total_points.replace(/[a-z, ]/gi, "");
    total_points = Number(total_points);
    var points_being_redemmed = Number(frmIBPointRedemptionLanding.txtPointsBeingRedeemed.text);
    var earned_AmntCashbackRopMile = frmIBPointRedemptionLanding.txtAmntCashbackRopMile.text;
    if (total_points < points_being_redemmed) {
        showAlertWithCallBack(kony.i18n.getLocalizedString("keyRewardsErrorPointNotEnough"), kony.i18n.getLocalizedString("info"), putFocusOnPointsBeingRedemmedTextboxIB);
    } else if (points_being_redemmed > 0 && earned_AmntCashbackRopMile == "") {
        var alert_message = kony.i18n.getLocalizedString("keyUsedPointNotMatchPointCriteria");
        var rewardDataSet = gblrewardsData["rewardsDataSet"][gblSelectedReward];
        var rewardPoint = rewardDataSet["rewardPoint"];
        var x = commaFormattedPoints(rewardPoint);
        var y = commaFormattedPoints(Number(rewardPoint) * 2);
        alert_message = alert_message.replace(/<X>/g, x);
        alert_message = alert_message.replace(/<Y>/g, y);
        showAlertWithCallBack(alert_message, kony.i18n.getLocalizedString("info"), putFocusOnPointsBeingRedemmedTextboxIB)
    }
}

function checkMinMemberNumberLengthIB() {
    var memberNumber = frmIBPointRedemptionLanding.txtMemberNumber.text;
    if (IBidentifyRewardType() == "AirAsia" && (memberNumber.length < 10 || memberNumber.match(/[0-9]{10}/) == undefined) && memberNumber.length > 0) {
        showAlertWithCallBack(kony.i18n.getLocalizedString("keyRewardsErrorMemberNoAirAsia"), kony.i18n.getLocalizedString("info"), putFocusOnMemberNumberTextboxIB);
    }
    if (IBidentifyRewardType() == "ROP" && (memberNumber.length != 7 || memberNumber.match(/\b[a-zA-Z]{2}[0-9]{5}\b/) == undefined) && memberNumber.length > 0) {
        showAlertWithCallBack(kony.i18n.getLocalizedString("keyRewardsErrorMemberNoROP"), kony.i18n.getLocalizedString("info"), putFocusOnMemberNumberTextboxIB);
    }
}

function putFocusOnMemberNumberTextboxIB() {
    frmIBPointRedemptionLanding.txtMemberNumber.text = "";
    frmIBPointRedemptionLanding.txtMemberNumber.setFocus(true);
}

function putFocusOnPointsBeingRedemmedTextboxIB() {
    frmIBPointRedemptionLanding.txtPointsBeingRedeemed.text = "";
    frmIBPointRedemptionLanding.txtPointsBeingRedeemed.setFocus(true);
    frmIBPointRedemptionLanding.hbxPointsBeingRedeemed.skin = "hbxProperFocus40px";
}

function checkMinLastNameLengthIB() {
    var lastName = frmIBPointRedemptionLanding.txtLastName.text;
    if (lastName.length > 0 && lastName.match(/^[a-zA-Z]+$/) == undefined) {
        showAlertWithCallBack(kony.i18n.getLocalizedString("keyRewardsErrorLastName"), kony.i18n.getLocalizedString("info"), putFocusOnLastNameTextboxIB)
    }
}

function putFocusOnLastNameTextboxIB() {
    frmIBPointRedemptionLanding.txtLastName.text = "";
    frmIBPointRedemptionLanding.txtLastName.setFocus(true);
}

function saveRedeemRewardsInSessionIB() {
    showLoadingScreenPopup();
    var inputParam = {};
    var sbStr = gblAccountTable["custAcctRec"][gblIndex]["accId"];
    var length = sbStr.length;
    sbStr = sbStr.substring(length - 4, length);
    var accNickName;
    if (isNotBlank(gblAccountTable["custAcctRec"][gblIndex]["acctNickName"])) {
        inputParam["cardNicknameEN"] = gblAccountTable["custAcctRec"][gblIndex]["acctNickName"];
        inputParam["cardNicknameTH"] = gblAccountTable["custAcctRec"][gblIndex]["acctNickName"];
    } else {
        inputParam["cardNicknameEN"] = gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"] + " " + sbStr;
        inputParam["cardNicknameTH"] = gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"] + " " + sbStr;
    }
    inputParam["cardAccId"] = gblAccountTable["custAcctRec"][gblIndex]["accId"];
    inputParam["cardNumber"] = frmIBPointRedemptionLanding.lblCardNumberValue.text;
    inputParam["totalAvailablePoints"] = replaceCommon(frmIBPointRedemptionLanding.lblAvailablePointsValue.text, ",", "");
    inputParam["expiryDate"] = reformatDate(gblAccountTable["custAcctRec"][gblIndex]["stmtDate"]);
    inputParam["pointsExpiringSoon"] = replaceCommon(frmIBPointRedemptionLanding.lblExpireDateValue.text, ",", "");
    inputParam["selectedRewardCode"] = gblrewardsData["rewardsDataSet"][gblSelectedReward]["rewardCode"];
    inputParam["selectedRewardPoint"] = gblrewardsData["rewardsDataSet"][gblSelectedReward]["rewardPoint"];
    inputParam["selectedRewardUnit"] = gblrewardsData["rewardsDataSet"][gblSelectedReward]["rewardUnit"];
    inputParam["selectedRewardType"] = IBidentifyRewardType();
    inputParam["selectedRewardNameEN"] = gblrewardsData["rewardsDataSet"][gblSelectedReward]["rewardNameEn"];
    inputParam["selectedRewardNameTH"] = gblrewardsData["rewardsDataSet"][gblSelectedReward]["rewardNameTh"];
    var rewardType = IBidentifyRewardType();
    if (rewardType == "AirAsia" || rewardType == "ROP" || rewardType == "OtherMile") {
        inputParam["lastName"] = frmIBPointRedemptionLanding.txtLastName.text;
        inputParam["memberNumber"] = frmIBPointRedemptionLanding.txtMemberNumber.text;
    }
    if (rewardType == "Cashback") {
        inputParam["transferToAccount"] = frmIBPointRedemptionLanding.lblTranLandToNum.text;
        inputParam["transferToAccountNickname"] = frmIBPointRedemptionLanding.lblTranLandToName.text;
        inputParam["transferToAccountNicknameEN"] = gblSelectedToAccount["lblAccountNameEN"];
        inputParam["transferToAccountNicknameTH"] = gblSelectedToAccount["lblAccountNameTH"];
        inputParam["transferToAccountId"] = gblSelectedToAccount["hiddenAccountId"];
    }
    inputParam["pointsBeingRedeemed"] = replaceCommon(frmIBPointRedemptionLanding.txtPointsBeingRedeemed.text, ",", "");
    inputParam["remainingPoints"] = replaceCommon(frmIBPointRedemptionLanding.txtRemainingPoints.text, ",", "");
    inputParam["amntCashbackRopMile"] = parseInt(replaceCommon(frmIBPointRedemptionLanding.txtAmntCashbackRopMile.text, ",", ""));
    invokeServiceSecureAsync("saveRedeemPointsTxn", inputParam, saveRedeemRewardsInSessionCallbackfunctionIB);
}

function saveRedeemRewardsInSessionCallbackfunctionIB(status, resulttable) {
    if (status == 400) {
        dismissLoadingScreenPopup();
        if (resulttable["opstatus"] == "0") {
            //Show the Confirm Form
            onClickNextFromRedemptionLandingIB();
        } else {
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}

function onClickNextFromRedemptionLandingIB() {
    frmIBPointRedemptionConfirmation.btnEditPointRedemption.setVisibility(true);
    frmIBPointRedemptionConfirmation.lblPointsBeingRedeemedValue.text = frmIBPointRedemptionLanding.txtPointsBeingRedeemed.text;
    frmIBPointRedemptionConfirmation.lblRemainingPointsValue.text = frmIBPointRedemptionLanding.txtRemainingPoints.text;
    frmIBPointRedemptionConfirmation.lblAmntCashbackRopMileValue.text = frmIBPointRedemptionLanding.txtAmntCashbackRopMile.text;
    frmIBPointRedemptionComplete.lblPointsBeingRedeemedValue.text = frmIBPointRedemptionLanding.txtPointsBeingRedeemed.text;
    frmIBPointRedemptionComplete.lblRemainingPointsValue.text = frmIBPointRedemptionLanding.txtRemainingPoints.text;
    frmIBPointRedemptionComplete.lblAmntCashbackRopMileValue.text = frmIBPointRedemptionLanding.txtAmntCashbackRopMile.text;
    if (frmIBPointRedemptionLanding.hbxMemberNoLastName.isVisible) {
        frmIBPointRedemptionConfirmation.hbxMemberNoLastName.isVisible = true;
        frmIBPointRedemptionConfirmation.lblLastNameValue.text = frmIBPointRedemptionLanding.txtLastName.text;
        frmIBPointRedemptionConfirmation.lblMemberNumberValue.text = frmIBPointRedemptionLanding.txtMemberNumber.text;
        frmIBPointRedemptionComplete.hbxMemberNoLastName.isVisible = true;
        frmIBPointRedemptionComplete.lblLastNameValue.text = frmIBPointRedemptionLanding.txtLastName.text;
        frmIBPointRedemptionComplete.lblMemberNumberValue.text = frmIBPointRedemptionLanding.txtMemberNumber.text;
    } else {
        frmIBPointRedemptionConfirmation.hbxMemberNoLastName.isVisible = false;
        frmIBPointRedemptionComplete.hbxMemberNoLastName.isVisible = false;
    }
    if (frmIBPointRedemptionLanding.hbxCashBack.isVisible) {
        frmIBPointRedemptionConfirmation.hbxCashBack.isVisible = true;
        frmIBPointRedemptionComplete.hbxCashBack.isVisible = true;
        frmIBPointRedemptionConfirmation.lineToAccount.setVisibility(false);
        frmIBPointRedemptionComplete.lineToAccount.setVisibility(true);
    } else {
        frmIBPointRedemptionConfirmation.hbxCashBack.isVisible = false;
        frmIBPointRedemptionComplete.hbxCashBack.isVisible = false;
        frmIBPointRedemptionConfirmation.lineToAccount.setVisibility(false);
        frmIBPointRedemptionComplete.lineToAccount.setVisibility(false);
    }
    gblPointRedeem = "genOTP"
    frmIBPointRedemptionConfirmation.show();
}

function onClickEditPointRedemptionConfirmation() {
    frmIBPointRedemptionLanding.show();
    syncIBPointRedemption();
}
// for OTP validtion
function OTPValdatnPointRedeem(text) {
    if (gblTokenSwitchFlag == false || gblTokenSwitchFlag == "0" || gblTokenSwitchFlag == "") {
        if (text == null || text == '') {
            showAlert(kony.i18n.getLocalizedString("Receipent_alert_correctOTP"), kony.i18n.getLocalizedString("info"));
            return false;
        }
        var txtLen = text.length;
        if (txtLen > gblOTPLENGTH) {
            alert("OTP code should be maximum " + gblOTPLENGTH + " characters");
            return false;
        }
    } else {
        text = frmIBPointRedemptionConfirmation.tbxToken.text;
        if (text == "" || text == null) {
            showAlert(kony.i18n.getLocalizedString("Receipent_tokenId"), kony.i18n.getLocalizedString("info"));
            return false;
        }
        var isNum = kony.string.isNumeric(text);
        if (!isNum) {
            alert("Token should be numeric");
            return false;
        }
    }
    IBverifyOTPForPointRedeem();
    return true;
}

function IBverifyOTPForPointRedeem() {
    showLoadingScreenPopup();
    var tokenorOTP;
    var locale = kony.i18n.getCurrentLocale();
    var inputParam = {};
    if (gblTokenSwitchFlag == true) {
        tokenorOTP = frmIBPointRedemptionConfirmation.tbxToken.text
    } else {
        tokenorOTP = frmIBPointRedemptionConfirmation.txtBxOTP.text;
    }
    inputParam["password"] = tokenorOTP;
    inputParam["locale"] = locale;
    inputParam["notificationAdd_appID"] = appConfig.appId;
    // TOKEN
    inputParam["TokenSwitchFlag"] = gblTokenSwitchFlag;
    inputParam["verifyToken_loginModuleId"] = "IB_HWTKN";
    inputParam["verifyToken_userStoreId"] = "DefaultStore";
    inputParam["verifyToken_retryCounterVerifyAccessPin"] = "0";
    inputParam["verifyToken_retryCounterVerifyTransPwd"] = "0";
    inputParam["verifyToken_userId"] = gblUserName;
    inputParam["verifyToken_sessionVal"] = "";
    inputParam["verifyToken_segmentId"] = "segmentId";
    inputParam["verifyToken_segmentIdVal"] = "MIB";
    inputParam["verifyToken_channel"] = "Internet Banking";
    //OTP
    inputParam["verifyPwd_retryCounterVerifyOTP"] = "0",
        inputParam["verifyPwd_userId"] = gblUserName,
        inputParam["verifyPwd_userStoreId"] = "DefaultStore",
        inputParam["verifyPwd_loginModuleId"] = "IBSMSOTP",
        inputParam["verifyPwd_segmentId"] = "MIB"
        //Mobile Pwd 
    inputParam["verifyPwdMB_loginModuleId"] = "MB_TxPwd";
    inputParam["verifyPwdMB_retryCounterVerifyAccessPin"] = "0";
    inputParam["verifyPwdMB_retryCounterVerifyTransPwd"] = "0";
    //Params for Redeem service
    inputParam["redeemPointAdd_cardId"] = gblAccountTable["custAcctRec"][gblIndex]["accId"];
    inputParam["redeemPointAdd_productCode"] = gblrewardsData["rewardsDataSet"][gblSelectedReward]["rewardCode"];
    inputParam["redeemPointAdd_baseRedeemPoint"] = gblrewardsData["rewardsDataSet"][gblSelectedReward]["rewardPoint"];
    inputParam["redeemPointAdd_redempUnit"] = gblrewardsData["rewardsDataSet"][gblSelectedReward]["rewardUnit"];
    inputParam["redeemPointAdd_tranDt"] = ""; // should be picked from server
    inputParam["redeemPointAdd_memberCode"] = frmIBPointRedemptionConfirmation.lblMemberNumberValue.text;
    inputParam["redeemPointAdd_memberSurName"] = frmIBPointRedemptionConfirmation.lblLastNameValue.text;
    inputParam["redeemPointAdd_toCardNo"] = gblSelectedToAccount["hiddenAccountId"];
    inputParam["redeemPointAdd_toAcctNo"] = gblSelectedToAccount["hiddenAccountId"];
    inputParam["redeemPointAdd_totalPointsRedeemed"] = replaceCommon(frmIBPointRedemptionConfirmation.lblPointsBeingRedeemedValue.text, ",", "");
    invokeServiceSecureAsync("RedeemPointCompositeService", inputParam, callBackRedeemCompJavaService);
}

function callBackRedeemCompJavaService(status, resulttable) {
    if (status == 400) {
        dismissLoadingScreenPopup();
        if (resulttable["opstatus"] == 0) {
            if (resulttable["pointRedeemActBusinessHrsFlag"] == "false") {
                var startTime = resulttable["pointRedeemStartTime"];
                var endTime = resulttable["pointRedeemEndTime"];
                var messageUnavailable = kony.i18n.getLocalizedString("keySoGooODServiceUnavailable");
                messageUnavailable = messageUnavailable.replace("{start_time}", startTime);
                messageUnavailable = messageUnavailable.replace("{end_time}", endTime);
                showAlert(messageUnavailable, kony.i18n.getLocalizedString("info"));
                if (channel == "MB") {
                    frmAccountDetailsMB.show();
                } else {
                    frmIBAccntSummary.show();
                }
                return false;
            } else {
                frmIBPointRedemptionComplete.lblRemainingPointsValue.text = commaFormattedPoints(resulttable["remainingCardPoint"]);
                frmIBPointRedemptionComplete.hbxSuccess.isVisible = true;
                frmIBPointRedemptionComplete.hbxFail.isVisible = false;
                frmIBPointRedemptionComplete.hbxCompleteShareOptions.isVisible = true;
                frmIBPointRedemptionComplete.show();
                syncIBPointRedemption();
            }
            gblRetryCountRequestOTP = 0;
        } else if (resulttable["errCode"] == "VrfyTxPWDErr00001" || resulttable["errCode"] == "VrfyTxPWDErr00002") {
            setTransPwdFailedError(kony.i18n.getLocalizedString("invalidTxnPwd"));
        } else if (resulttable["errCode"] == "VrfyTxPWDErr00003") {
            showTranPwdLockedPopup();
        } else if (resulttable["opstatus"] == 8005) {
            if (resulttable["errCode"] == "VrfyOTPErr00001") {
                gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
                frmIBPointRedemptionConfirmation.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone"); //kony.i18n.getLocalizedString("invalidOTP"); //
                frmIBPointRedemptionConfirmation.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
                // alert("" + kony.i18n.getLocalizedString("invalidOTP"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00002") {
                handleOTPLockedIB(resulttable);
                //startRcCrmUpdateProfilBPIB("04");
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00005") {
                alert("" + kony.i18n.getLocalizedString("KeyTokenSerialNumError"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00006") {
                gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
                alert("" + resulttable["errMsg"]);
                return false;
            } else if (resulttable["errCode"] == "GenOTPRtyErr00001") {
                dismissLoadingScreenPopup();
                showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00004") {
                alert("" + resulttable["errMsg"]);
                return false;
            } else {
                alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
                return false;
            }
        } else {
            alert("" + resulttable["errMsg"]);
            frmIBPointRedemptionComplete.hbxSuccess.isVisible = false;
            frmIBPointRedemptionComplete.hbxFail.isVisible = true;
            frmIBPointRedemptionComplete.lblCompleteMessage.text = kony.i18n.getLocalizedString("keyPointRedeemFail");
            frmIBPointRedemptionComplete.hbxCompleteShareOptions.isVisible = false;
            frmIBPointRedemptionComplete.lblPointsBeingRedeemedValue.text = "-";
            frmIBPointRedemptionComplete.lblAmntCashbackRopMileValue.text = "-";
            frmIBPointRedemptionComplete.lblRemainingPointsValue.text = commaFormattedPoints(gblAccountTable["custAcctRec"][gblIndex]["bonusPointAvail"]);
            frmIBPointRedemptionComplete.show();
            syncIBPointRedemption();
        }
    } else {
        if (status == 300) {
            dismissLoadingScreen();
            alert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"));
        }
    }
}

function frmIBPointRedemptionConfirmationPreShow() {
    displayLabelValueIBPointRedemption(frmIBPointRedemptionConfirmation);
    frmIBPointRedemptionConfirmation.hbxOtpBox.setVisibility(false);
}

function frmIBPointRedemptionCompletePreShow() {
    displayLabelValueIBPointRedemption(frmIBPointRedemptionComplete);
}

function checkCustStatus() {
    if (getCRMLockStatus()) {
        curr_form = kony.application.getCurrentForm().id;
        dismissLoadingScreenPopup();
        popIBBPOTPLocked.show();
    } else {
        checkpointRedeemBousinessHrs();
    }
}

function checkpointRedeemBousinessHrs() {
    var input_param = {};
    invokeServiceSecureAsync("pointRedeemBousinessHrs", input_param, checkpointRedeemBousinessHrsCallBack);
}

function checkpointRedeemBousinessHrsCallBack(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
            var serviceHrsFlag = result["pointRedeemActBusinessHrsFlag"];
            var startTime = result["pointRedeemStartTime"];
            pntRedeem_StartTime = result["pointRedeemStartTime"]
            var endTime = result["pointRedeemEndTime"];
            pntRedeem_EndTime = result["pointRedeemEndTime"];
            var messageUnavailable = kony.i18n.getLocalizedString("keyPointRedmptionProdFeature");
            messageUnavailable = messageUnavailable.replace("{start_time}", startTime);
            messageUnavailable = messageUnavailable.replace("{end_time}", endTime);
            if (serviceHrsFlag == "true") {
                frmIBPointRedemptionProdFeature.lblPointRedemptionProdFeature.text = messageUnavailable;
                frmIBPointRedemptionProdFeature.show();
            } else {
                var serviceHrs = kony.i18n.getLocalizedString("keySoGooODServiceUnavailable");
                serviceHrs = serviceHrs.replace("{start_time}", startTime);
                serviceHrs = serviceHrs.replace("{end_time}", endTime);
                showAlert(serviceHrs, kony.i18n.getLocalizedString("info"));
                if (channel == "MB") {
                    frmAccountDetailsMB.show();
                } else {
                    frmIBAccntSummary.show();
                }
                return false;
            }
        } else {
            dismissLoadingScreenBasedOnChannel(channel);
            if (resulttable["errMsg"] != null || resulttable["errMsg"] != "") {
                showAlert("Sorry, System found error : " + resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
                return false;
            } else {
                showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
                return false;
            }
        }
    }
}

function preOTPScreenPointRedemption() {
    if (frmIBPointRedemptionConfirmation.hbxCashBack.isVisible || frmIBPointRedemptionConfirmation.hbxOtpBox.isVisible) {
        IBverifyOTPForPointRedeem();
    } else {
        var locale = kony.i18n.getCurrentLocale();
        frmIBPointRedemptionConfirmation.hbxToken.setVisibility(false);
        frmIBPointRedemptionConfirmation.hbxOTPEntry.setVisibility(true);
        frmIBPointRedemptionConfirmation.hbxOtpBox.setVisibility(true);
        frmIBPointRedemptionConfirmation.txtBxOTP.setFocus(true);
        frmIBPointRedemptionConfirmation.hbxOTPRef.setVisibility(true);
        frmIBPointRedemptionConfirmation.hbxOTPsnt.setVisibility(true);
        frmIBPointRedemptionConfirmation.txtBxOTP.setFocus(true);
        frmIBPointRedemptionConfirmation.hbxBtnNext.margin = [0, 0, 0, 0];
        frmIBPointRedemptionConfirmation.btnEditPointRedemption.setVisibility(false);
        var inputParam = [];
        inputParam["crmId"] = gblcrmId;
        showLoadingScreenPopup();
        invokeServiceSecureAsync("tokenSwitching", inputParam, PntRedimtonTkenFlagCallbackfunction);
    }
}

function PntRedimtonTkenFlagCallbackfunction(status, callbackResponse) {
    if (status == 400) {
        if (callbackResponse["opstatus"] == 0) {
            if (callbackResponse["deviceFlag"].length == 0) {
                IBPntRedimptionGenereteOTP();
            }
            if (callbackResponse["deviceFlag"].length > 0) {
                var tokenFlag = callbackResponse["deviceFlag"][0]["TOKEN_DEVICE_FLAG"];
                var mediaPreference = callbackResponse["deviceFlag"][0]["MEDIA_PREFERENCE"];
                var tokenStatus = callbackResponse["deviceFlag"][0]["TOKEN_STATUS_ID"];
                if (tokenFlag == "Y" && (mediaPreference == "Token" || mediaPreference == "TOKEN") && tokenStatus == '02') {
                    gblTokenSwitchFlag = true;
                    //gblOpenAcctConfirm = "verOTP";
                    IBTokenForPntRedmption();
                } else {
                    gblTokenSwitchFlag = false;
                    IBPntRedimptionGenereteOTP();
                    gblTokenSwitchFlag = false;
                }
            }
        } else {
            dismissLoadingScreenPopup();
        }
    }
}

function IBTokenForPntRedmption() {
    frmIBPointRedemptionConfirmation.hbxOTPEntry.setVisibility(false);
    frmIBPointRedemptionConfirmation.hbxOTPsnt.setVisibility(false);
    frmIBPointRedemptionConfirmation.lblkeyOTPMsg.text = kony.i18n.getLocalizedString("Receipent_tokenId");
    // frmIBPointRedemptionConfirmation.hbxTokenMessage.isVisible = true;
    frmIBPointRedemptionConfirmation.hbxToken.setVisibility(true);
    frmIBPointRedemptionConfirmation.hbxOTPRef.isVisible = false;
    //gblOpenAcctConfirm = "verOTP";
    dismissLoadingScreenPopup();
    frmIBPointRedemptionConfirmation.tbxToken.text = "";
    frmIBPointRedemptionConfirmation.tbxToken.setFocus(true);
}

function IBPntRedimptionGenereteOTP() {
    showLoadingScreenPopup();
    var inputParams = {};
    inputParams["Channel"] = "pointRedeem";
    inputParams["retryCounterRequestOTP"] = gblRetryCountRequestOTP;
    inputParams["locale"] = kony.i18n.getCurrentLocale();
    //InputParms for activity logging
    var platformChannel = gblDeviceInfo.name;
    if (platformChannel == "thinclient") inputParams["channelId"] = GLOBAL_IB_CHANNEL;
    else inputParams["channelId"] = GLOBAL_MB_CHANNEL;
    inputParams["logLinkageId"] = "";
    inputParams["deviceNickName"] = "";
    inputParams["activityFlexValues2"] = "";
    invokeServiceSecureAsync("generateOTPWithUser", inputParams, callBackIBreqOTPIBPointRedi);
}

function callBackIBreqOTPIBPointRedi(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["errCode"] == "GenOTPRtyErr00002") {
            //alert("Test1");
            dismissLoadingScreenPopup();
            showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00002"), kony.i18n.getLocalizedString("info"));
            return false;
        } else if (callBackResponse["errCode"] == "JavaErr00001") {
            //alert("Test2");
            dismissLoadingScreenPopup();
            showAlertIB(kony.i18n.getLocalizedString("ECJavaErr00001"), kony.i18n.getLocalizedString("info"));
            return false;
        } else if (callBackResponse["errCode"] == "GenOTPRtyErr00001") {
            dismissLoadingScreenPopup();
            showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
            return false;
        } else if (callBackResponse["errCode"] == "GenOTPRtyErr00001") {
            dismissLoadingScreenPopup();
            showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
            return false;
        }
        if (callBackResponse["opstatus"] == 0) {
            //Resetting the OTP screen
            frmIBPointRedemptionConfirmation.hbxOTPEntry.setVisibility(true);
            frmIBPointRedemptionConfirmation.hbxOTPsnt.setVisibility(true);
            frmIBPointRedemptionConfirmation.lblkeyOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
            frmIBPointRedemptionConfirmation.hbxOTPRef.isVisible = true;
            frmIBPointRedemptionConfirmation.hbxToken.setVisibility(false);
            gblTokenSwitchFlag = false;
            dismissLoadingScreenPopup();
            //frmIBPointRedemptionConfirmation.tbxToken.text ="";
            //frmIBPointRedemptionConfirmation.tbxToken.setFocus(true);
            //Finish reset here
            //gblOTPdisabletime = kony.os.toNumber(callBackResponse["requestOTPEnableTime"]) * 100;
            gblRetryCountRequestOTP = kony.os.toNumber(callBackResponse["retryCounterRequestOTP"]);
            gblOTPLENGTH = kony.os.toNumber(callBackResponse["otpLength"]);
            var reqOtpTimer = kony.os.toNumber(callBackResponse["requestOTPEnableTime"]);
            kony.timer.schedule("OtpTimerPointRedeem", IBOTPTimercallbackPointRedeem, reqOtpTimer, false);
            gblOTPReqLimit = gblOTPReqLimit + 1;
            //	setTimeout('OTPTimercallbackOpenAccount()',reqOtpTimer);
            var refVal = "";
            for (var d = 0; d < callBackResponse["Collection1"].length; d++) {
                if (callBackResponse["Collection1"][d]["keyName"] == "pac") {
                    refVal = callBackResponse["Collection1"][d]["ValueString"];
                    break;
                }
            }
            gblPointRedeem = "verOTP";
            frmIBPointRedemptionConfirmation.btnOTPReq.skin = btnIBREQotp;
            frmIBPointRedemptionConfirmation.btnOTPReq.focusSkin = btnIBREQotp;
            frmIBPointRedemptionConfirmation.btnOTPReq.onClick = "";
            frmIBPointRedemptionConfirmation.lblBankRefVal.text = kony.i18n.getLocalizedString("keyIBbankrefno") + refVal;
            //curr_form = kony.application.getCurrentForm();
            frmIBPointRedemptionConfirmation.txtBxOTP.maxTextLength = gblOTPLENGTH;
            frmIBPointRedemptionConfirmation.txtBxOTP.text = "";
            frmIBPointRedemptionConfirmation.lblOTPMblNum.text = " " + maskingIB(gblPHONENUMBER);
            dismissLoadingScreenPopup();
        } else {
            dismissLoadingScreenPopup();
            alert("Error " + callBackResponse["errMsg"]);
        }
    } else {
        if (status == 300) {
            dismissLoadingScreenPopup();
            alert("Error");
        }
    }
}

function IBOTPTimercallbackPointRedeem() {
    frmIBPointRedemptionConfirmation.btnOTPReq.skin = btnIBREQotpFocus;
    frmIBPointRedemptionConfirmation.btnOTPReq.focusSkin = btnIBREQotpFocus;
    frmIBPointRedemptionConfirmation.btnOTPReq.onClick = IBPntRedimptionGenereteOTP;
    try {
        kony.timer.cancel("OtpTimerPointRedeem");
    } catch (e) {}
}

function displayLabelValueIBPointRedemption(currForm) {
    if ("frmIBPointRedemptionConfirmation" == currForm.id || "frmIBPointRedemptionComplete" == currForm.id || "frmIBPointRedemptionLanding" == currForm.id) {
        onClickPointRedemptionFromAccountDetailsIB(currForm);
        currForm.lblRewards.text = kony.i18n.getLocalizedString("keyChooseYourReward") + ":";
        if (currForm.hbxCashBack.isVisible) {
            currForm.lblCashbackTo.text = kony.i18n.getLocalizedString("keyCashbackToAccount");
            currForm.lblAmntCashbackRopMile.text = kony.i18n.getLocalizedString("keyTotalCashbackAmount");
        }
        if (currForm.hbxCommonRewardsEntry.isVisible) {
            currForm.lblPointsBeingRedeemed.text = kony.i18n.getLocalizedString("keyPointsbeingredeemed");
            currForm.lblRemainingPoints.text = kony.i18n.getLocalizedString("keyRemainingPoints");
        }
        if (currForm.hbxMemberNoLastName.isVisible) {
            currForm.lblMemberNumber.text = kony.i18n.getLocalizedString("keyRewardsMemberNo");
            currForm.lblLastName.text = kony.i18n.getLocalizedString("keyLastName");
            var rewardType = IBidentifyRewardType();
            if (rewardType == "AirAsia") {
                currForm.lblAmntCashbackRopMile.text = kony.i18n.getLocalizedString("keyReceivedBigPoint");
            } else if (rewardType == "ROP") {
                currForm.lblAmntCashbackRopMile.text = kony.i18n.getLocalizedString("keyReceivedROPMile");
            } else {
                currForm.lblAmntCashbackRopMile.text = kony.i18n.getLocalizedString("keyReceivedMile");
            }
        }
        if (gblSelectedReward != -1) {
            currForm.lblRewardsRate.text = commaFormattedPoints(gblrewardsData["rewardsDataSet"][gblSelectedReward]["rewardPoint"]) + " " + kony.i18n.getLocalizedString("keyMBPoints") + " = " + commaFormattedPoints(gblrewardsData["rewardsDataSet"][gblSelectedReward]["rewardUnit"]) + " " + kony.i18n.getLocalizedString(gblrewardsData["rewardsDataSet"][gblSelectedReward]["tphraseRewardUnitKey"]);
            if (kony.i18n.getCurrentLocale() == "th_TH") {
                currForm.lblRewardsName.text = gblrewardsData["rewardsDataSet"][gblSelectedReward]["rewardNameTh"];
            } else {
                currForm.lblRewardsName.text = gblrewardsData["rewardsDataSet"][gblSelectedReward]["rewardNameEn"];
            }
        }
    }
    if ("frmIBPointRedemptionLanding" == currForm.id) {
        frmIBPointRedemptionLanding.txtLastName.placeholder = kony.i18n.getLocalizedString("keyLastNameFirstFiveChars");
        if (frmIBPointRedemptionLanding.hbxAccountList.isVisible) {
            onClickToAccountForCashbackIB();
        }
        if (frmIBPointRedemptionLanding.hbxIBRewardsList.isVisible) {
            IBloadPointRedemptionOptions();
        }
    }
    if (undefined !== currForm.lblTranLandToName && currForm.lblTranLandToName.text != "") {
        if (kony.i18n.getCurrentLocale() == "th_TH") currForm.lblTranLandToName.text = gblSelectedToAccount["lblAccountNameTH"];
        else currForm.lblTranLandToName.text = gblSelectedToAccount["lblAccountNameEN"];
    }
    if ("frmIBPointRedemptionComplete" == currForm.id) {
        frmIBPointRedemptionComplete.hbxCompleteMessage.isVisible = true;
        if (frmIBPointRedemptionComplete.hbxSuccess.isVisible) {
            var reward_type = identifyRewardType();
            if (reward_type == "Cashback") {
                frmIBPointRedemptionComplete.lblCompleteMessage.text = kony.i18n.getLocalizedString("keyRewardsCompleteCashback");
            } else if (reward_type == "ROP") {
                frmIBPointRedemptionComplete.lblCompleteMessage.text = kony.i18n.getLocalizedString("keyRewardsCompleteROP");
            } else if (reward_type == "AirAsia") {
                frmIBPointRedemptionComplete.lblCompleteMessage.text = kony.i18n.getLocalizedString("keyRewardsCompleteBigPoint");
            } else {
                frmIBPointRedemptionComplete.hbxCompleteMessage.isVisible = false;
            }
        } else {
            frmIBPointRedemptionComplete.lblCompleteMessage.text = kony.i18n.getLocalizedString("keyPointRedeemFail");
        }
    }
    if ("frmIBPointRedemptionProdFeature" == currForm.id) {
        frmIBPointRedemptionProdFeaturePreShow();
    }
}

function syncIBPointRedemption() {
    var currForm = kony.application.getCurrentForm()
    displayLabelValueIBPointRedemption(currForm);
    if ("frmIBPointRedemptionTnC" == currForm.id) {
        frmIBPointRedemptionTnCPreShow();
    }
    if ((undefined !== currForm.btnMenuMyAccountSummary) && (currForm.id == "frmIBPointRedemptionProdFeature" || currForm.id == "frmIBPointRedemptionTnC" || currForm.id == "frmIBPointRedemptionLanding" || currForm.id == "frmIBPointRedemptionConfirmation" || currForm.id == "frmIBPointRedemptionComplete")) {
        if (kony.i18n.getCurrentLocale() == "th_TH") currForm.btnMenuMyAccountSummary.skin = "btnIBMenuMyAccountSummaryFocusThai";
        else currForm.btnMenuMyAccountSummary.skin = "btnIBMenuMyAccountSummaryFocus";
    }
    if ("frmIBPointRedemptionConfirmation" == currForm.id) {
        if (gblTokenSwitchFlag == true) {
            frmIBPointRedemptionConfirmation.lblkeyOTPMsg.text = kony.i18n.getLocalizedString("keyPleaseEnterToken");
        } else {
            frmIBPointRedemptionConfirmation.lblkeyOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
        }
    }
}

function saveRedeemCompleteAsPDFImageIB(filetype) {
    var inputParam = {};
    inputParam["filetype"] = filetype;
    inputParam["activityTypeId"] = "RedeemPoint";
    inputParam["futurePDF"] = "true";
    showLoadingScreenPopup()
    invokeServiceSecureAsync("generateImagePdf", inputParam, ibRenderFileCallbackfunction);
}

function postOnFBPointredemptionCompleteIB() {
    requestfromform = "frmIBPointRedemptionComplete";
    gblPOWcustNME = gblCustomerName;
    gblPOWtransXN = "PointRedemption";
    gblPOWchannel = "IB";
    postOnWall();
}

function onClickRedeemMoreCompleteIB() {
    /*var points_before_redemption=Number(gblAccountTable["custAcctRec"][gblIndex]["bonusPointAvail"]);
    var unformatted_points_after_redemption = replaceCommon(frmIBPointRedemptionComplete.lblRemainingPointsValue.text,",","");
    var points_after_redemption = Number(unformatted_points_after_redemption);
    var points_expiring = Number(gblAccountTable["custAcctRec"][gblIndex]["bonusPointUsed"]);
    var redeemed_points =  points_before_redemption-points_after_redemption;
    if((points_expiring - redeemed_points) > 0)
    	gblAccountTable["custAcctRec"][gblIndex]["bonusPointUsed"]=(points_expiring - redeemed_points)+"";
    else
    	gblAccountTable["custAcctRec"][gblIndex]["bonusPointUsed"]="0";
    gblAccountTable["custAcctRec"][gblIndex]["bonusPointAvail"]=unformatted_points_after_redemption;*/
    IBcreditCardCallServiceRewards();
}

function IBcreditCardCallServiceRewards() {
    //calling creditCardservice for getting 'Total amount due' and 'Minimum amount due
    var inputparam = {};
    gblCC_StmtPointAvail = "";
    gblCC_RemainingPoint = "";
    gblCC_PointExpRemaining = "";
    inputparam["cardId"] = gblAccountTable["custAcctRec"][gblIndex]["accId"];
    inputparam["tranCode"] = "1"; // as TRANSCODEMIN was made to 2 for ENH_111
    showLoadingScreenPopup();
    var status = invokeServiceSecureAsync("creditcardDetailsInq", inputparam, IBcreditCardRewardsCallBack);
}

function IBcreditCardRewardsCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (resulttable["stmtDate"] != "") {
                gblCC_StmtPointAvail = resulttable["bonusPointAvail"];
            }
            IBcreditCardCallServiceUnBillAmtRewards();
        }
    }
}

function IBcreditCardCallServiceUnBillAmtRewards() {
    var inputparam = {};
    inputparam["cardId"] = gblAccountTable["custAcctRec"][gblIndex]["accId"];
    inputparam["tranCode"] = TRANSCODEUN;
    var status = invokeServiceSecureAsync("creditcardDetailsInq", inputparam, IBcreditCardCallBackUnReward);
}

function IBcreditCardCallBackUnReward(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            var allowRedeemPoints = gblAccountTable["custAcctRec"][gblIndex]["allowRedeemPoints"];
            if (allowRedeemPoints == "1" && resulttable["remainingPoint"] != "") {
                gblAccountTable["custAcctRec"][gblIndex]["bonusPointAvail"] = resulttable["remainingPoint"];
                gblCC_RemainingPoint = resulttable["remainingPoint"];
            }
            if (allowRedeemPoints == "1" && resulttable["bonusPointUsed"] != "") {
                if (resulttable["stmtDate"] != "") {
                    gblAccountTable["custAcctRec"][gblIndex]["stmtDate"] = resulttable["stmtDate"];
                }
                if (resulttable["bonusPointUsed"] != "" && gblCC_StmtPointAvail != "" && resulttable["remainingPoint"] != "") {
                    gblCC_PointExpRemaining = Number(resulttable["bonusPointUsed"]) - (Number(gblCC_StmtPointAvail) - Number(resulttable["remainingPoint"]));
                    if (gblCC_PointExpRemaining < 0) {
                        gblCC_PointExpRemaining = 0;
                    }
                    gblCC_PointExpRemaining = gblCC_PointExpRemaining + "";
                }
                gblAccountTable["custAcctRec"][gblIndex]["bonusPointUsed"] = gblCC_PointExpRemaining;
            }
            dismissLoadingScreenPopup();
            frmIBPointRedemptionTnC.show();
        } else {
            alert(" " + resulttable["errMsg"]);
            dismissLoadingScreenPopup();
        }
    }
}