var otpIBChannel;
var otpIBCurrForm;

function preOTPScreeneIB(channelParam) {
    otpIBChannel = channelParam;
    otpIBCurrForm = kony.application.getCurrentForm();
    var functionName = otpIBCurrForm.id + "_txtBxOTP_onKeyUp";
    window[functionName] = function(eventobject) {
        dontAllowNonNeumaric(otpIBCurrForm.txtBxOTP);
    }
    showLoadingScreenPopup();
    otpIBCurrForm.hbxToken.setVisibility(false);
    otpIBCurrForm.hbxOTPEntry.setVisibility(true);
    otpIBCurrForm.hbxOtpBox.setVisibility(true);
    otpIBCurrForm.txtBxOTP.setFocus(true);
    otpIBCurrForm.hbxOTPRef.setVisibility(true);
    otpIBCurrForm.hbxOTPsnt.setVisibility(true);
    otpIBCurrForm.txtBxOTP.setFocus(true);
    otpIBCurrForm.txtBxOTP.text = "";
    otpIBCurrForm.lblOTPinCurr.text = "";
    otpIBCurrForm.lblPlsReEnter.text = "";
    var inputParam = [];
    invokeServiceSecureAsync("tokenSwitching", inputParam, preOTPScreeneIBCallback);
}

function preOTPScreeneIBCallback(status, callbackResponse) {
    if (status == 400) {
        if (callbackResponse["opstatus"] == 0) {
            if (callbackResponse["deviceFlag"].length == 0) {
                genereteOTPForIB();
            }
            if (callbackResponse["deviceFlag"].length > 0) {
                var tokenFlag = callbackResponse["deviceFlag"][0]["TOKEN_DEVICE_FLAG"];
                var mediaPreference = callbackResponse["deviceFlag"][0]["MEDIA_PREFERENCE"];
                var tokenStatus = callbackResponse["deviceFlag"][0]["TOKEN_STATUS_ID"];
                if (tokenFlag == "Y" && (mediaPreference == "Token" || mediaPreference == "TOKEN") && tokenStatus == '02') {
                    gblTokenSwitchFlag = true;
                    genereteTokenForIB();
                } else {
                    gblTokenSwitchFlag = false;
                    genereteOTPForIB();
                    gblTokenSwitchFlag = false;
                }
            }
        } else {
            dismissLoadingScreenPopup();
        }
    }
}

function genereteOTPForIB() {
    showLoadingScreenPopup();
    var inputParams = {};
    inputParams["Channel"] = otpIBChannel;
    inputParams["retryCounterRequestOTP"] = gblRetryCountRequestOTP;
    inputParams["locale"] = kony.i18n.getCurrentLocale();
    //InputParms for activity logging
    var platformChannel = gblDeviceInfo.name;
    if (platformChannel == "thinclient") inputParams["channelId"] = GLOBAL_IB_CHANNEL;
    else inputParams["channelId"] = GLOBAL_MB_CHANNEL;
    inputParams["logLinkageId"] = "";
    inputParams["deviceNickName"] = "";
    inputParams["activityFlexValues2"] = "";
    invokeServiceSecureAsync("generateOTPWithUser", inputParams, genereteOTPForIBCallBack);
}

function genereteOTPForIBCallBack(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["errCode"] == "GenOTPRtyErr00002") {
            dismissLoadingScreenPopup();
            showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00002"), kony.i18n.getLocalizedString("info"));
            return false;
        } else if (callBackResponse["errCode"] == "JavaErr00001") {
            dismissLoadingScreenPopup();
            showAlertIB(kony.i18n.getLocalizedString("ECJavaErr00001"), kony.i18n.getLocalizedString("info"));
            return false;
        } else if (callBackResponse["errCode"] == "GenOTPRtyErr00001") {
            dismissLoadingScreenPopup();
            showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
            return false;
        } else if (callBackResponse["errCode"] == "GenOTPRtyErr00001") {
            dismissLoadingScreenPopup();
            showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
            return false;
        }
        if (callBackResponse["opstatus"] == 0) {
            //Resetting the OTP screen
            otpIBCurrForm.hbxOTPEntry.setVisibility(true);
            otpIBCurrForm.hbxOTPsnt.setVisibility(true);
            otpIBCurrForm.lblkeyOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
            otpIBCurrForm.hbxOTPRef.isVisible = true;
            otpIBCurrForm.hbxToken.setVisibility(false);
            gblTokenSwitchFlag = false;
            dismissLoadingScreenPopup();
            //frmIBEStatementConfirm.tbxToken.text ="";
            //frmIBEStatementConfirm.tbxToken.setFocus(true);
            //Finish reset here
            //gblOTPdisabletime = kony.os.toNumber(callBackResponse["requestOTPEnableTime"]) * 100;
            gblRetryCountRequestOTP = kony.os.toNumber(callBackResponse["retryCounterRequestOTP"]);
            gblOTPLENGTH = kony.os.toNumber(callBackResponse["otpLength"]);
            gblOTPReqLimit = kony.os.toNumber(callBackResponse["otpRequestLimit"]);
            var reqOtpTimer = kony.os.toNumber(callBackResponse["requestOTPEnableTime"]);
            kony.timer.schedule("otpTimerCommon", IBOTPTimercallbackCommon, reqOtpTimer, false);
            //          	gblOTPReqLimit=gblOTPReqLimit+1;
            //	setTimeout('OTPTimercallbackOpenAccount()',reqOtpTimer);
            var refVal = "";
            for (var d = 0; d < callBackResponse["Collection1"].length; d++) {
                if (callBackResponse["Collection1"][d]["keyName"] == "pac") {
                    refVal = callBackResponse["Collection1"][d]["ValueString"];
                    break;
                }
            }
            otpIBCurrForm.btnOTPReq.skin = btnIBREQotp;
            otpIBCurrForm.btnOTPReq.focusSkin = btnIBREQotp;
            otpIBCurrForm.btnOTPReq.onClick = "";
            otpIBCurrForm.lblBankRefVal.text = refVal;
            otpIBCurrForm.lblBankRef.text = kony.i18n.getLocalizedString("keyIBbankrefno");
            //curr_form = kony.application.getCurrentForm();
            otpIBCurrForm.txtBxOTP.maxTextLength = gblOTPLENGTH;
            otpIBCurrForm.lblOTPMblNum.text = " " + maskingIB(gblPHONENUMBER);
            dismissLoadingScreenPopup();
        } else {
            dismissLoadingScreenPopup();
            alert("Error " + callBackResponse["errMsg"]);
        }
    } else {
        if (status == 300) {
            dismissLoadingScreenPopup();
            alert("Error");
        }
    }
}

function genereteTokenForIB() {
    dismissLoadingScreenPopup();
    otpIBCurrForm.hbxOTPEntry.setVisibility(false);
    otpIBCurrForm.hbxOTPsnt.setVisibility(false);
    otpIBCurrForm.lblkeyOTPMsg.text = kony.i18n.getLocalizedString("keyPleaseEnterToken");
    otpIBCurrForm.hbxToken.setVisibility(true);
    otpIBCurrForm.hbxOTPRef.isVisible = false;
    otpIBCurrForm.tbxToken.text = "";
    otpIBCurrForm.tbxToken.setFocus(true);
}

function VerifyOTPAndTokenInputFormatForIB() {
    var otpVal = otpIBCurrForm.txtBxOTP.text;
    if (gblTokenSwitchFlag == false || gblTokenSwitchFlag == "0" || gblTokenSwitchFlag == "") {
        if (otpVal == null || otpVal == '') {
            showAlert(kony.i18n.getLocalizedString("Receipent_alert_correctOTP"), kony.i18n.getLocalizedString("info"));
            return false;
        }
        var txtLen = otpVal.length;
        if (txtLen > gblOTPLENGTH) {
            showAlert("OTP code should be maximum " + gblOTPLENGTH + " characters", kony.i18n.getLocalizedString("info"));
            return false;
        }
    } else {
        otpVal = otpIBCurrForm.tbxToken.text;
        if (otpVal == "" || otpVal == null) {
            showAlert(kony.i18n.getLocalizedString("Receipent_tokenId"), kony.i18n.getLocalizedString("info"));
            return false;
        }
        var isNum = kony.string.isNumeric(otpVal);
        if (!isNum) {
            alert("Token should be numeric");
            return false;
        }
    }
    return true;
}

function arrangeInputParamOTPAndTokenForIB() {
    var inputParam = {};
    var tokenorOTP;
    if (gblTokenSwitchFlag == true) {
        //TOKEN
        tokenorOTP = otpIBCurrForm.tbxToken.text
        inputParam["verifyToken_loginModuleId"] = "IB_HWTKN";
        inputParam["verifyToken_userStoreId"] = "DefaultStore";
        inputParam["verifyToken_retryCounterVerifyAccessPin"] = gblVerifyOTPCounter;
        inputParam["verifyToken_retryCounterVerifyTransPwd"] = "0";
        inputParam["verifyToken_userId"] = gblUserName;
        inputParam["verifyToken_segmentId"] = "segmentId";
        inputParam["verifyToken_segmentIdVal"] = "MIB";
        inputParam["verifyToken_channel"] = "Internet Banking";
    } else {
        //OTP
        tokenorOTP = otpIBCurrForm.txtBxOTP.text
        inputParam["verifyPwd_loginModuleId"] = "IBSMSOTP",
            inputParam["verifyPwd_retryCounterVerifyOTP"] = gblVerifyOTPCounter,
            inputParam["verifyPwd_userId"] = gblUserName,
            inputParam["verifyPwd_userStoreId"] = "DefaultStore",
            inputParam["verifyPwd_segmentId"] = "MIB"
    }
    inputParam["password"] = tokenorOTP;
    inputParam["locale"] = locale;
    inputParam["notificationAdd_appID"] = appConfig.appId;
    inputParam["TokenSwitchFlag"] = gblTokenSwitchFlag;
    return inputParam;
}

function handleExceptionOTPAndTokenForIB(resulttable) {
    if (resulttable["errCode"] == "VrfyTxPWDErr00001" || resulttable["errCode"] == "VrfyTxPWDErr00002") {
        setTransPwdFailedError(kony.i18n.getLocalizedString("invalidTxnPwd"));
    } else if (resulttable["errCode"] == "VrfyTxPWDErr00003") {
        showTranPwdLockedPopup();
    } else if (resulttable["opstatus"] == 8005) {
        if (resulttable["errCode"] == "VrfyOTPErr00001") {
            gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
            otpIBCurrForm.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");
            otpIBCurrForm.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
            otpIBCurrForm.txtBxOTP.text = "";
            otpIBCurrForm.tbxToken.text = "";
            return false;
        } else if (resulttable["errCode"] == "VrfyOTPErr00002") {
            handleOTPLockedIB(resulttable);
            return false;
        } else if (resulttable["errCode"] == "VrfyOTPErr00005") {
            showAlertIB(kony.i18n.getLocalizedString("KeyTokenSerialNumError"), kony.i18n.getLocalizedString("info"));
            return false;
        } else if (resulttable["errCode"] == "VrfyOTPErr00006") {
            gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
            alert("" + resulttable["errMsg"]);
            showAlertIB(resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
            return false;
        } else if (resulttable["errCode"] == "GenOTPRtyErr00001") {
            dismissLoadingScreenPopup();
            showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
            return false;
        } else if (resulttable["errCode"] == "VrfyOTPErr00004") {
            showAlertIB(resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
            return false;
        } else {
            showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}

function IBOTPTimercallbackCommon() {
    if (gblRetryCountRequestOTP >= gblOTPReqLimit) {
        otpIBCurrForm.btnOTPReq.skin = btnIBREQotp;
        otpIBCurrForm.btnOTPReq.focusSkin = btnIBREQotp
        otpIBCurrForm.btnOTPReq.hoverSkin = btnIBREQotp
        otpIBCurrForm.btnOTPReq.onClick = "";
    } else {
        otpIBCurrForm.btnOTPReq.skin = btnIBREQotpFocus;
        otpIBCurrForm.btnOTPReq.focusSkin = btnIBREQotpFocus
        otpIBCurrForm.btnOTPReq.hoverSkin = btnIBREQotpFocus
        otpIBCurrForm.btnOTPReq.onClick = genereteOTPForIB;
    }
    try {
        kony.timer.cancel("otpTimerCommon");
    } catch (e) {}
}

function syncIBOTPAndToken() {
    var currForm = kony.application.getCurrentForm();
    if (undefined !== currForm.lblkeyOTPMsg) {
        if (gblTokenSwitchFlag == true) {
            currForm.lblkeyOTPMsg.text = kony.i18n.getLocalizedString("keyPleaseEnterToken");
        } else {
            currForm.lblkeyOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
        }
    }
}