/*frmContactUsCompleteScreenMB*/
function frmContactUsCompleteScreenMBMenuPreshow() {
    if (gblCallPrePost) {
        isMenuShown = false;
        isSignedUser = true;
        frmContactUsCompleteScreenMB.scrollboxMain.scrollToEnd();
        DisableFadingEdges.call(this, frmContactUsCompleteScreenMB);
    }
}

function frmContactUsCompleteScreenMBMenuPostshow() {
    if (gblCallPrePost) {
        //..
    }
    assignGlobalForMenuPostshow();
}
/*frmContactusFAQMB*/
function frmContactusFAQMBMenuPreshow() {
    if (gblCallPrePost) {
        frmContactusFAQMBPreshow.call(this);
    }
}

function frmContactusFAQMBMenuPostshow() {
    if (gblCallPrePost) {
        //..
    }
    assignGlobalForMenuPostshow();
}
/*frmContactUsMB*/
function frmContactUsMBMenuPreshow() {
    if (gblCallPrePost) {
        frmContactUSMBPreshowMenu.call(this);
    }
}

function frmContactUsMBMenuPostshow() {
    if (gblCallPrePost) {
        frmContactUsMBMenuPostshow.call(this);
    }
    assignGlobalForMenuPostshow();
}
/*frmFeedbackComplete*/
function frmFeedbackCompleteMenuPreshow() {
    if (gblCallPrePost) {
        frmFeedbackComplete.scrollboxMain.scrollToEnd();
        DisableFadingEdges.call(this, frmFeedbackComplete);
    }
}

function frmFeedbackCompleteMenuPostshow() {
    if (gblCallPrePost) {
        //..
    }
    assignGlobalForMenuPostshow();
}