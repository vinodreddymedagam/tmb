
BillerList = null;
function onClickLoanAcctSummary() {
 	showLoadingScreen();
 if(checkMBUserStatus()){
	gbl_accId = gblAccountTable["custAcctRec"][glb_accountId]["accId"];
	gbl_accId = gbl_accId.substring(1,gbl_accId.length )
	//
	
	gblReference1 = gbl_accId
	gblReference2 = gblReference1.substring(gblReference1.length-3,gblReference1.length);
	gblReference1 = gblReference1.substring(0,gblReference1.length-3);
	
	
	gblAccountType = gblAccountTable["custAcctRec"][glb_accountId]["accType"];
    gblProductCode = gblAccountTable["custAcctRec"][glb_accountId]["productID"];
	 
    gblMyBillerTopUpBB = 0;
    gblBillerPresentInMyBills = false;
    gblFromAccountSummary = true;
	getCompcodeMB(gblProductCode, gblAccountType);
	
    }
}
function onclickLoanfromDetails() {
 	showLoadingScreen();
	if(checkMBUserStatus()){
	  	gbl_accId = gblAccountTable["custAcctRec"][gblIndex]["accId"];
		gbl_accId = gbl_accId.substring(1,gbl_accId.length )
		//
		
		gblReference1 = gbl_accId
		gblReference2 = gblReference1.substring(gblReference1.length-3,gblReference1.length);
		gblReference1 = gblReference1.substring(0,gblReference1.length-3);
		
		
		gblAccountType = gblAccountTable["custAcctRec"][gblIndex]["accType"];
	    gblProductCode = gblAccountTable["custAcctRec"][gblIndex]["productID"];
		 
	    gblMyBillerTopUpBB = 0;
	    gblBillerPresentInMyBills = false;
		getCompcodeMB(gblProductCode, gblAccountType)
	}
}

function populateLoanOnBillers(ref1,ref2){
	
			var inputParam = {};
			//for TMB Loan
			var TMB_BANK_FIXED_CODE = "0001";
			var TMB_BANK_CODE_ADD = "0011";
			var ZERO_PAD = "0000";
			var BRANCH_CODE;
			var ref1AccountIDLoan = ref1;
			
			var fiident;
			if (ref1AccountIDLoan.length == 10) {
				
				fiident = TMB_BANK_CODE_ADD + TMB_BANK_FIXED_CODE + "0" + ref1AccountIDLoan[0] + ref1AccountIDLoan[1] +
					ref1AccountIDLoan[2] + ZERO_PAD;
				ref1AccountIDLoan = "0" + ref1AccountIDLoan + ref2;
			}
			if (ref1AccountIDLoan.length == 13) {
				
				fiident = TMB_BANK_CODE_ADD + TMB_BANK_FIXED_CODE + "0" + ref1AccountIDLoan[0] + ref1AccountIDLoan[1] +
					ref1AccountIDLoan[2] + ZERO_PAD;
				ref1AccountIDLoan = "0" + ref1AccountIDLoan;
			} else {
				fiident = TMB_BANK_CODE_ADD + TMB_BANK_FIXED_CODE + "0" + ref1AccountIDLoan[1] + ref1AccountIDLoan[2] +
					ref1AccountIDLoan[3] + ZERO_PAD;
			}
			
			inputParam["acctId"] = ref1AccountIDLoan;
			inputParam["acctType"] = "LOC";
			
			
			invokeServiceSecureAsync("doLoanAcctInq", inputParam, BillPaymentdoLoanAcctInqServiceCallBackMB);
			
			frmBillPayment.lblAmount.text = kony.i18n.getLocalizedString("TREnter_PL_Amount");
			gblPenalty = false;
			frmBillPayment.hbxPenalty.setVisibility(false);
			frmBillPayment.hbxMainAmountForPenalty.setVisibility(false);
			frmBillPayment.hbxFullMinSpecButtons.setVisibility(false);
			// if (!frmBillPayment.hbxFullSpecButton.isVisible) {
			frmBillPayment.hbxFullSpecButton.setVisibility(true);
			//frmBillPayment.lblForFullPayment.text = frmSelectBiller["segMyBills"]["selectedItems"][0]["fullAmount"] + "\u0E3F";
			frmBillPayment.btnFull2.skin = btnScheduleEndLeftFocus128;
			frmBillPayment.btnSpecified2.skin = btnScheduleEndRight128;	
}
			
function BillPaymentdoLoanAcctInqServiceCallBackMB(status, result) {

        fullAmt = "0.00";
        minAmt = "0.00";
        if (status == 400) //success responce
        {
            
            if (result["opstatus"] == 0) {
                
                var balAmount;
                fullAmt = result["regPmtCurAmt"];
                
                frmBillPayment.tbxAmount.setVisibility(false);
                
                frmBillPayment.lblForFullPayment.text = numberWithCommas(removeCommos(fullAmt));
                frmBillPayment.lblForFullPayment.setVisibility(true);
                frmBillPayment.tbxAmount.text = numberWithCommas(removeCommos(fullAmt));
                frmBillPayment.hbxFullSpecButton.setVisibility(true);
                frmBillPayment.hbxFullMinSpecButtons.setVisibility(false);
                gblFullPayment = true;
                fullPress();
                gblFirstTimeBillPayment =false;
                callBillPaymentCustomerAccountService();

            } else {
                
                dismissLoadingScreen();
                alert(kony.i18n.getLocalizedString("keyErrAccntNoInvalid"));

            }

        }

    }
