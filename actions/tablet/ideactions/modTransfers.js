/*
*************************************************************************************
		Module	: preShowfrmTransferLandingMB
		Author  : Kony
		Date    : July 01, 2013
		Purpose : Preshow of frmTransfer Landing MB
****************************************************************************************
*/

function preShowfrmTransferLandingMB() {
	changeStatusBarColor();
	setEnabledTransferLandingPage(true);
	frmTransferLanding.label475124774164.text = kony.i18n.getLocalizedString("Transfer");
	frmTransferLanding.lblDevitOnTitle.text = kony.i18n.getLocalizedString("TREnter_DebitOn");
	frmTransferLanding.lblTranLandNotifyRec.text = kony.i18n.getLocalizedString("TREnter_Notify");
	frmTransferLanding.lblFeeTransfer.text = kony.i18n.getLocalizedString("MIB_P2PTRFeeTransfer");
	frmTransferLanding.lblAmount.text = kony.i18n.getLocalizedString("TREnter_PL_Amount");
	frmTransferLanding.lblOnUsNotify.text = kony.i18n.getLocalizedString("TREnter_Notify");
	frmTransferLanding.txtTranLandAmt.placeholder =   "0.00"; 
	frmTransferLanding.tbxAccountNumber.placeholder =   kony.i18n.getLocalizedString("TREnter_PL_AccNo"); 
	frmTransferLanding.txtTranLandMyNote.placeholder =   kony.i18n.getLocalizedString("TREnter_PL_My_Note"); 
	frmTransferLanding.txtTranLandRecNote.placeholder =   kony.i18n.getLocalizedString("TREnter_PL_SNote"); 
	frmTransferLanding.txtCitizenID.placeholder =   kony.i18n.getLocalizedString("MIB_P2PTREnter_CI"); 
	frmTransferLanding.textRecNoteEmail.placeholder =   kony.i18n.getLocalizedString("TREnter_PL_SNote"); 
    frmTransferLanding.txtTransLndSms.placeholder = kony.i18n.getLocalizedString("keyEnterMobileNum");
    frmTransferLanding.txtTransLndSmsNEmail.placeholder = kony.i18n.getLocalizedString("keyIBPleaseEnterEmail");
    frmTransferLanding.lblRcvDate.text = kony.i18n.getLocalizedString("TREnter_Received");
    frmTransferLanding.lblRcvTime.text = kony.i18n.getLocalizedString("TREnter_Time_01");
    
	if(!frmTransferLanding.hbxPleaseSelectBank.isVisible){
		assignBankSelectDetails(gblisTMB);
	}
	if(popUpBankList.segBanklist != null && popUpBankList.segBanklist !== undefined){
 		popUpBankList.segBanklist.setData(getBankListForBankPopup(globalSelectBankData, globalSelectBankData.length));
	}

	var orftFlag = getORFTFlag(gblisTMB);
	var smartFlag = getSMARTFlag(gblisTMB);
	if(gblSelTransferMode == 1 && 
		(isNotBlank(orftFlag) && isNotBlank(smartFlag) && orftFlag == "N" && smartFlag == "Y")
		|| gblTransSMART == 1){
		frmTransferLanding.lblRcvTime.text = kony.i18n.getLocalizedString("TREnter_Time_02");
		if(frmTransferLanding.hbxFeeTransfer.isVisible){
			frmTransferLanding.lblFeeTransfer.text = kony.i18n.getLocalizedString("TREnter_SMART");
			frmTransferLanding.lblFeeTransfer.skin = "lblGrey36px";
		}
	}
	
	if(frmTransferLanding.hbxRecievedBy.isVisible){
		if((gblPaynow && gblTrasORFT) || (gblPaynow && gblisTMB == "11") || (gblSelTransferMode == 2 || gblSelTransferMode == 3)){
			frmTransferLanding.lblRecievedByValue.text = kony.i18n.getLocalizedString("keyNOW"); 
		}
	}

	if(frmTransferLanding.btnTransLndORFT.text != null){
		var arr = frmTransferLanding.btnTransLndORFT.text.split(' ');
		frmTransferLanding.btnTransLndORFT.text = arr[0] + " \n" + kony.i18n.getLocalizedString("TREnter_ORFT");
	}
	if(frmTransferLanding.btnTransLndSmart.text != null){
		var arr = frmTransferLanding.btnTransLndSmart.text.split(' ');
		frmTransferLanding.btnTransLndSmart.text = arr[0] + " \n" + kony.i18n.getLocalizedString("TREnter_SMART");
	}
	frmTransferLanding.btnTranLandNext.text = kony.i18n.getLocalizedString("Next");
	
	frmTransferLanding.btnTransCnfrmCancel.text = kony.i18n.getLocalizedString("Back");
  	
  	frmTransferLanding.txtOnUsMobileNo.placeholder = kony.i18n.getLocalizedString("MIB_P2PTREnter_MobNo");
	frmTransfersAck.lblTransNPbAckToAccountName.text = "";
	frmTransferConfirm.lblTransCnfmToAccountName.text = "";
	
 	if(frmTransferLanding.lblSchedSel2.isVisible && isNotBlank(frmTransferLanding.lblSchedSel2.text)){
 		if(isNotBlank(repeatNumberOfTimesText)){
 			var data = displayRecurringDates(repeatNumberOfTimesText);
	 		var scheduleValues = data.split(""+kony.i18n.getLocalizedString("keyRepeat")+"");
			frmTransferLanding.lblSchedSel.text = scheduleValues[0];
			frmTransferLanding.lblSchedSel2.text = kony.i18n.getLocalizedString("keyRepeat") + scheduleValues[1];
		}
	}
	
	frmTransferLanding.lblSelectBank.text = kony.i18n.getLocalizedString("keySelectBank");
	frmTransferLanding.lblSelectMaturity.text = kony.i18n.getLocalizedString("TREnter_PL_Maturiy");
	frmTransferLanding.lblMaturityDate.text = kony.i18n.getLocalizedString("TREnter_Maturity");
	
	if(gblSelTransferMode == 2 || gblSelTransferMode == 3){
		if(isNotBlank(gblisTMB)){
			frmTransferLanding.lblTMXBankName.text = getBankNameMB(gblisTMB);
		}
	}
	
	if(frmTransferLanding.hbxFeeTransfer.isVisible && isNotBlank(frmTransferLanding.lblITMXFee.text)){
		var fee = frmTransferLanding.lblITMXFee.text.indexOf(".");
		if(fee == -1){
			frmTransferLanding.lblITMXFee.text = kony.i18n.getLocalizedString("keyFreeTransfer");
		}
	}
		
	setOnClickSetCanlenderBtn(true);
	
	var temptdFlag = kony.i18n.getLocalizedString("termDeposit");
	if (gbltdFlag == temptdFlag) {
		getTDAccount();
	}
	frmTransferLanding.scrollboxMain.scrollToEnd();
}





/*
*************************************************************************************
		Module	: preShowfrmTransferConfirmMB
		Author  : Kony
		Date    : July 01, 2013
		Purpose : Preshow of frmTransfer Confirm MB
****************************************************************************************
*/

function preShowfrmTransferConfirmMB() {
	if(frmTransferLanding.hboxTD.isVisible){
  		frmTransferConfirm.lblTransAmtFeeTitle.text = kony.i18n.getLocalizedString("TRConfirm_NetAmount");
  	}else{
  		frmTransferConfirm.lblTransAmtFeeTitle.text = kony.i18n.getLocalizedString("TRConfirm_AmountFee");
  	}
  	if(gblPaynow){
  		frmTransferConfirm.lblTransCnfmTransDet.text = kony.i18n.getLocalizedString("TRConfirm_TranDetail");
  	}else{
  		frmTransferConfirm.lblTransCnfmTransDet.text = kony.i18n.getLocalizedString("TRConfirm_SchDetails");
  	}
	frmTransferConfirm.lblTransCnfmRefNum.text = kony.i18n.getLocalizedString("TRConfirm_RefNo");
	frmTransferConfirm.lblCnfmToRefTitle2.text = kony.i18n.getLocalizedString("TRConfirm_RefNo");
	
	if(!isNotBlank(gblSelectedRecipentName) && (gblSelTransferMode == 2 || gblSelTransferMode == 3)){
		frmTransferConfirm.lblTransCnfmToBankName.text = getPhraseToMobileNumber();
	}
  	
   	if(!(LocaleController.isFormUpdatedWithLocale(frmTransferConfirm.id))){
		frmTransferConfirm.lblHdrConfirm.text = kony.i18n.getLocalizedString("Confirmation");
		frmTransferConfirm.lblTransCnfmBalBef.text = kony.i18n.getLocalizedString("TRConfirm_BalBefore");
	
		frmTransferConfirm.lblTransCnfmDate.text = kony.i18n.getLocalizedString("TRConfirm_Debit");
		frmTransferConfirm.lblSmartDate.text = kony.i18n.getLocalizedString("TRConfirm_Receive");
		
		frmTransferConfirm.lblFreeTran.text = kony.i18n.getLocalizedString("TRConfirm_Remain");
		frmTransferConfirm.btnTransCnfrmConfirm.text = kony.i18n.getLocalizedString("keyConfirm");
		frmTransferConfirm.btnTransCnfrmCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
		LocaleController.updatedForm(frmTransferConfirm.id);
   }
    var orftFlag = getORFTFlag(gblisTMB);
	var smartFlag = getSMARTFlag(gblisTMB);
	if(gblSelTransferMode == 1 && 
		(isNotBlank(orftFlag) && isNotBlank(smartFlag) && orftFlag == "N" && smartFlag == "Y")
		|| gblTransSMART == 1){
		frmTransferConfirm.lblSmartDate.text = kony.i18n.getLocalizedString("TRConfirm_Receive02");
	}
}
/*
*************************************************************************************
		Module	: preShowfrmTransferAckMB
		Author  : Kony
		Date    : July 01, 2013
		Purpose : Preshow of frmTransfer Ack MB
****************************************************************************************
*/

function preShowfrmTransferAckMB() {
	changeStatusBarColor();
		frmTransfersAck.scrollboxMain.scrollToEnd();
	//if(!(LocaleController.isFormUpdatedWithLocale(frmTransfersAck.id))){
		// Header From section:
		frmTransfersAck.lblSucessScreenshot.text=kony.i18n.getLocalizedString("Complete");
		frmTransfersAck.lblHdrTxt.text = kony.i18n.getLocalizedString("TRComplete_Title");
	  	frmTransfersAck.lblTransAmtFeeTitle.text = kony.i18n.getLocalizedString("TRConfirm_AmountFee");
		frmTransfersAck.lblFreeTran.text = kony.i18n.getLocalizedString("TRConfirm_Remain");
		frmTransfersAck.lblTransNPbAckBalAfter.text = kony.i18n.getLocalizedString("TRComplete_BalAfter");
		frmTransfersAck.lblTransCnfmTransDet.text = kony.i18n.getLocalizedString("TRConfirm_TranDetail");
		// TD 
	 	if(frmTransferLanding.hboxTD.isVisible){
			frmTransfersAck.lblTransAmtFeeTitle.text = kony.i18n.getLocalizedString("TRConfirm_NetAmount");
			frmTransfersAck.lblPrincipalAmnt.text = kony.i18n.getLocalizedString("TRConfirm_Principal");
			frmTransfersAck.lblTransNPbAckPenaltyAmt.text = kony.i18n.getLocalizedString("TRConfirm_TDPenalty");
			frmTransfersAck.lblTransNPbAckIntAmt.text = kony.i18n.getLocalizedString("TRConfirm_TDInt");
			frmTransfersAck.lblTransNPbAckTaxAmt.text = kony.i18n.getLocalizedString("TRConfirm_TDTax");
			frmTransfersAck.lblTransNPbAckNetAmt.text = kony.i18n.getLocalizedString("TRConfirm_TDNet");
			frmTransfersAck.lblTransNPbAckRef.text = kony.i18n.getLocalizedString("TRConfirm_RefNo");
	  	}
		// Details : 
		frmTransfersAck.lblTransCnfmTotAmt.text = kony.i18n.getLocalizedString("TRConfirm_Amount");
		frmTransfersAck.lblCnfmToRefTitle2.text = kony.i18n.getLocalizedString("TRConfirm_RefNo");
		frmTransfersAck.lblCnfmToRefTitle1.text = kony.i18n.getLocalizedString("TRConfirm_RefNo");
	    frmTransfersAck.lblTransNPbAckDate.text = kony.i18n.getLocalizedString("TRConfirm_Debit");
		frmTransfersAck.lblSmartDate.text = kony.i18n.getLocalizedString("TRConfirm_Receive");
		var orftFlag = getORFTFlag(gblisTMB);
		var smartFlag = getSMARTFlag(gblisTMB);
		if(gblSelTransferMode == 1 && 
			(isNotBlank(orftFlag) && isNotBlank(smartFlag) && orftFlag == "N" && smartFlag == "Y")
			|| gblTransSMART == 1){
			frmTransfersAck.lblSmartDate.text = kony.i18n.getLocalizedString("TRConfirm_Receive02");
		}
		frmTransfersAck.btnTransNPbAckMakeAnthr.text = kony.i18n.getLocalizedString("TRComplete_Btn_More");
		frmTransfersAck.btnTransNPbAckReturn.text = kony.i18n.getLocalizedString("TRComplete_Btn_Return");
		LocaleController.updatedForm(frmTransfersAck.id);
    //}
	// Split Amount:
	if(gblsplitAmt.length > 0){
		frmTransfersAck.lblTransCnfmTotAmt2.text = kony.i18n.getLocalizedString("TRConfirm_Amount");
		frmTransfersAck.lblCnfmToRefTitle2.text = kony.i18n.getLocalizedString("TRConfirm_RefNo");
		if(frmTransfersAck.segTransAckSplit.isVisible){
			frmTransfersAck.lblHideMore.text = kony.i18n.getLocalizedString("Hide");
		}else{
			frmTransfersAck.lblHideMore.text = kony.i18n.getLocalizedString("show");
		}
	}
	if(!gblPaynow){
		frmTransfersAck.lblTransCnfmTransDet.text = kony.i18n.getLocalizedString("TRConfirm_SchDetails");
		frmTransfersAck.lblTransCnfmTotAmt2.text = kony.i18n.getLocalizedString("TRConfirm_Amount");
		frmTransfersAck.lblStartOn.text = kony.i18n.getLocalizedString("TRConfirm_SchDebit");
		frmTransfersAck.lblRepeatAs.text = kony.i18n.getLocalizedString("TRConfirm_SchRepeat");
		frmTransfersAck.lblEndOn.text = kony.i18n.getLocalizedString("TRConfirm_SchEnd");
		frmTransfersAck.lblExecute.text = kony.i18n.getLocalizedString("TRConfirm_SchExe");
		frmTransfersAck.lblScheduleNote.text = kony.i18n.getLocalizedString("TRConfirm_FeeNotice");
		var times = frmTransfersAck.lblExecuteValue.text.split(" ")[0];
		frmTransfersAck.lblExecuteValue.text = times + " "+ kony.i18n.getLocalizedString("keyTimesMB");
		if(kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Daily")){
			frmTransfersAck.lblRepeatValue.text = kony.i18n.getLocalizedString("keyDaily");
		}else if(kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Weekly")){
			frmTransfersAck.lblRepeatValue.text = kony.i18n.getLocalizedString("keyWeekly");
		}else if(kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Monthly")){
			frmTransfersAck.lblRepeatValue.text = kony.i18n.getLocalizedString("keyMonthly");
		}else if(kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Yearly")){
			frmTransfersAck.lblRepeatValue.text = kony.i18n.getLocalizedString("keyYearly");
		}else{
			frmTransfersAck.lblRepeatValue.text = kony.i18n.getLocalizedString("keyOnce");
		}
	}
	if(frmTransfersAck.hbxImageAddRecipient.isVisible){
		var locale = kony.i18n.getCurrentLocale(); 
		if(locale == 'en_US'){
			frmTransfersAck.imgAddRecipientIco.src = "add_recipient_en.png";
		}else{
			frmTransfersAck.imgAddRecipientIco.src = "add_recipient_th.png";
		}
	}
	if(!isNotBlank(gblSelectedRecipentName) && (gblSelTransferMode == 2 || gblSelTransferMode == 3)){
		frmTransfersAck.lblTransNPbAckToName.text = getPhraseToMobileNumber();
	}
	// for sharing
	frmTransfersAck.lblSaveImage.text = kony.i18n.getLocalizedString("TRShare_Image");
	frmTransfersAck.lblOthers.text = kony.i18n.getLocalizedString("More");
	ChangeCampaignLocale();
}

function preShowfrmTranfersToRecipentsMB() {
changeStatusBarColor();
   if(!(LocaleController.isFormUpdatedWithLocale(frmTranfersToRecipents.id))){
 		frmTranfersToRecipents.lblMsg.text = kony.i18n.getLocalizedString("keybillernotfound")
		frmTranfersToRecipents.txbXferSearch.placeholder = kony.i18n.getLocalizedString("keySearch")
		LocaleController.updatedForm(frmTranfersToRecipents.id);
   }
}

function enableTransferToRecipientsContactList() {
	recipientAddFromTransfer = true;
	startReceipentListingServiceMB();
	callAddManually();
}

/**************************************************************************************
		Module	: TransferRecipientSearch
		Author  : Kony
		Date    : May 05, 2013
		Purpose : This function search in dictionay view
****************************************************************************************
*/

function searchTransferToRecipient(eventObject, searchType) {
	var textLength;
	
	var sProduct = "";
	if (searchType == "1") {
		if (eventObject.text.length <= 1)
			frmTranfersToRecipents.btnSearchRecipeints.setEnabled(true);
		else
			frmTranfersToRecipents.btnSearchRecipeints.setEnabled(false);
		textLength = 0;
		sProduct = eventObject.text;
	} else if (searchType == "2") {
		textLength = 0;
		sProduct = frmTranfersToRecipents.txbXferSearch.text;
	} else {
		textLength = 0;
		sProduct = eventObject.text;
	}
	
	if (sProduct.length <= textLength) {
		frmTranfersToRecipents.lblMsg.setVisibility(false);
		frmTranfersToRecipents.segTransferToRecipients.data = gblTransferToRecipientCache;
		gblTransferToRecipientData = gblTransferToRecipientCache;
		frmTranfersToRecipents.lblMsg.setVisibility(false);
	} else {
		final_Product = [];
		var check = "";
		for (var i = 0;
			((gblTransferToRecipientCache) != null) && i < gblTransferToRecipientCache.length; i++) {
			val = gblTransferToRecipientCache[i].lblName;
 			check = (val.toLowerCase().indexOf(sProduct.toLowerCase()) > -1)
 			
 			if (check == true) {
				
				final_Product.push({
					lblName: gblTransferToRecipientCache[i].lblName,
					lblAccountNum: gblTransferToRecipientCache[i].lblAccountNum,
					imgArrow: "navarrow3.png",
					imgfav:gblTransferToRecipientCache[i].imgfav,
					imgprofilepic: gblTransferToRecipientCache[i].imgprofilepic,
					mobileNo: gblTransferToRecipientCache[i].mobileNo,
					imgmob: gblTransferToRecipientCache[i].imgmob,
					facebookNo: gblTransferToRecipientCache[i].facebookNo,
					imgfb: gblTransferToRecipientCache[i].imgfb,
					personalizedId: gblTransferToRecipientCache[i].personalizedId,
					hiddenmain: gblTransferToRecipientCache[i].hiddenmain,
					template: hbrt1
				});
			}
 		}
		frmTranfersToRecipents.segTransferToRecipients.data = final_Product;
		gblTransferToRecipientData = final_Product;
		
		if (final_Product.length == 0) {
			frmTranfersToRecipents.lblMsg.setVisibility(true);
		} else {
			frmTranfersToRecipents.lblMsg.setVisibility(false);
		}
	}
}
/**************************************************************************************
		Module	: enableTransferAckContactList
		Author  : Kony
		Date    : May 05, 2013
		Purpose : This function displays contactlist
****************************************************************************************
*/

function enableTransferAckContactList() {
	var btnskin = "btnShare";
	var btnFocusSkin = "btnShareFoc";
	/*if (frmTransfersAck.hboxaddfblist.isVisible) {
		frmTransfersAck.hboxaddfblist.isVisible = false;
		frmTransfersAck.btnRight.skin = btnskin;
		frmTransfersAck.imgHeaderMiddle.src = "arrowtop.png";
		frmTransfersAck.imgHeaderRight.src = "empty.png";
	} else {
		frmTransfersAck.hboxaddfblist.isVisible = true;
		frmTransfersAck.btnRight.skin = btnFocusSkin;
		frmTransfersAck.imgHeaderMiddle.src = "empty.png";
		frmTransfersAck.imgHeaderRight.src = "arrowtop.png";
	}*/
}
/**************************************************************************************
		Module	: hideUnhide
		Author  : Kony
		Date    : May 05, 2013
		Purpose : This function hides the balance after transfered
****************************************************************************************
*/

function hideUnhide() {
	if (gblAckFlage == "true") {
		frmTransfersAck.lblTransNPbAckFrmBal.setVisibility(false);
		frmTransfersAck.imgHide.src = "arrow_down.png";
		gblAckFlage = "false";
	} else {
		frmTransfersAck.lblTransNPbAckFrmBal.setVisibility(true);
		frmTransfersAck.imgHide.src = "arrow_up.png";
		gblAckFlage = "true";
	}
	
}


var index = -1;
var gblSmartAccountName;
function getToRecipientsAccounts() {
	var checkCon = frmTranfersToRecipents.segTransferToRecipients.selectedItems[0].hiddenmain;
		if (checkCon == "main") {
			Recp_category = 1;
			
			gblXferRecImg = frmTranfersToRecipents.segTransferToRecipients.selectedItems[0].imgprofilepic;
			gblRecipientname = frmTranfersToRecipents.segTransferToRecipients.selectedItems[0].lblName;
			
			var noOfAccounts = frmTranfersToRecipents.segTransferToRecipients.selectedItems[0].lblAccountNum;
			noOfAccounts = noOfAccounts.split(" ", 1);
			getTransferToAccountsNew();
			if(noOfAccounts[0] == 1) {
				var selectedData = frmTranfersToRecipents.segTransferToRecipients.data[selectedRow+1];
				selectRecipientAccountMB(true, selectedData);			
			}
			
		} else if (checkCon == "own") {
			gblXferPhoneNo1 = "";
			gblXferEmail1 = "";
			Recp_category = 2;
			gblRecipientname = frmTranfersToRecipents.segTransferToRecipients.selectedItems[0].lblName;
			
			var noOfAccounts = frmTranfersToRecipents.segTransferToRecipients.selectedItems[0].lblAccountNum;
			noOfAccounts = noOfAccounts.split(" ", 1);
			getTMBOWNAccountsNewMB();
			
			if(noOfAccounts[0] == 1) {
				var selectedData = frmTranfersToRecipents.segTransferToRecipients.data[selectedRow+1];
				selectRecipientAccountMB(true, selectedData);			
			}
			
		} else if (checkCon == "main1" || checkCon == "own1") {
			frmTranfersToRecipents.segTransferToRecipients.removeAll();
			gblTransferToRecipientData = gblTransferToRecipientCache
			dismissLoadingScreen();
			frmTranfersToRecipents.segTransferToRecipients.setData(gblTransferToRecipientCache);
		} else {
			selectRecipientAccountMB(false, "");
		}
	frmTranfersToRecipents.segTransferToRecipients.selectedIndex=[0,selectedRow];
	if(!isNotBlank(frmTransferLanding.txtTranLandAmt.text)){
		frmTransferLanding.txtTranLandAmt.setFocus(true);
		setEnabledTransferLandingPage(true);
	}
	
}


function selectRecipientAccountMB(isOneAccount, oneAccountData) {
	clearNotifyRecipientfields();
	enteringAccountNumberVisbilty(false);
	gblSelectedRecipentName = gblRecipientname;
	recipientAddFromTransfer = false;//DEF1158 to clear if user clicks clicks to recipients manually
	gblTransferFromRecipient = false;
	
	if(Recp_category == "1" || Recp_category == 1){
		gblTrasSMS = 0;
		gblTransEmail = 0;
		frmTransferLanding.btnTranLandSms.src = "mobile_grey.png";
		frmTransferLanding.btnTranLandEmail.src = "email_grey.png";
	}else{
		gblTrasSMS = 1;
		gblTransEmail = 1;
		gblTrasORFT = 0;
		gblTransSMART = 0;
	}
	
	gblXferPhoneNo = gblXferPhoneNo1;
	gblXferEmail = gblXferEmail1;
   	// frmTransferLanding.hbxTransLndFee.setVisibility(false);
	//frmTransferLanding.hboxFeeType.setVisibility(false);
	//frmTransferLanding.lineFeeButton.setVisibility(false);
	frmTransferLanding.hbxFeeTransfer.setVisibility(true);
	frmTransferLanding.lblITMXFee.text = "";
	frmTransferLanding.lblFeeTransfer.skin = "lblWhite36px";
	if(isOneAccount) {
		var lblName = oneAccountData.lbl3;
		var lblAccountNum = oneAccountData.lbl4;
		gblisTMB = oneAccountData.lbl5;
		gblBANKREF = oneAccountData.lblBankName;
		gblToAccountType = oneAccountData.accType;
	} else {
		var lblName = frmTranfersToRecipents.segTransferToRecipients.selectedItems[0].lbl3;
		var lblAccountNum = frmTranfersToRecipents.segTransferToRecipients.selectedItems[0].lbl4;
		gblisTMB = frmTranfersToRecipents.segTransferToRecipients.selectedItems[0].lbl5;
		gblBANKREF = frmTranfersToRecipents.segTransferToRecipients.selectedItems[0].lblBankName;
		gblToAccountType = frmTranfersToRecipents.segTransferToRecipients.selectedItems[0].accType;
	}
	if (isNotBlank(gblisTMB)) {
		if (gblTrasSMS == 1 && gblTransEmail == 1) {
			var i = gbltranFromSelIndex[1]
			displayHbxNotifyRecipient(false);
			fromData = frmTransferLanding.segTransFrm.data;
			var linkedAccnt= gblXferLnkdAccnts
       		var frmAccnt = fromData[i].accountNum;
       		frmAccnt = kony.string.replace(frmAccnt, "-", "");
			var prodCode = fromData[i].prodCode;
		    lblAccountNum = replaceCommon(lblAccountNum, "-", "");
			if (lblAccountNum[3] == "3" && gblisTMB == gblTMBBankCD){ //to check To account is TD for eventnotificationpolocy
				gblToTDFlag=true
			}else{
				gblToTDFlag=false;
			}
 			if(frmAccnt == linkedAccnt) {
 				if(isOneAccount) {
 					var prdCode = oneAccountData.prodCode;
 				} else {
	 				var prdCode = frmTranfersToRecipents.segTransferToRecipients.selectedItems[0].prodCode;
 				}
				if (prdCode == "206")
					return false;
			}
			if (lblAccountNum.length >= 10) {
				lblAccountNum = kony.string.replace(lblAccountNum, "-", "");
				var k=3;
				if(lblAccountNum.length==10){
					k=3;
				}else if(lblAccountNum.length==14){
					k=7;
				}
				if (gbltdFlag == kony.i18n.getLocalizedString("termDeposit")) {
					if (lblAccountNum[k] == "3" && gblisTMB == gblTMBBankCD)
						return false;
				}
				var acno = ""
				if(lblAccountNum.length==14 && gblisTMB == gblTMBBankCD){
					for (var i = 4; i < 14; i++)
						acno = acno + lblAccountNum[i];
				}else{
					acno = lblAccountNum;
				}
				lblAccountNum = encodeAccntNumbers(acno);
				
			}
		} else {
			if (gblisTMB == gblTMBBankCD) {
				lblAccountNum = kony.string.replace(lblAccountNum, "-", "");
				if (lblAccountNum[3] == "2" || lblAccountNum[3] == "7" || lblAccountNum[3] == "9" || lblAccountNum[3] == "1") {
					lblAccountNum = encodeAccntNumbers(lblAccountNum);
					
				} else {
					return false;
				}
			}
			if(frmTransferLanding.hbxTranLandMyNote.isVisible){
				displayHbxNotifyRecipient(true);
			}
		}
		frmTransferLanding.lblTranLandToAccountNumber.text = lblAccountNum;
		frmTransferLanding.tbxAccountNumber.text = lblAccountNum; 
		frmTransferLanding.lblTranLandToName.text = lblName;
		//frmTransferLanding.imgTranLandTo.src = gblXferRecImg;
	
		if(isOneAccount) {
			gblSmartAccountName = oneAccountData.accountName;
		} else {
			gblSmartAccountName= frmTranfersToRecipents.segTransferToRecipients.selectedItems[0].accountName;
		}
		
		if(gblSmartAccountName == null || gblSmartAccountName == undefined || gblSmartAccountName == "NONE" || gblSmartAccountName == "NA")
		{
			gblSmartAccountName="";
		}
		
		frmTransferLanding.show();
		//frmTransferLanding.lineRecipientDetails.skin = lineBlue;
		assignBankSelectDetails(gblisTMB);
		resetTransferLandingBasedOnToAccount();
		//transAmountOnDone(false); //Amount auto populate so we need to call calculate fee service.
	}
}

/**************************************************************************************
		Module	: 
		Author  : Kony
		Date    : May 05, 2013
		Purpose : This function paints the TransferHome page.
*****************************************************************************************/

function ResetTransferHomePage() {
 	if (gbltdFlag == kony.i18n.getLocalizedString("termDeposit")){
		transferLandingdefaultVisiblity();
		enableTransferTextBoxAmount(false);
		frmTransferLanding.txtTranLandAmt.text = "";
		makeWidgetsBelowTransferFeeButtonVisible(true);
	}else{
		enableTransferTextBoxAmount(true);
		frmTransferLanding.txtTranLandAmt.text = "";
		transferLandingdefaultVisiblity();
	}
	frmTransferLanding.lblTranLandToName.text = ""
	frmTransferLanding.tbxAccountNumber.text = ""

	frmTransferLanding.tbxAccountNumber.maxTextLength = 16;
	frmTransferLanding.hbxTransLndFee.setVisibility(false);
	frmTransferLanding.hbxFeeTransfer.setVisibility(true);
	frmTransferLanding.vbxSelRecipient.onClick = ehFrmTransferLanding_btnTranLandToSel_onClick;//for dream saving acnts
	frmTransferLanding.tbxAccountNumber.text = "";
	frmTransferLanding.lblTranLandToAccountNumber.text = "";
	frmTransferLanding.txtTranLandMyNote.text = "";
	frmTransferLanding.txtTranLandRecNote.text = "";
	frmTransferLanding.textRecNoteEmail.text = "";
	frmTransferLanding.txtOnUsMobileNo.text = "";
	frmTransferLanding.lblMobileNoTemp.text = "";
	frmTransferLanding.lblRecipientName.text = "";
	frmTransferLanding.lblSelectBank.setVisibility(true);
	frmTransferLanding.hbxSelectBank.setVisibility(false);
	frmTransferLanding.lblFeeTransfer.skin = "lblWhite36px";
	setOnClickSetCanlenderBtn(true);
	displayAmountTextBox(true);
	displayOnUsEnterMobileNumber(false);
	displayOnUsEnterCitizenID(false);
	displayP2PITMXBank(false);
	displayOnUsEnterBankAccount(true);
	displayP2PITMXFee(false,"");
	
	var date=new Date(GLOBAL_TODAY_DATE);
	var curMnth = date.getMonth()+1;
			var curDate = date.getDate();
			if ((curMnth.toString().length) == 1) {
				curMnth = "0" + curMnth;
			}
			if ((curDate.toString().length) == 1) {
				curDate = "0" + curDate;
			}
	var datetime = "" + curDate + "/" + curMnth + "/" + date.getFullYear();
	frmTransferLanding.lblSchedSel.text = datetime;
	frmTransferLanding.lblRecievedBy.text = datetime;
	frmTransferLanding.lblRecievedByValue.text = kony.i18n.getLocalizedString("keyNOW");
	repeatNumberOfTimesText = date; //for fix 5119
	frmTransferLanding.lblScheduleEndContainerHidden.text = "";
	frmTransferLanding.txtTransLndSms.setVisibility(false);
	frmTransferLanding.txtTransLndSmsNEmail.setVisibility(false);
	frmTransferLanding.btnTranLandSms.src = "mobile_grey.png";
	frmTransferLanding.btnTranLandEmail.src = "email_grey.png";
	frmTransferLanding.btnOnUsMobileSmS.src = "tran_notify_mobile_only.png";
	frmTransferLanding.btnTransLndORFT.skin = "btnFeeTop";
	frmTransferLanding.btnTransLndSmart.skin = "btnFeeBottom";
	frmTransferLanding.btnOnUsMobileSmS.skin = "btnMobileGrey";
	gblsplitAmt = [];
	gblsplitFee = [];
	gblSplitAckImg = [];
	gblPaynow = true;
	gblScheduleRepeatBP = "Once";
	gblScheduleEndBP = "none";
	gblNumberOfDays = "";
	gblStartBPDate = "";
	gblEndBPDate = "";
	gblTrasORFT = 0;
	gblTransSMART = 0;
	gblTrasSMS = 0;
	gblTransEmail = 0;
	gblisTMB = "";
	gblp2pAccountNumber = "";
	
	//Auto Populate TMB if from Account is only eligible to transfer to TMB
	//fromAccountIsOnlyAllowedForTMB();
	//frmTransferLanding.tbxAccountNumber.setFocus(true);
}

function fromAccountIsOnlyAllowedForTMB() {
	if(gblSelTransferMode == 2 || gblSelTransferMode == 3){
		return;
	}
	if(frmTransferLanding.segTransFrm.selectedItems != null) {
		var isOnlyTmb = frmTransferLanding.segTransFrm.selectedItems[0].isOtherBankAllowed;
	} else {
		return;
	}		        
	if(isOnlyTmb != "Y") {
		var bankData = getBankListForBankPopup(globalSelectBankData, 1);
		popUpBankList.segBanklist.setData(bankData);
		autoSelectRecepientBank(bankData);
		frmTransferLanding.hbxPleaseSelectBank.setEnabled(false);
		frmTransferLanding.hbxSelectBank.setEnabled(false);
	} else {
		frmTransferLanding.hbxPleaseSelectBank.setEnabled(true);
		frmTransferLanding.hbxSelectBank.setEnabled(true);
		popUpBankList.segBanklist.setData(getBankListForBankPopup(globalSelectBankData, globalSelectBankData.length));
	}
}

/*************************************************************************

	Module	: getTransferFromAccounts
	Author  : Kony
	Purpose : getting data for transfer landing screen of from account

****/

function getTransferFromAccounts() {
gblFlagMenu = "";

 if(checkMBUserStatus()){
	isMenuShown = false;
	if (kony.application.getCurrentForm().id == "frmTransferLanding")
	{
		dismissLoadingScreen();  // Need to dismiss loading indicator which is shown in Force touch for Logged In scenario.
		frmTransferLanding.scrollboxMain.scrollToEnd();
	}
	else
	{
		gblRetryCountRequestOTP = "0";
		/*if(!recipientAddFromTransfer){
		    ResetTransferHomePage();
		    //frmTransferLanding = null;
		    Recp_category = 0;
		    //frmTransferLandingGlobals();
			TMBUtil.DestroyForm(frmTransferLanding);
		}*/
		
		gblPaynow = true;
	    GBLFINANACIALACTIVITYLOG = {}
	    var inputParam = {}
	    inputParam["transferFlag"] = "true";
	    showLoadingScreen();
	    destroyMenuSpa();
	    invokeServiceSecureAsync("customerAccountInquiry", inputParam, callBackTransferFromAccounts);
	}
  }
}



/*************************************************************************

	Module	: getTDAccount
	Author  : Kony
	Purpose : getting data of TD Account

******************************************************************************/

function getTDAccount() {
		var inputParam = {}
		var fromAcctID;		
		var i = gbltranFromSelIndex[1]
		
		var fromData = frmTransferLanding.segTransFrm.data;
		var frmID = fromData[i].lblActNoval
		var fromFIIdent = fromData[i].fromFIIdent;
		fromAcctID = kony.string.replace(frmID,"-","");
		if (fromAcctID.length == 14) {
			fromAcctID = kony.string.sub(fromAcctID,4,fromAcctID.length)
		}
		inputParam["acctIdentValue"] = fromAcctID;
		inputParam["fIIdent"] = fromFIIdent;
		invokeServiceSecureAsync("tDDetailinq", inputParam, callBackTDAccount)
}


/*************************************************************************

	Module	: selectTDDetails
	Author  : Kony
	Purpose : selected TD Account details

****/

function selectTDDetails() {
	if(popTransfersTDAccount.segTransfersTD.data != null && popTransfersTDAccount.segTransfersTD.data.length > 0)
	{
		popTransfersTDAccount.show();
	}
}


/*************************************************************************

	Module	: onEditTransConfirm
	Author  : Kony
	Purpose : Defining on edit for tranfer confrimation screen

****/

function onEditTransConfirm() {
	isFromEdit=true;
	frmTransferLanding.show()
}


/*
************************************************************************

	Module	: transAmountOnClickMB
	Author  : Kony
	Purpose : Removing the comma for Amount field

****/
function transAmountOnClickMB() {
	gblAmountChange = true;
	if(frmTransferLanding.txtTranLandAmt.text !=undefined && frmTransferLanding.txtTranLandAmt.text.trim() != ""  ){
		//frmTransferLanding.txtTranLandAmt.text = replaceCommon(frmTransferLanding.txtTranLandAmt.text, ",", "");
	}
}

/*************************************************************************

	Module	: onTransferLndngNext
	Author  : Kony
	Purpose : Making validations for the data enterd in landing page

**************************************************************************/

function onTransferLndngNext() {
	if(gblSelTransferMode == 2 || gblSelTransferMode == 3){// p 2 p transfer  mobile number
		mobileNumberTransferNext();
	}else{// account transfer
		accountTransferNext();
	}
}
function mobileNumberTransferNext(){
	if(!validateTransferFieldsP2P()){
		return;
	}
	
	var inputParam = {}
	var fromData = "";
 	var i = gbltranFromSelIndex[1];
	fromData = frmTransferLanding.segTransFrm.data;
	var frmID = fromData[i].lblActNoval;
 	var fromAcctID = kony.string.replace(frmID,"-", "");
	
 	var toAcctID  = "";
 	var mobileOrCI = "";
 	if(gblSelTransferMode == 2){
		toAcctID  = removeHyphenIB(frmTransferLanding.txtOnUsMobileNo.text);
		mobileOrCI = "02";
 	} else if(gblSelTransferMode == 3){
 		toAcctID  = removeHyphenIB(frmTransferLanding.txtCitizenID.text);
 		mobileOrCI = "01";
 	}
	
	frmTransferConfirm.lblHiddenToAccount.text = toAcctID;
	inputParam["fromAcctNo"] = fromAcctID;
	inputParam["toAcctNo"] = toAcctID;
	inputParam["toFIIdent"] = gblisTMB;	
	inputParam["transferAmt"] = enteredAmount;
	inputParam["mobileOrCI"] = mobileOrCI;
	showLoadingScreen();
	invokeServiceSecureAsync("checkOnUsPromptPayinq", inputParam, callBackCheckOnUsPromptPayinqServiceMBOnNext);
}

function accountTransferNext(){
	/** check for Empty Fields on Tranfer LP ****/
    gblNofeeVar="";
	resetSharingDisplayToDefault();
 	frmTransferConfirm.lblTransCnfmToAccountName.text =  "";
 	//if(frmTransferLanding.hbxRecipientNumberAndName.isVisible){
	if(frmTransferLanding.tbxAccountNumber.isVisible){
 		frmTransferConfirm.lblTransCnfmToNum.text = frmTransferLanding.tbxAccountNumber.text;
 		frmTransferLanding.lblTranLandToName.text = "";
 		gblSmartAccountName = "";
 		frmTransferConfirm.lblTransCnfmToBankName.text = frmTransferLanding.lblBankShortName.text;
 	}else{
		frmTransferConfirm.lblTransCnfmToNum.text = frmTransferLanding.lblTranLandToAccountNumber.text;
		frmTransferConfirm.lblTransCnfmToBankName.text = frmTransferLanding.lblTranLandToName.text;
 	}
	frmTransferConfirm.lblHiddenToAccount.text = frmTransferConfirm.lblTransCnfmToNum.text;
	// Start #MIB-2274 : prevent double error on android device
	if(kony.string.equalsIgnoreCase(gblDeviceInfo["name"],"android")){
		if(!isAvoidDuplicateError){
			if (!isNotBlank(frmTransferLanding.txtTranLandAmt.text)) {
				showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_P2PkeyPleaseEnterAmount"), 
					kony.i18n.getLocalizedString("info"), callBackAmountFields);
				//frmTransferLanding.lineAmount.skin = linePopupBlack;
				setEnabledTransferLandingPage(true);
				return false;
			}
		}else{
			setEnabledTransferLandingPage(true);
			return false; // as error occurs already from the first check.
		}
	}else{
		if (!isNotBlank(frmTransferLanding.txtTranLandAmt.text)) {
			showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_P2PkeyPleaseEnterAmount"), 
				kony.i18n.getLocalizedString("info"), callBackAmountFields);
				//frmTransferLanding.lineAmount.skin = linePopupBlack;
			setEnabledTransferLandingPage(true);
			return false;
		}
	}
	//frmTransferLanding.lineAmount.skin = lineBlue;
	enteredAmount = frmTransferLanding.txtTranLandAmt.text;
	enteredAmount = kony.string.replace(enteredAmount, ",", "");
	frmTransferLanding.txtTranLandAmt.text = numberWithCommas(fixedToTwoDecimal(enteredAmount));
    enteredAmount = fixedToTwoDecimal(enteredAmount);
	if (parseFloat(enteredAmount, 10) == 0) {
		showAlertWithCallBack(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), 
				kony.i18n.getLocalizedString("info"), callBackAmountFields);
		frmTransferLanding.txtTranLandAmt.text = "";
		//frmTransferLanding.lineAmount.skin = linePopupBlack;
		setEnabledTransferLandingPage(true);
		return false;
	}
	// End #MIB-2274
	transAmountOnDone(false,false); //to prevent minimize keyboard issue
	
	if (!validateTransferFields()) {
		setEnabledTransferLandingPage(true);
		return false;
	}
	if(gblisTMB != gblTMBBankCD){
		
		getTransferFeeMBNextClick();
	}
	else{
		afterTransferFeeContinueNext();
	}
			
}


function afterTransferFeeContinueNext(){
    //if (!checkMaxLimitForTransfer()) {
    //   return false;
    //}
    if (frmTransferLanding.txtTranLandMyNote.text.length > 50) {
    	//TextBoxErrorSkin(frmTransferLanding.txtTranLandMyNote, txtErrorBG);
    	alert(kony.i18n.getLocalizedString("MaxlngthNoteError"));
    	setEnabledTransferLandingPage(true);
    	return false;
	}
	if(gblSmartAccountName == null || gblSmartAccountName == "null" || gblSmartAccountName == undefined || gblSmartAccountName == "NONE" || gblSmartAccountName == "NA"){
		gblSmartAccountName = "";
	}
	frmTransferConfirm.lblTransCnfmToAccountName.text = gblSmartAccountName;
	frmTransfersAck.lblTransNPbAckToAccountName.text = gblSmartAccountName;
	gblTDDateFlag=false;
 	/**crm Profile invocation */	
 	if(!gblPaynow && gblTransSMART == 1){
		var dateTemp = GBL_SMART_DATE_FT;
		dateTemp = GBL_SMART_DATE_FT.split("/");
		var monthTemp = parseInt(dateTemp[1],10)
		var d2 = new Date(dateTemp[2],monthTemp-1,dateTemp[0])
		if(d2.getDay() == 6 || d2.getDay() == 0){
			alert(kony.i18n.getLocalizedString("keyFTSMARTOnWeekend"));
			setEnabledTransferLandingPage(true);
			return;
		}
		var dateToSend = dateTemp[2]+"/"+dateTemp[1]+"/"+dateTemp[0];
		
		srvHolydayChkFutureTransSetMB(dateToSend);
	}else{
		checkCrmProfileInq();
	}
	gblsplitAmt = [];
	gblsplitFee = [];
	gblSplitAckImg = [];
	gblSplitCnt = 0;
	gblSplitStatusInfo = [];
	var activityTypeID = "";
	var activityFlexValues5 = "";
	var fee = "";
	var i = gbltranFromSelIndex[1];
	
	var fromData = frmTransferLanding.segTransFrm.data;
	
 	if (gblSMART_FREE_TRANS_CODES.indexOf(fromData[i].prodCode) >= 0) {
		ramainfeeValue = parseFloat(fromData[i].lblRemainFeeValue) < 0 ? 0 : fromData[i].lblRemainFeeValue ;
		frmTransferConfirm.hbxFreeTrans.setVisibility(true);
		frmTransfersAck.hbxFreeTrans.setVisibility(true);
		frmTransferConfirm.lblFreeTransValue.text=ramainfeeValue;
 	} else {
		frmTransferConfirm.hbxFreeTrans.setVisibility(false);
		frmTransfersAck.hbxFreeTrans.setVisibility(false);
 	}
 	frmTransferConfirm.hbxBalAfterTransfer.setVisibility(true);
	frmTransferConfirm.hbxTransCnfmBalBefVal.setVisibility(true);
 	if(!gblPaynow){
 		frmTransferConfirm.hbxFreeTrans.setVisibility(false);
		frmTransfersAck.hbxFreeTrans.setVisibility(false);
		frmTransferConfirm.hbxBalAfterTransfer.setVisibility(false);
		frmTransferConfirm.hbxTransCnfmBalBefVal.setVisibility(false);
 	}
 	frmTransferConfirm.hbxScheduleNote.setVisibility(false);
 	if(!gblPaynow){
 		if ((gblTrasORFT == 1 && gblORFT_FREE_TRANS_CODES.indexOf(fromData[i].prodCode) >= 0)  || (gblTransSMART == 1 &&  gblSMART_FREE_TRANS_CODES.indexOf(fromData[i].prodCode) >= 0))
 		{
			frmTransferConfirm.hbxScheduleNote.setVisibility(true);
 		}
		else{
		
			frmTransferConfirm.hbxScheduleNote.setVisibility(false);
		}
	}
	var randomnum = Math.floor((Math.random()*10000)+1);
	frmTransferConfirm.imgTransCnfmTo.src = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId=" + gblisTMB + "&modIdentifier=BANKICON&rr="+randomnum;
	frmTransfersAck.imgTransNPbAckTo.src = frmTransferConfirm.imgTransCnfmTo.src;

}
/*************************************************************************

	Module	: splitAmount
	Author  : Kony
	Purpose : spliting Amount

****/

function splitAmount(Amount) {
	var minFee;
	var maxFee;
	gblsplitAmt = [];
	gblsplitFee = [];
	var transORFTSplitAmnt = parseFloat(gblLimitORFTPerTransaction);
	var transSMARTSplitAmnt = parseFloat(gblLimitSMARTPerTransaction);
	
	var i = gbltranFromSelIndex[1]
	
	var fromData = frmTransferLanding.segTransFrm.data;
	var prodCode = fromData[i].prodCode;
	var remainingFee = fromData[i].remainingFee;
	remainingFee = parseInt(remainingFee);
	if (gblTrasORFT == 1) {
		minFee = parseInt(gblXerSplitData[0].ORFTSPlitFeeAmnt1);
		maxFee = parseInt(gblXerSplitData[0].ORFTSPlitFeeAmnt2);
		var orftRange1Lower = parseInt(gblXerSplitData[0].ORFTRange1Lower);
		var orftRange2Lower = parseInt(gblXerSplitData[0].ORFTRange2Lower);
		var orftRange1High = parseInt(gblXerSplitData[0].ORFTRange1Higher);
		var orftRange2High = parseInt(gblXerSplitData[0].ORFTRange2Higher);
		var SplitCnt = parseInt(Amount / transORFTSplitAmnt);
		
		var indexdot = Amount.indexOf(".");
		var decimal = "";
		var remAmt = "";
		if (indexdot > 0) {
			decimal = Amount.substr(indexdot, 3);
			Amount = Amount.substr(0, indexdot)
			remAmt = Amount % transORFTSplitAmnt;
			remAmt = remAmt + decimal;
			
		} else {
			remAmt = Amount % transORFTSplitAmnt;
			remAmt = remAmt + ".00";
		}
		for (var i = 0; i < SplitCnt; i++) {
			gblsplitAmt.push(transORFTSplitAmnt + ".00");
			if(gblORFT_ALL_FREE_TRANS_CODES.indexOf(prodCode) >= 0){
				gblsplitFee.push("0");
			}else if (gblORFT_FREE_TRANS_CODES.indexOf(prodCode) >= 0) {
				if (gblPaynow && remainingFee > 0) {
					gblsplitFee.push("0");
					remainingFee = remainingFee - 1;
				} else {
					if (transORFTSplitAmnt > orftRange1High)
						gblsplitFee.push(maxFee);
					else
						gblsplitFee.push(minFee);
				}
			} else {
				if (transORFTSplitAmnt > orftRange1High)
					gblsplitFee.push(maxFee);
				else
					gblsplitFee.push(minFee);
			}
		}
		if (remAmt > orftRange1Lower) {
			gblsplitAmt.push(remAmt);
			if (remAmt <= orftRange1High) {
				if(gblORFT_ALL_FREE_TRANS_CODES.indexOf(prodCode) >= 0){
					gblsplitFee.push("0");
				}else if (gblORFT_FREE_TRANS_CODES.indexOf(prodCode) >= 0) {
					if (gblPaynow && remainingFee > 0) {
						gblsplitFee.push("0");
						remainingFee = remainingFee - 1;
					} else
						gblsplitFee.push(minFee);
				} else
					gblsplitFee.push(minFee);
			} else {
				if(gblORFT_ALL_FREE_TRANS_CODES.indexOf(prodCode) >= 0){
					gblsplitFee.push("0");
				}else if (gblORFT_FREE_TRANS_CODES.indexOf(prodCode) >= 0) {
					if (gblPaynow && remainingFee > 0) {
						gblsplitFee.push("0");
						remainingFee = remainingFee - 1;
					} else
						gblsplitFee.push(maxFee);
				} else
					gblsplitFee.push(maxFee);
			}
		}
		
	} else if (gblTransSMART == 1) {
		
		minFee = parseInt(gblXerSplitData[0].SMARTSPlitFeeAmnt1);
		maxFee = parseInt(gblXerSplitData[0].SMARTSPlitFeeAmnt2);
		var smartRange1Lower = parseInt(gblXerSplitData[0].SMARTRange1Lower);
		var smartRange2Lower = parseInt(gblXerSplitData[0].SMARTRange2Lower);
		var smartRange1High = parseInt(gblXerSplitData[0].SMARTRange1Higher);
		var smartRange2High = parseInt(gblXerSplitData[0].SMARTRange2Higher);
		var SplitCnt = parseInt(Amount / transSMARTSplitAmnt);
		
		//
		var indexdot = Amount.indexOf(".");
		var decimal = "";
		var remAmt = "";
		if (indexdot > 0) {
			decimal = Amount.substr(indexdot, 3);
			Amount = Amount.substr(0, indexdot)
			remAmt = Amount % transSMARTSplitAmnt;
			remAmt = remAmt + decimal;
			
		} else {
			remAmt = Amount % transSMARTSplitAmnt;
			remAmt = remAmt + ".00"
		}
		for (var i = 0; i < SplitCnt; i++) {
			gblsplitAmt.push(transSMARTSplitAmnt + ".00");
			if (gblSMART_FREE_TRANS_CODES.indexOf(prodCode) >= 0 ) {
				if (gblPaynow && remainingFee > 0) {
					gblsplitFee.push("0");
					remainingFee = remainingFee - 1;
				} else {
					if (transSMARTSplitAmnt > smartRange1High)
						gblsplitFee.push(maxFee);
					else
						gblsplitFee.push(minFee);
				}
			} else if (gblALL_SMART_FREE_TRANS_CODES.indexOf(prodCode) >= 0) {
					gblsplitFee.push("0"); 
			} else {
				if (transSMARTSplitAmnt > smartRange1High)
					gblsplitFee.push(maxFee);
				else
					gblsplitFee.push(minFee);
			}
		}
		if (remAmt > smartRange1Lower) {
			gblsplitAmt.push(remAmt);
			if (remAmt <= smartRange1High) {
				if (gblSMART_FREE_TRANS_CODES.indexOf(prodCode) >= 0) {
					if (gblPaynow && remainingFee > 0) {
						gblsplitFee.push("0");
						remainingFee = remainingFee - 1;
					} else
						gblsplitFee.push(minFee);
				} else if (gblALL_SMART_FREE_TRANS_CODES.indexOf(prodCode) >= 0) {
					gblsplitFee.push("0");
				} else {
					gblsplitFee.push(minFee);
				}
			} else {
				if (gblSMART_FREE_TRANS_CODES.indexOf(prodCode) >= 0) {
					if (gblPaynow && remainingFee > 0) {
						gblsplitFee.push("0");
						remainingFee = remainingFee - 1;
					} else
						gblsplitFee.push(maxFee);
				} if (gblALL_SMART_FREE_TRANS_CODES.indexOf(prodCode) >= 0) {
					gblsplitFee.push("0"); 
				} else {
					gblsplitFee.push(maxFee);
				}
			}
		}
		
	}
}

/*************************************************************************

	Module	: splitingTransaction
	Author  : Kony
	Purpose : Setting data into Transfer confirm screens

****/

function splitingTransaction(confirm) {
	if (confirm == true) {
		resetHbxTransferDetails();
		if (gblPaynow) {
			displayNormalTransfer(true); // isMega = true
		} else {
			displayScheduleTransfer(true); // isMega = true
			frmTransferConfirm.lblHiddenVariableForScheduleEnd.text = frmTransferLanding.lblScheduleEndContainerHidden.text;
			frmTransferConfirm.lblStartOnValue.text = gblStartBPDate;
			frmTransferConfirm.lblEndOnValue.text = gblEndBPDate;
			
			var repeatValue="";
			if(kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Daily")){
				repeatValue = kony.i18n.getLocalizedString("keyDaily");
			}else if(kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Weekly")){
				repeatValue = kony.i18n.getLocalizedString("keyWeekly");
			}else if(kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Monthly")){
				repeatValue = kony.i18n.getLocalizedString("keyMonthly");
			}else if(kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Yearly")){
				repeatValue = kony.i18n.getLocalizedString("keyYearly");
			}else{
				repeatValue = kony.i18n.getLocalizedString("keyOnce");
			}
			
			frmTransferConfirm.lblRepeatValue.text = repeatValue;
			frmTransferConfirm.lblExecuteValue.text = gblGiveToConfirmationLabel;
			if (kony.string.equalsIgnoreCase(gblScheduleEndBP, "none")){
			   	frmTransferConfirm.lblEndOnValue.text = frmTransferConfirm.lblStartOnValue.text;
				frmTransferConfirm.lblExecuteValue.text = "1 times";
			} 
			if (kony.string.equalsIgnoreCase(gblScheduleEndBP, "Never")){
			    frmTransferConfirm.lblEndOnValue.text = "-";
				frmTransferConfirm.lblExecuteValue.text = "-";
		 	}
		}
		if(gblTransSMART == 1){
			frmTransferConfirm.lblSmartDateVal.skin = lblbluemedium142;
		}else{
			frmTransferConfirm.lblSmartDateVal.skin = lblGreyRegular142;
		}
		if (gblTransSMART == 1 && gblPaynow) {
			frmTransferConfirm.lblSmartDateVal.text = smartDate;
			frmTransfersAck.lblSmartDateVal.text = smartDate;
		} else {
			var receivedDate = frmTransferLanding.lblRecievedByValue.text;
			if(gblPaynow){
				receivedDate = frmTransferConfirm.lblTransCnfmDateVal.text; 
			}
			frmTransferConfirm.lblSmartDateVal.text = receivedDate;
			frmTransfersAck.lblSmartDateVal.text = receivedDate;
		}
		frmTransferConfirm.imgTransCnfmFrm.src  = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId=" + gblTMBBankCD + "&modIdentifier=BANKICON";
		
		
		frmTransferConfirm.lblTransCnfmFrmName.text = frmTransferLanding.segTransFrm.selectedItems[0].lblCustName;
		frmTransferConfirm.lblTransCnfmFrmNum.text = frmTransferLanding.segTransFrm.selectedItems[0].lblActNoval;
		frmTransferConfirm.lblTransCnfmBalBefAmt.text = frmTransferLanding.segTransFrm.selectedItems[0].lblBalance;
		frmTransferConfirm.lblTransCnfmCustomerName.text = frmTransferLanding.segTransFrm.selectedItems[0].custAcctName;
		frmTransferConfirm.lblTransCnfmTotVal.text = commaFormatted(parseFloat(enteredAmount).toFixed(2)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
		frmTransferConfirm.segTransCnfmSplit.widgetDataMap = {
			lblTransCnfmSplitAmtVal: "lblTransCnfmSplitAmtVal",
			lblTransCnfmSplitRefNoVal: "lblTransCnfmSplitRefNoVal"
		}
		var splitAmtLen = gblsplitAmt.length;
		var tempData = [];
		var totalFee = 0;
		for (var i = 0; i < splitAmtLen; i++) {
		    if(gblsplitFee[i] != 0)        //37806 Ticket fix
			 totalFee += gblsplitFee[i];
			var pad = "";
			if (i < 9){
				pad = "0" + (i + 1);
			}else{
				pad = (i + 1);
			}
			
			var refNoToDisplay = "";
			if(gblPaynow){
				refNoToDisplay = "NT"+ gblTransferRefNo + pad;
			}else{
				refNoToDisplay = "ST"+ gblTransferRefNo + pad;
			}
				
			var temp = {
				lblTransCnfmSplitAmtVal: commaFormatted(gblsplitAmt[i]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht") + " (" + commaFormatted(gblsplitFee[i]+"") + " " + kony.i18n.getLocalizedString("currencyThaiBaht") + ")",
				lblTransCnfmSplitRefNoVal: refNoToDisplay
			}
			tempData.push(temp);
		}
		frmTransferConfirm.lblTransCnfmTotFeeVal.text = "(" + commaFormatted(parseFloat(totalFee).toFixed(2)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht") + ")";
		frmTransferConfirm.lblTransCnfmTotVal3.text = frmTransferConfirm.lblTransCnfmTotVal.text 
			+ " (" + commaFormatted(parseFloat(totalFee).toFixed(2)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht") + ")";
		
		frmTransferConfirm.segTransCnfmSplit.data = tempData;
		frmTransferConfirm.show();
	} else {
		setEnabledTransferLandingPage(true);
		return;
	}
}
/*
************************************************************************

	Module	: normalTransaction
	Author  : Kony
	Purpose : Setting data into Transfer confirm screens

************************************************************************/

function normalTransaction(confirm) {
	if (confirm == true) {
		resetHbxTransferDetails();
		if (gblPaynow) {
			if(frmTransferLanding.hboxTD.isVisible){
				displayTDTransferDetails();
			}else{
				displayNormalTransfer(false); // isMega = false;
			}
		} else {
			displayScheduleTransfer(false); //isMega = false;
			frmTransferConfirm.lblHiddenVariableForScheduleEnd.text = frmTransferLanding.lblScheduleEndContainerHidden.text;
			frmTransferConfirm.lblStartOnValue.text = gblStartBPDate;
			frmTransferConfirm.lblEndOnValue.text = gblEndBPDate;
			
			var repeatValue="";
			if(kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Daily")){
				repeatValue = kony.i18n.getLocalizedString("keyDaily");
			}else if(kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Weekly")){
				repeatValue = kony.i18n.getLocalizedString("keyWeekly");
			}else if(kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Monthly")){
				repeatValue = kony.i18n.getLocalizedString("keyMonthly");
			}else if(kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Yearly")){
				repeatValue = kony.i18n.getLocalizedString("keyYearly");
			}else{
				repeatValue = kony.i18n.getLocalizedString("keyOnce");
			}			
			
			frmTransferConfirm.lblRepeatValue.text = repeatValue;
			frmTransferConfirm.lblExecuteValue.text = gblGiveToConfirmationLabel;
			
			if (kony.string.equalsIgnoreCase(gblScheduleEndBP, "none")){
				frmTransferConfirm.lblEndOnValue.text = frmTransferConfirm.lblStartOnValue.text;
				frmTransferConfirm.lblExecuteValue.text = "1 times";
			} 
			if (kony.string.equalsIgnoreCase(gblScheduleEndBP, "Never")){
		        frmTransferConfirm.lblEndOnValue.text = "-";
				frmTransferConfirm.lblExecuteValue.text = "-";
		 	}
		}
		
		frmTransferConfirm.imgTransCnfmFrm.src = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId=" + gblTMBBankCD + "&modIdentifier=BANKICON";
		frmTransferConfirm.lblTransCnfmFrmName.text = frmTransferLanding.segTransFrm.selectedItems[0].lblCustName;
		frmTransferConfirm.lblTransCnfmFrmNum.text = frmTransferLanding.segTransFrm.selectedItems[0].lblActNoval;
		frmTransferConfirm.lblTransCnfmBalBefAmt.text = frmTransferLanding.segTransFrm.selectedItems[0].lblBalance;
		frmTransferConfirm.lblTransCnfmCustomerName.text = frmTransferLanding.segTransFrm.selectedItems[0].custAcctName;
		
		
		//for TD account remove the comma in amount feild
		//var amtRcd = kony.string.replace(frmTransferLanding.txtTranLandAmt.text, kony.i18n.getLocalizedString("currencyThaiBaht"), "");
		//amtRcd = amtRcd.replace(/ /g, "");
		var amtRcd = commaFormatted(parseFloat(enteredAmount).toFixed(2))
		frmTransferConfirm.lblTransCnfmTotVal.text = amtRcd + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
		var transactionRefNo = "";
		if(gblPaynow){
			transactionRefNo = "NT" + gblTransferRefNo + "00";
		}else{
			transactionRefNo = "ST" + gblTransferRefNo + "00";
		}
		var fee;
		var indexOfB;
		if (gblisTMB != gblTMBBankCD && gblTrasORFT == 1) {
			fee = frmTransferLanding.btnTransLndORFT.text;
			frmTransferConfirm.lblTransCnfmTotFeeVal.text = "(" + parseFloat(fee).toFixed(2) + " " + kony.i18n.getLocalizedString("currencyThaiBaht") + ")";
		} else if (gblisTMB != gblTMBBankCD && gblTransSMART == 1) {
			fee = frmTransferLanding.btnTransLndSmart.text;
			frmTransferConfirm.lblTransCnfmTotFeeVal.text = "(" + parseFloat(fee).toFixed(2) + " " + kony.i18n.getLocalizedString("currencyThaiBaht") + ")";
		} 
		//ENH_129 SMART Transfer Add Date & Time
		if(gblTransSMART == 1){
			frmTransferConfirm.lblSmartDateVal.skin = lblbluemedium142;
		}else{
			frmTransferConfirm.lblSmartDateVal.skin = lblGreyRegular142;
		}
		if (gblTransSMART == 1 && gblPaynow) {
			frmTransferConfirm.lblSmartDateVal.text = smartDate;
			frmTransfersAck.lblSmartDateVal.text = smartDate;
		} else {
			var receivedDate = frmTransferLanding.lblRecievedByValue.text;
			if(gblPaynow){
				receivedDate = frmTransferConfirm.lblTransCnfmDateVal.text; 
			}
			frmTransferConfirm.lblSmartDateVal.text = receivedDate;
			frmTransfersAck.lblSmartDateVal.text = receivedDate;
		}
		
		if(frmTransferLanding.hboxTD.isVisible){
			frmTransferConfirm.lblTransCnfmRefNumVal2.text = transactionRefNo;
			fee = removeSquareBrackets(frmTransferConfirm.lblTransCnfmTotFeeVal.text); 
			frmTransferConfirm.lblTransCnfmTotVal.text = frmTransferConfirm.lblTransCnfmNetAmtVal.text;
			frmTransferConfirm.lblTransCnfmNetAmtVal.text = frmTransferConfirm.lblTransCnfmNetAmtVal.text + " (" + fee +")";
			frmTransferConfirm.lblTransCnfmTotFeeVal.text = "(" +gblTDTransfer + ")";
			frmTransferConfirm.lblPrincipalAmntVal.text = amtRcd + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
		}else{
			frmTransferConfirm.lblTransCnfmRefNumVal.text = transactionRefNo;
			fee = removeSquareBrackets(frmTransferConfirm.lblTransCnfmTotFeeVal.text);
			frmTransferConfirm.lblTransCnfmTotVal2.text = amtRcd + " " + kony.i18n.getLocalizedString("currencyThaiBaht") + " (" + fee +")";
		}
		if(!gblPaynow){
			frmTransferConfirm.lblTransCnfmDateVal.text = returnDateForFT();	
		}
		frmTransferConfirm.show();
		setEnabledTransferLandingPage(true);
		//TMBUtil.DestroyForm(frmTransfersAck)
		
	} else {
		setEnabledTransferLandingPage(true);
		return;
	}
}
/*************************************************************************

	Module	: onClickConfirmPop
	Author  : Kony
	Purpose : Onclick of PopupTransferConfirm

****/

function onClickConfirmPop() {
	if (popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text == "") {
		setTransPwdFailedError(kony.i18n.getLocalizedString("emptyMBTransPwd"));
		return false;
	} else
		checkVerifyPWDMB();
		//transferFlowAckMB()
}
/*************************************************************************

	Module	: transferFlowAckMB
	Author  : Kony
	Purpose : after validate OTP

****/

function transferFlowAckMB() {
	gblBalAfterXfer = ""
	//TMBUtil.DestroyForm(frmTransfersAck)
	popupTractPwd.dismiss();
	popTransactionPwd.dismiss();
	var transloop;
	if (gblsplitAmt.length > 0) {
		transloop = gblsplitAmt.length;
	} else {
		transloop = 1;
	}
	
	gblAckFlage = "false"
	
	for (var i = 0; i < transloop; i++) {
		if ((transloop - 1) == i)
			gblAckFlage = "true";
		else
			gblAckFlage = "false";
		if (!gblPaynow) {
			if (transloop > 1) {
				var amount = gblsplitAmt[i];
				var fee = gblsplitFee[i] + ".00";
				var amountStr = parseFloat(amount) ;
				var amtFinal = amountStr.toFixed(2);
				var pad = "";
				if (i < 9)
					pad = "0" + (i + 1);
				else
					pad = (i + 1);
				GBLFINANACIALACTIVITYLOG.finTxnRefId = "ST" + gblTransferRefNo+ pad ;
				var refnoFT = pad + gblTransferRefNo
				doFuturePaymentAddTransferMB(amtFinal, "ST" +gblTransferRefNo+ pad );
			} else {
				var amountOrg = frmTransferConfirm.lblTransCnfmTotVal.text;
				var feeOrg = removeSquareBrackets(frmTransferConfirm.lblTransCnfmTotFeeVal.text);
				var amountFormatted = amountOrg.replace(/,/g, "");
				var feeFormatted = feeOrg.replace(/,/g, "");
				var amt = parseFloat(amountFormatted) ;
				var amtFinal = amt.toFixed(2);
				GBLFINANACIALACTIVITYLOG.finTxnRefId = "ST"  +gblTransferRefNo+ "00" ;
				var refnoFT = "00" + gblTransferRefNo
				doFuturePaymentAddTransferMB(amtFinal, "ST" + gblTransferRefNo+ "00" );
			}
			continue;
		}
		if (gblisTMB != gblTMBBankCD && gblTransSMART == 1) {
			var transferAmt = "";
			var fee = "";
			if (transloop > 1) {
				transferAmt = gblsplitAmt[i];
				/*if(kony.string.containsChars(transferAmt, ["."]))
					transferAmt=transferAmt;
				else
					transferAmt=transferAmt;*/
				fee = gblsplitFee[i] + ".00";
			} else {
				transferAmt = frmTransferLanding.txtTranLandAmt.text;
				if (kony.string.containsChars(transferAmt, ["."]))
					transferAmt = transferAmt;
				else
					transferAmt = transferAmt + ".00";
				fee = removeSquareBrackets(frmTransferConfirm.lblTransCnfmTotFeeVal.text);
				
				if(fee != null && fee != undefined){
				  var indexOfB = fee.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht"));
				  fee = parseFloat(fee.substring(0, indexOfB));
				}
				
			}
			checkBillPaymentAddMB(transferAmt, fee);
		} else if (gblisTMB != gblTMBBankCD && gblTrasORFT == 1) {
			var ActionFlag = "";
			if (i > 0)
				ActionFlag = "Y"
			else
				ActionFlag = "N"
			var amt = "";
			if (transloop > 1) {
				amt = gblsplitAmt[i];
				/*if(kony.string.containsChars(amt, ["."]))
					amt=amt;
				else
					amt=amt;*/
			} else {
				amt = frmTransferLanding.txtTranLandAmt.text
				if (kony.string.containsChars(amt, ["."]))
					amt = amt;
				else
					amt = amt + ".00";
			}
			checkORFTTransferAddMB(amt, ActionFlag);
		} else {
			checkTransferAddMB();
		}
		
		
	}//for
	//checkNotificationAddMB()
}





/*************************************************************************

	Module	: makeAnotherTransfer
	Author  : Kony
	Purpose : making one more transaction

****/

function makeAnotherTransfer() {
	showLoadingScreen();
	var inputParam = {}
	TMBUtil.DestroyForm(frmTransferLanding);
	TMBUtil.DestroyForm(frmTransferConfirm);
	cleanUpGlobalVariableTransfersMB();
	isMenuShown = false;
	gblRetryCountRequestOTP = "0";
	gblPaynow = true;
	GBLFINANACIALACTIVITYLOG = {}
	inputParam["transferFlag"] = "true";
	destroyMenuSpa();
	recipientAddFromTransfer=false;
	gblTransferFromRecipient=false;
	gblfromCalender =false;
	glb_accId = "";
	invokeServiceSecureAsync("customerAccountInquiry", inputParam, callBackTransferFromAccountsFromSmry)
}

function transferFromMenu(){
	recipientAddFromTransfer=false;
	gblTransferFromRecipient=false;
	gblfromCalender =false;
	glb_accId="";
	ResetTransferHomePage();
	showPreTransferLandingPage();
	//getTransferFromAccounts();
}

function showPreTransferLandingPage(){
	if (kony.application.getCurrentForm().id == "frmPreTransferMB")
	{
		isMenuShown = false;
		frmPreTransferMB.scrollboxMain.scrollToEnd();
	}else{
		frmPreTransferMB.show();
	}
	dismissLoadingScreen();
}
/*************************************************************************

	Module	: getTransferFeeMB
	Author  : Kony
	Purpose : getting transfer Fee for Orft and Smart

****/

function getTransferFeeMB() {
	if(gblPaynow){
		frmTransferLanding.lblSchedSel2.setVisibility(false);
		frmTransferLanding.hbxRecievedBy.setVisibility(true);
	}else{
		frmTransferLanding.lblSchedSel2.setVisibility(true);
		frmTransferLanding.hbxRecievedBy.setVisibility(false);
	}
	if(gblisTMB == gblTMBBankCD){
		return;
	}
	var inputParam = {}
	var fromData;
	var amt = frmTransferLanding.txtTranLandAmt.text;
	amt = kony.string.replace(amt, ",", "")
	var fromAcctSelIndx = gbltranFromSelIndex[1]
	
	fromData = frmTransferLanding.segTransFrm.data;
	var prodCode = fromData[fromAcctSelIndx].prodCode;
	var remainingFee = fromData[fromAcctSelIndx].remainingFee;
	if(gblPaynow){
		inputParam["freeTransCnt"] = remainingFee;
	}else{
		inputParam["freeTransCnt"] = "0";
	}
	showLoadingScreen();
	//byPasscall(); //local environment function
	inputParam["Amount"] = amt
	inputParam["prodCode"] = prodCode;		
	invokeServiceSecureAsync("getTransferFee", inputParam, callBackgetTransferFeeMB)

}


/*************************************************************************

	Module	: checkCrmProfileInq
	Author  : Kony
	Purpose : checking for channel limit

**********************************************************************/

function checkCrmProfileInq() {
		var inputParam = {}
 		inputParam["transferFlag"] = "true";
 		var transferAmt = frmTransferLanding.txtTranLandAmt.text;  	
	    transferAmt = transferAmt.replace(/,/g, "");
	    transferAmt = kony.string.replace(transferAmt, kony.i18n.getLocalizedString("currencyThaiBaht"), "")
	    if (kony.string.containsChars(transferAmt, ["."])) transferAmt = transferAmt;
	    else transferAmt = transferAmt + ".00";
	     
	    inputParam["transferAmt"] = transferAmt; //future
		inputParam["gblisTMB"]=gblisTMB;
	    if (gblisTMB == gblTMBBankCD) {
			inputParam["gblTMBBankCD"]=gblTMBBankCD;
		}
			
			inputParam["gblPaynow"] = gblPaynow;
			
			var i = gbltranFromSelIndex[1];
			
			fromData = frmTransferLanding.segTransFrm.data;
			
			var fee="";	
			if (gblTransSMART == 1) {
				inputParam["gblTransSMART"]=gblTransSMART;
				activityTypeID = "025"
				activityFlexValues5 = "SMART"
				var txtFee = frmTransferLanding.btnTransLndSmart.text;
				if(fee != null && fee != undefined){
				  fee = txtFee.split(" ")[0];
				}
				
			} else if (gblTrasORFT == 1) {
			 	inputParam["gblTrasORFT"]=gblTrasORFT;
				activityTypeID = "025"
				activityFlexValues5 = "ORFT"
				var txtFee = frmTransferLanding.btnTransLndORFT.text;
				if(fee != null && fee != undefined){
				  fee = txtFee.split(" ")[0];
				}
				
			} else {
				activityTypeID = "024"
				activityFlexValues5 = ""
				fee = 0
			}
			var frmID = fromData[i].lblActNoval;
			var frmAccnt = replaceCommon(frmID, "-", "");
			inputParam["frmAccnt"]=frmAccnt;
			var toNum = frmTransferConfirm.lblHiddenToAccount.text;
			toNum = replaceCommon(toNum, "-", "");
			inputParam["toNum"]=toNum;
			var activityFlexValues3= getBankShortName(gblisTMB);   
			inputParam["bankRef"]=activityFlexValues3;
			inputParam["FuturetransferDate"] = changeDateFormatForService(gblStartBPDate);
			inputParam["fee"] = fee;
			
		inputParam["remainingFree"] = fromData[i].remainingFee;
		var transferData = ""; 
		var mobileOrCI = "";
		if(gblSelTransferMode == 2){
			mobileOrCI = "02";
			inputParam["P2P"] = removeHyphenIB(frmTransferLanding.txtOnUsMobileNo.text);
			transferData = frmAccnt+"|"+toNum+"|"+transferAmt+"|"+removeHyphenIB(frmTransferLanding.txtOnUsMobileNo.text);
		} else if(gblSelTransferMode == 3){
			mobileOrCI = "01";
			inputParam["P2P"] = removeHyphenIB(frmTransferLanding.txtCitizenID.text);
			transferData = frmAccnt+"|"+toNum+"|"+transferAmt+"|"+removeHyphenIB(frmTransferLanding.txtCitizenID.text);
		} else {
			inputParam["P2P"] = "";
			transferData = frmAccnt+"|"+toNum+"|"+transferAmt+"|"+"";
		}
		
		inputParam["mobileOrCI"] = mobileOrCI;	
		inputParam["encryptedData"] = encryptDPUsingE2EE(transferData);	
		inputParam["bankShortName"] = getBankShortName(gblisTMB);
		showLoadingScreen()
		invokeServiceSecureAsync("crmProfileInq", inputParam, callBackCrmProfileInq)
}
/*************************************************************************

	Module	: checkOrftAccountInq
	Author  : Kony
	Purpose : checking for channel limit

****/

function checkOrftAccountInq(amt) {
 	var inputParam = {}
	var fromData = "";
 	var i = gbltranFromSelIndex[1]
	fromData = frmTransferLanding.segTransFrm.data;
	var frmID = fromData[i].lblActNoval;
 	var fromAcctID = kony.string.replace(frmID,"-", "");
	var toAcctID  = frmTransferConfirm.lblHiddenToAccount.text;
	toAcctID = kony.string.replace(toAcctID,"-", "");
	if(amt != ""){
			if (kony.string.containsChars(amt, ["."]))
				amt = amt;
			else
				amt = amt + ".00";
					
			amt = parseFloat(amt);
			amt = amt.toFixed(2);
			inputParam["transferAmt"] = amt;
	}
	
	inputParam["fromAcctNo"] = fromAcctID
	inputParam["toAcctNo"] = toAcctID
	inputParam["toFIIdent"] = gblisTMB;		
	inputParam["AccountName"] = gblSmartAccountName;
	showLoadingScreen();
	
	invokeServiceSecureAsync("ORFTInq", inputParam, callBackOrftAccountInq)
 	
}
/*************************************************************************

	Module	: checkDepositAccountInq
	Author  : Kony
	Purpose : checking for TMB account Name

****/

function checkDepositAccountInq() {
 		var inputParam = {}
 		var toAcctID = frmTransferConfirm.lblHiddenToAccount.text;
		toAcctID = kony.string.replace(toAcctID, "-", "")
 		
		inputParam["acctId"] = toAcctID;
		//inputParam["acctType"] = accountType;
 		showLoadingScreen();
		invokeServiceSecureAsync("depositAccountInquiryNonSec", inputParam, callBackDepositAccountInq)
}
/*************************************************************************

	Module	: checkAccountWithdrawInq
	Author  : Kony
	Purpose : checking accountwithdrawInq

****************************************************************************/

function checkAccountWithdrawInq() {
 		var inputParam = {}	
		var fromAcctID;
 		var maturityDate = gblTDTransfer;
 		 
		var dateTD = maturityDate.substr(0, 2);
		var monthTD = maturityDate.substr(3, 2);
		var yearTD = maturityDate.substr(6, 4);
		 
		//frmTransferConfirm.hbxTransCnfmBalAftr.setVisibility(true);
		frmTransferConfirm.lblTransCnfmTotFeeVal.text = dateTD + "/" + monthTD + "/" + yearTD;
		var i = gbltranFromSelIndex[1]
		var fromData = frmTransferLanding.segTransFrm.data;
		 
		fromAcctID = fromData[i].lblActNoval;
		var fromFIIdent = fromData[i].fromFIIdent;
	    var depositeNo = gblTDDeposit;
		fromAcctID = kony.string.replace(fromAcctID, "-", "")
 		if (fromAcctID.length == 14) {
			fromAcctID = fromAcctID.substr(4, 10);
		}
		
		if (depositeNo.length == 1) {
			depositeNo = "00" + depositeNo;
		} else if (depositeNo.length == 2) {
			depositeNo = "0" + depositeNo;
		}
		
		var accno = fromAcctID + depositeNo;

 		var amt = frmTransferLanding.txtTranLandAmt.text + ""
 		amt = kony.string.replace(amt,kony.i18n.getLocalizedString("currencyThaiBaht"), "");
 		amt = kony.string.replace(amt, "," , "");
 		
		amt = amt.replace(/ /g, "");
		amt = amt + "";
        if (kony.string.containsChars(amt, ["."]))
		  	amt = amt;
		else
			amt = amt + ".00";
			 
 		showLoadingScreen();
		
 		inputParam["fIIdent"] = fromFIIdent;	
 		inputParam["fromAcctNo"] = accno;			
		inputParam["withdrawlAmt"] = amt;
		invokeServiceSecureAsync("AccountWithdrawInq", inputParam, callBackAccountWithdrawInq)
}

/*************************************************************************
 	Module	: checkTDBusinessHoursMB
	Author  : Kony
	Purpose : checkTDBusinessHoursMB

****/

function checkTDBusinessHoursMB() {
	
	var inputParam = {}
	invokeServiceSecureAsync("checkTDBussinessHours", inputParam, callBackTDBussinessHours)
}


/*************************************************************************
 	Module	: checkVerifyPWDMB
	Author  : Kony
	Purpose : updating channel limit of Crmprofile
 ************************************************************/

function checkVerifyPWDMB() {
	if (gblCRMProfileData[0]["mbFlowStatusIdRs"] == "03") {
		//alert("" + kony.i18n.getLocalizedString("keyErrTxnPasslock"));
        popTransferConfirmOTPLock.show();
		return false;
	} else {
			verifyTransferMB();
 	}
}

/*************************************************************************
 	Module	: checkFundTransferInqMB
	Author  : Kony
	Purpose : getting fee for if ToAccount is TMB
 *****************************************************************************/

function checkFundTransferInqMB() {
		var inputParam = {}
    	var fromData;
		var depositeNo; 
		var fromAcctID; 
		var toAcctID;		
 		var k=3;
		var i = gbltranFromSelIndex[1]
		
		fromData = frmTransferLanding.segTransFrm.data;
		fromAcctID = fromData[i].lblActNoval;
		fromAcctID = kony.string.replace(fromAcctID,"-","");
 		if(fromAcctID.length==14)
			k=7;
 		 if (fromAcctID[k] == "3") {
			depositeNo = gblTDDeposit;			
		}
 		//inputParam["fromAcctTypeValue"] = gbltdFlag;
		//inputParam["toAcctTypeValue"] = toaccountType;
		toAcctID= frmTransferConfirm.lblHiddenToAccount.text;
		toAcctID = kony.string.replace(toAcctID,"-","");
		
		var amt = frmTransferLanding.txtTranLandAmt.text + "";
		
		amt = parseFloat(removeCommos(amt)).toFixed(2);
		
		inputParam["transferAmt"] = amt
		inputParam["transferDate"] = gblTransferDate  
		inputParam["depositeNo"]=depositeNo;
		inputParam["fromAcctNo"] = fromAcctID
 		inputParam["toAcctNo"] = toAcctID
		showLoadingScreen();
		invokeServiceSecureAsync("fundTransferInq", inputParam, callBackFundTransferInqMB)
}



/*************************************************************************

	Module	: validateTransferFields
	Author  : Kony
	Purpose : validation on Tranfer Fields

***************************************************************************/

function validateTransferFields() {
		if(frmTransferLanding.hboxTD.isVisible){
			if(frmTransferLanding.lblSelectMaturity.isVisible){
				showAlert(kony.i18n.getLocalizedString("keySelectMaturityDate"), kony.i18n.getLocalizedString("info"));
				return false;
			}
		}
		if(frmTransferLanding.hbxPleaseSelectBank.isVisible){
			showAlert(kony.i18n.getLocalizedString("keySelectBank"), kony.i18n.getLocalizedString("info"));
			return false;
		}
		//if (frmTransferLanding.hbxRecipientNumberAndName.isVisible ){
		if (frmTransferLanding.tbxAccountNumber.isVisible ){
			var i = gbltranFromSelIndex[1];
			var fromData = frmTransferLanding.segTransFrm.data;
			
			var selectedFrmAccntProdCde = fromData[i].prodCode;
			if(selectedFrmAccntProdCde == "206"){
				showAlertWithCallBack(kony.i18n.getLocalizedString("linkedAccNotfound"), 
					kony.i18n.getLocalizedString("info"), callBackAccountNumberField);
				return false;
			}
			if( frmTransferLanding.tbxAccountNumber.text == "" ){
				showAlertWithCallBack(kony.i18n.getLocalizedString("TRErr_ToAcc"), 
					kony.i18n.getLocalizedString("info"), callBackAccountNumberField);
				return false;
			}else{
			
				MAX_ACC_LEN_LIST = getBankMaxLengths(gblisTMB);
				var enteredAccountLength =  kony.string.replace(frmTransferLanding.tbxAccountNumber.text,"-","").length;
				
				var  lenghtsList= MAX_ACC_LEN_LIST.split(",");
				var minimumLength = parseInt(lenghtsList[0]);
				if (MAX_ACC_LEN_LIST.indexOf(enteredAccountLength) < 0 || enteredAccountLength < minimumLength) {
					popUpBankList.dismiss();
					showAlertWithCallBack(kony.i18n.getLocalizedString("keyErrAccntNoInvalid"), 
						kony.i18n.getLocalizedString("info"), callBackWrongAccountNumber);
					return false;
				}
				
				if(isValidateFromToAccount(frmTransferLanding.tbxAccountNumber.text) == false){
					showAlertWithCallBack(kony.i18n.getLocalizedString("TRErr_ToAccNotEligible"), 
						kony.i18n.getLocalizedString("info"), callBackAccountNumberField);
					return false;
				} 		
			}
		} else if (frmTransferLanding.lblTranLandToName.text == "") {
			var i = gbltranFromSelIndex[1];
			var fromData = frmTransferLanding.segTransFrm.data;
			var selectedFrmAccntProdCde = fromData[i].prodCode;
			if(selectedFrmAccntProdCde == "206"){
				showAlertWithCallBack(kony.i18n.getLocalizedString("linkedAccNotfound"), 
					kony.i18n.getLocalizedString("info"), callBackAccountNumberField);
			}else{
				showAlertWithCallBack(kony.i18n.getLocalizedString("TRErr_ToAcc"), 
					kony.i18n.getLocalizedString("info"), callBackAccountNumberField);
			}
			return false;
		}else{	
			if(isValidateFromToAccount(frmTransferLanding.lblTranLandToAccountNumber.text) == false){
				showAlertWithCallBack(kony.i18n.getLocalizedString("TRErr_ToAccNotEligible"), 
						kony.i18n.getLocalizedString("info"), callBackAccountNumberField);
				return false;
			} 	
		}
		if (!isNotBlank(frmTransferLanding.txtTranLandAmt.text)) {
			showAlertWithCallBack(kony.i18n.getLocalizedString("keyPleaseEnterAmount"), 
				kony.i18n.getLocalizedString("info"), callBackAmountFields);
			return false;
		}
		//MIB-4884-Allow special characters for My Note and Note to recipient field
		/*
		if (isNotBlank(frmTransferLanding.txtTranLandMyNote.text)){
			if(!MyNoteValid(frmTransferLanding.txtTranLandMyNote.text)){	
				showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_TRkeyMynoteInvalid"), 
					kony.i18n.getLocalizedString("info"), callBackMyNoteFields);
				return false;
			}		
		}
		*/
		
		if (isNotBlank(frmTransferLanding.txtTranLandMyNote.text)){
			if(checkSpecialCharMyNote(frmTransferLanding.txtTranLandMyNote.text)){	
				showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_MyNoteInvalidSpecialChar"), 
					kony.i18n.getLocalizedString("info"), callBackMyNoteFields);
				return false;
			}		
		}
		
		if (gblisTMB != gblTMBBankCD && gblTrasORFT == 0 && gblTransSMART == 0) {
			showAlert(kony.i18n.getLocalizedString("keyPleaseSelectTransferType"), kony.i18n.getLocalizedString("info"));
			if(!frmTransferLanding.hbxTransLndFee.isVisible){
				frmTransferLanding.txtTranLandAmt.text = numberWithCommas(fixedToTwoDecimal(enteredAmount));
				getTransferFeeMB();
			}
			return false;
		}
		if(!gblPaynow &&  gblTransSMART == true && gblScheduleRepeatBP == "Daily"){
		   showAlert(kony.i18n.getLocalizedString("keyFTSmartDailyNotAllowed"), kony.i18n.getLocalizedString("info"));
	       return false;
		}
		if(frmTransferLanding.hbxTranLandNotifyRec.isVisible){
			if (gblTransEmail == 1) {
				if (gblTrasSMS == 1) {
					return true;
				} else {
					var emailBlankChk = frmTransferLanding.txtTransLndSmsNEmail.text;
					var isValidEmail = validateEmail(frmTransferLanding.txtTransLndSmsNEmail.text);
					
					if (!isNotBlank(emailBlankChk)){
						showAlertWithCallBack(kony.i18n.getLocalizedString("transferEmailID"), 
							kony.i18n.getLocalizedString("info"), callBackTransLndSmsNEmailFields);
						return false;
					}
					if (isValidEmail == false) {
						showAlertWithCallBack(kony.i18n.getLocalizedString("keyPleaseEnterValidEMAILID"), 
							kony.i18n.getLocalizedString("info"), callBackTransLndSmsNEmailFields);
						return false;
					}
					
					if(frmTransferLanding.textRecNoteEmail.text == ""){
						showAlertWithCallBack(kony.i18n.getLocalizedString("keyPleaseEnterRcipientNote"), 
							kony.i18n.getLocalizedString("info"), callBankRecNoteEmailFields);
						return false;
					}
					//MIB-4884-Allow special characters for My Note and Note to recipient field
					/*
					if (isNotBlank(frmTransferLanding.textRecNoteEmail.text)){
						if(!MyNoteValid(frmTransferLanding.textRecNoteEmail.text)){		
							showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_TRkeyRecNoteInvalid"), 
								kony.i18n.getLocalizedString("info"), callBankRecNoteEmailFields);
							return false;
						}		
					}
					*/
					if (isNotBlank(frmTransferLanding.textRecNoteEmail.text)){
						if(checkSpecialCharMyNote(frmTransferLanding.textRecNoteEmail.text)){		
							showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_MyNoteInvalidSpecialChar"), 
								kony.i18n.getLocalizedString("info"), callBankRecNoteEmailFields);
							return false;
						}		
					}
				}
			} else if (gblTrasSMS == 1) {
				if (gblTransEmail == 1) {
					return true;
				} else {
					var txtLen = removeHyphenIB(frmTransferLanding.txtTransLndSms.text);
					if (!isNotBlank(txtLen)) {
						showAlertWithCallBack(kony.i18n.getLocalizedString("keyEnterMobileNum"), 
							kony.i18n.getLocalizedString("info"), callBackTransLndSmsFields);
						return false;
					}
					var isNum = "";
					if (txtLen != null && txtLen != undefined && txtLen.length > 2){
						isNum = txtLen[0] + txtLen[1];
					}
					if ((txtLen != null && txtLen != undefined) && (txtLen.length != 10 || (Gbl_StartDigsMobileNum.indexOf(isNum) < 0))) {
						showAlertWithCallBack(kony.i18n.getLocalizedString("keyEnteredMobileNumberisnotvalid"), 
							kony.i18n.getLocalizedString("info"), callBackTransLndSmsFields);
						return false;
					}
					if(frmTransferLanding.txtTranLandRecNote.text==""){
						showAlertWithCallBack(kony.i18n.getLocalizedString("keyPleaseEnterRcipientNote"), 
							kony.i18n.getLocalizedString("info"), callBackTranLandRecNoteFields);
						return false;
					}
					//MIB-4884-Allow special characters for My Note and Note to recipient field
					/*
					if (isNotBlank(frmTransferLanding.txtTranLandRecNote.text)){
						if(!MyNoteValid(frmTransferLanding.txtTranLandRecNote.text)){		
							showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_TRkeyRecNoteInvalid"), 
								kony.i18n.getLocalizedString("info"), callBackTranLandRecNoteFields);
							return false;
						}		
					}
					*/
					if (isNotBlank(frmTransferLanding.txtTranLandRecNote.text)){
						if(checkSpecialCharMyNote(frmTransferLanding.txtTranLandRecNote.text)){		
							showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_MyNoteInvalidSpecialChar"), 
								kony.i18n.getLocalizedString("info"), callBackTranLandRecNoteFields);
							return false;
						}		
					}
				}
			}
		}
 		return true;
		//}
}


/*************************************************************************

	Module	: checkTransferTypeMB
	Author  : Kony
	Purpose : checking for whether transfer type is split or normal

***************************************************************************/

function checkTransferTypeMB() {
 	var transORFTSplitAmnt = parseFloat(gblLimitORFTPerTransaction)
	var transSMARTSplitAmnt = parseFloat(gblLimitSMARTPerTransaction);
 	var transferAmt = frmTransferLanding.txtTranLandAmt.text;
 	transferAmt = kony.string.replace(transferAmt, ",", "");
	if (gblisTMB != gblTMBBankCD && transferAmt > transORFTSplitAmnt && gblTrasORFT == 1) {
		splitAmount(transferAmt);
		showAlertForSplitTransfers(kony.i18n.getLocalizedString("keyYourAmountexcededthechannellimit"), constants.ALERT_TYPE_CONFIRMATION,
			"", kony.i18n.getLocalizedString("keyYes"), kony.i18n.getLocalizedString("keyNo"), splitingTransaction)
	} else if (gblisTMB != gblTMBBankCD && transferAmt > transSMARTSplitAmnt && gblTransSMART == 1) {
		
		splitAmount(transferAmt);
		showAlertForSplitTransfers(kony.i18n.getLocalizedString("keyYourAmountexcededthechannellimit"), constants.ALERT_TYPE_CONFIRMATION,
			"", kony.i18n.getLocalizedString("keyYes"), kony.i18n.getLocalizedString("keyNo"), splitingTransaction)
	}
  	else {
		normalTransaction(true);
 	}
}
/*************************************************************************

	Module	: cleanUpGlobalVariableTransfersMB
	Author  : Kony
	Purpose : cleaning Up all global variable for Transfer Now MB

***************************************************************************/

function cleanUpGlobalVariableTransfersMB() {
	gbltdFlag = "";
	gblisTMB = "";
	gblfromCalender =false;
	gblTransEmail = 0;
	gbltranFromSelIndex = [0, 0];
	gblTransferRefNo = "";
	gblTransferToRecipientCache = [];
	gblTransferToRecipientData = [];
	gblTrasSMS = 0;
	gblLimitSMARTPerTransaction = "";
	gblLimitORFTPerTransaction = "";
	gblSplitAckImg = [];
	gblsplitAmt = [];
	gblsplitFee = [];
	gblSplitCnt = 0;
	gblSplitStatusInfo = [];
	gblTransferDate = "";
	gblCRMProfileData = [];
 	GBLFINANACIALACTIVITYLOG = {};
	gblXferRecImg = "";
	gblFundXferData = []
	gblBalAfterXfer = "";
	gblBANKREF="";
	gblXerSplitData=[];
	gblTransSMART = 0;
	gblTrasORFT = 0;
	Recp_category = 0;
}
/*************************************************************************

	Module	: showTransPwdPopupForTransfers
	Author  : Kony
	Purpose : display to enter txn pwd

***************************************************************************/

function showTransPwdPopupForTransfers() {

		if(gblisTMB == gblTMBBankCD && ((gblTransEmail == 1 && gblTrasSMS == 1) || isOwnAccountP2P)){
			verifyTransferMB();
		}else{
			gblVerifyOTP = 0;
			var tranPWD = kony.i18n.getLocalizedString("transPasswordSub");
			//showOTPPopup(tranPWD , "", "", onClickConfirmPop, 3);
			showOTPSSLPopup(onClickConfirmPop);
		}
	
}

/**
 * For SPA
 */

	
function execXferServiceParamsInSessionSPA(amount, accName, accNum, bankName, module) {
  /*  inputParam = {};
	inputParam["amount"] = amount;
	inputParam["accountName"] = accName;
	inputParam["accountNum"] = accNum;
	inputParam["bankName"] = bankName;
	inputParam["module"] = module;
	
	invokeServiceSecureAsync("ExecuteTransferValidationService", inputParam, execXferServiceParamsSPACallBack);*/
	var locale = kony.i18n.getCurrentLocale();    
			if (gblIBFlowStatus == "04") {
				//showAlert(kony.i18n.getLocalizedString("Receipent_OTPLocked"), kony.i18n.getLocalizedString("info"));
				popTransferConfirmOTPLock.show();
				
				//return false;
			}
			else {
				var inputParams = {}
				spaChnage="transfers"
				gblOTPFlag = true;
				var toAccountNumber = frmTransferConfirm.lblHiddenToAccount.text
				toAccountNumber = replaceCommon(toAccountNumber, "-", "");
				var gblToTDFlag = false;
                if (toAccountNumber[3] == "3" && gblisTMB == gblTMBBankCD) { //to check To account is TD for eventnotificationpolocy
                    gblToTDFlag = true
                } 
				try {
					kony.timer.cancel("otpTimer")
				}
				catch (e) {
					
				}
				if (gbltdFlag == "CDA") {
					gblSpaChannel = "WithdrawTD";
					//SpaSMSSubject = "MIB_WithdrawTD_"
					//SpaEventNotificationPolicy = "MIB_WithdrawTD_"
				}
				else {
					if(gblPaynow){
						if(gblToTDFlag){
							gblSpaChannel = "DepositTD"
						}else{
						gblSpaChannel = "ExecuteTransfer";
						}
					}else{
						gblSpaChannel = "FutureThirdPartyTransfer";
					}
					//SpaSMSSubject = "MIB_iORFT_"
					//SpaEventNotificationPolicy = "MIB_iORFT_"
				}
				onClickOTPRequestSpa();
			}
}




function onClickCoverFlowTransfersMB() {
	gbltranFromSelIndex = frmTransferLanding.segTransFrm.selectedIndex;
	gbltdFlag = frmTransferLanding.segTransFrm.selectedItems[0].tdFlag;
	gblSelTransferFromAcctNo = frmTransferLanding.segTransFrm.selectedItems[0].accountNum;
	//toggleTDMaturityCombobox(gbltdFlag);
}

function grtFormattedAmount(amountStr) {
	if (amountStr.indexOf(".") > 0) {
		var amountFormat = amountStr.split(".");
		if (amountFormat.length > 1) {
			if (amountFormat[1].length == 1) {
				amountStr = amountFormat[1] + "0";
			}
		} else {
			amountStr = amountFormat[0] + "00";
		}
	} else {
		amountStr = amountStr + ".00"
	}
	return amountStr;
}
function getTransferIndex() {}
	getTransferIndex.prototype.paginationSwitch = function (sectionIndex, rowIndex) {
	if (isNotBlank(gbltdFlag) && gbltdFlag == kony.i18n.getLocalizedString("termDeposit")) {
			frmTransferLanding.txtTranLandAmt.text = "";//Redesign transfer clearing amount if previous from account is TD
	}
	var segdata = frmTransferLanding.segTransFrm.data;
	gbltdFlag = segdata[rowIndex]["tdFlag"];
	rowIndex = parseFloat(rowIndex);
	frmTransferLanding.segTransFrm.selectedIndex = [0, rowIndex];
	gbltranFromSelIndex = frmTransferLanding.segTransFrm.selectedIndex;
	gblSelTransferFromAcctNo = frmTransferLanding.segTransFrm.selectedItems[0].accountNum;
 	toggleTDMaturityCombobox(gbltdFlag);
 	resetTransferLandingBasedOnToAccount();
}

function depositAccInqDreamSavMB() {
	var inputparam = {};
	var fromData;
	var i = gbltranFromSelIndex[1]
	fromData = frmTransferLanding.segTransFrm.data;
	var accId = fromData[i].lblActNoval
    accId =kony.string.replace(accId, "-", "");
  	inputparam["acctId"] =accId;
  	showLoadingScreen();
  	invokeServiceSecureAsync("depositAccountInquiry", inputparam, depositAccInqDreamSavCallBackMB);
}

function depositAccInqDreamSavCallBackMB (status, resulttable) {
   	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
           if (resulttable["linkedacc"].length == 0) {
             	gblXferLnkdAccnts="";
             	frmTransferLanding.tbxAccountNumber.text="";//kony.i18n.getLocalizedString("lblNotFound");
			 	frmTransferLanding.vbxSelRecipient.onClick = onclickGetRecipientsDreamSavings;//for dream saving acnts
             	enteringAccountNumberVisbilty(true);
             	showAlert(kony.i18n.getLocalizedString("linkedAccNotfound"), kony.i18n.getLocalizedString("info"));
           
           }else {
				 var strlen = resulttable["linkedacc"].length - 10;
				 var linkAccount = resulttable["linkedacc"].substr(strlen,resulttable["linkedacc"].length)                  		
				 gblXferLnkdAccnts = linkAccount;
          		 var accountName = "";
				 var icon=gblMyProfilepic;
				
				for(var l=0;l<gblAccountTable["custAcctRec"].length;l++){
					
					if(gblXferLnkdAccnts==gblAccountTable["custAcctRec"][l]["accId"] 
						|| ("0000"+gblXferLnkdAccnts)==(gblAccountTable["custAcctRec"][l]["accId"])){
						
						accountName = gblAccountTable["custAcctRec"][l]["acctNickName"]
						if (accountName == "" || accountName == undefined) {
							var acctnum = gblXferLnkdAccnts
							acctnum = acctnum.substr(6, 4)
							accountName = gblAccountTable["custAcctRec"][l]["ProductNameEng"] + " " + acctnum
						}
					}
	 					
				}//for
          		 	
          		gblisTMB = gblTMBBankCD;
          		assignBankSelectDetails(gblisTMB);
          		frmTransferLanding.lblTranLandToName.text = accountName;
          		frmTransferLanding.lblTranLandToAccountNumber.text = addHyphenMB(gblXferLnkdAccnts);
          		frmTransferLanding.vbxSelRecipient.onClick = onclickGetRecipientsDreamSavings;
				enteringAccountNumberVisbilty(false);
				if(frmTransferLanding.hbxTranLandMyNote.isVisible){
					frmTransferLanding.lineRecipientNote.setVisibility(true);
				}else{
					frmTransferLanding.lineRecipientNote.setVisibility(false);
				}
				frmTransferLanding.lineMyNote.setVisibility(false);
				frmTransferLanding.hbxTranLandNotifyRec.setVisibility(false);
				frmTransferLanding.lineNotifyRecipientButton.setVisibility(false);
				
				frmTransferLanding.txtTransLndSms.setVisibility(false);
				frmTransferLanding.txtTransLndSmsNEmail.setVisibility(false);
				frmTransferLanding.lineMobileEmailTextBox.setVisibility(false);
				
				frmTransferLanding.hbxTranLandRecNote.setVisibility(false);
				frmTransferLanding.hbxRecNoteEmail.setVisibility(false);
				frmTransferLanding.hbxTransLndFee.setVisibility(false);
				frmTransferLanding.hbxFeeTransfer.setVisibility(true);
				gblTrasSMS = 1;
				gblTransEmail = 1;
				gblTrasORFT = 0;
				gblTransSMART = 0;
				transAmountOnDone(false,true); //Amount auto populate so we need to call calculate fee service.
				
          		//gblisTMB = 11;
          		if(accountName == ""){
          		  	 dismissLoadingScreen();
					 accountName = "";
					 enteringAccountNumberVisbilty(true);
					 frmTransferLanding.tbxAccountNumber.text = "";
				 	 showAlert(kony.i18n.getLocalizedString("linkedAccNotfound"), kony.i18n.getLocalizedString("info"));
				}
			}  
            dismissLoadingScreen();
        }else {
			if(resulttable["errMsg"] != undefined){
        		alert(resulttable["errMsg"]);
        	}else{
        		alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
        	}   
        	dismissLoadingScreen();
         }
  	}
	dismissLoadingScreen();
}
function getFeeforFutureTrans(){
	if(gblPaynow){
		frmTransferLanding.lblSchedSel2.setVisibility(false);
		frmTransferLanding.hbxRecievedBy.setVisibility(true);
	}else{
		frmTransferLanding.lblSchedSel2.setVisibility(true);
		frmTransferLanding.hbxRecievedBy.setVisibility(false);
	}
	if(gblisTMB == gblTMBBankCD){
		return;
	}
	var inputParam = {}
	var fromData;
	var amt = frmTransferLanding.txtTranLandAmt.text;
	
	var i = gbltranFromSelIndex[1]
	
	fromData = frmTransferLanding.segTransFrm.data;
	//amt = kony.string.replace(amt, ",", "")
	amt = kony.string.replace(amt, ",", "");
	var prodCode = fromData[i].prodCode;
	var remainingFee = fromData[i].remainingFee;
	showLoadingScreen();
	inputParam["Amount"] = amt
	inputParam["prodCode"] = prodCode;
	inputParam["freeTransCnt"] = "0";
	
	//adding this bypass call for local qa testing for SPA
	invokeServiceSecureAsync("getTransferFee", inputParam, callBackgetTransferFeeMB)
}

function getToRecipientsNewMB(prod,acc) {
		gblTransferToRecipientData = [];
		gblTransferToRecipientCache = [];
		//startDisplaySelectbankMB();
		frmTranfersToRecipents.btnSearchRecipeints.setEnabled(false);
		frmTranfersToRecipents.lblMsg.setVisibility(false);
		var inputParam = {}
		//inputParam["crmId"] = gblcrmId;
		inputParam["prodId"]=prod;
		inputParam["accId"]=acc;
		/** input params are crmid,personalizeID - Assumption this will handled at session level ***/
		showLoadingScreen()
		
		invokeServiceSecureAsync("getRecipientsForTransfer", inputParam, callBackTransferToReciepientsNewMB)
	
	}
	
	gblRcAllAccountsData=[]
	gblOwnRcData=[]
function callBackTransferToReciepientsNewMB(status, resulttable) {
 	if (status == 400) //success responce
	{
 		popUpBankList.dismiss();
		if ((resulttable["OwnAccounts"].length>0 || resulttable["RecepientsList"].length >0)) {
			selectedRow=0;
			gblRcAllAccountsData=resulttable["AccountsList"];
			gblOwnRcData=resulttable["OwnAccounts"];
			var mobileimg = "";
			var mobileno = "";
			var fbimg = "";
			var fbno = "";
			var favimg="";
			var temp1 = [];
			var acctLen = resulttable["RecepientsList"].length;
			
			var fromData = "";
			
			fromData = frmTransferLanding.segTransFrm.data;
			var i = gbltranFromSelIndex[1]
			var prodCode = fromData[i].prodCode;
			var linkedAccnt= addHyphenMB(gblXferLnkdAccnts);
    		var frmAccnt = fromData[i].accountNum;
    		frmAccnt = kony.string.replace(frmAccnt, "-", "");
			var len = 0;
			var randomnum = Math.floor((Math.random()*10000)+1); 
			gblMyProfilepic = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=Y&personalizedId=&billerId=&dummy=" +randomnum;
				var mbimg;
				if (gblPHONENUMBER.length == 0 && gblPHONENUMBER == "")
					mbimg = "empty.png"
				else
					mbimg = "mobile2.png";
				var temp = [{
					lblName: gblCustomerName,
					lblAccountNum: resulttable["ownCount"]  + " " + kony.i18n.getLocalizedString("Receipent_Accounts"),
					imgArrow: "navarrow3.png",
					imgprofilepic: gblMyProfilepic,
					imgmob: mbimg,
					imgfav:favimg,
					mobileNo: gblPHONENUMBER,
					//imgfb: fbimg,
					//facebookNo: fbno,
					personalizedMailId: gblEmailId,
					hiddenmain: "own",
					personalizedId: "",
					template: hbrt1
						}]
				kony.table.insert(temp1, temp[0])
		
			var i = gbltranFromSelIndex[1]
			var prodCode = fromData[i].prodCode;
			
			
			if (gbltdFlag != kony.i18n.getLocalizedString("termDeposit") && prodCode != "206") {
				if (acctLen != 0) {
					resulttable.CRMAccountList = resulttable["RecepientsList"];
					for (var i = 0; i < resulttable.CRMAccountList.length; i++) {
						//checking for mobile No exists or not
						if (resulttable.CRMAccountList[i].personalizedMobileNum != null && resulttable.CRMAccountList[i].personalizedMobileNum.trim()!="" && 
							resulttable.CRMAccountList[i].personalizedMobileNum.length > 0) {
							mobileimg = "mobile2.png";
							mobileno = resulttable.CRMAccountList[i].personalizedMobileNum.trim();
						} else {
							mobileimg = "empty.png";
							mobileno = resulttable.CRMAccountList[i].personalizedMobileNum;
						}
						
						if (resulttable.CRMAccountList[i].personalizedFavFlag != null &&
							resulttable.CRMAccountList[i].personalizedFavFlag=="Y") {
							favimg = "starblue.png";
						} else {
							favimg = "starwhite.png";
						}
						//checking for facebook ID exists or not	
						if (resulttable.CRMAccountList[i].personalizedFbookId != null && resulttable.CRMAccountList[i].personalizedFbookId.trim() != "" && 
							resulttable.CRMAccountList[i].personalizedFbookId.length > 0) {
							fbimg = "facebook2.png";
							fbno = resulttable.CRMAccountList[i].personalizedFbookId.trim();
						} else {
							fbimg = "empty.png";
							fbno = resulttable.CRMAccountList[i].personalizedFbookId;
						}
						//profile picture
						var rcImg = "";
						var profilePic = resulttable.CRMAccountList[i].personalizedPicId;
						var XferRcURL ="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext +
							"/ImageRender?billerId=&crmId=Y&personalizedId=";
						var randomnum = Math.floor((Math.random()*10000)+1); 
						if (profilePic == null || profilePic == "") {
							rcImg = XferRcURL + resulttable.CRMAccountList[i].personalizedId +"&rr="+randomnum;
						} else {
							var check = kony.string.endsWith(profilePic + "", "nouserimg.jpg", true)
							if (check == true) {
								rcImg = XferRcURL + resulttable.CRMAccountList[i].personalizedId +"&rr="+randomnum;
							} else {
								var check = kony.string.startsWith(profilePic + "", "http", true)
								if (check == true) {
									rcImg = profilePic;
								} else {
										rcImg = XferRcURL + resulttable.CRMAccountList[i].personalizedId +"&rr="+randomnum;									
								}
							}
						}
						//ToDo imgprofilepic shouldcomple from middleware dynamically.
						if(resulttable.CRMAccountList[i].accountCount==0 && (resulttable.CRMAccountList[i].personalizedMobileNum == null ||
							resulttable.CRMAccountList[i].personalizedMobileNum =="No Number" || resulttable.CRMAccountList[i].personalizedMobileNum =="")){
							//do nothing if mobile and account not present
						}
						//else{
						var temp = [{
							lblName: resulttable.CRMAccountList[i].personalizedName,
							lblAccountNum: resulttable.CRMAccountList[i].accountCount + " " + kony.i18n.getLocalizedString(
								"Receipent_Accounts"),
							imgArrow: "navarrow3.png",
							imgprofilepic: rcImg,
							imgfav:favimg,
							imgmob: mobileimg,
							mobileNo: mobileno,
							imgfb: fbimg,
							facebookNo: fbno,
							personalizedMailId: resulttable.CRMAccountList[i].personalizedEmailId,
							hiddenmain: "main",
							personalizedId: resulttable.CRMAccountList[i].personalizedId,
							template: hbrt1
							}]
						
						kony.table.insert(temp1, temp[0])
						//}
					} //for
				}
			}
			gblTransferToRecipientData = temp1; //caching for 
			gblTransferToRecipientCache = temp1; //caching for ToDo
			frmTranfersToRecipents.segTransferToRecipients.widgetDataMap = {
				lblName: "lblName",
				lblAccountNum: "lblAccountNum",
				imgArrow: "imgArrow",
				imgfav:"imgfav",
				imgprofilepic: "imgprofilepic",
				imgmob: "imgmob",
				imgfb: "imgfb"
			};
			frmTranfersToRecipents.segTransferToRecipients.data = gblTransferToRecipientData;
			// for row selection of segment
			//gblAckFlage="true";
			dismissLoadingScreen();
			frmTranfersToRecipents.lblHdrTxt.text = kony.i18n.getLocalizedString("Transfer_selectRecipient");
			frmTranfersToRecipents.show();
		} else {
			dismissLoadingScreen();
				     showAlert(kony.i18n.getLocalizedString("TRErr_NoEligibleToAcc"), kony.i18n.getLocalizedString("info"));
	    }
	}
}


function getTMBOWNAccountsNewMB() {
	selectedRow=0;
	var tab1 = []
	var indexOfSelectedIndex = frmTranfersToRecipents.segTransferToRecipients.selectedItems[0];
	var mno = indexOfSelectedIndex.mobileNo;
	//var fbno = indexOfSelectedIndex.facebookNo;
	
	//
	var mobileimg = ""
	var fbimg = ""
	if (mno != null && mno.length > 0) {
		mobileimg = "mobi.png"
	} else {
		mobileimg = "empty.png"
	}
	var selectedData = [{
		lblName: indexOfSelectedIndex.lblName,
		lblAccountNum: indexOfSelectedIndex.lblAccountNum,
		imgArrow: "navdownarrow.png",
		imgprofilepic: gblMyProfilepic,
		imgmob: mobileimg,
		mobileNo: indexOfSelectedIndex.mobileNo,
		//imgfb: fbimg,
		//facebookNo: indexOfSelectedIndex.facebookNo,
		personalizedId: indexOfSelectedIndex.personalizedId,
		hiddenmain: "own1",
		template: hbrt5
			}];
	gblXferRecImg = gblMyProfilepic;
	//frmTransferLanding.imgTranLandTo.src = indexOfSelectedIndex.imgprofilepic;
	kony.table.insert(tab1, selectedData[0])
	for (var i = 0; i < gblOwnRcData.length; i++) {
			var fromAcctID = gblOwnRcData[i].acctId;
 			var custName = gblOwnRcData[i].acctNickName;
			var prdCode = gblOwnRcData[i].prdCode;
			if (custName == undefined || custName=="" || custName==null){
  				if (kony.i18n.getCurrentLocale() == "en_US"){
					custName = gblOwnRcData[i].productNmeEN
				}else{
					custName = gblOwnRcData[i].productNameTH
				}
  			}
			
			
			if(prdCode=="otherbank"){
			fromAcctID=encodeAccntNumbers(fromAcctID);
			var pic="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+gblOwnRcData[i].bankCde+"&modIdentifier=BANKICON";//""https://vit.tau2904.com/tmb/Bank-logo/bank-logo-22.jpg
			var bn=getBankNameMB(gblOwnRcData[i].bankCde);
			
			var accountData = {
				img2: pic,
				lbl3: custName,
				lbl4: fromAcctID,
				lbl5: gblOwnRcData[i].bankCde,
				lblBankName: bn,
				img3: "starwhite.png",
				hiddenmain: "sub",
				accountName: gblOwnRcData[i].acctName,
				template: hbrt3
			 }
			}
			else{
			
				if (fromAcctID.length == 14) {
					var frmid = ""
					for (var j = 4; j < 14; j++)
					frmid = frmid + fromAcctID[j];
					fromAcctID = frmid;
				}
				fromAcctID=formatAccountNo(fromAcctID);
			var imageName="avatar_dis.png";
			imageName="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+gblOwnRcData[i]["ICON_ID"]+"&modIdentifier=PRODICON";
			var accountData = {
				img2:imageName,
				lbl3: custName,
				lbl4: fromAcctID,
				lbl5: gblTMBBankCD,
				lblBankName: kony.i18n.getLocalizedString("keyTMBBank"),
				img3: "starwhite.png",
				hiddenmain: "sub",
				accType:gblOwnRcData[i].acctType,
				template: hbrt3
			}
			}
			
			kony.table.insert(tab1, accountData);
		}
	
	var indexOfSelectedRow = frmTranfersToRecipents.segTransferToRecipients.selectedIndex[1];
	var mno = frmTranfersToRecipents.segTransferToRecipients.selectedItems[0].mobileNo;
	//var fbno = indexOfSelectedIndex.facebookNo;
	
	//
	var mobileimg = ""
	var fbimg = ""
	if (mno != null && mno.length > 0) {
		mobileimg = "mobi.png"
	} else {
		mobileimg = "empty.png"
	}
	/**if (fbno != null && fbno.length > 0) {
		fbimg = "facebook4.png"
	}
	else {
		fbimg = "empty.png"
	}**/
	var tempMbFb = [{
			img2: "mobileicotrf.png",
			lbl3: "Mobile",
			lbl4: mno,
			lbl5: "",
			img3: "starwhite.png",
			hiddenmain: "sub",
			lblBankName: "",
			template: hbrt3
			}
			/**, 
			{
				img2: "facebookico.png",
				lbl3: "Facebook",
				lbl4: fbno,
				lbl5: "",
				img3: "starwhite.png",
				hiddenmain: "sub",
				template: hbrt3
			}**/
			]
	/*for (var i = 0; i < tempMbFb.length; i++) {
		if (i == 0) {
			if (mno != null && mno.length > 0)
				kony.table.insert(tab1, tempMbFb[i])
		}
		else
			kony.table.insert(tab1, tempMbFb[i])
	}*/
	frmTranfersToRecipents.segTransferToRecipients.widgetDataMap = {
		lblName: "lblName",
		lblAccountNum: "lblAccountNum",
		imgArrow: "imgArrow",
		imgprofilepic: "imgprofilepic",
		imgmob: "imgmob",
		imgfb: "imgfb",
		hiddenmain: "hiddenmain",
		lbl3: "lbl3",
		lbl4: "lbl4",
		lbl5: "lbl5",
		lblBankName: "lblBankName",
		img2: "img2",
		img3: "img3"
	}
	var tab2 = []
	for (var i = 0; i < indexOfSelectedRow; i++) {
		
		kony.table.insert(tab2, gblTransferToRecipientData[i])
		
	}
	
	//appending selected row and account list of selected row
	kony.table.append(tab2, tab1);
	
	//adding data after selected row of segment
	for (var j = indexOfSelectedRow + 1; j < gblTransferToRecipientData.length; j++) {
		
		kony.table.insert(tab2, gblTransferToRecipientData[j]);
	}
	frmTranfersToRecipents.segTransferToRecipients.removeAll()
	dismissLoadingScreen();
	frmTranfersToRecipents.segTransferToRecipients.setData(tab2);
	
}




function getTransferToAccountsNew() {
	 var resulttable = [];
	var pid= frmTranfersToRecipents["segTransferToRecipients"]["selectedItems"][0]["personalizedId"];
	frmTranfersToRecipents.txbXferSearch.text = "";
	var accounts = [];
	for(var i=0;i<gblRcAllAccountsData.length;i++)
	{
		var ind = "Accounts"+i;
		if(gblRcAllAccountsData[i][ind][0] != undefined || gblRcAllAccountsData[i][ind][0] != null){
			if(pid==gblRcAllAccountsData[i][ind][0].personalizedId){
				accounts=gblRcAllAccountsData[i][ind]
				break;
			}
		}
		else{
		accounts=[];
		}
		
	}
	var indexOfSelectedRow = frmTranfersToRecipents.segTransferToRecipients.selectedIndex[1];
			var dataLen = gblTransferToRecipientData.length;
			var segLen = frmTranfersToRecipents.segTransferToRecipients.data.length;
			//var selectedRow;
 			if (dataLen == segLen) {
				selectedRow = indexOfSelectedRow
			} else if (selectedRow < indexOfSelectedRow) {
				indexOfSelectedRow = indexOfSelectedRow - (segLen - dataLen);
				selectedRow = indexOfSelectedRow;
			} else {
				selectedRow = indexOfSelectedRow;
			}
			var bnkLogoURL = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext +
				"/Bank-logo" + "/bank-logo-";
			
			var indexOfSelectedIndex = frmTranfersToRecipents.segTransferToRecipients.selectedItems[0];
			var mno = indexOfSelectedIndex.mobileNo;
			var fbno = indexOfSelectedIndex.facebookNo;
			
			
			var mobileimg = ""
			var fbimg = ""
			if (mno != undefined && mno != "undefined" && mno != null && mno.length > 0) {
				mobileimg = "mobi.png"
				mno = kony.string.replace(mno, "-", "");
				gblXferPhoneNo1 = mno;
				gblXferEmail1 = indexOfSelectedIndex.personalizedMailId;
			} else {
				mobileimg = "empty.png"
				gblXferPhoneNo1 = "";
				gblXferEmail1 = indexOfSelectedIndex.personalizedMailId;
			}
			if (fbno != undefined && fbno != "undefined" && fbno != null && fbno.length > 0) {
				fbimg = "fb_icon_white.png"
			} else {
				fbimg = "empty.png"
			}
			var selectedData = [{
				lblName: indexOfSelectedIndex.lblName,
				lblAccountNum: indexOfSelectedIndex.lblAccountNum,
				imgArrow: "navdownarrow.png",
				imgprofilepic: indexOfSelectedIndex.imgprofilepic,
				imgmob: mobileimg,
				imgfav: indexOfSelectedIndex.imgfav,
				mobileNo: indexOfSelectedIndex.mobileNo,
				imgfb: fbimg,
				facebookNo: indexOfSelectedIndex.facebookNo,
				personalizedId: indexOfSelectedIndex.personalizedId,
				hiddenmain: "main1",
				template: hbrt5
			}];
			
			gblXferRecImg = indexOfSelectedIndex.imgprofilepic;
			//frmTransferLanding.imgTranLandTo.src = indexOfSelectedIndex.imgprofilepic;
			var temp = []
			var tab1 = []
			kony.table.append(tab1, selectedData)
			
			frmTranfersToRecipents.segTransferToRecipients.widgetDataMap = {
				lblName: "lblName",
				lblAccountNum: "lblAccountNum",
				imgArrow: "imgArrow",
				imgprofilepic: "imgprofilepic",
				imgmob: "imgmob",
				imgfb: "imgfb",
				imgfav:"imgfav",
				hiddenmain: "hiddenmain",
				lbl3: "lbl3",
				lbl4: "lbl4",
				lbl5: "lbl5",
				lblBankName: "lblBankName",
				img2: "img2",
				img3: "img3"
			}
			resulttable.CRMAccountsInq = accounts;
			
			
			for (var i = 0; i < resulttable.CRMAccountsInq.length; i++) {
				var isAccntFav = resulttable.CRMAccountsInq[i]["acctFavFlag"];
				//kony.type(accFavoriteFlag)
				var btnFavSkin;
				if (isAccntFav != null && isAccntFav.length > 0) {
					
					if (isAccntFav.toString() == "y" || isAccntFav.toString() == "Y") {
						btnFavSkin = "starblue.png"
					} else {
						btnFavSkin = "starwhite.png";
					}
				} else {
					btnFavSkin = "starwhite.png";
				}
				
				var bankName = getBankNameMB(resulttable.CRMAccountsInq[i].bankCde)
				
				var acctNo = resulttable.CRMAccountsInq[i].acctId;
				acctNo = kony.string.replace(acctNo, "-", "")
				
				//added code to show account Id 10 digits if the accnt id is 14 digits
				
				 if(resulttable.CRMAccountsInq[i].bankCde=="11"){
				 	if (acctNo.length == 14) {
					var frmid = ""
					for (var j = 4; j < 14; j++)
						frmid = frmid + acctNo[j];
					acctNo = frmid;
 			    }
       				 acctNo=addHyphenIB(acctNo)
      			 }
        		else{
        			acctNo=encodeAccntNumbers(acctNo);
        		 }				
				var bankLogo = "";
				/*if (resulttable.CRMAccountsInq[i].bankCde == 11)
					bankLogo = "icon.png";
				else*/
					bankLogo = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+resulttable.CRMAccountsInq[i].bankCde+"&modIdentifier=BANKICON";
				var accountData = {
					img2: bankLogo,
					lbl3: resulttable.CRMAccountsInq[i].acctNickName,
					lbl4: acctNo,
					lbl5: resulttable.CRMAccountsInq[i].bankCde,
					lblBankName: bankName,
					img3: btnFavSkin,
					hiddenmain: "sub",
				    accountName: resulttable.CRMAccountsInq[i].acctName,					
					template: hbrt3
				}
				
				kony.table.insert(tab1, accountData);
			}
			var tempMbFb = [{
				img2: "mobileicotrf.png",
				lbl3: kony.i18n.getLocalizedString("keymobilespa"),
				lbl4: mno,
				lbl5: "",
				img3: "starwhite.png",
				hiddenmain: "sub",
				lblBankName: "",
				template: hbrt3
			}, {
				img2: "facebookicotrf.png",
				lbl3: "Facebook",
				lbl4: fbno,
				lbl5: "",
				lblBankName: "",
				img3: "starwhite.png",
				hiddenmain: "sub",
				template: hbrt3
			}]
			/*for (var i = 0; i < tempMbFb.length; i++) {
				if (i == 0) {
					if (mno != null && mno.length > 0)
						kony.table.insert(tab1, tempMbFb[i])
				} else if (i == 1) {
					if (fbno != null && fbno.length > 0)
						kony.table.insert(tab1, tempMbFb[i])
				} else
					kony.table.insert(tab1, tempMbFb[i])
			} */
 			var tab2 = []
			for (var i = 0; i < selectedRow; i++) {
 				kony.table.insert(tab2, gblTransferToRecipientData[i])
 			}
 			//appending selected row and account list of selected row
			kony.table.append(tab2, tab1);
 			//adding data after selected row of segment
			for (var j = selectedRow + 1; j < gblTransferToRecipientData.length; j++) {
 				kony.table.insert(tab2, gblTransferToRecipientData[j]);
			}
 			frmTranfersToRecipents.segTransferToRecipients.removeAll();
			frmTranfersToRecipents.segTransferToRecipients.setData(tab2);
}

function getBankShortNameTransferMB(bankFullName){
	if (globalSelectBankData != null && globalSelectBankData.length > 0) {
		for (var i = 0; i < globalSelectBankData.length; i++) {
			if (bankFullName == globalSelectBankData[i][2] || bankFullName == globalSelectBankData[i][8] || bankFullName == globalSelectBankData[i][7]) {
				return globalSelectBankData[i][2]; //return globalSelectBankData[i][3]; commented due to 1221 defect
			}
		}
	}
	return null;
}

function getBankShortName(bankCode){
	if (globalSelectBankData != null && globalSelectBankData.length > 0) {
		for (var i = 0; i < globalSelectBankData.length; i++) {
			if (bankCode == globalSelectBankData[i][0]) {
				return globalSelectBankData[i][2]; //return globalSelectBankData[i][3]; commented due to 1221 defect
			}
		}
	}
	return null;
}
function getBankMaxLengths(bankCode){
	if (globalSelectBankData != null && globalSelectBankData.length > 0) {
		for (var i = 0; i < globalSelectBankData.length; i++) {
			if (bankCode == globalSelectBankData[i][0]) {
				return globalSelectBankData[i][6]; //return globalSelectBankData[i][3]; commented due to 1221 defect
			}
		}
	}
	return "";
}

function getBankNameCurrentLocaleTransfer(bankCD) {
	
	if (globalSelectBankData != undefined && globalSelectBankData != null && globalSelectBankData.length > 0) {
		
		var locale = kony.i18n.getCurrentLocale();
		for (var j = 0; j < globalSelectBankData.length; j++) {
			if (bankCD == globalSelectBankData[j][0]) {
				if (kony.string.startsWith(locale, "en", true) == true)
					return globalSelectBankData[j][7];
				else
					return globalSelectBankData[j][8];
			}
		}
	}
	else{
		return kony.i18n.getLocalizedString("keyTMBBank");
	}
}


function removeSquareBrackets(data){
	var res = "";
	if(isNotBlank(data)){
		 var res1 = data.replace(")", "");
    	 res= res1 .replace("(", "");
	}
    return res;
}

function shareButtonOnClick(){
	if(frmTransfersAck.imgTransNBpAckComplete.src == "iconnotcomplete.png" || frmTransfersAck.imgTransNBpAckComplete.src == "mes2.png")
	{
		alert(kony.i18n.getLocalizedString("Err_ShareFailed"));
		return;
	}
	else
	{
		if(frmTransfersAck.hbxShareOption.isVisible){
			frmTransfersAck.hbxShareOption.isVisible = false;
			frmTransfersAck.imgHeaderMiddle.src = "arrowtop.png";
		}else{
			frmTransfersAck.hbxShareOption.isVisible = true;
			frmTransfersAck.imgHeaderMiddle.src = "empty.png";
		}	
	}	
}

function initialFrmTransfersAckPreShow(){
	frmTransfersAck.hbxShareOption.isVisible = false;
	frmTransfersAck.lblTransNPbAckFrmBal.setVisibility(true);
	frmTransfersAck.imgHide.src = "arrow_up.png";
	gblAckFlage = "true";
}

function resetSharingDisplayToDefault(){
	frmTransfersAck.hbxShareOption.isVisible = false;
	//frmTransfersAck.hbxArrow.isVisible = true;
	//frmTransfersAck.imgHeaderMiddle.src = "arrowtop.png";
}

function isValidateFromToAccount(toAcctField){
	var formAcctNum = removeHyphenIB(frmTransferLanding.segTransFrm.selectedItems[0].accountNum);
	var toAcctNum = removeHyphenIB(toAcctField);
			
	if (formAcctNum == toAcctNum && gblisTMB == gblTMBBankCD){
		//showAlert(kony.i18n.getLocalizedString("TRErr_ToAccNotEligible"), kony.i18n.getLocalizedString("info"));
		return false;
	}else{
		return true;
	}
}

function preShowfrmPreTransferMB() {
	changeStatusBarColor();
    frmPreTransferMB.scrollboxMain.scrollToEnd();
    gblIndex = -1;
    frmPreTransferMB.label475124774164.text = kony.i18n.getLocalizedString("Transfer");
    isMenuShown = false;
	displayImgToAccount(false);
	displayImgToMobile(false);
	displayImgToCitizen(false);
	DisableFadingEdges(frmPreTransferMB);
}

function displayImgToAccount(isActive){
	var locale = kony.i18n.getCurrentLocale();
	if(locale == 'en_US'){
		if(isActive){
			gblSelTransferMode = 1;
			frmPreTransferMB.imgToAccount.src = "account_en_tap.png";
			displayImgToMobile(false);
			displayImgToCitizen(false);
		}else{
			frmPreTransferMB.imgToAccount.src = "account_en.png";
		}
	}else{
		if(isActive){
			gblSelTransferMode = 1;
			frmPreTransferMB.imgToAccount.src = "account_th_tap.png";
			displayImgToMobile(false);
			displayImgToCitizen(false);
		}else{
			frmPreTransferMB.imgToAccount.src = "account_th.png";
		}
	}
}

function displayImgToMobile(isActive){
	var locale = kony.i18n.getCurrentLocale();
	if(locale == 'en_US'){
		if(isActive){
			gblSelTransferMode = 2;
			frmPreTransferMB.imgToMobile.src = "mobile_en_tap.png";
			displayImgToAccount(false);
			displayImgToCitizen(false);
		}else{
			frmPreTransferMB.imgToMobile.src = "mobile_en.png";
		}
	}else{
		if(isActive){
			gblSelTransferMode = 2;
			frmPreTransferMB.imgToMobile.src = "mobile_th_tap.png";
			displayImgToAccount(false);
			displayImgToCitizen(false);
		}else{
			frmPreTransferMB.imgToMobile.src = "mobile_th.png";
		}
	}
}

function displayImgToCitizen(isActive){
	var locale = kony.i18n.getCurrentLocale();
	if(locale == 'en_US'){
		if(isActive){
			gblSelTransferMode = 3;
			frmPreTransferMB.imgToCitizen.src = "citizen_en_tap.png";
			displayImgToAccount(false);
			displayImgToMobile(false);
		}else{
			frmPreTransferMB.imgToCitizen.src = "citizen_en.png";
		}
	}else{
		if(isActive){
			gblSelTransferMode = 3;
			frmPreTransferMB.imgToCitizen.src = "citizen_th_tap.png";
			displayImgToAccount(false);
			displayImgToMobile(false);
		}else{
			frmPreTransferMB.imgToCitizen.src = "citizen_th.png";
		}
	}
}

function toTransferPageFromPreLandingPage(){
	if(checkMBUserStatus()){
		isMenuShown = false;
		gblRetryCountRequestOTP = "0";
		gblPaynow = true;
		GBLFINANACIALACTIVITYLOG = {}
		var inputParam = {}
		inputParam["transferFlag"] = "true";
		showLoadingScreen();
		destroyMenuSpa();
		invokeServiceSecureAsync("customerAccountInquiry", inputParam, callBackTransferFromAccountsFromSmry)
	}
}