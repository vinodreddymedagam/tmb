gblCustomerBBIBInqRs = [];
gblmasterBillerAndTopupBB = [];
gblmyBillerListBB = [];
gblcustomerAccountsListBB = [];
gblBillerCategoriesBBIB = [];
gblBillerCategoriesBBIBTH = [];
gblBillersForSearch = [];
gblSugBillersForSearch = [];
gblBBMore = 0;
gblSugBBMore = 0;
gblOnClickSug = true;
gblUpdateMasterDataCategory = true;
//gblPrevForm = "";
tempRec = [];
messageObj = {};
isRefreshCalender = true;
executionFlowBB = false;
gblToUpdateBBListIB = false;
isToUpdateAccList = false;
gblTokenSwitchFlagBB = false;
gblLocaleChangeBB = false;
gblUpdateMasterDataIB = false;
gblBillerMethodBB = "";
gblBillerCompCodeBB = ""
gblRef1LenBB = "";
gblRef2LenBB = "";
gblPmtStatus = "";

function checkUserStatusBBIB() {
    var inputParam = {
        rqUUId: "",
        channelName: "IB-INQ"
    }
    showLoadingScreenPopup();
    invokeServiceSecureAsync("crmProfileInq", inputParam, callBackUSerStatusBBIB)
}

function callBackUSerStatusBBIB(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == 0) {
            var IBUserStatus = callBackResponse["mbFlowStatusIdRs"];
            if (IBUserStatus == "04") {
                dismissLoadingScreenPopup();
                showAlert(kony.i18n.getLocalizedString("keyIBUSerIDlocked"), kony.i18n.getLocalizedString("info"))
            } else {
                gblUserLockStatusIB = IBUserStatus;
                dismissLoadingScreenPopup();
                getSelectBillerCategoryServiceBB();
            }
        } else {
            dismissLoadingScreenPopup();
            showAlert(kony.i18n.getLocalizedString("keyIBUSerIDlocked"), kony.i18n.getLocalizedString("info"))
        }
    }
}

function getSelectBillerCategoryServiceBB() {
    showLoadingScreenPopup();
    var inputParams = {
        BillerCategoryGroupType: gblBillerCategoryGroupType
            //clientDate:getCurrentDate()not working on IE 8
    };
    invokeServiceSecureAsync("billerCategoryInquiry", inputParams, getSelectBillerListServiceAsyncCallbackBB);
}

function getSelectBillerListServiceAsyncCallbackBB(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == 0) {
            onSelectBBSubMenuIB();
        }
    }
}

function onSelectBBSubMenuIB() {
    gblMyBillerTopUpBB = 2;
    executionFlowBB = false;
    isToUpdateAccList = true;
    gblToUpdateBBListIB = false;
    //alert(gblmasterBillerAndTopupBB.length + " : length")
    if (gblmasterBillerAndTopupBB.length == 0) {
        inquireMasterBillerAndTopup();
    } else {
        //populatefrmBBSelect(gblmasterBillerAndTopupBB);
        //alert("inside updateAccountListBB==0 :: " + gblmasterBillerAndTopupBB.length)
        updateAccountListBB();
    }
    frmIBBeepAndBillList.hboxScreenLevel2.setVisibility(false)
    frmIBBeepAndBillList.hbxImage.setVisibility(true)
}

function showBeepAndBillListIB() {
    gblToUpdateBBListIB = false;
    inquireCustomerBeepAndBillListIB();
}

function updateBeepAndBillListIB() {
    gblToUpdateBBListIB = true;
    inquireCustomerBeepAndBillListIB();
}

function inquireCustomerBeepAndBillListIB() {
    tempRec = [];
    index = 0;
    inputParam = {};
    inputParam["rmNum"] = "";
    showLoadingScreenPopup();
    invokeServiceSecureAsync("customerBBInquiry", inputParam, inquireCustomerBeepAndBillIBCallBack);
}

function isEndsWithColon(str) {
    if (str == undefined || str == null) return true;
    if (str.lastIndexOf(":") == str.length - 1) return true;
    return false;
}

function inquireCustomerBeepAndBillIBCallBack(status, result) {
    var tmpRef2BB = "";
    var tmpRef2Type = "";
    var tmpRef2Data = "";
    var labelRef1EN = "";
    var labelRef1TH = "";
    var labelRef2EN = "";
    var labelRef2TH = "";
    if (status == 400) {
        if (result["opstatus"] == 0) {
            var statusCode = result["custBBInqRs"][0]["statusCode"];
            var severity = result["custBBInqRs"][0]["severity"];
            var statusDesc = result["custBBInqRs"][0]["statusDesc"];
            if (statusCode != 0) {
                dismissLoadingScreenPopup();
                showAlert(result["custBBInqRs"][0]["addStatusDesc"], kony.i18n.getLocalizedString("info"));
                return false;
            } else {
                var i = 0;
                recLength = result["custBBInqRs"].length;
                //alert(recLength + " recLength");
                if (recLength == 1 && executionFlowBB == false) {
                    //alert("dsgwet " + gblLocaleChangeBB )
                    if (gblLocaleChangeBB == false) {
                        frmIBBBTnC.show();
                    } else {
                        //frmIBBeepAndBillList.show();
                    }
                    gblLocaleChangeBB = false;
                    gblCustomerBBIBInqRs = [];
                    dismissLoadingScreenPopup();
                    frmIBBeepAndBillList.lblMyBills.setVisibility(false);
                    frmIBBeepAndBillList.hbxMore.setVisibility(false);
                } else {
                    for (i = 1; i < recLength; i++) {
                        var tempRecLocal = {};
                        tempRecLocal["lblRef1Value"] = result["custBBInqRs"][i]["ref1"];
                        tempRecLocal["lblRef2Value"] = result["custBBInqRs"][i]["ref2"];
                        tempRecLocal["lblBillerNickname"] = result["custBBInqRs"][i]["bbNickName"];
                        tempRecLocal["hdnAccountNum"] = result["custBBInqRs"][i]["acctIdentValue"];
                        tempRecLocal["hdnBillerCompcode"] = result["custBBInqRs"][i]["bbCompCode"];
                        tempRecLocal["hdnStatus"] = result["custBBInqRs"][i]["applStatus"];
                        tempRecLocal["imgBillerLogo"] = {
                            "src": BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + result["custBBInqRs"][i]["bbCompCode"] + "&modIdentifier=MyBillers"
                        }
                        tempRecLocal["imgBillersRtArrow"] = {
                            "src": "bg_arrow_right.png"
                        }
                        for (var j = 0; j < gblmasterBillerAndTopupBB.length; j++) {
                            if (gblmasterBillerAndTopupBB[j]["BillerCompcode"] == result["custBBInqRs"][i]["bbCompCode"]) {
                                tmpRef2BB = "N";
                                tmpRef2Type = "";
                                tmpRef2Data = "";
                                if (gblmasterBillerAndTopupBB[j]["BillerMiscData"].length > 0) {
                                    for (var k = 0; k < gblmasterBillerAndTopupBB[j]["BillerMiscData"].length; k++) {
                                        if (gblmasterBillerAndTopupBB[j]["BillerMiscData"][k]["MiscName"] == "BB.IsRequiredRefNumber2") {
                                            tmpRef2BB = gblmasterBillerAndTopupBB[j]["BillerMiscData"][k]["MiscText"];
                                        }
                                        if (gblmasterBillerAndTopupBB[j]["BillerMiscData"][k]["MiscName"] == "BB.Ref2.INPUT.TYPE") {
                                            tmpRef2Type = gblmasterBillerAndTopupBB[j]["BillerMiscData"][k]["MiscText"];
                                        }
                                        if (gblmasterBillerAndTopupBB[j]["BillerMiscData"][k]["MiscName"] == "BB.Ref2.INPUT.VALUE") {
                                            tmpRef2Data = gblmasterBillerAndTopupBB[j]["BillerMiscData"][k]["MiscText"];
                                        }
                                        if (gblmasterBillerAndTopupBB[j]["BillerMiscData"][k]["MiscName"] == "BB.Ref1.LABEL.EN") {
                                            labelRef1EN = gblmasterBillerAndTopupBB[j]["BillerMiscData"][k]["MiscText"];
                                        }
                                        if (gblmasterBillerAndTopupBB[j]["BillerMiscData"][k]["MiscName"] == "BB.Ref2.LABEL.EN") {
                                            labelRef2EN = gblmasterBillerAndTopupBB[j]["BillerMiscData"][k]["MiscText"];
                                        }
                                        if (gblmasterBillerAndTopupBB[j]["BillerMiscData"][k]["MiscName"] == "BB.Ref1.LABEL.TH") {
                                            labelRef1TH = gblmasterBillerAndTopupBB[j]["BillerMiscData"][k]["MiscText"];
                                        }
                                        if (gblmasterBillerAndTopupBB[j]["BillerMiscData"][k]["MiscName"] == "BB.Ref2.LABEL.TH") {
                                            labelRef2TH = gblmasterBillerAndTopupBB[j]["BillerMiscData"][k]["MiscText"];
                                        }
                                    }
                                }
                                tempRecLocal["hdnIsReqRef2Add"] = gblmasterBillerAndTopupBB[j]["IsRequiredRefNumber2Add"];
                                tempRecLocal["hdnIsReqRef2Pay"] = gblmasterBillerAndTopupBB[j]["IsRequiredRefNumber2Pay"];
                                tempRecLocal["hdnBillerMethod"] = gblmasterBillerAndTopupBB[j]["BillerMethod"];
                                tempRecLocal["hdnBillerCategoryID"] = gblmasterBillerAndTopupBB[j]["BillerCategoryID"];
                                tempRecLocal["hdnBillerNameEN"] = gblmasterBillerAndTopupBB[j]["BillerNameEN"];
                                tempRecLocal["hdnBillerNameTH"] = gblmasterBillerAndTopupBB[j]["BillerNameTH"];
                                if (labelRef1EN == "") {
                                    labelRef1EN = gblmasterBillerAndTopupBB[j]["LabelReferenceNumber1EN"];
                                    labelRef1TH = gblmasterBillerAndTopupBB[j]["LabelReferenceNumber1TH"];
                                    labelRef2EN = gblmasterBillerAndTopupBB[j]["LabelReferenceNumber2EN"];
                                    labelRef2TH = gblmasterBillerAndTopupBB[j]["LabelReferenceNumber2TH"];
                                }
                                //alert("Before " + labelRef1EN + " " + labelRef1TH + " " + labelRef2EN + " " + labelRef2TH)
                                if (isEndsWithColon(labelRef1EN) == false) labelRef1EN = labelRef1EN + ":";
                                if (isEndsWithColon(labelRef1TH) == false) labelRef1TH = labelRef1TH + ":";
                                if (isEndsWithColon(labelRef2EN) == false) labelRef2EN = labelRef2EN + ":";
                                if (isEndsWithColon(labelRef2TH) == false) labelRef2TH = labelRef2TH + ":";
                                //     alert("Before " + labelRef1EN + " " + labelRef1TH + " " + labelRef2EN + " " + labelRef2TH) 
                                tempRecLocal["hdnLabelRefNum1EN"] = labelRef1EN;
                                tempRecLocal["hdnLabelRefNum1TH"] = labelRef1TH;
                                tempRecLocal["hdnLabelRefNum2EN"] = labelRef2EN;
                                tempRecLocal["hdnLabelRefNum2TH"] = labelRef2TH;
                                tempRecLocal["hdnBillerGroupType"] = gblmasterBillerAndTopupBB[j]["BillerGroupType"];
                                tempRecLocal["hdnRef2BB"] = tmpRef2BB;
                                tempRecLocal["hdnRef2Type"] = tmpRef2Type;
                                tempRecLocal["hdnRef2Data"] = tmpRef2Data;
                                tempRecLocal["lblBillerCompCode"] = gblmasterBillerAndTopupBB[j]["BillerNameEN"] + "(" + gblmasterBillerAndTopupBB[j]["BillerCompcode"] + ")";
                                if (kony.i18n.getCurrentLocale() == "en_US") {
                                    tempRecLocal["lblRef1"] = labelRef1EN;
                                    tempRecLocal["lblBillerCompCode"] = gblmasterBillerAndTopupBB[j]["BillerNameEN"] + "(" + gblmasterBillerAndTopupBB[j]["BillerCompcode"] + ")";
                                } else {
                                    tempRecLocal["lblRef1"] = labelRef1TH;
                                    tempRecLocal["lblBillerCompCode"] = gblmasterBillerAndTopupBB[j]["BillerNameTH"] + "(" + gblmasterBillerAndTopupBB[j]["BillerCompcode"] + ")";
                                }
                                if (tmpRef2BB == "Y") {
                                    if (kony.i18n.getCurrentLocale() == "en_US") {
                                        tempRecLocal["lblRef2"] = labelRef2EN;
                                    } else {
                                        tempRecLocal["lblRef2"] = labelRef2TH;
                                    }
                                    tempRecLocal["hdnLabelRefNum2EN"] = labelRef2EN;
                                    tempRecLocal["hdnLabelRefNum2TH"] = labelRef2TH;
                                } else {
                                    tempRecLocal["lblRef2"] = "";
                                    tempRecLocal["hdnLabelRefNum2EN"] = "";
                                    tempRecLocal["hdnLabelRefNum2TH"] = "";
                                }
                                break;
                            }
                        }
                        tempRec.push(tempRecLocal);
                    }
                    frmIBBeepAndBillList.segBillersList.removeAll();
                    gblCustomerBBIBInqRs = [];
                    gblCustomerBBIBInqRs = tempRec;
                    gblBBMore = 0;
                    gblBillersForSearch = [];
                    gblBillersForSearch = tempRec;
                    if (executionFlowBB == false && gblToUpdateBBListIB == false) {
                        frmIBBeepAndBillList.show();
                        frmIBBeepAndBillList.lblNoBills.setVisibility(false);
                        frmIBBeepAndBillList.hbxImage.setVisibility(true);
                        onMyBBSelectBillersMoreDynamicIB();
                    } else if (executionFlowBB == true) {
                        getBBRefrenceNoIB();
                    }
                    dismissLoadingScreenPopup();
                }
            }
        } else {
            dismissLoadingScreenPopup();
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}
/*function fetchMasterBillerIBForBBCallBack(status, result) {
    
    
    if (status == 400) {
        
        if (result["opstatus"] == 0) {
            tmpRef2BB = "N";
            tmpRef2Type = "";
            tmpRef2Data = "";
            if (result["MasterBillerInqRs"][0]["BillerMiscData"].length > 0) {
                for (var j = 0; j < result["MasterBillerInqRs"][0]["BillerMiscData"].length; j++) {
                    if (result["MasterBillerInqRs"][0]["BillerMiscData"][j]["MiscName"] == "BB.IsRequiredRefNumber2") {
                        tmpRef2BB = result["MasterBillerInqRs"][0]["BillerMiscData"][j]["MiscText"];
                    }
                    if (result["MasterBillerInqRs"][0]["BillerMiscData"][j]["MiscName"] == "BB.Ref2.INPUT.TYPE") {
                        tmpRef2Type = result["MasterBillerInqRs"][0]["BillerMiscData"][j]["MiscText"];
                    }

                    if (result["MasterBillerInqRs"][0]["BillerMiscData"][j]["MiscName"] == "BB.Ref2.INPUT.VALUE") {
                        tmpRef2Data = result["MasterBillerInqRs"][0]["BillerMiscData"][j]["MiscText"];
                    }
                }
            }
            tempRec[index]["hdnIsReqRef2Add"] = result["MasterBillerInqRs"][0]["IsRequiredRefNumber2Add"];
            tempRec[index]["hdnIsReqRef2Pay"] = result["MasterBillerInqRs"][0]["IsRequiredRefNumber2Pay"];
            tempRec[index]["hdnBillerMethod"] = result["MasterBillerInqRs"][0]["BillerMethod"];
            tempRec[index]["hdnBillerCategoryID"] = result["MasterBillerInqRs"][0]["BillerCategoryID"];
            tempRec[index]["hdnBillerNameEN"] = result["MasterBillerInqRs"][0]["BillerNameEN"];
            tempRec[index]["hdnBillerNameTH"] = result["MasterBillerInqRs"][0]["BillerNameTH"];
            tempRec[index]["hdnLabelRefNum1EN"] = result["MasterBillerInqRs"][0]["LabelReferenceNumber1EN"];
            tempRec[index]["hdnLabelRefNum1TH"] = result["MasterBillerInqRs"][0]["LabelReferenceNumber1TH"];
            tempRec[index]["hdnBillerGroupType"] = result["MasterBillerInqRs"][0]["BillerGroupType"];
            tempRec[index]["hdnRef2BB"] = tmpRef2BB;
            tempRec[index]["hdnRef2Type"] = tmpRef2Type;
            tempRec[index]["hdnRef2Data"] = tmpRef2Type;

            tempRec[index]["lblBillerCompCode"] = result["MasterBillerInqRs"][0]["BillerNameEN"] + "(" + result["MasterBillerInqRs"][0]["BillerCompcode"] + ")";


            tempRec[index]["lblRef1"] = result["MasterBillerInqRs"][0]["LabelReferenceNumber1EN"];
            if (result["MasterBillerInqRs"][0]["IsRequiredRefNumber2Pay"] == "Y") {
                tempRec[index]["lblRef2"] = result["MasterBillerInqRs"][0]["LabelReferenceNumber2EN"];
                tempRec[index]["hdnLabelRefNum2EN"] = result["MasterBillerInqRs"][0]["LabelReferenceNumber2EN"];
                tempRec[index]["hdnLabelRefNum2TH"] = result["MasterBillerInqRs"][0]["LabelReferenceNumber2TH"];
            } else {
                tempRec[index]["lblRef2"] = "";
                tempRec[index]["hdnLabelRefNum2EN"] = "";
                tempRec[index]["hdnLabelRefNum2TH"] = "";
            }
            index++;
            if (index == recLength - 1) {
                
                
                frmIBBeepAndBillList.segBillersList.removeAll();
                gblCustomerBBIBInqRs = [];
                gblCustomerBBIBInqRs = tempRec;

                gblBBMore = 0;
                gblBillersForSearch = [];
                gblBillersForSearch = tempRec;

                if (executionFlowBB == false && gblToUpdateBBListIB == false) {
                    frmIBBeepAndBillList.show();
                    onMyBBSelectBillersMoreDynamicIB();
                } else if (executionFlowBB == true) {
                    getBBRefrenceNoIB();
                }

                dismissLoadingScreenPopup();
            }


        } else {
            dismissLoadingScreenPopup();
            
            showAlert(result["errMsg"], kony.i18n.getLocalizedString("info"));
            return false;
        }
    } else {
        
    }
}*/
function selectBillerToViewBBForIB() {
    frmIBBeepAndBillList.hbxImage.setVisibility(false);
    frmIBBeepAndBillList.hboxScreenLevel2.setVisibility(true);
    frmIBBeepAndBillList.lblBillerNicknameView.text = frmIBBeepAndBillList.segBillersList.selectedItems[0].lblBillerNickname;
    frmIBBeepAndBillList.lblBillerCompCodeView.text = frmIBBeepAndBillList.segBillersList.selectedItems[0].lblBillerCompCode;
    frmIBBeepAndBillList.lblRef1View.text = frmIBBeepAndBillList.segBillersList.selectedItems[0].lblRef1;
    frmIBBeepAndBillList.lblRef2View.text = frmIBBeepAndBillList.segBillersList.selectedItems[0].lblRef2;
    frmIBBeepAndBillList.lblRef1ValueView.text = frmIBBeepAndBillList.segBillersList.selectedItems[0].lblRef1Value;
    frmIBBeepAndBillList.lblRef2ValueView.text = frmIBBeepAndBillList.segBillersList.selectedItems[0].lblRef2Value;
    frmIBBeepAndBillList.lblAccTypeView.text = frmIBBeepAndBillList.segBillersList.selectedItems[0].hdnAccType;
    frmIBBeepAndBillList.lblBalanceValueView.text = commaFormattedOpenAct(frmIBBeepAndBillList.segBillersList.selectedItems[0].hdnBalacne + kony.i18n.getLocalizedString("currencyThaiBaht"));
    frmIBBeepAndBillList.lblAccNoView.text = addHyphenIB(frmIBBeepAndBillList.segBillersList.selectedItems[0].hdnAccountNum);
    frmIBBeepAndBillList.lblBillerNameView.text = frmIBBeepAndBillList.segBillersList.selectedItems[0].hdnBillerNameEN;
    frmIBBeepAndBillList.imgViewBillerLogo.src = frmIBBeepAndBillList.segBillersList.selectedItems[0].imgBillerLogo.src;
    frmIBBeepAndBillList.segBillersList.rowFocusSkin = segFocusBlue
    var status = frmIBBeepAndBillList.segBillersList.selectedItems[0].hdnStatus;
    if (status == "S" || status == "s") {
        frmIBBeepAndBillList.lblStatusValue.text = kony.i18n.getLocalizedString("keySuccessApplied");
    } else if (status == "W" || status == "w") {
        frmIBBeepAndBillList.lblStatusValue.text = kony.i18n.getLocalizedString("keyWaiting");
    } else {
        frmIBBeepAndBillList.lblStatusValue.text = kony.i18n.getLocalizedString("keyReject");
    }
    frmIBBeepAndBillList.vboxBillersLevel2.setVisibility(true);
    gblBillerCompCodeBB = frmIBBeepAndBillList.segBillersList.selectedItems[0].hdnBillerCompcode;
    gblBillerMethodBB = frmIBBeepAndBillList.segBillersList.selectedItems[0].hdnBillerMethod;
    if (frmIBBeepAndBillList.segBillersList.selectedItems[0].hdnRef2BB == "Y") {
        frmIBBeepAndBillList.hbxViewBillerRef2.setVisibility(true);
    } else {
        frmIBBeepAndBillList.hbxViewBillerRef2.setVisibility(false);
    }
    for (var i = 0; i < gblcustomerAccountsListBB.length; i++) {
        var tempAccId = gblcustomerAccountsListBB[i]["accId"];
        var accId = removeHyphenIB(tempAccId);
        accId = accId.substring(accId.length - 10, accId.length);
        var accLen = tempAccId.length;
        if (accId == frmIBBeepAndBillList.segBillersList.selectedItems[0].hdnAccountNum) {
            frmIBBeepAndBillList.lblBalanceValueView.text = commaFormattedOpenAct(gblcustomerAccountsListBB[i]["availableBal"]) + kony.i18n.getLocalizedString("currencyThaiBaht");
            if (kony.i18n.getCurrentLocale() == "en_US") {
                frmIBBeepAndBillList.lblAccTypeView.text = gblcustomerAccountsListBB[i]["ProductNameEng"];
                if (gblcustomerAccountsListBB[i]["acctNickName"] != undefined) frmIBBeepAndBillList.lblBillerNameView.text = gblcustomerAccountsListBB[i]["acctNickName"]
                else frmIBBeepAndBillList.lblBillerNameView.text = gblcustomerAccountsListBB[i]["ProductNameEng"] + accId.substring(accId.length - 4, accId.length);
            } else {
                frmIBBeepAndBillList.lblAccTypeView.text = gblcustomerAccountsListBB[i]["ProductNameThai"]
                if (gblcustomerAccountsListBB[i]["acctNickName"] != undefined) frmIBBeepAndBillList.lblBillerNameView.text = gblcustomerAccountsListBB[i]["acctNickName"]
                else frmIBBeepAndBillList.lblBillerNameView.text = gblcustomerAccountsListBB[i]["ProductNameThai"] + accId.substring(accId.length - 4, accId.length);
            }
            //frmIBBeepAndBillList.lblBillerNameView.text = gblcustomerAccountsListBB[i]["accountName"];
            //frmIBBeepAndBillList.lblAccTypeView.text = gblcustomerAccountsListBB[i]["ProductNameEng"];
            frmIBBeepAndBillList.imgAccDetails.src = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + gblcustomerAccountsListBB[i]["ICON_ID"] + "&modIdentifier=PRODICON";
        }
    }
}

function onSelectSugBillToAddBBForIB() {
    gblOnClickSug = true;
    var gblPrevForm = "";
    var currForm = kony.application.getCurrentForm();
    if (currForm.id == 'frmIBBeepAndBillApply') {
        currForm = frmIBBeepAndBillApply
    } else if (currForm.id == 'frmIBBeepAndBillList') {
        currForm = frmIBBeepAndBillList
    } else {
        currForm = gblPrevForm;
    }
    gblPrevForm = currForm;
    frmIBBeepAndBillApplyCW.imgAddBillerLogo.src = currForm.segSuggestedBillersList.selectedItems[0].imgSugBillerLogo.src;
    gblBillerMethodBB = currForm.segSuggestedBillersList.selectedItems[0].hdnBillerMethod;
    gblBillerCompCodeBB = currForm.segSuggestedBillersList.selectedItems[0].hdnBillerCompCode.text;
    gblRef1LenBB = currForm.segSuggestedBillersList.selectedItems[0].hdnRef1Len.text;
    gblRef2LenBB = currForm.segSuggestedBillersList.selectedItems[0].hdnRef2Len.text;
    var isRef2BB = currForm.segSuggestedBillersList.selectedItems[0].hdnRef2BB.text;
    if (kony.i18n.getCurrentLocale() == "en_US") {
        frmIBBeepAndBillApplyCW.lblAddBillerRef1.text = currForm.segSuggestedBillersList.selectedItems[0].hdnLabelRefNum1EN.text
        frmIBBeepAndBillApplyCW.lblAddBillerRef2.text = currForm.segSuggestedBillersList.selectedItems[0].hdnLabelRefNum2EN.text
        frmIBBeepAndBillApplyCW.lblAddBillerCompCode.text = currForm.segSuggestedBillersList.selectedItems[0].hdnSugBillerCompCodeEN.text;
    } else if (kony.i18n.getCurrentLocale() == "th_TH") {
        frmIBBeepAndBillApplyCW.lblAddBillerRef1.text = currForm.segSuggestedBillersList.selectedItems[0].hdnLabelRefNum1TH.text
        frmIBBeepAndBillApplyCW.lblAddBillerRef2.text = currForm.segSuggestedBillersList.selectedItems[0].hdnLabelRefNum2TH.text
        frmIBBeepAndBillApplyCW.lblAddBillerCompCode.text = currForm.segSuggestedBillersList.selectedItems[0].hdnSugBillerCompCodeTH.text;
    }
    frmIBBeepAndBillApplyCW.txtAddBillerRef2.setVisibility(true);
    frmIBBeepAndBillApplyCW.comboboxRef2.setVisibility(false);
    if (isRef2BB == "Y" || isRef2BB == "y") {
        var isRef2Type = currForm.segSuggestedBillersList.selectedItems[0].hdnRef2Type.text;
        var ref2Data = currForm.segSuggestedBillersList.selectedItems[0].hdnRef2Data.text;
        if (isRef2Type == "Dropdown") {
            frmIBBeepAndBillApplyCW.txtAddBillerRef2.setVisibility(false);
            frmIBBeepAndBillApplyCW.comboboxRef2.setVisibility(true);
            var masterData = [
                [0, "" + kony.i18n.getLocalizedString("keySelectRef2BB")]
            ];
            var tempData;
            var ref2List = ref2Data.split(",");
            for (var i = 0; i < ref2List.length; i++) {
                tempData = [i + 1, ref2List[i]];
                masterData.push(tempData);
            }
            frmIBBeepAndBillApplyCW.comboboxRef2.masterData = masterData;
        } else if (isRef2Type == "text") {
            frmIBBeepAndBillApplyCW.txtAddBillerRef2.isVisible = true;
            frmIBBeepAndBillApplyCW.comboboxRef2.setVisibility(false)
        }
        //frmIBBeepAndBillApplyCW.comboboxRef2.text = currForm.segSuggestedBillersList.selectedItems[0].hdnRef2Data.text;
        frmIBBeepAndBillApplyCW.hbxBillerRef2.setVisibility(true);
        frmIBBeepAndBillApplyCW.line44473234056543.isVisible = true;
    } else {
        frmIBBeepAndBillApplyCW.hbxBillerRef2.setVisibility(false);
        frmIBBeepAndBillApplyCW.line44473234056543.isVisible = false;
    }
    frmIBBeepAndBillApplyCW.txtAddBillerRef1.maxTextLength = gblRef1LenBB;
    frmIBBeepAndBillApplyCW.txtAddBillerRef2.maxTextLength = gblRef2LenBB;
    if (gblLocaleChangeBB == false) {
        frmIBBeepAndBillApplyCW.txtAddBillerNickName.text = "";
        frmIBBeepAndBillApplyCW.txtAddBillerRef1.text = "";
    }
    gblLocaleChangeBB = false;
    if (kony.application.getCurrentForm().id == 'frmIBBeepAndBillApply' || kony.application.getCurrentForm().id == 'frmIBBeepAndBillList') {
        frmIBBeepAndBillApplyCW.txtAddBillerNickName.text = "" //frmIBBeepAndBillApply.txtAddBillerNickName.text;
        frmIBBeepAndBillApplyCW.txtAddBillerRef1.text = "" //frmIBBeepAndBillApply.txtAddBillerRef1.text
        frmIBBeepAndBillApplyCW.txtAddBillerRef2.text = "" //frmIBBeepAndBillApply.txtAddBillerRef2.text
    }
    //frmIBBeepAndBillApplyCW.hbxChargeTo.setVisibility(false)
}

function updateAccountListBB() {
    isToUpdateAccList = true;
    callBillPaymentCustomerBBIBAccountService();
}

function showAccountListBB() {
    isToUpdateAccList = false;
    callBillPaymentCustomerBBIBAccountService();
}

function callBillPaymentCustomerBBIBAccountService() {
    var inputParam = {}
    inputParam["bankCd"] = "11";
    inputParam["rqUID"] = "";
    inputParam["crmId"] = "";
    //alert(inputParam)
    showLoadingScreenPopup();
    invokeServiceSecureAsync("customerBBAccountInquiry", inputParam, billPaymentCustomerBBIBAccountCallBack);
}

function billPaymentCustomerBBIBAccountCallBack(status, resulttable) {
    if (status == 400) //success responce
    {
        if (resulttable["opstatus"] == 0) {
            var fromData = []
            if (resulttable.custAcctRec == undefined) {
                resulttable.custAcctRec = [];
            }
            var resuls = resulttable["custAcctRec"];
            var length = resuls.length
            var count = 0;
            var countAcc = 0;
            //	var fromData = []
            //	var j = 1
            gblTransfersOtherAccounts = []
            gblNoOfFromAcs = resulttable.custAcctRec.length;
            /*** below are configurable params driving from Backend and get Cached. **/
            gblMaxTransferORFT = resulttable.ORFTTransLimit;
            gblMaxTransferSMART = resulttable.SMARTTransLimit;
            gblTransORFTSplitAmnt = resulttable.ORFTTransSplitAmnt;
            gblTransSMARTSplitAmnt = resulttable.SMARTTransAmnt
            gblLimitORFTPerTransaction = resulttable.ORFTTransSplitAmnt;
            gblLimitSMARTPerTransaction = resulttable.SMARTTransAmnt
                /*** till hereee  ***/
            gblcustomerAccountsListBB = [];
            gblcustomerAccountsListBB = resulttable["custAcctRec"];
            if (isToUpdateAccList == false) {
                for (j = 0; j < length; j++) {
                    //availableBalance+=parseFloat(resultable["custAcctRec"][j]["availableBal"]);
                    //	if (resulttable.custAcctRec[j].personalisedAcctStatusCode == "01")
                    //
                    //alert(resulttable["custAcctRec"][j]["personalizedAcctStatus"])
                    if (resulttable["custAcctRec"][j]["personalizedAcctStatus"] != "02") {
                        countAcc++;
                        //}else{
                        var availableBalance = resulttable["custAcctRec"][j]["availableBal"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht")
                            //Filtering joint accounts 
                        var jointActXfer = resulttable.custAcctRec[j].partyAcctRelDesc
                        if (jointActXfer == "PRIJNT" || jointActXfer == "SECJNT" || jointActXfer == "OTHJNT" || jointActXfer == "SECJAN") {
                            var temp = [{
                                custName: resulttable.custAcctRec[j].accountName,
                                acctNo: addHyphenIB(resulttable.custAcctRec[j].accId)
                            }]
                            kony.table.insert(gblTransfersOtherAccounts, temp[0])
                        } else {
                            // code for product icons in custom widgets
                            //var icon = accountImageBBIB(resulttable.custAcctRec[j]);
                            var icon = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + resulttable["custAcctRec"][j]["ICON_ID"] + "&modIdentifier=PRODICON";
                            // end of the code for product icons in custom widgets
                            /*
                             * Account number formating
                             */
                            var acctID = resulttable.custAcctRec[j].accId;
                            //if (acctID.length == 14) {
                            //							var frmId = "";
                            //							for (var k = 4; k < 14; k++) {
                            //							frmId = frmId + acctID[k]
                            //						}
                            //							acctID = frmId
                            //						}
                            //end of account formating
                            var lastDigitsOfAccID = removeHyphenIB(acctID);
                            lastDigitsOfAccID = lastDigitsOfAccID.substring(lastDigitsOfAccID.length - 4, lastDigitsOfAccID.length)
                            var productName = "";
                            if (kony.i18n.getCurrentLocale() == "en_US") {
                                productName = resulttable["custAcctRec"][j]["ProductNameEng"];
                            } else {
                                productName = resulttable["custAcctRec"][j]["ProductNameThai"];
                            }
                            var noFeeFlag = resulttable["custAcctRec"][j]["VIEW_NAME"];
                            var ramainfee = "";
                            var ramainfeeValue = "";
                            if (noFeeFlag == "PRODUCT_CODE_NOFEESAVING_TABLE") {
                                ramainfee = kony.i18n.getLocalizedString("keyRemainingFree");
                                //ramainfee = kony.i18n.getLocalizedString(resulttable.custAcctRec[j].keyremainingFree) + ": ";
                                ramainfeeValue = resulttable["custAcctRec"][j]["remainingFee"];
                            } else {
                                ramainfee = "";
                                ramainfeeValue = "";
                            }
                            var accountName = resulttable["custAcctRec"][j]["accountName"]
                                //if (accountName == null || accountName == undefined || accountName == "") {
                                //    accountName = productName + " " + lastDigitsOfAccID
                                //}
                            var acctNickName = resulttable["custAcctRec"][j]["acctNickName"];
                            if (acctNickName == undefined || acctNickName == "" || acctNickName == null) {
                                acctNickName = productName + " " + lastDigitsOfAccID
                            }
                            if (count == 0) {
                                count = count + 1
                            } else if (count == 1) {
                                count = count + 1
                            } else if (count == 2) {
                                count = 0
                            }
                            fromData.push({
                                lblProduct: kony.i18n.getLocalizedString("Product"),
                                lblProductVal: productName,
                                lblACno: kony.i18n.getLocalizedString("AccountNo"),
                                img1: icon,
                                lblCustName: acctNickName,
                                lblBalance: commaFormatted(resulttable["custAcctRec"][j]["availableBal"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
                                lblActNoval: addHyphenIB(acctID),
                                lblSliderAccN1: kony.i18n.getLocalizedString("Product") + ".:",
                                lblSliderAccN2: resulttable["custAcctRec"][j]["accType"],
                                lblDummy: resulttable["custAcctRec"][j]["fiident"],
                                lblRemFeeval: ramainfeeValue,
                                lblRemFee: ramainfee,
                                remainingFee: resulttable["custAcctRec"][j]["remainingFee"],
                                prodCode: resulttable["custAcctRec"][j]["productID"],
                                nickName: acctNickName,
                                custAcctName: resulttable["custAcctRec"][j]["accountName"],
                                accountWOF: addHyphenIB(resulttable["custAcctRec"][j]["accId"])
                            })
                        }
                    }
                }
            }
            gblcustomerAccountsListBB = [];
            gblcustomerAccountsListBB = resulttable["custAcctRec"];
            if (isToUpdateAccList == false) {
                frmIBBeepAndBillApplyCW.customFromAccountIB.data = fromData;
                frmIBBeepAndBillApplyCW.customFromAccountIB.onSelect = customWidgetSelectEventIBBeepAndBill;
                for (var i = 0; i < gblmasterBillerAndTopupBB.length; i++) {
                    if (gblmasterBillerAndTopupBB[i]["BillerCompcode"] == gblBillerCompCodeBB) {
                        if (kony.i18n.getCurrentLocale() == "th_TH") {
                            frmIBBeepAndBillApplyCW.lblAddBillerCompCode.text = gblmasterBillerAndTopupBB[i]["BillerNameTH"] + "(" + gblmasterBillerAndTopupBB[i]["BillerCompcode"] + ")";
                            frmIBBeepAndBillApplyCW.lblAddBillerRef1.text = gblmasterBillerAndTopupBB[i]["LabelReferenceNumber1TH"];
                            frmIBBeepAndBillApplyCW.lblAddBillerRef2.text = gblmasterBillerAndTopupBB[i]["LabelReferenceNumber2TH"];
                        } else {
                            frmIBBeepAndBillApplyCW.lblAddBillerCompCode.text = gblmasterBillerAndTopupBB[i]["BillerNameEN"] + "(" + gblmasterBillerAndTopupBB[i]["BillerCompcode"] + ")";
                            frmIBBeepAndBillApplyCW.lblAddBillerRef1.text = gblmasterBillerAndTopupBB[i]["LabelReferenceNumber1EN"];
                            frmIBBeepAndBillApplyCW.lblAddBillerRef2.text = gblmasterBillerAndTopupBB[i]["LabelReferenceNumber2EN"];
                        }
                    }
                }
                if (countAcc == 0) {
                    dismissLoadingScreenPopup();
                    alert(kony.i18n.getLocalizedString("keyNoEligibleAct"));
                } else {
                    frmIBBeepAndBillApplyCW.show();
                    if (frmIBBeepAndBillApplyCW.hbxChargeTo.isVisible) {
                        customWidgetSelectEventIBBeepAndBill();
                    }
                }
                dismissLoadingScreenPopup();
                return;
            }
            if (executionFlowBB == false && isToUpdateAccList == true && gblToUpdateBBListIB == false) {
                showBeepAndBillListIB();
            } else if (executionFlowBB == true) {
                updateBeepAndBillListIB();
            }
        } else {
            dismissLoadingScreenPopup();
            //alert(" " + resulttable["errMsg"]);
            alert("**" + " " + kony.i18n.getLocalizedString("keyIBNtwrkFail") + " " + "**");
            frmIBPreLogin.show();
            return false
                //popUpIBXfersNwFailure.show();
        }
    } //status 400
}

function commonValidationsApplyBBextIB() {
    if (frmIBBeepAndBillApplyCW.lblAddBillerCompCode.text == null) {
        showAlert(kony.i18n.getLocalizedString("KeyPlzSelBiller"), kony.i18n.getLocalizedString("info"));
        return false;
    }
    if (frmIBBeepAndBillApplyCW.txtAddBillerNickName.text == null || frmIBBeepAndBillApplyCW.txtAddBillerNickName.text == "") {
        showAlert(kony.i18n.getLocalizedString("Valid_BillerNicknameMandatory"), kony.i18n.getLocalizedString("info"))
        return false;
    }
    if (frmIBBeepAndBillApplyCW.txtAddBillerNickName.text.length > 20) {
        showAlert(kony.i18n.getLocalizedString("keyMaxNickNameChars"), kony.i18n.getLocalizedString("info"))
        return false;
    }
    var isValidNickName = benNameAlpNumValidation(frmIBBeepAndBillApplyCW.txtAddBillerNickName.text);
    var isValidRef1 = validateRef1ValueMB(frmIBBeepAndBillApplyCW.txtAddBillerRef1.text);
    var isValidRef2 = validateRef2ValueAll(frmIBBeepAndBillApplyCW.txtAddBillerRef2.text);
    if (isValidNickName == false) {
        showAlert(kony.i18n.getLocalizedString("keyincorrectNickName"), kony.i18n.getLocalizedString("info"));
        return false;
    } else if (isValidRef1 == false || frmIBBeepAndBillApplyCW.txtAddBillerRef1.text == null) {
        showAlert(kony.i18n.getLocalizedString("keyWrngRef1Val") + " " + frmIBBeepAndBillApplyCW.lblAddBillerRef1.text, kony.i18n.getLocalizedString("info"));
        return false;
    } else if (frmIBBeepAndBillApplyCW.hbxBillerRef2.isVisible == true) {
        if (frmIBBeepAndBillApplyCW.txtAddBillerRef2.isVisible == true) {
            if (isValidRef2 == false) {
                showAlert(kony.i18n.getLocalizedString("keyWrngRef2Val") + " " + frmIBBeepAndBillApplyCW.lblAddBillerRef2.text, kony.i18n.getLocalizedString("info"));
                return false;
            }
        } else {
            var keyValue = frmIBBeepAndBillApplyCW.comboboxRef2.selectedKeyValue;
            if (keyValue[0] == 0) {
                showAlert(kony.i18n.getLocalizedString("keyWrngRef2Val") + " " + frmIBBeepAndBillApplyCW.lblAddBillerRef2.text, kony.i18n.getLocalizedString("info"));
                return false;
            }
        }
    }
    return true;
}

function onClickApplyNextBBIB() {
    var inputParam = [];
    showLoadingScreenPopup();
    invokeServiceSecureAsync("tokenSwitching", inputParam, onClickApplyNextBBIBTokenSwitchCallBack);
}

function onClickApplyNextBBIBTokenSwitchCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (frmIBBeepAndBillApplyCW.hbxChargeTo.isVisible == false) {
                showAlert(kony.i18n.getLocalizedString("keyPleaseSelectToAccount"), kony.i18n.getLocalizedString("info"));
                dismissLoadingScreenPopup();
                return false;
            }
            if (commonValidationsApplyBBextIB() == true) {
                var ref1ValBillPay = frmIBBeepAndBillApplyCW.txtAddBillerRef1.text;
                var billerNickname = frmIBBeepAndBillApplyCW.txtAddBillerNickName.text;
                var isRef2Exist = "N";
                if (frmIBBeepAndBillApplyCW.hbxBillerRef2.isVisible) {
                    isRef2Exist = "Y"
                    if (frmIBBeepAndBillApplyCW.txtAddBillerRef2.isVisible) {
                        var ref2ValBillPay = frmIBBeepAndBillApplyCW.txtAddBillerRef2.text;
                    } else {
                        var keyValue = frmIBBeepAndBillApplyCW.comboboxRef2.selectedKeyValue;
                        var ref2ValBillPay = keyValue[1];
                    }
                } else {
                    var ref2ValBillPay = "";
                }
                //alert(billerNickname + " " + gblBillerCompCodeBB + " " + ref1ValBillPay + " " + ref2ValBillPay);
                callBillerValidationBillPayServiceBB(billerNickname, gblBillerCompCodeBB, ref1ValBillPay, isRef2Exist, ref2ValBillPay);
            }
        }
        dismissLoadingScreenPopup();
    }
}

function callBillerValidationBillPayServiceBB(billerNickname, compCodeBB, ref1, isRef2Exist, ref2) {
    inputParam = {};
    inputParam["compCode"] = compCodeBB;
    inputParam["BBNickname"] = frmIBBeepAndBillApplyCW.txtAddBillerNickName.text;
    inputParam["ref1"] = ref1;
    inputParam["ref2"] = ref2;
    inputParam["isRef2Exist"] = isRef2Exist;
    invokeServiceSecureAsync("beepAndBillerValidation", inputParam, callBillerValidationBillPayServiceCallBackBB);
}

function callBillerValidationBillPayServiceCallBackBB(status, result) {
    if (status == 400) //success responce
    {
        dismissLoadingScreenPopup();
        var validationBillPayMBFlag = "";
        if (result["opstatus"] == 0) {
            validationBillPayMBFlag = result["validationResult"];
        } else {
            dismissLoadingScreenPopup();
            validationBillPayMBFlag = result["validationResult"];
        }
        if (validationBillPayMBFlag == "true") {
            billerValidations();
        } else {
            alert(kony.i18n.getLocalizedString("keyBBValidationFail"));
            //billerValidations();
            return false;
        }
    }
}

function customWidgetSelectEventIBBeepAndBill() {
    var selectedItem = frmIBBeepAndBillApplyCW.customFromAccountIB.selectedItem;
    var selectedData = frmIBBeepAndBillApplyCW.customFromAccountIB.data[gblCWSelectedItem];
    frmIBBeepAndBillConfAndComp.lblAccType.text = selectedData.lblProductVal;
    frmIBBeepAndBillConfAndComp.lblAccHolderName.text = selectedData.lblCustName;
    frmIBBeepAndBillConfAndComp.lblAccNumValue.text = selectedData.lblActNoval;
    frmIBBeepAndBillConfAndComp.lblBalanceValue.text = selectedData.lblBalance;
    frmIBBeepAndBillConfAndComp.imgAccDetails.src = selectedData.img1
    frmIBBeepAndBillApplyCW.lblFromAccntType.text = selectedData.lblCustName;
    frmIBBeepAndBillApplyCW.lblFromAccNum.text = selectedData.lblActNoval;
    frmIBBeepAndBillApplyCW.lblFromNameRcvd.text = selectedData.lblProductVal;
    frmIBBeepAndBillApplyCW.lblFromBalanceAmtVal.text = selectedData.lblBalance;
    frmIBBeepAndBillApplyCW.hbxChargeTo.setVisibility(true);
    frmIBBeepAndBillApplyCW.imageXferFrmLP.src = selectedData.img1;
}

function billerValidations() {
    //fetched from MasterBilllerInquiry
    if (gblBillerMethodBB["text"] == "1") {
        onlinePaymentInqBBIB();
    } else if (gblBillerMethodBB["text"] == "2") {
        validateCreditCardBBIB();
    } else if (gblBillerMethodBB["text"] == "3") {
        validateLoanAccBBIB();
    } else {
        if (checkDuplicateCompCodeRef1Ref2IB() == true) {
            if (checkDuplicateNicknameOnBeepAndBillIB() == true) {
                addBeepAndBillerToIBNext()
            }
        }
    }
    return true;
}

function onlinePaymentInqBBIB() {
    var inputParam = {};
    inputParam["TrnId"] = "";
    inputParam["BankRefId"] = "";
    inputParam["BankId"] = "011";
    inputParam["BranchId"] = "0001";
    inputParam["EffDt"] = "";
    inputParam["Ref1"] = frmIBBeepAndBillApplyCW.txtAddBillerRef1.text;
    if (frmIBBeepAndBillApplyCW.hbxBillerRef2.isVisible == true) {
        if (frmIBBeepAndBillApplyCW.txtAddBillerRef2.isVisible == true) {
            inputParam["Ref2"] = frmIBBeepAndBillApplyCW.txtAddBillerRef2.text;
        } else {
            var keyValue = frmIBBeepAndBillApplyCW.comboboxRef2.selectedKeyValue;
            inputParam["Ref2"] = keyValue[1];
        }
    } else {
        inputParam["Ref2"] = "";
    }
    inputParam["Ref3"] = "";
    inputParam["Ref4"] = "";
    inputParam["MobileNumber"] = frmIBBeepAndBillApplyCW.txtAddBillerRef1.text;
    //inputParam["compCode"] = frmIBBeepAndBillApplyCW.lblAddBillerCompCode.text;
    inputParam["compCode"] = gblBillerCompCodeBB;
    inputParam["Amt"] = "0.0";
    inputParam["isChannel"] = "IB";
    inputParam["commandType"] = "Inquiry";
    inputParam["BillerGroupType"] = "0";
    showLoadingScreenPopup();
    invokeServiceSecureAsync("onlinePaymentInq", inputParam, onlinePaymentBBIBServiceAsyncCallback);
}

function onlinePaymentBBIBServiceAsyncCallback(status, result) {
    var ind;
    if (status == 400) //success responce
    {
        if (result["opstatus"] == 0) {
            if (result["StatusCode"] == 0) {
                if (checkDuplicateCompCodeRef1Ref2IB() == true) {
                    if (checkDuplicateNicknameOnBeepAndBillIB() == true) {
                        addBeepAndBillerToIBNext()
                    }
                }
                return true;
            } else {
                dismissLoadingScreenPopup();
                //ind = result["additionalDS"].length;
                //showAlert(result["additionalDS"][ind-1]["StatusDesc"],kony.i18n.getLocalizedString("info"));
                showAlert(result["errMsg"], kony.i18n.getLocalizedString("info"));
                return false;
            }
        } else {
            dismissLoadingScreenPopup();
            //ind = result["additionalDS"].length;
            showAlert(result["errMsg"], kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}

function validateCreditCardBBIB() {
    var inputparam = [];
    inputparam["cardId"] = "00000000" + removeHyphenIB(frmIBBeepAndBillApplyCW.txtAddBillerRef1.text);
    inputparam["waiverCode"] = "";
    inputparam["tranCode"] = TRANSCODEUN;
    inputparam["postedDt"] = getTodaysDate(); // current date you inq for smt
    inputparam["rqUUId"] = "";
    //alert("calling creditcardDetailsInq ");
    showLoadingScreenPopup();
    var status = invokeServiceSecureAsync("creditcardDetailsInqNonSec", inputparam, validateCreditCardCallBackForBBIB);
}

function validateCreditCardCallBackForBBIB(status, result) {
    //alert("called creditcardDetailsInqBB ");
    var ind;
    if (status == 400) //success responce
    {
        if (result["opstatus"] == 0) {
            //var StatusCode = result["statusCode"];
            //				/** checking for Soap status below  */
            //				
            dismissLoadingScreenPopup();
            if (result["StatusCode"] == 0) {
                if (checkDuplicateCompCodeRef1Ref2IB() == true) {
                    if (checkDuplicateNicknameOnBeepAndBillIB() == true) {
                        frmIBBeepAndBillConfAndComp.lblViewBillerRef1Value.text = result["cardNo"];
                        addBeepAndBillerToIBNext();
                    }
                }
            } else {
                //ind = result["additionalDS"].length;
                dismissLoadingScreenPopup();
                showAlert(result["errMsg"], kony.i18n.getLocalizedString("info"));
                return false;
            }
        } else {
            dismissLoadingScreen();
            dismissLoadingScreenPopup();
            showAlert(result["errMsg"], kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}

function validateLoanAccBBIB() {
    var inputparam = [];
    var accNumLon = removeHyphenIB(frmIBBeepAndBillApplyCW.txtAddBillerRef1.text);
    inputparam["acctId"] = "0" + accNumLon + "001"; //frmIBBillPaymentView.lblBPRef1Val.text;
    inputparam["acctType"] = kony.i18n.getLocalizedString("Loan");
    inputparam["rqUUId"] = "";
    invokeServiceSecureAsync("doLoanAcctInqNonSec", inputparam, validateLoanAccCallBackForBBIB);
}

function validateLoanAccCallBackForBBIB(status, result) {
    //alert("called creditcardDetailsInqBB ");
    var ind;
    if (status == 400) //success responce
    {
        if (result["opstatus"] == 0) {
            var StatusCode = result["StatusCode"];
            var Severity = result["Severity"];
            var StatusDesc = result["StatusDesc"];
            /** checking for Soap status below  */
            var balAmount;
            if (StatusCode == 0 || StatusCode == "0") {
                dismissLoadingScreenPopup();
                if (checkDuplicateCompCodeRef1Ref2IB() == true) {
                    if (checkDuplicateNicknameOnBeepAndBillIB() == true) {
                        addBeepAndBillerToIBNext()
                    }
                }
            } else {
                dismissLoadingScreenPopup();
                //ind = result["additionalDS"].length;
                showAlert(result["errMsg"], kony.i18n.getLocalizedString("info"));
                //showAlert(result["additionalDS"][ind-1]["StatusDesc"], kony.i18n.getLocalizedString("info"));
                return false;
            }
        } else {
            dismissLoadingScreenPopup();
            showAlert(result["errMsg"], kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}

function addBeepAndBillerToIBNext() {
    isToAccSelected = false;
    frmIBBeepAndBillConfAndComp.lblBillerName.text = frmIBBeepAndBillApplyCW.txtAddBillerNickName.text;
    frmIBBeepAndBillConfAndComp.lblCompCode.text = frmIBBeepAndBillApplyCW.lblAddBillerCompCode.text;
    frmIBBeepAndBillConfAndComp.lblViewBillerRef1.text = frmIBBeepAndBillApplyCW.lblAddBillerRef1.text;
    frmIBBeepAndBillConfAndComp.lblViewBillerRef1Value.text = frmIBBeepAndBillApplyCW.txtAddBillerRef1.text;
    frmIBBeepAndBillConfAndComp.hbxBillersOTPContainer.setVisibility(false);
    frmIBBeepAndBillConfAndComp.hbxSuccess.setVisibility(false);
    frmIBBeepAndBillConfAndComp.btnEditConfirm.setVisibility(true)
    frmIBBeepAndBillConfAndComp.hbxMobileNumber.setVisibility(false);
    frmIBBeepAndBillConfAndComp.lblOTPMobileNumBB.text = maskingIB(gblPHONENUMBER);
    frmIBBeepAndBillConfAndComp.hbxBPSave.setVisibility(false);
    frmIBBeepAndBillConfAndComp.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
    frmIBBeepAndBillConfAndComp.btnConfirm.text = kony.i18n.getLocalizedString("keyConfirm");
    frmIBBeepAndBillConfAndComp.lblHdrtxt.text = kony.i18n.getLocalizedString("Confirmation");
    frmIBBeepAndBillConfAndComp.btnEditConfirm.setVisibility(true);
    frmIBBeepAndBillConfAndComp.hbxToken.setVisibility(false);
    frmIBBeepAndBillConfAndComp.tbxToken.text = "";
    frmIBBeepAndBillConfAndComp.hbxAdv.setVisibility(false);
    if (frmIBBeepAndBillApplyCW.hbxBillerRef2.isVisible == true) {
        frmIBBeepAndBillConfAndComp.hbxViewBillerRef2.setVisibility(true);
        frmIBBeepAndBillConfAndComp.lblViewBillerRef2.text = frmIBBeepAndBillApplyCW.lblAddBillerRef2.text;
        if (frmIBBeepAndBillApplyCW.comboboxRef2.isVisible == true) {
            var keyValue = frmIBBeepAndBillApplyCW.comboboxRef2.selectedKeyValue;
            frmIBBeepAndBillConfAndComp.lblViewBillerRef2Value.text = keyValue[1];
        } else {
            frmIBBeepAndBillConfAndComp.lblViewBillerRef2Value.text = frmIBBeepAndBillApplyCW.txtAddBillerRef2.text;
        }
    } else {
        frmIBBeepAndBillConfAndComp.hbxViewBillerRef2.setVisibility(false);
    }
    frmIBBeepAndBillConfAndComp.hbxAddDelOfflineMsg.setVisibility(false);
    frmIBBeepAndBillConfAndComp.hbxAdv.setVisibility(false);
    frmIBBeepAndBillConfAndComp.show();
    var temp = frmIBBeepAndBillApplyCW.lblAddBillerCompCode.text;
    gblBillerCompCodeBB = temp.substring(temp.length - 5, temp.length - 1);
}

function addBBToListIB() {
    var accNo = frmIBBeepAndBillConfAndComp.lblAccNumValue.text;
    accNo = removeHyphenIB(accNo);
    accNo = accNo.substring(accNo.length - 10, accNo.length);
    var compCode = frmIBBeepAndBillConfAndComp.lblCompCode.text;
    compCode = compCode.substring(compCode.length - 5, compCode.length - 1)
    inputParam = {};
    inputParam["rmNum"] = "";
    inputParam["rqUUId"] = "";
    inputParam["BBNickname"] = frmIBBeepAndBillConfAndComp.lblBillerName.text;
    inputParam["BBCompCode"] = gblBillerCompCodeBB;
    inputParam["Ref1"] = frmIBBeepAndBillConfAndComp.lblViewBillerRef1Value.text;
    inputParam["Ref2"] = frmIBBeepAndBillConfAndComp.lblViewBillerRef2Value.text;
    inputParam["AcctIdentValue"] = accNo;
    //inputParam["mobileNumber"] = gblPHONENUMBER;
    inputParam["ActionFlag"] = "A";
    inputParam["MIBFlag"] = "N";
    inputParam["Channel"] = "MIB";
    inputParam["ApplyDate"] = getTodaysDate();
    inputParam["ApplyTime"] = "";
    showLoadingScreenPopup();
    invokeServiceSecureAsync("customerBBAdd", inputParam, applyBeepAndBillIBCallBack);
}

function applyBeepAndBillIBCallBack(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
            if (result["additionalStatus"][0]["statusCode"] == 0 && result["additionalStatus"][0]["addStatusCode"] == 0) {
                callNotificationAddServiceIBBBApply()
                dismissLoadingScreenPopup();
                frmIBBeepAndBillConfAndComp.hbxBillersOTPContainer.setVisibility(false);
                frmIBBeepAndBillConfAndComp.hbxToken.setVisibility(false);
                frmIBBeepAndBillConfAndComp.hbxSuccess.setVisibility(true)
                frmIBBeepAndBillConfAndComp.btnCancel.text = kony.i18n.getLocalizedString("keyReturn");
                frmIBBeepAndBillConfAndComp.btnConfirm.text = kony.i18n.getLocalizedString("keyApplyMore");
                frmIBBeepAndBillConfAndComp.lblHdrtxt.text = kony.i18n.getLocalizedString("Complete");
                frmIBBeepAndBillConfAndComp.btnEditConfirm.setVisibility(false);
                frmIBBeepAndBillConfAndComp.hbxBPSave.setVisibility(true);
                frmIBBeepAndBillConfAndComp.hbxAdv.setVisibility(true);
                if (gblBillerMethodBB["text"] != "1") {
                    //showAlert(kony.i18n.getLocalizedString("keyBBDelAddOfflineMsg"), kony.i18n.getLocalizedString("info"));
                    frmIBBeepAndBillConfAndComp.hbxAddDelOfflineMsg.setVisibility(true);
                } else {
                    frmIBBeepAndBillConfAndComp.hbxAddDelOfflineMsg.setVisibility(false);
                }
                activityLogServiceCall("032", "", "01", "", frmIBBeepAndBillConfAndComp.lblBillerName.text, frmIBBeepAndBillConfAndComp.lblAccNumValue.text, gblPHONENUMBER, frmIBBeepAndBillConfAndComp.lblViewBillerRef1Value.text, "", "");
            } else {
                dismissLoadingScreenPopup();
                activityLogServiceCall("032", "", "02", "", frmIBBeepAndBillConfAndComp.lblBillerName.text, frmIBBeepAndBillConfAndComp.lblAccNumValue.text, gblPHONENUMBER, frmIBBeepAndBillConfAndComp.lblViewBillerRef1Value.text, "", "");
                showAlert(result["additionalStatus"][0]["addStatusDesc"], kony.i18n.getLocalizedString("info"));
                return false;
            }
        } else {
            dismissLoadingScreenPopup();
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}

function callNotificationAddServiceIBBBApply() {
    var inputparam = [];
    //inputparam["channelName"] = "Internet Banking"; please confirm whether it should be IB or MB or InternetBanking
    inputparam["notificationType"] = "Email"; // check this : which value we have to pass
    //inputparam["phoneNumber"] = gblPHONENUMBER;
    //inputparam["emailId"] = gblEmailId;
    inputparam["customerName"] = gblCustomerName;
    inputparam["fromAccount"] = frmIBBeepAndBillConfAndComp.lblAccNumValue.text;
    // need partial masked account number, 5-9 digits unmasked
    inputparam["fromAcctNick"] = frmIBBeepAndBillConfAndComp.lblAccHolderName.text;
    inputparam["billerName"] = frmIBBeepAndBillConfAndComp.lblBillerName.text;
    inputparam["billerCompCode"] = frmIBBeepAndBillConfAndComp.lblCompCode.text;
    inputparam["ref1Label"] = frmIBBeepAndBillConfAndComp.lblViewBillerRef1.text;
    inputparam["ref1Value"] = frmIBBeepAndBillConfAndComp.lblViewBillerRef1Value.text;
    if (frmIBBeepAndBillConfAndComp.hbxViewBillerRef2.isVisible == true) {
        inputparam["ref2Label"] = frmIBBeepAndBillConfAndComp.lblViewBillerRef2.text;
        inputparam["ref2Value"] = frmIBBeepAndBillConfAndComp.lblViewBillerRef2Value.text;
    } else {
        inputparam["ref2Label"] = "";
        inputparam["ref2Value"] = "";
    }
    // check for ref2 exists
    //inputparam["todayDate"] = getTodaysDate(); 
    //inputparam["todayTime"] = ""; // why blank ?
    //inputparam["channelID"] = "01";
    inputparam["source"] = "sendEmailForBBApply";
    inputparam["Locale"] = kony.i18n.getCurrentLocale();
    invokeServiceSecureAsync("NotificationAdd", inputparam, callBackNotificationAddService)
}

function searchBillsIBBB() {
    if (kony.application.getCurrentForm().id == 'frmMyTopUpList') {
        frmMyTopUpList.segSuggestedBillers.removeAll();
        //frmMyTopUpList.segBillersList.removeAll();
    } else if (kony.application.getCurrentForm().id == 'frmMyTopUpSelect') {
        frmMyTopUpSelect.segSelectList.removeAll();
    }
    if (isSearched) {
        var inputParams = {
            IsActive: "1",
            billerName: gblsearchtxt
        };
    } else {
        var inputParams = {
            IsActive: "1"
        };
    }
    inputParams["flagBillerList"] = "BB";
    showLoadingScreen();
    invokeServiceSecureAsync("masterBillerInquiry", inputParams, startMySuggestBillListMBBBServiceAsyncCallback);
}
/*function populateMySelectBBSuggest(collectionDataa) {
    
    
    var masterData = [];
    MySelectBillSuggestListRs.length = 0;
    for (var i = 0; i < collectionDataa.length; i++) {
        if (collectionDataa[i]["BillerGroupType"] == 0) {
            var billerName = collectionDataa[i]["BillerNameEN"] + "(" + collectionDataa[i]["BillerCompcode"] + ")";
            var tempRecord = {
                "lblSgstdBillerName": billerName,
                "btnSugBillerAdd": {
                    "text": "Add",
                    "skin": "btnIBaddsmall"
                },
                "BillerID": collectionDataa[i]["BillerID"],
                "BillerCompCode": collectionDataa[i]["BillerCompcode"],
                "Ref1Label": collectionDataa[i]["LabelReferenceNumber1EN"],
                "Ref2Label": collectionDataa[i]["LabelReferenceNumber2EN"],
                "IsRequiredRefNumber2Add": collectionDataa[i]["IsRequiredRefNumber2Add"],
                "EffDt": collectionDataa[i]["EffDt"],
                "BillerMethod": collectionDataa[i]["BillerMethod"]
            }
            MySelectBillSuggestListRs.push(tempRecord);
        }
    }
    frmIBBeepAndBillList.segSuggestedBillersList.removeAll();
    frmIBBeepAndBillList.segSuggestedBillersList.setData(MySelectBillSuggestListRs);
}*/
function setDataOnsegSelectBBIBService() {
    if (isSearched) {
        var inputParams = {
            billerName: gblsearchtxt,
            IsActive: "1",
            clientDate: getCurrentDate()
        };
    } else {
        var inputParams = {
            IsActive: "1",
            clientDate: getCurrentDate()
        };
    }
    inputParams["flagBillerList"] = "BB";
    invokeServiceSecureAsync("masterBillerInquiry", inputParams, displayBBListServiceCallback);
}

function displayBBListServiceCallback(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == "0") {
            var responseData = callBackResponse["MasterBillerInqRs"];
            if (responseData.length > 0) {
                populatefrmBBSelect(callBackResponse["MasterBillerInqRs"]);
                dismissLoadingScreenPopup();
            } else {
                dismissLoadingScreenPopup();
                showAlert(kony.i18n.getLocalizedString("keybillernotfound") + kony.i18n.getLocalizedString("info"));
                return true;
            }
        } else {
            //alert("No Suggested Biller found");
            dismissLoadingScreenPopup();
            showAlert(callBackResponse["errMsg"], kony.i18n.getLocalizedString("info"));
            return false;
        }
    } else {
        if (status == 300) {}
    }
}

function populatefrmBBSelect(collectiondata) {
    var tempSeg = [];
    myTopUpSelect.length = 0;
    var tmpRef2Len = 0;
    var billername = "";
    var billernameEN = "";
    var billernameTH = "";
    var ref1label = "";
    var ref2label = "";
    var ref1labelEN = "";
    var ref2labelEN = "";
    var ref1labelTH = "";
    var ref2labelTH = "";
    var ref1Len = "";
    var ref2Len = "";
    var masterData = [];
    var locale = kony.i18n.getCurrentLocale();
    var tmpRef2BB = "";
    var tmpRef2Type = "";
    var tmpRef2Data = "";
    billernameEN = "BillerNameEN";
    ref1labelEN = "LabelReferenceNumber1EN";
    ref2labelEN = "LabelReferenceNumber2EN";
    billernameTH = "BillerNameTH";
    ref1labelTH = "LabelReferenceNumber1TH";
    ref2labelTH = "LabelReferenceNumber2TH";
    for (var i = 0; i < collectiondata.length; i++) {
        //if (collectiondata[i]["BillerGroupType"] == gblMyBillerTopUpBB) {
        tmpRef2BB = "N";
        tmpRef2Type = "";
        tmpRef2Data = "";
        if (kony.i18n.getCurrentLocale() == "en_US") {
            billername = collectiondata[i]["BillerNameEN"];
            ref1label = collectiondata[i]["LabelReferenceNumber1EN"];
            ref2label = collectiondata[i]["LabelReferenceNumber2EN"];
        } else {
            billername = collectiondata[i]["BillerNameTH"];
            ref1label = collectiondata[i]["LabelReferenceNumber1TH"];
            ref2label = collectiondata[i]["LabelReferenceNumber2TH"];
        }
        billernameEN = collectiondata[i]["BillerNameEN"];
        ref1labelEN = collectiondata[i]["LabelReferenceNumber1EN"];
        ref2labelEN = collectiondata[i]["LabelReferenceNumber2EN"];
        billernameTH = collectiondata[i]["BillerNameTH"];
        ref1labelTH = collectiondata[i]["LabelReferenceNumber1TH"];
        ref2labelTH = collectiondata[i]["LabelReferenceNumber2TH"];
        if (collectiondata[i]["BillerMiscData"].length > 0) {
            for (var j = 0; j < collectiondata[i]["BillerMiscData"].length; j++) {
                if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BB.IsRequiredRefNumber2") {
                    tmpRef2BB = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                }
                if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref2.INPUT.TYPE") {
                    tmpRef2Type = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                }
                if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref2.INPUT.VALUE") {
                    tmpRef2Data = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                }
                if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref1.LABEL.EN") {
                    ref1labelEN = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                }
                if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref1.LABEL.TH") {
                    ref1labelTH = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                }
                if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref2.LABEL.EN") {
                    ref2labelEN = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                }
                if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref2.LABEL.TH") {
                    ref12abelTH = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                }
                if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref1.LEN") {
                    ref1Len = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                }
                if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref2.LEN") {
                    ref2Len = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                }
            }
        }
        if (ref1Len == "") {
            ref1Len = collectiondata[i]["Ref1Len"];
        }
        if (ref2Len == "") {
            ref2Len = collectiondata[i]["Ref2Len"];
        }
        var tempRecord = {
            "lblSugBillerCompCode": {
                "text": billername + "(" + collectiondata[i]["BillerCompcode"] + ")"
            },
            "hdnSugBillerCompCodeEN": {
                "text": billernameEN + "(" + collectiondata[i]["BillerCompcode"] + ")"
            },
            "hdnSugBillerCompCodeTH": {
                "text": billernameTH + "(" + collectiondata[i]["BillerCompcode"] + ")"
            },
            "btnSugBillerAdd": {
                "text": "  ",
                "skin": "btnAdd"
            },
            "hdnBillerCompCode": {
                "text": collectiondata[i]["BillerCompcode"]
            },
            "imgSugBillerLogo": {
                "src": BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + collectiondata[i]["BillerCompcode"] + "&modIdentifier=MyBillers"
            },
            "hdnIsReqRef2Add": {
                "text": collectiondata[i]["IsRequiredRefNumber2Add"]
            },
            "hdnIsReqRef2Pay": {
                "text": collectiondata[i]["IsRequiredRefNumber2Pay"]
            },
            "hdnBillerMethod": {
                "text": collectiondata[i]["BillerMethod"]
            },
            "hdnBillerCategoryID": {
                "text": collectiondata[i]["BillerCategoryID"]
            },
            "hdnRef1Len": {
                "text": ref1Len
            },
            "hdnRef2Len": {
                "text": ref2Len
            },
            "hdnRef2BB": {
                "text": tmpRef2BB
            },
            "hdnRef2Type": {
                "text": tmpRef2Type
            },
            "hdnRef2Data": {
                "text": tmpRef2Data
            },
            "hdnLabelRefNum1EN": {
                "text": ref1labelEN
            },
            "hdnLabelRefNum1TH": {
                "text": ref1labelTH
            },
            "hdnLabelRefNum2EN": {
                "text": ref2labelEN
            },
            "hdnLabelRefNum2TH": {
                "text": ref2labelTH
            }
        }
        tempSeg.push(tempRecord);
        //}
    }
    // myTopupSuggestListMB = myTopUpSuggestListRs;
    gblSugBillersForSearch = tempSeg;
    //frmMyTopUpSelect.segSelectList.setData(myTopUpSelect);
    gblSugBBMore = 0;
    onMyBBSelectMoreDynamic();
}

function onMyBBSelectMoreDynamic() {
    var maxLength = kony.os.toNumber(GLOBAL_LOAD_MORE); //configurable parameter also initilaize gblTopUpMore to max length
    gblSugBBMore = gblSugBBMore + maxLength;
    var newData = [];
    var totalData = gblSugBillersForSearch; //data from static population 
    var totalLength = totalData.length;
    var currForm = kony.application.getCurrentForm();
    if (currForm.id != 'frmIBBeepAndBillApply') currForm = frmIBBeepAndBillList;
    if (totalLength > gblSugBBMore) {
        currForm.hbxSugMore.setVisibility(true);
        var endPoint = gblSugBBMore
    } else {
        currForm.hbxSugMore.setVisibility(false);
        var endPoint = totalLength;
    }
    if (totalLength > gblSugBBMore) {
        var endPoint = gblSugBBMore
    } else {
        var endPoint = totalLength;
    }
    for (i = 0; i < endPoint; i++) {
        newData.push(gblSugBillersForSearch[i])
    }
    if (currForm.id != 'frmIBBeepAndBillApply') currForm = frmIBBeepAndBillList;
    currForm.segSuggestedBillersList.removeAll();
    currForm.segSuggestedBillersList.setVisibility(true)
    currForm.segSuggestedBillersList.data = newData;
}

function deleteBBConfirmationIB(callBackOnConfirm) {
    popupDeleteBiller.show();
    popupDeleteBiller.btnBillerDeleteYes.skin = btnIBPopUp24px;
    popupDeleteBiller.lblBillerDeleteMsg.text = kony.i18n.getLocalizedString("keyDeleteBillerSure");
    popupDeleteBiller.btnBillerDeleteYes.onClick = callBackOnConfirm;
}

function deleteBBSelectedIB() {
    var tempAccNo = frmIBBeepAndBillList.lblAccNoView.text;
    var accNo = "";
    for (var i = 0; i < tempAccNo.length; i++) {
        if (tempAccNo[i] != '-') accNo = accNo + tempAccNo[i]
    }
    accNo = accNo.substring(accNo.length - 10, accNo.length)
    inputParam = {};
    inputParam["rmNum"] = "";
    inputParam["rqUUId"] = "";
    inputParam["BBNickName"] = frmIBBeepAndBillList.lblBillerNicknameView.text;
    inputParam["BBCompCode"] = gblBillerCompCodeBB;
    inputParam["Ref1"] = frmIBBeepAndBillList.lblRef1ValueView.text;
    if (frmIBBeepAndBillList.lblRef2ValueView.text == "" || frmIBBeepAndBillList.lblRef2ValueView.text == undefined) inputParam["Ref2"] = "";
    else inputParam["Ref2"] = frmIBBeepAndBillList.lblRef2ValueView.text;
    inputParam["AcctIdentValue"] = accNo;
    //inputParam["mobileNumber"] = gblPHONENUMBER;//from session
    inputParam["ActionFlag"] = "C";
    inputParam["MIBFlag"] = "N";
    inputParam["Channel"] = "MIB";
    inputParam["ApplyDate"] = getTodaysDate();
    inputParam["ApplyTime"] = "145612";
    inputParam["channel"] = "wap";
    popupDeleteBiller.dismiss();
    showLoadingScreenPopup();
    invokeServiceSecureAsync("customerBBDelete", inputParam, deleteBeepAndBillIBCallBack);
}

function deleteBeepAndBillIBCallBack(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
            //var statusCode = result["additionalStatus"][0]["statusCode"];
            //var severity = result["additionalStatus"][0]["severity"];
            //var statusDesc = result["additionalStatus"][0]["statusDesc"];
            //
            //
            //
            dismissLoadingScreenPopup();
            //activityLogServiceCall("033", "", "01", "", frmIBBeepAndBillList.lblBillerCompCodeView.text,
            //    frmIBBeepAndBillList.lblAccNoView.text, gblPHONENUMBER, frmIBBeepAndBillList.lblRef1ValueView.text, "", "");
            //alert(result["CustBBDelRs"][0]["ApplStatus"] + " ApplStautsfsd")
            if (gblBillerMethodBB != "1") {
                showAlert(kony.i18n.getLocalizedString("keyBBDelAddOfflineMsg"), kony.i18n.getLocalizedString("info"));
            } else {
                frmIBBeepAndBillList.segBillersList.removeAll();
                gblCustomerBBIBInqRs = [];
                showBeepAndBillListIB();
            }
            //call Notification Add
            frmIBBeepAndBillList.hboxScreenLevel2.setVisibility(false);
            frmIBBeepAndBillList.hbxImage.setVisibility(true)
        } else {
            dismissLoadingScreenPopup();
            popupDeleteBiller.dismiss();
            alert(kony.i18n.getLocalizedString("keyErrResponseOne"));
            //showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}

function checkDuplicateNicknameOnBeepAndBillIB() {
    var nickname = frmIBBeepAndBillApplyCW.txtAddBillerNickName.text;
    var check = "";
    var val = "";
    for (var i = 0; i < gblCustomerBBIBInqRs.length; i++) {
        val = gblCustomerBBIBInqRs[i].lblBillerNickname;
        if (nickname == val) {
            showAlert(kony.i18n.getLocalizedString("Receipent_alert_correctnickname"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
    return true;
}

function checkDuplicateCompCodeRef1Ref2IB() {
    //var compCode = frmIBBeepAndBillApplyCW.lblAddBillerCompCode.text;
    var ref1 = frmIBBeepAndBillApplyCW.txtAddBillerRef1.text;
    var ref2 = "";
    if (frmIBBeepAndBillApplyCW.hbxBillerRef2.isVisible && frmIBBeepAndBillApplyCW.txtAddBillerRef2.isVisible) {
        ref2 = frmIBBeepAndBillApplyCW.txtAddBillerRef2.text;
    } else if (frmIBBeepAndBillApplyCW.hbxBillerRef2.isVisible && frmIBBeepAndBillApplyCW.comboboxRef2.isVisible) {
        var temp = frmIBBeepAndBillApplyCW.comboboxRef2.selectedKeyValue;
        ref2 = temp[1];
    }
    //
    var custBBInqLen = gblCustomerBBIBInqRs.length;
    var tmpCompCode = "";
    var tmpRef1 = "";
    var tmpRef2 = "";
    for (var i = 0; i < custBBInqLen; i++) {
        tmpCompCode = gblCustomerBBIBInqRs[i].hdnBillerCompcode;
        tmpRef1 = gblCustomerBBIBInqRs[i].lblRef1Value;
        if (frmIBBeepAndBillApplyCW.hbxBillerRef2.isVisible) tmpRef2 = gblCustomerBBIBInqRs[i].lblRef2Value;
        //if (frmIBBeepAndBillApplyCW.hbxBillerRef2.isVisible) {
        if (false) {
            if (tmpCompCode == gblBillerCompCodeBB && tmpRef1 == ref1 && tmpRef2 == ref2) {
                alert("Combination of CompCode Ref1 and Ref2(if applicable) should be unique");
                return false;
            }
        } else {
            if (tmpCompCode == gblBillerCompCodeBB && tmpRef1 == ref1) {
                alert("Combination of CompCode and Ref1 should be unique");
                return false;
            }
        }
    }
    return true;
}

function reqOTPBB() {
    showLoadingScreenPopup();
    var inputParams = {};
    var locale = kony.i18n.getCurrentLocale();
    if (kony.application.getCurrentForm().id == 'frmIBBeepAndBillConfAndComp') {
        /*
        if (kony.i18n.getCurrentLocale() == "en_US") {
            inputParams["eventNotificationPolicyId"] = "MIB_OTPApply_SVC_EN";
            inputParams["SMS_Subject"] = "MIB_OTPApply_SVC_EN";
        } else {
            inputParams["eventNotificationPolicyId"] = "MIB_OTPApply_SVC_TH";
            inputParams["SMS_Subject"] = "MIB_OTPApply_SVC_TH";
        }*/
        inputParams["Channel"] = "ApplyBB";
    } else if (kony.application.getCurrentForm().id == 'frmIBBeepAndBillExecuteConfComp') {
        /*
        if (kony.i18n.getCurrentLocale() == "en_US") {
            inputParams["eventNotificationPolicyId"] = "MIB_BillPayment_EN";
            inputParams["SMS_Subject"] = "MIB_BillPayment_EN";
        } else {
            inputParams["eventNotificationPolicyId"] = "MIB_BillPayment_TH";
            inputParams["SMS_Subject"] = "MIB_BillPayment_TH";
        }*/
        inputParams["Channel"] = "BillPayment";
    }
    inputParams["locale"] = locale;
    //inputParams["Channel"] = "Internet Banking";
    invokeServiceSecureAsync("generateOTPWithUser", inputParams, reqOTPBBcallBack);
}

function reqOTPBBcallBack(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["errCode"] == "GenOTPRtyErr00001") {
            dismissLoadingScreenPopup();
            showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
            return false;
        }
        if (callBackResponse["errCode"] == "GenOTPRtyErr00002") {
            dismissLoadingScreenPopup();
            showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00002"), kony.i18n.getLocalizedString("info"));
            return false;
        }
        if (callBackResponse["opstatus"] == 0) {
            var currForm = kony.application.getCurrentForm()
            currForm.hbxToken.setVisibility(false);
            currForm.hbxBillersOTPContainer.setVisibility(true);
            gblTokenSwitchFlagBB = false;
            dismissLoadingScreenPopup();
            if (callBackResponse["errCode"] == "GenOTPRtyErr00001") {
                //alert("Test1");
                showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (callBackResponse["errCode"] == "JavaErr00001") {
                //alert("Test2");
                //dismissLoadingScreenPopup();
                showAlertIB(kony.i18n.getLocalizedString("ECJavaErr00001"), kony.i18n.getLocalizedString("info"));
                return false;
            }
            var reqOtpTimer = kony.os.toNumber(callBackResponse["requestOTPEnableTime"]);
            gblRetryCountRequestOTP = kony.os.toNumber(callBackResponse["retryCounterRequestOTP"]);
            //frmIBBeepAndBillConfAndComp.hbxBillersOTPContainer.setVisibility(true);
            gblOTPLENGTH = kony.os.toNumber(callBackResponse["otpLength"]);
            kony.timer.schedule("OTPTimerReq", OTPTimerCallBackBB, reqOtpTimer, false);
            frmIBBeepAndBillConfAndComp.hbxBankRef.setVisibility(true);
            frmIBBeepAndBillConfAndComp.hbxOTPMsgNphnNum.setVisibility(true);
            currForm.hbxBillersOTPContainer.setVisibility(true);
            var currForm = kony.application.getCurrentForm();
            currForm.hbxBillersOTPContainer.setVisibility(true);
            currForm.hbxToken.setVisibility(false);
            if (kony.application.getCurrentForm().id == 'frmIBBeepAndBillConfAndComp') {
                frmIBBeepAndBillConfAndComp.hbxBillersOTPContainer.setVisibility(true);
                frmIBBeepAndBillConfAndComp.lblBankRef.text = kony.i18n.getLocalizedString("keyIBbankrefno");
                frmIBBeepAndBillConfAndComp.lblMsgRequset.text = kony.i18n.getLocalizedString("keyotpmsgreq");
                frmIBBeepAndBillConfAndComp.hbox4510182417419.setVisibility(true);
                if (callBackResponse["Collection1"][8]["keyName"] == "pac") {
                    frmIBBeepAndBillConfAndComp.lblBankRefBBVal.text = callBackResponse["Collection1"][8]["ValueString"];
                } else {
                    for (var i = 0; i < callBackResponse["Collection1"].length; i++) {
                        if (callBackResponse["Collection1"][i]["keyName"] == "pac") {
                            frmIBBeepAndBillConfAndComp.lblBankRefBBVal.text = callBackResponse["Collection1"][i]["ValueString"];
                            break;
                        }
                    }
                }
                frmIBBeepAndBillConfAndComp.lblMobileNumVal.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
                frmIBBeepAndBillConfAndComp.btnOTPReq.skin = btnIBREQotp;
                frmIBBeepAndBillConfAndComp.btnOTPReq.setEnabled(false);
            } else if (kony.application.getCurrentForm().id == 'frmIBBeepAndBillExecuteConfComp') {
                frmIBBeepAndBillExecuteConfComp.hbxBankRef.setVisibility(true);
                frmIBBeepAndBillExecuteConfComp.hbxOTPMsgNphnNum.setVisibility(true);
                frmIBBeepAndBillExecuteConfComp.lblBankRefBB.text = kony.i18n.getLocalizedString("keyIBbankrefno");
                frmIBBeepAndBillExecuteConfComp.lblMsgRequset.text = kony.i18n.getLocalizedString("keyotpmsgreq");
                frmIBBeepAndBillExecuteConfComp.hbox4510182417419.setVisibility(true);
                if (callBackResponse["Collection1"][8]["keyName"] == "pac") {
                    frmIBBeepAndBillExecuteConfComp.lblBankRefBB.text = callBackResponse["Collection1"][8]["ValueString"];
                } else {
                    for (var i = 0; i < callBackResponse["Collection1"].length; i++) {
                        if (callBackResponse["Collection1"][i]["keyName"] == "pac") {
                            frmIBBeepAndBillExecuteConfComp.lblBankRefBB.text = callBackResponse["Collection1"][i]["ValueString"];
                            break;
                        }
                    }
                }
                frmIBBeepAndBillExecuteConfComp.lblBankRefBB.text = callBackResponse["Collection1"][8]["ValueString"];
                //frmIBBeepAndBillExecuteConfComp.lblBankRefVal.text = callBackResponse["Collection1"][8]["ValueString"];
                frmIBBeepAndBillExecuteConfComp.lblOTPMobileNumBB.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
                frmIBBeepAndBillExecuteConfComp.btnOTPReq.skin = btnIBREQotp;
                frmIBBeepAndBillExecuteConfComp.btnOTPReq.setEnabled(false);
            }
            dismissLoadingScreenPopup();
        } else {
            dismissLoadingScreenPopup();
        }
    }
}

function OTPTimerCallBackBB() {
    //frmIBTransferNowConfirmation.btnOTPReq.skin = btnIB158active;
    kony.application.getCurrentForm().btnOTPReq.skin = btnIBREQotpFocus;
    kony.application.getCurrentForm().btnOTPReq.setEnabled(true);
    try {
        kony.timer.cancel("OTPTimerReq")
    } catch (e) {}
}

function srvTokenSwitchingBB() {
    var inputParam = [];
    inputParam["crmId"] = gblcrmId;
    showLoadingScreenPopup();
    invokeServiceSecureAsync("tokenSwitching", inputParam, srvTokenSwitchingBBCallBack);
}

function srvTokenSwitchingBBCallBack(status, callbackResponse) {
    if (status == 400) {
        if (callbackResponse["opstatus"] == 0) {
            if (callbackResponse["deviceFlag"].length > 0) {
                var tokenFlag = callbackResponse["deviceFlag"][0]["TOKEN_DEVICE_FLAG"];
                var mediaPreference = callbackResponse["deviceFlag"][0]["MEDIA_PREFERENCE"];
                if (tokenFlag == "Y" && mediaPreference == "Token") {
                    gblTokenSwitchFlagBB = true;
                    var currForm = kony.application.getCurrentForm();
                    currForm.hbxBillersOTPContainer.setVisibility(true);
                    currForm.hbox4510182417419.setVisibility(false);
                    currForm.hbxBankRef.setVisibility(false);
                    currForm.hbxOTPMsgNphnNum.setVisibility(false);
                    currForm.hbxToken.setVisibility(true);
                    currForm.lblMsgRequset.text = kony.i18n.getLocalizedString("keyPleaseEnterToken");
                    dismissLoadingScreenPopup();
                } else {
                    gblTokenSwitchFlagBB = false;
                    reqOTPBB()
                }
            } else {
                gblTokenSwitchFlagBB = false;
                reqOTPBB()
            }
        } else {
            dismissLoadingScreenPopup();
        }
    }
}

function verifyOTPBB() {
    var currForm = kony.application.getCurrentForm()
    var text = currForm.txtBxOTPBB.text;
    var isNumOTP = kony.string.isNumeric(text);
    var txtLen1 = text.length;
    if (gblTokenSwitchFlagBB == false) {
        if (text == "" || text == null) {
            alert(kony.i18n.getLocalizedString("Receipent_alert_correctOTP"));
            return false;
        }
        //txtLen1 != gblOTPLENGTH || 
        if (isNumOTP == false) {
            showAlert("OTP Should be Numeric" + gblOTPLENGTH, kony.i18n.getLocalizedString("info"));
            return false;
        }
        var inputParam = {
            retryCounterVerifyOTP: gblVerifyOTP,
            userId: gblUserName,
            userStoreId: "DefaultStore",
            loginModuleId: "IBSMSOTP",
            password: text,
            segmentId: "MIB"
        };
        showLoadingScreenPopup();
        frmIBBeepAndBillConfAndComp.txtBxOTPBB.text = "";
        currForm.txtBxOTPBB.text = "";
        invokeServiceSecureAsync("verifyPasswordEx", inputParam, verifyTokenOTPBBCallback)
    } else {
        if (currForm.tbxToken.text == "" || currForm.tbxToken.text == null) {
            alert(kony.i18n.getLocalizedString("Receipent_tokenId"));
            return false;
        }
        var inputParam = {};
        inputParam["loginModuleId"] = "IB_HWTKN";
        inputParam["userStoreId"] = "DefaultStore";
        inputParam["retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
        inputParam["retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
        inputParam["userId"] = gblUserName;
        inputParam["password"] = currForm.tbxToken.text;
        inputParam["sessionVal"] = "";
        inputParam["segmentId"] = "segmentId";
        inputParam["segmentIdVal"] = "MIB";
        inputParam["channel"] = "InterNet Banking";
        showLoadingScreenPopup();
        currForm.tbxToken.text == ""
        invokeServiceSecureAsync("verifyTokenEx", inputParam, verifyTokenOTPBBCallback)
    }
}

function verifyTokenOTPBBCallback(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            var StatusCode = resulttable["StatusCode"];
            var Severity = resulttable["Severity"];
            var StatusDesc = resulttable["StatusDesc"];
            dismissLoadingScreenPopup();
            //
            gblVerifyOTPCounter = "0";
            var currForm = kony.application.getCurrentForm();
            if (currForm["id"] == 'frmIBBeepAndBillConfAndComp') {
                addBBToListIB();
            } else if (currForm.id == 'frmIBBeepAndBillExecuteConfComp') {
                //
                var inputParam = {};
                inputParam["rmNum"] = "";
                inputParam["PINcode"] = messageObj["pinCode"];
                invokeServiceSecureAsync("customerBBPaymentAdd", inputParam, invokeCustomerBBPaymentAddCallback)
            }
        } else if (resulttable["opstatus"] == 8005) {
            if (resulttable["errCode"] == "VrfyOTPErr00001") {
                gblVerifyOTP = resulttable["retryCounterVerifyOTP"];
                dismissLoadingScreenPopup();
                alert("" + kony.i18n.getLocalizedString("invalidOTP"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00002") {
                dismissLoadingScreenPopup();
                handleOTPLockedIB(resulttable);
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00004") {
                dismissLoadingScreenPopup();
                showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00004"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00005") {
                dismissLoadingScreenPopup();
                //showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00005"), kony.i18n.getLocalizedString("info"));
                alert("" + kony.i18n.getLocalizedString("invalidOTP"));
                return false;
            } else {
                dismissLoadingScreenPopup();
                //showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
                showAlert(resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
                return false;
            }
        } else {
            dismissLoadingScreenPopup();
            showAlert(resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
        }
    }
}

function invokeCustomerBBPaymentAddCallback(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
            if (result["additionalStatus"][0]["statusCode"] == 0) {
                frmIBBeepAndBillExecuteConfComp.hbxBillersOTPContainer.setVisibility(false);
                frmIBBeepAndBillExecuteConfComp.hbxSuccess.setVisibility(true);
                frmIBBeepAndBillExecuteConfComp.hbxCanConfBtnContainer.setVisibility(false);
                frmIBBeepAndBillExecuteConfComp.hbxReturn.setVisibility(true);
                frmIBBeepAndBillExecuteConfComp.ScreenTitle.text = kony.i18n.getLocalizedString("Complete");
                frmIBBeepAndBillExecuteConfComp.lblBalance.text = kony.i18n.getLocalizedString("keyBalanceAfterPayment");
                //frmIBBeepAndBillExecuteConfComp.lblBalanceVal.text=result["custAcctRec"][0]["balAfterPmnt"]+kony.i18n.getLocalizedString("currencyThaiBaht");
                frmIBBeepAndBillExecuteConfComp.hbxSave.setVisibility(true);
                frmIBBeepAndBillExecuteConfComp.lblPaymentDateVal.text = result["currentTime"];
                dismissLoadingScreenPopup();
                callNotificationAddServiceBB();
                var tellerId = result["custAcctRec"][0]["TellerID"];
                var errorCd = result["additionalStatus"][0]["statusDesc"];
                createFinActivityLogObjBB(errorCd, tellerId);
                return true;
            } else {
                dismissLoadingScreenPopup();
                showAlert(result["additionalStatus"][0]["addStatusDesc"], kony.i18n.getLocalizedString("info"));
                return false;
            }
            //setFinancialActivityLogIBBB(finTxnRefId, finTxnAmount, finTxnFee, finTxnStatus, tellerId, finLinkageId, errorCd)
        } else {
            dismissLoadingScreenPopup();
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}

function customerPaymentStatusInquiryForBBNotificationHistory(Obj) {
    messageObj["pinCode"] = Obj["pinCode"];
    messageObj["dueDate"] = Obj["dueDate"];
    messageObj["toAcctNickname"] = Obj["toAcctNickname"];
    messageObj["ref1"] = Obj["ref1"];
    messageObj["finTxnAmount"] = Obj["finTxnAmount"];
    messageObj["txnFeeAmount"] = Obj["txnFeeAmount"];
    messageObj["BBCompcode"] = Obj["BBCompcode"];
    //messageObj["finTxnRefID"] = result["finTxnRefID"];
    if (messageObj["ref2"] != undefined) {
        messageObj["ref2"] = Obj["ref2"];
    } else {
        messageObj["ref2"] = "";
    }
    frmIBBeepAndBillExecuteConfComp.lblAmountVal.text = commaFormattedOpenAct(messageObj["finTxnAmount"]) + kony.i18n.getLocalizedString("currencyThaiBaht");
    var feeValue = commaFormattedOpenAct(messageObj["txnFeeAmount"]);
    if (feeValue == ".00") frmIBBeepAndBillExecuteConfComp.lblFeeVal.text = "0.00" + kony.i18n.getLocalizedString("currencyThaiBaht");
    else frmIBBeepAndBillExecuteConfComp.lblFeeVal.text = feeValue + kony.i18n.getLocalizedString("currencyThaiBaht");
    frmIBBeepAndBillExecuteConfComp.lblTransactionIDVal.text = messageObj["pinCode"];
    //frmIBBeepAndBillExecuteConfComp.lblFeeVal.text = feeAmount;
    var date = messageObj["dueDate"];
    frmIBBeepAndBillExecuteConfComp.lblDueDateVal.text = date.split("-")[2] + "/" + date.split("-")[1] + "/" + date.split("-")[0];
    var inputParam = {};
    inputParam["rmNum"] = "";
    inputParam["PIN"] = messageObj["pinCode"];
    inputParam["finTxnAmount"] = messageObj["finTxnAmount"];
    inputParam["ref1"] = messageObj["ref1"];
    //inputParam["BillerName"] = getBillerNameIB(messageObj["BBCompcode"]);
    if (gblmasterBillerAndTopupBB.length == 0) {
        gblUpdateMasterDataIB = true;
        gblUpdateMasterDataCategory = true;
        inquireMasterBillerAndTopup();
    } else {
        inputParam["BillerName"] = getBillerNameIB(messageObj["BBCompcode"]);
        showLoadingScreenPopup();
        invokeServiceSecureAsync("CustomerBBPaymentStatusInquiry", inputParam, customerBBPayemntStatusInquiryIBCallBack);
    }
}

function getBillerNameIB(compCode) {
    for (var i = 0; i < gblmasterBillerAndTopupBB.length; i++) {
        if (compCode == gblmasterBillerAndTopupBB[i]["BillerCompcode"]) {
            if (kony.i18n.getCurrentLocale() == "en_US") return gblmasterBillerAndTopupBB[i]["BillerNameEN"];
            else return gblmasterBillerAndTopupBB[i]["BillerNameTH"];
        }
    }
}
//function applyBeepAndBillIB() {
//	var inputParam = {};
//	inputParam["serviceID"] = "verifyOTP";
//}
function customerPaymentStatusInquiryForBBIBCalendar(result) {
    //kony.application.showLoadingScreen(frmLoading, "", d.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
    // get msg from the unique ID from DB
    //parsing message
    //var splittedMsg = messsage.split(',');
    //frmIBBeepAndBillExecuteConfComp.destroy();
    //executionFlowBB = true;
    //updateAccountListBB();
    //
    var BBObj = result["finFlexValues"].split('|');
    messageObj["pinCode"] = BBObj[0];
    messageObj["dueDate"] = BBObj[1];
    messageObj["toAcctNickname"] = BBObj[2];
    messageObj["ref1"] = BBObj[3];
    messageObj["finTxnAmount"] = BBObj[4];
    messageObj["txnFeeAmount"] = BBObj[5];
    messageObj["BBCompcode"] = BBObj[6];
    messageObj["finTxnRefID"] = result["finTxnRefID"];
    if (BBObj.length == 8) {
        messageObj["ref2"] = BBObj[7];
    } else {
        messageObj["ref2"] = "";
    }
    //alert(result["finFlexValues"] + "  finTxnID is : " +result["finTxnRefID"]);
    frmIBBeepAndBillExecuteConfComp.lblAmountVal.text = commaFormattedOpenAct(messageObj["finTxnAmount"]) + kony.i18n.getLocalizedString("currencyThaiBaht");
    frmIBBeepAndBillExecuteConfComp.lblFeeVal.text = commaFormattedOpenAct(messageObj["txnFeeAmount"]) + kony.i18n.getLocalizedString("currencyThaiBaht");
    var feeValue = commaFormattedOpenAct(messageObj["txnFeeAmount"])
    if (feeValue == ".00") frmIBBeepAndBillExecuteConfComp.lblFeeVal.text = "0.00" + kony.i18n.getLocalizedString("currencyThaiBaht");
    else frmIBBeepAndBillExecuteConfComp.lblFeeVal.text = feeValue + kony.i18n.getLocalizedString("currencyThaiBaht");
    frmIBBeepAndBillExecuteConfComp.lblTransactionIDVal.text = messageObj["pinCode"];
    //frmIBBeepAndBillExecuteConfComp.lblFeeVal.text = feeAmount;
    var date = messageObj["dueDate"];
    frmIBBeepAndBillExecuteConfComp.lblDueDateVal.text = date.substring(0, 2) + "/" + date.substring(2, 4) + "/" + date.substring(4, 8);
    var inputParam = {};
    inputParam["rmNum"] = "";
    inputParam["PIN"] = messageObj["pinCode"];
    inputParam["finTxnAmount"] = messageObj["finTxnAmount"];
    inputParam["ref1"] = messageObj["ref1"];
    //inputParam["BillerName"] = getBillerNameIB(messageObj["BBCompcode"]);
    if (gblmasterBillerAndTopupBB.length == 0) {
        gblUpdateMasterDataIB = true;
        gblUpdateMasterDataCategory = true;
        inputParam["BillerName"] = inquireMasterBillerAndTopup();
    } else {
        inputParam["BillerName"] = getBillerNameIB(messageObj["BBCompcode"]);
        showLoadingScreenPopup();
        invokeServiceSecureAsync("CustomerBBPaymentStatusInquiry", inputParam, customerBBPayemntStatusInquiryIBCallBack);
    }
}

function customerBBPayemntStatusInquiryIBCallBack(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
            var statusCode = result["additionalStatus"][0]["statusCode"];
            var severity = result["additionalStatus"][0]["severity"];
            var statusDesc = result["additionalStatus"][0]["statusDesc"];
            if (statusCode == 0) {
                //alert("PmtStatus : " + result["custBBPmtStatInqRs"][0]["PmtStatus"]);
                gblPmtStatus = result["custBBPmtStatInqRs"][0]["PmtStatus"];
                if (result["custBBPmtStatInqRs"][0]["PmtStatus"] == "S") {
                    //alert("This Bill is already paid");
                    isRefreshCalender = false;
                    executionFlowBB = true;
                    isToUpdateAccList = true;
                    gblToUpdateBBListIB = true;
                    if (gblmasterBillerAndTopupBB.length == 0) {
                        showLoadingScreenPopup();
                        gblUpdateMasterDataCategory = true;
                        inquireMasterBillerAndTopup();
                    } else {
                        showLoadingScreenPopup();
                        updateAccountListBB();
                    }
                    //frmIBBeepAndBillExecuteConfComp.lblTransactionIDVal.text = result["custBBPmtStatInqRs"][0]["TransID"];
                    frmIBBeepAndBillExecuteConfComp.hbxBillersOTPContainer.setVisibility(false);
                    frmIBBeepAndBillExecuteConfComp.hbxSuccess.setVisibility(true);
                    //frmIBBeepAndBillExecuteConfComp.btnConfirm.text = kony.i18n.getLocalizedString("keyReturn");
                    frmIBBeepAndBillExecuteConfComp.hbxReturn.setVisibility(true);
                    frmIBBeepAndBillExecuteConfComp.hbxCanConfBtnContainer.setVisibility(false);
                    frmIBBeepAndBillExecuteConfComp.ScreenTitle.text = kony.i18n.getLocalizedString("Complete");
                    frmIBBeepAndBillExecuteConfComp.hbxSave.setVisibility(true);
                    var date = result["custBBPmtStatInqRs"][0]["PmtDate"];
                    frmIBBeepAndBillExecuteConfComp.lblPaymentDateVal.text = date.substring(8, 10) + "/" + date.substring(5, 7) + "/" + date.substring(0, 4);
                    //frmIBBeepAndBillExecuteConfComp.lblTransactionIDVal.text = result["custBBPmtStatInqRs"][0]["TransID"];
                    return;
                    //show complete screen
                } else if (result["custBBPmtStatInqRs"][0]["PmtStatus"] == "P") {
                    //show comfirmatin page
                    //frmBBExecuteConfirmAndComplete.lblPaymentDateVal.text = result["PmtDate"]
                    //frmIBBeepAndBillExecuteConfComp.lblTransactionIDVal.text = result["custBBPmtStatInqRs"][0]["TransID"];
                    isRefreshCalender = true;
                    executionFlowBB = true;
                    isToUpdateAccList = true;
                    gblToUpdateBBListIB = true;
                    if (gblmasterBillerAndTopupBB.length == 0) {
                        showLoadingScreenPopup();
                        gblUpdateMasterDataCategory = true;
                        inquireMasterBillerAndTopup();
                    } else {
                        showLoadingScreenPopup();
                        updateAccountListBB();
                    }
                    //frmIBBeepAndBillExecuteConfComp.hbxBillersOTPContainer.setVisibility(true);
                    //frmIBBeepAndBillExecuteConfComp.lblTransactionIDVal.text = result["custBBPmtStatInqRs"][0]["TransID"];
                    frmIBBeepAndBillExecuteConfComp.hbxSuccess.setVisibility(false);
                    frmIBBeepAndBillExecuteConfComp.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
                    frmIBBeepAndBillExecuteConfComp.hbxCanConfBtnContainer.setVisibility(true);
                    frmIBBeepAndBillExecuteConfComp.hbxReturn.setVisibility(false);
                    frmIBBeepAndBillExecuteConfComp.ScreenTitle.text = kony.i18n.getLocalizedString("Confirmation");
                    frmIBBeepAndBillExecuteConfComp.btnConfirm.setVisibility(true);
                    frmIBBeepAndBillExecuteConfComp.btnConfirm.text = kony.i18n.getLocalizedString("keyConfirm");
                    frmIBBeepAndBillExecuteConfComp.hbxSave.setVisibility(false);
                    frmIBBeepAndBillExecuteConfComp.txtBxOTPBB.text = "";
                    var date = getTodaysDate();
                    frmIBBeepAndBillExecuteConfComp.lblPaymentDateVal.text = result["currentTime"]; //date.substring(8,10) +"/"+date.substring(5,7)+"/"+date.substring(0,4);
                    frmIBBeepAndBillExecuteConfComp.lblBalance.text = kony.i18n.getLocalizedString("keyBalanceBeforePayment");
                    //frmIBBeepAndBillExecuteConfComp.show();
                }
                // populate updated list in frmMyTopupList
            } else {
                dismissLoadingScreenPopup();
                showAlert(result["additionalStatus"][0]["addStatusDesc"], kony.i18n.getLocalizedString("info"));
                return false;
            }
        } else {
            dismissLoadingScreenPopup();
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}

function populateOnExecuteBBIBPage() {
    //alert("inside populateOnExecuteBBIBPage");
    var bbListLength = gblCustomerBBIBInqRs.length;
    var tempFlag = false;
    var flag = true;
    for (var i = 0; i < bbListLength; i++) {
        var ref2Flag = true;
        if (messageObj["ref2"] == "") {
            ref2Flag = false;
        } else {
            ref2Flag = true;
        }
        var flag = true;
        if (ref2Flag == true) {
            flag = (gblCustomerBBIBInqRs[i]["lblRef1Value"] == messageObj["ref1"] && gblCustomerBBIBInqRs[i]["lblRef2Value"] == messageObj["ref2"])
        } else {
            flag = (gblCustomerBBIBInqRs[i]["lblRef1Value"] == messageObj["ref1"]);
        }
        if (gblCustomerBBIBInqRs[i]["hdnBillerCompcode"] == messageObj["BBCompcode"] && flag) {
            //alert("match found at : " + gblCustomerBBIBInqRs[i]["lblBillerNickname"] + " " + gblCustomerBBIBInqRs[i]["hdnRef2BB"]);
            frmIBBeepAndBillExecuteConfComp.lblRef1.text = gblCustomerBBIBInqRs[i]["lblRef1"];
            frmIBBeepAndBillExecuteConfComp.lblRef2.text = gblCustomerBBIBInqRs[i]["lblRef2"];
            frmIBBeepAndBillExecuteConfComp.lblRef1Value.text = gblCustomerBBIBInqRs[i]["lblRef1Value"];
            frmIBBeepAndBillExecuteConfComp.lblRef2Value.text = gblCustomerBBIBInqRs[i]["lblRef2Value"];
            frmIBBeepAndBillExecuteConfComp.lblCompCode.text = gblCustomerBBIBInqRs[i]["lblBillerCompCode"];
            gblBillerCompCodeBB = gblCustomerBBIBInqRs[i]["hdnBillerCompcode"];
            frmIBBeepAndBillExecuteConfComp.lblAccountNo.text = addHyphenIB(gblCustomerBBIBInqRs[i]["hdnAccountNum"]);
            frmIBBeepAndBillExecuteConfComp.lblNickname.text = gblCustomerBBIBInqRs[i]["lblBillerNickname"];
            frmIBBeepAndBillExecuteConfComp.imgBillerLogo.src = BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + gblCustomerBBIBInqRs[i]["hdnBillerCompcode"] + "&modIdentifier=MyBillers"
            for (var j = 0; j < gblcustomerAccountsListBB.length; j++) {
                var accId = removeHyphenIB(gblcustomerAccountsListBB[j]["accId"]);
                accId = accId.substring(accId.length - 10, accId.length);
                //alert(accId + " " + frmIBBeepAndBillExecuteConfComp.lblAccountNo.text);
                if (accId == removeHyphenIB(frmIBBeepAndBillExecuteConfComp.lblAccountNo.text)) {
                    tempFlag = true;
                    if (kony.i18n.getCurrentLocale() == "th_TH") {
                        frmIBBeepAndBillExecuteConfComp.lblName.text = gblcustomerAccountsListBB[j]["ProductNameThai"];
                    } else {
                        frmIBBeepAndBillExecuteConfComp.lblName.text = gblcustomerAccountsListBB[j]["ProductNameEng"];
                    }
                    //frmIBBeepAndBillExecuteConfComp.lblName.text = gblcustomerAccountsListBB[j]["ProductNameEng"];
                    frmIBBeepAndBillExecuteConfComp.lblFromName.text = gblcustomerAccountsListBB[j]["accountName"];
                    frmIBBeepAndBillExecuteConfComp.lblAccountNo.text = addHyphenIB(gblcustomerAccountsListBB[j]["accId"]);
                    var bal = gblcustomerAccountsListBB[j]["availableBal"];
                    frmIBBeepAndBillExecuteConfComp.lblBalanceVal.text = commaFormattedStmtIB(bal) + kony.i18n.getLocalizedString("currencyThaiBaht");
                    frmIBBeepAndBillExecuteConfComp.imgAccDetails.src = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + gblcustomerAccountsListBB[j]["ICON_ID"] + "&modIdentifier=PRODICON";
                    frmIBBeepAndBillExecuteConfComp.hbxHiddenAcc.setVisibility(false);
                    frmIBBeepAndBillExecuteConfComp.hbxFrom.setVisibility(true);
                    frmIBBeepAndBillExecuteConfComp.btnConfirm.setEnabled(true);
                    frmIBBeepAndBillExecuteConfComp.hbxBalance.setVisibility(true);
                    frmIBBeepAndBillExecuteConfComp.Fromaccount.setVisibility(true);
                    frmIBBeepAndBillExecuteConfComp.btnConfirm.skin = btnIB158;
                    frmIBBeepAndBillExecuteConfComp.btnConfirm.hoverSkin = btnIB158active;
                    if ((gblPmtStatus == "p" || gblPmtStatus == "P") && (gblcustomerAccountsListBB[j]["personalizedAcctStatus"] == "02")) {
                        frmIBBeepAndBillExecuteConfComp.richTxtLnkdAcctHidden.text = kony.i18n.getLocalizedString("S2S_DelAcctMsg") + "<a onclick= \"return onClickCallBackFunctionIB();\" href = \"#\"  >" + " " + kony.i18n.getLocalizedString("S2S_MyAcctList") + " " + "</a>" + kony.i18n.getLocalizedString("S2S_BeforeProcTrans");
                        frmIBBeepAndBillExecuteConfComp.hbxHiddenAcc.setVisibility(true);
                        //frmIBBeepAndBillExecuteConfComp.hbxFrom.setVisibility(false);
                        frmIBBeepAndBillExecuteConfComp.hbxBalance.setVisibility(false);
                        frmIBBeepAndBillExecuteConfComp.hbxFrom.setVisibility(false);
                        frmIBBeepAndBillExecuteConfComp.btnConfirm.setEnabled(false);
                        frmIBBeepAndBillExecuteConfComp.btnConfirm.skin = btnIB158disabled;
                        frmIBBeepAndBillExecuteConfComp.btnConfirm.hoverSkin = btnIB158disabled;
                    }
                    break;
                }
            }
            if (tempFlag == false) {
                frmIBBeepAndBillExecuteConfComp.richTxtLnkdAcctHidden.text = kony.i18n.getLocalizedString("S2S_DelAcctMsg") + "<a onclick= \"return onClickCallBackFunctionIB();\" href = \"#\"  >" + " " + kony.i18n.getLocalizedString("S2S_MyAcctList") + " " + "</a>" + kony.i18n.getLocalizedString("S2S_BeforeProcTrans");
                frmIBBeepAndBillExecuteConfComp.hbxHiddenAcc.setVisibility(true);
                //frmIBBeepAndBillExecuteConfComp.hbxFrom.setVisibility(false);
                frmIBBeepAndBillExecuteConfComp.hbxBalance.setVisibility(false);
                frmIBBeepAndBillExecuteConfComp.hbxFrom.setVisibility(false);
                frmIBBeepAndBillExecuteConfComp.btnConfirm.setEnabled(false);
                frmIBBeepAndBillExecuteConfComp.btnConfirm.skin = btnIB158disabled;
                frmIBBeepAndBillExecuteConfComp.btnConfirm.hoverSkin = btnIB158disabled;
            }
            if (gblPmtStatus == "S" || gblPmtStatus == "s") {
                showAlert(kony.i18n.getLocalizedString("keyBBExAlreadyPaidMsg"), kony.i18n.getLocalizedString("info"));
            }
            if (isRefreshCalender == true) {
                //frmIBBeepAndBillExecuteConfComp.hbxBillersOTPContainer.setVisibility(true);
                frmIBBeepAndBillExecuteConfComp.hbxSuccess.setVisibility(false);
                frmIBBeepAndBillExecuteConfComp.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
                frmIBBeepAndBillExecuteConfComp.btnCancel.setVisibility(true);
                frmIBBeepAndBillExecuteConfComp.ScreenTitle.text = kony.i18n.getLocalizedString("Confirmation");
                frmIBBeepAndBillExecuteConfComp.txtBxOTPBB.text = "";
            }
            frmIBBeepAndBillExecuteConfComp.show();
            return true;
        }
    }
    //if(flag == false) alert("No bb list data");
    return false;
}

function onMyBBSelectBillersMoreDynamicIB() {
    var currentForm = kony.application.getCurrentForm();
    if (currentForm["id"] != 'frmIBBeepAndBillList') currentForm = frmIBBeepAndBillApply;
    var maxLength = kony.os.toNumber(GLOBAL_LOAD_MORE); //configurable parameter also initilaize gblCustTopUpMore to max length
    gblBBMore = gblBBMore + maxLength;
    var newData = [];
    var totalData = gblBillersForSearch; //data from static population 
    var totalLength = totalData.length;
    if (totalLength == 0) {
        currentForm.lblMyBills.setVisibility(false);
    } else {
        currentForm.lblMyBills.setVisibility(true);
    }
    if (totalLength > gblBBMore) {
        currentForm.hbxMore.setVisibility(true);
        var endPoint = gblBBMore;
    } else {
        currentForm.hbxMore.setVisibility(false);
        var endPoint = totalLength;
    }
    for (i = 0; i < endPoint; i++) {
        newData.push(gblBillersForSearch[i])
    }
    currentForm.segBillersList.data = newData;
    //Fix for DEF15858
    frmIBBeepAndBillApply.segBillersList.data = newData;
}

function searchSuggestedBillersBB() {
    var search = "";
    var currForm = kony.application.getCurrentForm();
    search = currForm.tbxSearch.text;
    var tmpCatSelected = currForm.cbxSelectCat.selectedKey;
    if (search.length < 3 && (tmpCatSelected == null || tmpCatSelected == 0)) {
        currForm.lblSuggestedBiller.setVisibility(true);
        currForm.lblErrorMsg.setVisibility(false);
        if (currForm.id == 'frmIBBeepAndBillList') {
            if (gblCustomerBBIBInqRs.length > 0) {
                currForm.lblMyBills.setVisibility(true);
                currForm.lblNoBills.setVisibility(false);
            } else {
                currForm.lblMyBills.setVisibility(false);
                currForm.lblNoBills.setVisibility(true);
            }
        } else if (currForm.id == 'frmIBBeepAndBillApply') {
            if (gblmyBillerListBB.length > 0) currForm.lblMyBills.setVisibility(true);
            else currForm.lblMyBills.setVisibility(false);
        }
        populatefrmBBSelect(gblmasterBillerAndTopupBB);
        currForm.segBillersList.removeAll();
        gblBBMore = 0;
        if (currForm.id == 'frmIBBeepAndBillList') {
            gblBillersForSearch = gblCustomerBBIBInqRs;
            //currForm.segBillersList.setData(gblCustomerBBIBInqRs);
        } else if (currForm.id == 'frmIBBeepAndBillApply') {
            //currForm.segBillersList.setData(gblmyBillerListBB);
            gblBillersForSearch = gblmyBillerListBB;
        }
        onMyBBSelectBillersMoreDynamicIB();
        return;
    }
    if (search.length >= 3 || tmpCatSelected != null) {
        currForm.lblSuggestedBiller.setVisibility(true);
        isSearchedBiller = true;
        getBillerLPSuggestListBBIB();
    }
}

function getBillerLPSuggestListBBIB() {
    var currForm = kony.application.getCurrentForm()
    var search = currForm.tbxSearch.text;
    var tmpCatSelected = currForm.cbxSelectCat.selectedKey;
    if (tmpCatSelected == null) tmpCatSelected = 0;
    var tempMaterBillerTopupBB = [];
    var tempBeepAndBills = [];
    var lenMasterBiller = gblmasterBillerAndTopupBB.length;
    if (currForm.id == 'frmIBBeepAndBillList') {
        var lenBBList = gblCustomerBBIBInqRs.length;
    } else if (currForm.id == 'frmIBBeepAndBillApply') {
        var lenBBList = gblmyBillerListBB.length;
    }
    if (search.length >= 3 && tmpCatSelected != 0) {
        var lowCaseSearch = search.toLowerCase();
        for (var i = 0; i < lenMasterBiller; i++) {
            var tmpString = gblmasterBillerAndTopupBB[i]["BillerNameEN"];
            tmpString = tmpString.toLowerCase();
            var flag = tmpString.search(lowCaseSearch);
            if (gblmasterBillerAndTopupBB[i]["BillerCategoryID"] == tmpCatSelected && (flag != "-1" || flag >= 0)) {
                tempMaterBillerTopupBB.push(gblmasterBillerAndTopupBB[i]);
            }
        }
        for (var i = 0; i < lenBBList; i++) {
            if (currForm.id == 'frmIBBeepAndBillList') {
                var tmpString = gblCustomerBBIBInqRs[i]["lblBillerNickname"];
                var bbCompcode = gblCustomerBBIBInqRs[i]["lblBillerCompCode"];
            } else if (currForm.id == 'frmIBBeepAndBillApply') {
                var tmpString = gblmyBillerListBB[i]["lblBillerNickname"].text;
                var bbCompcode = gblmyBillerListBB[i]["lblBillerName"].text;
            }
            if (tmpString == undefined) tmpString = "";
            if (bbCompcode == undefined) bbCompcode = "";
            tmpString = tmpString.toLowerCase();
            bbCompcode = bbCompcode.toLowerCase();
            var flag = tmpString.search(lowCaseSearch);
            var flag1 = bbCompcode.search(lowCaseSearch);
            if (currForm.id == 'frmIBBeepAndBillList') {
                if (gblCustomerBBIBInqRs[i]["hdnBillerCategoryID"] == tmpCatSelected && ((flag != "-1" || flag >= 0) || (flag1 != "-1" || flag1 >= 0))) {
                    tempBeepAndBills.push(gblCustomerBBIBInqRs[i]);
                }
            } else if (currForm.id == 'frmIBBeepAndBillApply') {
                if (gblmyBillerListBB[i]["BillerCategoryID"].text == tmpCatSelected && ((flag != "-1" || flag >= 0) || (flag1 != "-1" || flag1 >= 0))) {
                    tempBeepAndBills.push(gblmyBillerListBB[i]);
                }
            }
        }
    } else if (search.length < 3 && tmpCatSelected != 0) {
        for (var i = 0; i < lenMasterBiller; i++) {
            if (gblmasterBillerAndTopupBB[i]["BillerCategoryID"] == tmpCatSelected) tempMaterBillerTopupBB.push(gblmasterBillerAndTopupBB[i]);
        }
        for (var i = 0; i < lenBBList; i++) {
            if (currForm.id == 'frmIBBeepAndBillList') {
                if (gblCustomerBBIBInqRs[i]["hdnBillerCategoryID"] == tmpCatSelected) {
                    tempBeepAndBills.push(gblCustomerBBIBInqRs[i]);
                }
            } else if (currForm.id == 'frmIBBeepAndBillApply') {
                if (gblmyBillerListBB[i]["BillerCategoryID"].text == tmpCatSelected) {
                    tempBeepAndBills.push(gblmyBillerListBB[i]);
                }
            }
        }
    } else if (search.length >= 3 && tmpCatSelected == 0) {
        var lowCaseSearch = search.toLowerCase();
        for (var i = 0; i < lenMasterBiller; i++) {
            var tmpString = gblmasterBillerAndTopupBB[i]["BillerNameEN"];
            tmpString = tmpString.toLowerCase();
            var flag = tmpString.search(lowCaseSearch);
            //alert(tmpString + " " + lowCaseSearch + " " + upperCaseSearch)
            if (flag != "-1" || flag >= 0) tempMaterBillerTopupBB.push(gblmasterBillerAndTopupBB[i]);
        }
        for (var i = 0; i < lenBBList; i++) {
            if (currForm.id == 'frmIBBeepAndBillList') {
                var tmpString = gblCustomerBBIBInqRs[i]["lblBillerNickname"];
                var bbCompcode = gblCustomerBBIBInqRs[i]["lblBillerCompCode"];
            } else if (currForm.id == 'frmIBBeepAndBillApply') {
                var tmpString = gblmyBillerListBB[i]["lblBillerNickname"].text;
                var bbCompcode = gblmyBillerListBB[i]["lblBillerName"].text;
            }
            if (tmpString == undefined) tmpString = "";
            if (bbCompcode == undefined) bbCompcode = "";
            tmpString = tmpString.toLowerCase();
            bbCompcode = bbCompcode.toLowerCase();
            var flag = tmpString.search(lowCaseSearch);
            var flag1 = bbCompcode.search(lowCaseSearch);
            if (flag != "-1" || flag >= 0 || flag1 != "-1" || flag1 >= 0) {
                if (currForm.id == 'frmIBBeepAndBillList') {
                    tempBeepAndBills.push(gblCustomerBBIBInqRs[i]);
                } else if (currForm.id == 'frmIBBeepAndBillApply') {
                    tempBeepAndBills.push(gblmyBillerListBB[i]);
                }
            }
        }
    } else {
        currForm.lblSuggestedBiller.setVisibility(false);
        currForm.lblMyBills.setVisibility(false);
        currForm.segSuggestedBillersList.removeAll();
        currForm.segBillersList.removeAll();
    }
    if (tempMaterBillerTopupBB.length == 0 && tempBeepAndBills.length == 0) {
        //showAlert(kony.i18n.getLocalizedString("keybillernotfound"),kony.i18n.getLocalizedString("info"));
        currForm.lblNoBills.setVisibility(true);
        currForm.lblErrorMsg.setVisibility(false);
        currForm.hbxSugMore.setVisibility(false);
        currForm.lblSuggestedBiller.setVisibility(false);
        currForm.lblMyBills.setVisibility(false);
        currForm.hbxMore.setVisibility(false);
    }
    if (tempMaterBillerTopupBB.length == 0) {
        currForm.hbxSugMore.setVisibility(false);
        currForm.lblSuggestedBiller.setVisibility(false);
    } else {
        currForm.hbxSugMore.setVisibility(true);
        currForm.lblSuggestedBiller.setVisibility(true);
    }
    if (tempBeepAndBills.length == 0) {
        currForm.lblMyBills.setVisibility(false);
        currForm.hbxMore.setVisibility(false);
    } else {
        currForm.hbxMore.setVisibility(true);
        currForm.lblMyBills.setVisibility(true);
    }
    if (tempMaterBillerTopupBB.length > 0) {
        currForm.lblSuggestedBiller.setVisibility(true);
        gblSugBBMore = 0;
        populatefrmBBSelect(tempMaterBillerTopupBB);
    } else {
        currForm.lblSuggestedBiller.setVisibility(false);
        currForm.segSuggestedBillersList.removeAll();
    }
    if (tempBeepAndBills.length > 0) {
        currForm.lblMyBills.setVisibility(true);
        currForm.segBillersList.removeAll();
        //currForm.segBillersList.setData(tempBeepAndBills);
        gblBillersForSearch = [];
        gblBillersForSearch = tempBeepAndBills;
        gblBBMore = 0;
        onMyBBSelectBillersMoreDynamicIB();
    } else {
        currForm.lblMyBills.setVisibility(false);
        currForm.segBillersList.removeAll();
    }
    if (lenBBList == 0 && tempMaterBillerTopupBB.length > 0) {
        currForm.lblNoBills.setVisibility(true);
        currForm.lblErrorMsg.setVisibility(false);
    } else if (lenBBList == 0 && tempMaterBillerTopupBB.length == 0) {
        currForm.lblNoBills.setVisibility(false);
        currForm.lblErrorMsg.setVisibility(true);
    }
    if (lenBBList > 0 && tempBeepAndBills.length == 0 && tempMaterBillerTopupBB.length == 0) {
        currForm.lblNoBills.setVisibility(false);
        currForm.lblErrorMsg.setVisibility(true);
    } else if (lenBBList > 0 && (tempBeepAndBills.length > 0 || tempMaterBillerTopupBB.length > 0)) {
        currForm.lblNoBills.setVisibility(false);
        currForm.lblErrorMsg.setVisibility(false);
    }
}

function getSelectBBCategoryService() {
    var inputParams = {
        BillerCategoryGroupType: gblBillerCategoryGroupType
    };
    showLoadingScreenPopup()
    invokeServiceSecureAsync("billerCategoryInquiry", inputParams, getSelectBBCategoryServiceCallback);
}

function getSelectBBCategoryServiceCallback(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == 0) {
            var collectionData = callBackResponse["BillerCategoryInqRs"];
            var masterData = [
                [0, "Select Category"]
            ];
            var tempData;
            for (var i = 0; i < collectionData.length; i++) {
                invokeCommonIBLogger("BILLER CODE " + collectionData[i]["BillerCategoryID"] + "BILLER NAME " + collectionData[i]["BillerCategoryDescEN"]);
                tempData = [collectionData[i]["BillerCategoryID"], collectionData[i]["BillerCategoryDescEN"]];
                masterData.push(tempData);
            }
            if (kony.application.getCurrentForm().id == 'frmIBBeepAndBillList') {
                //alert("inside frmIBTopUpLandingPage");
                frmIBBeepAndBillList.cbxSelectCat.masterData = masterData;
                //getMySelectTopUpSuggestListIB()
                //setDataOnsegSelectBBIBService();
                // getMySelectTopUpListIB();
            } else if (kony.application.getCurrentForm().id == 'frmIBBeepAndBillApply') {
                frmIBBeepAndBillApply.cbxSelectCat.masterData = masterData;
            }
            //	frmIBTopUpLandingPage.combobox1010778103113652.masterData= masterData;        	
            dismissLoadingScreenPopup();
        } else {
            var masterData = [];
            var tempData = ["Biller", "Biller"];
            masterData.push(tempData);
            if (kony.application.getCurrentForm().id == 'frmIBBeepAndBillList') {
                frmIBBeepAndBillList.cbxSelectCat.masterData = masterData;
            } else if (kony.application.getCurrentForm().id == 'frmIBBeepAndBillApply') {
                frmIBBeepAndBillApply.cbxSelectCat.masterData = masterData;
            }
        }
    } else {
        if (status == 300) {
            var masterData = [];
            var tempData = ["Biller", "Biller"];
            masterData.push(tempData);
            dismissLoadingScreenPopup();
            if (kony.application.getCurrentForm().id == 'frmIBBeepAndBillList') {
                frmIBBeepAndBillList.cbxSelectCat.masterData = masterData;
            } else if (kony.application.getCurrentForm().id == 'frmIBBeepAndBillApply') {
                frmIBBeepAndBillApply.cbxSelectCat.masterData = masterData;
            }
        }
    }
    dismissLoadingScreenPopup();
}

function inquireMasterBillerAndTopup() {
    var inputParams = {
        IsActive: "1",
        BillerGroupType: "0"
    };
    if (gblUpdateMasterDataCategory == true) {
        showLoadingScreenPopup();
        gblmasterBillerAndTopupBB = [];
        inputParams["flagBillerList"] = "BB";
        invokeServiceSecureAsync("masterBillerInquiry", inputParams, getMasterBillerForBB);
    }
    gblUpdateMasterDataCategory = false;
}

function updateMasterBillerAndTopup() {
    var inputParams = {
        IsActive: "1",
        BillerGroupType: "0"
    };
    showLoadingScreenPopup();
    gblmasterBillerAndTopupBB = [];
    inputParams["flagBillerList"] = "BB";
    invokeServiceSecureAsync("masterBillerInquiry", inputParams, updateMasterBillerForBB);
}

function updateMasterBillerForBB(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == 0 || callBackResponse["opstatus"] == "0") {
            if (callBackResponse["StatusCode"] == 0 || callBackResponse["StatusCode"] == "0") {
                var length = callBackResponse["MasterBillerInqRs"].length;
                //alert(length + "  topups length");
                for (var i = 0; i < length; i++) {
                    var noOfChannels = callBackResponse["MasterBillerInqRs"][i]["ValidChannel"].length;
                    for (var j = 0; j < noOfChannels; j++) {
                        if (callBackResponse["MasterBillerInqRs"][i]["ValidChannel"][j]["ChannelCode"] == "51") {
                            gblmasterBillerAndTopupBB.push(callBackResponse["MasterBillerInqRs"][i]);
                            break;
                        }
                    }
                }
                for (var i = 0; i < gblmasterBillerAndTopupBB.length; i++) {
                    if (isEndsWithColon(gblmasterBillerAndTopupBB[i]["LabelReferenceNumber1EN"]) == false) gblmasterBillerAndTopupBB[i]["LabelReferenceNumber1EN"] = gblmasterBillerAndTopupBB[i]["LabelReferenceNumber1EN"] + ":";
                    if (isEndsWithColon(gblmasterBillerAndTopupBB[i]["LabelReferenceNumber1TH"]) == false) gblmasterBillerAndTopupBB[i]["LabelReferenceNumber1TH"] = gblmasterBillerAndTopupBB[i]["LabelReferenceNumber1TH"] + ":";
                    if (isEndsWithColon(gblmasterBillerAndTopupBB[i]["LabelReferenceNumber2EN"]) == false) gblmasterBillerAndTopupBB[i]["LabelReferenceNumber2EN"] = gblmasterBillerAndTopupBB[i]["LabelReferenceNumber2EN"] + ":";
                    if (isEndsWithColon(gblmasterBillerAndTopupBB[i]["LabelReferenceNumber2TH"]) == false) gblmasterBillerAndTopupBB[i]["LabelReferenceNumber2TH"] = gblmasterBillerAndTopupBB[i]["LabelReferenceNumber2TH"] + ":";
                    for (var j = 0; j < gblmasterBillerAndTopupBB[i]["BillerMiscData"].length; j++) {
                        if (gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref1.LABEL.EN") {
                            if (isEndsWithColon(gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscText"]) == false) gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscText"] = gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscText"] + ":";
                        }
                        if (gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref1.LABEL.TH") {
                            if (isEndsWithColon(gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscText"]) == false) gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscText"] = gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscText"] + ":";
                        }
                        if (gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref2.LABEL.EN") {
                            if (isEndsWithColon(gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscText"]) == false) gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscText"] = gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscText"] + ":";
                        }
                        if (gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref2.LABEL.TH") {
                            if (isEndsWithColon(gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscText"]) == false) gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscText"] = gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscText"] + ":";
                        }
                    }
                }
                //gblBillerCategoriesBBIB = [[0, "Select Category"]];
                gblBillerCategoriesBBIB = [];
                gblBillerCategoriesBBIBTH = [];
                var tempData;
                var tempDateTH;
                for (var i = 0; i < gblmasterBillerAndTopupBB.length; i++) {
                    tempData = [gblmasterBillerAndTopupBB[i]["BillerCategoryID"], gblmasterBillerAndTopupBB[i]["BillerCatgoryNameEN"]];
                    tempDataTH = [gblmasterBillerAndTopupBB[i]["BillerCategoryID"], gblmasterBillerAndTopupBB[i]["BillerCatgoryNameTH"]];
                    var isExist = false;
                    var isExistTH = false;
                    for (var j = 0; j < gblBillerCategoriesBBIB.length; j++) {
                        //alert(gblBillerCategoriesBBIB[j][0] + " and " + gblmasterBillerAndTopupBB[i]["BillerCategoryID"]);
                        if (gblBillerCategoriesBBIB[j][0] == tempData[0]) {
                            isExist = true;
                            break;
                        }
                        if (gblBillerCategoriesBBIBTH[j][0] == tempDataTH[0]) {
                            isExistTH = true;
                            break;
                        }
                    }
                    if (isExist == false) {
                        gblBillerCategoriesBBIB.push(tempData);
                    }
                    if (isExist == false) {
                        gblBillerCategoriesBBIBTH.push(tempDataTH);
                    }
                }
                gblBillerCategoriesBBIB.sort(function(a, b) {
                    if (a[1] > b[1]) return 1;
                    if (a[1] < b[1]) return -1;
                    return 0;
                });
                gblBillerCategoriesBBIBTH.sort(function(a, b) {
                    if (a[1] > b[1]) return 1;
                    if (a[1] < b[1]) return -1;
                    return 0;
                });
                var currLocale = kony.i18n.getCurrentLocale();
                if (currLocale == "en_US") {
                    gblBillerCategoriesBBIB.unshift([0, kony.i18n.getLocalizedString("keyBillPaymentSelectCategory")]);
                    kony.i18n.setCurrentLocaleAsync("th_TH", onsuccesscallbackLocaleChange, onfailurecallback, "");
                } else {
                    gblBillerCategoriesBBIBTH.unshift([0, kony.i18n.getLocalizedString("keyBillPaymentSelectCategory")]);
                    kony.i18n.setCurrentLocaleAsync("en_US", onsuccesscallbackLocaleChangeTH, onfailurecallback, "");
                }
                toSetRef1Ref2();
                showAccountListBB();
            }
        } else {
            dismissLoadingScreenPopup();
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}

function getMasterBillerForBB(status, callBackResponse) {
    showLoadingScreenPopup();
    if (status == 400) {
        if (callBackResponse["opstatus"] == 0 || callBackResponse["opstatus"] == "0") {
            if (callBackResponse["StatusCode"] == 0 || callBackResponse["StatusCode"] == "0") {
                var length = callBackResponse["MasterBillerInqRs"].length;
                //alert(length + "  topups length");
                for (var i = 0; i < length; i++) {
                    var noOfChannels = callBackResponse["MasterBillerInqRs"][i]["ValidChannel"].length;
                    for (var j = 0; j < noOfChannels; j++) {
                        if (callBackResponse["MasterBillerInqRs"][i]["ValidChannel"][j]["ChannelCode"] == "51") {
                            gblmasterBillerAndTopupBB.push(callBackResponse["MasterBillerInqRs"][i]);
                            break;
                        }
                    }
                }
                for (var i = 0; i < gblmasterBillerAndTopupBB.length; i++) {
                    if (isEndsWithColon(gblmasterBillerAndTopupBB[i]["LabelReferenceNumber1EN"]) == false) gblmasterBillerAndTopupBB[i]["LabelReferenceNumber1EN"] = gblmasterBillerAndTopupBB[i]["LabelReferenceNumber1EN"] + ":";
                    if (isEndsWithColon(gblmasterBillerAndTopupBB[i]["LabelReferenceNumber1TH"]) == false) gblmasterBillerAndTopupBB[i]["LabelReferenceNumber1TH"] = gblmasterBillerAndTopupBB[i]["LabelReferenceNumber1TH"] + ":";
                    if (isEndsWithColon(gblmasterBillerAndTopupBB[i]["LabelReferenceNumber2EN"]) == false) gblmasterBillerAndTopupBB[i]["LabelReferenceNumber2EN"] = gblmasterBillerAndTopupBB[i]["LabelReferenceNumber2EN"] + ":";
                    if (isEndsWithColon(gblmasterBillerAndTopupBB[i]["LabelReferenceNumber2TH"]) == false) gblmasterBillerAndTopupBB[i]["LabelReferenceNumber2TH"] = gblmasterBillerAndTopupBB[i]["LabelReferenceNumber2TH"] + ":";
                    for (var j = 0; j < gblmasterBillerAndTopupBB[i]["BillerMiscData"].length; j++) {
                        if (gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref1.LABEL.EN") {
                            if (isEndsWithColon(gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscText"]) == false) gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscText"] = gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscText"] + ":";
                        }
                        if (gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref1.LABEL.TH") {
                            if (isEndsWithColon(gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscText"]) == false) gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscText"] = gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscText"] + ":";
                        }
                        if (gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref2.LABEL.EN") {
                            if (isEndsWithColon(gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscText"]) == false) gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscText"] = gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscText"] + ":";
                        }
                        if (gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref2.LABEL.TH") {
                            if (isEndsWithColon(gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscText"]) == false) gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscText"] = gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscText"] + ":";
                        }
                    }
                }
                //gblBillerCategoriesBBIB = [[0, "Select Category"]];
                gblBillerCategoriesBBIB = [];
                gblBillerCategoriesBBIBTH = [];
                var tempData;
                var tempDateTH;
                for (var i = 0; i < gblmasterBillerAndTopupBB.length; i++) {
                    tempData = [gblmasterBillerAndTopupBB[i]["BillerCategoryID"], gblmasterBillerAndTopupBB[i]["BillerCatgoryNameEN"]];
                    tempDataTH = [gblmasterBillerAndTopupBB[i]["BillerCategoryID"], gblmasterBillerAndTopupBB[i]["BillerCatgoryNameTH"]];
                    var isExist = false;
                    var isExistTH = false;
                    for (var j = 0; j < gblBillerCategoriesBBIB.length; j++) {
                        //alert(gblBillerCategoriesBBIB[j][0] + " and " + gblmasterBillerAndTopupBB[i]["BillerCategoryID"]);
                        if (gblBillerCategoriesBBIB[j][0] == tempData[0]) {
                            isExist = true;
                            break;
                        }
                        if (gblBillerCategoriesBBIBTH[j][0] == tempDataTH[0]) {
                            isExistTH = true;
                            break;
                        }
                    }
                    if (isExist == false) {
                        gblBillerCategoriesBBIB.push(tempData);
                    }
                    if (isExist == false) {
                        gblBillerCategoriesBBIBTH.push(tempDataTH);
                    }
                }
                gblBillerCategoriesBBIB.sort(function(a, b) {
                    if (a[1] > b[1]) return 1;
                    if (a[1] < b[1]) return -1;
                    return 0;
                });
                gblBillerCategoriesBBIBTH.sort(function(a, b) {
                    if (a[1] > b[1]) return 1;
                    if (a[1] < b[1]) return -1;
                    return 0;
                });
                var currLocale = kony.i18n.getCurrentLocale();
                if (currLocale == "en_US") {
                    gblBillerCategoriesBBIB.unshift([0, kony.i18n.getLocalizedString("keyBillPaymentSelectCategory")]);
                    kony.i18n.setCurrentLocaleAsync("th_TH", onsuccesscallbackLocaleChange, onfailurecallback, "");
                } else {
                    gblBillerCategoriesBBIBTH.unshift([0, kony.i18n.getLocalizedString("keyBillPaymentSelectCategory")]);
                    kony.i18n.setCurrentLocaleAsync("en_US", onsuccesscallbackLocaleChangeTH, onfailurecallback, "");
                }
                if (kony.i18n.getLocalizedString("en_US")) {
                    frmIBBeepAndBillList.cbxSelectCat.masterData = gblBillerCategoriesBBIB;
                    frmIBBeepAndBillApply.cbxSelectCat.masterData = gblBillerCategoriesBBIB;
                } else {
                    frmIBBeepAndBillList.cbxSelectCat.masterData = gblBillerCategoriesBBIBTH;
                    frmIBBeepAndBillApply.cbxSelectCat.masterData = gblBillerCategoriesBBIBTH;
                }
                if (gblUpdateMasterDataIB == false) {
                    showLoadingScreenPopup();
                    updateAccountListBB();
                } else {
                    gblUpdateMasterDataIB = false;
                    var inputParam = {};
                    inputParam["rmNum"] = "";
                    inputParam["PIN"] = messageObj["pinCode"];
                    inputParam["finTxnAmount"] = messageObj["finTxnAmount"];
                    inputParam["ref1"] = messageObj["ref1"];
                    inputParam["BillerName"] = getBillerNameIB(messageObj["BBCompcode"]);
                    invokeServiceSecureAsync("CustomerBBPaymentStatusInquiry", inputParam, customerBBPayemntStatusInquiryIBCallBack);
                }
            }
        } else {
            dismissLoadingScreenPopup();
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}

function onsuccesscallbackLocaleChange() {
    gblBillerCategoriesBBIBTH.unshift([0, kony.i18n.getLocalizedString("keyBillPaymentSelectCategory")]);
    kony.i18n.setCurrentLocaleAsync("en_US", onsuccesscallbackBB, onfailurecallbackBB, "");
}

function onsuccesscallbackLocaleChangeTH() {
    gblBillerCategoriesBBIB.unshift([0, kony.i18n.getLocalizedString("keyBillPaymentSelectCategory")]);
    kony.i18n.setCurrentLocaleAsync("th_TH", onsuccesscallbackBB, onfailurecallbackBB, "");
}

function onsuccesscallbackBB() {}

function onfailurecallbackBB() {}

function onClickPdfImgForBBTandCIB(filetype) {
    var inputParam = {};
    var pdfImagedata = {};
    var fileType = filetype;
    var pdfImagedata = {
        "tctext": frmIBBBTnC.lblTnC.text
    }
    inputParam["filetype"] = fileType;
    inputParam["templatename"] = "TermsAndConditions";
    inputParam["datajson"] = JSON.stringify(pdfImagedata, "", "");
    invokeServiceSecureAsync("generatePdfImage", inputParam, ibRenderFileCallbackfunction);
}
//apply
function onClickGeneratePDFImageForBB(filetype) {
    var inputParam = {};
    var scheduleDetails = "";
    var pdfImagedata = {};
    fileType = filetype;
    var accNo = frmIBBeepAndBillConfAndComp.lblAccNumValue.text;
    accNo = removeUnwatedSymbols(accNo);
    var pdfImagedata = {
        "AccountNo": "xxx-x-" + accNo.substring(4, 9) + "-x",
        "AccountName": frmIBBeepAndBillConfAndComp.lblAccHolderName.text,
        "BillerName": frmIBBeepAndBillConfAndComp.lblCompCode.text,
        "Ref1Label": frmIBBeepAndBillConfAndComp.lblViewBillerRef1.text,
        "Ref1Val": frmIBBeepAndBillConfAndComp.lblViewBillerRef1Value.text,
        "localeId": kony.i18n.getCurrentLocale()
    }
    if (frmIBBeepAndBillConfAndComp.hbxViewBillerRef2.isVisible == true) {
        pdfImagedata["Ref2Label"] = frmIBBeepAndBillConfAndComp.lblViewBillerRef2.text;
        pdfImagedata["Ref2Val"] = frmIBBeepAndBillConfAndComp.lblViewBillerRef2Value.text;
    } else {
        pdfImagedata["Ref2Label"] = "";
        pdfImagedata["Ref2Val"] = "";
    }
    inputParam["filetype"] = fileType;
    inputParam["templatename"] = "ApplyBeepAndBillTemplate";
    inputParam["outputtemplatename"] = "Apply_Beep_And_Bill_Set_Details_" + kony.os.date("DDMMYYYY");
    inputParam["localeId"] = kony.i18n.getCurrentLocale();
    inputParam["datajson"] = JSON.stringify(pdfImagedata, "", "");
    invokeServiceSecureAsync("generatePdfImage", inputParam, ibRenderFileCallbackfunction);
}

function onClickGeneratePDFImageForBBEx(filetype) {
    var inputParam = {};
    var scheduleDetails = "";
    var pdfImagedata = {};
    fileType = filetype;
    var accNo = frmIBBeepAndBillExecuteConfComp.lblAccountNo.text;
    accNo = removeUnwatedSymbols(accNo);
    var pdfImagedata = {
        "AccountNo": "xxx-x-" + accNo.substring(4, 9) + "-x",
        "AccountName": frmIBBeepAndBillExecuteConfComp.lblName.text,
        "BillerName": frmIBBeepAndBillExecuteConfComp.lblNickname.text,
        "Ref1Label": frmIBBeepAndBillExecuteConfComp.lblRef1.text,
        "Ref1Value": frmIBBeepAndBillExecuteConfComp.lblRef1Value.text,
        "Amount": frmIBBeepAndBillExecuteConfComp.lblAmountVal.text,
        "Fee": frmIBBeepAndBillExecuteConfComp.lblFeeVal.text,
        "PaymentDate": frmIBBeepAndBillExecuteConfComp.lblPaymentDateVal.text,
        "TransactionRefNo": frmIBBeepAndBillExecuteConfComp.lblTransactionIDVal.text,
        "localeId": kony.i18n.getCurrentLocale()
    }
    if (frmIBBeepAndBillExecuteConfComp.hbxRef2.isVisible == true) {
        pdfImagedata["Ref2Label"] = frmIBBeepAndBillExecuteConfComp.lblRef2.text;
        pdfImagedata["Ref2Value"] = frmIBBeepAndBillExecuteConfComp.lblRef2Value.text;
    } else {
        pdfImagedata["Ref2Label"] = "";
        pdfImagedata["Ref2Value"] = "";
    }
    inputParam["filetype"] = fileType;
    inputParam["templatename"] = "ExecuteBeepAndBillPaymentTemplate";
    inputParam["outputtemplatename"] = "Execute_Beep_And_Bill_Set_Details_" + kony.os.date("DDMMYYYY");
    inputParam["localeId"] = kony.i18n.getCurrentLocale();
    inputParam["datajson"] = JSON.stringify(pdfImagedata, "", "");
    invokeServiceSecureAsync("generatePdfImage", inputParam, ibRenderFileCallbackfunction);
}

function getBBRefrenceNoIB() {
    var inputParam = {}
    inputParam["transRefType"] = "BB";
    showLoadingScreenPopup();
    invokeServiceSecureAsync("generateTransferRefNo", inputParam, callBackgetBBRefrenceNoIB)
}

function callBackgetBBRefrenceNoIB(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            gblBBRefNo = resulttable.transRefNum;
            frmIBBeepAndBillExecuteConfComp.lblXrefNoVal.text = gblBBRefNo + "00";
            populateOnExecuteBBIBPage();
            dismissLoadingScreenPopup();
            //checking transfer type
        } else {
            dismissLoadingScreenPopup();
            alert(resulttable["errMsg"]);
        }
    }
}

function createFinActivityLogObjBB(errorCd, tellerId) {
    GBLFINANACIALACTIVITYLOG.crmId = gblcrmId;
    GBLFINANACIALACTIVITYLOG.finTxnRefId = gblBBRefNo + "00";
    GBLFINANACIALACTIVITYLOG.tellerId = tellerId;
    //var date = frmIBBeepAndBillExecuteConfComp.lblPaymentDateVal.text;
    //if(date != null) date = replaceCommon(date, "/", "");
    //else date = "000000";
    GBLFINANACIALACTIVITYLOG.finTxnDate = frmIBBeepAndBillExecuteConfComp.lblPaymentDateVal.text;;
    var feeAmount = frmIBBeepAndBillExecuteConfComp.lblFeeVal.text;
    feeAmount = feeAmount.substring(0, feeAmount.length - 1)
    GBLFINANACIALACTIVITYLOG.finTxnFee = feeAmount;
    GBLFINANACIALACTIVITYLOG.billerRef1 = frmIBBeepAndBillExecuteConfComp.lblRef1Value.text;
    GBLFINANACIALACTIVITYLOG.billerRef2 = frmIBBeepAndBillExecuteConfComp.lblRef2Value.text;
    GBLFINANACIALACTIVITYLOG.billerCommCode = gblBillerCompCodeBB;
    GBLFINANACIALACTIVITYLOG.beepAndBillTxnId = frmIBBeepAndBillExecuteConfComp.lblTransactionIDVal.text;
    var DueDate = frmIBBeepAndBillExecuteConfComp.lblDueDateVal.text;
    GBLFINANACIALACTIVITYLOG.dueDate = DueDate.split("/").join("-");
    GBLFINANACIALACTIVITYLOG.activityTypeId = "034";
    GBLFINANACIALACTIVITYLOG.channelId = "01";
    GBLFINANACIALACTIVITYLOG.fromAcctId = removeUnwatedSymbols(frmIBBeepAndBillExecuteConfComp.lblAccountNo.text);
    GBLFINANACIALACTIVITYLOG.fromAcctName = frmIBBeepAndBillExecuteConfComp.lblFromName.text;
    GBLFINANACIALACTIVITYLOG.fromAcctNickname = frmIBBeepAndBillExecuteConfComp.lblNickname.text;
    var masterBillerRec = "";
    for (var i = 0; i < gblmasterBillerAndTopupBB.length; i++) {
        //alert(gblmasterBillerAndTopupBB[i]["BillerCompcode"] + " == " + gblBillerCompCodeBB )
        if (gblmasterBillerAndTopupBB[i]["BillerCompcode"] == gblBillerCompCodeBB) {
            if (gblmasterBillerAndTopupBB[i]["BillerMethod"] == "2" || gblmasterBillerAndTopupBB[i]["BillerMethod"] == "3") GBLFINANACIALACTIVITYLOG.toAcctId = frmIBBeepAndBillExecuteConfComp.lblRef1Value.text;
            else GBLFINANACIALACTIVITYLOG.toAcctId = gblmasterBillerAndTopupBB[i]["ToAccountKey"];
            if (kony.i18n.getCurrentLocale() == "en_US") GBLFINANACIALACTIVITYLOG.toAcctName = gblmasterBillerAndTopupBB[i]["BillerNameEN"];
            else GBLFINANACIALACTIVITYLOG.toAcctName = gblmasterBillerAndTopupBB[i]["BillerNameTH"];
            //alert(GBLFINANACIALACTIVITYLOG.toAcctId + " and " + GBLFINANACIALACTIVITYLOG.toAcctName )
            break;
        }
    }
    //GBLFINANACIALACTIVITYLOG.toAcctId = "";//removeUnwatedSymbols(frmSSSExecute.lblSendToAccNo.text);
    //GBLFINANACIALACTIVITYLOG.toAcctName = "";//exeToAcctType;
    GBLFINANACIALACTIVITYLOG.toAcctNickname = ""; //exeToAcctName;
    //	GBLFINANACIALACTIVITYLOG.bankCD = "11";
    GBLFINANACIALACTIVITYLOG.toBankAcctCd = "11";
    var amount = frmIBBeepAndBillExecuteConfComp.lblAmountVal.text;
    amount = amount.substring(0, amount.length - 1);
    GBLFINANACIALACTIVITYLOG.finTxnAmount = "" + amount;
    GBLFINANACIALACTIVITYLOG.finTxnBalance = ""; //retailLiqTrnsAdd["FromAmt"];
    if (gblPmtStatus == "S" || gblPmtStatus == "s") {
        GBLFINANACIALACTIVITYLOG.finTxnStatus = "01";
        GBLFINANACIALACTIVITYLOG.clearingStatus = "01";
    } else {
        GBLFINANACIALACTIVITYLOG.finTxnStatus = "02";
        GBLFINANACIALACTIVITYLOG.clearingStatus = "02";
    }
    GBLFINANACIALACTIVITYLOG.finTxnStatus = "01";
    GBLFINANACIALACTIVITYLOG.clearingStatus = "01";
    //if(retailLiqTrnsAdd["statusCode"] != 0) {
    GBLFINANACIALACTIVITYLOG.errorCd = errorCd;
    //} else {
    //GBLFINANACIALACTIVITYLOG.errorCd = retailLiqTrnsAdd["addStatusCode"];
    //}
    GBLFINANACIALACTIVITYLOG.eventId = gblBBRefNo.replace("BB", "");
    GBLFINANACIALACTIVITYLOG.txnType = "007";
    //GBLFINANACIALACTIVITYLOG.dueDate = frmIBBeepAndBillExecuteConfComp.lblDueDateVal.text + "";
    //GBLFINANACIALACTIVITYLOG.beepAndBillTxnId = frmIBBeepAndBillExecuteConfComp.lblTransactionIDVal.text + "";
    //
    GBLFINANACIALACTIVITYLOG.finFlexValues1 = gblPHONENUMBER;
    GBLFINANACIALACTIVITYLOG.finFlexValues2 = "" + amount;
    GBLFINANACIALACTIVITYLOG.finFlexValues3 = frmIBBeepAndBillExecuteConfComp.lblAccountNo.text;
    GBLFINANACIALACTIVITYLOG.finFlexValues4 = "";
    GBLFINANACIALACTIVITYLOG.finFlexValues5 = "";
    financialActivityLogServiceCall(GBLFINANACIALACTIVITYLOG);
}

function callNotificationAddServiceBB() {
    var inputparam = [];
    //inputparam["channelName"] = "Internet Banking";
    inputparam["notificationType"] = "Email"; // check this : which value we have to pass
    //inputparam["phoneNumber"] = gblPHONENUMBER;
    //inputparam["emailId"] = gblEmailId;
    inputparam["customerName"] = gblCustomerName;
    inputparam["fromAccount"] = frmIBBeepAndBillExecuteConfComp.lblAccountNo.text;
    // need partial masked account number, 5-9 digits unmasked
    inputparam["fromAcctNick"] = frmIBBeepAndBillExecuteConfComp.lblFromName.text;
    inputparam["billerNick"] = frmIBBeepAndBillExecuteConfComp.lblNickname.text;
    inputparam["billerNameCompCode"] = frmIBBeepAndBillExecuteConfComp.lblCompCode.text;
    inputparam["ref1Label"] = frmIBBeepAndBillExecuteConfComp.lblRef1.text;
    inputparam["ref1Value"] = frmIBBeepAndBillExecuteConfComp.lblRef1Value.text;
    if (frmIBBeepAndBillExecuteConfComp.hbxRef2.isVisible == true) {
        inputparam["ref2Label"] = frmIBBeepAndBillExecuteConfComp.lblRef2.text;
        inputparam["ref2Value"] = frmIBBeepAndBillExecuteConfComp.lblRef2Value.text;
    }
    inputparam["amount"] = frmIBBeepAndBillExecuteConfComp.lblAmountVal.text;
    inputparam["fee"] = frmIBBeepAndBillExecuteConfComp.lblFeeVal.text;
    inputparam["initiationDt"] = frmIBBeepAndBillExecuteConfComp.lblPaymentDateVal.text;
    //inputparam["todayDate"] = getTodaysDate(); 
    //inputparam["todayTime"] = ""; // why blank ?
    inputparam["transactoinRefNum"] = frmIBBeepAndBillExecuteConfComp.lblXrefNoVal.text;
    //inputparam["channelID"] = "01";
    inputparam["source"] = "sendEmailForBBExecute";
    inputparam["Locale"] = kony.i18n.getCurrentLocale();
    invokeServiceSecureAsync("NotificationAdd", inputparam, callBackNotificationAddService)
}

function toSetRef1Ref2() {
    var tmpRef2BB = "N";
    var tmpRef2Type = "";
    var tmpRef2Data = "";
    //alert(collectiondata[i]["BillerMiscData"].length + " collection data")
    for (var i = 0; i < gblmasterBillerAndTopupBB.length; i++) {
        if (gblBillerCompCodeBB == gblmasterBillerAndTopupBB[i]["BillerCompcode"]) {
            if (gblmasterBillerAndTopupBB[i]["BillerMiscData"].length > 0) {
                for (var j = 0; j < gblmasterBillerAndTopupBB[i]["BillerMiscData"].length; j++) {
                    if (gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscName"] == "BB.IsRequiredRefNumber2") {
                        tmpRef2BB = gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscText"];
                    }
                    if (gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref2.INPUT.TYPE") {
                        tmpRef2Type = gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscText"];
                    }
                    if (gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref2.INPUT.VALUE") {
                        tmpRef2Data = gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscText"];
                    }
                }
            }
        }
    }
    if (tmpRef2BB == "Y" || tmpRef2BB == "y") {
        frmIBBeepAndBillApplyCW.hbxBillerRef2.setVisibility(true);
        if (tmpRef2Type == "Dropdown") {
            frmIBBeepAndBillApplyCW.txtAddBillerRef2.setVisibility(false);
            frmIBBeepAndBillApplyCW.comboboxRef2.setVisibility(true);
            var masterData = [
                [0, "" + kony.i18n.getLocalizedString("keySelectRef2BB")]
            ];
            var tempData;
            var ref2List = tmpRef2Data.split(",");
            for (var i = 0; i < ref2List.length; i++) {
                tempData = [i + 1, ref2List[i]];
                masterData.push(tempData);
            }
            frmIBBeepAndBillApplyCW.comboboxRef2.masterData = masterData;
        }
        //frmIBBeepAndBillApplyCW.txtAddBillerRef2.text = currForm.segSuggestedBillersList.selectedItems[0].hdnRef2Data.text;
        frmIBBeepAndBillApplyCW.hbxBillerRef2.setVisibility(true);
        frmIBBeepAndBillApplyCW.line44473234056543.isVisible = true;
        frmIBBeepAndBillApplyCW.hbxBillerRef2.setVisibility(true);
        frmIBBeepAndBillApplyCW.comboboxRef2.setVisibility(true);
    } else {
        frmIBBeepAndBillApplyCW.txtAddBillerRef2.setVisibility(true);
        frmIBBeepAndBillApplyCW.hbxBillerRef2.setVisibility(false);
        frmIBBeepAndBillApplyCW.line44473234056543.isVisible = false;
    }
}

function onClickBillerLink() {
    frmIBBeepAndBillApplyCW.imgAddBillerLogo.src = frmIBMyBillersHome.imgViewBillerLogo.src;
    frmIBBeepAndBillApplyCW.txtAddBillerNickName.text = frmIBMyBillersHome.lblViewBillerName.text;
    frmIBBeepAndBillApplyCW.lblAddBillerCompCode.text = frmIBMyBillersHome.lblViewBillerNameCompCode.text;
    var str = frmIBBeepAndBillApplyCW.lblAddBillerCompCode.text;
    gblBillerCompCodeBB = str.substring(str.length - 5, str.length - 1);
    frmIBBeepAndBillApplyCW.lblAddBillerRef1.text = frmIBMyBillersHome.lblViewBillerRef1.text;
    frmIBBeepAndBillApplyCW.txtAddBillerRef1.text = frmIBMyBillersHome.lblViewBillerRef1Value.text;
    //if(frmIBMyBillersHome.hbxViewBillerRef2.isVisible == true){
    //alert(frmIBMyBillersHome.hbxViewBillerRef2.isVisible + " " + frmIBMyBillersHome.lblViewBillerRef1.isVisible)
    frmIBBeepAndBillApplyCW.lblAddBillerRef2.text = frmIBMyBillersHome.lblViewBillerRef1.text;
    frmIBBeepAndBillApplyCW.txtAddBillerRef2.text = frmIBMyBillersHome.lblViewBillerRef2Value.text;
    //}else{
    //	frmIBBeepAndBillApplyCW.hbxBillerRef2.isVisible = false;
    //}
    //if(gblCustomerBBIBInqRs.length == 0){
    //	updateBeepAndBillListIB();
    //}
    if (gblmasterBillerAndTopupBB.length == 0) {
        updateMasterBillerAndTopup();
    } else {
        toSetRef1Ref2();
        showAccountListBB();
    }
}

function getMyBillListIBBB() {
    var inputParams = {
        crmId: gblcrmId
            //clientDate: getCurrentDate() not working on IE 8
    };
    showLoadingScreenPopup();
    invokeServiceSecureAsync("customerBillInquiry", inputParams, startMyBillListIBBBServiceAsyncCallback);
}

function startMyBillListIBBBServiceAsyncCallback(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == "0") {
            frmIBBeepAndBillApply.segBillersList.removeAll();
            var responseData = callBackResponse["CustomerBillInqRs"];
            if (responseData.length > 0) {
                frmIBBeepAndBillApply.hbxSugMore.setVisibility(true);
                frmIBBeepAndBillApply.lblNoBills.setVisibility(false);
                populateMyBillBB(callBackResponse["CustomerBillInqRs"]);
            } else {
                frmIBBeepAndBillApply.segBillersList.removeAll();
                frmIBBeepAndBillApply.hbxMore.setVisibility(false);
                frmIBBeepAndBillApply.lblNoBills.setVisibility(true);
                gblBillersForSearch = [];
                dismissLoadingScreenPopup();
                frmIBBeepAndBillApply.show();
                //frmIBBeepAndBillApply.show();
                //alert(kony.i18n.getLocalizedString("keyaddbillerstolist"));
                //alert(kony.i18n.getLocalizedString("ECGenericError"));
            }
        }
    } else {
        if (status == 300) {
            frmIBBeepAndBillApply.segBillersList.removeAll();
            dismissLoadingScreenPopup();
            alert(kony.i18n.getLocalizedString("ECGenericError"));
        }
    }
}

function populateMyBillBB(collectionData) {
    search = "";
    search = frmIBMyBillersHome.tbxSearch.text;
    var searchtxt = search.toLowerCase();
    var i = 0;
    var tmpSearchList = [];
    var tmpCatList = [];
    tmpSearchList.length = 0;
    tmpCatList.length = 0;
    var tmpRef2label = "";
    var tmpRef2Len = 0;
    var billername = "";
    var ref1label = "";
    var ref2label = "";
    var tmpRef2label = "";
    var ref2val = ""
    var tmpIsRef2Req = "";
    if (kony.i18n.getCurrentLocale() == "en_US") {
        billername = "BillerNameEN";
        ref1label = "LabelReferenceNumber1EN";
        ref2label = "LabelReferenceNumber2EN";
    } else if (kony.i18n.getCurrentLocale() == "th_TH") {
        billername = "BillerNameTH";
        ref1label = "LabelReferenceNumber1TH";
        ref2label = "LabelReferenceNumber2TH";
    }
    var ref1EN = "LabelReferenceNumber1EN"
    var ref2EN = "LabelReferenceNumber2EN"
    var ref1TH = "LabelReferenceNumber1TH"
    var ref2TH = "LabelReferenceNumber2TH"
        //   if(!isSearchedBiller && !isCatFilteredBiller )
        //   {
        //  var i = 0;
    gblmyBillerListBB.length = 0;
    for (i = 0; i < collectionData.length; i++) {
        if (collectionData[i]["BeepNBillFlag"] == "Y" || collectionData[i]["BeepNBillFlag"] == "y") {
            //if(true){
            tmpIsRef2Req = collectionData[i]["IsRequiredRefNumber2Pay"];
            if (tmpIsRef2Req == "Y" || tmpIsRef2Req == "y") {
                ref2val = collectionData[i]["ReferenceNumber2"];
            } else {
                ref2val = "";
                collectionData[i][ref2label] = "";
            }
            var ref2Len = 0;
            if (collectionData[i]["Ref2Len"] != undefined) {
                ref2Len = collectionData[i]["Ref2Len"];
            }
            if (isEndsWithColon(collectionData[i][ref1label]) == false) collectionData[i][ref1label] = collectionData[i][ref1label] + ":";
            if (isEndsWithColon(collectionData[i][ref2label]) == false) collectionData[i][ref2label] = collectionData[i][ref2label] + ":";
            var imagesUrl = BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + collectionData[i]["BillerCompcode"] + "&modIdentifier=MyBillers"
            var tempRecord = {
                "crmId": collectionData[i]["CRMID"],
                "lblBillerNickname": {
                    "text": collectionData[i]["BillerNickName"]
                },
                "hdnIsReqRef2Pay": {
                    "text": collectionData[i]["IsRequiredRefNumber2Pay"]
                },
                "hdnIsReqRef2Add": {
                    "text": collectionData[i]["IsRequiredRefNumber2Pay"]
                },
                "lblBillerName": {
                    "text": collectionData[i][billername] + "(" + collectionData[i]["BillerCompcode"] + ")"
                },
                "hdnBillerNameEN": {
                    "text": collectionData[i]["BillerNameEN"] + "(" + collectionData[i]["BillerCompcode"] + ")"
                },
                "hdnBillerNameTH": {
                    "text": collectionData[i]["BillerNameTH"] + "(" + collectionData[i]["BillerCompcode"] + ")"
                },
                "lblRef1Value": {
                    "text": collectionData[i]["ReferenceNumber1"]
                },
                "lblRef2Value": {
                    "text": ref2val
                },
                "imgBillerLogo": {
                    "src": imagesUrl
                },
                "imgBillersRtArrow": {
                    "src": "bg_arrow_right.png"
                },
                "lblRef1": {
                    "text": collectionData[i][ref1label]
                },
                "lblRef2": {
                    "text": collectionData[i][ref2label]
                },
                "hdnRef1EN": {
                    "text": collectionData[i][ref1EN]
                },
                "hdnRef2EN": {
                    "text": collectionData[i][ref2EN]
                },
                "hdnRef1TH": {
                    "text": collectionData[i][ref1TH]
                },
                "hdnRef2TH": {
                    "text": collectionData[i][ref2TH]
                },
                "Ref1Len": {
                    "text": collectionData[i]["Ref1Len"]
                },
                "Ref2Len": {
                    "text": collectionData[i]["Ref2Len"]
                },
                "BillerID": {
                    "text": collectionData[i]["BillerID"]
                },
                "BillerCompCode": {
                    "text": collectionData[i]["BillerCompcode"]
                },
                "BillerMethod": {
                    "text": collectionData[i]["BillerMethod"]
                },
                "BeepndBill": {
                    "text": collectionData[i]["BeepNBillFlag"]
                },
                "CustomerBillerID": {
                    "text": collectionData[i]["CustomerBillID"]
                },
                "BillerCategoryID": {
                    "text": collectionData[i]["BillerCategoryID"]
                }
            }
            gblmyBillerListBB.push(tempRecord);
        }
    }
    if (gblmyBillerListBB.length == 0) {
        //kony.application.getCurrentForm().segBillersList.removeAll();
        frmIBBeepAndBillApply.lblMyBills.setVisibility(false);
        gblBillersForSearch = [];
        frmIBBeepAndBillApply.hbxMore.setVisibility(false);
        frmIBBeepAndBillApply.lblNoBills.setVisibility(true)
        frmIBBeepAndBillApply.show();
        //alert(kony.i18n.getLocalizedString("keyaddbillerstolist"));
    } else {
        frmIBBeepAndBillApply.lblMyBills.setVisibility(true);
        frmIBBeepAndBillApply.segBillersList.removeAll();
        frmIBBeepAndBillApply.hbxMore.setVisibility(true);
        gblBBMore = 0;
        gblBillersForSearch = gblmyBillerListBB;
        onMyBBSelectBillersMoreDynamicIB();
        frmIBBeepAndBillApply.show();
        //frmIBBeepAndBillApply.show();
        //frmIBBeepAndBillApply.segBillersList.setData(gblmyBillerListBB);
        //earchCustomBillsIB();
    }
    dismissLoadingScreenPopup();
}

function onSelectSegBillListIBBB() {
    gblOnClickSug = false;
    var indexOfSelectedRow = frmIBBeepAndBillApply.segBillersList.selectedIndex[1];
    var indexOfSelectedIndex = frmIBBeepAndBillApply.segBillersList.selectedItems[0];
    //frmBBMyBeepAndBill.imgBB.src = bill_img01.png;
    //alert("lblCOnfirmNickName : " + indexOfSelectedRow);
    //frmBBMyBeepAndBill.lblSelectedIndex.text = indexOfSelectedRow;
    //var currForm = kony.application.getCurrentForm();
    gblBillerMethodBB = frmIBBeepAndBillApply.segBillersList.selectedItems[0].BillerMethod;
    gblBillerCompCodeBB = frmIBBeepAndBillApply.segBillersList.selectedItems[0].BillerCompCode.text;
    gblRef1LenBB = frmIBBeepAndBillApply.segBillersList.selectedItems[0].Ref1Len.text;
    gblRef2LenBB = frmIBBeepAndBillApply.segBillersList.selectedItems[0].Ref2Len.text;
    frmIBBeepAndBillApplyCW.imgAddBillerLogo.src = indexOfSelectedIndex.imgBillerLogo.src;
    frmIBBeepAndBillApplyCW.txtAddBillerNickName.text = indexOfSelectedIndex.lblBillerNickname.text;
    frmIBBeepAndBillApplyCW.lblAddBillerCompCode.text = indexOfSelectedIndex.lblBillerName.text;
    if (kony.i18n.getCurrentLocale() == "en_US") {
        frmIBBeepAndBillApplyCW.lblAddBillerRef1.text = indexOfSelectedIndex.hdnRef1EN.text;
        frmIBBeepAndBillApplyCW.lblAddBillerCompCode.text = indexOfSelectedIndex.hdnBillerNameEN.text;
    } else {
        frmIBBeepAndBillApplyCW.lblAddBillerRef1.text = indexOfSelectedIndex.hdnRef1TH.text;
        frmIBBeepAndBillApplyCW.lblAddBillerCompCode.text = indexOfSelectedIndex.hdnBillerNameTH.text;
    }
    //frmIBBeepAndBillApply.lblAddBillerRef1Val.text = indexOfSelectedIndex.lblRef1Value.text;
    frmIBBeepAndBillApplyCW.txtAddBillerRef1.text = indexOfSelectedIndex.lblRef1Value.text;
    //alert(indexOfSelectedIndex.isRef2Pay.text)
    /*if (indexOfSelectedIndex.hdnIsReqRef2Pay.text == "Y" || indexOfSelectedIndex.hdnIsReqRef2Pay.text == "y") {
        frmIBBeepAndBillApplyCW.hbxBillerRef2.setVisibility(true);
        
        if(kony.i18n.getCurrentLocale() == "en_US")
    		frmIBBeepAndBillApplyCW.lblAddBillerRef2.text = indexOfSelectedIndex.hdnRef2EN.text+":";
		else
			frmIBBeepAndBillApplyCW.lblAddBillerRef2.text = indexOfSelectedIndex.hdnRef2TH.text +":";
        
        frmIBBeepAndBillApplyCW.comboboxRef2.setVisibility(false);
        frmIBBeepAndBillApplyCW.txtAddBillerRef2.setVisibility(true);
        //frmIBBeepAndBillApply.lblAddBillerRef2Val.text = indexOfSelectedIndex.lblRef2Value.text;
        frmIBBeepAndBillApplyCW.txtAddBillerRef2.text = indexOfSelectedIndex.lblRef2Value.text;

        frmIBBeepAndBillApplyCW.line44473234056543.isVisible = true;
    } else {
        frmIBBeepAndBillApplyCW.hbxBillerRef2.setVisibility(false);
        frmIBBeepAndBillApplyCW.line44473234056543.isVisible = false;
    }*/
    frmIBBeepAndBillApplyCW.hbxChargeTo.setVisibility(false);
}

function IBTnCTextBB() {
    var input_param = {};
    input_param["localeCd"] = kony.i18n.getCurrentLocale();
    input_param["moduleKey"] = "BeepBill";
    invokeServiceSecureAsync("readUTFFile", input_param, setIBTnCBB);
}

function setIBTnCBB(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            frmIBBBTnC.lblTnC.text = resulttable["fileContent"];
            frmIBBBTnC.buttonAgree.setEnabled(true);
            //frmIBBeepAndBillTandC.show();
        } else {
            if (kony.i18n.getCurrentLocale() == "th_TH") frmIBBBTnC.lblTnC.text = "";
            else frmIBBBTnC.lblTnC.text = "";
            frmIBBBTnC.buttonAgree.setEnabled(false);
        }
    }
}

function verifyOTPBBComposite() {
    var currForm = kony.application.getCurrentForm()
    var text = currForm.txtBxOTPBB.text;
    var isNumOTP = kony.string.isNumeric(text);
    var txtLen1 = text.length;
    var inputParam = {};
    inputParam["flowspa"] = "false";
    inputParam["channel"] = "wap";
    inputParam["appID"] = "TMB";
    setInputParamsaddBBToListIBComposite(inputParam);
    callNotificationAddServiceIBBBApplyComposite(inputParam);
    setactivityLogServiceCallComposite(inputParam);
    if (gblTokenSwitchFlagBB == false) {
        if (text == "" || text == null) {
            alert(kony.i18n.getLocalizedString("Receipent_alert_correctOTP"));
            return false;
        }
        /* local validation temporarily removed to make the flow consistent with other places 
		if (txtLen1 != gblOTPLENGTH || isNumOTP == false || text == "") {
			showAlert("OTP Should be Numeric and of length " + gblOTPLENGTH , kony.i18n.getLocalizedString("info"));
			return false;
		}*/
        inputParam["gblTokenSwitchFlagBB"] = "false";
        inputParam["verifyPasswordEx_retryCounterVerifyOTP"] = gblVerifyOTP;
        inputParam["verifyPasswordEx_userId"] = gblUserName;
        inputParam["verifyPasswordEx_userStoreId"] = "DefaultStore";
        inputParam["verifyPasswordEx_loginModuleId"] = "IBSMSOTP";
        inputParam["verifyPasswordEx_password"] = text;
        inputParam["password"] = text;
        inputParam["verifyPasswordEx_segmentId"] = "MIB";
        showLoadingScreenPopup();
        frmIBBeepAndBillConfAndComp.txtBxOTPBB.text = "";
        currForm.txtBxOTPBB.text = "";
        //invokeServiceSecureAsync("verifyPasswordEx", inputParam, verifyTokenOTPBBCallback)
        invokeServiceSecureAsync("BeepAndBillApplyJavaService", inputParam, verifyTokenOTPBBCallbackComposite);
    } else {
        if (currForm.tbxToken.text == "" || currForm.tbxToken.text == null) {
            alert(kony.i18n.getLocalizedString("Receipent_tokenId"));
            return false;
        }
        inputParam["gblTokenSwitchFlagBB"] = "true";
        inputParam["verifyTokenEx_loginModuleId"] = "IB_HWTKN";
        inputParam["verifyTokenEx_userStoreId"] = "DefaultStore";
        inputParam["verifyTokenEx_retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
        inputParam["verifyTokenEx_retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
        inputParam["verifyTokenEx_userId"] = gblUserName;
        inputParam["verifyTokenEx_password"] = currForm.tbxToken.text;
        inputParam["password"] = currForm.tbxToken.text;
        inputParam["verifyTokenEx_sessionVal"] = "";
        inputParam["verifyTokenEx_segmentId"] = "segmentId";
        inputParam["verifyTokenEx_segmentIdVal"] = "MIB";
        inputParam["verifyTokenEx_channel"] = "InterNet Banking";
        showLoadingScreenPopup();
        currForm.tbxToken.text == ""
            //invokeServiceSecureAsync("verifyTokenEx", inputParam, verifyTokenOTPBBCallback)
        invokeServiceSecureAsync("BeepAndBillApplyJavaService", inputParam, verifyTokenOTPBBCallbackComposite);
    }
}

function verifyTokenOTPBBCallbackComposite(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            var StatusCode = resulttable["StatusCode"];
            var Severity = resulttable["Severity"];
            var StatusDesc = resulttable["StatusDesc"];
            dismissLoadingScreenPopup();
            //
            gblVerifyOTPCounter = "0";
            var currForm = kony.application.getCurrentForm();
            if (currForm["id"] == 'frmIBBeepAndBillConfAndComp') {
                //addBBToListIB();
                dismissLoadingScreenPopup();
                frmIBBeepAndBillConfAndComp.hbxBillersOTPContainer.setVisibility(false);
                frmIBBeepAndBillConfAndComp.hbxToken.setVisibility(false);
                frmIBBeepAndBillConfAndComp.hbxSuccess.setVisibility(true)
                frmIBBeepAndBillConfAndComp.btnCancel.text = kony.i18n.getLocalizedString("keyReturn");
                frmIBBeepAndBillConfAndComp.btnConfirm.text = kony.i18n.getLocalizedString("keyApplyMore");
                frmIBBeepAndBillConfAndComp.lblHdrtxt.text = kony.i18n.getLocalizedString("Complete");
                frmIBBeepAndBillConfAndComp.btnEditConfirm.setVisibility(false);
                frmIBBeepAndBillConfAndComp.hbxBPSave.setVisibility(true);
                frmIBBeepAndBillConfAndComp.hbxAdv.setVisibility(true);
                if (gblBillerMethodBB["text"] != "1") {
                    //showAlert(kony.i18n.getLocalizedString("keyBBDelAddOfflineMsg"), kony.i18n.getLocalizedString("info"));
                    frmIBBeepAndBillConfAndComp.hbxAddDelOfflineMsg.setVisibility(true);
                } else {
                    frmIBBeepAndBillConfAndComp.hbxAddDelOfflineMsg.setVisibility(false);
                }
                campaginService("img1", "frmIBBeepAndBillConfAndComp", "I");
            } else if (currForm.id == 'frmIBBeepAndBillExecuteConfComp') {
                //
                /*var inputParam = {};
					inputParam["rmNum"] = "";
					inputParam["PINcode"] = messageObj["pinCode"];
					invokeServiceSecureAsync("customerBBPaymentAdd", inputParam, invokeCustomerBBPaymentAddCallback)*/
                /*frmIBBeepAndBillExecuteConfComp.hbxBillersOTPContainer.setVisibility(false);
					frmIBBeepAndBillExecuteConfComp.hbxSuccess.setVisibility(true);
					frmIBBeepAndBillExecuteConfComp.btnConfirm.text = kony.i18n.getLocalizedString("keyReturn");

					frmIBBeepAndBillExecuteConfComp.btnCancel.setVisibility(false);
					frmIBBeepAndBillExecuteConfComp.ScreenTitle.text = kony.i18n.getLocalizedString("Complete");
					*/
                if (flowSpa) popOtpSpa.dismiss();
                frmIBBeepAndBillExecuteConfComp.hbxBillersOTPContainer.setVisibility(false);
                frmIBBeepAndBillExecuteConfComp.hbxToken.setVisibility(false);
                frmIBBeepAndBillExecuteConfComp.hbxSuccess.setVisibility(true);
                frmIBBeepAndBillExecuteConfComp.hbxCanConfBtnContainer.setVisibility(false);
                frmIBBeepAndBillExecuteConfComp.hbxReturn.setVisibility(true);
                frmIBBeepAndBillExecuteConfComp.ScreenTitle.text = kony.i18n.getLocalizedString("Complete");
                frmIBBeepAndBillExecuteConfComp.hbxSave.setVisibility(true);
                frmIBBeepAndBillExecuteConfComp.lblPaymentDateVal.text = resulttable["currentTime"];
                frmIBBeepAndBillExecuteConfComp.lblBalanceVal.text = commaFormattedOpenAct(resulttable["updatedBal"]) + kony.i18n.getLocalizedString("currencyThaiBaht");
                frmIBBeepAndBillExecuteConfComp.lblBalance.text = kony.i18n.getLocalizedString("keyBalanceAfterPayment");
                dismissLoadingScreenPopup();
                //callNotificationAddServiceBB();
                //var tellerId = result["custAcctRec"][0]["TellerID"];
                //var errorCd = result["additionalStatus"][0]["statusDesc"];
                //createFinActivityLogObjBB(errorCd,tellerId);
                return true;
            }
        } else if (resulttable["opstatus"] == 8005) {
            if (resulttable["errCode"] == "VrfyOTPErr00001") {
                gblVerifyOTP = resulttable["retryCounterVerifyOTP"];
                dismissLoadingScreenPopup();
                alert("" + kony.i18n.getLocalizedString("invalidOTP"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00002") {
                dismissLoadingScreenPopup();
                //alert("" + kony.i18n.getLocalizedString("ECVrfyOTPErr"));
                //startRcCrmUpdateProIB("04");
                handleOTPLockedIB(resulttable);
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00004") {
                dismissLoadingScreenPopup();
                showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00004"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00005") {
                dismissLoadingScreenPopup();
                //showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00005"), kony.i18n.getLocalizedString("info"));
                alert("" + kony.i18n.getLocalizedString("invalidOTP"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00006") {
                dismissLoadingScreenPopup();
                showAlert(resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errCode"] == "GenOTPRtyErr00001") {
                dismissLoadingScreenPopup();
                showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errCode"] == "depErr02") {
                dismissLoadingScreenPopup();
                showAlertIB(kony.i18n.getLocalizedString("keyOpenActGenErr"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errCode"] == "depErr01") {
                dismissLoadingScreenPopup();
                showAlertIB(kony.i18n.getLocalizedString("keyOpenActGenErr"), kony.i18n.getLocalizedString("info"));
                return false;
            } else {
                dismissLoadingScreenPopup();
                showCommonAlertMYA(" " + kony.i18n.getLocalizedString("Receipent_alert_Error") + ":" + resulttable["errMsg"], null);
                return false;
            }
        } else if (resulttable["opstatus"] == "8006") {
            dismissLoadingScreenPopup();
            alert(kony.i18n.getLocalizedString("keyErrResponseOne"));
            //alert(kony.i18n.getLocalizedString(resulttable["errMsg"] + ""));
        } else if (resulttable["opstatus"] == "8004") {
            dismissLoadingScreenPopup();
            alert(kony.i18n.getLocalizedString("keyErrResponseOne"));
            //alert(kony.i18n.getLocalizedString(resulttable["errMsg"] + ""));
        } else if (resulttable["errMsg"] != undefined) {
            dismissLoadingScreenPopup();
            alert(resulttable["errMsg"]);
            //alert(kony.i18n.getLocalizedString(resulttable["errMsg"] + ""));
        } else {
            dismissLoadingScreenPopup();
            alert(kony.i18n.getLocalizedString("keyErrResponseOne"));
            //showAlert(resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
        }
        return false;
    }
}

function setInputParamsaddBBToListIBComposite(inputParam) {
    var accNo = frmIBBeepAndBillConfAndComp.lblAccNumValue.text;
    accNo = removeHyphenIB(accNo);
    /*for(var i=0;i<tempAccNo.length;i++){
		if(tempAccNo[i]!= '-') accNo = accNo+tempAccNo[i]
	}*/
    accNo = accNo.substring(accNo.length - 10, accNo.length);
    var compCode = frmIBBeepAndBillConfAndComp.lblCompCode.text;
    compCode = compCode.substring(compCode.length - 5, compCode.length - 1)
        //inputParam = {};
    inputParam["customerBBAdd_rmNum"] = "";
    inputParam["customerBBAdd_rqUUId"] = "";
    inputParam["customerBBAdd_BBNickname"] = frmIBBeepAndBillConfAndComp.lblBillerName.text;
    inputParam["customerBBAdd_BBCompCode"] = gblBillerCompCodeBB;
    inputParam["customerBBAdd_Ref1"] = frmIBBeepAndBillConfAndComp.lblViewBillerRef1Value.text;
    inputParam["customerBBAdd_Ref1Label"] = frmIBBeepAndBillConfAndComp.lblViewBillerRef1.text;
    if (frmIBBeepAndBillConfAndComp.hbxViewBillerRef2.isVisible) {
        inputParam["customerBBAdd_Ref2"] = frmIBBeepAndBillConfAndComp.lblViewBillerRef2Value.text;
        inputParam["customerBBAdd_Ref2Label"] = frmIBBeepAndBillConfAndComp.lblViewBillerRef2.text;
    } else {
        inputParam["customerBBAdd_Ref2"] = "";
        inputParam["customerBBAdd_Ref2Label"] = "";
    }
    inputParam["customerBBAdd_AcctIdentValue"] = accNo;
    inputParam["customerBBAdd_mobileNumber"] = gblPHONENUMBER;
    inputParam["customerBBAdd_ActionFlag"] = "A";
    inputParam["customerBBAdd_MIBFlag"] = "N";
    inputParam["customerBBAdd_Channel"] = "MIB";
    inputParam["customerBBAdd_ApplyDate"] = getTodaysDate();
    inputParam["customerBBAdd_ApplyTime"] = "155712";
    //showLoadingScreenPopup();
    //invokeServiceSecureAsync("customerBBAdd", inputParam, applyBeepAndBillIBCallBack);
}

function callNotificationAddServiceIBBBApplyComposite(inputParam) {
    //var inputparam = [];
    //inputparam["channelName"] = "Internet Banking"; please confirm whether it should be IB or MB or InternetBanking
    inputParam["NotificationAdd_notificationType"] = "Email"; // check this : which value we have to pass
    //inputparam["phoneNumber"] = gblPHONENUMBER;
    //inputparam["emailId"] = gblEmailId;
    inputParam["NotificationAdd_customerName"] = gblCustomerName;
    inputParam["NotificationAdd_fromAccount"] = frmIBBeepAndBillConfAndComp.lblAccNumValue.text;
    // need partial masked account number, 5-9 digits unmasked
    inputParam["NotificationAdd_fromAcctNick"] = frmIBBeepAndBillConfAndComp.lblAccHolderName.text;
    inputParam["NotificationAdd_billerName"] = frmIBBeepAndBillConfAndComp.lblBillerName.text;
    inputParam["NotificationAdd_billerCompCode"] = frmIBBeepAndBillConfAndComp.lblCompCode.text;
    var labelRef1 = frmIBBeepAndBillConfAndComp.lblViewBillerRef1.text;
    labelRef1 = labelRef1.substring(0, labelRef1.length - 1);
    inputParam["NotificationAdd_ref1Label"] = labelRef1;
    inputParam["NotificationAdd_ref1Value"] = frmIBBeepAndBillConfAndComp.lblViewBillerRef1Value.text;
    if (frmIBBeepAndBillConfAndComp.hbxViewBillerRef2.isVisible == true) {
        var labelRef2 = frmIBBeepAndBillConfAndComp.lblViewBillerRef2.text;
        labelRef2 = labelRef2.substring(0, labelRef2.length - 1);
        inputParam["NotificationAdd_ref2Label"] = labelRef2;
        inputParam["NotificationAdd_ref2Value"] = frmIBBeepAndBillConfAndComp.lblViewBillerRef2Value.text;
    } else {
        inputParam["NotificationAdd_ref2Label"] = "";
        inputParam["NotificationAdd_ref2Value"] = "";
    }
    // check for ref2 exists
    //inputparam["todayDate"] = getTodaysDate(); 
    //inputparam["todayTime"] = ""; // why blank ?
    //inputparam["channelID"] = "01";
    inputParam["NotificationAdd_source"] = "sendEmailForBBApply";
    inputParam["NotificationAdd_Locale"] = kony.i18n.getCurrentLocale();
    //invokeServiceSecureAsync("NotificationAdd", inputparam, callBackNotificationAddService)
}

function setactivityLogServiceCallComposite(inputParam) {
    var platformChannel = gblDeviceInfo.name;
    //inputParam = {};
    //inputParam["ipAddress"] = ""; 
    inputParam["activationActivityLog_activityTypeID"] = "032";
    //For failures the error code received from ECAS/XPRESS/Other systems. For Success case it should be "0000".
    inputParam["activationActivityLog_errorCd"] = "";
    //inputParam["activationActivityLog_errorCd"] = errorCode; 
    //This status willl be "01" for success and "02" for failure of the actual event/instance
    inputParam["activationActivityLog_activityStatus"] = "01";
    inputParam["activationActivityLog_deviceNickName"] = gblDeviceNickName;
    inputParam["activationActivityLog_activityFlexValues1"] = frmIBBeepAndBillConfAndComp.lblBillerName.text;
    inputParam["activationActivityLog_activityFlexValues2"] = frmIBBeepAndBillConfAndComp.lblAccNumValue.text;
    inputParam["activationActivityLog_activityFlexValues3"] = gblPHONENUMBER;
    inputParam["activationActivityLog_activityFlexValues4"] = frmIBBeepAndBillConfAndComp.lblViewBillerRef1Value.text;
    inputParam["activationActivityLog_activityFlexValues5"] = frmIBBeepAndBillConfAndComp.lblViewBillerRef2Value.text;
    if (platformChannel == "thinclient") inputParam["activationActivityLog_channelId"] = GLOBAL_IB_CHANNEL;
    else inputParam["activationActivityLog_channelId"] = GLOBAL_MB_CHANNEL;
    inputParam["activationActivityLog_logLinkageId"] = "";
    //invokeServiceSecureAsync("activationActivityLog", inputParam, callBackActivityLog)
}

function verifyOTPBBCompositeEx() {
    var currForm = kony.application.getCurrentForm()
    var text = currForm.txtBxOTPBB.text;
    var isNumOTP = kony.string.isNumeric(text);
    var txtLen1 = text.length;
    var inputParam = {};
    inputParam["flowspa"] = "false";
    inputParam["channel"] = "wap";
    inputParam["appID"] = "TMB";
    inputParam["customerBBPaymentAdd_PINcode"] = messageObj["pinCode"];
    callNotificationAddServiceIBBBExecuteComposite(inputParam);
    setactivityLogServiceCallBBExecuteComposite(inputParam);
    setInputParamFinancialActivityLogBBEx(inputParam);
    if (gblTokenSwitchFlagBB == false) {
        if (text == "" || text == null) {
            alert(kony.i18n.getLocalizedString("Receipent_alert_correctOTP"));
            return false;
        }
        /* local validation temporarily removed to make the flow consistent with other places 
		if (txtLen1 != gblOTPLENGTH || isNumOTP == false || text == "") {
			showAlert("OTP Should be Numeric and of length " + gblOTPLENGTH , kony.i18n.getLocalizedString("info"));
			return false;
		}*/
        inputParam["gblTokenSwitchFlagBB"] = "false";
        inputParam["verifyPasswordEx_retryCounterVerifyOTP"] = gblVerifyOTP;
        inputParam["verifyPasswordEx_userId"] = gblUserName;
        inputParam["verifyPasswordEx_userStoreId"] = "DefaultStore";
        inputParam["verifyPasswordEx_loginModuleId"] = "IBSMSOTP";
        inputParam["verifyPasswordEx_password"] = text;
        inputParam["password"] = text;
        inputParam["verifyPasswordEx_segmentId"] = "MIB";
        showLoadingScreenPopup();
        frmIBBeepAndBillConfAndComp.txtBxOTPBB.text = "";
        currForm.txtBxOTPBB.text = "";
        //invokeServiceSecureAsync("verifyPasswordEx", inputParam, verifyTokenOTPBBCallback)
        invokeServiceSecureAsync("BeepAndBillExecuteJavaService", inputParam, verifyTokenOTPBBCallbackComposite);
    } else {
        if (currForm.tbxToken.text == "" || currForm.tbxToken.text == null) {
            alert(kony.i18n.getLocalizedString("Receipent_tokenId"));
            return false;
        }
        inputParam["gblTokenSwitchFlagBB"] = "true";
        inputParam["verifyTokenEx_loginModuleId"] = "IB_HWTKN";
        inputParam["verifyTokenEx_userStoreId"] = "DefaultStore";
        inputParam["verifyTokenEx_retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
        inputParam["verifyTokenEx_retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
        inputParam["verifyTokenEx_userId"] = gblUserName;
        inputParam["verifyTokenEx_password"] = currForm.tbxToken.text;
        inputParam["password"] = currForm.tbxToken.text;
        inputParam["verifyTokenEx_sessionVal"] = "";
        inputParam["verifyTokenEx_segmentId"] = "segmentId";
        inputParam["verifyTokenEx_segmentIdVal"] = "MIB";
        inputParam["verifyTokenEx_channel"] = "InterNet Banking";
        showLoadingScreenPopup();
        currForm.tbxToken.text == ""
            //invokeServiceSecureAsync("verifyTokenEx", inputParam, verifyTokenOTPBBCallback)
        invokeServiceSecureAsync("BeepAndBillExecuteJavaService", inputParam, verifyTokenOTPBBCallbackComposite);
    }
}

function callNotificationAddServiceIBBBExecuteComposite(inputParam) {
    //inputparam["channelName"] = "Internet Banking";
    inputParam["NotificationAdd_notificationType"] = "Email"; // check this : which value we have to pass
    //inputparam["phoneNumber"] = gblPHONENUMBER;
    //inputparam["emailId"] = gblEmailId;
    inputParam["NotificationAdd_customerName"] = gblCustomerName;
    inputParam["NotificationAdd_fromAccount"] = frmIBBeepAndBillExecuteConfComp.lblAccountNo.text;
    // need partial masked account number, 5-9 digits unmasked
    inputParam["NotificationAdd_fromAcctNick"] = frmIBBeepAndBillExecuteConfComp.lblFromName.text;
    inputParam["NotificationAdd_billerNick"] = frmIBBeepAndBillExecuteConfComp.lblNickname.text;
    inputParam["NotificationAdd_billerCompCode"] = frmIBBeepAndBillExecuteConfComp.lblCompCode.text;
    inputParam["NotificationAdd_ref1Label"] = frmIBBeepAndBillExecuteConfComp.lblRef1.text;
    inputParam["NotificationAdd_ref1Value"] = frmIBBeepAndBillExecuteConfComp.lblRef1Value.text;
    if (frmIBBeepAndBillExecuteConfComp.hbxRef2.isVisible == true) {
        inputParam["NotificationAdd_ref2Label"] = frmIBBeepAndBillExecuteConfComp.lblRef2.text;
        inputParam["NotificationAdd_ref2Value"] = frmIBBeepAndBillExecuteConfComp.lblRef2Value.text;
    }
    inputParam["NotificationAdd_amount"] = frmIBBeepAndBillExecuteConfComp.lblAmountVal.text;
    inputParam["NotificationAdd_fee"] = frmIBBeepAndBillExecuteConfComp.lblFeeVal.text;
    inputParam["NotificationAdd_initiationDt"] = frmIBBeepAndBillExecuteConfComp.lblPaymentDateVal.text;
    //inputparam["todayDate"] = getTodaysDate(); 
    //inputparam["todayTime"] = ""; // why blank ?
    inputParam["NotificationAdd_transactionRefNum"] = frmIBBeepAndBillExecuteConfComp.lblXrefNoVal.text;
    //inputparam["channelID"] = "01";
    inputParam["NotificationAdd_source"] = "sendEmailForBBExecute";
    inputParam["Locale"] = kony.i18n.getCurrentLocale();
}

function setactivityLogServiceCallBBExecuteComposite(inputParam) {
    inputParam["activationActivityLog_activityTypeID"] = "034";
    //For failures the error code received from ECAS/XPRESS/Other systems. For Success case it should be "0000".
    inputParam["activationActivityLog_errorCd"] = "";
    //inputParam["activationActivityLog_errorCd"] = errorCode; 
    //This status willl be "01" for success and "02" for failure of the actual event/instance
    inputParam["activationActivityLog_activityStatus"] = "01";
    inputParam["activationActivityLog_deviceNickName"] = gblDeviceNickName;
    inputParam["activationActivityLog_activityFlexValues1"] = frmIBBeepAndBillExecuteConfComp.lblName.text;
    inputParam["activationActivityLog_activityFlexValues2"] = gblPHONENUMBER
    inputParam["activationActivityLog_activityFlexValues3"] = frmIBBeepAndBillExecuteConfComp.lblAccountNo.text;;
    inputParam["activationActivityLog_activityFlexValues4"] = frmIBBeepAndBillExecuteConfComp.lblRef1Value.text;
    inputParam["activationActivityLog_activityFlexValues5"] = frmIBBeepAndBillExecuteConfComp.lblAmountVal.text + "+" + frmIBBeepAndBillExecuteConfComp.lblFeeVal.text;
    //if (platformChannel == "thinclient")
    //	inputParam["activationActivityLog_channelId"] = GLOBAL_IB_CHANNEL;
    //else
    inputParam["activationActivityLog_channelId"] = "01";
    inputParam["activationActivityLog_logLinkageId"] = "";
}

function setInputParamFinancialActivityLogBBEx(inputParam) {
    inputParam["financialActivityLog_crmId"] = gblcrmId;
    inputParam["financialActivityLog_finTxnRefId"] = gblBBRefNo + "00";
    inputParam["financialActivityLog_tellerId"] = "";
    //var date = frmIBBeepAndBillExecuteConfComp.lblPaymentDateVal.text;
    //if(date != null) date = replaceCommon(date, "/", "");
    //else date = "000000";
    inputParam["financialActivityLog_finTxnDate"] = frmIBBeepAndBillExecuteConfComp.lblPaymentDateVal.text;;
    var feeAmount = frmIBBeepAndBillExecuteConfComp.lblFeeVal.text;
    feeAmount = feeAmount.substring(0, feeAmount.length - 1);
    feeAmount = feeAmount.replace(",", "");
    inputParam["financialActivityLog_finTxnFee"] = feeAmount;
    inputParam["financialActivityLog_billerRef1"] = frmIBBeepAndBillExecuteConfComp.lblRef1Value.text;
    inputParam["financialActivityLog_billerRef2"] = frmIBBeepAndBillExecuteConfComp.lblRef2Value.text;
    inputParam["financialActivityLog_billerCommCode"] = gblBillerCompCodeBB;
    inputParam["financialActivityLog_beepAndBillTxnId"] = frmIBBeepAndBillExecuteConfComp.lblTransactionIDVal.text;
    var DueDate = frmIBBeepAndBillExecuteConfComp.lblDueDateVal.text;
    inputParam["financialActivityLog_dueDate"] = DueDate.split("/").join("-");
    inputParam["financialActivityLog_activityTypeId"] = "034";
    inputParam["financialActivityLog_channelId"] = "01";
    inputParam["financialActivityLog_fromAcctId"] = removeUnwatedSymbols(frmIBBeepAndBillExecuteConfComp.lblAccountNo.text);
    inputParam["financialActivityLog_fromAcctName"] = frmIBBeepAndBillExecuteConfComp.lblFromName.text;
    inputParam["financialActivityLog_fromAcctNickname"] = frmIBBeepAndBillExecuteConfComp.lblNickname.text;
    var masterBillerRec = "";
    for (var i = 0; i < gblmasterBillerAndTopupBB.length; i++) {
        //alert(gblmasterBillerAndTopupBB[i]["BillerCompcode"] + " == " + gblBillerCompCodeBB )
        if (gblmasterBillerAndTopupBB[i]["BillerCompcode"] == gblBillerCompCodeBB) {
            if (gblmasterBillerAndTopupBB[i]["BillerMethod"] == "2" || gblmasterBillerAndTopupBB[i]["BillerMethod"] == "3") inputParam["financialActivityLog_toAcctId"] = frmIBBeepAndBillExecuteConfComp.lblRef1Value.text;
            else inputParam["financialActivityLog_toAcctId"] = gblmasterBillerAndTopupBB[i]["ToAccountKey"];
            if (kony.i18n.getCurrentLocale() == "en_US") inputParam["financialActivityLog_toAcctName"] = gblmasterBillerAndTopupBB[i]["BillerNameEN"];
            else inputParam["financialActivityLog_toAcctName"] = gblmasterBillerAndTopupBB[i]["BillerNameTH"];
            //alert(GBLFINANACIALACTIVITYLOG.toAcctId + " and " + GBLFINANACIALACTIVITYLOG.toAcctName )
            break;
        }
    }
    //GBLFINANACIALACTIVITYLOG.toAcctId = "";//removeUnwatedSymbols(frmSSSExecute.lblSendToAccNo.text);
    //GBLFINANACIALACTIVITYLOG.toAcctName = "";//exeToAcctType;
    inputParam["financialActivityLog_toAcctNickname"] = ""; //exeToAcctName;
    //	GBLFINANACIALACTIVITYLOG.bankCD = "11";
    inputParam["financialActivityLog_toBankAcctCd"] = "11";
    var amount = frmIBBeepAndBillExecuteConfComp.lblAmountVal.text;
    amount = amount.substring(0, amount.length - 1);
    amount = amount.replace(",", "");
    inputParam["financialActivityLog_finTxnAmount"] = "" + amount;
    inputParam["financialActivityLog_finTxnBalance"] = ""; //retailLiqTrnsAdd["FromAmt"];
    if (gblPmtStatus == "S" || gblPmtStatus == "s") {
        inputParam["financialActivityLog_finTxnStatus"] = "01";
        inputParam["financialActivityLog_clearingStatus"] = "01";
    } else {
        inputParam["financialActivityLog_finTxnStatus"] = "02";
        inputParam["financialActivityLog_clearingStatus"] = "02";
    }
    //if(retailLiqTrnsAdd["statusCode"] != 0) {
    inputParam["financialActivityLog_errorCd"] = "";
    //} else {
    //GBLFINANACIALACTIVITYLOG.errorCd = retailLiqTrnsAdd["addStatusCode"];
    //}
    inputParam["financialActivityLog_eventId"] = gblBBRefNo.replace("BB", "") + "00";
    inputParam["financialActivityLog_txnType"] = "007";
    //GBLFINANACIALACTIVITYLOG.dueDate = frmIBBeepAndBillExecuteConfComp.lblDueDateVal.text + "";
    //GBLFINANACIALACTIVITYLOG.beepAndBillTxnId = frmIBBeepAndBillExecuteConfComp.lblTransactionIDVal.text + "";
    //
    inputParam["financialActivityLog_finFlexValues1"] = frmIBBeepAndBillExecuteConfComp.lblFromName.text;
    inputParam["financialActivityLog_finFlexValues2"] = "" + gblPHONENUMBER;
    inputParam["financialActivityLog_finFlexValues3"] = frmIBBeepAndBillExecuteConfComp.lblAccountNo.text;
    inputParam["financialActivityLog_finFlexValues4"] = frmIBBeepAndBillExecuteConfComp.lblRef1Value.text;
    inputParam["financialActivityLog_finFlexValues5"] = frmIBBeepAndBillExecuteConfComp.lblAmountVal.text + "+" + frmIBBeepAndBillExecuteConfComp.lblFeeVal.text;
}

function onLocaleChangeBBListPage() {
    gblLocaleChangeBB = true;
    getSelectBillerCategoryServiceBB();
    //inquireMasterBillerAndTopup();
    frmIBBeepAndBillList.tbxSearch.placeholder = kony.i18n.getLocalizedString("Receipent_Search");
    if (kony.i18n.getCurrentLocale() == "en_US") {
        frmIBBeepAndBillList.cbxSelectCat.masterData = gblBillerCategoriesBBIB;
    } else {
        frmIBBeepAndBillList.cbxSelectCat.masterData = gblBillerCategoriesBBIBTH;
    }
    populatefrmBBSelect(gblmasterBillerAndTopupBB);
}

function onLocaleChangeBBApply() {
    getMyBillListIBBB();
    //frmIBBeepAndBillApply.show();
    if (kony.i18n.getCurrentLocale() == "en_US") {
        frmIBBeepAndBillApply.cbxSelectCat.masterData = gblBillerCategoriesBBIB;
    } else {
        frmIBBeepAndBillApply.cbxSelectCat.masterData = gblBillerCategoriesBBIBTH;
    }
    frmIBBeepAndBillApply.tbxSearch.placeholder = kony.i18n.getLocalizedString("Receipent_Search");
    frmIBBeepAndBillApply.lblAddBill.text = kony.i18n.getLocalizedString("keyBBApplyPayment");
    populatefrmBBSelect(gblmasterBillerAndTopupBB);
}

function onLocaleChangeBBApplyCW() {
    gblLocaleChangeBB = true;
    if (gblOnClickSug == true) {
        onSelectSugBillToAddBBForIB();
    } else {
        onSelectSegBillListIBBB();
    }
    frmIBBeepAndBillApplyCW.lblAddBiller.text = kony.i18n.getLocalizedString("keyBillPaymentBiller");
    showAccountListBB();
    if (frmIBBeepAndBillApplyCW.hbxChargeTo.isVisible) {
        customWidgetSelectEventIBBeepAndBill();
    }
}

function onLocaleChangeBBConfirmPage() {
    var accNo = removeHyphenIB(frmIBBeepAndBillApplyCW.lblFromAccNum.text);
    if (frmIBBeepAndBillConfAndComp.lblBalance.isVisible) frmIBBeepAndBillConfAndComp.link474016987105121.text = kony.i18n.getLocalizedString("Hide")
    else frmIBBeepAndBillConfAndComp.link474016987105121.text = kony.i18n.getLocalizedString("keyUnhide")
    for (var i = 0; i < gblcustomerAccountsListBB.length; i++) {
        var acc = gblcustomerAccountsListBB[i]["accId"].substring(gblcustomerAccountsListBB[i]["accId"].length - 10, gblcustomerAccountsListBB[i]["accId"].length);
        if (acc == accNo) {
            if (kony.i18n.getCurrentLocale() == "th_TH") {
                frmIBBeepAndBillConfAndComp.lblAccType.text = gblcustomerAccountsListBB[i]["ProductNameThai"];
                if (gblcustomerAccountsListBB[i]["acctNickName"] != undefined) frmIBBeepAndBillConfAndComp.lblAccHolderName.text = gblcustomerAccountsListBB[i]["acctNickName"]
                else frmIBBeepAndBillConfAndComp.lblAccHolderName.text = gblcustomerAccountsListBB[i]["ProductNameThai"] + acc.substring(acc.length - 4, acc.length);
            } else {
                frmIBBeepAndBillConfAndComp.lblAccType.text = gblcustomerAccountsListBB[i]["ProductNameEng"];
                if (gblcustomerAccountsListBB[i]["acctNickName"] != undefined) frmIBBeepAndBillConfAndComp.lblAccHolderName.text = gblcustomerAccountsListBB[i]["acctNickName"]
                else frmIBBeepAndBillConfAndComp.lblAccHolderName.text = gblcustomerAccountsListBB[i]["ProductNameEng"] + acc.substring(acc.length - 4, acc.length);
            }
        }
    }
    for (var i = 0; i < gblmasterBillerAndTopupBB.length; i++) {
        if (gblmasterBillerAndTopupBB[i]["BillerCompcode"] == gblBillerCompCodeBB) {
            if (kony.i18n.getCurrentLocale() == "th_TH") {
                frmIBBeepAndBillConfAndComp.lblCompCode.text = gblmasterBillerAndTopupBB[i]["BillerNameTH"] + "(" + gblmasterBillerAndTopupBB[i]["BillerCompcode"] + ")";
                frmIBBeepAndBillConfAndComp.lblViewBillerRef1.text = gblmasterBillerAndTopupBB[i]["LabelReferenceNumber1TH"];
                frmIBBeepAndBillConfAndComp.lblViewBillerRef2.text = gblmasterBillerAndTopupBB[i]["LabelReferenceNumber2TH"];
            } else {
                frmIBBeepAndBillConfAndComp.lblCompCode.text = gblmasterBillerAndTopupBB[i]["BillerNameEN"] + "(" + gblmasterBillerAndTopupBB[i]["BillerCompcode"] + ")";
                frmIBBeepAndBillConfAndComp.lblViewBillerRef1.text = gblmasterBillerAndTopupBB[i]["LabelReferenceNumber1EN"];
                frmIBBeepAndBillConfAndComp.lblViewBillerRef2.text = gblmasterBillerAndTopupBB[i]["LabelReferenceNumber2EN"];
            }
        }
    }
    if (frmIBBeepAndBillConfAndComp.hbxSuccess.isVisible == false) {
        frmIBBeepAndBillConfAndComp.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
        frmIBBeepAndBillConfAndComp.btnConfirm.text = kony.i18n.getLocalizedString("keyConfirm");
        frmIBBeepAndBillConfAndComp.lblHdrtxt.text = kony.i18n.getLocalizedString("Confirmation");
        frmIBBeepAndBillConfAndComp.lblBankRef.text = kony.i18n.getLocalizedString("keybankrefno");
        frmIBBeepAndBillConfAndComp.btnOTPReq.text = kony.i18n.getLocalizedString("RequestOTP");
        //frmIBBeepAndBillConfAndComp.txtBxOTPBB.placeholder = kony.i18n.getLocalizedString("enterOTP");
        frmIBBeepAndBillConfAndComp.btnToken.text = kony.i18n.getLocalizedString("keySwitchToOTP");
        //frmIBBeepAndBillConfAndComp.tbxToken.placeholder = kony.i18n.getLocalizedString("enterToken");
    } else {
        frmIBBeepAndBillConfAndComp.btnCancel.text = kony.i18n.getLocalizedString("keyReturn");
        frmIBBeepAndBillConfAndComp.btnConfirm.text = kony.i18n.getLocalizedString("keyApplyMore");
        frmIBBeepAndBillConfAndComp.lblHdrtxt.text = kony.i18n.getLocalizedString("Complete");
    }
}

function onLocaleChangeBBExecuteConfirmPage() {
    var tmpRef2BB = "N";
    if (frmIBBeepAndBillExecuteConfComp.lblBalance.isVisible) {
        frmIBBeepAndBillExecuteConfComp.link474016987103118.text = kony.i18n.getLocalizedString("Hide");
    } else {
        frmIBBeepAndBillExecuteConfComp.link474016987103118.text = kony.i18n.getLocalizedString("keyUnhide");
    }
    for (var i = 0; i < gblmasterBillerAndTopupBB.length; i++) {
        if (messageObj["BBCompcode"] == gblmasterBillerAndTopupBB[i]["BillerCompcode"]) {
            for (var k = 0; k < gblmasterBillerAndTopupBB[i]["BillerMiscData"].length; k++) {
                if (gblmasterBillerAndTopupBB[i]["BillerMiscData"][k]["MiscName"] == "BB.IsRequiredRefNumber2") {
                    tmpRef2BB = gblmasterBillerAndTopupBB[i]["BillerMiscData"][k]["MiscText"];
                }
            }
            if (kony.i18n.getCurrentLocale() == "en_US") {
                frmIBBeepAndBillExecuteConfComp.lblRef1.text = gblmasterBillerAndTopupBB[i]["LabelReferenceNumber1EN"];
                frmIBBeepAndBillExecuteConfComp.lblRef1Value.text = messageObj["ref1"];
                frmIBBeepAndBillExecuteConfComp.lblCompCode.text = gblmasterBillerAndTopupBB[i]["BillerNameEN"] + "(" + messageObj["BBCompcode"] + ")";
                if (tmpRef2BB == "N") {
                    frmIBBeepAndBillExecuteConfComp.lblRef2.text = "";
                    frmIBBeepAndBillExecuteConfComp.lblRef2Value.text = "";
                } else {
                    frmIBBeepAndBillExecuteConfComp.lblRef2.text = gblmasterBillerAndTopupBB[i]["LabelReferenceNumber2EN"];
                    frmIBBeepAndBillExecuteConfComp.lblRef2Value.text = messageObj["ref2"];
                }
                for (var k = 0; k < gblmasterBillerAndTopupBB[i]["BillerMiscData"].length; k++) {
                    if (gblmasterBillerAndTopupBB[i]["BillerMiscData"][k]["MiscName"] == "BB.Ref1.LABEL.EN") {
                        frmIBBeepAndBillExecuteConfComp.lblRef1.text = gblmasterBillerAndTopupBB[i]["BillerMiscData"][k]["MiscText"];
                    }
                    if (gblmasterBillerAndTopupBB[i]["BillerMiscData"][k]["MiscName"] == "BB.Ref2.LABEL.EN") {
                        frmIBBeepAndBillExecuteConfComp.lblRef2.text = gblmasterBillerAndTopupBB[i]["BillerMiscData"][k]["MiscText"];
                    }
                }
            } else {
                frmIBBeepAndBillExecuteConfComp.lblRef1.text = gblmasterBillerAndTopupBB[i]["LabelReferenceNumber1TH"];
                frmIBBeepAndBillExecuteConfComp.lblRef1Value.text = messageObj["ref1"];
                frmIBBeepAndBillExecuteConfComp.lblCompCode.text = gblmasterBillerAndTopupBB[i]["BillerNameTH"] + "(" + messageObj["BBCompcode"] + ")";
                if (tmpRef2BB == "N") {
                    frmIBBeepAndBillExecuteConfComp.lblRef2.text = "";
                    frmIBBeepAndBillExecuteConfComp.lblRef2Value.text = "";
                } else {
                    frmIBBeepAndBillExecuteConfComp.lblRef2.text = gblmasterBillerAndTopupBB[i]["LabelReferenceNumber2TH"];
                    frmIBBeepAndBillExecuteConfComp.lblRef2Value.text = messageObj["ref2"];
                }
                for (var k = 0; k < gblmasterBillerAndTopupBB[i]["BillerMiscData"].length; k++) {
                    if (gblmasterBillerAndTopupBB[i]["BillerMiscData"][k]["MiscName"] == "BB.Ref1.LABEL.TH") {
                        frmIBBeepAndBillExecuteConfComp.lblRef1.text = gblmasterBillerAndTopupBB[i]["BillerMiscData"][k]["MiscText"];
                    }
                    if (gblmasterBillerAndTopupBB[i]["BillerMiscData"][k]["MiscName"] == "BB.Ref2.LABEL.TH") {
                        frmIBBeepAndBillExecuteConfComp.lblRef2.text = gblmasterBillerAndTopupBB[i]["BillerMiscData"][k]["MiscText"];
                    }
                }
            }
        }
    }
    var accNo = removeHyphenIB(frmIBBeepAndBillExecuteConfComp.lblAccountNo.text);
    for (var i = 0; i < gblcustomerAccountsListBB.length; i++) {
        var acc = gblcustomerAccountsListBB[i]["accId"].substring(gblcustomerAccountsListBB[i]["accId"].length - 10, gblcustomerAccountsListBB[i]["accId"].length);
        if (acc == accNo) {
            if (kony.i18n.getCurrentLocale() == "th_TH") {
                frmIBBeepAndBillExecuteConfComp.lblName.text = gblcustomerAccountsListBB[i]["ProductNameThai"];
            } else {
                frmIBBeepAndBillExecuteConfComp.lblName.text = gblcustomerAccountsListBB[i]["ProductNameEng"];
            }
        }
    }
    if (frmIBBeepAndBillExecuteConfComp.hbxSuccess.isVisible == false) {
        frmIBBeepAndBillExecuteConfComp.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
        frmIBBeepAndBillExecuteConfComp.btnConfirm.text = kony.i18n.getLocalizedString("keyConfirm");
        frmIBBeepAndBillExecuteConfComp.ScreenTitle.text = kony.i18n.getLocalizedString("Confirmation");
        frmIBBeepAndBillExecuteConfComp.label449290336612155.text = kony.i18n.getLocalizedString("keybankrefno");
        frmIBBeepAndBillExecuteConfComp.btnOTPReq.text = kony.i18n.getLocalizedString("RequestOTP");
        //	frmIBBeepAndBillExecuteConfComp.txtBxOTPBB.placeholder = kony.i18n.getLocalizedString("enterOTP");
        frmIBBeepAndBillExecuteConfComp.btnToken.text = kony.i18n.getLocalizedString("keySwitchToOTP");
        //frmIBBeepAndBillExecuteConfComp.tbxToken.placeholder = kony.i18n.getLocalizedString("enterToken");
        frmIBBeepAndBillExecuteConfComp.lblBalance.text = kony.i18n.getLocalizedString("keyBalanceBeforePayment");
    } else {
        frmIBBeepAndBillExecuteConfComp.ScreenTitle.text = kony.i18n.getLocalizedString("Complete");
        frmIBBeepAndBillExecuteConfComp.lblBalance.text = kony.i18n.getLocalizedString("keyBalanceAfterPayment");
    }
}

function populateOnFrmIBBeepAndBillList() {
    var currForm = kony.application.getCurrentForm();
    for (var i = 0; i < gblmasterBillerAndTopupBB.length; i++) {
        if (gblmasterBillerAndTopupBB[i]["BillerCompcode"] == gblBillerCompCodeBB) {
            if (kony.i18n.getCurrentLocale() == "th_TH") {
                currForm.lblAddBillerCompCode.text = gblmasterBillerAndTopupBB[i]["BillerNameTH"] + "(" + gblmasterBillerAndTopupBB[i]["BillerCompcode"] + ")";
                currForm.lblAddBillerRef1.text = gblmasterBillerAndTopupBB[i]["LabelReferenceNumber1TH"];
                currForm.lblAddBillerRef2.text = gblmasterBillerAndTopupBB[i]["LabelReferenceNumber2TH"];
            } else {
                currForm.lblAddBillerCompCode.text = gblmasterBillerAndTopupBB[i]["BillerNameEN"] + "(" + gblmasterBillerAndTopupBB[i]["BillerCompcode"] + ")";
                currForm.lblAddBillerRef1.text = gblmasterBillerAndTopupBB[i]["LabelReferenceNumber1EN"];
                currForm.lblAddBillerRef2.text = gblmasterBillerAndTopupBB[i]["LabelReferenceNumber2EN"];
            }
            currForm.txtAddBillerRef1.maxTextLength = gblmasterBillerAndTopupBB[i]["Ref1Len"];
            currForm.txtAddBillerRef2.maxTextLength = gblmasterBillerAndTopupBB[i]["Ref2Len"];
            var tmpRef2BB = "N";
            var tmpRef2Type = "";
            var tmpRef2Data = "";
            for (var k = 0; k < gblmasterBillerAndTopupBB[i]["BillerMiscData"].length; k++) {
                if (gblmasterBillerAndTopupBB[i]["BillerMiscData"][k]["MiscName"] == "BB.IsRequiredRefNumber2") {
                    tmpRef2BB = gblmasterBillerAndTopupBB[i]["BillerMiscData"][k]["MiscText"];
                }
                if (gblmasterBillerAndTopupBB[i]["BillerMiscData"][k]["MiscName"] == "BB.Ref2.INPUT.TYPE") {
                    tmpRef2Type = gblmasterBillerAndTopupBB[i]["BillerMiscData"][k]["MiscText"];
                }
                if (gblmasterBillerAndTopupBB[i]["BillerMiscData"][k]["MiscName"] == "BB.Ref2.INPUT.VALUE") {
                    tmpRef2Data = gblmasterBillerAndTopupBB[i]["BillerMiscData"][k]["MiscText"];
                }
            }
            if (tmpRef2BB == "Y") {
                currForm.hbxBillerRef2.setVisibility(true);
                currForm.line1183973868203.isVisible = true;
                if (tmpRef2Type == "text") {
                    currForm.txtAddBillerRef2.setVisibility(true);
                    if (currForm.id == 'frmIBBeepAndBillApplyCW') currForm.comboboxRef2.setVisibility(false);
                } else if (tmpRef2Type == "Dropdown") {
                    currForm.txtAddBillerRef2.setVisibility(false);
                    if (currForm.id == 'frmIBBeepAndBillApplyCW') currForm.comboboxRef2.setVisibility(true);
                    var masterData = [
                        [0, "" + kony.i18n.getLocalizedString("keySelectRef2BB")]
                    ];
                    var tempData;
                    var ref2List = tmpRef2Data.split(",");
                    for (var j = 0; j < ref2List.length; j++) {
                        tempData = [j + 1, ref2List[j]];
                        masterData.push(tempData);
                    }
                    if (currForm.id == 'frmIBBeepAndBillApplyCW') currForm.comboboxRef2.masterData = masterData;
                }
            } else {
                currForm.hbxBillerRef2.setVisibility(false);
                currForm.line1183973868203.isVisible = false;
            }
            if (kony.i18n.getCurrentLocale() == "en_US") {
                for (var j = 0; j < gblmasterBillerAndTopupBB[i]["BillerMiscData"].length; j++) {
                    if (gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref1.LABEL.EN") {
                        currForm.lblAddBillerRef1.text = gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscText"];
                    }
                    if (gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref2.LABEL.EN") {
                        currForm.lblAddBillerRef2.text = gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscText"];
                    }
                    if (gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref1.LEN") {
                        currForm.txtAddBillerRef1.maxTextLength = gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscText"];
                    }
                    if (gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref2.LEN") {
                        currForm.txtAddBillerRef1.maxTextLength = gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscText"];
                    }
                }
            } else {
                for (var j = 0; j < gblmasterBillerAndTopupBB[i]["BillerMiscData"].length; j++) {
                    if (gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref1.LABEL.TH") {
                        currForm.lblAddBillerRef1.text = gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscText"];
                    }
                    if (gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref2.LABEL.TH") {
                        currForm.lblAddBillerRef2.text = gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscText"];
                    }
                    if (gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref1.LEN") {
                        currForm.txtAddBillerRef1.maxTextLength = gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscText"];
                    }
                    if (gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref2.LEN") {
                        currForm.txtAddBillerRef1.maxTextLength = gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscText"];
                    }
                }
            }
        }
    }
}