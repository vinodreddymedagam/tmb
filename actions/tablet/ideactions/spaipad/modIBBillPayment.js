gBillerEndTime = "";
gBillerStartTime = "";

function segBPBillsListOnClick() {
    showLoadingScreenPopup();
    gblBillerPresentInMyBills = true;
    gblFirstTimeBillPayment = false;
    gblFullPayment = false;
    gblBillPaymentEdit = false;
    gblFromAccountSummary = false;
    IBBillsCurrentSystemDate();
    clearIBBillPaymentLP();
    showSchedBillerNicknameIB();
    frmIBBillPaymentLP.lblFullPayment.setVisibility(false);
    frmIBBillPaymentLP.lblBPAmount.containerWeight = 20;
    frmIBBillPaymentLP.lblBPAmount.setVisibility(true);
    frmIBBillPaymentLP.txtBillAmt.setVisibility(true);
    frmIBBillPaymentLP.lblTHB.setVisibility(true);
    frmIBBillPaymentLP.imgBPBillerImage.setVisibility(true);
    fn_resetIBMEABillerDetails();
    billerMethod = frmIBBillPaymentLP["segBPBillsList"]["selectedItems"][0]["billerMethod"];
    gblBillerMethod = billerMethod;
    gblToAccountKey = frmIBBillPaymentLP["segBPBillsList"]["selectedItems"][0]["ToAccountKey"];
    gblToAccountKeyUnAltered = frmIBBillPaymentLP["segBPBillsList"]["selectedItems"][0]["ToAccountKey"];
    gblBillerID = frmIBBillPaymentLP["segBPBillsList"]["selectedItems"][0]["CustomerBillID"];
    gblPayFull = frmIBBillPaymentLP["segBPBillsList"]["selectedItems"][0]["IsFullPayment"];
    //ENh 113
    //gBillerStartTime = indexOfSelectedIndex.billerStartTime;
    //gBillerEndTime = indexOfSelectedIndex.billerEndTime;
    // billerMethod = 0;
    gblbillerGroupType = frmIBBillPaymentLP["segBPBillsList"]["selectedItems"][0]["billerGroupType"];
    gblreccuringDisableAdd = frmIBBillPaymentLP["segBPBillsList"]["selectedItems"][0]["IsRequiredRefNumber2Add"];
    gblreccuringDisablePay = frmIBBillPaymentLP["segBPBillsList"]["selectedItems"][0]["IsRequiredRefNumber2Pay"];
    gblBillerBancassurance = frmIBBillPaymentLP["segBPBillsList"]["selectedItems"][0]["billerBancassurance"];
    gblAllowRef1AlphaNum = frmIBBillPaymentLP["segBPBillsList"]["selectedItems"][0]["allowRef1AlphaNum"];
    if (!frmIBBillPaymentLP.hbxRef1.isVisible) {
        frmIBBillPaymentLP.hbxRef1.setVisibility(true);
    }
    if (gblreccuringDisablePay == "Y") {
        frmIBBillPaymentLP.hbxReference2.setVisibility(true);
        frmIBBillPaymentLP.lblRefVal2.setEnabled(true);
    } else {
        frmIBBillPaymentLP.hbxReference2.setVisibility(false);
    }
    gblCompCode = frmIBBillPaymentLP["segBPBillsList"]["selectedItems"][0]["BillerCompCode"];
    frmIBBillPaymentLP.lblBPBillerName.text = frmIBBillPaymentLP["segBPBillsList"]["selectedItems"][0]["lblBillCompCode"];
    frmIBBillPaymentLP.imgBPBillerImage.src = frmIBBillPaymentLP["segBPBillsList"]["selectedItems"][0]["imgBills"].src;
    frmIBBillPaymentLP.lblBPRef1Val.text = frmIBBillPaymentLP["segBPBillsList"]["selectedItems"][0]["ccOrAccNumMasked"];
    //below line is added for CR - PCI-DSS masked Credit card no
    frmIBBillPaymentLP.lblBPRef1ValMasked.text = frmIBBillPaymentLP["segBPBillsList"]["selectedItems"][0]["lblRef1"];
    frmIBBillPaymentLP.lblBPRef1.text = frmIBBillPaymentLP["segBPBillsList"]["selectedItems"][0]["lblRf1"];
    frmIBBillPaymentLP.lblBPRef1Val.maxTextLength = parseInt(frmIBBillPaymentLP["segBPBillsList"]["selectedItems"][0]["Ref1Len"]);
    gblRef1LblEN = frmIBBillPaymentLP["segBPBillsList"]["selectedItems"][0]["lblRf1EN"];
    gblRef2LblEN = frmIBBillPaymentLP["segBPBillsList"]["selectedItems"][0]["lblRf2EN"];
    gblRef1LblTH = frmIBBillPaymentLP["segBPBillsList"]["selectedItems"][0]["lblRf1TH"];
    gblRef2LblTH = frmIBBillPaymentLP["segBPBillsList"]["selectedItems"][0]["lblRf2TH"];
    gblBillerCompCodeEN = frmIBBillPaymentLP["segBPBillsList"]["selectedItems"][0]["lblBillCompCodeEN"];
    gblBillerCompCodeTH = frmIBBillPaymentLP["segBPBillsList"]["selectedItems"][0]["lblBillCompCodeTH"];
    frmIBBillPaymentLP.lblRefVal2.text = frmIBBillPaymentLP["segBPBillsList"]["selectedItems"][0]["lblRef2"];
    frmIBBillPaymentLP.lblRefVal2.maxTextLength = parseInt(frmIBBillPaymentLP["segBPBillsList"]["selectedItems"][0]["Ref2Len"]);
    frmIBBillPaymentLP.lblRef2.text = frmIBBillPaymentLP["segBPBillsList"]["selectedItems"][0]["lblRf2"];
    frmIBBillPaymentLP.lblToCompCode.text = frmIBBillPaymentLP["segBPBillsList"]["selectedItems"][0]["lblBillsName"]; //frmIBBillPaymentLP["segBPBillsList"]["selectedItems"][0]["lblBillCompCode"];
    //frmIBBillPaymentLP.lblRefVal2.setEnabled(true);
    if (gblCompCode == "2533") {
        gblSegBillerData = frmIBBillPaymentLP["segBPBillsList"]["selectedItems"][0];
        // var tmpIsRef2Req = "";
        gBillerStartTime = frmIBBillPaymentLP["segBPBillsList"]["selectedItems"][0]["billerStartTime"];
        gBillerEndTime = frmIBBillPaymentLP["segBPBillsList"]["selectedItems"][0]["billerEndTime"];
        gblbillerTransType = frmIBBillPaymentLP["segBPBillsList"]["selectedItems"][0]["billerTransType"];
        gblbillerServiceType = frmIBBillPaymentLP["segBPBillsList"]["selectedItems"][0]["billerServiceType"];
        gblMeaFeeAmount = frmIBBillPaymentLP["segBPBillsList"]["selectedItems"][0]["billerFeeAmount"];
    } else {
        frmIBBillPaymentLP.btnBPPayOn.setEnabled(true);
    }
    if (billerMethod == 0) { //Offline Biller
        mapOfflineBiller();
    } else if (billerMethod == 1) { //Online Biller
        mapOnlineBiller();
    } else if (billerMethod == 2) { // TMB Credit Card/Ready Cash
        mapTMBCreditCard();
    } else if (billerMethod == 3) { //TMB Loan
        mapTMBLoan();
    } else if (billerMethod == 4) { //Ready Cash		
        mapReadyCash();
    }
    frmIBBillPaymentLP.hbxToBiller.setVisibility(false);
    frmIBBillPaymentLP.hbxImage.setVisibility(true);
    //frmIBBillPaymentLP.show();	
}

function mapOfflineBiller() {
    showLoadingScreenPopup();
    gblPenalty = false;
    gblFullPayment = false;
    frmIBBillPaymentLP.lblBPAmount.text = kony.i18n.getLocalizedString("keyAmount");
    //Penalty Hide
    frmIBBillPaymentLP.hbxPenalty.setVisibility(false);
    //Hide All
    frmIBBillPaymentLP.btnAmtFull.setVisibility(false);
    frmIBBillPaymentLP.btnAmtFull.setEnabled(false);
    frmIBBillPaymentLP.btnAmtMinimum.setVisibility(false);
    frmIBBillPaymentLP.btnAmtMinimum.setEnabled(false);
    frmIBBillPaymentLP.btnAmtSpecified.setEnabled(false);
    frmIBBillPaymentLP.btnAmtSpecified.setVisibility(false);
    frmIBBillPaymentLP.hbxMinMax.setVisibility(false);
    frmIBBillPaymentLP.txtBillAmt.text = ""; //set value from service
    frmIBBillPaymentLP.txtBillAmt.setEnabled(true);
    frmIBBillPaymentLP.txtBillAmt.setVisibility(true);
    frmIBBillPaymentLP.lblTHB.setVisibility(true);
    frmIBBillPaymentLP.hbxAmtVal.setVisibility(true);
    frmIBBillPaymentLP.lblFullPayment.setVisibility(false);
    if (gblBillerBancassurance == "Y") {
        gblPolicyNumber = "";
        var inputParam = {};
        inputParam["policyNumber"] = frmIBBillPaymentLP.lblBPRef1Val.text;
        inputParam["dataSet"] = "0";
        invokeServiceSecureAsync("BAGetPolicyDetails", inputParam, BillPaymentBAPolicyDetailsServiceCallBack);
    } else {
        gblPayPremium = "N";
        dismissLoadingScreenPopup();
    }
}

function BillPaymentBAPolicyDetailsServiceCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            var billPaymentValueDS = resulttable["billPaymentValueDS"];
            var policyDetailsValueDS = resulttable["policyDetailsValueDS"];
            if (policyDetailsValueDS.length > 0) {
                if (policyDetailsValueDS[0]["value_EN"] == "1") {
                    if (billPaymentValueDS.length > 0) {
                        frmIBBillPaymentLP.lblRefVal2.text = billPaymentValueDS[3]["value_EN"];
                        frmIBBillPaymentLP.txtBillAmt.text = billPaymentValueDS[4]["value_EN"];
                        frmIBBillPaymentLP.lblRefVal2.setEnabled(true);
                        frmIBBillPaymentLP.txtBillAmt.setEnabled(true);
                        dismissLoadingScreenPopup();
                        gblPayPremium = "Y";
                    } else {
                        gblPayPremium = "N";
                        //Biller details not found in system, allow payment as normal offline biller 
                        dismissLoadingScreenPopup();
                    }
                } else {
                    gblPayPremium = "N";
                    clearIBBillPaymentLPOnRef1Ref2Done();
                    //clearIBBillPaymentLP();
                    dismissLoadingScreenPopup();
                    showAlert(kony.i18n.getLocalizedString("BA_Policy_Already_Paid"), kony.i18n.getLocalizedString("info"));
                    return false;
                }
            } else {
                gblPayPremium = "N";
                //Policy details not found in system, allow payment as normal offline biller 
                dismissLoadingScreenPopup();
            }
        } else {
            gblPayPremium = "N";
            //Policy not found in system, allow payment as normal offline biller 
            dismissLoadingScreenPopup();
        }
    }
}

function mapOnlineBiller() {
    showLoadingScreenPopup();
    //  callGenerateTransferRefNoserviceIBBillPay();
    var inputParam = {};
    // inputParam["SvcProvider"] = "AIS";
    // inputParam["ChannelName"] = "INTERNET";
    //inputParam["RqUID"] = "";
    var ref2Input = "";
    if (typeof(frmIBBillPaymentLP.lblRefVal2.text) != "undefined") {
        ref2Input = frmIBBillPaymentLP.lblRefVal2.text;
    }
    inputParam["TrnId"] = "";
    inputParam["BankId"] = "011";
    inputParam["BranchId"] = "0001";
    inputParam["BankRefId"] = "";
    inputParam["Amt"] = "0.00";
    //inputParam["OnlinePmtType"] = "";
    inputParam["Ref1"] = frmIBBillPaymentLP.lblBPRef1Val.text;
    inputParam["Ref2"] = ref2Input; //frmIBBillPaymentLP.lblRefVal2.text;
    inputParam["Ref3"] = "";
    inputParam["Ref4"] = "";
    inputParam["MobileNumber"] = frmIBBillPaymentLP.lblBPRef1Val.text;
    inputParam["compCode"] = gblCompCode; //frmIBBillPaymentLP["segBPBillsList"]["selectedItems"][0]["BillerCompCode"];
    inputParam["isChannel"] = "IB";
    inputParam["commandType"] = "Inquiry";
    inputParam["BillerGroupType"] = "0";
    if (gblCompCode == "2533") {
        inputParam["BillerStartTime"] = gBillerStartTime;
        inputParam["BillerEndTime"] = gBillerEndTime;
    }
    invokeServiceSecureAsync("onlinePaymentInq", inputParam, BillPaymentOnlinePaymentInqServiceCallBackIB);
    gblPenalty = true;
    //frmIBBillPaymentLP.lblBPAmount.text = kony.i18n.getLocalizedString("keyAmount");
    //Penalty Show
    // frmIBBillPaymentLP.hbxPenalty.setVisibility(true);
    //Only Full and Specified
    if (gblCompCode == "2533" || gblCompCode == "0016" || gblCompCode == "0947") { //No need to show these for MEA biller
        frmIBBillPaymentLP.btnAmtFull.setEnabled(false);
        frmIBBillPaymentLP.btnAmtFull.setVisibility(false);
        frmIBBillPaymentLP.btnAmtMinimum.setEnabled(false);
        frmIBBillPaymentLP.btnAmtMinimum.setVisibility(false);
        frmIBBillPaymentLP.btnAmtSpecified.setEnabled(false);
        frmIBBillPaymentLP.btnAmtSpecified.setVisibility(false);
        frmIBBillPaymentLP.hbxMinMax.setVisibility(false);
        frmIBBillPaymentLP.txtBillAmt.setVisibility(false); //set value
        frmIBBillPaymentLP.lblTHB.setVisibility(false);
    } else {
        frmIBBillPaymentLP.btnAmtFull.setEnabled(true);
        frmIBBillPaymentLP.btnAmtFull.setVisibility(true);
        frmIBBillPaymentLP.btnAmtMinimum.setEnabled(false);
        frmIBBillPaymentLP.btnAmtMinimum.setVisibility(false);
        frmIBBillPaymentLP.btnAmtSpecified.setEnabled(true);
        frmIBBillPaymentLP.btnAmtSpecified.setVisibility(true);
        frmIBBillPaymentLP.hbxMinMax.setVisibility(true);
        //Store Full value in Global
        frmIBBillPaymentLP.txtBillAmt.text = ""; //set value
    }
}

function mapTMBCreditCard() {
    showLoadingScreenPopup();
    //for CreditCard and Ready cash
    ownCard = false;
    var inputParam = {};
    var toDayDate = getTodaysDate();
    var cardId = frmIBBillPaymentLP.lblBPRef1Val.text;
    inputParam["cardId"] = cardId;
    //inputParam["waiverCode"] = WAIVERCODE;
    inputParam["tranCode"] = TRANSCODEMIN;
    inputParam["postedDt"] = toDayDate;
    var accountDetails = gblAccountTable;
    var accountLength = gblAccountTable["custAcctRec"].length;
    for (var i = 0; i < accountLength; i++) {
        if (gblAccountTable["custAcctRec"][i].accType == "CCA") {
            var accountNo = gblAccountTable["custAcctRec"][i].accId;
            accountNo = accountNo.substring(accountNo.length - 16, accountNo.length);
            if (accountNo == cardId) {
                ownCard = true;
                break;
            }
        }
    }
    if (!ownCard) {
        frmIBBillPaymentLP.btnAmtFull.setEnabled(false);
        frmIBBillPaymentLP.btnAmtFull.setVisibility(false);
        frmIBBillPaymentLP.btnAmtMinimum.setEnabled(false);
        frmIBBillPaymentLP.btnAmtMinimum.setVisibility(false);
        frmIBBillPaymentLP.btnAmtSpecified.setEnabled(false);
        frmIBBillPaymentLP.btnAmtSpecified.setVisibility(false);
        frmIBBillPaymentLP.hbxMinMax.setVisibility(false);
        frmIBBillPaymentLP.hbxAmtVal.isVisible = true;
        frmIBBillPaymentLP.txtBillAmt.text = "";
        frmIBBillPaymentLP.txtBillAmt.setEnabled(true);
        frmIBBillPaymentLP.hbxAmtVal.setVisibility(true);
        frmIBBillPaymentLP.txtBillAmt.setEnabled(true);
        frmIBBillPaymentLP.txtBillAmt.setVisibility(true);
        gblFullPayment = false;
        frmIBBillPaymentLP.lblTHB.setVisibility(true);
        frmIBBillPaymentLP.lblFullPayment.setVisibility(false);
        dismissLoadingScreenPopup();
        return;
    }
    invokeServiceSecureAsync("creditcardDetailsInq", inputParam, BillPaymentcreditcardDetailsInqServiceCallBackIB);
}

function mapReadyCash() {
    showLoadingScreenPopup();
    var TMB_BANK_FIXED_CODE = "0001";
    var TMB_BANK_CODE_ADD = "0011";
    var ZERO_PAD = "0000";
    var BRANCH_CODE;
    var ref1AccountIDDeposit = frmIBBillPaymentLP.lblBPRef1Val.text;
    var fiident;
    var inputParam = {};
    inputparam["acctId"] = ref1AccountIDDeposit;
    ownRC = false;
    var accountDetails = gblAccountTable;
    var accountLength = gblAccountTable["custAcctRec"].length;
    for (var i = 0; i < accountLength; i++) {
        if (gblAccountTable.accType == "LOC") {
            var accountNo = gblAccountTable["custAcctRec"][i].accId;
            accountNo = accountNo.substring(accountNo.length - 16, accountNo.length);
            if (accountNo == ref1AccountIDDeposit) {
                ownRC = true;
                break;
            }
        }
    }
    if (!ownRC) {
        gblPenalty = false;
        frmIBBillPaymentLP.lblBPAmount.text = kony.i18n.getLocalizedString("keyAmount");
        //Penalty Hide
        frmIBBillPaymentLP.hbxPenalty.setVisibility(false);
        frmIBBillPaymentLP.btnAmtFull.setEnabled(false);
        frmIBBillPaymentLP.btnAmtFull.setVisibility(false);
        frmIBBillPaymentLP.btnAmtMinimum.setEnabled(false);
        frmIBBillPaymentLP.btnAmtMinimum.setVisibility(false);
        frmIBBillPaymentLP.btnAmtSpecified.setEnabled(false);
        frmIBBillPaymentLP.btnAmtSpecified.setVisibility(false);
        frmIBBillPaymentLP.hbxMinMax.setVisibility(false);
    }
    invokeServiceSecureAsync("depositAccountInquiry", inputParam, BillPaymentdepositAccountInquiryServiceCallBackIB);
    gblPenalty = false;
    frmIBBillPaymentLP.lblBPAmount.text = kony.i18n.getLocalizedString("keyAmount");
    //Penalty Hide
    frmIBBillPaymentLP.hbxPenalty.setVisibility(true);
    frmIBBillPaymentLP.btnAmtFull.setEnabled(true);
    frmIBBillPaymentLP.btnAmtFull.setVisibility(true);
    frmIBBillPaymentLP.btnAmtMinimum.setEnabled(false);
    frmIBBillPaymentLP.btnAmtMinimum.setVisibility(false);
    frmIBBillPaymentLP.btnAmtSpecified.setEnabled(true);
    frmIBBillPaymentLP.btnAmtSpecified.setVisibility(true);
    frmIBBillPaymentLP.hbxMinMax.setVisibility(true);
}

function mapTMBLoan() {
    showLoadingScreenPopup();
    //for TMB Loan
    var TMB_BANK_FIXED_CODE = "0001";
    var TMB_BANK_CODE_ADD = "0011";
    var ZERO_PAD = "0000";
    var BRANCH_CODE;
    var ref1AccountIDLoan = frmIBBillPaymentLP.lblBPRef1Val.text;
    var ref2 = frmIBBillPaymentLP.lblRefVal2.text;
    var fiident;
    if (ref1AccountIDLoan.length == 10) {
        fiident = TMB_BANK_CODE_ADD + TMB_BANK_FIXED_CODE + "0" + ref1AccountIDLoan[0] + ref1AccountIDLoan[1] + ref1AccountIDLoan[2] + ZERO_PAD;
        ref1AccountIDLoan = "0" + ref1AccountIDLoan + ref2;
    }
    if (ref1AccountIDLoan.length == 13) {
        fiident = TMB_BANK_CODE_ADD + TMB_BANK_FIXED_CODE + "0" + ref1AccountIDLoan[0] + ref1AccountIDLoan[1] + ref1AccountIDLoan[2] + ZERO_PAD;
        ref1AccountIDLoan = "0" + ref1AccountIDLoan
    } else {
        fiident = TMB_BANK_CODE_ADD + TMB_BANK_FIXED_CODE + "0" + ref1AccountIDLoan[1] + ref1AccountIDLoan[2] + ref1AccountIDLoan[3] + ZERO_PAD;
    }
    var inputParam = {};
    inputParam["acctId"] = ref1AccountIDLoan;
    // inputParam["acctId"]="001100010330000003305702239001";
    //inputparam["acctId"] = gblAccountTable["custAcctRec"][gblIndex]["fiident"] + gblAccountTable["custAcctRec"][gblIndex]["accId"]
    //inputparam["acctType"] = gblAccountTable["custAcctRec"][gblIndex]["accType"];
    inputParam["acctType"] = "LOC";
    ownLoan = false;
    var accountDetails = gblAccountTable;
    var accountLength = gblAccountTable["custAcctRec"].length;
    for (var i = 0; i < accountLength; i++) {
        if (gblAccountTable["custAcctRec"][i].accType == "LOC") {
            var accountNo = gblAccountTable["custAcctRec"][i].accId;
            //accountNo = accountNo.substring(accountNo.length-13 , accountNo.length);
            if (accountNo == ref1AccountIDLoan) {
                ownLoan = true;
                break;
            }
        }
    }
    if (!ownLoan) {
        frmIBBillPaymentLP.btnAmtFull.setEnabled(false);
        frmIBBillPaymentLP.btnAmtFull.setVisibility(false);
        frmIBBillPaymentLP.btnAmtMinimum.setEnabled(false);
        frmIBBillPaymentLP.btnAmtMinimum.setVisibility(false);
        frmIBBillPaymentLP.btnAmtSpecified.setEnabled(false);
        frmIBBillPaymentLP.btnAmtSpecified.setVisibility(false);
        frmIBBillPaymentLP.hbxMinMax.setVisibility(false);
        frmIBBillPaymentLP.hbxAmtVal.isVisible = false;
        frmIBBillPaymentLP.txtBillAmt.text = "";
        frmIBBillPaymentLP.txtBillAmt.setEnabled(true);
        frmIBBillPaymentLP.txtBillAmt.setVisibility(true);
        gblFullPayment = false;
        frmIBBillPaymentLP.lblTHB.setVisibility(true);
        frmIBBillPaymentLP.hbxAmtVal.setVisibility(true);
        dismissLoadingScreenPopup();
        return;
    }
    invokeServiceSecureAsync("doLoanAcctInq", inputParam, BillPaymentdoLoanAcctInqServiceCallBackIB);
    gblPenalty = false;
    frmIBBillPaymentLP.lblBPAmount.text = kony.i18n.getLocalizedString("keyAmount");
    //Penalty Hide
    frmIBBillPaymentLP.hbxPenalty.setVisibility(false);
    //min hide
    frmIBBillPaymentLP.btnAmtFull.setEnabled(true);
    frmIBBillPaymentLP.btnAmtFull.setVisibility(true);
    frmIBBillPaymentLP.btnAmtMinimum.setEnabled(false);
    frmIBBillPaymentLP.btnAmtMinimum.setVisibility(false);
    frmIBBillPaymentLP.btnAmtSpecified.setEnabled(true);
    frmIBBillPaymentLP.btnAmtSpecified.setVisibility(true);
    frmIBBillPaymentLP.hbxMinMax.setVisibility(true);
}
/**************************************************************************************
		Module	: onClickButnBillerIB
		Author  : Kony
		Date    : June 27, 2013
		Purpose : on clikc enables the biller module
*****************************************************************************************/
function onClickButnBillerIB() {
    if (frmIBBillPaymentLP.hbxToBiller.isVisible == false) {
        frmIBBillPaymentLP.hbxToBiller.setVisibility(true);
        showLoadingScreenPopup();
        getSelectBillerCategoryService();
    } else {
        frmIBBillPaymentLP.hbxToBiller.setVisibility(false);
        frmIBBillPaymentLP.hbxImage.setVisibility(true);
    }
    billPaymentList = frmIBBillPaymentLP.segBPBillsList.data;
    billPaymentSuggestList = frmIBBillPaymentLP.segBPSgstdBillerList.data;
    frmIBBillPaymentLP.line1010718521255436.setVisibility(true);
}

function clearIBBillPaymentLP() {
    if (gblBillPaymentEdit) {
        gblBillPaymentEdit = false;
        if (frmIBBillPaymentLP.lblFullPayment.isVisible) {
            gblFullPayment = true;
        } else {
            gblFullPayment = false;
        }
    } else {
        //	gblFullPayment=true;
        frmIBBillPaymentLP.lblBPBillerName.text = "";
        frmIBBillPaymentLP.lblToCompCode.text = "";
        frmIBBillPaymentLP.lblBPRef1Val.text = "";
        //below line is added for CR - PCI-DSS masked Credit card no
        frmIBBillPaymentLP.lblBPRef1ValMasked.text = "";
        frmIBBillPaymentLP.lblRefVal2.text = "";
        frmIBBillPaymentLP.hbxRef1.setVisibility(false);
        frmIBBillPaymentLP.hbxReference2.setVisibility(false);
        frmIBBillPaymentLP.hbxMinMax.setVisibility(false);
        frmIBBillPaymentLP.txtBillAmt.text = "";
        frmIBBillPaymentLP.lblPenaltyVal.text = "";
        frmIBBillPaymentLP.txtAreaBPMyNote.text = "";
        frmIBBillPaymentLP.lblDatesFuture.text = "";
        frmIBBillPaymentLP.lblrepeats.text = "";
        frmIBBillPaymentLP.imgBPBillerImage.setVisibility(false);
        frmIBBillPaymentLP.txtBillAmt.placeholder = "0.00";
        frmIBBillPaymentLP.lblFullPayment.text = "0.00 " + kony.i18n.getLocalizedString("currencyThaiBaht");
        frmIBBillPaymentLP.lblBPBillPayDate.setVisibility(true);
        frmIBBillPaymentLP.lblDatesFuture.setVisibility(false);
    }
}

function customWidgetSelectEventBillPmntIB() {
    showLoadingScreenPopup();
    billPaySummary = false;
    var selectedItem = frmIBBillPaymentCW.customIBBPFromAccount.selectedItem;
    var selectedData = frmIBBillPaymentCW.customIBBPFromAccount.data[gblCWSelectedItem];
    frmIBBillPaymentLP.lblBPFromNameRcvd.text = selectedData.lblCustName;
    accntNme = selectedData.AccountName
    gblNickNameEN = selectedData.lblAccountNickNameEN;
    gblNickNameTH = selectedData.lblAccountNickNameTH;
    gblProductNameEN = selectedData.lblProductValEN;
    gblProductNameTH = selectedData.lblProductValTH;
    frmIBBillPaymentLP.lblAcctType.text = selectedData.lblSliderAccN2;
    frmIBBillPaymentLP.lblFromAccNo.text = selectedData.lblActNoval;
    //gblAccountNameOfUser=selectedData.AccountName;
    // //
    //getAccountNameOfUser(selectedData.lblActNoval);
    frmIBBillPaymentLP.lblBalanceFromValue.text = selectedData.lblBalance;
    frmIBBillPaymentLP.imgFrom.src = selectedData.img1;
    frmIBBillPaymentLP.lblDummy.text = selectedData.lblDummy;
    frmIBBillPaymentLP.lblProductType.text = selectedData.lblProductVal;
    frmIBBillPaymentLP.hbxFromBalance.isVisible = true;
    frmIBBillPaymentLP.hbxFrom.setVisibility(true);
    frmIBBillPaymentLP.hbxImage.setVisibility(true);
    if (selectedData.lblRemFee != "" && selectedData.lblRemFeeval != "") {
        frmIBBillPaymentLP.lblFreeTran.text = selectedData.lblRemFee;
        frmIBBillPaymentLP.lblFreeTransValue.text = selectedData.lblRemFeeval;
        frmIBBillPaymentLP.hbxfreeTransaction.setVisibility(true);
        gblBillpaymentNoFee = true;
    } else {
        gblBillpaymentNoFee = false;
        frmIBBillPaymentLP.hbxfreeTransaction.setVisibility(false);
        frmIBBillPaymentLP.lblFreeTran.text = "";
        frmIBBillPaymentLP.lblFreeTransValue.text = "";
    }
    // gblFromAccountSummary = true;
    if (gblFromAccountSummary || gblFromAccountSummaryBillerAdded) { //by default false
        //few more things to map  
        frmIBBillPaymentLP.lblBPBillerName.text = frmIBBillPaymentCW.lblBPBillerName.text;
        frmIBBillPaymentLP.lblToCompCode.text = frmIBBillPaymentCW.lblToCompCode.text;
        frmIBBillPaymentLP.imgBPBillerImage.src = frmIBBillPaymentCW.imgBPBillerImage.src;
        frmIBBillPaymentLP.hbxRef1.setVisibility(true);
        frmIBBillPaymentLP.imgBPBillerImage.setVisibility(true);
        frmIBBillPaymentLP.lblBPRef1.text = frmIBBillPaymentCW.lblBPRef1.text;
        frmIBBillPaymentLP.lblBPRef1Val.text = frmIBBillPaymentCW.lblBPRef1Val.text;
        //below line is added for CR - PCI-DSS masked Credit card no
        frmIBBillPaymentLP.lblBPRef1ValMasked.text = frmIBBillPaymentCW.lblBPRef1ValMasked.text;
        if (frmIBBillPaymentCW.hbxRef2.isVisible) {
            frmIBBillPaymentLP.hbxReference2.setVisibility(true);
            frmIBBillPaymentLP.lblRef2.text = frmIBBillPaymentCW.lblBPRef2.text;
            frmIBBillPaymentLP.lblRefVal2.text = frmIBBillPaymentCW.txtBPRef2Val.text;
        } else {
            frmIBBillPaymentLP.hbxReference2.setVisibility(false);
            frmIBBillPaymentLP.lblRef2.text = "";
            frmIBBillPaymentLP.lblRefVal2.text = "";
        }
        frmIBBillPaymentLP.hbxAmount.setVisibility(true);
        frmIBBillPaymentLP.hbxMinMax.setVisibility(true);
        if (frmIBBillPaymentCW.lblFullPayment.isVisible) {
            frmIBBillPaymentLP.lblFullPayment.setVisibility(true);
        } else {
            frmIBBillPaymentLP.lblFullPayment.setVisibility(false);
        }
        if (frmIBBillPaymentCW.txtBillAmt.isVisible) {
            frmIBBillPaymentLP.txtBillAmt.setVisibility(true);
            frmIBBillPaymentLP.lblTHB.setVisibility(true);
            frmIBBillPaymentLP.txtBillAmt.setEnabled(true);
            frmIBBillPaymentLP.txtBillAmt.text = frmIBBillPaymentCW.txtBillAmt.text;
        } else {
            frmIBBillPaymentLP.txtBillAmt.setVisibility(false);
            frmIBBillPaymentLP.lblTHB.setVisibility(false);
            frmIBBillPaymentLP.txtBillAmt.setEnabled(false);
            frmIBBillPaymentLP.txtBillAmt.text = "";
        }
        if (frmIBBillPaymentCW.btnAmtMinimum.isVisible) {
            frmIBBillPaymentLP.btnAmtMinimum.setVisibility(true)
            frmIBBillPaymentLP.btnAmtMinimum.setEnabled(true)
        } else {
            frmIBBillPaymentLP.btnAmtMinimum.setVisibility(false)
        }
        if (frmIBBillPaymentCW.btnAmtFull.isVisible) {
            frmIBBillPaymentLP.btnAmtFull.setVisibility(true)
            frmIBBillPaymentLP.btnAmtFull.setEnabled(true)
        } else {
            frmIBBillPaymentLP.btnAmtFull.setVisibility(false)
        }
        if (frmIBBillPaymentCW.btnAmtSpecified.isVisible) {
            frmIBBillPaymentLP.btnAmtSpecified.setVisibility(true)
            frmIBBillPaymentLP.btnAmtSpecified.setEnabled(true);
        } else {
            frmIBBillPaymentLP.btnAmtSpecified.setVisibility(false)
        }
        if (gblBillerBancassurance == "Y") {
            //Ref2 should not be editable when coming from BA Policy Details PayBill shortcut
            frmIBBillPaymentLP.lblRefVal2.setEnabled(false);
            frmIBBillPaymentLP.txtBillAmt.setEnabled(false);
        } else {
            var fullSkin = frmIBBillPaymentCW.btnAmtFull.skin;
            var miniSkin = frmIBBillPaymentCW.btnAmtMinimum.skin;
            var speciSkin = frmIBBillPaymentCW.btnAmtSpecified.skin;
            if (fullSkin == "btnIBTab3LeftFocus77px") {
                frmIBBillPaymentLP.lblFullPayment.text = frmIBBillPaymentCW.lblFullPayment.text;
                eh_frmIBBillPaymentLP_btnAmtFull_onClick();
            } else if (miniSkin == "btnIBTab3MidFocus77px") {
                frmIBBillPaymentLP.txtBillAmt.text = frmIBBillPaymentCW.txtBillAmt.text;
                eh_frmIBBillPaymentLP_btnAmtMinimum_onClick();
            } else if (speciSkin == "btnIBTab3RightFocus77px") {
                frmIBBillPaymentLP.txtBillAmt.text = frmIBBillPaymentCW.txtBillAmt.text;
                eh_frmIBBillPaymentLP_btnAmtSpecified_onClick();
            }
        }
    }
    frmIBBillPaymentLP.show();
}

function validateChannelIBBillPay() {
    if (frmIBBillPaymentLP.lblBPBillerName.text == null || frmIBBillPaymentLP.lblBPBillerName.text == "" || frmIBBillPaymentLP.lblBPBillerName.text == " " || parseFloat(frmIBBillPaymentLP.lblBPBillerName.text) == NaN) {
        alert(kony.i18n.getLocalizedString("Valid_SelBiller"));
        return;
    } else {
        var inputParams = {
            billerCompcode: gblCompCode
        };
        showLoadingScreenPopup();
        invokeServiceSecureAsync("masterBillerInquiry", inputParams, validateChannelIBBillPayCallBack);
    }
}

function validateChannelIBBillPayCallBack(status, callBackResponse) {
    if (status == 400) {
        dismissLoadingScreenPopup();
        if (callBackResponse["opstatus"] == "0") {
            var responseData = callBackResponse["MasterBillerInqRs"];
            if (responseData.length > 0) {
                gblBillerCategoryID = responseData[0]["BillerCategoryID"];
                gblBillerId = responseData[0]["BillerID"];
                validatingBPEnteredDetailsIB();
            } else {
                if (callBackResponse["validFor"] == "02") alert(kony.i18n.getLocalizedString("keyNotValidForIB"));
                else alert(kony.i18n.getLocalizedString("keyNotValidForBillPay"));
            }
        } else {
            dismissLoadingScreenPopup();
            alert("No Suggested Biller found");
            return;
        }
    } else {
        if (status == 300) {
            dismissLoadingScreenPopup();
            alert("No Suggested Biller found");
            return;
        }
    }
}

function validatingBPEnteredDetailsIB() {
    var billerSelected = false;
    var amountSelected = false;
    billAmountBillPay = "";
    //lblBillerName - lblBPBillerName
    if (frmIBBillPaymentLP.lblBPBillerName.text == null || frmIBBillPaymentLP.lblBPBillerName.text == "" || frmIBBillPaymentLP.lblBPBillerName.text == " " || parseFloat(frmIBBillPaymentLP.lblBPBillerName.text) == NaN) {
        alert(kony.i18n.getLocalizedString("Valid_SelBiller"));
        billerSelected = false;
        //dismissLoadingScreenPopup();
        return;
    } else {
        billerSelected = true;
    }
    if (!isNotBlank(frmIBBillPaymentLP.lblBPRef1Val.text)) {
        var noRefMsg = kony.i18n.getLocalizedString("MIB_BPNoRef");
        noRefMsg = noRefMsg.replace("{ref_label}", frmIBBillPaymentLP.lblBPRef1.text + "");
        alert(noRefMsg);
        frmIBBillPaymentLP.lblBPRef1Val.setFocus(true);
        return;
    }
    if (gblreccuringDisablePay == "Y" && !isNotBlank(frmIBBillPaymentLP.lblRefVal2.text)) {
        var noRefMsg = kony.i18n.getLocalizedString("MIB_BPNoRef");
        noRefMsg = noRefMsg.replace("{ref_label}", frmIBBillPaymentLP.lblRef2.text + "");
        alert(noRefMsg);
        frmIBBillPaymentLP.lblRefVal2.setFocus(true);
        return;
    }
    if (gblFullPayment) {
        //lblForFullPayment, tbxAmount - txtBillAmt
        var fullPayAmt = parseFloat(removeCommos(frmIBBillPaymentLP.lblFullPayment.text));
        if (fullPayAmt == "0" || fullPayAmt == "0.0" || frmIBBillPaymentLP.lblFullPayment.text == "0.00") {
            //dismissLoadingScreenPopup();
            alert(kony.i18n.getLocalizedString("MIB_BPNoAmt"));
            amountSelected = false;
            return;
        } else {
            var regex = /^(?:\d*\.\d{1,2}|\d+)$/
            if (regex.test(parseFloat(removeCommos(frmIBBillPaymentLP.lblFullPayment.text)))) {
                amountSelected = true;
                billAmountBillPay = parseFloat(removeCommos(frmIBBillPaymentLP.lblFullPayment.text));
            } else {
                dismissLoadingScreenPopup();
                alert(kony.i18n.getLocalizedString("MIB_BPNoAmt"));
                amountSelected = false;
                return;
            }
        }
    } else {
        if (frmIBBillPaymentLP.txtBillAmt.text == "") {
            //dismissLoadingScreenPopup()
            alert(kony.i18n.getLocalizedString("MIB_BPNoAmt"));
            amountSelected = false;
            frmIBBillPaymentLP.txtBillAmt.setFocus(true);
            return;
        }
        var amount = parseFloat(removeCommos(frmIBBillPaymentLP.txtBillAmt.text)).toFixed(2);
        if (amount == "0" || amount == "0.0" || amount == "0.00") {
            //dismissLoadingScreenPopup()
            alert(kony.i18n.getLocalizedString("MIB_BPNoAmt"));
            amountSelected = false;
            frmIBBillPaymentLP.txtBillAmt.setFocus(true);
            return;
        } else {
            var regex = /^(?:\d*\.\d{1,2}|\d+)$/
            if (regex.test(parseFloat(removeCommos(frmIBBillPaymentLP.txtBillAmt.text)))) {
                amountSelected = true;
                billAmountBillPay = parseFloat(removeCommos(frmIBBillPaymentLP.txtBillAmt.text));
            } else {
                //dismissLoadingScreenPopup();
                alert(kony.i18n.getLocalizedString("MIB_BPNoAmt"));
                amountSelected = false;
                frmIBBillPaymentLP.txtBillAmt.setFocus(true);
                return
            }
        }
    }
    var amountBalance = parseFloat(removeCommos(frmIBBillPaymentLP.lblBalanceFromValue.text));
    var amount = amountBalance; //amountBalance.replace(/,/g,"");
    amount = parseFloat(amount);
    if (amount < billAmountBillPay && gblPaynow) {
        //dismissLoadingScreenPopup();
        alert(kony.i18n.getLocalizedString("MIB_BPLessAvailBal"));
        if (frmIBBillPaymentLP.hbxAmtVal.isVisible) {
            frmIBBillPaymentLP.txtBillAmt.setFocus(true);
        }
        return;
    }
    if (amountSelected && billerSelected) {
        //CRF71 Implementation for IB
        if (gblBillerMethod == 1) {
            if (gblPayFull != "Y") {
                if (isNotBlank(gblMinAmount) && isNotBlank(gblMaxAmount)) {
                    if (!(billAmountBillPay >= parseFloat(gblMinAmount) && billAmountBillPay <= parseFloat(gblMaxAmount))) {
                        var message = kony.i18n.getLocalizedString("PlzEnterValidMinMaxAmount");
                        message = message.replace("[min]", commaFormatted(parseFloat(gblMinAmount).toFixed(2)));
                        message = message.replace("[max]", commaFormatted(parseFloat(gblMaxAmount).toFixed(2)));
                        alert(message);
                        if (frmIBBillPaymentLP.hbxAmtVal.isVisible) {
                            frmIBBillPaymentLP.txtBillAmt.setFocus(true);
                        }
                        return;
                    }
                }
            }
        }
        //start of biller validation, validating ref1/ref2.amount/date 
        var compCodeBillPay = gblCompCode;
        var ref1ValBillPay = frmIBBillPaymentLP.lblBPRef1Val.text;
        var ref2ValBillPay; //= frmIBBillPaymentLP.lblRefVal2.text;
        if (gblreccuringDisablePay == "Y") { //ref 2 not required
            ref2ValBillPay = frmIBBillPaymentLP.lblRefVal2.text;
            if (!validateRef2ValueAll(ref2ValBillPay)) {
                var wrongRefMsg = kony.i18n.getLocalizedString("keyWrngRef2Val");
                wrongRefMsg = wrongRefMsg.replace("{ref_label}", removeColonFromEnd(frmIBBillPaymentLP.lblRef2.text + ""));
                alert(wrongRefMsg);
                frmIBBillPaymentLP.lblRefVal2.setFocus(true);
                return;
            }
        } else {
            ref2ValBillPay = "";
        }
        var newNickName = frmIBBillPaymentLP.txtScheduleNickname.text;
        if (!gblPaynow && !gblBillerPresentInMyBills) {
            if (isNotBlank(newNickName)) {
                if (NickNameValid(newNickName)) {
                    if (checkBillerNicknameInMyBills(gblMyBillList, newNickName)) {
                        //nickname exists alert
                        showAlert(kony.i18n.getLocalizedString("Valid_DuplicateNickname"), kony.i18n.getLocalizedString("info"));
                        frmIBBillPaymentLP.txtScheduleNickname.setFocus(true);
                        return false;
                    }
                } else {
                    showAlert(kony.i18n.getLocalizedString("MIB_BPkeyInvalidNickName"), kony.i18n.getLocalizedString("info"));
                    frmIBBillPaymentLP.txtScheduleNickname.setFocus(true);
                    return false;
                }
            } else {
                showAlert(kony.i18n.getLocalizedString("MIB_BPErr_NoAccNickname"), kony.i18n.getLocalizedString("info"));
                frmIBBillPaymentLP.txtScheduleNickname.setFocus(true);
                return false;
            }
        }
        //MIB-4884-Allow special characters for My Note and Note to recipient field
        /*
        	if(isNotBlank(frmIBBillPaymentLP.txtAreaBPMyNote.text)){
        		if(!MyNoteValid(frmIBBillPaymentLP.txtAreaBPMyNote.text)){
        			alert(kony.i18n.getLocalizedString("MIB_TRkeyMynoteInvalid"));
        			frmIBBillPaymentLP.txtAreaBPMyNote.setFocus(true);
        			return false;
        		}
        	}
        */
        if (isNotBlank(frmIBBillPaymentLP.txtAreaBPMyNote.text)) {
            if (checkSpecialCharMyNote(frmIBBillPaymentLP.txtAreaBPMyNote.text)) {
                alert(kony.i18n.getLocalizedString("MIB_MyNoteInvalidSpecialChar"));
                frmIBBillPaymentLP.txtAreaBPMyNote.setFocus(true);
                return false;
            }
        }
        if (isNotBlank(frmIBBillPaymentLP.txtAreaBPMyNote.text) && frmIBBillPaymentLP.txtAreaBPMyNote.text.length > 50) {
            alert(kony.i18n.getLocalizedString("MaxlngthNoteError"));
            frmIBBillPaymentLP.txtAreaBPMyNote.setFocus(true);
            return;
        }
        //var billAmountBillPay = frmIBBillPaymentLP.txtBillAmt.text;
        var scheduleDtBillPay = frmIBBillPaymentLP.lblBPBillPayDate.text;
        var billerType = "Biller";
        callBillerValidationBillPayService(compCodeBillPay, ref1ValBillPay, ref2ValBillPay, billAmountBillPay, scheduleDtBillPay, billerType);
    }
}

function gotoBillPaymentConfirmationIB() {
    //billPayConfirmCheckChangeSkin();
    if (gblPaynow) {
        //make schedule fields invisible
        frmIBBillPaymentConfirm.lblPaymentDate.text = kony.i18n.getLocalizedString('keyIBPaymentDate');
        frmIBBillPaymentConfirm.hbxScheduleDetails.setVisibility(false);
    } else {
        frmIBBillPaymentConfirm.hbxBalanceAfter.setVisibility(false);
        frmIBBillPaymentConfirm.hbxScheduleDetails.setVisibility(true);
        frmIBBillPaymentConfirm.lblPaymentDate.text = kony.i18n.getLocalizedString('keyBillPaymentPaymentOrderDate');
        var dates = frmIBBillPaymentLP.lblDatesFuture.text.split(kony.i18n.getLocalizedString("keyTo"));
        var firstDate = dates[0].trim();
        var secondDate; //= dates[1].trim();
        if (dates[1] == null || dates[1] == "" || dates[1] == undefined) {
            secondDate = "-";
        } else {
            secondDate = dates[1].trim();
        }
        if (gblTimes == "-1") {
            gblTimes = "-";
        }
        frmIBBillPaymentConfirm.lblStartOnValue.text = firstDate;
        if (gblClicked == "Daily") {
            frmIBBillPaymentConfirm.lblRepeatValue.text = kony.i18n.getLocalizedString("keyDaily");
        } else if (gblClicked == "Weekly") {
            frmIBBillPaymentConfirm.lblRepeatValue.text = kony.i18n.getLocalizedString("keyWeekly");
        } else if (gblClicked == "Monthly") {
            frmIBBillPaymentConfirm.lblRepeatValue.text = kony.i18n.getLocalizedString("keyMonthly");
        } else if (gblClicked == "Yearly") {
            frmIBBillPaymentConfirm.lblRepeatValue.text = kony.i18n.getLocalizedString("keyYearly");
        } else {
            frmIBBillPaymentConfirm.lblRepeatValue.text = kony.i18n.getLocalizedString("keyOnce");
        }
        frmIBBillPaymentConfirm.lblEnDONValue.text = secondDate;
        if (gblClicked == "once" || gblClicked == "Once") {
            frmIBBillPaymentConfirm.lblExecuteValue.text = "1";
        } else {
            frmIBBillPaymentConfirm.lblExecuteValue.text = gblTimes;
        }
    }
    if (kony.string.equalsIgnoreCase(gblScheduleEndBP, "none") || kony.string.equalsIgnoreCase(gblScheduleEndBP, "Never")) {}
    if (gblreccuringDisablePay == "Y") {
        frmIBBillPaymentConfirm.hbxRef2.isVisible = true;
    } else {
        frmIBBillPaymentConfirm.hbxRef2.isVisible = false;
    }
    var currentdate = new Date();
    if (GblBillTopFlag) {
        if (gblPaynow && gblBillpaymentNoFee) {
            frmIBBillPaymentConfirm.hbxFreeTrans.setVisibility(true);
            frmIBBillPaymentConfirm.lblFreeTran.text = frmIBBillPaymentLP.lblFreeTran.text;
            frmIBBillPaymentConfirm.lblFreeTransValue.text = frmIBBillPaymentLP.lblFreeTransValue.text;
        } else {
            frmIBBillPaymentConfirm.hbxFreeTrans.setVisibility(false);
        }
        frmIBBillPaymentConfirm.lblAcctType.text = frmIBBillPaymentLP.lblAcctType.text;
        frmIBBillPaymentConfirm.lblBalance.text = frmIBBillPaymentLP.lblBalanceFromValue.text;
        if (gblPaynow || gblBillerPresentInMyBills || !isNotBlank(frmIBBillPaymentLP.txtScheduleNickname.text)) {
            frmIBBillPaymentConfirm.lblBPBillerName.text = frmIBBillPaymentLP.lblToCompCode.text;
        } else {
            frmIBBillPaymentConfirm.lblBPBillerName.text = frmIBBillPaymentLP.txtScheduleNickname.text;
        }
        frmIBBillPaymentConfirm.imgBPBillerImage.src = frmIBBillPaymentLP.imgBPBillerImage.src
        frmIBBillPaymentConfirm.lblBPRef1.text = frmIBBillPaymentLP.lblBPRef1.text + " ";
        // frmIBBillPaymentConfirm.lblBPRef1Val.text = frmIBBillPaymentLP.lblBPRef1Val.text;
        frmIBBillPaymentConfirm.lblBPRef2.text = frmIBBillPaymentLP.lblRef2.text + " ";
        frmIBBillPaymentConfirm.lblBPRef2Val.text = frmIBBillPaymentLP.lblRefVal2.text;
        // frmIBBillPaymentConfirm.lblCompCode.text = frmIBBillPaymentLP.lblToCompCode.text;
        //frmIBBillPaymentConfirm.lblAmtVal.text = commaFormatted(parseFloat(billAmountBillPay).toFixed(2))+ "\u0E3F";
        //frmIBBillPaymentConfirm.lblDateVal.text = currentSystemDate() + " [" + formatAMPM(currentdate) + "]";
        //var d =new Date();
        //frmIBBillPaymentConfirm.lblDateVal.text = currentSystemDate();//+ d.getHours()+":" + d.getMinutes();
        //MIB-4884-Allow special characters for My Note and Note to recipient field
        frmIBBillPaymentConfirm.lblMyNoteVal.text = replaceHtmlTagChars(frmIBBillPaymentLP.txtAreaBPMyNote.text);
        frmIBBillPaymentConfirm.imgFromAcc.src = frmIBBillPaymentLP.imgFrom.src; //["segBPBillsList"]["selectedItems"][0].img1;
        frmIBBillPaymentConfirm.lblFromAccName.text = frmIBBillPaymentLP.lblBPFromNameRcvd.text; //["segBPBillsList"]["selectedItems"][0].lblCustName;
        frmIBBillPaymentConfirm.lblFromAccountNo.text = frmIBBillPaymentLP.lblFromAccNo.text; //["segBPBillsList"]["selectedItems"][0].lblActNoval;
        frmIBBillPaymentConfirm.lblBalance.text = frmIBBillPaymentLP.lblBalanceFromValue.text; //["segBPBillsList"]["selectedItems"][0].lblBalance;
        frmIBBillPaymentConfirm.lblAcctType.text = frmIBBillPaymentLP.lblAcctType.text;
        frmIBBillPaymentConfirm.lblProductType.text = frmIBBillPaymentLP.lblProductType.text;
        gblSelectedAccBal = parseFloat(removeCommos(frmIBBillPaymentLP.lblBalanceFromValue.text)).toFixed(2);
        var locale = kony.i18n.getCurrentLocale();
        /*if(locale == "en_US"){
        	frmIBBillPaymentConfirm.lblCustomerName.text = gblCustomerName; ///acount name english	
        } else {
        	frmIBBillPaymentConfirm.lblCustomerName.text = gblCustomerNameTh;///acount name Thai 
        }*/
        if (billPaySummary) {
            frmIBBillPaymentConfirm.lblCustomerName.text = gblAccountTable["custAcctRec"][gblIndex]["accountName"];
        } else {
            frmIBBillPaymentConfirm.lblCustomerName.text = accntNme;
        }
    }
    gblBillPaymentEdit = false;
    frmIBBillPaymentConfirm.show();
    dismissLoadingScreenPopup();
}

function removeCommos(amountBalance) {
    amountBalance = amountBalance + "";
    if (amountBalance != null && amountBalance != undefined && amountBalance != "") {
        amountBalance = amountBalance.replace(/,/g, "");
        amountBalance = parseFloat(amountBalance);
    }
    return amountBalance;
}

function populateBillPaymentCompleteScreen(resulttable) {
    if (gblPaynow && gblBillpaymentNoFee) {
        frmIBBillPaymentCompletenow.hbxFreeTrans.setVisibility(true);
        frmIBBillPaymentCompletenow.lblFreeTran.text = frmIBBillPaymentConfirm.lblFreeTran.text;
        var prodCode = "";
        if (frmIBBillPaymentCW.customIBBPFromAccount.data == undefined || frmIBBillPaymentCW.customIBBPFromAccount.data.length == 0) prodCode = gblProductCode;
        else prodCode = frmIBBillPaymentCW.customIBBPFromAccount.data[gblCWSelectedItem].prodCode;
        if (parseFloat(frmIBBillPaymentConfirm.lblFreeTransValue.text).toFixed(0) != "0") {
            if ((prodCode == "225" || prodCode == "226") && (gblCompCode == "0699" || gblCompCode == "CC01" || gblCompCode == "AL02" || gblCompCode == "AL01")) frmIBBillPaymentCompletenow.lblFreeTransValue.text = frmIBBillPaymentConfirm.lblFreeTransValue.text;
            else if (resulttable["RemainingFee"] != undefined && resulttable["RemainingFee"] != null) frmIBBillPaymentCompletenow.lblFreeTransValue.text = resulttable["RemainingFee"];
        } else {
            frmIBBillPaymentCompletenow.lblFreeTransValue.text = "0";
        }
        //frmIBBillPaymentCompletenow.lblFreeTransValue.text = "";
        //if(resulttable["RemainingFee"] != undefined && resulttable["RemainingFee"] != null)
        //	frmIBBillPaymentCompletenow.lblFreeTransValue.text = resulttable["RemainingFee"];
    } else {
        frmIBBillPaymentCompletenow.hbxFreeTrans.setVisibility(false);
    }
    var balanceBefore = removeCommos(frmIBBillPaymentConfirm.lblBalance.text);
    var balanceAfter = parseFloat(balanceBefore) - parseFloat(removeCommos(frmIBBillPaymentConfirm.lblAmtVal.text)) - parseFloat(removeCommos(frmIBBillPaymentConfirm.lblFeeVal.text));
    frmIBBillPaymentCompletenow.lblName.text = frmIBBillPaymentConfirm.lblAcctType.text; //from account type
    frmIBBillPaymentCompletenow.lblCustName.text = frmIBBillPaymentConfirm.lblCustomerName.text;
    frmIBBillPaymentCompletenow.lblAccountNo.text = frmIBBillPaymentConfirm.lblFromAccountNo.text; //from account no
    frmIBBillPaymentCompletenow.lblFromName.text = frmIBBillPaymentConfirm.lblFromAccName.text; //from name //
    //frmIBBillPaymentCompletenow.lblBBPaymentValue.text = commaFormatted(balanceAfter.toFixed(2).toString())+ kony.i18n.getLocalizedString("currencyThaiBaht")//frmIBBillPaymentConfirm.lblBalance.text; //balance
    frmIBBillPaymentCompletenow.lblePayNickname.text = frmIBBillPaymentConfirm.lblBPBillerName.text; //nick
    //frmIBBillPaymentCompletenow.lblCompCode.text = frmIBBillPaymentConfirm.lblCompCode.text; //compcode
    frmIBBillPaymentCompletenow.lblRef1.text = frmIBBillPaymentConfirm.lblBPRef1.text; //ref1
    //below line is changed for CR - PCI-DSS masked Credit card no
    frmIBBillPaymentCompletenow.lblRef1Value.text = frmIBBillPaymentConfirm.lblBPRef1ValMasked.text; //frmIBBillPaymentConfirm.lblBPRef1Val.text; //ref1 value
    //frmIBBillPaymentCompletenow.label588684466285184.text=frmIBBillPaymentConfirm.//topup name Doubts
    //frmIBBillPaymentCompletenow.label588684466285186.text=frmIBBillPaymentConfirm.//card balance Doubts
    frmIBBillPaymentCompletenow.lblAmtVal.text = frmIBBillPaymentConfirm.lblAmtVal.text; //amt value
    frmIBBillPaymentCompletenow.lblFeeVal.text = frmIBBillPaymentConfirm.lblFeeVal.text; //fee amount
    //MIB-4884-Allow special characters for My Note and Note to recipient field
    frmIBBillPaymentCompletenow.lblMNVal.text = replaceHtmlTagChars(frmIBBillPaymentConfirm.lblMyNoteVal.text); //note
    frmIBBillPaymentCompletenow.lblTRNVal.text = frmIBBillPaymentConfirm.lblTRNVal.text; //ref no
    //frmIBTopUpComplete.label588684466285188.text=frmIBBillPaymentConfirm.;//ref id Doubts
    frmIBBillPaymentCompletenow.imgFrom.src = frmIBBillPaymentConfirm.imgFromAcc.src;
    frmIBBillPaymentCompletenow.imgBiller.src = frmIBBillPaymentConfirm.imgBPBillerImage.src;
    frmIBBillPaymentCompletenow.lblProductType.text = frmIBBillPaymentConfirm.lblProductType.text;
    if (!gblPaynow) {
        frmIBBillPaymentCompletenow.hbxScheduleDetails.setVisibility(true);
        frmIBBillPaymentCompletenow.hbxBalanceAfter.setVisibility(false);
        frmIBBillPaymentCompletenow.lblStartOnValue.text = frmIBBillPaymentConfirm.lblStartOnValue.text;
        if (gblClicked == "Daily") {
            frmIBBillPaymentCompletenow.lblRepeatValue.text = kony.i18n.getLocalizedString("keyDaily");
        } else if (gblClicked == "Weekly") {
            frmIBBillPaymentCompletenow.lblRepeatValue.text = kony.i18n.getLocalizedString("keyWeekly");
        } else if (gblClicked == "Monthly") {
            frmIBBillPaymentCompletenow.lblRepeatValue.text = kony.i18n.getLocalizedString("keyMonthly");
        } else if (gblClicked == "Yearly") {
            frmIBBillPaymentCompletenow.lblRepeatValue.text = kony.i18n.getLocalizedString("keyYearly");
        } else {
            frmIBBillPaymentCompletenow.lblRepeatValue.text = kony.i18n.getLocalizedString("keyOnce");
        }
        frmIBBillPaymentCompletenow.lblEnDONValue.text = frmIBBillPaymentConfirm.lblEnDONValue.text;
        frmIBBillPaymentCompletenow.lblExecuteValue.text = frmIBBillPaymentConfirm.lblExecuteValue.text;
        frmIBBillPaymentCompletenow.lblTransfer.text = kony.i18n.getLocalizedString("keyBillPaymentPaymentOrderDate");
        frmIBBillPaymentCompletenow.lblePayDate.text = gblCurrentBillPayTime;
    } else {
        frmIBBillPaymentCompletenow.hbxScheduleDetails.setVisibility(false);
        frmIBBillPaymentCompletenow.hbxBalanceAfter.setVisibility(true);
        frmIBBillPaymentCompletenow.lblTransfer.text = kony.i18n.getLocalizedString("keyIBPaymentDate");
        //frmIBBillPaymentCompletenow.lblePayDate.text = frmIBBillPaymentConfirm.lblDateVal.text 
    }
    if (gblreccuringDisablePay == "Y") {
        frmIBBillPaymentCompletenow.hbxref2.isVisible = true;
        frmIBBillPaymentCompletenow.lblref2.text = frmIBBillPaymentConfirm.lblBPRef2.text;
        frmIBBillPaymentCompletenow.lblRef2Value.text = frmIBBillPaymentConfirm.lblBPRef2Val.text;
    } else {
        frmIBBillPaymentCompletenow.hbxref2.isVisible = false;
    }
    frmIBBillPaymentCompletenow.hbxAmountDetailsMEA.setVisibility(false);
    if (gblCompCode == "2533") {
        frmIBBillPaymentCompletenow.hbxHideBtn.setVisibility(true);
        frmIBBillPaymentCompletenow.lnkHide.text = kony.i18n.getLocalizedString("show");
        frmIBBillPaymentCompletenow.label589607987325263.text = frmIBBillPaymentConfirm.lblAmt.text;
        frmIBBillPaymentCompletenow.lblAmtLabel.text = frmIBBillPaymentConfirm.lblAmtLabel.text;
        frmIBBillPaymentCompletenow.lblAmtValue.text = frmIBBillPaymentConfirm.lblAmtValue.text;
        frmIBBillPaymentCompletenow.lblAmtInterest.text = frmIBBillPaymentConfirm.lblAmtInterest.text;
        frmIBBillPaymentCompletenow.lblAmtInterestValue.text = frmIBBillPaymentConfirm.lblAmtInterestValue.text;
        frmIBBillPaymentCompletenow.lblAmtDisconnected.text = frmIBBillPaymentConfirm.lblAmtDisconnected.text;
        frmIBBillPaymentCompletenow.lblAmtDisconnectedValue.text = frmIBBillPaymentConfirm.lblAmtDisconnectedValue.text;
        frmIBBillPaymentCompletenow.hbxMEACustDetails.setVisibility(true);
        showMEACustDetails(frmIBBillPaymentCompletenow);
    } else {
        frmIBBillPaymentCompletenow.hbxHideBtn.setVisibility(false);
        frmIBBillPaymentCompletenow.hbxMEACustDetails.setVisibility(false);
    }
    //frmIBBillPaymentCompletenow.lblCompCode.text = frmIBBillPaymentConfirm.lblCompCode.text;
    frmIBBillPaymentCompletenow.show();
}

function IBBillsCurrentSystemDate() {
    var today = new Date(GLOBAL_TODAY_DATE);
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    today = dd + '/' + mm + '/' + yyyy;
    gblPaynow = true;
    frmIBBillPaymentLP.lblBPBillPayDate.text = today;
    // gblClicked = "once";
}

function ehFrmIBBillPaymentLP_CalendarXferDate_onSelection(eventobject, isValidDateSelected) {
    date1 = frmIBBillPaymentLP.CalendarXferDate.formattedDate;
    var today = currentSystemDate();
    if ((parseDate(date1) - parseDate(today)) == 0) {
        clearBillPaySchedRepeatOptions();
        clearBillPaySchedEndingOptions();
        return;
    }
}

function futureBillPayCalDateIB() {
    var currentDate = new Date(new Date().getTime());
    var day = currentDate.getDate();
    var month = currentDate.getMonth() + 1;
    var year = currentDate.getFullYear();
    frmIBBillPaymentLP.CalendarXferDate.validStartDate = [day, month, year];
    frmIBBillPaymentLP.calendarXferUntil.validStartDate = [day, month, year];
}

function populateSelectedScheduleBillPay() {
    var scheduleOrNowDate = "";
    if (gblPaynow) {
        scheduleOrNowDate = frmIBBillPaymentLP.lblBPBillPayDate.text.trim();
        scheduleOrNowDate = scheduleOrNowDate.split("/", 3);
        var day = scheduleOrNowDate[0];
        var month = scheduleOrNowDate[1];
        var year = scheduleOrNowDate[2];
        frmIBBillPaymentLP.CalendarXferDate.dateComponents = [day, month, year];
        clearBillPaySchedRepeatOptions();
        clearBillPaySchedEndingOptions();
    } else {
        scheduleOrNowDate = frmIBBillPaymentLP.lblDatesFuture.text.trim();
        var scheduleRepeatOption = frmIBBillPaymentLP.lblrepeats.text.trim();
        scheduleOrNowDate = scheduleOrNowDate.split(" ");
        scheduleRepeatOption = scheduleRepeatOption.split(" ");
        if (gblClicked == "Daily") {
            IBOnClickDailyFutureBillS();
            populateEndingSchedBillPayOptions(scheduleOrNowDate, scheduleRepeatOption);
        } else if (gblClicked == "Weekly") {
            IBOnClickWeeklyFutureBills();
            populateEndingSchedBillPayOptions(scheduleOrNowDate, scheduleRepeatOption);
        } else if (gblClicked == "Monthly") {
            IBOnClickMonthlyFutureBills();
            populateEndingSchedBillPayOptions(scheduleOrNowDate, scheduleRepeatOption);
        } else if (gblClicked == "Yearly") {
            IBOnClickYearlyFutureBills();
            populateEndingSchedBillPayOptions(scheduleOrNowDate, scheduleRepeatOption);
        } else {
            //Once
            scheduleOrNowDate = scheduleOrNowDate[0].split("/", 3);
            var day = scheduleOrNowDate[0];
            var month = scheduleOrNowDate[1];
            var year = scheduleOrNowDate[2];
            frmIBBillPaymentLP.CalendarXferDate.dateComponents = [day, month, year];
            clearBillPaySchedRepeatOptions();
            clearBillPaySchedEndingOptions();
        }
    }
}

function populateEndingSchedBillPayOptions(scheduleOrNowDate, scheduleRepeatOption) {
    if (gblrepeat == 1) {
        IBOnClickAfterFutureBills();
        frmIBBillPaymentLP.txtEndTimes.text = scheduleRepeatOption[2];
    } else if (gblrepeat == 2) {
        IBOnClickOnDateFutureBills();
        scheduleOrNowDate = scheduleOrNowDate[2].split("/", 3);
        var day = scheduleOrNowDate[0];
        var month = scheduleOrNowDate[1];
        var year = scheduleOrNowDate[2];
        frmIBBillPaymentLP.calendarXferUntil.dateComponents = [day, month, year];
    } else {
        IBOnClickNeverFutureBills();
    }
}

function clearBillPaySchedEndingOptions() {
    frmIBBillPaymentLP.lblXferEnding.setVisibility(false);
    frmIBBillPaymentLP.lblEnd.setVisibility(false);
    frmIBBillPaymentLP.hbxrec.setVisibility(false);
    frmIBBillPaymentLP.btnNever.skin = "btnIBTab3LeftFocus";
    frmIBBillPaymentLP.btnAfter.skin = "btnIBTab3MidNrml";
    frmIBBillPaymentLP.hbxTimes.setVisibility(false);
    frmIBBillPaymentLP.btnOnDate.skin = "btnIBTab3RightNrml";
    frmIBBillPaymentLP.hbxEndCalendar.setVisibility(false);
    frmIBBillPaymentLP.lineAfterBill.setVisibility(false);
    frmIBBillPaymentLP.txtEndTimes.text = "";
    frmIBBillPaymentLP.calendarXferUntil.clear();
}

function clearBillPaySchedRepeatOptions() {
    gblClicked = "once";
    timesRepeat = "-1"
    frmIBBillPaymentLP.btnDaily.skin = "btnIBTab4LeftNrml";
    frmIBBillPaymentLP.btnWeekly.skin = "btnIBTab4MidNrml";
    frmIBBillPaymentLP.btnMonthly.skin = "btnIBTab4MidNrml";
    frmIBBillPaymentLP.btnYearly.skin = "btnIbTab4RightNrml";
}

function shouldNotAllowRecurringForTodayDateBillPay() {
    var selectedDate = frmIBBillPaymentLP.CalendarXferDate.formattedDate;
    var today = currentSystemDate();
    if (!isNotBlank(selectedDate)) {
        alert(kony.i18n.getLocalizedString("KeySelStartDate"));
        return true;
    } else if ((parseDate(selectedDate) - parseDate(today)) == 0) {
        alert(kony.i18n.getLocalizedString("Schedule_TodayErr"));
        return true;
    } else {
        return false;
    }
}

function IBOnClickDailyFutureBillS() {
    if (shouldNotAllowRecurringForTodayDateBillPay()) {
        return;
    }
    frmIBBillPaymentLP.btnWeekly.skin = "btnIBTab4MidNrml";
    frmIBBillPaymentLP.btnMonthly.skin = "btnIBTab4MidNrml";
    frmIBBillPaymentLP.btnYearly.skin = "btnIbTab4RightNrml";
    if (gblreccuringDisableAdd == "N" && gblreccuringDisablePay == "Y") {
        frmIBBillPaymentLP.btnDaily.skin = "btnIBTab4LeftNrml";
        alert(kony.i18n.getLocalizedString("Valid_RecurringDisabled"));
        gblClicked = "once";
        timesRepeat = "-1"
        return;
    } else {
        if (frmIBBillPaymentLP.btnDaily.skin == "btnIBTab4LeftNrml") {
            gblClicked = "Daily";
            timesRepeat = "0";
            frmIBBillPaymentLP.btnDaily.skin = "btnIBTab4LeftFocus";
            frmIBBillPaymentLP.lblXferEnding.setVisibility(true);
            frmIBBillPaymentLP.hbxrec.setVisibility(true);
        } else {
            gblClicked = "once";
            timesRepeat = "-1"
            frmIBBillPaymentLP.btnDaily.skin = "btnIBTab4LeftNrml";
            clearBillPaySchedEndingOptions();
        }
    }
    //gblrepeat = 3;
}

function IBOnClickWeeklyFutureBills() {
    if (shouldNotAllowRecurringForTodayDateBillPay()) {
        return;
    }
    frmIBBillPaymentLP.btnDaily.skin = "btnIBTab4LeftNrml";
    frmIBBillPaymentLP.btnMonthly.skin = "btnIBTab4MidNrml";
    frmIBBillPaymentLP.btnYearly.skin = "btnIbTab4RightNrml";
    if (gblreccuringDisableAdd == "N" && gblreccuringDisablePay == "Y") {
        frmIBBillPaymentLP.btnWeekly.skin = "btnIBTab4MidNrml";
        alert(kony.i18n.getLocalizedString("Valid_RecurringDisabled"));
        gblClicked = "once";
        timesRepeat = "-1"
        return;
    } else {
        if (frmIBBillPaymentLP.btnWeekly.skin == "btnIBTab4MidNrml") {
            gblClicked = "Weekly";
            timesRepeat = "0";
            frmIBBillPaymentLP.btnWeekly.skin = "btnIBTab4MidFocus";
            frmIBBillPaymentLP.lblXferEnding.setVisibility(true);
            frmIBBillPaymentLP.hbxrec.setVisibility(true);
        } else {
            gblClicked = "once";
            timesRepeat = "-1"
            frmIBBillPaymentLP.btnWeekly.skin = "btnIBTab4MidNrml";
            clearBillPaySchedEndingOptions();
        }
    }
    // gblrepeat = 3;
}

function IBOnClickMonthlyFutureBills() {
    if (shouldNotAllowRecurringForTodayDateBillPay()) {
        return;
    }
    frmIBBillPaymentLP.btnDaily.skin = "btnIBTab4LeftNrml";
    frmIBBillPaymentLP.btnWeekly.skin = "btnIBTab4MidNrml";
    frmIBBillPaymentLP.btnYearly.skin = "btnIbTab4RightNrml";
    if (gblreccuringDisableAdd == "N" && gblreccuringDisablePay == "Y") {
        frmIBBillPaymentLP.btnMonthly.skin = "btnIBTab4MidNrml";
        alert(kony.i18n.getLocalizedString("Valid_RecurringDisabled"));
        gblClicked = "once";
        timesRepeat = "-1"
        return;
    } else {
        if (frmIBBillPaymentLP.btnMonthly.skin == "btnIBTab4MidNrml") {
            gblClicked = "Monthly";
            timesRepeat = "0";
            frmIBBillPaymentLP.btnMonthly.skin = "btnIBTab4MidFocus";
            frmIBBillPaymentLP.lblXferEnding.setVisibility(true);
            frmIBBillPaymentLP.hbxrec.setVisibility(true);
        } else {
            gblClicked = "once";
            timesRepeat = "-1"
            frmIBBillPaymentLP.btnMonthly.skin = "btnIBTab4MidNrml";
            clearBillPaySchedEndingOptions();
        }
    }
    // gblrepeat = 3;
}

function IBOnClickYearlyFutureBills() {
    if (shouldNotAllowRecurringForTodayDateBillPay()) {
        return;
    }
    frmIBBillPaymentLP.btnDaily.skin = "btnIBTab4LeftNrml";
    frmIBBillPaymentLP.btnWeekly.skin = "btnIBTab4MidNrml";
    frmIBBillPaymentLP.btnMonthly.skin = "btnIBTab4MidNrml";
    if (gblreccuringDisableAdd == "N" && gblreccuringDisablePay == "Y") {
        frmIBBillPaymentLP.btnYearly.skin = "btnIbTab4RightNrml";
        alert(kony.i18n.getLocalizedString("Valid_RecurringDisabled"));
        gblClicked = "once";
        timesRepeat = "-1"
        return;
    } else {
        if (frmIBBillPaymentLP.btnYearly.skin == "btnIbTab4RightNrml") {
            gblClicked = "Yearly";
            timesRepeat = "0";
            frmIBBillPaymentLP.btnYearly.skin = "btnIBTab4RightFocus";
            frmIBBillPaymentLP.lblXferEnding.setVisibility(true);
            frmIBBillPaymentLP.hbxrec.setVisibility(true);
        } else {
            gblClicked = "once";
            timesRepeat = "-1"
            frmIBBillPaymentLP.btnYearly.skin = "btnIbTab4RightNrml";
            clearBillPaySchedEndingOptions();
        }
    }
    //gblrepeat = 3;
}

function IBOnClickNeverFutureBills() {
    frmIBBillPaymentLP.hbxEndCalendar.isVisible = false;
    // frmIBBillPaymentLP.txtEndTimes.isVisible = false;
    // frmIBBillPaymentLP.lblIclude.text = "";
    frmIBBillPaymentLP.btnNever.skin = "btnIBTab3LeftFocus";
    frmIBBillPaymentLP.btnNever.focusSkin = "btnIBTab3LeftFocus";
    frmIBBillPaymentLP.btnAfter.skin = "btnIBTab3MidNrml";
    frmIBBillPaymentLP.btnOnDate.skin = "btnIBTab3RightNrml";
    frmIBBillPaymentLP.hbxTimes.setVisibility(false);
    frmIBBillPaymentLP.lblEnd.setVisibility(false);
    frmIBBillPaymentLP.lineAfterBill.setVisibility(false);
    frmIBBillPaymentLP.lblIclude.setVisibility(false);
    timesRepeat = "0";
    //gblrepeat = 3;
}

function IBOnClickAfterFutureBills() {
    frmIBBillPaymentLP.hbxEndCalendar.isVisible = false;
    // frmIBBillPaymentLP.txtEndTimes.isVisible = true;
    frmIBBillPaymentLP.lblIclude.text = kony.i18n.getLocalizedString("keyIncludeThisTime"); //added i18 keys
    frmIBBillPaymentLP.btnNever.skin = "btnIBTab3LeftNrml";
    frmIBBillPaymentLP.btnAfter.focusSkin = "btnIBTab3MidFocus";
    frmIBBillPaymentLP.btnAfter.skin = "btnIBTab3MidFocus";
    frmIBBillPaymentLP.btnOnDate.skin = "btnIBTab3RightNrml";
    frmIBBillPaymentLP.lblEnd.setVisibility(true);
    frmIBBillPaymentLP.lineAfterBill.setVisibility(true);
    frmIBBillPaymentLP.lblEnd.text = kony.i18n.getLocalizedString("keyEndAfter");
    frmIBBillPaymentLP.hbxTimes.setVisibility(true);
    frmIBBillPaymentLP.lblIclude.setVisibility(false);
    timesRepeat = "1";
    //gblrepeat = 1;
}

function IBOnClickOnDateFutureBills() {
    frmIBBillPaymentLP.hbxEndCalendar.isVisible = true;
    //  frmIBBillPaymentLP.txtEndTimes.isVisible = false;
    // frmIBBillPaymentLP.lblIclude.text = "";
    frmIBBillPaymentLP.btnNever.skin = "btnIBTab3LeftNrml";
    frmIBBillPaymentLP.btnAfter.skin = "btnIBTab3MidNrml";
    frmIBBillPaymentLP.btnOnDate.skin = "btnIBTab3RightFocus";
    frmIBBillPaymentLP.btnOnDate.focusSkin = "btnIBTab3RightFocus";
    frmIBBillPaymentLP.hbxTimes.setVisibility(false);
    frmIBBillPaymentLP.lblEnd.setVisibility(true);
    frmIBBillPaymentLP.lblEnd.text = kony.i18n.getLocalizedString("keyEndDate");
    frmIBBillPaymentLP.lineAfterBill.setVisibility(true);
    frmIBBillPaymentLP.lblIclude.setVisibility(false);
    timesRepeat = "2";
    //gblrepeat = 2;
}

function IBOnClickSaveFutureBills() {
    //var timesOrDate = "0";
    if (!gblBillPaymentEdit) gblTimes = -1;
    var firstDate = frmIBBillPaymentLP.CalendarXferDate.formattedDate;
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    today = dd + '/' + mm + '/' + yyyy;
    //alert(parseDate(firstDate)-parseDate(today));
    if (!gblBillPaymentEdit) {
        times = "-1";
    }
    if (firstDate == "" || firstDate == null) {
        alert(kony.i18n.getLocalizedString("Valid_SelectvalidFutureDate"));
        return;
    } else if ((parseDate(firstDate) - parseDate(today)) <= 0) {
        gblPaynow = true;
        frmIBBillPaymentLP.hbxImage.setVisibility(true);
        frmIBBillPaymentLP.hbxFutureBillPayment.setVisibility(false);
        frmIBBillPaymentLP.lblDatesFuture.setVisibility(false);
        frmIBBillPaymentLP.lblBPBillPayDate.setVisibility(true);
        frmIBBillPaymentLP.lblrepeats.setVisibility(false);
        frmIBBillPaymentLP.lblBPBillPayDate.text = firstDate;
        frmIBBillPaymentLP.lblDatesFuture.text = "";
        frmIBBillPaymentLP.lblrepeats.text = "";
        showSchedBillerNicknameIB();
        if (frmIBBillPaymentLP.hbxScheduleNickName.isVisible) {
            frmIBBillPaymentLP.txtScheduleNickname.setFocus(true);
        }
        return;
    } else {
        frmIBBillPaymentLP.lblBPBillPayDate.text = firstDate;
        var secondDate = "";
        if (frmIBBillPaymentLP.hbxEndCalendar.isVisible) {
            //timesOrDate = "1";
            gblrepeat = 2;
            frmIBBillPaymentLP.lblDatesFuture.setVisibility(true);
            frmIBBillPaymentLP.lblBPBillPayDate.setVisibility(false);
            frmIBBillPaymentLP.lblrepeats.setVisibility(true);
            secondDate = frmIBBillPaymentLP.calendarXferUntil.formattedDate;
            if (secondDate == "" || secondDate == null) {
                alert(kony.i18n.getLocalizedString("Valid_SelectEndDate"));
                return;
            }
            if ((parseDate(secondDate) - parseDate(firstDate)) <= 0) {
                alert(kony.i18n.getLocalizedString("keySelectValidEndDateFT"));
                return;
            }
            var EditTime = times;
            times = numberOfDaysIB(firstDate, secondDate);
            times = Number(times);
            if (times > 99) {
                alert(kony.i18n.getLocalizedString("Valid_ErrorRecLessthan99"));
                times = EditTime;
                return;
            }
            if (isNaN(times) || times < 1) {
                alert(kony.i18n.getLocalizedString("Valid_EntervalidRecValue"));
                times = EditTime;
                return;
            }
        } else if (frmIBBillPaymentLP.hbxTimes.isVisible) {
            //timesOrDate = "0";
            gblrepeat = 1;
            frmIBBillPaymentLP.lblDatesFuture.setVisibility(true);
            frmIBBillPaymentLP.lblBPBillPayDate.setVisibility(false);
            frmIBBillPaymentLP.lblrepeats.setVisibility(true);
            var EditTimes = times;
            times = frmIBBillPaymentLP.txtEndTimes.text.trim();
            //alert(times);
            times = Number(times);
            if (isNaN(times) || times < 1) {
                times = EditTimes;
                alert(kony.i18n.getLocalizedString("Valid_EntervalidRecValue"));
                return;
            }
            if (times > 99) {
                times = EditTimes;
                alert(kony.i18n.getLocalizedString("Valid_ErrorRecLessthan99"));
                return;
            }
            var startDate = parseDate(firstDate)
            secondDate = endDateCalculatorBillIB(startDate, times)
        } else if (!frmIBBillPaymentLP.hbxrec.isVisible) {
            gblrepeat = 0;
            frmIBBillPaymentLP.lblDatesFuture.setVisibility(true);
            frmIBBillPaymentLP.lblBPBillPayDate.setVisibility(false);
            frmIBBillPaymentLP.lblrepeats.setVisibility(true);
            secondDate = firstDate;
        } else {
            gblrepeat = 3;
            frmIBBillPaymentLP.lblBPBillPayDate.setVisibility(false);
            frmIBBillPaymentLP.lblDatesFuture.setVisibility(true);
            frmIBBillPaymentLP.lblrepeats.setVisibility(true);
            gblPaynow = false;
            frmIBBillPaymentLP.lblDatesFuture.text = firstDate;
            if (gblClicked == "Daily") {
                frmIBBillPaymentLP.lblrepeats.text = kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyDaily");
            } else if (gblClicked == "Weekly") {
                frmIBBillPaymentLP.lblrepeats.text = kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyWeekly");
            } else if (gblClicked == "Monthly") {
                frmIBBillPaymentLP.lblrepeats.text = kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyMonthly");
            } else if (gblClicked == "Yearly") {
                frmIBBillPaymentLP.lblrepeats.text = kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyYearly");
            } else {
                secondDate = firstDate;
                frmIBBillPaymentLP.lblrepeats.text = kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyOnce");
            }
            frmIBBillPaymentLP.hbxImage.setVisibility(true);
            frmIBBillPaymentLP.hbxFutureBillPayment.setVisibility(false);
            showSchedBillerNicknameIB();
            if (frmIBBillPaymentLP.hbxScheduleNickName.isVisible) {
                frmIBBillPaymentLP.txtScheduleNickname.setFocus(true);
            }
            return;
        }
        labelTransfertop = firstDate + " " + kony.i18n.getLocalizedString("keyTo") + " " + secondDate;
        //    labelTransferBottomWeek = kony.i18n.getLocalizedString("keyRepeatWeekly") + " " + times + " " + kony.i18n.getLocalizedString("keyTimesMB");
        //    labelTransferBottomMonth = kony.i18n.getLocalizedString("keyRepeatMonthly") + " " + times + " " + kony.i18n.getLocalizedString("keyTimesMB");
        //     labelTransferBottomYear = kony.i18n.getLocalizedString("keyRepeatYearly") + " " + times + " " + kony.i18n.getLocalizedString("keyTimesMB");
        //     labelTransferBottomDaily = kony.i18n.getLocalizedString("keyRepeatDaily") + " " + times + " " + kony.i18n.getLocalizedString("keyTimesMB");
        frmIBBillPaymentLP.lblDatesFuture.text = labelTransfertop;
        gblPaynow = false;
        if (gblClicked == "Daily") {
            frmIBBillPaymentLP.lblrepeats.text = kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyDaily") + " " + times + " " + kony.i18n.getLocalizedString("keyTimesMB");
        } else if (gblClicked == "Weekly") {
            frmIBBillPaymentLP.lblrepeats.text = kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyWeekly") + " " + times + " " + kony.i18n.getLocalizedString("keyTimesMB");
        } else if (gblClicked == "Monthly") {
            frmIBBillPaymentLP.lblrepeats.text = kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyMonthly") + " " + times + " " + kony.i18n.getLocalizedString("keyTimesMB");
        } else if (gblClicked == "Yearly") {
            frmIBBillPaymentLP.lblrepeats.text = kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyYearly") + " " + times + " " + kony.i18n.getLocalizedString("keyTimesMB");
        } else {
            frmIBBillPaymentLP.lblrepeats.text = kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyOnce");
        }
        showSchedBillerNicknameIB();
        if (frmIBBillPaymentLP.hbxScheduleNickName.isVisible) {
            frmIBBillPaymentLP.txtScheduleNickname.setFocus(true);
        }
    }
    gblTimes = times;
    frmIBBillPaymentLP.hbxImage.setVisibility(true);
    frmIBBillPaymentLP.hbxFutureBillPayment.setVisibility(false);
}

function checkBillPaymentCrmProfileInqIB() {
    var inputParam = {}
        //inputParam["transferFlag"] = "true"
    showLoadingScreenPopup();
    invokeServiceSecureAsync("crmProfileInq", inputParam, callBackBillPayCrmProfileInqIB)
}

function callBackBillPayCrmProfileInqIB(status, resulttable) {
    showLoadingScreenPopup();
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            /// check channel limit and available balance
            var StatusCode = resulttable["statusCode"];
            var Severity = resulttable["Severity"];
            var StatusDesc = resulttable["StatusDesc"];
            if (StatusCode == 0) {
                gblUserLockStatusIB = resulttable["ibUserStatusIdTr"]
                if (resulttable["ibUserStatusIdTr"] == gblFinancialTxnIBLock) {
                    dismissLoadingScreenPopup();
                    alert("" + kony.i18n.getLocalizedString("ECVrfyOTPErr"));
                    return;
                }
                //setCrmProfileInqData(resulttable);  //caching profile information for profile Update; 
                //ebAccuUsgAmtDaily: resulttable["ebAccuUsgAmtDaily"]
                var temp1 = [];
                var temp = {
                    ebAccuUsgAmtDaily: resulttable["ebAccuUsgAmtDaily"]
                }
                kony.table.insert(temp1, temp);
                gblCRMProfileData = temp1;
                ebAccuUsgAmtDaily = resulttable["ebAccuUsgAmtDaily"];
                // alert(ebAccuUsgAmtDaily);
                DailyLimit = parseFloat(resulttable["ebMaxLimitAmtCurrent"].toString());
                UsageLimit = parseFloat(resulttable["ebAccuUsgAmtDaily"].toString());
                gblEmailIdBillerNotification = resulttable["emailAddr"]
                    //     DailyLimit = 7000000;
                    //     UsageLimit = 500000;
                    // gblTransferRefNo=resulttable["acctIdentValue"]
                    // 
                    //alert(gblEmailIdBillerNotification);
                if (gblFullPayment) {
                    var billAmount = frmIBBillPaymentLP.lblFullPayment.text;
                } else {
                    var billAmount = frmIBBillPaymentLP.txtBillAmt.text;
                }
                var billAmountFee = frmIBBillPaymentConfirm.lblFeeVal.text
                var totalBillPayAmt = parseFloat(removeCommos(billAmount)); // + parseFloat(removeCommos(billAmountFee));
                gblchannelLimit = DailyLimit - UsageLimit;
                gblchannelLimit = fixedToTwoDecimal(gblchannelLimit);
                var BPdailylimitExceedMsg = kony.i18n.getLocalizedString("MIB_BPExcDaily");
                BPdailylimitExceedMsg = BPdailylimitExceedMsg.replace("{remaining_limit}", commaFormatted(gblchannelLimit + ""));
                if (gblPaynow) {
                    if (gblchannelLimit > 0) {
                        if (gblchannelLimit < totalBillPayAmt) {
                            dismissLoadingScreenPopup();
                            showAlert(BPdailylimitExceedMsg, kony.i18n.getLocalizedString("info"));
                            //alert(kony.i18n.getLocalizedString("keyexceedsdailylimit"));
                            // alert(" "+kony.i18n.getLocalizedString("keyUserchannellimitexceedsfortheday"));
                            return false;
                        } else {
                            callGenerateTransferRefNoserviceIBBillPay();
                            return true;
                        }
                    } else {
                        dismissLoadingScreenPopup();
                        showAlert(BPdailylimitExceedMsg, kony.i18n.getLocalizedString("info"));
                        //alert(kony.i18n.getLocalizedString("keyexceedsdailylimit"));
                    }
                } else {
                    if (DailyLimit > 0) {
                        if (DailyLimit < totalBillPayAmt) {
                            dismissLoadingScreenPopup();
                            showAlert(BPdailylimitExceedMsg, kony.i18n.getLocalizedString("info"));
                            //alert(kony.i18n.getLocalizedString("keyexceedsdailylimit"));
                            return false;
                        } else {
                            callGenerateTransferRefNoserviceIBBillPay();
                            return true;
                        }
                    } else {
                        // alert("user channel limit exceeds for the day!!!");
                        dismissLoadingScreenPopup();
                        showAlert(BPdailylimitExceedMsg, kony.i18n.getLocalizedString("info"));
                        //alert(kony.i18n.getLocalizedString("keyexceedsdailylimit"));
                    }
                }
            } else {
                dismissLoadingScreenPopup();
                alert(" " + StatusDesc)
                return false;
            }
        } else {
            dismissLoadingScreenPopup();
            alert(" " + resulttable["errMsg"]);
            /**dismissLoadingScreen();**/
        }
    }
}

function callGenerateTransferRefNoserviceIBBillPay() {
    inputParam = {};
    if (gblPaynow) {
        inputParam["transRefType"] = "NB";
    } else {
        inputParam["transRefType"] = "SB";
    }
    invokeServiceSecureAsync("generateTransferRefNo", inputParam, generateTransferRefNoserviceIBBillPayCallBack);
}

function generateTransferRefNoserviceIBBillPayCallBack(status, result) {
    if (status == 400) //success responce
    {
        if (result["opstatus"] == 0) {
            refNum = result["transRefNum"];
            frmIBBillPaymentConfirm.lblTRNVal.text = refNum + "00";
            // gotoBillPaymentConfirmationIB();
            saveToSessionBillPayment();
            //frmBillPaymentConfirmationFuture.lblTxnNumValue.text = refNum;   
        } else {
            dismissLoadingScreenPopup();
            alert(" " + result["errMsg"]);
        }
    }
}

function endDateCalculatorBillIB(staringDateFromCalendar, repeatTimesTextBoxValue) {
    var endingDate = staringDateFromCalendar;
    var numberOfDaysToAdd = 0;
    if (gblClicked == "Daily") {
        numberOfDaysToAdd = repeatTimesTextBoxValue * 1;
        endingDate.setDate(endingDate.getDate() + numberOfDaysToAdd - 1);
    } else if (gblClicked == "Weekly") {
        numberOfDaysToAdd = ((repeatTimesTextBoxValue - 1) * 7);
        endingDate.setDate(endingDate.getDate() + numberOfDaysToAdd);
    } else if (gblClicked == "Monthly") {
        endingDate.setDate(endingDate.getDate());
        var dd = endingDate.getDate();
        var mm = endingDate.getMonth() + 1;
        var newmm = parseFloat(mm.toString()) + parseFloat(repeatTimesTextBoxValue - 1);
        var newmmadd = newmm % 12;
        if (newmmadd == 0) {
            newmmadd = 12;
        }
        var yearAdd = Math.floor((newmm / 12));
        var y = endingDate.getFullYear();
        if (newmmadd == 12) {
            y = parseFloat(y) + parseFloat(yearAdd) - 1;
        } else {
            y = parseFloat(y) + parseFloat(yearAdd);
        }
        mm = parseFloat(mm.toString()) + newmmadd;
        if (newmmadd == 2) {
            if (dd > 28) {
                if (!leapYear(y)) {
                    dd = 28;
                } else {
                    dd = 29;
                }
            }
        }
        if (newmmadd == 4 || newmmadd == 6 || newmmadd == 9 || newmmadd == 11) {
            if (dd > 30) {
                dd = 30;
            }
        }
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (newmmadd < 10) {
            newmmadd = '0' + newmmadd;
        }
        var someFormattedDate = dd + '/' + newmmadd + '/' + y;
        return someFormattedDate;
    } else if (gblClicked == "Yearly") {
        var dd = endingDate.getDate();
        var mm = endingDate.getMonth() + 1;
        var y = endingDate.getFullYear();
        var newYear = parseFloat(y) + parseFloat(repeatTimesTextBoxValue - 1);
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        if (dd > 28 && mm == "02") {
            if (leapYear(newYear)) {
                dd = "29";
            } else {
                dd = "28";
            }
        }
        var someFormattedDate = dd + '/' + mm + '/' + newYear;
        return someFormattedDate;
    }
    var dd = endingDate.getDate();
    var mm = endingDate.getMonth() + 1;
    var y = endingDate.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    var someFormattedDate = dd + '/' + mm + '/' + y;
    return someFormattedDate;
}
var billerTypeCheck = "";

function callBillerValidationBillPayService(compCodeBillPay, ref1ValBillPay, ref2ValBillPay, billAmountBillPay, scheduleDtBillPay, billerType) {
    inputParam = {};
    inputParam["BillerCompcode"] = compCodeBillPay;
    inputParam["ReferenceNumber1"] = ref1ValBillPay;
    inputParam["ReferenceNumber2"] = ref2ValBillPay;
    inputParam["Amount"] = billAmountBillPay;
    amountpaid = billAmountBillPay;
    inputParam["ScheduleDate"] = scheduleDtBillPay;
    inputParam["billerCategoryID"] = gblBillerCategoryID;
    inputParam["billerMethod"] = gblBillerMethod;
    inputParam["ModuleName"] = "BillPay";
    if (gblBillerCompCodeEN == "2533") {
        inputParam["BillerStartTime"] = gBillerStartTime;
        inputParam["BillerEndTime"] = gBillerEndTime;
    }
    billerTypeCheck = billerType;
    showLoadingScreenPopup();
    invokeServiceSecureAsync("billerValidation", inputParam, callBillerValidationBillPayServiceCallBack);
}

function callBillerValidationBillPayServiceCallBack(status, result) {
    if (status == 400) //success responce
    {
        var validationBillPayFlag = "";
        var isValidOnlineBiller = result["isValidOnlineBiller"];
        if (result["opstatus"] == 0) {
            validationBillPayFlag = result["validationResult"];
        } else {
            dismissLoadingScreenPopup();
            validationBillPayFlag = result["validationResult"];
        }
        if (billerTypeCheck == "Biller") {
            if (validationBillPayFlag == "true") {
                // BillPaymentInquiryModuleIB();
                if (gblBillerMethod == "0" || gblBillerMethod == "1") {
                    BillPaymentInquiryModuleIB();
                } else {
                    frmIBBillPaymentConfirm.lblFeeVal.text = "0.00" + " \u0E3F";
                    checkBillPaymentCrmProfileInqIB();
                }
            } else {
                dismissLoadingScreenPopup();
                if (isValidOnlineBiller != undefined && isValidOnlineBiller == "true") {
                    alert(kony.i18n.getLocalizedString("keyInvalidOnlineBillerDetails"));
                } else {
                    //alert(kony.i18n.getLocalizedString("keyBillerValidationFailed"));
                    var wrongRefMsg = kony.i18n.getLocalizedString("keyWrngRef1Val");
                    wrongRefMsg = wrongRefMsg.replace("{ref_label}", removeColonFromEnd(frmIBBillPaymentLP.lblBPRef1.text + ""));
                    alert(wrongRefMsg);
                }
                frmIBBillPaymentLP.lblBPRef1Val.setFocus(true);
                return false;
            }
        }
        if (billerTypeCheck == "TopUp") {
            if (validationBillPayFlag == "true") {
                if (gblBillerMethod == "0" || gblBillerMethod == "1") {
                    getFeeForTopUpIB();
                } else if (gblBillerMethod == "2") {
                    //doing credit card verify here
                    validateFleetCardTopUpIB();
                } else {
                    frmIBTopUpConfirmation.lblFeeVal.text = "0.00" + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                    checkTopUpCrmProfileInqIB();
                    // populateTopUpConfirmation(resultable["BillPmtInqRs"]);
                }
            } else {
                dismissLoadingScreenPopup();
                if (isValidOnlineBiller != undefined && isValidOnlineBiller == "true") {
                    alert(kony.i18n.getLocalizedString("keyInvalidOnlineBillerDetails"));
                } else {
                    var errmsg = kony.i18n.getLocalizedString("MIB_BPWrongRef"); // new key as per Re-design Top Up document.
                    errmsg = errmsg.replace("{ref_label}", removeColonFromEnd(frmIBTopUpLandingPage.ref1.text));
                    alert(errmsg);
                }
                return false;
            }
        }
    }
}

function addSuggestedBillPaymentIB() {
    //if (gblAddToMyBill) {
    //        frmIBMyBillersHome.hbxBillersAddContainer.setVisibility(true);
    //        frmIBMyBillersHome.hbxBillersConfirmContainer.setVisibility(false);
    //        frmIBMyBillersHome.hbxBillersCompleteContainer.setVisibility(false);
    //        frmIBMyBillersHome.hbxBillersEditContainer.setVisibility(false);
    //        frmIBMyBillersHome.hbxBillersViewContainer.setVisibility(false);
    //        frmIBMyBillersHome.imgTMBLogo.setVisibility(false);
    //        frmIBMyBillersHome.lblAddBillerRef1.text = kony.i18n.getLocalizedString("keyRef1");
    //        frmIBMyBillersHome.segBillersList.setVisibility(false);
    //        frmIBMyBillersHome.segSuggestedBillersList.removeAll();
    //        frmIBMyBillersHome.hbxSugBillersContainer.setVisibility(true);
    //        isSelectBillerEnabled = true;
    //        //alert("done");
    frmIBMyBillersHome.segBillersList.setVisibility(false);
    frmIBMyBillersHome.lblMyBills.setVisibility(false);
    frmIBMyBillersHome.hbxMore.setVisibility(false);
    frmIBMyBillersHome.segSuggestedBillersList.removeAll();
    frmIBMyBillersHome.hbxSugBillersContainer.setVisibility(true);
    isSelectBillerEnabled = true;
    gblTopUpMore = 0;
    gblCustTopUpMore = 0;
    //}
}

function editBillPaymentIB() {
    gblBillPaymentEdit = true;
    frmIBBillPaymentLP.lblFromAccNo.text = frmIBBillPaymentConfirm.lblFromAccountNo.text;
    frmIBBillPaymentLP.lblAcctType.text = frmIBBillPaymentConfirm.lblAcctType.text;
    frmIBBillPaymentLP.lblBalanceFromValue.text = frmIBBillPaymentConfirm.lblBalance.text;
    frmIBBillPaymentLP.lblBPBillerName.text = frmIBBillPaymentConfirm.lblBPBillerName.text;
    frmIBBillPaymentLP.imgBPBillerImage.src = frmIBBillPaymentConfirm.imgBPBillerImage.src;
    var bpRef1 = frmIBBillPaymentConfirm.lblBPRef1.text;
    if (isNotBlank(bpRef1)) {
        frmIBBillPaymentLP.lblBPRef1.text = bpRef1.trim();
    }
    frmIBBillPaymentLP.lblBPRef1Val.text = frmIBBillPaymentConfirm.lblBPRef1Val.text;
    //below line is added for CR - PCI-DSS masked Credit card no
    frmIBBillPaymentLP.lblBPRef1ValMasked.text = frmIBBillPaymentConfirm.lblBPRef1ValMasked.text;
    var bpRef2 = frmIBBillPaymentConfirm.lblBPRef2.text;
    if (isNotBlank(bpRef2)) {
        frmIBBillPaymentLP.lblRef2.text = bpRef2.trim();
    }
    frmIBBillPaymentLP.lblRefVal2.text = frmIBBillPaymentConfirm.lblBPRef2Val.text;
    // frmIBBillPaymentLP.lblToCompCode.text = frmIBBillPaymentConfirm.lblCompCode.text;
    frmIBBillPaymentLP.hbxToBiller.setVisibility(false);
    frmIBBillPaymentLP.hbxImage.setVisibility(true);
    if (gblreccuringDisablePay == "Y") {
        frmIBBillPaymentLP.hbxReference2.setVisibility(true);
        frmIBBillPaymentLP.lblRefVal2.setEnabled(true);
    } else {
        frmIBBillPaymentLP.hbxReference2.setVisibility(false);
    }
    if (gblBillerMethod == 0) { //Offline Biller
        gblPenalty = false;
        gblFullPayment = false;
        frmIBBillPaymentLP.lblBPAmount.text = kony.i18n.getLocalizedString("keyAmount");
        //Penalty Hide
        frmIBBillPaymentLP.hbxPenalty.setVisibility(false);
        //Hide All
        frmIBBillPaymentLP.btnAmtFull.setVisibility(false);
        frmIBBillPaymentLP.btnAmtFull.setEnabled(false);
        frmIBBillPaymentLP.btnAmtMinimum.setVisibility(false);
        frmIBBillPaymentLP.btnAmtMinimum.setEnabled(false);
        frmIBBillPaymentLP.btnAmtSpecified.setEnabled(false);
        frmIBBillPaymentLP.btnAmtSpecified.setVisibility(false);
        frmIBBillPaymentLP.hbxMinMax.setVisibility(false);
        frmIBBillPaymentLP.txtBillAmt.text = removeCurrencyThaiBath(frmIBBillPaymentConfirm.lblAmtVal.text); //set value from service
        frmIBBillPaymentLP.txtBillAmt.setEnabled(true);
        frmIBBillPaymentLP.txtBillAmt.setVisibility(true)
        frmIBBillPaymentLP.lblTHB.setVisibility(true);
        frmIBBillPaymentLP.hbxAmtVal.setVisibility(true);
        frmIBBillPaymentLP.lblFullPayment.setVisibility(false);
        if (gblBillerBancassurance == "Y" && (gblFromAccountSummary || gblFromAccountSummaryBillerAdded)) {
            frmIBBillPaymentLP.txtBillAmt.setEnabled(false);
            frmIBBillPaymentLP.lblRefVal2.setEnabled(false);
        }
    } else if (gblBillerMethod == 1) { //Online Biller
        // mapOnlineBiller();
    } else if (gblBillerMethod == 2) { // TMB Credit Card/Ready Cash
        // mapTMBCreditCard();
    } else if (gblBillerMethod == 3) { //TMB Loan
        // mapTMBLoan();
    } else if (gblBillerMethod == 4) { //Ready Cash		
        // mapReadyCash();
    }
    if (gblPaynow) {
        frmIBBillPaymentLP.lblBPBillPayDate.text = frmIBBillPaymentConfirm.lblDateVal.text;
        frmIBBillPaymentLP.lblBPBillPayDate.setVisibility(true);
        frmIBBillPaymentLP.lblDatesFuture.setVisibility(false);
        frmIBBillPaymentLP.lblrepeats.setVisibility(false);
    } else {
        if (frmIBBillPaymentConfirm.lblEnDONValue.text == "-") {
            frmIBBillPaymentLP.lblDatesFuture.text = frmIBBillPaymentConfirm.lblStartOnValue.text;
            frmIBBillPaymentLP.lblrepeats.text = kony.i18n.getLocalizedString("keyRepeat") + " " + frmIBBillPaymentConfirm.lblRepeatValue.text;
        } else {
            frmIBBillPaymentLP.lblDatesFuture.text = frmIBBillPaymentConfirm.lblStartOnValue.text + " " + kony.i18n.getLocalizedString("keyTo") + " " + frmIBBillPaymentConfirm.lblEnDONValue.text;
            if (frmIBBillPaymentConfirm.lblRepeatValue.text == "once" || frmIBBillPaymentConfirm.lblRepeatValue.text == "Once") {
                frmIBBillPaymentLP.lblrepeats.text = kony.i18n.getLocalizedString("keyRepeat") + " " + frmIBBillPaymentConfirm.lblRepeatValue.text;
            } else {
                frmIBBillPaymentLP.lblrepeats.text = kony.i18n.getLocalizedString("keyRepeat") + " " + frmIBBillPaymentConfirm.lblRepeatValue.text + " " + frmIBBillPaymentConfirm.lblExecuteValue.text + " " + kony.i18n.getLocalizedString("keyTimesMB");
            }
        }
        frmIBBillPaymentLP.lblBPBillPayDate.setVisibility(false);
        frmIBBillPaymentLP.lblDatesFuture.setVisibility(true);
        frmIBBillPaymentLP.lblrepeats.setVisibility(true);
    }
    if (gblCompCode == "2533") {
        frmIBBillPaymentLP.lblAmtLabel.text = frmIBBillPaymentConfirm.lblAmtLabel.text;
        frmIBBillPaymentLP.lblAmtInterest.text = frmIBBillPaymentConfirm.lblAmtInterest.text;
        frmIBBillPaymentLP.lblAmtDisconnected.text = frmIBBillPaymentConfirm.lblAmtDisconnected.text;
        frmIBBillPaymentLP.lblAmtValue.text = frmIBBillPaymentConfirm.lblAmtValue.text;
        frmIBBillPaymentLP.lblAmtInterestValue.text = frmIBBillPaymentConfirm.lblAmtInterestValue.text;
        frmIBBillPaymentLP.lblAmtDisconnectedValue.text = frmIBBillPaymentConfirm.lblAmtDisconnectedValue.text;
        frmIBBillPaymentLP.hbxHideBtn.setVisibility(true);
    } else {
        frmIBBillPaymentLP.hbxHideBtn.setVisibility(false);
    }
    frmIBBillPaymentLP.show();
}

function onclickSchedule() {
    frmIBBillPaymentLP.lblXferEnding.setVisibility(false);
    frmIBBillPaymentLP.hbxrec.setVisibility(false);
    frmIBBillPaymentLP.lblEnd.setVisibility(false);
    frmIBBillPaymentLP.hbxEndCalendar.setVisibility(false);
    frmIBBillPaymentLP.hbxTimes.setVisibility(false);
    frmIBBillPaymentLP.lblIclude.setVisibility(false);
    frmIBBillPaymentLP.btnDaily.skin = "btnIBTab4LeftNrml";
    frmIBBillPaymentLP.btnMonthly.skin = "btnIBTab4MidNrml";
    frmIBBillPaymentLP.btnWeekly.skin = "btnIBTab4MidNrml";
    frmIBBillPaymentLP.btnYearly.skin = "btnIbTab4RightNrml";
    frmIBBillPaymentLP.btnNever.skin = "btnIBTab3LeftFocus";
    frmIBBillPaymentLP.btnAfter.skin = "btnIBTab3MidNrml";
    frmIBBillPaymentLP.btnOnDate.skin = "btnIBTab3RightNrml";
    frmIBBillPaymentLP.txtEndTimes.text = "";
    frmIBBillPaymentLP.lineAfterBill.setVisibility(false);
    gblrepeat = 0;
}

function leapYear(year) {
    return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
}

function saveToSessionBillPayment() {
    inputParam = {};
    inputParam["toAccountNo"] = gblToAccountKey;
    inputParam["billerMethod"] = gblBillerMethod;
    inputParam["fromAccountNo"] = removeHyphenIB(frmIBBillPaymentLP.lblFromAccNo.text);
    inputParam["ref1"] = frmIBBillPaymentLP.lblBPRef1Val.text;
    inputParam["amount"] = parseFloat(removeCommos(billAmountBillPay.toString())).toFixed(2);
    inputParam["compCode"] = gblCompCode;
    var billerName = "";
    if ("en_US" == kony.i18n.getCurrentLocale()) {
        billerName = gblBillerCompCodeEN;
    } else {
        billerName = gblBillerCompCodeTH;
    }
    billerName = billerName.substring(0, billerName.lastIndexOf("("));
    inputParam["flex1"] = billerName.trim();
    inputParam["flex2"] = parseFloat(removeCommos(frmIBBillPaymentConfirm.lblFeeVal.text)).toFixed(2);
    if (gblreccuringDisablePay == "Y") {
        inputParam["ref2"] = frmIBBillPaymentLP.lblRefVal2.text;
    } else {
        inputParam["ref2"] = "";
    }
    if (gblPaynow) {
        inputParam["typeID"] = "027";
    } else {
        inputParam["typeID"] = "028";
    }
    inputParam["billerId"] = gblBillerId;
    invokeServiceSecureAsync("billPaymentValidationService", inputParam, saveToSessionBillPaymentcallBack);
}

function saveToSessionBillPaymentcallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            frmIBBillPaymentConfirm.lblBPRef1Val.text = resulttable["ref1"];
            //below line is added for CR - PCI-DSS masked Credit card no
            frmIBBillPaymentConfirm.lblBPRef1ValMasked.text = maskCreditCard(resulttable["ref1"]);
            frmIBBillPaymentConfirm.lblAmtVal.text = commaFormatted(parseFloat(resulttable["amount"]).toFixed(2)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
            frmIBBillPaymentConfirm.lblDateVal.text = resulttable["paymentServerDate"];
            gotoBillPaymentConfirmationIB();
        }
    }
}

function saveToSessionTopUpIB() {
    inputParam = {};
    inputParam["toAccountNo"] = gblToAccountKey;
    inputParam["billerMethod"] = gblBillerMethod;
    inputParam["fromAccountNo"] = removeHyphenIB(frmIBTopUpLandingPage.lblAccountNo.text);
    inputParam["ref1"] = frmIBTopUpLandingPage.txtAddBillerRef1.text;
    inputParam["amount"] = parseFloat(removeCommos(amountpaid.toString())).toFixed(2);
    inputParam["compCode"] = gblCompCode;
    var flex1 = frmIBTopUpLandingPage.lblCompCode.text;
    var flex1 = "";
    if ("en_US" == kony.i18n.getCurrentLocale()) {
        flex1 = gblBillerCompCodeEN;
    } else {
        flex1 = gblBillerCompCodeTH;
    }
    flex1 = flex1.substring(0, flex1.lastIndexOf("("));
    inputParam["flex1"] = flex1.trim();
    inputParam["flex2"] = parseFloat(removeCommos(frmIBTopUpConfirmation.lblFeeVal.text)).toFixed(2);
    if (gblPaynow) {
        inputParam["typeID"] = "030"
    } else {
        inputParam["typeID"] = "031"
    }
    inputParam["billerId"] = gblBillerId;
    invokeServiceSecureAsync("billPaymentValidationService", inputParam, saveToSessionTopupIBCallBack);
}

function saveToSessionTopupIBCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            frmIBTopUpConfirmation.lblRef1Value.text = resulttable["ref1"];
            //below line is added for CR - PCI-DSS masked Credit card no
            frmIBTopUpConfirmation.lblRef1ValueMasked.text = maskCreditCard(resulttable["ref1"]);
            frmIBTopUpConfirmation.lblAmtVal.text = commaFormatted(parseFloat(resulttable["amount"]).toFixed(2)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
            frmIBTopUpConfirmation.lblTransferVal.text = resulttable["paymentServerDate"];
            populateTopUpConfirmation();
        }
    }
}

function populateFromAddTopUpIB() {
    gblEasyPassCustomer = false;
    gblEditTopUp = true;
    var tmpAddBillData = "";
    if (frmIBMyTopUpsHome.segBillersConfirm.data != null) tmpAddBillData = frmIBMyTopUpsHome.segBillersConfirm.data;
    frmIBTopUpLandingPage.imgTopUp.setVisibility(true);
    frmIBTopUpLandingPage.imgTopUp.setVisibility(true);
    frmIBTopUpLandingPage.vboxTo.setVisibility(true);
    frmIBTopUpLandingPage.hbxRef1TopUpScreen.setVisibility(true);
    gblCompCode = gblBillerCompCode;
    if (gblCompCode == "2151") {
        gblEasyPassCustomer = true;
    } else {
        gblEasyPassCustomer = false;
    }
    frmIBTopUpLandingPage.imgTopUp.src = tmpAddBillData[0]["imgConfirmBillerLogo"].src;
    frmIBTopUpLandingPage.lblNickTopUp.text = tmpAddBillData[0]["lblConfirmBillerName"];
    frmIBTopUpLandingPage.lblCompCode.text = tmpAddBillData[0]["lblConfirmBillerNameCompCode"];
    frmIBTopUpLandingPage.ref1.text = tmpAddBillData[0]["lblConfirmBillerRef1"];
    frmIBTopUpLandingPage.ref1value.text = tmpAddBillData[0]["lblConfirmBillerRef1Value"];
    frmIBTopUpLandingPage.txtAddBillerRef1.text = tmpAddBillData[0]["lblConfirmBillerRef1Value"];
    //below line is added for CR - PCI-DSS masked Credit card no
    frmIBTopUpLandingPage.ref1valueMasked.text = maskCreditCard(tmpAddBillData[0]["lblConfirmBillerRef1Value"]);
    billerMethod = gblBillerMethod;
    var groupType = gblbillerGroupType;
    if (billerMethod == 0) { //Offline Biller
        mapOfflineTopUp();
        dismissLoadingScreenPopup();
        hideShowScheduleNicknameTopUpIB(false);
        frmIBTopUpLandingPage.show();
    } else if (billerMethod == 1) { //Online Biller
        mapOnlineTopUp();
        dismissLoadingScreenPopup();
    } else if (billerMethod == 2) { // TMB Credit Card/Ready Cash
        mapTMBCreditCardTopUp();
        dismissLoadingScreenPopup();
    } else if (billerMethod == 3) { //TMB Loan
        mapTMBLoanTopUp();
    } else if (billerMethod == 4) { //Ready Cash
        mapReadyCashTopUp();
    }
    gblEditTopUp = false;
    frmIBMyTopUpsHome.segBillersConfirm.removeAll();
}

function populateFromAddBillerIB() {
    showLoadingScreenPopup();
    var tmpAddBillData = "";
    if (frmIBMyBillersHome.segBillersConfirm.data != null) tmpAddBillData = frmIBMyBillersHome.segBillersConfirm.data;
    gblBillPaymentEdit = true;
    gblFirstTimeBillPayment = false;
    gblFullPayment = false;
    frmIBBillPaymentLP.lblFullPayment.setVisibility(false)
    frmIBBillPaymentLP.lblBPAmount.setVisibility(true);
    frmIBBillPaymentLP.txtBillAmt.setVisibility(true);
    frmIBBillPaymentLP.lblTHB.setVisibility(true);
    frmIBBillPaymentLP.imgBPBillerImage.setVisibility(true);
    billerMethod = gblBillerMethod;
    var billerGroupType = gblbillerGroupType;
    //var billerGroupType = 0;
    if (!frmIBBillPaymentLP.hbxRef1.isVisible) {
        frmIBBillPaymentLP.hbxRef1.setVisibility(true);
    }
    if (gblreccuringDisablePay == "Y") {
        frmIBBillPaymentLP.hbxReference2.setVisibility(true);
        frmIBBillPaymentLP.lblRefVal2.setEnabled(true);
    } else {
        frmIBBillPaymentLP.hbxReference2.setVisibility(false);
    }
    gblCompCode = tmpAddBillData[0]["BillerCompCode"];
    frmIBBillPaymentLP.lblBPBillerName.text = tmpAddBillData[0]["lblConfirmBillerName"];
    frmIBBillPaymentLP.imgBPBillerImage.src = tmpAddBillData[0]["imgConfirmBillerLogo"].src;
    frmIBBillPaymentLP.lblBPRef1ValMasked.text = maskCreditCard(tmpAddBillData[0]["lblConfirmBillerRef1Value"]);
    frmIBBillPaymentLP.lblBPRef1Val.text = tmpAddBillData[0]["lblConfirmBillerRef1Value"];
    frmIBBillPaymentLP.lblBPRef1.text = tmpAddBillData[0]["lblConfirmBillerRef1"];
    frmIBBillPaymentLP.lblRefVal2.text = tmpAddBillData[0]["lblConfirmBillerRef2Value"];
    frmIBBillPaymentLP.lblRefVal2.maxTextLength = gblRef2LenIB;
    if (kony.i18n.getCurrentLocale() == "en_US") {
        frmIBBillPaymentLP.lblRef2.text = gblCurRef2LblEN;
    } else {
        frmIBBillPaymentLP.lblRef2.text = gblCurRef2LblTH;
    }
    frmIBBillPaymentLP.lblToCompCode.text = tmpAddBillData[0]["lblConfirmBillerNameCompCode"];
    //frmIBBillPaymentLP.lblRefVal2.setEnabled(true);
    if (billerMethod == 0) { //Offline Biller
        mapOfflineBiller();
    } else if (billerMethod == 1) { //Online Biller
        mapOnlineBiller();
    } else if (billerMethod == 2) { // TMB Credit Card/Ready Cash
        mapTMBCreditCard();
    } else if (billerMethod == 3) { //TMB Loan
        mapTMBLoan();
    } else if (billerMethod == 4) { //Ready Cash		
        mapReadyCash();
    }
    frmIBMyBillersHome.segBillersConfirm.removeAll();
}

function gotoBillPaymentFromBillerComplete() {
    showLoadingScreenPopup();
    var tmpAddBillData = "";
    if (frmIBMyBillersHome.segAddBillerComplete.data != null) tmpAddBillData = frmIBMyBillersHome.segAddBillerComplete.data;
    if (gblBillerMethod == 0) { //Offline Biller
        mapOfflineBillerCW();
        callBillPaymentCustomerAccountServiceIB()
    } else if (gblBillerMethod == 1) { //Online Biller
        mapOnlineBillerCW();
    } else if (gblBillerMethod == 2) { // TMB Credit Card/Ready Cash
        mapCreditCardCW(frmIBBillPaymentCW.lblBPRef1Val.text);
    } else if (gblBillerMethod == 3) { //TMB Loan
        populateLoanonBillPayCw(frmIBBillPaymentCW.lblBPRef1Val.text, frmIBBillPaymentCW.txtBPRef2Val.text);
    } else if (gblBillerMethod == 4) { //Ready Cash		
        mapReadyCashCW();
    }
}

function mapOfflineBillerCW() {
    gblPenalty = false;
    gblFullPayment = false;
    frmIBBillPaymentCW.lblBPAmount.text = kony.i18n.getLocalizedString("keyAmount");
    //Hide All
    frmIBBillPaymentCW.btnAmtFull.setVisibility(false);
    frmIBBillPaymentCW.btnAmtFull.setEnabled(false);
    frmIBBillPaymentCW.btnAmtMinimum.setVisibility(false);
    frmIBBillPaymentCW.btnAmtMinimum.setEnabled(false);
    frmIBBillPaymentCW.btnAmtSpecified.setEnabled(false);
    frmIBBillPaymentCW.btnAmtSpecified.setVisibility(false);
    frmIBBillPaymentCW.hbxMinMax.setVisibility(false);
    frmIBBillPaymentCW.txtBillAmt.text = ""; //set value from service
    frmIBBillPaymentCW.txtBillAmt.setEnabled(true);
    frmIBBillPaymentCW.txtBillAmt.setVisibility(true)
    frmIBBillPaymentCW.hbxAmtVal.setVisibility(true);
    frmIBBillPaymentCW.lblFullPayment.setVisibility(false);
    //dismissLoadingScreenPopup();
}

function mapOnlineBillerCW() {
    var inputParam = {};
    var ref2Input = "";
    if (typeof(frmIBBillPaymentCW.txtBPRef2Val.text) != "undefined") {
        ref2Input = frmIBBillPaymentCW.txtBPRef2Val.text;
    }
    inputParam["TrnId"] = "";
    inputParam["BankId"] = "011";
    inputParam["BranchId"] = "0001";
    inputParam["BankRefId"] = "";
    inputParam["Amt"] = "0.00";
    //inputParam["OnlinePmtType"] = "";
    inputParam["Ref1"] = frmIBBillPaymentCW.lblBPRef1Val.text;
    inputParam["Ref2"] = ref2Input; //frmIBBillPaymentLP.lblRefVal2.text;
    inputParam["Ref3"] = "";
    inputParam["Ref4"] = "";
    inputParam["MobileNumber"] = frmIBBillPaymentCW.lblBPRef1Val.text;
    inputParam["compCode"] = gblCompCode; //frmIBBillPaymentLP["segBPBillsList"]["selectedItems"][0]["BillerCompCode"];
    inputParam["isChannel"] = "IB";
    inputParam["commandType"] = "Inquiry";
    inputParam["BillerGroupType"] = "0";
    invokeServiceSecureAsync("onlinePaymentInq", inputParam, callBackOnlinePmtInqBPCW);
    gblPenalty = true;
    //Only Full and Specified
    frmIBBillPaymentCW.btnAmtFull.setEnabled(true);
    frmIBBillPaymentCW.btnAmtFull.setVisibility(true);
    frmIBBillPaymentCW.btnAmtMinimum.setEnabled(false);
    frmIBBillPaymentCW.btnAmtMinimum.setVisibility(false);
    frmIBBillPaymentCW.btnAmtSpecified.setEnabled(true);
    frmIBBillPaymentCW.btnAmtSpecified.setVisibility(true);
    frmIBBillPaymentCW.hbxMinMax.setVisibility(true);
    //Store Full value in Global
    frmIBBillPaymentCW.txtBillAmt.text = ""; //set value
}

function callBackOnlinePmtInqBPCW(status, result) {
    penalty = "0.00";
    fullAmt = "0.00";
    minAmt = "0.00";
    gblRef1 = "";
    gblRef2 = "";
    gblRef3 = "";
    gblRef4 = "";
    BankRefId = "";
    TranId = "";
    gblMinAmount = "";
    gblMaxAmount = "";
    if (status == 400) //success responce
    {
        if (result["opstatus"] == 0) {
            if (result["StatusCode"] == 0) {
                for (var i = 0; i < result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"].length; i++) {
                    if (result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscName"] == "PenaltyAmt") penalty = result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscText"];
                    if (result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscName"] == "FullAmt") fullAmt = result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscText"];
                    if (result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscName"] == "BilledAmt") minAmt = result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscText"];
                    //CR71 Implementation	
                    if (kony.string.equalsIgnoreCase(result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscName"], "Minimum")) {
                        gblMinAmount = result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscText"];
                    }
                    if (kony.string.equalsIgnoreCase(result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscName"], "Maximum")) {
                        gblMaxAmount = result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscText"];
                    }
                }
                BankRefId = result["OnlinePmtInqRs"][0]["BankRefId"];
                TranId = result["OnlinePmtInqRs"][0]["TrnId"];
                gblRef1 = result["OnlinePmtInqRs"][0]["Ref1"];
                gblRef2 = result["OnlinePmtInqRs"][0]["Ref2"];
                gblRef3 = result["OnlinePmtInqRs"][0]["Ref3"];
                gblRef4 = result["OnlinePmtInqRs"][0]["Ref4"];
                frmIBBillPaymentCW.lblFullPayment.setVisibility(true);
                frmIBBillPaymentCW.txtBillAmt.setVisibility(false);
                gblFullPayment = true;
                if (penalty == "0.00" || penalty == undefined) {
                    gblPenalty = false;
                } else {
                    gblPenalty = true;
                }
                // need clarification
                if (gblPenalty) {
                    frmIBBillPaymentLP.lblPenaltyVal.text = penalty + kony.i18n.getLocalizedString("currencyThaiBaht");
                    frmIBBillPaymentLP.lblAmountValue.text = fullAmt + kony.i18n.getLocalizedString("currencyThaiBaht");
                    frmIBBillPaymentLP.lblFullPayment.text = (parseInt(fullAmt) + parseInt(penalty)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                    frmIBBillPaymentLP.hbxPenalty.setVisibility(true);
                    frmIBBillPaymentLP.txtBillAmt.text = "";
                } else {
                    frmIBBillPaymentLP.lblFullPayment.text = numberWithCommas(removeCommos(fullAmt)) + kony.i18n.getLocalizedString("currencyThaiBaht");
                    frmIBBillPaymentLP.hbxPenalty.setVisibility(false);
                    frmIBBillPaymentLP.txtBillAmt.text = "";
                    frmIBBillPaymentCW.lblFullPayment.text = numberWithCommas(removeCommos(fullAmt)) + kony.i18n.getLocalizedString("currencyThaiBaht");
                    frmIBBillPaymentCW.txtBillAmt.text = "";
                }
                if (gblPayFull == "Y") {
                    gblFullPayment = true;
                    frmIBBillPaymentCW.lblFullPayment.setVisibility(true);
                    frmIBBillPaymentCW.hbxAmtVal.setVisibility(false);
                    frmIBBillPaymentCW.btnAmtFull.setVisibility(false);
                    frmIBBillPaymentCW.btnAmtMinimum.setVisibility(false);
                    frmIBBillPaymentCW.btnAmtSpecified.setVisibility(false);
                    frmIBBillPaymentCW.hbxMinMax.setVisibility(false);
                }
                callBillPaymentCustomerAccountServiceIB()
            } else {
                dismissLoadingScreenPopup();
                alert(" " + result["errMsg"]);
            }
        } else {
            alert(" " + result["errMsg"]);
            dismissLoadingScreenPopup();
        }
    }
}

function mapReadyCashCW() {
    var TMB_BANK_FIXED_CODE = "0001";
    var TMB_BANK_CODE_ADD = "0011";
    var ZERO_PAD = "0000";
    var BRANCH_CODE;
    var ref1AccountIDDeposit = frmIBBillPaymentCW.lblBPRef1Val.text;
    var fiident;
    var inputParam = {};
    inputparam["acctId"] = ref1AccountIDDeposit;
    ownRC = false;
    var accountDetails = gblAccountTable;
    var accountLength = gblAccountTable["custAcctRec"].length;
    for (var i = 0; i < accountLength; i++) {
        if (gblAccountTable.accType == "LOC") {
            var accountNo = gblAccountTable["custAcctRec"][i].accId;
            accountNo = accountNo.substring(accountNo.length - 16, accountNo.length);
            if (accountNo == ref1AccountIDDeposit) {
                ownRC = true;
                break;
            }
        }
    }
    if (!ownRC) {
        gblPenalty = false;
        frmIBBillPaymentCW.lblBPAmount.text = kony.i18n.getLocalizedString("keyAmount");
        //Penalty Hide
        frmIBBillPaymentCW.hbxPenalty.setVisibility(false);
        frmIBBillPaymentCW.btnAmtFull.setEnabled(false);
        frmIBBillPaymentCW.btnAmtFull.setVisibility(false);
        frmIBBillPaymentCW.btnAmtMinimum.setEnabled(false);
        frmIBBillPaymentCW.btnAmtMinimum.setVisibility(false);
        frmIBBillPaymentCW.btnAmtSpecified.setEnabled(false);
        frmIBBillPaymentCW.btnAmtSpecified.setVisibility(false);
        frmIBBillPaymentCW.hbxMinMax.setVisibility(false);
    }
    invokeServiceSecureAsync("depositAccountInquiry", inputParam, callBackDepositAccInqBPCW);
    gblPenalty = false;
    frmIBBillPaymentCW.lblBPAmount.text = kony.i18n.getLocalizedString("keyAmount");
    frmIBBillPaymentCW.btnAmtFull.setEnabled(true);
    frmIBBillPaymentCW.btnAmtFull.setVisibility(true);
    frmIBBillPaymentCW.btnAmtMinimum.setEnabled(false);
    frmIBBillPaymentCW.btnAmtMinimum.setVisibility(false);
    frmIBBillPaymentCW.btnAmtSpecified.setEnabled(true);
    frmIBBillPaymentCW.btnAmtSpecified.setVisibility(true);
    frmIBBillPaymentCW.hbxMinMax.setVisibility(true);
}

function callBackDepositAccInqBPCW(status, result) {
    if (status == 400) //success responce
    {
        if (result["opstatus"] == 0) {
            var BalType;
            var Amt = "0.00";
            var CurCode;
            for (var i = 0; i < result["AcctBal"].length; i++) {
                BalType = result["AcctBal"][i]["BalType"];
                if (BalType == "Principle") {
                    Amt = result["AcctBal"][i]["Amt"];
                    CurCode = result["AcctBal"][i]["CurCode"];
                }
            }
            frmIBBillPaymentCW.txtBillAmt.text = numberWithCommas(removeCommos(Amt));
            dismissLoadingScreenPopup();
        } else {
            alert(" " + result["errMsg"]);
            dismissLoadingScreenPopup();
        }
    }
}

function mapCreditCardCW(cardId) {
    //for CreditCard and Ready cash
    ownCard = false;
    var inputParam = {};
    var toDayDate = getTodaysDate();
    inputParam["cardId"] = cardId;
    inputParam["tranCode"] = TRANSCODEMIN;
    inputParam["postedDt"] = toDayDate;
    var accountDetails = gblAccountTable;
    var accountLength = gblAccountTable["custAcctRec"].length;
    for (var i = 0; i < accountLength; i++) {
        if (gblAccountTable["custAcctRec"][i].accType == "CCA") {
            var accountNo = gblAccountTable["custAcctRec"][i].accId;
            accountNo = accountNo.substring(accountNo.length - 16, accountNo.length);
            if (accountNo == cardId) {
                ownCard = true;
                break;
            }
        }
    }
    if (!ownCard) {
        frmIBBillPaymentCW.btnAmtFull.setEnabled(false);
        frmIBBillPaymentCW.btnAmtFull.setVisibility(false);
        frmIBBillPaymentCW.btnAmtMinimum.setEnabled(false);
        frmIBBillPaymentCW.btnAmtMinimum.setVisibility(false);
        frmIBBillPaymentCW.btnAmtSpecified.setEnabled(false);
        frmIBBillPaymentCW.btnAmtSpecified.setVisibility(false);
        frmIBBillPaymentCW.hbxMinMax.setVisibility(false);
        frmIBBillPaymentCW.hbxAmtVal.isVisible = true;
        frmIBBillPaymentCW.txtBillAmt.text = "";
        frmIBBillPaymentCW.txtBillAmt.setEnabled(true);
        frmIBBillPaymentCW.txtBillAmt.setVisibility(true);
        frmIBBillPaymentCW.hbxAmtVal.setVisibility(true);
        frmIBBillPaymentCW.txtBillAmt.setEnabled(true);
        frmIBBillPaymentCW.txtBillAmt.setVisibility(true);
        frmIBBillPaymentCW.lblFullPayment.setVisibility(false);
        callBillPaymentCustomerAccountServiceIB()
        return;
    }
    invokeServiceSecureAsync("creditcardDetailsInq", inputParam, callBackCreditCardDetInqBPCW);
}

function callBackCreditCardDetInqBPCW(status, result) {
    fullAmt = "0.00";
    minAmt = "0.00";
    if (status == 400) //success responce
    {
        var accountlist = gblAccountTable;
        if (result["opstatus"] == 0) {
            if (result["StatusCode"] == 0) {
                var Amt = result["fullPmtAmt"];
                fullAmt = result["fullPmtAmt"];
                if (parseInt(fullAmt) < 0) gblPaymentOverpaidFlag = true;
                minAmt = result["minPmtAmt"]
                if (fullAmt == "") fullAmt = "0.00";
                if (minAmt == "") minAmt = "0.00";
                gblPenalty = false;
                frmIBBillPaymentCW.lblBPAmount.text = kony.i18n.getLocalizedString("keyAmount");
                frmIBBillPaymentCW.btnAmtFull.setEnabled(true);
                frmIBBillPaymentCW.btnAmtFull.setVisibility(true);
                frmIBBillPaymentCW.btnAmtMinimum.setEnabled(true);
                frmIBBillPaymentCW.btnAmtMinimum.setVisibility(true);
                frmIBBillPaymentCW.btnAmtSpecified.setEnabled(true);
                frmIBBillPaymentCW.btnAmtSpecified.setVisibility(true);
                frmIBBillPaymentCW.hbxMinMax.setVisibility(true);
                frmIBBillPaymentCW.hbxAmtVal.isVisible = true;
                frmIBBillPaymentCW.txtBillAmt.text = numberWithCommas(removeCommos(fullAmt));
                frmIBBillPaymentCW.txtBillAmt.setVisibility(false);
                frmIBBillPaymentCW.lblFullPayment.setVisibility(true);
                gblFullPayment = true;
                frmIBBillPaymentCW.lblFullPayment.setVisibility(true);
                frmIBBillPaymentCW.hbxAmtVal.setVisibility(false);
                frmIBBillPaymentCW.lblFullPayment.text = numberWithCommas(removeCommos(fullAmt)) + kony.i18n.getLocalizedString("currencyThaiBaht");
                callBillPaymentCustomerAccountServiceIB()
            } else {
                dismissLoadingScreenPopup();
                alert(" " + result["errMsg"]);
            }
        } else {
            dismissLoadingScreenPopup();
            alert(" " + result["errMsg"]);
        }
    }
}

function mapLoanCW(ref1AccountIDLoan, ref2) {
    //for TMB Loan
    var TMB_BANK_FIXED_CODE = "0001";
    var TMB_BANK_CODE_ADD = "0011";
    var ZERO_PAD = "0000";
    var BRANCH_CODE;
    var fiident;
    if (ref1AccountIDLoan.length == 10) {
        fiident = TMB_BANK_CODE_ADD + TMB_BANK_FIXED_CODE + "0" + ref1AccountIDLoan[0] + ref1AccountIDLoan[1] + ref1AccountIDLoan[2] + ZERO_PAD;
        ref1AccountIDLoan = "0" + ref1AccountIDLoan + ref2;
    }
    if (ref1AccountIDLoan.length == 13) {
        fiident = TMB_BANK_CODE_ADD + TMB_BANK_FIXED_CODE + "0" + ref1AccountIDLoan[0] + ref1AccountIDLoan[1] + ref1AccountIDLoan[2] + ZERO_PAD;
        ref1AccountIDLoan = "0" + ref1AccountIDLoan
    } else {
        fiident = TMB_BANK_CODE_ADD + TMB_BANK_FIXED_CODE + "0" + ref1AccountIDLoan[1] + ref1AccountIDLoan[2] + ref1AccountIDLoan[3] + ZERO_PAD;
    }
    var inputParam = {};
    inputParam["acctId"] = ref1AccountIDLoan;
    inputParam["acctType"] = "LOC";
    ownLoan = false;
    var accountDetails = gblAccountTable;
    var accountLength = gblAccountTable["custAcctRec"].length;
    for (var i = 0; i < accountLength; i++) {
        if (gblAccountTable["custAcctRec"][i].accType == "LOC") {
            var accountNo = gblAccountTable["custAcctRec"][i].accId;
            if (accountNo == ref1AccountIDLoan) {
                ownLoan = true;
                break;
            }
        }
    }
    if (!ownLoan) {
        frmIBBillPaymentCW.btnAmtFull.setEnabled(false);
        frmIBBillPaymentCW.btnAmtFull.setVisibility(false);
        frmIBBillPaymentCW.btnAmtMinimum.setEnabled(false);
        frmIBBillPaymentCW.btnAmtMinimum.setVisibility(false);
        frmIBBillPaymentCW.btnAmtSpecified.setEnabled(false);
        frmIBBillPaymentCW.btnAmtSpecified.setVisibility(false);
        frmIBBillPaymentCW.hbxMinMax.setVisibility(false);
        frmIBBillPaymentCW.hbxAmtVal.isVisible = false;
        frmIBBillPaymentCW.txtBillAmt.text = "";
        frmIBBillPaymentCW.txtBillAmt.setEnabled(true);
        frmIBBillPaymentCW.txtBillAmt.setVisibility(true);
        frmIBBillPaymentCW.hbxAmtVal.setVisibility(true);
        callBillPaymentCustomerAccountServiceIB()
        return;
    }
    invokeServiceSecureAsync("doLoanAcctInq", inputParam, callBackLoanAccInqBPCW);
    gblPenalty = false;
    frmIBBillPaymentCW.lblBPAmount.text = kony.i18n.getLocalizedString("keyAmount");
    //min hide
    frmIBBillPaymentCW.btnAmtFull.setEnabled(true);
    frmIBBillPaymentCW.btnAmtFull.setVisibility(true);
    frmIBBillPaymentCW.btnAmtMinimum.setEnabled(false);
    frmIBBillPaymentCW.btnAmtMinimum.setVisibility(false);
    frmIBBillPaymentCW.btnAmtSpecified.setEnabled(true);
    frmIBBillPaymentCW.btnAmtSpecified.setVisibility(true);
    frmIBBillPaymentCW.hbxMinMax.setVisibility(true);
}

function callBackLoanAccInqBPCW(status, result) {
    showLoadingScreenPopup();
    fullAmt = "0.00"
    if (status == 400) //success responce
    {
        if (result["opstatus"] == 0) {
            var balAmount;
            fullAmt = result["regPmtCurAmt"];
            frmIBBillPaymentCW.lblFullPayment.text = numberWithCommas(removeCommos(fullAmt)) + kony.i18n.getLocalizedString("currencyThaiBaht");
            frmIBBillPaymentCW.lblFullPayment.setVisibility(true);
            frmIBBillPaymentCW.txtBillAmt.text = numberWithCommas(removeCommos(fullAmt));
            frmIBBillPaymentCW.txtBillAmt.setVisibility(false);
            callBillPaymentCustomerAccountServiceIB()
        } else {
            alert(" " + result["errMsg"]);
            dismissLoadingScreenPopup();
        }
    }
}
//End: ENH_020 - Allow to pay bills directly from My Bill/MyTop ups 
//Common function to handle the alert for ENH_111
function paymentOverpaidCheck() {
    if (gblPaymentOverpaidFlag) {
        //setTimeout(function(){alert(kony.i18n.getLocalizedString("keyPaymentOverpaid"))},2000);
        kony.timer.schedule("creditpay", function() {
            alert(kony.i18n.getLocalizedString("keyPaymentOverpaid"));
            kony.timer.cancel("creditpay");
        }, 2, false);
    }
    gblPaymentOverpaidFlag = false;
}