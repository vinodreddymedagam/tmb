function gblVarEditFTMB() {
    gblFreeTransactionsMB = "" //to use for getTransferFee
    gblFrmaccntProdCodeMB = "" //to use for getTransferFee
    Accnts_RelTableMB = []; //To save Accnt_Rel Data  globally
    TMBAccntDataMB = []; // To save CustmerAccntInq Data globally
    gblFrmAccntIDMB = "" // Chek the format of the Acnt Num which one used in all cases
    gblFrmAccntTypeMB = ""
    gblToAccntIDMB = ""
    gblToAccntTypeMB = ""
  //  gblToBankIdMB = ""
    gblTransfrAmtMB = "" // Saving to use for getTranferFee
    //gblBankTypeMB = ""; // Used to get TO account Name b4 navigationg to Edit Confirmation page
    gblToBankCdFTMB = ""; // Used to get TO account Name b4 navigationg to Edit Confirmation page
    gblFromBankCdFTMB = "" // Used to get TO account Name b4 navigationg to Edit Confirmation page
    gblEditFTSchduleMB = false; // Have to add @schdule
    gblFtDelMB = false; // Used to handle delete service from edit flow n delete flow
    gblTransCodeFTMB = ""; //To use for functranferInq i/p
   // gblcustPayeeIdFTMB = ""; //for doPmtAdd 
    gblpmtMethodFTMB = ""; //for doPmtAdd 
    gblOrderDateFTMB = "";
    gblAccntName_ORFT_MB = "";
    gblCRMProfileInqCalMB = false;
    gblFrmAccntfIIdentFTMB = "";
    gblToAccntfIIdentFTMB = "";
    gblFrmAccntdepositeNoMB = "";

    //gblSchduleRefIDFTMB = ""
    //Adding to save old data 	
    gblFTViewInitiateDateMB = "";
    gblFTViewStartOnDateMB = "";
    gblFTViewRepeatAsValMB = "";
    gblFTViewEndDateMB = "";
    gblFTViewExcuteValMB = "";
    gblFTViewExcuteremaingMB = "";
    gblFTViewEndValMB = ""

    gblEndValTempMB = "";
    gblSMARTTransAmntMB = "";
    gblORFTPerTransAmntLimtMB = "";

    srvPaymentInqMB();

}

//Service to check from account status , account nickname and account name

function getAccountListServiceMB() {
    var inputparam = {};
    inputparam["transferFlag"] = "true";
    invokeServiceSecureAsync("customerAccountInquiry", inputparam, getAccountListServiceCallBackMB);

}

function getAccountListServiceCallBackMB(status, resulttable) {

    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
			gblOwnPersonlisedIdFTMB = "";
			gblFrmAccntNickEN = "";
            gblFrmAccntNickTH = "";
            TMBAccntDataMB = [];
            if (resulttable.custAcctRec != null) {
          //  	gblcrmIdFTMB = resulttable.custAcctRec[0].crmId;
            	
                for (var i = 0; i < resulttable.custAcctRec.length; i++) {
                    var nickName = "";
                    var accntStatus = "";
                    var bankcd = "";
                    var accntName = "";
                    var accntNum = "";
                    var ICON_ID = "";
                    var nickNameTH = "";
                    if (resulttable.custAcctRec[i].acctNickName != undefined && resulttable.custAcctRec[i].acctNickName != null && resulttable.custAcctRec[i].acctNickName != "") {
                        nickName = resulttable.custAcctRec[i].acctNickName
                    } else {
                        var len = resulttable.custAcctRec[i].accId.length;
                        nickName = resulttable.custAcctRec[i].productNmeEN + resulttable.custAcctRec[i].accId.substring(len - 4, len);
                    }
                    if (resulttable.custAcctRec[i].acctNickNameTh != undefined){
                          nickNameTH = resulttable.custAcctRec[i].acctNickNameTh; 
                    }else
                    	nickNameTH = "";
                    	
                    
                    if (resulttable.custAcctRec[i].personalizedAcctStatus != undefined) accntStatus = resulttable.custAcctRec[i].personalizedAcctStatus;
                    else accntStatus = "01";

                    if (resulttable.custAcctRec[i].bankCD != null) bankcd = resulttable.custAcctRec[i].bankCD;

                    if (resulttable.custAcctRec[i].accountName != null && resulttable.custAcctRec[i].accountName != "") accntName = resulttable.custAcctRec[i].accountName;
                    if (resulttable.custAcctRec[i].accId != null && resulttable.custAcctRec[i].accId != "") accntNum = resulttable.custAcctRec[i].accId;

                    var availBal = resulttable.custAcctRec[i].availableBal;
                    var numOfFreeTrnsctns = resulttable.custAcctRec[i].remainingFee;
                    var prodCode = resulttable.custAcctRec[i].productID;
                    var fiident = resulttable.custAcctRec[i].fiident;


 				if (resulttable.custAcctRec[i].persionlizedId != null && resulttable.custAcctRec[i].persionlizedId != "") 
            			gblOwnPersonlisedIdFTMB = resulttable.custAcctRec[i].persionlizedId;
            	
            	if (resulttable.custAcctRec[i].ICON_ID != null && resulttable.custAcctRec[i].ICON_ID != "") 
            			ICON_ID = resulttable.custAcctRec[i].ICON_ID;		

                    TMBAccntDataMB.push({
                        "nickName": nickName,
                        "accntStatus": accntStatus,
                        "bankcd": bankcd,
                        "accntName": accntName,
                        "accntNum": accntNum,
                        "availBal": availBal,
                        "numOfFreeTrnsctns": numOfFreeTrnsctns,
                        "prodCode": prodCode,
                        "fiident": fiident,
                        "ICON_ID":ICON_ID,
                        "nickNameTH":nickNameTH
                    });
                }

                if (resulttable.OtherAccounts.length > 0) {
                    for (var i = 0; i < resulttable.OtherAccounts.length; i++) {
                        var nickName = "";
                        var accntStatus = "";
                        var bankcd = "";
                        var accntName = "";
                        var accntNum = "";
                        if (resulttable.OtherAccounts[i].acctNickName != undefined && resulttable.OtherAccounts[i].acctNickName != null && resulttable.OtherAccounts[i].acctNickName != "") {
                            nickName = resulttable.OtherAccounts[i].acctNickName
                        } else {
                            var len = resulttable.OtherAccounts[i].accId.length;
                            if (kony.i18n.getCurrentLocale() == "en_US") {
                                nickName = resulttable.OtherAccounts[i].productNmeEN + resulttable.OtherAccounts[i].accId.substring(len - 4, len);

                            } else if (kony.i18n.getCurrentLocale() == "th_TH") {
                                nickName = resulttable.OtherAccounts[i].productNmeTH + resulttable.OtherAccounts[i].accId.substring(len - 4, len);
                            }

                        }
                        if (resulttable.OtherAccounts[i].personalizedAcctStatus != undefined) accntStatus = resulttable.OtherAccounts[i].personalizedAcctStatus;
                        else accntStatus = "01";
                        if (resulttable.OtherAccounts[i].bankCD != null) bankcd = resulttable.OtherAccounts[i].bankCD;

                        if (resulttable.OtherAccounts[i].accountName != null && resulttable.OtherAccounts[i].accountName != "") accntName = resulttable.OtherAccounts[i].accountName;
                        if (resulttable.OtherAccounts[i].accId != null && resulttable.OtherAccounts[i].accId != "") accntNum = resulttable.OtherAccounts[i].accId;

						/*
						if(resulttable.OtherAccounts[i].productID == "otherbank"){
						  	if(accntNum.length >= 10){
						  		accntNum = accntNum.substring(accntNum.length-10,accntNum.length);
						  	}
						}
						*/	
                        var availBal = resulttable.OtherAccounts[i].availableBal;
                        var numOfFreeTrnsctns = resulttable.OtherAccounts[i].remainingFee;
                        var prodCode = resulttable.OtherAccounts[i].productID;
                        var fiident = resulttable.OtherAccounts[i].fiident;
						
                        TMBAccntDataMB.push({
                            "nickName": nickName,
                            "accntStatus": accntStatus,
                            "bankcd": bankcd,
                            "accntName": accntName,
                            "accntNum": accntNum,
                            "availBal": availBal,
                            "numOfFreeTrnsctns": numOfFreeTrnsctns,
                            "prodCode": prodCode,
                            "fiident": fiident

                        });

                    }
                }

                // Checking whether the "From account" is existing or not in CustmerAccntInq Response Table 
                for (var k = 0; k < TMBAccntDataMB.length; k++) {
                    var tempAccnt = gblFrmAccntIDMB;

                    if(gblFrmAccntTypeMB == "SDA" && gblFrmAccntIDMB.length == 10 )
                       tempAccnt = "0000"+tempAccnt;
                    if(gblFrmAccntTypeMB == "CDA" && gblFrmAccntIDMB.length == 10 )
                       tempAccnt = "0000"+tempAccnt;	
                    if (tempAccnt == TMBAccntDataMB[k].accntNum) {

                        gblFromBankCdFTMB = TMBAccntDataMB[k].banckcd;
                        gblFreeTransactionsMB = TMBAccntDataMB[k].numOfFreeTrnsctns; // Saving to use for getTransferFee
                        gblFrmaccntProdCodeMB = TMBAccntDataMB[k].prodCode; // Saving to use for getTransferFee
                        gblFrmAccntfIIdentFTMB = TMBAccntDataMB[k].fiident;
                        
                        gblFrmAccntNickEN = TMBAccntDataMB[k].nickName;
                        if(TMBAccntDataMB[k].nickNameTH != ""){
                        	gblFrmAccntNickTH = TMBAccntDataMB[k].nickNameTH;
                        }else
                        	gblFrmAccntNickTH = "";

                        //if( TMBAccntDataMB[k].bankcd == "11" ){
                        if (TMBAccntDataMB[k].accntStatus == "02") {
                            //Show link with add myaccount link;
                            //frmIBFTrnsrView.rchtxtError.setVisibility(true);
                            frmMBFTView.hbxFrmAccntHidenError.setVisibility(true);
                            frmMBFTView.hbxFromAccnt.setVisibility(false);
                            frmMBFTView.rchtxtErrorFrmAccnt.setVisibility(true);
                            //frmMBFTView.rchtxtErrorFrmAccnt.text = "You already deleted this account. Please add the account under <a onclick= addMyAccntFTMB()>My Account List</a> before proceeding with this future transfer!"
							if(flowSpa){
                            	frmMBFTView.rchtxtErrorFrmAccnt.text = "You already deleted this account.<br> Please add the account under <br> <a onclick= addMyAccntFTMB()>My Account List</a> <br> before proceeding with this future transfer!"
                            }
                            else {
                            	frmMBFTView.rchtxtErrorFrmAccnt.text = "You already deleted this account. <br> \Please add the account under <a onclick= addMyAccntFTMB()>My Account List</a> <br> before proceeding with this future transfer!"
                            }
                            frmMBFTView.rchtxtErrorFrmAccnt.skin = lblGray;
                            //frmMBFTView.rchtxtErrorFrmAccnt.onClick = addMyAccntFTMB;	
                            frmMBFTView.hbxFrmAccntHidenError.onClick = addMyAccntFTMB;
                            //frmMBFTView.rchtxtErrorFrmAccnt.onClick = frmMyAccountList.show();
                            //Disabling all Edit btn click 
                            frmMBFTView.btnFTEditFlow.setEnabled(false);

                        } else {
                            frmMBFTView.hbxFrmAccntHidenError.setVisibility(false);
                            frmMBFTView.hbxFromAccnt.setVisibility(true);
                            frmMBFTView.rchtxtErrorFrmAccnt.setVisibility(false);

                            frmMBFTView.btnFTEditFlow.setEnabled(true);
                            frmMBFTView.lblFrmAccntName.setVisibility(true);

                            if (frmMBFTView.lblFrmAccntName.text != null || frmMBFTView.lblFrmAccntName.text != "") frmMBFTView.lblFrmAccntName.text = TMBAccntDataMB[k].accntName;
                            if (frmMBFTView.lblFrmAccntNickName.text != null || frmMBFTView.lblFrmAccntNickName.text != "") frmMBFTView.lblFrmAccntNickName.text = TMBAccntDataMB[k].nickName;

                            var amt = commaFormatted(TMBAccntDataMB[k].availBal);
                            //frmMBFTEdit.lblFTEditAftrAvailBalVal.text = TMBAccntDataMB[k].availBal + kony.i18n.getLocalizedString("currencyThaiBaht");							    	    }
                            frmMBFTEdit.lblFTEditAftrAvailBalVal.text = amt + kony.i18n.getLocalizedString("currencyThaiBaht");

						var tmp_ICON_ID = TMBAccntDataMB[k].ICON_ID;
  						var frmProdIcon = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+tmp_ICON_ID+"&modIdentifier=PRODICON";
					//	var frmProdIcon = "https://vit.tau2904.com:443/tmb/ImageRender?crmId=&&personalizedId=&billerId="+tmp_ICON_ID+"&modIdentifier=PRODICON";
						  frmMBFTView.imgFrmAccnt.src = frmProdIcon+".png";
                          frmMBFTEdit.imgFrmAccnt.src = frmProdIcon+".png";
                          frmMBFTEditCnfrmtn.imgFrmAccnt.src = frmProdIcon+".png";
                          frmMBFTEditCmplete.imgFrmAccnt.src = frmProdIcon+".png";

			

                        }

                        //}
                    }

                }

                gblSMARTTransAmntMB = resulttable["SMARTTransAmnt"];
                gblORFTPerTransAmntLimtMB = resulttable["ORFTTransSplitAmnt"];
                gblMBStatus = resulttable["MBStatus"];

                //Fee caluculation Logic for ORFT
                var trnferType = gblpmtMethodFTMB;

                //ORFT var
                var feeORFTAmt = 0.00;
                var ORFT_FREE_TRANS_CODES = resulttable["ORFT_FREE_TRANS_CODES"]; //"219"
                var freeTransactions = "";
                var ORFTRange1Lower = resulttable["ORFTRange1Lower"]; //"0"
                ORFTRange1Lower = parseFloat(ORFTRange1Lower.toString());
                var ORFTRange1Higher = resulttable["ORFTRange1Higher"] //"20000"
                ORFTRange1Higher = parseFloat(ORFTRange1Higher.toString());

                var ORFT_FEE_AMNT1 = resulttable["ORFTSPlitFeeAmnt1"] //"25"
                var ORFT_FEE_AMNT2 = resulttable["ORFTSPlitFeeAmnt2"] //"35"
                var ORFT_FEE_RANGE1_Lower2 = resulttable["ORFTRange2Lower"]; //"20001"
                ORFT_FEE_RANGE1_Lower2 = parseFloat(ORFT_FEE_RANGE1_Lower2.toString());

				gblORFT_FEE_RANGE_MB = resulttable["ORFT_FEE_RANGE1"];  //"20001-50000"
				gblORFT_FEE_AMNT2_MB = resulttable["ORFTSPlitFeeAmnt2"] //"35"
				gblORFT_FEE_AMNT1_MB = resulttable["ORFTSPlitFeeAmnt1"] //"25"

                var temp = resulttable["ORFT_FEE_RANGE1"]; //"20001-50000"
                temp = temp.split("-", 2);

                var ORFT_FEE_RANGE1_Higher2 = temp[1]; //"50000"
                ORFT_FEE_RANGE1_Higher2 = parseFloat(ORFT_FEE_RANGE1_Higher2.toString());

                //SMART var
                var SMART_FREE_TRANS_CODES = resulttable["SMART_FREE_TRANS_CODES"]; //"219,220,222"
                var tempSmart = resulttable["SMART_FEE_RANGE1"]; //"0-100000"
                tempSmart = tempSmart.split("-", 2);

                var SMART_FEE_RANGE1_Lower1 = tempSmart[0];
                SMART_FEE_RANGE1_Lower1 = parseFloat(SMART_FEE_RANGE1_Lower1.toString());

                var SMART_FEE_RANGE1_Higher1 = tempSmart[1];
                SMART_FEE_RANGE1_Higher1 = parseFloat(SMART_FEE_RANGE1_Higher1.toString());
                var SMART_FEE_AMNT1 = resulttable["SMART_FEE_AMNT_RANGE1"];
                var feeSMARTAmt = 0.00;

                var amtTransr = frmMBFTView.lblFTViewAmountVal.text
                amtTransr = amtTransr.substring("0", amtTransr.length - 1);

                if (amtTransr.indexOf(",") != -1) {
                	amtTransr = kony.string.replace(amtTransr, ",", "");
                }
                amtTransr = parseFloat(amtTransr.toString());

                if (trnferType == "ORFT") {
					freeTransactions = 0;
					/*	
                    for (var i = 0; ORFT_FREE_TRANS_CODES.length; i++) {
                        if (gblFrmaccntProdCodeMB != ORFT_FREE_TRANS_CODES[i]) {
                            // gblFreeTransactionsMB = 0;
                            freeTransactions = 0;
                        }
                    }*/
					if (gblORFT_ALL_FREE_TRANS_CODES.indexOf(gblFrmaccntProdCodeMB) >= 0) {
                    	freeTransactions = 1;
                    }
                    if (freeTransactions == 0) {
                        if (amtTransr > ORFTRange1Lower && amtTransr <= ORFTRange1Higher) {
                            feeORFTAmt = ORFT_FEE_AMNT1;
							frmMBFTView.lblFeeVal.text = eval(feeORFTAmt).toFixed(2) + kony.i18n.getLocalizedString("currencyThaiBaht");
                        } else if (amtTransr > ORFT_FEE_RANGE1_Lower2 && amtTransr <= ORFT_FEE_RANGE1_Higher2) {
                            feeORFTAmt = ORFT_FEE_AMNT2;
                            frmMBFTView.lblFeeVal.text = eval(feeORFTAmt).toFixed(2)+ kony.i18n.getLocalizedString("currencyThaiBaht");
                        }
                    } else frmMBFTView.lblFeeVal.text = "0.00 " + kony.i18n.getLocalizedString("currencyThaiBaht");

                } else if (trnferType == "SMART") {
						freeTransactions = 0;
					/*	
                    for (var i = 0; i < SMART_FREE_TRANS_CODES.length; i++) {
                        if (gblFrmaccntProdCodeMB == SMART_FREE_TRANS_CODES[i]) freeTransactions = 0;
                    }*/

                    if (gblALL_SMART_FREE_TRANS_CODES.indexOf(gblFrmaccntProdCodeMB) >= 0) {
                    	freeTransactions = 1;
                    }

                    if (freeTransactions == 0) {
                        if (amtTransr > SMART_FEE_RANGE1_Lower1 && amtTransr <= SMART_FEE_RANGE1_Higher1) {
                            feeSMARTAmt = SMART_FEE_AMNT1;
                            frmMBFTView.lblFeeVal.text = eval(feeSMARTAmt).toFixed(2)+ kony.i18n.getLocalizedString("currencyThaiBaht");
                        }
                    } else frmMBFTView.lblFeeVal.text = 0.00 +" "+ kony.i18n.getLocalizedString("currencyThaiBaht");

                }

                // Call personalisedInq service
                getAllAccountsInfoMB();

            } else {
                dismissLoadingScreen();
                alert("No Accounts");
                return false;
            }

        } else {
            dismissLoadingScreen();
            alert("Error " + resulttable["errMsg"]);
            return false;
        }
    }

}


// JDBC which gives all the list of accounts present in AccountRelation Table 

function getAllAccountsInfoMB() {
	var temp = gblToAccntIDMB;
	if(temp.length > 10){
	  temp = temp.substring(temp.length-10, temp.length); 
	}
	
    var inputParam = {}
    inputParam["crmId"] = gblcrmId;   //"001100000000000000000002235124"//
	inputParam["personalizedAcctId"] = temp;
    
    invokeServiceSecureAsync("crmAllAccountsInquiryKony", inputParam, getAllAccountsInfocallBackMB)
}

function getAllAccountsInfocallBackMB(status, resulttable) {
//
//
    //dismissLoadingScreen();
    if (status == 400) {
    	gblPersoanlizedIdMB = "";
    	gblpersonalizedPicIdMB = "";
    	gblpersonalizedNameMB = "";
    	gblToAccntNickUpdatedMB = "";
        if (resulttable["opstatus"] == 0) {
            if (resulttable.Results.length != 0) {
                Accnts_RelTableMB = []; // global array to search from/to account status n nickname and accnut name if TMB
                for (var i = 0; i < resulttable.Results.length; i++) {
                    var accntNickName = "";
                    var accntStatus = "";
                    var accntName = "";
                    var bankcd = "";
                    var accntNum = "";
					var personalizedId = "";
					var personalizedPicId = "";
					var personalizedName = "";
						
                    if (resulttable.Results[i].acctNickName != null && resulttable.Results[i].acctNickName != "") accntNickName = resulttable.Results[i].acctNickName;
                    if (resulttable.Results[i].accntStatus != null && resulttable.Results[i].accntStatus != "") accntStatus = resulttable.Results[i].accntStatus;
                    if (resulttable.Results[i].accntName != null && resulttable.Results[i].accntName != "") accntName = resulttable.Results[i].accntName;
                    if (resulttable.Results[i].bankCD != null && resulttable.Results[i].bankCD != "") bankcd = resulttable.Results[i].bankCD;
                    if (resulttable.Results[i].personalizedAcctId != null && resulttable.Results[i].personalizedAcctId != "") accntNum = resulttable.Results[i].personalizedAcctId;
                    
                     if (resulttable.Results[i].personalizedId != null && resulttable.Results[i].personalizedId != "") 
                     				personalizedId = resulttable.Results[i].personalizedId;
                     				
                     if (resulttable.Results[i].personalizedPicId != undefined) 
                     				personalizedPicId = resulttable.Results[i].personalizedPicId;
                     				
                      if (resulttable.Results[i].personalizedName != undefined && resulttable.Results[i].personalizedName != null) 
                     				personalizedName = resulttable.Results[i].personalizedName;							
                     				
                     				
					/*
					if(bankcd != "11" && accntNum.length >= 10){
						accntNum = accntNum.substring(accntNum.length-10, accntNum.length);
					}
					*/

                    Accnts_RelTableMB.push({
                        "accntNickName": accntNickName,
                        "accntStatus": accntStatus,
                        "accntName": accntName,
                        "bankcd": bankcd,
                        "accntNum": accntNum,
                        "personalizedId":personalizedId,
                        "personalizedPicId":personalizedPicId,
                        "personalizedName":personalizedName

                    });
                }

                //Checking existenace of "TO Account" //New Changes
                var accntFound = false;
                isTMB = false;
                var isOwnAccnt = false;
                var accIndex = "";
                var tmpAccnt = gblToAccntIDMB;
                gblPersoanlizedIdMB = "";
                
               if(gblpmtMethodFTMB == "INTERNAL_TRANSFER"){
                   if (gblToAccntTypeMB == "SDA" || gblToAccntTypeMB == "CDA") {
                      if (gblToAccntIDMB.length == 10) tmpAccnt = "0000" + tmpAccnt;
                   }
               }  
                for (var j = 0; j < Accnts_RelTableMB.length; j++) {

                    if (tmpAccnt == Accnts_RelTableMB[j].accntNum) {
                        accIndex = j;
                        gblAccntName_ORFT_MB = Accnts_RelTableMB[j].accntName;
                        gblToBankCdFTMB = Accnts_RelTableMB[accIndex].bankcd;
                        gblPersoanlizedIdMB  = Accnts_RelTableMB[accIndex].personalizedId;
                        gblpersonalizedPicIdMB = Accnts_RelTableMB[accIndex].personalizedPicId;
                        gblpersonalizedNameMB = Accnts_RelTableMB[accIndex].personalizedName;
                        
                        if(isNotBlank(Accnts_RelTableMB[j].accntNickName)) {
	                        gblToAccntNickUpdatedMB = Accnts_RelTableMB[j].accntNickName;
    	                    frmMBFTView.lblToAccntNickName.text = Accnts_RelTableMB[j].accntNickName;
                        } else {
                        	gblToAccntNickUpdatedMB = frmMBFTView.lblToAccntNickName.text;
                        }
                        if(Accnts_RelTableMB[accIndex].accntName!=null && Accnts_RelTableMB[accIndex].accntName != undefined && Accnts_RelTableMB[accIndex].accntName != "NONE" && Accnts_RelTableMB[accIndex].accntName != "NA")
                        { 
                       		frmMBFTView.lblToAccntName.text = Accnts_RelTableMB[accIndex].accntName;
                        }
                        else{
                       		frmMBFTView.lblToAccntName.text = "";
                        }
                        if (Accnts_RelTableMB[j].bankcd == "11") {
                            if(gblOwnPersonlisedIdFTMB == gblPersoanlizedIdMB){
                            	isTMB = true;
                            }else
                            	isTMB = false;
                            
                            if (Accnts_RelTableMB[j].accntStatus == "02") {
                                accntFound = false;
                            } else {
                            	accntFound = true;
                                frmMBFTView.lblToAccntName.text = Accnts_RelTableMB[accIndex].accntName;
                               // frmMBFTView.lblToAccntNickName.text = Accnts_RelTableMB[accIndex].accntNickName;
                            }
                        } else accntFound = true;
                    }
                }


                if (accntFound == false) {
                    for (var k = 0; k < TMBAccntDataMB.length; k++) {
                        if (tmpAccnt == TMBAccntDataMB[k].accntNum) {
                            accIndex = k;
                            gblAccntName_ORFT_MB = TMBAccntDataMB[k].accntName;
                            gblToBankCdFTMB = "11"
                            gblToAccntfIIdentFTMB = TMBAccntDataMB[k].fiident;
                           // isTMB = true;
                           
					                            
                            if(TMBAccntDataMB[k].bankcd != ""){
                            	//gblToBankCdFTMB = TMBAccntDataMB[k].bankcd;
                            	isTMB = false;
                            	isOwnAccnt = true;
                            }else{
                            	gblToBankCdFTMB = "11";
                            	isTMB = true;
                            	isOwnAccnt = true;
                            } 
                            
                            accntFound = true;
                            if (TMBAccntDataMB[k].accntStatus == "02") {
                                accntFound = false;
                            } else {
                                frmMBFTView.lblToAccntName.text = TMBAccntDataMB[k].accntName;
                                //frmMBFTView.lblToAccntNickName.text = TMBAccntDataMB[k].nickName;
                            }
                        }
                    }
                }

                //var bnkLogoURL = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/Bank-logo" + "/bank-logo-";
                if(gblPersoanlizedIdMB != ""){
					var recepentImageURL = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/ImageRender?billerId=&crmId=Y&personalizedId="+gblPersoanlizedIdMB	
				}else
					var recepentImageURL = "";
              
              var ownProfilePic = "";
			  ownProfilePic = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=Y&personalizedId=&billerId=";
                
                if (accntFound == true) {
                    // gblToBankCdFTMB = Accnts_RelTableMB[accIndex].bankcd; 
                    frmMBFTView.hbxToAccnt.setVisibility(true);
                    frmMBFTView.hbxToAccntHidenError.setVisibility(false);
                    frmMBFTView.rchtxtErrorToAccnt.setVisibility(false);
                    if (frmMBFTView.hbxFrmAccntHidenError.isVisible == false) {
                        frmMBFTView.btnFTEditFlow.setEnabled(true);
                    } else frmMBFTView.btnFTEditFlow.setEnabled(false);


                    if (isTMB == true) {
                        //   frmMBFTView.btnFTEditFlow.setEnabled(true);
						frmMBFTView.imgToAccnt.src =  ownProfilePic;  //"avatar_dis.jpg";	//"icon.png";
	                    frmMBFTEdit.imgToAccnt.src = ownProfilePic; //"avatar_dis.jpg";	//"icon.png";
	                    frmMBFTEditCnfrmtn.imgToAccnt.src = ownProfilePic; //"avatar_dis.jpg";	//"icon.png";
	                    frmMBFTEditCmplete.imgToAccnt.src = ownProfilePic;  //"avatar_dis.jpg";	//"icon.png";

                        frmMBFTView.lblToAccntName.setVisibility(true); //Making visible as accnt name is not shown for other banks

						frmMBFTView.hbxNoteToReceipent.setVisibility(false);
						frmMBFTView.hbxNotifyReceipent.setVisibility(false);

                        //Search in CustmerAccntInq Response if not found in Account_RelTable 
					/*	
                        if (frmMBFTView.lblToAccntNickName.text == undefined || frmMBFTView.lblToAccntNickName.text == "" || frmMBFTView.lblToAccntNickName.text == null) {
                            for (var k = 0; k < TMBAccntDataMB.length; k++) {
                                if (gblToAccntIDMB == TMBAccntDataMB[k].accntNum) {
                                    frmMBFTView.lblToAccntName.setVisibility(true);
                                    frmMBFTView.lblToAccntNickName.text = TMBAccntDataMB[k].nickName;
                                }
                            }
                        }
					*/
                    } else {
                    	if(isOwnAccnt == true){
                    		frmMBFTView.lblToAccntName.setVisibility(true);
                    		
                    		frmMBFTView.imgToAccnt.src =  ownProfilePic;  
		                    frmMBFTEdit.imgToAccnt.src = ownProfilePic; 
		                    frmMBFTEditCnfrmtn.imgToAccnt.src = ownProfilePic; 
		                    frmMBFTEditCmplete.imgToAccnt.src = ownProfilePic; 
		                    
		                    frmMBFTView.hbxNoteToReceipent.setVisibility(false);
							frmMBFTView.hbxNotifyReceipent.setVisibility(false);
		                    
		                  /*  
		                    if (frmMBFTView.lblToAccntNickName.text == undefined || frmMBFTView.lblToAccntNickName.text == "" || frmMBFTView.lblToAccntNickName.text == null) {
                               for (var k = 0; k < TMBAccntDataMB.length; k++) {
                                 if (gblToAccntIDMB == TMBAccntDataMB[k].accntNum) 
                                    	frmMBFTView.lblToAccntNickName.text = TMBAccntDataMB[k].nickName;
                               }
                            }
                    	*/
                    	}else{
                    	
	                    	if(gblOwnPersonlisedIdFTMB == gblPersoanlizedIdMB){
	                    	 	frmMBFTView.hbxNoteToReceipent.setVisibility(false);
								frmMBFTView.hbxNotifyReceipent.setVisibility(false);
								
								frmMBFTView.imgToAccnt.src =  ownProfilePic;  
			                    frmMBFTEdit.imgToAccnt.src = ownProfilePic; 
			                    frmMBFTEditCnfrmtn.imgToAccnt.src = ownProfilePic; 
			                    frmMBFTEditCmplete.imgToAccnt.src = ownProfilePic; 
								
	                    	}else{
	                    	 	frmMBFTView.hbxNoteToReceipent.setVisibility(true);
								frmMBFTView.hbxNotifyReceipent.setVisibility(true);
								
								if(gblpersonalizedPicIdMB != null && gblpersonalizedPicIdMB != "" ){
								   if(gblpersonalizedPicIdMB.indexOf("fbcdn") >= 0){
								   		frmMBFTView.imgToAccnt.src = gblpersonalizedPicIdMB;	
				                        frmMBFTEdit.imgToAccnt.src = gblpersonalizedPicIdMB; 
				                        frmMBFTEditCnfrmtn.imgToAccnt.src = gblpersonalizedPicIdMB; 
				                        frmMBFTEditCmplete.imgToAccnt.src = gblpersonalizedPicIdMB; 
								   }else{
									    frmMBFTView.imgToAccnt.src = recepentImageURL;	
				                        frmMBFTEdit.imgToAccnt.src = recepentImageURL; 
				                        frmMBFTEditCnfrmtn.imgToAccnt.src = recepentImageURL; 
				                        frmMBFTEditCmplete.imgToAccnt.src = recepentImageURL; 
								   }
								}else{
								    frmMBFTView.imgToAccnt.src = recepentImageURL;	
			                        frmMBFTEdit.imgToAccnt.src = recepentImageURL; 
			                        frmMBFTEditCnfrmtn.imgToAccnt.src = recepentImageURL; 
			                        frmMBFTEditCmplete.imgToAccnt.src = recepentImageURL; 
								}
		                     }
                    	
	                    	//if (Accnts_RelTableMB[accIndex].accntNickName != null || Accnts_RelTableMB[accIndex].accntNickName != "") frmMBFTView.lblToAccntNickName.text = Accnts_RelTableMB[accIndex].accntNickName;
							frmMBFTView.lblToAccntName.setVisibility(true);
                    	}
                    }

                } else {
                	/*
                    frmMBFTView.hbxToAccnt.setVisibility(false);
                    frmMBFTView.hbxToAccntHidenError.setVisibility(true);
                    frmMBFTView.rchtxtErrorToAccnt.setVisibility(true);
                    frmMBFTView.rchtxtErrorToAccnt.text = "You already deleted this recipient. Please add the recipient under <a onclick= addReceipentFTMB() style= 'color:blue'>My Receipent List</a> before proceeding with this future transfer!"
                    frmMBFTView.rchtxtErrorToAccnt.skin = lblGray;
                    //frmMBFTView.rchtxtErrorToAccnt.onClick = addReceipentFTMB;
                    frmMBFTView.hbxToAccntHidenError.onClick = addReceipentFTMB;

                    //frmMBFTView.hbxFrmAccntHidenError.onClick = frmMyRecipients.show();
					*/
					frmMBFTView.lblToAccntNickName.text = "";
					frmMBFTView.lblToAccntName.text = "";
                    frmMBFTView.imgToAccnt.src = "avatar_dis.png";
                    //frmMBFTView.btnFTEditFlow.setEnabled(false);

                }
                //calling getBankDetails Service to get the Bank ShortName
                // srvGetBankTypeFTMB(gblToBankCdFTMB);
                if (gblpmtMethodFTMB == "INTERNAL_TRANSFER") {
                    if (gblFrmAccntTypeMB == "CDA") {
                        getTDAccountDetailsFTMB(gblFrmAccntIDMB, gblFrmAccntfIIdentFTMB);

                    } else if (gblToAccntTypeMB == "CDA") {
                        getTDAccountDetailsFTMB(gblToAccntIDMB, gblToAccntfIIdentFTMB);

                    } else feeTrnsfrInqMB();

                } else srvGetBankTypeFTMB(gblToBankCdFTMB); //calling getBankDetails Service to get the Bank ShortName

            } else {
                dismissLoadingScreen();
                alert("No records in Account Relation Table");
                return false;
            }

        } else {
      //      dismissLoadingScreen();
       //     alert(resulttable["errMsg"]);
       //     return false;
// New changes 24/12
        
       //Checking existenace of "TO Account" //New Changes
                var accntFound = false;
                isTMB = false;
                var accIndex = "";
                var tmpAccnt = gblToAccntIDMB;
                
               if(gblpmtMethodFTMB == "INTERNAL_TRANSFER"){
                   if (gblToAccntTypeMB == "SDA" || gblToAccntTypeMB == "CDA") {
                      if (gblToAccntIDMB.length == 10) tmpAccnt = "0000" + tmpAccnt;
                   }
               }  

                    for (var k = 0; k < TMBAccntDataMB.length; k++) {
                        if (tmpAccnt == TMBAccntDataMB[k].accntNum) {
                            accIndex = k;
                            gblAccntName_ORFT_MB = TMBAccntDataMB[k].accntName;
                            gblToBankCdFTMB = "11"
					                            
                            if(TMBAccntDataMB[k].bankcd != ""){
                            	gblToBankCdFTMB = TMBAccntDataMB[k].bankcd;
                            	isTMB = false;
                            }else{
                            	gblToBankCdFTMB = "11";
                            	isTMB = true;
                            }
                            
                            gblToAccntfIIdentFTMB = TMBAccntDataMB[k].fiident;
                          //  isTMB = true;
                            accntFound = true;
                            if (TMBAccntDataMB[k].accntStatus == "02") {
                                accntFound = false;
                            } else {
                                frmMBFTView.lblToAccntName.text = TMBAccntDataMB[k].accntName;
                               // frmMBFTView.lblToAccntNickName.text = TMBAccntDataMB[k].nickName;
                            }
                        }
                    }
               
                if (accntFound == true) {
                    // gblToBankCdFTMB = Accnts_RelTableMB[accIndex].bankcd; 
                    frmMBFTView.hbxToAccnt.setVisibility(true);
                    frmMBFTView.hbxToAccntHidenError.setVisibility(false);
                    frmMBFTView.rchtxtErrorToAccnt.setVisibility(false);
                    if (frmMBFTView.hbxFrmAccntHidenError.isVisible == false) {
                        frmMBFTView.btnFTEditFlow.setEnabled(true);
                    } else frmMBFTView.btnFTEditFlow.setEnabled(false);

				/*	
					if (frmMBFTView.lblToAccntNickName.text == undefined || frmMBFTView.lblToAccntNickName.text == "" || frmMBFTView.lblToAccntNickName.text == null ) {
                        for (var k = 0; k < TMBAccntDataMB.length; k++) {
                            if (gblToAccntIDMB == TMBAccntDataMB[k].accntNum) {
                                frmMBFTView.lblToAccntName.setVisibility(true);
                                frmMBFTView.lblToAccntNickName.text = TMBAccntDataMB[k].nickName;
                            }
                        }
                    }
				*/
				frmMBFTView.hbxNoteToReceipent.setVisibility(false);
				frmMBFTView.hbxNotifyReceipent.setVisibility(false);

			 	 	var ownProfilePic = "";
			  		ownProfilePic = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=Y&personalizedId=&billerId=";

					frmMBFTView.imgToAccnt.src = ownProfilePic;
                    frmMBFTEdit.imgToAccnt.src = ownProfilePic;
                    frmMBFTEditCnfrmtn.imgToAccnt.src = ownProfilePic;
                    frmMBFTEditCmplete.imgToAccnt.src = ownProfilePic;

                    if (isTMB == true) {
                        frmMBFTView.lblToAccntName.setVisibility(true); 
                    } else 
						frmMBFTView.lblToAccntName.setVisibility(true);

                } else {
                	//Changes as per DEF15787
					/*
                    frmMBFTView.hbxToAccnt.setVisibility(false);
                    frmMBFTView.hbxToAccntHidenError.setVisibility(true);
                    frmMBFTView.rchtxtErrorToAccnt.setVisibility(true);
                    frmMBFTView.rchtxtErrorToAccnt.text = "You already deleted this recipient. Please add the recipient under <a onclick= addReceipentFTMB() style= 'color:blue'>My Receipent List</a> before proceeding with this future transfer!"
                    frmMBFTView.rchtxtErrorToAccnt.skin = lblGray;
                    //frmMBFTView.rchtxtErrorToAccnt.onClick = addReceipentFTMB;
                    frmMBFTView.hbxToAccntHidenError.onClick = addReceipentFTMB;
						
                    //frmMBFTView.hbxFrmAccntHidenError.onClick = frmMyRecipients.show();
                    */
                    //frmMBFTView.lblToAccntNickName.text = "";
                    frmMBFTView.imgToAccnt.src = "avatar_dis.png";
                    //frmMBFTView.btnFTEditFlow.setEnabled(false);

                }
                //calling getBankDetails Service to get the Bank ShortName
                // srvGetBankTypeFTMB(gblToBankCdFTMB);
                if (gblpmtMethodFTMB == "INTERNAL_TRANSFER") {
                    if (gblFrmAccntTypeMB == "CDA") {
                        getTDAccountDetailsFTMB(gblFrmAccntIDMB, gblFrmAccntfIIdentFTMB);

                    } else if (gblToAccntTypeMB == "CDA") {
                        getTDAccountDetailsFTMB(gblToAccntIDMB, gblToAccntfIIdentFTMB);

                    } else feeTrnsfrInqMB();

                } else srvGetBankTypeFTMB(gblToBankCdFTMB); //calling getBankDetails Service to get the Bank ShortName
            
        }
    }
}


// to get TD details


function getTDAccountDetailsFTMB(acctID, fIIdent) {
    var inputParam = {}
    var fromAcctID = "";
    if (acctID.indexOf("-", 0) != -1) {
    	acctID = kony.string.replace(acctID, "-", "");
    }

    if (acctID.length == 14) {
        acctID = acctID.substring(acctID.length - 10, acctID.length);
    }

    inputParam["acctIdentValue"] = acctID;
    inputParam["fIIdent"] = fIIdent;
    //showLoadingScreenPopup();
    invokeServiceSecureAsync("tDDetailinq", inputParam, getTDAccountDetailsFTCallBackMB);

}

function getTDAccountDetailsFTCallBackMB(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            //resulttable.tdDetailsRec[0].depositNo  //&& gblToBankCdFT == "11"
			if(resulttable.tdDetailsRec.length == 0){
				dismissLoadingScreen();
				alert(kony.i18n.getLocalizedString("ECGenericError")+" ");
			}
	
            var inputParam = {};
            inputParam["fromAcctTypeValue"] = gblFrmAccntTypeMB;
            inputParam["toAcctTypeValue"] = gblToAccntTypeMB;
            var tempAmt = frmMBFTView.lblFTViewAmountVal.text;
           
			tempAmt = parseFloat(removeCommos(tempAmt)).toFixed(2);
			
            inputParam["transferAmt"] = tempAmt;
            
            
            inputParam["transferDate"] = hypnFormatDateFT(frmMBFTView.lblViewStartOnDateVal.text) //yyyy-mm-dd
            inputParam["waiverCode"] = "I";
            inputParam["tranCode"] = gblTransCodeFTMB;

            if (gblFrmAccntTypeMB == "CDA" && gblToAccntTypeMB == "CDA" && gblFrmAccntdepositeNoMB == "") {
                gblFrmAccntdepositeNoMB = resulttable.tdDetailsRec[0].depositNo;
                getTDAccountDetailsFTMB(gblToAccntIDMB, gblToAccntfIIdentFTMB);
            } else {
                if (gblFrmAccntTypeMB == "CDA" && gblToAccntTypeMB == "CDA" && gblFrmAccntdepositeNoMB != "") {
                    var depositeNo = resulttable.tdDetailsRec[0].depositNo;
                    if (depositeNo.length == 1) {
                        depositeNo = "00" + depositeNo;
                    } else if (depositeNo.length == 2) depositeNo = "0" + depositeNo;

                    if (gblFrmAccntdepositeNoMB.length == 1) {
                        gblFrmAccntdepositeNoMB = "00" + gblFrmAccntdepositeNoMB;
                    } else if (gblFrmAccntdepositeNoMB.length == 2) gblFrmAccntdepositeNoMB = "0" + gblFrmAccntdepositeNoMB;

                    inputParam["fromAcctNo"] = gblFrmAccntIDMB + gblFrmAccntdepositeNoMB;
                    inputParam["toAcctNo"] = gblToAccntIDMB + depositeNo;
                    invokeServiceSecureAsync("fundTransferInq", inputParam, feeTrnsfrInqcallBackMB);
                } else {
                    var depositeNo = resulttable.tdDetailsRec[0].depositNo;
                    if (depositeNo.length == 1) {
                        depositeNo = "00" + depositeNo;
                    } else if (depositeNo.length == 2) depositeNo = "0" + depositeNo;

                    if (gblFrmAccntTypeMB == "CDA") inputParam["fromAcctNo"] = gblFrmAccntIDMB + depositeNo;
                    else inputParam["fromAcctNo"] = gblFrmAccntIDMB;

                    if (gblToAccntTypeMB == "CDA") inputParam["toAcctNo"] = gblToAccntIDMB + depositeNo;
                    else inputParam["toAcctNo"] = gblToAccntIDMB;

                    invokeServiceSecureAsync("fundTransferInq", inputParam, feeTrnsfrInqcallBackMB);
                }
            }
        } else {
            dismissLoadingScreenPopup();
            alert(" " + resulttable["errMsg"]);
        }
    }
}


// PaymentInquiry service to get payment details
function srvPaymentInqMB() {
    showLoadingScreen();
    var inputparam = {};
  // inputparam["scheRefID"] = "ST1400000005295900" //"ST1400000005246200"; //"ST1400000005263900";  //"ST1400000003413200";//"ST1400000003407600" //"ST1300000001472200"//"FT1300000001276100"//"13000000011409";
    inputparam["scheRefID"] =  gblSchduleRefIDFT;  		
    inputparam["rqUID"] = "";
    
    invokeServiceSecureAsync("doPmtInq", inputparam, srvPaymentInqCallBackMB);

}

function srvPaymentInqCallBackMB(status, resulttable) {
    
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            
            if (resulttable["StatusCode"] == "0") {
                if (resulttable["PmtInqRs"].length == 0) {
                    dismissLoadingScreen();
                    //alert("" + resulttable["StatusDesc"]);
                    alert("" + resulttable["AdditionalStatusDesc"]);
                    return false;
                }

                gblIsEditAftrFrmServiceMB = false;
                gblIsEditOnDateFrmServiceMB = false;
                gblIsEditAftrMB = false;
                gblIsEditOnDateMB = false;
                gblScheduleEndFTMB = "none"

                gblFrmAccntIDMB = resulttable["PmtInqRs"][0]["FromAccId"]
                gblFrmAccntTypeMB = resulttable["PmtInqRs"][0]["FromAccType"]
                gblToAccntIDMB = resulttable["PmtInqRs"][0]["ToAccId"];
                gblToAccntTypeMB = resulttable["PmtInqRs"][0]["ToAccType"]
                gblToAccntNickMB =  resulttable["PmtInqRs"][0]["ToAcctNickname"];
                
                if(resulttable["PmtInqRs"][0]["BankId"] != undefined)
                		gblToBankCdFTMB = resulttable["PmtInqRs"][0]["BankId"];	//gblToBankCdFT	gblToBankIdMB
                gblTransfrAmtMB = resulttable["PmtInqRs"][0]["Amt"]
                gblTransCodeFTMB = resulttable["PmtInqRs"][0]["TransCode"];
                gblOrderDateFTMB = resulttable["PmtInqRs"][0]["InitiatedDt"];
                gblpmtMethodFTMB = resulttable["PmtInqRs"][0]["PmtMethod"];

				 if (gblpmtMethodFTMB == "INTERNAL_TRANSFER") {
				    var len = gblToAccntIDMB.length;
				    if (gblToAccntTypeMB == "CDA") 
				    	if(len == "13")gblToAccntIDMB = gblToAccntIDMB.substring(0, len-3);
				 }


				frmMBFTView.lblToAccntNickName.text = resulttable["PmtInqRs"][0]["ToAcctNickname"];
                frmMBFTView.lblFTViewAmountVal.text = commaFormatted(resulttable["PmtInqRs"][0]["Amt"])+ " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                frmMBFTView.imgFrmAccnt.src = "icon_pig.png"; // Have to add images
                frmMBFTView.imgToAccnt.src = "icon_pig.png"; // Have to add images
                //var tmp = gblFrmAccntIDMB;
				//masking CR code
                var tmp = resulttable["PmtInqRs"][0]["maskedFromAccId"];
                tmp = tmp.substring(tmp.length - 10, tmp.length); // Last 10 digit	
                frmMBFTView.lblFrmAccntNum.text = accntNumFormat(tmp); //Formatting the AccntNum
                //var tmpToAcnt = gblToAccntIDMB;
				//masking CR code
				var tmpToAcnt = resulttable["PmtInqRs"][0]["maskedToAccId"];
                if (tmpToAcnt.length >= 10) {
                	if(gblpmtMethodFTMB != "SMART" && gblpmtMethodFTMB != "ORFT"){
						if(resulttable["PmtInqRs"][0]["ToAccType"]=="CDA"){
	                    	if(tmpToAcnt.length == 13){
	                    		tmpToAcnt = tmpToAcnt.substring(tmpToAcnt.length - 13, tmpToAcnt.length-3);//first 10 digit
	                    	}
	                    	frmMBFTView.lblToAccntNum.text = accntNumFormat(tmpToAcnt);//for term deposit acc
	                    }else{
	                		tmpToAcnt = tmpToAcnt.substring(tmpToAcnt.length - 10, tmpToAcnt.length); // Last 10 digit
	                    	frmMBFTView.lblToAccntNum.text = accntNumFormat(tmpToAcnt);
						}
	                }else{
						if(resulttable["PmtInqRs"][0]["ToAccType"]=="CDA"){
	                    	if(tmpToAcnt.length == 13){
								tmpToAcnt = tmpToAcnt.substring(tmpToAcnt.length - 13, tmpToAcnt.length-3);//first 10 digit
	                    	}
	                    	frmMBFTView.lblToAccntNum.text = accntNumFormat(tmpToAcnt);
						}else{
							if(tmpToAcnt.length == 10){
								frmMBFTView.lblToAccntNum.text = accntNumFormat(tmpToAcnt);
							}else{
								frmMBFTView.lblToAccntNum.text = tmpToAcnt;
							}
						}
                    }
                } else frmMBFTView.lblToAccntNum.text = tmpToAcnt; //gblToAccntIDMB;  //Formatting the AccntNum

                frmMBFTView.lblOrderDateVal.text = dateFormatForDisplayWithTimeStampFTMB(resulttable["PmtInqRs"][0]["InitiatedDt"]);
                frmMBFTView.lblViewStartOnDateVal.text = formatDateFTMB(resulttable["PmtInqRs"][0]["Duedt"])
				gblNotificationTypeMB = "";	   	
				
				//due to DEF1528 defect changed NotivicationType value
                //notificationType = 1 = "Email" and notificationType = 0 = "SMS"
				
				if(resulttable["PmtInqRs"][0]["NotificationType"] != undefined){
					gblNotificationTypeMB = resulttable["PmtInqRs"][0]["NotificationType"];
					if(resulttable["PmtInqRs"][0]["RecipientEmailAddr"] != undefined && resulttable["PmtInqRs"][0]["NotificationType"] == 1){
						frmMBFTView.lblNotifyRecipientVal.text = resulttable["PmtInqRs"][0]["RecipientEmailAddr"];
					}else if( resulttable["PmtInqRs"][0]["RecipientMobileNbr"] != undefined && resulttable["PmtInqRs"][0]["NotificationType"] == 0){
						frmMBFTView.lblNotifyRecipientVal.text = resulttable["PmtInqRs"][0]["RecipientMobileNbr"];
					}else
						frmMBFTView.lblNotifyRecipientVal.text = "-";
				}else
					frmMBFTView.lblNotifyRecipientVal.text = "-";   		
					   	

                if (resulttable["PmtInqRs"][0]["RecipientNote"] != undefined) {
                    frmMBFTView.lblNoteToRecipientVal.text = resulttable["PmtInqRs"][0]["RecipientNote"];
                } else frmMBFTView.lblNoteToRecipientVal.text = "-";

				if(resulttable["PmtInqRs"][0]["MyNote"] != undefined){
					frmMBFTView.lblMyNoteVal.text = resulttable["PmtInqRs"][0]["MyNote"];
				}else
				    frmMBFTView.lblMyNoteVal.text = "-";
			                
                var scheRefNo = resulttable["PmtInqRs"][0]["scheRefNo"];
                frmMBFTView.lblScheduleRefNoVal.text = scheRefNo;

                if (resulttable["PmtInqRs"][0]["RepeatAs"] == "Daily" || resulttable["PmtInqRs"][0]["RepeatAs"] == "Weekly" || resulttable["PmtInqRs"][0]["RepeatAs"] == "Monthly" || resulttable["PmtInqRs"][0]["RepeatAs"] == "Annually") {
                    frmMBFTView.lblRepeatAsVal.text = resulttable["PmtInqRs"][0]["RepeatAs"];
                    gblFTViewRepeatAsValMB = resulttable["PmtInqRs"][0]["RepeatAs"];

                    if (resulttable["PmtInqRs"][0]["RepeatAs"] == "Daily") gblScheduleRepeatFTMB = kony.i18n.getLocalizedString("keyDaily");
                    if (resulttable["PmtInqRs"][0]["RepeatAs"] == "Weekly") gblScheduleRepeatFTMB = kony.i18n.getLocalizedString("keyWeekly");
                    if (resulttable["PmtInqRs"][0]["RepeatAs"] == "Monthly") gblScheduleRepeatFTMB = kony.i18n.getLocalizedString("keyMonthly");
                    if (resulttable["PmtInqRs"][0]["RepeatAs"] == "Annually") {
                        gblScheduleRepeatFTMB = kony.i18n.getLocalizedString("keyYearly");
                        gblFTViewRepeatAsValMB = "Yearly" //-- global variable gets loaded with server response for all except annually, it is made to Yearly
                        frmMBFTView.lblRepeatAsVal.text = kony.i18n.getLocalizedString("keyYearly");
                    }
                } else {
                    frmMBFTView.lblRepeatAsVal.text = kony.i18n.getLocalizedString("keyOnce"); //"Once";
                    gblScheduleRepeatFTMB = "Once";
                    gblFTViewRepeatAsValMB = "Once";
                    //gblScheduleEndFTMB = "none";
                }
				gblRepeatAsCopy=gblFTViewRepeatAsValMB;
				                //Storing globally to map the old data after schedule edit in edit page and then cancel click		
                gblFTViewInitiateDateMB = resulttable["PmtInqRs"][0]["InitiatedDt"];
                gblFTViewStartOnDateMB = resulttable["PmtInqRs"][0]["Duedt"];
                if (gblScheduleRepeatFTMB != "Once") {
                    if (resulttable["PmtInqRs"][0]["EndDate"] != undefined) {
                        
                        frmMBFTView.lblEndOnDateVal.text = formatDateFTMB(resulttable["PmtInqRs"][0]["EndDate"]);
                        gblFTViewEndDateMB = resulttable["PmtInqRs"][0]["EndDate"];

                        //Adding these to differentiate between After and OnDate to show with focus on click of schedule button	
                        gblIsEditAftrFrmServiceMB = false;
                        gblIsEditOnDateFrmServiceMB = true;

                        gblIsEditAftrMB = false;
                        gblIsEditOnDateMB = true;

                        gblScheduleEndFTMB = kony.i18n.getLocalizedString("keyOnDate")
                        gblFTViewEndValMB = kony.i18n.getLocalizedString("keyOnDate");
                        frmMBFTView.lblExcuteVal.text = (numberOfExecution(frmMBFTView.lblViewStartOnDateVal.text, frmMBFTView.lblEndOnDateVal.text, gblFTViewRepeatAsValMB)).toString(); // toString() add due to DEF665
                        gblFTViewExcuteValMB = numberOfExecution(frmMBFTView.lblViewStartOnDateVal.text, frmMBFTView.lblEndOnDateVal.text, gblFTViewRepeatAsValMB);

                        frmMBFTView.lblRemainingVal.text = "- "+kony.i18n.getLocalizedString("keyTimesMB")+")";
                        gblFTViewExcuteremaingMB = "- )";
                        
                       gblFTViewExcuteremaingMB = parseFloat(gblFTViewExcuteValMB.toString()) - parseFloat(resulttable["PmtInqRs"][0]["TotalProcessesTransactions"]); //have to chek 
                       frmMBFTView.lblRemainingVal.text = gblFTViewExcuteremaingMB+" "+kony.i18n.getLocalizedString("keyTimesMB")+")"; 
                        

                    } else if (resulttable["PmtInqRs"][0]["ExecutionTimes"] != undefined) {
                        
                        frmMBFTView.lblExcuteVal.text = resulttable["PmtInqRs"][0]["ExecutionTimes"]

                        gblIsEditAftrFrmServiceMB = true;
                        gblIsEditOnDateFrmServiceMB = false;
                        gblIsEditAftrMB = true;
                        gblIsEditOnDateMB = false;

                        var tempDate = formatDateFTMB(resulttable["PmtInqRs"][0]["Duedt"])
                        var times = resulttable["PmtInqRs"][0]["ExecutionTimes"]
                        gblScheduleEndFTMB = kony.i18n.getLocalizedString("keyAfter");
                        gblFTViewEndValMB = kony.i18n.getLocalizedString("keyAfter");
                        endDateCalMB(formatDateFTMB(resulttable["PmtInqRs"][0]["Duedt"]), times);

                        gblFTViewExcuteValMB = resulttable["PmtInqRs"][0]["ExecutionTimes"];
                        gblFTViewExcuteremaingMB = parseFloat(resulttable["PmtInqRs"][0]["ExecutionTimes"]) - parseFloat(resulttable["PmtInqRs"][0]["TotalProcessesTransactions"]); //have to chek
						frmMBFTView.lblRemainingVal.text = gblFTViewExcuteremaingMB +" "+kony.i18n.getLocalizedString("keyTimesMB")+")";

                    } else if ((resulttable["PmtInqRs"][0]["EndDate"] == undefined) && (resulttable["PmtInqRs"][0]["ExecutionTimes"] == undefined)) {

                        frmMBFTView.lblExcuteVal.text = "-";
                        frmMBFTView.lblRemainingVal.text = "-"+" "+kony.i18n.getLocalizedString("keyTimesMB")+")";
                        frmMBFTView.lblEndOnDateVal.text = "-"

                        gblFTViewExcuteValMB = "-";
                        gblFTViewExcuteremaingMB = "-";
                        gblScheduleEndFTMB = kony.i18n.getLocalizedString("keyNever");
                        gblFTViewEndValMB = kony.i18n.getLocalizedString("keyNever");
                    }

                } else {
                    gblScheduleEndFTMB = "none";
                    gblFTViewEndValMB = "none"

                    frmMBFTView.lblEndOnDateVal.text =  formatDateFTMB(resulttable["PmtInqRs"][0]["Duedt"]);
                    frmMBFTView.lblExcuteVal.text = "1";
                    frmMBFTView.lblRemainingVal.text = "1"+" "+kony.i18n.getLocalizedString("keyTimesMB")+")";

                    gblFTViewExcuteValMB = "1"
                    gblFTViewEndDateMB = formatDateFTMB(resulttable["PmtInqRs"][0]["Duedt"]);
                    gblFTViewExcuteremaingMB = "1";

                }

                if (resulttable["PmtInqRs"][0]["PmtMethod"] == "INTERNAL_TRANSFER") {
                    frmMBFTView.lblMethodName.text = "";
                    frmMBFTView.lblFee.text = "Fee:";
                    frmMBFtSchedule.btnDaily.setEnabled(true);
                } else if (resulttable["PmtInqRs"][0]["PmtMethod"] == "ORFT") {
                    frmMBFTView.lblMethodName.text = resulttable["XferExcutionTime"]; //"ORFT";
                    frmMBFTView.lblFee.text = "Fee ORFT:";
                    frmMBFTEdit.lblFee.text = "Fee ORFT:";
                    frmMBFTEditCnfrmtn.lblFee.text = "Fee ORFT:";
                    frmMBFTEditCmplete.lblFee.text = "Fee ORFT:";

                    frmMBFtSchedule.btnDaily.setEnabled(true);
                    // call ORFT trnfr fee cal service (feeEngine) 
                    //Caluculating the fee for ORFT , Logic without feeEngine service call in the response of cusomerAccountInq
                } else if (resulttable["PmtInqRs"][0]["PmtMethod"] == "SMART") {
                    frmMBFTView.lblMethodName.text = resulttable["XferExcutionTime"]; //"SMART";
                    frmMBFTView.lblFee.text = "Fee SMART:";
                    frmMBFTEdit.lblFee.text = "Fee SMART:";
                    frmMBFTEditCnfrmtn.lblFee.text = "Fee SMART:";
                    frmMBFTEditCmplete.lblFee.text = "Fee SMART:";


                    //Diabling the "daily" button on schedule
                    frmMBFtSchedule.btnDaily.setEnabled(false);
                    // call SMART trnfr fee cal service (feeEngine) 
                    // Caluculating Fee for SMART	Logic without feeEngine service call in the response of cusomerAccountInq
                }
                //Calling custmerAccntInq service
                getAccountListServiceMB();

            } else {
                dismissLoadingScreen();
                //alert(resulttable["StatusDesc"]);
                alert("" + resulttable["AdditionalStatusDesc"]);
                return false;
            }

        } else {
            dismissLoadingScreen();
            //alert(resulttable["errmsg"]);
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }

    }
}

// Service to check TMB Internal Transfer is 0 or not 
function feeTrnsfrInqMB() {
    //
    var inputParam = {};
    //inputparam["crmId"]= gblcrmId;
    inputParam["fromAcctNo"] = gblFrmAccntIDMB
    inputParam["fromAcctTypeValue"] = gblFrmAccntTypeMB;
    inputParam["toAcctNo"] = gblToAccntIDMB
    inputParam["toAcctTypeValue"] = gblToAccntTypeMB;
    var tempAmt = frmMBFTView.lblFTViewAmountVal.text;
    
	tempAmt = parseFloat(removeCommos(tempAmt)).toFixed(2);
	
    inputParam["transferAmt"] = tempAmt;
    
    inputParam["transferDate"] = hypnFormatDateFT(frmMBFTView.lblViewStartOnDateVal.text) //yyyy-mm-dd  hardcoading as of now.
    inputParam["waiverCode"] = "I";
    inputParam["tranCode"] = gblTransCodeFTMB;
    //gblTransCodeFTMB = "88"+frmAccntVal+toAccntVal;   // Used for payment add service
    
    invokeServiceSecureAsync("fundTransferInq", inputParam, feeTrnsfrInqcallBackMB)

}

function feeTrnsfrInqcallBackMB(status, resulttable) {


    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (resulttable["StatusCode"] == 0) {

                if (resulttable["transferFee"] > 0) {

                    frmMBFTView.lblMethodName.text = resulttable["FlagFeeReg"];
                    frmMBFTView.lblFee.text = "Fee Cross Region:";
                    frmMBFTEdit.lblFee.text = "Fee Cross Region:";
                    frmMBFTEditCnfrmtn.lblFee.text = "Fee Cross Region:";
                    frmMBFTEditCmplete.lblFee.text = "Fee Cross Region:";

                    frmMBFTView.lblFeeVal.text = resulttable["transferFee"] +" "+ kony.i18n.getLocalizedString("currencyThaiBaht");
                } else {
                    frmMBFTView.lblMethodName.text = "";
                    frmMBFTView.lblFeeVal.text = resulttable["transferFee"] +" "+ kony.i18n.getLocalizedString("currencyThaiBaht");
                }
                //Call the custmerAccntInq service
                //getAccountListServiceMB();
                srvGetBankTypeFTMB(gblToBankCdFTMB);

            } else {
                dismissLoadingScreen();
                alert(resulttable["errMsg"]);
                return false;
            }

        } else {
            dismissLoadingScreen();
            alert(resulttable["StatusDesc"]);
            return false;
        }

    } else {
        

    }

}


// Service to get daily channel for ORFT n SMART
// Validation for Amount


function getFeeCrmProfileInqMB() {
    showLoadingScreen();
    var inputParam = {}
    //inputParam["crmId"] = gblcrmId;
	if(gblCRMProfileInqCalMB == false){
		var amt = frmMBFTEdit.txtEditAmnt.text;
		var Fee = frmMBFTEditCnfrmtn.lblFeeVal.text;
		var startOnDate = frmMBFTEdit.lblViewStartOnDateVal.text;
		startOnDate = changeDateFormatForService(startOnDate);
		if(amt.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht")) != -1){
			amt = amt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim();
		}
		
		 if (amt.indexOf(",") != -1) {
       		 amt = parseFloat(replaceCommon(amt, ",", "")).toFixed(2);
   		 }else
    	     amt = parseFloat(amt).toFixed(2);
		
		//amt = replaceCommon(amt, ",", "");
		if(Fee.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht")) != -1){
			Fee = Fee.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim();
		}
		
		inputParam["module"] = "schedule";
		if (gblFrmAccntTypeMB == "CDA") {
        	inputParam["module"] = "withdraw"
       	}
       	var pmtMethod = frmMBFTView.lblMethodName.text;
       	if (gblpmtMethodFTMB == "SMART" || gblpmtMethodFTMB == "ORFT") {
       		inputParam["module"] = "execute";
        }
		
		inputParam["AmtEdited"] = amt;
		inputParam["toAccName"] = frmMBFTEdit.lblToAccntNickName.text;
		inputParam["toAccNum"] = frmMBFTEdit.lblToAccntNum.text;
		inputParam["bankName"] = frmMBFTEdit.lblBankNameVal.text;
		gblisEditFTFlow = true;
		inputParam["isEditFTFlow"] = gblisEditFTFlow+"";
		inputParam["FeeFT"] = Fee;
		
		if(flowSpa){
			inputParam["TransferDate"] = startOnDate.toString();
		}else
			inputParam["TransferDate"] = startOnDate;
		
	}

   // inputParam["transferFlag"] = "true";
    invokeServiceSecureAsync("crmProfileInq", inputParam, getFeeCrmProfileInqcallBackMB)
}


function getFeeCrmProfileInqcallBackMB(status, resulttable) {
   // 
    if (status == 400) {

        var activityTypeID = "060";
        var errorCode = "";
        var activityStatus = "00";
        var deviceNickName = "";
        var activityFlexValues1 = "Edit";
        var activityFlexValues2 = ""; //frmIBTransferNowConfirmation.lblAccountNo.text;
        var activityFlexValues3 = "";
        var activityFlexValues4 = "";
        var activityFlexValues5 = frmMBFTView.lblMethodName.text;
        
        if(activityFlexValues5 == ""){
            activityFlexValues5 = "Own";
        }
        var logLinkageId = "";
		//gblMBStatus = "00";
        if (resulttable["opstatus"] == 0) {
            if (resulttable["statusCode"] == 0) {
                activityStatus = "00";
                if (gblCRMProfileInqCalMB == true) {
                    var mbStatus = resulttable["mbFlowStatusIdRs"];
                    var SPAuserStatus = resulttable["ibUserStatusIdTr"]
                   
                    if (flowSpa) 
						{
							if (SPAuserStatus == "04") {
                					gblIBFlowStatus = "04";
                					dismissLoadingScreen();
                					//showAlert(kony.i18n.getLocalizedString("Receipent_OTPLocked"), kony.i18n.getLocalizedString("info"));
									popTransferConfirmOTPLock.show();	
                					return false;
									} 
							else 
                				gblIBFlowStatus = SPAuserStatus;
                       			
                       		}
                       else
							{
                   		 		if (mbStatus == gblMBStatus) {
                        		dismissLoadingScreen(); //gblFinancialTxnMBLock  
                        		//alert("User Channel Status is Locked");
									popTransferConfirmOTPLock.show();
                       			 //alert(kony.i18n.getLocalizedString("Error_UserStatusLocked"));
                        		return false;
                   				 }
                   			}
                   			
                    activityFlexValues2 = formatDateFT(gblFTViewStartOnDateMB);
                    activityFlexValues3 = gblFTViewRepeatAsValMB;
                    activityFlexValues4 = gblTransfrAmtMB;

                    frmMBFTEdit.lblFrmAccntName.text = frmMBFTView.lblFrmAccntName.text;
                    frmMBFTEdit.lblFrmAccntNickName.text = frmMBFTView.lblFrmAccntNickName.text;
                    frmMBFTEdit.lblFrmAccntNum.text = frmMBFTView.lblFrmAccntNum.text;
                    frmMBFTEdit.lblToAccntName.text = frmMBFTView.lblToAccntName.text;
                    frmMBFTEdit.lblToAccntNickName.text = frmMBFTView.lblToAccntNickName.text;
                    frmMBFTEdit.lblToAccntNum.text = frmMBFTView.lblToAccntNum.text;
                    frmMBFTEdit.lblBankNameVal.text = frmMBFTView.lblBankNameVal.text;
                    frmMBFTEdit.lblFeeVal.text = frmMBFTView.lblFeeVal.text;
                    frmMBFTEdit.lblOrderDateVal.text = frmMBFTView.lblOrderDateVal.text;
                    frmMBFTEdit.lblViewStartOnDateVal.text = frmMBFTView.lblViewStartOnDateVal.text;
                    frmMBFTEdit.lblRepeatAsVal.text = frmMBFTView.lblRepeatAsVal.text;
                    frmMBFTEdit.lblRemainingVal.text = frmMBFTView.lblRemainingVal.text;
                    frmMBFTEdit.lblEndOnDateVal.text = frmMBFTView.lblEndOnDateVal.text;
                    frmMBFTEdit.lblExcuteVal.text = frmMBFTView.lblExcuteVal.text;
                    frmMBFTEdit.lblNotifyRecipientVal.text = frmMBFTView.lblNotifyRecipientVal.text;
                    frmMBFTEdit.lblNoteToRecipientVal.text = frmMBFTView.lblNoteToRecipientVal.text;
                    frmMBFTEdit.lblMyNoteVal.text = frmMBFTView.lblMyNoteVal.text;
                    frmMBFTEdit.lblMethodName.text = frmMBFTView.lblMethodName.text;
                    frmMBFTEdit.lblScheduleRefNoVal.text = frmMBFTView.lblScheduleRefNoVal.text;

                    var tmpAmt = frmMBFTView.lblFTViewAmountVal.text;
                    tmpAmt = tmpAmt.substring(0, tmpAmt.length - 1);
                    tmpAmt = tmpAmt.trim();
                    
                    frmMBFTEdit.txtEditAmnt.text = frmMBFTView.lblFTViewAmountVal.text; //tmpAmt;

					if(gblToAccntNickUpdatedMB != ""){
						frmMBFTEdit.lblToAccntNickName.text = gblToAccntNickUpdatedMB;
					}

                    gblCRMProfileInqCalMB = false;

                    dismissLoadingScreen();
                    frmMBFTEdit.show();

                } else {
						
                    activityFlexValues2 = formatDateFT(gblFTViewStartOnDateMB) + "+" + frmMBFTEdit.lblViewStartOnDateVal.text //frmIBTransferNowConfirmation.lblAccountNo.text;
                    activityFlexValues3 = gblFTViewRepeatAsValMB + "+" + frmMBFTEdit.lblRepeatAsVal.text;
                    activityFlexValues4 = gblTransfrAmtMB + "+" + frmMBFTEdit.txtEditAmnt.text;
			
                    var DailyLimit = parseFloat(resulttable["ebMaxLimitAmtCurrent"].toString());
                    var transferAmt = frmMBFTEdit.txtEditAmnt.text;
                    var len = transferAmt.length;
                   // transferAmt = transferAmt.substring("0", len - 1);
                    if (transferAmt.indexOf(",") != -1) {
                    	transferAmt = kony.string.replace(transferAmt, ",", "");
                    }

                    transferAmt = parseFloat(transferAmt.toString());
					//gblOwnPersonlisedIdFTMB != gblPersoanlizedIdMB   //frmMBFTView.hbxNotifyReceipent.isVisible

                    if (transferAmt > DailyLimit && frmMBFTView.hbxNotifyReceipent.isVisible) {
                        dismissLoadingScreen();
                        //alert("Enterd Amount Exceeds The Daily Channel Limit");
                        alert(kony.i18n.getLocalizedString("Error_MaxTransactnLimit"));
                        return false;
                    } else {
						
						frmMBFTEditCnfrmtn.lblToAccntName.text = frmMBFTEdit.lblToAccntName.text;
						
                        frmMBFTEditCnfrmtn.lblFrmAccntName.text = frmMBFTEdit.lblFrmAccntName.text;
                        frmMBFTEditCnfrmtn.lblFrmAccntNum.text = frmMBFTEdit.lblFrmAccntNum.text;
                        frmMBFTEditCnfrmtn.lblFrmAccntNickName.text = frmMBFTEdit.lblFrmAccntNickName.text;
                        frmMBFTEditCnfrmtn.lblToAccntNickName.text = frmMBFTEdit.lblToAccntNickName.text;
                        frmMBFTEditCnfrmtn.lblToAccntNum.text = frmMBFTEdit.lblToAccntNum.text;
                        frmMBFTEditCnfrmtn.lblBankNameVal.text = frmMBFTEdit.lblBankNameVal.text;
                        var amt = frmMBFTEdit.txtEditAmnt.text;
                        if(amt.indexOf(",") != -1){
                        	amt = replaceCommon(amt, ",", "");
                        }
                        
                        
                        if(amt.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht"))!= -1){
                        	frmMBFTEditCnfrmtn.lblFTViewAmountVal.text = commaFormatted(amt);
                        }else
                            frmMBFTEditCnfrmtn.lblFTViewAmountVal.text = commaFormatted(amt) +" "+ kony.i18n.getLocalizedString("currencyThaiBaht");
                       // frmMBFTEditCnfrmtn.lblFeeVal.text = frmMBFTEdit.lblFeeVal.text;
                        frmMBFTEditCnfrmtn.lblOrderDateVal.text = frmMBFTEdit.lblOrderDateVal.text;
                        frmMBFTEditCnfrmtn.lblViewStartOnDateVal.text = frmMBFTEdit.lblViewStartOnDateVal.text;
                        frmMBFTEditCnfrmtn.lblRepeatAsVal.text = frmMBFTEdit.lblRepeatAsVal.text;
                        frmMBFTEditCnfrmtn.lblEndOnDateVal.text = frmMBFTEdit.lblEndOnDateVal.text;
                        frmMBFTEditCnfrmtn.lblExcuteVal.text = frmMBFTEdit.lblExcuteVal.text;
                        frmMBFTEditCnfrmtn.lblNotifyRecipientVal.text = frmMBFTEdit.lblNotifyRecipientVal.text;
                        frmMBFTEditCnfrmtn.lblNoteToRecipientVal.text = frmMBFTEdit.lblNoteToRecipientVal.text;
                        frmMBFTEditCnfrmtn.lblMyNoteVal.text = frmMBFTEdit.lblMyNoteVal.text;
                        frmMBFTEditCnfrmtn.lblScheduleRefNoVal.text = frmMBFTEdit.lblScheduleRefNoVal.text;
                        frmMBFTEditCnfrmtn.lblRemainingVal.text = frmMBFTEdit.lblRemainingVal.text;
                        frmMBFTEditCnfrmtn.lblMethodName.text = frmMBFTEdit.lblMethodName.text;

                        dismissLoadingScreen();
                        frmMBFTEditCnfrmtn.show();
                    }
                }

            } else {
                dismissLoadingScreen();
                activityStatus = "02";
                alert(resulttable["StatusDesc"]);
            }

        } else {
            dismissLoadingScreen();
            activityStatus = "02";
            alert(resulttable["errMsg"]);
        }
        
    if(gblCRMProfileInqCalMB == false){
       activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1, activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId);
    }
        

    }
}


//If To bank is TMB 
function srvDepositeInqFTMB() {
    var inputparam = {};
    inputparam["acctId"] = gblToAccntIDMB;
    var status = invokeServiceSecureAsync("depositAccountInquiryNonSec", inputparam, srvDepositeInqFTCallBackMB);
}

function srvDepositeInqFTCallBackMB(status, resulttable) {
    
    if (status == 400) {
        
        if (resulttable["opstatus"] == 0) {
            
            
            

            //frmMBFTEditCnfrmtn.lblToAccntName.text = resulttable["accountTitle"];
             frmMBFTView.lblToAccntName.text = resulttable["accountTitle"];
             dismissLoadingScreen();
             frmMBFTView.show();

        } else {
            dismissLoadingScreen();
            
            alert(" " + resulttable["errMsg"]);
        }
    } else if (status == 300) {
        dismissLoadingScreen();
        

    }
}

function srvOrftAccountInqFTMB(acctNo) {
    var amtTransr = frmMBFTView.lblFTViewAmountVal.text;
    amtTransr = amtTransr.substring("0", amtTransr.length-1);
    if (amtTransr.indexOf(",") != -1) {
    	amtTransr = kony.string.replace(amtTransr, ",", "");
    }
    var inputParam = {}
    inputParam["fromAcctNo"] = gblFrmAccntIDMB;
    inputParam["toAcctNo"] = acctNo;
    inputParam["toFIIdent"] = gblToBankCdFTMB;
    inputParam["transferAmt"] = "500";	
 	inputParam["AccountName"] = frmMBFTView.lblToAccntName.text 
    invokeServiceSecureAsync("ORFTInq", inputParam, srvOrftAccountInqFTCallBackMB)
}

function srvOrftAccountInqFTCallBackMB(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (resulttable["StatusCode"] == 0) {
                frmMBFTView.lblToAccntName.text = resulttable["ORFTTrnferInqRs"][0]["toAcctName"];
                dismissLoadingScreen();
                frmMBFTView.show();
            } else {
                dismissLoadingScreen();
                alert(" " + resulttable["errMsg"]);
            }
        } else {
            dismissLoadingScreen();
            alert(" " + resulttable["errMsg"]);
        }
    } else if (status == 300) {
        dismissLoadingScreen();
    }
}

function srvFutureTransferDeleteMB() {
    showLoadingScreen();
    var inputparam = {};
    inputparam["rqUID"] = "12345654-2341-9876-9283-000000000011";
    inputparam["clientDate"] = gblOrderDateFTMB;
    //inputparam["PmtId"] = ""; 
    if (gblFtDelMB == false) {
        var scheID = frmMBFTEditCnfrmtn.lblScheduleRefNoVal.text;
        inputparam["scheID"] = scheID.replace("F", "S");
        invokeServiceSecureAsync("doPmtCan", inputparam, srvFutureTransferDeletecallBackMB)
    } else {
        var scheID = frmMBFTView.lblScheduleRefNoVal.text;
        inputparam["scheID"] = scheID.replace("F", "S");
        
        var activityFlexValues5 = gblpmtMethodFTMB;
        if(gblpmtMethodFTMB == "INTERNAL_TRANSFER"){
            //activityFlexValues5 = "Own";
			if(isTMB)
            {
             activityFlexValues5 = "My TMB";
            }
            else
            {
             activityFlexValues5 = "Other TMB";
            }
        }
        var logLinkageId = "";
        var tmpAmt = frmMBFTView.lblFTViewAmountVal.text;
        tmpAmt = tmpAmt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim();
        inputparam["EditFTFlow"] = "EditFT";
        inputparam["isCancelFlow"] = "true";
        inputparam["activityFlexValues1"] = "Cancel";
        inputparam["activityFlexValues2"] = formatDateFT(gblFTViewStartOnDateMB);
        inputparam["activityFlexValues3"] = gblFTViewRepeatAsValMB
        inputparam["activityFlexValues4"] = tmpAmt;
        inputparam["activityFlexValues5"] = activityFlexValues5;
        invokeServiceSecureAsync("doPmtCan", inputparam, srvFutureTransferDeleteOnlycallBackMB)
    }
}

function srvFutureTransferDeletecallBackMB(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
            var StatusCode = result["StatusCode"];
            var Severity = result["Severity"];
            var StatusDesc = result["StatusDesc"];
            if (StatusCode == 0) {
                var finSchduleRefId = frmMBFTEditCnfrmtn.lblScheduleRefNoVal.text;
                finSchduleRefId = finSchduleRefId.replace("F", "S");
                // setFinancialActivityLogFTDelete(finSchduleRefId); 
                srvGenerateNewTransRefIDFTMB();
            } else {
                dismissLoadingScreen();
                alert("Service failed with Status code : " + StatusCode + " and Status description as " + StatusDesc);
                return false;
            }
        } else {
            dismissLoadingScreen();
            alert("Service failed with opstatus code : " + result["opstatus"]);
        }
    }
}


function srvFutureTransferDeleteOnlycallBackMB(status, result) {
    if (status == 400) {
        var activityTypeID = "060";
        var errorCode = "";
        var activityStatus = "00";
        var deviceNickName = "";
        var activityFlexValues1 = "Edit";
        var activityFlexValues2 = formatDateFT(gblFTViewStartOnDateMB); //frmIBTransferNowConfirmation.lblAccountNo.text;
        var activityFlexValues3 = gblFTViewRepeatAsValMB;
        var activityFlexValues4 = gblTransfrAmtMB;
        var activityFlexValues5 = gblpmtMethodFTMB;
        if(gblpmtMethodFTMB == "INTERNAL_TRANSFER"){
            //activityFlexValues5 = "Own";
			if(isTMB)
            {
             activityFlexValues5 = "My TMB";
            }
            else
            {
             activityFlexValues5 = "Other TMB";
            }
        }
        var logLinkageId = "";
        var tmpAmt = frmMBFTView.lblFTViewAmountVal.text;
        var len = tmpAmt.length;
        tmpAmt = tmpAmt.substring("0", len - 1);
        tmpAmt = parseFloat(tmpAmt.toString());
        var feeAmt = frmMBFTView.lblFeeVal.text;
        var lenFee = feeAmt.length;
        feeAmt = feeAmt.substring("0", lenFee - 1);
        feeAmt = parseFloat(feeAmt.toString());
        if (result["opstatus"] == 0) {
            var StatusCode = result["StatusCode"];
            var Severity = result["Severity"];
            var StatusDesc = result["StatusDesc"];
            if (StatusCode == 0) {
                if (gblFtDelMB == true) {
                    activityStatus = "01";
                    dismissLoadingScreen();
                    var finSchduleRefId = frmMBFTView.lblScheduleRefNoVal.text;
                    finSchduleRefId = finSchduleRefId.replace("F", "S");
					MBMyActivitiesReloadAndShowCalendar();
                }
            } else {
                dismissLoadingScreen();
                activityStatus = "02";
                alert("Service failed with Status code : " + StatusCode + " and Status description as " + StatusDesc);
                return false;
            }

        } else {
            dismissLoadingScreen();
            activityStatus = "02";
            alert("Service failed with opstatus code : " + result["opstatus"]);
        }
        activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1, activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId);
    }
}

//Service to get To-Bank short name

function srvGetBankTypeFTMB(bankCd) {
    var inputparam = {};
    inputparam["bankCode"] = bankCd;
    invokeServiceSecureAsync("getBankDetails", inputparam, srvGetBankTypeFTCallBackMB);
}

function srvGetBankTypeFTCallBackMB(status, resulttable) {
    
    if (status == 400) {
    	var gblBankTypeMB = "";
        if (resulttable["opstatus"] == 0) {

            gblBankTypeMB = resulttable.Results[0].orftFlag;

            if (resulttable.Results[0].errMsg == undefined) {
				if (kony.i18n.getCurrentLocale() == "en_US") { 
              	 frmMBFTView.lblBankNameVal.text = resulttable.Results[0].bankNameEng;
              	}else{
              	 frmMBFTView.lblBankNameVal.text = resulttable.Results[0].bankNameThai;
               	}
            } else frmMBFTView.lblBankNameVal.text = "-";
			if(frmMBFTView.lblToAccntNickName.text == ""){
				frmMBFTView.lblToAccntNickName.text = resulttable.Results[0].bankShortName;
			}
			if((frmMBFTView.hbxFrmAccntHidenError.isVisible) || (frmMBFTView.lblToAccntNickName.text == "")){
				frmMBFTView.btnFTEditFlow.setEnabled(false);
			}else
				frmMBFTView.btnFTEditFlow.setEnabled(true);

            if (gblToBankCdFTMB == "11") {
                frmMBFTView.lblToAccntName.setVisibility(true);
                srvDepositeInqFTMB()
            } else if(gblBankTypeMB == "Y") {
            	frmMBFTView.lblToAccntName.setVisibility(true);
            	srvOrftAccountInqFTMB(gblToAccntIDMB);
            }else{
            	
                dismissLoadingScreen();
                frmMBFTView.show();
            }
        } else {
            dismissLoadingScreen();
            alert(" " + resulttable["errMsg"]);
        }
    } else if (status == 300) {
        dismissLoadingScreen();
    }
}

function onEditFtConfirm() {

if(isTMB){
	srvVerifyPasswordExFT_CS_MB(""); 
}else{
    if (flowSpa) {
   	if(gblIBFlowStatus == "04") {
		 alertUserStatusLocked();
		}
		else {	
      	  	var inputParams = {}
     		spaChnage = "editFutureTranspayments"
       	 	gblOTPFlag = true;
        	try {
           	 	kony.timer.cancel("otpTimer")
        	} catch (e) {
            	
        	}
        	//input params for SPA OTP
        	gblSpaChannel = "FutureThirdPartyTransfer";
        	if (gblFrmAccntTypeMB == "CDA") {
        		gblSpaChannel = "WithdrawTD";
        	}
        	var pmtMethod = frmMBFTView.lblMethodName.text;
        	if (gblpmtMethodFTMB == "SMART" || gblpmtMethodFTMB == "ORFT") {
        		gblSpaChannel = "ExecuteTransfer";
        	}
	 		onClickOTPRequestSpa();
    	} 
	}
   
 	else {
    	var lblText = kony.i18n.getLocalizedString("transPasswordSub");
    	var refNo = "";
    	var mobNO = "";
    
    	popupTractPwd.hbxPopupTranscPwd.skin = hbxPopupTrnsPwdBlue;
		popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = tbxPopupBlue; 
		popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = tbxPopupBlue;
    
    	showOTPPopup(lblText, refNo, mobNO, editFTConfirmation, 3);
	}
}
}

function editFTConfirmation(tranPassword) {

   // var transFlag = trassactionPwdValidatn(tranPassword);
    if (tranPassword != "") {
        //verifyPasswordEditFT(tranPassword);
        srvVerifyPasswordExFT_CS_MB(tranPassword);

    } else {
        //alert("INVALID TRANSACTION PASSWORD");
		dismissLoadingScreen();
		popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text = "";
		popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = txtErrorBG;
		popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = txtErrorBG;
		popupTractPwd.hbxPopupTranscPwd.skin = txtErrorBG;
		popupTractPwd.lblPopupTract7.skin = lblRedNormal;
		popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("invalidTxnPwd");

    }
}

//Called when delete button is clicked
function activityLogCancelFTMB() {
    var activityTypeID = "060";
    var errorCode = "";
    var activityStatus = "";
    var deviceNickName = "";
    var activityFlexValues1 = "Cancel";
    
    var activityFlexValues2 = formatDateFT(gblFTViewStartOnDateMB);
    var activityFlexValues3 = gblFTViewRepeatAsValMB;
    var activityFlexValues4 = gblTransfrAmtMB;
    var activityFlexValues5 = gblpmtMethodFTMB;
    
      if(gblpmtMethodFTMB == "INTERNAL_TRANSFER"){
          //activityFlexValues5 = "Own";
		  if(isTMB)
            {
             activityFlexValues5 = "My TMB";
            }
            else
            {
             activityFlexValues5 = "Other TMB";
            }	
      }
    var logLinkageId = "";
    activityStatus = "00";
    activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1, activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId);

}

function srvCrmProfileInqforFTCancelMB() {
    showLoadingScreen();
    var inputParam = {};
    inputParam["transferFlag"] = "true";
    invokeServiceSecureAsync("crmProfileInq", inputParam, srvCrmProfileInqforFTCancelCallBackMB)

}

function srvCrmProfileInqforFTCancelCallBackMB(status, resulttable) {
    if (status == 400) {

        var activityTypeID = "060";
        var errorCode = "";
        var activityStatus = "";
        var deviceNickName = "";
        var activityFlexValues1 = "Cancel";
        
        var activityFlexValues2 = formatDateFT(gblFTViewStartOnDateMB);
        var activityFlexValues3 = gblFTViewRepeatAsValMB;
        var activityFlexValues4 = gblTransfrAmtMB;
        var activityFlexValues5 = gblpmtMethodFTMB;
        
        if(activityFlexValues5 == ""){
            activityFlexValues5 = "Own";
        }
        
        
        var logLinkageId = "";
        activityStatus = "00";

        if (resulttable["opstatus"] == 0) {
            if (resulttable["statusCode"] == 0) {
                activityStatus = "00"

                var mbStatus = resulttable["mbFlowStatusIdRs"];
                var SPAuserStatus = resulttable["ibUserStatusIdTr"]
                 if (flowSpa) 
					{
						if (SPAuserStatus == "04") {
                		gblIBFlowStatus = "04";
                		dismissLoadingScreen();
                		showAlert(kony.i18n.getLocalizedString("Receipent_OTPLocked"), kony.i18n.getLocalizedString("info"));
               			 return false;
               			}
               			else 
                		gblIBFlowStatus = SPAuserStatus;
               		 }
                else
              	{  
               		 	if (mbStatus == gblMBStatus) {
                  	 	 dismissLoadingScreen(); //gblFinancialTxnIBLock  
                   	 //alert("User Channel Status is Locked");
                   		 alert(kony.i18n.getLocalizedString("Error_UserStatusLocked"));
                    		return false;
                		}
              }
                dismissLoadingScreen();
                if(flowSpa) {
					popupFTdel.containerHeight = constants.HEIGHT_BY_DEVICE_REFERENCE;
				}else if (gblDeviceInfo.name == "android") {
					popupFTdel.containerHeight = 31;//28;
				}else{
					popupFTdel.containerHeight = 30;
//					kony_6.0 popUp issue fix
					//popupFTdel.containerHeightReference = constants.HEIGHT_BY_DEVICE_REFERENCE
				}
				//popupFTdel.containerHeightReference = constants.HEIGHT_BY_DEVICE_REFERENCE
                
                popupFTdel.show();

            } else {
                dismissLoadingScreen();
                activityStatus = "02";
                alert(resulttable["StatusDesc"]);
            }

        } else {
            dismissLoadingScreen();
            activityStatus = "02";
            alert(resulttable["errMsg"]);
        }
        activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1, activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId);

    }
}



function saveAsPDFFutureTransferMB(filetype) {
//	showLoadingScreen();
    var inputParam = {}
    inputParam["templatename"] = "FutureTransferComplete";
    inputParam["filetype"] = filetype;
    var amt = frmMBFTEditCmplete.lblFTViewAmountVal.text;
    //amt = replaceCommon(amt, ",", "");
 //   var len = amt.length;
    amt = amt.substring("0", amt.length - 1).trim();
    //amt = parseFloat(amt.toString());

    var fee = frmMBFTEditCmplete.lblFeeVal.text
    //var lenfee = fee.length;
    fee = fee.substring("0", fee.length - 1).trim();

/*
    if(frmIBFTrnsrEditCnfmtn.lblNoteToRecipientVal.text == "-"){
     inputParam["noteToRecipient"] = "";
    }else
       inputParam["noteToRecipient"] = frmIBFTrnsrEditCnfmtn.lblNoteToRecipientVal.text;
       
    */
    var notifyVia = "";
    var notifyThru = frmMBFTEditCmplete.lblNotifyRecipientVal.text
    if (notifyThru == "-") {
        notifyVia = "";
    } else {
        if (notifyThru.indexOf("@", 0) != -1) {
            notifyVia = "Email: " + notifyThru;
        } else notifyVia = "SMS: " + notifyThru;
    }

    var transferOrderDate = frmMBFTEditCmplete.lblOrderDateVal.text
    transferOrderDate = transferOrderDate.split(" ")[0];

    var transferSchedule = "";
    if (frmMBFTEditCmplete.lblRepeatAsVal.text == kony.i18n.getLocalizedString("keyOnce")) {
        transferSchedule = frmMBFTEditCmplete.lblViewStartOnDateVal.text + " " + " Repeat " + frmMBFTEditCmplete.lblRepeatAsVal.text;
    } else if (frmMBFTEditCmplete.lblExcuteVal.text == "-") {
        transferSchedule = frmMBFTEditCmplete.lblViewStartOnDateVal.text + " " + " Repeat " + frmMBFTEditCmplete.lblRepeatAsVal.text;
    } else {
        transferSchedule = frmMBFTEditCmplete.lblViewStartOnDateVal.text + " to " + frmMBFTEditCmplete.lblEndOnDateVal.text + " " + " Repeat " + frmMBFTEditCmplete.lblRepeatAsVal.text + " for " + frmMBFTEditCmplete.lblExcuteVal.text + " times ";
    }

	var toAccntName = frmMBFTEditCmplete.lblToAccntNickName.text;
	if(frmMBFTView.lblToAccntName.isVisible && frmMBFTView.lblToAccntName.text != ""){
		toAccntName = frmMBFTEditCmplete.lblToAccntName.text;
	}

	var fromAcctNo =  frmMBFTEditCmplete.lblFrmAccntNum.text;
	if(fromAcctNo.indexOf("-") != -1)fromAcctNo = replaceCommon(fromAcctNo, "-", "");
	var len = fromAcctNo.length;
	fromAcctNo = "XXX-X-"+fromAcctNo.substring(len-6, len-1)+"-X";
	
	var toAcctNo = frmMBFTEditCmplete.lblToAccntNum.text;
	if(toAcctNo.indexOf("-") != -1)toAcctNo = replaceCommon(toAcctNo, "-", "");
	var len1 = toAcctNo.length;
	if(len1==10){
		toAcctNo = "XXX-X-"+toAcctNo.substring(len1-6, len1-1)+"-X";
	}else
		toAcctNo = frmMBFTEditCmplete.lblToAccntNum.text 

    var pdfImagedata = {
        "localeId": kony.i18n.getCurrentLocale(),
        "fromAcctNo": fromAcctNo,
        "fromAcctName": frmMBFTEditCmplete.lblFrmAccntName.text,
        "toAcctNo": toAcctNo,
        "toAcctName": toAccntName,
        "bankName": frmMBFTEditCmplete.lblBankNameVal.text,
        "transferAmount": amt,
        "transferFee": fee,
        "myNote": frmMBFTEditCmplete.lblMyNoteVal.text,
        "noteToRecipient": frmMBFTEditCmplete.lblNoteToRecipientVal.text,
        "notifyVia": notifyVia,
        "transactionRefNo": frmMBFTEditCmplete.lblScheduleRefNoVal.text,
        "transferOrderDate": transferOrderDate,
        "transferSchedule": transferSchedule

    };

    inputParam["datajson"] = JSON.stringify(pdfImagedata, "", "");
    inputParam["outputtemplatename"] = "Future_Transfer_Set_Details_"+kony.os.date("ddmmyyyy");
    
    // var url = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext;
    //  var url = "http://10.20.6.55:8080/middleware/GenericPdfImageServlet";
    var url = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/GenericPdfImageServlet";
    // kony.net.invokeServiceAsync(url, inputParam, callbackfunctionForPDFImageFTMB);
   
   // invokeServiceSecureAsync("generatePdfImage", inputParam, mbRenderFileCallbackfunction);
	saveFuturePDF(filetype, "060", frmMBFTEditCmplete.lblScheduleRefNoVal.text);

}


function srvHolydayChkMB(date){
 var inputParam = {}
 inputParam["date"] = date;  //date formate dd-mm-yyyy;   //"29-AUG-13";
 invokeServiceSecureAsync("getHolydayFlag", inputParam, srvHolydayChkcallBackMB)

}

function srvHolydayChkcallBackMB(status,resulttable){
	
    if (status == 400) {
        
        
        if (resulttable["opstatus"] == 0) {

				var accountFound = resulttable["statusCode"];
				if(resulttable["statusCode"].trim() == "1"){
					
					alert(kony.i18n.getLocalizedString("keyFTSMARTOnHoliday"));
					//alert("Your selected transfer date is on Bank Holiday. Money will be transferred on next working day. If it 's not your preferred date,please reset");
					return false;							
				}else{
					 
				 	frmMBFTEdit.show();
				
				}

        } else {
            dismissLoadingScreen();
            alert("getHolydayFlag service Not returning opstatus 0 ");

        }

    } else if (status == 300) {
        dismissLoadingScreen();
        

    }
	
}

function srvVerifyPasswordExFT_CS_MB(pwd){

	showLoadingScreen();
	var inputParam = {}
    //VerifyPassword
	var transferDate = frmMBFTEditCnfrmtn.lblViewStartOnDateVal.text;
	transferDate = changeDateFormatForService(transferDate);
	inputParam["flowspa"] = false;
	inputParam["gblEditFTSchdule"] = gblEditFTSchduleMB+"";
	inputParam["ClientDt"] = gblOrderDateFTMB;
	inputParam["channel"] = "rc";
    //inputParam["isTMB"] = "true";
   
 //SPA Changes 
 
   if(flowSpa){
   		inputParam["flowspa"] = "true";
   		inputParam["channel"] = "wap";
		if(gblTokenSwitchFlag){
		  	
		  inputParam["gblTokenSwitchFlag"] = "true";
	      inputParam["verifyTokenEx_retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
	      inputParam["verifyTokenEx_retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
	      inputParam["verifyTokenEx_userId"] = gblUserName;
	      inputParam["verifyTokenEx_password"] = pwd;
	      inputParam["verifyTokenEx_channel"] = "InterNet Banking";
	
		}else{
			
			inputParam["gblTokenSwitchFlag"] = "false";
			inputParam["verifyPasswordEx_retryCounterVerifyOTP"] = gblRetryCountRequestOTP;
			inputParam["verifyPasswordEx_password"] = pwd;
			inputParam["verifyPasswordEx_userId"] = gblUserName;
		}
   }else{
   		inputParam["flowspa"] = "false";
   
   }
//  -- SPA CHANGES  
    inputParam["verifyPassword_retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
    inputParam["verifyPassword_retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
    inputParam["verifyPassword_password"] = pwd;
    
    var transferAmt = frmMBFTEditCnfrmtn.lblFTViewAmountVal.text;  
    var len = transferAmt.length;
    transferAmt = transferAmt.substring("0", len - 1);
    transferAmt = transferAmt.trim();
    
     if (transferAmt.indexOf(",") != -1) {
        transferAmt = parseFloat(replaceCommon(transferAmt, ",", "")).toFixed(2);
     }else
    	transferAmt = parseFloat(transferAmt).toFixed(2);

   /*
    transferAmt = parseFloat(transferAmt.toString());
    transferAmt = "" + transferAmt;
    if (transferAmt.indexOf(".") != -1) {
        var tmp = transferAmt.split(".", 2);
        if (tmp[1].length == 1) {
            transferAmt = transferAmt + "0";
        } else if (tmp[1].length > 2) {
            tmp[1] = tmp[1].substring("0", "2")
            transferAmt = ""
            transferAmt = tmp[0] + "." + tmp[1];
        }
    }*/
    inputParam["Amt"] = transferAmt;
    
//doPmtMod

    inputParam["doPmtMod_rqUID"] = "";
   // inputParam["doPmtMod_Amt"] = transferAmt; // HAve to get after fee cal
    var tempDate = frmMBFTEditCnfrmtn.lblViewStartOnDateVal.text;
    tempDate = hypnFormatDateFT(tempDate)
    inputParam["doPmtMod_Starton"] = tempDate;
    inputParam["doPmtMod_channelId"] = "MB";
//doPmtAdd
    
    inputParam["doPmtAdd_toAccTType"] = gblToAccntTypeMB; // To Biller Type ((from PaymentInq)
	inputParam["doPmtAdd_toNickName"] = gblToAccntNickMB; //frmMBFTEditCmplete.lblToAccntNickName.text;		// CR Change

    inputParam["doPmtAdd_dueDate"] = changeDateFormatForService(frmMBFTEditCnfrmtn.lblViewStartOnDateVal.text); // Start On Date
   // inputParam["doPmtAdd_transCode"] = gblTransCodeFTMB; //TransCode (from PaymentInq) // // Have to chek for FT
    inputParam["doPmtAdd_memo"] = frmMBFTEditCnfrmtn.lblMyNoteVal.text; // My Note 
    if (gblScheduleEndFTMB == kony.i18n.getLocalizedString("keyAfter")) {
        inputParam["doPmtAdd_NumInsts"] = frmMBFTEditCnfrmtn.lblExcuteVal.text; // Execution times 
    }
    if (gblScheduleEndFTMB == kony.i18n.getLocalizedString("keyOnDate")) {
        inputParam["doPmtAdd_FinalDueDt"] = changeDateFormatForService(frmMBFTEditCnfrmtn.lblEndOnDateVal.text); // End On Date
    }
    var frequency = frmMBFTEditCnfrmtn.lblRepeatAsVal.text;

    if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyWeekly"))) {
        inputParam["dayOfWeek"] = "";
    }
    if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyMonthly"))) {
        inputParam["dayOfMonth"] = "";
    }
    if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyYearly"))) {
        frequency = "Annually";
        inputParam["dayofMonth"] = "";
        inputParam["monthofYear"] = "";
    }
    inputParam["doPmtAdd_freq"] = frequency;

    if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Daily"))) {
        inputParam["doPmtAdd_freq"] = "Daily";
    }
    if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Weekly"))) {
        inputParam["doPmtAdd_freq"] = "Weekly";
    }
    if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Monthly"))) {
        inputParam["doPmtAdd_freq"] = "Monthly";
    }
    if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Yearly")) || kony.string.equalsIgnoreCase(frequency, "Yearly")) {
        inputParam["doPmtAdd_freq"] = "Annually";
    }
    if (kony.string.equalsIgnoreCase(frequency, "Once")) {
        inputParam["doPmtAdd_freq"] = "once";
    }
    
    if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyOnce"))) {
        inputParam["doPmtAdd_freq"] = "once";
    }
    
    inputParam["doPmtAdd_channelId"] = "MB";
	inputParam["doPmtAdd_toBankId"] = gblToBankCdFTMB;
    
    var pmtMethod = gblpmtMethodFTMB;
    if(gblpmtMethodFTMB == "SMART" || gblpmtMethodFTMB == "ORFT" ){
		if(gblNotificationTypeMB == "Email" && frmMBFTEditCnfrmtn.lblNotifyRecipientVal.text != "-"){
			inputParam["doPmtAdd_notificationType"] = "1";
			inputParam["doPmtAdd_recipientEmailAddr"] = frmMBFTEditCnfrmtn.lblNotifyRecipientVal.text;
			inputParam["doPmtAdd_receipientNote"] = frmMBFTEditCnfrmtn.lblNoteToRecipientVal.text;
		}else if(gblNotificationTypeMB == "SMS" && frmMBFTEditCnfrmtn.lblNotifyRecipientVal.text != "-"){
			inputParam["doPmtAdd_notificationType"] = "0";
			inputParam["doPmtAdd_recipientMobileNbr"] = frmMBFTEditCnfrmtn.lblNotifyRecipientVal.text;
			inputParam["doPmtAdd_receipientNote"] = frmMBFTEditCnfrmtn.lblNoteToRecipientVal.text;
		}
	}
	if(gblpmtMethodFTMB == "ORFT"){
		inputParam["doPmtAdd_PmtMiscType"] = "MOBILE";
		inputParam["doPmtAdd_MiscText"] = gblPHONENUMBER;
	}
	
//NotificationAdd

	var deliveryMethod;
    var phoneNo = gblPHONENUMBER;

    //var emailID = gblEmailId;
    var BANKREF = frmMBFTEditCnfrmtn.lblBankNameVal.text;
    var notifyThru = frmMBFTEditCnfrmtn.lblNotifyRecipientVal.text;
    
    
    if(frmMBFTEditCnfrmtn.hbxNotifyReceipent.isVisible && notifyThru != "-" ){
    	if( notifyThru.indexOf("@") != -1  ){
    		inputParam["NotificationAdd_recipientEmail"] = notifyThru;
			inputParam["NotificationAdd_recipientMobile"] = "-";
    	}else{
    		inputParam["NotificationAdd_recipientEmail"] = "-";
			inputParam["NotificationAdd_recipientMobile"] = "xxx-xxx-"+notifyThru.substr(6, 4);;
    	}
    }else{
     	inputParam["NotificationAdd_recipientEmail"] = "-";
	    inputParam["NotificationAdd_recipientMobile"] = "-";
    }
    
    if(frmMBFTEditCnfrmtn.hbxNoteToReceipent.isVisible){
    	inputParam["NotificationAdd_recipientMessage"] = frmMBFTEditCnfrmtn.lblNoteToRecipientVal.text;
    }else
    	inputParam["NotificationAdd_recipientMessage"] = "-";
    
    
    inputParam["NotificationAdd_phoneNumber"] = phoneNo;
    inputParam["NotificationAdd_phoneNo"] = phoneNo;
    inputParam["NotificationAdd_custName"] = gblCustomerName; //frmMBFTEditCmplete.lblFrmAccntNickName.text;
    inputParam["NotificationAdd_frmAcctName"] = frmMBFTEditCnfrmtn.lblFrmAccntName.text;
    inputParam["NotificationAdd_toAcctName"] = frmMBFTEditCnfrmtn.lblToAccntName.text;
    inputParam["NotificationAdd_toAcctBank"] = BANKREF;
    inputParam["NotificationAdd_Locale"] = kony.i18n.getCurrentLocale();
    inputParam["NotificationAdd_transferAmt"] = transferAmt; //frmMBFTEditCnfrmtn.lblFTViewAmountVal.text;
    inputParam["NotificationAdd_memo"] = frmMBFTEditCnfrmtn.lblMyNoteVal.text;
    inputParam["NotificationAdd_channelID"] = "MB";

    var dateTr = frmMBFTEditCnfrmtn.lblViewStartOnDateVal.text;
    var txnRefNo = frmMBFTEditCnfrmtn.lblScheduleRefNoVal.text;
    var fee = frmMBFTEditCnfrmtn.lblFeeVal.text;
    
    if(fee.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht")) != -1)
        	fee = fee.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim();

    
    inputParam["NotificationAdd_transferDate"] = dateTr;
    inputParam["NotificationAdd_transferFee"] = fee;
    inputParam["NotificationAdd_transferOrderDate"] = frmMBFTEditCnfrmtn.lblOrderDateVal.text;
    inputParam["NotificationAdd_recurring"] = frmMBFTEditCnfrmtn.lblRepeatAsVal.text;
    inputParam["NotificationAdd_endDate"] = frmMBFTEditCnfrmtn.lblEndOnDateVal.text;
    
    if(gblpersonalizedNameMB != ""){
    	inputParam["NotificationAdd_RecipentName"] = gblpersonalizedNameMB;
    }else{
    	inputParam["NotificationAdd_RecipentName"] = frmMBFTEditCnfrmtn.lblToAccntNickName.text;
    }
    
    if( gblPersoanlizedIdMB == "" || gblpersonalizedNameMB == "_MyTMB_" ||gblOwnPersonlisedIdFTMB == gblPersoanlizedIdMB){
    	
    	inputParam["NotificationAdd_RecipentName"] = gblCustomerName;
    }
    
 //ActivityLogging
	if(gblEditFTSchduleMB){
		var activityFlexValues2 = formatDateFT(gblFTViewStartOnDateMB) + "+" + frmMBFTEditCnfrmtn.lblViewStartOnDateVal.text; //frmIBTransferNowConfirmation.lblAccountNo.text;
     	var activityFlexValues3 = gblFTViewRepeatAsValMB + "+" + frmMBFTEditCnfrmtn.lblRepeatAsVal.text;
	}else{
		var activityFlexValues2 = formatDateFT(gblFTViewStartOnDateMB);
      	var activityFlexValues3 = gblFTViewRepeatAsValMB;
	}
    
     var tmpAmt = frmMBFTEditCnfrmtn.lblFTViewAmountVal.text;
     var len = tmpAmt.length;
     tmpAmt = tmpAmt.substring("0", len - 1);
     tmpAmt = parseFloat(tmpAmt.toString());
     
     var activityFlexValues4 = gblTransfrAmtMB + "+" + tmpAmt;
     var activityFlexValues5 = gblpmtMethodFTMB;
     if(gblpmtMethodFTMB == "INTERNAL_TRANSFER"){
     	     //activityFlexValues5 = "Own";
	     	if(isTMB)
	            {
	             activityFlexValues5 = "My TMB";
	            }
	            else
	            {
	             activityFlexValues5 = "Other TMB";
	         }
     }
         
        
      inputParam["activityLog_channelId"] ="02";
      
      if(flowSpa)
      	  inputParam["activityLog_channelId"] ="01";
      
      
	  inputParam["activityLog_activityFlexValues2"] =activityFlexValues2;
	  inputParam["activityLog_activityFlexValues3"] =activityFlexValues3;
	  inputParam["activityLog_activityFlexValues4"] = activityFlexValues4;
	  inputParam["activityLog_activityFlexValues5"] =  activityFlexValues5;  
       
      inputParam["TransferDate"] = transferDate; 
      
      
       var transferSchedule = "";
	    if (frmMBFTEditCnfrmtn.lblRepeatAsVal.text == "Once") {
	        transferSchedule = frmMBFTEditCnfrmtn.lblViewStartOnDateVal.text + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + frmMBFTEditCnfrmtn.lblRepeatAsVal.text;
	    } else if (frmMBFTEditCnfrmtn.lblExcuteVal.text == "-") {
	        transferSchedule = frmMBFTEditCnfrmtn.lblViewStartOnDateVal.text + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + frmMBFTEditCnfrmtn.lblRepeatAsVal.text;
	    } else {
	        transferSchedule = frmMBFTEditCnfrmtn.lblViewStartOnDateVal.text + " " + kony.i18n.getLocalizedString("keyTo") + " " + frmMBFTEditCnfrmtn.lblEndOnDateVal.text + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + frmMBFTEditCnfrmtn.lblRepeatAsVal.text + " " + kony.i18n.getLocalizedString("keyFor") + " " + frmMBFTEditCnfrmtn.lblExcuteVal.text + " " + kony.i18n.getLocalizedString("keyTimesIB");
	    }
    
    	inputParam["PaymentSchedule"] = transferSchedule;
    
      invokeServiceSecureAsync("EditFutureTransferCompositeService", inputParam, callBackExcuteFTCompositeMB);  

}

function callBackExcuteFTCompositeMB(status,result){

    dismissLoadingScreen();
//
	if(result["opstatusVPX"] == "0"){
		//popupTractPwd.dismiss();
        //dismissLoadingScreen();
        
        
        if(flowSpa)
        	gblVerifyOTPCounter = "0";
        	gblRetryCountRequestOTP = "0";
            
        if(!flowSpa)     
        		popupTractPwd.dismiss();
        		
        if(result["opstatus"] == "0"){
         	if (gblEditFTSchduleMB == true) {
		   		frmMBFTEditCmplete.lblScheduleRefNoVal.text = result["transRefNum"];
	            frmMBFTEditCmplete.lblOrderDateVal.text = result["currentTime"];
       		 } else {
				frmMBFTEditCmplete.lblScheduleRefNoVal.text = frmMBFTEditCnfrmtn.lblScheduleRefNoVal.text;
	            var initiatedDt = result["InitiatedDt"];
	            frmMBFTEditCmplete.lblOrderDateVal.text = dateFormatForDisplayWithTimeStampFTMB(initiatedDt);
	        }
	         completeProcessMB();
        }else{
        	dismissLoadingScreen();
            alert(result["errMsg"]);
   			return false;
        }
        
	}else{
	
		if(flowSpa){
			dismissLoadingScreen();
			if(result["opstatusVPX"] == "8005"){
				
				if(result["errCode"] == "VrfyOTPErr00001"){
					gblVerifyOTPCounter = result["retryCounterVerifyOTP"];
                	alert("" + kony.i18n.getLocalizedString("invalidOTP"));
                	return false;
				}else if(result["errCode"] == "VrfyOTPErr00002"){
               		//alert("" + kony.i18n.getLocalizedString("ECVrfyOTPErr"));
               		popTransferConfirmOTPLock.show();
	                return false;
				}else if(result["errCode"] == "VrfyOTPErr00005"){
               		alert("" + kony.i18n.getLocalizedString("KeyTokenSerialNumError"));
                	return false;
				}else if(result["errCode"] == "VrfyOTPErr00006"){
                	gblVerifyOTPCounter = result["retryCounterVerifyOTP"];
                	//alert("" + result["errMsg"]);
                	showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
                	return false;
				}else{
					//alert("" + result["errMsg"]);
					showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
                	return false;
				}
			}else{
				dismissLoadingScreen();
            	//alert(" "+result["errMsg"]);
            	showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
			}
				
				
		}else{
			gblRtyCtrVrfyTxPin = result["retryCounterVerifyTransPwd"];
	        gblRtyCtrVrfyAxPin = result["retryCounterVerifyAccessPin"];
	        dismissLoadingScreen();
	        if (result["errCode"] == "VrfyTxPWDErr00003") {
	        	showTranPwdLockedPopup();
	        } else {
	            //alert("" + kony.i18n.getLocalizedString("KeyIncorrectPWD"));
				popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text = "";
				popupTractPwd.hbxPopupTranscPwd.skin = txtErrorBG;
				popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = txtErrorBG;
				popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = txtErrorBG;
				popupTractPwd.lblPopupTract7.skin = lblRedNormal;
				popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("invalidTxnPwd")
	            gblVerifyOTP = gblVerifyOTP + 1;
	            return false;
	        }
		
	    }
      return false;
	}

}



