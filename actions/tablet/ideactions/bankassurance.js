oldBAselectedInd  = 0;
currBAselectedInd = 0;

function callBAPolicyListService(){

	var inputParam = {};
	showLoadingScreenPopup();
	currBAselectedInd = 1;
    invokeServiceSecureAsync("BAGetPolicyList", inputParam, callBAPolicyListServiceCallBack);
}

function callBAPolicyListServiceCallBack(status,resulttable){
	if (status == 400) {
        if (resulttable["opstatus"] == 0) {
        	dismissLoadingScreenPopup();
        	gblEmailId=resulttable["emailAddr"];
        	var segmentData= populateDataBankAssuranceSummary(resulttable);
         
       		frmIBBankAssuranceSummary.segAccountDetails.widgetDataMap ={
						lblHead: "lblHead",
				       	imgLogo:"imgLogo",
						lblPolicyName:"lblPolicyName",
						lblPolicyNumber: "lblPolicyNumber",
						lblSumInsured:"lblSumInsured",
						lblSumInsuredValue:"lblSumInsuredValue",
						imageRightArrow:"imageRightArrow"
				    };
	         frmIBBankAssuranceSummary.segAccountDetails.removeAll();
	       	 frmIBBankAssuranceSummary.segAccountDetails.setData(segmentData);
	         frmIBBankAssuranceSummary.hbxAccntDetails.setVisibility(false);
			 //begin MIB-3313
			 frmIBBankAssuranceSummary.imgProfile.src = frmIBAccntSummary.imgProfile.src;
			 frmIBBankAssuranceSummary.lblUsername.text = frmIBAccntSummary.lblUsername.text;
			 var totalSumInsured = gblAccountTable["totalSumInsured"];
			 if(totalSumInsured != "-") {
				 totalSumInsured = commaFormatted(parseFloat(totalSumInsured).toFixed(2)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
			 }
			 frmIBBankAssuranceSummary.lblInvestmentValue.text = totalSumInsured;
			 //end MIB-3313
	         frmIBBankAssuranceSummary.show();
       	
       	 //details service
        callBAPolicyDetailsService(frmIBBankAssuranceSummary.segAccountDetails.data[1].lblPolicyNumber, frmIBBankAssuranceSummary.segAccountDetails.data[1].imgLogo);
       	
      // 	 frmIBBankAssuranceSummary.lblAccountNameHeader.text = frmIBBankAssuranceSummary.segAccountDetails.data[1].lblfundName;
	  //   frmIBBankAssuranceSummary.lblAccountBalanceHeader.text = frmIBBankAssuranceSummary.segAccountDetails.data[1].lblinvestmentValue;
	  //   frmIBBankAssuranceSummary.imgAccountDetailsPic.src = frmIBBankAssuranceSummary.segAccountDetails.data[1].imgLogo;
			
        } else {
        	dismissLoadingScreenPopup();
	        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
			return false;
        }
     }else{
        dismissLoadingScreenPopup();
     }
}


function populateDataBankAssuranceSummary(resulttable){
		
		var policyHeadersDS = resulttable["policyHeadersDS"];
		var policyDetailsDS = resulttable["policyDetailsDS"];
		var locale= kony.i18n.getCurrentLocale();
		var segmentData = [];
		for(var i=0;i<policyDetailsDS.length;i++){

			var companyName= "";
			var companynamePrev = "";
			var sumInsuredLabel="";
			var policyUnique="";
			var isFlag = "";
			var logo="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext +
						 "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+"BA_"+policyDetailsDS[i]["DATA_SOURCE_VALUE_EN"]+"&modIdentifier=MFLOGOS";
			
				if(policyDetailsDS[i]["ALLOW_PAYMENT_VALUE_EN"] == "1"){
					isFlag = "ico_flag_active.png"
				}
				
				if(locale == "en_US"){
					companyName = policyDetailsDS[i]["COMPANY_NAME_VALUE_EN"];
					sumInsuredLabel = policyHeadersDS[0]["SUM_INSURE_EN"];
					policyUnique = policyDetailsDS[i]["POLICY_UNIQUE_NAME_VALUE_EN"];
					if(i!=0)
						companynamePrev = policyDetailsDS[i-1]["COMPANY_NAME_VALUE_EN"];
				}else{
					companyName = policyDetailsDS[i]["COMPANY_NAME_VALUE_TH"];
					sumInsuredLabel = policyHeadersDS[0]["SUM_INSURE_TH"];
					policyUnique = policyDetailsDS[i]["POLICY_UNIQUE_NAME_VALUE_TH"];
					if(i!=0)
						companynamePrev = policyDetailsDS[i-1]["COMPANY_NAME_VALUE_TH"];
				}
				if(companynamePrev != companyName){
						var dataObjectHeader = {
						lblHead: companyName,
						lblHeadEN: policyDetailsDS[i]["COMPANY_NAME_VALUE_EN"],
						lblHeadTH: policyDetailsDS[i]["COMPANY_NAME_VALUE_TH"],
						template:MFHeader
					};
					segmentData.push(dataObjectHeader);
				}
				
			 var dataObject	= {
								imgLogo:logo,
								imageRightArrow:isFlag,
								companyNameEN : policyDetailsDS[i]["COMPANY_NAME_VALUE_EN"],
								companyNameTH : policyDetailsDS[i]["COMPANY_NAME_VALUE_TH"],
								policyNameEN : policyDetailsDS[i]["POLICY_NAME_VALUE_EN"],
								policyNameTH : policyDetailsDS[i]["POLICY_NAME_VALUE_TH"],
								lblPolicyName : policyUnique,
								lblPolicyNameEN : policyDetailsDS[i]["POLICY_UNIQUE_NAME_VALUE_EN"],
								lblPolicyNameTH : policyDetailsDS[i]["POLICY_UNIQUE_NAME_VALUE_TH"],
								lblPolicyNumber : policyDetailsDS[i]["POLICY_NO_VALUE_EN"],
								lblSumInsured : sumInsuredLabel+":",
								sumInsuredEN : policyHeadersDS[0]["SUM_INSURE_EN"]+":",
								sumInsuredTH : policyHeadersDS[0]["SUM_INSURE_TH"]+":",
								lblSumInsuredValue : commaFormatted(policyDetailsDS[i]["SUM_INSURE_VALUE_EN"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
								allowPayBill: policyDetailsDS[i]["ALLOW_PAYMENT_VALUE_EN"],
								allowTaxDoc: policyDetailsDS[i]["ALLOW_TAX_DOCUMENT_VALUE_EN"],
							    template:hbxBASummary
						    
						}
				
			segmentData.push(dataObject);
		}
		return segmentData;
}


function callBAPolicyDetailsService(policynumber,imgLogo){
	if(!isNotBlank(policynumber)){
		return;
	}
	var inputParam = {};
	showLoadingScreenPopup();
	
	inputParam["policyNumber"] =policynumber;
	frmIBBankAssuranceSummary.imgAccountDetailsPic.src = imgLogo;
	inputParam["dataSet"] = "0";
    invokeServiceSecureAsync("BAGetPolicyDetails", inputParam, callBAPolicyDetailsServiceCallBack);
}

function callBAPolicyDetailsServiceCallBack(status,resulttable){
	if (status == 400) {
        if (resulttable["opstatus"] == 0) {
        	dismissLoadingScreenPopup();
        	gblBAPolicyDetailsResult = resulttable;
        	assignValuesToDetailsSection(resulttable);
        	frmIBBankAssuranceSummary.hbxAccntDetails.setVisibility(true);
        	swapSelectedBASkin();
        }else{
         	dismissLoadingScreenPopup();
        }
     }
 }       
 
function swapSelectedBASkin(){
	var j = 0;
	for (var i=0;i<document.getElementsByTagName("li").length;i++) {
		var tmp = document.getElementsByTagName("li")[i];
		if(undefined != tmp && undefined != tmp.parentNode){
			if(tmp.childNodes[0].id == "hbxBASummary_hbxBASummary"){break;}
			if(tmp.childNodes[0].id != "hbxBASummary_hbxBASummary" && tmp.childNodes[0].id != "MFHeader_MFHeader"){
				if(tmp.childNodes[0].id.indexOf("loadingScreenDiv") == -1){
					j += 1;
				}
			}
		}
	}  
	if(j == 0){
		swapSelectedBASkinSingle();
	} else {
		swapSelectedBASkinMultiple(j);
	}
}

function swapSelectedBASkinSingle(){
	if(null != frmIBBankAssuranceSummary.segAccountDetails.selectedIndex && undefined != frmIBBankAssuranceSummary.segAccountDetails.selectedIndex){
 		currBAselectedInd = frmIBBankAssuranceSummary.segAccountDetails.selectedIndex[1];
 	}
 	if(currBAselectedInd != 0){
		var tempOld = document.getElementsByTagName("li")[oldBAselectedInd];
		if(undefined != tempOld && undefined != tempOld.parentNode){
			if(tempOld.parentNode.parentNode.id == "frmIBBankAssuranceSummary_segAccountDetails"){		
				removeBGColor(tempOld.childNodes[0]);
				removeBGImage(tempOld.childNodes[0]);
				removeClass(tempOld.childNodes[0] ,"segSelectedRow");				
				removeClass(tempOld.childNodes[0] ,"hbxAcctSummarySegItemFocus");		
				tempOld.setAttribute("class","sknsegm");
				if(oldBAselectedInd != 0){
					if(!hasClass(tempOld.childNodes[0] ,"hbxAcctSummarySeg")){
						addClass(tempOld.childNodes[0] ,"hbxAcctSummarySeg");				
					}		
					tempOld.onmouseover = function() { 
						if(oldBAselectedInd != currBAselectedInd ){
							addClass(tempOld.childNodes[0] ,"hbxAcctSummarySegItemFocus");
						} 
					}
				}
			}
		}		
		
		var tempCurr = document.getElementsByTagName("li")[currBAselectedInd];
		if(undefined != tempCurr && undefined != tempCurr.parentNode){
			if(tempCurr.parentNode.parentNode.id == "frmIBBankAssuranceSummary_segAccountDetails"){
				if(hasClass(tempCurr.childNodes[0],"segSelectedRow")){
					removeClass(tempCurr.childNodes[0],"segSelectedRow");	
				}else{	
					if(currBAselectedInd != 0){	
						addClass(tempCurr.childNodes[0],"segSelectedRow");				
						tempCurr.childNodes[0].style.backgroundImage = "url(./desktopweb/images/bg_arrow_right_grayb2.png)";
					}		
				}
					
				tempCurr.onmouseover = function() { 
					removeClass(this ,"hbxAcctSummarySegItemFocus");
					removeClass(this.childNodes[0] ,"hbxAcctSummarySegItemFocus");
					this.childNodes[0].style.backgroundColor = '#e1eff8'; 
					this.childNodes[0].style.backgroundImage = "url(./desktopweb/images/bg_arrow_right_grayb2.png)";
					if(oldBAselectedInd != currBAselectedInd){		
						addClass(this,"segSelectedRow");	
						addClass(this.childNodes[0],"segSelectedRow");	
					} 
				}
			}	
		}
		oldBAselectedInd = currBAselectedInd;
	}
}

function swapSelectedBASkinMultiple(j){
	if(null != frmIBBankAssuranceSummary.segAccountDetails.selectedIndex && undefined != frmIBBankAssuranceSummary.segAccountDetails.selectedIndex){
 		currBAselectedInd = frmIBBankAssuranceSummary.segAccountDetails.selectedIndex[1];
 	}	 
 	if(currBAselectedInd != 0){ 
 		currBAselectedInd += j;	
		oldBAselectedInd  += j;
		var tempCurr = document.getElementsByTagName("li")[currBAselectedInd];
		if(undefined != tempCurr && undefined != tempCurr.parentNode){
			if(tempCurr.parentNode.parentNode.id == "frmIBBankAssuranceSummary_segAccountDetails"){
				if(hasClass(tempCurr.childNodes[0],"segSelectedRow")){
					removeClass(tempCurr.childNodes[0],"segSelectedRow");	
				}else{	
					if(currBAselectedInd != 0){	
						addClass(tempCurr.childNodes[0],"segSelectedRow");	
						tempCurr.childNodes[0].style.backgroundColor = '#e1eff8'; 			
						tempCurr.childNodes[0].style.backgroundImage = "url(./desktopweb/images/bg_arrow_right_grayb2.png)";
					}		
				}
					
				tempCurr.onmouseover = function() { 
					removeClass(this ,"hbxAcctSummarySegItemFocus");
					removeClass(this.childNodes[0] ,"hbxAcctSummarySegItemFocus");
					this.childNodes[0].style.backgroundColor = '#e1eff8'; 
					this.childNodes[0].style.backgroundImage = "url(./desktopweb/images/bg_arrow_right_grayb2.png)";
					if(oldBAselectedInd != currBAselectedInd){		
						addClass(this,"segSelectedRow");	
						addClass(this.childNodes[0],"segSelectedRow");	
					} 
					var c = document.getElementsByTagName("li");
						for(var i = 0; i < c.length; i++){  
							if(i != currBAselectedInd){
								var tmp = document.getElementsByTagName("li")[i];
								removeClass(tmp ,"hbxAcctSummarySegItemFocus");
								removeClass(tmp.childNodes[0] ,"hbxAcctSummarySegItemFocus");
							}
					}	
				}
			}	
		}
		
		var tempOld = document.getElementsByTagName("li")[oldBAselectedInd];
		if(undefined != tempOld && undefined != tempOld.parentNode){
			if(tempOld.parentNode.parentNode.id == "frmIBBankAssuranceSummary_segAccountDetails"){		
			   if(oldBAselectedInd != currBAselectedInd){
					removeBGColor(tempOld.childNodes[0]);
					removeBGImage(tempOld.childNodes[0]);
					removeClass(tempOld.childNodes[0] ,"segSelectedRow");				
					removeClass(tempOld.childNodes[0] ,"hbxAcctSummarySegItemFocus");		
				}
				
				tempOld.setAttribute("class","sknsegm");
				if(oldBAselectedInd != 0){
					if(!hasClass(tempOld.childNodes[0] ,"hbxAcctSummarySeg")){
						addClass(tempOld.childNodes[0] ,"hbxAcctSummarySeg");				
					}		
					tempOld.onmouseover = function() { 
						if(oldBAselectedInd != currBAselectedInd ){
							if(!hasClass(tempOld.childNodes[0] ,"hbxAcctSummarySegItemFocus")){
								addClass(tempOld.childNodes[0] ,"hbxAcctSummarySegItemFocus");
							} 
						}
						var c = document.getElementsByTagName("li");
						for(var i = 0; i < c.length; i++){  
							//if(i != oldBAselectedInd){
								var tmp = document.getElementsByTagName("li")[i];
								removeClass(tmp ,"hbxAcctSummarySegItemFocus");
								removeClass(tmp.childNodes[0] ,"hbxAcctSummarySegItemFocus");
							//}
						}
					}
					
				}
			}
		}		
		
		oldBAselectedInd = currBAselectedInd -j;
	}
}

 
 function assignValuesToDetailsSection(resulttable){
	var locale = kony.i18n.getCurrentLocale();
	var languageExtension = "EN";
	if(locale == "en_US"){
		languageExtension = "EN";
	}else{
		languageExtension = "TH";
	}
	 
	var policyDetailsValueDS = resulttable["policyDetailsValueDS"];
	var premiumPaymentValueDS = resulttable["premiumPaymentValueDS"];
	var coverageValueDS = resulttable["coverageValueDS"];
	var cashbackValueDS = resulttable["cashbackValueDS"];
	var billPaymentValueDS = resulttable["billPaymentValueDS"];
	
	removeWidgetsFromContainer();
	if(policyDetailsValueDS.length > 0){
		addHeaderToSesction("policyDetails",kony.i18n.getLocalizedString("BA_lbl_Policy_details"));
	}
			
			
	frmIBBankAssuranceSummary.hbxOnHandClick.setVisibility(true);
    if(policyDetailsValueDS.length > 0 && policyDetailsValueDS[0]["value_EN"] == "1" && billPaymentValueDS.length>0){
		frmIBBankAssuranceSummary.btnPayBill.setVisibility(true);
		frmIBBankAssuranceSummary.hbxOnHandClick.setVisibility(true);
	}else{
		frmIBBankAssuranceSummary.btnPayBill.setVisibility(false);
	}
		
	if(policyDetailsValueDS[1]["value_EN"] == "1"){
		frmIBBankAssuranceSummary.BtnTaxDoc.setVisibility(true);
		frmIBBankAssuranceSummary.hbxOnHandClick.setVisibility(true);
	}else{
		frmIBBankAssuranceSummary.BtnTaxDoc.setVisibility(false);
	}
	
	if(!frmIBBankAssuranceSummary.BtnTaxDoc.isVisible && !frmIBBankAssuranceSummary.btnPayBill.isVisible){
		frmIBBankAssuranceSummary.hbxOnHandClick.setVisibility(false);
	}
    			
	frmIBBankAssuranceSummary.lblPolicyName.text = policyDetailsValueDS[3]["value_"+languageExtension];
	frmIBBankAssuranceSummary.lblPolicyNo.text = policyDetailsValueDS[2]["displayName_"+languageExtension]+":";
	frmIBBankAssuranceSummary.lblPolicyNoValue.text = policyDetailsValueDS[2]["value_"+languageExtension];
	frmIBBankAssuranceSummary.lblCompany.text = policyDetailsValueDS[7]["displayName_"+languageExtension]+":";
	frmIBBankAssuranceSummary.lblCompanyValue.text = policyDetailsValueDS[7]["value_"+languageExtension];
	frmIBBankAssuranceSummary.lblStatus.text = policyDetailsValueDS[6]["displayName_"+languageExtension]+":";
	frmIBBankAssuranceSummary.lblStatusValue.text = policyDetailsValueDS[6]["value_"+languageExtension];
	var counter = 0;
	for(var i=9;i<policyDetailsValueDS.length;i++){
		addwidgetsforDeatils("policyDetails",policyDetailsValueDS[i]["displayName_"+languageExtension], policyDetailsValueDS[i]["value_"+languageExtension], counter);
		if(isNotBlank(policyDetailsValueDS[i]["displayName_"+languageExtension]) && isNotBlank(policyDetailsValueDS[i]["value_"+languageExtension])){
			counter++;
		}
	}
	
	counter = 0;
	if(coverageValueDS.length > 0)
	    addHeaderToSesction("coverageDetails", kony.i18n.getLocalizedString("BA_lbl_Coverage_info"));
	for(var i=0;i<coverageValueDS.length;i++){
		addwidgetsforDeatils("coverageDetails",coverageValueDS[i]["displayName_"+languageExtension], coverageValueDS[i]["value_"+languageExtension], counter);
		if(isNotBlank(coverageValueDS[i]["displayName_"+languageExtension]) && isNotBlank(coverageValueDS[i]["value_"+languageExtension])){
			counter++;
		}
	}
	counter = 0;
	if(cashbackValueDS.length > 0)
	    addHeaderToSesction("cashbackDetails", kony.i18n.getLocalizedString("BA_lbl_Cash_back_dividend"));
	for(var i=0;i<cashbackValueDS.length;i++){
		addwidgetsforDeatils("cashbackDetails",cashbackValueDS[i]["displayName_"+languageExtension], cashbackValueDS[i]["value_"+languageExtension],counter);
		if(isNotBlank(cashbackValueDS[i]["displayName_"+languageExtension]) && isNotBlank(cashbackValueDS[i]["value_"+languageExtension])){
			counter++;
		}
	}
	
	counter = 0;
	if(premiumPaymentValueDS.length > 0)
		addHeaderToSesction("premiumPayment", kony.i18n.getLocalizedString("BA_lbl_Premium_payment_info"));
	for(var i=0;i<premiumPaymentValueDS.length;i++){
		addwidgetsforDeatils("premiumPayment",premiumPaymentValueDS[i]["displayName_"+languageExtension], premiumPaymentValueDS[i]["value_"+languageExtension],counter);
		if(isNotBlank(premiumPaymentValueDS[i]["displayName_"+languageExtension]) && isNotBlank(premiumPaymentValueDS[i]["value_"+languageExtension])){
			counter++;
		}
	}
 }
 
 function frmIBBankAssuranceSummaryPreShow(){
	var locale = kony.i18n.getCurrentLocale();
	if (locale == "en_US"){		      
      	frmIBBankAssuranceSummary.lblUsername.text = gblCustomerName;		      
    }else{
  		frmIBBankAssuranceSummary.lblUsername.text = gblCustomerNameTh;	
  	}
	// BA Summary
	frmIBBankAssuranceSummary.label449290336722095.text = kony.i18n.getLocalizedString("BA_Acc_Summary_Title");
	frmIBBankAssuranceSummary.lblnvestment.text = kony.i18n.getLocalizedString("BA_lbl_Total_Sum_Insured");
	frmIBBankAssuranceSummary.segAccountDetails.setData(updateLocaleDataSegBankAssurance(frmIBBankAssuranceSummary.segAccountDetails.data));
	
	// BA Details
	frmIBBankAssuranceSummary.BtnTaxDoc.text = kony.i18n.getLocalizedString("BA_Tax_doc");
	frmIBBankAssuranceSummary.btnPayBill.text =  kony.i18n.getLocalizedString("PayBill");;
	frmIBBankAssuranceSummary.label449290336675636.text = kony.i18n.getLocalizedString("BA_lbl_Policy_details");
	
	assignValuesToDetailsSection(gblBAPolicyDetailsResult);
}


function updateLocaleDataSegBankAssurance(segData){
	if(segData != null){
		var locale = kony.i18n.getCurrentLocale();
		for (var i = 0; i < segData.length; i++) {
        	if(locale == "en_US"){
    			if(segData[i].template == MFHeader){
    				segData[i].lblHead = segData[i].lblHeadEN;
    			}else{
    				segData[i].lblPolicyName = segData[i].lblPolicyNameEN;
    				segData[i].lblSumInsured = segData[i].sumInsuredEN;
    			}
        	}else{
    			if(segData[i].template == MFHeader){
    				segData[i].lblHead = segData[i].lblHeadTH;
    			}else{
    				segData[i].lblPolicyName = segData[i].lblPolicyNameTH;
    				segData[i].lblSumInsured = segData[i].sumInsuredTH;
    			}
        	}
	   	}
	}
   	return segData;	
}
function removeWidgetsFromContainer(){
	frmIBBankAssuranceSummary.vbxDynamicDetails.removeFromParent();
	
	 var vbxDynamicDetails = new kony.ui.Box({
        "id": "vbxDynamicDetails",
        "isVisible": true,
        "orientation": constants.BOX_LAYOUT_VERTICAL
    }, {
        "containerWeight": 100,
        "margin": [0, 0, 0, 0],
        "padding": [0, 0, 0, 0],
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "vExpand": false,
        "hExpand": true,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {});
    vbxDynamicDetails.add();
	        	
    frmIBBankAssuranceSummary.hbxDynamicDetails.add(
    vbxDynamicDetails);
}

function addwidgetsforDeatils(dataSetName,displayName,value,number){
		if(!isNotBlank(value) || !isNotBlank(displayName)){
			return;
		}
		var hboxSkin = "";
		if(number%2 == 0){
			hboxSkin =	"hboxLightGrey";
		}else{
			hboxSkin =	"hboxWhite";
		}
	
		 var lblUseful = new kony.ui.Label({
        "id": dataSetName+"lblUseful"+number,
        "isVisible": true,
        "text": displayName+":",
        "skin": "lblIB18pxBlackNoMed"
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 50
    }, {
        "toolTip": null
    });
    var lblUsefulValue = new kony.ui.Label({
        "id": dataSetName+"lblUsefulValue"+number,
        "isVisible": true,
        "text": value,
        "skin": "lblIB18pxBlackNoMed"
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_RIGHT,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 50
    }, {
        "toolTip": null
    });
    var hbxUseful = new kony.ui.Box({
        "id": dataSetName+"hbxUseful"+number,
        "isVisible": true,
        "position": constants.BOX_POSITION_AS_NORMAL,
        "skin": hboxSkin,
        "orientation": constants.BOX_LAYOUT_HORIZONTAL
    }, {
        "containerWeight": 5,
        "percent": true,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
        "margin": [0, 0, 0, 0],
        "padding": [3, 0, 3, 0],
        "vExpand": false,
        "marginInPixel": false,
        "paddingInPixel": false,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {});
    hbxUseful.add(
    lblUseful, lblUsefulValue);
    frmIBBankAssuranceSummary.vbxDynamicDetails.add(hbxUseful);

}

function addHeaderToSesction(headerName,headerText){
	var lblHeader1 = new kony.ui.Label({
	        "id": "lblHeader"+headerName,
	        "isVisible": true,
	        "text": headerText,
	        "skin": "lblIB18pxBlackBold"
	    }, {
	        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
	        "vExpand": false,
	        "hExpand": true,
	        "margin": [2, 5, 0, 2],
	        "padding": [0, 0, 0, 0],
	        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
	        "marginInPixel": false,
	        "paddingInPixel": false,
	        "containerWeight": 98
	    }, {
	        "toolTip": null
	    });
	   frmIBBankAssuranceSummary.vbxDynamicDetails.add(lblHeader1);
}

function showBATaxDocEmailConfirmPopupIB(){
	if(emailValidatn(gblEmailId)){
		fbDlink=false;
		var message=kony.i18n.getLocalizedString("keyBATaxDocReqConfirmation");
		popupIBLogout.label44747680938085.text=message.replace("[emailId]", gblEmailId);
		popupIBLogout.show();
	}else{
		showAlert(kony.i18n.getLocalizedString("keyEmailInCorrect"), kony.i18n.getLocalizedString("info"));
	}	
}

function callBARequestTaxDocService(){
	popupIBLogout.dismiss();
	var locale = kony.i18n.getCurrentLocale();
	var languageExtension = "EN";
	if(locale == "en_US"){
		languageExtension = "EN";
	}else{
		languageExtension = "TH";
	}

	var policynumber = gblBAPolicyDetailsResult["policyDetailsValueDS"][2]["value_"+languageExtension];
	var inputParam = {};
	showLoadingScreenPopup();
	inputParam["policyNumber"] = policynumber;
	inputParam["policyName"] = gblBAPolicyDetailsResult["policyDetailsValueDS"][3]["value_EN"];
	
    invokeServiceSecureAsync("BAReqTaxDocument", inputParam, callBARequestTaxDocServiceCallBack);
}

function callBARequestTaxDocServiceCallBack(status,resulttable){
	if (status == 400) {
        if (resulttable["opstatus"] == 0 && resulttable["StatusCode"] == "0000") {
        	dismissLoadingScreenPopup();
        	showAlert(newLineTextIB(kony.i18n.getLocalizedString("keyBATaxDocReqSuccess")), kony.i18n.getLocalizedString("info"));
        	
        }else{
        	dismissLoadingScreenPopup();
        	if(resulttable["StatusCode"] == "BA3031"){
        		showAlert(newLineTextIB(kony.i18n.getLocalizedString("keyBATaxDocReqAlready")), kony.i18n.getLocalizedString("info"));
        	}else{
        		showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        	}
        }
     }
 }