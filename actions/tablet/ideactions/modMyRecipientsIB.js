function executeRecipient(otptext) {
	var inputParams = {};
	var gblTransactionType="0";
	inputParams["gblTokenSwitchFlag"]=gblTokenSwitchFlag;
	inputParams["password"]=otptext;
	inputParams["retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
    inputParams["retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
    inputParams["retryCounterVerifyOTP"]=gblVerifyOTPCounter;
    //added for fb-image
	if(frmIBMyReceipentsAddContactManually.image210107168135.src!=null&&frmIBMyReceipentsAddContactManually.image210107168135.src!=undefined){
    beforeOTPIB= frmIBMyReceipentsAddContactManually.image210107168135.src
	if(beforeOTPIB.indexOf("fbcdn")>=0)
	tempImg=frmIBMyReceipentsAddContactManually.image210107168135.src
	else
	tempImg="png"
	}
	if (gblAdd_Receipent_State != gblEXISTING_RC_EDITION) {
		if (gblAdd_Receipent_State == gblNEW_RC_ADDITION && (frmIBMyReceipentsAddContactManually.segmentRcAccounts.data == null || frmIBMyReceipentsAddContactManually.segmentRcAccounts.data == "")) {
			//var totalData = [];
			//var RcData = [frmIBMyReceipentsAddContactManually.lblrecipientname.text, tempImg, frmIBMyReceipentsAddContactManually.lblemail.text,
			//frmIBMyReceipentsAddContactManually.txtmobnum.text, frmIBMyReceipentsAddContactManually.lblfbidstudio19.text, "Added","N"];//Modified by Studio Viz
			//totalData.push(RcData);
			// ***** Adding below code  to resolve Mobile Fabric issue.
			var totalData = frmIBMyReceipentsAddContactManually.lblrecipientname.text+ ","  + tempImg+ ","  + frmIBMyReceipentsAddContactManually.lblemail.text+ ","  +
					frmIBMyReceipentsAddContactManually.txtmobnum.text+ ","  + frmIBMyReceipentsAddContactManually.lblfbidstudio19.text+ ","  + "Added"+ ","  +"N";                                   //Modified by Studio Viz
			inputParams["receipentList"]= totalData;
			inputParams["gblTransactionType"]="1";	
						
				//startNewOnlyRecipientAddService();
		} else {
		       	if (gblAdd_Receipent_State == gblNEW_RC_ADDITION_PRECONFIRM || gblAdd_Receipent_State == gblNEW_RC_ADDITION){
					if (parseInt(formId.toString()) == 3) {
						inputParams["receipentList"]= prepareNewRcFBList()
						inputParams["gblTransactionType"]="1";	
							
					}else{	
//						var totalData = [];
//						var RcData = [frmIBMyReceipentsAddContactManually.lblrecipientname.text, tempImg, frmIBMyReceipentsAddContactManually.lblemail.text,
//						frmIBMyReceipentsAddContactManually.txtmobnum.text, frmIBMyReceipentsAddContactManually.lblfbidstudio19.text, "Added","N"];//Modified by Studio Viz
//						totalData.push(RcData);
                        var totalData = frmIBMyReceipentsAddContactManually.lblrecipientname.text+ ","  + tempImg+ ","  + frmIBMyReceipentsAddContactManually.lblemail.text+ ","  +
						 frmIBMyReceipentsAddContactManually.txtmobnum.text+ ","  + frmIBMyReceipentsAddContactManually.lblfbidstudio19.text+ ","  + "Added"+ ","  +"N";					//Modified by Studio Viz
						inputParams["receipentList"]= totalData;
						inputParams["personalizedAccList"]=prepareNewAccountListNewRc("");
						inputParams["gblTransactionType"]="3";
						inputParams["oldRcName"]=frmIBMyReceipentsAddContactManually.lblrecipientname.text;
						inputParams["oldRcEmail"]=frmIBMyReceipentsAddContactManually.lblemail.text;
						inputParams["oldRcmobile"]=removeHyphenIB(frmIBMyReceipentsAddContactManually.txtmobnum.text);
						inputParams["oldFbId"]=frmIBMyReceipentsAddContactManually.lblfbidstudio19.text;	//Modified by Studio Viz
							
						//startNewRecipientPlusAccountAddService();
					  }				
				}else if (gblAdd_Receipent_State == gblEXISTING_RC_BANKACCNT_ADDITION) {
					if (frmIBMyReceipentsAddBankAccnt.segementTobeAddedAccnts.data.length == 0) {
						
					} else {
					    gblRefreshRcCache = true;
						inputParams["personalizedAccList"]=prepareNewAccountListExistingRc()
						inputParams["gblTransactionType"]="4";
						inputParams["oldRcName"]=frmIBMyReceipentsAccounts.lblRcName.text;
						inputParams["oldRcmobile"]=removeHyphenIB(frmIBMyReceipentsAccounts.lblRcMobileNo.text);
						inputParams["oldRcEmail"]=frmIBMyReceipentsAccounts.lblRcEmail.text;
						inputParams["oldFbId"]=frmIBMyReceipentsAccounts.lblRcFbId.text;
							
						//startExistingRcAccountAddService();
					}
				}else {
					//startExistingRcAccountEditService();
				}
			}
		} else {
				inputParams["personalizedId"]= gblselectedRcId;
				inputParams["personalizedName"]=frmIBMyReceipentsAddContactManually.lblrecipientname.text;
				inputParams["personalizedPictureId"]= frmIBMyReceipentsAddContactManually.image210107168135.src;
				inputParams["personalizedMailId"]=frmIBMyReceipentsAddContactManually.lblemail.text;
				inputParams["personalizedMobileNumber"]= removeHyphenIB(frmIBMyReceipentsAddContactManually.txtmobnum.text);
				inputParams["personalizedFacebookId"]=frmIBMyReceipentsAddContactManually.lblfbidstudio19.text;//Modified by Studio Viz
				inputParams["personalizedStatus"]="Added";
				inputParams["gblTransactionType"]="2";
				inputParams["oldRcName"]=frmIBMyReceipentsAccounts.lblRcName.text
				inputParams["oldRcmobile"]=removeHyphenIB(frmIBMyReceipentsAccounts.lblRcMobileNo.text)
				inputParams["oldRcEmail"]=frmIBMyReceipentsAccounts.lblRcEmail.text
				inputParams["oldFbId"]=frmIBMyReceipentsAccounts.lblRcFbId.text
				//startExistingRcProfileEditService();
				}
	inputParams["serviceFlow"]="recipientsComp";
	if(recipientAddFromTransfer){
		inputParams["productCode"] = gblcwselectedData.prodCode;
	}
	clearOTP();
	showLoadingScreenPopup();
	invokeServiceSecureAsync("ExecuteMyRecipientAddService", inputParams, compositeServiceCallback);			
}
function compositeServiceCallback(status,resulttable){
if (status == 400){ 
	if(resulttable["opstatus"]==0){
		if (gblAdd_Receipent_State != gblEXISTING_RC_EDITION) {
				if (gblAdd_Receipent_State == gblNEW_RC_ADDITION && (frmIBMyReceipentsAddContactManually.segmentRcAccounts.data == null || frmIBMyReceipentsAddContactManually.segmentRcAccounts.data == "")) {
						dismissLoadingScreenPopup();
						gblRefreshRcCache = true;
						frmIBMyReceipentsAddContactManuallyConf.show();
						frmIBMyReceipentsAddContactManuallyConf.lblrcname.text = frmIBMyReceipentsAddContactManually.lblrecipientname.text;
						if (frmIBMyReceipentsAddContactManually.lblmobnum.text == null || frmIBMyReceipentsAddContactManually.lblmobnum.text ==	"")
							frmIBMyReceipentsAddContactManuallyConf.lblmobnum.text = "";
						else
							frmIBMyReceipentsAddContactManuallyConf.lblmobnum.text = frmIBMyReceipentsAddContactManually.lblmobnum.text;
						frmIBMyReceipentsAddContactManuallyConf.lblfbid.text = frmIBMyReceipentsAddContactManually.lblfbidstudio19.text;//Modified by Studio Viz
						if (frmIBMyReceipentsAddContactManually.lblemail.text == null || frmIBMyReceipentsAddContactManually.lblemail.text == "")
							frmIBMyReceipentsAddContactManuallyConf.lblemail.text = "";
						else
							frmIBMyReceipentsAddContactManuallyConf.lblemail.text = frmIBMyReceipentsAddContactManually.lblemail.text;
						frmIBMyReceipentsAddContactManuallyConf.image2101071681311897.src = frmIBMyReceipentsAddContactManually.image210107168135.src;
						frmIBMyReceipentsAddContactManuallyConf.label10107168132952.setVisibility(false);
						frmIBMyReceipentsAddContactManuallyConf.segmentRcAccounts.setVisibility(false);
						var perId = resulttable["personalizedIdList"][0]["personalizedId"];
						startImageServiceCallRecipientsIB(perId);		
							//startNewOnlyRecipientAddService();
					} else {
						if (gblAdd_Receipent_State == gblNEW_RC_ADDITION_PRECONFIRM || gblAdd_Receipent_State == gblNEW_RC_ADDITION){
							if (parseInt(formId.toString()) == 3) {
								gblRefreshRcCache = true;
								frmIBMyReceipentsFBContactConf.show();
								setToAddDatatoFBConfirmationSeg();
								dismissLoadingScreenPopup();
								//fb
							}else{	
								if(recipientAddFromTransfer){//enh069
									closeRightPanelTransfer();
				//DEF1330 Data is not cleared if the user tries to add new recipient through transfers flow   START
									resetContentsIB();
									onChangeToRecip();
				//DEF1330 Data is not cleared if the user tries to add new recipient through transfers flow  END 
									var isTransferAllowed=resulttable["isTransferAllowed"];
									var accountData=frmIBMyReceipentsAddContactManually.segmentRcAccounts.data[0];
									gblBANKREF = accountData["lblBankName"];
									gblisTMB = accountData["bankCode"];
									personalizedID=resulttable["personalizedIdList"][0]["personalizedId"];
									gblSelectedRecipentName=frmIBMyReceipentsAddContactManually.lblrecipientname.text;
									frmIBTranferLP.imgXferToImage.src=RC_LOGO_URL+personalizedID;
									frmIBTranferLP.lblXferToNameRcvd.text = accountData["lblNickName"];
									frmIBTranferLP.lblXferToContactRcvd.text = accountData["lblNumber"];
									frmIBTranferLP.lblXferToBankNameRcvd.text = gblBANKREF;
									gblXferPhoneNo = frmIBMyReceipentsAddContactManually.lblmobnum.text;
									gblXferEmail = frmIBMyReceipentsAddContactManually.lblemail.text;
									if(gblXferPhoneNo == "" || gblXferPhoneNo == null){
										gblXferPhoneNo="";
									}
									if(gblXferEmail == "" || gblXferEmail == null){
										gblXferEmail="";
									}
									frmIBTranferLP.imgXferToImage.setVisibility(true);
									frmIBTranferLP.show();
									//setSelTabBankAccountOrMobile();
									if(isTransferAllowed == "N"){
										alert(kony.i18n.getLocalizedString("fromAccNotEligible"));
										gblSelTransferMode = 1;//default account transfer
										getFromXferAccountsIB();
										frmIBTransferCustomWidgetLP.lblXferToNameRcvd.text= accountData["lblNickName"];;
										frmIBTransferCustomWidgetLP.lblXferToContactRcvd.text= accountData["lblNumber"];
										frmIBTransferCustomWidgetLP.lblXferToBankNameRcvd.text=accountData["lblBankName"];
										frmIBTransferCustomWidgetLP.imgXferToImage.src=RC_LOGO_URL+personalizedID;
										frmIBTransferCustomWidgetLP.imgXferToImage.setVisibility(true);
										return;
									}
									//accountData["lblAccountName"];
															
								}else{
									gblRefreshRcCache = true;
									frmIBMyReceipentsAddContactManuallyConf.show();
									frmIBMyReceipentsAddContactManuallyConf.lblrcname.text = frmIBMyReceipentsAddContactManually.lblrecipientname.text;
									if (frmIBMyReceipentsAddContactManually.lblmobnum.text == null || frmIBMyReceipentsAddContactManually.lblmobnum.text ==	"")
										frmIBMyReceipentsAddContactManuallyConf.lblmobnum.text = "";
									else
										frmIBMyReceipentsAddContactManuallyConf.lblmobnum.text = frmIBMyReceipentsAddContactManually.lblmobnum.text;
									frmIBMyReceipentsAddContactManuallyConf.lblfbid.text = frmIBMyReceipentsAddContactManually.lblfbidstudio19.text;//Modified by Studio Viz
									if (frmIBMyReceipentsAddContactManually.lblemail.text == null || frmIBMyReceipentsAddContactManually.lblemail.text == "")
										frmIBMyReceipentsAddContactManuallyConf.lblemail.text = "";
									else
										frmIBMyReceipentsAddContactManuallyConf.lblemail.text = frmIBMyReceipentsAddContactManually.lblemail.text;
									frmIBMyReceipentsAddContactManuallyConf.image2101071681311897.src = frmIBMyReceipentsAddContactManually.image2101071681311897.src;
									frmIBMyReceipentsAddContactManuallyConf.label10107168132952.setVisibility(true);
									frmIBMyReceipentsAddContactManuallyConf.segmentRcAccounts.setVisibility(true);
									setToAddDatatoManualConfirmationSeg();
								}				
								var perId = resulttable["personalizedIdList"][0]["personalizedId"];
								startImageServiceCallRecipientsIB(perId);	
								dismissLoadingScreenPopup();
								//startNewRecipientPlusAccountAddService();
							}				
						}else if (gblAdd_Receipent_State == gblEXISTING_RC_BANKACCNT_ADDITION) {
							gblRefreshRcCache = true;
							frmIBMyReceipentsAddContactManuallyConf.show();
							setToBeSaveBankDatatoExistingRCConfirmationSeg();
							dismissLoadingScreenPopup();
							//startExistingRcAccountAddService();
							
						}else {
							dismissLoadingScreenPopup();
							//startExistingRcAccountEditService();
						}
					}
				} else {
							gblRefreshRcCache = true;
							frmIBMyReceipentsAddContactManuallyConf.show();
							frmIBMyReceipentsAddContactManuallyConf.lblrcname.text = frmIBMyReceipentsAddContactManually.lblrecipientname.text;
							frmIBMyReceipentsAddContactManuallyConf.lblmobnum.text = frmIBMyReceipentsAddContactManually.lblmobnum.text;
							frmIBMyReceipentsAddContactManuallyConf.lblfbid.text = frmIBMyReceipentsAddContactManually.lblfbidstudio19.text;//Modified by Studio Viz
							frmIBMyReceipentsAddContactManuallyConf.lblemail.text = frmIBMyReceipentsAddContactManually.lblemail.text;
							frmIBMyReceipentsAddContactManuallyConf.image2101071681311897.src = frmIBMyReceipentsAddContactManually.image2101071681311897.src;
							frmIBMyReceipentsAddContactManuallyConf.label10107168132952.setVisibility(false);
							frmIBMyReceipentsAddContactManuallyConf.scrollboxRcAccounts.setVisibility(false);
							frmIBMyReceipentsAddContactManuallyConf.segmentRcAccounts.setVisibility(false);
							var mob="";
							var mail="";
							var fbid="";
							var rcname="";				
							if(frmIBMyReceipentsAccounts.lblRcName.text != frmIBMyReceipentsAddContactManually.txtRecipientName.text)
								rcname=frmIBMyReceipentsAccounts.lblRcName.text + "+" + frmIBMyReceipentsAddContactManually.txtRecipientName.text;
							if(frmIBMyReceipentsAccounts.lblRcMobileNo.text!=  frmIBMyReceipentsAddContactManually.txtmobnum.text)
								mob=frmIBMyReceipentsAccounts.lblRcMobileNo.text + "+" +  frmIBMyReceipentsAddContactManually.txtmobnum.text;
							if(frmIBMyReceipentsAccounts.lblRcEmail.text != frmIBMyReceipentsAddContactManually.txtemail.text)
								mail=frmIBMyReceipentsAccounts.lblRcEmail.text + "+" +   frmIBMyReceipentsAddContactManually.txtemail.text;
							if(frmIBMyReceipentsAccounts.lblRcFbId.text	!=frmIBMyReceipentsAddContactManually.tbxFbID.text)
								fbid=frmIBMyReceipentsAccounts.lblRcFbId.text	+"+"+ frmIBMyReceipentsAddContactManually.tbxFbID.text;
							
							dismissLoadingScreenPopup();
							var perId = gblselectedRcId;
							if(frmIBMyReceipentsAddContactManually.image210107168135.src==null)
							startImageServiceCallRecipientsIB(perId);
							else
							frmIBMyReceipentsAddContactManuallyConf.image2101071681311897.src=	frmIBMyReceipentsAddContactManually.image210107168135.src
							//startExistingRcProfileEditService();
						}
		
	}//opstatus 0
		else if (resulttable["opstatus"] == 8005) 
			{
 				dismissLoadingScreenPopup();
 				if (resulttable["errCode"] == "VrfyOTPErr00001") {
                	gblVerifyOTP = resulttable["retryCounterVerifyOTP"];
					  if(gblTokenSwitchFlag == true  && gblSwitchToken == false) 
						{   
						    frmIBTransferNowConfirmation.hbxToken.setVisibility(true);
                	 		frmIBTransferNowConfirmation.tbxToken.setFocus(true);
							var currFrm = kony.application.getCurrentForm().id;
							//alert("currFrm--->"+currFrm);
							if(currFrm == "frmIBMyReceipentsAddBankAccnt"){
								//alert(kony.i18n.getLocalizedString("wrongOTP"));//commented by swapna
			                    frmIBMyReceipentsAddBankAccnt.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");//kony.i18n.getLocalizedString("invalidOTP"); //
			                    frmIBMyReceipentsAddBankAccnt.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
			                    frmIBMyReceipentsAddBankAccnt.hbxOTPincurrect.isVisible = true;
			                    frmIBMyReceipentsAddBankAccnt.hbox476047582127699.isVisible = false;
			                    frmIBMyReceipentsAddBankAccnt.hbxOTPsnt.isVisible = false;
			                    frmIBMyReceipentsAddBankAccnt.textbox247428873338513.text = "";
			                    frmIBMyReceipentsAddBankAccnt.textbox247428873338513.setFocus(true);
			                 }else if(currFrm == "frmIBMyReceipentsAddContactFB"){
			                    frmIBMyReceipentsAddContactFB.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");//kony.i18n.getLocalizedString("invalidOTP"); //
			                    frmIBMyReceipentsAddContactFB.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
			                    frmIBMyReceipentsAddContactFB.hbxOTPincurrect.isVisible = true;
			                    frmIBMyReceipentsAddContactFB.hbox476047582127699.isVisible = false;
			                    frmIBMyReceipentsAddContactFB.hbxOTPsnt.isVisible = false;
			                    frmIBMyReceipentsAddContactFB.textbox247428873338513.text = "";
			                    frmIBMyReceipentsAddContactFB.textbox247428873338513.setFocus(true);		                    
			                 }else if(currFrm == "frmIBMyReceipentsAddContactManually"){
			                 	frmIBMyReceipentsAddContactManually.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");//kony.i18n.getLocalizedString("invalidOTP"); //
			                    frmIBMyReceipentsAddContactManually.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
			                    frmIBMyReceipentsAddContactManually.hbxOTPincurrect.isVisible = true;
			                    frmIBMyReceipentsAddContactManually.hbox101089555968194.isVisible = false;
			                    frmIBMyReceipentsAddContactManually.hbox101089555968200.isVisible = false;	
			                    frmIBMyReceipentsAddContactManually.txtotp.text = "";	
			                    frmIBMyReceipentsAddContactManually.txtotp.setFocus(true);		
			                 
			                 }
			                	 		
                	 		//frmIBMyReceipentsAddContactManually.txtotp.setFocus(true);
						} else {
					       // alert("currFrm-dddd-->");
							var currFrm = kony.application.getCurrentForm().id;
							//alert("currFrm--->"+currFrm);
							if(currFrm == "frmIBMyReceipentsAddBankAccnt"){
								//alert(kony.i18n.getLocalizedString("wrongOTP"));//commented by swapna
			                    frmIBMyReceipentsAddBankAccnt.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");//kony.i18n.getLocalizedString("invalidOTP"); //
			                    frmIBMyReceipentsAddBankAccnt.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
			                    frmIBMyReceipentsAddBankAccnt.hbxOTPincurrect.isVisible = true;
			                    frmIBMyReceipentsAddBankAccnt.hbox476047582127699.isVisible = false;
			                    frmIBMyReceipentsAddBankAccnt.hbxOTPsnt.isVisible = false;
			                    frmIBMyReceipentsAddBankAccnt.textbox247428873338513.text = "";
			                    frmIBMyReceipentsAddBankAccnt.textbox247428873338513.setFocus(true);
			                 }else if(currFrm == "frmIBMyReceipentsAddContactFB"){
			                    frmIBMyReceipentsAddContactFB.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");//kony.i18n.getLocalizedString("invalidOTP"); //
			                    frmIBMyReceipentsAddContactFB.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
			                    frmIBMyReceipentsAddContactFB.hbxOTPincurrect.isVisible = true;
			                    frmIBMyReceipentsAddContactFB.hbox476047582127699.isVisible = false;
			                    frmIBMyReceipentsAddContactFB.hbxOTPsnt.isVisible = false;
			                    frmIBMyReceipentsAddContactFB.textbox247428873338513.text = "";
			                    frmIBMyReceipentsAddContactFB.textbox247428873338513.setFocus(true);		                    
			                 }else if(currFrm == "frmIBMyReceipentsAddContactManually"){
			                 	frmIBMyReceipentsAddContactManually.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");//kony.i18n.getLocalizedString("invalidOTP"); //
			                    frmIBMyReceipentsAddContactManually.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
			                    frmIBMyReceipentsAddContactManually.hbxOTPincurrect.isVisible = true;
			                    frmIBMyReceipentsAddContactManually.hbox101089555968194.isVisible = false;
			                    frmIBMyReceipentsAddContactManually.hbox101089555968200.isVisible = false;	
			                    frmIBMyReceipentsAddContactManually.txtotp.text = "";	
			                    frmIBMyReceipentsAddContactManually.txtotp.setFocus(true);		
			                 
			                 }
							//alert(kony.i18n.getLocalizedString("wrongOTP")); //commented by swapna
						}
                    return false;
           		}else if (resulttable["errCode"] == "VrfyOTPErr00002") {
                    dismissLoadingScreenPopup();
					handleOTPLockedIB(resulttable);
                   	return false;
           		}else if (resulttable["errCode"] == "GenOTPRtyErr00001") {
                    dismissLoadingScreenPopup();
                    showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                    return false;
           		} else if(resulttable["errCode"] == "VrfyOTPErr00006"){
           		       		var currFrm = kony.application.getCurrentForm().id;
							//alert("currFrm--->"+currFrm);
							if(currFrm == "frmIBMyReceipentsAddBankAccnt"){
								//alert(kony.i18n.getLocalizedString("wrongOTP"));//commented by swapna
			                    frmIBMyReceipentsAddBankAccnt.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");//kony.i18n.getLocalizedString("invalidOTP"); //
			                    frmIBMyReceipentsAddBankAccnt.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
			                    frmIBMyReceipentsAddBankAccnt.hbxOTPincurrect.isVisible = true;
			                    frmIBMyReceipentsAddBankAccnt.hbox476047582127699.isVisible = false;
			                    frmIBMyReceipentsAddBankAccnt.hbxOTPsnt.isVisible = false;
			                    frmIBMyReceipentsAddBankAccnt.textbox247428873338513.text = "";
			                    frmIBMyReceipentsAddBankAccnt.textbox247428873338513.setFocus(true);
			                 }else if(currFrm == "frmIBMyReceipentsAddContactFB"){
			                    frmIBMyReceipentsAddContactFB.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");//kony.i18n.getLocalizedString("invalidOTP"); //
			                    frmIBMyReceipentsAddContactFB.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
			                    frmIBMyReceipentsAddContactFB.hbxOTPincurrect.isVisible = true;
			                    frmIBMyReceipentsAddContactFB.hbox476047582127699.isVisible = false;
			                    frmIBMyReceipentsAddContactFB.hbxOTPsnt.isVisible = false;
			                    frmIBMyReceipentsAddContactFB.textbox247428873338513.text = "";
			                    frmIBMyReceipentsAddContactFB.textbox247428873338513.setFocus(true);		                    
			                 }else if(currFrm == "frmIBMyReceipentsAddContactManually"){
			                 	frmIBMyReceipentsAddContactManually.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");//kony.i18n.getLocalizedString("invalidOTP"); //
			                    frmIBMyReceipentsAddContactManually.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
			                    frmIBMyReceipentsAddContactManually.hbxOTPincurrect.isVisible = true;
			                    frmIBMyReceipentsAddContactManually.hbox101089555968194.isVisible = false;
			                    frmIBMyReceipentsAddContactManually.hbox101089555968200.isVisible = false;	
			                    frmIBMyReceipentsAddContactManually.txtotp.text = "";	
			                    frmIBMyReceipentsAddContactManually.txtotp.setFocus(true);		
			                 
			                 }
           		
           		}
 	    }else {
				var currFrm = kony.application.getCurrentForm().id;
				if(currFrm == "frmIBMyReceipentsAddBankAccnt"){
					//alert(kony.i18n.getLocalizedString("wrongOTP"));//commented by swapna
                    frmIBMyReceipentsAddBankAccnt.lblOTPinCurr.text = " ";//kony.i18n.getLocalizedString("invalidOTP"); //
                    frmIBMyReceipentsAddBankAccnt.lblPlsReEnter.text = " "; 
                    frmIBMyReceipentsAddBankAccnt.hbxOTPincurrect.isVisible = false;
                    frmIBMyReceipentsAddBankAccnt.hbox476047582127699.isVisible = true;
                    frmIBMyReceipentsAddBankAccnt.hbxOTPsnt.isVisible = true;
                    frmIBMyReceipentsAddBankAccnt.textbox247428873338513.setFocus(true);
                 }else if(currFrm == "frmIBMyReceipentsAddContactFB"){
                    frmIBMyReceipentsAddContactFB.lblOTPinCurr.text = " ";//kony.i18n.getLocalizedString("invalidOTP"); //
                    frmIBMyReceipentsAddContactFB.lblPlsReEnter.text = " "; 
                    frmIBMyReceipentsAddContactFB.hbxOTPincurrect.isVisible = false;
                    frmIBMyReceipentsAddContactFB.hbox476047582127699.isVisible = true;
                    frmIBMyReceipentsAddContactFB.hbxOTPsnt.isVisible = true;
                    frmIBMyReceipentsAddContactFB.textbox247428873338513.setFocus(true);			                    
                 }else if(currFrm == "frmIBMyReceipentsAddContactManually"){
                 	frmIBMyReceipentsAddContactManually.lblOTPinCurr.text = " ";//kony.i18n.getLocalizedString("invalidOTP"); //
                    frmIBMyReceipentsAddContactManually.lblPlsReEnter.text = " "; 
                    frmIBMyReceipentsAddContactManually.hbxOTPincurrect.isVisible = false;
                    frmIBMyReceipentsAddContactManually.hbox101089555968194.isVisible = true;
                    frmIBMyReceipentsAddContactManually.hbox101089555968200.isVisible = true;			
	            }
			dismissLoadingScreenPopup();
			if(resulttable["errMsg"]!=undefined)
				alert(" " + resulttable["errMsg"]);
			else
				alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
		}
	}//400 status
}

var gblFBIBStateTrack = false;
var personalizedID;
/**
 * Method to invoke service that uploads the new rc from FB list
 * @returns {}
 */
 function startInquiryCustomerAccountsService()
 {
 	var inputParams={
 	crmId: gblcrmId
 	}
 	try {
		invokeServiceSecureAsync("inquiryCustomerAccounts", inputParams, startInquiryCustomerAccountsCallback);
	} catch (e) {
		// todo: handle exception
		invokeCommonIBLogger("Exception in invoking service");
	}
 	
 }
 
 function startInquiryCustomerAccountsCallback(status, callBackResponse){
 		if (status == 400){ 			
 			if (callBackResponse["opstatus"] == 0  && callBackResponse["Results"]!=null) {
 				otherBankMyAccounts=[];
 				for(var i=0;i<callBackResponse["Results"].length;i++){ 					
 					otherBankMyAccounts.push(callBackResponse["Results"][i]["accntId"]);
 					
 	 			}
 			}
 			if(navigator.userAgent.match(/iPad/i)){
 				frmIBMyReceipentsHome.lblMyReceipents.setFocus(true);
 			}
 			else{
 			frmIBMyReceipentsHome.tbxSearch.setFocus(true);
 			}
 		}
 	}


function startNewRcFBListAddService() {
	var inputParams = {
		crmId: gblcrmId,
		receipentList: prepareNewRcFBList()
	};
	showLoadingScreenPopup();
	try {
		invokeServiceSecureAsync("receipentNewAddService", inputParams, startNewRcFBListAddServiceAsyncCallback);
	} catch (e) {
		// todo: handle exception
		invokeCommonIBLogger("Exception in invoking service");
	}
}
/**
 * Callback method for startNewRcFBListAddService()
 * @returns {}
 */

function startNewRcFBListAddServiceAsyncCallback(status, callBackResponse) {
	if (status == 400) {
		if (callBackResponse["opstatus"] == 0) {
			gblRefreshRcCache = true;
			frmIBMyReceipentsFBContactConf.show();
			setToAddDatatoFBConfirmationSeg();
			dismissLoadingScreenPopup();
		} else {
			dismissLoadingScreenPopup();
			if (callBackResponse["errMsg"] != null) {
				if (kony.string.endsWith(callBackResponse["errMsg"], "(J200000001)")) {
					showCommonAlert(kony.i18n.getLocalizedString("J200000001"), null);
				} else {
					showCommonAlert(callBackResponse["errMsg"], null);
				}
			}
		}
	}
}
/**
 * Method to invoke service that uploads the new rc along with Bank accounts list
 * @returns {}
 */

function startNewRcAddwithAccntService() {
	var inputParams = {
		crmId: gblcrmId,
		personalizedName: frmIBMyReceipentsAddContactManually.lblrecipientname.text,
		personalizedPictureId: frmIBMyReceipentsAddContactManually.image2101071681311897.src,
		//personalizedPictureId: "nouserimg.jpg",
		personalizedMailId: frmIBMyReceipentsAddContactManually.lblemail.text,
		personalizedMobileNumber: frmIBMyReceipentsAddContactManually.txtmobnum.text,
		personalizedFacebookId: frmIBMyReceipentsAddContactManually.lblfbidstudio19.text,//Modified by Studio Viz
		personalizedStatus: "Added",
		ownFlag: "N",
		personalizedAccList: prepareNewRcAccountList()
	};
	showLoadingScreenPopup();
	try {
		invokeServiceSecureAsync("receipentNewAddwithAccntService", inputParams, startNewRcAddwithAccntServiceAsyncCallback);
	} catch (e) {
		// todo: handle exception
		invokeCommonIBLogger("Exception in invoking service");
	}
}
/**
 * Callback method for startNewRcAddwithAccntService()
 * @returns {}
 */

function startNewRcAddwithAccntServiceAsyncCallback(status, callBackResponse) {
	if (status == 400) {
		if (callBackResponse["opstatus"] == 0) {
			dismissLoadingScreenPopup();
			startRcNotificationService("Added Recipient with account(s)", "A Recipient has been added with one or more accounts",
				frmIBMyReceipentsAddContactManually.lblrecipientname.text, null, frmIBMyReceipentsAddContactManually.image2101071681311897
				.src, createListForRcAccountNotification(),
				"recipientsAdd");
			gblRefreshRcCache = true;
			frmIBMyReceipentsAddContactManuallyConf.show();
			frmIBMyReceipentsAddContactManuallyConf.lblrcname.text = frmIBMyReceipentsAddContactManually.lblrecipientname.text;
			frmIBMyReceipentsAddContactManuallyConf.lblmobnum.text = frmIBMyReceipentsAddContactManually.lblmobnum.text;
			frmIBMyReceipentsAddContactManuallyConf.lblfbid.text = frmIBMyReceipentsAddContactManually.lblfbidstudio19.text;//Modified by Studio Viz
			frmIBMyReceipentsAddContactManuallyConf.lblemail.text = frmIBMyReceipentsAddContactManually.lblemail.text;
			frmIBMyReceipentsAddContactManuallyConf.image2101071681311897.src = frmIBMyReceipentsAddContactManually.image2101071681311897
				.src;
			frmIBMyReceipentsAddContactManuallyConf.label10107168132952.setVisibility(true);
			frmIBMyReceipentsAddContactManuallyConf.segmentRcAccounts.setVisibility(true);
			if (frmIBMyReceipentsAddContactManuallyConf.lblfbid.text == "Not Available") {
				activityLogServiceCall("045", "", "01", "", "Add", frmIBMyReceipentsAddContactManuallyConf.lblrcname.text,
					frmIBMyReceipentsAddContactManuallyConf.lblmobnum.text,
					frmIBMyReceipentsAddContactManuallyConf.lblemail.text, "", "");
			} else {
				activityLogServiceCall("045", "", "01", "", "Add", frmIBMyReceipentsAddContactManuallyConf.lblrcname.text,
					frmIBMyReceipentsAddContactManuallyConf.lblmobnum.text,
					frmIBMyReceipentsAddContactManuallyConf.lblemail.text, frmIBMyReceipentsAddContactManuallyConf.lblfbid.text, "");
			}
			setToAddDatatoManualConfirmationSeg();
			var perId = callBackResponse["personalizedIdList"][0]["personalizedId"];
			startImageServiceCallRecipientsIB(perId);
		} else if (callBackResponse["opstatus"] == 1) {
			if (callBackResponse["errMsg"] != null) {
				dismissLoadingScreenPopup();
				if (kony.string.endsWith(callBackResponse["errMsg"], "(J200000001)")) {
					showCommonAlert(kony.i18n.getLocalizedString("J200000001"), null);
				} else {
					showCommonAlert(callBackResponse["errMsg"], null);
				}
				frmIBMyReceipentsHome.show();
				destroyAllForms();
				return;
			}
		} else {
			dismissLoadingScreenPopup();
			showCommonAlert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"), null);
		}
	} else {
		if (status == 300) {
			dismissLoadingScreenPopup();
			showCommonAlert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"), null);
		}
	}
}
/**
 * Method to invoke service that gets bank accounts for receipent
 * @returns {}
 */

function startgetBankAccntforRCService() {
	invokeCommonIBLogger("############startgetBankAccntforRCService()...receipent iD is " + gblselectedRcId);
	var inputParams = {
		crmId: gblcrmId,
		personalizedId: gblselectedRcId
	};
	showLoadingScreenPopup();
	try {
		invokeServiceSecureAsync("receipentAllDetailsService", inputParams, startgetBankAccntforRCServiceAsyncCallback);
	} catch (e) {
		// todo: handle exception
		invokeCommonIBLogger("Exception in invoking service");
	}
}
/**
 * Callback method for startgetBankAccntforRCService()
 * @returns {}
 */

function startgetBankAccntforRCServiceAsyncCallback(status, callBackResponse) {
	invokeCommonIBLogger("############startgetBankAccntforRCServiceAsyncCallback status" + status);
	checkRcProfileDataVisibility();
	if (status == 400) {
		invokeCommonIBLogger("############startgetBankAccntforRCServiceAsyncCallback opstatus" + callBackResponse["opstatus"]);
		if (callBackResponse["opstatus"] == 0) {
			var responseData = callBackResponse["Results"];
			if ((responseData.length - 1) > 0) {
				if (parseInt(formId.toString()) != 6) {
					frmIBMyReceipentsAccounts.show();
				}
				invokeCommonIBLogger("#############startgetBankAccntforRCServiceAsyncCallback data##########" + callBackResponse[
					"Results"]);
				collectionData = callBackResponse["Results"];
				setTimertoLoadAccounts();
			} else {
				setDataToRcBankAccntList(responseData);
				if (frmIBMyReceipentsAccounts.segmentRcAccounts.data.length >= MAX_BANK_ACCNT_PER_RC) {
				frmIBMyReceipentsAccounts.btnAddAccount.skin = btnIBaddsmalldis;
				frmIBMyReceipentsAccounts.btnAddAccount.setEnabled(false);
					} else {
				frmIBMyReceipentsAccounts.btnAddAccount.skin = btnIBaddsmall;
				frmIBMyReceipentsAccounts.btnAddAccount.setEnabled(true);
					}
				dismissLoadingScreenPopup();
				showCommonAlert(kony.i18n.getLocalizedString("Receipent_alert_NoRcBankaccnts"), null);
			}
		} else {
			dismissLoadingScreenPopup();
			if(callBackResponse["errMsg"] != undefined)
	        	{
	        		alert(callBackResponse["errMsg"]);
	        	}
	        	else
	        	{
	        		alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
	        	}			}
	} else {
		if (status == 300) {
			dismissLoadingScreenPopup();
			showCommonAlert("" + kony.i18n.getLocalizedString("Receipent_alert_NoRcBankaccnts"), null);
		}
	}
}
/**
 * Method to fire timer to load accnts for RC
 * @returns {}
 */

function setTimertoLoadAccounts() {
	try {
		kony.timer.cancel("104");
	} catch (e) {
		// todo: handle exception
	}
	invokeCommonIBLogger("FIRE TIMER");
	kony.timer.schedule("104", banklistTimerCallBack, 1, false);
}
/**
 * Timer callback
 * @returns {}
 */

function banklistTimerCallBack() {
	invokeCommonIBLogger("TIMER CALLBACK");
	setDataToRcBankAccntList(collectionData);
	if (frmIBMyReceipentsAccounts.segmentRcAccounts.data != null && frmIBMyReceipentsAccounts.segmentRcAccounts.data.length >
		0) {
		if (frmIBMyReceipentsAccounts.segmentRcAccounts.data.length >= MAX_BANK_ACCNT_PER_RC) {
			frmIBMyReceipentsAccounts.btnAddAccount.skin = btnIBaddsmalldis;
			frmIBMyReceipentsAccounts.btnAddAccount.setEnabled(false);
		} else {
			frmIBMyReceipentsAccounts.btnAddAccount.skin = btnIBaddsmall;
			frmIBMyReceipentsAccounts.btnAddAccount.setEnabled(true);
		}
	}
	dismissLoadingScreenPopup();
}
/**
 * Method to invoke service that loads the RC list
 * @returns {}
 */

function startReceipentListingService(gblcrmId, form) {
	formId = form;
	var inputParams = {
		crmId: gblcrmId
	};
	showLoadingScreenPopup();
	try {
		invokeServiceSecureAsync("receipentListingService", inputParams, startReceipentListingServiceAsyncCallback);
	} catch (e) {
		// todo: handle exception
		invokeCommonIBLogger("Exception in invoking service");
	}
}
/**
 * Callback method for startReceipentListingService()
 * @returns {}
 */

function startReceipentListingServiceAsyncCallback(status, callBackResponse) {
	var currentFormId = kony.application.getCurrentForm();
	if (status == 400) {
		if (callBackResponse["opstatus"] == 0) {
			var responseData = callBackResponse["Results"];
			gblReceipentAccts = callBackResponse["custAcctRec"];
			MAX_BANK_ACCNT_PER_RC = callBackResponse["recipientAccountMaxCount"];
			MAX_RECIPIENT_COUNT = callBackResponse["recipientMaxCount"];
			MAX_BANK_ACCNT_BULK_ADD = callBackResponse["recipientBulkAccntAddCount"];
			MAX_RECIPIENT_BULK_COUNT = callBackResponse["recipientBulkMaxCount"];
			if (responseData.length > 0) {
				currentFormId.lblNoRecipients.setVisibility(false);
				setDataToReceipentsListToForm(callBackResponse["Results"], formId);
				checknSetUIforRcCountLimit(formId);
				cacheRcList(formId);
				dismissLoadingScreenPopup();
			} else {
				segAboutMeLoad();
				pagespecificSubMenu();
				dismissLoadingScreenPopup();
				currentFormId.lblNoRecipients.setVisibility(true);
				currentFormId.lblNoRecipients.text = kony.i18n.getLocalizedString("Receipent_alert_NoRecipientfound");
				if (currentFormId.segmentReceipentListing.data != null && currentFormId.segmentReceipentListing.data.length > 0) {
					currentFormId.segmentReceipentListing.removeAll();
				}
			}
		} else {
			segAboutMeLoad();
			pagespecificSubMenu();
			dismissLoadingScreenPopup();
				if(callBackResponse["errMsg"] != undefined)
	        	{
	        		alert(callBackResponse["errMsg"]);
	        	}
	        	else
	        	{
	        		alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
	        	}				currentFormId.lblNoRecipients.setVisibility(true);
			currentFormId.lblNoRecipients.text = kony.i18n.getLocalizedString("Receipent_alert_NoRecipientfound");
			if (currentFormId.segmentReceipentListing.data != null && currentFormId.segmentReceipentListing.data.length > 0) {
				currentFormId.segmentReceipentListing.removeAll();
			}
			MAX_RECIPIENT_COUNT = 99;
			MAX_BANK_ACCNT_BULK_ADD = 2;
			MAX_BANK_ACCNT_PER_RC = 50;
		}
	} else {
		if (status == 300) {
			dismissLoadingScreenPopup();
			segAboutMeLoad();
			pagespecificSubMenu();
			currentFormId.lblNoRecipients.setVisibility(true);
			currentFormId.lblNoRecipients.text = kony.i18n.getLocalizedString("Receipent_alert_NoRecipientfound");
			if (currentFormId.segmentReceipentListing.data != null && currentFormId.segmentReceipentListing.data.length > 0) {
				currentFormId.segmentReceipentListing.removeAll();
			}
			MAX_RECIPIENT_COUNT = 99;
			MAX_BANK_ACCNT_BULK_ADD = 2;
			MAX_BANK_ACCNT_PER_RC = 50;
		}
	}
}
/**
 * Method to invoke service that deletes the receipent
 * @returns {}
 */

function startRcDeleteService() {
	var mob;
	var mail;
	var fbId;
	if (frmIBMyReceipentsAccounts.lblRcMobileNo.text == "Not Available")
		mob = "";
	else
		mob = frmIBMyReceipentsAccounts.lblRcMobileNo.text;
	if (frmIBMyReceipentsAccounts.lblRcEmail.text == "Not Available")
		mail = "";
	else
		mail = frmIBMyReceipentsAccounts.lblRcEmail.text;
	if (frmIBMyReceipentsAccounts.lblRcFbId.text == "Not Available" || frmIBMyReceipentsAccounts.lblRcFbId.text == null
		|| frmIBMyReceipentsAccounts.lblRcFbId.text == "")
		fbId = "";
	else
		fbId = frmIBMyReceipentsAccounts.lblRcFbId.text;
	var inputParams = {
		crmId: gblcrmId,
		personalizedId: gblselectedRcId,
		mobile:mob,
		mail:mail,
		fbid:fbId,
		rcName:frmIBMyReceipentsAccounts.lblRcName.text
	};
	showLoadingScreenPopup();
	try {
		invokeServiceSecureAsync("ExecuteMyRecipientDeleteService", inputParams, startRcDeleteServiceAsyncCallback);
	} catch (e) {
		// todo: handle exception
		invokeCommonIBLogger("Exception in invoking service");
	}
}
/**
 * Callback method for startRcDeleteService()
 * @returns {}
 */

function startRcDeleteServiceAsyncCallback(status, callBackResponse) {
	if (status == 400) {
		if (callBackResponse["opstatus"] == 0) {
			resetRcSegData("1", "1");
			deleteReceipent("1");
			frmIBMyReceipentsHome.show();
			dismissLoadingScreenPopup();
		} else {
			dismissLoadingScreenPopup();
			if(callBackResponse["errMsg"] != undefined)
	        	{
	        		alert(callBackResponse["errMsg"]);
	        	}
	        	else
	        	{
	        		alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
	        	}			}
	} else {
		if (status == 300) {
			dismissLoadingScreenPopup();
			showCommonAlert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"), null);
		}
	}
}
/**
 * Method to invoke service that edits the existing rc data
 * @returns {}
 */

function startExistingRcProfileEditService() {
	var inputParams = {
		crmId: gblcrmId,
		personalizedId: gblselectedRcId,
		personalizedName: frmIBMyReceipentsAddContactManually.lblrecipientname.text,
		personalizedPictureId: frmIBMyReceipentsAddContactManually.image2101071681311897.src,
		personalizedMailId: frmIBMyReceipentsAddContactManually.lblemail.text,
		personalizedMobileNumber: frmIBMyReceipentsAddContactManually.txtmobnum.text,
		personalizedFacebookId: frmIBMyReceipentsAddContactManually.lblfbidstudio19.text,//Modified by Studio Viz
		personalizedStatus: "Added"
	};
	showLoadingScreenPopup();
	try {
		invokeServiceSecureAsync("receipentEditService", inputParams, startExistingRcProfileEditServiceAsyncCallback);
	} catch (e) {
		// todo: handle exception
		invokeCommonIBLogger("Exception in invoking service");
	}
}
/**
 * Callback method for startExistingRcEditService()
 * @returns {}
 */

function startExistingRcProfileEditServiceAsyncCallback(status, callBackResponse) {
	if (status == 400) {
		if (callBackResponse["opstatus"] == 0) {
			gblRefreshRcCache = true;
			frmIBMyReceipentsAddContactManuallyConf.show();
			frmIBMyReceipentsAddContactManuallyConf.lblrcname.text = frmIBMyReceipentsAddContactManually.lblrecipientname.text;
			frmIBMyReceipentsAddContactManuallyConf.lblmobnum.text = frmIBMyReceipentsAddContactManually.lblmobnum.text;
			frmIBMyReceipentsAddContactManuallyConf.lblfbid.text = frmIBMyReceipentsAddContactManually.lblfbidstudio19.text;//Modified by Studio Viz
			frmIBMyReceipentsAddContactManuallyConf.lblemail.text = frmIBMyReceipentsAddContactManually.lblemail.text;
			frmIBMyReceipentsAddContactManuallyConf.image2101071681311897.src = frmIBMyReceipentsAddContactManually.image2101071681311897
				.src;
			frmIBMyReceipentsAddContactManuallyConf.label10107168132952.setVisibility(false);
			frmIBMyReceipentsAddContactManuallyConf.scrollboxRcAccounts.setVisibility(false);
			frmIBMyReceipentsAddContactManuallyConf.segmentRcAccounts.setVisibility(false);
			var mob="";
			var mail="";
			var fbid="";
			var rcname="";
		
			if(frmIBMyReceipentsAccounts.lblRcName.text != frmIBMyReceipentsAddContactManually.txtRecipientName.text){
				rcname=frmIBMyReceipentsAccounts.lblRcName.text + "+" + frmIBMyReceipentsAddContactManually.txtRecipientName.text;
			}
			
			if(frmIBMyReceipentsAccounts.lblRcMobileNo.text!=  frmIBMyReceipentsAddContactManually.txtmobnum.text)
			{
				mob=frmIBMyReceipentsAccounts.lblRcMobileNo.text + "+" +  frmIBMyReceipentsAddContactManually.txtmobnum.text;
			}
			if(frmIBMyReceipentsAccounts.lblRcEmail.text != frmIBMyReceipentsAddContactManually.txtemail.text){
				mail=frmIBMyReceipentsAccounts.lblRcEmail.text + "+" +   frmIBMyReceipentsAddContactManually.txtemail.text;
			}
			if(frmIBMyReceipentsAccounts.lblRcFbId.text	!=frmIBMyReceipentsAddContactManually.tbxFbID.text){
				fbid=frmIBMyReceipentsAccounts.lblRcFbId.text	+"+"+ frmIBMyReceipentsAddContactManually.tbxFbID.text;
			}
			/*if (frmIBMyReceipentsAccounts.lblRcMobileNo.text == "Not Available" || frmIBMyReceipentsAccounts.lblRcMobileNo.text==undefined)
				mobold = "";
			else
				mobold = frmIBMyReceipentsAccounts.lblRcMobileNo.text;
		
			if (frmIBMyReceipentsAddContactManuallyConf.lblmobnum.text == "Not Available")
				mob = mobold+"+"+"";
			else
				mob = mobold+"+"+frmIBMyReceipentsAddContactManuallyConf.lblmobnum.text;
			if (frmIBMyReceipentsAccounts.lblRcEmail.text == "Not Available")
				mailold = "";
			else
				mailold = frmIBMyReceipentsAccounts.lblRcEmail.text;
				
			if (frmIBMyReceipentsAddContactManuallyConf.lblemail.text == "Not Available")
				mail = mailold+"+"+ "";
			else
				mail = mailold+"+"+ frmIBMyReceipentsAddContactManuallyConf.lblemail.text;
				
				
			if (frmIBMyReceipentsAccounts.lblRcFbId.text == "Not Available" ||
				frmIBMyReceipentsAccounts.lblRcFbId.text  == null || frmIBMyReceipentsAccounts.lblRcFbId.text  == "")
				fbidold = "";
			else
				fbidold = frmIBMyReceipentsAddContactManually.tbxFbID.text;
					
			if (frmIBMyReceipentsAddContactManuallyConf.lblfbid.text == "Not Available" ||
				frmIBMyReceipentsAddContactManuallyConf.lblfbid.text == null || frmIBMyReceipentsAddContactManuallyConf.lblfbid.text == "")
				fbId = fbidold+"+"+"";
			else
				fbId = fbidold+"+"+frmIBMyReceipentsAddContactManuallyConf.lblfbid.text;*/
				
				activityLogServiceCall("045", "", "01", "", "Edit",rcname, mob,
				mail, fbid,"");
			dismissLoadingScreenPopup();
			//startRcNotificationService("Edited Recipient", "A Recipient has been edited",
			//	frmIBMyReceipentsAddContactManuallyConf.lblrcname.text, null, frmIBMyReceipentsAddContactManuallyConf.image2101071681311897
			//	.src, null, "recipientsEdit");
			var perId = gblselectedRcId;
			startImageServiceCallRecipientsIB(perId);
		} else {
			dismissLoadingScreenPopup();
			if(callBackResponse["errMsg"] != undefined)
	        	{
	        		alert(callBackResponse["errMsg"]);
	        	}
	        	else
	        	{
	        		alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
	        	}			}
	} else {
		if (status == 300) {
			dismissLoadingScreenPopup();
			showCommonAlert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"), null);
		}
	}
}
/**
 * Method to invoke service that edits the existing rc bank data
 * @returns {}
 */

function startExistingRcAccountEditService(bankcode, accountId, nickname, accName, opstatus) {
	if (opstatus.toString() == "Deleted") {
		var accNum = decodeAccountNumbers(accountId);
		var inputParams = {
			personalizedAcctId: accNum,
			acctNickName: nickname,
			personalizedId: gblselectedRcId,
			bankCD: bankcode,
			acctStatus: "Deleted",
			nickname:frmIBMyReceipentsAccounts.lblNickNameAccntDetails.text,
			bankname:frmIBMyReceipentsAccounts.lblBankNameAccntDetails.text,
			accnumber:frmIBMyReceipentsAccounts.lblNumberAccntDetails.text
		};
			inputParams["AccountName"]=frmIBMyReceipentsAccounts.lblAcctNameAccntDetails.text;
		
		try {
			invokeServiceSecureAsync("ExecuteMyRecipientAccountEditDeleteService", inputParams,
				startExistingRcAccountDeleteServiceAsyncCallback);
		} catch (e) {
			// todo: handle exception
			invokeCommonIBLogger("Exception in invoking service");
		}
	} else {
		var accNum = decodeAccountNumbers(accountId);
		var inputParams = {
			personalizedAcctId: accNum,
			acctNickName: nickname,
			personalizedId: gblselectedRcId,
			bankCD: bankcode,
			acctStatus: "Added",
			nickname:frmIBMyReceipentsAccounts.lblNickNameAccntDetails.text +" + "+nickname,
			bankname:frmIBMyReceipentsAccounts.lblBankNameAccntDetails.text,
			accnumber:frmIBMyReceipentsAccounts.lblNumberAccntDetails.text
		};
		if(getORFTFlagIB(gblSelectedAccountCode) == "Y"){
			inputParams["AccountName"]=frmIBMyReceipentsAccounts.lblAcctNameAccntDetails.text;
			inputParams["AccountNameOLDNEW"]=frmIBMyReceipentsAccounts.lblAcctNameAccntDetails.text+" + "+frmIBMyReceipentsAccounts.lblAcctNameAccntDetails.text;
		}else{
			inputParams["AccountName"]=accName;
			inputParams["AccountNameOLDNEW"]=frmIBMyReceipentsAccounts.lblAcctNameAccntDetails.text+" + "+accName;
		}
		try {
			invokeServiceSecureAsync("ExecuteMyRecipientAccountEditDeleteService", inputParams,
				startExistingRcAccountEditServiceAsyncCallback);
		} catch (e) {
			// todo: handle exception
			invokeCommonIBLogger("Exception in invoking service");
		}
	}
	showLoadingScreenPopup();
}
/**
 * Callback method for startExistingRcAccountEditService
 * @param {} startExistingRcAccountEditService
 * @returns {}
 */

function startExistingRcAccountDeleteServiceAsyncCallback(status, callBackResponse) {
	if (status == 400) {
		if (callBackResponse["opstatus"] == 0) {
			/*activityLogServiceCall("046", "", "01", "", "Deleted Recipient account", frmIBMyReceipentsAccounts.lblNickNameAccntDetails
				.text,
				frmIBMyReceipentsAccounts.lblBankNameAccntDetails.text, frmIBMyReceipentsAccounts.lblNumberAccntDetails.text, "",
				"");*/
			//Refresh the screen...dont reshow the form
			startReceipentListingService(gblcrmId, "6");
			deleteAccount();
			dismissLoadingScreenPopup();
			//startRcNotificationService("Deleted Recipient Account", "An account belonging to a Recipient has been deleted", null,
		//		null, null, null, null);
		} else {
			dismissLoadingScreenPopup();
		if(callBackResponse["errMsg"] != undefined)
	        	{
	        		alert(callBackResponse["errMsg"]);
	        	}
	        	else
	        	{
	        		alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
	        	}			}
	} else {
		if (status == 300) {
			dismissLoadingScreenPopup();
			showCommonAlert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"), null);
		}
	}
}
/**
 * Callback method for startExistingRcAccountEditService
 * @param {} startExistingRcAccountEditService
 * @returns {}
 */

function startExistingRcAccountEditServiceAsyncCallback(status, callBackResponse) {
	if (status == 400) {
		if (callBackResponse["opstatus"] == 0) {
			frmIBMyReceipentsEditAccountConf.show();
			setEditedRecipientAccounData();
			dismissLoadingScreenPopup();
		//	startRcNotificationService("Edited Recipient Account", "An account belonging to a Recipient has been edited", null,
		//		null, null, null, null);
		} else {
			dismissLoadingScreenPopup();
		if(callBackResponse["errMsg"] != undefined)
	        	{
	        		alert(callBackResponse["errMsg"]);
	        	}
	        	else
	        	{
	        		alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
	        	}			}
	} else {
		if (status == 300) {
			dismissLoadingScreenPopup();
			showCommonAlert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"), null);
		}
	}
}
/**
 * Method to invoke service that adds to the existing rc bank data
 * @returns {}
 */

function startExistingRcAccountAddService() {
	var inputParams = {
		crmId: gblcrmId,
		personalizedAccList: prepareNewAccountListExistingRc()
	};
	showLoadingScreenPopup();
	try {
		invokeServiceSecureAsync("receipentAddBankAccntService", inputParams, startExistingRcAccountAddServiceAsyncCallback);
	} catch (e) {
		// todo: handle exception
		invokeCommonIBLogger("Exception in invoking service");
	}
}
/**
 * Callback method for startExistingRcAccountAddService
 * @param {} startExistingRcAccountAddService
 * @returns {}
 */

function startExistingRcAccountAddServiceAsyncCallback(status, callBackResponse) {
	if (status == 400) {
		if (callBackResponse["opstatus"] == 0) {
			gblRefreshRcCache = true;
			frmIBMyReceipentsAddContactManuallyConf.show();
			setToBeSaveBankDatatoExistingRCConfirmationSeg();
			dismissLoadingScreenPopup();
			startRcNotificationService("Added Recipient with account(s)", "A Recipient has been added with one or more accounts",
				frmIBMyReceipentsAddContactManuallyConf.lblrcname.text, null, frmIBMyReceipentsAddContactManuallyConf.image2101071681311897
				.src, createListForRcAccountNotificationExistingRc(),
				"recipientsAdd");
		} else if (callBackResponse["opstatus"] == 1) {
			if (callBackResponse["errMsg"] != null) {
				dismissLoadingScreenPopup();
				showCommonAlert("" + callBackResponse["errMsg"], null);
				frmIBMyReceipentsHome.show();
				destroyAllForms();
				return;
			}
		} else {
			dismissLoadingScreenPopup();
			showCommonAlert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"), null);
		}
	} else {
		if (status == 300) {
			dismissLoadingScreenPopup();
			showCommonAlert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"), null);
		}
	}
}
/**
 * Method to invoke service that edits the existing rc bank data
 * @returns {}
 */

function startExistingRcAccountSetFavService(bankCode, accountId, nickName, isfav) {
	var accNum = decodeAccountNumbers(accountId);
	var inputParams = {
		personalizedAcctId: accNum,
		personalizedId: gblselectedRcId,
		bankCD: bankCode,
		acctStatus: "Added",
		accFavoriteFlag: isfav
	};
	showLoadingScreenPopup();
	try {
		invokeServiceSecureAsync("receipentSetFavBankAccntService", inputParams,
			startExistingRcAccountSetFavServiceAsyncCallback);
	} catch (e) {
		// todo: handle exception
		invokeCommonIBLogger("Exception in invoking service");
	}
}
/**
 * Callback method for startExistingRcAccountSetFavService
 * @param {}
 * @returns {}
 */

function startExistingRcAccountSetFavServiceAsyncCallback(status, callBackResponse) {
	if (status == 400) {
		if (callBackResponse["opstatus"] == 0) {
			invalidateAccountListingSortbyFav();
			dismissLoadingScreenPopup();
		} else {
			dismissLoadingScreenPopup();
			showCommonAlert(callBackResponse["errMsg"], null);
		}
	} else {
		if (status == 300) {
			dismissLoadingScreenPopup();
			showCommonAlert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"), null);
		}
	}
}
/**
 * Method to invoke service to select the bank from the combo box
 * @returns {}
 */

function startDisplaySelectbankService() {
	var inputParams = {};
	showLoadingScreenPopup();
	try {
		invokeServiceSecureAsync("receipentgetBankService", inputParams, startDisplaySelectbankServiceAsyncCallback);
	} catch (e) {
		// todo: handle exception
		invokeCommonIBLogger("Exception in invoking service");
	}
}
/**
 * Callback method for startDisplaySelectbankService
 * @param {} startDisplaySelectbankService
 * @returns {}
 */

function startDisplaySelectbankServiceAsyncCallback(status, callBackResponse) {
	var locale = kony.i18n.getCurrentLocale();
	if (status == 400) {
		if (callBackResponse["opstatus"] == 0) {
			var collectionData = callBackResponse["Results"];
			if (globalSelectBankData != null) {
				for (var i = 0; i < globalSelectBankData.length; i++) {
					globalSelectBankData.pop();
				}
			}
			globalSelectBankData = [];
			var masterData = [];
			bankListArray=[];
			//var bn=kony.i18n.getLocalizedString("keySelectBank");
			//	var tempData = ["Bank", bn];
			var tempData = ["Bank", kony.i18n.getLocalizedString("keylblBankName")];
			masterData.push(tempData);
			globalSelectBankData.push(tempData);
			for (var i = 0; i < collectionData.length; i++) {
				var tempData = [];
				var tempRecord = [];
				if ((collectionData[i].bankStatus.toUpperCase() == "ACTIVE") && (collectionData[i].smartFlag.toUpperCase() == "Y" || collectionData[i].orftFlag.toUpperCase() == "Y")){
					if (locale == "en_US") {
						tempData = [collectionData[i]["bankCode"], collectionData[i]["bankNameEng"], collectionData[i]["bankShortName"]];
					} else {
						tempData = [collectionData[i]["bankCode"], collectionData[i]["bankNameThai"], collectionData[i]["bankShortName"]];
					}
					
					if (locale == "en_US") {
						tempRecord = [collectionData[i]["bankCode"], collectionData[i]["bankNameEng"], collectionData[i]["bankShortName"], collectionData[i]["orftFlag"],
							collectionData[i]["smartFlag"],collectionData[i]["bankAccntLen"],collectionData[i]["bankAccntLengths"],collectionData[i]["bankNameEng"],collectionData[i]["bankNameThai"]];
					} else {
						tempRecord = [collectionData[i]["bankCode"], collectionData[i]["bankNameThai"], collectionData[i]["bankShortName"], collectionData[i]["orftFlag"],
							collectionData[i]["smartFlag"],collectionData[i]["bankAccntLen"],collectionData[i]["bankAccntLengths"],collectionData[i]["bankNameEng"],collectionData[i]["bankNameThai"]];
					}
					var obj=[];
					obj[0] = collectionData[i]["bankCode"];
					obj[1] = collectionData[i]["bankNameThai"];
					obj[2] = collectionData[i]["bankShortName"];
					obj[3] = collectionData[i]["bankNameEng"];
					obj[4] = collectionData[i]["bankNameThai"];
					bankListArray.push(obj)
					globalSelectBankData.push(tempRecord);
					masterData.push(tempData);
				}	
			}
			invokeCommonIBLogger(masterData);
			frmIBMyReceipentsAddBankAccnt.comboBankName.masterData = [];
			frmIBMyReceipentsAddBankAccnt.comboBankName.masterData = masterData;
			dismissLoadingScreenPopup();
		} else {
			var masterData = [];
			//var tempData = ["Bank", bn];
			var tempData = ["Bank", kony.i18n.getLocalizedString("keylblBankName")];
			masterData.push(tempData);
			invokeCommonIBLogger(masterData);
			frmIBMyReceipentsAddBankAccnt.comboBankName.masterData = masterData;
			dismissLoadingScreenPopup();
			showCommonAlert(callBackResponse["errMsg"], null);
		}
	} else {
		if (status == 300) {
			var masterData = [];
			//var tempData = ["Bank", bn];
			var tempData = ["Bank",  kony.i18n.getLocalizedString("keylblBankName")];
			masterData.push(tempData);
			invokeCommonIBLogger(masterData);
			frmIBMyReceipentsAddBankAccnt.comboBankName.masterData = masterData;
			dismissLoadingScreenPopup();
			showCommonAlert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"), null);
		}
	}
}
/**
 * Method to invoke service that uploads the new rc from FB list
 * @returns {}
 * ============================
	gblOffsetMB = "0";
	gblLimitMB = "0";
	gblStart = "0";
	================================
 */

function startRcFBFriendsListService() {
	//if (frmIBMyReceipentsAddContactFB.segmentSelectRc.data != null && frmIBMyReceipentsAddContactFB.segmentSelectRc.data.length > 0) {
//		offset = offset + frmIBMyReceipentsAddContactFB.segmentSelectRc.data.length;
//	} else {
//		offset = 0;
//	}

if(gblFBCode == null || gblFBCode==""){
	startIBFBFriendsService();
}else{
		var inputParams = {
			offset: gblOffsetMB,
			limit: gblLimitMB,
			search: gblSearch,
			start: gblStart,
			httpheaders: {},
			httpconfigs: {},
			character: "0"
		};
		showLoadingScreenPopup();
		try {
			invokeServiceSecureAsync("getFriends", inputParams, startRcFBFriendsListServiceAsyncCallback);
			//FBTimer();
			//gblFBLoginSameThreadTrack = false;
		} catch (e) {
			// todo: handle exception
			invokeCommonIBLogger("Exception in invoking service");
		}
	}
}
/**
 * Callback method for startRcFBFriendsListService()
 * @returns {}
 */
 
 
 function startRcFBFriendsListServiceAfter() {
	//if (frmIBMyReceipentsAddContactFB.segmentSelectRc.data != null && frmIBMyReceipentsAddContactFB.segmentSelectRc.data.length > 0) {
//		offset = offset + frmIBMyReceipentsAddContactFB.segmentSelectRc.data.length;
//	} else {
//		offset = 0;
//	}
       
		var fbinputs={};
		invokeServiceSecureAsync("getfbUserCode", fbinputs, getfbcodecalback);
		
		var inputParams = {
			offset: gblOffsetMB,
			limit: gblLimitMB,
			search: gblSearch,
			start: gblStart,
			httpheaders: {},
			httpconfigs: {},
			character: "0"
		};
		showLoadingScreenPopup();
		
		try {
			invokeServiceSecureAsync("getFriends", inputParams, startRcFBFriendsListServiceAsyncCallback);
			
			//FBTimer();
			//gblFBLoginSameThreadTrack = false;
		} catch (e) {
			// todo: handle exception
			invokeCommonIBLogger("Exception in invoking service");
			
		}
}
 

//var FBTimerCount = 0;

function startRcFBFriendsListServiceAsyncCallback(status, callBackResponse) {

   
	var currentFormId = kony.application.getCurrentForm().id;
	kony.print("form id "+currentFormId);
	
	
	if (status == 400) {
	
	var opStatus = callBackResponse["opstatus"];	
		if (callBackResponse["opstatus"] == 0) {
		    
			dismissLoadingScreenPopup();
			//gblFBCode=getFriends["facebookCode"];
          
			if (callBackResponse["invokeServlet"] == "yes") {
			
				if(gblFBIBStateTrack == true){
					//var form = kony.application.getPreviousForm();
					//form.show();
                  
					if(gblfbSelectionState == 0){					
						frmIBMyReceipentsHome.show();
					}
					else{					   
						frmIBMyReceipentsAddContactManually.show();
					}
					gblFBIBStateTrack = false;					 
					return;
				}
				else{
					//gblFBLoginSameThreadTrack = true;

					startIBFBFriendsService();
				}
			} else {
				//Wait for friends list service call invocation
               
				var searchText = frmIBMyReceipentsAddContactFB.tbxSelectRcSearch.text;				
				
                var searchTextLength = searchText.length; 
               
                               
				if(searchTextLength == 0){
				
					frmIBMyReceipentsAddContactFB.show();
					startRcFBFriendsListServiceIfIdPresentAsyncCallback(callBackResponse);
				}
				else{
				  
				   startRcFBFriendsListServiceIfIdPresentAsyncCallback(callBackResponse);
				}
					
			}
		} else {
			frmIBMyReceipentsAddContactFB.linkMore.setVisibility(false);
			dismissLoadingScreenPopup();
			currentFormId.lblFbNoFriends.setVisibility(true);
			frmIBMyReceipentsAddContactFB.btnSelectRcNext.setVisibility(false);
			currentFormId.lblFbNoFriends.text = kony.i18n.getLocalizedString("Receipent_alert_NoFacebookFriends");
			if (currentFormId.segmentReceipentListing.data != null && currentFormId.segmentReceipentListing.data.length > 0) {
				currentFormId.segmentSelectRc.removeAll();
			}
			
			
		}
	} else {
		if (status == 300) {
		
			frmIBMyReceipentsAddContactFB.linkMore.setVisibility(false);
			dismissLoadingScreenPopup();
 			currentFormId.lblFbNoFriends.setVisibility(true);
			frmIBMyReceipentsAddContactFB.btnSelectRcNext.setVisibility(false);
			currentFormId.lblFbNoFriends.text = kony.i18n.getLocalizedString("Receipent_alert_NoFacebookFriends");
			if (currentFormId.segmentReceipentListing.data != null && currentFormId.segmentReceipentListing.data.length > 0) {
				currentFormId.segmentSelectRc.removeAll();
			}
		}
	}
	gblFBIBStateTrack = false;
	
}
/**
 * Method to start the service that request OTP
 * @returns {}
 */

function startOTPRequestService() {
    var locale = kony.i18n.getCurrentLocale();
    var eventNotificationPolicy;
    var SMSSubject;
    var Channel;
    var AccntDetailsMessage;
    var AccountName;
    var fbRc = [];
    // alert("in startOTPRequestService ");
     frmIBMyReceipentsAddContactManually.lblOTPinCurr.text = " ";//kony.i18n.getLocalizedString("invalidOTP"); //
     frmIBMyReceipentsAddContactManually.lblPlsReEnter.text = " "; 
     frmIBMyReceipentsAddContactManually.hbxOTPincurrect.isVisible = false;
     frmIBMyReceipentsAddContactManually.hbox101089555968194.isVisible = true;
     frmIBMyReceipentsAddContactManually.hbox101089555968200.isVisible = true;	
    if (gblAdd_Receipent_State == gblEXISTING_RC_EDITION || gblAdd_Receipent_State == gblEXISTING_RC_BANKACCNT_ADDITION) {
    	/*if (locale == "en_US") {
            eventNotificationPolicy = "MIB_EditRecipient_EN";
            SMSSubject = "MIB_EditRecipient_EN";
        } else {
            eventNotificationPolicy = "MIB_EditRecipient_TH";
            SMSSubject = "MIB_EditRecipient_TH";
        }*/
        Channel = "EditRecipient";
    } else if (gblAdd_Receipent_State == gblNEW_RC_ADDITION || gblAdd_Receipent_State == gblNEW_RC_ADDITION_PRECONFIRM) {
    	/*if (locale == "en_US") {
            eventNotificationPolicy = "MIB_AddRecipient_EN";
            SMSSubject = "MIB_AddRecipient_EN";
        } else {
            eventNotificationPolicy = "MIB_AddRecipient_TH";
            SMSSubject = "MIB_AddRecipient_TH";
        }*/
        Channel = "AddRecipient";
    }
    var em = frmIBMyReceipentsAddContactManually.txtemail.text;
    var ph = frmIBMyReceipentsAddContactManually.txtmobnum.text;
    var fbid = frmIBMyReceipentsAddContactManually.lblfbidstudio19.text;//Modified by Studio Viz
    if (em == "Not Available" || em == "" || em == undefined || em == null || em == "No Mail") {
        em = "";
    } else {
        em = "Email:" + em + ". ";
    }
    if (ph == "Not Available" || ph == null || ph == undefined || ph == "" || ph == "No Number") {
        ph = "";
    } else {
        ph = removeHyphenIB(ph);
        ph = "Mobile Phone No.= xxx-xxx-" + ph.substring(6, 10) + ". ";
    }
    if (fbid == "Not Available" || fbid == null || fbid == undefined || fbid == "") {
        fbid = "";
    } else {
        fbid = "Facebook:" + fbid + " ";
    }
    if (gblAdd_Receipent_State == gblEXISTING_RC_EDITION) {
        AccntDetailsMessage = "";
        AccountName = frmIBMyReceipentsAddContactManually.txtRecipientName.text;
        //CR 13 change start
        var fbId = frmIBMyReceipentsAddContactManually.tbxFbID.text;
        var mobNo = frmIBMyReceipentsAddContactManually.txtmobnum.text;
        
        
          if (mobNo == null || mobNo == undefined || mobNo == "") {
            
         }else{
		  AccntDetailsMessage = "Mobile x" + mobNo.substring(6, 10) ;
            
	    }

       if (fbId == null || fbId == undefined || fbId == ""){
       }else{
       AccntDetailsMessage = AccntDetailsMessage+ " FB" + fbId.substring(0, 10);
       }
        //CR 13 changes end
    } else if (gblAdd_Receipent_State == gblEXISTING_RC_BANKACCNT_ADDITION) {
        //alert("gblEXISTING_RC_BANKACCNT_ADDITION")
        var noOfAccounts;

        /*if (frmIBMyReceipentsAddBankAccnt.segementTobeAddedAccnts.data != null)
			noOfAccounts = frmIBMyReceipentsAddBankAccnt.segementTobeAddedAccnts.data.length;
		else
			noOfAccounts = 0;
		if (noOfAccounts == 0) {
			AccountName = "No Accounts";
		} else {
			AccountName = "Multiple Accounts";
		}
		*/
        AccountName = frmIBMyReceipentsAddBankAccnt.lblRcName.text;
        ph = frmIBMyReceipentsAddBankAccnt.lblAddRcConfMobileNo.text;
        em = frmIBMyReceipentsAddBankAccnt.lblAddRcConfEmail.text;
        fbid = frmIBMyReceipentsAddBankAccnt.lblAddRcConfFbId.text;
        if (em == "Not Available" || em == "" || em == undefined || em == null || em == "No Mail") {
            em = "";
        } else {
            em = "Email:" + em + ". ";
        }
        if (ph == "Not Available" || ph == null || ph == undefined || ph == "" || ph == "No Number") {
            ph = "";
        } else {
            ph = removeHyphenIB(ph);
            ph = "Mobile Phone No.= xxx-xxx-" + ph.substring(6, 10) + ". ";
        }
        if (fbid == "Not Available" || fbid == null || fbid == undefined || fbid == "") {
            fbid = "";
        } else {
            fbid = "Facebook:" + fbid + " ";
        }
        AccntDetailsMessage = "";
        AccntDetailsMessage = AccntDetailsMessage + getAccountDetailMessageRc(frmIBMyReceipentsAddBankAccnt.segementTobeAddedAccnts.data)
        AccountName = frmIBMyReceipentsAddBankAccnt.lblRcName.text;

    } else if (gblAdd_Receipent_State == gblNEW_RC_ADDITION || gblAdd_Receipent_State == gblNEW_RC_ADDITION_PRECONFIRM) {
        if (isAddFb) {
            fbRc = frmIBMyReceipentsAddContactFB.segmentSelectRcConf.data;
            AccountName = "" //frmIBMyReceipentsAddContactManually.lblrecipientname.text;
            var AccntDetailsMessage2 = "";
            var rcount = 0;
            if (fbRc != null && fbRc != undefined) {
                //Commented for CR 13
                /*for (var t = 0; t < fbRc.length; t++) {
                    AccntDetailsMessage2 = AccntDetailsMessage2 + fbRc[t]["lblSelectRcName"] + ", "
                }*/
                rcount = fbRc.length;
                if (rcount > 1) {
                    AccntDetailsMessage = rcount + "Recipients";
                } else {
                    AccntDetailsMessage = "" + fbRc[0]["facebookId"];
                }
            }
            //Commented for CR 13
            /*if (AccntDetailsMessage2 != null && AccntDetailsMessage2 != undefined) {
                AccntDetailsMessage2 = AccntDetailsMessage2.substring(0, AccntDetailsMessage2.length - 2)
            }
            AccntDetailsMessage = rcount + " Recipient (" + AccntDetailsMessage2 + ").";*/
        } else {
        	AccountName = frmIBMyReceipentsAddContactManually.lblrecipientname.text;
            var phn = frmIBMyReceipentsAddContactManually.lblmobnum.text
            if (phn == "Not Available" || phn == null || phn == undefined || phn == "" || phn == "No Number") {
                phn = "";
            } else {
                phn = removeHyphenIB(phn);
                phn = "Mobile x" + phn.substring(6, 10);
            }
            AccntDetailsMessage = "";
            //Commented for CR13
            // AccntDetailsMessage = AccntDetailsMessage + getAccountDetailMessageRc(frmIBMyReceipentsAddContactManually.segmentRcAccounts.data)
            if (frmIBMyReceipentsAddContactManually.segmentRcAccounts.data.length == 0) AccntDetailsMessage = phn + "";
            else AccntDetailsMessage = AccntDetailsMessage + getAccountDetailMessageRc(frmIBMyReceipentsAddContactManually.segmentRcAccounts.data) + phn + "";
        }
    }
    
    if(AccountName != null && AccountName.length > 13)
      AccountName = AccountName.substring(0, 13); //CR13 change

    var inputParams = {
        retryCounterRequestOTP: gblRetryCountRequestOTP,
        Channel: Channel,
        locale: locale,
        AccDetailMsg: AccntDetailsMessage,
        userAccountName: AccountName
    };
    showLoadingScreenPopup();
    try {
        //invokeServiceSecureAsync("generateOTP", inputParams, startOTPRequestServiceAsyncCallback);
        invokeServiceSecureAsync("generateOTPWithUser", inputParams, startOTPRequestServiceAsyncCallback);
    } catch (e) {
        // todo: handle exception
        invokeCommonIBLogger("Exception in invoking service");
    }
}
/**
 * Callback method for startOTPRequestService
 * @param {} status
 * @param {} callBackResponse
 * @returns {}
 */

function startOTPRequestServiceAsyncCallback(status, callBackResponse) {
	if (status == 400) {
		if (callBackResponse["errCode"] == "GenOTPRtyErr00001") {
            dismissLoadingScreenPopup();
            showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
            return false;
	    }
		if (callBackResponse["opstatus"] == 0) {
			
			if (callBackResponse["errCode"] == "GenOTPRtyErr00002") {
				dismissLoadingScreenPopup();
				showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (callBackResponse["errCode"] == "JavaErr00001") {
				dismissLoadingScreenPopup();
				showAlert(kony.i18n.getLocalizedString("ECJavaErr00001"), kony.i18n.getLocalizedString("info"));
				return false;
			}
			gblOTPReqLimit=kony.os.toNumber(callBackResponse["otpRequestLimit"]);
			var reqOtpTimer = kony.os.toNumber(callBackResponse["requestOTPEnableTime"]);
			gblRetryCountRequestOTP = kony.os.toNumber(callBackResponse["retryCounterRequestOTP"]);
			gblOTPLENGTH = kony.os.toNumber(callBackResponse["otpLength"]);
			
			try {
				kony.timer.cancel("OTPTimer");
			} catch (e) {
				// todo: handle exception
			}
			kony.timer.schedule("OtpTimer", OTPTimercallbackRc, reqOtpTimer, false);
		 if (gblAdd_Receipent_State == gblEXISTING_RC_EDITION || gblAdd_Receipent_State == gblNEW_RC_ADDITION || gblAdd_Receipent_State == gblNEW_RC_ADDITION_PRECONFIRM) {
			if(isAddFb){
				frmIBMyReceipentsAddContactFB.hbox476047582127699.setVisibility(true);
				frmIBMyReceipentsAddContactFB.hbxOTPsnt.setVisibility(true);
				frmIBMyReceipentsAddContactFB.textbox247428873338513.setFocus(true);
				frmIBMyReceipentsAddContactFB.btnOTPReq.skin = btn100Disabled;
				frmIBMyReceipentsAddContactFB.btnOTPReq.focusSkin = btn100Disabled;
				frmIBMyReceipentsAddContactFB.btnOTPReq.setEnabled(false);
				frmIBMyReceipentsAddContactFB.lblBankRef.setVisibility(true);
				frmIBMyReceipentsAddContactFB.label476047582127701.setVisibility(true);
				frmIBMyReceipentsAddContactFB.label476047582115277.setVisibility(true);
				frmIBMyReceipentsAddContactFB.label476047582115279.setVisibility(true);
				//	frmIBMyReceipentsAddContactFB.lblBankRef.text=kony.i18n.getLocalizedString("Bank Ref. No");
			var refVal="";
			for(var d=0;d<callBackResponse["Collection1"].length;d++){
				if(callBackResponse["Collection1"][d]["keyName"] == "pac"){
						refVal=callBackResponse["Collection1"][d]["ValueString"];
						break;
					}
				}
				frmIBMyReceipentsAddContactFB.label476047582127701.text =refVal// callBackResponse["Collection1"][8]["ValueString"];
				frmIBMyReceipentsAddContactFB.label476047582115277.text = kony.i18n.getLocalizedString("keyotpmsg");
				frmIBMyReceipentsAddContactFB.label476047582115279.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
			}else{
				
				frmIBMyReceipentsAddContactManually.hbox101089555968194.setVisibility(true);
				frmIBMyReceipentsAddContactManually.hbox101089555968200.setVisibility(true);
				frmIBMyReceipentsAddContactManually.btnotp.skin = btn100Disabled;
				frmIBMyReceipentsAddContactManually.btnOTPReq.focusSkin = btn100Disabled;
				frmIBMyReceipentsAddContactManually.txtotp.setFocus(true);
				frmIBMyReceipentsAddContactManually.btnotp.setEnabled(false);
				frmIBMyReceipentsAddContactManually.lblref.setVisibility(true);
				frmIBMyReceipentsAddContactManually.lblrefvalue.setVisibility(true);
				frmIBMyReceipentsAddContactManually.lblotpsent.setVisibility(true);
				frmIBMyReceipentsAddContactManually.lblotpmob.setVisibility(true);
				
				//frmIBMyReceipentsAddContactManually.lblref.text=kony.i18n.getLocalizedString("Bank Ref. No");
			var refVal="";
			for(var d=0;d<callBackResponse["Collection1"].length;d++){
				if(callBackResponse["Collection1"][d]["keyName"] == "pac"){
						refVal=callBackResponse["Collection1"][d]["ValueString"];
						break;
					}
				}
				frmIBMyReceipentsAddContactManually.lblrefvalue.text =refVal// callBackResponse["Collection1"][8]["ValueString"];
				frmIBMyReceipentsAddContactManually.lblotpsent.text = kony.i18n.getLocalizedString("keyotpmsg");
				frmIBMyReceipentsAddContactManually.lblotpmob.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
				dismissLoadingScreenPopup();
				frmIBMyReceipentsAddContactManually.txtotp.setFocus(true);
				}
				
			} else if (gblAdd_Receipent_State == gblEXISTING_RC_BANKACCNT_ADDITION || gblAdd_Receipent_State ==
				gblEXISTING_RC_BANKACCNT_EDITION) {
				dismissLoadingScreenPopup();
				frmIBMyReceipentsAddBankAccnt.hbox476047582127699.setVisibility(true);
				frmIBMyReceipentsAddBankAccnt.hbxOTPsnt.setVisibility(true);
				frmIBMyReceipentsAddBankAccnt.textbox247428873338513.setFocus(true);
				frmIBMyReceipentsAddBankAccnt.btnOTPReq.skin = btn100Disabled;
				frmIBMyReceipentsAddBankAccnt.btnOTPReq.focusSkin = btn100Disabled;
				frmIBMyReceipentsAddBankAccnt.btnOTPReq.setEnabled(false);
				frmIBMyReceipentsAddBankAccnt.lblBankRef.setVisibility(true);
				frmIBMyReceipentsAddBankAccnt.label476047582127701.setVisibility(true);
				frmIBMyReceipentsAddBankAccnt.label476047582115277.setVisibility(true);
				frmIBMyReceipentsAddBankAccnt.label476047582115279.setVisibility(true);
				//	frmIBMyReceipentsAddBankAccnt.lblBankRef.text=kony.i18n.getLocalizedString("Bank Ref. No");
				var refVal="";
			for(var d=0;d<callBackResponse["Collection1"].length;d++){
				if(callBackResponse["Collection1"][d]["keyName"] == "pac"){
						refVal=callBackResponse["Collection1"][d]["ValueString"];
						break;
					}
				}
				frmIBMyReceipentsAddBankAccnt.label476047582127701.text = refVal //callBackResponse["Collection1"][8]["ValueString"];
				frmIBMyReceipentsAddBankAccnt.label476047582115277.text = kony.i18n.getLocalizedString("keyotpmsg");
				frmIBMyReceipentsAddBankAccnt.label476047582115279.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
			} else if (gblfbSelectionState == 0) {
				
				frmIBMyReceipentsAddContactFB.hbox476047582127699.setVisibility(true);
				frmIBMyReceipentsAddContactFB.hbxOTPsnt.setVisibility(true);
				
				frmIBMyReceipentsAddContactFB.textbox247428873338513.setFocus(true);
				frmIBMyReceipentsAddContactFB.btnOTPReq.skin = btn100Disabled;
				frmIBMyReceipentsAddContactFB.btnOTPReq.focusSkin = btn100Disabled;
				frmIBMyReceipentsAddContactFB.btnOTPReq.setEnabled(false);
				frmIBMyReceipentsAddContactFB.lblBankRef.setVisibility(true);
				frmIBMyReceipentsAddContactFB.label476047582127701.setVisibility(true);
				frmIBMyReceipentsAddContactFB.label476047582115277.setVisibility(true);
				frmIBMyReceipentsAddContactFB.label476047582115279.setVisibility(true);
				//	frmIBMyReceipentsAddContactFB.lblBankRef.text=kony.i18n.getLocalizedString("Bank Ref. No");
			var refVal="";
			for(var d=0;d<callBackResponse["Collection1"].length;d++){
				if(callBackResponse["Collection1"][d]["keyName"] == "pac"){
						refVal=callBackResponse["Collection1"][d]["ValueString"];
						break;
					}
				}
				frmIBMyReceipentsAddContactFB.label476047582127701.text =refVal// callBackResponse["Collection1"][8]["ValueString"];
				frmIBMyReceipentsAddContactFB.label476047582115277.text = kony.i18n.getLocalizedString("keyotpmsg");
				frmIBMyReceipentsAddContactFB.label476047582115279.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
				dismissLoadingScreenPopup();
				var currFrm =  kony.application.getCurrentForm();
				if(currFrm == "frmIBMyReceipentsAddContactFB"){
				  frmIBMyReceipentsAddContactFB.textbox247428873338513.setFocus(true);
				}else if(currFrm == "frmIBMyReceipentsAddContactManually"){
				  frmIBMyReceipentsAddContactManually.txtotp.setFocus(true);
				}
				
			}
		//	dismissLoadingScreenPopup();
		} else {
			dismissLoadingScreenPopup();
			//showCommonAlert(callBackResponse["errMsg"], null);
				if(callBackResponse["errMsg"] != undefined)
	        	{
	        		alert(callBackResponse["errMsg"]);
	        	}
	        	else
	        	{
	        		alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
	        	}		
		}
	} else {
		if (status == 300) {
			dismissLoadingScreenPopup();
			//showCommonAlert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"), null);
		}
	}
}
/**
 * Method to invoke service that validates the OTP
 * @returns {}
 */

function startOTPValidationService(otptext) {
	executeRecipient(otptext)
}


/**
 * Method to invoke service that edits the existing rc bank data
 * @returns {}
 */

function startRcSetFavService(gblcrmId, receipentId, isfav, rcname, pic, mailid, mobNum, fbId) {
	invokeCommonIBLogger("Favorite flag is" + isfav);
	var inputParams = {
		crmId: gblcrmId,
		personalizedId: receipentId,
		personalizedName: rcname,
		personalizedPictureId: pic,
		personalizedMailId: mailid,
		personalizedMobileNumber: mobNum,
		personalizedFacebookId: fbId,
		personalizedStatus: "Added",
		favoriteFlag: isfav
	};
	showLoadingScreenPopup();
	try {
		invokeServiceSecureAsync("receipentSetFavService", inputParams, startRcSetFavServiceAsyncCallback);
	} catch (e) {
		// todo: handle exception
		invokeCommonIBLogger("Exception in invoking service");
	}
}
/**
 * Callback method for startRcSetFavService
 * @param {}
 * @returns {}
 */

function startRcSetFavServiceAsyncCallback(status, callBackResponse) {
	if (status == 400) {
		if (callBackResponse["opstatus"] == 0) {
			if (callBackResponse["errMsg"] != null && callBackResponse["errMsg"] != "") {
				showCommonAlert(callBackResponse["errMsg"], null);
			} else {
				invalidateRcListingSortbyFav(formId);
				dismissLoadingScreenPopup();
			}
		} else {
			dismissLoadingScreenPopup();
			showCommonAlert(callBackResponse["errMsg"], null);
		}
	} else {
		if (status == 300) {
			dismissLoadingScreenPopup();
			showCommonAlert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"), null);
		}
	}
}
/**
 * Method to invoke deposit account enquiry
 * @returns {}
 */

function startDepositAccountEnquiryService() {
	var toAccnt = decodeAccountNumbers(frmIBMyReceipentsAddBankAccnt.tbxAddAccntNumber.text);
	if (toAccnt[3] == "2" || toAccnt[3] == "7" || toAccnt[3] == "9") {
		//dont delete this
	} else if (toAccnt[3] == "1") {
		//dont delete this
	} else if (toAccnt[3] == "3") {
		//dont delete this
	} else if (toAccnt[3] == "5" || toAccnt[3] == "6") {
		showCommonAlert("" + kony.i18n.getLocalizedString("Receipent_alert_correctdetails"), null);
		return;
	} else {
		showCommonAlert("" + kony.i18n.getLocalizedString("Receipent_alert_correctdetails"), null);
		return;
	}
	var inputParams = {
		acctId: toAccnt,
		toAcctNo:toAccnt,
		isFromRecipient: "yes",
		bankcode :"11"
	};
	showLoadingScreenPopup();
	try {
		invokeServiceSecureAsync("depositAccountInquiryNonSec", inputParams, startDepositAccountEnquiryServiceAsyncCallback);
	} catch (e) {
		// todo: handle exception
		invokeCommonIBLogger("Exception in invoking service");
	}
}
/**
 * Callback method for startDepositAccountEnquiryService
 * @param {}
 * @returns {}
 */

function startDepositAccountEnquiryServiceAsyncCallback(status, callBackResponse) {
	if (status == 400) {
		dismissLoadingScreenPopup();
		if (callBackResponse["opstatus"] == 0) {
			dismissLoadingScreenPopup();
			if (callBackResponse["StatusCode"] == 0) {
				if (callBackResponse["accountTitle"] != null && callBackResponse["accountTitle"] != "") {
					proceedOnSuccesssfulVerification(callBackResponse["accountTitle"]);
				} else {
					showCommonAlert("" + kony.i18n.getLocalizedString("Receipent_alert_correctdetails"), null);
				}
			} else {
				//showCommonAlert(callBackResponse["errMsg"], callBackResponse["XPServerStatCode"]);
				showCommonAlert("" + kony.i18n.getLocalizedString("Receipent_alert_correctdetails"), null);
			}
		} 
		else if(callBackResponse["opstatus"] == 1){
			if(callBackResponse["errMsg"] != undefined)
	        	{
	        		alert(callBackResponse["errMsg"]);
	        	}
	        	else
	        	{
	        		alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
	        	}	
		}
		else if(callBackResponse["opstatus"] == 2){
	        		alert(kony.i18n.getLocalizedString("keyReceipent_alert_MEAccountAdd"));
		}
		else {
			dismissLoadingScreenPopup();
			showCommonAlert("" + kony.i18n.getLocalizedString("Receipent_alert_correctdetails"), null);
		}
	} else {
		if (status == 300) {
			dismissLoadingScreenPopup();
			showCommonAlert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"), null);
		}
	}
}
/**
 * Method to invoke ORFT account enquiry
 * @returns {}
 */

function startORFTAccountEnquiryService() {
	try {
	  	var inputParams = {}
		var toAccnt = decodeAccountNumbers(frmIBMyReceipentsAddBankAccnt.tbxAddAccntNumber.text);
		var toAccntBnkCde =frmIBMyReceipentsAddBankAccnt.comboBankName.selectedKey;
		inputParams["toFIIdent"]= toAccntBnkCde;
		inputParams["isFromRecipient"]= "yes";
		inputParams["toAcctNo"]= toAccnt;
		inputParams["bankcode"]= toAccntBnkCde;
		showLoadingScreenPopup();
		invokeServiceSecureAsync("ORFTInq", inputParams, startORFTAccountEnquiryServiceAsyncCallback);
	} catch (e) {
		invokeCommonIBLogger("Exception in invoking service");
	}
}
/**
 * Callback method for startORFTAccountEnquiryService
 * @param {}
 * @returns {}
 */

function startORFTAccountEnquiryServiceAsyncCallback(status, callBackResponse) {
	if (status == 400) {
		if (callBackResponse["opstatus"] == 0) {
			dismissLoadingScreenPopup();
			if (callBackResponse["StatusCode"] == 0) {
				invokeCommonIBLogger("To Account name " + callBackResponse["ORFTTrnferInqRs"][0]["toAcctName"]);
				if (callBackResponse["ORFTTrnferInqRs"][0]["toAcctName"] != null && callBackResponse["ORFTTrnferInqRs"][0]["toAcctName"] != "") {
					proceedOnSuccesssfulVerification(callBackResponse["ORFTTrnferInqRs"][0]["toAcctName"]);
				} else {
					/*if (callBackResponse["errMsg"] == undefined || callBackResponse["errMsg"] == null)
							showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
						else
							alert(" " + callBackResponse["errMsg"]);*///Ticket#28938
					proceedOnSuccesssfulVerification("");
							
				}
			} else {
				if (callBackResponse["errMsg"] == undefined || callBackResponse["errMsg"] == null)
							showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
						else
							alert(" " + callBackResponse["errMsg"]);
			}
		} else if(callBackResponse["opstatus"]==1){
			dismissLoadingScreenPopup();
			if (callBackResponse["errMsg"] == undefined || callBackResponse["errMsg"] == null)
							showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
						else
							alert(" " + callBackResponse["errMsg"]);
		}
		else{
			dismissLoadingScreenPopup();
			if (callBackResponse["errMsg"] == undefined || callBackResponse["errMsg"] == null)
							showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
						else
							alert(" " + callBackResponse["errMsg"]);
		}
	} else {
		if (status == 300) {
			dismissLoadingScreenPopup();
			showCommonAlert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"), null);
		}
	}
}
/**
 * Method to start the validation service before proceeding
 * @returns {}
 */

function startRCTransactionSecurityValidationService() {
	if (securityValFlag == 0) {
		var inputParams = {
			crmId: gblcrmId,
			rqUUId: "",
			channelName: "IB-INQ"
		};
		showLoadingScreenPopup();
		try {
			invokeServiceSecureAsync("crmProfileInq", inputParams, startRCTransactionSecurityValidationServiceAsyncCallback);
		} catch (e) {
			// todo: handle exception
			invokeCommonIBLogger("Exception in invoking service");
		}
	} else {
		disableOtpBoxUi();
		switch (parseInt(securityValFlag.toString())) {
		case 1:
			{
				if (gblAdd_Receipent_State == gblEXISTING_RC_EDITION) {
					frmIBMyReceipentsAddContactManually.label476047582115288.text = kony.i18n.getLocalizedString("Receipent_tokenId");
					frmIBMyReceipentsAddContactManually.label476047582115284.text = kony.i18n.getLocalizedString("Receipent_Token");
					frmIBMyReceipentsAddContactManually.btnOTPReq.text = kony.i18n.getLocalizedString("Receipent_SwitchtoOTP");
				} else if (gblAdd_Receipent_State == gblEXISTING_RC_BANKACCNT_ADDITION || gblAdd_Receipent_State ==
					gblEXISTING_RC_BANKACCNT_EDITION) {
					frmIBMyReceipentsAddBankAccnt.label476047582115288.text = kony.i18n.getLocalizedString("Receipent_tokenId");
					frmIBMyReceipentsAddBankAccnt.label476047582115284.text = kony.i18n.getLocalizedString("Receipent_Token");
					frmIBMyReceipentsAddBankAccnt.btnOTPReq.text = kony.i18n.getLocalizedString("Receipent_SwitchtoOTP");
				} else if (gblAdd_Receipent_State == gblNEW_RC_ADDITION) {
					if (parseInt(formId.toString()) == 3) {
						frmIBMyReceipentsAddContactFB.label476047582115288.text = kony.i18n.getLocalizedString("Receipent_tokenId");
						frmIBMyReceipentsAddContactFB.label476047582115284.text = kony.i18n.getLocalizedString("Receipent_Token");
						frmIBMyReceipentsAddContactFB.btnOTPReq.text = kony.i18n.getLocalizedString("Receipent_SwitchtoOTP");
					} else {
						frmIBMyReceipentsAddContactManually.otpmsglbl.text = kony.i18n.getLocalizedString("Receipent_tokenId");
						frmIBMyReceipentsAddContactManually.lblOTP.text = kony.i18n.getLocalizedString("Receipent_Token");
						frmIBMyReceipentsAddContactManually.btnotp.text = kony.i18n.getLocalizedString("Receipent_SwitchtoOTP");
					    frmIBMyReceipentsAddContactManually.txtotp.setFocus(true);
					}
				} else {
					frmIBMyReceipentsAddContactFB.label476047582115288.text = kony.i18n.getLocalizedString("Receipent_tokenId");
					frmIBMyReceipentsAddContactFB.label476047582115284.text = kony.i18n.getLocalizedString("Receipent_Token");
					frmIBMyReceipentsAddContactFB.btnOTPReq.text = kony.i18n.getLocalizedString("Receipent_SwitchtoOTP");
				}
				//startBackGroundTokenRequestService();
				tokenFlag = true;
			}
			break;
		case 2:
			{
				if (gblAdd_Receipent_State == gblEXISTING_RC_EDITION) {
					frmIBMyReceipentsAddContactManually.label476047582115288.text = kony.i18n.getLocalizedString("keyotpmsgreq");
					frmIBMyReceipentsAddContactManually.label476047582115284.text = kony.i18n.getLocalizedString("Receipent_OTP");
					frmIBMyReceipentsAddContactManually.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
				} else if (gblAdd_Receipent_State == gblEXISTING_RC_BANKACCNT_ADDITION || gblAdd_Receipent_State ==
					gblEXISTING_RC_BANKACCNT_EDITION) {
					frmIBMyReceipentsAddBankAccnt.label476047582115288.text = kony.i18n.getLocalizedString("keyotpmsgreq");
					frmIBMyReceipentsAddBankAccnt.label476047582115284.text = kony.i18n.getLocalizedString("Receipent_OTP");
					frmIBMyReceipentsAddBankAccnt.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
				} else if (gblAdd_Receipent_State == gblNEW_RC_ADDITION) {
					if (parseInt(formId.toString()) == 3) {
						frmIBMyReceipentsAddContactFB.label476047582115288.text = kony.i18n.getLocalizedString("keyotpmsgreq");
						frmIBMyReceipentsAddContactFB.label476047582115284.text = kony.i18n.getLocalizedString("Receipent_OTP");
						frmIBMyReceipentsAddContactFB.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
					} else {
						frmIBMyReceipentsAddContactManually.otpmsglbl.text = kony.i18n.getLocalizedString("keyotpmsgreq");
						frmIBMyReceipentsAddContactManually.lblOTP.text = kony.i18n.getLocalizedString("Receipent_OTP");;
						frmIBMyReceipentsAddContactManually.btnotp.text = kony.i18n.getLocalizedString("keyRequest");
					}
				} else {
					frmIBMyReceipentsAddContactFB.label476047582115288.text = kony.i18n.getLocalizedString("keyotpmsgreq");
					frmIBMyReceipentsAddContactFB.label476047582115284.text = kony.i18n.getLocalizedString("Receipent_OTP");
					frmIBMyReceipentsAddContactFB.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
				}
				//startBackGroundOTPRequestService();
				tokenFlag = false;
			}
			break;
		default:
			break;
		}
	}
}
/**
 * Callback method for startRCTransactionSecurityValidationService()
 * @returns {}
 */

function startRCTransactionSecurityValidationServiceAsyncCallback(status, callBackResponse) {
	if (status == 400) {
		if (callBackResponse["opstatus"] == 0) {
			storeRcCrmInqData(callBackResponse);
			if (callBackResponse["tokenDeviceFlag"] == "N") {
				tokenFlag = false;
				securityValFlag = 2;
				if (gblAdd_Receipent_State == gblEXISTING_RC_EDITION) {
					frmIBMyReceipentsAddContactManually.hbox476047582127699.setVisibility(true);
					frmIBMyReceipentsAddContactManually.hbxOTPsnt.setVisibility(true);
					frmIBMyReceipentsAddContactManually.txtotp.setFocus(true);
					frmIBMyReceipentsAddContactManually.label476047582115288.text = kony.i18n.getLocalizedString("keyotpmsgreq");
					frmIBMyReceipentsAddContactManually.label476047582115284.text = kony.i18n.getLocalizedString("Receipent_OTP");
					frmIBMyReceipentsAddContactManually.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
				} else if (gblAdd_Receipent_State == gblEXISTING_RC_BANKACCNT_ADDITION || gblAdd_Receipent_State ==
					gblEXISTING_RC_BANKACCNT_EDITION) {
					frmIBMyReceipentsAddBankAccnt.hbox476047582127699.setVisibility(true);
					frmIBMyReceipentsAddBankAccnt.hbxOTPsnt.setVisibility(true);
					frmIBMyReceipentsAddBankAccnt.textbox247428873338513.setFocus(true);
					frmIBMyReceipentsAddBankAccnt.label476047582115288.text = kony.i18n.getLocalizedString("keyotpmsgreq");
					frmIBMyReceipentsAddBankAccnt.label476047582115284.text = kony.i18n.getLocalizedString("Receipent_OTP");
					frmIBMyReceipentsAddBankAccnt.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
				} else if (gblAdd_Receipent_State == gblNEW_RC_ADDITION) {
					if (parseInt(formId.toString()) == 3) {
						frmIBMyReceipentsAddContactFB.hbox476047582127699.setVisibility(true);
						frmIBMyReceipentsAddContactFB.hbxOTPsnt.setVisibility(true);
						frmIBMyReceipentsAddContactFB.textbox247428873338513.setFocus(true);
						frmIBMyReceipentsAddContactFB.label476047582115288.text = kony.i18n.getLocalizedString("keyotpmsgreq");
						frmIBMyReceipentsAddContactFB.label476047582115284.text = kony.i18n.getLocalizedString("Receipent_OTP");
						frmIBMyReceipentsAddContactFB.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
					} else {
						frmIBMyReceipentsAddContactManually.hbox101089555968194.setVisibility(true);
						frmIBMyReceipentsAddContactManually.hbox101089555968200.setVisibility(true);
						frmIBMyReceipentsAddContactManually.otpmsglbl.text = kony.i18n.getLocalizedString("keyotpmsgreq");
						frmIBMyReceipentsAddContactManually.lblOTP.text = kony.i18n.getLocalizedString("Receipent_OTP");;
						frmIBMyReceipentsAddContactManually.btnotp.text = kony.i18n.getLocalizedString("keyRequest");
					}
				} else {
					frmIBMyReceipentsAddContactFB.hbox476047582127699.setVisibility(true);
					frmIBMyReceipentsAddContactFB.hbxOTPsnt.setVisibility(true);
					frmIBMyReceipentsAddContactFB.textbox247428873338513.setFocus(true);
					frmIBMyReceipentsAddContactFB.label476047582115288.text = kony.i18n.getLocalizedString("keyotpmsgreq");
					frmIBMyReceipentsAddContactFB.label476047582115284.text = kony.i18n.getLocalizedString("Receipent_OTP");
					frmIBMyReceipentsAddContactFB.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
				}
				//startBackGroundOTPRequestService();
			} else {
				tokenFlag = true;
				securityValFlag = 1;
				if (gblAdd_Receipent_State == gblEXISTING_RC_EDITION) {
					frmIBMyReceipentsAddContactManually.hbox476047582127699.setVisibility(false);
					frmIBMyReceipentsAddContactManually.hbxOTPsnt.setVisibility(false);
					frmIBMyReceipentsAddContactManually.label476047582115288.text = kony.i18n.getLocalizedString("Receipent_tokenId");
					frmIBMyReceipentsAddContactManually.label476047582115284.text = kony.i18n.getLocalizedString("Receipent_Token");
					frmIBMyReceipentsAddContactManually.btnOTPReq.text = kony.i18n.getLocalizedString("Receipent_SwitchtoOTP");
				} else if (gblAdd_Receipent_State == gblEXISTING_RC_BANKACCNT_ADDITION || gblAdd_Receipent_State ==
					gblEXISTING_RC_BANKACCNT_EDITION) {
					frmIBMyReceipentsAddBankAccnt.hbox476047582127699.setVisibility(false);
					frmIBMyReceipentsAddBankAccnt.hbxOTPsnt.setVisibility(false);
					frmIBMyReceipentsAddBankAccnt.label476047582115288.text = kony.i18n.getLocalizedString("Receipent_tokenId");
					frmIBMyReceipentsAddBankAccnt.label476047582115284.text = kony.i18n.getLocalizedString("Receipent_Token");
					frmIBMyReceipentsAddBankAccnt.btnOTPReq.text = kony.i18n.getLocalizedString("Receipent_SwitchtoOTP");
				} else if (gblAdd_Receipent_State == gblNEW_RC_ADDITION) {
					if (parseInt(formId.toString()) == 3) {
						frmIBMyReceipentsAddContactFB.hbox476047582127699.setVisibility(false);
						frmIBMyReceipentsAddContactFB.hbxOTPsnt.setVisibility(false);
						frmIBMyReceipentsAddContactFB.label476047582115288.text = kony.i18n.getLocalizedString("Receipent_tokenId");
						frmIBMyReceipentsAddContactFB.label476047582115284.text = kony.i18n.getLocalizedString("Receipent_Token");
						frmIBMyReceipentsAddContactFB.btnOTPReq.text = kony.i18n.getLocalizedString("Receipent_SwitchtoOTP");
					} else {
						frmIBMyReceipentsAddContactManually.hbox101089555968194.setVisibility(false);
						frmIBMyReceipentsAddContactManually.hbox101089555968200.setVisibility(false);
						frmIBMyReceipentsAddContactManually.otpmsglbl.text = kony.i18n.getLocalizedString("Receipent_tokenId");
						frmIBMyReceipentsAddContactManually.lblOTP.text = kony.i18n.getLocalizedString("Receipent_Token");
						frmIBMyReceipentsAddContactManually.btnotp.text = kony.i18n.getLocalizedString("Receipent_SwitchtoOTP");
					    frmIBMyReceipentsAddContactManually.txtotp.setFocus(true);
					}
				} else {
					frmIBMyReceipentsAddContactFB.hbox476047582127699.setVisibility(false);
					frmIBMyReceipentsAddContactFB.hbxOTPsnt.setVisibility(false);
					frmIBMyReceipentsAddContactFB.label476047582115288.text = kony.i18n.getLocalizedString("Receipent_tokenId");
					frmIBMyReceipentsAddContactFB.label476047582115284.text = kony.i18n.getLocalizedString("Receipent_Token");
					frmIBMyReceipentsAddContactFB.btnOTPReq.text = kony.i18n.getLocalizedString("Receipent_SwitchtoOTP");
				}
				//startBackGroundTokenRequestService();
			}
		} else {
			dismissLoadingScreenPopup();
			
		}
	} else {
		if (status == 300) {
			dismissLoadingScreenPopup();
			
		}
	}
}

/**
 * Callback method for startIBRcCrmProInqService()
 * @returns {}
 */


/**
 * Method to invoke service to select the bank from the combo box
 * @returns {}
 */

function startDisplaySelectbankCacheService() {
	BANK_LOGO_URL = "https://" + appConfig.serverIp + "/" + appConfig.middlewareContext + "/Bank-logo/bank-logo-";
	RC_LOGO_URL = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext +
		"/ImageRender?billerId=&crmId=Y&personalizedId=";
	var inputParams = {};
	showLoadingScreenPopup();
	try {
		invokeServiceSecureAsync("receipentgetBankService", inputParams, startDisplaySelectbankCacheServiceAsyncCallback);
	} catch (e) {
		// todo: handle exception
		invokeCommonIBLogger("Exception in invoking service");
	}
}
/**
 * Callback method for startDisplaySelectbankCacheService
 * @param {} startDisplaySelectbankCacheService
 * @returns {}
 */

function startDisplaySelectbankCacheServiceAsyncCallback(status, callBackResponse) {
	var locale = kony.i18n.getCurrentLocale();
	if (status == 400) {
		if (callBackResponse["opstatus"] == 0) {
			var collectionData = callBackResponse["Results"];
			if (globalSelectBankData != null) {
				for (var i = 0; i < globalSelectBankData.length; i++) {
					globalSelectBankData.pop();
				}
			}
			var masterData = [];
			globalSelectBankData = [];
	//		var tempData = ["Bank", kony.i18n.getLocalizedString("keySelectBank")];
				var tempData = ["Bank", kony.i18n.getLocalizedString("keylblBankName")];
			globalSelectBankData.push(tempData);
				masterData.push(tempData);	
			for (var i = 0; i < collectionData.length; i++) {
				var tempData;
				var tempRecord;
				if ((collectionData[i].bankStatus.toUpperCase() == "ACTIVE") && (collectionData[i].smartFlag.toUpperCase() == "Y" || collectionData[i].orftFlag.toUpperCase() == "Y")){
				if (locale == "en_US") {
					tempData = [collectionData[i]["bankCode"], collectionData[i]["bankNameEng"], collectionData[i]["bankShortName"]];
				} else {
					
					tempData = [collectionData[i]["bankCode"], collectionData[i]["bankNameThai"],collectionData[i]["bankShortName"]];
				}
				if (locale == "en_US") {
					tempRecord = [collectionData[i]["bankCode"], collectionData[i]["bankNameEng"], collectionData[i]["bankShortName"], collectionData[i]["orftFlag"],
						collectionData[i]["smartFlag"],collectionData[i]["bankAccntLen"],collectionData[i]["bankAccntLengths"],collectionData[i]["bankNameEng"],collectionData[i]["bankNameThai"]];
				} else {
					tempRecord = [collectionData[i]["bankCode"], collectionData[i]["bankNameThai"], collectionData[i]["bankShortName"], collectionData[i]["orftFlag"],
						collectionData[i]["smartFlag"],collectionData[i]["bankAccntLen"],collectionData[i]["bankAccntLengths"],collectionData[i]["bankNameEng"],collectionData[i]["bankNameThai"]];
				}
				globalSelectBankData.push(tempRecord);
				masterData.push(tempData);
			}
			}
			dismissLoadingScreenPopup();
			var currentFormId = kony.application.getCurrentForm()
				.id;
			if (currentFormId == "frmIBMyReceipentsAddBankAccnt") {
				invokeCommonIBLogger("RESETTING BANK DATA>>>>");
				var selKey = frmIBMyReceipentsAddBankAccnt.comboBankName.selectedKey;
				frmIBMyReceipentsAddBankAccnt.comboBankName.masterData = masterData;
				frmIBMyReceipentsAddBankAccnt.comboBankName.selectedKey = selKey;
			}
		}
	} else {
		if (status == 300) {
			dismissLoadingScreenPopup();
			showCommonAlert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"), null);
		}
	}
}
/**
 * Method to store the crm inq data for reuse in crm profile update
 * @param {} collectionData
 * @returns {}
 */

function storeRcCrmInqData(collectionData) {
	//Dead method
}
/**
 * Method to validate the OTP generated for IB apply services
 * @returns {}
 */



/**
 * Method to start the service that request OTP in background
 * @returns {}
 */


function startBackGroundOTPRequestService() {
    //alert("startBackGroundOTPRequestService");
    disableOtpBoxUi();
    var locale = kony.i18n.getCurrentLocale();
    var eventNotificationPolicy;
    var SMSSubject;
    var Channel;
    var AccntDetailsMessage;
    var AccountName;
    var fbRc = [];
    
    
    
    if (gblAdd_Receipent_State == gblEXISTING_RC_EDITION || gblAdd_Receipent_State == gblEXISTING_RC_BANKACCNT_ADDITION) {
        /*if (locale == "en_US") {
            eventNotificationPolicy = "MIB_EditRecipient_EN";
            SMSSubject = "MIB_EditRecipient_EN";
        } else {
            eventNotificationPolicy = "MIB_EditRecipient_TH";
            SMSSubject = "MIB_EditRecipient_TH";
        }*/
        Channel = "EditRecipient";
    } else if (gblAdd_Receipent_State == gblNEW_RC_ADDITION || gblAdd_Receipent_State == gblNEW_RC_ADDITION_PRECONFIRM) {
    	/*if (locale == "en_US") {
            eventNotificationPolicy = "MIB_AddRecipient_EN";
            SMSSubject = "MIB_AddRecipient_EN";
        } else {
            eventNotificationPolicy = "MIB_AddRecipient_TH";
            SMSSubject = "MIB_AddRecipient_TH";
        }*/
        Channel = "AddRecipient";
    }
    var em = frmIBMyReceipentsAddContactManually.txtemail.text;
    var ph = frmIBMyReceipentsAddContactManually.txtmobnum.text;
    var fbid = frmIBMyReceipentsAddContactManually.lblfbidstudio19.text;//Modified by Studio Viz
    if (em == "Not Available" || em == "" || em == undefined || em == null || em == "No Mail") {
        em = "";
    } else {
        em = "Email:" + em + ". ";
    }
    if (ph == "Not Available" || ph == null || ph == undefined || ph == "" || ph == "No Number") {
        ph = "";
    } else {
        ph = removeHyphenIB(ph);
        ph = "Mobile Phone No.= xxx-xxx-" + ph.substring(6, 10) + ". ";
    }
    if (fbid == "Not Available" || fbid == null || fbid == undefined || fbid == "") {
        fbid = "";
    } else {
        fbid = "Facebook:" + fbid + " ";
    }
    if (gblAdd_Receipent_State == gblEXISTING_RC_EDITION) {
        //alert("gblEXISTING_RC_EDITION")
        //	AccountName = frmIBMyReceipentsAddBankAccnt.tbxAddAccntNickName.text;


        AccntDetailsMessage = "";
        AccountName = frmIBMyReceipentsAddContactManually.txtRecipientName.text;
        //CR 13 change start
        var fbId = frmIBMyReceipentsAddContactManually.tbxFbID.text;
        var mobNo = frmIBMyReceipentsAddContactManually.txtmobnum.text;
       if (mobNo == null || mobNo == undefined || mobNo == "") {
            
         }else{
		  AccntDetailsMessage = "Mobile x" + mobNo.substring(6, 10) ;
            
	    }

       if (fbId == null || fbId == undefined || fbId == ""){
       }else{
       AccntDetailsMessage = AccntDetailsMessage+ " FB" + fbId.substring(0, 10);
       }
       
        //CR 13 changes end
    } else if (gblAdd_Receipent_State == gblEXISTING_RC_BANKACCNT_ADDITION) {
        //alert("gblEXISTING_RC_BANKACCNT_ADDITION")
        var noOfAccounts;

        //CR13
        if (frmIBMyReceipentsAddBankAccnt.segementTobeAddedAccnts.data != null)
            noOfAccounts = frmIBMyReceipentsAddBankAccnt.segementTobeAddedAccnts.data.length;
        else
            noOfAccounts = 0;
        if (noOfAccounts == 0) {
            //AccountName = "No Accounts";
        } else {
            var acctData = frmIBMyReceipentsAddBankAccnt.segementTobeAddedAccnts.data;
            for (var i = 0; i < noOfAccounts; i++) {
                
            }
        } //CR13 end

        AccountName = frmIBMyReceipentsAddBankAccnt.lblRcName.text;
        ph = frmIBMyReceipentsAddBankAccnt.lblAddRcConfMobileNo.text;
        em = frmIBMyReceipentsAddBankAccnt.lblAddRcConfEmail.text;
        fbid = frmIBMyReceipentsAddBankAccnt.lblAddRcConfFbId.text;
        if (em == "Not Available" || em == "" || em == undefined || em == null || em == "No Mail") {
            em = "";
        } else {
            em = "Email:" + em + ". ";
        }
        if (ph == "Not Available" || ph == null || ph == undefined || ph == "" || ph == "No Number") {
            ph = "";
        } else {
            ph = removeHyphenIB(ph);
            ph = "Mobile Phone No.= xxx-xxx-" + ph.substring(6, 10) + ". ";
        }
        if (fbid == "Not Available" || fbid == null || fbid == undefined || fbid == "") {
            fbid = "";
        } else {
            fbid = "Facebook:" + fbid + " ";
        }
        AccntDetailsMessage = "";
        AccntDetailsMessage = AccntDetailsMessage + getAccountDetailMessageRc(frmIBMyReceipentsAddBankAccnt.segementTobeAddedAccnts.data)
        AccountName = frmIBMyReceipentsAddBankAccnt.lblRcName.text;

    } else if (gblAdd_Receipent_State == gblNEW_RC_ADDITION || gblAdd_Receipent_State == gblNEW_RC_ADDITION_PRECONFIRM) {
        if (isAddFb) {
            fbRc = frmIBMyReceipentsAddContactFB.segmentSelectRcConf.data;
            AccountName = "" //frmIBMyReceipentsAddContactManually.lblrecipientname.text;
            var AccntDetailsMessage2 = "";
            var rcount = 0;
            if (fbRc != null && fbRc != undefined) {
                //Commented for CR 13
                /*for (var t = 0; t < fbRc.length; t++) {
                    AccntDetailsMessage2 = AccntDetailsMessage2 + fbRc[t]["lblSelectRcName"] + ", "
                }*/
                rcount = fbRc.length;
                if (rcount > 1 ) {
                    AccntDetailsMessage = rcount + "Recipients";
                } else {
                    AccntDetailsMessage = "" + fbRc[0]["facebookId"];
                }
            }
            //Commented for CR 13
            /*if (AccntDetailsMessage2 != null && AccntDetailsMessage2 != undefined) {
                AccntDetailsMessage2 = AccntDetailsMessage2.substring(0, AccntDetailsMessage2.length - 2)
            }
            AccntDetailsMessage = rcount + " Recipient (" + AccntDetailsMessage2 + ").";*/
        } else {
            //alert("gblNEW_RC_ADDITION  gblNEW_RC_ADDITION_PRECONFIRM")
            AccountName = frmIBMyReceipentsAddContactManually.lblrecipientname.text;
            var phn = frmIBMyReceipentsAddContactManually.lblmobnum.text

            if (phn == "Not Available" || phn == null || phn == undefined || phn == "" || phn == "No Number") {
                phn = "";
            } else {
                phn = removeHyphenIB(phn);
                phn = "Mobile x" + phn.substring(6, 10);
            }
            AccntDetailsMessage = "";
            //Commented for CR13
            // AccntDetailsMessage = AccntDetailsMessage + getAccountDetailMessageRc(frmIBMyReceipentsAddContactManually.segmentRcAccounts.data)
            if (frmIBMyReceipentsAddContactManually.segmentRcAccounts.data.length == 0)
                AccntDetailsMessage = phn + "";
            else
                AccntDetailsMessage = AccntDetailsMessage + getAccountDetailMessageRc(frmIBMyReceipentsAddContactManually.segmentRcAccounts.data) + phn + "";
        }
    }
    //AccountName=frmIBMyReceipentsAddContactManually.lblrecipientname.text;
  if(AccountName != null && AccountName.length > 13)
    AccountName = AccountName.substring(0, 13); //CR13 change
    var inputParams = {
        retryCounterRequestOTP: gblRetryCountRequestOTP,
        Channel: Channel,
        locale: locale,
        AccDetailMsg: AccntDetailsMessage,
        userAccountName: AccountName
    };
    showLoadingScreenPopup();
    try {
        //invokeServiceSecureAsync("generateOTP", inputParams, startBackGroundOTPRequestServiceAsyncCallback);
        invokeServiceSecureAsync("generateOTPWithUser", inputParams, startOTPRequestServiceAsyncCallback);
        dismissLoadingScreenPopup();//def505(UAT)
    } catch (e) {
        // todo: handle exception
        invokeCommonIBLogger("Exception in invoking service");
    }
}

/**
 * Method to invoke service that validates the Token
 * @returns {}
 */

/*function startTokenValidationService(tokentext) {
	var tokenText = tokentext;
	if (gblAdd_Receipent_State == gblEXISTING_RC_EDITION) {
		tokenText = frmIBMyReceipentsAddContactManually.textbox247428873338513.text;
	} else if (gblAdd_Receipent_State == gblNEW_RC_ADDITION || gblAdd_Receipent_State == gblNEW_RC_ADDITION_PRECONFIRM) {
		tokenText = frmIBMyReceipentsAddContactManually.txtotp.text;
	} else if (gblAdd_Receipent_State == gblEXISTING_RC_BANKACCNT_ADDITION || gblAdd_Receipent_State ==
		gblEXISTING_RC_BANKACCNT_EDITION) {
		tokenText = frmIBMyReceipentsAddBankAccnt.textbox247428873338513.text;
	} else {
		tokenText = frmIBMyReceipentsAddContactFB.textbox247428873338513.text;
	}
	var inputParams = {
		token: tokenText
	};
	showLoadingScreenPopup();
	try {
		proceedOnSecurityValidation();
		//invokeServiceSecureAsync("verifyToken", inputParams, startTokenValidationServiceAsyncCallback);
	} catch (e) {
		// todo: handle exception
		invokeCommonIBLogger("Exception in invoking service");
	}
}*/

/**
 * Method to start check the Rc op lock status
 * @returns {}
 */

function startRCLockStatusQueryService() {
	var inputParams = {
		crmId: gblcrmId,
		rqUUId: "",
		channelName: "IB-INQ"
	};
	try {
		invokeServiceSecureAsync("crmProfileInq", inputParams, startRCLockStatusQueryServiceAsyncCallback);
	} catch (e) {
		// todo: handle exception
		invokeCommonIBLogger("Exception in invoking service");
	}
}
/**
 * Callback method for startRCLockStatusQueryService()
 * @returns {}
 */

function startRCLockStatusQueryServiceAsyncCallback(status, callBackResponse) {
	if (status == 400) {
		if (callBackResponse["opstatus"] == 0) {
			gblFBId = callBackResponse["facebookId"];
			gblcrmId = callBackResponse["crmId"];
			gblEmailId = callBackResponse["emailAddr"];
			var currentFormId = kony.application.getCurrentForm()
				.id;
			if (currentFormId == "frmIBMyReceipentsAddContactFB") {
				if (gblFBId != null && gblFBId != "") {
					if (gblInvokeFbListingSrv) {
						checkAndInvokeFbFriendsListingService();
						gblInvokeFbListingSrv = false;
					}
				}
			}
			gblUserLockStatusIB=callBackResponse["ibUserStatusIdTr"]
			if (callBackResponse["ibUserStatusIdTr"] == gblFinancialTxnIBLock) {
				gblRcOpLockStatus = true;
			} else {
				gblRcOpLockStatus = false;
			}
		} else {
			
		}
	} else {
		if (status == 300) {
			
		}
	}
}


/**
 * Method to start check the Rc op lock status
 * @returns {}
 */

function startRcNotificationService(subject, content, RecipientName, RecipientNickName, ProfilePic, AddedAccountDetails,
	Source) {
	var locale = kony.i18n.getCurrentLocale();
	var em=frmIBMyReceipentsAddContactManually.lblemail.text;
	var ph=frmIBMyReceipentsAddContactManually.lblmobnum.text;
	var fbid=frmIBMyReceipentsAddContactManually.lblfbidstudio19.text;//Modified by Studio Viz
	var fbRc;
	var rName=frmIBMyReceipentsAddContactManually.lblrecipientname.text;
		
	if (gblAdd_Receipent_State == gblEXISTING_RC_BANKACCNT_ADDITION) {
		ph=frmIBMyReceipentsAddBankAccnt.lblAddRcConfMobileNo.text;
		em=frmIBMyReceipentsAddBankAccnt.lblAddRcConfEmail.text;
		fbid=frmIBMyReceipentsAddBankAccnt.lblAddRcConfFbId.text;
		rName=frmIBMyReceipentsAddBankAccnt.lblRcName.text;
	}	
	else if (gblAdd_Receipent_State == gblNEW_RC_ADDITION || gblAdd_Receipent_State == gblNEW_RC_ADDITION_PRECONFIRM) {
		if(isAddFb){
		em="";
		ph="";
		fbRc=frmIBMyReceipentsAddContactFB.segmentSelectRcConf.data;
		fbRc=frmIBMyReceipentsAddContactFB.segmentSelectRcConf.data;
		rName="";
		var rcount=0;
		if(fbRc!=null && fbRc!=undefined){
		rcount=fbRc.length;
		for(var t=0;t<fbRc.length;t++)
		  {
			rName=rName+fbRc[t]["lblSelectRcName"]+", "
		  }
		}
		if(rName !=null && rName != undefined)
			{
				rName=rName.substring(0, rName.length-2)
			}
		
		}
		
	}
	
	
	
	if(em=="Not Available" || em=="" || em==undefined || em==null){
	em="";
	}
	
	if(ph=="Not Available" || ph== null || ph==undefined || ph==""){
	ph="";
	}
	else{
	ph=removeHyphenIB(ph);
	ph=maskingIB(ph);
	}
	if(fbid=="Not Available" || fbid== null || fbid==undefined || fbid==""){
	fbid="";
	}
	var inputParams = {
		//channelName: "IB-INQ",
		notificationType: "Email",
		phoneNumber: gblPHONENUMBER,
		//emailId:gblEmailId ,
		notificationSubject: subject,
		notificationContent: content,
		recipientName: rName,
		recipientNickName: rName,
		profilePic: ProfilePic,
		addedAccountDetails: AddedAccountDetails,
		source: Source,
		Locale: locale,
		Channel: "IB",
		mobile: ph ,
		mail: em,
		facebook:fbid,
		custNME: gblCustomerName
	};
	try {
		invokeServiceSecureAsync("NotificationAdd", inputParams, startRcNotificationServiceAsyncCallback);
	} catch (e) {
		// todo: handle exception
		invokeCommonIBLogger("Exception in invoking service");
	}
}
/**
 * Callback method for startdummycrmService()
 * @returns {}
 */

function startRcNotificationServiceAsyncCallback(status, callBackResponse) {
	if (status == 400) {
		if (callBackResponse["opstatus"] == 0) {
			invokeCommonIBLogger("Notification sent successfully");
		}
	} else {
		if (status == 300) {
			
		}
	}
}
/**
 * Method to start the image upload service
 */

function startImageServiceCallRecipientsIB(perId) {
	if (perId == null) {
		personalizedID = gblselectedRcId;
	} else {
		personalizedID = perId;
	}
	inputParam = {
		crmId:gblcrmId,
		personalizedId: personalizedID
	};
	try {
	
		invokeServiceSecureAsync("confirmPicUpload", inputParam, startImageServiceCallRecipientsIBAsyncCallback)
	} catch (e) {
		// todo: handle exception
		invokeCommonIBLogger("Exception in invoking service");
	}
}
/**
 * Image upload service async callback
 * @param {} status
 * @param {} callBackResponse
 * @returns {}
 */

function startImageServiceCallRecipientsIBAsyncCallback(status, callBackResponse) {
	if (status == 400) {
			if(callBackResponse["opstatus"] == 0){
			 var randomnum = Math.floor((Math.random()*10000)+1); 
					frmIBMyReceipentsAddContactManuallyConf.image2101071681311897.src =RC_LOGO_URL+personalizedID+"&rr="+randomnum;
					setDatatoReceipentsList("8");
									if(recipientAddFromTransfer){//enh069
										frmIBTranferLP.imgXferToImage.src=RC_LOGO_URL+personalizedID+"&rr="+randomnum;
										frmIBTransferCustomWidgetLP.imgXferToImage.src=RC_LOGO_URL+personalizedID+"&rr="+randomnum;
									}
			}
		//DO NOTHING
	} else {
		if (status == 300) {
			//showCommonAlert("Error", null);
		}
	}
}
/**
 * Method to invoke service that adds the new rc only
 * @returns {}
 */

function startNewOnlyRecipientAddService() {
	var totalData = [];
	var RcData = [frmIBMyReceipentsAddContactManually.lblrecipientname.text, "png", frmIBMyReceipentsAddContactManually.lblemail.text,
					 frmIBMyReceipentsAddContactManually.txtmobnum.text, frmIBMyReceipentsAddContactManually.lblfbidstudio19.text, "Added",//Modified by Studio Viz
		"N"];
	totalData.push(RcData);
	var inputParams = {
		crmId: gblcrmId,
		receipentList: totalData
	};
	showLoadingScreenPopup();
	try {
		invokeServiceSecureAsync("receipentNewAddService", inputParams, startNewOnlyRecipientAddServiceAsyncCallback);
	} catch (e) {
		// todo: handle exception
		invokeCommonIBLogger("Exception in invoking service");
	}
}
/**
 * Callback method for startNewOnlyRecipientAddService()
 * @returns {}
 */

function startNewOnlyRecipientAddServiceAsyncCallback(status, callBackResponse) {
	if (status == 400) {
		if (callBackResponse["opstatus"] == 0) {
			dismissLoadingScreenPopup();
			startRcNotificationService("Added Recipient with account(s)", "A Recipient has been added with one or more accounts",
				frmIBMyReceipentsAddContactManually.lblrecipientname.text, null, frmIBMyReceipentsAddContactManually.image2101071681311897
				.src, createListForRcAccountNotification(),
				"recipientsAdd");
			gblRefreshRcCache = true;
			frmIBMyReceipentsAddContactManuallyConf.show();
			frmIBMyReceipentsAddContactManuallyConf.lblrcname.text = frmIBMyReceipentsAddContactManually.lblrecipientname.text;
			if (frmIBMyReceipentsAddContactManually.lblmobnum.text == null || frmIBMyReceipentsAddContactManually.lblmobnum.text ==
				"")
				frmIBMyReceipentsAddContactManuallyConf.lblmobnum.text = "";
			else
				frmIBMyReceipentsAddContactManuallyConf.lblmobnum.text = frmIBMyReceipentsAddContactManually.lblmobnum.text;
			frmIBMyReceipentsAddContactManuallyConf.lblfbid.text = frmIBMyReceipentsAddContactManually.lblfbidstudio19.text;//Modified by Studio Viz
			if (frmIBMyReceipentsAddContactManually.lblemail.text == null || frmIBMyReceipentsAddContactManually.lblemail.text ==
				"")
				frmIBMyReceipentsAddContactManuallyConf.lblemail.text = "";
			else
				frmIBMyReceipentsAddContactManuallyConf.lblemail.text = frmIBMyReceipentsAddContactManually.lblemail.text;
			frmIBMyReceipentsAddContactManuallyConf.image2101071681311897.src = frmIBMyReceipentsAddContactManually.image2101071681311897.src;
			frmIBMyReceipentsAddContactManuallyConf.label10107168132952.setVisibility(false);
			frmIBMyReceipentsAddContactManuallyConf.segmentRcAccounts.setVisibility(false);
			if (frmIBMyReceipentsAddContactManuallyConf.lblfbid.text == "Not Available") {
				activityLogServiceCall("045", "", "01", "", "Add", frmIBMyReceipentsAddContactManuallyConf.lblrcname.text,
					frmIBMyReceipentsAddContactManuallyConf.lblmobnum.text,
					frmIBMyReceipentsAddContactManuallyConf.lblemail.text, frmIBMyReceipentsAddContactManuallyConf.lblfbid.text, "");
			} else {
				activityLogServiceCall("045", "", "01", "", "Add", frmIBMyReceipentsAddContactManuallyConf.lblrcname.text,
					frmIBMyReceipentsAddContactManuallyConf.lblmobnum.text,
					frmIBMyReceipentsAddContactManuallyConf.lblemail.text, frmIBMyReceipentsAddContactManuallyConf.lblfbid.text, "");
			}
			var perId = callBackResponse["personalizedIdList"][0]["personalizedId"];
			startImageServiceCallRecipientsIB(perId);
		} else if (callBackResponse["opstatus"] == 1) {
			if (callBackResponse["errMsg"] != null) {
				dismissLoadingScreenPopup();
				if (kony.string.endsWith(callBackResponse["errMsg"], "(J200000001)")) {
					showCommonAlert(kony.i18n.getLocalizedString("J200000001"), null);
				} else {
					showCommonAlert(callBackResponse["errMsg"], null);
				}
				frmIBMyReceipentsHome.show();
				destroyAllForms();
				return;
			}
		} else {
			dismissLoadingScreenPopup();
			showCommonAlert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"), null);
		}
	} else {
		if (status == 300) {
			dismissLoadingScreenPopup();
			showCommonAlert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"), null);
		}
	}
}
/**
 * Method to invoke service that adds the new rc only
 * @returns {}
 */

function startNewRecipientPlusAccountAddService() {
	var totalData = [];
	var RcData = [frmIBMyReceipentsAddContactManually.lblrecipientname.text, "png", frmIBMyReceipentsAddContactManually.lblemail.text,
					 frmIBMyReceipentsAddContactManually.txtmobnum.text, frmIBMyReceipentsAddContactManually.lblfbidstudio19.text, "Added",//Modified by Studio Viz
		"N"];
	totalData.push(RcData);
	var inputParams = {
		crmId: gblcrmId,
		receipentList: totalData
	};
	showLoadingScreenPopup();
	try {
		invokeServiceSecureAsync("receipentNewAddService", inputParams, startNewRecipientPlusAccountAddServiceAsyncCallback);
	} catch (e) {
		// todo: handle exception
		invokeCommonIBLogger("Exception in invoking service");
	}
}
/**
 * Callback method for startNewRecipientPlusAccountAddService()
 * @returns {}
 */

function startNewRecipientPlusAccountAddServiceAsyncCallback(status, callBackResponse) {
	if (status == 400) {
		if (callBackResponse["opstatus"] == 0) {
			if (frmIBMyReceipentsAddContactManually.lblfbidstudio19.text == "Not Available") {//Modified by Studio Viz
				activityLogServiceCall("045", "", "01", "", "Add", frmIBMyReceipentsAddContactManually.lblrecipientname.text,
					frmIBMyReceipentsAddContactManually.lblmobnum.text,
					frmIBMyReceipentsAddContactManually.lblemail.text, "", "");
			} else {
				activityLogServiceCall("045", "", "01", "", "Add", frmIBMyReceipentsAddContactManually.lblrecipientname.text,
					frmIBMyReceipentsAddContactManually.lblmobnum.text,
					frmIBMyReceipentsAddContactManually.lblemail.text, frmIBMyReceipentsAddContactManually.lblfbidstudio19.text, "");//Modified by Studio Viz
			}
			dismissLoadingScreenPopup();
			startRcNotificationService("Added Recipient with account(s)", "A Recipient has been added with one or more accounts",
				frmIBMyReceipentsAddContactManually.lblrecipientname.text, null, frmIBMyReceipentsAddContactManually.image2101071681311897
				.src, createListForRcAccountNotification(),
				"recipientsAdd");
			gblRefreshRcCache = true;
			startNewRcAccountAddService(callBackResponse["personalizedIdList"][0]["personalizedId"]);
			var perId = callBackResponse["personalizedIdList"][0]["personalizedId"];
			startImageServiceCallRecipientsIB(perId);
		} else if (callBackResponse["opstatus"] == 1) {
			if (callBackResponse["errMsg"] != null) {
				dismissLoadingScreenPopup();
				if (kony.string.endsWith(callBackResponse["errMsg"], "(J200000001)")) {
					showCommonAlert(kony.i18n.getLocalizedString("J200000001"), null);
				} else {
					showCommonAlert(callBackResponse["errMsg"], null);
				}
				frmIBMyReceipentsHome.show();
				destroyAllForms();
				return;
			}
		} else {
			dismissLoadingScreenPopup();
			showCommonAlert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"), null);
		}
	} else {
		if (status == 300) {
			dismissLoadingScreenPopup();
			showCommonAlert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"), null);
		}
	}
}
/**
 * Method to add accounts to new Rc
 */

function startNewRcAccountAddService(personalizedId) {
	var inputParams = {
		crmId: gblcrmId,
		personalizedAccList: prepareNewAccountListNewRc(personalizedId)
	};
	showLoadingScreenPopup();
	try {
		invokeServiceSecureAsync("receipentAddBankAccntService", inputParams, startNewRcAccountAddServiceAsyncCallback);
	} catch (e) {
		// todo: handle exception
		invokeCommonIBLogger("Exception in invoking service");
	}
}
/**
 * Callback method for startNewRcAccountAddService
 * @param {} startExistingRcAccountAddService
 * @returns {}
 */

function startNewRcAccountAddServiceAsyncCallback(status, callBackResponse) {
	if (status == 400) {
		if (callBackResponse["opstatus"] == 0) {
			gblRefreshRcCache = true;
			frmIBMyReceipentsAddContactManuallyConf.show();
			frmIBMyReceipentsAddContactManuallyConf.lblrcname.text = frmIBMyReceipentsAddContactManually.lblrecipientname.text;
			if (frmIBMyReceipentsAddContactManually.lblmobnum.text == null || frmIBMyReceipentsAddContactManually.lblmobnum.text ==
				"")
				frmIBMyReceipentsAddContactManuallyConf.lblmobnum.text = "";
			else
				frmIBMyReceipentsAddContactManuallyConf.lblmobnum.text = frmIBMyReceipentsAddContactManually.lblmobnum.text;
			frmIBMyReceipentsAddContactManuallyConf.lblfbid.text = frmIBMyReceipentsAddContactManually.lblfbidstudio19.text;//Modified by Studio Viz
			if (frmIBMyReceipentsAddContactManually.lblemail.text == null || frmIBMyReceipentsAddContactManually.lblemail.text ==
				"")
				frmIBMyReceipentsAddContactManuallyConf.lblemail.text = "";
			else
				frmIBMyReceipentsAddContactManuallyConf.lblemail.text = frmIBMyReceipentsAddContactManually.lblemail.text;
			frmIBMyReceipentsAddContactManuallyConf.image2101071681311897.src = frmIBMyReceipentsAddContactManually.image2101071681311897
				.src;
			frmIBMyReceipentsAddContactManuallyConf.label10107168132952.setVisibility(true);
			frmIBMyReceipentsAddContactManuallyConf.segmentRcAccounts.setVisibility(true);
			setToAddDatatoManualConfirmationSeg();
			dismissLoadingScreenPopup();
			//startRcNotificationService("Account(s) Added to Recipient", "One or more account(s) has been added to a Recipient",
//				null, null, null, null, null);
		} else if (callBackResponse["opstatus"] == 1) {
			if (callBackResponse["errMsg"] != null) {
				dismissLoadingScreenPopup();
				showCommonAlert("" + callBackResponse["errMsg"], null);
				frmIBMyReceipentsHome.show();
				destroyAllForms();
				return;
			}
		} else {
			dismissLoadingScreenPopup();
			showCommonAlert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"), null);
		}
	} else {
		if (status == 300) {
			dismissLoadingScreenPopup();
			showCommonAlert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"), null);
		}
	}
}


/**
 * Callback method for startRcFBFriendsListServiceIfIdPresent()
 * @returns {}
 */

function startRcFBFriendsListServiceIfIdPresentAsyncCallback(callBackResponse) {
	getnSetDataToFBListing(callBackResponse);
}
/**
 * Method to check and invoke the Facebokk friends service
 * @returns {}
 */

function checkAndInvokeFbFriendsListingService() {
	invokeCommonIBLogger("gblFBId " + gblFBId);
	startRcFBFriendsListService();
}
/*--------------------------------------------------------------------------------------------*/
/**
 * Method to invoke service that uploads the caches new rc from FB list
 * @returns {}
 */

function startRcFBFriendsListBgndCacheService() {
	var cacheoffset;
	if (curFBFriendRetVal != null && curFBFriendRetVal != "" && curFBFriendRetVal != 0) {
		cacheoffset = cacheoffset + curFBFriendRetVal;
	} else {
		cacheoffset = 0;
	}
	var inputParams = {
		MyID: gblFBId,
		accTok: "122372981273533|IhmGD-iYH7mBwvLqhBOfCVxROZg",
		offset: cacheoffset
	};
	showLoadingScreenPopup();
	try {
		invokeServiceSecureAsync("FBFriendsListingService", inputParams, startRcFBFriendsListBgndCacheServiceAsyncCallback);
	} catch (e) {
		// todo: handle exception
		invokeCommonIBLogger("Exception in invoking service");
	}
}
/**
 * Callback method for startRcFBFriendsListBgndCacheService()
 * @returns {}
 */

function startRcFBFriendsListBgndCacheServiceAsyncCallback(status, callBackResponse) {
	var currentFormId = kony.application.getCurrentForm();
	if (status == 400) {
		if (callBackResponse["opstatus"] == 0) {
			dismissLoadingScreenPopup();
			var responseData = callBackResponse["collectiondata"];
			if (responseData != null && responseData.length > 0) {
				invokeCommonIBLogger("Number of friends " + responseData.length);
				curFBFriendRetVal = responseData.length;
				cacheFBFullList(responseData);
				if (curFBFriendRetVal >= FB_FRIENDS_SIZE_PER_CALL) {
					startRcFBFriendsListBgndCacheService(); //Re invoke the friends listi cache service
				}
				invokeCommonIBLogger("Number of All friends " + globalFBCacheData.length);
			} else {
				invokeCommonIBLogger("No Facebook Friends ");
				if (curFBFriendRetVal == 0) {
					frmIBMyReceipentsAddContactFB.btnSelectRcNext.text = kony.i18n.getLocalizedString("keybtnReturn");
					frmIBMyReceipentsAddContactFB.btnSelectRcNext.onClick = outOfFacebookScreen;
				} else {
					frmIBMyReceipentsAddContactFB.btnSelectRcNext.text = kony.i18n.getLocalizedString("Receipent_Next");
					frmIBMyReceipentsAddContactFB.btnSelectRcNext.onClick = setTobeAddedFBContactsforRC;
				}
			}
		} else {
			dismissLoadingScreenPopup();
			invokeCommonIBLogger("Service opStatus " + callBackResponse["opstatus"]);
		}
	} else {
		if (status == 300) {
			dismissLoadingScreenPopup();
			invokeCommonIBLogger("Service status " + status);
		}
	}
}
//Added by Bhaskar to implement Token Switching
function onEditReceipentNextIB() {  
	var currForm = kony.application.getCurrentForm().id;
	

	if(gblSwitchToken == false && gblTokenSwitchFlag == false){
		checkEditReceipentTokenFlag();
	} 
	if(gblTokenSwitchFlag == true && gblSwitchToken == false){
				 if (gblAdd_Receipent_State == gblEXISTING_RC_BANKACCNT_ADDITION || gblAdd_Receipent_State ==
					gblEXISTING_RC_BANKACCNT_EDITION) {
					frmIBMyReceipentsAddBankAccnt.label476047582115288.text = kony.i18n.getLocalizedString("Receipent_tokenId");
					frmIBMyReceipentsAddBankAccnt.label476047582115284.text = kony.i18n.getLocalizedString("Receipent_Token");
					frmIBMyReceipentsAddBankAccnt.btnOTPReq.text = kony.i18n.getLocalizedString("Receipent_SwitchtoOTP");
					frmIBMyReceipentsAddBankAccnt.textbox247428873338513.setFocus(true);
					//frmIBMyReceipentsAddBankAccnt.textbox247428873338513.placeholder = kony.i18n.getLocalizedString("enterToken");
					frmIBMyReceipentsAddBankAccnt.textbox247428873338513.setFocus(true);
				} else if (gblAdd_Receipent_State == gblEXISTING_RC_EDITION || gblAdd_Receipent_State == gblNEW_RC_ADDITION) {
					if (parseInt(formId.toString()) == 3) {
						frmIBMyReceipentsAddContactFB.label476047582115288.text = kony.i18n.getLocalizedString("Receipent_tokenId");
						frmIBMyReceipentsAddContactFB.label476047582115284.text = kony.i18n.getLocalizedString("Receipent_Token");
						frmIBMyReceipentsAddContactFB.btnOTPReq.text = kony.i18n.getLocalizedString("Receipent_SwitchtoOTP");
						//frmIBMyReceipentsAddContactFB.textbox247428873338513.placeholder = kony.i18n.getLocalizedString("enterToken");
					} else {
						frmIBMyReceipentsAddContactManually.otpmsglbl.text = kony.i18n.getLocalizedString("Receipent_tokenId");
						frmIBMyReceipentsAddContactManually.lblOTP.text = kony.i18n.getLocalizedString("Receipent_Token");
						frmIBMyReceipentsAddContactManually.btnotp.text = kony.i18n.getLocalizedString("Receipent_SwitchtoOTP");
					    frmIBMyReceipentsAddContactManually.txtotp.setFocus(true);
					//	frmIBMyReceipentsAddContactManually.txtotp.placeholder = kony.i18n.getLocalizedString("enterToken");
					}
				} else {
					frmIBMyReceipentsAddContactFB.label476047582115288.text = kony.i18n.getLocalizedString("Receipent_tokenId");
					frmIBMyReceipentsAddContactFB.label476047582115284.text = kony.i18n.getLocalizedString("Receipent_Token");
					frmIBMyReceipentsAddContactFB.btnOTPReq.text = kony.i18n.getLocalizedString("Receipent_SwitchtoOTP");
					frmIBMyReceipentsAddContactManually.otpmsglbl.text = kony.i18n.getLocalizedString("Receipent_tokenId");
					frmIBMyReceipentsAddContactManually.lblOTP.text = kony.i18n.getLocalizedString("Receipent_Token");
					frmIBMyReceipentsAddContactManually.btnotp.text = kony.i18n.getLocalizedString("Receipent_SwitchtoOTP");
					frmIBMyReceipentsAddContactManually.txtotp.setFocus(true);
					//frmIBMyReceipentsAddContactFB.textbox247428873338513.placeholder = kony.i18n.getLocalizedString("enterToken");
				}
				//startBackGroundTokenRequestService();
					                 
	} else if(gblTokenSwitchFlag ==false && gblSwitchToken == true) {
				 if (gblAdd_Receipent_State == gblEXISTING_RC_BANKACCNT_ADDITION || gblAdd_Receipent_State ==
					gblEXISTING_RC_BANKACCNT_EDITION) {
					frmIBMyReceipentsAddBankAccnt.label476047582115288.text = kony.i18n.getLocalizedString("keyotpmsgreq");
					frmIBMyReceipentsAddBankAccnt.label476047582115284.text = kony.i18n.getLocalizedString("Receipent_OTP");
					frmIBMyReceipentsAddBankAccnt.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
					//frmIBMyReceipentsAddBankAccnt.textbox247428873338513.placeholder = kony.i18n.getLocalizedString("enterOTP");
				} else if (gblAdd_Receipent_State == gblEXISTING_RC_EDITION || gblAdd_Receipent_State == gblNEW_RC_ADDITION) {
					if (parseInt(formId.toString()) == 3) {
						frmIBMyReceipentsAddContactFB.label476047582115288.text = kony.i18n.getLocalizedString("keyotpmsgreq");
						frmIBMyReceipentsAddContactFB.label476047582115284.text = kony.i18n.getLocalizedString("Receipent_OTP");
						frmIBMyReceipentsAddContactFB.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
						//frmIBMyReceipentsAddContactFB.textbox247428873338513.placeholder =  kony.i18n.getLocalizedString("enterOTP");
					} else {
						frmIBMyReceipentsAddContactManually.otpmsglbl.text = kony.i18n.getLocalizedString("keyotpmsgreq");
						frmIBMyReceipentsAddContactManually.lblOTP.text = kony.i18n.getLocalizedString("Receipent_OTP");;
						frmIBMyReceipentsAddContactManually.btnotp.text = kony.i18n.getLocalizedString("keyRequest");
						//frmIBMyReceipentsAddContactManually.txtotp.placeholder = kony.i18n.getLocalizedString("enterOTP");
					}
				} else {
					frmIBMyReceipentsAddContactFB.label476047582115288.text = kony.i18n.getLocalizedString("keyotpmsgreq");
					frmIBMyReceipentsAddContactFB.label476047582115284.text = kony.i18n.getLocalizedString("Receipent_OTP");
					frmIBMyReceipentsAddContactFB.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
					frmIBMyReceipentsAddContactManually.otpmsglbl.text = kony.i18n.getLocalizedString("keyotpmsgreq");
					frmIBMyReceipentsAddContactManually.lblOTP.text = kony.i18n.getLocalizedString("Receipent_OTP");;
					frmIBMyReceipentsAddContactManually.btnotp.text = kony.i18n.getLocalizedString("keyRequest");
					//frmIBMyReceipentsAddContactFB.textbox247428873338513.placeholder =  kony.i18n.getLocalizedString("enterOTP");
				}   
				
			startBackGroundOTPRequestService();
				             
	}

}

function checkEditReceipentTokenFlag() {
		var inputParam = [];
		inputParam["crmId"] = gblcrmId;
		showLoadingScreen();
		invokeServiceSecureAsync("tokenSwitching", inputParam, ibEditReceipentTokenFlagCallbackfunction);
}
function ibEditReceipentTokenFlagCallbackfunction(status,callbackResponse){
		var currForm = kony.application.getCurrentForm().id;
		
		if(status==400){
				if(callbackResponse["opstatus"] == 0){
					dismissLoadingScreen();
					if(callbackResponse["deviceFlag"].length==0){
						
						//return
					}
					if ( callbackResponse["deviceFlag"].length > 0 ){
							var tokenFlag = callbackResponse["deviceFlag"][0]["TOKEN_DEVICE_FLAG"];
							var mediaPreference = callbackResponse["deviceFlag"][0]["MEDIA_PREFERENCE"];
							var tokenStatus = callbackResponse["deviceFlag"][0]["TOKEN_STATUS_ID"];
                            if ( tokenFlag=="Y" && ( mediaPreference == "Token" || mediaPreference == "TOKEN") && tokenStatus=='02'){							
							     if (gblAdd_Receipent_State == gblEXISTING_RC_BANKACCNT_ADDITION || gblAdd_Receipent_State ==
									gblEXISTING_RC_BANKACCNT_EDITION) {
									frmIBMyReceipentsAddBankAccnt.label476047582115288.text = kony.i18n.getLocalizedString("Receipent_tokenId");
									frmIBMyReceipentsAddBankAccnt.label476047582115284.text = kony.i18n.getLocalizedString("Receipent_Token");
									frmIBMyReceipentsAddBankAccnt.btnOTPReq.text = kony.i18n.getLocalizedString("Receipent_SwitchtoOTP");
									//frmIBMyReceipentsAddBankAccnt.textbox247428873338513.placeholder = kony.i18n.getLocalizedString("enterToken");
									} else if (gblAdd_Receipent_State == gblEXISTING_RC_EDITION || gblAdd_Receipent_State == gblNEW_RC_ADDITION) {
											if (parseInt(formId.toString()) == 3) {
											frmIBMyReceipentsAddContactFB.label476047582115288.text = kony.i18n.getLocalizedString("Receipent_tokenId");
											frmIBMyReceipentsAddContactFB.label476047582115284.text = kony.i18n.getLocalizedString("Receipent_Token");
											frmIBMyReceipentsAddContactFB.btnOTPReq.text = kony.i18n.getLocalizedString("Receipent_SwitchtoOTP");
										//	frmIBMyReceipentsAddContactFB.textbox247428873338513.placeholder = kony.i18n.getLocalizedString("enterToken");
										} else {
											frmIBMyReceipentsAddContactManually.otpmsglbl.text = kony.i18n.getLocalizedString("Receipent_tokenId");
											frmIBMyReceipentsAddContactManually.lblOTP.text = kony.i18n.getLocalizedString("Receipent_Token");
											frmIBMyReceipentsAddContactManually.btnotp.text = kony.i18n.getLocalizedString("Receipent_SwitchtoOTP");
											frmIBMyReceipentsAddContactManually.txtotp.setFocus(true);
											//frmIBMyReceipentsAddContactManually.txtotp.placeholder = kony.i18n.getLocalizedString("enterToken");
											}
									} else {
									frmIBMyReceipentsAddContactFB.label476047582115288.text = kony.i18n.getLocalizedString("Receipent_tokenId");
									frmIBMyReceipentsAddContactFB.label476047582115284.text = kony.i18n.getLocalizedString("Receipent_Token");
									frmIBMyReceipentsAddContactFB.btnOTPReq.text = kony.i18n.getLocalizedString("Receipent_SwitchtoOTP");
									frmIBMyReceipentsAddContactManually.otpmsglbl.text = kony.i18n.getLocalizedString("Receipent_tokenId");
									frmIBMyReceipentsAddContactManually.lblOTP.text = kony.i18n.getLocalizedString("Receipent_Token");
									frmIBMyReceipentsAddContactManually.btnotp.text = kony.i18n.getLocalizedString("Receipent_SwitchtoOTP");
									frmIBMyReceipentsAddContactManually.txtotp.setFocus(true);
									//frmIBMyReceipentsAddContactFB.textbox247428873338513.placeholder = kony.i18n.getLocalizedString("enterToken");
									}             
								gblTokenSwitchFlag = true;
						} else {						
							if (gblAdd_Receipent_State == gblEXISTING_RC_BANKACCNT_ADDITION || gblAdd_Receipent_State ==
										gblEXISTING_RC_BANKACCNT_EDITION) {
								frmIBMyReceipentsAddBankAccnt.label476047582115288.text = kony.i18n.getLocalizedString("keyotpmsgreq");
								frmIBMyReceipentsAddBankAccnt.label476047582115284.text = kony.i18n.getLocalizedString("Receipent_OTP");
								frmIBMyReceipentsAddBankAccnt.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
								//frmIBMyReceipentsAddBankAccnt.textbox247428873338513.placeholder = kony.i18n.getLocalizedString("enterOTP");
							} else if (gblAdd_Receipent_State == gblEXISTING_RC_EDITION || gblAdd_Receipent_State == gblNEW_RC_ADDITION) {
									if (parseInt(formId.toString()) == 3) {
										frmIBMyReceipentsAddContactFB.label476047582115288.text = kony.i18n.getLocalizedString("keyotpmsgreq");
										frmIBMyReceipentsAddContactFB.label476047582115284.text = kony.i18n.getLocalizedString("Receipent_OTP");
										frmIBMyReceipentsAddContactFB.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
									//	frmIBMyReceipentsAddContactFB.textbox247428873338513.placeholder = kony.i18n.getLocalizedString("enterOTP");
									} else {
										frmIBMyReceipentsAddContactManually.otpmsglbl.text = kony.i18n.getLocalizedString("keyotpmsgreq");
										frmIBMyReceipentsAddContactManually.lblOTP.text = kony.i18n.getLocalizedString("Receipent_OTP");;
										frmIBMyReceipentsAddContactManually.btnotp.text = kony.i18n.getLocalizedString("keyRequest");
								//		frmIBMyReceipentsAddContactManually.txtotp.placeholder = kony.i18n.getLocalizedString("enterOTP");
									}
							} else {
								frmIBMyReceipentsAddContactFB.label476047582115288.text = kony.i18n.getLocalizedString("keyotpmsgreq");
								frmIBMyReceipentsAddContactFB.label476047582115284.text = kony.i18n.getLocalizedString("Receipent_OTP");
								frmIBMyReceipentsAddContactFB.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
								frmIBMyReceipentsAddContactManually.otpmsglbl.text = kony.i18n.getLocalizedString("keyotpmsgreq");
								frmIBMyReceipentsAddContactManually.lblOTP.text = kony.i18n.getLocalizedString("Receipent_OTP");;
								frmIBMyReceipentsAddContactManually.btnotp.text = kony.i18n.getLocalizedString("keyRequest");
						//		frmIBMyReceipentsAddContactFB.textbox247428873338513.placeholder = kony.i18n.getLocalizedString("enterOTP");
							}
								gblTokenSwitchFlag = false;
								gblSwitchToken=true;
								startBackGroundOTPRequestService();
						}
					}
				 else//deviceFlag length is 0.
					{
					 		if (gblAdd_Receipent_State == gblEXISTING_RC_BANKACCNT_ADDITION || gblAdd_Receipent_State ==
										gblEXISTING_RC_BANKACCNT_EDITION) {
								frmIBMyReceipentsAddBankAccnt.label476047582115288.text = kony.i18n.getLocalizedString("keyotpmsgreq");
								frmIBMyReceipentsAddBankAccnt.label476047582115284.text = kony.i18n.getLocalizedString("Receipent_OTP");
								frmIBMyReceipentsAddBankAccnt.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
							//	frmIBMyReceipentsAddBankAccnt.textbox247428873338513.placeholder = kony.i18n.getLocalizedString("enterOTP");
							} else if (gblAdd_Receipent_State == gblEXISTING_RC_EDITION || gblAdd_Receipent_State == gblNEW_RC_ADDITION) {
									if (parseInt(formId.toString()) == 3) {
										frmIBMyReceipentsAddContactFB.label476047582115288.text = kony.i18n.getLocalizedString("keyotpmsgreq");
										frmIBMyReceipentsAddContactFB.label476047582115284.text = kony.i18n.getLocalizedString("Receipent_OTP");
										frmIBMyReceipentsAddContactFB.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
								//		frmIBMyReceipentsAddContactFB.textbox247428873338513.placeholder = kony.i18n.getLocalizedString("enterOTP");
									} else {
										frmIBMyReceipentsAddContactManually.otpmsglbl.text = kony.i18n.getLocalizedString("keyotpmsgreq");
										frmIBMyReceipentsAddContactManually.lblOTP.text = kony.i18n.getLocalizedString("Receipent_OTP");;
										frmIBMyReceipentsAddContactManually.btnotp.text = kony.i18n.getLocalizedString("keyRequest");
								//		frmIBMyReceipentsAddContactManually.txtotp.placeholder = kony.i18n.getLocalizedString("enterOTP");
									}
							} else {
								frmIBMyReceipentsAddContactFB.label476047582115288.text = kony.i18n.getLocalizedString("keyotpmsgreq");
								frmIBMyReceipentsAddContactFB.label476047582115284.text = kony.i18n.getLocalizedString("Receipent_OTP");
								frmIBMyReceipentsAddContactFB.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
						//		frmIBMyReceipentsAddContactFB.textbox247428873338513.placeholder = kony.i18n.getLocalizedString("enterOTP");
							}
								gblTokenSwitchFlag = false;
								gblSwitchToken=true;
								startBackGroundOTPRequestService()
								            
					 }
						
				}
		}
		
} 
function invokeCommonIBLogger(str1) {
	if (gblLOG_DEBUG) {
		
	}
}


/**
 * Method to set data to the to bank account list for a receipent
 * @returns {}
 */

function setDataToRcBankAccntList(collectionData) {
	invokeCommonIBLogger("#############setDataToRcBankAccntList data##########" + collectionData);
	var totalData = [];
		
if (collectionData != null) {
		for (var i = 1; i < collectionData.length; i++) {
			var isAccntFav = collectionData[i]["accFavoriteFlag"];
			var btnFavSkin;
			if (isAccntFav != null) {
				invokeCommonIBLogger("#############setDataToRcBankAccntList accFavoriteFlag##########" + collectionData[i][
					"accFavoriteFlag"]);
				if (isAccntFav.toString() == "y" || isAccntFav.toString() == "Y") {
					btnFavSkin = {
						text: "",
						skin: "btnIBStarIconFocus"
					};
				} else {
					btnFavSkin = {
						text: "",
						skin: "btnIBStarIcon"
					};
				}
			} else {
				btnFavSkin = {
					text: "",
					skin: "btnIBStarIcon"
				};
			}
			//Get the bank name in EN
			var bankName;
			var bankCode;
			var banklogo = "myrecpicon.png";
			var textbtnTransfer = {
								text: kony.i18n.getLocalizedString("Transfer"),
								skin: "btnIB80px"
								};
			if (temp != null && temp.length > 0) {
				temp = null;
			}
			var temp = [];
			temp.push(globalSelectBankData);
			for (var j = 0; j < temp[0].length; j++) {
				if (temp[0][j][0].toString() == collectionData[i]["bankCD"]) {
					bankName = temp[0][j][1].toString();
					bankCode = temp[0][j][0].toString();
					break;
				}
			}
			var segDataMap;
			var accNum = encodeAccntNumbers(collectionData[i]["personalizedAcctId"]);
			if (false) {
				segDataMap = {
					lblBankTitle: kony.i18n.getLocalizedString("Receipent_Bank"),
					lblNumberTitle: kony.i18n.getLocalizedString("Receipent_Number"),
					lblNickNameTitle: kony.i18n.getLocalizedString("Receipent_NickName"),
					btnTransfer: textbtnTransfer,
					lblBankName: bankName,
					lblNumber: accNum,
					lblNickName: collectionData[i]["PersonalizedAcctNickName"],
					FavoriteAcct: isAccntFav,
					btnfav: btnFavSkin,
					selectedBankCD: collectionData[i]["bankCD"],
					bankLogo:"https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+collectionData[i]["bankCD"]+"&modIdentifier=BANKICON"
				};
			} else {
				segDataMap = {
					lblBankTitle: kony.i18n.getLocalizedString("Receipent_Bank"),
					lblNumberTitle: kony.i18n.getLocalizedString("Receipent_Number"),
					lblNickNameTitle: kony.i18n.getLocalizedString("Receipent_NickName"),
					btnTransfer: textbtnTransfer,
					lblBankName: bankName,
					lblNumber: accNum,
					lblNickName: collectionData[i]["PersonalizedAcctNickName"],
					FavoriteAcct: isAccntFav,
					btnfav: btnFavSkin,
					selectedBankCD: collectionData[i]["bankCD"],
					lblAccountNameTitle: kony.i18n.getLocalizedString("AccountName"),
					lblAccountName: collectionData[i]["PersonalizedAcctName"],
					bankLogo: "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+collectionData[i]["bankCD"]+"&modIdentifier=BANKICON"
				};
			}
			totalData.push(segDataMap);
		}
		frmIBMyReceipentsAccounts.segmentRcAccounts.setData(totalData);
		if (frmIBMyReceipentsAccounts.segmentRcAccounts.data.length > 1) {
			frmIBMyReceipentsAccounts.lblAccountTitle.text = kony.i18n.getLocalizedString("Receipent_Accounts");
		} else {
			frmIBMyReceipentsAccounts.lblAccountTitle.text = kony.i18n.getLocalizedString("Receipent_Account");
		}
	}
}
/**
 * Method to prepare the new account list for new receipent
 * @returns {}
 */

function prepareNewRcAccountList() {
	var tempData = [];
	tempData.push(frmIBMyReceipentsAddContactManually.segmentRcAccounts.data);
	var totalData = [];
	if (tempData != null) {
		for (var i = 0; i < tempData[0].length; i++) {
			var BankName = tempData[0][i].lblBankName;
			var Number = decodeAccountNumbers(tempData[0][i].lblNumber);
			var NickName = tempData[0][i].lblNickName;
			var bankCode = tempData[0][i].bankCode;
			var bankData;
			///if (true) {
				bankData = [bankCode, Number, NickName, "Added", tempData[0][i].lblAccountName];
		//	} else {
		//		bankData = [bankCode, Number, NickName, "Added", "NA"];
		//	}
			totalData.push(bankData);
		}
	} else {
		return null;
	}
	invokeCommonIBLogger("#######Account list#########" + totalData);
	return totalData;
}
/**
 * Method to prepare the new account list for adding bank accounts for existing receipent
 * @returns {}
 */

function prepareNewAccountListExistingRc() {
	var tempData = [];
	
	tempData.push(frmIBMyReceipentsAddBankAccnt.segementTobeAddedAccnts.data);
	var totalData = [];
	for (var i = 0; i < tempData[0].length; i++) {
		var BankName = tempData[0][i].lblBankName;
		var Number = decodeAccountNumbers(tempData[0][i].lblNumber);
		var NickName = tempData[0][i].lblNickName;
		var bankCode = tempData[0][i].bankCode;
		var bankData;
		acctName = tempData[0][i].lblAccountName;
		if(acctName == null || acctName == undefined || acctName == "")
		{
					acctName = "Not returned from GSB";
		}
	//	if (bankCode == TMB_BANK_CODE || bankCode == TMB_BANK_CODE_ZERO_PAD) {
			//bankData = [gblcrmId, gblselectedRcId, bankCode, Number, NickName, "Added", acctName];
             bankData = gblcrmId+ ","  + gblselectedRcId+ ","  +bankCode+ ","  +Number+ ","  +NickName+ ","  +"Added"+ ","  +acctName;
	//	} else {
	//		bankData = [gblcrmId, gblselectedRcId, bankCode, Number, NickName, "Added", "NA"];
	//	}

		//totalData.push(bankData);
	}
	invokeCommonIBLogger("#######Account list#########" + bankData);
	return bankData;
}
/**
 * Method to invalidate the account listing and sort by Favorite accounts first
 * @returns {}
 */

function invalidateAccountListingSortbyFav() {
	startgetBankAccntforRCService();
}
/**
 * Method to remove "-" from account numbers
 * @param {} number
 * @returns {}
 */

function decodeAccountNumbers(number) {
	var retNum;
	for (var i = 0; i < number.length; i++) {
		if (number[i] != "-") {
			if (retNum == null) {
				retNum = number[i];
			} else {
				retNum = retNum + number[i];
			}
		}
	}
	return retNum;
}
/**
 * Method to add - to account numbers
 * @param {} txt
 * @returns {}
 */

function encodeAccntNumbers(txt) {
	if (txt == null) return false;
	
	if(txt.length == 10){
		return addHyphenIB(txt);
	}
	else{
		return txt
	}
	var numChars = txt.length;
	var temp = "";
	var i, txtLen = numChars;
	var currLen = numChars;
	for (i = 0; i < numChars; ++i) {
		if (txt[i] != '-') {
			temp = temp + txt[i];
		} else {
			txtLen--;
		}
	}
	var iphenText = "";
	for (i = 0; i < txtLen; i++) {
		iphenText += temp[i];
		if (i == 2 || i == 3 || i == 8) {
			iphenText += '-';
		}
	}
	return iphenText;
}
/**
 * Method to prepare the new account list for adding bank accounts for new receipent
 * @returns {}
 */

function prepareNewAccountListNewRc(personalizedId) {
	var tempData = [];
	tempData.push(frmIBMyReceipentsAddContactManually.segmentRcAccounts.data);
	//var totalData = [];
	for (var i = 0; i < tempData[0].length; i++) {
		var BankName = tempData[0][i].lblBankName;
		var Number = decodeAccountNumbers(tempData[0][i].lblNumber);
		var NickName = tempData[0][i].lblNickName;
		var bankCode = tempData[0][i].bankCode;
		var bankData;
		acctName = tempData[0][i].lblAccountName
		if(acctName == null || acctName == undefined || acctName == "")
		{
					acctName = "Not returned from GSB";
		}
		//if (bankCode == TMB_BANK_CODE || bankCode == TMB_BANK_CODE_ZERO_PAD) {
			//bankData = [gblcrmId, personalizedId, bankCode, Number, NickName, "Added", acctName];
	//	} else {
	//		bankData = [gblcrmId, personalizedId, bankCode, Number, NickName, "Added", "NA"];
	//	}
	//	totalData.push(bankData);
	}
	var totalData = gblcrmId + ","  + personalizedId + "," + bankCode +"," + Number + "," + NickName + "," + "Added" + "," + acctName; // Adding this to resolve Mobile Fabric issue.
	invokeCommonIBLogger("#######Account list#########" + totalData);
	return totalData;
}

/**
 * Method to store the to be save bank data
 * @returns {}
 */

function cacheTobeSavedBankData() {
	var tempBankDataDetails = [frmIBMyReceipentsAddBankAccnt.comboBankName.selectedKeyValue[1],
		frmIBMyReceipentsAddBankAccnt.tbxAddAccntNumber.text,
		frmIBMyReceipentsAddBankAccnt.tbxAddAccntNickName.text,
		frmIBMyReceipentsAddBankAccnt.comboBankName.selectedKey		
	];
	globalTobeAddedBankData.push(tempBankDataDetails);
	globalTobeAddedBankDataConfirmation.push(tempBankDataDetails);
}
/**
 * Method to cache RC list for search
 * form 1 is frmIBMyReceipentsHome
 * form 2 is frmIBMyReceipentsAddBankAccnt
 * form 3 is frmIBMyReceipentsAddContactFB
 * form 4 is frmIBMyReceipentsAddContactManually
 * form 5 is frmIBMyReceipentsEditAccountConf
 * form 6 is frmIBMyReceipentsAccounts
 * form 7 is frmIBMyReceipentsFBContactConf
 * form 8 is frmIBMyReceipentsAddContactManuallyConf
 * @returns {}
 */

function cacheRcList(form) {
	//segAboutMeLoad();
	nameofform = kony.application.getCurrentForm();
	//nameofform.btnMenuAboutMe.skin = "btnIBMenuAboutMe";
	clearCachedRcList();
	if (form == 1) {
		globalRcData.push(frmIBMyReceipentsHome.segmentReceipentListing.data);
	} else if (form == 2) {
		globalRcData.push(frmIBMyReceipentsAddBankAccnt.segmentReceipentListing.data);
	} else if (form == 3) {
		globalRcData.push(frmIBMyReceipentsAddContactFB.segmentReceipentListing.data);
	} else if (form == 4) {
		globalRcData.push(frmIBMyReceipentsAddContactManually.segmentReceipentListing.data);
	} else if (form == 5) {
		globalRcData.push(frmIBMyReceipentsEditAccountConf.segmentReceipentListing.data);
	} else if (form == 6) {
		globalRcData.push(frmIBMyReceipentsAccounts.segmentReceipentListing.data);
	} else if (form == 7) {
		globalRcData.push(frmIBMyReceipentsFBContactConf.segmentReceipentListing.data);
	} else if (form == 8) {
		globalRcData.push(frmIBMyReceipentsAddContactManuallyConf.segmentReceipentListing.data);
	}
	gblRefreshRcCache = false;
}
/**
 * Method to clear the cached Rc list
 * @returns {}
 */

function clearCachedRcList() {
	//Clear existing global data first
	if (globalRcData != null) {
		for (var i = 0; i < globalRcData.length; i++) {
			globalRcData.pop();
		}
	}
	globalRcData = [];
	if (globalFBData != null) {
		for (var i = 0; i < globalFBData.length; i++) {
			globalFBData.pop();
		}
	}
	globalFBData = [];
}
/**
 * Method to check for the availability of cache rc list
 * @returns {}
 */

function isCachedRcListAvailable() {
	invokeCommonIBLogger("CACHED RECIPIENT LIST " + globalRcData[0]);
	if (globalRcData[0] != null && globalRcData[0].length > 0) {
		if (gblRcEnableCache) {
			return true;
		} else {
			return false;
		}
	} else {
		return false;
	}
}
/**
 * Method to check for the availability fo cached select bank list
 * @returns {}
 */

function isCachedSelectBankListAvailable() {

	return false;
}

/**
 * Method to edit the profile of the account holder from my accounts
 * @returns {}
 */

function editAccountHolderProfileMyReceipents() {
	gblAdd_Receipent_State = gblEXISTING_RC_EDITION;
	showLoadingScreenPopup();
	frmIBMyReceipentsAddContactManually.show();
	frmIBMyReceipentsAddContactManually.txtRecipientName.text = frmIBMyReceipentsAccounts.lblRcName.text;
	if(frmIBMyReceipentsAccounts.lblRcMobileNo.text!=null && frmIBMyReceipentsAccounts.lblRcMobileNo.text!="")
	{
		frmIBMyReceipentsAddContactManually.txtmobnum.text = decodePhoneNumber(frmIBMyReceipentsAccounts.lblRcMobileNo.text);
	}
	else
	{
		frmIBMyReceipentsAddContactManually.txtmobnum.text="";
	}
	
	frmIBMyReceipentsAddContactManually.txtemail.text = frmIBMyReceipentsAccounts.lblRcEmail.text;
	frmIBMyReceipentsAddContactManually.tbxFbID.text = frmIBMyReceipentsAccounts.lblRcFbId.text;
	frmIBMyReceipentsAddContactManually.image210107168135.src = frmIBMyReceipentsAccounts.imgReceipentProfile.src;
	frmIBMyReceipentsAddContactManually.txtRecipientName.setFocus(true);
	dismissLoadingScreenPopup();
}
/**
 * Method to edit the profile of the account holder from my add account confirmation
 * @returns {}
 */

//function editAccountHolderProfileAddAccnyConf() {
//	frmIBMyReceipentsAddContactManually.show();
//	frmIBMyReceipentsAddContactManually.txtRecipientName.text = frmIBMyReceipentsAddBankAccnt.lblRcName.text;
//	frmIBMyReceipentsAddContactManually.txtmobnum.text = frmIBMyReceipentsAddBankAccnt.lblAddRcConfMobileNo.text;
//	frmIBMyReceipentsAddContactManually.txtemail.text = frmIBMyReceipentsAddBankAccnt.lblAddRcConfEmail.text;
//}
/**
 * Method to check for already account record
 * @returns {}
 */

function checkforDuplicaterecord() {}
/**
 * Method to get the list of existing accounts for an user
 * @returns {}
 */

function getListofExistingAccounts() {}
/**
 * Method to prepare the new account list for existing receipent
 * @returns {}
 */

//function prepareExistingRcTBAddedAccountList() {
//	var tempData = [];
//	tempData.push(frmIBMyReceipentsAddBankAccnt.segementTobeAddedAccnts.data);
//	var totalData = [];
//	for (var i = 0; i < tempData[0].length; i++) {
//		var BankName = tempData[0][i].lblBankName;
//		var Number = tempData[0][i].lblNumber;
//		var NickName = tempData[0][i].lblNickName;
//		var bankData = [BankName, Number, NickName, "Added"];
//		totalData.push(bankData);
//	}
//	invokeCommonIBLogger("#######Account list#########" + totalData);
//	return totalData;
//}
/**
 * Method to set the bank data to combo box
 * @returns {}
 */

function setBankDatatoComboBox() {
	if (gblAdd_Receipent_State == gblEXISTING_RC_BANKACCNT_EDITION) {
		var masterData = [
			["Bank1", frmIBMyReceipentsAccounts.lblBankNameAccntDetails.text.toString()]
		];
		frmIBMyReceipentsAddBankAccnt.comboBankName.masterData = masterData;
	} else {
		var masterData = [
			["Bank1", "Bank"],
			["Bank2", "TMB"],
			["Bank3", "HDFC"],
			["Bank4", "ICICI"],
			["Bank5", "CITIBANK"],
			["Bank6", "KOTAK"]
		];
		frmIBMyReceipentsAddBankAccnt.comboBankName.masterData = masterData;
	}
}
/**
 * Method to delete the selected bank account before addition to CRMdb
 * @param {} position
 * @returns {}
 */
  function confirmRecAccntPopup(){
		popupDeleAccnt.dismiss();
		deleteSelectedBankAccount(frmIBMyReceipentsAddContactManually.segmentRcAccounts.selectedIndex[1],4);
		if(recipientAddFromTransfer){
			addAccountClick()
		}
}
 
 
 function confirmPopup(){
		popupDeleAccnt.dismiss();
		deleteSelectedBankAccount(frmIBMyReceipentsAddBankAccnt.segementTobeAddedAccnts.selectedIndex[1],2);
}

function dismissPopup(){
	popupDeleAccnt.dismiss();
	
}

//function dismissPopupmanually()
//{
//popupDeleAccnt.dismiss();
//	deleteSelectedBankAccount(frmIBMyReceipentsAddContactManually.segmentRcAccounts.selectedIndex[1],4);
//}
function deleteSelectedBankAccount(position, form) {
	
	if (form == 2) {
		frmIBMyReceipentsAddBankAccnt.segementTobeAddedAccnts.removeAt(position);
		if (frmIBMyReceipentsAddBankAccnt.segementTobeAddedAccnts.data.length > 1) {
			frmIBMyReceipentsAddBankAccnt.lblAddRcConfAccountTitle.text = kony.i18n.getLocalizedString("Receipent_Accounts");
		} else {
			frmIBMyReceipentsAddBankAccnt.lblAddRcConfAccountTitle.text = kony.i18n.getLocalizedString("Receipent_Account");
		}
		if ((checkAccountCountBulkAddition() == MAX_BANK_ACCNT_BULK_ADD) || checkRcAccountLimitReached()) {
			frmIBMyReceipentsAddBankAccnt.btnRcConfAddAccount.skin = btnIBaddsmalldis;
			frmIBMyReceipentsAddBankAccnt.btnRcConfAddAccount.setEnabled(false);
			return;
		} else {
			frmIBMyReceipentsAddBankAccnt.btnRcConfAddAccount.skin = btnIBaddsmall;
			frmIBMyReceipentsAddBankAccnt.btnRcConfAddAccount.setEnabled(true);
		}
	}
	if (form == 4) {
		frmIBMyReceipentsAddContactManually.segmentRcAccounts.removeAt(position);
		frmIBMyReceipentsAddContactManually.hbox101000183152453.setVisibility(true)
		frmIBMyReceipentsAddContactManually.link50273743118087.skin=lnkIB18pxBlue;
		frmIBMyReceipentsAddContactManually.lblPlus.skin=lblIB30pxBlue;
		frmIBMyReceipentsAddContactManually.hbox101000183152453.setVisibility(true);
		if (frmIBMyReceipentsAddContactManually.segmentRcAccounts.data.length > 1) {
			frmIBMyReceipentsAddContactManually.label10107168132952.text = kony.i18n.getLocalizedString("Receipent_Accounts");
		} else if(frmIBMyReceipentsAddContactManually.segmentRcAccounts.data.length == 1){
			frmIBMyReceipentsAddContactManually.label10107168132952.text = kony.i18n.getLocalizedString("Receipent_Account");
		}
		else
		{
		frmIBMyReceipentsAddContactManually.label10107168132952.setVisibility(false);
		frmIBMyReceipentsAddContactManually.scrollboxRcAccounts.setVisibility(false);
		}
	}
	
}
/**
 * Method to set the available banks fron cache to selection combo box
 * @returns {}
 */

function setAvailableBanksFromCacheToCombo() {
	frmIBMyReceipentsAddBankAccnt.comboBankName.masterData = globalSelectBankData;
}
/**
 * Method to procced to nect step on successfule verification on TMB/non TMB account
 * @returns {}
 */

function proceedToBackOnCancel() {
if(gblAdd_Receipent_State==gblEXISTING_RC_BANKACCNT_EDITION)
frmIBMyReceipentsAccounts.show();
else{
if(checkAccountCountBulkAddition()==0){
		if(gblAdd_Receipent_State == gblEXISTING_RC_BANKACCNT_ADDITION){
			frmIBMyReceipentsAccounts.show();
		}else{
			frmIBMyReceipentsAddContactManually.show();
		}
}else{
	invokeCommonIBLogger("gblAdd_Receipent_State" + gblAdd_Receipent_State);
	invokeCommonIBLogger("VERIFICATION DONE>>>>>>PROCEED....");
	if (gblAdd_Receipent_State == gblNEW_RC_ADDITION || gblAdd_Receipent_State == gblNEW_RC_ADDITION_PRECONFIRM) {
		if (checkAccountCountBulkAddition() == MAX_BANK_ACCNT_BULK_ADD) {
		frmIBMyReceipentsAddContactManually.hbox101000183152453.setVisibility(true)
       frmIBMyReceipentsAddContactManually.link50273743118087.skin=link18pxGreyIB;
       frmIBMyReceipentsAddContactManually.lblPlus.skin=lblIB30pxGrey;
	  } 
		frmIBMyReceipentsAddContactManually.show();
		//setToBeSaveBankDatatoNewUserPreConfirmationSeg(accountName);
		frmIBMyReceipentsAddContactManually.hboxMyRecipientDetail.setVisibility(true);
		frmIBMyReceipentsAddContactManually.hboxMyRecipient.setVisibility(false);
		frmIBMyReceipentsAddContactManually.label10107168132952.setVisibility(true);
		frmIBMyReceipentsAddContactManually.scrollboxRcAccounts.setVisibility(true);
		frmIBMyReceipentsAddContactManually.line101071681314188.setVisibility(true);
		frmIBMyReceipentsAddContactManually.hbxotpreconfrm.setVisibility(false);
	}
	if (gblAdd_Receipent_State == gblEXISTING_RC_EDITION ||
		gblAdd_Receipent_State == gblEXISTING_RC_BANKACCNT_ADDITION) {
		//getListofExistingAccounts();
		//cacheTobeSavedBankData();
		//setToBeSaveBankDatatoSeg(accountName);
		//checkforDuplicaterecord();
frmIBMyReceipentsAddBankAccnt.show();
		frmIBMyReceipentsAddBankAccnt.hboxAddAccntConf.setVisibility(true);
		frmIBMyReceipentsAddBankAccnt.hboxTMBSelectAccnt.setVisibility(false);
		if ((checkAccountCountBulkAddition() == MAX_BANK_ACCNT_BULK_ADD) || checkRcAccountLimitReached()) {
			frmIBMyReceipentsAddBankAccnt.btnRcConfAddAccount.skin = btnIBaddsmalldis;
			frmIBMyReceipentsAddBankAccnt.btnRcConfAddAccount.setEnabled(false);
			return;
		} else {
			frmIBMyReceipentsAddBankAccnt.btnRcConfAddAccount.skin = btnIBaddsmall;
			frmIBMyReceipentsAddBankAccnt.btnRcConfAddAccount.setEnabled(true);
		}
	}
	
	}
	}
}

function proceedOnSuccesssfulVerification(accountName) {
	invokeCommonIBLogger("gblAdd_Receipent_State" + gblAdd_Receipent_State);
	invokeCommonIBLogger("VERIFICATION DONE>>>>>>PROCEED....");
	if (gblAdd_Receipent_State == gblNEW_RC_ADDITION || gblAdd_Receipent_State == gblNEW_RC_ADDITION_PRECONFIRM) {
		if (checkAccountCountBulkAddition() == MAX_BANK_ACCNT_BULK_ADD-1) {
		frmIBMyReceipentsAddContactManually.hbox101000183152453.setVisibility(true)
      	 	frmIBMyReceipentsAddContactManually.link50273743118087.skin=link18pxGreyIB;
       		frmIBMyReceipentsAddContactManually.lblPlus.skin=lblIB30pxGrey;
	 	 } 
		frmIBMyReceipentsAddContactManually.show();
		
		setToBeSaveBankDatatoNewUserPreConfirmationSeg(accountName);
		frmIBMyReceipentsAddContactManually.hboxMyRecipientDetail.setVisibility(true);
		frmIBMyReceipentsAddContactManually.hboxMyRecipient.setVisibility(false);
		frmIBMyReceipentsAddContactManually.label10107168132952.setVisibility(true);
		frmIBMyReceipentsAddContactManually.scrollboxRcAccounts.setVisibility(true);
		frmIBMyReceipentsAddContactManually.line101071681314188.setVisibility(true);
		frmIBMyReceipentsAddContactManually.hbxotpreconfrm.setVisibility(false);
		
		if(checkAccountCountBulkAddition()==1 && recipientAddFromTransfer){
      		frmIBMyReceipentsAddContactManually.hbox101000183152453.setVisibility(false);
      	}else{
      		frmIBMyReceipentsAddContactManually.hbox101000183152453.setVisibility(true);
      	}
	}
	if (gblAdd_Receipent_State == gblEXISTING_RC_EDITION ||
		gblAdd_Receipent_State == gblEXISTING_RC_BANKACCNT_ADDITION) {
		getListofExistingAccounts();
		cacheTobeSavedBankData();
		setToBeSaveBankDatatoSeg(accountName);
		checkforDuplicaterecord();
		
		//hiding OTP screen
		frmIBMyReceipentsAddBankAccnt.hbxOtpBox.setVisibility(false);
		frmIBMyReceipentsAddBankAccnt.hboxAddAccntConf.setVisibility(true);
		frmIBMyReceipentsAddBankAccnt.hboxTMBSelectAccnt.setVisibility(false);
		if ((checkAccountCountBulkAddition() == MAX_BANK_ACCNT_BULK_ADD) || checkRcAccountLimitReached()) {
		
			frmIBMyReceipentsAddBankAccnt.btnRcConfAddAccount.skin = btnIBaddsmalldis;
			frmIBMyReceipentsAddBankAccnt.btnRcConfAddAccount.setEnabled(false);
			return;
		} else {
			frmIBMyReceipentsAddBankAccnt.btnRcConfAddAccount.skin = btnIBaddsmall;
			frmIBMyReceipentsAddBankAccnt.btnRcConfAddAccount.setEnabled(true);
		}
	}
	if (gblAdd_Receipent_State == gblEXISTING_RC_BANKACCNT_EDITION) {
		editReceipentAccount(frmIBMyReceipentsAddBankAccnt.comboBankName.selectedKey[0], frmIBMyReceipentsAddBankAccnt.tbxAddAccntNumber
			.text,
			frmIBMyReceipentsAddBankAccnt.tbxAddAccntNickName.text,frmIBMyReceipentsAddBankAccnt.tbxAddAccntAccountName.text);
	}
}
/**
 * Method to set the edited recipient account data
 * @returns {}
 */

function setEditedRecipientAccounData() {
	var totalData = [];
	var textbtnTransfer = {
								text: kony.i18n.getLocalizedString("Transfer"),
								skin: "btnIB80px"
								};
	var segDataMap;
	if (frmIBMyReceipentsAddBankAccnt.comboBankName.selectedKey != null && frmIBMyReceipentsAddBankAccnt.comboBankName.selectedKey ==
		TMB_BANK_CODE) {
		segDataMap = {
			lblBankTitle: kony.i18n.getLocalizedString("Receipent_Bank"),
			lblNumberTitle: kony.i18n.getLocalizedString("Receipent_Number"),
			lblNickNameTitle: kony.i18n.getLocalizedString("Receipent_NickName"),
			btnTransfer: textbtnTransfer,
			lblBankName: frmIBMyReceipentsAddBankAccnt.comboBankName.selectedKeyValue[1],
			lblNumber: frmIBMyReceipentsAddBankAccnt.tbxAddAccntNumber.text,
			lblNickName: frmIBMyReceipentsAddBankAccnt.tbxAddAccntNickName.text,
			bankLogo: "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+frmIBMyReceipentsAddBankAccnt.comboBankName.selectedKey+"&modIdentifier=BANKICON"
		};
	} else {
			if(getORFTFlagIB(frmIBMyReceipentsAddBankAccnt.comboBankName.selectedKey) == "Y"){
				segDataMap = {
					lblBankTitle: kony.i18n.getLocalizedString("Receipent_Bank"),
					lblNumberTitle: kony.i18n.getLocalizedString("Receipent_Number"),
					lblNickNameTitle: kony.i18n.getLocalizedString("Receipent_NickName"),
					btnTransfer: textbtnTransfer,
					lblBankName: frmIBMyReceipentsAddBankAccnt.comboBankName.selectedKeyValue[1],
					lblNumber: frmIBMyReceipentsAddBankAccnt.tbxAddAccntNumber.text,
					lblNickName: frmIBMyReceipentsAddBankAccnt.tbxAddAccntNickName.text,
					bankLogo: "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+frmIBMyReceipentsAddBankAccnt.comboBankName.selectedKey+"&modIdentifier=BANKICON"
				};
		}else{
			segDataMap = {
					lblBankTitle: kony.i18n.getLocalizedString("Receipent_Bank"),
					lblNumberTitle: kony.i18n.getLocalizedString("Receipent_Number"),
					lblNickNameTitle: kony.i18n.getLocalizedString("Receipent_NickName"),
					lblAccountNameTitle: kony.i18n.getLocalizedString("AccountName"),
					btnTransfer: textbtnTransfer,
					lblAccountName:frmIBMyReceipentsAddBankAccnt.tbxAddAccntAccountName.text,
					lblBankName: frmIBMyReceipentsAddBankAccnt.comboBankName.selectedKeyValue[1],
					lblNumber: frmIBMyReceipentsAddBankAccnt.tbxAddAccntNumber.text,
					lblNickName: frmIBMyReceipentsAddBankAccnt.tbxAddAccntNickName.text,
					bankLogo: "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+frmIBMyReceipentsAddBankAccnt.comboBankName.selectedKey+"&modIdentifier=BANKICON"
				};
		}
	}
	totalData.push(segDataMap);
	frmIBMyReceipentsEditAccountConf.segementTobeAddedAccntsConf.setData(totalData);
	/*activityLogServiceCall("046", "", "01", "", "Edited Recipient account", frmIBMyReceipentsAddBankAccnt.tbxAddAccntNickName.text,
		frmIBMyReceipentsAddBankAccnt.comboBankName.selectedKeyValue[1], frmIBMyReceipentsAddBankAccnt.tbxAddAccntNumber.text,
		"", "");*/
}
/**
 * Method to validate account for TMB/non TMB
 * @returns {}
 */

function checknValidateAccountToProceed() {
	var toAccnt = decodeAccountNumbers(frmIBMyReceipentsAddBankAccnt.tbxAddAccntNumber.text);
	invokeCommonIBLogger("To be added account number " + toAccnt);
	invokeCommonIBLogger("To be added account number length " + toAccnt.length);
	var x=getBankAccountLengths(frmIBMyReceipentsAddBankAccnt.comboBankName.selectedKey)
	var y=x.split(",");
	var z=parseInt(y[0])
	if (toAccnt.length < z || getBankAccountLengths(frmIBMyReceipentsAddBankAccnt.comboBankName.selectedKey).indexOf(toAccnt.length)<0) {
		alert(kony.i18n.getLocalizedString("keyincorrectAccNum"));
		return;
	}
	if (gblAdd_Receipent_State == gblEXISTING_RC_BANKACCNT_EDITION) {
		proceedOnSuccesssfulVerification(null);
	} else {
		if (frmIBMyReceipentsAddBankAccnt.comboBankName.selectedKey == TMB_BANK_CODE || frmIBMyReceipentsAddBankAccnt.comboBankName
			.selectedKey == TMB_BANK_CODE_ZERO_PAD || frmIBMyReceipentsAddBankAccnt.comboBankName.selectedKeyValue[1] ==
			"TMB Bank") {
			invokeCommonIBLogger("Its a TMB Bank Account");
			startDepositAccountEnquiryService();
		} else {
			invokeCommonIBLogger("Its a non TMB Bank Account");
			if (getORFTFlagIB(frmIBMyReceipentsAddBankAccnt.comboBankName.selectedKey) == "Y") {
				startORFTAccountEnquiryService();
			} else {
			  	var inputParams = {}
				var toAccnt = decodeAccountNumbers(frmIBMyReceipentsAddBankAccnt.tbxAddAccntNumber.text);
				var toAccntBnkCde =frmIBMyReceipentsAddBankAccnt.comboBankName.selectedKey;
				gblDefaultAccountNum = "0012005864";
				inputParams["fromAcctNo"]= gblDefaultAccountNum;
				inputParams["toFIIdent"]= toAccntBnkCde;
				inputParams["isFromRecipient"]= "yes";
				inputParams["AccountCheck"]= "yes";
				inputParams["toAcctNo"]= toAccnt;
				inputParams["bankcode"]= toAccntBnkCde;
				showLoadingScreenPopup();
				invokeServiceSecureAsync("ORFTInq", inputParams, startORFTAccountEnquiryServiceAsyncCallbackSMART);
				//proceedOnSuccesssfulVerification(null);
			}
		}
	}
}


function startORFTAccountEnquiryServiceAsyncCallbackSMART(status, callBackResponse){
		if (status == 400) {
			dismissLoadingScreenPopup();
			if (callBackResponse["opstatus"] == 0) {
				proceedOnSuccesssfulVerification(frmIBMyReceipentsAddBankAccnt.tbxAddAccntAccountName.text);
			}
			else{
				if (callBackResponse["errMsg"] == undefined || callBackResponse["errMsg"] == null)
							showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
						else
							alert(" " + callBackResponse["errMsg"]);
			}
		}
}

/**
 * Method to set the data for bankAccount edit
 * @returns {}
 */
function setDataForBankAccntEdit() {
	var masterData = [];
	tempData = [gblSelectedAccountCode, frmIBMyReceipentsAccounts.lblBankNameAccntDetails.text];
	masterData.push(tempData);
	frmIBMyReceipentsAddBankAccnt.comboBankName.masterData = masterData;
	frmIBMyReceipentsAddBankAccnt.tbxAddAccntNumber.text = frmIBMyReceipentsAccounts.lblNumberAccntDetails.text;
	frmIBMyReceipentsAddBankAccnt.tbxAddAccntNickName.text = frmIBMyReceipentsAccounts.lblNickNameAccntDetails.text;
	if(getORFTFlagIB(gblSelectedAccountCode) == "Y"){
		frmIBMyReceipentsAddBankAccnt.hboxAddAccntAccountName.setVisibility(false);
		frmIBMyReceipentsAddBankAccnt.line967337254161523.setVisibility(false)
	}else{
		frmIBMyReceipentsAddBankAccnt.tbxAddAccntAccountName.text=frmIBMyReceipentsAccounts.lblAcctNameAccntDetails.text;
		frmIBMyReceipentsAddBankAccnt.hboxAddAccntAccountName.setVisibility(true);
		frmIBMyReceipentsAddBankAccnt.line967337254161523.setVisibility(true)
	}
}
/**
 * Method to check the account for duplicate nicknames while addition
 * @returns {}
 */

function checkAccountForDuplicateNickNames() {
	var tempData = [];
	//Clear tempData
	if (tempData != null) {
		for (var j = 0; j < tempData.length; j++) {
			tempData.pop();
		}
	}
	if (NickNameValid(frmIBMyReceipentsAddBankAccnt.tbxAddAccntNickName.text) == false) {
		return true;
	}
	invokeCommonIBLogger("Add_Receipent_State" + gblAdd_Receipent_State);
	if (gblAdd_Receipent_State == gblNEW_RC_ADDITION || gblAdd_Receipent_State == gblNEW_RC_ADDITION_PRECONFIRM) {
		tempData.push(frmIBMyReceipentsAddContactManually.segmentRcAccounts.data);
		if (tempData != null && tempData[0] != null) {
			for (var i = 0; i < tempData[0].length; i++) {
				invokeCommonIBLogger("Src nickname " + frmIBMyReceipentsAddBankAccnt.tbxAddAccntNickName.text);
				invokeCommonIBLogger("Target nickname " + tempData[0][i].lblNickName);
				//Check nickname
				if (frmIBMyReceipentsAddBankAccnt.tbxAddAccntNickName.text == tempData[0][i].lblNickName) {
					alert("" + kony.i18n.getLocalizedString("Receipent_alert_correctnickname"));
					return true;
				}
				//Check account number
				if (frmIBMyReceipentsAddBankAccnt.tbxAddAccntNumber.text == tempData[0][i].lblNumber) {
					alert("" + kony.i18n.getLocalizedString("Receipent_alert_correctaccount"));
					return true;
				}
			}
		}
	} else if (gblAdd_Receipent_State == gblEXISTING_RC_BANKACCNT_ADDITION) {
		tempData.push(frmIBMyReceipentsAddBankAccnt.segementTobeAddedAccnts.data);
		tempData.push(frmIBMyReceipentsAccounts.segmentRcAccounts.data);
		if (tempData != null) {
			if (tempData[0] != null) {
				//Check for duplicate nickname while adding in bulk
				for (var i = 0; i < tempData[0].length; i++) {
					invokeCommonIBLogger("Src nickname " + frmIBMyReceipentsAddBankAccnt.tbxAddAccntNickName.text);
					invokeCommonIBLogger("Target nickname " + tempData[0][i].lblNickName);
					//Check nickname
					if (frmIBMyReceipentsAddBankAccnt.tbxAddAccntNickName.text == tempData[0][i].lblNickName) {
						alert("" + kony.i18n.getLocalizedString("Receipent_alert_correctnickname"));
						return true;
					}
					//Check account number
					if (frmIBMyReceipentsAddBankAccnt.tbxAddAccntNumber.text == tempData[0][i].lblNumber) {
						alert("" + kony.i18n.getLocalizedString("Receipent_alert_correctaccount"));
						return true;
					}
				}
			}
			if (tempData[1] != null) {
				//Check for duplicate nickname with already existing accounts
				for (var i = 0; i < tempData[1].length; i++) {
					invokeCommonIBLogger("Src nickname " + frmIBMyReceipentsAddBankAccnt.tbxAddAccntNickName.text);
					invokeCommonIBLogger("Target nickname " + tempData[1][i].lblNickName);
					//Check nickname
					if (frmIBMyReceipentsAddBankAccnt.tbxAddAccntNickName.text == tempData[1][i].lblNickName) {
						alert("" + kony.i18n.getLocalizedString("Receipent_alert_correctnickname"));
						return true;
					}
					//Check account number
					if (frmIBMyReceipentsAddBankAccnt.tbxAddAccntNumber.text == tempData[1][i].lblNumber) {
						alert("" + kony.i18n.getLocalizedString("Receipent_alert_correctaccount"));
						return true;
					}
				}
			}
		}
	} else if (gblAdd_Receipent_State == gblEXISTING_RC_BANKACCNT_EDITION) {
		tempData.push(frmIBMyReceipentsAccounts.segmentRcAccounts.data);
		if (tempData != null && tempData[0] != null) {
			for (var i = 0; i < tempData[0].length; i++) {
				invokeCommonIBLogger("Src nickname " + frmIBMyReceipentsAddBankAccnt.tbxAddAccntNickName.text);
				invokeCommonIBLogger("Target nickname " + tempData[0][i].lblNickName);
				//Check nickname
				if (frmIBMyReceipentsAddBankAccnt.tbxAddAccntNickName.text == tempData[0][i].lblNickName) {
					alert("" + kony.i18n.getLocalizedString("Receipent_alert_correctnickname"));
					return true;
				}
			}
		}
	}
	return false;
}
/**
 * Method to check the account count in case of bulk addition...this is max 5 at one go
 * @returns {}
 */

function checkAccountCountBulkAddition() {
	switch (parseInt(gblAdd_Receipent_State)) {
	case gblNEW_RC_ADDITION:
	case gblNEW_RC_ADDITION_PRECONFIRM:
		{
			if (frmIBMyReceipentsAddContactManually.segmentRcAccounts.data != null) {
				if (frmIBMyReceipentsAddContactManually.segmentRcAccounts.data.length == MAX_BANK_ACCNT_BULK_ADD) {
					return MAX_BANK_ACCNT_BULK_ADD;
				} else {
					return frmIBMyReceipentsAddContactManually.segmentRcAccounts.data.length;
				}
			}
			return 0;
		}
		break;
	case gblEXISTING_RC_BANKACCNT_ADDITION:
		{
			if (frmIBMyReceipentsAddBankAccnt.segementTobeAddedAccnts.data != null) {
				if (frmIBMyReceipentsAddBankAccnt.segementTobeAddedAccnts.data.length == MAX_BANK_ACCNT_BULK_ADD) {
					return MAX_BANK_ACCNT_BULK_ADD;
				} else {
					return frmIBMyReceipentsAddBankAccnt.segementTobeAddedAccnts.data.length;
				}
			}
			return 0;
		}
		break;
	default:
		break;
	}
	return 0;
}
/**
 * Method to check if the account limit is reached for adding recipient accounts
 * @returns {}
 */

function checkRcAccountLimitReached() {
	if (frmIBMyReceipentsAccounts.segmentRcAccounts.data != null && frmIBMyReceipentsAccounts.segmentRcAccounts.data.length >
		0) {
		if (frmIBMyReceipentsAccounts.segmentRcAccounts.data.length >= MAX_BANK_ACCNT_PER_RC) {
			return true;
		} else {
			if (frmIBMyReceipentsAccounts.segmentRcAccounts.data.length < MAX_BANK_ACCNT_PER_RC) {
				if (frmIBMyReceipentsAddBankAccnt.segementTobeAddedAccnts.data != null &&
					frmIBMyReceipentsAddBankAccnt.segementTobeAddedAccnts.data.length > 0) {
					var total = frmIBMyReceipentsAccounts.segmentRcAccounts.data.length + frmIBMyReceipentsAddBankAccnt.segementTobeAddedAccnts
						.data.length;
					invokeCommonIBLogger("Total accounts for recipient " + total);
					if (parseInt(total.toString()) >= MAX_BANK_ACCNT_PER_RC) {
						return true;
					}
				}
			}
		}
	}
	return false;
}

/**
 * Method to check the mapped bank account length
 * @returns {}
 */

function checkBankAccountLength(bankCode) {
	if (globalSelectBankData != null && globalSelectBankData.length > 0) {
		for (var i = 0; i < globalSelectBankData.length; i++) {
			if (bankCode == globalSelectBankData[i][0]) {
				IS_ORFT = globalSelectBankData[i][3];		// added for ENH_112
				if(IS_ORFT=="Y"){
					frmIBMyReceipentsAddBankAccnt.hboxAddAccntAccountName.isVisible = true;
					frmIBMyReceipentsAddBankAccnt.line101000183142692.isVisible = true;
				}	
				else{
					frmIBMyReceipentsAddBankAccnt.hboxAddAccntAccountName.isVisible = false;
					frmIBMyReceipentsAddBankAccnt.line101000183142692.isVisible = false;
				}		
				return globalSelectBankData[i][5];
			}
		}
	}
	
	invokeCommonIBLogger("Length of account is 10");
	return 10;
}
function getBankAccountLengths(bankCode) {
	if (globalSelectBankData != null && globalSelectBankData.length > 0) {
		for (var i = 0; i < globalSelectBankData.length; i++) {
			if (bankCode == globalSelectBankData[i][0]) {
				return globalSelectBankData[i][6];
			}
		}
	}
	return 10;
}

/**
 * Method to show the details of the account on which the user clicks
 * @returns {}
 */

function showClickedAccntDetails() {
	gblSelectedAccountIndex = frmIBMyReceipentsAccounts.segmentRcAccounts.selectedIndex[1];
	gblSelectedAccountCode = frmIBMyReceipentsAccounts.segmentRcAccounts.selectedItems[0]["selectedBankCD"];
	invokeCommonIBLogger("gblSelectedAccountIndex on click" + gblSelectedAccountIndex);
	invokeCommonIBLogger("gblSelectedAccountCode on click" + gblSelectedAccountCode);
	frmIBMyReceipentsAccounts.lblBankNameAccntDetails.text = frmIBMyReceipentsAccounts.segmentRcAccounts.selectedItems[0][
		"lblBankName"];
	frmIBMyReceipentsAccounts.lblNumberAccntDetails.text = frmIBMyReceipentsAccounts.segmentRcAccounts.selectedItems[0][
		"lblNumber"];
	frmIBMyReceipentsAccounts.lblNickNameAccntDetails.text = frmIBMyReceipentsAccounts.segmentRcAccounts.selectedItems[0][
		"lblNickName"];
	if ((frmIBMyReceipentsAccounts.segmentRcAccounts.selectedItems[0]["selectedBankCD"] == TMB_BANK_CODE ||
			frmIBMyReceipentsAccounts.segmentRcAccounts.selectedItems[0]["selectedBankCD"] == TMB_BANK_CODE_ZERO_PAD) &&
		frmIBMyReceipentsAccounts.segmentRcAccounts.selectedItems[0]["lblBankName"] == "TMB Bank") {
		invokeCommonIBLogger("TMB BANK ACCOUNT");
		frmIBMyReceipentsAccounts.hboxAcctNameAcctDetails.setVisibility(true);
		if (frmIBMyReceipentsAccounts.segmentRcAccounts.selectedItems[0]["lblAccountName"] != null) {
			frmIBMyReceipentsAccounts.lblAcctNameAccntDetails.text = frmIBMyReceipentsAccounts.segmentRcAccounts.selectedItems[0]
			["lblAccountName"];
		}
	} else {
			if (frmIBMyReceipentsAccounts.segmentRcAccounts.selectedItems[0]["lblAccountName"] != null) {
			frmIBMyReceipentsAccounts.lblAcctNameAccntDetails.text = frmIBMyReceipentsAccounts.segmentRcAccounts.selectedItems[0]
			["lblAccountName"];
			}
		frmIBMyReceipentsAccounts.hboxAcctNameAcctDetails.setVisibility(true);
	}
}
/**
 * Method to delete an account
 * @returns {}
 */

function deleteAccount() {
	invokeCommonIBLogger("gblSelectedAccountIndex on delete" + gblSelectedAccountIndex);
	frmIBMyReceipentsAccounts.segmentRcAccounts.removeAt(gblSelectedAccountIndex);
	if (frmIBMyReceipentsAccounts.segmentRcAccounts.data != null && frmIBMyReceipentsAccounts.segmentRcAccounts.data.length >
		0) {
		if (frmIBMyReceipentsAccounts.segmentRcAccounts.data.length >= MAX_BANK_ACCNT_PER_RC) {
			frmIBMyReceipentsAccounts.btnAddAccount.skin = btnIBaddsmalldis;
			frmIBMyReceipentsAccounts.btnAddAccount.setEnabled(false);
		} else {
			frmIBMyReceipentsAccounts.btnAddAccount.skin = btnIBaddsmall;
			frmIBMyReceipentsAccounts.btnAddAccount.setEnabled(true);
		}
	}
}

/**
 * Method to handle the locale change for Rc Home screen
 * @returns {}
 */

function handleLocalChangeRcHomeScreen() {
	invokeCommonIBLogger("Syncing locale......frmIBMyReceipentsHome");
	frmIBMyReceipentsHome.lblMyReceipents.text = kony.i18n.getLocalizedString("Receipent_MyReceipents");
	var isIE8 = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
	 if(!isIE8){
	frmIBMyReceipentsHome.tbxSearch.placeholder = kony.i18n.getLocalizedString("keySearch");
	}
	 if(navigator.userAgent.match(/iPad/i)){
			frmIBMyReceipentsHome.lblMyReceipents.setFocus(true);
		}
		else{
	frmIBMyReceipentsHome.tbxSearch.setFocus(true);
		}
}
/**
 * Method to handle locale change for Rc Accounts screen
 * @returns {}
 */

function handleLocaleChangeRcAccountsScreen() {
	invokeCommonIBLogger("Syncing locale......frmIBMyReceipentsAccounts");
	frmIBMyReceipentsAccounts.lblMyReceipents.text = kony.i18n.getLocalizedString("Receipent_MyReceipents");
	frmIBMyReceipentsAccounts.lblRcDataTitle.text = kony.i18n.getLocalizedString("Receipent_MyReceipents");
	frmIBMyReceipentsAccounts.lblMobileRcTitle.text = kony.i18n.getLocalizedString("Receipent_Mobileno");
	frmIBMyReceipentsAccounts.labelRcEmailTitle.text = kony.i18n.getLocalizedString("Receipent_Email");
	frmIBMyReceipentsAccounts.lblRcFbIdTitle.text = kony.i18n.getLocalizedString("Receipent_FacebookID");
	frmIBMyReceipentsAccounts.lblAccountTitle.text = kony.i18n.getLocalizedString("Receipent_Accounts");
	frmIBMyReceipentsAccounts.lblRcAccntDetailsTitle.text = kony.i18n.getLocalizedString("Receipent_ReceipentAccount");
	frmIBMyReceipentsAccounts.lblBankTitleAccntDetails.text = kony.i18n.getLocalizedString("Receipent_Bank");
	frmIBMyReceipentsAccounts.lblNumberTitleAccntDetails.text = kony.i18n.getLocalizedString("Receipent_Number");
	frmIBMyReceipentsAccounts.lblNickNameTitleAccntDetails.text = kony.i18n.getLocalizedString("Receipent_NickName");
	frmIBMyReceipentsAccounts.btnBackAccntDetails.text = kony.i18n.getLocalizedString("Receipent_Back");
	frmIBMyReceipentsAccounts.lblAcctNameTitleAcctDetails.text = kony.i18n.getLocalizedString("AccountName");
	frmIBMyReceipentsAccounts.tbxSearch.placeholder= kony.i18n.getLocalizedString("keySearch");
	frmIBMyReceipentsAccounts.tbxSearch.placeholder= kony.i18n.getLocalizedString("FindTMB_Search");
	
}
/**
 * Method to handle locale change for Rc Add Bank Account screen
 * @returns {}
 */

function handleLocaleChangeRcAddBankAccountScreen() {
	invokeCommonIBLogger("Syncing locale......frmIBMyReceipentsAddBankAccnt");
	frmIBMyReceipentsAddBankAccnt.lblMyReceipents.text = kony.i18n.getLocalizedString("Receipent_MyReceipents");
		if (gblAdd_Receipent_State == gblEXISTING_RC_BANKACCNT_EDITION) {
			frmIBMyReceipentsAddBankAccnt.lblAddBnkAccnt.text = kony.i18n.getLocalizedString("keyRecipientEditAccount");
		}else
		{
			frmIBMyReceipentsAddBankAccnt.lblAddBnkAccnt.text = kony.i18n.getLocalizedString("Receipent_AddBankAccount");
		}
	
	frmIBMyReceipentsAddBankAccnt.lblAddAccntNumberTitle.text = kony.i18n.getLocalizedString("keyAccountNoIB");
	frmIBMyReceipentsAddBankAccnt.lblAddAccntNameTitle.text = kony.i18n.getLocalizedString("Receipent_NickName");
	frmIBMyReceipentsAddBankAccnt.lblAccntConfTitle.text = kony.i18n.getLocalizedString("Receipent_Confirmation");
	frmIBMyReceipentsAddBankAccnt.lblMobileAddRcConfTitle.text = kony.i18n.getLocalizedString("Receipent_Mobileno");
	frmIBMyReceipentsAddBankAccnt.lblAddRcConfEmailTitle.text = kony.i18n.getLocalizedString("Receipent_Email");
	frmIBMyReceipentsAddBankAccnt.lblAddRcConfFbTitle.text = kony.i18n.getLocalizedString("Receipent_FacebookID");
	frmIBMyReceipentsAddBankAccnt.btnAddAccntCancel.text = kony.i18n.getLocalizedString("Receipent_Cancel");
//	frmIBMyReceipentsAddBankAccnt.tbxAddAccntNumber.placeholder = kony.i18n.getLocalizedString("AccountNumber");
	frmIBMyReceipentsAddBankAccnt.tbxSearch.placeholder = kony.i18n.getLocalizedString("keySearch");
	if(frmIBMyReceipentsAddBankAccnt.segementTobeAddedAccnts.data != null && frmIBMyReceipentsAddBankAccnt.segementTobeAddedAccnts.data != undefined){
		if (frmIBMyReceipentsAddBankAccnt.segementTobeAddedAccnts.data.length > 1) {
			frmIBMyReceipentsAddBankAccnt.lblAddRcConfAccountTitle.text = kony.i18n.getLocalizedString("Receipent_Accounts");
		} else {
			frmIBMyReceipentsAddBankAccnt.lblAddRcConfAccountTitle.text = kony.i18n.getLocalizedString("Receipent_Account");
		}
	}
	if (gblAdd_Receipent_State == gblEXISTING_RC_BANKACCNT_EDITION) {
		frmIBMyReceipentsAddBankAccnt.btnAddAccntNext.text = kony.i18n.getLocalizedString("keysave");
	}
	//reload account list segment to sync locale
	var tempData = [];
	tempData.push(frmIBMyReceipentsAddBankAccnt.segementTobeAddedAccnts.data);
	var totalData = [];
	if (tempData[0] != null && tempData[0]!=undefined) {
		for (var i = 0; i < tempData[0].length; i++) {
			var segDataMap;
			var bankNameLocale = "NULL";
			bankNameLocale = getBankNameCurrentLocale(globalTobeAddedBankDataConfirmation[i][3]);
			if (tempData[0][i].lblAccountName != null && tempData[0][i].lblAccountName != "") {
				segDataMap = {
					lblBankTitle: tempData[0][i].lblBankTitle,
					lblNumberTitle: kony.i18n.getLocalizedString("Receipent_Number"),
					lblNickNameTitle: kony.i18n.getLocalizedString("Receipent_NickName"),
					lblBankName: bankNameLocale,
					lblNumber: tempData[0][i].lblNumber,
					lblNickName: tempData[0][i].lblNickName,
					lblAccountNameTitle: kony.i18n.getLocalizedString("AccountName"),
					lblAccountName: tempData[0][i].lblAccountName,
					bankLogo: tempData[0][i].bankLogo
				};
			} else {
				segDataMap = {
					lblBankTitle: tempData[0][i].lblBankTitle,
					lblNumberTitle: kony.i18n.getLocalizedString("Receipent_Number"),
					lblNickNameTitle: kony.i18n.getLocalizedString("Receipent_NickName"),
					lblBankName: bankNameLocale,
					lblNumber: tempData[0][i].lblNumber,
					lblNickName: tempData[0][i].lblNickName,
					bankLogo: tempData[0][i].bankLogo
				};
			}
			totalData.push(segDataMap);
			/*activityLogServiceCall("046", "", "01", "", "Added Recipient account", tempData[0][i].lblNickName, tempData[0][i].lblBankName,
				tempData[0][i].lblNumber, "", "");*/
		}
		frmIBMyReceipentsAddBankAccnt.segementTobeAddedAccnts.setData(totalData);
	}
	frmIBMyReceipentsAddBankAccnt.btnAddRcConfCancel.text = kony.i18n.getLocalizedString("Receipent_Cancel");
	frmIBMyReceipentsAddBankAccnt.btnAddRcConfConfirm.text = kony.i18n.getLocalizedString("Receipent_Confirm");
	frmIBMyReceipentsAddBankAccnt.btnEditAddRcConf.text = kony.i18n.getLocalizedString("Receipent_Confirm");
	frmIBMyReceipentsAddBankAccnt.lblBankRef.text = kony.i18n.getLocalizedString("BankRef.No");
	frmIBMyReceipentsAddBankAccnt.label476047582115277.text=kony.i18n.getLocalizedString("keyotpmsg");
	if(gblTokenSwitchFlag == true && gblSwitchToken == false){
			frmIBMyReceipentsAddBankAccnt.label476047582115288.text = kony.i18n.getLocalizedString("Receipent_tokenId");
			frmIBMyReceipentsAddBankAccnt.label476047582115284.text = kony.i18n.getLocalizedString("Receipent_Token");
			frmIBMyReceipentsAddBankAccnt.btnOTPReq.text = kony.i18n.getLocalizedString("Receipent_SwitchtoOTP");
		//	frmIBMyReceipentsAddBankAccnt.textbox247428873338513.placeholder = kony.i18n.getLocalizedString("enterToken");
		}else{				
			frmIBMyReceipentsAddBankAccnt.label476047582115288.text = kony.i18n.getLocalizedString("keyotpmsgreq");
			frmIBMyReceipentsAddBankAccnt.label476047582115284.text = kony.i18n.getLocalizedString("Receipent_OTP");
			frmIBMyReceipentsAddBankAccnt.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
		//	frmIBMyReceipentsAddBankAccnt.textbox247428873338513.placeholder = kony.i18n.getLocalizedString("enterOTP");
		}
}
/**
 * Method to handle locale change for Rc Add contact Fb Screen
 * @returns {}
 */

function handleLocaleChangeRcAddContactFbScreen() {
	invokeCommonIBLogger("Syncing locale......frmIBMyReceipentsAddContactFB");
	frmIBMyReceipentsAddContactFB.lblMyReceipents.text = kony.i18n.getLocalizedString("Receipent_MyReceipents");
	frmIBMyReceipentsAddContactFB.lblSelectRcTitle.text = kony.i18n.getLocalizedString("Receipent_SelectReceipents");
	frmIBMyReceipentsAddContactFB.lblSelectRcConfTitle.text = kony.i18n.getLocalizedString("Receipent_Confirmation");
	frmIBMyReceipentsAddContactFB.btnSelectRcNext.text = kony.i18n.getLocalizedString("Receipent_Next");
	//frmIBMyReceipentsAddContactFB.btnSelectRcConfNext.text = kony.i18n.getLocalizedString("Receipent_Next");
	frmIBMyReceipentsAddContactFB.lblBankRef.text = kony.i18n.getLocalizedString("BankRef.No");
	frmIBMyReceipentsAddContactFB.label476047582115277.text=kony.i18n.getLocalizedString("keyotpmsg")
	frmIBMyReceipentsAddContactFB.tbxSearch.placeholder = kony.i18n.getLocalizedString("keySearch");
	frmIBMyReceipentsAddContactFB.tbxSelectRcSearch.placeholder = kony.i18n.getLocalizedString("keySearch");
	frmIBMyReceipentsAddContactFB.linkMore.text = kony.i18n.getLocalizedString("keyMore");
	if(gblTokenSwitchFlag == true && gblSwitchToken == false){
			frmIBMyReceipentsAddContactFB.label476047582115288.text = kony.i18n.getLocalizedString("Receipent_tokenId");
			frmIBMyReceipentsAddContactFB.label476047582115284.text = kony.i18n.getLocalizedString("Receipent_Token");
			frmIBMyReceipentsAddContactFB.btnOTPReq.text = kony.i18n.getLocalizedString("Receipent_SwitchtoOTP");
			//frmIBMyReceipentsAddContactFB.textbox247428873338513.placeholder = kony.i18n.getLocalizedString("enterToken");
	}
	else{
			frmIBMyReceipentsAddContactFB.label476047582115288.text = kony.i18n.getLocalizedString("keyotpmsgreq");
			frmIBMyReceipentsAddContactFB.label476047582115284.text = kony.i18n.getLocalizedString("Receipent_OTP");
			frmIBMyReceipentsAddContactFB.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
			//frmIBMyReceipentsAddContactFB.textbox247428873338513.placeholder =  kony.i18n.getLocalizedString("enterOTP");
	}
}
/**
 * Method to handle locale change for Rc Add Contact Manually screen
 * @returns {}
 */

function handleLocaleChangeRcAddContactManuallyScreen() {
	invokeCommonIBLogger("Syncing locale......frmIBMyReceipentsAddContactManually");
	var isIE8N = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
	frmIBMyReceipentsAddContactManually.lblMyReceipents.text = kony.i18n.getLocalizedString("Receipent_MyReceipents");
	frmIBMyReceipentsAddContactManually.lblheadingReceipent.text = kony.i18n.getLocalizedString("keyMyRecipient");
	frmIBMyReceipentsAddContactManually.label101071681314.text = kony.i18n.getLocalizedString("Receipent_Mobileno");
	frmIBMyReceipentsAddContactManually.label101071681349.text = kony.i18n.getLocalizedString("Receipent_Email");
	frmIBMyReceipentsAddContactManually.label101071681311679.text = kony.i18n.getLocalizedString("keyMyRecipient");
	frmIBMyReceipentsAddContactManually.lblmob.text = kony.i18n.getLocalizedString("Receipent_Mobileno");
	frmIBMyReceipentsAddContactManually.label10107168132681.text = kony.i18n.getLocalizedString("Receipent_Email");
	frmIBMyReceipentsAddContactManually.label10107168132696.text = kony.i18n.getLocalizedString("Receipent_FacebookID");
	frmIBMyReceipentsAddContactManually.link50273743118078.text = kony.i18n.getLocalizedString("Receipent_AddBankAccount");
	frmIBMyReceipentsAddContactManually.link50273743118087.text = kony.i18n.getLocalizedString("Receipent_AddBankAccount");
	frmIBMyReceipentsAddContactManually.lblBankRef.text = kony.i18n.getLocalizedString("BankRef.No");
	frmIBMyReceipentsAddContactManually.lblref.text = kony.i18n.getLocalizedString("BankRef.No");
	frmIBMyReceipentsAddContactManually.button1010716813157.text = kony.i18n.getLocalizedString("Receipent_Next");
	frmIBMyReceipentsAddContactManually.btnAddAccntNext.text = kony.i18n.getLocalizedString("Receipent_Confirm");
	frmIBMyReceipentsAddContactManually.lblotpsent.text = kony.i18n.getLocalizedString("keyotpmsg");
    frmIBMyReceipentsAddContactManually.tbxSearch.placeholder = kony.i18n.getLocalizedString("keySearch");
    if(!isIE8N){
    frmIBMyReceipentsAddContactManually.txtRecipientName.placeholder = kony.i18n.getLocalizedString("Receipent_ReceipentName");
	}else{
	frmIBMyReceipentsAddContactManually.txtRecipientName.placeholder="";
	}
	if(gblTokenSwitchFlag == true && gblSwitchToken == false){
		frmIBMyReceipentsAddContactManually.otpmsglbl.text = kony.i18n.getLocalizedString("Receipent_tokenId");
		frmIBMyReceipentsAddContactManually.lblOTP.text = kony.i18n.getLocalizedString("Receipent_Token");
		frmIBMyReceipentsAddContactManually.btnotp.text = kony.i18n.getLocalizedString("Receipent_SwitchtoOTP");
	    frmIBMyReceipentsAddContactManually.txtotp.setFocus(true);
	//	frmIBMyReceipentsAddContactManually.txtotp.placeholder = kony.i18n.getLocalizedString("enterToken");
		}else{
				frmIBMyReceipentsAddContactManually.otpmsglbl.text = kony.i18n.getLocalizedString("keyotpmsgreq");
				frmIBMyReceipentsAddContactManually.lblOTP.text = kony.i18n.getLocalizedString("Receipent_OTP");;
				frmIBMyReceipentsAddContactManually.btnotp.text = kony.i18n.getLocalizedString("keyRequest");
			//	frmIBMyReceipentsAddContactManually.txtotp.placeholder = kony.i18n.getLocalizedString("enterOTP");			
		}
	//frmIBMyReceipentsAddContactManually.btnRcCancel=kony.i18n.getLocalizedString("Receipent_Cancel");
	//frmIBMyReceipentsAddContactManually.btnrcCancel2=kony.i18n.getLocalizedString("Receipent_Cancel");
	
}
/**
 * Method to handle locale change for Rc Add Contact manually conf screen
 * @returns {}
 */

function handleLocaleChangeRcAddContactManuallyConfScreen() {
	invokeCommonIBLogger("Syncing locale......frmIBMyReceipentsAddContactManuallyConf");
	frmIBMyReceipentsAddContactManuallyConf.tbxSearch.placeholder = kony.i18n.getLocalizedString("keySearch");
	frmIBMyReceipentsAddContactManuallyConf.lblMyReceipents.text = kony.i18n.getLocalizedString("Receipent_MyReceipents");
	frmIBMyReceipentsAddContactManuallyConf.lblConfirmation.text = kony.i18n.getLocalizedString("Complete");
	frmIBMyReceipentsAddContactManuallyConf.lblmob.text = kony.i18n.getLocalizedString("Receipent_Mobileno");
	frmIBMyReceipentsAddContactManuallyConf.label10107168132681.text = kony.i18n.getLocalizedString("Receipent_Email");
	frmIBMyReceipentsAddContactManuallyConf.label10107168132696.text = kony.i18n.getLocalizedString("Receipent_FacebookID");
	frmIBMyReceipentsAddContactManuallyConf.label10107168132952.text = kony.i18n.getLocalizedString("Receipent_Account");
	frmIBMyReceipentsAddContactManuallyConf.btnRcBackToHome.text = kony.i18n.getLocalizedString("keyReturn");
	frmIBMyReceipentsAddContactManuallyConf.btnAddMoreRc.text = kony.i18n.getLocalizedString("keyAddMoreIB");
	
	var tempData = [];
	var textbtnTransfer = {
								text: kony.i18n.getLocalizedString("Transfer"),
								skin: "btnIB80px"
								};
	if(gblAdd_Receipent_State == gblEXISTING_RC_BANKACCNT_ADDITION)
	tempData.push(frmIBMyReceipentsAddBankAccnt.segementTobeAddedAccnts.data);
	else
	tempData.push(frmIBMyReceipentsAddContactManually.segmentRcAccounts.data);
	var totalData = [];
	if (tempData[0] != null) {
		for (var i = 0; i < tempData[0].length; i++) {
			var bankNameLocale = "NULL";
			  bankNameLocale = getBankNameCurrentLocale(tempData[0][i].bankCode);
			var segDataMap;
			if (tempData[0][i].lblAccountName != null && tempData[0][i].lblAccountName != "") {
				segDataMap = {
					lblBankTitle: tempData[0][i].lblBankTitle,
					lblNumberTitle: kony.i18n.getLocalizedString("Receipent_Number"),
					lblNickNameTitle: kony.i18n.getLocalizedString("Receipent_NickName"),
					btnTransfer: textbtnTransfer,
					lblBankName: bankNameLocale,
					lblNumber: tempData[0][i].lblNumber,
					lblNickName: tempData[0][i].lblNickName,
					lblAccountNameTitle: kony.i18n.getLocalizedString("AccountName"),
					lblAccountName: tempData[0][i].lblAccountName,
					bankLogo: tempData[0][i].bankLogo,
					bankCode:tempData[0][i].bankCode
				};
			} else {
				segDataMap = {
					lblBankTitle: tempData[0][i].lblBankTitle,
					lblNumberTitle: kony.i18n.getLocalizedString("Receipent_Number"),
					lblNickNameTitle: kony.i18n.getLocalizedString("Receipent_NickName"),
					btnTransfer: textbtnTransfer,
					lblBankName: bankNameLocale,
					lblNumber: tempData[0][i].lblNumber,
					lblNickName: tempData[0][i].lblNickName,
					bankLogo: tempData[0][i].bankLogo,
					bankCode:tempData[0][i].bankCode
				};
			}
			totalData.push(segDataMap);
			/*activityLogServiceCall("046", "", "01", "", "Added Recipient account", tempData[0][i].lblNickName, tempData[0][i].lblBankName,
				tempData[0][i].lblNumber, "", "");*/
		}
		frmIBMyReceipentsAddContactManuallyConf.segmentRcAccounts.setData(totalData);
	}
}
/**
 * Method to hande locale change for Rc Edit account conf screen
 * @returns {}
 */

function handleLocaleChangeRcEditAccountConfScreen() {
	invokeCommonIBLogger("Syncing locale......frmIBMyReceipentsEditAccountConf");
	frmIBMyReceipentsEditAccountConf.lblMyReceipents.text = kony.i18n.getLocalizedString("Receipent_MyReceipents");
	frmIBMyReceipentsEditAccountConf.lblAccntConfTitle.text = kony.i18n.getLocalizedString("keyMyRecipientAccount");
	frmIBMyReceipentsEditAccountConf.btnBackToHome.text = kony.i18n.getLocalizedString("Receipent_Back");
	frmIBMyReceipentsEditAccountConf.tbxSearch.placeholder = kony.i18n.getLocalizedString("keySearch");
}
/**
 * Method to handle the locale change for Rc Fb contact conf screen
 * @returns {}
 */

function handleLocaleChangeRcFbContactConfScreen() {
	invokeCommonIBLogger("Syncing locale......frmIBMyReceipentsFBContactConf");
	frmIBMyReceipentsFBContactConf.lblMyReceipents.text = kony.i18n.getLocalizedString("Receipent_MyReceipents");
	frmIBMyReceipentsFBContactConf.lblFBConfTitle.text = kony.i18n.getLocalizedString("Complete");
	frmIBMyReceipentsFBContactConf.tbxSearch.placeholder = kony.i18n.getLocalizedString("keySearch");
}
/**
 * Global method to sync the Rc locale
 * @returns {}
 */

function syncIBRcLocale() {
	var currentFormId = kony.application.getCurrentForm()
		.id;
	if (currentFormId == "frmIBMyReceipentsHome") {
		setDatatoReceipentsList("1");
		if (gblRC_QA_TEST_VAL == 1) {
			startDisplaySelectbankCacheService();
		}
	} else if (currentFormId == "frmIBMyReceipentsAccounts") {
		setDatatoReceipentsList("6");
		setDatatoReceipentBankList();
		if (gblRC_QA_TEST_VAL == 1) {
			startDisplaySelectbankCacheService();
		}
	} else if (currentFormId == "frmIBMyReceipentsAddBankAccnt") {
		setDatatoReceipentsList("2");
		if (gblRC_QA_TEST_VAL == 1) {
			startDisplaySelectbankCacheService();
		}
	} else if (currentFormId == "frmIBMyReceipentsAddContactFB") {
		setDatatoReceipentsList("3");
		if (gblRC_QA_TEST_VAL == 1) {
			startDisplaySelectbankCacheService();
		}
	} else if (currentFormId == "frmIBMyReceipentsAddContactManually") {
		setDatatoReceipentsList("4");
		if (gblRC_QA_TEST_VAL == 1) {
			startDisplaySelectbankCacheService();
		}
	} else if (currentFormId == "frmIBMyReceipentsAddContactManuallyConf") {
		setDatatoReceipentsList("8");
		if (gblRC_QA_TEST_VAL == 1) {
			startDisplaySelectbankCacheService();
		}
	} else if (currentFormId == "frmIBMyReceipentsEditAccountConf") {
		setDatatoReceipentsList("5");
		if (gblRC_QA_TEST_VAL == 1) {
			startDisplaySelectbankCacheService();
		}
	} else if (currentFormId == "frmIBMyReceipentsFBContactConf") {
		setDatatoReceipentsList("7");
		if (gblRC_QA_TEST_VAL == 1) {
			startDisplaySelectbankCacheService();
		}
	}
}

/**
 * Method to set data to the to receipents list for passed frm id
 * @returns {}
 */

function setDataToReceipentsListToForm(collectionData, form) {
	var tempBackUp = collectionData;
	var totalData = [];
	invokeCommonIBLogger("#####Number of receipents#####" + tempBackUp.length);
	invokeCommonIBLogger("#####setDataToReceipentsListToForm collection data#####" + collectionData);
	for (var i = 0; i < tempBackUp.length; i++) {
		var isMobilePresent;
		var isFbPresent;
		var isEmailPresent;
		var accntTitle;
		var totalAccnt = collectionData[i]["totalCount"];
		var isFavourite = collectionData[i]["favoriteFlag"];
		var btnFavSkin;
		if (isFavourite != null) {
			invokeCommonIBLogger("#############setDataToReceipentsListToForm FavoriteFlag##########" + collectionData[i][
				"favoriteFlag"]);
			if (isFavourite.toString() == "y" || isFavourite.toString() == "Y") {
				btnFavSkin = {
					text: "",
					skin: "btnIBStarIconFocus"
				};
			} else {
				btnFavSkin = {
					text: "",
					skin: "btnIBStarIcon"
				};
			}
		} else {
			btnFavSkin = {
				text: "",
				skin: "btnIBStarIcon"
			};
		}
		if ((collectionData[i]["personalizedMobileNumber"] != null && collectionData[i]["personalizedMobileNumber"].trim()!="" && collectionData[i]["personalizedMobileNumber"] !="undefined" && collectionData[i]["personalizedMobileNumber"] !="Not Available" && collectionData[i]["personalizedMobileNumber"] !="No Number") && (collectionData[i]["personalizedMobileNumber"].length ==
			12 || collectionData[i]["personalizedMobileNumber"].length == 10)) {
			isMobilePresent = "phoneicon.png";
		} else {
			isMobilePresent = "";
		}
		if (collectionData[i]["personalizedFacebookId"] != null && collectionData[i]["personalizedFacebookId"].trim()!="" && collectionData[i]["personalizedFacebookId"]!="undefined" && collectionData[i]["personalizedFacebookId"].length > 0 && collectionData[i]["personalizedFacebookId"] !="Not Available") {
			isFbPresent = "facebookicon.png";
		} else {
			isFbPresent = "";
		}
		if (collectionData[i]["personalizedMailId"] != null && collectionData[i]["personalizedMailId"].trim()!="" && collectionData[i]["personalizedMailId"]!="undefined" && collectionData[i]["personalizedMailId"].length > 0 && collectionData[i]["personalizedMailId"] != "Not Available" && collectionData[i]["personalizedMailId"] != "No Mail") {
			isEmailPresent = "emailicon.png";
		} else {
			isEmailPresent = "";
		}
		if (totalAccnt != null) {
			if (parseInt(totalAccnt.toString()) > 1) {
				accntTitle = kony.i18n.getLocalizedString("Receipent_Accounts");
			} else {
				accntTitle = kony.i18n.getLocalizedString("Receipent_Account");
			}
		} else {
			accntTitle = kony.i18n.getLocalizedString("Receipent_Account");
		}
		var rcPicture;
		 var randomnum = Math.floor((Math.random()*10000)+1); 
		if (collectionData[i]["personalizedPictureId"] == "" || collectionData[i]["personalizedPictureId"] == null) {
			rcPicture = RC_LOGO_URL + collectionData[i]["personalizedId"]+"&rr="+randomnum;
		} else {
			if (kony.string.endsWith(collectionData[i]["personalizedPictureId"], "nouserimg.jpg")) {
				rcPicture = RC_LOGO_URL + collectionData[i]["personalizedId"]+"&rr="+randomnum;
			} else {
				if (collectionData[i]["personalizedPictureId"].indexOf("fbcdn") >= 0 ) {
					rcPicture = collectionData[i]["personalizedPictureId"];
				} else {
					rcPicture = RC_LOGO_URL + collectionData[i]["personalizedId"]+"&rr="+randomnum;
					//rcPicture = "nouserimg.jpg";
				}
			}
		}
		var recName = "";
		if(isNotBlank(collectionData[i]["personalizedName"])){
			recName = collectionData[i]["personalizedName"];
		}
		
		var segDataMap = {
			imgReceipentPic: rcPicture,
			lblReceipentName: recName,
			lblNumberofAccounts: collectionData[i]["totalCount"] + " " + accntTitle.toString(),
			imgMobile: isMobilePresent,
			imgMail: isEmailPresent,
			imgFb: isFbPresent,
			imgNxt: "bg_arrow_right.png",
			receipentID: collectionData[i]["personalizedId"],
			mobileNumber: collectionData[i]["personalizedMobileNumber"],
			emailId: collectionData[i]["personalizedMailId"],
			facebookId: collectionData[i]["personalizedFacebookId"],
			Favorite: isFavourite,
			btnfav: btnFavSkin
		};
		totalData.push(segDataMap);
	}
	switch (parseInt(form.toString())) {
	case 1:
		frmIBMyReceipentsHome.segmentReceipentListing.setData(totalData);
		if(navigator.userAgent.match(/iPad/i)){
				frmIBMyReceipentsHome.lblMyReceipents.setFocus(true);
			}
			else{
		frmIBMyReceipentsHome.tbxSearch.setFocus(true);
			}
		break;
	case 2:
		frmIBMyReceipentsAddBankAccnt.segmentReceipentListing.setData(totalData);
		break;
	case 3:
		frmIBMyReceipentsAddContactFB.segmentReceipentListing.setData(totalData);
		break;
	case 4:
		frmIBMyReceipentsAddContactManually.segmentReceipentListing.setData(totalData);
		break;
	case 5:
		frmIBMyReceipentsEditAccountConf.segmentReceipentListing.setData(totalData);
		break;
	case 6:
		frmIBMyReceipentsAccounts.segmentReceipentListing.setData(totalData);
		break;
	case 7:
		frmIBMyReceipentsFBContactConf.segmentReceipentListing.setData(totalData);
		break;
	case 8:
		frmIBMyReceipentsAddContactManuallyConf.segmentReceipentListing.setData(totalData);
		break;
	default:
		break;
	}
}
/**
 * Method to delete the selected receipent
 * @returns {}
 */

function deleteReceipent(removeIndexinForm) {
	var tobedeletedIndex;
	var numAccnt;
	var name = gblselectedRcId;
	invokeCommonIBLogger("#####Name......" + name);
	var noOfAccounts = frmIBMyReceipentsAccounts.segmentRcAccounts.data.length;
	invokeCommonIBLogger("####Number of Accounts for " + name + "is " + noOfAccounts);
	invokeCommonIBLogger("#####Number of receipents......" + globalRcData[0].length);
	for (var i = 0; i < globalRcData[0].length; i++) {
		var tempNum = globalRcData[0][i]["lblNumberofAccounts"];
		numAccnt = "";
		for (var j = 0; j < tempNum.length; j++) {
			if (tempNum[j] == " ") {
				break;
			} else {
				if (numAccnt == null || numAccnt.length == 0) {
					numAccnt = tempNum[j];
				} else {
					numAccnt = numAccnt + tempNum[j];
				}
			}
		}
		invokeCommonIBLogger("#####Number of Accounts integer" + numAccnt);
		invokeCommonIBLogger("#####Number of Accounts" + globalRcData[0][i]["lblNumberofAccounts"]);
		invokeCommonIBLogger("#####Name[ID]......" + globalRcData[0][i]["receipentID"]);
		if (name.toString() == globalRcData[0][i]["receipentID"] && noOfAccounts == numAccnt) {
			tobedeletedIndex = i;
			break;
		}
	}
	invokeCommonIBLogger("#####Deleted index" + tobedeletedIndex);
	if (tobedeletedIndex == null || tobedeletedIndex.toString() == "" || tobedeletedIndex.toString()
		.length == 0)
		alert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"));
	else {
		switch (parseInt(removeIndexinForm.toString())) {
		case 1:
			frmIBMyReceipentsHome.segmentReceipentListing.removeAt(tobedeletedIndex);
			break;
		case 2:
			frmIBMyReceipentsAddBankAccnt.segmentReceipentListing.removeAt(tobedeletedIndex);
			break;
		case 3:
			frmIBMyReceipentsAddContactFB.segmentReceipentListing.removeAt(tobedeletedIndex);
			break;
		case 4:
			frmIBMyReceipentsAddContactManually.segmentReceipentListing.removeAt(tobedeletedIndex);
			break;
		case 5:
			frmIBMyReceipentsEditAccountConf.segmentReceipentListing.removeAt(tobedeletedIndex);
			break;
		case 6:
			frmIBMyReceipentsAccounts.segmentReceipentListing.removeAt(tobedeletedIndex);
			break;
		case 7:
			frmIBMyReceipentsFBContactConf.segmentReceipentListing.removeAt(tobedeletedIndex);
			break;
		case 8:
			frmIBMyReceipentsAddContactManuallyConf.segmentReceipentListing.removeAt(tobedeletedIndex);
			break;
		default:
			break;
		}
	}
	//Update the cached list
	cacheRcList(removeIndexinForm);
	globalSeletedRcIndex = -1;
}
/**
 * Method to check if receipent already exists in Crm
 * @returns {}
 */

function checkForRcExistenceinCrm() {
	if (gblAdd_Receipent_State == gblNEW_RC_ADDITION || gblAdd_Receipent_State == gblNEW_RC_ADDITION_PRECONFIRM) {
		if (globalRcData[0] != null) {
			for (var i = 0; i < globalRcData[0].length; i++) {
				var srcRcName = globalRcData[0][i].lblReceipentName;
				var targetRcName = frmIBMyReceipentsAddContactManually.lblrecipientname.text;
				invokeCommonIBLogger("SRC RC NAME: " + srcRcName.toString());
				invokeCommonIBLogger("TARGET RC NAME: " + targetRcName.toString());
				check = kony.string.equalsIgnoreCase(srcRcName.toString()
					.trim(), targetRcName.toString()
					.trim());
				if (check) {
					break;
				}
			}
			if (check) {
				alert("" + kony.i18n.getLocalizedString("Receipent_alert_differentRcname"));
				return true;
			} else {
				//New receipent..carry on
				return false;
			}
		} else {
			return false;
		}
	} else if (gblAdd_Receipent_State == gblEXISTING_RC_EDITION) {
		//Code to be added
		return false;
	}
}
/**
 * Method to invalidate the rc listing and sort by Favorite
 * @returns {}
 */

function invalidateRcListingSortbyFav(form) {
	startReceipentListingService(gblcrmId, form);
}
/**
 * Method to check the recipient name existence in CRM apart from its own name
 * @param {} recipientName
 * @returns {}
 */

function checkForOtherRcExistenceinCrm(recipientName) {
    cacheRcList(4);
    if (gblAdd_Receipent_State == gblEXISTING_RC_EDITION || gblAdd_Receipent_State == gblNEW_RC_ADDITION) {
        invokeCommonIBLogger("globalSeletedRcIndex: " + globalSeletedRcIndex);
        if (globalRcData[0] != null) {
      		check = false;
            for (var i = 0; i < globalRcData[0].length; i++) {
                if (parseInt(i.toString()) != parseInt(globalSeletedRcIndex.toString())) {
                    var srcRcName = globalRcData[0][i].lblReceipentName;
    				var recepntId = globalRcData[0][i].receipentID;
                    var targetRcName = recipientName;
                    invokeCommonIBLogger("SRC RC NAME: " + srcRcName.toString());
                    invokeCommonIBLogger("TARGET RC NAME: " + targetRcName.toString());
                    check = kony.string.equals(srcRcName.toString().trim(), targetRcName.toString().trim());
				    if (check && gblselectedRcId != recepntId) {
				      	break;
                    }
                }
            }

		    if (check && gblselectedRcId != recepntId) {
                alert("" + kony.i18n.getLocalizedString("RecipientExists"));
                return true;
            } else {
                //New receipent..carry on
                return false;
            }
        } else {
            return false;
        }
    } else if (gblAdd_Receipent_State == gblEXISTING_RC_EDITION) {
        //Code to be added
        return false;
    }
}

function uploadrecipinetpictimerCallbackfail(){
	dismissLoadingScreenPopup();
    popupDisplayPicOptionsRecipients.dismiss();
}


function uploadRecipienttimerCallback(){
   var randomnum = Math.floor((Math.random()*10000)+1); 
	dismissLoadingScreenPopup();
 	var tempurl="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/"+ appConfig.middlewareContext + "/" + "ImageRender?crmId=Y&personalizedId=temp&billerId="+"&fromtemp=Y&dummy="+randomnum;
	frmIBMyReceipentsAddContactManually.image210107168135.src=tempurl;
	if(gblAdd_Receipent_State == gblEXISTING_RC_EDITION){
		frmIBMyReceipentsAddContactManually.image210107168135.src=null;
	}
	document.getElementById("frmIBMyReceipentsAddContactManually_image210107168135").src=tempurl;
	popupDisplayPicOptionsRecipients.dismiss();
}



//Function for SPA Image Upload
function uploadEditMyProfileCallbackSpa() 
{
    dismissLoadingScreen();
    var randomnum = Math.floor((Math.random()*10000)+1); 
    var tempurl = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=Y&personalizedId=&billerId=" + "&fromtemp=Y&dummy=" + randomnum;
   //frmeditMyProfile.imgprofpic.src = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=" + gblcrmId + "&personalizedId=temp&billerId=" + "&fromtemp=Y";
    //frmeditMyProfile.imgprofpic.src="";
    frmeditMyProfile.imgprofpic.src = tempurl;
    popUploadPicSpa.dismiss();
}

//Function for SPA Image Upload
function uploadRecipienttimerCallbackSpa()
{
	var randomnum = Math.floor((Math.random()*10000)+1);
	var tempurl="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/"+ appConfig.middlewareContext + "/" + "ImageRender?crmId=Y&personalizedId=temp&billerId="+"&fromtemp=Y&dummy="+randomnum;
	dismissLoadingScreen();
	var curForm=kony.application.getCurrentForm().id;
	if(curForm=="frmMyRecipientEditProfile")
	{
		frmMyRecipientEditProfile.imgprofilepic.src=tempurl;
	}
	else if(curForm=="frmMyRecipientAddProfile")
	{
		frmMyRecipientAddProfile.imgProfilePic.src=tempurl;
	}
	else
	{
		curForm.imgProfilePic.src="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/"+ appConfig.middlewareContext + "/" + "ImageRender?crmId=Y&personalizedId=temp&billerId="+"&fromtemp=Y";
	}
	popUploadPicSpa.dismiss();

}

function uploadRecipienttimerCallbackFailSpa()
{
	dismissLoadingScreen();
	popUploadPicSpa.dismiss();

}

function uploadEditMyProfileCallbackFailSpa()
{
	dismissLoadingScreen();
	frmeditMyProfile.imgprofpic.src=frmMyProfile.image247502979411375.src;	
	popUploadPicSpa.dismiss();
}



/**
 * Method to destroy the to be saved bank data
 * @returns {}
 */

 function getAccountDetailMessageRc(segData) {
    var accDetailMsg = "";
    var locale = kony.i18n.getCurrentLocale();
    var tem = segData //frmIBMyReceipentsAddBankAccnt.segementTobeAddedAccnts.data;
    var count1 = 0;
    var count2 = 0;
    var oCount = [];
    var acctTemp = []; //CR13
    var tmpAcctId = "";
    var obn = "";
    var accString = kony.i18n.getLocalizedString("Receipent_Account");
    
    
    for (var f = 0; f < globalSelectBankData.length; f++) {
        oCount.push(0);
        acctTemp.push(""); //CR13
    }
    for (var i = 0; i < tem.length; i++) {
        var bnName = tem[i]["lblBankName"];
        
        count2++;
        
        for (var g = 0; g < globalSelectBankData.length; g++) {
            
            if (bnName == globalSelectBankData[g][1]) {
                oCount[g] = kony.os.toNumber(oCount[g]);
                
                obn = globalSelectBankData[g][2]
                oCount[g]++;
                
                tmpAcctId = removeHyphenIB(tem[i]["lblNumber"]);
                tmpAcctId = "x" + tmpAcctId.substring(tmpAcctId.length-4);
                acctTemp[g] = acctTemp[g] + "," + tmpAcctId;
            }
        }
    }
    var tcount = count2;
    var accDetailMsgt = ""
    var naccDetailMsgt = ""
    if (tcount > 0) {
        for (var h = 0; h < oCount.length; h++) {
            if (oCount[h] > 1) {
                
                accDetailMsgt = accDetailMsgt + oCount[h] + ":" + globalSelectBankData[h][2] + ", "

            } else if (oCount[h] == 1) {
                accDetailMsgt = accDetailMsgt + globalSelectBankData[h][2] + ", ";

            }
            if (oCount[h] == 1 || oCount[h] > 1) {
                var str = acctTemp[h];
                naccDetailMsgt = naccDetailMsgt + globalSelectBankData[h][2] + "(" + str.substring(1) + ")";
                
            }
        }
    }
    if (accDetailMsgt.length > 1) {
        accDetailMsgt = accDetailMsgt.substring(0, accDetailMsgt.length - 2);
    }
    if (tcount > 0) {
        accDetailMsg = tcount + " " + accString + " (" + accDetailMsgt + ").";
    }
    return naccDetailMsgt;
}

function destroySavedBankData() {
	if (globalTobeAddedBankData != null) {
		for (var i = 0; i < globalTobeAddedBankData.length; i++) {
			globalTobeAddedBankData.pop();
		}
		globalTobeAddedBankData = [];
	}
	if (globalTobeAddedBankDataConfirmation != null) {
		for (var i = 0; i < globalTobeAddedBankDataConfirmation.length; i++) {
			globalTobeAddedBankDataConfirmation.pop();
		}
		globalTobeAddedBankDataConfirmation = [];
	}
}
/**
 * Method to set the to be saved bank data to segment
 * @returns {}
 */

function setToBeSaveBankDatatoSeg(accountName) {
	if (gblAdd_Receipent_State == gblEXISTING_RC_EDITION) {
		frmIBMyReceipentsAddBankAccnt.lblRcName.text = frmIBMyReceipentsAddContactManually.txtRecipientName.text;
		frmIBMyReceipentsAddBankAccnt.lblAddRcConfMobileNo.text = frmIBMyReceipentsAddContactManually.txtmobnum.text;
		frmIBMyReceipentsAddBankAccnt.lblAddRcConfEmail.text = frmIBMyReceipentsAddContactManually.txtemail.text;
		frmIBMyReceipentsAddBankAccnt.lblAddRcConfFbId.text = frmIBMyReceipentsAccounts.lblRcFbId.text;
		frmIBMyReceipentsAddBankAccnt.btnRcConfAddAccount.setVisibility(true);
	} else if (gblAdd_Receipent_State == gblEXISTING_RC_BANKACCNT_EDITION) {
		frmIBMyReceipentsAddBankAccnt.lblRcName.text = frmIBMyReceipentsAccounts.lblRcName.text;
		frmIBMyReceipentsAddBankAccnt.lblAddRcConfMobileNo.text = frmIBMyReceipentsAccounts.lblRcMobileNo.text;
		frmIBMyReceipentsAddBankAccnt.lblAddRcConfEmail.text = frmIBMyReceipentsAccounts.lblRcEmail.text;
		frmIBMyReceipentsAddBankAccnt.lblAddRcConfFbId.text = frmIBMyReceipentsAccounts.lblRcFbId.text;
		frmIBMyReceipentsAddBankAccnt.imgReceipentProfileRcConf.src = frmIBMyReceipentsAccounts.imgReceipentProfile.src;
		frmIBMyReceipentsAddBankAccnt.btnRcConfAddAccount.setVisibility(false);
	} else if (gblAdd_Receipent_State == gblEXISTING_RC_BANKACCNT_ADDITION) {
		frmIBMyReceipentsAddBankAccnt.lblRcName.text = frmIBMyReceipentsAccounts.lblRcName.text;
		frmIBMyReceipentsAddBankAccnt.lblAddRcConfMobileNo.text = frmIBMyReceipentsAccounts.lblRcMobileNo.text;
		frmIBMyReceipentsAddBankAccnt.lblAddRcConfEmail.text = frmIBMyReceipentsAccounts.lblRcEmail.text;
		frmIBMyReceipentsAddBankAccnt.lblAddRcConfFbId.text = frmIBMyReceipentsAccounts.lblRcFbId.text;
		frmIBMyReceipentsAddBankAccnt.imgReceipentProfileRcConf.src = frmIBMyReceipentsAccounts.imgReceipentProfile.src;
		frmIBMyReceipentsAddBankAccnt.btnRcConfAddAccount.setVisibility(true);
	} else {
		frmIBMyReceipentsAddBankAccnt.lblRcName.text = frmIBMyReceipentsAddContactManually.txtRecipientName.text;
		frmIBMyReceipentsAddBankAccnt.lblAddRcConfMobileNo.text = frmIBMyReceipentsAddContactManually.txtmobnum.text;
		frmIBMyReceipentsAddBankAccnt.lblAddRcConfEmail.text = frmIBMyReceipentsAddContactManually.txtemail.text;
		frmIBMyReceipentsAddBankAccnt.lblAddRcConfFbId.text = "";
		frmIBMyReceipentsAddBankAccnt.btnRcConfAddAccount.setVisibility(true);
	}
	var tempBackUp = [];
	var totalData = [];
	for (var i = 0; i < globalTobeAddedBankData.length; i++) {
		//Retrieve the data and push it back
		var tempDataforSeg = globalTobeAddedBankData.pop();
		tempBackUp.push(tempDataforSeg);
		var input;
		if (gblAdd_Receipent_State != gblEXISTING_RC_BANKACCNT_EDITION) {
			input = {
				skin: "btnIBdelicon"
			};
		}
		var segDataMap;
		if (false) {
			invokeCommonIBLogger("ITS A NON TMB BANK ACCOUNT THAT IS ADDED " + accountName);
			segDataMap = {
				lblBankTitle: kony.i18n.getLocalizedString("Receipent_Bank"),
				lblNumberTitle: kony.i18n.getLocalizedString("Receipent_Number"),
				lblNickNameTitle: kony.i18n.getLocalizedString("Receipent_NickName"),
				lblBankName: tempDataforSeg[0],
				lblNumber: tempDataforSeg[1],
				lblNickName: tempDataforSeg[2],
				bankCode: tempDataforSeg[3],
				btndelete1: input,
				bankLogo: "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+tempDataforSeg[3]+"&modIdentifier=BANKICON"
			};
		} else {
			invokeCommonIBLogger("ITS A TMB BANK ACCOUNT THAT IS ADDED " + accountName);
			if (accountName == null) {
				segDataMap = {
					lblBankTitle: kony.i18n.getLocalizedString("Receipent_Bank"),
					lblNumberTitle: kony.i18n.getLocalizedString("Receipent_Number"),
					lblNickNameTitle: kony.i18n.getLocalizedString("Receipent_NickName"),
					lblBankName: tempDataforSeg[0],
					lblNumber: tempDataforSeg[1],
					lblNickName: tempDataforSeg[2],
					bankCode: tempDataforSeg[3],
					btndelete1: input,
					bankLogo: "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+tempDataforSeg[3]+"&modIdentifier=BANKICON"
				};
			} else {
				segDataMap = {
					lblBankTitle: kony.i18n.getLocalizedString("Receipent_Bank"),
					lblNumberTitle: kony.i18n.getLocalizedString("Receipent_Number"),
					lblNickNameTitle: kony.i18n.getLocalizedString("Receipent_NickName"),
					lblBankName: tempDataforSeg[0],
					lblNumber: tempDataforSeg[1],
					lblNickName: tempDataforSeg[2],
					bankCode: tempDataforSeg[3],
					btndelete1: input,
					lblAccountNameTitle: kony.i18n.getLocalizedString("AccountName"),
					lblAccountName: accountName,
					bankLogo: "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+tempDataforSeg[3]+"&modIdentifier=BANKICON"
				};
			}
		}
		totalData.push(segDataMap);
	}
	frmIBMyReceipentsAddBankAccnt.segementTobeAddedAccnts.addAll(totalData);
	//Restore the back up data to global variable
	globalTobeAddedBankData = tempBackUp;
	if (frmIBMyReceipentsAddBankAccnt.segementTobeAddedAccnts.data.length > 1) {
		frmIBMyReceipentsAddBankAccnt.lblAddRcConfAccountTitle.text = kony.i18n.getLocalizedString("Receipent_Accounts");
	} else {
		frmIBMyReceipentsAddBankAccnt.lblAddRcConfAccountTitle.text = kony.i18n.getLocalizedString("Receipent_Account");
	}
}


/**
 * Method to set the to be saved bank data to segment
 * @returns {}
 */

function setToBeSaveBankDatatoExistingRCConfirmationSeg() {
	frmIBMyReceipentsAddContactManuallyConf.lblrcname.text = frmIBMyReceipentsAddBankAccnt.lblRcName.text;
	frmIBMyReceipentsAddContactManuallyConf.lblmobnum.text = frmIBMyReceipentsAddBankAccnt.lblAddRcConfMobileNo.text;
	frmIBMyReceipentsAddContactManuallyConf.lblfbid.text = frmIBMyReceipentsAddBankAccnt.lblAddRcConfFbId.text;
	frmIBMyReceipentsAddContactManuallyConf.lblemail.text = frmIBMyReceipentsAddBankAccnt.lblAddRcConfEmail.text;
	frmIBMyReceipentsAddContactManuallyConf.image2101071681311897.src = frmIBMyReceipentsAddBankAccnt.imgReceipentProfileRcConf
		.src;
	var tempData = [];
	tempData.push(frmIBMyReceipentsAddBankAccnt.segementTobeAddedAccnts.data);
	var totalData = [];
	var textbtnTransfer = {
								text: kony.i18n.getLocalizedString("Transfer"),
								skin: "btnIB80px"
								};
	if (tempData[0] != null) {
		for (var i = 0; i < tempData[0].length; i++) {
			var segDataMap;
			if (tempData[0][i].lblAccountName != null && tempData[0][i].lblAccountName != "") {
				segDataMap = {
					lblBankTitle: tempData[0][i].lblBankTitle,
					lblNumberTitle: tempData[0][i].lblNumberTitle,
					lblNickNameTitle: tempData[0][i].lblNickNameTitle,
					btnTransfer: textbtnTransfer,
					lblBankName: tempData[0][i].lblBankName,
					lblNumber: tempData[0][i].lblNumber,
					lblNickName: tempData[0][i].lblNickName,
					lblAccountNameTitle: kony.i18n.getLocalizedString("AccountName"),
					lblAccountName: tempData[0][i].lblAccountName,
					bankLogo: tempData[0][i].bankLogo,
					bankCode:tempData[0][i].bankCode
				};
			} else {
				segDataMap = {
					lblBankTitle: tempData[0][i].lblBankTitle,
					lblNumberTitle: tempData[0][i].lblNumberTitle,
					lblNickNameTitle: tempData[0][i].lblNickNameTitle,
					btnTransfer: textbtnTransfer,
					lblBankName: tempData[0][i].lblBankName,
					lblNumber: tempData[0][i].lblNumber,
					lblNickName: tempData[0][i].lblNickName,
					bankLogo: tempData[0][i].bankLogo,
					bankCode:tempData[0][i].bankCode
				};
			}
			totalData.push(segDataMap);
			/*activityLogServiceCall("046", "", "01", "", "Added Recipient account", tempData[0][i].lblNickName, tempData[0][i].lblBankName,
				tempData[0][i].lblNumber, "", "");*/
		}
		frmIBMyReceipentsAddContactManuallyConf.segmentRcAccounts.setData(totalData);
		if (frmIBMyReceipentsAddContactManuallyConf.segmentRcAccounts.data.length > 1) {
			frmIBMyReceipentsAddContactManuallyConf.label10107168132952.text = kony.i18n.getLocalizedString("Receipent_Accounts");
		} else {
			frmIBMyReceipentsAddContactManuallyConf.label10107168132952.text = kony.i18n.getLocalizedString("Receipent_Account");
		}
	}
}
/**
 * Method to set the to be saved bank data to segment
 * @returns {}
 */

//function setToBeSaveBankDatatoNewUserConfirmationSeg() {
//	var tempData = [];
//	tempData.push(frmIBMyReceipentsAddBankAccnt.segementTobeAddedAccnts.data);
//	var totalData = [];
//	if (tempData[0] != null) {
//		for (var i = 0; i < tempData[0].length; i++) {
//			var segDataMap = {
//				lblBankTitle: tempData[0][i].lblBankTitle,
//				lblNumberTitle: tempData[0][i].lblNumberTitle,
//				lblNickNameTitle: tempData[0][i].lblNickNameTitle,
//				lblBankName: tempData[0][i].lblBankName,
//				lblNumber: tempData[0][i].lblNumber,
//				lblNickName: tempData[0][i].lblNickName,
//				bankLogo: tempData[0][i].bankLogo
//			};
//			totalData.push(segDataMap);
//		}
//		frmIBMyReceipentsAddContactManuallyConf.segmentRcAccounts.setData(totalData);
//	}
//}
/**
 * Method to set the to be saved bank data to segment
 * @returns {}
 */

function setToBeSaveBankDatatoNewUserPreConfirmationSeg(accountName) {
	frmIBMyReceipentsAddContactManually.image2101071681311897.src = frmIBMyReceipentsAddContactManually.image210107168135.src;
	frmIBMyReceipentsAddContactManually.lblrecipientname.text = frmIBMyReceipentsAddContactManually.txtRecipientName.text;
	frmIBMyReceipentsAddContactManually.lblmobnum.text = frmIBMyReceipentsAddContactManually.txtmobnum.text;
	frmIBMyReceipentsAddContactManually.lblemail.text = frmIBMyReceipentsAddContactManually.txtemail.text;
	invokeCommonIBLogger("##################" + frmIBMyReceipentsAddContactManually.lblrecipientname.text);
	invokeCommonIBLogger("##################" + frmIBMyReceipentsAddContactManually.lblmob.text);
	invokeCommonIBLogger("##################" + frmIBMyReceipentsAddContactManually.lblemail.text);
	var totalData = [];
	var input = {
		skin: "btnIBdelicon"
	};
	var segDataMap;
	if (false) {
		invokeCommonIBLogger("ITS A NON TMB BANK ACCOUNT THAT IS ADDED " + accountName);
		segDataMap = {
			lblBankTitle: kony.i18n.getLocalizedString("Receipent_Bank"),
			lblNumberTitle: kony.i18n.getLocalizedString("Receipent_Number"),
			lblNickNameTitle: kony.i18n.getLocalizedString("Receipent_NickName"),
			lblBankName: frmIBMyReceipentsAddBankAccnt.comboBankName.selectedKeyValue[1],
			bankCode: frmIBMyReceipentsAddBankAccnt.comboBankName.selectedKey,
			lblNumber: frmIBMyReceipentsAddBankAccnt.tbxAddAccntNumber.text,
			lblNickName: frmIBMyReceipentsAddBankAccnt.tbxAddAccntNickName.text,
			btndelete1: input,
			bankLogo: "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+frmIBMyReceipentsAddBankAccnt.comboBankName.selectedKey+"&modIdentifier=BANKICON"
		};
	} else {
		invokeCommonIBLogger("ITS A TMB BANK ACCOUNT THAT IS ADDED " + accountName);
		if (accountName == null) {
			segDataMap = {
				lblBankTitle: kony.i18n.getLocalizedString("Receipent_Bank"),
				lblNumberTitle: kony.i18n.getLocalizedString("Receipent_Number"),
				lblNickNameTitle: kony.i18n.getLocalizedString("Receipent_NickName"),
				lblBankName: frmIBMyReceipentsAddBankAccnt.comboBankName.selectedKeyValue[1],
				bankCode: frmIBMyReceipentsAddBankAccnt.comboBankName.selectedKey,
				lblNumber: frmIBMyReceipentsAddBankAccnt.tbxAddAccntNumber.text,
				lblNickName: frmIBMyReceipentsAddBankAccnt.tbxAddAccntNickName.text,
				btndelete1: input,
				bankLogo: "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+frmIBMyReceipentsAddBankAccnt.comboBankName.selectedKey+"&modIdentifier=BANKICON"
			};
		} else {
			segDataMap = {
				lblBankTitle: kony.i18n.getLocalizedString("Receipent_Bank"),
				lblNumberTitle: kony.i18n.getLocalizedString("Receipent_Number"),
				lblNickNameTitle: kony.i18n.getLocalizedString("Receipent_NickName"),
				lblBankName: frmIBMyReceipentsAddBankAccnt.comboBankName.selectedKeyValue[1],
				bankCode: frmIBMyReceipentsAddBankAccnt.comboBankName.selectedKey,
				lblNumber: frmIBMyReceipentsAddBankAccnt.tbxAddAccntNumber.text,
				lblNickName: frmIBMyReceipentsAddBankAccnt.tbxAddAccntNickName.text,
				btndelete1: input,
				lblAccountNameTitle: kony.i18n.getLocalizedString("AccountName"),
				lblAccountName: accountName,
				bankLogo: "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+frmIBMyReceipentsAddBankAccnt.comboBankName.selectedKey+"&modIdentifier=BANKICON"
			};
		}
	}
	totalData.push(segDataMap);
	frmIBMyReceipentsAddContactManually.segmentRcAccounts.addAll(totalData);
	invokeCommonIBLogger("##################" + frmIBMyReceipentsAddBankAccnt.comboBankName.selectedKeyValue[1]);
	invokeCommonIBLogger("##################" + frmIBMyReceipentsAddBankAccnt.tbxAddAccntNumber.text);
	invokeCommonIBLogger("##################" + frmIBMyReceipentsAddBankAccnt.tbxAddAccntNickName.text);
	if (frmIBMyReceipentsAddContactManually.segmentRcAccounts.data.length > 1) {
		frmIBMyReceipentsAddContactManually.label10107168132952.text = kony.i18n.getLocalizedString("Receipent_Accounts");
	} else {
		frmIBMyReceipentsAddContactManually.label10107168132952.text = kony.i18n.getLocalizedString("Receipent_Account");
	}
}
/**
 * Method to destroy all the forms
 * @returns {}
 */

function destroyAllForms() {
	try {
		kony.timer.cancel("100");
	} catch (e) {
		// todo: handle exception
	}
	invokeCommonIBLogger("FIRE TIMER ....." + "mytimer");
	kony.timer.schedule("100", destroyFrmCallback, 1, false);
}
/**
 * Callback method for destroying the forms
 * @returns {}
 */

function destroyFrmCallback() {
	invokeCommonIBLogger("TIMER CALLBACK.....");
	try {
		TMBUtil.DestroyForm(frmIBMyReceipentsAddBankAccnt);
		TMBUtil.DestroyForm(frmIBMyReceipentsAddContactFB);
		TMBUtil.DestroyForm(frmIBMyReceipentsAddContactManually);
		//TMBUtil.DestroyForm(frmIBMyReceipentsHome);
		TMBUtil.DestroyForm(frmIBMyReceipentsEditAccountConf);
		TMBUtil.DestroyForm(frmIBMyReceipentsAddContactManuallyConf);
		TMBUtil.DestroyForm(frmIBMyReceipentsFBContactConf);
	} catch (e) {
		// todo: handle exception
		invokeCommonIBLogger("EXCEPTION...may be other forms not in stack");
	}
}
/**
 * Method return true if OTP is locked and false if unlocked
 * @returns {}
 */

//function isOTPLocked() {}
/**
 * Method used to get the count of number of receipents a user has
 * @returns {}
 */

function getReceipentCount() {
	curr_form = kony.application.getCurrentForm();
	var tempData = [];
	tempData.push(curr_form.segmentReceipentListing.data);
	var count = tempData[0].length;
	invokeCommonIBLogger("Number of recipients***************" + count);
	return count;
}
/**
 * Method to get the number of accounts that are already added for the receipent
 * @returns {}
 */

//function getNumberofAccountsforReceipent() {
//	var tempData = [];
//	tempData.push(frmIBMyReceipentsAccounts.segmentRcAccounts.data);
//	var count = tempData[0].length;
//	return count;
//}
/**
 * Method to implement search functionality...1 as parameter means search in RC list...2 as parameter means search in FB friends list
 * form 1 is frmIBMyReceipentsHome
 * form 2 is frmIBMyReceipentsAddBankAccnt
 * form 3 is frmIBMyReceipentsAddContactFB
 * form 4 is frmIBMyReceipentsAddContactManually
 * form 5 is frmIBMyReceipentsEditAccountConf
 * form 6 is frmIBMyReceipentsAccounts
 * form 7 is frmIBMyReceipentsFBContactConf
 * form 8 is frmIBMyReceipentsAddContactManuallyConf
 * @param {} type
 * @returns {}
 */

function searchFilter(type, form) {
	var currentFormId = kony.application.getCurrentForm();
	if (type == 1) {
		if (globalRcData[0] == null) {
			currentFormId.lblNoRecipients.setVisibility(true);
			currentFormId.lblNoRecipients.text = kony.i18n.getLocalizedString("keybillernotfound");
			return;
		} else {
			currentFormId.lblNoRecipients.setVisibility(false);
		}
	} else {
		if (globalFBData[0] == null) {
			currentFormId.lblFbNoFriends.setVisibility(true);
			currentFormId.lblFbNoFriends.text = kony.i18n.getLocalizedString("Receipent_alert_NoFacebookFriends");
			return;
		} else {
			currentFormId.lblFbNoFriends.setVisibility(false);
		}
	}
	var searchtext;
	if (form == 1) {
		searchtext = frmIBMyReceipentsHome.tbxSearch.text;
	} else if (form == 2) {
		searchtext = frmIBMyReceipentsAddBankAccnt.tbxSearch.text;
	} else if (form == 3) {
		if (type == 2) {
			invokeCommonIBLogger("Loading text from fb search");
			searchtext = frmIBMyReceipentsAddContactFB.tbxSelectRcSearch.text;
		} else {
			searchtext = frmIBMyReceipentsAddContactFB.tbxSearch.text;
		}
	} else if (form == 4) {
		searchtext = frmIBMyReceipentsAddContactManually.tbxSearch.text;
	} else if (form == 5) {
		searchtext = frmIBMyReceipentsEditAccountConf.tbxSearch.text;
	} else if (form == 6) {
		searchtext = frmIBMyReceipentsAccounts.tbxSearch.text;
	} else if (form == 7) {
		searchtext = frmIBMyReceipentsFBContactConf.tbxSearch.text;
	} else if (form == 8) {
		searchtext = frmIBMyReceipentsAddContactManuallyConf.tbxSearch.text;
	}
	var totalData = [];
	switch (parseInt(type.toString())) {
	case 1:
		{
			if (globalRcData[0] != null) {
				for (var i = 0; i < globalRcData[0].length; i++) {
					var isFavourite = globalRcData[0][i]["Favorite"];
					var btnFavSkin;
					if (isFavourite != null) {
						invokeCommonIBLogger("#############setDataToReceipentsListToForm FavoriteFlag##########" + globalRcData[0][i][
							"Favorite"]);
						if (isFavourite.toString() == "y" || isFavourite.toString() == "Y") {
							btnFavSkin = {
								text: "",
								skin: "btnIBStarIconFocus"
							};
						} else {
							btnFavSkin = {
								text: "",
								skin: "btnIBStarIcon"
							};
						}
					} else {
						btnFavSkin = {
							text: "",
							skin: "btnIBStarIcon"
						};
					}
					invokeCommonIBLogger("globalRcData[0][i].lblReceipentName " + globalRcData[0][i].lblReceipentName);
					check = kony.string.startsWith(globalRcData[0][i].lblReceipentName.toString(), searchtext);
					if (check == true) {
						var segDataMap = {
							imgReceipentPic: globalRcData[0][i].imgReceipentPic,
							lblReceipentName: globalRcData[0][i].lblReceipentName,
							lblNumberofAccounts: globalRcData[0][i].lblNumberofAccounts,
							imgMobile: globalRcData[0][i].imgMobile,
							imgMail: globalRcData[0][i].imgMail,
							imgFb: globalRcData[0][i].imgFb,
							imgNxt: globalRcData[0][i].imgNxt,
							receipentID: globalRcData[0][i]["receipentID"],
							mobileNumber: globalRcData[0][i]["mobileNumber"],
							emailId: globalRcData[0][i]["emailId"],
							facebookId: globalRcData[0][i]["facebookId"],
							Favorite: isFavourite,
							btnfav: btnFavSkin
						};
						totalData.push(segDataMap);
					}
				}
			}
		}
		break;
	case 2:
		{
			invokeCommonIBLogger("in case 2");
			frmIBMyReceipentsAddContactFB.linkMore.setVisibility(false);
			if (globalFBData != null) {
				invokeCommonIBLogger("globalFBCacheData.length" + globalFBCacheData.length);
				for (var i = 0; i < globalFBCacheData.length; i++) {
					invokeCommonIBLogger("globalFBCacheData[0][2] name " + globalFBCacheData[i][2]);
					check = kony.string.startsWith(globalFBCacheData[i][2].toString(), searchtext);
					if (check == true) {
						var segDataMap = {
							imgSelected: globalFBCacheData[i][0],
							imgSelectRcProfilePic: globalFBCacheData[i][1],
							lblSelectRcName: globalFBCacheData[i][2],
							lblSelectRcNumber: globalFBCacheData[i][3],
							lblSelectRcEmail: globalFBCacheData[i][4],
							facebookId: globalFBCacheData[i][5],
							lblSelectRcFbId: globalFBCacheData[i][5]
						};
						totalData.push(segDataMap);
					}
				}
			}
		}
		break;
	default:
		break;
	}
	switch (parseInt(form.toString())) {
	case 1:
		frmIBMyReceipentsHome.segmentReceipentListing.setData(totalData);
		break;
	case 2:
		frmIBMyReceipentsAddBankAccnt.segmentReceipentListing.setData(totalData);
		break;
	case 3:
		{
			if (type == 2) {
				invokeCommonIBLogger("filtering data for search");
				frmIBMyReceipentsAddContactFB.segmentSelectRc.setData(totalData);
			} else {
				frmIBMyReceipentsAddContactFB.segmentReceipentListing.setData(totalData);
			}
		}
		break;
	case 4:
		frmIBMyReceipentsAddContactManually.segmentReceipentListing.setData(totalData);
		break;
	case 5:
		frmIBMyReceipentsEditAccountConf.segmentReceipentListing.setData(totalData);
		break;
	case 6:
		frmIBMyReceipentsAccounts.segmentReceipentListing.setData(totalData);
		break;
	case 7:
		frmIBMyReceipentsFBContactConf.segmentReceipentListing.setData(totalData);
		break;
	case 8:
		frmIBMyReceipentsAddContactManuallyConf.segmentReceipentListing.setData(totalData);
		break;
	default:
		break;
	}
	if ((totalData == null || totalData.length == 0)) {
		var currentFormId = kony.application.getCurrentForm();
		if (type == 1) {
			currentFormId.lblNoRecipients.setVisibility(true);
			currentFormId.lblNoRecipients.text = kony.i18n.getLocalizedString("keybillernotfound");
		} else {
			currentFormId.lblFbNoFriends.setVisibility(true);
			currentFormId.lblFbNoFriends.text = kony.i18n.getLocalizedString("Receipent_alert_NoFacebookFriends");
		}
	} else {
		var currentFormId = kony.application.getCurrentForm();
		if (type == 1) {
			currentFormId.lblNoRecipients.setVisibility(false);
		} else {
			currentFormId.lblFbNoFriends.setVisibility(false);
		}
	}
}

/**
 * Method to reset the RC segment data back to initial
 * @returns {}
 */

function resetRcSegData(type, form) {
	var currentFormId = kony.application.getCurrentForm();
	if (type == 1) {
		if (globalRcData[0] == null) {
			currentFormId.lblNoRecipients.setVisibility(true);
			currentFormId.lblNoRecipients.text = kony.i18n.getLocalizedString("keybillernotfound");
			return;
		} else {
			currentFormId.lblNoRecipients.setVisibility(false);
		}
	} else {
		if (globalFBData[0] == null) {
			currentFormId.lblFbNoFriends.setVisibility(true);
			currentFormId.lblFbNoFriends.text = kony.i18n.getLocalizedString("Receipent_alert_NoFacebookFriends");
			return;
		} else {
			currentFormId.lblFbNoFriends.setVisibility(false);
		}
	}
	invokeCommonIBLogger("SETTING DATA..." + globalRcData[0]);
	if (type == 1) {
		currentFormId.lblNoRecipients.setVisibility(false);
	} else {
		if (form == 3) {
			currentFormId.lblFbNoFriends.setVisibility(false);
		}
	}
	var totalData = [];
	if (type == 1) {
		if (globalRcData[0] != null) {
			for (var i = 0; i < globalRcData[0].length; i++) {
				var isFavourite = globalRcData[0][i]["Favorite"];
				var btnFavSkin;
				var segItemSkin = {
					isVisible: "true",
					skin: "hbxSkinSegFocus"
				};
				if (isFavourite != null) {
					invokeCommonIBLogger("#############setDataToReceipentsListToForm FavoriteFlag##########" + globalRcData[0][i][
						"Favorite"]);
					if (isFavourite.toString() == "y" || isFavourite.toString() == "Y") {
						btnFavSkin = {
							text: "",
							skin: "btnIBStarIconFocus"
						};
					} else {
						btnFavSkin = {
							text: "",
							skin: "btnIBStarIcon"
						};
					}
				} else {
					btnFavSkin = {
						text: "",
						skin: "btnIBStarIcon"
					};
				}
				var noOfaccounts;
				noOfaccounts = "";
				for (var k = 0; k < globalRcData[0][i].lblNumberofAccounts.length; k++) {
					if (globalRcData[0][i].lblNumberofAccounts[k] == " ") {
						break;
					} else {
						if (noOfaccounts == null) {
							noOfaccounts = globalRcData[0][i].lblNumberofAccounts[k];
						} else {
							noOfaccounts += globalRcData[0][i].lblNumberofAccounts[k];
						}
					}
				}
				if (noOfaccounts != null) {
					if (parseInt(noOfaccounts.toString()) > 1) {
						accntTitle = kony.i18n.getLocalizedString("Receipent_Accounts");
					} else {
						accntTitle = kony.i18n.getLocalizedString("Receipent_Account");
					}
				} else {
					accntTitle = kony.i18n.getLocalizedString("Receipent_Account");
				}
				invokeCommonIBLogger("Prepare cache data and start set");
				var segDataMap;
				var recipientNameFocus = {
					text: globalRcData[0][i].lblReceipentName,
					skin: lblIB20pxDBOzoneMedBlack1
				};
				var accountofRecipientFocus = {
					text: noOfaccounts.toString() + " " + accntTitle,
					skin: lblIB16pxDBOzoneX1
				};
				var recipientNameNonFocus = {
					text: globalRcData[0][i].lblReceipentName,
					skin: lblIB20pxDBOzoneMedBlack1
				};
				var accountofRecipientNonFocus = {
					text: noOfaccounts.toString() + " " + accntTitle,
					skin: lblIB16pxDBOzoneX1
				};
				var mobileImage;
				var fbImage;
				var mailImage;
				if (globalRcData[0][i].imgMobile != null && globalRcData[0][i].imgMobile != "") {
					mobileImage = "phoneicon.png";
				} else {
					mobileImage = "";
				}
				if (globalRcData[0][i].imgMail != null && globalRcData[0][i].imgMail != "") {
					mailImage = "emailicon.png";
				} else {
					mailImage = "";
				}
				if (globalRcData[0][i].imgFb != null && globalRcData[0][i].imgFb != "") {
					fbImage = "facebookicon.png";
				} else {
					fbImage = "";
				}
				if (parseInt(form.toString()) == 6) {
					invokeCommonIBLogger("SETTING FOCUS " + globalSeletedRcIndex + " " + i);
					if (globalSeletedRcIndex != -1) {
						if (globalSeletedRcIndex == i) {
							segDataMap = {
								imgReceipentPic: globalRcData[0][i].imgReceipentPic,
								lblReceipentName: recipientNameFocus,
								lblNumberofAccounts: accountofRecipientFocus,
								imgMobile: mobileImage,
								imgMail: mailImage,
								imgFb: fbImage,
								imgNxt: globalRcData[0][i].imgNxt,
								receipentID: globalRcData[0][i]["receipentID"],
								mobileNumber: globalRcData[0][i]["mobileNumber"],
								emailId: globalRcData[0][i]["emailId"],
								facebookId: globalRcData[0][i]["facebookId"],
								Favorite: isFavourite,
								btnfav: btnFavSkin,
								hbox1010001831100041: segItemSkin
							};
						} else {
							segDataMap = {
								imgReceipentPic: globalRcData[0][i].imgReceipentPic,
								lblReceipentName: recipientNameNonFocus,
								lblNumberofAccounts: accountofRecipientNonFocus,
								imgMobile: globalRcData[0][i].imgMobile,
								imgMail: globalRcData[0][i].imgMail,
								imgFb: globalRcData[0][i].imgFb,
								imgNxt: globalRcData[0][i].imgNxt,
								receipentID: globalRcData[0][i]["receipentID"],
								mobileNumber: globalRcData[0][i]["mobileNumber"],
								emailId: globalRcData[0][i]["emailId"],
								facebookId: globalRcData[0][i]["facebookId"],
								Favorite: isFavourite,
								btnfav: btnFavSkin
							};
						}
					} else {
						segDataMap = {
							imgReceipentPic: globalRcData[0][i].imgReceipentPic,
							lblReceipentName: recipientNameNonFocus,
							lblNumberofAccounts: accountofRecipientNonFocus,
							imgMobile: globalRcData[0][i].imgMobile,
							imgMail: globalRcData[0][i].imgMail,
							imgFb: globalRcData[0][i].imgFb,
							imgNxt: globalRcData[0][i].imgNxt,
							receipentID: globalRcData[0][i]["receipentID"],
							mobileNumber: globalRcData[0][i]["mobileNumber"],
							emailId: globalRcData[0][i]["emailId"],
							facebookId: globalRcData[0][i]["facebookId"],
							Favorite: isFavourite,
							btnfav: btnFavSkin
						};
					}
				} else {
					segDataMap = {
						imgReceipentPic: globalRcData[0][i].imgReceipentPic,
						lblReceipentName: globalRcData[0][i].lblReceipentName,
						lblNumberofAccounts: noOfaccounts.toString() + " " + accntTitle,
						imgMobile: globalRcData[0][i].imgMobile,
						imgMail: globalRcData[0][i].imgMail,
						imgFb: globalRcData[0][i].imgFb,
						imgNxt: globalRcData[0][i].imgNxt,
						receipentID: globalRcData[0][i]["receipentID"],
						mobileNumber: globalRcData[0][i]["mobileNumber"],
						emailId: globalRcData[0][i]["emailId"],
						facebookId: globalRcData[0][i]["facebookId"],
						Favorite: isFavourite,
						btnfav: btnFavSkin
					};
				}
				totalData.push(segDataMap);
			}
		}
	} else {
		if (globalFBData[0] != null) {
			for (var i = 0; i < globalFBData[0].length; i++) {
				var segDataMap = {
					imgSelected: globalFBData[0][i].imgSelected,
					imgSelectRcProfilePic: globalFBData[0][i].imgSelectRcProfilePic,
					lblSelectRcName: globalFBData[0][i].lblSelectRcName,
					lblSelectRcNumber: globalFBData[0][i].lblSelectRcNumber,
					lblSelectRcEmail: globalFBData[0][i].lblSelectRcEmail,
					lblSelectRcFbId: globalFBData[0][i].lblSelectRcFbId
				};
				totalData.push(segDataMap);
			}
		}
	}
	switch (parseInt(form.toString())) {
	case 1:
		frmIBMyReceipentsHome.segmentReceipentListing.setData(totalData);
		break;
	case 2:
		frmIBMyReceipentsAddBankAccnt.segmentReceipentListing.setData(totalData);
		break;
	case 3:
		{
			if (type == 2) {
				invokeCommonIBLogger("Actual Current View Friends list size is" + globalFBData[0].length);
				frmIBMyReceipentsAddContactFB.segmentSelectRc.setData(totalData);
				if (globalFBData[0].length >= FB_FRIENDS_SIZE_PER_CALL) {
					frmIBMyReceipentsAddContactFB.linkMore.setVisibility(true);
				} else {
					frmIBMyReceipentsAddContactFB.linkMore.setVisibility(false);
				}
			} else {
				frmIBMyReceipentsAddContactFB.segmentReceipentListing.setData(totalData);
			}
		}
		break;
	case 4:
		frmIBMyReceipentsAddContactManually.segmentReceipentListing.setData(totalData);
		break;
	case 5:
		frmIBMyReceipentsEditAccountConf.segmentReceipentListing.setData(totalData);
		break;
	case 6:
		{
			frmIBMyReceipentsAccounts.segmentReceipentListing.setData(totalData);
			//frmIBMyReceipentsAccounts.segmentReceipentListing.selectedIndex = globalSeletedRcIndex;
		}
		break;
	case 7:
		frmIBMyReceipentsFBContactConf.segmentReceipentListing.setData(totalData);
		break;
	case 8:
		frmIBMyReceipentsAddContactManuallyConf.segmentReceipentListing.setData(totalData);
		break;
	default:
		break;
	}
}
/**
 * Method to validate the entered phonenumber
 * @param {} phoneNumber
 * @returns {}
 */

function validatePhoneNumber(phoneNumber) {
	if (phoneNumber != null && phoneNumber != "") {
		var tempPhStr = phoneNumber.toString();
		if (kony.string.isNumeric(tempPhStr)) {
			if (tempPhStr.length == 10) {
			var res = tempPhStr.substring(0, 2);
				if (Gbl_StartDigsMobileNum.indexOf(res) >= 0) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	} else {
		return true;
	}
}
/**
 * Method to encode the entered phone number
 * @param {} accountNumber
 * @returns {}
 */

function encodePhoneNumber(phoneNumber) {
	if (phoneNumber != null && phoneNumber != "") {
		var encodedPhStrArray = [];
		var tempPhStr = phoneNumber.toString();
		for (var i = 0; i < tempPhStr.length; i++) {
			encodedPhStrArray.push(tempPhStr[i]);
		}
		for (var i = 0; i < tempPhStr.length - 4; i++) {
			if (i == 2 || i == 5) {
				encodedPhStrArray[i] = "x-";
			} else {
				encodedPhStrArray[i] = "x";
			}
		}
		invokeCommonIBLogger("encodedPhStrArray" + encodedPhStrArray.toString());
		var retStr;
		for (var i = 0; i < encodedPhStrArray.length; i++) {
			if (i == 0) {
				retStr = encodedPhStrArray[i];
			} else {
				retStr = retStr + encodedPhStrArray[i];
			}
		}
		return retStr.toString();
	}
}
/**
 * Method to validate the entered mail id
 * @param {} mailId
 * @returns {}
 */

function validateEmail(mailId) {
	var pat1 = /[=<>()"',:;\s]/g;
	var noChars = pat1.test(mailId);
	if (noChars == true) {
		return false;
	}
	if (mailId != null && mailId != "") {
		if (kony.string.isValidEmail(mailId)) {
			var exceptionMail = "facebook.com";
			if (kony.string.endsWith(mailId, exceptionMail.toString())) {
				return false;
			}
			return true;
		} else {
			return false;
		}
	} else {
		return true;
	}
}
/**
 * Method to disable or enable the f+ an c+ buttons in count > 99 & vice versa
 * form 1 is frmIBMyReceipentsHome
 * form 2 is frmIBMyReceipentsAddBankAccnt
 * form 3 is frmIBMyReceipentsAddContactFB
 * form 4 is frmIBMyReceipentsAddContactManually
 * form 5 is frmIBMyReceipentsEditAccountConf
 * form 6 is frmIBMyReceipentsAccounts
 * form 7 is frmIBMyReceipentsFBContactConf
 * form 8 is frmIBMyReceipentsAddContactManuallyConf
 * @param {} form
 * @returns {}
 */

function checknSetUIforRcCountLimit(form) {

	invokeCommonIBLogger("parseInt(form.toString())" + parseInt(form.toString()));
	switch (parseInt(form.toString())) {
	case 1:
		{
			//Disable f+ and add contact is more than 99 receipents
			if (getReceipentCount() >= MAX_RECIPIENT_COUNT) {
				invokeCommonIBLogger("DISABLING BUTTONS");
				frmIBMyReceipentsHome.btnAddFrmfb.setEnabled(false);
				frmIBMyReceipentsHome.btnAddManually.setEnabled(false);
				frmIBMyReceipentsHome.btnAddFrmfb.skin = btnIBfbplusdis;
				frmIBMyReceipentsHome.btnAddManually.skin = btnIBlistplusdis;
				frmIBMyReceipentsHome.btnAddFrmfb.focusSkin = btnIBfbplusdis;
				frmIBMyReceipentsHome.btnAddManually.focusSkin = btnIBlistplusdis;
				return;
			} else {
				invokeCommonIBLogger("ENABLING BUTTONS");
				frmIBMyReceipentsHome.btnAddFrmfb.setEnabled(true);
				frmIBMyReceipentsHome.btnAddManually.setEnabled(true);
				frmIBMyReceipentsHome.btnAddFrmfb.skin = btnIBfbplus;
				frmIBMyReceipentsHome.btnAddManually.skin = btnIBlistplus;
				frmIBMyReceipentsHome.btnAddFrmfb.focusSkin = btnIBfbplus;
				frmIBMyReceipentsHome.btnAddManually.focusSkin = btnIBlistplus;
				return;
			}
		}
		break;
	case 2:
		{
			//Disable f+ and add contact is more than 99 receipents
			if (getReceipentCount() >= MAX_RECIPIENT_COUNT ) {
				invokeCommonIBLogger("DISABLING BUTTONS");
				frmIBMyReceipentsAddBankAccnt.btnAddFrmfb.setEnabled(false);
				frmIBMyReceipentsAddBankAccnt.btnAddManually.setEnabled(false);
				frmIBMyReceipentsAddBankAccnt.btnAddFrmfb.skin = btnIBfbplusdis;
				frmIBMyReceipentsAddBankAccnt.btnAddManually.skin = btnIBlistplusdis;
				return;
			} else {
				invokeCommonIBLogger("ENABLING BUTTONS");
				frmIBMyReceipentsAddBankAccnt.btnAddFrmfb.setEnabled(true);
				frmIBMyReceipentsAddBankAccnt.btnAddManually.setEnabled(true);
				frmIBMyReceipentsAddBankAccnt.btnAddFrmfb.skin = btnIBfbplus;
				frmIBMyReceipentsAddBankAccnt.btnAddManually.skin = btnIBlistplus;
				return;
			}
		}
		break;
	case 3:
		{
			//Disable f+ and add contact is more than 99 receipents
			if (getReceipentCount() >= MAX_RECIPIENT_COUNT ) {
				invokeCommonIBLogger("DISABLING BUTTONS");
				frmIBMyReceipentsAddContactFB.btnAddFrmfb.setEnabled(false);
				frmIBMyReceipentsAddContactFB.btnAddManually.setEnabled(false);
				frmIBMyReceipentsAddContactFB.btnAddFrmfb.skin = btnIBfbplusdis;
				frmIBMyReceipentsAddContactFB.btnAddManually.skin = btnIBlistplusdis;
				return;
			} else {
				invokeCommonIBLogger("ENABLING BUTTONS");
				frmIBMyReceipentsAddContactFB.btnAddFrmfb.setEnabled(true);
				frmIBMyReceipentsAddContactFB.btnAddManually.setEnabled(true);
				frmIBMyReceipentsAddContactFB.btnAddFrmfb.skin = btnIBfbplus;
				frmIBMyReceipentsAddContactFB.btnAddManually.skin = btnIBlistplus;
				return;
			}
		}
		break;
	case 4:
		{
			//Disable f+ and add contact is more than 99 receipents
			if (getReceipentCount() >= MAX_RECIPIENT_COUNT ) {
				invokeCommonIBLogger("DISABLING BUTTONS");
				frmIBMyReceipentsAddContactManually.btnAddFrmfb.setEnabled(false);
				frmIBMyReceipentsAddContactManually.btnAddManually.setEnabled(false);
				frmIBMyReceipentsAddContactManually.btnAddFrmfb.skin = btnIBfbplusdis;
				frmIBMyReceipentsAddContactManually.btnAddManually.skin = btnIBlistplusdis;
				return;
			} else {
				invokeCommonIBLogger("ENABLING BUTTONS");
				frmIBMyReceipentsAddContactManually.btnAddFrmfb.setEnabled(true);
				frmIBMyReceipentsAddContactManually.btnAddManually.setEnabled(true);
				frmIBMyReceipentsAddContactManually.btnAddFrmfb.skin = btnIBfbplus;
				frmIBMyReceipentsAddContactManually.btnAddManually.skin = btnIBlistplus;
				return;
			}
			gblfbSelectionState = 0;
		}
		break;
	case 5:
		{
			//Disable f+ and add contact is more than 99 receipents
			if (getReceipentCount() >= MAX_RECIPIENT_COUNT ) {
				invokeCommonIBLogger("DISABLING BUTTONS");
				frmIBMyReceipentsEditAccountConf.btnAddFrmfb.setEnabled(false);
				frmIBMyReceipentsEditAccountConf.btnAddManually.setEnabled(false);
				frmIBMyReceipentsEditAccountConf.btnAddFrmfb.skin = btnIBfbplusdis;
				frmIBMyReceipentsEditAccountConf.btnAddManually.skin = btnIBlistplusdis;
				return;
			} else {
				invokeCommonIBLogger("ENABLING BUTTONS");
				frmIBMyReceipentsEditAccountConf.btnAddFrmfb.setEnabled(true);
				frmIBMyReceipentsEditAccountConf.btnAddManually.setEnabled(true);
				frmIBMyReceipentsEditAccountConf.btnAddFrmfb.skin = btnIBfbplus;
				frmIBMyReceipentsEditAccountConf.btnAddManually.skin = btnIBlistplus;
				return;
			}
		}
		break;
	case 6:
		{
			//Disable f+ and add contact is more than 99 receipents
			if (getReceipentCount() >= MAX_RECIPIENT_COUNT) {
				invokeCommonIBLogger("DISABLING BUTTONS");
				frmIBMyReceipentsAccounts.btnAddFrmfb.setEnabled(false);
				frmIBMyReceipentsAccounts.btnAddManually.setEnabled(false);
				frmIBMyReceipentsAccounts.btnAddFrmfb.skin = btnIBfbplusdis;
				frmIBMyReceipentsAccounts.btnAddManually.skin = btnIBlistplusdis;
				return;
			} else {
				invokeCommonIBLogger("ENABLING BUTTONS");
				frmIBMyReceipentsAccounts.btnAddFrmfb.setEnabled(true);
				frmIBMyReceipentsAccounts.btnAddManually.setEnabled(true);
				frmIBMyReceipentsAccounts.btnAddFrmfb.skin = btnIBfbplus;
				frmIBMyReceipentsAccounts.btnAddManually.skin = btnIBlistplus;
				return;
			}
			//Disable f+ and add contact crm status is locked
			//if (getCRMLockStatus()) {
//				invokeCommonIBLogger("DISABLING BUTTONS");
//				frmIBMyReceipentsAccounts.btnAddFrmfb.setEnabled(false);
//				frmIBMyReceipentsAccounts.btnAddManually.setEnabled(false);
//				frmIBMyReceipentsAccounts.btnAddFrmfb.skin = btnIBfbplusdis;
//				frmIBMyReceipentsAccounts.btnAddManually.skin = btnIBlistplusdis;
//				frmIBMyReceipentsAccounts.btnAddAccount.setEnabled(false);
//				frmIBMyReceipentsAccounts.btnDelete.setEnabled(false);
//				frmIBMyReceipentsAccounts.btnEdit.setEnabled(false);
//				frmIBMyReceipentsAccounts.button50273743117731.setEnabled(false);
//				frmIBMyReceipentsAccounts.button50273743117733.setEnabled(false);
//				return;
//			} else {
//				invokeCommonIBLogger("ENABLING BUTTONS");
//				frmIBMyReceipentsAccounts.btnAddFrmfb.setEnabled(true);
//				frmIBMyReceipentsAccounts.btnAddManually.setEnabled(true);
//				frmIBMyReceipentsAccounts.btnAddFrmfb.skin = btnIBfbplus;
//				frmIBMyReceipentsAccounts.btnAddManually.skin = btnIBlistplus;
//				frmIBMyReceipentsAccounts.btnAddAccount.setEnabled(true);
//				frmIBMyReceipentsAccounts.btnDelete.setEnabled(true);
//				frmIBMyReceipentsAccounts.btnEdit.setEnabled(true);
//				frmIBMyReceipentsAccounts.button50273743117731.setEnabled(true);
//				frmIBMyReceipentsAccounts.button50273743117733.setEnabled(true);
//				return;
//			}
		}
		break;
	case 7:
		{
			//Disable f+ and add contact is more than 99 receipents
			if (getReceipentCount() >= MAX_RECIPIENT_COUNT ) {
				invokeCommonIBLogger("DISABLING BUTTONS");
				frmIBMyReceipentsFBContactConf.btnAddFrmfb.setEnabled(false);
				frmIBMyReceipentsFBContactConf.btnAddManually.setEnabled(false);
				frmIBMyReceipentsFBContactConf.btnAddFrmfb.skin = btnIBfbplusdis;
				frmIBMyReceipentsFBContactConf.btnAddManually.skin = btnIBlistplusdis;
				frmIBMyReceipentsFBContactConf.btnaddother.setEnabled(false);
				frmIBMyReceipentsFBContactConf.btnaddother.skin = btnIB320dis;
				return;
			} else {
				invokeCommonIBLogger("ENABLING BUTTONS");
				frmIBMyReceipentsFBContactConf.btnAddFrmfb.setEnabled(true);
				frmIBMyReceipentsFBContactConf.btnAddManually.setEnabled(true);
				frmIBMyReceipentsFBContactConf.btnAddFrmfb.skin = btnIBfbplus;
				frmIBMyReceipentsFBContactConf.btnAddManually.skin = btnIBlistplus;
				frmIBMyReceipentsFBContactConf.btnaddother.setEnabled(true);
				frmIBMyReceipentsFBContactConf.btnaddother.skin = btnIB320;
				return;
			}
		}
		break;
	case 8:
		{
			//Disable f+ and add contact is more than 99 receipents
			if (getReceipentCount() >= MAX_RECIPIENT_COUNT) {
				invokeCommonIBLogger("DISABLING BUTTONS");
				frmIBMyReceipentsAddContactManuallyConf.btnAddFrmfb.setEnabled(false);
				frmIBMyReceipentsAddContactManuallyConf.btnAddManually.setEnabled(false);
				frmIBMyReceipentsAddContactManuallyConf.btnAddFrmfb.skin = btnIBfbplusdis;
				frmIBMyReceipentsAddContactManuallyConf.btnAddManually.skin = btnIBlistplusdis;
				frmIBMyReceipentsAddContactManuallyConf.btnAddMoreRc.setEnabled(false);
				frmIBMyReceipentsAddContactManuallyConf.btnAddMoreRc.skin = btnIB158disabled;
				return;
			} else {
				invokeCommonIBLogger("ENABLING BUTTONS");
				frmIBMyReceipentsAddContactManuallyConf.btnAddFrmfb.setEnabled(true);
				frmIBMyReceipentsAddContactManuallyConf.btnAddManually.setEnabled(true);
				frmIBMyReceipentsAddContactManuallyConf.btnAddFrmfb.skin = btnIBfbplus;
				frmIBMyReceipentsAddContactManuallyConf.btnAddManually.skin = btnIBlistplus;
				frmIBMyReceipentsAddContactManuallyConf.btnAddMoreRc.setEnabled(true);
				frmIBMyReceipentsAddContactManuallyConf.btnAddMoreRc.skin = btnIB158;
				return;
			}
		}
		break;
	default:
		break;
	}
}
/**
 * Methof to launch the Receipents main page
 * @returns {}
 */

//function launchRcHomeFormnDestroyOthers() {
//	try {
//		TMBUtil.DestroyForm(frmIBMyReceipentsAddBankAccnt);
//		TMBUtil.DestroyForm(frmIBMyReceipentsAddContactFB);
//		TMBUtil.DestroyForm(frmIBMyReceipentsAddContactManually);
//		TMBUtil.DestroyForm(frmIBMyReceipentsAccounts);
//		TMBUtil.DestroyForm(frmIBMyReceipentsEditAccountConf);
//		TMBUtil.DestroyForm(frmIBMyReceipentsAddContactManuallyConf);
//		TMBUtil.DestroyForm(frmIBMyReceipentsFBContactConf);
//	} catch (e) {
//		// todo: handle exception
//		alert("" + Receipent_alert_EXCEPTION);
//	}
//	frmIBMyReceipentsHome.show();
//}
/**
 * Method to conreol the IB menu
 * @returns {}
 */

/*function controlIBMenu() {
	var curr_form = kony.application.getCurrentForm()
	curr_form.segMenuOptions.setData([{
		"lblSegData": {
			"skin": "lblIBSegMenu",
			"text": "My Profile"
		}
			}, {
		"lblSegData": {
			"skin": "lblIBSegMenu",
			"text": "My Accounts"
		}
			}, {
		"lblSegData": {
			"skin": "lblIBSegMenu",
			"text": "Open New Account"
		}
			}, {
		"lblSegData": {
			"skin": "lblIBsegMenuFocus",
			"text": "My Recipients"
		}
			}, {
		"lblSegData": {
			"skin": "lblIBSegMenu",
			"text": "My Bills"
		}
			}, {
		"lblSegData": {
			"skin": "lblIBSegMenu",
			"text": "My Top-Ups"
		}
			}]);
	gblMenuSelection = 1;
}*/
/**
 * Method to show the loading screen
 * @returns {}
 */

function showLoadingScreenPopup() {
	if(gblDeviceInfo["category"]=="IE" && gblDeviceInfo["version"]=="8")
	{
		if(document.getElementById("popupIBLoading") == null)
		popupIBLoading.show();
		
	}
	else
	{
		kony.application.showLoadingScreen("frmLoading", "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null); // Adding this code to fix iPad hang issues.Please comment this and uncomment above 2 lines if any issue is there
	}
}
/**
 * Method to dismiss the loading screen
 * @returns {}
 */

function dismissLoadingScreenPopup() {
	if(gblDeviceInfo["category"]=="IE" && gblDeviceInfo["version"]=="8"){
			popupIBLoading.dismiss();
	}else{
		kony.application.dismissLoadingScreen(); // Adding this code to fix iPad hang issues.Please comment this and uncomment above line if any issue is there
	}
}

function showAlert(keyMsg, KeyTitle) {
	var okk = kony.i18n.getLocalizedString("keyOK");
	//Defining basicConf parameter for alert
	var basicConf = {
		message: keyMsg,
		alertType: constants.ALERT_TYPE_INFO,
		alertTitle: KeyTitle,
		yesLabel: okk,
		noLabel: "",
		alertHandler: handle2
	};
	//Defining pspConf parameter for alert
	var pspConf = {};
	//Alert definition
	var infoAlert = kony.ui.Alert(basicConf, pspConf);

	function handle2(response) {}
	return;
}


function showAlertWithCallBack(keyMsg, KeyTitle, callBack) {
	var okk = kony.i18n.getLocalizedString("keyOK");
	//Defining basicConf parameter for alert
	var basicConf = {
		message: keyMsg,
		alertType: constants.ALERT_TYPE_INFO,
		alertTitle: KeyTitle,
		yesLabel: okk,
		noLabel: "",
		alertHandler: callBack
	};
	//Defining pspConf parameter for alert
	var pspConf = {};
	//Alert definition
	var infoAlert = kony.ui.Alert(basicConf, pspConf);
}

function editAccNum(txt) {
	if (txt == null) return false;
	if(txt.length != 10)
	return txt;
	var numChars = txt.length;
	var temp = "";
	var i, txtLen = numChars;
	var currLen = numChars;
	for (i = 0; i < numChars; ++i) {
		if (txt[i] != '-') {
			temp = temp + txt[i];
		} else {
			txtLen--;
		}
	}
	var iphenText = "";
	for (i = 0; i < txtLen; i++) {
		iphenText += temp[i];
		if (i == 2 || i == 3 || i == 8) {
			iphenText += '-';
		}
	}
	frmIBMyReceipentsAddBankAccnt.tbxAddAccntNumber.text = iphenText;
	
}
//account no format
/**
 * Method to check if need token/OTP validation
 */

function validateSecurityForTransactionServiceRc() {
	if (gblRC_QA_TEST_VAL == 0) {
		//No Support as of now
	} else {
		if (gblRcValidateAccnt) {
			invokeCommonIBLogger("startRCTransactionSecurityValidationService");
			startRCTransactionSecurityValidationService();
		}
	}
}
/**
 * Method to check if need token/OTP validation
 */

function validateSecurityForTransactionRc(form) {
	if (gblRC_QA_TEST_VAL == 0) {
		//No Support as of now
	} else {
		if (gblRcValidateAccnt) {
			//if (tokenFlag == false && gblTokenSwitchFlag ==false) {
				validateOTP(form);
			//} else {
//				validateTokenRc(form);
//			}
		} else {
			if (form == frmIBMyReceipentsAddContactFB) {
				addFBUsers();
			} else if (form == frmIBMyReceipentsAddBankAccnt) {
				confirmBankAccntEdit();
			} else if (form == frmIBMyReceipentsAddContactManually) {
				if (gblAdd_Receipent_State == gblEXISTING_RC_EDITION) {
					frmIBMyReceipentsAddContactManually.lblrecipientname.text = frmIBMyReceipentsAddContactManually.txtRecipientName.text;
					frmIBMyReceipentsAddContactManually.lblmobnum.text = encodePhoneNumber(frmIBMyReceipentsAddContactManually.txtmobnum.text);
					frmIBMyReceipentsAddContactManually.lblemail.text = frmIBMyReceipentsAddContactManually.txtemail.text;
					frmIBMyReceipentsAddContactManually.label10107168132952.setVisibility(false);
					frmIBMyReceipentsAddContactManually.scrollboxRcAccounts.setVisibility(false);
					frmIBMyReceipentsAddContactManually.line101071681314188.setVisibility(false);
					frmIBMyReceipentsAddContactManually.hboxMyRecipientDetail.setVisibility(true);
					frmIBMyReceipentsAddContactManually.hboxMyRecipient.setVisibility(false);
				} else {
					confirmBankAccntEdit();
				}
			} else {
				frmIBMyReceipentsAddContactManually.lblrecipientname.text = frmIBMyReceipentsAddContactManually.txtRecipientName.text;
				frmIBMyReceipentsAddContactManually.lblmobnum.text = encodePhoneNumber(frmIBMyReceipentsAddContactManually.txtmobnum.text);
				frmIBMyReceipentsAddContactManually.lblemail.text = frmIBMyReceipentsAddContactManually.txtemail.text;
				frmIBMyReceipentsAddContactManually.label10107168132952.setVisibility(false);
				frmIBMyReceipentsAddContactManually.scrollboxRcAccounts.setVisibility(false);
				frmIBMyReceipentsAddContactManually.line101071681314188.setVisibility(false);
				frmIBMyReceipentsAddContactManually.hboxMyRecipientDetail.setVisibility(true);
				frmIBMyReceipentsAddContactManually.hboxMyRecipient.setVisibility(false);
			}
		}
	}
}
/**
 * Method that returns the crm lock status...false if unlocked and true if locked
 * @returns {}
 */

function getCRMLockStatus() {
if(gblUserLockStatusIB==gblFinancialTxnIBLock)
	gblRcOpLockStatus=true;
else{
gblRcOpLockStatus=false;
}
return gblRcOpLockStatus;
}
/**
 * Method to reset all the Rc cached variables
 * @returns {}
 */

function resetAllRcCacheData() {
	if (globalTobeAddedFBDataConfirmation != null) {
		for (var i = 0; i < globalTobeAddedFBDataConfirmation.length; i++) {
			globalTobeAddedFBDataConfirmation.pop();
		}
		globalTobeAddedFBDataConfirmation = [];
	}
	if (globalFBConfData != null) {
		for (var i = 0; i < globalFBConfData.length; i++) {
			globalFBConfData.pop();
		}
		globalFBConfData = [];
	}
	if (globalFBCacheData != null) {
		for (var i = 0; i < globalFBCacheData.length; i++) {
			globalFBCacheData.pop();
		}
	}
	globalFBCacheData = [];
	destroySavedBankData();
	securityValFlag = 0;
	clearCachedRcList();
	if (globalSelectBankData != null) {
		for (var i = 0; i < globalSelectBankData.length; i++) {
			globalSelectBankData.pop();
		}
		globalSelectBankData = [];
	}
	gblRcOpLockStatus = false;
}
/**
 * Method to hode the otp UI
 * @returns {}
 */

function disableOtpBoxUi() {
	if (gblAdd_Receipent_State == gblEXISTING_RC_BANKACCNT_ADDITION || gblAdd_Receipent_State ==
		gblEXISTING_RC_BANKACCNT_EDITION) {
		frmIBMyReceipentsAddBankAccnt.hbox476047582127699.setVisibility(false);
		frmIBMyReceipentsAddBankAccnt.hbxOTPsnt.setVisibility(false);
	} else if (gblAdd_Receipent_State == gblEXISTING_RC_EDITION || gblAdd_Receipent_State == gblNEW_RC_ADDITION) {
		frmIBMyReceipentsAddContactManually.hbox101089555968194.setVisibility(false);
		frmIBMyReceipentsAddContactManually.hbox101089555968200.setVisibility(false);
	} else {
		frmIBMyReceipentsAddContactFB.hbox476047582127699.setVisibility(false);
		frmIBMyReceipentsAddContactFB.hbxOTPsnt.setVisibility(false);
	}
}
/**
 * Method to reset the data on the bank addition form
 * @returns {}
 */

function clearDataOnRcBankAdditionForm() {
	frmIBMyReceipentsAddBankAccnt.tbxAddAccntNumber.text = "";
	frmIBMyReceipentsAddBankAccnt.tbxAddAccntNickName.text = "";
	listAvailableBanksforSelection();
}
/**
 * Method to reset the data on the receipent addition form
 * @returns {}
 */

function clearDataOnRcNewRecipientAdditionForm() {
	isAddFb=false;
	recipientAddFromTransfer=false;
	gblTransferFromRecipient=false;
	frmIBMyReceipentsAddContactManually.btnOTPReq.skin = btn100;
	frmIBMyReceipentsAddContactManually.btnOTPReq.focusSkin= btn100;
	frmIBMyReceipentsAddContactManually.btnOTPReq.setEnabled(true);
	frmIBMyReceipentsAddContactManually.btnotp.skin = btn100;
	frmIBMyReceipentsAddContactManually.btnotp.focusSkin = btn100;
	frmIBMyReceipentsAddContactManually.btnotp.setEnabled(true);
	frmIBMyReceipentsAddContactManually.lblfbidstudio19.text="";//Modified by Studio Viz
	frmIBMyReceipentsAddContactManually.lblPlusSign.setVisibility(true);
	frmIBMyReceipentsAddContactManually.lblPlus.setVisibility(true);
    frmIBMyReceipentsAddContactManually.link50273743118087.skin=lnkNormal;
   	frmIBMyReceipentsAddContactManually.hbox101000183152453.setVisibility(true)
    frmIBMyReceipentsAddContactManually.lblPlus.skin=lblIB30pxBlue;
	frmIBMyReceipentsAddContactManually.image210107168135.src = "nouserimg.jpg";
	frmIBMyReceipentsAddContactManually.txtemail.text = "";
	frmIBMyReceipentsAddContactManually.txtmobnum.text = "";
	frmIBMyReceipentsAddContactManually.txtRecipientName.text = "";
	frmIBMyReceipentsAddContactManually.lblFbIDstudio18.setVisibility(false);//Modified by Studio Viz
	frmIBMyReceipentsAddContactManually.lblfbidstudio19.text ="";//Modified by Studio Viz
	frmIBMyReceipentsAddContactManually.tbxFbID.text="";
	frmIBMyReceipentsAddContactManually.hbox104300825096149.setVisibility(false);
	frmIBMyReceipentsAddContactManually.segmentRcAccounts.removeAll();
	frmIBMyReceipentsAddContactManually.txtRecipientName.setFocus(true);
}
/**
 * Method to invoke code after security validation is completed
 * @returns {}
 */

/*function proceedOnSecurityValidation() {
	dismissLoadingScreenPopup();
	if (gblAdd_Receipent_State == gblEXISTING_RC_EDITION) {
		frmIBMyReceipentsAddContactManually.image2101071681311897.src = frmIBMyReceipentsAddContactManually.image210107168135.src;
		frmIBMyReceipentsAddContactManually.lblrecipientname.text = frmIBMyReceipentsAddContactManually.txtRecipientName.text;
		if (frmIBMyReceipentsAddContactManually.txtmobnum.text == null || frmIBMyReceipentsAddContactManually.txtmobnum.text ==	"")
			frmIBMyReceipentsAddContactManually.lblmobnum.text = "";
		else
			frmIBMyReceipentsAddContactManually.lblmobnum.text = encodePhoneNumber(frmIBMyReceipentsAddContactManually.txtmobnum
				.text);
		if (frmIBMyReceipentsAddContactManually.txtemail.text == null || frmIBMyReceipentsAddContactManually.txtemail.text ==
			"")
			frmIBMyReceipentsAddContactManually.lblemail.text = "";
		else
			frmIBMyReceipentsAddContactManually.lblemail.text = frmIBMyReceipentsAddContactManually.txtemail.text;
			confirmNewRcAdd();	
	} else if (gblAdd_Receipent_State == gblEXISTING_RC_BANKACCNT_ADDITION || gblAdd_Receipent_State ==
		gblEXISTING_RC_BANKACCNT_EDITION) {
		//Add code here
		confirmBankAccntEdit();
	} else if (gblAdd_Receipent_State == gblNEW_RC_ADDITION || gblAdd_Receipent_State == gblNEW_RC_ADDITION_PRECONFIRM) {
		//Add code here
		if (parseInt(formId.toString()) == 3) {
			addFBUsers();
		} else {
			confirmNewRcAdd();
		}
	} else {
		//Add code here
		addFBUsers();
	}
}*/
/**
 * Method to show common alert accross Recipients module
 * @param {} msg
 * @param {} statusCode
 * @returns {}
 */

function showCommonAlertIB(resultable) {
	if (resultable.StatusCode != null && resultable.StatusCode != 0) {
		var errStr = kony.i18n.getLocalizedString(resultable.StatusCode);
		if (errStr != null && errStr.length > 0) {
			alert(errStr + "[" + statusCode + "]");
		} else {
			if (msg != null && msg.length > 0) {
				alert(msg);
			} else {
				alert(kony.i18n.getLocalizedString("ECGenericError")); 
			}
		}
		return true;
	}
	return false;
}
/**
 * Method to show common alert accross Recipients module
 * @param {} msg
 * @param {} statusCode
 * @returns {}
 */

function showCommonAlert(msg, statusCode) {
		if (msg != null && msg.length > 0) { 
			alert(msg);
		} else {
			alert(kony.i18n.getLocalizedString("ECGenericError")); //Show general error here...i18n Key to be added
		}
 	}
/**
 * Method to check of the to be added account number already exists in the list of Crm accounts
 * @returns {}
 */

function checkAccounNumbersWithCrmAccounts(accountNumber) {

	accountNumber = decodeAccountNumbers(accountNumber);
	//var custOwnAccunts = gblAccountTable["custAcctRec"];
	if(gblReceipentAccts != undefined && gblReceipentAccts.length > 0){
		for(var i = 0; i < gblReceipentAccts.length ; i++){
			var testAcc = decodeAccountNumbers(gblReceipentAccts[i]["accId"]);
			if(testAcc.length == 14){
				testAcc = testAcc.substr(4);
			}
			if(decodeAccountNumbers(accountNumber) == testAcc){
				//showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_correctaccount"), kony.i18n.getLocalizedString("info"), "info")
				if(gblReceipentAccts[i]["productID"] == Gbl_Prod_Code_MEACC )
					return "MEAccount";
				else
					return true;
				
			}
		}
	}
	if(otherBankMyAccounts != undefined && otherBankMyAccounts.length > 0){
		for(var i = 0; i < otherBankMyAccounts.length ; i++){
			var testAcc = decodeAccountNumbers(otherBankMyAccounts[i]);
			if(testAcc.length == 14){
				testAcc = testAcc.substr(4);
			}
			if(decodeAccountNumbers(accountNumber) == testAcc){
				//showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_correctaccount"), kony.i18n.getLocalizedString("info"), "info")
				return true;
			}
		}
	}
	
	
	return false;
}
/**
 * Method to find out the actual recipient index if filtered data
 * @returns {}
 */

function checkActualRcSegItemIndex() {
	var curr_form = kony.application.getCurrentForm();
	if (globalRcData[0].length == curr_form.segmentReceipentListing.data.length) {
		//No filtering has been done
		return false;
	} else {
		for (var i = 0; i < globalRcData[0].length; i++) {
			if (curr_form.segmentReceipentListing.selectedItems[0]["lblReceipentName"] == globalRcData[0][i]["lblReceipentName"]) {
				globalSeletedRcIndex = i;
				return true;
			}
		}
	}
	return false;
}
/**
 * Method to create list for Rc Account notification
 * @returns {}
 */

function createListForRcAccountNotification() {
	var tempData = [];
	tempData.push(frmIBMyReceipentsAddContactManually.segmentRcAccounts.data);
	var totalData = [];
	if (tempData[0] != null) {
		for (var i = 0; i < tempData[0].length; i++) {
			totalData.push(tempData[0][i].lblNumber);
			totalData.push(tempData[0][i].lblNickName);
			totalData.push(tempData[0][i].lblBankName);			
		}
		invokeCommonIBLogger("LIST FOR NOTIFICATION SERVICE########" + totalData.toString());
		return totalData;
	}else{
		totalData.push("No Accounts Added");
		return totalData;
	}
}
/**
 * Method to create list for Rc Account notification
 * @returns {}
 */

function createListForRcAccountNotificationExistingRc() {
	var tempData = [];
	tempData.push(frmIBMyReceipentsAddContactManuallyConf.segmentRcAccounts.data);
	var totalData = [];
	if (tempData[0] != null) {
		for (var i = 0; i < tempData[0].length; i++) {
			totalData.push(tempData[0][i].lblNumber);
			totalData.push(tempData[0][i].lblNickName);
			totalData.push(tempData[0][i].lblBankName);
		}
	}
	invokeCommonIBLogger("LIST FOR NOTIFICATION SERVICE########" + totalData.toString());
	return totalData;
}
/**
 * Method to remove - from numbers
 * @param {} phone
 * @returns {}
 */

function decodePhoneNumber(phone) {
	var retNum;
	for (var i = 0; i < phone.length; i++) {
		if (phone[i] != "-") {
			if (retNum == null) {
				retNum = phone[i];
			} else {
				retNum = retNum + phone[i];
			}
		}
	}
	return retNum;
}
/**
 * Method to remove the OTP
 * @returns {}
 */

function clearOTP() {
	invokeCommonIBLogger("gblAdd_Receipent_State " + gblAdd_Receipent_State + "formId " + formId);
	if (gblAdd_Receipent_State == gblEXISTING_RC_EDITION || gblAdd_Receipent_State == gblNEW_RC_ADDITION || gblAdd_Receipent_State == gblNEW_RC_ADDITION_PRECONFIRM) {
		if (parseInt(formId.toString()) == 3) {
			frmIBMyReceipentsAddContactFB.textbox247428873338513.text = "";
		} else {
			frmIBMyReceipentsAddContactManually.txtotp.text = "";
		}
	}  else if (gblAdd_Receipent_State == gblEXISTING_RC_BANKACCNT_ADDITION || gblAdd_Receipent_State ==
		gblEXISTING_RC_BANKACCNT_EDITION) {
		frmIBMyReceipentsAddBankAccnt.textbox247428873338513.text = "";
	}
}
/**
 * Method to encrypt the customer CRM id
 * Mapping logic is with numbers to reverse alphabets
 * @param {} custCrmId
 * @returns {}
 */

function encryptCRMId(custCrmId) {
	invokeCommonIBLogger("encryptCRMId");
	var encyyptionStr = "zyxwvutsrq";
	var encryptedCrmId = "";
	for (var i = 0; i < custCrmId.length; i++) {
		var crmIdDigit = custCrmId[i];
		switch (parseInt(crmIdDigit.toString())) {
		case 0:
			encryptedCrmId = encryptedCrmId + encyyptionStr[0];
			break;
		case 1:
			encryptedCrmId = encryptedCrmId + encyyptionStr[1];
			break;
		case 2:
			encryptedCrmId = encryptedCrmId + encyyptionStr[2];
			break;
		case 3:
			encryptedCrmId = encryptedCrmId + encyyptionStr[3];
			break;
		case 4:
			encryptedCrmId = encryptedCrmId + encyyptionStr[4];
			break;
		case 5:
			encryptedCrmId = encryptedCrmId + encyyptionStr[5];
			break;
		case 6:
			encryptedCrmId = encryptedCrmId + encyyptionStr[6];
			break;
		case 7:
			encryptedCrmId = encryptedCrmId + encyyptionStr[7];
			break;
		case 8:
			encryptedCrmId = encryptedCrmId + encyyptionStr[8];
			break;
		case 9:
			encryptedCrmId = encryptedCrmId + encyyptionStr[9];
			break;
		default:
			break;
		}
	}
	invokeCommonIBLogger("Encrypted id is " + encryptedCrmId);
	return encryptedCrmId;
}
/**
 * Method to decrypt the customer CRM id
 * @param {} custCrmId
 * @returns {}
 */

//function decryptCRMId(custCrmId) {
//	invokeCommonIBLogger("decryptCRMId");
//	var decyyptionStr = "0123456789";
//	var referStr = "zyxwvutsrq";
//	var decryptedCrmId = "";
//	for (var i = 0; i < custCrmId.length; i++) {
//		for (var j = 0; j < referStr.length; j++) {
//			if (custCrmId[i] == referStr[j]) {
//				decryptedCrmId = decryptedCrmId + decyyptionStr[j];
//				break;
//			}
//		}
//	}
//	invokeCommonIBLogger("Decrypted id is " + decryptedCrmId);
//	return decryptedCrmId;
//}
/**
 * Mrthod to enable the OTP/token req button
 * @returns {}
 */

function enableOTPReqButton() {
	if (gblAdd_Receipent_State == gblEXISTING_RC_EDITION) {
		frmIBMyReceipentsAddContactManually.btnOTPReq.skin = btn100;
		frmIBMyReceipentsAddContactManually.txtotp.text = "";
		if (securityValFlag == 2)
			frmIBMyReceipentsAddContactManually.btnOTPReq.onClick = startOTPRequestService;
	} else if (gblAdd_Receipent_State == gblNEW_RC_ADDITION || gblAdd_Receipent_State == gblNEW_RC_ADDITION_PRECONFIRM) {
		frmIBMyReceipentsAddContactManually.btnotp.skin = btn100;
		frmIBMyReceipentsAddContactManually.btnOTPReq.skin = btn100;
		frmIBMyReceipentsAddContactManually.txtotp.text = "";
		frmIBMyReceipentsAddContactManually.textbox247428873338513.text = "";
		if (securityValFlag == 2) {
			frmIBMyReceipentsAddContactManually.btnOTPReq.onClick = startOTPRequestService;
			frmIBMyReceipentsAddContactManually.btnotp.onClick = startOTPRequestService;
		}
	} else if (gblAdd_Receipent_State == gblEXISTING_RC_BANKACCNT_ADDITION || gblAdd_Receipent_State ==
		gblEXISTING_RC_BANKACCNT_EDITION) {
		frmIBMyReceipentsAddBankAccnt.btnOTPReq.skin = btn100;
		frmIBMyReceipentsAddBankAccnt.textbox247428873338513.text = "";
		if (securityValFlag == 2)
			frmIBMyReceipentsAddBankAccnt.btnOTPReq.onClick = startOTPRequestService;
	} else {
		frmIBMyReceipentsAddContactFB.btnOTPReq.skin = btn100;
		frmIBMyReceipentsAddContactFB.textbox247428873338513.text = "";
		if (securityValFlag == 2)
			frmIBMyReceipentsAddContactFB.btnOTPReq.onClick = startOTPRequestService;
	}
}

/**
 * Method to check th module locally or after service integration dynamically
 * @returns {}
 */

function setDatatoReceipentsList(form) {
	formId = form;
	curr_form = kony.application.getCurrentForm();
	curr_form.tbxSearch.text = "";
	invokeCommonIBLogger("FORM ID IS " + formId.toString());
	segAboutMeLoad();
	enableOTPReqButton();
	switch (parseInt(formId.toString())) {
	case 1:
		handleLocalChangeRcHomeScreen();
		break;
	case 2:
		handleLocaleChangeRcAddBankAccountScreen();
		break;
	case 3:
		handleLocaleChangeRcAddContactFbScreen();
		break;
	case 4:
		handleLocaleChangeRcAddContactManuallyScreen();
		break;
	case 5:
		handleLocaleChangeRcEditAccountConfScreen();
		break;
	case 6:
		handleLocaleChangeRcAccountsScreen();
		break;
	case 7:
		handleLocaleChangeRcFbContactConfScreen();
		break;
	case 8:
		handleLocaleChangeRcAddContactManuallyConfScreen();
		break;
	default:
		break;
	}
	if (gblRC_QA_TEST_VAL == 0) {
		
	} else {
		invokeCommonIBLogger("REFRESH CACHE ????? " + gblRefreshRcCache);
		//Invoke service to check the lock status
		startRCLockStatusQueryService();
		if (isCachedRcListAvailable() && gblRefreshRcCache == false) {
			//For Recipients home form always refresh the data from service
			if (parseInt(form.toString()) == 1) {
				startReceipentListingService(gblcrmId, form);
			} else {
				resetRcSegData("1", form);
				checknSetUIforRcCountLimit(form);
			}
		} else {
			startReceipentListingService(gblcrmId, form);
		}
		//Load & cache the bank list here
		if (parseInt(form.toString()) == 1) {
			if (isCachedSelectBankListAvailable() == false) {
				startDisplaySelectbankCacheService();
			}
		}
	}
}
/**
 * Method to check th module locally or after service integration dynamically
 * @returns {}
 */

function setDatatoReceipentBankList() {
	checkRcProfileDataVisibility();
	startgetBankAccntforRCService();
}
/**
 * Method to check the module locally or after service integration dynamically
 * @returns {}
 */

function deleteReceipentfromList() {
	gblRefreshRcCache = true;
	startRcDeleteService();	
}


function cacheFBFullList(serviceData) {
	for (var i = 0; i < serviceData.length; i++) {
		var name = serviceData[i]["name"];
		if (name == null || name.toString()
			.length == 0) {
			name = "Unknown User";
		}
		var segDataMap = ["chkboxnon.png", serviceData[i]["imageurl"], name, "No Number",
						  "No Mail", serviceData[i]["username"], serviceData[i]["username"]];
		globalFBCacheData.push(segDataMap);
	}
}
/**
 * Method to check the module locally or after service integration dynamically
 * @returns {}
 */

function confirmBankAccntEdit() { 

	if (gblAdd_Receipent_State == gblEXISTING_RC_EDITION) {
			gblRefreshRcCache = true;
			startExistingRcProfileEditService();
		} else if (gblAdd_Receipent_State == gblEXISTING_RC_BANKACCNT_ADDITION) {
			if (frmIBMyReceipentsAddBankAccnt.segementTobeAddedAccnts.data.length == 0) {
				alert("" + kony.i18n.getLocalizedString("Receipent_alert_atleastonebankaccount"));
			} else {
				gblRefreshRcCache = true;
				startExistingRcAccountAddService();
			}
		} else if (gblAdd_Receipent_State == gblEXISTING_RC_BANKACCNT_EDITION) {
			editReceipentAccount(frmIBMyReceipentsAddBankAccnt.comboBankName.selectedKey[0], frmIBMyReceipentsAddBankAccnt.tbxAddAccntNumber
				.text,
				frmIBMyReceipentsAddBankAccnt.tbxAddAccntNickName.text);
		} else if (gblAdd_Receipent_State == gblNEW_RC_ADDITION || gblAdd_Receipent_State == gblNEW_RC_ADDITION_PRECONFIRM) {
			gblRefreshRcCache = true;
			startNewRcAddwithAccntService();
		} else {
			startExistingRcProfileEditService();
			startExistingRcAccountEditService();
		}
		
}
/**
 * Method to check the module locally or after service integration dynamically
 * @returns {}
 */

function confirmNewRcAdd() {
	invokeCommonIBLogger("ADD RECEIPENT STATE -- confirmNewRcAdd" + gblAdd_Receipent_State);
	if (gblRC_QA_TEST_VAL == 0) {} else {
		if (gblAdd_Receipent_State != gblEXISTING_RC_EDITION) {
			invokeCommonIBLogger("######User addition state   startNewOnlyRecipientAddService" + gblAdd_Receipent_State);
			if (frmIBMyReceipentsAddContactManually.segmentRcAccounts.data == null || frmIBMyReceipentsAddContactManually.segmentRcAccounts
				.data == "") {
				if (checkForRcExistenceinCrm() == false) {
					startNewOnlyRecipientAddService();
				}
			} else {
				invokeCommonIBLogger("######User addition state startNewRecipientPlusAccountAddService" + gblAdd_Receipent_State);
				if (gblAdd_Receipent_State == gblNEW_RC_ADDITION_PRECONFIRM ||
					gblAdd_Receipent_State == gblNEW_RC_ADDITION) {
					if (checkForRcExistenceinCrm() == false) {
						//THIS IS A WORKAROUND----NEED TO REVISIT THIS IN JAVA SERVICE
						startNewRecipientPlusAccountAddService();
						//startNewRcAddwithAccntService();
					}
				} else {
					startExistingRcAccountEditService();
				}
			}
		} else {
			if (checkForRcExistenceinCrm() == false) {
				startExistingRcProfileEditService();
			}
		}
	}
}
/**
 * Method to check the module locally or after service integration dynamically
 * @returns {}
 */

function addFBUsers() {
	
		startNewRcFBListAddService();
}
/**
 * Method to check the module locally or after service integration dynamically
 * @returns {}
 */

function listFacebookFriends() {
		offset = 0;
		if (gblRcValidateAccnt) {
			frmIBMyReceipentsAddContactFB.linkMore.setVisibility(true);
			//frmIBMyReceipentsAddContactFB.segmentSelectRc.removeAll();
			checkAndInvokeFbFriendsListingService();
		} else {
			alert("Exception Rc");
		}
}
/**
 * Method to check the module locally or after service integration dynamically
 * @returns {}
 */

function listAvailableBanksforSelection() {
	if (gblAdd_Receipent_State == gblEXISTING_RC_BANKACCNT_EDITION) {
		//Account text box & combo box should be non editable
		frmIBMyReceipentsAddBankAccnt.tbxAddAccntNumber.setEnabled(false);
		frmIBMyReceipentsAddBankAccnt.comboBankName.setEnabled(false);
		frmIBMyReceipentsAddBankAccnt.tbxAddAccntNumber.setVisibility(false);
		frmIBMyReceipentsAddBankAccnt.comboBankName.setVisibility(false);
		frmIBMyReceipentsAddBankAccnt.hboxAddAccntNumber.setVisibility(false);
		frmIBMyReceipentsAddBankAccnt.line101000183142460.setVisibility(false);
		frmIBMyReceipentsAddBankAccnt.line101000183142691.setVisibility(false);
	} else {
		frmIBMyReceipentsAddBankAccnt.tbxAddAccntNumber.setEnabled(true);
		frmIBMyReceipentsAddBankAccnt.comboBankName.setEnabled(true);
		frmIBMyReceipentsAddBankAccnt.tbxAddAccntNumber.setVisibility(true);
		frmIBMyReceipentsAddBankAccnt.comboBankName.setVisibility(true);
		frmIBMyReceipentsAddBankAccnt.hboxAddAccntNumber.setVisibility(true);
	//	frmIBMyReceipentsAddBankAccnt.line101000183142460.setVisibility(true);
		// Commented above line and added below line for Def9734 for customized combo box
		frmIBMyReceipentsAddBankAccnt.line101000183142460.setVisibility(false);
		frmIBMyReceipentsAddBankAccnt.line101000183142691.setVisibility(true);
	}
	if (gblRC_QA_TEST_VAL == 0) {
		setBankDatatoComboBox();
	} else {
		if (gblAdd_Receipent_State == gblEXISTING_RC_BANKACCNT_EDITION) {
			var masterData = [
				["Bank1", frmIBMyReceipentsAccounts.lblBankNameAccntDetails.text.toString()]
			];
			frmIBMyReceipentsAddBankAccnt.comboBankName.masterData = masterData;
		} else {
			if (isCachedSelectBankListAvailable()) {
				setAvailableBanksFromCacheToCombo();
			} else {
				startDisplaySelectbankService();
			}
		}
	}
}
/**
 * Method to check the module locally or after service integration dynamically
 * @returns {}
 */

function deleteReceipentAccount(bankcode, accountId, nickname) {
	if (gblRC_QA_TEST_VAL == 0) {
		deleteAccount();
	} else {
		startExistingRcAccountEditService(gblSelectedAccountCode, accountId, nickname,frmIBMyReceipentsAccounts.lblAcctNameAccntDetails.text, "Deleted");
	}
}
/**
 * Method to check the module locally or after service integration dynamically
 * @returns {}
 */

function editReceipentAccount(bankcode, accountId, nickname,accName) {
	if (gblRC_QA_TEST_VAL == 0) {
		//Not implemented
	} else {
		startExistingRcAccountEditService(gblSelectedAccountCode, accountId, nickname, accName, "Added");
	}
}
/**
 * Method to check the module locally or after service integration dynamically
 * @returns {}
 */

function setReceipentAccountasFav(bankCode, accountId, nickName, isFav) {
	if (gblRC_QA_TEST_VAL == 0) {
		//No support
	} else {
		var fav = frmIBMyReceipentsAccounts.segmentRcAccounts.selectedItems[0]["FavoriteAcct"];
		if (fav != null) {
			if (fav.toString() == "y" || fav.toString() == "Y") {
				startExistingRcAccountSetFavService(bankCode, accountId, nickName, "N");
			} else {
				startExistingRcAccountSetFavService(bankCode, accountId, nickName, "Y");
			}
		} else {
			startExistingRcAccountSetFavService(bankCode, accountId, nickName, "Y");
		}
	}
}

/**
 * Method to check the module locally or after service integration dynamically
 * @returns {}
 */

function validateOTP(form) {
	if (gblRC_QA_TEST_VAL == 0) {
	} else {
		if (gblRcValidateAccnt) {
			if (form == frmIBMyReceipentsAddContactManually) {
				if (gblAdd_Receipent_State == gblNEW_RC_ADDITION || gblAdd_Receipent_State == gblNEW_RC_ADDITION_PRECONFIRM || gblAdd_Receipent_State == gblEXISTING_RC_EDITION) {
					if (frmIBMyReceipentsAddContactManually.txtotp.text == null ||
						frmIBMyReceipentsAddContactManually.txtotp.text == "") {
						if(gblTokenSwitchFlag)
						alert("" + kony.i18n.getLocalizedString("Receipent_tokenId"));
						else
						alert("" + kony.i18n.getLocalizedString("Receipent_alert_correctOTP"));
					} else {
						var otptext = frmIBMyReceipentsAddContactManually.txtotp.text;
						validateOTPtext(otptext);
					}
				} else {
			 if(!gblTokenSwitchFlag){
				if (frmIBMyReceipentsAddContactManually.textbox247428873338513.text == null || frmIBMyReceipentsAddContactManually.textbox247428873338513
					.text == "") {
					alert("" + kony.i18n.getLocalizedString("Receipent_alert_correctOTP"));
					return false;
				} 
			  }else if(gblTokenSwitchFlag){
				if (frmIBMyReceipentsAddContactManually.txtToken.text == null || frmIBMyReceipentsAddContactManually.txtToken
					.text == "") {
					alert("" + kony.i18n.getLocalizedString("Receipent_tokenId"));
					return  false;
				} 
			  }  
			
						var otptext = frmIBMyReceipentsAddContactManually.textbox247428873338513.text;
						validateOTPtext(otptext);
					
				}
			} else if (form == frmIBMyReceipentsAddContactFB) {
				if (frmIBMyReceipentsAddContactFB.textbox247428873338513.text == null ||
					frmIBMyReceipentsAddContactFB.textbox247428873338513.text == "") {
					if(gblTokenSwitchFlag)
					alert("" + kony.i18n.getLocalizedString("Receipent_tokenId"));
					else
					alert("" + kony.i18n.getLocalizedString("Receipent_alert_correctOTP"));
				} else {
					var otptext = frmIBMyReceipentsAddContactFB.textbox247428873338513.text;
					validateOTPtext(otptext);
				}
			} else if (form == frmIBMyReceipentsAddBankAccnt) {
				if (frmIBMyReceipentsAddBankAccnt.textbox247428873338513.text == null ||
					frmIBMyReceipentsAddBankAccnt.textbox247428873338513.text == "") {
					if(gblTokenSwitchFlag)
					alert("" + kony.i18n.getLocalizedString("Receipent_tokenId"));
					else
					alert("" + kony.i18n.getLocalizedString("Receipent_alert_correctOTP"));
				} else {
					var otptext = frmIBMyReceipentsAddBankAccnt.textbox247428873338513.text;
					validateOTPtext(otptext);
				}
			}
		}
	}
}
/**
 * Method to set the receipent as favorite or vice versa
 * @returns {}
 */

function setReceipentAsFavorite() {
	if (gblRC_QA_TEST_VAL == 0) {
		//No support
	} else {
		curr_form = kony.application.getCurrentForm();
		var fav = curr_form.segmentReceipentListing.selectedItems[0]["Favorite"];
		var receipentId = curr_form.segmentReceipentListing.selectedItems[0]["receipentID"];
		var rcname = curr_form.segmentReceipentListing.selectedItems[0]["lblReceipentName"];
		var pic = curr_form.segmentReceipentListing.selectedItems[0]["imgReceipentPic"];
		var mailid = curr_form.segmentReceipentListing.selectedItems[0]["emailId"];
		var mobNum = curr_form.segmentReceipentListing.selectedItems[0]["mobileNumber"];
		var fbId = curr_form.segmentReceipentListing.selectedItems[0]["facebookId"];
		invokeCommonIBLogger("Favorite status retrieved from segment is " + fav);
		if (fav != null) {
			if (fav.toString() == "y" || fav.toString() == "Y") {
				startRcSetFavService(gblcrmId, receipentId, "N", rcname, pic, mailid, mobNum, fbId);
			} else {
				startRcSetFavService(gblcrmId, receipentId, "Y", rcname, pic, mailid, mobNum, fbId);
			}
		} else {
			startRcSetFavService(gblcrmId, receipentId, "Y", rcname, pic, mailid, mobNum, fbId);
		}
	}
}
/**
 * Method to validate the OTP
 * @param {} otptext
 * @returns {}
 */

function validateOTPtext(otptext) {
	startOTPValidationService(otptext);
}

/**
 * Method to call Crm profile inq and check if to do cache refresh or not?
 * @returns {}
 */

function doRcCacheRefresh() {
	if (gblRC_QA_TEST_VAL == 0) {
		//No support now
	} else {
		if (gblRcValidateAccnt) {
			//startIBRcCrmProInqService();
		} else {
			//No support
		}
	}
}

/**
 * Method to check and display the rc profile data
 * @returns {}
 */

function checkRcProfileDataVisibility() {
	if (frmIBMyReceipentsAccounts.lblRcMobileNo.text == "") {
		frmIBMyReceipentsAccounts.hboxRcMobile.setVisibility(false);
	} else {
		frmIBMyReceipentsAccounts.hboxRcMobile.setVisibility(true);
	}
	if (frmIBMyReceipentsAccounts.lblRcEmail.text == "") {
		frmIBMyReceipentsAccounts.hboxEmail.setVisibility(false);
	} else {
		frmIBMyReceipentsAccounts.hboxEmail.setVisibility(true);
	}
	if (frmIBMyReceipentsAccounts.lblRcFbId.text == "") {
		frmIBMyReceipentsAccounts.hboxFbId.setVisibility(false);
	} else {
		frmIBMyReceipentsAccounts.hboxFbId.setVisibility(true);
	}
}

var gblRcIBFBState = null;
var gblFBListDataIB = [];
/**
 * Method to set the data retrieved from FB frnds listing service to segment
 * @returns {}
 */

function getnSetDataToFBListing(getFriends) {
    var tempBackUp = getFriends;
    var totalData = [];
    var len = getFriends["NewResult"].length;
    if (gblStart == "0") {
        if (len > 0) {
            gblFBFriendsAvailability = true;
        } else {
            gblFBFriendsAvailability = false;
        }
    }
    if (len > 0) { //friendlist is returned
        if (getFriends["flag"] != "0") { //no more friends
            if (getFriends["flag"] == "1") {
                //len = len - 1;
                frmIBMyReceipentsAddContactFB.linkMore.setVisibility(false);
            }
        } else { //more friends available
            frmIBMyReceipentsAddContactFB.linkMore.setVisibility(true);
        }
        if (gblLimitMB == "0") {
            frmIBMyReceipentsAddContactFB.linkMore.setVisibility(false);
        }
        frmIBMyReceipentsAddContactFB.btnSelectRcNext.setVisibility(true);
        frmIBMyReceipentsAddContactFB.lblFbNoFriends.setVisibility(false);
        if (frmIBMyReceipentsAddContactFB.id == "frmMyRecipientSelectFacebook")
            select = "chkbox2.png";
        else {
            select = "radiobtn2.png";
        }
        for (var i = 0; i < len; i++) {
            //tempData.push({
            //							"imgprofilepic" : getFriends["NewResult"][i1]["picture"],
            //							"lblName": getFriends["NewResult"][i1]["name"],
            //							"lblMobile": getFriends["NewResult"][i1]["mobile"],
            //							"lblUsername": getFriends["NewResult"][i1]["username"],
            //							"imgchbxbtn": select
            //						});

            var segDataMap = {
                imgSelected: "chkboxnon.png",
                imgSelectRcProfilePic: getFriends["NewResult"][i]["picture"],
                lblSelectRcName: getFriends["NewResult"][i]["name"],
                lblSelectRcNumber: getFriends["NewResult"][i]["mobile"],
                lblSelectRcEmail: getFriends["NewResult"][i]["mobile"],
                lblSelectRcFbId: getFriends["NewResult"][i]["id"],
                facebookId: getFriends["NewResult"][i]["id"]
            };
            totalData.push(segDataMap);
            if(gblStart == "0")
            	gblFBListDataIB.push(segDataMap);

        }
        //frmIBMyReceipentsAddContactFB.segMyRecipient.data = tempData;
		if(gblMORE == true){
			frmIBMyReceipentsAddContactFB.segmentSelectRc.addAll(totalData);
			gblMORE = false;
		}
		else{
			frmIBMyReceipentsAddContactFB.segmentSelectRc.removeAll();
	        frmIBMyReceipentsAddContactFB.segmentSelectRc.setData(totalData);
		}
	        dismissLoadingScreenPopup();
		
        //frmIBMyReceipentsAddContactFB.show();
    } else { //friendlist not returned
        frmIBMyReceipentsAddContactFB.segmentSelectRc.removeAll();
        dismissLoadingScreenPopup();
        if (gblFBFriendsAvailability == true) {
            frmIBMyReceipentsAddContactFB.linkMore.setVisibility(false);
            frmIBMyReceipentsAddContactFB.lblFbNoFriends.setVisibility(true);

        } else {
            frmIBMyReceipentsAddContactFB.linkMore.setVisibility(false);
            frmIBMyReceipentsAddContactFB.lblFbNoFriends.setVisibility(true);

        }
         frmIBMyReceipentsAddContactFB.btnSelectRcNext.setVisibility(false);
    }


    //frmIBMyReceipentsAddContactFB.segmentSelectRc.setData(totalData);
}
/**
 * Method to set the to be added FB contacts as RC for confirmation
 * @returns {}
 */

function setTobeAddedFBContactsforRC() {
	var totalData = [];
	var arraylength = frmIBMyReceipentsAddContactFB.segmentSelectRc.selectedIndices;
	if (arraylength == null || arraylength[0][1] == null || arraylength[0][1].length == 0) {
		if (gblfbSelectionState == gblFB_SINGLE_SELECTION) {
		frmIBMyReceipentsAddContactManually.show();		
		}
		else{
		alert(kony.i18n.getLocalizedString("Receipent_alert_selectone"));
		return;
		}
	}
	if (gblfbSelectionState == gblFB_SINGLE_SELECTION) {
		if (arraylength[0][1].length > 1) {
			alert(kony.i18n.getLocalizedString("Receipent_alert_selectlessthanfive").replace("<1>","1"));
			return;
		}
	} else {
		if (arraylength[0][1].length > MAX_RECIPIENT_BULK_COUNT) {
			alert(kony.i18n.getLocalizedString("Receipent_alert_selectlessthanfive").replace("<1>",MAX_RECIPIENT_BULK_COUNT));
			return;
		}
		else{
			for(var i = 0; i < frmIBMyReceipentsAddContactFB.segmentSelectRc.selectedItems.length; i++){
				for(var j = 0; j < frmIBMyReceipentsAddContactFB.segmentReceipentListing.data.length; j++){
					if(frmIBMyReceipentsAddContactFB.segmentReceipentListing.data[j].lblReceipentName.replace(/^\s+|\s+$/g, "") == frmIBMyReceipentsAddContactFB.segmentSelectRc.selectedItems[i].lblSelectRcName.replace(/^\s+|\s+$/g, "")){
						alert(kony.i18n.getLocalizedString("RecipientExists"));
						return;
					}
				}
			}
		}
	}
	if (gblfbSelectionState != gblFB_SINGLE_SELECTION) {
		isAddFb=true;
		frmIBMyReceipentsAddContactFB.hboxSelectrcpntsConf.setVisibility(true);
		frmIBMyReceipentsAddContactFB.hboxTMBSelectRcpnts.setVisibility(false);
		for (var i = 0; i < (arraylength[0][1].length); i++) {
			var segDataMap = {
				imgSelectRcProfilePic: frmIBMyReceipentsAddContactFB.segmentSelectRc.selectedItems[i]["imgSelectRcProfilePic"],
				lblSelectRcName: frmIBMyReceipentsAddContactFB.segmentSelectRc.selectedItems[i]["lblSelectRcName"],
				lblSelectRcNumber: frmIBMyReceipentsAddContactFB.segmentSelectRc.selectedItems[i]["lblSelectRcNumber"],
				lblSelectRcEmail: frmIBMyReceipentsAddContactFB.segmentSelectRc.selectedItems[i]["lblSelectRcEmail"],
				facebookId: frmIBMyReceipentsAddContactFB.segmentSelectRc.selectedItems[i]["facebookId"],
				lblSelectRcFbId: frmIBMyReceipentsAddContactFB.segmentSelectRc.selectedItems[i]["facebookId"]
			};
			totalData.push(segDataMap);
			var tempData = [];
			tempData = [frmIBMyReceipentsAddContactFB.segmentSelectRc.selectedItems[i]["imgSelectRcProfilePic"],
				frmIBMyReceipentsAddContactFB.segmentSelectRc.selectedItems[i]["lblSelectRcName"],
				frmIBMyReceipentsAddContactFB.segmentSelectRc.selectedItems[i]["lblSelectRcNumber"],
				frmIBMyReceipentsAddContactFB.segmentSelectRc.selectedItems[i]["lblSelectRcEmail"],
				frmIBMyReceipentsAddContactFB.segmentSelectRc.selectedItems[i]["facebookId"]
			];
			globalTobeAddedFBDataConfirmation.push(tempData);
		}
		frmIBMyReceipentsAddContactFB.segmentSelectRcConf.setData(totalData);
		gblAdd_Receipent_State = gblNEW_RC_ADDITION_PRECONFIRM;
		clearOTP();
	} else {
		frmIBMyReceipentsAddContactManually.show();
		frmIBMyReceipentsAddContactManually.image210107168135.src = frmIBMyReceipentsAddContactFB.segmentSelectRc.selectedItems[
			0]["imgSelectRcProfilePic"];
		frmIBMyReceipentsAddContactManually.txtRecipientName.text = frmIBMyReceipentsAddContactFB.segmentSelectRc.selectedItems[
			0]["lblSelectRcName"];
		frmIBMyReceipentsAddContactManually.txtmobnum.text = frmIBMyReceipentsAddContactFB.segmentSelectRc.selectedItems[0][
			"lblSelectRcNumber"];
		frmIBMyReceipentsAddContactManually.txtemail.text = frmIBMyReceipentsAddContactFB.segmentSelectRc.selectedItems[0][
			"lblSelectRcEmail"];
		frmIBMyReceipentsAddContactManually.label10107168132696.isVisible = true;
		frmIBMyReceipentsAddContactManually.lblfbidstudio19.isVisible = true;//Modified by Studio Viz
		frmIBMyReceipentsAddContactManually.lblFbIDstudio18.isVisible = true;//Modified by Studio Viz
		frmIBMyReceipentsAddContactManually.hbox104300825096149.setVisibility(true)
		frmIBMyReceipentsAddContactManually.lblfbidstudio19.text = frmIBMyReceipentsAddContactFB.segmentSelectRc.selectedItems[0][//Modified by Studio Viz
			"lblSelectRcFbId"];
		frmIBMyReceipentsAddContactManually.tbxFbID.text = frmIBMyReceipentsAddContactFB.segmentSelectRc.selectedItems[0][
			"lblSelectRcFbId"];
	}
}
/**
 * Method to set the to be saved bank data to segment
 * @returns {}
 */

function setToAddDatatoFBConfirmationSeg() {
	var totalData = [];
	var tempSegData = globalFBConfData;
	for (var i = 0; i < tempSegData.length; i++) {
		var segDataMap = {
			imgSelectRcProfilePic: tempSegData[i][1],
			lblSelectRcName: tempSegData[i][0],
			lblSelectRcNumber: tempSegData[i][3],
			lblSelectRcEmail: tempSegData[i][2],
			lblSelectRcFbId: tempSegData[i][4]
		};
		totalData.push(segDataMap);
		/*if (tempSegData[i][3] == "No Number" && tempSegData[i][2] == "No mail") {
			activityLogServiceCall("045", "", "01", "", "Add", tempSegData[i][0], "", "", tempSegData[i][4], "");
		} else {
			activityLogServiceCall("045", "", "01", "", "Add", tempSegData[i][0], tempSegData[i][3], tempSegData[i][2],tempSegData[i][4], "");
		}
		startRcNotificationService("Added Recipients", "One or more Recipients have been added", tempSegData[i][0], null,
//			tempSegData[i][1], "", "recipientsAdd");*/
	}
	//startRcNotificationService("Added Recipients", "One or more Recipients have been added", "", null,"", "", "recipientsAdd");	
	frmIBMyReceipentsFBContactConf.segementTobeAddedFBConf.setData(totalData);
}
/**
 * Method to prepare the new rc list from FB
 * @returns {}
 */

function prepareNewRcFBList() {
	if (globalFBConfData != null) {
		for (var i = 0; i < globalFBConfData.length; i++) {
			globalFBConfData.pop();
		}
		globalFBConfData = [];
	}
	var tempData = [];
	tempData.push(frmIBMyReceipentsAddContactFB.segmentSelectRcConf.data);
	var totalData = [];
	for (var i = 0; i < tempData[0].length; i++) {
		var Name = tempData[0][i].lblSelectRcName;
		var Number = tempData[0][i].lblSelectRcNumber;
		var Email = tempData[0][i].lblSelectRcEmail;
		var fbId = tempData[0][i].facebookId;
		var picId = tempData[0][i].imgSelectRcProfilePic;
		//var picId = "nouserimg.jpg";
		if (checkForRcExistenceinCrmforFBFriend(tempData[0][i].lblSelectRcName))
			continue;
//		var fbData = [Name, picId, Email, Number, fbId, "Added", "N"];
//		totalData.push(fbData);
        var fbData = Name+ "," +picId+ "," + Email+ "," +Number+ "," +fbId+ "," + "Added"+ "," + "N";
		globalFBConfData.push(fbData);
	}
	//invokeCommonIBLogger("#######FB list#########" + totalData);
	return totalData;
}
/**
 * Method to check for FB friends presence in CRM
 * @returns {}
 */

function checkForRcExistenceinCrmforFBFriend(friendName) {
	if (globalRcData[0] != null) {
		for (var i = 0; i < globalRcData[0].length; i++) {
			var srcRcName = globalRcData[0][i].lblReceipentName;
			var targetRcName = friendName;
			invokeCommonIBLogger("SRC RC NAME: " + srcRcName.toString());
			invokeCommonIBLogger("TARGET RC NAME: " + targetRcName.toString());
			check = kony.string.equalsIgnoreCase(srcRcName.toString(), targetRcName.toString());
			if (check) {
				break;
			}
		}
		if (check) {
			alert("" + kony.i18n.getLocalizedString("Recipient_Friend_NotAdded"));
			return true;
		} else {
			//New receipent..carry on
			return false;
		}
	} else {
		return false;
	}
}
/**
 * Method to navigate out of facebook screen
 * @returns {}
 */

function outOfFacebookScreen() {
	frmIBMyReceipentsHome.show();
	TMBUtil.DestroyForm(frmIBMyReceipentsAddContactFB);
}


/**
 * Method to set the to be saved bank data to segment
 * @returns {}
 */

function setToAddDatatoManualConfirmationSeg() {
	var tempData = [];
	tempData.push(frmIBMyReceipentsAddContactManually.segmentRcAccounts.data);
	var totalData = [];
	var textbtnTransfer = {
								text: kony.i18n.getLocalizedString("Transfer"),
								skin: "btnIB80px"
								};
	if (tempData[0] != null) {
		for (var i = 0; i < tempData[0].length; i++) {
			var segDataMap;
			if (tempData[0][i].lblAccountName != null && tempData[0][i].lblAccountName != "") {
				segDataMap = {
					lblBankTitle: tempData[0][i].lblBankTitle,
					lblNumberTitle: tempData[0][i].lblNumberTitle,
					lblNickNameTitle: tempData[0][i].lblNickNameTitle,
					btnTransfer: textbtnTransfer,
					lblBankName: tempData[0][i].lblBankName,
					lblNumber: tempData[0][i].lblNumber,
					lblNickName: tempData[0][i].lblNickName,
					lblAccountNameTitle: kony.i18n.getLocalizedString("AccountName"),
					lblAccountName: tempData[0][i].lblAccountName,
					bankLogo: tempData[0][i].bankLogo,
					bankCode:tempData[0][i].bankCode
				};
			} else {
				segDataMap = {
					lblBankTitle: tempData[0][i].lblBankTitle,
					lblNumberTitle: tempData[0][i].lblNumberTitle,
					lblNickNameTitle: tempData[0][i].lblNickNameTitle,
					btnTransfer: textbtnTransfer,
					lblBankName: tempData[0][i].lblBankName,
					lblNumber: tempData[0][i].lblNumber,
					lblNickName: tempData[0][i].lblNickName,
					bankLogo: tempData[0][i].bankLogo,
					bankCode:tempData[0][i].bankCode
				};
			}
			totalData.push(segDataMap);
			/*activityLogServiceCall("046", "", "01", "", "Added Recipient account", tempData[0][i].lblNickName, tempData[0][i].lblBankName,
				tempData[0][i].lblNumber, "", "");*/
		}
		frmIBMyReceipentsAddContactManuallyConf.segmentRcAccounts.setData(totalData);
		if (frmIBMyReceipentsAddContactManuallyConf.segmentRcAccounts.data.length > 1) {
			frmIBMyReceipentsAddContactManuallyConf.label10107168132952.text = kony.i18n.getLocalizedString("Receipent_Accounts");
		} else {
			frmIBMyReceipentsAddContactManuallyConf.label10107168132952.text = kony.i18n.getLocalizedString("Receipent_Account");
		}
	}
	destroyfrmIBMyReceipentsAddContactManually();
}
/**
 * Method to fire timer to destrpy required forms
 * @returns {}
 */

function destroyfrmIBMyReceipentsAddContactManually() {
	try {
		kony.timer.cancel("102");
	} catch (e) {
		// todo: handle exception
	}
	invokeCommonIBLogger("FIRE TIMER .....");
	kony.timer.schedule("102", destroyfrmIBMyReceipentsAddContactManuallyCallback, 1, false);
}
/**
 * Timer callback
 * @returns {}
 */

function destroyfrmIBMyReceipentsAddContactManuallyCallback() {
	invokeCommonIBLogger("TIMER CALLBACK.....");
	try {
		TMBUtil.DestroyForm(frmIBMyReceipentsAddContactManually);
		TMBUtil.DestroyForm(frmIBMyReceipentsAddBankAccnt);
	} catch (e) {
		// todo: handle exception
	}
}
/**
 * Cancelling the otpTimer
 * @returns {}
 */

function OTPTimercallbackRc() {
	if (gblAdd_Receipent_State == gblEXISTING_RC_EDITION || gblAdd_Receipent_State == gblNEW_RC_ADDITION || gblAdd_Receipent_State == gblNEW_RC_ADDITION_PRECONFIRM) {
		
		if(isAddFb){
			frmIBMyReceipentsAddContactFB.btnOTPReq.skin = btn100;
			frmIBMyReceipentsAddContactFB.btnOTPReq.focusSkin = btn100;
			frmIBMyReceipentsAddContactFB.btnOTPReq.setEnabled(true);
		}else{
			frmIBMyReceipentsAddContactManually.btnotp.skin = btn100;
			frmIBMyReceipentsAddContactManually.btnotp.focusSkin = btn100;
			frmIBMyReceipentsAddContactManually.btnotp.setEnabled(true);
			}
	} else if (gblAdd_Receipent_State == gblEXISTING_RC_BANKACCNT_ADDITION || gblAdd_Receipent_State ==
		gblEXISTING_RC_BANKACCNT_EDITION) {
		frmIBMyReceipentsAddBankAccnt.btnOTPReq.skin = btn100;
		frmIBMyReceipentsAddBankAccnt.btnOTPReq.focusSkin = btn100;
		frmIBMyReceipentsAddBankAccnt.btnOTPReq.setEnabled(true);
	} 
	
	if(gblRetryCountRequestOTP>=gblOTPReqLimit)
	{
		if (gblAdd_Receipent_State == gblEXISTING_RC_EDITION || gblAdd_Receipent_State == gblNEW_RC_ADDITION || gblAdd_Receipent_State == gblNEW_RC_ADDITION_PRECONFIRM) {
			if(isAddFb){
				frmIBMyReceipentsAddContactFB.btnOTPReq.skin = btn100Disabled;
				frmIBMyReceipentsAddContactFB.btnOTPReq.focusSkin = btn100Disabled;
				frmIBMyReceipentsAddContactFB.btnOTPReq.setEnabled(false);
			}else{
				frmIBMyReceipentsAddContactManually.btnotp.skin = btn100Disabled;
				frmIBMyReceipentsAddContactManually.btnotp.focusSkin = btn100Disabled;
				frmIBMyReceipentsAddContactManually.btnotp.setEnabled(false);
				}
		} else if (gblAdd_Receipent_State == gblEXISTING_RC_BANKACCNT_ADDITION || gblAdd_Receipent_State ==
			gblEXISTING_RC_BANKACCNT_EDITION) {
			frmIBMyReceipentsAddBankAccnt.btnOTPReq.skin = btn100Disabled;
			frmIBMyReceipentsAddBankAccnt.btnOTPReq.focusSkin = btn100Disabled;
			frmIBMyReceipentsAddBankAccnt.btnOTPReq.setEnabled(false);
		} 
	}
	try {
		kony.timer.cancel("OtpTimer");
	} catch (e) {
		
	}
}



function saveRecipientDeatils(){
	recipientTokenExchangeService();
}

function saveRecipientDeatilsCallBack(status,results){
	if(status==400){
	dismissLoadingScreenPopup();
		if(results["opstatus"] == 1){
			alert(results["errMsg"]);
			return false;
		}else{
		dismissLoadingScreenPopup();
		
		onEditReceipentNextIB();
		}
	}
}


/*function getNewAccountNumebrsListExistingRc() {
	var tempData = [];
	tempData.push(frmIBMyReceipentsAddBankAccnt.segementTobeAddedAccnts.data);
	var totalData ="";
	for (var i = 0; i < tempData[0].length; i++) {
		var BankName = tempData[0][i].lblBankName;
		totalData =totalData+decodeAccountNumbers(tempData[0][i].lblNumber);
	}
	invokeCommonIBLogger("#######Account list#########" + totalData);
	return totalData;
}*/
/*function getNewAccountListNewRc() {
	var tempData = [];
	tempData.push(frmIBMyReceipentsAddContactManually.segmentRcAccounts.data);
	var totalData = "";
	for (var i = 0; i < tempData[0].length; i++) {
		totalData=totalData+ decodeAccountNumbers(tempData[0][i].lblNumber);
	}
	invokeCommonIBLogger("#######Account list#########" + totalData);
	return totalData;
}*/

function getRecipientsOtpParams(parameter) {
    var locale = kony.i18n.getCurrentLocale();
    var eventNotificationPolicy;
    var SMSSubject;
    var Channel;
    var AccntDetailsMessage;
    var AccountName;
    var fbRc = [];
    if (gblAdd_Receipent_State == gblEXISTING_RC_EDITION || gblAdd_Receipent_State == gblEXISTING_RC_BANKACCNT_ADDITION) {
    	/*if (locale == "en_US") {
            eventNotificationPolicy = "MIB_EditRecipient_EN";
            SMSSubject = "MIB_EditRecipient_EN";
        } else {
            eventNotificationPolicy = "MIB_EditRecipient_TH";
            SMSSubject = "MIB_EditRecipient_TH";
        }*/
        Channel = "EditRecipient";
        AccountName = frmIBMyReceipentsAddBankAccnt.lblRcName.text;
    } else if (gblAdd_Receipent_State == gblNEW_RC_ADDITION || gblAdd_Receipent_State == gblNEW_RC_ADDITION_PRECONFIRM) {
    	/*if (locale == "en_US") {
            eventNotificationPolicy = "MIB_AddRecipient_EN";
            SMSSubject = "MIB_AddRecipient_EN";
        } else {
            eventNotificationPolicy = "MIB_AddRecipient_TH";
            SMSSubject = "MIB_AddRecipient_TH";
        }*/
        Channel = "AddRecipient";
        AccountName=frmIBMyReceipentsAddContactManually.lblrecipientname.text
    }
    //AccountName = frmIBMyReceipentsAddBankAccnt.lblRcName.text;
    var em = frmIBMyReceipentsAddContactManually.txtemail.text;
    var ph = frmIBMyReceipentsAddContactManually.txtmobnum.text;
    var fbid = frmIBMyReceipentsAddContactManually.lblfbidstudio19.text;//Modified by Studio Viz
    if (em == "Not Available" || em == "" || em == undefined || em == null || em == "No Mail") {
        em = "";
    } else {
        em = "Email:" + em + ". ";
    }
    if (ph == "Not Available" || ph == null || ph == undefined || ph == "" || ph == "No Number") {
        ph = "";
    } else {
        ph = removeHyphenIB(ph);
        ph = "Mobile Phone No.= xxx-xxx-" + ph.substring(6, 10) + ". ";
    }
    if (fbid == "Not Available" || fbid == null || fbid == undefined || fbid == "") {
        fbid = "";
    } else {
        fbid = "Facebook:" + fbid + " ";
    }
    if (gblAdd_Receipent_State == gblEXISTING_RC_EDITION) {
        AccntDetailsMessage = "";
        AccountName = frmIBMyReceipentsAddContactManually.txtRecipientName.text;
        //CR 13 change start
        var fbId = frmIBMyReceipentsAddContactManually.tbxFbID.text;
        var mobNo = frmIBMyReceipentsAddContactManually.txtmobnum.text;
        
        
          if (mobNo == null || mobNo == undefined || mobNo == "") {
            
         }else{
		  AccntDetailsMessage = "Mobile x" + mobNo.substring(6, 10) ;
            
	    }

       if (fbId == null || fbId == undefined || fbId == ""){
       }else{
       AccntDetailsMessage = AccntDetailsMessage+ " FB" + fbId.substring(0, 10);
       }
        //CR 13 changes end
    } else if (gblAdd_Receipent_State == gblEXISTING_RC_BANKACCNT_ADDITION) {
        //alert("gblEXISTING_RC_BANKACCNT_ADDITION")
        var noOfAccounts;

        /*if (frmIBMyReceipentsAddBankAccnt.segementTobeAddedAccnts.data != null)
			noOfAccounts = frmIBMyReceipentsAddBankAccnt.segementTobeAddedAccnts.data.length;
		else
			noOfAccounts = 0;
		if (noOfAccounts == 0) {
			AccountName = "No Accounts";
		} else {
			AccountName = "Multiple Accounts";
		}
		*/
        AccountName = frmIBMyReceipentsAddBankAccnt.lblRcName.text;
        ph = frmIBMyReceipentsAddBankAccnt.lblAddRcConfMobileNo.text;
        em = frmIBMyReceipentsAddBankAccnt.lblAddRcConfEmail.text;
        fbid = frmIBMyReceipentsAddBankAccnt.lblAddRcConfFbId.text;
        if (em == "Not Available" || em == "" || em == undefined || em == null || em == "No Mail") {
            em = "";
        } else {
            em = "Email:" + em + ". ";
        }
        if (ph == "Not Available" || ph == null || ph == undefined || ph == "" || ph == "No Number") {
            ph = "";
        } else {
            ph = removeHyphenIB(ph);
            ph = "Mobile Phone No.= xxx-xxx-" + ph.substring(6, 10) + ". ";
        }
        if (fbid == "Not Available" || fbid == null || fbid == undefined || fbid == "") {
            fbid = "";
        } else {
            fbid = "Facebook:" + fbid + " ";
        }
        AccntDetailsMessage = "";
        AccntDetailsMessage = AccntDetailsMessage + getAccountDetailMessageRc(frmIBMyReceipentsAddBankAccnt.segementTobeAddedAccnts.data)
        AccountName = frmIBMyReceipentsAddBankAccnt.lblRcName.text;

    } else if (gblAdd_Receipent_State == gblNEW_RC_ADDITION || gblAdd_Receipent_State == gblNEW_RC_ADDITION_PRECONFIRM) {
        if (isAddFb) {
            fbRc = frmIBMyReceipentsAddContactFB.segmentSelectRcConf.data;
            AccountName = frmIBMyReceipentsAddContactManually.lblrecipientname.text; // For DEF68
            var AccntDetailsMessage2 = "";
            var rcount = 0;
            if (fbRc != null && fbRc != undefined) {
                //Commented for CR 13
                /*for (var t = 0; t < fbRc.length; t++) {
                    AccntDetailsMessage2 = AccntDetailsMessage2 + fbRc[t]["lblSelectRcName"] + ", "
                }*/
                rcount = fbRc.length;
                if (rcount > 1) {
                    AccntDetailsMessage = rcount + "Recipients";
                } else {
                    AccntDetailsMessage = "" + fbRc[0]["facebookId"];
                }
            }
            //Commented for CR 13
            /*if (AccntDetailsMessage2 != null && AccntDetailsMessage2 != undefined) {
                AccntDetailsMessage2 = AccntDetailsMessage2.substring(0, AccntDetailsMessage2.length - 2)
            }
            AccntDetailsMessage = rcount + " Recipient (" + AccntDetailsMessage2 + ").";*/
        } else {
            var phn = frmIBMyReceipentsAddContactManually.lblmobnum.text
            if (phn == "Not Available" || phn == null || phn == undefined || phn == "" || phn == "No Number") {
                phn = "";
            } else {
                phn = removeHyphenIB(phn);
                phn = "Mobile x" + phn.substring(6, 10);
            }
            AccntDetailsMessage = "";
            //Commented for CR13
            // AccntDetailsMessage = AccntDetailsMessage + getAccountDetailMessageRc(frmIBMyReceipentsAddContactManually.segmentRcAccounts.data)
            if (frmIBMyReceipentsAddContactManually.segmentRcAccounts.data.length == 0) AccntDetailsMessage = phn + "";
            else AccntDetailsMessage = AccntDetailsMessage + getAccountDetailMessageRc(frmIBMyReceipentsAddContactManually.segmentRcAccounts.data) + phn + "";
        }
    }
    
    if(AccountName != null && AccountName.length > 13)
      AccountName = AccountName.substring(0, 13); //CR13 change


	if(parameter == "Channel" ){
		return Channel;
	}else if(parameter == "AccDetailMsg"){
		return AccntDetailsMessage;
	}
	else if(parameter = "userAccountName"){
		return AccountName;
	}
    
}
function recipientTokenExchangeService() {
 var inputParam = [];
 showLoadingScreenPopup();
 invokeServiceSecureAsync("tokenSwitching", inputParam, recipientTokenExchangeServiceCallback);
}

function recipientTokenExchangeServiceCallback(status,resulttable){
//added this below if for facebook manual flow image retriving & displaying
if(frmIBMyReceipentsAddContactManually.image210107168135.src!=null&&frmIBMyReceipentsAddContactManually.image210107168135.src!=undefined){
	beforeOTPIB= frmIBMyReceipentsAddContactManually.image210107168135.src
	if(beforeOTPIB.indexOf("fbcdn")>=0)
	tempImg=frmIBMyReceipentsAddContactManually.image210107168135.src
	else
	tempImg="png"
	//added for 1264def
}
  if (status == 400) {
   if(resulttable["opstatus"] == 0){
   
   	var inputParams={};
	if (gblAdd_Receipent_State != gblEXISTING_RC_EDITION) { //first
        		if (gblAdd_Receipent_State == gblNEW_RC_ADDITION && (frmIBMyReceipentsAddContactManually.segmentRcAccounts.data == null || frmIBMyReceipentsAddContactManually.segmentRcAccounts.data == "")) {
//						var totalData = [];
//						var RcData = [frmIBMyReceipentsAddContactManually.lblrecipientname.text, tempImg, frmIBMyReceipentsAddContactManually.lblemail.text,
//						frmIBMyReceipentsAddContactManually.txtmobnum.text, frmIBMyReceipentsAddContactManually.lblfbidstudio19.text, "Added","N"];//Modified by Studio Viz
//						totalData.push(RcData);

		         	var totalData = frmIBMyReceipentsAddContactManually.lblrecipientname.text+ ","  + tempImg+ ","  + frmIBMyReceipentsAddContactManually.lblemail.text+ ","  +
						frmIBMyReceipentsAddContactManually.txtmobnum.text+ ","  + frmIBMyReceipentsAddContactManually.lblfbidstudio19.text+ ","  + "Added"+ ","  +"N";//Modified by Studio Viz
						
						
						inputParams["receipentList"]= totalData;
						inputParams["gblTransactionType"]="1";	
									
								//startNewOnlyRecipientAddService();
					} else { //second
							if (gblAdd_Receipent_State == gblNEW_RC_ADDITION_PRECONFIRM || gblAdd_Receipent_State == gblNEW_RC_ADDITION){//third
                                  if (parseInt(formId.toString()) == 3) {
											inputParams["receipentList"]= prepareNewRcFBList()
											inputParams["gblTransactionType"]="1";	
												
									}else{					
											//var totalData = [];
											//var RcData = [frmIBMyReceipentsAddContactManually.lblrecipientname.text, "png", frmIBMyReceipentsAddContactManually.lblemail.text,
											//frmIBMyReceipentsAddContactManually.txtmobnum.text, frmIBMyReceipentsAddContactManually.lblfbidstudio19.text, "Added","N"];//Modified by Studio Viz
											//totalData.push(RcData);					
											var totalData = frmIBMyReceipentsAddContactManually.lblrecipientname.text+ ","  + "png"+ ","  + frmIBMyReceipentsAddContactManually.lblemail.text+ ","  +
						                    frmIBMyReceipentsAddContactManually.txtmobnum.text+ ","  + frmIBMyReceipentsAddContactManually.lblfbidstudio19.text+ ","  + "Added"+ ","  +"N";//Modified by Studio Viz
											inputParams["receipentList"]= totalData;
											inputParams["personalizedAccList"]=prepareNewAccountListNewRc("");
											//inputParams["accountNumebers"]=getNewAccountListNewRc();
											inputParams["gblTransactionType"]="3";
											inputParams["oldRcName"]=frmIBMyReceipentsAddContactManually.lblrecipientname.text;
											inputParams["oldRcEmail"]=frmIBMyReceipentsAddContactManually.lblemail.text;
											inputParams["oldRcmobile"]=removeHyphenIB(frmIBMyReceipentsAddContactManually.txtmobnum.text);
											inputParams["oldFbId"]=frmIBMyReceipentsAddContactManually.lblfbidstudio19.text;	//Modified by Studio Viz
												
											//startNewRecipientPlusAccountAddService();
										}				
								}//end of third
									else if (gblAdd_Receipent_State == gblEXISTING_RC_BANKACCNT_ADDITION) {
											if (frmIBMyReceipentsAddBankAccnt.segementTobeAddedAccnts.data.length == 0) {
												
											} else {
												gblRefreshRcCache = true;
												inputParams["personalizedAccList"]=prepareNewAccountListExistingRc()
												inputParams["gblTransactionType"]="4";
												inputParams["oldRcName"]=frmIBMyReceipentsAccounts.lblRcName.text;
												inputParams["oldRcmobile"]=removeHyphenIB(frmIBMyReceipentsAccounts.lblRcMobileNo.text);
												inputParams["oldRcEmail"]=frmIBMyReceipentsAccounts.lblRcEmail.text;
												inputParams["oldFbId"]=frmIBMyReceipentsAccounts.lblRcFbId.text;
											//	inputParams["accountNumebers"]=getNewAccountNumebrsListExistingRc();
													
												//startExistingRcAccountAddService();
											}
									}else {
											  //no need of otp	
										//startExistingRcAccountEditService();
										}
							}//end of second
						} else {//  end of first  edit case
										
									inputParams["personalizedId"]= gblselectedRcId;
									inputParams["personalizedName"]=frmIBMyReceipentsAddContactManually.lblrecipientname.text;
									inputParams["personalizedPictureId"]= frmIBMyReceipentsAddContactManually.image2101071681311897.src;
									inputParams["personalizedMailId"]=frmIBMyReceipentsAddContactManually.lblemail.text;
									inputParams["personalizedMobileNumber"]= removeHyphenIB(frmIBMyReceipentsAddContactManually.txtmobnum.text);
									inputParams["personalizedFacebookId"]=frmIBMyReceipentsAddContactManually.lblfbidstudio19.text;//Modified by Studio Viz
									inputParams["personalizedStatus"]="Added";
									inputParams["gblTransactionType"]="2";
									inputParams["oldRcName"]=frmIBMyReceipentsAccounts.lblRcName.text;
									inputParams["oldRcmobile"]=removeHyphenIB(frmIBMyReceipentsAccounts.lblRcMobileNo.text);
									inputParams["oldRcEmail"]=frmIBMyReceipentsAccounts.lblRcEmail.text;
									inputParams["oldFbId"]=frmIBMyReceipentsAccounts.lblRcFbId.text;
									//startExistingRcProfileEditService();
								}
			inputParams["flow"]="recipients";
			inputParams["Channel"]=getRecipientsOtpParams("Channel");
			inputParams["AccDetailMsg"]=getRecipientsOtpParams("AccDetailMsg")
			inputParams["userAccountName"]=getRecipientsOtpParams("userAccountName")
			showLoadingScreenPopup();
			invokeServiceSecureAsync("saveAddAccountDetails", inputParams, saveRecipientDeatilsCallBack);
   
   
   }else{
    dismissLoadingScreenPopup();
    alert(kony.i18n.getLocalizedString("keyErrResponseOne"));
   }
   }
}
function onSaveEditRecAccount(){
	
	//added validations code for 723,709, 499,527,534,540
	if(frmIBMyReceipentsAddBankAccnt.comboBankName.selectedKeyValue[1]+":"== kony.i18n.getLocalizedString("keyBank")&&frmIBMyReceipentsAddBankAccnt.vboxTMBSelectAccnt.isVisible==true){
		showAlert(kony.i18n.getLocalizedString("keySelectBank"), kony.i18n.getLocalizedString("info"))
		return false;
	}
	if(frmIBMyReceipentsAddBankAccnt.tbxAddAccntNumber.text == ""||frmIBMyReceipentsAddBankAccnt.tbxAddAccntNumber.text == null&&frmIBMyReceipentsAddBankAccnt.hboxAddAccntNumber.isVisible==true){
	showAlert(kony.i18n.getLocalizedString("keyErrAccntNoLen"), kony.i18n.getLocalizedString("info"))
		return false;
	}
	else if(kony.string.isNumeric(decodeAccountNumbers(frmIBMyReceipentsAddBankAccnt.tbxAddAccntNumber.text.toString())) == false){
       			alert(kony.i18n.getLocalizedString("keyincorrectAccNum"));
       			return;
    	}
    	frmIBMyReceipentsAddBankAccnt.tbxAddAccntNickName.text= frmIBMyReceipentsAddBankAccnt.tbxAddAccntNickName.text.trim();
	var text1=NickNameValid(frmIBMyReceipentsAddBankAccnt.tbxAddAccntNickName.text);
	if (text1 == false) {
	showAlert(kony.i18n.getLocalizedString("keyInvalidNickName"), kony.i18n.getLocalizedString("info"));
	return false;
	}
	var text2=AccountNameValid(frmIBMyReceipentsAddBankAccnt.tbxAddAccntAccountName.text);
	if (text2 == false&&frmIBMyReceipentsAddBankAccnt.hboxAddAccntAccountName.isVisible==true) {
	showAlert(kony.i18n.getLocalizedString("keyInvalidaccountName"), kony.i18n.getLocalizedString("info"));
	return false;
	}
if(frmIBMyReceipentsAddBankAccnt.tbxAddAccntNumber.text != "" && 
    frmIBMyReceipentsAddBankAccnt.tbxAddAccntNumber.text != null &&
    frmIBMyReceipentsAddBankAccnt.tbxAddAccntNickName.text != null &&
    frmIBMyReceipentsAddBankAccnt.tbxAddAccntNickName.text != "" &&
    frmIBMyReceipentsAddBankAccnt.comboBankName.selectedKeyValue[1] !=  kony.i18n.getLocalizedString("keylblBankName")){
    	confirmFlag=true;
    	if(kony.string.isNumeric(decodeAccountNumbers(frmIBMyReceipentsAddBankAccnt.tbxAddAccntNumber.text.toString())) == false){
       			alert(kony.i18n.getLocalizedString("keyincorrectAccNum"));
       			return;
    	}
		if(hasWhiteSpaceIB(decodeAccountNumbers(frmIBMyReceipentsAddBankAccnt.tbxAddAccntNumber.text.toString()))){
		alert(kony.i18n.getLocalizedString("keyincorrectAccNum"));
		return;
		}
	
    	//Check for duplicate nicknames
		if(!(getORFTFlagIB(frmIBMyReceipentsAddBankAccnt.comboBankName.selectedKey) == "Y")){
			if(	(frmIBMyReceipentsAddBankAccnt.tbxAddAccntNickName.text == frmIBMyReceipentsAccounts.lblNickNameAccntDetails.text) && (frmIBMyReceipentsAddBankAccnt.tbxAddAccntAccountName.text == frmIBMyReceipentsAccounts.lblAcctNameAccntDetails.text)){
				proceedToBackOnCancel();
				return false;
				
			}else{
				if(frmIBMyReceipentsAddBankAccnt.tbxAddAccntNickName.text != frmIBMyReceipentsAccounts.lblNickNameAccntDetails.text){
					if(checkAccountForDuplicateNickNames()){
			    		return;
			    	}
				}
				if(AccountNameValid(frmIBMyReceipentsAddBankAccnt.tbxAddAccntAccountName.text) == false){
					showAlert(kony.i18n.getLocalizedString("keyInvalidaccountName"), kony.i18n.getLocalizedString("info"));
					return false;
				}
			}
		}else{
			if(frmIBMyReceipentsAddBankAccnt.tbxAddAccntNickName.text == frmIBMyReceipentsAccounts.lblNickNameAccntDetails.text){
				proceedToBackOnCancel();
				return false;
			}
	    	if(checkAccountForDuplicateNickNames()){
	    		return;
	    	}
    	}
    	//Check if account number part of Crm accounts
		var resAccCheck = checkAccounNumbersWithCrmAccounts(frmIBMyReceipentsAddBankAccnt.tbxAddAccntNumber.text);
    	if(resAccCheck)
    	{
    		if(resAccCheck == "MEAccount")
    				alert(kony.i18n.getLocalizedString("keyReceipent_alert_MEAccountAdd"));
			else	alert(kony.i18n.getLocalizedString("Receipent_alert_correctaccount"));
			return;
    	}
    	checknValidateAccountToProceed();
    		
	}else{
		alert(kony.i18n.getLocalizedString("keyEnterValidData"));
	}

}

function onSelectionOfRecipientAddAccountBank(){
	var bankAccntLen = checkBankAccountLength(frmIBMyReceipentsAddBankAccnt.comboBankName.selectedKey);
	frmIBMyReceipentsAddBankAccnt.tbxAddAccntNumber.maxTextLength = bankAccntLen;
	frmIBMyReceipentsAddBankAccnt.tbxAddAccntNumber.text = "";
	frmIBMyReceipentsAddBankAccnt.tbxAddAccntNickName.text = "";
	frmIBMyReceipentsAddBankAccnt.tbxAddAccntAccountName.text = "";
	
	if(getORFTFlagIB(frmIBMyReceipentsAddBankAccnt.comboBankName.selectedKey) == "Y"){
		frmIBMyReceipentsAddBankAccnt.hboxAddAccntAccountName.setVisibility(false);
		frmIBMyReceipentsAddBankAccnt.line967337254161523.setVisibility(false)
	}else{
		frmIBMyReceipentsAddBankAccnt.hboxAddAccntAccountName.setVisibility(true);
		frmIBMyReceipentsAddBankAccnt.line967337254161523.setVisibility(true);	
	}
}



function recipientAddNextClick(){
	if(recipientAddFromTransfer){
		addAccountClick()
	}else{
		gblRetryCountRequestOTP = 0;
		confirmFlag=false;
		var isIE8N = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
		if(isIE8N){
			frmIBMyReceipentsAddContactManually.txtRecipientName.placeholder = "";
		}
		if(frmIBMyReceipentsAddContactManually.txtRecipientName.text != "" && frmIBMyReceipentsAddContactManually.txtRecipientName.text != null){
			if(validatePhoneNumber(frmIBMyReceipentsAddContactManually.txtmobnum.text) || frmIBMyReceipentsAddContactManually.txtmobnum.text=="Not Available"){
				if(validateEmail(frmIBMyReceipentsAddContactManually.txtemail.text) || frmIBMyReceipentsAddContactManually.txtemail.text=="Not Available" ){
					gblnewRcPhNumber = frmIBMyReceipentsAddContactManually.txtmobnum.text;
		
					if(gblAdd_Receipent_State == gblNEW_RC_ADDITION || gblAdd_Receipent_State == gblEXISTING_RC_EDITION){
						if(checkForOtherRcExistenceinCrm(frmIBMyReceipentsAddContactManually.txtRecipientName.text)){
							return;//do nothing
						}else{
							if(frmIBMyReceipentsAddContactManually.segmentRcAccounts.data == null ||
							    frmIBMyReceipentsAddContactManually.segmentRcAccounts.data.length == 0){
								frmIBMyReceipentsAddContactManually.label10107168132952.setVisibility(false);
								frmIBMyReceipentsAddContactManually.scrollboxRcAccounts.setVisibility(false);
								frmIBMyReceipentsAddContactManually.line101071681314188.setVisibility(false);
							}else{
								frmIBMyReceipentsAddContactManually.label10107168132952.setVisibility(true);
								frmIBMyReceipentsAddContactManually.scrollboxRcAccounts.setVisibility(true);
								frmIBMyReceipentsAddContactManually.line101071681314188.setVisibility(true);
							}
						}
					}
				
					frmIBMyReceipentsAddContactManually.hboxMyRecipientDetail.setVisibility(true);
					frmIBMyReceipentsAddContactManually.hboxMyRecipient.setVisibility(false);
					document.getElementById("frmIBMyReceipentsAddContactManually_image2101071681311897").src = document.getElementById("frmIBMyReceipentsAddContactManually_image210107168135").src;
					frmIBMyReceipentsAddContactManually.lblrecipientname.text=frmIBMyReceipentsAddContactManually.txtRecipientName.text;
					if(frmIBMyReceipentsAddContactManually.txtmobnum.text == null || frmIBMyReceipentsAddContactManually.txtmobnum.text == ""){
						frmIBMyReceipentsAddContactManually.lblmobnum.text="";
					}else{	
						frmIBMyReceipentsAddContactManually.lblmobnum.text= encodePhoneNumber(frmIBMyReceipentsAddContactManually.txtmobnum.text);
					}
						
					if(frmIBMyReceipentsAddContactManually.txtemail.text == null || frmIBMyReceipentsAddContactManually.txtemail.text == ""){
						frmIBMyReceipentsAddContactManually.lblemail.text="";
					}else{
						frmIBMyReceipentsAddContactManually.lblemail.text=frmIBMyReceipentsAddContactManually.txtemail.text;
						frmIBMyReceipentsAddContactManually.lblfbidstudio19.text=frmIBMyReceipentsAddContactManually.tbxFbID.text;//Modified by Studio Viz
					}
				}else{
					alert(kony.i18n.getLocalizedString("keyPleaseEnterValidEMAILID"));
					return;
				}
			}else{
				alert(kony.i18n.getLocalizedString("keyenternumber"));
				return;
			}
	    }else{
			alert(kony.i18n.getLocalizedString("keyEnterRecipientName"));
			return;
		}
	}
}

function addAccountClick(){
	frmIBMyReceipentsAddBankAccnt.hboxTMBSelectAccnt.isVisible = true;
	frmIBMyReceipentsAddBankAccnt.hboxAddAccntConf.isVisible = false;
	if(gblAdd_Receipent_State != gblEXISTING_RC_EDITION){
		gblAdd_Receipent_State=gblNEW_RC_ADDITION;
	}
	var isIE8 = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
	if(isIE8){
		frmIBMyReceipentsAddContactManually.txtRecipientName.placeholder="";
	}

	if(frmIBMyReceipentsAddContactManually.txtRecipientName.text != "" && 
	    frmIBMyReceipentsAddContactManually.txtRecipientName.text != null){
			if(validatePhoneNumber(frmIBMyReceipentsAddContactManually.txtmobnum.text)){
				if(validateEmail(frmIBMyReceipentsAddContactManually.txtemail.text)){
					if(checkForOtherRcExistenceinCrm(frmIBMyReceipentsAddContactManually.txtRecipientName.text)){
						return;//do nothing
					}
					//ALL VALID DATA ENTERED
					//Store the actual mobile number
					newRcPhNumber = frmIBMyReceipentsAddContactManually.txtmobnum.text;
					showLoadingScreenPopup();
					frmIBMyReceipentsAddBankAccnt.show();
					clearDataOnRcBankAdditionForm();
				}else{
					alert(kony.i18n.getLocalizedString("keyPleaseEnterValidEMAILID"));
					return;
				}
			}else{
				alert(kony.i18n.getLocalizedString("keyenternumber"));
				return;
			}
	}else{
		alert(kony.i18n.getLocalizedString("keyEnterRecipientName"));
		return;
	}
}

function transferButtonClick(){
	if(getCRMLockStatus())
	{
		popIBBPOTPLocked.show();
		return false;
	}		
	gblSelTransferMode = 1; // transfer to account type
	gblTransferFromRecipient=true;
	accountData="";
	var curr_form = kony.application.getCurrentForm();
	if(curr_form.id == "frmIBMyReceipentsAddContactManuallyConf"){
		gblSelectedRecipentName = frmIBMyReceipentsAddContactManuallyConf.lblrcname.text;
		gblXferPhoneNo = frmIBMyReceipentsAddContactManuallyConf.lblmobnum.text;
		gblXferEmail = frmIBMyReceipentsAddContactManuallyConf.lblemail.text;
		accountData = frmIBMyReceipentsAddContactManuallyConf.segmentRcAccounts.selectedItems[0];
		var selectedAccount= frmIBMyReceipentsAddContactManuallyConf.segmentRcAccounts.selectedIndex[1];
		gblisTMB = accountData["bankCode"];
		frmIBTransferCustomWidgetLP.imgXferToImage.src =  frmIBMyReceipentsAddContactManuallyConf.image2101071681311897.src;
	}else if(curr_form.id == "frmIBMyReceipentsEditAccountConf"){
		gblSelectedRecipentName = frmIBMyReceipentsAccounts.lblRcName.text;
		accountData = frmIBMyReceipentsEditAccountConf.segementTobeAddedAccntsConf.selectedItems[0];
		gblisTMB = frmIBMyReceipentsAccounts.segmentRcAccounts.selectedItems[0]["selectedBankCD"];
		gblXferPhoneNo = frmIBMyReceipentsAccounts.lblRcMobileNo.text;
		gblXferEmail = frmIBMyReceipentsAccounts.lblRcEmail.text;
		frmIBTransferCustomWidgetLP.imgXferToImage.src=RC_LOGO_URL+gblselectedRcId;
	}else{
		gblSelectedRecipentName = frmIBMyReceipentsAccounts.lblRcName.text;
		if(!isNotBlank(gblSelectedRecipentName)){
           showAlertIB(kony.i18n.getLocalizedString("Recipient_TR_NameInvalid"), kony.i18n.getLocalizedString("info"));
           return;
		}
		accountData = frmIBMyReceipentsAccounts.segmentRcAccounts.selectedItems[0];
		gblisTMB = accountData["selectedBankCD"];
		gblXferPhoneNo = frmIBMyReceipentsAccounts.lblRcMobileNo.text;
		gblXferEmail = frmIBMyReceipentsAccounts.lblRcEmail.text;
		frmIBTransferCustomWidgetLP.imgXferToImage.src=RC_LOGO_URL+gblselectedRcId;
	}
		if(gblXferPhoneNo == "" || gblXferPhoneNo == null){
			gblXferPhoneNo="";
		}
		if(gblXferEmail == "" || gblXferEmail == null){
			gblXferEmail="";
		}
		gblPaynow = true;
	gblSelTransferMode = 1;//default account transfer
	getFromXferAccountsIB();
	gblBANKREF = accountData["lblBankName"];
	//frmIBTransferCustomWidgetLP.imgXferToImage.src=frmIBMyReceipentsAccounts.segmentRcAccounts.selectedItems[0].bankLogo;
	frmIBTransferCustomWidgetLP.lblXferToNameRcvd.text = accountData["lblNickName"];
	frmIBTransferCustomWidgetLP.lblXferToContactRcvd.text = accountData["lblNumber"];
	frmIBTransferCustomWidgetLP.lblXferToBankNameRcvd.text = gblBANKREF;
	frmIBTransferCustomWidgetLP.imgXferToImage.setVisibility(true);
}