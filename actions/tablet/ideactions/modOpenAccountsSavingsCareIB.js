gblOpenActBenList = [];
gblBenificiaryCount = 0;
gblNoOfBenificiariesAdded = 0;
gblRelationshipData = [];

function getRelationShipsFromService(){
				inputparam["channelId"] = "";
                var status = invokeServiceSecureAsync("getBeneficiaryRelationships", inputparam, getRelationShipsFromServiceCallback);
}


function getRelationShipsFromServiceCallback(status, result){
	
	if (status == 400) {
		if (result["opstatus"] == 0) {
			var masterDataEN = [];
			var masterDataTH = [];	
			var tempData99EN = [];
			var tempData99TH = [];
				masterDataEN.push(["P",kony.i18n.getLocalizedString("keyBeneficiaryPlsSel")])
			 	masterDataTH.push(["P",kony.i18n.getLocalizedString("keyBeneficiaryPlsSel")])
				for(var value in result["relationshipsEN"][0]){
					if(value != "99") {
						var tempData = [value,result["relationshipsEN"][0][value]]
						masterDataEN.push(tempData)					
					} else {
						tempData99EN = [value,result["relationshipsEN"][0][value]]
					}
				}
				if(tempData99EN != null) masterDataEN.push(tempData99EN)	
					
				for(var value in result["relationshipsTH"][0]){
					if(value != "99") {
						var tempData = [value,result["relationshipsTH"][0][value]]
						masterDataTH.push(tempData)
					} else {
						tempData99TH = [value,result["relationshipsTH"][0][value]]
					}
				}
				if(tempData99TH != null) masterDataTH.push(tempData99TH)	
				
				gblRelationshipData.push(masterDataEN)
				gblRelationshipData.push(masterDataTH)
		}
	}
}

function changeSkinForSpecifyBtn(eventObject) {
    var buttonId = eventObject["id"];
    var currForm = kony.application.getCurrentForm().id;
    if (currForm == "frmIBOpenNewSavingsCareAcc") {
        if (buttonId == "btnSpecify") {
            if (frmIBOpenNewSavingsCareAcc.btnSpecify.skin == "btnIBTab4LeftNrml") {
                frmIBOpenNewSavingsCareAcc.btnSpecify.skin = "btnIBTab4LeftFocus"
                frmIBOpenNewSavingsCareAcc.btnNotSpecify.skin = "btnIbTab4RightNrml"
                frmIBOpenNewSavingsCareAcc.hboxNoBenificiaryMessage.setVisibility(false);
                frmIBOpenNewSavingsCareAcc.hboxBenificiariesList.setVisibility(true);
                frmIBOpenNewSavingsCareAcc.hbxLink3.setVisibility(true);
                clearBenificiariesContent();
                onClickAddBenificiary();
                
            }

        } else if (buttonId == "btnNotSpecify") {
            if (frmIBOpenNewSavingsCareAcc.btnNotSpecify.skin == "btnIbTab4RightNrml") {
                if(validateBenOnClickNotSpecified()){
                	gblOpenActBenList = [];
	                clearBenificiariesContent();
	                gblNoOfBenificiariesAdded = 0;
	                gblBenificiaryCount = 0;
	                frmIBOpenNewSavingsCareAcc.btnNotSpecify.skin = "btnIBTab4RightFocus"
	                frmIBOpenNewSavingsCareAcc.btnSpecify.skin = "btnIBTab4LeftNrml"
	                frmIBOpenNewSavingsCareAcc.hboxNoBenificiaryMessage.setVisibility(true);
	                frmIBOpenNewSavingsCareAcc.hboxBenificiariesList.setVisibility(false);
	                frmIBOpenNewSavingsCareAcc.hbxLink3.setVisibility(false);
                }else{
                    var basicConfig = {};
                    basicConfig.message = kony.i18n.getLocalizedString("keyBeneficiaryCnfrmDelDetail");
                    basicConfig.alertType = constants.ALERT_TYPE_CONFIRMATION;
                    basicConfig.yesLabel = "Yes";
                    basicConfig.noLabel = "No";
                    basicConfig.alertHandler = callbackNotSpecicifiedButton;
                    var pspConfig = {};
                    var FBDelinkPopup = kony.ui.Alert(basicConfig, pspConfig);
                                
                }
            }
        }

    }
}


function clearBenificiariesContent(){
	for(var i=0;i<gblBenificiaryCount ; i++){
		if( frmIBOpenNewSavingsCareAcc["txtFirstName" + i] != undefined){
		    kony.print("Index" + i);
		    frmIBOpenNewSavingsCareAcc["hbxOASCBenDel" + i].removeFromParent();
		    frmIBOpenNewSavingsCareAcc["hboxBenificiary" + i].removeFromParent();
		    //gblBenificiaryCount = gblBenificiaryCount - 1;
			gblNoOfBenificiariesAdded = gblNoOfBenificiariesAdded -1;
			}
	}
	gblBenificiaryCount = 0

}
function callbackNotSpecicifiedButton (response){
				if(response){
				clearBenificiariesContent();
				frmIBOpenNewSavingsCareAcc.btnNotSpecify.skin = "btnIBTab4RightFocus"
                frmIBOpenNewSavingsCareAcc.btnSpecify.skin = "btnIBTab4LeftNrml"
                frmIBOpenNewSavingsCareAcc.hboxNoBenificiaryMessage.setVisibility(true);
                frmIBOpenNewSavingsCareAcc.hboxBenificiariesList.setVisibility(false);
                frmIBOpenNewSavingsCareAcc.hbxLink3.setVisibility(false);
                gblOpenActBenList = [];
				
				gblNoOfBenificiariesAdded = 0;
                
                }
}

function openSavingsCareDefault() {
	var prevForm = kony.application.getPreviousForm().id;
	if(prevForm != "frmIBOpenNewSavingsCareAccConfirmation"){
		frmIBOpenNewSavingsCareAcc.txtNickName.text = "";
		clearBenificiariesContent();
		clearBenificiariesContent();
		gblOpenActBenList = [];
		gblNoOfBenificiariesAdded = 0;
		 frmIBOpenNewSavingsCareAcc.btnNotSpecify.skin = "btnIBTab4RightFocus"
	  frmIBOpenNewSavingsCareAcc.btnSpecify.skin = "btnIBTab4LeftNrml"
	  frmIBOpenNewSavingsCareAcc.richTextBenNotSpecified.text = kony.i18n.getLocalizedString("keyBeneficiariesNotSpecifyDetail")
	  frmIBOpenNewSavingsCareAcc.hboxNoBenificiaryMessage.setVisibility(true);
	  frmIBOpenNewSavingsCareAcc.hbxLink3.setVisibility(false);
	  frmIBOpenNewSavingsCareAcc.hboxBenificiariesList.setVisibility(false);
	}
	getRelationShipsFromService();
 
  
}








function defaultFormToShowConfirmation() {
    if (frmIBOpenNewSavingsCareAcc.hboxNoBenificiaryMessage.isVisible) {
        frmIBOpenNewSavingsCareAccConfirmation.hboxNoBenificiaryMessage.setVisibility(true);
        frmIBOpenNewSavingsCareAccConfirmation.segmentBenificiaries.setVisibility(false);
        frmIBOpenNewSavingsCareAccConfirmation.hbxBeneficiaryText.setVisibility(false);
        frmIBOpenNewSavingsCareAccConfirmation.lblOAPayIntAck.text = kony.i18n.getLocalizedString("keyBeneficiaries") + ":" + kony.i18n.getLocalizedString("keyBeneficiariesNotSpecify")
    } else {
        frmIBOpenNewSavingsCareAccConfirmation.hboxNoBenificiaryMessage.setVisibility(false);
        setBenificiariesToSavingsCareIB(frmIBOpenNewSavingsCareAccConfirmation);
        frmIBOpenNewSavingsCareAccConfirmation.segmentBenificiaries.setVisibility(true);
        frmIBOpenNewSavingsCareAccConfirmation.hbxBeneficiaryText.setVisibility(true);
        frmIBOpenNewSavingsCareAccConfirmation.lblOAPayIntAck.text = kony.i18n.getLocalizedString("keyBeneficiaries") + ":" + kony.i18n.getLocalizedString("keyBeneficiariesSpecify")


    }


}

function setBenificiariesToSavingsCareIB(frmName) {
    if (frmName.id = "frmIBOpenNewSavingsCareAccConfirmation") {
        // Need to set the data here
    } else if (frmName.id = "frmIBOpenNewSavingsCareAccConfirmation") {
        // Need to set the data here
    }
}

function SavingsCareDynamicIB(i) {
	 var lblFiller = new kony.ui.Label({
        "id": "lblFiller"+i,
        "isVisible": true,
        "text": null,
        "skin": "lblNormal"
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "padding": [1, 1, 1, 1],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 91
    }, {
        "toolTip": null
    });
     var button96743040886568 = new kony.ui.Button({
        "id": "btnDeleteBenificiary" + i,
        "isVisible": true,
        "text": null,
        "skin": "btnIBdelicon",
        "focusSkin": "btnIBdeliconHover",
        "onClick": deleteBenificiaryIB
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_MIDDLE_RIGHT,
        "vExpand": false,
        "hExpand": false,
        "margin": [0, 2, 0, 2],
        "padding": [0, 8, 0, 8],
        "displayText": true,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 9
    }, {
        "hoverskin": "btnIBdeliconHover",
        "toolTip": null
    });
    
     var hbxOASCBenDel = new kony.ui.Box({
        "id":"hbxOASCBenDel"+i,
        "isVisible": true,
        "position": constants.BOX_POSITION_AS_NORMAL,
        "orientation": constants.BOX_LAYOUT_HORIZONTAL
    }, {
        "containerWeight": 26,
        "percent": true,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
        "margin": [0, 3, 0, 3],
        "padding": [0, 0, 0, 0],
        "vExpand": false,
        "marginInPixel": false,
        "paddingInPixel": false,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {});
    hbxOASCBenDel.add(lblFiller, button96743040886568);

    
    var lblBefFirstName = new kony.ui.Label({
        "id": "lblBefFirstName"+i,
        "isVisible": true,
        "text": kony.i18n.getLocalizedString("keyFirstName"),
        "skin": "lblIB20pxBlack",
        "i18n_text": "kony.i18n.getLocalizedString('keyFirstName')"
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 30
    }, {
        "toolTip": null
    });
    var txtFirstName = new kony.ui.TextBox2({
        "id": "txtFirstName"+i,
        "isVisible": true,
        "text": null,
        "secureTextEntry": false,
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "maxTextLength": 19,
        "placeholder": null,
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "skin": "txtIB20pxGreyBackground",
        "focusSkin": "txtIB20pxGreyFocus"
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "padding": [1, 1, 1, 1],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "containerHeightMode": constants.TEXTBOX_DEFAULT_PLATFORM_HEIGHT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 70
    }, {
        "onBeginEditing": "",
        "onEndEditing": "",
        "autoCorrect": false,
        "autoComplete": false
        //"onKeyUp": ontextChangeFirstName
    });
    var hbxBefFirstName = new kony.ui.Box({
        "id": "hbxBefFirstName"+i,
        "isVisible": true,
        "position": constants.BOX_POSITION_AS_NORMAL,
        "skin": "hbxProper",
        "orientation": constants.BOX_LAYOUT_HORIZONTAL
    }, {
        "containerWeight": 21,
        "percent": true,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
        "margin": [0, 2, 0, 1],
        "padding": [3, 0, 3, 0],
        "vExpand": false,
        "marginInPixel": false,
        "paddingInPixel": false,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {});
    hbxBefFirstName.add(lblBefFirstName, txtFirstName);
    var lineBefFirstName = new kony.ui.Line({
        "id": "lineBefFirstName"+i,
        "isVisible": true,
        "skin": "line300px"
    }, {
        "thickness": 1,
        "margin": [0, 0, 0, 0],
        "marginInPixel": false,
        "paddingInPixel": false
    }, {});
    
    var lblBefLastName = new kony.ui.Label({
        "id": "lblBefLastName"+i,
        "isVisible": true,
        "text": kony.i18n.getLocalizedString("keyLastName"),
        "skin": "lblIB20pxBlack",
        "i18n_text": "kony.i18n.getLocalizedString('keyLastName')"
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 30
    }, {
        "toolTip": null
    });
    var txtLastName = new kony.ui.TextBox2({
        "id": "txtLastName"+i,
        "isVisible": true,
        "text": null,
        "secureTextEntry": false,
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "maxTextLength": 20,
        "placeholder": null,
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "skin": "txtIB20pxGreyBackground",
        "focusSkin": "txtIB20pxGreyFocus"
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "padding": [1, 1, 1, 1],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "containerHeightMode": constants.TEXTBOX_DEFAULT_PLATFORM_HEIGHT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 70
    }, {
        "onBeginEditing": "",
        "onEndEditing": "",
        "autoCorrect": false,
        "autoComplete": false
       // "onKeyUp": ontextChangeLastName
    });
    var hbxBefLastName = new kony.ui.Box({
        "id": "hbxBefLastName"+i,
        "isVisible": true,
        "position": constants.BOX_POSITION_AS_NORMAL,
        "skin": "hbxProper",
        "orientation": constants.BOX_LAYOUT_HORIZONTAL
    }, {
        "containerWeight": 21,
        "percent": true,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
        "margin": [0, 1, 0, 1],
        "padding": [3, 0, 3, 0],
        "vExpand": false,
        "marginInPixel": false,
        "paddingInPixel": false,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {});
    hbxBefLastName.add(lblBefLastName, txtLastName);
    var lineBefLastName = new kony.ui.Line({
        "id": "lineBefLastName"+i,
        "isVisible": true,
        "skin": "line300px"
    }, {
        "thickness": 1,
        "margin": [0, 0, 0, 0],
        "marginInPixel": false,
        "paddingInPixel": false
    }, {});
    var masterData = [];
    var currLocale = kony.i18n.getCurrentLocale();
   	if (currLocale == "en_US") {
   		masterData = gblRelationshipData[0]
   	}else{
   		masterData = gblRelationshipData[1]
   	}
    
    var comboRelationship = new kony.ui.ComboBox({
        "id": "comboRelationship" + i,
        "isVisible": true,
        "masterData":masterData,
        "selectedKey": "P",
        "skin": "cbxIBDropDownArrowGreyNoLine",
        "focusSkin": "cbxIBDropDownArrowGreyNoLineFocus",
        "onSelection": ""
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "padding": [0, 1, 0, 1],
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 100
    }, {
        "viewType": constants.COMBOBOX_VIEW_TYPE_LISTVIEW,
        "hoverSkin": "cbxIBDropDownArrowGreyNoLineFocus"
    });
    var hbxBefRelation = new kony.ui.Box({
        "id": "hbxBefRelation"+i,
        "isVisible": true,
        "position": constants.BOX_POSITION_AS_NORMAL,
        "orientation": constants.BOX_LAYOUT_HORIZONTAL
    }, {
        "containerWeight": 21,
        "percent": true,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
        "margin": [0, 0, 0, 0],
        "padding": [3, 0, 0, 0],
        "vExpand": false,
        "marginInPixel": false,
        "paddingInPixel": false,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {});
     hbxBefRelation.add(comboRelationship);
   var lineBefRelation = new kony.ui.Line({
        "id": "lineBefRelation"+i,
        "isVisible": true,
        "skin": "line300px"
    }, {
        "thickness": 1,
        "margin": [0, 0, 0, 0],
        "marginInPixel": false,
        "paddingInPixel": false
    }, {});
     var lineBefRelation = new kony.ui.Line({
        "id": "lineBefRelation"+i,
        "isVisible": true,
        "skin": "line300px"
    }, {
        "thickness": 1,
        "margin": [0, 0, 0, 0],
        "marginInPixel": false,
        "paddingInPixel": false
    }, {});
    var lblBefBenefit = new kony.ui.Label({
        "id": "lblBefBenefit"+i,
        "isVisible": true,
        "text": kony.i18n.getLocalizedString("keyBenfit"),
        "skin": "lblIB20pxBlack",
        "i18n_text": "kony.i18n.getLocalizedString('keyBenfit')"
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 33
    }, {
        "toolTip": null
    });
    var txtBefBenefit = new kony.ui.TextBox2({
        "id": "txtBenifit"+i,
        "isVisible": true,
        "text": null,
        "secureTextEntry": false,
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "maxTextLength": 3,
        "placeholder": null,
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "skin": "txtIB20pxGreyBackground",
        "focusSkin": "txtIB20pxGreyFocus"
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "padding": [1, 1, 1, 1],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "containerHeightMode": constants.TEXTBOX_DEFAULT_PLATFORM_HEIGHT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 67
    }, {
        "onBeginEditing": "",
        "onEndEditing": "",
        "autoCorrect": false,
        "autoComplete": false
    });
    var hbxBefBenefit = new kony.ui.Box({
        "id": "hbxBefBenefit"+i,
        "isVisible": true,
        "position": constants.BOX_POSITION_AS_NORMAL,
        "skin": "hbxProper",
        "orientation": constants.BOX_LAYOUT_HORIZONTAL
    }, {
        "containerWeight": 26,
        "percent": true,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
        "margin": [0, 1, 0, 1],
        "padding": [4, 0, 0, 0],
        "vExpand": false,
        "marginInPixel": false,
        "paddingInPixel": false,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {});
    hbxBefBenefit.add(lblBefBenefit, txtBefBenefit);
    var lineBefBenefit = new kony.ui.Line({
        "id": "lineBefBenefit"+i,
        "isVisible": true,
        "skin": "line300pxBottom"
    }, {
        "thickness": 1,
        "margin": [0, 0, 0, 0],
        "marginInPixel": false,
        "paddingInPixel": false
    }, {});
    var vbxEachBeneficiaryDetail = new kony.ui.Box({
        "id": "vbxEachBeneficiaryDetail"+i,
        "isVisible": true,
        "orientation": constants.BOX_LAYOUT_VERTICAL
    }, {
        "containerWeight": 100,
        "margin": [0, 0, 0, 0],
        "padding": [0, 0, 0, 0],
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "vExpand": false,
        "hExpand": true,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {});
	vbxEachBeneficiaryDetail.add(hbxBefFirstName, lineBefFirstName, hbxBefLastName, lineBefLastName, hbxBefRelation, lineBefRelation, hbxBefBenefit, lineBefBenefit);
     hboxBenificiary = new kony.ui.Box({
        "id": "hboxBenificiary"+i,
        "isVisible": true,
        "position": constants.BOX_POSITION_AS_NORMAL,
        "skin": "hboxLightGrey",
        "orientation": constants.BOX_LAYOUT_HORIZONTAL
    }, {
        "containerWeight": 74,
        "percent": true,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
        "margin": [0, 0, 0, 0],
        "padding": [0, 0, 0, 0],
        "vExpand": false,
        "marginInPixel": false,
        "paddingInPixel": false,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {});
    hboxBenificiary.add(vbxEachBeneficiaryDetail);

    frmIBOpenNewSavingsCareAcc.vboxBenificiaries.add(
        hbxOASCBenDel, hboxBenificiary);
    addNumberCheckListnerZeroToNine("txtBenifit"+i);
    
    //frmIBOpenNewSavingsCareAcc["comboRelationship"+i].selectedKey = "P"
}



function onClickAddBenificiary() {
    if (gblNoOfBenificiariesAdded < gblMaxBeneficiaryConfig) {
    	if(gblNoOfBenificiariesAdded > 0){
    		var isValidated = validateBenificiaries();
    	
    		if(isValidated){
    			frmIBOpenNewSavingsCareAcc.hboxNoBenificiaryMessage.isVisible = false;
		        SavingsCareDynamicIB(gblBenificiaryCount);
		        kony.print("555")
		        gblBenificiaryCount = gblBenificiaryCount + 1;
		        gblNoOfBenificiariesAdded = gblNoOfBenificiariesAdded + 1;
    		}
    	}else{
    	
    	
    	frmIBOpenNewSavingsCareAcc.hboxNoBenificiaryMessage.isVisible = false;
        SavingsCareDynamicIB(gblBenificiaryCount);
        kony.print("555")
        gblBenificiaryCount = gblBenificiaryCount + 1;
        gblNoOfBenificiariesAdded = gblNoOfBenificiariesAdded + 1;
        }
        }
        if(gblNoOfBenificiariesAdded == gblMaxBeneficiaryConfig){
        	frmIBOpenNewSavingsCareAcc.hbxLink3.isVisible = false;
        }
    
}
function deleteBenificiaryIB(eventobject) {
			popupIBCommonConfirmCancel.lblPopUpHeader.text=kony.i18n.getLocalizedString("Confirmation");
			popupIBCommonConfirmCancel.lblPopUpMessage.text=kony.i18n.getLocalizedString("keyBeneficiaryConfirmDelete");
			popupIBCommonConfirmCancel.show();
			popupIBCommonConfirmCancel.btnPopUpConfirm.onClick=function(){callbackDeleteBenificiary(eventobject)};
	}

function callbackDeleteBenificiary(eventobject){
	popupIBCommonConfirmCancel.dismiss();
	 var i = eventobject["id"].replace("btnDeleteBenificiary", "")
    var widgetReference = "hbxOASCBenDel" + i
    kony.print("widgetReference" + widgetReference);
    frmIBOpenNewSavingsCareAcc["hbxOASCBenDel" + i].removeFromParent();
    var widgetReference = "hboxBenificiaryOne" + i
    kony.print("widgetReference" + widgetReference);
    frmIBOpenNewSavingsCareAcc["hboxBenificiary" + i].removeFromParent();
    //gblBenificiaryCount = gblBenificiaryCount - 1;
	gblNoOfBenificiariesAdded = gblNoOfBenificiariesAdded -1;
	 if(gblNoOfBenificiariesAdded == gblMaxBeneficiaryConfig-1){
        	frmIBOpenNewSavingsCareAcc.hbxLink3.isVisible = true;
        }
     
     if(gblNoOfBenificiariesAdded == 0){
     	gblOpenActBenList = [];
				
				gblNoOfBenificiariesAdded = 0;
            	clearBenificiariesContent();
                frmIBOpenNewSavingsCareAcc.btnNotSpecify.skin = "btnIBTab4RightFocus"
                frmIBOpenNewSavingsCareAcc.btnSpecify.skin = "btnIBTab4LeftNrml"
                frmIBOpenNewSavingsCareAcc.hboxNoBenificiaryMessage.setVisibility(true);
                frmIBOpenNewSavingsCareAcc.hboxBenificiariesList.setVisibility(false);
                frmIBOpenNewSavingsCareAcc.hbxLink3.setVisibility(false);
     
     
}

}






function onClickNextSavingsCareIB() {
	gblOnClickCoverflow = "SAVINGSCARE"
    if (gblOnClickCoverflow == "SAVINGSCARE") {
        frmIBOpenNewSavingsCareAcc.coverFlowTP.onSelect = IBonclickSavingsCareAccountCoverFlow;
        frmIBOpenNewSavingsCareAccConfirmation.lblOASCOpenActVal.text = frmIBOpenNewSavingsCareAcc.txtAmount.text
        gblRetryCountRequestOTP = "0";
        
        //doValidationSavingsCareIB()
            IBonClickSavingCareLanding();
        
      //  if(doValidationSavingsCareIB()){
        	//pushBenificiaryDatatoArray();
        	
        //}

    }
}



function validatePercentagesIB() {
    var totBenPer = 0;
    for (var i = 0; i < gblBenificiaryCount; i++) {
        if (frmIBOpenNewSavingsCareAcc["hbxOASCBenDel" + i].isVisible && frmIBOpenNewSavingsCareAcc["txtBenifit" + i] != undefined) {
            var amount = frmIBOpenNewSavingsCareAcc["txtBenifit" + i].text;
            totBenPer = totBenPer + parseInt(amount)
        }
        kony.print("amount"+amount)
    }
    return totBenPer;

}

function validateBenificiaries() {
	gblHaveErrorFlag = false;
	if(gblBenificiaryCount > 0){
    for (var i = 0; i < gblBenificiaryCount; i++) {
    	kony.print("I::"+i)
        if (frmIBOpenNewSavingsCareAcc["hbxOASCBenDel"+i].isVisible && frmIBOpenNewSavingsCareAcc["txtFirstName" + i] != undefined) {
            var firsTxtName = frmIBOpenNewSavingsCareAcc["txtFirstName" + i];
            var scndTxtName = frmIBOpenNewSavingsCareAcc["txtLastName" + i];
            var befTxtName = frmIBOpenNewSavingsCareAcc["txtBenifit" + i];
            var relSel = frmIBOpenNewSavingsCareAcc["comboRelationship" + i].selectedKey;
            if (firsTxtName.text == "" || firsTxtName.text == null) {
                showAlert(kony.i18n.getLocalizedString("keyBeneficiaryAddDetail"), kony.i18n.getLocalizedString("info"));
                firsTxtName.skin = "txtErrorBG";
                return false;
            }else if(!firstNameAlpNumValidation(firsTxtName.text)){
            	showAlert(kony.i18n.getLocalizedString("keyBenFirstNameAlphaNum"), kony.i18n.getLocalizedString("info"));
                firsTxtName.skin = "txtErrorBG";
                return false;
            
            } else if (scndTxtName.text == "" || scndTxtName.text == null) {
                showAlert(kony.i18n.getLocalizedString("keyBeneficiaryAddDetail"), kony.i18n.getLocalizedString("info"));
                scndTxtName.skin = "txtErrorBG";
                return false;
            }else if(!lastNameAlpNumValidation(scndTxtName.text)){
            	showAlert(kony.i18n.getLocalizedString("keyBenSecondNameAlphNum"), kony.i18n.getLocalizedString("info"));
                scndTxtName.skin = "txtErrorBG";
                return false;
            
            } else if (befTxtName.text == "" || befTxtName.text == null) {
                showAlert(kony.i18n.getLocalizedString("keyBeneficiaryAddDetail"), kony.i18n.getLocalizedString("info"));
                befTxtName.skin = "txtErrorBG";
                return false;
            } else if (relSel == "P" || relSel == null) {

                showAlert(kony.i18n.getLocalizedString("keyBeneficiarySelRelationship"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (!(benNameAlpNumValidation(firsTxtName.text))) {

                showAlert(kony.i18n.getLocalizedString("keyBenFirstNameAlphaNum"), kony.i18n.getLocalizedString("info"));
                firsTxtName.skin = txtErrorBG;
                return false;
            } else if (!(benNameAlpNumValidation(scndTxtName.text))) {
                showAlert(kony.i18n.getLocalizedString("keyBenSecondNameAlphNum"), kony.i18n.getLocalizedString("info"));
                scndTxtName.skin = txtErrorBG;
                return false;
            } else if ( (kony.os.toNumber(befTxtName.text)) > 100 ) {//MIB-4914
	                showAlert(kony.i18n.getLocalizedString("keyBenpercentage"), kony.i18n.getLocalizedString("info"));
	                befTxtName.skin = "txtErrorBG";
	                return false;
            } else if ( (kony.os.toNumber(befTxtName.text)) <= 0 ) {//MIB-4914
            	gblHaveErrorFlag = true;
        	} 
                

        }
		
    }
	return true;
    }else{
    	return false;
    }

}

function checkForDuplicateNames() {
    var uniqArr = [];
    for (var i = 0; i < gblBenificiaryCount; i++) {
        if (frmIBOpenNewSavingsCareAcc["hbxOASCBenDel" + i].isVisible && frmIBOpenNewSavingsCareAcc["txtFirstName" + i] != undefined) {	
            var usrName = frmIBOpenNewSavingsCareAcc["txtFirstName" + i].text + frmIBOpenNewSavingsCareAcc["txtLastName" + i].text
            uniqArr.push(usrName);
        }
        
    }
    
    for (i = 0; i < uniqArr.length; i++) {

        for (j = 0; j < uniqArr.length; j++) {
            if (i != j) {
				kony.print("Before return true"+uniqArr[i])
				kony.print("Before return true"+uniqArr[j])
                if (uniqArr[i].toUpperCase() == uniqArr[j].toUpperCase()) {
                    return false;
                }
            }
        }
    }
	return true;
}




function resetSkinSavingCareIB() {
    /* frmIBOpenNewSavingsCareAcc.txtAmount.skin = "txtIB20pxGrey";
    frmIBOpenNewSavingsCareAcc.txtNickName.skin="txtIB20pxGrey";
    frmIBOpenNewSavingsCareAcc.txtBefBen1.skin="txtIB20pxGreyBackground";
    frmIBOpenNewSavingsCareAcc.txtBefBen2.skin="txtIB20pxGreyBackground";
    frmIBOpenNewSavingsCareAcc.txtBefBen3.skin="txtIB20pxGreyBackground"
    frmIBOpenNewSavingsCareAcc.txtBefBen4.skin="txtIB20pxGreyBackground"
    frmIBOpenNewSavingsCareAcc.txtBefBen5.skin="txtIB20pxGreyBackground";
    frmIBOpenNewSavingsCareAcc.txtBefFisrtName1.skin="txtIB20pxGreyBackground"
    frmIBOpenNewSavingsCareAcc.txtBefFisrtName2.skin="txtIB20pxGreyBackground"
    frmIBOpenNewSavingsCareAcc.txtBefFisrtName3.skin="txtIB20pxGreyBackground"
    frmIBOpenNewSavingsCareAcc.txtBefFisrtName4.skin="txtIB20pxGreyBackground"
    frmIBOpenNewSavingsCareAcc.txtBefFisrtName5.skin="txtIB20pxGreyBackground";
    frmIBOpenNewSavingsCareAcc.txtBefSecName1.skin="txtIB20pxGreyBackground";
    frmIBOpenNewSavingsCareAcc.txtBefSecName2.skin="txtIB20pxGreyBackground";
    frmIBOpenNewSavingsCareAcc.txtBefSecName3.skin="txtIB20pxGreyBackground"
    frmIBOpenNewSavingsCareAcc.txtBefSecName4.skin="txtIB20pxGreyBackground"
    frmIBOpenNewSavingsCareAcc.txtBefSecName5.skin="txtIB20pxGreyBackground"; */
}


function setBenificiariesSegment(){
		var data = [];
		if(gblOpenActBenList.length != 0){
		for(var i = 0; i< gblOpenActBenList.length; i++){
			
			var benifDataTemp = {"lblSegBeneficiaryNameValue": gblOpenActBenList[i]["firstName"] + " " + gblOpenActBenList[i]["lastName"],
								 "lblSegBeneficiaryRelationValue":gblOpenActBenList[i]["relationShip"],
								 "lblSegBeneficiaryBenefitValue": gblOpenActBenList[i]["benifit"]}
			data.push(benifDataTemp);
		}
		kony.print(benifDataTemp);
		
		frmIBOpenNewSavingsCareAccConfirmation.segmentBenificiaries.setData(data);
		frmIBOpenNewSavingsCareAccConfirmation.lblSegBeneficiaryName.text=kony.i18n.getLocalizedString("keyBeneficiaryName");
		frmIBOpenNewSavingsCareAccConfirmation.lblSegBeneficiaryRelation.text=kony.i18n.getLocalizedString("keyBeneficiaryRelationship");
		frmIBOpenNewSavingsCareAccConfirmation.lblSegBeneficiaryBenefit.text=kony.i18n.getLocalizedString("keyBeneficiaryBenefit");
		frmIBOpenNewSavingsCareAccConfirmation.segmentBenificiaries.isVisible = true;
		frmIBOpenNewSavingsCareAccConfirmation.hbxBeneficiaryText.isVisible = true;
		frmIBOpenNewSavingsCareAccConfirmation.hboxNoBenificiaryMessage.isVisible = false;
		//frmIBOpenNewSavingsCareAccConfirmation.show();
		}else{
			frmIBOpenNewSavingsCareAccConfirmation.segmentBenificiaries.isVisible = false;
			frmIBOpenNewSavingsCareAccConfirmation.hbxBeneficiaryText.isVisible = false;
			frmIBOpenNewSavingsCareAccConfirmation.hboxNoBenificiaryMessage.isVisible = true;
		}
}

function pushBenificiaryDatatoArray(){
	gblOpenActBenList = [];
	
	for(var i=0;i<gblBenificiaryCount ; i++){
		if( frmIBOpenNewSavingsCareAcc["txtFirstName" + i] != undefined){
			var benifPercent = parseInt(frmIBOpenNewSavingsCareAcc["txtBenifit"+i].text);
			if(benifPercent > 0) {
				benifPercent = benifPercent + "";
				gblOpenActBenList.push({"firstName":frmIBOpenNewSavingsCareAcc["txtFirstName"+i].text,"lastName":frmIBOpenNewSavingsCareAcc["txtLastName"+i].text,"benifit":benifPercent,"relationShip":frmIBOpenNewSavingsCareAcc["comboRelationship"+i].selectedKeyValue[1]})
			}
		}
	}
	setBenificiariesSegment();

}

function validateBenOnClickNotSpecified() {
	
	if(gblBenificiaryCount > 0){
    for (var i = 0; i < gblBenificiaryCount; i++) {
    	kony.print("I::"+i)
        if (frmIBOpenNewSavingsCareAcc["hbxOASCBenDel"+i].isVisible && frmIBOpenNewSavingsCareAcc["txtFirstName" + i] != undefined) {
            var firsTxtName = frmIBOpenNewSavingsCareAcc["txtFirstName" + i];
            var scndTxtName = frmIBOpenNewSavingsCareAcc["txtLastName" + i];
            var befTxtName = frmIBOpenNewSavingsCareAcc["txtBenifit" + i];
            var relSel = frmIBOpenNewSavingsCareAcc["comboRelationship" + i].selectedKey;
            if (firsTxtName.text != "") {
            
                return false;
            } else if (scndTxtName.text != "") {
              
                return false;
            } else if (befTxtName.text != "") {
                
                return false;
            } else if (relSel != "P") {

                return false;
            }
        }
    }
	return true;
    }else{
    	return false;
    }

}

function setDynamicDatatoCompleteScreen(){
	if(gblOpenActBenList.length != 0){
	frmIBOpenNewSavingsCareAccComplete.lblOAPayIntAck.text = kony.i18n.getLocalizedString("keyBeneficiaries") + ":" + kony.i18n.getLocalizedString("keyBeneficiariesSpecify")
	for(var i = 0; i< gblOpenActBenList.length; i++){
		var lblBefNameCnfrm1 = new kony.ui.Label({
        "id": "lblBefNameCnfrm"+i,
        "isVisible": true,
        "text": kony.i18n.getLocalizedString("keyNameOpen"),
        "skin": "lblIB18pxBlack",
        "i18n_text": "kony.i18n.getLocalizedString('keyNameOpen')"
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 39
    }, {
        "toolTip": null
    });
    var lblBefNameValCnfrm1 = new kony.ui.Label({
        "id": "lblBefNameValCnfrm"+i,
        "isVisible": true,
        "text": gblOpenActBenList[i]["firstName"] + " " + gblOpenActBenList[i]["lastName"],
        "skin": "lblIB18pxBlack"
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 29
    }, {
        "toolTip": null
    });
    var hbxBefNameCnfrm1 = new kony.ui.Box({
        "id": "hbxBefNameCnfrm"+i,
        "isVisible": true,
        "position": constants.BOX_POSITION_AS_NORMAL,
        "skin": "hbox320pxpadding",
        "orientation": constants.BOX_LAYOUT_HORIZONTAL
    }, {
        "containerWeight": 1,
        "percent": true,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
        "margin": [0, 1, 0, 0],
        "padding": [0, 0, 0, 0],
        "vExpand": false,
        "marginInPixel": false,
        "paddingInPixel": false,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {});
    hbxBefNameCnfrm1.add(
    lblBefNameCnfrm1, lblBefNameValCnfrm1);
    var lblBefRsCnfrm1 = new kony.ui.Label({
        "id": "lblBefRsCnfrm"+i,
        "isVisible": true,
        "text": kony.i18n.getLocalizedString("keyRelation"),
        "skin": "lblIB18pxBlack",
        "i18n_text": "kony.i18n.getLocalizedString('keyRelation')"
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 52
    }, {
        "toolTip": null
    });
    var lblBefRsCnfrmVal1 = new kony.ui.Label({
        "id": "lblBefRsCnfrmVal"+i,
        "isVisible": true,
        "text": gblOpenActBenList[i]["relationShip"],
        "skin": "lblIB18pxBlack"
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 32
    }, {
        "toolTip": null
    });
    var hbxBefRsCnfrm1 = new kony.ui.Box({
        "id": "hbxBefRsCnfrm"+i,
        "isVisible": true,
        "position": constants.BOX_POSITION_AS_NORMAL,
        "skin": "hboxLightGrey320px",
        "orientation": constants.BOX_LAYOUT_HORIZONTAL
    }, {
        "containerWeight": 1,
        "percent": true,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
        "margin": [0, 1, 0, 0],
        "padding": [0, 0, 0, 0],
        "vExpand": false,
        "marginInPixel": false,
        "paddingInPixel": false,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {});
    hbxBefRsCnfrm1.add(
    lblBefRsCnfrm1, lblBefRsCnfrmVal1);
    var lblBefPerCnfrm1 = new kony.ui.Label({
        "id": "lblBefPerCnfrm"+i,
        "isVisible": true,
        "text": kony.i18n.getLocalizedString("keyBenfit"),
        "skin": "lblIB18pxBlack",
        "i18n_text": "kony.i18n.getLocalizedString('keyBenfit')"
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 39
    }, {
        "toolTip": null
    });
    var lblBefPerCnfrmVal1 = new kony.ui.Label({
        "id": "lblBefPerCnfrmVal"+i,
        "isVisible": true,
        "text": gblOpenActBenList[i]["benifit"],
        "skin": "lblIB18pxBlack"
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 29
    }, {
        "toolTip": null
    });
    var hbxBefPerCnfrm1 = new kony.ui.Box({
        "id": "hbxBefPerCnfrm"+i,
        "isVisible": true,
        "position": constants.BOX_POSITION_AS_NORMAL,
        "skin": "hbox320pxpadding",
        "orientation": constants.BOX_LAYOUT_HORIZONTAL
    }, {
        "containerWeight": 2,
        "percent": true,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
        "margin": [0, 1, 0, 0],
        "padding": [0, 0, 0, 0],
        "vExpand": false,
        "marginInPixel": false,
        "paddingInPixel": false,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {});
    hbxBefPerCnfrm1.add(
    lblBefPerCnfrm1, lblBefPerCnfrmVal1);
    var lineOASC1 = new kony.ui.Line({
        "id": "lineOASC"+i,
        "isVisible": true,
        "skin": "lineGrey"
    }, {
        "thickness": 1,
        "margin": [0, 0, 0, 0],
        "marginInPixel": false,
        "paddingInPixel": false
    }, {});
		 frmIBOpenNewSavingsCareAccComplete.vboxBenificiaries.add(
        hbxBefNameCnfrm1, hbxBefRsCnfrm1,hbxBefPerCnfrm1,lineOASC1);
		}
		frmIBOpenNewSavingsCareAccComplete.hboxBenificiariesList.isVisible = true;
			frmIBOpenNewSavingsCareAccComplete.hboxNoBenificiaryMessage.isVisible = false;
		}else{
			frmIBOpenNewSavingsCareAccComplete.lblOAPayIntAck.text = kony.i18n.getLocalizedString("keyBeneficiaries") + ":" + kony.i18n.getLocalizedString("keyBeneficiariesNotSpecify")
			frmIBOpenNewSavingsCareAccComplete.hboxBenificiariesList.isVisible = false;
			frmIBOpenNewSavingsCareAccComplete.hboxNoBenificiaryMessage.isVisible = true;
		}
}


function changeMasterdatainRelation(){
	 var currLocale = kony.i18n.getCurrentLocale();
	 if(gblRelationshipData != null && gblRelationshipData.length > 0) {
 
	   	if (currLocale == "en_US") {
			gblRelationshipData[0].splice(0, 1, ["P",kony.i18n.getLocalizedString("keyBeneficiaryPlsSel")])
	   		masterData = gblRelationshipData[0];
	   	}else{
	   		gblRelationshipData[1].splice(0, 1, ["P",kony.i18n.getLocalizedString("keyBeneficiaryPlsSel")])
	   		masterData = gblRelationshipData[1];
	   	}
	   	for(var i=0;i<gblBenificiaryCount ; i++){
			if( frmIBOpenNewSavingsCareAcc["comboRelationship" + i] != undefined){
				var currentVal = frmIBOpenNewSavingsCareAcc["comboRelationship" + i].selectedKey;
				frmIBOpenNewSavingsCareAcc["comboRelationship" + i].masterData = masterData;
				frmIBOpenNewSavingsCareAcc["comboRelationship" + i].selectedKey = currentVal;
			
			}}
		}
}

function getRelationIndex(tempData,currentRelValue){
	var result = -1;
	for( var j = 0 ; j < tempData.length; j++ ) {
	    if( tempData[j][1] == currentRelValue ) {
	        result = j;
	    }
	}
	return result;
}

function changeRelationTab(){
	var locale = kony.i18n.getCurrentLocale();
	for(var i=0; i < gblOpenActBenList.length ; i++){
		var currentRelValue = gblOpenActBenList[i]["relationShip"];
		var curIdx = getRelationIndex(gblRelationshipData[0],currentRelValue);
		var curIdx2 = getRelationIndex(gblRelationshipData[1],currentRelValue);
		if( curIdx > 0 && locale == "th_TH") {
			newRelValue = gblRelationshipData[1][curIdx];
			gblOpenActBenList[i]["relationShip"] = newRelValue[1]
		} else if( curIdx2 > 0 && locale == "en_US"){
			newRelValue = gblRelationshipData[0][curIdx2];
			gblOpenActBenList[i]["relationShip"] = newRelValue[1]
		}
	}
}

function changeRelationOnComplete(){
	changeRelationTab();
   	for(var i=0;i<gblOpenActBenList.length ; i++){
		if( frmIBOpenNewSavingsCareAccComplete["lblBefRsCnfrmVal" + i] != undefined){
				frmIBOpenNewSavingsCareAccComplete["lblBefRsCnfrmVal" + i].text = gblOpenActBenList[i]["relationShip"];
		}}
}
