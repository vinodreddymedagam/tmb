var ie_timeout = 30000;
function onClickFindATMIB(){
	try{
		
		frmIBATMBranch.btnAtm.skin = "tabIBFindATMLefthover";
		frmIBATMBranch.btnBrch.skin = "tabIBFindATMMiddle";
		frmIBATMBranch.btnExgBooth.skin = "tabIBFindATMRight";
		gblBrachTypeIB = "ATM";
		if(frmIBATMBranch.hboxListDetails.isVisible){
			frmIBATMBranch.hboxListDetails.setVisibility(false);
			frmIBATMBranch.hboxImage.setVisibility(true);
		}
		if(gblFindTMBViewTypeIB == "MapView")
		{
			getGeoLocationIB();
		}else if(gblFindTMBViewTypeIB == "ListView"){
			setMapDataIB();
		}
	}catch(e){
		
	}
}

function onClickBranchIB(){
	try{
		
		gblBrachTypeIB = "BRANCH";
		frmIBATMBranch.btnAtm.skin = "tabIBFindATMLeft";
		frmIBATMBranch.btnBrch.skin = "tabIBFindATMMiddlehover";
		frmIBATMBranch.btnExgBooth.skin = "tabIBFindATMRight";
		if(frmIBATMBranch.hboxListDetails.isVisible){
			frmIBATMBranch.hboxListDetails.setVisibility(false);
			frmIBATMBranch.hboxImage.setVisibility(true);
		}
		if(gblFindTMBViewTypeIB == "MapView"){
			setMapDataIB();	
		}else if(gblFindTMBViewTypeIB == "ListView"){
			setMapDataIB();
		}
	}catch(e){
		
	}
}

function onClickEXIB(){
	try{
		
		gblBrachTypeIB = "EX";
		frmIBATMBranch.btnAtm.skin = "tabIBFindATMLeft";
		frmIBATMBranch.btnBrch.skin = "tabIBFindATMMiddle";
		frmIBATMBranch.btnExgBooth.skin = "tabIBFindATMRighthover";
		if(frmIBATMBranch.hboxListDetails.isVisible){
			frmIBATMBranch.hboxListDetails.setVisibility(false);
			frmIBATMBranch.hboxImage.setVisibility(true);
		}
		if(gblFindTMBViewTypeIB == "MapView"){
			setMapDataIB();	
		}else if(gblFindTMBViewTypeIB == "ListView"){
			setMapDataIB();
		}
	}catch(e){
		
	}
}

function getGeoLocationIB(){
	try{
		
		var locationData = [];
		showLoadingScreenPopup();	
		
		var options = null;
		if (isIE()) {
			options = {maximumAge:Infinity, timeout:ie_timeout};
		}	
		
		if(gblLatitudeIB == "" || gblLongitudeIB == ""){
			gblIsCurrentLcoationIB = false;
			locationData = kony.location.getCurrentPosition(successcallbackIB, errorcallbackIB, options);
		}else{
			goATMBranchesIB();
		}
		frmIBATMBranch.show();
		
	}catch(e){
		
	}
}

function successcallbackIB(position){
	try{
		
		showLoadingScreenPopup();
  		if(position.coords.latitude != "" || position.coords.longitude != ""){
  			gblLatitudeIB = position.coords.latitude;
  			gblLongitudeIB = position.coords.longitude;
  			gblIsCurrentLcoationIB = true;
  			gblCurrentLatIB = gblLatitudeIB;
  			gblCurrentLonIB = gblLongitudeIB;
  			goATMBranchesIB();
  		}else{
  			gblLatitudeIB = kony.i18n.getLocalizedString("TMBLatitude");
  			gblLongitudeIB = kony.i18n.getLocalizedString("TMBLongitude");
  			goATMBranchesIB();
  			alert("**No GeoLocation found**");
  		}
	}catch(e){
		
	}
}

function errorcallbackIB(positionerror){
	try{
		
		//showLoadingScreenPopup();
  		var errorMesg = "Error code:::::" + positionerror.code + "/@@@Message:::::" + positionerror.message;
  		
  		if(positionerror.code == 103){
  			
  		}else if(positionerror.code == 105){
  			
  		}
  		gblIsCurrentLcoationIB = false;
  		gblLatitudeIB = kony.i18n.getLocalizedString("TMBLatitude");
  		gblLongitudeIB = kony.i18n.getLocalizedString("TMBLongitude");
  		
  		//IE && 1 = PERMISSION_DENIED
  		if (isIE() && positionerror.code == 1) {
  			ie_timeout = 0; //next time don't wait
	  		if (typeof google === "undefined" || typeof google.maps === "undefined") {
	  			if(!document.getElementById('fix-ie-googlemap')) {
		  			var head = document.getElementsByTagName('head')[0];
					var script = document.createElement('script');
					script.id = 'fix-ie-googlemap';
					script.src = "//maps-api-ssl.google.com/maps/api/js?sensor=false&callback=$KW.Map.setUpInteractiveCanvasMap";
					head.appendChild(script);
					//console.log("google.maps load manual");
				}
	  		}else{
	  			//console.log("google.maps loaded");
	  		}
  		}
  		
  		goATMBranchesIB();
	}catch(e){
		
	}
}

function goATMBranchesIB(){
	try{
		frmIBATMBranch.btnSearchIcon.skin = "btnFindSearch"
		frmIBATMBranch.btnMapView.skin = "btnIBTabType2Focus"
		frmIBATMBranch.btnListView.skin = "btnIBTabType2Normal"
		if(frmIBATMBranch.hboxSearch.isVisible){
			frmIBATMBranch.hboxSearch.setVisibility(false)
		}
		try{
			frmIBATMBranch.hboxMapTab.setVisibility(true);
		}catch(e){
		}
		try{
			frmIBATMBranch.hboxViewTab.setVisibility(false);
		}catch(e){
		}
		try{
			frmIBATMBranch.hboxImage.setVisibility(true);
		}catch(e){
		}
		try{
			frmIBATMBranch.hboxListDetails.setVisibility(false);
		}catch(e){
		}
		//dismissLoadingScreenPopup();
		setMapDataIB();
		gblFindTMBViewTypeIB = "MapView";
		
	}catch(e){
		//alert(e)
	}
}

function setMapDataIB(){
	try{
		
		if(gblLatitudeIB == "" && gblLongitudeIB == ""){
			gblLatitudeIB = kony.i18n.getLocalizedString("TMBLatitude");
  			gblLongitudeIB = kony.i18n.getLocalizedString("TMBLongitude");
		}
		var locatorInputParams = {};
		showLoadingScreenPopup();
		locatorInputParams["applicationchannel"] = "IB";
		locatorInputParams["branchType"] = gblBrachTypeIB;
		locatorInputParams["latitude"] = gblLatitudeIB;
		locatorInputParams["longitude"] = gblLongitudeIB;
		invokeServiceSecureAsync("ATMLocatorService", locatorInputParams, successSetMapDataIB)
	}catch(e){
		
	}
}
function successSetMapDataIB(status, resulttable){
	try{
		dismissLoadingScreen();
		gblFindTMBMapData = [];
		gblFindTMBMapData = resulttable;
		if(gblFindTMBViewTypeIB == "MapView")
			if(isFindTMBSearch){
				setDataMapCallBackIB(400, gblFindTMBListData);
			}else{
				setDataMapCallBackIB(status, resulttable)
			}
			
		else if(gblFindTMBViewTypeIB == "ListView")
			if(isFindTMBSearch){
				onClickSearchBtnIB();
			}else{
				onSuccesATMSearchIB(status, gblFindTMBMapData);
			}
			
	}catch(e){
		
	}

}

function setDataMapCallBackIB(status, resulttable){
	try{
		
		//
		var locatorResultSetIB = [];
		var tempLocaterDataIB = [];
		if(status == 400){
			if(resulttable["opstatus"] == 0){
				frmIBATMBranch.mapATMBranch.widgetDataMapForCallout = {lblName:"tmbName", lblAddress:"tmbAddress", btnGetDirection:"btnGetDirection", btnDetals:"btnDetals"};
				if(gblIsCurrentLcoationIB){
					
					tempLocaterDataIB = { 	lat: gblCurrentLatIB, 
											lon: gblCurrentLonIB,
											image: "currentpin.png",
											showcallout: true,
											calloutData:{tmbName:""+kony.i18n.getLocalizedString("FindTMB_Youarehere"), tmbAddress:"",btnGetDirection:{"isVisible":false},btnDetals:{"isVisible":false}}
										};
					locatorResultSetIB.push(tempLocaterDataIB);
				}
				for (var i = 0; i < resulttable["mapDataOfATMLocator"].length; i++) {
					tempLocaterDataIB = [];
					var address = "";
					var name = ""
					var workingHrs = "";
					var currentLocale = kony.i18n.getCurrentLocale();
					if(gblBrachTypeIB == "ATM"){
						if(currentLocale == "en_US"){
							name = resulttable["mapDataOfATMLocator"][i]["ATM_ADM_NAME_EN"];
							address = resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_ADDRESS_EN"];
							workingHrs = " "; 
						}else if(currentLocale == "th_TH"){
							name = resulttable["mapDataOfATMLocator"][i]["ATM_ADM_NAME_TH"];
							address = resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_ADDRESS_TH"];
							workingHrs = " "; 
						}
						tempLocaterDataIB = { lat: resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_LADTITUDE_INFO"], 
											lon: resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_LONGTITUDE_INFO"],
											image: "locator_pin.png",
											showcallout: true,
											calloutData:{tmbName:name, tmbAddress:address,btnGetDirection:{"isVisible":true,"text":kony.i18n.getLocalizedString("FindTMB_GetDirections")},btnDetals:{"isVisible":true,"text":kony.i18n.getLocalizedString("FindTMB_Details")}},
											branchName: name,
											addressLine: address,
											primaryPhoneNo: resulttable["mapDataOfATMLocator"][i]["PRIMARY_PHONE_NUMBER"],
											otherPhoneNo: resulttable["mapDataOfATMLocator"][i]["OTHER_PHONE_NUMBER"],
											faxNumber: resulttable["mapDataOfATMLocator"][i]["OTHER_PHONE_NUMBER"],
											officeHrs: workingHrs
											};
						
					}else if(gblBrachTypeIB == "BRANCH"){
						if(currentLocale == "en_US"){
							name = resulttable["mapDataOfATMLocator"][i]["BRANCH_NAME_EN"];
							address = resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_ADDRESS_EN"];
							workingHrs = resulttable["mapDataOfATMLocator"][i]["BRANCH_WORKING_TIME_EN"];
						}else if(currentLocale == "th_TH"){
							name = resulttable["mapDataOfATMLocator"][i]["BRANCH_NAME_TH"];
							address = resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_ADDRESS_TH"];
							workingHrs = resulttable["mapDataOfATMLocator"][i]["BRANCH_WORKING_TIME_TH"];
						}
						tempLocaterDataIB = { lat: resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_LADTITUDE_INFO"], 
											lon: resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_LONGTITUDE_INFO"],
											image: "locator_pin.png",
											showcallout: true,
											calloutData:{tmbName:name, tmbAddress:address,btnGetDirection:{"isVisible":true,"text":kony.i18n.getLocalizedString("FindTMB_GetDirections")},btnDetals:{"isVisible":true,"text":kony.i18n.getLocalizedString("FindTMB_Details")}},
											branchName: name,
											addressLine: address,
											primaryPhoneNo: resulttable["mapDataOfATMLocator"][i]["PRIMARY_PHONE_NUMBER"],
											otherPhoneNo: resulttable["mapDataOfATMLocator"][i]["OTHER_PHONE_NUMBER"],
											faxNumber: resulttable["mapDataOfATMLocator"][i]["OTHER_PHONE_NUMBER"],
											officeHrs: workingHrs};
					}if(gblBrachTypeIB == "EX"){
						if(currentLocale == "en_US"){
							name = resulttable["mapDataOfATMLocator"][i]["EXCHANGE_BOOTH_NAME_EN"];
							if(name != null || name != undefined || name != ""){
								name = " ";
							}else{
								name = resulttable["mapDataOfATMLocator"][i]["EXCHANGE_BOOTH_NAME_EN"];
							}
							address = resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_ADDRESS_EN"];
							workingHrs = resulttable["mapDataOfATMLocator"][i]["BOOTH_WORKING_TIME_EN"];
						}else if(currentLocale == "th_TH"){
							name = resulttable["mapDataOfATMLocator"][i]["EXCHANGE_BOOTH_NAME_TH"];
							if(name != null || name != undefined || name != ""){
								name = " ";
							}else{
								name = resulttable["mapDataOfATMLocator"][i]["EXCHANGE_BOOTH_NAME_TH"];
							}
							address = resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_ADDRESS_TH"];
							workingHrs = resulttable["mapDataOfATMLocator"][i]["BOOTH_WORKING_TIME_TH"];
						}
						tempLocaterDataIB = { lat: resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_LADTITUDE_INFO"], 
											lon: resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_LONGTITUDE_INFO"],
											image: "locator_pin.png",
											showcallout: true,
											calloutData:{tmbName:name, tmbAddress:address,btnGetDirection:{"isVisible":true,"text":kony.i18n.getLocalizedString("FindTMB_GetDirections")},btnDetals:{"isVisible":true,"text":kony.i18n.getLocalizedString("FindTMB_Details")}},
											branchName: name,
											addressLine: address,
											primaryPhoneNo: resulttable["mapDataOfATMLocator"][i]["PRIMARY_PHONE_NUMBER"],
											otherPhoneNo: resulttable["mapDataOfATMLocator"][i]["OTHER_PHONE_NUMBER"], 
											faxNumber: resulttable["mapDataOfATMLocator"][i]["OTHER_PHONE_NUMBER"],
											officeHrs: workingHrs};
					}
					locatorResultSetIB.push(tempLocaterDataIB);
					/*if(i == 0){
						
						frmIBATMBranch.mapATMBranch.navigateToLocation(locatorResultSetIB);
					}*/
				}
				
				
				dismissLoadingScreenPopup();
				//frmIBATMBranch.show();
				if(locatorResultSetIB.length == 0){
					
					frmIBATMBranch.hboxMapTab.setVisibility(false);
					alert(kony.i18n.getLocalizedString("FindTMBResultErrorMsg"));
				}else{
					frmIBATMBranch.hboxMapTab.setVisibility(true);
					var ua = navigator.userAgent;
					var isiPad = /iPad/i.test(ua);
					if(isiPad)
						frmIBATMBranch.mapATMBranch.calloutWidth = 30;
					else	frmIBATMBranch.mapATMBranch.calloutWidth = 0;	
					frmIBATMBranch.mapATMBranch.locationData = locatorResultSetIB;
					frmIBATMBranch.mapATMBranch.navigateToLocation(locatorResultSetIB[0]);
				}
				
			}else{
				
				dismissLoadingScreenPopup();
				alert(kony.i18n.getLocalizedString("ECGenericError"));
				//frmIBATMBranch.show();
			}
		}else{
			dismissLoadingScreenPopup();
			alert(kony.i18n.getLocalizedString("ECGenericError"));
		}
	}catch(e){
		
	}
}

function onSelectMapPinIB(mapLocationData){
	try{
		
		var btnSkin = frmIBATMBranch.btnSearchIcon.skin;
		if(btnSkin == "btnFindSearchFocus"){
			frmIBATMBranch.btnSearchIcon.skin = "btnFindSearch";
			frmIBATMBranch.hboxSearch.setVisibility(false);
		}
		currentLocDetailsIB = mapLocationData;
		/*var name = "";
		var address = "";
		name = mapLocationData.branchName;
		address = mapLocationData.addressLine;
		if(name == null || name == ""){popIBATMBranch.lblName.text = "";}else{popIBATMBranch.lblName.text = name;}
		if(address == null || address == ""){popIBATMBranch.lblAddress.text = "";}else{popIBATMBranch.lblAddress.text = address;}
		//var popUpContext= {"widget":popIBATMBranch.hboxMapTemplate, "anchor":"center", "sizetoanchorwidth":false};
		//popIBATMBranch.setContext(popUpContext);
		popIBATMBranch.show();*/
	}catch(e){
		
	}
}

function onClickDetailsIB(){
	try{
		
		
		var phoneNumber = "";
		var faxNumber = "";
		var officeHrs = "";
		//changing the below line to fix the issue DEF16215. In future when Fax number is populated then it has to handlied appropriately
		//phoneNumber = currentLocDetailsIB.primaryPhoneNo+", "+currentLocDetailsIB.otherPhoneNo;
		if(currentLocDetailsIB.primaryPhoneNo !=""){
			phoneNumber = currentLocDetailsIB.primaryPhoneNo
		}
		if(currentLocDetailsIB.faxNumber !=""){
			faxNumber = currentLocDetailsIB.faxNumber;
		}
		if(currentLocDetailsIB.officeHrs != null && currentLocDetailsIB.officeHrs != ""){
			officeHrs = currentLocDetailsIB.officeHrs;
		}
		//popIBATMBranch.dismiss();
		frmIBATMBranch.hboxImage.setVisibility(false);
		frmIBATMBranch.hboxListDetails.setVisibility(true);
		if(gblBrachTypeIB == "ATM"){
			frmIBATMBranch.lblTitle.text = kony.i18n.getLocalizedString("FindTMB_ATMDetails");
		}else if(gblBrachTypeIB == "BRANCH"){
			frmIBATMBranch.lblTitle.text = kony.i18n.getLocalizedString("FindTMB_BranchDetails");
		}else if(gblBrachTypeIB == "EX"){
			frmIBATMBranch.lblTitle.text = kony.i18n.getLocalizedString("FindTMB_ExgBoothDetails");
		}
		frmIBATMBranch.lblBRName.text = currentLocDetailsIB.branchName//popIBATMBranch.lblName.text;
		frmIBATMBranch.lblAddress.text = currentLocDetailsIB.addressLine//popIBATMBranch.lblAddress.text;
		frmIBATMBranch.lblPhoneNo.text = phoneNumber;
		frmIBATMBranch.lblFax.text = faxNumber;
		frmIBATMBranch.lblWorkingHrs.text = officeHrs;
		if(gblBrachTypeIB == "ATM"){
			if(frmIBATMBranch.hboxTel.isVisible){
				frmIBATMBranch.hboxTel.setVisibility(false);
				frmIBATMBranch.hboxFax.setVisibility(false);
				frmIBATMBranch.hboxOfficeHrs.setVisibility(false);
			}
		}else{
			if(!frmIBATMBranch.hboxTel.isVisible){
				frmIBATMBranch.hboxTel.setVisibility(true);
				frmIBATMBranch.hboxFax.setVisibility(true);
				frmIBATMBranch.hboxOfficeHrs.setVisibility(true);
			}
		}
	}catch(e){
		
	}
}

function onListView(){
	try{
		
		frmIBATMBranch.btnMapView.skin = "btnIBTabType2Normal";
		frmIBATMBranch.btnListView.skin = "btnIBTabType2Focus";
		frmIBATMBranch.btnSearchIcon.skin = "btnFindSearch";
		if(frmIBATMBranch.hboxSearch.isVisible){
			frmIBATMBranch.hboxSearch.setVisibility(false)
		}
		frmIBATMBranch.hboxMapTab.setVisibility(false);
		frmIBATMBranch.hboxViewTab.setVisibility(true);
		frmIBATMBranch.hboxImage.setVisibility(true);
		frmIBATMBranch.hboxListDetails.setVisibility(false);
		//provinceIDIB = "";
		//districtIDIB = "";
		if(gblFindTMBViewTypeIB == "MapView"){
			if(isFindTMBSearch){
				onSuccesATMSearchIB(400, gblFindTMBListData);
			}else{
				onSuccesATMSearchIB(400, gblFindTMBMapData);
			}
		}
		gblFindTMBViewTypeIB = "ListView";
		if(provinceIDIB == "")
			callATMProvinceDataIB();
		
	}catch(e){
		
	}
}

function onclickBySearchIconIB(){
	try{
		
		var btnSkin = frmIBATMBranch.btnSearchIcon.skin;
		/*var province = [];
		province.push([1,kony.i18n.getLocalizedString("FindTMB_Province")]);
		frmIBATMBranch.btnProvince.masterData = province;
		var district = [];
		district.push([1,kony.i18n.getLocalizedString("FindTMB_District")]);
		frmIBATMBranch.btnDistrict.masterData = district;
		frmIBATMBranch.txtKeyword.text = "";
		frmIBATMBranch.btnDistrict.setEnabled(false);
		provinceIDIB = "";
		districtIDIB = "";*/
		if(btnSkin == "btnFindSearch"){
			frmIBATMBranch.btnSearchIcon.skin = "btnFindSearchFocus";
			frmIBATMBranch.hboxSearch.setVisibility(true);
			//frmIBATMBranch.txtKeyword.text = "";
		}else if(btnSkin == "btnFindSearchFocus"){
			frmIBATMBranch.btnSearchIcon.skin = "btnFindSearch";
			frmIBATMBranch.hboxSearch.setVisibility(false);
		}
		if(gblFindTMBViewTypeIB == "MapView"){
			if(provinceIDIB == ""){
				callATMProvinceDataIB();
			}
		}
		if(provinceIDIB == 1 || provinceIDIB == ""){
			var district = [];
			district.push([1,kony.i18n.getLocalizedString("FindTMB_District")]);
			frmIBATMBranch.btnDistrict.masterData = district;
			frmIBATMBranch.btnDistrict.setEnabled(false);
		}
		
		//callATMProvinceDataIB();
	}catch(e){
		
	}
}

function callATMProvinceDataIB(){
	try{
		
		showLoadingScreenPopup();
		gblATMSearchFlagIB = "Province"
		var inputParams = {};
		invokeServiceSecureAsync("GetProvinceID", inputParams, getProvinceDateCallBackIB);
	}catch(e){
		
	}
}

function callATMDistrictDataIB() {
	try{
		
		showLoadingScreenPopup();
		gblATMSearchFlagIB = "District";
		var inputParams = {};
		inputParams["provinceCD"] = provinceIDIB;
		invokeServiceSecureAsync("GetDistrictID", inputParams, getProvinceDateCallBackIB);
	}catch(e){
		
	}
}

function getProvinceDateCallBackIB(status, resulttable) {
	try{
		
		if (status == 400) {
			if (resulttable["opstatus"] == 0) {
				if (gblATMSearchFlagIB == "Province") {
					if (resulttable["state"][0].length != 0) {
						gblProvinceResultSet = resulttable["state"];
						populateProvinceDistrictDataIB(resulttable["state"]);
					}
				}else if(gblATMSearchFlagIB == "District") {
					if (resulttable["district"][0].length != 0) {
						gblDistrictResultSet = resulttable["district"];
						populateProvinceDistrictDataIB(resulttable["district"]);
					}
				} 
			}else{
				dismissLoadingScreenPopup();
				alert(kony.i18n.getLocalizedString("ECGenericError"));
			}
		}else{
			dismissLoadingScreenPopup();
			alert(kony.i18n.getLocalizedString("ECGenericError"));
		}
	}catch(e){
		
	}
}

function populateProvinceDistrictDataIB(provinceDistrictResultSet){
	try{
		
		var resultTableDataIB = [];
		var currentLocale = kony.i18n.getCurrentLocale();
		if(gblATMSearchFlagIB == "Province"){
			resultTableDataIB.push([1,kony.i18n.getLocalizedString("FindTMB_Province")]);
		}else if(gblATMSearchFlagIB == "District"){
			resultTableDataIB.push([1,kony.i18n.getLocalizedString("FindTMB_District")]);
		}
		for (var i = 0; i < provinceDistrictResultSet.length; i++){
			var tempData = [];
			var provinceDTName = "";
			if(gblATMSearchFlagIB == "Province"){
				if(currentLocale == "en_US"){
					provinceDTName = provinceDistrictResultSet[i].ProvinceNameEN;
				}else if(currentLocale ==  "th_TH"){
					provinceDTName = provinceDistrictResultSet[i].ProvinceNameTH;
				}
				resultTableDataIB.push([ provinceDistrictResultSet[i].ProvinceCD, provinceDTName ]);
			}else if(gblATMSearchFlagIB == "District"){
				if(currentLocale == "en_US"){
					provinceDTName = provinceDistrictResultSet[i].DistrictNameEN;
				}else if(currentLocale ==  "th_TH"){
					provinceDTName = provinceDistrictResultSet[i].DistrictNameTH;
				}
				resultTableDataIB.push([ provinceDistrictResultSet[i].DistrictCD, provinceDTName]);
			}
		}
		if(gblATMSearchFlagIB == "Province"){
			frmIBATMBranch.btnProvince.masterData = resultTableDataIB;
		}else if(gblATMSearchFlagIB == "District"){
			frmIBATMBranch.btnDistrict.masterData = resultTableDataIB;
		}
		dismissLoadingScreenPopup();
	}catch(e){
		
	}
}

function onSelectProvince(){
	try{
		
		provinceIDIB = frmIBATMBranch.btnProvince.selectedKey;
		
		if(provinceIDIB != null || provinceIDIB != ""){
			frmIBATMBranch.btnDistrict.setEnabled(true);
			if(provinceIDIB != 1){
				callATMDistrictDataIB();
			}else{
				var dist = [];
				dist.push([1,kony.i18n.getLocalizedString("FindTMB_District")])
				frmIBATMBranch.btnDistrict.masterData = dist;
				districtIDIB = "";
				frmIBATMBranch.btnDistrict.setEnabled(false);
			}
		}
	}catch(e){
		
	}
}

function onSelectDistrict(){
	try{
		
		districtIDIB = frmIBATMBranch.btnDistrict.selectedKey;
		
		
	}catch(e){
		
	}
}

function onClickSearchBtnIB(){
	try{
		
		isFindTMBSearch = true;
		var textKey = frmIBATMBranch.txtKeyword.text;
		
		if(textKey == "" && provinceIDIB == "" && districtIDIB == ""){
			if(!frmIBATMBranch.hboxSearch.isVisible)
				frmIBATMBranch.hboxSearch.setVisibility(true);
			if(frmIBATMBranch.hboxViewTab.isVisible)
				frmIBATMBranch.hboxViewTab.setVisibility(false);
			alert(kony.i18n.getLocalizedString("FindTMB_SearchValidation1"));
		}else if(textKey == "" && provinceIDIB != "" && districtIDIB == ""){
			if(!frmIBATMBranch.hboxSearch.isVisible)
				frmIBATMBranch.hboxSearch.setVisibility(true);
			if(frmIBATMBranch.hboxViewTab.isVisible)
				frmIBATMBranch.hboxViewTab.setVisibility(false);
			alert(kony.i18n.getLocalizedString("FindTMB_SearchValidation2"));
		}else{
			frmIBATMBranch.btnSearchIcon.skin = "btnFindSearch";
			frmIBATMBranch.hboxSearch.setVisibility(false);
			if(gblFindTMBViewTypeIB == "MapView"){
				frmIBATMBranch.hboxMapTab.setVisibility(false);
				frmIBATMBranch.hboxViewTab.setVisibility(true);
				frmIBATMBranch.btnMapView.skin = "btnIBTabType2Normal";
				frmIBATMBranch.btnListView.skin = "btnIBTabType2Focus";
				gblFindTMBViewTypeIB = "ListView";
			}
			callSearch();
		}
	}catch(e){
		
	}
}

function callSearch(){
	try{
		//added lines to fix the DEF16893
		if(provinceIDIB=="1" || districtIDIB == "1")
		{
			provinceIDIB="";
			districtIDIB="";
		}
		var inputParams = {};
		showLoadingScreenPopup();
		inputParams["branchType"] = gblBrachTypeIB;
		inputParams["provinceId"] = provinceIDIB;
		inputParams["districtId"] = districtIDIB;
		inputParams["serachText"] = frmIBATMBranch.txtKeyword.text;
		invokeServiceSecureAsync("ATMLocatorService", inputParams, successCallSearch);
		
	}catch(e){
		
	}
}

function successCallSearch(status, resulttable){
	try{
		
		var gblFindTMBListData = [];
		gblFindTMBListData = resulttable;
		onSuccesATMSearchIB(status, gblFindTMBListData);
	}catch(e){
		
	}
}

function onSuccesATMSearchIB(status, resulttable){
	try{
		
		var listDataIB = [];
		var currentLocale = kony.i18n.getCurrentLocale();
		if(status == 400){
			if (resulttable["opstatus"] == 0) {
				for (var i = 0; i < resulttable["mapDataOfATMLocator"].length; i++) {
					var tempData = [];
					var address = "";
					var name = "";
					var workingHrs = "";
					if(gblBrachTypeIB == "ATM"){
						if(currentLocale == "en_US"){
							name = resulttable["mapDataOfATMLocator"][i]["ATM_ADM_NAME_EN"];
							address = resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_ADDRESS_EN"];
							//workingHrs = resulttable["mapDataOfATMLocator"][i]["BRANCH_WORKING_TIME_EN"];
						}else if(currentLocale == "th_TH"){
							name = resulttable["mapDataOfATMLocator"][i]["ATM_ADM_NAME_TH"];
							address = resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_ADDRESS_TH"];
							//workingHrs = resulttable["mapDataOfATMLocator"][i]["BRANCH_WORKING_TIME_TH"];
						}
						tempData = { 
									 lat: resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_LADTITUDE_INFO"],
									 lon: resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_LONGTITUDE_INFO"],
									 lblSegName: name,
									 engName: resulttable["mapDataOfATMLocator"][i]["ATM_ADM_NAME_EN"],
									 thaiName: resulttable["mapDataOfATMLocator"][i]["ATM_ADM_NAME_TH"],
									 lblSegAddress: address,
									 engAddress: resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_ADDRESS_EN"],
									 thaiAddress: resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_ADDRESS_TH"],
									 imgArrow: "bg_arrow_right.png",
									 lblPhoneNo:resulttable["mapDataOfATMLocator"][i]["PRIMARY_PHONE_NUMBER"],
									 lblFaxno: resulttable["mapDataOfATMLocator"][i]["OTHER_PHONE_NUMBER"],
									 lblWorkingHrs: workingHrs,
									 engWorkingHrs: workingHrs,
									 thaiWorkingHrs: workingHrs
									};
					}else if(gblBrachTypeIB == "BRANCH"){
						if(currentLocale == "en_US"){
							name = resulttable["mapDataOfATMLocator"][i]["BRANCH_NAME_EN"];
							address = resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_ADDRESS_EN"];
							workingHrs = resulttable["mapDataOfATMLocator"][i]["BRANCH_WORKING_TIME_EN"];
						}else if(currentLocale == "th_TH"){
							name = resulttable["mapDataOfATMLocator"][i]["BRANCH_NAME_TH"];
							address = resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_ADDRESS_TH"];
							workingHrs = resulttable["mapDataOfATMLocator"][i]["BRANCH_WORKING_TIME_TH"];
						}
						tempData = { 
									 lat: resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_LADTITUDE_INFO"],
									 lon: resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_LONGTITUDE_INFO"],
									 lblSegName: name,
									 engName: resulttable["mapDataOfATMLocator"][i]["BRANCH_NAME_EN"],
									 thaiName: resulttable["mapDataOfATMLocator"][i]["BRANCH_NAME_TH"],
									 lblSegAddress: address,
									 engAddress: resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_ADDRESS_EN"],
									 thaiAddress: resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_ADDRESS_TH"],
									 imgArrow: "bg_arrow_right.png",
									 lblPhoneNo: resulttable["mapDataOfATMLocator"][i]["PRIMARY_PHONE_NUMBER"],
									 lblFaxno: resulttable["mapDataOfATMLocator"][i]["OTHER_PHONE_NUMBER"],
									 lblWorkingHrs: workingHrs,
									 engWorkingHrs: resulttable["mapDataOfATMLocator"][i]["BRANCH_WORKING_TIME_EN"],
									 thaiWorkingHrs: resulttable["mapDataOfATMLocator"][i]["BRANCH_WORKING_TIME_TH"]
									};
					}else if(gblBrachTypeIB == "EX"){
						if(currentLocale == "en_US"){
							name = resulttable["mapDataOfATMLocator"][i]["EXCHANGE_BOOTH_NAME_EN"];
							address = resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_ADDRESS_EN"];
							workingHrs = resulttable["mapDataOfATMLocator"][i]["BOOTH_WORKING_TIME_EN"];
						}else if(currentLocale == "th_TH"){
							name = resulttable["mapDataOfATMLocator"][i]["EXCHANGE_BOOTH_NAME_TH"];
							address = resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_ADDRESS_TH"];
							workingHrs = resulttable["mapDataOfATMLocator"][i]["BOOTH_WORKING_TIME_TH"];
						}
						tempData = { 
									 lat: resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_LADTITUDE_INFO"],
									 lon: resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_LONGTITUDE_INFO"],
									 lblSegName: name,
									 engName: resulttable["mapDataOfATMLocator"][i]["EXCHANGE_BOOTH_NAME_EN"],
									 thaiName: resulttable["mapDataOfATMLocator"][i]["EXCHANGE_BOOTH_NAME_TH"],
									 lblSegAddress: address,
									 engAddress: resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_ADDRESS_EN"],
									 thaiAddress: resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_ADDRESS_TH"],
									 imgArrow: "bg_arrow_right.png",
									 lblPhoneNo: resulttable["mapDataOfATMLocator"][i]["PRIMARY_PHONE_NUMBER"],
									 lblFaxno:resulttable["mapDataOfATMLocator"][i]["OTHER_PHONE_NUMBER"],
									 lblWorkingHrs: workingHrs,
									 engWorkingHrs: resulttable["mapDataOfATMLocator"][i]["BOOTH_WORKING_TIME_EN"],
									 thaiWorkingHrs: resulttable["mapDataOfATMLocator"][i]["BOOTH_WORKING_TIME_TH"]
									};
					}
					listDataIB.push(tempData);
				}//End of for loop.
				
				
				if(resulttable["mapDataOfATMLocator"].length == 0){
					frmIBATMBranch.segATMListDetails.removeAll();
					frmIBATMBranch.hboxImage.setVisibility(true);
					frmIBATMBranch.hboxListDetails.setVisibility(false);
					dismissLoadingScreenPopup();
					alert(kony.i18n.getLocalizedString("FindTMBResultErrorMsg"));
				}else{
					frmIBATMBranch.segATMListDetails.setData(listDataIB);
					document.getElementById("frmIBATMBranch_scrollbox864947284161140").className = "kwt100 scrBoxBills";
					dismissLoadingScreenPopup();
				}
			}else{
				dismissLoadingScreenPopup();
				alert(kony.i18n.getLocalizedString("ECGenericError"));
				if(frmIBATMBranch.hboxListDetails.isVisible){
					frmIBATMBranch.hboxListDetails.setVisibility(false);
					frmIBATMBranch.hboxImage.setVisibility(true);
				}
			}
		}else{
			dismissLoadingScreenPopup();
			alert(kony.i18n.getLocalizedString("ECGenericError"));
			if(frmIBATMBranch.hboxListDetails.isVisible){
				frmIBATMBranch.hboxListDetails.setVisibility(false);
				frmIBATMBranch.hboxImage.setVisibility(true);
			}
		}
	}catch(e){
		
	}
}
gblSelectedSegData = [];
function onSelectListSegIB(){
	try{
		
		frmIBATMBranch.hboxImage.setVisibility(false);
		frmIBATMBranch.hboxListDetails.setVisibility(true);
		if(gblBrachTypeIB == "ATM"){
			frmIBATMBranch.lblTitle.text = kony.i18n.getLocalizedString("FindTMB_ATMDetails");
		}else if(gblBrachTypeIB == "BRANCH"){
			frmIBATMBranch.lblTitle.text = kony.i18n.getLocalizedString("FindTMB_BranchDetails");
		}else if(gblBrachTypeIB == "EX"){
			frmIBATMBranch.lblTitle.text = kony.i18n.getLocalizedString("FindTMB_ExgBoothDetails");
		}
		var selectedName = "";
		var address = "";
		var workingHrs = "";
		var phoneNumber = "";
		var faxNumber = "";
		var currentLocale = kony.i18n.getCurrentLocale();
		
		
		if(currentLocale == "en_US"){
			selectedName = frmIBATMBranch.segATMListDetails.selectedItems[0].engName;
			address = frmIBATMBranch.segATMListDetails.selectedItems[0].engAddress;
			workingHrs = frmIBATMBranch.segATMListDetails.selectedItems[0].engWorkingHrs;
		}else if(currentLocale == "th_TH"){
			selectedName = frmIBATMBranch.segATMListDetails.selectedItems[0].thaiName;
			address = frmIBATMBranch.segATMListDetails.selectedItems[0].thaiAddress;
			workingHrs = frmIBATMBranch.segATMListDetails.selectedItems[0].thaiWorkingHrs;
		}
		phoneNumber = frmIBATMBranch.segATMListDetails.selectedItems[0].lblPhoneNo;
		faxNumber = frmIBATMBranch.segATMListDetails.selectedItems[0].lblFaxno;
		
		if(selectedName == null || selectedName == ""){ frmIBATMBranch.lblBRName.text = ""; }else{ frmIBATMBranch.lblBRName.text = selectedName; }
		if(address == null || address == ""){ frmIBATMBranch.lblAddress.text = ""; }else{ frmIBATMBranch.lblAddress.text = address;	}
		if(phoneNumber == null || phoneNumber == ""){ frmIBATMBranch.lblPhoneNo.text = ""; }else{ frmIBATMBranch.lblPhoneNo.text = phoneNumber; }
		if(faxNumber == null || faxNumber == ""){ frmIBATMBranch.lblFax.text = ""; }else{ frmIBATMBranch.lblFax.text = faxNumber; }
		if(workingHrs == null || workingHrs == ""){	frmIBATMBranch.lblWorkingHrs.text = "";	}else{ frmIBATMBranch.lblWorkingHrs.text = workingHrs; }
		
		if(gblBrachTypeIB == "ATM"){
			if(frmIBATMBranch.hboxTel.isVisible){
				frmIBATMBranch.hboxTel.setVisibility(false);
				frmIBATMBranch.hboxFax.setVisibility(false);
				frmIBATMBranch.hboxOfficeHrs.setVisibility(false);
			}
		}else{
			if(!frmIBATMBranch.hboxTel.isVisible){
				frmIBATMBranch.hboxTel.setVisibility(true);
				frmIBATMBranch.hboxFax.setVisibility(true);
				frmIBATMBranch.hboxOfficeHrs.setVisibility(true);
			}
		}
		// added gblSelectedSegData global variable to fix lang switch	defect# DEF16215
		gblSelectedSegData["nameEn"] = frmIBATMBranch.segATMListDetails.selectedItems[0].engName;
		gblSelectedSegData["addressEn"] = frmIBATMBranch.segATMListDetails.selectedItems[0].engAddress;
		gblSelectedSegData["workingHrsEn"] = frmIBATMBranch.segATMListDetails.selectedItems[0].engWorkingHrs;
		gblSelectedSegData["nameTh"] = frmIBATMBranch.segATMListDetails.selectedItems[0].thaiName;
		gblSelectedSegData["addressTh"] = frmIBATMBranch.segATMListDetails.selectedItems[0].thaiAddress;
		gblSelectedSegData["workingHrsTh"] = frmIBATMBranch.segATMListDetails.selectedItems[0].thaiWorkingHrs;
		gblSelectedSegData["lat"] = frmIBATMBranch.segATMListDetails.selectedItems[0].lat;
		gblSelectedSegData["lon"] = frmIBATMBranch.segATMListDetails.selectedItems[0].lon;
				
	}catch(e){
		
	}
}

function onClickGetDirectionIB(){
	try{
		
		var url = "";
		popIBATMBranch.dismiss();
		if(gblFindTMBViewTypeIB == "MapView"){
			url = "https://maps.google.com/maps?saddr="+gblLatitudeIB+","+gblLongitudeIB+"&daddr="+currentLocDetailsIB.lat+","+currentLocDetailsIB.lon.trim();
		}else  if(gblFindTMBViewTypeIB == "ListView"){
			var selectedLatitude = "";
			var selectedLongitude = "";
			selectedLatitude = gblSelectedSegData["lat"].trim();
			selectedLongitude = gblSelectedSegData["lon"].trim();
			//alert("selectedLatitude : "+selectedLatitude+" , selectedLongitude : "+selectedLongitude);			
			url = "https://maps.google.com/maps?saddr="+gblLatitudeIB+","+gblLongitudeIB+"&daddr="+selectedLatitude+","+selectedLongitude;
			//url = "https://maps.google.com/maps?saddr="+gblLatitudeIB+","+gblLongitudeIB+"&daddr="+frmIBATMBranch.segATMListDetails.selectedItems[0].lat+","+frmIBATMBranch.segATMListDetails.selectedItems[0].lon.trim();
		}
		//alert("gblFindTMBViewTypeIB : "+gblFindTMBViewTypeIB+" , url : "+url);
		kony.application.openURL(url);
	}catch(e){
		
	}
}

function finTMBfooterLinks(){ 
	try{
        var locale = kony.i18n.getCurrentLocale();
        if(gblIsNewOffersExists){          	 
           		hbxFooterPrelogin.linkHotPromoImg.isVisible = true;
	        } else {
	        	hbxFooterPrelogin.linkHotPromoImg.isVisible = false;
	        	
	    }
		frmIBATMBranch.hbxFooterPrelogin.linkFindTmb.skin = "footerLinkFindFoc";
		frmIBATMBranch.hbxFooterPrelogin.linkExchangeRate.skin = "footerLinkER";
		frmIBATMBranch.hbxFooterPrelogin.linkSiteTour.skin = "footerLinkST";
		frmIBATMBranch.hbxFooterPrelogin.linkTnC.skin = "footerLinkTC";
		frmIBATMBranch.hbxFooterPrelogin.linkSecurity.skin = "footerLinkSecurity";
		
	}catch(e){
		
	}
}

function syncIBFindTMB(){
	try{
		if (kony.application.getCurrentForm().id == "frmIBATMBranch"){
			frmIBATMBranch.txtKeyword.placeholder = kony.i18n.getLocalizedString("FindTMB_Keyword");
			setProvinceDistrictSyn();
			if(gblBrachTypeIB == "ATM"){
				frmIBATMBranch.lblTitle.text = kony.i18n.getLocalizedString("FindTMB_ATMDetails");
			}else if(gblBrachTypeIB == "BRANCH"){
				frmIBATMBranch.lblTitle.text = kony.i18n.getLocalizedString("FindTMB_BranchDetails");
			}else if(gblBrachTypeIB == "EX"){
				frmIBATMBranch.lblTitle.text = kony.i18n.getLocalizedString("FindTMB_ExgBoothDetails");
			}
			if(gblFindTMBViewTypeIB == "ListView"){
				// commented to fix lang switch
				//onSuccesATMSearchIB(400, gblFindTMBListData);
				if(frmIBATMBranch.hboxListDetails.isVisible){
					// commented to fix lang switch
					//onSelectListSegIB();
					if(undefined != gblSelectedSegData){
						var currentLocale =  kony.i18n.getCurrentLocale();
						if(currentLocale == "en_US"){
							frmIBATMBranch.lblBRName.text = gblSelectedSegData["nameEn"];
							frmIBATMBranch.lblAddress.text = gblSelectedSegData["addressEn"];
							frmIBATMBranch.lblWorkingHrs.text = gblSelectedSegData["workingHrsEn"];
						}else if(currentLocale == "th_TH"){
							frmIBATMBranch.lblBRName.text = gblSelectedSegData["nameTh"];
							frmIBATMBranch.lblAddress.text = gblSelectedSegData["addressTh"];
							frmIBATMBranch.lblWorkingHrs.text = gblSelectedSegData["workingHrsTh"];
						}
					}
				}
			}else{
				setDataMapCallBackIB(400, gblFindTMBMapData)
			}
		}
	}catch(e){
		
	}
}

function setProvinceDistrictSyn(){
	try{
		
		/*var btnSkin = frmIBATMBranch.btnSearchIcon.skin;
		var province = [];
		province.push([1,kony.i18n.getLocalizedString("FindTMB_Province")]);
		frmIBATMBranch.btnProvince.masterData = province;
		var district = [];
		district.push([1,kony.i18n.getLocalizedString("FindTMB_District")]);
		frmIBATMBranch.btnDistrict.masterData = district;
		frmIBATMBranch.txtKeyword.text = "";
		frmIBATMBranch.btnDistrict.setEnabled(false);
		provinceIDIB = "";
		districtIDIB = "";*/
		gblATMSearchFlagIB = "Province"
		populateProvinceDistrictDataIB(gblProvinceResultSet);
		if(provinceIDIB != null || provinceIDIB != ""){
			gblATMSearchFlagIB = "District";
			provinceIDIB = 1;
			populateProvinceDistrictDataIB(gblDistrictResultSet);
		}
		if(provinceIDIB == 1){
			frmIBATMBranch.btnDistrict.setEnabled(false);
		}else{
			frmIBATMBranch.btnDistrict.setEnabled(true);
		}
		
	}catch(e){
		
	}
}

function setHeaderPrePostLogin(){
	try{
		
		var currentForm = kony.application.getCurrentForm();
		if(gblLoggedIn){
			hbxIBPostLogin.imgProfilePic.setVisibility(true);
			hbxIBPostLogin.vboxUserName.setVisibility(true);
			hbxIBPostLogin.vboxHome.setVisibility(true);
			hbxIBPostLogin.lnkHome.setVisibility(true);
			hbxIBPostLogin.vboxLogout.setVisibility(true);
			hbxIBPostLogin.lnkLogOut.setVisibility(true);
		}else{
			hbxIBPostLogin.imgProfilePic.setVisibility(false);
			hbxIBPostLogin.vboxUserName.setVisibility(false);
			hbxIBPostLogin.vboxHome.setVisibility(false);
			hbxIBPostLogin.lnkHome.setVisibility(false);
			hbxIBPostLogin.vboxLogout.setVisibility(false);
			hbxIBPostLogin.lnkLogOut.setVisibility(false);
		}
	}catch(e){
		
	}
}
function isIE() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        return true;
    }
    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
        return true;
    }
    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
       return true;
    }
    return false;
}