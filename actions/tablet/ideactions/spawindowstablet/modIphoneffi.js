//function FirstFFIFunction()
//{
//	
//	var deviceInfo = kony.os.deviceInfo();
//	if (deviceInfo["name"] == "android") {
//		
//	}
//	if (deviceInfo["name"] == "iPhone") {
//	
//	passParams();
//	}
//
//}
//function callAll()
//{
//	malwarePresent();
//	malwareName();
//	osVersion();
//	configStatus();
//	rootedStatus();
//	riskScore();
//	sslValidation();
//}
function TrusteerDeviceId() {
    var lObject = {};
    lObject["deviceId"] = deviceID;
    lObject["message"] = "yes";
    ffinamespace.getDeviceId(lObject);
}

function malwarePresent() {
    var MalObj = {};
    MalObj["malwareAny"] = malCallBack;
    MalObj["message"] = "yes";
    ffinamespace.getMalware(MalObj);
}

function malwareName() {
    var MalNameObj = {};
    MalNameObj["malwareNames"] = malNameCallBack;
    MalNameObj["message"] = "yes";
    ffinamespace.getMalwareName(MalNameObj);
}

function osVersion() {
    var osVersionObj = {};
    osVersionObj["osVersion"] = osVersionCallBack;
    osVersionObj["message"] = "yes";
    ffinamespace.getosVersion(osVersionObj);
}

function osVersionCallBack(str) {
    GBL_RiskScoreIph = GBL_RiskScoreIph + "," + str;
}

function configStatus() {
    var configStatusObj = {};
    configStatusObj["configUpdate"] = configUpdateCallBack;
    configStatusObj["message"] = "yes";
    ffinamespace.getconfigStatus(configStatusObj);
}
/*function rootedStatus()
{
	var rootedObj = {};
	rootedObj["RootedStatus"] = rootedCallBack;
	rootedObj["message"] = "yes";
	
	
	ffinamespace.getrootedStatus(rootedObj);
}*/
/*function rootedCallBack(str)
{
	if(str == 0){
		GBL_rootedFlag =0;
	}else{
		GBL_rootedFlag =1;
	}
	
	GBL_RiskScoreIph = GBL_RiskScoreIph+","+str;
}*/
/*function riskScore()
{
	var riskScoreObj = {};
	riskScoreObj["riskScore"] = riskScoreCallBack;
	riskScoreObj["message"] = "yes";
	
	
	ffinamespace.getriskScore(riskScoreObj);
}*/
/*function riskScoreCallBack(str)
{
	
	GBL_RiskScoreIph = str
	callServiceToLogRiskInfo();
}*/
function sslValidation() {
    var sslObj = {};
    sslObj["SSLValidation"] = sslCallBack;
    sslObj["message"] = "yes";
    sslObj["url"] = appConfig.serverIp;
    ffinamespace.getsslValidation(sslObj);
}
/*function passParams()
{
	
	
	var lObj = {};
	lObj["callback"] = callBack;
	lObj["message"] = "Hey Welcome U All........";
	
	ffinamespace.jsfunction(lObj);
	
	
}*/
function callBack(pRes) {
    var finalStringReceived = kony.string.split(pRes, ",");
    var finalResult = "";
    if (finalStringReceived[1] == 0) {
        finalResult = "Configuration files are updated" + "\n";
    } else {
        finalResult = "Configuration files are not updated" + "\n";
    }
    if (finalStringReceived[2] == 0) {
        finalResult = finalResult + "OS Version is updated" + "\n";
    } else {
        finalResult = finalResult + "OS Version is not updated" + "\n";
    }
    if (finalStringReceived[3] == 1000) {
        finalResult = finalResult + "Device Rooted - YES" + "\n";
    } else {
        finalResult = finalResult + "Device Rooted - NO" + "\n";
    }
    if (finalStringReceived[4] == 1000) {
        finalResult = finalResult + "Malware Present - YES" + "\n";
    } else {
        finalResult = finalResult + "Malware Present - NO" + "\n";
    }
    if (finalStringReceived[7] == 0) {
        finalResult = finalResult + "SSL Certificates Valid" + "\n";
    } else {
        finalResult = finalResult + "SSL Certificates not Valid" + "\n";
    }
    finalResult = finalResult + "Device Risk Score is " + finalStringReceived[5] + "\n" + " Unique Device ID is " + finalStringReceived[6];
    dismissLoadingScreen();
    var basicProperties = {
        message: finalResult + "\n Do you want to proceed ?",
        alertType: constants.
        ALERT_TYPE_CONFIRMATION,
        alertTitle: "Security Warning",
        yesLabel: "Yes",
        noLabel: "No",
        alertHandler: handleSecurityBreach
    };
    var platformSpecificProperties = {};
    kony.ui.Alert(basicProperties, platformSpecificProperties);
}

function handleSecurityBreach(response) {
    if (response) {
        //showStartUp();
        //getToRecipients();
    } else {
        try {
            kony.application.exit();
        } catch (Error) {}
    }
}

function startSecurityValidationIphone() {
    GBL_MALWARE_FOR_RISK_FLAG = false;
    sslValidation();
}

function sslCallBack(str) {
    // 0 or non zero value
    if (str == 0 || str == "0" || str == 99 || str == "99") {
        //configStatus();
    } else {
        //showAlertForUniqueGenFail(kony.i18n.getLocalizedString("keySSLValidationFailureMsg"));
        //alert(kony.i18n.getLocalizedString("keySSLValidationFailureMsg"));
    }
    configStatus();
}

function configUpdateCallBack(str) {
    GBL_RiskScoreIph = str;
    // 0 or 1000 
    if (str == 0) {
        //GBL_RiskScoreIph = str;
    } else {}
    malwarePresent();
}

function malCallBack(str) {
    //GBL_Risk_DataTo_Log = GBL_Risk_DataTo_Log+","+str+",";
    if (str == 0 || str == "0") {} else {
        malwareName();
    }
}

function malNameCallBack(str) {
    GBL_MalWare_Names = str;
    var messageErr = kony.i18n.getLocalizedString("keyMallwareFoundMsg");
    if (messageErr == null) {
        messageErr = "The dangerous malware/suspicious app being present on device 1. The bank strongly recommend to cleansing malware before access to the application. Without cleansing the malware and access to the application, you can�t transfer money out. Please contact # Call center for more advice."
    }
    messageErr = messageErr.replace("NAMES", str);
    var getEncrKeyFromDevice = kony.store.getItem("encrytedText");
    if (getEncrKeyFromDevice != null) {
        if (!GBL_MALWARE_FOR_RISK_FLAG) {
            showAlertForSecValidation(messageErr);
        }
    } else {
        showAlertForUniqueGenFail(messageErr);
    }
}

function startRiskDataCollForIphone() {
    //configStatus();
    //osVersion();
    //rootedStatus();
    //riskScore();
    GBL_RiskScoreIph = "";
    getDeviceRiskScoreToUpdate();
}

function getDeviceRiskScoreToUpdate() {
    function configStatusMW() {
        var configStatusObj = {};
        configStatusObj["configUpdate"] = configUpdateCallBackMW;
        configStatusObj["message"] = "yes";
        ffinamespace.getconfigStatus(configStatusObj);
    }

    function configUpdateCallBackMW(str) {
        GBL_RiskScoreIph = str;
        osVersionMW();
    }

    function osVersionMW() {
        var osVersionObj = {};
        osVersionObj["osVersion"] = osVersionCallBackMW;
        osVersionObj["message"] = "yes";
        ffinamespace.getosVersion(osVersionObj);
    }

    function osVersionCallBackMW(str) {
        GBL_RiskScoreIph = GBL_RiskScoreIph + "," + str;
        rootedStatusMW();
    }

    function rootedStatusMW() {
        var rootedObj = {};
        rootedObj["RootedStatus"] = rootedCallBackMW;
        rootedObj["message"] = "yes";
        ffinamespace.getrootedStatus(rootedObj);
    }

    function rootedCallBackMW(str) {
        if (str == 0) {
            GBL_rootedFlag = 0;
        } else {
            GBL_rootedFlag = 1;
        }
        GBL_RiskScoreIph = GBL_RiskScoreIph + "," + str;
        riskScoreMW();
    }

    function riskScoreMW() {
        var riskScoreObj = {};
        riskScoreObj["riskScore"] = riskScoreCallBackMW;
        riskScoreObj["message"] = "yes";
        ffinamespace.getriskScore(riskScoreObj);
    }

    function riskScoreCallBackMW(str) {
        GBL_RiskScoreIph = GBL_RiskScoreIph + "," + str;
        //callServiceToLogRiskInfo();
        malwarePresentMW();
    }

    function malwarePresentMW() {
        var MalObj = {};
        MalObj["malwareAny"] = malCallBackMW;
        MalObj["message"] = "yes";
        ffinamespace.getMalware(MalObj);
    }

    function malCallBackMW(str) {
        //GBL_Risk_DataTo_Log = GBL_Risk_DataTo_Log+","+str+",";
        if (str == 0 || str == "0") {
            GBL_RiskScoreIph = GBL_RiskScoreIph + "," + str;
            GBL_Session_LOCK = false;
            callServiceToLogRiskInfo();
        } else {
            malwareNameMW();
        }
    }

    function malwareNameMW() {
        var MalNameObj = {};
        MalNameObj["malwareNames"] = malNameCallBackMW;
        MalNameObj["message"] = "yes";
        ffinamespace.getMalwareName(MalNameObj);
    }

    function malNameCallBackMW(str) {
        GBL_MalWare_Names = str;
        GBL_Session_LOCK = true;
        GBL_RiskScoreIph = GBL_RiskScoreIph + "," + str
        callServiceToLogRiskInfo();
    }
    configStatusMW();
}