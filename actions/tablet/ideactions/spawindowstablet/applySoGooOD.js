gblSoGooODTransactionServiceData = []; //Total So GooOD Transactions - Json Array from Service
soGooODTxnsList = []; // Modified So GooOD Transactions - Segment Data - used in Search 
selectedTxnIds = []; // Selected Transaction Ids to apply So GooOD
gblCurrSelTxnIds = [];
gblTransactionPlanServiceData = []; // Total Selected So GooOD Transactions with Selected So GooOD Plan 
soGooODTxnPlanList = []; // Selected So GooOD Transactions with Selected So GooOD Plan
gblPlan = []; // Selected Plan Details
channel = "";
countTryAgain = 0;
soGooOD_StartTime = "";
soGooOD_EndTime = "";

function onClickApplySoGooODLink() {
    if (kony.application.getCurrentForm().id == "frmAccountDetailsMB") {
        channel = "MB";
        if (checkMBUserStatus()) {
            checkBusinessHrs();
        }
    } else if (kony.application.getCurrentForm().id == "frmIBAccntSummary") {
        channel = "IB";
        if (getCRMLockStatus()) {
            curr_form = kony.application.getCurrentForm().id;
            dismissLoadingScreenPopup();
            popIBBPOTPLocked.show();
        } else {
            checkBusinessHrs();
        }
    }
}

function checkBusinessHrs() {
    var input_param = {};
    invokeServiceSecureAsync("soGooodBusinessHrs", input_param, checkBusinessHrsCallBack);
}

function checkBusinessHrsCallBack(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
            var serviceHrsFlag = result["soGoodServiceHoursFlag"];
            soGooOD_StartTime = result["soGooOD_StartTime"];
            soGooOD_EndTime = result["soGooOD_EndTime"];
            if (serviceHrsFlag == "true") {
                if (channel == "MB") {
                    frmMBSoGooodProdBrief.show();
                } else {
                    frmIBSoGooodProdBrief.show();
                }
            } else {
                var errorMsg = kony.i18n.getLocalizedString("keySoGooODServiceUnavailable");
                errorMsg = errorMsg.replace("{start_time}", soGooOD_StartTime);
                errorMsg = errorMsg.replace("{end_time}", soGooOD_EndTime);
                showAlert(errorMsg, kony.i18n.getLocalizedString("info"));
                return false;
            }
        } else {
            dismissLoadingScreenBasedOnChannel(channel);
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}

function getSoGooODTransactions() {
    selectedTxnIds = [];
    gblCurrSelTxnIds = [];
    gblTransactionPlanServiceData = [];
    disableNextButton();
    getSoGoodTxnsAndPlansService();
}

function loadSoGooODTxnsInSegment(transactionData) {
    frmMBSoGooODTranasactions.lblNoSoGoooDTxns.setVisibility(false);
    frmMBSoGooODTranasactions.segSoGooODTxns.setVisibility(true);
    frmMBSoGooODTranasactions.hbxFooter.setVisibility(true);
    soGooODTxnsList = [];
    frmMBSoGooODTranasactions.txtSearch.text = "";
    if (transactionData == null || transactionData.length == 0) {
        frmMBSoGooODTranasactions.lblNoSoGoooDTxns.setVisibility(true);
        frmMBSoGooODTranasactions.FlexContent.setVisibility(false);
        frmMBSoGooODTranasactions.segSoGooODTxns.setVisibility(false);
        disableNextButton();
        frmMBSoGooODTranasactions.lblNoSoGoooDTxns = {
            contentAlignment: constants.CONTENT_ALIGN_CENTER
        };
        frmMBSoGooODTranasactions.lblNoSoGoooDTxns.text = kony.i18n.getLocalizedString("keyNoTransactionFound");
        gblHaveTransaction = 0;
        frmMBSoGooODTranasactions.lblSel.setEnabled(false);
    } else {
        gblHaveTransaction = transactionData.length;
        for (var i = 0; i < transactionData.length; i++) {
            var tempRecord = {
                "transactionId": transactionData[i].transactionId,
                "lblTxnDate": kony.i18n.getLocalizedString("keyIBTransactionDate") + ": " + transactionData[i].txnDate,
                "lblDesc": kony.i18n.getLocalizedString("keyMBDescriptionc") + " " + transactionData[i].txnDesc,
                "lblAmount": kony.i18n.getLocalizedString("keyAmount"),
                "lblAmountValue": commaFormatted(transactionData[i].txnAmount) + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
                "imgCheckBox": {
                    "src": "chkbox2.png"
                }
            };
            soGooODTxnsList.push(tempRecord);
        }
        frmMBSoGooODTranasactions.lblNoSoGoooDTxns.setVisibility(true);
        frmMBSoGooODTranasactions.FlexContent.setVisibility(true);
        frmMBSoGooODTranasactions.segSoGooODTxns.setVisibility(true);
        frmMBSoGooODTranasactions.segSoGooODTxns.setData(soGooODTxnsList);
        if (transactionData.length == 1) frmMBSoGooODTranasactions.lblNoSoGoooDTxns.text = "[" + transactionData.length + " " + kony.i18n.getLocalizedString("keyResult") + "]";
        else {
            if (kony.i18n.getCurrentLocale() == "th_TH") {
                frmMBSoGooODTranasactions.lblNoSoGoooDTxns.text = "[" + transactionData.length + " " + kony.i18n.getLocalizedString("keyResult") + "]";
            } else {
                frmMBSoGooODTranasactions.lblNoSoGoooDTxns.text = "[" + transactionData.length + " " + kony.i18n.getLocalizedString("keyResult") + "s]";
            }
        }
        frmMBSoGooODTranasactions.lblSel.setEnabled(true);
    }
    frmMBSoGooODTranasactions.show();
}

function loadRemainingSoGoooDTransactionsMB() {
    gblCurrSelTxnIds = [];
    //disableNextButton();
    var transactionData = gblSoGooODTransactionServiceData;
    var remainingSoGoooDTxnData = [];
    for (var i = 0; i < transactionData.length; i++) {
        if (selectedTxnIds.indexOf(transactionData[i].transactionId) < 0) {
            remainingSoGoooDTxnData.push(transactionData[i]);
        }
    }
    loadSoGooODTxnsInSegment(remainingSoGoooDTxnData);
}

function localeTransaction() {
    var transactionData = gblSoGooODTransactionServiceData;
    var remainingSoGoooDTxnData = [];
    for (var i = 0; i < transactionData.length; i++) {
        if (selectedTxnIds.indexOf(transactionData[i].transactionId) < 0) {
            remainingSoGoooDTxnData.push(transactionData[i]);
        }
    }
    var transactionData = remainingSoGoooDTxnData;
    soGooODTxnsList = [];
    frmMBSoGooODTranasactions.txtSearch.text = "";
    if (transactionData == null || transactionData.length == 0) {
        frmMBSoGooODTranasactions.lblNoSoGoooDTxns.setVisibility(true);
        frmMBSoGooODTranasactions.FlexContent.setVisibility(false);
        frmMBSoGooODTranasactions.segSoGooODTxns.setVisibility(false);
        disableNextButton();
        frmMBSoGooODTranasactions.lblNoSoGoooDTxns = {
            contentAlignment: constants.CONTENT_ALIGN_CENTER
        };
        frmMBSoGooODTranasactions.lblNoSoGoooDTxns.text = kony.i18n.getLocalizedString("keyNoTransactionFound");
    } else {
        gblHaveTransaction = transactionData.length;
        for (var i = 0; i < transactionData.length; i++) {
            var tempRecord = {
                "transactionId": transactionData[i].transactionId,
                "lblTxnDate": kony.i18n.getLocalizedString("keyIBTransactionDate") + ": " + transactionData[i].txnDate,
                "lblDesc": kony.i18n.getLocalizedString("keyMBDescriptionc") + " " + transactionData[i].txnDesc,
                "lblAmount": kony.i18n.getLocalizedString("keyAmount"),
                "lblAmountValue": commaFormatted(transactionData[i].txnAmount) + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
                "imgCheckBox": {
                    "src": "chkbox2.png"
                }
            };
            soGooODTxnsList.push(tempRecord);
        }
        frmMBSoGooODTranasactions.lblNoSoGoooDTxns.setVisibility(true);
        frmMBSoGooODTranasactions.FlexContent.setVisibility(true);
        frmMBSoGooODTranasactions.segSoGooODTxns.setVisibility(true);
        frmMBSoGooODTranasactions.segSoGooODTxns.setData(soGooODTxnsList);
        if (transactionData.length == 1) frmMBSoGooODTranasactions.lblNoSoGoooDTxns.text = "[" + transactionData.length + " " + kony.i18n.getLocalizedString("keyResult") + "]";
        else {
            if (kony.i18n.getCurrentLocale() == "th_TH") {
                frmMBSoGooODTranasactions.lblNoSoGoooDTxns.text = "[" + transactionData.length + " " + kony.i18n.getLocalizedString("keyResult") + "]";
            } else {
                frmMBSoGooODTranasactions.lblNoSoGoooDTxns.text = "[" + transactionData.length + " " + kony.i18n.getLocalizedString("keyResult") + "s]";
            }
        }
    }
}

function loadRemainingSoGoooDTransactions() {
    clearSelectedTxnOnBackBtn();
    loadRemainingSoGoooDTransactionsMB();
    disableTransactionNextButton();
}

function selectSoGooODTransaction() {}

function selectSoGooODTransactionForAndroid() {
    var selectedItems = frmMBSoGooODTranasactions.segSoGooODTxns.selectedItems;
    var txtDeSelectAll = kony.i18n.getLocalizedString("keydeselectall");
    if (frmMBSoGooODTranasactions.segSoGooODTxns.data.length == 1) {
        if (selectedItems != null && selectedItems.length > 0) {
            selectAllTrans();
            frmMBSoGooODTranasactions.lblSel.text = kony.i18n.getLocalizedString("keydeselectall");
        } else {
            frmMBSoGooODTranasactions.lblSel.text = kony.i18n.getLocalizedString("keyselectall");
        }
    } else {
        frmMBSoGooODTranasactions.segSoGooODTxns.selectedRowIndex = [0, 1];
        if (frmMBSoGooODTranasactions.lblSel.text == txtDeSelectAll) {
            frmMBSoGooODTranasactions.lblSel.text = kony.i18n.getLocalizedString("keyselectall");
        }
    }
    //Enable or Disable OK Button
    if (selectedItems != null && selectedItems.length > 0) {
        enableNextButton();
    } else {
        disableNextButton();
    }
}

function selectSoGooODTransactionForIPhone() {
    var selectedItems = frmMBSoGooODTranasactions.segSoGooODTxns.selectedItems;
    var txtDeSelectAll = kony.i18n.getLocalizedString("keydeselectall");
    if (frmMBSoGooODTranasactions.segSoGooODTxns.data.length == 1) {
        if (selectedItems != null && selectedItems.length > 0) frmMBSoGooODTranasactions.lblSel.text = kony.i18n.getLocalizedString("keydeselectall");
        else frmMBSoGooODTranasactions.lblSel.text = kony.i18n.getLocalizedString("keyselectall");
    } else {
        if (frmMBSoGooODTranasactions.lblSel.text == txtDeSelectAll) frmMBSoGooODTranasactions.lblSel.text = kony.i18n.getLocalizedString("keyselectall");
    }
    //Enable or Disable OK Button
    if (selectedItems != null && selectedItems.length > 0) {
        enableNextButton();
    } else {
        disableNextButton();
    }
}

function searchSoGooODTransactions(isClicked) {
    frmMBSoGooODTranasactions.lblSel.text = kony.i18n.getLocalizedString("keyselectall");
    var searchText = frmMBSoGooODTranasactions.txtSearch.text;
    searchText = searchText.toLowerCase();
    var soGooODTxnsSegData = soGooODTxnsList; //frmMBSoGooODTranasactions.segSoGooODTxns.data;
    var searchList = searchResult(soGooODTxnsSegData, searchText, isClicked);
    if (frmMBSoGooODTranasactions.segSoGooODTxns.data != null && frmMBSoGooODTranasactions.segSoGooODTxns.data != undefined) {
        if (frmMBSoGooODTranasactions.segSoGooODTxns.data.length != searchList.length) {
            frmMBSoGooODTranasactions.segSoGooODTxns.setData(searchList);
        }
    } else {
        frmMBSoGooODTranasactions.segSoGooODTxns.setData(searchList);
    }
    if (searchList.length <= 0) {
        gblCurrSelTxnIds = [];
        frmMBSoGooODTranasactions.lblNoSoGoooDTxns.setVisibility(true);
        if (gblSoGooODTransactionServiceData.length == 0) {
            frmMBSoGooODTranasactions.lblNoSoGoooDTxns.text = kony.i18n.getLocalizedString("keyNoTransactionFound");
            frmMBSoGooODTranasactions.lblNoSoGoooDTxns = {
                contentAlignment: constants.CONTENT_ALIGN_CENTER
            };
        } else {
            frmMBSoGooODTranasactions.lblNoSoGoooDTxns.text = kony.i18n.getLocalizedString("keybillernotfound");
            frmMBSoGooODTranasactions.lblNoSoGoooDTxns = {
                contentAlignment: constants.CONTENT_ALIGN_CENTER
            };
        }
        //frmMBSoGooODTranasactions.hbxCardName.setVisibility(false);
        frmMBSoGooODTranasactions.segSoGooODTxns.setVisibility(false);
    } else {
        frmMBSoGooODTranasactions.lblNoSoGoooDTxns.setVisibility(true);
        frmMBSoGooODTranasactions.lblNoSoGoooDTxns = {
            contentAlignment: constants.CONTENT_ALIGN_MIDDLE_LEFT
        };
        if (searchList.length == 1) frmMBSoGooODTranasactions.lblNoSoGoooDTxns.text = "[" + searchList.length + " " + kony.i18n.getLocalizedString("keyResult") + "]";
        else {
            if (kony.i18n.getCurrentLocale() == "th_TH") {
                frmMBSoGooODTranasactions.lblNoSoGoooDTxns.text = "[" + searchList.length + " " + kony.i18n.getLocalizedString("keyResult") + "]";
            } else {
                frmMBSoGooODTranasactions.lblNoSoGoooDTxns.text = "[" + searchList.length + " " + kony.i18n.getLocalizedString("keyResult") + "s]";
            }
        }
        //frmMBSoGooODTranasactions.hbxCardName.setVisibility(true);
        frmMBSoGooODTranasactions.segSoGooODTxns.setVisibility(true);
    }
    //Enable or Disable OK Button
    if (gblCurrSelTxnIds.length > 0) {
        enableNextButton();
    } else {
        disableNextButton();
    }
}

function searchResult(soGooODTxnsSegData, searchText, isClicked) {
    var searchList = [];
    var soGooODTxnLength = soGooODTxnsSegData.length;
    var txnDate = "";
    var txnDesc = "";
    var txnAmount = "";
    var txnAmountComma = "";
    if (soGooODTxnLength > 0) { // Atleast one transaction should be avilable
        if (searchText.length >= 3 || (isClicked == "true" && searchText.length >= 1)) { //Atleast 3 characters should be entered in search box
            for (j = 0, i = 0; i < soGooODTxnLength; i++) {
                txnDate = soGooODTxnsSegData[i].lblTxnDate.split(":")[1].trim();
                txnDesc = soGooODTxnsSegData[i].lblDesc.split(":")[1].trim();
                txnAmountComma = soGooODTxnsSegData[i].lblAmountValue;
                txnAmount = removeCommos(txnAmountComma) + "";
                if (txnDate.indexOf(searchText) > -1 || txnDesc.toLowerCase().indexOf(searchText) > -1 || txnAmount.indexOf(searchText) > -1 || txnAmountComma.indexOf(searchText) > -1) {
                    searchList[j] = soGooODTxnsSegData[i];
                    j++;
                }
            } //For Loop End
        } else { //searchText condition End
            //If you Clear the Search Text then show All Transactions
            clearSelectedTxnOnBackBtn();
            gblCurrSelTxnIds = [];
            searchList = soGooODTxnsList;
        }
    } //soGooODTxnLength condition End
    return searchList;
}

function gotoSoGooODTxnPlanDetails() {
    var selectedItems = frmMBSoGooODTranasactions.segSoGooODTxns.selectedItems;
    gblCurrSelTxnIds = [];
    var totalSelLen = selectedItems.length + selectedTxnIds.length;
    //Enable or Disable OK Button
    /*
    if(selectedItems != null) {
    	if(totalSelLen > gblMaxApplySoGood) {
    		var errorMsg = kony.i18n.getLocalizedString("keyValidMaxSoGoodSelection");
    		errorMsg = errorMsg.replace("[maxOfSoGoodTxn]", gblMaxApplySoGood);
    		alert(errorMsg);
    		return false;
    	} else {
    		for(i = 0; i < selectedItems.length; i++) {
    			var txnId = selectedItems[i]["transactionId"];
    			if(selectedTxnIds.indexOf(txnId) < 0) {
    				selectedTxnIds.push(txnId);
    			}	
    			gblCurrSelTxnIds.push(txnId);
    		}
    	
    		loadInterestPlans();
    	}
    }*/
    if (selectedItems != null) {
        for (i = 0; i < selectedItems.length; i++) {
            var txnId = selectedItems[i]["transactionId"];
            if (selectedTxnIds.indexOf(txnId) < 0) {
                selectedTxnIds.push(txnId);
            }
            gblCurrSelTxnIds.push(txnId);
        }
        loadInterestPlans();
    }
    kony.print("selectedItems " + JSON.stringify(selectedItems));
    kony.print("selectedTxnIds " + JSON.stringify(selectedTxnIds));
    kony.print("gblCurrSelTxnIds " + JSON.stringify(gblCurrSelTxnIds));
}

function frmMBSoGooodTnCPreShow() {
    changeStatusBarColor();
    var currentLocale = kony.i18n.getCurrentLocale();
    frmMBSoGooodTnC.lblHdrTxt.text = kony.i18n.getLocalizedString('keyApplySoGooOD')
    frmMBSoGooodTnC.btnBack1.text = kony.i18n.getLocalizedString('Back')
        //frmMBSoGooodTnC.btnAgree.text = kony.i18n.getLocalizedString('keyAgreeButton')
    var input_param = {};
    input_param["localeCd"] = currentLocale;
    input_param["moduleKey"] = 'TMBSoGooOD';
    showLoadingScreen();
    invokeServiceSecureAsync("readUTFFile", input_param, setSoGooodTnC);
}

function setSoGooodTnC(status, result) {
    if (status == 400) {
        dismissLoadingScreen();
        if (result["opstatus"] == 0) {
            frmMBSoGooodTnC.btnBack1.text = kony.i18n.getLocalizedString('Back')
                //frmMBSoGooodTnC.btnAgree.text = kony.i18n.getLocalizedString('keyAgreeButton')
            frmMBSoGooodTnC.lblHdrTxt.text = kony.i18n.getLocalizedString('keyApplySoGooOD')
            frmMBSoGooodTnC.lblTandC.text = "";
            frmMBSoGooodTnC.lblTandC.text = result["fileContent"];
        } else {
            dismissLoadingScreenBasedOnChannel(channel);
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}

function frmMBSoGooodProdBriefPreShow() {
    changeStatusBarColor();
    var currentLocale = kony.i18n.getCurrentLocale();
    frmMBSoGooodProdBrief.lblHdrTxt.text = kony.i18n.getLocalizedString('keyApplySoGooOD')
    frmMBSoGooodProdBrief.lblImgSoGooodHead.text = kony.i18n.getLocalizedString('keySoGooODPrdBriefHead');
    frmMBSoGooodProdBrief.btnCancel.text = kony.i18n.getLocalizedString('Back')
    frmMBSoGooodProdBrief.btnNext.text = kony.i18n.getLocalizedString('Next')
    frmMBSoGooodProdBrief.lblagree1.text = kony.i18n.getLocalizedString('cbReadAcceptTC');
    frmMBSoGooodProdBrief.lblagree2.text = kony.i18n.getLocalizedString('lkSoGooODTC');
    //As per PO's change request
    var soGoooDTnCMsg = kony.i18n.getLocalizedString('keySoGooODPrdBrief');
    soGoooDTnCMsg = soGoooDTnCMsg.replace("{start_time}", soGooOD_StartTime);
    soGoooDTnCMsg = soGoooDTnCMsg.replace("{end_time}", soGooOD_EndTime);
    frmMBSoGooodProdBrief.lblTandC.text = soGoooDTnCMsg;
    frmMBSoGooodProdBrief.lblImgSoGooodHead.text = kony.i18n.getLocalizedString('keySoGooODPrdBriefHead');
    var apply_sogoood_head_image_name = "ApplySoGooodHead";
    var locale = "";
    if (currentLocale == "th_TH") {
        locale = "_TH";
    } else {
        locale = "_EN";
    }
    var apply_sogoood_tail_image_name = "ApplySoGooodTail" + locale;
    var randomnum = Math.floor((Math.random() * 10000) + 1);
    var apply_sogoood_head_image = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + apply_sogoood_head_image_name + "&modIdentifier=PRODUCTPACKAGEIMG&rr=" + randomnum;
    var apply_sogoood_tail_image = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + apply_sogoood_tail_image_name + "&modIdentifier=PRODUCTPACKAGEIMG&rr=" + randomnum;
    frmMBSoGooodProdBrief.imgSoGooodHead.src = apply_sogoood_head_image;
    frmMBSoGooodProdBrief.imgSoGooodHead.setVisibility(true);
    frmMBSoGooodProdBrief.imgSoGooodTail.src = apply_sogoood_tail_image;
    frmMBSoGooodProdBrief.imgSoGooodTail.setVisibility(true);
    frmMBSoGooodProdBrief.btnChk.skin = "btnCheckFoc";
    frmMBSoGooodProdBrief.btnNext.setEnabled(true);
    frmMBSoGooodProdBrief.btnNext.skin = "btnBlueSkin";
    frmMBSoGooodProdBrief.btnNext.focusSkin = "btnDarkBlueFoc";
    isCheck = true;
}

function frmMBSoGooodConfPreShow() {
    changeStatusBarColor();
    var currentLocale = kony.i18n.getCurrentLocale();
    frmMBSoGooodConf.lblProductHdr.text = frmMBSoGooodPlanList.lblCardNickName.text;
    frmMBSoGooodConf.lblHdrTxt.text = kony.i18n.getLocalizedString('keyConfirmation');
    frmMBSoGooodConf.btnCancel.text = kony.i18n.getLocalizedString('keyCancelButton');
    frmMBSoGooodConf.btnNext.text = kony.i18n.getLocalizedString('keyConfirm');
    loadSoGooODTransactionConf(gblTransactionPlanServiceData);
}

function loadSoGooODTransactionConf(transactionPlanData) {
    var segmentData = [];
    for (var i = 0; i < transactionPlanData.length; i++) {
        var tempRecord = getTransactionPlanRecordBasic(transactionPlanData[i]);
        tempRecord["linkMoreHide"] = kony.i18n.getLocalizedString("Hide");
        segmentData.push(tempRecord);
    }
    frmMBSoGooodConf.segTransDetail.setData(segmentData);
}

function onClickMoreHideOnTnxConf() {
    var selectedItem = frmMBSoGooodConf.segTransDetail.selectedIndex[1];
    var selectedData = frmMBSoGooodConf.segTransDetail.data[selectedItem];
    if (selectedData.linkMoreHide == kony.i18n.getLocalizedString("More")) {
        selectedData = moreHidePlanRecord(selectedData, kony.i18n.getLocalizedString("Hide"), true);
    } else {
        selectedData = moreHidePlanRecord(selectedData, kony.i18n.getLocalizedString("More"), false);
    }
    frmMBSoGooodConf.segTransDetail.setDataAt(selectedData, selectedItem);
}

function onClickApplySoGooODConfirm() {
    showLoadingScreen();
    // call service to apply the transactions
    loadExcutedSoGooODTxnsInSegment(gblTransactionPlanServiceData);
    frmApplySoGooODComplete.lblCardName.text = frmMBSoGooodConf.lblProductHdr.text;
    frmApplySoGooODComplete.show();
    dismissLoadingScreen();
}
//Plan Details List
function getSelectedPlanForSelectedTxns(channel) {
    var soGoooDTxns = gblSoGooODTransactionServiceData;
    for (var j = 0; j < gblPlan.length; j++) {
        var tempRecord = {
            transactionId: gblPlan[j].transactionId,
            planId: gblPlan[j].planId,
            lblTransactionDateVal: gblPlan[j].txnDate,
            lblTransaction: gblPlan[j].txnDesc,
            lblAmountVal: commaFormatted(gblPlan[j].txnAmount) + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
            lblPlanVal: gblPlan[j].plan + " " + kony.i18n.getLocalizedString("keymonths"),
            lblInstallmentVal: commaFormatted(gblPlan[j].installment) + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
            lblInterestVal: commaFormatted(gblPlan[j].interestVal) + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
            lblTotalAmountVal: commaFormatted(gblPlan[j].totalAmount) + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
            lblPostedDateVal: gblPlan[j].postedDate,
            lblAmount: kony.i18n.getLocalizedString("keyAmount") + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
            lblPlan: kony.i18n.getLocalizedString("keySoGooODInstPlan"),
            lblInstallment: kony.i18n.getLocalizedString("keySoGooODMonthlyInst"),
            lblInterest: kony.i18n.getLocalizedString("keySoGooODIntCharge"),
            lblTotalAmount: kony.i18n.getLocalizedString("keySoGooODTotAmt"),
            lblTransactionDate: kony.i18n.getLocalizedString("keyIBTransactionDate"),
            lblPostedDate: kony.i18n.getLocalizedString("keyPostedDate")
        };
        gblTransactionPlanServiceData.push(tempRecord);
    }
    loadSoGooODTxnPlansInSegment(gblTransactionPlanServiceData, channel);
    if (channel == "IB") {
        showSelectedPlanTxn();
    }
}

function loadSoGooODTxnPlansInSegment(transactionPlanData, channel) {
    soGooODTxnPlanList = [];
    for (var i = 0; i < transactionPlanData.length; i++) {
        var tempRecord = getTransactionPlanRecord(transactionPlanData[i], kony.i18n.getLocalizedString("More"), false);
        soGooODTxnPlanList.push(tempRecord);
    }
    if (channel == "MB") {
        frmMBSoGooodPlanList.segPlanData.setData(soGooODTxnPlanList);
        if (selectedTxnIds.length == 0) {
            frmApplySoGooodLanding.show();
            return false;
        } else {
            enableNextPlanList();
        }
    } else {
        frmIBApplySoGooODPlanList.hbxSelectedTrxnBox.setVisibility(true);
        frmIBApplySoGooODPlanList.segPlanData.setData(soGooODTxnPlanList);
        if (selectedTxnIds.length == 0) {
            frmIBApplySoGooODPlanList.hbxSelectedTrxnBox.setVisibility(false);
            disableNextButtonOfLandingIB();
        }
    }
    if (selectedTxnIds.length >= gblMaxApplySoGood) {
        disableAddButton(channel);
    } else {
        enableAddButton(channel);
    }
    if (channel == "MB" && kony.application.getCurrentForm().id != "frmMBSoGooodPlanList") {
        frmMBSoGooodPlanList.show();
    } else if (channel == "IB" && kony.application.getCurrentForm().id != "frmIBApplySoGooODPlanList") {
        frmIBApplySoGooODPlanList.show();
    }
}

function getTransactionPlanRecord(transactionPlanData, moreHide, visibleFlag) {
    var tempRecord = getTransactionPlanRecordBasic(transactionPlanData);
    tempRecord["linkMoreHide"] = moreHide;
    tempRecord["hbxInstallment"] = {
        "isVisible": visibleFlag
    };
    tempRecord["hbxInterest"] = {
        "isVisible": visibleFlag
    };
    tempRecord["hbxTotalAmount"] = {
        "isVisible": visibleFlag
    };
    tempRecord["hbxTransactionDate"] = {
        "isVisible": visibleFlag
    };
    tempRecord["hbxPostedDate"] = {
        "isVisible": visibleFlag
    };
    return tempRecord;
}

function getTransactionPlanRecordBasic(transactionPlanData) {
    var tempRecord = {
        "transactionId": transactionPlanData.transactionId,
        "planId": transactionPlanData.planId,
        "lblTransaction": transactionPlanData.lblTransaction,
        "lblAmount": kony.i18n.getLocalizedString("keyAmount"),
        "lblAmountVal": transactionPlanData.lblAmountVal,
        "lblPlan": kony.i18n.getLocalizedString("keySoGooODInstPlan") + ":",
        "lblPlanVal": getPlanData(transactionPlanData.planId) + " " + kony.i18n.getLocalizedString("keymonths"),
        "lblInstallment": kony.i18n.getLocalizedString("keySoGooODMonthlyInst") + ":",
        "lblInstallmentVal": transactionPlanData.lblInstallmentVal,
        "lblInterest": kony.i18n.getLocalizedString("keySoGooODIntCharge") + ":",
        "lblInterestVal": transactionPlanData.lblInterestVal,
        "lblTotalAmount": kony.i18n.getLocalizedString("keySoGooODTotAmt"),
        "lblTotalAmountVal": transactionPlanData.lblTotalAmountVal,
        "lblTransactionDate": kony.i18n.getLocalizedString("keyIBTransactionDate") + ":",
        "lblTransactionDateVal": transactionPlanData.lblTransactionDateVal,
        "lblPostedDate": kony.i18n.getLocalizedString("keyPostedDate"),
        "lblPostedDateVal": transactionPlanData.lblPostedDateVal,
        "imgMoreHide": {
            "src": "hideico.png"
        },
        "btnDel": " ",
        "linkMoreHide": kony.i18n.getLocalizedString("More"),
        "status": transactionPlanData.status,
        "imgStatus": transactionPlanData.imgStatus
    };
    return tempRecord;
}

function showHideMoreDetailsOfTransaction() {
    var selectedItem = frmMBSoGooodPlanList.segPlanData.selectedIndex[1];
    var selectedData = frmMBSoGooodPlanList.segPlanData.data[selectedItem];
    if (selectedData.linkMoreHide == kony.i18n.getLocalizedString("More")) {
        selectedData = moreHidePlanRecord(selectedData, kony.i18n.getLocalizedString("Hide"), true);
    } else {
        selectedData = moreHidePlanRecord(selectedData, kony.i18n.getLocalizedString("More"), false);
    }
    frmMBSoGooodPlanList.segPlanData.setDataAt(selectedData, selectedItem);
}

function deleteSelectedSoGoooDTransaction() {
    var selectedItem = frmMBSoGooodPlanList.segPlanData.selectedIndex[1];
    var selectedData = frmMBSoGooodPlanList.segPlanData.data[selectedItem];
    var selTxnId = selectedData.transactionId;
    selectedTxnIds.splice(selectedTxnIds.indexOf(selTxnId), 1);
    deleteTnxInGblTnxDataList(selTxnId, "MB");
}

function deleteTnxInGblTnxDataList(deleteTxnId, channel) {
    var txnListTmp = [];
    for (var i = 0; i < gblTransactionPlanServiceData.length; i++) {
        if (gblTransactionPlanServiceData[i].transactionId != deleteTxnId) {
            txnListTmp.push(gblTransactionPlanServiceData[i]);
        }
    }
    gblTransactionPlanServiceData = txnListTmp;
    loadSoGooODTxnPlansInSegment(gblTransactionPlanServiceData, channel);
}
//--------------------------------------------------------- Complete apply so good transaction
function loadExcutedSoGooODTxnsInSegment(excutedTransData) {
    var totalExcutedTnx = excutedTransData.length;
    var excutedSoGooODTxnsList = [];
    var failedTnxcount = 0;
    for (var i = 0; i < excutedTransData.length; i++) {
        var tempRecord;
        var failFlag = false;
        var srcImage = "correct.png";
        // fail status then display flase icon
        if (excutedTransData[i].status == '0') {
            failFlag = true;
            srcImage = "icon_false.png";
        }
        tempRecord = getTransactionPlanRecordBasic(excutedTransData[i]);
        tempRecord["imgStatus"] = srcImage;
        excutedSoGooODTxnsList.push(tempRecord);
        if (failFlag) {
            failedTnxcount++;
        }
    }
    var completedTnx = totalExcutedTnx - failedTnxcount;
    // display icon incomplete if there is fail transaction
    if (failedTnxcount > 0) {
        frmApplySoGooODComplete.imageStatus.src = "iconnotcomplete.png";
        frmApplySoGooODComplete.btnApplyMore.text = kony.i18n.getLocalizedString("btnTryAgain");
        frmApplySoGooODComplete.lblTransactionInfo.text = "Completed " + completedTnx + " of " + totalExcutedTnx + " transactions. \nPlease try again or contact 1558";
    } else {
        frmApplySoGooODComplete.imageStatus.src = "completeico.png";
        frmApplySoGooODComplete.lblTransactionInfo.text = "Completed " + completedTnx + " of " + totalExcutedTnx + " transactions.";
    }
    if (getSuccessTransactionCount() > 0) {
        frmApplySoGooODComplete.btnRight.setVisibility(true);
    } else {
        frmApplySoGooODComplete.btnRight.setVisibility(false);
    }
    frmApplySoGooODComplete.segApplyTrans.setData(excutedSoGooODTxnsList);
}

function onClickOfExcutedSoGooODTnxDetailsMore() {
    var selectedItem = frmApplySoGooODComplete.segApplyTrans.selectedIndex[1];
    var selectedData = frmApplySoGooODComplete.segApplyTrans.data[selectedItem];
    if (selectedData.linkMoreHide == kony.i18n.getLocalizedString("More")) {
        selectedData = moreHidePlanRecord(selectedData, kony.i18n.getLocalizedString("Hide"), true);
    } else {
        selectedData = moreHidePlanRecord(selectedData, kony.i18n.getLocalizedString("More"), false);
    }
    frmApplySoGooODComplete.segApplyTrans.setDataAt(selectedData, selectedItem);
}

function onClickApplySoGooODMore() {
    if (frmMBApplySoGooodComplete.btnApplyMore.text == kony.i18n.getLocalizedString("keyApplyMore")) {
        frmMBSoGooodProdBrief.show();
    } else {
        countTryAgain++;
        callApplySogoodConfirmationService();
    }
}

function onClickOfCancelSogoood() {
    //frmAccountDetailsMBPreShow();
    shortCutActDetailsPage();
    frmMBSoGooodProdBrief.show();
    //frmAccountDetailsMB.show();
}

function onClickOfBackSogoood() {
    shortCutActDetailsPage();
    frmAccountDetailsMB.show();
}

function onClickOfApplySogooodPlan() {
    var gblTotalTran = "1";
    if (gblTotalTran == "0") {
        dismissLoadingScreen();
        showAlert(callBackResponse["errMsg"], kony.i18n.getLocalizedString("info"));
        return false
    }
    frmMBSoGooodConf.lblHdrTxt.text = kony.i18n.getLocalizedString("keyConfirmation");
    frmMBSoGooodConf.lblProductHdr.text = frmMBSoGooodPlanList.lblCardNickName.text;
    frmMBSoGooodConf.show();
}

function frmApplySoGooodLandingPreshow() {
    changeStatusBarColor();
    frmApplySoGooodLanding.lblCardNickName.text = frmAccountDetailsMB.lblAccountNameHeader.text;
    frmApplySoGooodLanding.lblHdrTxt.text = kony.i18n.getLocalizedString("keyApplySoGooOD");
    frmApplySoGooodLanding.lblInfo.text = kony.i18n.getLocalizedString("keyAddSoGooOD");
    frmApplySoGooodLanding.btnCancel1.text = kony.i18n.getLocalizedString("keyCancelButton");
    frmApplySoGooodLanding.btnNext.text = kony.i18n.getLocalizedString("Next");
}

function frmMBSoGooodPlanSelectPreshow() {
    changeStatusBarColor();
    var locale = kony.i18n.getCurrentLocale();
    if (gblIndex > -1) {
        if (null != gblAccountTable["custAcctRec"][gblIndex]["acctNickName"]) {
            accNickName = gblAccountTable["custAcctRec"][gblIndex]["acctNickName"];
        } else {
            if (locale == "en_US") {
                accNickName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"];
            } else {
                accNickName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"];
            }
        }
        frmMBSoGooodPlanSelect.lblCreditCardNickName.text = accNickName;
    } else {
        frmMBSoGooodPlanSelect.lblCreditCardNickName.text = frmAccountDetailsMB.lblAccountNameHeader.text;
    }
    frmMBSoGooodPlanSelect.lblCardPlan1.text = gblPlan1 + " " + kony.i18n.getLocalizedString("keymonths");
    frmMBSoGooodPlanSelect.lblCardPlan2.text = gblPlan2 + " " + kony.i18n.getLocalizedString("keymonths");
    frmMBSoGooodPlanSelect.lblCardPlan3.text = gblPlan3 + " " + kony.i18n.getLocalizedString("keymonths");
    frmMBSoGooodPlanSelect.lblHdrTxt.text = kony.i18n.getLocalizedString("keySelectSoGooODPlan");
    frmMBSoGooodPlanSelect.lblMonthlyInstallment1.text = kony.i18n.getLocalizedString("keySoGooODMonthlyInst") + ":";
    frmMBSoGooodPlanSelect.lblMonthlyInstallment2.text = kony.i18n.getLocalizedString("keySoGooODMonthlyInst") + ":";
    frmMBSoGooodPlanSelect.lblMonthlyInstallment3.text = kony.i18n.getLocalizedString("keySoGooODMonthlyInst") + ":";
    frmMBSoGooodPlanSelect.lblTotalInterest1.text = kony.i18n.getLocalizedString("keySoGooODIntCharge") + ":";
    frmMBSoGooodPlanSelect.lblTotalInterest2.text = kony.i18n.getLocalizedString("keySoGooODIntCharge") + ":";
    frmMBSoGooodPlanSelect.lblTotalInterest3.text = kony.i18n.getLocalizedString("keySoGooODIntCharge") + ":";
    frmMBSoGooodPlanSelect.lblTotalAmount1.text = kony.i18n.getLocalizedString("keySoGooODTotAmt");
    frmMBSoGooodPlanSelect.lblTotalAmount2.text = kony.i18n.getLocalizedString("keySoGooODTotAmt");
    frmMBSoGooodPlanSelect.lblTotalAmount3.text = kony.i18n.getLocalizedString("keySoGooODTotAmt");
    frmMBSoGooodPlanSelect.btnSelect1.text = kony.i18n.getLocalizedString("btnSelect");
    frmMBSoGooodPlanSelect.btnSelect2.text = kony.i18n.getLocalizedString("btnSelect");
    frmMBSoGooodPlanSelect.btnSelect3.text = kony.i18n.getLocalizedString("btnSelect");
    frmMBSoGooodPlanSelect.btnSoGooODBack.text = kony.i18n.getLocalizedString("Receipent_Back");
    enableSelectButtonOfPlanSelect();
}

function clickBackToPlanList() {
    if (gblTransactionPlanServiceData !== null && gblTransactionPlanServiceData.length > 0) {
        frmMBSoGooodPlanList.show();
    } else {
        frmMBSoGooodTnC.show();
        //frmApplySoGooodLanding.show();
    }
}

function updateLocaleDataSeg(segData) {
    if (segData != null) {
        setIsVisibleAsFalse(segData);
        for (var i = 0; i < segData.length; i++) {
            segData[i].lblAmount = kony.i18n.getLocalizedString("keyAmount");
            segData[i].lblPlan = kony.i18n.getLocalizedString("keySoGooODInstPlan") + ":";
            segData[i].lblPlanVal = getPlanData(segData[i].planId) + " " + kony.i18n.getLocalizedString("keymonths");
            segData[i].lblInstallment = kony.i18n.getLocalizedString("keySoGooODMonthlyInst") + ":";
            segData[i].lblInterest = kony.i18n.getLocalizedString("keySoGooODIntCharge") + ":";
            segData[i].lblTotalAmount = kony.i18n.getLocalizedString("keySoGooODTotAmt");
            segData[i].lblTransactionDate = kony.i18n.getLocalizedString("keyIBTransactionDate") + ":";
            segData[i].lblPostedDate = kony.i18n.getLocalizedString("keyPostedDate");
            if (segData[i].hbxPostedDate.isVisible == true) {
                segData[i].linkMoreHide = kony.i18n.getLocalizedString("Hide");
            } else {
                segData[i].linkMoreHide = kony.i18n.getLocalizedString("More");
            }
        }
    }
    return segData;
}

function getPlanData(planId) {
    if (planId == "1") {
        return gblPlan1;
    } else if (planId == "2") {
        return gblPlan2;
    } else if (planId == "3") {
        return gblPlan3;
    }
    return "none";
}

function frmMBSoGooodPlanListPreshow() {
    changeStatusBarColor();
    frmMBSoGooodPlanList.lblCardNickName.text = frmApplySoGooodLanding.lblCardNickName.text;
    frmMBSoGooodPlanList.label474136157108684.text = kony.i18n.getLocalizedString("keyApplySoGooOD");
    frmMBSoGooodPlanList.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
    frmMBSoGooodPlanList.btnNext.text = kony.i18n.getLocalizedString("Next");
    frmMBSoGooodPlanList.segPlanData.setData(updateLocaleDataSeg(frmMBSoGooodPlanList.segPlanData.data));
}

function frmMBSoGooODTranasactionsPreshow() {
    changeStatusBarColor();
    frmMBSoGooODTranasactions.lblHdrTxt.text = kony.i18n.getLocalizedString("keySelectSoGooODTran");
    frmMBSoGooODTranasactions.btnSoGooODBack.text = kony.i18n.getLocalizedString("Receipent_Back");
    frmMBSoGooODTranasactions.btnNext.text = kony.i18n.getLocalizedString("Next");
    frmMBSoGooODTranasactions.lblSel.text = kony.i18n.getLocalizedString("keyselectall");
    localeTransaction();
    frmMBSoGooODTranasactions.segSoGooODTxns.selectedRowIndices = null;
    disableNextButton();
    if (gblHaveTransaction == 0) {
        frmMBSoGooODTranasactions.lblNoSoGoooDTxns = {
            contentAlignment: constants.CONTENT_ALIGN_CENTER
        };
        frmMBSoGooODTranasactions.lblNoSoGoooDTxns.text = kony.i18n.getLocalizedString("keyNoTransactionFound");
    } else if (gblHaveTransaction == 1) {
        frmMBSoGooODTranasactions.lblNoSoGoooDTxns.text = "[" + gblHaveTransaction + " " + kony.i18n.getLocalizedString("keyResult") + "]";
    } else {
        if (kony.i18n.getCurrentLocale() == "th_TH") {
            frmMBSoGooODTranasactions.lblNoSoGoooDTxns.text = "[" + gblHaveTransaction + " " + kony.i18n.getLocalizedString("keyResult") + "]";
        } else {
            frmMBSoGooODTranasactions.lblNoSoGoooDTxns.text = "[" + gblHaveTransaction + " " + kony.i18n.getLocalizedString("keyResult") + "s]";
        }
    }
}

function frmApplySoGooODCompletePreshow() {
    changeStatusBarColor();
    frmApplySoGooODComplete.lblCardName.text = frmMBSoGooodConf.lblProductHdr.text
    frmApplySoGooODComplete.lblTransactionInfo.text = getApplySoGooODMessage();
    frmApplySoGooODComplete.btnStatement.text = kony.i18n.getLocalizedString("keyReturn");
    frmApplySoGooODComplete.lblHdrTxt.text = kony.i18n.getLocalizedString("Complete");
    if (countTryAgain < maxTryAgain) {
        frmApplySoGooODComplete.btnApplyMore.setEnabled(true);
        frmApplySoGooODComplete.btnApplyMore.skin = btnBlueSkin;
        frmApplySoGooODComplete.btnApplyMore.focusSkin = btnBlueClose;
    } else {
        frmApplySoGooODComplete.btnApplyMore.setEnabled(false);
        frmApplySoGooODComplete.btnApplyMore.skin = btnDisableGrey;
        frmApplySoGooODComplete.btnApplyMore.focusSkin = btnDisableGrey;
    }
    if (getFailedTransactionCount() > 0) {
        frmApplySoGooODComplete.btnApplyMore.text = kony.i18n.getLocalizedString("btnTryAgain");
    } else {
        frmApplySoGooODComplete.btnApplyMore.text = kony.i18n.getLocalizedString("keyApplyMore");
        frmApplySoGooODComplete.btnApplyMore.setEnabled(true);
        frmApplySoGooODComplete.btnApplyMore.skin = btnBlueSkin;
        frmApplySoGooODComplete.btnApplyMore.focusSkin = btnBlueClose;
    }
    frmApplySoGooODComplete.segApplyTrans.setData(updateLocaleDataSeg(frmApplySoGooODComplete.segApplyTrans.data));
}

function getApplySoGooODMessage() {
    var SuccessTran = parseFloat(getSuccessTransactionCount());
    var FailTran = parseFloat(getFailedTransactionCount());
    var TotalTran = SuccessTran + FailTran;
    var message = "";
    if (SuccessTran == TotalTran) {
        message = kony.i18n.getLocalizedString("keySoGooODSuccess");
    } else {
        message = kony.i18n.getLocalizedString("keySoGooODFail");
    }
    message = message.replace("[succ]", SuccessTran);
    message = message.replace("[tot]", TotalTran);
    var locale = kony.i18n.getCurrentLocale();
    if (locale == 'en_US') {
        if (TotalTran > 1) {
            message = message.replace("[noOfTxn]", "s");
        } else {
            message = message.replace("[noOfTxn]", "");
        }
    }
    if (((FailTran > 0) && (FailTran < TotalTran)) || (SuccessTran == TotalTran)) {
        message = message + " " + kony.i18n.getLocalizedString("keySoGooODMoreDetails");
    }
    return message;
}

function getSuccessTransactionCount() {
    var successCount = 0;
    for (var i = 0; i < gblTransactionPlanServiceData.length; i++) {
        if (gblTransactionPlanServiceData[i]["status"] == '1') {
            successCount++;
        }
    }
    return successCount;
}

function getFailedTransactionCount() {
    var failCount = 0;
    for (var i = 0; i < gblTransactionPlanServiceData.length; i++) {
        if (gblTransactionPlanServiceData[i]["status"] == '0') {
            failCount++;
        }
    }
    return failCount;
}

function enableAddButton(channel) {
    if (channel == "MB") {
        frmMBSoGooodPlanList.btnRight.setEnabled(true);
        frmMBSoGooodPlanList.btnRight.skin = btnAddRecipient;
        frmMBSoGooodPlanList.btnRight.focusSkin = btnAddRecipient;
    } else {
        frmIBApplySoGooODPlanList.btnAddTxn.setEnabled(true);
        frmIBApplySoGooODPlanList.btnAddTxn.skin = btnIBaddicon;
        frmIBApplySoGooODPlanList.btnAddTxn.focusSkin = btnIBaddicon;
    }
}

function disableAddButton(channel) {
    if (channel == "MB") {
        frmMBSoGooodPlanList.btnRight.setEnabled(false);
        frmMBSoGooodPlanList.btnRight.skin = btnAddDisableRect;
        frmMBSoGooodPlanList.btnRight.focusSkin = btnAddDisableRect;
    } else {
        frmIBApplySoGooODPlanList.btnAddTxn.setEnabled(false);
        frmIBApplySoGooODPlanList.btnAddTxn.skin = btnAddDisableRect;
        frmIBApplySoGooODPlanList.btnAddTxn.focusSkin = btnAddDisableRect;
    }
}

function enableNextPlanList() {
    frmMBSoGooodPlanList.btnNext.setEnabled(true);
    frmMBSoGooodPlanList.btnNext.skin = btnBlueSkin;
    frmMBSoGooodPlanList.btnNext.focusSkin = "btnBlueSkin";
}

function enableNextButton() {
    frmMBSoGooODTranasactions.btnNext.setEnabled(true);
    frmMBSoGooODTranasactions.btnNext.skin = btnBlueSkin;
    frmMBSoGooODTranasactions.btnNext.focusSkin = "btnDarkBlueFoc";
}

function disableNextButton() {
    frmMBSoGooODTranasactions.btnNext.setEnabled(false);
    frmMBSoGooODTranasactions.btnNext.skin = btnDisableGrey;
    frmMBSoGooODTranasactions.btnNext.focusSkin = btnDisableGrey;
}

function frmMBSoGooODTranasactionsPreShow() {
    //frmMBSoGooODTranasactions.lblCardName.text = frmApplySoGooodLanding.lblCardNickName.text;
    frmMBSoGooODTranasactions.lblHdrTxt.text = kony.i18n.getLocalizedString("keySelectSoGooODTran");
    frmMBSoGooODTranasactions.btnSoGooODBack.text = kony.i18n.getLocalizedString("Receipent_Back");
    frmMBSoGooODTranasactions.btnNext.text = kony.i18n.getLocalizedString("Next");
}

function frmMBApplysogooodConfirmPreShow() {
    frmMBApplysogooodConfirm.lblHdrTxt.text = kony.i18n.getLocalizedString("lbSummaryTitle");
    frmMBApplysogooodConfirm.btnBack.text = kony.i18n.getLocalizedString("CAV05_btnCancel");
    frmMBApplysogooodConfirm.btnConfirm.text = kony.i18n.getLocalizedString("CAV07_btnConfirm");
}

function loadTermsNConditionSoGooOD() {
    var input_param = {};
    var locale = kony.i18n.getCurrentLocale();
    if (locale == "en_US") {
        input_param["localeCd"] = "en_US";
    } else {
        input_param["localeCd"] = "th_TH";
    }
    input_param["moduleKey"] = 'TMBSoGooOD';
    invokeServiceSecureAsync("readUTFFile", input_param, loadTNCSoGooODCallBack);
}

function loadTNCSoGooODCallBack(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
            var data = result["fileContent"];
            var tncTitle = kony.i18n.getLocalizedString("keyTermsNConditions");
            if (tncTitle == "Terms & Conditions") {
                tncTitle = "TermsandConditions";
            }
            showPopup(tncTitle, data);
        } else {
            dismissLoadingScreenBasedOnChannel(channel);
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}

function getSoGoodTxnsAndPlansService() {
    kony.print("getAllTxnsFromCCStmntInq!!!");
    var inputparam = {};
    showLoadingScreenBasedOnChannel(channel);
    invokeServiceSecureAsync("getAllTxnsFromCCStmntInq", inputparam, getSoGoodTxnsAndPlansServiceCallback);
}

function getSoGoodTxnsAndPlansServiceCallback(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            gblSoGooODTransactionServiceData = resulttable["CCCampaignStmtDetailDS"];
            if (resulttable["soGoooDPlanDataSet"].length >= 3) {
                gblPlan1 = formatNumberToFloat(resulttable["soGoooDPlanDataSet"][0]["interestRate"]) + "% " + resulttable["soGoooDPlanDataSet"][0]["paymentTerm"] + " ";
                gblPlan2 = formatNumberToFloat(resulttable["soGoooDPlanDataSet"][1]["interestRate"]) + "% " + resulttable["soGoooDPlanDataSet"][1]["paymentTerm"] + " ";
                gblPlan3 = formatNumberToFloat(resulttable["soGoooDPlanDataSet"][2]["interestRate"]) + "% " + resulttable["soGoooDPlanDataSet"][2]["paymentTerm"] + " ";
            }
            if (isNotBlank(resulttable["maxApplySoGooDTxns"])) {
                gblMaxApplySoGood = kony.os.toNumber(resulttable["maxApplySoGooDTxns"]);
            }
            if (isNotBlank(resulttable["maxTryAgainSoGood"])) {
                maxTryAgain = kony.os.toNumber(resulttable["maxTryAgainSoGood"]);
            }
            if (channel == "MB") {
                frmMBSoGooODTranasactions.lblNoSoGoooDTxns.text = "";
                frmMBSoGooODTranasactions.segSoGooODTxns.removeAll();
                loadSoGooODTxnsInSegment(gblSoGooODTransactionServiceData);
            } else {
                //do nothing for IB
                frmIBApplySoGooODPlanList.show();
            }
            dismissLoadingScreenBasedOnChannel();
        } else {
            dismissLoadingScreenBasedOnChannel(channel);
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}

function formatNumberToFloat(inputNumber) {
    if (isNotBlank(inputNumber) && !isNaN(inputNumber)) {
        if (parseFloat(inputNumber) > 0) {
            inputNumber = parseFloat(inputNumber).toFixed(2);
        } else {
            inputNumber = parseFloat(inputNumber);
        }
    } else {
        inputNumber = "";
    }
    return inputNumber;
}

function calculateInterestForTxns(plan, channel) {
    if (plan == "untoggle") {
        if (channel == "MB") {
            frmMBSoGooodPlanSelect.lblMonthlyInstallmentVal.text = "";
            frmMBSoGooodPlanSelect.lblTotalInterestValue.text = "";
            frmMBSoGooodPlanSelect.lblTotalAmountVal.text = "";
        } else {
            frmIBApplySoGooODPlanList.lblMonthlyInstallmentVal.text = "";
            frmIBApplySoGooODPlanList.lblTotalInterestValue.text = "";
            frmIBApplySoGooODPlanList.lblTotalAmountValue.text = "";
        }
    } else {
        var inputparam = {};
        var planValues = plan.split(' ');
        if (planValues[1] == "3") {
            gblPlanNo = "3";
        } else if (planValues[1] == "6") {
            gblPlanNo = "6";
        } else {
            gblPlanNo = "10";
        }
        var selectedtrxnIds = "";
        for (var i = 0; i < gblCurrSelTxnIds.length; i++) {
            selectedtrxnIds += gblCurrSelTxnIds[i] + "#"
        }
        inputparam["selectedTxnIds"] = selectedtrxnIds.toString();
        inputparam["selectedPlanTerm"] = planValues[1];
        inputparam["selectedInterestValue"] = planValues[0].replace('%', '');
        inputparam["calculateOrApply"] = "calculate";
        showLoadingScreenBasedOnChannel(channel);
        //alert(JSON.stringify(inputparam));
        invokeServiceSecureAsync("applySoGoodCalculateInterest", inputparam, calculateInterestForTxnsCallBack);
    }
}

function calculateAllPlanInterestForTxns() {
    var inputparam = {};
    var selectedtrxnIds = "";
    for (var i = 0; i < gblCurrSelTxnIds.length; i++) {
        selectedtrxnIds += gblCurrSelTxnIds[i] + "#"
    }
    inputparam["selectedTxnIds"] = selectedtrxnIds.toString();
    inputparam["selectedPlanTerm"] = 3; // this param not using to calculate all plan value - can put any param.
    inputparam["selectedInterestValue"] = 0; // this param not using to calculate all plan value - can put any param.
    inputparam["calculateOrApply"] = "calculate_all_plans";
    showLoadingScreenBasedOnChannel(channel);
    frmMBSoGooODTranasactions.btnNext.setEnabled(false);
    frmMBSoGooODTranasactions.btnNext.skin = btnDisableGrey;
    frmMBSoGooODTranasactions.btnNext.focusSkin = btnDisableGrey;
    invokeServiceSecureAsync("applySoGoodCalculateInterest", inputparam, calculateAllInterestForTxnsCallBack);
}

function calculateAllInterestForTxnsCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            var serviceHrsFlag = resulttable["soGoodServiceHoursFlag"];
            if (serviceHrsFlag != "true") {
                dismissLoadingScreenBasedOnChannel(channel);
                var soGooOD_StartTime = resulttable["soGooOD_StartTime"];
                var soGooOD_EndTime = resulttable["soGooOD_EndTime"];
                var errorMsg = kony.i18n.getLocalizedString("keySoGooODServiceUnavailable");
                errorMsg = errorMsg.replace("{start_time}", soGooOD_StartTime);
                errorMsg = errorMsg.replace("{end_time}", soGooOD_EndTime);
                showAlertWithCallBack(errorMsg, kony.i18n.getLocalizedString("info"), callingFrmDetailPage);
                return false;
            }
            if (channel == "MB") { //MB
                frmMBSoGooodPlanSelect.lblMonthlyInstallmentVal1.text = commaFormatted(resulttable["totalMonthlyInstallment_3"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                frmMBSoGooodPlanSelect.lblTotalInterestValue1.text = commaFormatted(resulttable["totalInterest_3"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                frmMBSoGooodPlanSelect.lblTotalAmountVal1.text = commaFormatted(resulttable["totalAmount_3"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                frmMBSoGooodPlanSelect.lblMonthlyInstallmentVal2.text = commaFormatted(resulttable["totalMonthlyInstallment_6"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                frmMBSoGooodPlanSelect.lblTotalInterestValue2.text = commaFormatted(resulttable["totalInterest_6"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                frmMBSoGooodPlanSelect.lblTotalAmountVal2.text = commaFormatted(resulttable["totalAmount_6"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                frmMBSoGooodPlanSelect.lblMonthlyInstallmentVal3.text = commaFormatted(resulttable["totalMonthlyInstallment_10"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                frmMBSoGooodPlanSelect.lblTotalInterestValue3.text = commaFormatted(resulttable["totalInterest_10"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                frmMBSoGooodPlanSelect.lblTotalAmountVal3.text = commaFormatted(resulttable["totalAmount_10"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                frmMBSoGooodPlanSelect.show();
            }
            dismissLoadingScreenBasedOnChannel(channel);
        } else {
            dismissLoadingScreenBasedOnChannel(channel);
            //Handle error in case of over credit limit
            if (resulttable["errCode"] == "99") {
                if (channel == "MB") {
                    disableSelectButtonOfPlanSelect();
                }
                showAlert(kony.i18n.getLocalizedString("keySoGooODOverCreditLimit"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errMsg"] != null || resulttable["errMsg"] != "") {
                if (channel == "MB") {
                    disableSelectButtonOfPlanSelect();
                }
                showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
                return false;
            } else {
                if (channel == "MB") {
                    disableSelectButtonOfPlanSelect();
                }
                showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
                return false;
            }
        }
        dismissLoadingScreenBasedOnChannel();
    }
}

function showLoadingScreenBasedOnChannel(channel) {
    if (channel == "MB") {
        showLoadingScreen();
    } else {
        showLoadingScreenPopup();
    }
}

function dismissLoadingScreenBasedOnChannel(channel) {
    if (channel == "MB") {
        dismissLoadingScreen();
    } else {
        dismissLoadingScreenPopup();
    }
}

function calculateInterestForTxnsCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            var serviceHrsFlag = resulttable["soGoodServiceHoursFlag"];
            if (serviceHrsFlag != "true") {
                dismissLoadingScreenBasedOnChannel(channel);
                var soGooOD_StartTime = resulttable["soGooOD_StartTime"];
                var soGooOD_EndTime = resulttable["soGooOD_EndTime"];
                var errorMsg = kony.i18n.getLocalizedString("keySoGooODServiceUnavailable");
                errorMsg = errorMsg.replace("{start_time}", soGooOD_StartTime);
                errorMsg = errorMsg.replace("{end_time}", soGooOD_EndTime);
                /*	showAlert(errorMsg, kony.i18n.getLocalizedString("info"));
              	if (channel == "MB"){
                      frmAccountDetailsMB.show();
               	}else{
                      frmIBAccntSummary.show();
               	}*/
                showAlertWithCallBack(errorMsg, kony.i18n.getLocalizedString("info"), callingFrmDetailPage);
                return false;
            }
            soGoooDPlanSelectDataSet = resulttable["soGoooDPlanSelectDataSet"];
            gblPlan = [];
            for (var i = 0; i < soGoooDPlanSelectDataSet.length; i++) {
                var selectedPlan = {
                    "plan": soGoooDPlanSelectDataSet[i]["plan"],
                    "planId": soGoooDPlanSelectDataSet[i]["planId"],
                    "transactionId": soGoooDPlanSelectDataSet[i]["transactionId"],
                    "txnAmount": soGoooDPlanSelectDataSet[i]["txnAmount"],
                    "installment": soGoooDPlanSelectDataSet[i]["installment"],
                    "interestVal": soGoooDPlanSelectDataSet[i]["interestVal"],
                    "txnDate": soGoooDPlanSelectDataSet[i]["txnDate"],
                    "postedDate": soGoooDPlanSelectDataSet[i]["postedDate"],
                    "txnDesc": soGoooDPlanSelectDataSet[i]["txnDesc"],
                    "totalAmount": soGoooDPlanSelectDataSet[i]["totalAmount"]
                }
                gblPlan.push(selectedPlan);
            }
            kony.print("gblPlanNo " + gblPlanNo);
            if (channel == "MB") { //MB
                if (gblPlanNo == "3") {
                    kony.print("Plan3 " + JSON.stringify(gblPlan));
                    frmMBSoGooodPlanSelect.lblMonthlyInstallmentVal1.text = commaFormatted(resulttable["totalMonthlyInstallment"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                    frmMBSoGooodPlanSelect.lblTotalInterestValue1.text = commaFormatted(resulttable["totalInterest"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                    frmMBSoGooodPlanSelect.lblTotalAmountVal1.text = commaFormatted(resulttable["totalAmount"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                } else if (gblPlanNo == "6") {
                    kony.print("Plan6 " + JSON.stringify(gblPlan));
                    frmMBSoGooodPlanSelect.lblMonthlyInstallmentVal2.text = commaFormatted(resulttable["totalMonthlyInstallment"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                    frmMBSoGooodPlanSelect.lblTotalInterestValue2.text = commaFormatted(resulttable["totalInterest"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                    frmMBSoGooodPlanSelect.lblTotalAmountVal2.text = commaFormatted(resulttable["totalAmount"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                } else {
                    kony.print("Plan10 " + JSON.stringify(gblPlan));
                    frmMBSoGooodPlanSelect.lblMonthlyInstallmentVal3.text = commaFormatted(resulttable["totalMonthlyInstallment"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                    frmMBSoGooodPlanSelect.lblTotalInterestValue3.text = commaFormatted(resulttable["totalInterest"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                    frmMBSoGooodPlanSelect.lblTotalAmountVal3.text = commaFormatted(resulttable["totalAmount"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                }
            } else {
                frmIBApplySoGooODPlanList.lblMonthlyInstallmentVal.text = commaFormatted(resulttable["totalMonthlyInstallment"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                frmIBApplySoGooODPlanList.lblTotalInterestValue.text = commaFormatted(resulttable["totalInterest"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                frmIBApplySoGooODPlanList.lblTotalAmountValue.text = commaFormatted(resulttable["totalAmount"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
            }
            if (channel == "MB") {
                //enableOKButtonOfPlanSelect();
            } else {
                enableOKButtonOfPlanSelectIB();
            }
            dismissLoadingScreenBasedOnChannel(channel);
        } else {
            dismissLoadingScreenBasedOnChannel(channel);
            //Handle error in case of over credit limit
            if (resulttable["errCode"] == "99") {
                if (channel == "MB") {
                    //disableOKButtonOfPlanSelect();
                } else {
                    disableOKButtonOfPlanSelectIB();
                }
                showAlert(kony.i18n.getLocalizedString("keySoGooODOverCreditLimit"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errMsg"] != null || resulttable["errMsg"] != "") {
                if (channel == "MB") {
                    //disableOKButtonOfPlanSelect();
                } else {
                    disableOKButtonOfPlanSelectIB();
                }
                showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
                return false;
            } else {
                if (channel == "MB") {
                    //disableOKButtonOfPlanSelect();
                } else {
                    disableOKButtonOfPlanSelectIB();
                }
                showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
                return false;
            }
        }
        dismissLoadingScreenBasedOnChannel();
    }
}

function confirmApplySoGoodTxns() {
    countTryAgain = 0;
    callApplySogoodConfirmationService();
}

function callApplySogoodConfirmationService() {
    var inputparam = {};
    var selectedtrxnIds = "";
    for (var i = 0; i < selectedTxnIds.length; i++) {
        selectedtrxnIds += selectedTxnIds[i] + "#"
    }
    inputparam["selectedTxnIds"] = selectedtrxnIds.toString();
    inputparam["selectedPlanTerm"] = "3";
    inputparam["selectedInterestValue"] = "0";
    inputparam["calculateOrApply"] = "confirm";
    showLoadingScreenBasedOnChannel(channel);
    invokeServiceSecureAsync("applySoGoodCalculateInterest", inputparam, confirmApplySoGoodTxnsCallBack);
}

function confirmApplySoGoodTxnsCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            var serviceHrsFlag = resulttable["soGoodServiceHoursFlag"];
            if (serviceHrsFlag != "true") {
                dismissLoadingScreenBasedOnChannel(channel);
                var soGooOD_StartTime = resulttable["soGooOD_StartTime"];
                var soGooOD_EndTime = resulttable["soGooOD_EndTime"];
                var errorMsg = kony.i18n.getLocalizedString("keySoGooODServiceUnavailable");
                errorMsg = errorMsg.replace("{start_time}", soGooOD_StartTime);
                errorMsg = errorMsg.replace("{end_time}", soGooOD_EndTime);
                showAlert(errorMsg, kony.i18n.getLocalizedString("info"));
                if (channel == "MB") {
                    frmAccountDetailsMB.show();
                } else {
                    frmIBAccntSummary.show();
                }
                return false;
            }
            soGoooDPlanSelectDataSet = resulttable["soGoooDPlanSelectDataSet"];
            gblTransactionPlanServiceData = [];
            for (var i = 0; i < soGoooDPlanSelectDataSet.length; i++) {
                var status = "1";
                if (soGoooDPlanSelectDataSet[i]["status"] == "fail") {
                    status = "0";
                } else {
                    status = "1";
                }
                var tempRecord = {
                    transactionId: soGoooDPlanSelectDataSet[i].transactionId,
                    planId: soGoooDPlanSelectDataSet[i].planId,
                    lblTransactionDateVal: soGoooDPlanSelectDataSet[i].txnDate,
                    lblTransaction: soGoooDPlanSelectDataSet[i].txnDesc,
                    lblAmountVal: commaFormatted(soGoooDPlanSelectDataSet[i].txnAmount) + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
                    lblPlanVal: soGoooDPlanSelectDataSet[i].plan + " " + kony.i18n.getLocalizedString("keymonths"),
                    lblInstallmentVal: commaFormatted(soGoooDPlanSelectDataSet[i].installment) + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
                    lblInterestVal: commaFormatted(soGoooDPlanSelectDataSet[i].interestVal) + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
                    lblTotalAmountVal: commaFormatted(soGoooDPlanSelectDataSet[i].totalAmount) + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
                    lblPostedDateVal: soGoooDPlanSelectDataSet[i].postedDate,
                    lblAmount: kony.i18n.getLocalizedString("keyAmount") + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
                    lblPlan: kony.i18n.getLocalizedString("keySoGooODInstPlan"),
                    lblInstallment: kony.i18n.getLocalizedString("keySoGooODMonthlyInst"),
                    lblInterest: kony.i18n.getLocalizedString("keySoGooODIntCharge"),
                    lblTotalAmount: kony.i18n.getLocalizedString("keySoGooODTotAmt"),
                    lblTransactionDate: kony.i18n.getLocalizedString("keyIBTransactionDate"),
                    lblPostedDate: kony.i18n.getLocalizedString("keyPostedDate"),
                    status: status
                };
                gblTransactionPlanServiceData.push(tempRecord);
            }
            if (channel == "MB") { //MB
                onClickApplySoGooODConfirm();
                if (countTryAgain > max) {
                    if (getFailedTransactionCount() > 0 && countTryAgain >= maxTryAgain) {
                        frmApplySoGooODComplete.btnApplyMore.setEnabled(false);
                        frmApplySoGooODComplete.btnApplyMore.skin = btnDisableGrey;
                        frmApplySoGooODComplete.btnApplyMore.focusSkin = btnDisableGrey;
                    } else if (getFailedTransactionCount() > 0 && countTryAgain < maxTryAgain) {
                        frmApplySoGooODComplete.btnApplyMore.text = kony.i18n.getLocalizedString("btnTryAgain");
                        frmApplySoGooODComplete.btnApplyMore.setEnabled(true);
                        frmApplySoGooODComplete.btnApplyMore.skin = btnBlueSkin;
                        frmApplySoGooODComplete.btnApplyMore.focusSkin = btnBlueClose;
                    } else {
                        frmApplySoGooODComplete.btnApplyMore.text = kony.i18n.getLocalizedString("keyApplyMore");
                        frmApplySoGooODComplete.btnApplyMore.setEnabled(true);
                        frmApplySoGooODComplete.btnApplyMore.skin = btnBlueSkin;
                        frmApplySoGooODComplete.btnApplyMore.focusSkin = btnBlueClose;
                    }
                }
            } else {
                onClickApplySoGooODConfirmIB();
                if (countTryAgain > 0) {
                    if (getFailedTransactionCount() > 0 && countTryAgain >= maxTryAgain) {
                        frmIBApplySoGooODComplete.btnApplyMore.setEnabled(false);
                        frmIBApplySoGooODComplete.btnApplyMore.skin = btnIB158disabled;
                        frmIBApplySoGooODComplete.btnApplyMore.focusSkin = btnIB158disabled;
                        frmIBApplySoGooODComplete.btnApplyMore.hoverSkin = NoSkin;
                    } else if (getFailedTransactionCount() > 0 && countTryAgain < maxTryAgain) {
                        frmIBApplySoGooODComplete.btnApplyMore.text = kony.i18n.getLocalizedString("btnTryAgain");
                        frmIBApplySoGooODComplete.btnApplyMore.setEnabled(true);
                        frmIBApplySoGooODComplete.btnApplyMore.skin = btnIB158;
                        frmIBApplySoGooODComplete.btnApplyMore.focusSkin = btnIB158active;
                        frmIBApplySoGooODComplete.btnApplyMore.hoverSkin = btnIB158active;
                    } else {
                        frmIBApplySoGooODComplete.btnApplyMore.text = kony.i18n.getLocalizedString("keyApplyMore");
                        frmIBApplySoGooODComplete.btnApplyMore.setEnabled(true);
                        frmIBApplySoGooODComplete.btnApplyMore.skin = btnIB158;
                        frmIBApplySoGooODComplete.btnApplyMore.focusSkin = btnIB158active;
                        frmIBApplySoGooODComplete.btnApplyMore.hoverSkin = btnIB158active;
                    }
                }
            }
            dismissLoadingScreenBasedOnChannel(channel);
        } else {
            dismissLoadingScreenBasedOnChannel(channel);
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
        dismissLoadingScreenBasedOnChannel(channel);
    }
}

function enableOKButtonOfPlanSelect() {
    frmMBSoGooodPlanSelect.btnNext.setEnabled(true);
    frmMBSoGooodPlanSelect.btnNext.skin = btnBlueSkin;
    frmMBSoGooodPlanSelect.btnNext.focusSkin = btnBlueSkin;
}

function disableOKButtonOfPlanSelect() {
    frmMBSoGooodPlanSelect.btnNext.setEnabled(false);
    frmMBSoGooodPlanSelect.btnNext.skin = btnDisableGrey;
    frmMBSoGooodPlanSelect.btnNext.focusSkin = btnDisableGrey;
}

function disableSelectButtonOfPlanSelect() {
    frmMBSoGooodPlanSelect.btnSelect1.setEnabled(false);
    frmMBSoGooodPlanSelect.btnSelect1.skin = btnDisableGrey;
    frmMBSoGooodPlanSelect.btnSelect1.focusSkin = btnDisableGrey;
    frmMBSoGooodPlanSelect.btnSelect2.setEnabled(false);
    frmMBSoGooodPlanSelect.btnSelect2.skin = btnDisableGrey;
    frmMBSoGooodPlanSelect.btnSelect2.focusSkin = btnDisableGrey;
    frmMBSoGooodPlanSelect.btnSelect3.setEnabled(false);
    frmMBSoGooodPlanSelect.btnSelect3.skin = btnDisableGrey;
    frmMBSoGooodPlanSelect.btnSelect3.focusSkin = btnDisableGrey;
}

function enableSelectButtonOfPlanSelect() {
    frmMBSoGooodPlanSelect.btnSelect1.setEnabled(true);
    frmMBSoGooodPlanSelect.btnSelect1.skin = btnBlueSkin;
    frmMBSoGooodPlanSelect.btnSelect1.focusSkin = btnBlueSkin;
    frmMBSoGooodPlanSelect.btnSelect2.setEnabled(true);
    frmMBSoGooodPlanSelect.btnSelect2.skin = btnBlueSkin;
    frmMBSoGooodPlanSelect.btnSelect2.focusSkin = btnBlueSkin;
    frmMBSoGooodPlanSelect.btnSelect3.setEnabled(true);
    frmMBSoGooodPlanSelect.btnSelect3.skin = btnBlueSkin;
    frmMBSoGooodPlanSelect.btnSelect3.focusSkin = btnBlueSkin;
}

function saveSoGooODCompleteAsPDFImage(filetype) {
    var inputParam = {};
    inputParam["filetype"] = filetype;
    inputParam["activityTypeId"] = "ApplySoGooOD";
    inputParam["futurePDF"] = "true";
    invokeServiceSecureAsync("generateImagePdf", inputParam, mbRenderFileCallbackfunction);
}

function postOnFBSoGooODComplete() {
    requestfromform = "frmMBApplySoGooODComplete";
    gblPOWcustNME = gblCustomerName;
    gblPOWtransXN = "ApplySoGooOD";
    gblPOWchannel = "MB";
    postOnWall();
}

function onClickEmailTnCApplySoGooOD() {
    showLoadingScreenBasedOnChannel(channel)
    var inputparam = {};
    if (channel == "MB") {
        inputparam["channelName"] = "Mobile Banking";
        inputparam["channelID"] = "02";
    } else {
        inputparam["channelName"] = "Internet Banking";
        inputparam["channelID"] = "01";
    }
    inputparam["notificationType"] = "Email"; // always email
    inputparam["phoneNumber"] = gblPHONENUMBER;
    inputparam["mail"] = gblEmailId;
    inputparam["customerName"] = gblCustomerName;
    inputparam["localeCd"] = kony.i18n.getCurrentLocale();
    inputparam["moduleKey"] = "ApplySoGooOD";
    inputparam["productName"] = "";
    inputparam["productNameTH"] = "";
    invokeServiceSecureAsync("TCEMailService", inputparam, callBackEmailTnCApplySoGooOD);
}

function callBackEmailTnCApplySoGooOD(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
            var StatusCode = result["StatusCode"];
            var Severity = result["Severity"];
            var StatusDesc = result["StatusDesc"];
            if (StatusCode == 0) {
                showAlert(kony.i18n.getLocalizedString("keyTandCApplySoGooODPopUp"), kony.i18n.getLocalizedString("info"));
                dismissLoadingScreenBasedOnChannel(channel);
            } else {
                dismissLoadingScreenBasedOnChannel(channel)
                return false;
            }
        } else {
            dismissLoadingScreenBasedOnChannel(channel)
        }
    }
}
var isCheck = true;

function onBtnCheck() {
    if (frmMBSoGooodProdBrief.btnChk.skin == "btnCheckFoc") {
        frmMBSoGooodProdBrief.btnChk.skin = "btnCheck";
        isCheck = false;
        frmMBSoGooodProdBrief.btnNext.setEnabled(false);
        frmMBSoGooodProdBrief.btnNext.skin = "btnDisableGrey";
        frmMBSoGooodProdBrief.btnNext.focusSkin = "btnDisableGrey";
    } else {
        frmMBSoGooodProdBrief.btnChk.skin = "btnCheckFoc";
        isCheck = true;
        frmMBSoGooodProdBrief.btnNext.setEnabled(true);
        frmMBSoGooodProdBrief.btnNext.skin = "btnBlueSkin";
        frmMBSoGooodProdBrief.btnNext.focusSkin = "btnDarkBlueFoc";
    }
}

function showNextTnc() {
    if (isCheck) {
        getSoGooODTransactions();
    } else {
        frmMBSoGooodTnC.show();
    }
}

function selectAllTrans() {
    /*
     *  We are constructiing array as below
     *  As we dont have sections, first element 0 indicates first section and others indicates rows 
     * as we have 10 rows.
     * 
     * frmMBSoGooODTranasactions.segSoGooODTxns.selectedIndices = [[0,[0,1,2,3,4,6,7,8,9,0]]]
     */
    var datalen = frmMBSoGooODTranasactions.segSoGooODTxns.data.length;
    var SelectedRowIndicesData = []; // Array contains selected indices across all the sections.
    var sectionindexdata = []; // Array contains selected indices specific to sections.
    var selectedRows = [];
    sectionindexdata[0] = 0;
    for (i = 0; i < datalen; i++) {
        selectedRows.push(i);
    }
    sectionindexdata.push(selectedRows);
    SelectedRowIndicesData.push(sectionindexdata);
    frmMBSoGooODTranasactions.segSoGooODTxns.selectedRowIndices = SelectedRowIndicesData;
}

function selectAllMB() {
    var txtSel = frmMBSoGooODTranasactions.lblSel.text;
    var txtSelectAll = kony.i18n.getLocalizedString("keyselectall");
    var txtDeSelectAll = kony.i18n.getLocalizedString("keydeselectall");
    var txtVal = "";
    var obj = frmMBSoGooODTranasactions.segSoGooODTxns;
    if (null != obj) {
        if (obj.data.length > 0) {
            if (txtSel == txtSelectAll) {
                frmMBSoGooODTranasactions.segSoGooODTxns.selectedRowIndices = null;
                txtVal = txtDeSelectAll;
                selectAllTrans(); // select all trans working for android and iphone
                //try to select all but not working on iphone
                //for (var i = 0; i < obj.data.length; i++) {
                //obj.selectedIndex = [0, i];
                //}
            } else {
                txtVal = txtSelectAll;
                frmMBSoGooODTranasactions.segSoGooODTxns.selectedRowIndices = null;
            }
            frmMBSoGooODTranasactions.lblSel.text = txtVal;
        }
    }
    var selectedItems = frmMBSoGooODTranasactions.segSoGooODTxns.selectedItems;
    if (selectedItems != null && selectedItems.length > 0) {
        enableNextButton();
    } else {
        disableNextButton();
    }
}