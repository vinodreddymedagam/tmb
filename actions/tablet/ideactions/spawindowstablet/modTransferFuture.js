/*function callBackOrftAccountInqIBFuture(status, resulttable) {
	if (status == 400) {
		
		if (resulttable["opstatus"] == 0) {
			
			var StatusCode = resulttable["StatusCode"];
			var Severity = resulttable["Severity"];
			var StatusDesc = resulttable["StatusDesc"];
			if (StatusCode != "0") {
				dismissLoadingScreenPopup();
				alert(resulttable["errMsg"])
				return false;
			} else {
				var ToAccountName = resulttable["ORFTTrnferInqRs"][0]["toAcctName"];
				frmIBTranferLP.lblXferToNameRcvd.text = ToAccountName;
				if (ToAccountName == null || ToAccountName == "") {
					dismissLoadingScreenPopup();
					alert("No Account Found with the given Number,Please Check");
					return false;
				} else if (ToAccountName == resulttable["ORFTTrnferInqRs"][0]["acqRoutNum"]) {
					checkDepositAccountInqIBFuture();
				}
				dismissLoadingScreenPopup();
				frmIBTransferNowConfirmation.show();
			}
		} else {
			dismissLoadingScreenPopup();
			alert(" " + resulttable["errMsg"]);
 		}
	}
}*/
/*
************************************************************************

	Module	: checkDepositAccountInq
	Author  : Kony
	Purpose : checking for TMB account Name

****/
/*function checkDepositAccountInqIBFuture() {
		var inputParam = {}
		var toAcctID;
		for (var i = 0; i < frmIBTranferLP.lblXferToContactRcvd.text.length; i++) {
			if (frmIBTranferLP.lblXferToContactRcvd.text[i] != "-") {
				if (toAcctID == null) {
					toAcctID = frmIBTranferLP.lblXferToContactRcvd.text[i];
				} else {
					toAcctID = toAcctID + frmIBTranferLP.lblXferToContactRcvd.text[i];
				}
			}
		}
		var TMB_BANK_CODE = "11";
		var TMB_BANK_FIXED_CODE = "0001";
		var TMB_BANK_CODE_ADD = "0011";
		var ZERO_PAD = "0000";
		var inputParam = {}
		// var toAcctID="0012905287";
		var accountType;
		var acctIndnt;
		if (toAcctID[3] == "2" || toAcctID[3] == "7" || toAcctID[3] == "9") {
			accountType = kony.i18n.getLocalizedString("Saving");
			acctIndnt = "0200"
		} else if (toAcctID[3] == "1") {
			accountType = kony.i18n.getLocalizedString("Current");
			acctIndnt = "0000"
		} else if (toAcctID[3] == "3") {
			accountType = kony.i18n.getLocalizedString("termDeposit");
			acctIndnt = "0300"
		} else if (toAcctID[3] == "5" || toAcctID[3] == "6") {
			accountType = "LOAN";
			alert("" + kony.i18n.getLocalizedString("Receipent_alert_correctdetails"));
			return;
		} else {
			accountType = "CC";
			alert("" + kony.i18n.getLocalizedString("Receipent_alert_correctdetails"));
			return;
		}
		var accntId;
		if (accountType == kony.i18n.getLocalizedString("Saving") || accountType == kony.i18n.getLocalizedString(
			"termDeposit")) {
			accntId = TMB_BANK_CODE_ADD + TMB_BANK_FIXED_CODE + "0" + toAcctID[0] + toAcctID[1] + toAcctID[2] + acctIndnt +
				ZERO_PAD + toAcctID;
		} else {
			accntId = TMB_BANK_CODE_ADD + TMB_BANK_FIXED_CODE + "0" + toAcctID[0] + toAcctID[1] + toAcctID[2] + acctIndnt +
				toAcctID;
		}
		
		inputParam["acctId"] = accntId;
		inputParam["acctType"] = accountType;
		inputParam["toAcctNo"] = toAcctID;
		//showLoadingScreenPopup();
		invokeServiceSecureAsync("depositAccountInquiryNonSec", inputParam, callBackDepositAccountInqIBFuture)
}*/
//function callBackDepositAccountInqIBFuture(status, resulttable) {
//	
//	if (status == 400) {
//		
//		
//		if (resulttable["opstatus"] == 0) {
//			
//			// fetching To Account Name for TMB Inq
//			var StatusCode = resulttable["StatusCode"];
//			var Severity = resulttable["Severity"];
//			var StatusDesc = resulttable["StatusDesc"];
//			dismissLoadingScreenPopup();
//			
//			if (StatusCode != "0") {
//				alert("----DepositAccountInq StatusDesc:" + StatusDesc)
//				return false;
//			} else {
//				var ToAccountName = resulttable["accountTitle"]
//				
//				if (ToAccountName != null && ToAccountName.length > 0) {
//					//happy Path; 
//					frmIBTransferNowConfirmation.lblXferToName.text = ToAccountName;
//				} else {
//					alert(" Select To Recepient Doesn't exist, Please check ");
//					return false;
//				}
//				/*** invoking fundTransferInquiry service to get the Fee Amnt fo Transaction.  */
//				checkFundTransferInqIBFuture();
//			}
//		} else {
//			dismissLoadingScreenPopup();
//			alert(" " + resulttable["errMsg"]);
//			/**dismissLoadingScreen();**/
//		}
//	}
//}
//function checkFundTransferInqIBFuture() {
//	if (gblRC_QA_TEST_VAL == 0) {} else {
//		var inputParam = {}
//		//var fromAcctID;
//		//fromAcctID=gblcwselectedData.lblActNoval;
//		var fromAcctID;
//		for (var i = 0; i < gblcwselectedData.lblActNoval.length; i++) {
//			if (gblcwselectedData.lblActNoval[i] != "-") {
//				if (fromAcctID == null) {
//					fromAcctID = gblcwselectedData.lblActNoval[i];
//				} else {
//					fromAcctID = fromAcctID + gblcwselectedData.lblActNoval[i];
//				}
//			}
//		}
//		inputParam["fromAcctNo"] = fromAcctID
//		var frmacctTypeValue;
//		if (fromAcctID[7] == "2" || fromAcctID[7] == "7" || fromAcctID[7] == "9") {
//			accountType = kony.i18n.getLocalizedString("Saving");
//			acctIndnt = "0200"
//			frmacctTypeValue = "2"
//		} else if (fromAcctID[7] == "1") {
//			accountType = kony.i18n.getLocalizedString("Current");
//			acctIndnt = "0000"
//			frmacctTypeValue = "1"
//		} else if (fromAcctID[7] == "3") {
//			accountType = kony.i18n.getLocalizedString("termDeposit");
//			acctIndnt = "0300"
//			frmacctTypeValue = "3"
//		} else if (fromAcctID[7] == "5" || fromAcctID[8] == "6") {
//			accountType = "LOAN";
//			alert("" + kony.i18n.getLocalizedString("Receipent_alert_correctdetails"));
//			return;
//		} else {
//			accountType = "CC";
//			alert("" + kony.i18n.getLocalizedString("Receipent_alert_correctdetails"));
//			return;
//		}
//		// 01 or 11
//		inputParam["fromAcctTypeValue"] = gbltdFlag;
//		//hard coding into 11 as always  will have TMB accounts in from list
//		//inputParam["fromFIIdent"] ="11";
//		var toAcctID;
//		for (var i = 0; i < frmIBTranferLP.lblXferToContactRcvd.text.length; i++) {
//			if (frmIBTranferLP.lblXferToContactRcvd.text[i] != "-") {
//				if (toAcctID == null) {
//					toAcctID = frmIBTranferLP.lblXferToContactRcvd.text[i];
//				} else {
//					toAcctID = toAcctID + frmIBTranferLP.lblXferToContactRcvd.text[i];
//				}
//			}
//		}
//		var toaccountType;
//		var toacctTypeValue;
//		if (toAcctID[3] == "2" || toAcctID[3] == "7" || toAcctID[3] == "9") {
//			toaccountType = kony.i18n.getLocalizedString("Saving");
//			toacctTypeValue = "2";
//		} else if (toAcctID[3] == "1") {
//			toaccountType = kony.i18n.getLocalizedString("Current");
//			toacctTypeValue = "1";
//		} else if (toAcctID[3] == "3") {
//			toaccountType = kony.i18n.getLocalizedString("termDeposit");
//			toacctTypeValue = "3";
//		} else if (toAcctID[3] == "5" || toAcctID[3] == "6") {
//			toaccountType = "LOAN";
//			alert("" + kony.i18n.getLocalizedString("Receipent_alert_correctdetails"));
//			return;
//		} else {
//			toaccountType = "CC";
//			alert("" + kony.i18n.getLocalizedString("Receipent_alert_correctdetails"));
//			return;
//		}
//		var accntId;
//		if (toaccountType == kony.i18n.getLocalizedString("Saving")) {
//			accntId = ZERO_PAD + toAcctID;
//		} else {
//			accntId = toAcctID;
//		}
//		inputParam["toAcctNo"] = accntId;
//		//hard coding value
//		inputParam["toAcctTypeValue"] = toaccountType;
//		inputParam["transferAmt"] = frmIBTranferLP.txbXferAmountRcvd.text + ".00";
//		inputParam["transferDate"] = gblTransferDate; //yyyy-mm-dd  hardcoading as of now.
//		inputParam["waiverCode"] = "I";
//		inputParam["tranCode"] = "88" + frmacctTypeValue + toacctTypeValue;
//		//showLoadingScreen();
//		invokeServiceSecureAsync("fundTransferInq", inputParam, callBackFundTransferInqIBFuture)
//	}
/*
*************************************************************************************
		Module	: callBackFundTransferInqIB
		Author  : Kony
		Date    : May 05, 2013
		Purpose : callback function for callBackFundTransferInqIB
****************************************************************************************
*/
//	function callBackFundTransferInqIBFuture(status, resulttable) {
//		
//		if (status == 400) {
//			
//			
//			if (resulttable["opstatus"] == 0) {
//				
//				// fetching To Account Name for TMB Inq
//				var StatusCode = resulttable["StatusCode"];
//				var Severity = resulttable["Severity"];
//				var StatusDesc = resulttable["StatusDesc"];
//				var fee = resulttable["transferFee"];
//				var FlagFeeReg = resulttable["FlagFeeReg"];
//				dismissLoadingScreenPopup();
//				
//				if (StatusCode != "0") {
//					alert("----callBackFundTransferInqMB status code:" + StatusCode)
//					return false;
//				} else {
//					if (FlagFeeReg == "I") {
//						frmIBTransferNowConfirmation.lblSplitTotFeeVal.text = fee + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
//						frmIBTransferNowConfirmation.lblFeeVal.text = fee + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
//					} else {
//						frmIBTransferNowConfirmation.lblSplitTotFeeVal.text = 0.00 + " " + kony.i18n.getLocalizedString(
//							"currencyThaiBaht");
//						frmIBTransferNowConfirmation.lblFeeVal.text = 0.00 + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
//					}
//					checkTransferTypeIB()
//					getTransferRefrenceNoIB();
//				}
//			} else {
//				dismissLoadingScreenPopup();
//				alert(" " + resulttable["errMsg"]);
//				/**dismissLoadingScreen();**/
//			}
//		}
//	}
//}
function endDateCalculatorTransferIB(staringDateFromCalendar, repeatTimesTextBoxValue) {
    var endingDate = staringDateFromCalendar;
    var numberOfDaysToAdd = 0;
    if (DailyClicked == true) {
        numberOfDaysToAdd = repeatTimesTextBoxValue * 1;
        endingDate.setDate(endingDate.getDate() + numberOfDaysToAdd - 1);
    } else if (weekClicked == true) {
        numberOfDaysToAdd = ((repeatTimesTextBoxValue - 1) * 7);
        endingDate.setDate(endingDate.getDate() + numberOfDaysToAdd);
    } else if (monthClicked == true) {
        //numberOfDaysToAdd = repeatTimesTextBoxValue * 30;
        endingDate.setDate(endingDate.getDate());
        var dd = endingDate.getDate();
        var mm = endingDate.getMonth() + 1;
        var newmm = parseFloat(mm.toString()) + parseFloat(repeatTimesTextBoxValue - 1);
        var newmmadd = newmm % 12;
        if (newmmadd == 0) {
            newmmadd = 12;
        }
        var yearAdd = Math.floor((newmm / 12));
        var y = endingDate.getFullYear();
        if (newmmadd == 12) {
            y = parseFloat(y) + parseFloat(yearAdd) - 1;
        } else y = parseFloat(y) + parseFloat(yearAdd);
        mm = parseFloat(mm.toString()) + newmmadd;
        if (newmmadd == 2) {
            if (dd > 28) {
                if (!leapYear(y)) {
                    dd = 28;
                } else {
                    dd = 29;
                }
            }
        }
        if (newmmadd == 4 || newmmadd == 6 || newmmadd == 9 || newmmadd == 11) {
            if (dd > 30) {
                dd = 30;
            }
        }
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (newmmadd < 10) {
            newmmadd = '0' + newmmadd;
        }
        var someFormattedDate = dd + '/' + newmmadd + '/' + y;
        return someFormattedDate;
    } else if (YearlyClicked == true) {
        //numberOfDaysToAdd = repeatTimesTextBoxValue * 365.26;
        var dd = endingDate.getDate();
        var mm = endingDate.getMonth() + 1;
        var y = endingDate.getFullYear();
        var newYear = parseFloat(y) + parseFloat(repeatTimesTextBoxValue - 1);
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        if (dd > 28 && mm == "02") {
            if (leapYear(newYear)) {
                dd = "29";
            } else {
                dd = "28";
            }
        }
        var someFormattedDate = dd + '/' + mm + '/' + newYear;
        return someFormattedDate;
    }
    //endingDate.setDate(endingDate.getDate() + (numberOfDaysToAdd - 1));
    var dd = endingDate.getDate();
    var mm = endingDate.getMonth() + 1;
    var y = endingDate.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    var someFormattedDate = dd + '/' + mm + '/' + y;
    return someFormattedDate;
}
//"2013/11/30" date format
//getHolydayFlag
function srvHolydayChkFutureTransSet(date) {
    var inputParam = {}
    inputParam["date"] = date;
    invokeServiceSecureAsync("getHolydayFlag", inputParam, srvHolydayChkFutureTransSetcallBack)
}

function srvHolydayChkFutureTransSetcallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            var accountFound = resulttable["statusCode"];
            if (resulttable["statusCode"].trim() == "1") {
                alert(kony.i18n.getLocalizedString("keyFTSMARTOnHoliday"));
                return false;
            } else {
                transferTokenExchangeService();
            }
        } else {
            dismissLoadingScreen();
            alert("getHolydayFlag service Not returning opstatus 0 ");
        }
    }
}

function srvHolydayChkFutureTransSetMB(date) {
    var inputParam = {}
    inputParam["date"] = date;
    invokeServiceSecureAsync("getHolydayFlag", inputParam, srvHolydayChkFutureTransSetcallBackMB)
}

function srvHolydayChkFutureTransSetcallBackMB(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            var accountFound = resulttable["statusCode"];
            if (resulttable["statusCode"].trim() == "1") {
                alert(kony.i18n.getLocalizedString("keyFTSMARTOnHoliday"));
                setEnabledTransferLandingPage(true);
                return false;
            } else {
                checkCrmProfileInq();
            }
        } else {
            dismissLoadingScreen();
            alert("getHolydayFlag service Not returning opstatus 0 ");
            setEnabledTransferLandingPage(true);
        }
    } else {
        setEnabledTransferLandingPage(true);
    }
}

function returnDateForFT() {
    var todaysDate = new Date();
    var year = todaysDate.getFullYear();
    var date = todaysDate.getDate();
    if ((date.toString().length) == 1) {
        date = "0" + date;
    }
    var month = todaysDate.getMonth() + 1;
    if ((month.toString().length) == 1) {
        month = "0" + month;
    }
    var hourTime = todaysDate.getHours();
    if ((hourTime.toString().length) == 1) {
        hourTime = "0" + hourTime;
    }
    var minuteTime = todaysDate.getMinutes();
    var timeMode = "";
    if ((minuteTime.toString().length) == 1) {
        minuteTime = "0" + minuteTime;
    }
    var sesondsTime = todaysDate.getSeconds();
    if ((sesondsTime.toString().length) == 1) {
        sesondsTime = "0" + sesondsTime;
    }
    //return  date + "/" + month + "/" + year + " " + "[" + hourTime + ":" + minuteTime+":"+sesondsTime +"]";
    // display format change for redesign transfer
    return date + "/" + month + "/" + year + " " + hourTime + ":" + minuteTime + ":" + sesondsTime;
}