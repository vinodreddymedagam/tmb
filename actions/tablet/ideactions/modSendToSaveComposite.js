









function checkVerifyOTPIBForS2SExecute(otp){
 var inputParam={};
    
    setFinActivityInputParam(inputParam);
    setNotificationAddService(inputParam);
    inputParam["appID"] = appConfig.appId;
    //activitylogging
    var maxAmount=(frmIBSSSExcuteTrnsfr.lblAmntLmtMax.text).replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    var minAmount=(frmIBSSSExcuteTrnsfr.lblAmntLmtMin.text).replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    var Amount =  frmIBSSSExcuteTrnsfr.lblTrnsAmnt.text;
	if(Amount.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht")) != -1){
		Amount = Amount.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim();
	}
    inputParam["activationActivityLog_channelId"] = "01";
    inputParam["activationActivityLog_activityTypeID"]="038"
    inputParam["activationActivityLog_activityFlexValues1"]=gblPHONENUMBER;    
    inputParam["activationActivityLog_activityFlexValues2"]=removeUnwatedSymbols(frmIBSSSExcuteTrnsfr.lblAccNo.text);
    inputParam["activationActivityLog_activityFlexValues3"]=removeUnwatedSymbols(frmIBSSSExcuteTrnsfr.lblAccNo2.text);
    inputParam["activationActivityLog_activityFlexValues4"]=removeCommas(Amount);
    
    if(gblTokenSwitchFlag ==false){
	inputParam["flowspa"] = false;
	inputParam["gblTokenSwitchFlag"] = false;
	inputParam["verifyPasswordEx_retryCounterVerifyOTP"]= gblRetryCountRequestOTP;				//gblVerifyOTP;
	inputParam["verifyPasswordEx_password"]=otp;
	inputParam["verifyPasswordEx_userId"]=gblUserName;
	frmIBSSSExcuteTrnsfr.txtBxOTP.text="";	
    invokeServiceSecureAsync("SendToSaveExecuteConfirm", inputParam, callBackExecuteComposite)
	
	}
	else{
	var inputParam ={};
	setFinActivityInputParam(inputParam);
    setNotificationAddService(inputParam);
    inputParam["appID"] = appConfig.appId;
    //activitylogging
    var maxAmount=(frmIBSSSExcuteTrnsfr.lblAmntLmtMax.text).replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    var minAmount=(frmIBSSSExcuteTrnsfr.lblAmntLmtMin.text).replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    inputParam["activationActivityLog_channelId"] = "01";
    inputParam["activationActivityLog_activityTypeID"]="038"
    inputParam["activationActivityLog_activityFlexValues1"]=gblPHONENUMBER;    
    inputParam["activationActivityLog_activityFlexValues2"]=removeUnwatedSymbols(frmIBSSSExcuteTrnsfr.lblAccNo.text);
    inputParam["activationActivityLog_activityFlexValues3"]=removeUnwatedSymbols(frmIBSSSExcuteTrnsfr.lblAccNo2.text);
    inputParam["activationActivityLog_activityFlexValues4"]=removeCommas(maxAmount);
    inputParam["activationActivityLog_activityFlexValues4"]=removeCommas(minAmount);
    inputParam["flowspa"] = false;
	inputParam["gblTokenSwitchFlag"] = true;
	inputParam["verifyTokenEx_loginModuleId"] = "IB_HWTKN";
    inputParam["verifyTokenEx_userStoreId"] = "DefaultStore";
	inputParam["verifyTokenEx_retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
    inputParam["verifyTokenEx_retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
	inputParam["verifyTokenEx_userId"] = gblUserName;
	
	inputParam["verifyTokenEx_password"] = otp;				//frmIBSSApplyCnfrmtn.tbxToken.text;
	
    inputParam["verifyTokenEx_sessionVal"] = "";
    inputParam["verifyTokenEx_segmentId"] = "segmentId";
	inputParam["verifyTokenEx_segmentIdVal"] = "MIB";
    inputParam["verifyTokenEx_channel"] = "InterNet Banking";
		
	frmIBSSSExcuteTrnsfr.tbxToken.text="";   
    invokeServiceSecureAsync("SendToSaveExecuteConfirm", inputParam, callBackExecuteComposite)
	}
}
function callBackExecuteComposite(status,result){
   
   
   if(result["opstatusVPX"] == "0"){
   	   gblVerifyOTPCounter = "0";	
   	   if(result["opstatus"] == "0"){
   	     dismissLoadingScreenPopup();
   	     executeCompleteS2S();
	     frmIBSSSExcuteTrnsfrCmplete.lblAfterBal.text = commaFormatted(result["FromAmt"])+kony.i18n.getLocalizedString("currencyThaiBaht");
	     frmIBSSSExcuteTrnsfrCmplete.lblBal2.text = commaFormatted(result["ToAmt"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
	     frmIBSSSExcuteTrnsfrCmplete.lblTrnsAmnt.text = commaFormatted(result["TranAmt"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
	     frmIBSSSExcuteTrnsfrCmplete.show();
       }else{
      	dismissLoadingScreenPopup();
   	    alert(result["errMsg"]);
   		return false;
      }
  }else{
  		
  		frmIBSSSExcuteTrnsfr.txtBxOTP.text = "";
  		if(result["opstatusVPX"] == "8005"){
  		   if(result["errCode"] == "VrfyOTPErr00001"){
  		   		gblVerifyOTPCounter = result["retryCounterVerifyOTP"];
  		   		dismissLoadingScreenPopup(); 		   		
				//alert("" + kony.i18n.getLocalizedString("invalidOTP")); //commented by swapna.
                 var curFrm = kony.application.getCurrentForm().id;
                    
                 if(curFrm == "frmIBSSSExcuteTrnsfr"){
                    frmIBSSSExcuteTrnsfr.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");//kony.i18n.getLocalizedString("invalidOTP"); //
                    frmIBSSSExcuteTrnsfr.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
                    frmIBSSSExcuteTrnsfr.hbxOTPincurrect.isVisible = true;
                    frmIBSSSExcuteTrnsfr.hbox476047582127699.isVisible = false;
                    frmIBSSSExcuteTrnsfr.hbxOTPsnt.isVisible = false;
                    frmIBSSSExcuteTrnsfr.txtBxOTP.text = "";
                    frmIBSSSExcuteTrnsfr.txtBxOTP.setFocus(true);
                  }else if(curFrm == "frmIBSSApplyCnfrmtn"){
                    frmIBSSApplyCnfrmtn.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");//kony.i18n.getLocalizedString("invalidOTP"); //
                    frmIBSSApplyCnfrmtn.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
                    frmIBSSApplyCnfrmtn.hbxOTPincurrect.isVisible = true;
                    frmIBSSApplyCnfrmtn.hboxBankRef.isVisible = false;
                    frmIBSSApplyCnfrmtn.hbxOTPsnt.isVisible = false;  
                    frmIBSSApplyCnfrmtn.txtBxOTP.text = "";    
                    frmIBSSApplyCnfrmtn.txtBxOTP.setFocus(true);                
                  }
				return false;			 
			 	
  		   }else if(result["errCode"] == "VrfyOTPErr00002"){
  		   		dismissLoadingScreenPopup();
				handleOTPLockedIB(result);
				return false;
  		   }else if (resulttable["errCode"] == "VrfyOTPErr00005") {
                dismissLoadingScreenPopup();
                alert("" + kony.i18n.getLocalizedString("invalidOTP"));
                return false;
           }else if (resulttable["errCode"] == "VrfyOTPErr00006") {
            	dismissLoadingScreenPopup();
                gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
                alert("" + resulttable["errMsg"]);
                return false;
            }else if (resulttable["errCode"] == "GenOTPRtyErr00001") {
                dismissLoadingScreenPopup();
                showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                return false;
           	}    
  		   else{
                 if(curFrm == "frmIBSSSExcuteTrnsfr"){
                    frmIBSSSExcuteTrnsfr.lblOTPinCurr.text = " ";//kony.i18n.getLocalizedString("invalidOTP"); //
                    frmIBSSSExcuteTrnsfr.lblPlsReEnter.text = " "; 
                    frmIBSSSExcuteTrnsfr.hbxOTPincurrect.isVisible = false;
                    frmIBSSSExcuteTrnsfr.hbox476047582127699.isVisible = true;
                    frmIBSSSExcuteTrnsfr.hbxOTPsnt.isVisible = true;
                    frmIBSSSExcuteTrnsfr.txtBxOTP.setFocus(true);
                  }else if(curFrm == "frmIBSSApplyCnfrmtn"){
                    frmIBSSApplyCnfrmtn.lblOTPinCurr.text = " ";//kony.i18n.getLocalizedString("invalidOTP"); //
                    frmIBSSApplyCnfrmtn.lblPlsReEnter.text = " "; 
                    frmIBSSApplyCnfrmtn.hbxOTPincurrect.isVisible = false;
                    frmIBSSApplyCnfrmtn.hboxBankRef.isVisible = true;
                    frmIBSSApplyCnfrmtn.hbxOTPsnt.isVisible = true; 
                    frmIBSSApplyCnfrmtn.txtBxOTP.setFocus(true);                 
                  }
  		   		dismissLoadingScreenPopup();
				alert(" "+result["errMsg"]);
				return false;
  		   }
  		}else{
  			dismissLoadingScreenPopup();
			//alert(" "+result["errmsg"]);
			showAlert(kony.i18n.getLocalizedString("ECGenericError"),kony.i18n.getLocalizedString("info"));
			return false;
  		
  		}
  	}
}

function setNotificationAddService(inputParam){
   // var maxAmount=(frmIBSSSExcuteTrnsfr.lblAmntLmtMax.text).replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    //var minAmount=(frmIBSSSExcuteTrnsfr.lblAmntLmtMin.text).replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");   
	
	var locale = kony.i18n.getCurrentLocale();	
	inputParam["NotificationAdd_channelName"] ="IB-INQ";
	inputParam["NotificationAdd_Locale"]= kony.i18n.getCurrentLocale();//"en_US";
	inputParam["NotificationAdd_notificationSubject"] ="";
	inputParam["NotificationAdd_notificationContent"] ="";
	inputParam["NotificationAdd_emailId"]= gblEmailId;
	inputParam["NotificationAdd_customerName"] =gblCustomerName;
	inputParam["NotificationAdd_notificationType"] ="Email";
    inputParam["NotificationAdd_source"] = "sendEmailForS2SExecution";
	inputParam["NotificationAdd_lblAmntMax"] = (frmIBSSSExcuteTrnsfr.lblAmntLmtMax.text).replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
	inputParam["NotificationAdd_lblAmntMin"] = frmIBSSSExcuteTrnsfr.lblAmntLmtMin.text;
	inputParam["NotificationAdd_lblFromAccName"] = frmIBSSSExcuteTrnsfr.lblCustName.text;
	inputParam["NotificationAdd_lblFromAccNo"] = "x-xxx-"+(frmIBSSSExcuteTrnsfr.lblAccNo.text).substring(6,11)+"-x";
	inputParam["NotificationAdd_lblToAccName"] = frmIBSSSExcuteTrnsfr.lblCustName2.text;
	inputParam["NotificationAdd_lblToAccNo"] = "x-xxx-"+(frmIBSSSExcuteTrnsfr.lblAccNo2.text).substring(6,11)+"-x";
	inputParam["NotificationAdd_lblTransAmnt"] = (frmIBSSSExcuteTrnsfr.lblTrnsAmnt.text).replace(kony.i18n.getLocalizedString("currencyThaiBaht"), ""); 
	inputParam["NotificationAdd_lblTransFee"] = " ";
	//inputParam["NotificationAdd_lblDate"] = frmIBSSSExcuteTrnsfr.lblTransDate.text;
	inputParam["NotificationAdd_lblRefNo"] = frmIBSSSExcuteTrnsfr.lblTrnsRefNo.text;
}
function setFinActivityInputParam(inputParam) {							
	inputParam["financialActivityLog_finTxnRefId"] = frmIBSSSExcuteTrnsfr.lblTrnsRefNo.text	
	var lblTrnsAmnt=(frmIBSSSExcuteTrnsfr.lblTrnsAmnt.text).replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
	//var date = frmIBSSSExcuteTrnsfr.lblTransDate.text;
    //date = replaceCommon(date, "/", "");
	//inputParam["financialActivityLog_finTxnDate"] = date;	
	inputParam["financialActivityLog_activityTypeId"] = "038";	
	inputParam["financialActivityLog_channelId"] = "01";	
	inputParam["financialActivityLog_fromAcctId"]  = removeUnwatedSymbols(frmIBSSSExcuteTrnsfr.lblAccNo.text);	
	inputParam["financialActivityLog_fromAcctName"] = exeIBFromAcctType;	
	inputParam["financialActivityLog_fromAcctNickname"] = exeIBFromAcctName;	
	inputParam["financialActivityLog_toAcctId"] = removeUnwatedSymbols(frmIBSSSExcuteTrnsfr.lblAccNo2.text);	
	inputParam["financialActivityLog_toAcctName"] = exeIBToAcctType;	
	inputParam["financialActivityLog_toAcctNickname"] = exeIBToAcctName;	
	inputParam["financialActivityLog_toBankAcctCd"] = "11";
	/*var lblTrnsAmnt=(frmIBSSSExcuteTrnsfr.lblTrnsAmnt.text).replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
	lblTrnsAmnt=removeCommas(lblTrnsAmnt);
	inputParam["financialActivityLog_finTxnAmount"] = lblTrnsAmnt;	
	*/
	inputParam["financialActivityLog_eventId"] = (frmIBSSSExcuteTrnsfr.lblTrnsRefNo.text).replace("SS", "");	
	inputParam["financialActivityLog_txnType"] = "008";	
	if(exeToAccountProdId=="221")
	inputParam["financialActivityLog_sendToSavePoint"] = "1";
	else
	inputParam["financialActivityLog_sendToSavePoint"] = "0";	
	inputParam["financialActivityLog_finFlexValues1"] = " ";
	inputParam["financialActivityLog_finFlexValues2"] = lblTrnsAmnt;
	
	
} 
 function executeCompleteS2S(){
            frmIBSSSExcuteTrnsfrCmplete.image247502979411375.src=frmIBSSSExcuteTrnsfr.image247502979411375.src;
            frmIBSSSExcuteTrnsfrCmplete.image2448366816169765.src=frmIBSSSExcuteTrnsfr.image2448366816169765.src;
            frmIBSSSExcuteTrnsfrCmplete.lblCustName.text = frmIBSSSExcuteTrnsfr.lblCustName.text;
			frmIBSSSExcuteTrnsfrCmplete.lblAcctType.text = frmIBSSSExcuteTrnsfr.lblAcctType.text;
			//frmIBSSSExcuteTrnsfrCmplete.lblAcc.text = frmIBSSSExcuteTrnsfr.lblAcc.text;
			frmIBSSSExcuteTrnsfrCmplete.lblAccNo.text = frmIBSSSExcuteTrnsfr.lblAccNo.text;
			frmIBSSSExcuteTrnsfrCmplete.lblBeforeBalMsg.text = frmIBSSSExcuteTrnsfr.lblBeforeBalMsg.text;
			frmIBSSSExcuteTrnsfrCmplete.lblBeforeBal.text = frmIBSSSExcuteTrnsfr.lblBeforeBal.text;
			frmIBSSSExcuteTrnsfrCmplete.lblAfterBalMsg.text = kony.i18n.getLocalizedString("S2S_BalAfterTrans")+":";
			
			frmIBSSSExcuteTrnsfrCmplete.lblSaveTo.text = frmIBSSSExcuteTrnsfr.lblSaveTo.text;
			frmIBSSSExcuteTrnsfrCmplete.lbLAccName2.text = frmIBSSSExcuteTrnsfr.lblCustName2.text;
			
			//frmIBSSSExcuteTrnsfrCmplete.lblAcc2.text = frmIBSSSExcuteTrnsfr.label448366816169771.text;
			frmIBSSSExcuteTrnsfrCmplete.lblAccNo2.text = frmIBSSSExcuteTrnsfr.lblAccNo2.text;
			frmIBSSSExcuteTrnsfrCmplete.lblAccType2.text = frmIBSSSExcuteTrnsfr.lblAccType2.text;			
			//frmIBSSSExcuteTrnsfrCmplete.lblS2Spoint.text = frmIBSSSExcuteTrnsfr.lblS2Spoint.text;
			//frmIBSSSExcuteTrnsfrCmplete.lbltoNoFix.text = frmIBSSSExcuteTrnsfr.lbltoNoFix.text;
			frmIBSSSExcuteTrnsfrCmplete.lblAmntLmtMax.text = frmIBSSSExcuteTrnsfr.lblAmntLmtMax.text;			
			//frmIBSSSExcuteTrnsfrCmplete.lblFrmMsg.text = frmIBSSSExcuteTrnsfr.lblFrmMsg.text;
			frmIBSSSExcuteTrnsfrCmplete.lblAmntLmtMin.text = frmIBSSSExcuteTrnsfr.lblAmntLmtMin.text;			
			//frmIBSSSExcuteTrnsfrCmplete.lblTrnsAmtMsg.text = frmIBSSSExcuteTrnsfr.lblTrnsAmtMsg.text;		
			//frmIBSSSExcuteTrnsfrCmplete.lblTrnDtMsg.text = frmIBSSSExcuteTrnsfr.lblTrnDtMsg.text;
			frmIBSSSExcuteTrnsfrCmplete.lblTransDate.text = gblCurrentDateS2S;
		   // frmIBSSSExcuteTrnsfrCmplete.lblTrnsRefMsg.text = frmIBSSSExcuteTrnsfr.lblTrnsRefMsg.text;
			frmIBSSSExcuteTrnsfrCmplete.lblTrnsRefNo.text = frmIBSSSExcuteTrnsfr.lblTrnsRefNo.text;
		    frmIBSSSExcuteTrnsfrCmplete.hbxBPSave.isVisible=true;
		    
			TMBUtil.DestroyForm(frmIBSSSExcuteTrnsfr);
 }
