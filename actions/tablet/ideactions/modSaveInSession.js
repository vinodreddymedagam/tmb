







function saveStopChequeServiceParamsInSession() {
	var inputParam = [];
    showLoadingScreenPopup();
    invokeServiceSecureAsync("tokenSwitching", inputParam, stopChequeTokenSwitchCallBack);
}

function stopChequeTokenSwitchCallBack(status, resulttable) {
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			var chequeNo = frmIBChequeServiceStopChequeLanding.txtChequeNo.text.trim();
    		inputParam = {};
			inputParam["chequeNum"] = chequeNo;
			
			invokeServiceSecureAsync("SaveStopChequeParamsInSession", inputParam, stopChequeParamsSaveInSessionCallBack);
		}
	} 
}

function stopChequeParamsSaveInSessionCallBack(status, result){
    if (status == 400) {
		
		
		if (result["opstatus"] == 0) {
			dismissLoadingScreenPopup();
            frmIBChequeServiceStopChequeAck.show();
			
		}
	}
}

function changeLimitServiceParamsInSession(amount, amount2) {
    inputParam = {};
	inputParam["amount"] = amount;
	inputParam["amount2"] = amount2;
	
	invokeServiceSecureAsync("SaveChangeLimitParamsInSession", inputParam, changeLimitSaveInSessionCallBack);
}

function changeLimitSaveInSessionCallBack(status, result){
    if (status == 400) {
		
		
		if (result["opstatus"] == 0) {
			onMyProfileMobTxLimitNextIB();
			
		}
	}
}

function execXferServiceParamsInSession(amount, accName, accNum, bankName, module) {
   /* inputParam = {};
	inputParam["amount"] = amount;
	inputParam["accountName"] = accName;
	inputParam["accountNum"] = accNum;
	inputParam["bankName"] = bankName;
	inputParam["module"] = module;
	
	invokeServiceSecureAsync("ExecuteTransferValidationService", inputParam, execXferServiceParamsCallBack);*/
	onSavingSessionXferOTPGenerate();
}   
