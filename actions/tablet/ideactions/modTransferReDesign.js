var gblAccountListTmp = [];
var gblRecipientListTmp = [];
var gblOwnAccountsListTmp = [];
var gblDeletedOwnAccountsTmp = [];

function populateMBMyRecepientBankList() {
	if(popUpBankList.segBanklist.data != null && popUpBankList.segBanklist.data.length > 0)
	{
		popUpBankList.show();
	}
			
}

function getBankListForBankPopup(globalSelectBankData, bankDataLen){
		
		var masterData = [];
		
		for (var i = 0; i < bankDataLen; i++) {
			if (globalSelectBankData[i][3].toUpperCase() == "Y" || globalSelectBankData[i][4].toUpperCase() == "Y"){			
						var bankName ="";
						
						if(kony.i18n.getCurrentLocale() == 'en_US'){
							bankName = globalSelectBankData[i][7];
						}else{
							bankName = globalSelectBankData[i][8];
						}
						
						//bankName = trimGivenNameAndAppendGivenText(bankName, 37, "..");
						var randomnum = Math.floor((Math.random() * 10000) + 1);
						var bankLogoURL="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+globalSelectBankData[i][0]+"&modIdentifier=BANKICON&dummy=" + randomnum;
						var tempData = {
							bankCode: globalSelectBankData[i][0],
							lblBankName: bankName,
							bankShortName: globalSelectBankData[i][2],
							imgBankLogo: bankLogoURL,
							bankAccntLen: globalSelectBankData[i][5],
							bankAccntLengths: globalSelectBankData[i][6]
						};
						
						masterData.push(tempData);
			}			
		}
	
	return masterData;
	
}






function onDoneToAccountNumber(){
	gblPrevLen=0;
	clearNotifyRecipientfields();
	if(frmTransferLanding.tbxAccountNumber.text.length > 0){
		if(frmTransferLanding.hbxSelectBank.isVisible){
			var  lenghtsList= MAX_ACC_LEN_LIST.split(",");
			var minimumLength = parseInt(lenghtsList[0]);
			var enteredAccountLength = removeHyphenIB(frmTransferLanding.tbxAccountNumber.text).length;
			if( MAX_ACC_LEN_LIST.indexOf(enteredAccountLength) < 0 || enteredAccountLength < minimumLength){
				popUpBankList.dismiss();
				frmTransferLanding.tbxAccountNumber.text = "";
				showAlertWithCallBack(kony.i18n.getLocalizedString("keyErrAccntNoInvalid"), kony.i18n.getLocalizedString("info"), callBackWrongAccountNumber);
				return false;
			}
			checkEnteredAccountPresentInRecipientList(false);
		} 
	}
}

function onDoneToVerifyAccountNumber(){
	gblPrevLen=0;
	clearNotifyRecipientfields();
	if(frmTransferLanding.tbxAccountNumber.text.length > 0){
		if(frmTransferLanding.hbxSelectBank.isVisible){
			var  lenghtsList= MAX_ACC_LEN_LIST.split(",");
			var minimumLength = parseInt(lenghtsList[0]);
			var enteredAccountLength = removeHyphenIB(frmTransferLanding.tbxAccountNumber.text).length;
			if( MAX_ACC_LEN_LIST.indexOf(enteredAccountLength) < 0 || enteredAccountLength < minimumLength){
				popUpBankList.dismiss();
				frmTransferLanding.tbxAccountNumber.text = "";
				showAlertWithCallBack(kony.i18n.getLocalizedString("keyErrAccntNoInvalid"), kony.i18n.getLocalizedString("info"), callBackWrongAccountNumber);
				setEnabledTransferLandingPage(true);
				return false;
			}
		} 
	}
	if(!isNotBlank(frmTransferLanding.txtTranLandAmt.text)){
		frmTransferLanding.txtTranLandAmt.setFocus(true);
		setEnabledTransferLandingPage(true);
	}
}

function onTextChangeToAccountNumber(){
	if(frmTransferLanding.tbxAccountNumber.text.length > 0){
		var  lenghtsList= MAX_ACC_LEN_LIST.split(",");
		var maxLengh =  parseInt(lenghtsList[lenghtsList.length-1])
		if(lenghtsList.length <2 && maxLengh == 10){
			onEditAccNumTransferLanding(frmTransferLanding.tbxAccountNumber.text);
		}
	}
}

function retrieveRecipientList(){
	var accountNumber = frmTransferLanding.tbxAccountNumber.text.length;
	var maxTextLength = parseInt(frmTransferLanding.tbxAccountNumber.maxTextLength);
	if(isNotBlank(accountNumber)){
		if(accountNumber == maxTextLength){
			onDoneToAccountNumber();
		}
	}
}
function selectRecepientBank() {
	var selectedData = popUpBankList.segBanklist.selectedItems[0];
	
	gblisTMB = selectedData["bankCode"];
	MAX_ACC_LEN_LIST = selectedData["bankAccntLengths"];
	
	var  lenghtsList= MAX_ACC_LEN_LIST.split(",");
	var minimumLength = parseInt(lenghtsList[0]);
	var maxLengh =  parseInt(lenghtsList[lenghtsList.length-1])
	if(lenghtsList.length <2 && maxLengh == 10){
		frmTransferLanding.tbxAccountNumber.maxTextLength =  maxLengh+3;
	}else{
		frmTransferLanding.tbxAccountNumber.maxTextLength =  maxLengh;
	}	
	frmTransferLanding.lblBankFullName.text = selectedData["lblBankName"];
	frmTransferLanding.lblBankShortName.text = selectedData["bankShortName"];
	frmTransferLanding.imgBankLogo.src = selectedData["imgBankLogo"];	
	
	frmTransferLanding.hbxPleaseSelectBank.setVisibility(false);
	frmTransferLanding.hbxSelectBank.setVisibility(true);
	//frmTransferLanding.lineBankSelect.skin = lineBlue;
	popUpBankList.dismiss();
	var enteredAccountLength =  kony.string.replace(frmTransferLanding.tbxAccountNumber.text,"-","").length;
	if (frmTransferLanding.hbxSelRecipientNumberAndName.isVisible){
		enteringAccountNumberVisbilty(true);
		frmTransferLanding.tbxAccountNumber.text = frmTransferLanding.lblTranLandToAccountNumber.text;
		frmTransferLanding.tbxAccountNumber.setFocus(true);
	//}else if (frmTransferLanding.hbxRecipientNumberAndName.isVisible && enteredAccountLength != 0){
	}else if (frmTransferLanding.tbxAccountNumber.isVisible && enteredAccountLength != 0){
			if( MAX_ACC_LEN_LIST.indexOf(enteredAccountLength) < 0 || enteredAccountLength < minimumLength){
				popUpBankList.dismiss();
				frmTransferLanding.tbxAccountNumber.text = "";
				showAlertWithCallBack(kony.i18n.getLocalizedString("keyErrAccntNoInvalid"), kony.i18n.getLocalizedString("info"), callBackWrongAccountNumber);
			}
			
			checkEnteredAccountPresentInRecipientList();
	}	else {
		frmTransferLanding.tbxAccountNumber.setFocus(true);
	}
}

function autoSelectRecepientBank(bankData) {
	var selectedData = bankData[0];
	
	gblisTMB = selectedData["bankCode"];
	MAX_ACC_LEN_LIST = getBankMaxLengths(gblisTMB);
	var  lenghtsList= MAX_ACC_LEN_LIST.split(",");
	var minimumLength = parseInt(lenghtsList[0]);
	var maxLengh =  parseInt(lenghtsList[lenghtsList.length-1])
	if(lenghtsList.length <2 && maxLengh == 10){
		frmTransferLanding.tbxAccountNumber.maxTextLength =  maxLengh+3;
	}else{
		frmTransferLanding.tbxAccountNumber.maxTextLength =  maxLengh;
	}	
	frmTransferLanding.lblBankFullName.text = selectedData["lblBankName"];
	frmTransferLanding.lblBankShortName.text = selectedData["bankShortName"];
	frmTransferLanding.imgBankLogo.src = selectedData["imgBankLogo"];	
	
	frmTransferLanding.hbxSelectBank.setVisibility(true);
	//frmTransferLanding.lineBankSelect.skin = lineBlue;
	frmTransferLanding.hbxPleaseSelectBank.setVisibility(false);
}

function callBackWrongAccountNumber() {
	enteringAccountNumberVisbilty(true);
	frmTransferLanding.tbxAccountNumber.setFocus(true);
	//frmTransferLanding.lineRecipientDetails.skin = linePopupBlack
}

function makeWidgetsBelowTransferFeeButtonVisible(value){
	frmTransferLanding.hbxTranLandShec.setVisibility(value);
	frmTransferLanding.lineTransferSchedule.setVisibility(value);
	frmTransferLanding.hbxTranLandMyNote.setVisibility(value);
	frmTransferLanding.lineRecipientNote.setVisibility(value);
	frmTransferLanding.lineRecievedBy.setVisibility(value);
	if(!gblPaynow){
		value = false;
	}
	frmTransferLanding.hbxRecievedBy.setVisibility(value);
	frmTransferLanding.lblSchedSel2.setVisibility(!value);
}

function enteringAccountNumberVisbilty(value){
	frmTransferLanding.tbxAccountNumber.setVisibility(value);
	frmTransferLanding.hbxSelRecipientNumberAndName.setVisibility(!value);
}

function transferLandingdefaultVisiblity(){
	frmTransferLanding.tbxAccountNumber.text = "";
	//frmTransferLanding.lineRecipientDetails.skin = linePopupBlack;
	//revist
	//frmTransferLanding.hboxFeeType.setVisibility(false);
	frmTransferLanding.hbxTransLndFee.setVisibility(false);
	frmTransferLanding.hbxFeeTransfer.setVisibility(true);
	frmTransferLanding.lblITMXFee.text = "";
	frmTransferLanding.lblFeeTransfer.skin = "lblWhite36px";
	//frmTransferLanding.lineFeeButton.setVisibility(false);
	
	makeWidgetsBelowTransferFeeButtonVisible(false);
	frmTransferLanding.lineMyNote.setVisibility(false);
	frmTransferLanding.hbxTranLandNotifyRec.setVisibility(false);
	frmTransferLanding.hbxOnUsNotifyRecipient.setVisibility(false);
	frmTransferLanding.lineNotifyRecipientButton.setVisibility(false);
	
	enteringAccountNumberVisbilty(true);
	
	frmTransferLanding.txtTransLndSmsNEmail.setVisibility(false);
	frmTransferLanding.txtTransLndSms.setVisibility(false);
	frmTransferLanding.lineMobileEmailTextBox.setVisibility(false);
   
    frmTransferLanding.hbxPleaseSelectBank.setVisibility(true);
    frmTransferLanding.hbxSelectBank.setVisibility(false);
    frmTransferLanding.hbxTranLandRecNote.setVisibility(false);
    
    //frmTransferLanding.hbxArrowEmailMobile.setVisibility(false);
    frmTransferLanding.hbxRecNoteEmail.setVisibility(false);
	frmTransferLanding.lineRecipientNote.setVisibility(false);
    frmTransferLanding.lblBankFullName.text = "";
	frmTransferLanding.lblBankShortName.text = "";
	frmTransferLanding.lblSchedSel2.text = "";
	frmTransferLanding.imgBankLogo.src = "tran_blank_logo.png";
}

function checkEnteredAccountPresentInRecipientList(gotoNextPage){
	var pr = "";
    var acc = ""
    var nickname ="";
	if(frmTransferLanding.segTransFrm.selectedItems == null ) {
		if(gotoNextPage){
			onTransferLndngNext();
			return;
		} else {
			setEnabledTransferLandingPage(true);
			return;
		}
	}
    pr = frmTransferLanding.segTransFrm.selectedItems[0].prodCode;
    acc = frmTransferLanding.segTransFrm.selectedItems[0].lblActNoval;
    acc = kony.string.replace(acc, "-", "");
			    
			    
	gblTransferToRecipientData = [];
	gblTransferToRecipientCache = [];
	var inputParam = {}
	//inputParam["crmId"] = gblcrmId;
	inputParam["prodId"]=pr;
	inputParam["accId"]=acc;
	var enteredAccountNumber = kony.string.replace(frmTransferLanding.tbxAccountNumber.text,"-","");
	if(acc == enteredAccountNumber && gblTMBBankCD == gblisTMB){
		gblRecipientname = gblCustomerName;
		gblTrasSMS = 1;
		gblTransEmail = 1;
		frmTransferLanding.hbxSelRecipientNumberAndName.setVisibility(true);
		//frmTransferLanding.hbxRecipientNumberAndName.setVisibility(false);
		frmTransferLanding.tbxAccountNumber.setVisibility(false);
		frmTransferLanding.lblTranLandToAccountNumber.text = encodeAccntNumbers(enteredAccountNumber);
		frmTransferLanding.lblTranLandToName.text = nickname;
		if(gotoNextPage){
		} else {
			return false;
		}
	}
	showLoadingScreen();
	var i = gbltranFromSelIndex[1];
	var fromData=[];
	fromData = frmTransferLanding.segTransFrm.data;
	var selectedFrmAccntProdCde = fromData[i].prodCode;
	if(gotoNextPage){
		if(selectedFrmAccntProdCde == "206"){
			onTransferLndngNext();
		}else{
			invokeServiceSecureAsync("getRecipientsForTransfer", inputParam, callBackFromTransferNextBtn)
		}
	}else{
		if(selectedFrmAccntProdCde != "206"){
						if(!isNotBlank(frmTransferLanding.txtTranLandAmt.text)){
							frmTransferLanding.txtTranLandAmt.setFocus(true);
							setEnabledTransferLandingPage(true);
						}
			invokeServiceSecureAsync("getRecipientsForTransfer", inputParam, callBackcheckEnteredAccountPresentInRecipientList)
		}
	}
}


function callBackcheckEnteredAccountPresentInRecipientList(status, resulttable) {
 	if (status == 400) //success responce
	{
		if (resulttable["opstatus"] == 0 || resulttable["OwnAccounts"].length>0) {
				gblAccountListTmp = resulttable["AccountsList"];
				gblRecipientListTmp = resulttable["RecepientsList"];
				gblOwnAccountsListTmp = resulttable["OwnAccounts"];
				gblDeletedOwnAccountsTmp =  resulttable["deletedOwnAccounts"];
				var enteredAccountNumber = kony.string.replace(frmTransferLanding.tbxAccountNumber.text,"-","");
				frmTransferLanding.tbxAccountNumber.text = encodeAccntNumbers(enteredAccountNumber);
				var flag = false;
				gblXferPhoneNo = "";
				gblXferEmail = "";
				if(frmTransferLanding.hbxTranLandMyNote.isVisible){
					displayHbxNotifyRecipient(true);
				}
				for(var i=0;i < gblAccountListTmp.length;i++){
					var accounts = gblAccountListTmp[i]["Accounts"+i];
					for(var j=0;j < accounts.length ; j++){
						if((accounts[j].acctId == "0000" + enteredAccountNumber || accounts[j].acctId == enteredAccountNumber) && accounts[j].bankCde == gblisTMB){
							gblRecipientname = gblRecipientListTmp [i] ["personalizedName"];

							var mno = gblRecipientListTmp [i] ["personalizedMobileNum"];
							var emailID = gblRecipientListTmp [i] ["personalizedEmailId"];
							
							 gblXferPhoneNo = "";
							 gblXferEmail = "";
							 
							if (emailID != undefined  && emailID != null && emailID.length > 0) {
								gblXferEmail = gblRecipientListTmp [i] ["personalizedEmailId"];
							}
							
							if (mno!= undefined && mno != "undefined" && mno != null && mno.length > 0) {
								gblXferPhoneNo = mno;
							}
							
							frmTransferLanding.hbxSelRecipientNumberAndName.setVisibility(true);
							frmTransferLanding.tbxAccountNumber.setVisibility(false);
							frmTransferLanding.lblTranLandToAccountNumber.text = encodeAccntNumbers(enteredAccountNumber);
							frmTransferLanding.lblTranLandToName.text = accounts [j] ["acctNickName"];
							if(frmTransferLanding.hbxTranLandNotifyRec.isVisible){
								if(frmTransferLanding.btnTranLandSms.src == "mobile_blue.png"
									|| frmTransferLanding.btnTranLandEmail.src == "email_blue.png"){
									// data has been selected before change recipient name.
								}else{
									gblTrasSMS = 0;
									gblTransEmail = 0;
								}
							}
							
							flag = true;
							break;
						}
					
					}
					
				}
				
				for(var i=0;i < gblOwnAccountsListTmp.length;i++){
					var bankCode = gblOwnAccountsListTmp[i].bankCde;
					if(!isNotBlank(bankCode)){
						//TMB Default bank code = 11
						bankCode = gblTMBBankCD;
					}
				
					if((gblOwnAccountsListTmp[i].acctId == "0000" + enteredAccountNumber || gblOwnAccountsListTmp[i].acctId == enteredAccountNumber) && bankCode == gblisTMB){
						gblRecipientname = gblCustomerName;
						var randomnum = Math.floor((Math.random()*10000)+1); 
						gblMyProfilepic = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=Y&personalizedId=&billerId=&dummy=" +randomnum;
						gblTrasSMS = 1;
						gblTransEmail = 1;
						frmTransferLanding.hbxSelRecipientNumberAndName.setVisibility(true);
						frmTransferLanding.tbxAccountNumber.setVisibility(false);
						displayHbxNotifyRecipient(false);
						frmTransferLanding.lblTranLandToAccountNumber.text = encodeAccntNumbers(enteredAccountNumber);
						frmTransferLanding.lblTranLandToName.text = gblOwnAccountsListTmp [i] ["acctNickName"];
						gblToAccountType == gblOwnAccountsListTmp[i].acctType;
						flag = true;
						break;
					}
				}
				if(flag) {
					enteringAccountNumberVisbilty(false);
					transAmountOnDone(false,true); //Amount auto populate so we need to call calculate fee service.
				} else {
					enteringAccountNumberVisbilty(true);
				}
				for(var i=0;i < gblDeletedOwnAccountsTmp.length;i++){
					if((gblDeletedOwnAccountsTmp[i].acctId == "0000" + enteredAccountNumber || gblDeletedOwnAccountsTmp[i].acctId == enteredAccountNumber) && gblTMBBankCD == gblisTMB){
						gblRecipientname = gblCustomerName;
						var randomnum = Math.floor((Math.random()*10000)+1); 
						gblMyProfilepic = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/ImageRender?crmId=Y&personalizedId=&billerId=&dummy=" +randomnum;
						gblTrasSMS = 1;
						gblTransEmail = 1;
						displayHbxNotifyRecipient(false);
						frmTransferLanding.tbxAccountNumber.text = encodeAccntNumbers(enteredAccountNumber);
						frmTransferLanding.lblTranLandToName.text = "";// gblDeletedOwnAccountsTmp [i] ["acctNickName"];  showing blank for nickname
						gblToAccountType = gblDeletedOwnAccountsTmp[i].acctType;
						flag = true;
						break;
					}
				}
				dismissLoadingScreen();
				if(!flag) {
					var isOtherTmb = frmTransferLanding.segTransFrm.selectedItems[0].isOtherTMBAllowed;
					if(isOtherTmb != "Y") {
						popUpBankList.dismiss();
						showAlertWithCallBack(kony.i18n.getLocalizedString("TRErr_NotEligibleToAcc"), kony.i18n.getLocalizedString("info"), callBackNotAllowOtherTMB);
						setEnabledTransferLandingPage(true);
					}else{
						transAmountOnDone(false,true); //Amount auto populate so we need to call calculate fee service.
						if(!isNotBlank(frmTransferLanding.txtTranLandAmt.text)){
							frmTransferLanding.txtTranLandAmt.setFocus(true);
							setEnabledTransferLandingPage(true);
						}
					}
				}else{
					if(!isNotBlank(frmTransferLanding.txtTranLandAmt.text)){
						setEnabledTransferLandingPage(true);
						frmTransferLanding.txtTranLandAmt.setFocus(true);
						return false;
					}
				}
		} else {
			dismissLoadingScreen();
			setEnabledTransferLandingPage(true);
			if(!isNotBlank(frmTransferLanding.txtTranLandAmt.text)){
				frmTransferLanding.txtTranLandAmt.setFocus(true);
				return false;
			}
		}
	}else{
		setEnabledTransferLandingPage(true);
		if(!isNotBlank(frmTransferLanding.txtTranLandAmt.text)){
			frmTransferLanding.txtTranLandAmt.setFocus(true);
			return false;
		}
	} 
}

function callBackFromTransferNextBtn(status, resulttable){
	callBackcheckEnteredAccountPresentInRecipientList(status, resulttable);
	onTransferLndngNext();
}
function callBackNotAllowOtherTMB() {
	frmTransferLanding.tbxAccountNumber.text = "";
	frmTransferLanding.tbxAccountNumber.setFocus(true);
	//frmTransferLanding.lineRecipientDetails.skin = linePopupBlack;
}




function onclickRecipientVBox(){//to focus back on text box
	enteringAccountNumberVisbilty(true);
	frmTransferLanding.tbxAccountNumber.text =   frmTransferLanding.lblTranLandToAccountNumber.text;
	frmTransferLanding.tbxAccountNumber.setFocus(true);
	onBeginEditAccountNumber();
	
}



function loadBankListForTransfers(collectionData){
		
		globalSelectBankData = [];
		
		for (var i = 0; i < collectionData.length; i++) {
			var bankName ="";
			if (collectionData[i].bankStatus.toUpperCase() == "ACTIVE"){
			
				if (kony.i18n.getCurrentLocale() == "en_US") {
					tempRecord = [collectionData[i]["bankCode"], collectionData[i]["bankNameEng"], collectionData[i]["bankShortName"], collectionData[i]["orftFlag"],
						collectionData[i]["smartFlag"],collectionData[i]["bankAccntLen"],collectionData[i]["bankAccntLengths"],collectionData[i]["bankNameEng"],collectionData[i]["bankNameThai"],collectionData[i]["promptPayFlag"]];
				} else {
					tempRecord = [collectionData[i]["bankCode"], collectionData[i]["bankNameThai"], collectionData[i]["bankShortName"], collectionData[i]["orftFlag"],
						collectionData[i]["smartFlag"],collectionData[i]["bankAccntLen"],collectionData[i]["bankAccntLengths"],collectionData[i]["bankNameEng"],collectionData[i]["bankNameThai"],collectionData[i]["promptPayFlag"]];
				}
				
				globalSelectBankData.push(tempRecord);
			}
		}		


}




function getDateFormatForTransfers(inputDate) {
	var currentdate = new Date(inputDate);
	var curMnth = currentdate.getMonth()+1;
	var curDate = currentdate.getDate();
	if ((curMnth.toString().length) == 1) {
		curMnth = "0" + curMnth;
	}
	if ((curDate.toString().length) == 1) {
		curDate = "0" + curDate;
	}
	var datetime = "" + curDate + "/" + curMnth + "/" + currentdate.getFullYear();
	
	return  datetime;
}




function resetHbxTransferDetails(){
	frmTransferConfirm.hbxAmntFeeRefNo.setVisibility(false);
	frmTransferConfirm.hbxAmountFeeSplit.setVisibility(false);
	frmTransferConfirm.hbxDebitNSmartDate.setVisibility(false);
	// for TD account
	frmTransferConfirm.hbxIntTaxAmnt.setVisibility(false);
	frmTransferConfirm.hbxNetAmntRefNo.setVisibility(false);
	frmTransferConfirm.hbxPrincipalPenalty.setVisibility(false);
	// for schedule
	frmTransferConfirm.hbxStartEndDate.setVisibility(false);
	frmTransferConfirm.hbxRepeatExcuteTimes.setVisibility(false);
	frmTransferConfirm.segTransCnfmSplit.setVisibility(false);	
	frmTransferConfirm.segTransCnfmSplit.removeAll();
}

function resetHbxTransferCompDetails(){
	frmTransfersAck.hbxAmntFeeRefNo.setVisibility(false);
	frmTransfersAck.hbxAmountFeeSplit.setVisibility(false);
	frmTransfersAck.hbxDebitNSmartDate.setVisibility(false);
	// for TD account
	frmTransfersAck.hbxIntTaxAmnt.setVisibility(false);
	frmTransfersAck.hbxNetAmntRefNo.setVisibility(false);
	frmTransfersAck.hbxPrincipalPenalty.setVisibility(false);
	// for schedule
	frmTransfersAck.hbxScheduleNote.setVisibility(false);
	frmTransfersAck.hbxStartEndDate.setVisibility(false);
	frmTransfersAck.hbxRepeatExcuteTimes.setVisibility(false);
	//frmTransfersAck.segTransAckSplit.setVisibility(false);	
	frmTransfersAck.segTransAckSplit.removeAll();
}
function displayNormalTransferComp(isMega){
	frmTransfersAck.lblTransCnfmTransDet.text = kony.i18n.getLocalizedString("TRConfirm_TranDetail");
	if(isMega){
		frmTransfersAck.hbxAmountFeeSplit.setVisibility(true);
		frmTransfersAck.lblTransCnfmTotAmt2.text = kony.i18n.getLocalizedString("TRConfirm_Amount");
		frmTransfersAck.lblCnfmToRefTitle2.text = kony.i18n.getLocalizedString("TRConfirm_RefNo");
		frmTransfersAck.lblHideMore.text = kony.i18n.getLocalizedString("show");
		frmTransfersAck.imgMore.src = "arrow_down.png";
	}else{
		frmTransfersAck.hbxAmntFeeRefNo.setVisibility(true);
		frmTransfersAck.lblTransCnfmTotAmt.text = kony.i18n.getLocalizedString("TRConfirm_Amount");
		frmTransfersAck.lblCnfmToRefTitle1.text = kony.i18n.getLocalizedString("TRConfirm_RefNo");
	}
	frmTransfersAck.hbxDebitNSmartDate.setVisibility(true);
	frmTransfersAck.lblTransNPbAckDate.text = kony.i18n.getLocalizedString("TRConfirm_Debit");
	frmTransfersAck.lblSmartDate.text = kony.i18n.getLocalizedString("TRConfirm_Receive");
	var orftFlag = getORFTFlag(gblisTMB);
	var smartFlag = getSMARTFlag(gblisTMB);
	if(gblSelTransferMode == 1 && 
		(isNotBlank(orftFlag) && isNotBlank(smartFlag) && orftFlag == "N" && smartFlag == "Y")
		|| gblTransSMART == 1){
		frmTransfersAck.lblSmartDate.text = kony.i18n.getLocalizedString("TRConfirm_Receive02");
	}
}
function displayNormalTransfer(isMega){
	frmTransferConfirm.lblTransCnfmTransDet.text = kony.i18n.getLocalizedString("TRConfirm_TranDetail");
	if(isMega){
		frmTransferConfirm.hbxAmountFeeSplit.setVisibility(true);
		frmTransferConfirm.lblTransCnfmTotAmt2.text = kony.i18n.getLocalizedString("TRConfirm_Amount");
		frmTransferConfirm.lblCnfmToRefTitle2.text = kony.i18n.getLocalizedString("TRConfirm_RefNo");
		frmTransferConfirm.lblHideMore.text = kony.i18n.getLocalizedString("show");
		frmTransferConfirm.imgMore.src = "arrow_down.png";
	}else{
		frmTransferConfirm.hbxAmntFeeRefNo.setVisibility(true);
		frmTransferConfirm.lblTransCnfmTotAmt.text = kony.i18n.getLocalizedString("TRConfirm_Amount");
		frmTransferConfirm.lblCnfmToRefTitle1.text = kony.i18n.getLocalizedString("TRConfirm_RefNo");
	}
	frmTransferConfirm.hbxDebitNSmartDate.setVisibility(true);
	frmTransferConfirm.lblTransCnfmDate.text = kony.i18n.getLocalizedString("TRConfirm_Debit");
	frmTransferConfirm.lblSmartDate.text = kony.i18n.getLocalizedString("TRConfirm_Receive");
	var orftFlag = getORFTFlag(gblisTMB);
	var smartFlag = getSMARTFlag(gblisTMB);
	if(gblSelTransferMode == 1 && 
		(isNotBlank(orftFlag) && isNotBlank(smartFlag) && orftFlag == "N" && smartFlag == "Y")
		|| gblTransSMART == 1){
		frmTransferConfirm.lblSmartDate.text = kony.i18n.getLocalizedString("TRConfirm_Receive02");
	}
}

function displayScheduleTransfer(isMega){
	frmTransferConfirm.lblTransCnfmTransDet.text = kony.i18n.getLocalizedString("TRConfirm_SchDetails");
	if(isMega){
		frmTransferConfirm.hbxAmountFeeSplit.setVisibility(true);
		frmTransferConfirm.lblTransCnfmTotAmt2.text = kony.i18n.getLocalizedString("TRConfirm_Amount");
		frmTransferConfirm.lblCnfmToRefTitle2.text = kony.i18n.getLocalizedString("TRConfirm_RefNo");
		frmTransferConfirm.lblScheduleNote.text = kony.i18n.getLocalizedString("TRConfirm_FeeNotice");
		frmTransferConfirm.lblHideMore.text = kony.i18n.getLocalizedString("show");
		frmTransferConfirm.imgMore.src = "arrow_down.png";
	}else{
		frmTransferConfirm.hbxAmntFeeRefNo.setVisibility(true);
		frmTransferConfirm.lblTransCnfmTotAmt.text = kony.i18n.getLocalizedString("TRConfirm_Amount");
		frmTransferConfirm.lblCnfmToRefTitle1.text = kony.i18n.getLocalizedString("TRConfirm_RefNo");
	}
	frmTransferConfirm.hbxStartEndDate.setVisibility(true);
	frmTransferConfirm.lblStartOn.text = kony.i18n.getLocalizedString("TRConfirm_SchDebit");
	frmTransferConfirm.lblEndOn.text = kony.i18n.getLocalizedString("TRConfirm_SchEnd");
	frmTransferConfirm.hbxRepeatExcuteTimes.setVisibility(true);
	frmTransferConfirm.lblRepeatAs.text = kony.i18n.getLocalizedString("TRConfirm_SchRepeat");
	frmTransferConfirm.lblExecute.text = kony.i18n.getLocalizedString("TRConfirm_SchExe");
}

function displayScheduleTransferComp(isMega){
	frmTransfersAck.lblTransCnfmTransDet.text = kony.i18n.getLocalizedString("TRConfirm_SchDetails");
	if(frmTransferConfirm.hbxScheduleNote.isVisible){
		frmTransfersAck.hbxScheduleNote.setVisibility(true);
		frmTransfersAck.lblScheduleNote.text = kony.i18n.getLocalizedString("TRConfirm_FeeNotice");
	}
	if(isMega){
		frmTransfersAck.hbxAmountFeeSplit.setVisibility(true);
		frmTransfersAck.lblTransCnfmTotAmt2.text = kony.i18n.getLocalizedString("TRConfirm_Amount");
		frmTransfersAck.lblCnfmToRefTitle2.text = kony.i18n.getLocalizedString("TRConfirm_RefNo");

		frmTransfersAck.lblHideMore.text = kony.i18n.getLocalizedString("show");
		frmTransfersAck.imgMore.src = "arrow_down.png";
	}else{
		frmTransfersAck.hbxAmntFeeRefNo.setVisibility(true);
		frmTransfersAck.lblTransCnfmTotAmt.text = kony.i18n.getLocalizedString("TRConfirm_Amount");
		frmTransfersAck.lblCnfmToRefTitle1.text = kony.i18n.getLocalizedString("TRConfirm_RefNo");
	}
	frmTransfersAck.hbxStartEndDate.setVisibility(true);
	frmTransfersAck.lblStartOn.text = kony.i18n.getLocalizedString("TRConfirm_SchDebit");
	frmTransfersAck.lblEndOn.text = kony.i18n.getLocalizedString("TRConfirm_SchEnd");
	frmTransfersAck.hbxRepeatExcuteTimes.setVisibility(true);
	frmTransfersAck.lblRepeatAs.text = kony.i18n.getLocalizedString("TRConfirm_SchRepeat");
	frmTransfersAck.lblExecute.text = kony.i18n.getLocalizedString("TRConfirm_SchExe");
}
function displayTDTransferDetails(){
	frmTransferConfirm.lblTransCnfmTransDet.text = kony.i18n.getLocalizedString("TRConfirm_TranDetail");
	frmTransferConfirm.lblTransAmtFeeTitle.text = kony.i18n.getLocalizedString("TRConfirm_NetAmount");
	frmTransferConfirm.hbxPrincipalPenalty.setVisibility(true);
	frmTransferConfirm.lblTransCnfmPenaltyAmt.text = kony.i18n.getLocalizedString("TRConfirm_TDPenalty");
	frmTransferConfirm.lblPrincipalAmnt.text = kony.i18n.getLocalizedString("TRConfirm_Principal");
	frmTransferConfirm.hbxIntTaxAmnt.setVisibility(true);
	frmTransferConfirm.lblTransCnfmIntAmt.text = kony.i18n.getLocalizedString("TRConfirm_TDInt");
	frmTransferConfirm.lblTransCnfmTaxAmt.text = kony.i18n.getLocalizedString("TRConfirm_TDTax");
	frmTransferConfirm.hbxNetAmntRefNo.setVisibility(true);
	frmTransferConfirm.lblTransCnfmNetAmt.text = kony.i18n.getLocalizedString("TRConfirm_TDNet");
	frmTransferConfirm.lblTransCnfmRefNum.text = kony.i18n.getLocalizedString("TRConfirm_RefNo");
	frmTransferConfirm.hbxDebitNSmartDate.setVisibility(true);
	frmTransferConfirm.lblTransCnfmDate.text = kony.i18n.getLocalizedString("TRConfirm_Debit");
	frmTransferConfirm.lblSmartDate.text = kony.i18n.getLocalizedString("TRConfirm_Receive");
}

function displayTDTransferCompDetails(){
	frmTransfersAck.lblTransCnfmTransDet.text = kony.i18n.getLocalizedString("TRConfirm_TranDetail");
	frmTransfersAck.lblTransAmtFeeTitle.text = kony.i18n.getLocalizedString("TRConfirm_NetAmount");
	frmTransfersAck.hbxPrincipalPenalty.setVisibility(true);
	frmTransfersAck.lblTransNPbAckPenaltyAmt.text = kony.i18n.getLocalizedString("TRConfirm_TDPenalty");
	frmTransfersAck.lblPrincipalAmnt.text = kony.i18n.getLocalizedString("TRConfirm_Principal");
	frmTransfersAck.hbxIntTaxAmnt.setVisibility(true);
	frmTransfersAck.lblTransNPbAckIntAmt.text = kony.i18n.getLocalizedString("TRConfirm_TDInt");
	frmTransfersAck.lblTransNPbAckTaxAmt.text = kony.i18n.getLocalizedString("TRConfirm_TDTax");
	frmTransfersAck.hbxNetAmntRefNo.setVisibility(true);
	frmTransfersAck.lblTransNPbAckNetAmt.text = kony.i18n.getLocalizedString("TRConfirm_TDNet");
	frmTransfersAck.lblTransNPbAckRef.text = kony.i18n.getLocalizedString("TRConfirm_RefNo");
	frmTransfersAck.hbxDebitNSmartDate.setVisibility(true);
	frmTransfersAck.lblTransNPbAckDate.text = kony.i18n.getLocalizedString("TRConfirm_Debit");
	frmTransfersAck.lblSmartDate.text = kony.i18n.getLocalizedString("TRConfirm_Receive");
}



function ehFrmTransferLandingHbxRecNoteEmailOnClick(){
	if(isNotBlank(frmTransferLanding.txtTransLndSmsNEmail.text)){
		frmTransferLanding.textRecNoteEmail.setFocus(true);
	}
}

function ehFrmTransferLandingHbxRecNoteSmsOnClick(){
	if(isNotBlank(frmTransferLanding.txtTransLndSms.text)){
		var mobileNo = removeHyphenIB(frmTransferLanding.txtTransLndSms.text);
		frmTransferLanding.txtTransLndSms.text = onEditMobileNumberP2P(mobileNo);
		frmTransferLanding.txtTranLandRecNote.setFocus(true);
	}
}

function showAmountFeeSplit(){
	if(frmTransferConfirm.segTransCnfmSplit.isVisible){
		frmTransferConfirm.lblHideMore.text = kony.i18n.getLocalizedString("show");
		frmTransferConfirm.imgMore.src = "arrow_down.png";
		frmTransferConfirm.segTransCnfmSplit.setVisibility(false);
		
	}else{
		frmTransferConfirm.lblHideMore.text = kony.i18n.getLocalizedString("Hide");
		frmTransferConfirm.imgMore.src = "arrow_up.png";
		frmTransferConfirm.segTransCnfmSplit.setVisibility(true);
	}
}
function showAmountFeeSplitTransferComp(){
	if(frmTransfersAck.segTransAckSplit.isVisible){
		frmTransfersAck.lblHideMore.text = kony.i18n.getLocalizedString("show");
		frmTransfersAck.imgMore.src = "arrow_down.png";
		frmTransfersAck.segTransAckSplit.setVisibility(false);
	}else{
		frmTransfersAck.lblHideMore.text = kony.i18n.getLocalizedString("Hide");
		frmTransfersAck.imgMore.src = "arrow_up.png";
		frmTransfersAck.segTransAckSplit.setVisibility(true);
	}
}






function onTextChangetxtTranLandRecNote(){
	if(frmTransferLanding.txtTranLandRecNote.text == ""){
		//frmTransferLanding.lineRecipientNote.skin = linePopupBlack;
	}else{
		//frmTransferLanding.lineRecipientNote.skin = lineBlue;
	}
} 



/**************************************************************************************
		Module	: toggleTDMaturityCombobox
		Author  : Kony
		Date    : Feb 29, 2016
		Purpose : displaying TDmaturityDetails combobox if selected from account is TD
****************************************************************************************/

function toggleTDMaturityCombobox(gbltdFlag) {
	var temptdFlag;
	temptdFlag = kony.i18n.getLocalizedString("termDeposit");
	frmTransferLanding.hbxTranLandAmt.setEnabled(true);
	frmTransferLanding.vbxSelRecipient.onClick = ehFrmTransferLanding_btnTranLandToSel_onClick;
	frmTransferLanding.hbxSelRecipientNumberAndName.setEnabled(true);
	enableTransferTextBoxAmount(true);
	if (gbltdFlag == temptdFlag) {
		showLoadingScreen();
		frmTransferLanding.hboxTD.setVisibility(true);
		frmTransferLanding.lineTD.setVisibility(true); 
		frmTransferLanding.hbxMaturityPrincipal.setVisibility(false);
		frmTransferLanding.lblSelectMaturity.setVisibility(true);
		frmTransferLanding.tbxAccountNumber.setEnabled(true);
		enableTransferTextBoxAmount(false);
		makeWidgetsBelowTransferFeeButtonVisible(true);
		frmTransferLanding.lineMyNote.setVisibility(false);
		frmTransferLanding.lineNotifyRecipientButton.setVisibility(false);
		frmTransferLanding.lineMobileEmailTextBox.setVisibility(false);
		frmTransferLanding.lineRecipientNote.setVisibility(true);
		getTDAccount();
	} else {
		frmTransferLanding.hboxTD.setVisibility(false);
 		frmTransferLanding.lineTD.setVisibility(false);
		var i = gbltranFromSelIndex[1];
		var fromData = frmTransferLanding.segTransFrm.data;
		var selectedFrmAccntProdCde = fromData[i].prodCode;
		if(selectedFrmAccntProdCde == "206"){
			frmTransferLanding.hbxSelRecipientNumberAndName.setEnabled(false);
			depositAccInqDreamSavMB();
			frmTransferLanding.tbxAccountNumber.setEnabled(false);
		}
		else{
		  	frmTransferLanding.vbxSelRecipient.onClick = ehFrmTransferLanding_btnTranLandToSel_onClick;
		    frmTransferLanding.tbxAccountNumber.setEnabled(true);
		}
	}
	if(frmTransferLanding.hbxTranLandMyNote.isVisible){
		frmTransferLanding.lineRecipientNote.setVisibility(true);
	}else{
		frmTransferLanding.lineRecipientNote.setVisibility(false);
	}
	if(frmTransferLanding.hbxTranLandNotifyRec.isVisible || 
		frmTransferLanding.hbxOnUsNotifyRecipient.isVisible){
		frmTransferLanding.lineMyNote.setVisibility(true);
	}else{
		frmTransferLanding.lineMyNote.setVisibility(false);
	}
 }
 
function resetTransferLandingBasedOnToAccount(){
	if(frmTransferLanding.segTransFrm.selectedItems != null) {
		var isOtherBank = frmTransferLanding.segTransFrm.selectedItems[0].isOtherBankAllowed;
		var isOtherTmb = frmTransferLanding.segTransFrm.selectedItems[0].isOtherTMBAllowed;
		var isAllowedSA = frmTransferLanding.segTransFrm.selectedItems[0].isAllowedSA;
		var isAllowedCA = frmTransferLanding.segTransFrm.selectedItems[0].isAllowedCA;
		var isAllowedTD = frmTransferLanding.segTransFrm.selectedItems[0].isAllowedTD;
		var fromAccNumber = frmTransferLanding.segTransFrm.selectedItems[0].accountNum;
		var prodCode = frmTransferLanding.segTransFrm.selectedItems[0].prodCode;
	}
	if(prodCode == "206"){ // account dream saving = 206
		fromAccountIsOnlyAllowedForTMB();
		return;
	}
	//if(frmTransferLanding.hbxRecipientNumberAndName.isVisible && isNotBlank(frmTransferLanding.tbxAccountNumber.text) && isNotBlank(gblisTMB)){
	if(frmTransferLanding.tbxAccountNumber.isVisible && isNotBlank(frmTransferLanding.tbxAccountNumber.text) && isNotBlank(gblisTMB)){
		if(gblTMBBankCD != gblisTMB){//other bank
			if(isOtherBank != "Y") {
				ResetTransferHomePage();
				//showAlertWithCallBack("TODO: RAVI Can not enter Other Bank", kony.i18n.getLocalizedString("info"), callBackNotAllowOtherTMB);
			}else{
				transAmountOnDone(false,true); //Amount auto populate so we need to call calculate fee service.
			}
		}else{
			if(removeHyphenIB(frmTransferLanding.tbxAccountNumber.text) == removeHyphenIB(fromAccNumber)){
					//ResetTransferHomePage();
			}
			if(isOtherTmb != "Y") {//other TMB
				ResetTransferHomePage();
				//showAlertWithCallBack("TODO:RAVI  Can not enter Other TMB", kony.i18n.getLocalizedString("info"), callBackNotAllowOtherTMB);
			}else{
				transAmountOnDone(false,true); //Amount auto populate so we need to call calculate fee service.
			}
		}
	}else if(frmTransferLanding.hbxSelRecipientNumberAndName.isVisible && isNotBlank(gblisTMB)){
		if(gblTMBBankCD != gblisTMB){//other bank
			if(isOtherBank != "Y") {
				ResetTransferHomePage();
				showAlertWithCallBack(kony.i18n.getLocalizedString("TRErr_NotEligibleToAcc"), kony.i18n.getLocalizedString("info"), callBackNotAllowOtherTMB);
			}else{
				transAmountOnDone(false,true); //Amount auto populate so we need to call calculate fee service.
			}
		}else{
			if( gblTrasSMS == 1 && gblTransEmail == 1){//own TMB  TRErr_
				if(gblToAccountType == "SDA"){
					if(isAllowedSA != "Y") {
						ResetTransferHomePage();
						showAlertWithCallBack(kony.i18n.getLocalizedString("TRErr_NotEligibleToAcc"), kony.i18n.getLocalizedString("info"), callBackNotAllowOtherTMB);
					}else{
						transAmountOnDone(false,true); //Amount auto populate so we need to call calculate fee service.
					}
				}else if(gblToAccountType == "DDA"){
					if(isAllowedCA != "Y") {
						ResetTransferHomePage();
						showAlertWithCallBack(kony.i18n.getLocalizedString("TRErr_NotEligibleToAcc"), kony.i18n.getLocalizedString("info"), callBackNotAllowOtherTMB);
					}else{
						transAmountOnDone(false,true); //Amount auto populate so we need to call calculate fee service.
					}
				}else if(gblToAccountType == "CDA"){
					if(isAllowedTD != "Y") {
						ResetTransferHomePage();
						showAlertWithCallBack(kony.i18n.getLocalizedString("TRErr_NotEligibleToAcc"), kony.i18n.getLocalizedString("info"), callBackNotAllowOtherTMB);
					}else{
						transAmountOnDone(false,true); //Amount auto populate so we need to call calculate fee service.
					}
				}else{
						transAmountOnDone(false,true); //Amount auto populate so we need to call calculate fee service.
				}
			}else if(isOtherTmb != "Y"  ) {//other TMB
				ResetTransferHomePage();
				showAlertWithCallBack(kony.i18n.getLocalizedString("TRErr_NotEligibleToAcc"), kony.i18n.getLocalizedString("info"), callBackNotAllowOtherTMB);
			}else{
				transAmountOnDone(false,true); //Amount auto populate so we need to call calculate fee service.
			}
		}
	}
	fromAccountIsOnlyAllowedForTMB();
}




/*************************************************************************

	Module	: onClickLeftArrow1Transfer
	Author  : Kony
	Purpose : Onclick for left arrow in cover flow

****/

function onClickLeftArrowTransfer() {
		if (isNotBlank(gbltdFlag) && gbltdFlag == kony.i18n.getLocalizedString("termDeposit")) {
			frmTransferLanding.txtTranLandAmt.text = "";//Redesign transfer clearing amount if previous from account is TD
		}
		//frmTransferLanding.btnSchedTo.onClick = ehFrmTransferLanding_btnSchedTo_onClick;

		if (gbltranFromSelIndex[1] == 0){
			frmTransferLanding.segTransFrm.selectedIndex = [0, (gbltranFromSelIndex[1] + (gblNoOfFromAcs - 1))];
			gbltranFromSelIndex = frmTransferLanding.segTransFrm.selectedIndex;
			gbltdFlag = frmTransferLanding.segTransFrm.selectedItems[0].tdFlag;
			gblSelTransferFromAcctNo = frmTransferLanding.segTransFrm.selectedItems[0].accountNum;
			if(gblDeviceInfo["name"] != "iPhone"){
				toggleTDMaturityCombobox(gbltdFlag);
			}
		}
		else{
		
			frmTransferLanding.segTransFrm.selectedIndex = [0, (gbltranFromSelIndex[1] - 1)];
			gbltranFromSelIndex = frmTransferLanding.segTransFrm.selectedIndex;
			gbltdFlag = frmTransferLanding.segTransFrm.selectedItems[0].tdFlag;
			gblSelTransferFromAcctNo = frmTransferLanding.segTransFrm.selectedItems[0].accountNum;
		}
	
		if(gblDeviceInfo["name"] == "iPhone"){
			toggleTDMaturityCombobox(gbltdFlag);
		}
			
		resetTransferLandingBasedOnToAccount();
		popUpBankList.dismiss();
}
/*************************************************************************

	Module	: onClickRightArrow1Transfer
	Author  : Kony
	Purpose : Onclick for right arrow in cover flow

****/



function clearNotifyRecipientfields(){
	frmTransferLanding.txtTransLndSms.setVisibility(false);
	frmTransferLanding.txtTransLndSmsNEmail.setVisibility(false);
	frmTransferLanding.txtTranLandRecNote.text = "";
	frmTransferLanding.textRecNoteEmail.text = "";
	frmTransferLanding.hbxTranLandRecNote.setVisibility(false);
	frmTransferLanding.hbxRecNoteEmail.setVisibility(false);
	frmTransferLanding.btnTranLandSms.src = "mobile_grey.png";
	frmTransferLanding.btnTranLandEmail.src = "email_grey.png";
	frmTransferLanding.btnOnUsMobileSmS.src = "tran_notify_mobile_only.png";
	frmTransferLanding.lineNotifyRecipientButton.setVisibility(false);
	frmTransferLanding.lineMobileEmailTextBox.setVisibility(false);
	gblTrasSMS = 0;
	gblTransEmail = 0;
}

function holdPreviouslySelectedNotifyDetails(){
	if(!(gblTrasSMS == 1 && gblTransEmail == 1)){
		displayHbxNotifyRecipient(true);
		if(gblTrasSMS == 1){
    		frmTransferLanding.txtTransLndSms.setVisibility(true);
        	frmTransferLanding.txtTransLndSmsNEmail.setVisibility(false);
        	frmTransferLanding.hbxTranLandRecNote.setVisibility(true);
   			frmTransferLanding.hbxRecNoteEmail.setVisibility(false);
    		frmTransferLanding.lineRecipientNote.setVisibility(true);
		}else if(gblTransEmail == 1){
    		frmTransferLanding.hbxTranLandRecNote.setVisibility(false);
        	frmTransferLanding.hbxRecNoteEmail.setVisibility(true);
        	frmTransferLanding.txtTransLndSms.setVisibility(false);
   			frmTransferLanding.txtTransLndSmsNEmail.setVisibility(true);
   			frmTransferLanding.lineRecipientNote.setVisibility(true);
		}
	}
}



function transferAgainFromCalenderTransaction(){
	//MIB-1265
	gblfromCalender = true;
	showLoadingScreen();
	getTransferFromAccountsFromAccSmry(removeHyphenIB(frmTransfersAckCalendar.lblFromAcctNumHidden.text));
}

function transferAgainMobileTransaction(){
	showLoadingScreen();
	gblSelTransferMode = 2;
	setOnClickSetCanlenderBtn(true);
	displayOnUsEnterMobileNumber(true);
	displayP2PITMXBank(false);
	displayOnUsEnterBankAccount(false);	
	gblisTMB = frmTransfersAckCalendar.lblToBankCDHidden.text;
	frmTransferLanding.txtOnUsMobileNo.setVisibility(true);
	frmTransferLanding.txtOnUsMobileNo.text = frmTransfersAckCalendar.lblToAcctNumHidden.text;
	var amt = frmTransfersAckCalendar.lblHiddenTransferAmt.text;
	frmTransferLanding.txtTranLandAmt.text = commaFormatted(parseFloat(removeCommaIB(amt)).toFixed(2));
	checkDeviceContactsForNickName();
	frmTransferLanding.txtTranLandMyNote.text = frmTransfersAckCalendar.lblMyNote.text.trim();
	makeWidgetsBelowTransferFeeButtonVisible(true);	
	frmTransferLanding.txtTranLandRecNote.text = ""; 
	frmTransferLanding.imgOnUsMob.src = "tran_smartphone.png";
	frmTransferLanding.imgOnUsMob.margin = [0,1,0,0];
	frmTransferLanding.txtTranLandRecNote.placeholder =  kony.i18n.getLocalizedString("TREnter_PL_SNote"); 
}

function transferAgainCitizenTransaction(){
	showLoadingScreen();
	gblSelTransferMode = 3;
	setOnClickSetCanlenderBtn(true);
	displayOnUsEnterMobileNumber(false);
	displayOnUsEnterCitizenID(true);
	displayP2PITMXBank(false);
	displayOnUsEnterBankAccount(false);	
	gblisTMB = frmTransfersAckCalendar.lblToBankCDHidden.text;
	frmTransferLanding.txtCitizenID.setVisibility(true);
	frmTransferLanding.txtCitizenID.text = frmTransfersAckCalendar.lblToAcctNumHidden.text;
	var amt = frmTransfersAckCalendar.lblHiddenTransferAmt.text;
	frmTransferLanding.txtTranLandAmt.text = commaFormatted(parseFloat(removeCommaIB(amt)).toFixed(2));
	frmTransferLanding.txtTranLandMyNote.text = frmTransfersAckCalendar.lblMyNote.text.trim();
	makeWidgetsBelowTransferFeeButtonVisible(true);	
	frmTransferLanding.txtTranLandRecNote.text = ""; 
	frmTransferLanding.imgOnUsMob.src = "citizen_icon_id.png";
	frmTransferLanding.imgOnUsMob.margin = [0,3,0,0];
	frmTransferLanding.txtTranLandRecNote.placeholder =  kony.i18n.getLocalizedString("TREnter_PL_SNote"); 
}

function assignBankSelectDetails(bankCode){
	frmTransferLanding.hbxPleaseSelectBank.setVisibility(false);
	frmTransferLanding.hbxSelectBank.setVisibility(true);
	frmTransferLanding.lblBankFullName.text = getBankNameOfcurrentLocale(bankCode);//selectedData["lblBankName"];
	frmTransferLanding.lblBankShortName.text = getBankShortName(bankCode);
	MAX_ACC_LEN_LIST = getBankMaxLengths(gblisTMB);
	var  lenghtsList= MAX_ACC_LEN_LIST.split(",");
	var minimumLength = parseInt(lenghtsList[0]);
	var maxLengh =  parseInt(lenghtsList[lenghtsList.length-1])
	if(lenghtsList.length <2 && maxLengh == 10){
		frmTransferLanding.tbxAccountNumber.maxTextLength =  maxLengh+3;
	}else{
		frmTransferLanding.tbxAccountNumber.maxTextLength =  maxLengh;
	}	
	frmTransferLanding.imgBankLogo.src = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+bankCode+"&modIdentifier=BANKICON";
	//frmTransferLanding.lineBankSelect.skin = lineBlue;
}






function shareButtonCalendarOnClick(){
	if(frmTransfersAckCalendar.imgTransNBpAckComplete.src == "iconnotcomplete.png")
	{
		alert(kony.i18n.getLocalizedString("Err_ShareFailed"));
		return;
	}
	else
	{
		if(frmTransfersAckCalendar.hbxShareOption.isVisible){
			frmTransfersAckCalendar.hbxShareOption.isVisible = false;
			frmTransfersAckCalendar.imgHeaderMiddle.src = "arrowtop.png";
		}else{
			frmTransfersAckCalendar.hbxShareOption.isVisible = true;
			frmTransfersAckCalendar.imgHeaderMiddle.src = "empty.png";
		}	
	}	
}

function onclickGetRecipientsDreamSavings(){
		alert("" + kony.i18n.getLocalizedString("TRErr_DreamAccNotAllowSelect"))
}


function populateCompletedTransferDataOnTransferAgain() {
	gblisTMB = frmTransfersAckCalendar.lblToBankCDHidden.text;
	assignBankSelectDetails(gblisTMB);
	frmTransferLanding.tbxAccountNumber.text = frmTransfersAckCalendar.lblToAcctNumHidden.text;
	
	frmTransferLanding.txtTranLandAmt.text = parseFloat(removeCommaIB(frmTransfersAckCalendar.lblTransNPbAckTotVal.text)) + "";
	frmTransferLanding.txtTranLandMyNote.text = frmTransfersAckCalendar.lblMyNote.text.trim();
	//MIB-1682 Trim space and check no have mynote 
	if (frmTransferLanding.txtTranLandMyNote.text.length == 0){
		//frmTransferLanding.lineMyNote.skin = linePopupBlack;
	}else{
		//frmTransferLanding.lineMyNote.skin = lineBlue;
	}
	
	var smartFlag = frmTransfersAckCalendar.lblSmartFlag.text;
	if(gblisTMB == gblTMBBankCD){
	
	} else if(smartFlag == "Y"){
		gblTrasORFT = 0;
		gblTransSMART = 1;
		frmTransferLanding.btnTransLndORFT.skin = "btnFeeTop";
		frmTransferLanding.btnTransLndSmart.skin = "btnFeeBottomFoc";
		frmTransferLanding.btnTransLndSmart.setVisibility(true);
		frmTransferLanding.btnTransLndORFT.setVisibility(true);
		frmTransferLanding.hbxTransLndFee.setVisibility(true);
		frmTransferLanding.hbxFeeTransfer.setVisibility(false);
	} else {
		gblTrasORFT = 1;
		gblTransSMART = 0;
		frmTransferLanding.btnTransLndORFT.skin = "btnFeeTopFoc";
		frmTransferLanding.btnTransLndSmart.skin = "btnFeeBottom";
		frmTransferLanding.btnTransLndSmart.setVisibility(true);
		frmTransferLanding.btnTransLndORFT.setVisibility(true);
		frmTransferLanding.hbxTransLndFee.setVisibility(true);
		frmTransferLanding.hbxFeeTransfer.setVisibility(false);
	
	}
	if(getORFTFlag(gblisTMB) == "Y"){
		frmTransferLanding.hbxTransLndFee.setVisibility(true);
		frmTransferLanding.hbxFeeTransfer.setVisibility(false);
	}
	onDoneToAccountNumber(); //It will check the Bank & Account number already exist in recipient list or not 
	
}

function onEditAccNumTransferLanding(txt) {
	if (txt == null) return false;
	var numChars = txt.length;
	var temp = "";
	var i, txtLen = numChars;
	var currLen = numChars;
	if (gblPrevLen < currLen) {
		for (i = 0; i < numChars; ++i) {
			if (txt[i] != '-') {
				temp = temp + txt[i];
			} else {
				txtLen--;
			}
		}
		var iphenText = "";
		for (i = 0; i < txtLen; i++) {
			iphenText += temp[i];
			if (i == 2 || i == 3 || i == 8) {
				iphenText += '-';
			}
		}
			frmTransferLanding.tbxAccountNumber.text = iphenText;
	}
	
	gblPrevLen = currLen;
}


function getTransferFeeMBNextClick() {
		var inputParam = {}
		var amt = frmTransferLanding.txtTranLandAmt.text;
		amt = kony.string.replace(amt, ",", "")
 		var fromAcctSelIndx = gbltranFromSelIndex[1]
		var fromData = frmTransferLanding.segTransFrm.data;
		var prodCode = fromData[fromAcctSelIndx].prodCode;
		var remainingFee = fromData[fromAcctSelIndx].remainingFee;
		if(gblPaynow){
			inputParam["freeTransCnt"] = remainingFee;
		}else{
			inputParam["freeTransCnt"] = "0";
		}
 		showLoadingScreen();
		//byPasscall(); //local environment function
		inputParam["Amount"] = amt
		inputParam["prodCode"] = prodCode;		
		invokeServiceSecureAsync("getTransferFee", inputParam, callBackgetTransferFeeMBNextClick)

}



function callBackgetTransferFeeMBNextClick(status, resulttable) {
 	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			var orftFee = resulttable["orftFee"];
			var smartFee = resulttable["smartFee"];
			var cutoffInd = resulttable["cutoffInd"];
			var currentDate = resulttable["orftFeeDate"];
			smartDate = resulttable["smartFeeDate"];
			// changed/added below code to fix DEF1068
			var smartDateNew = resulttable["smartFeeDateNew"];
			var smartfuturetime = resulttable["smartFutureNew"];
			var orftFutureTime = resulttable["orftFuture"]; 
			orftFee = commaFormatted(orftFee);
			smartFee = commaFormatted(smartFee);
			//ENH_129 SMART Transfer Add Date & Time
		
			var orftFlag = getORFTFlag(gblisTMB);
			var smartFlag = getSMARTFlag(gblisTMB);
			
			if (orftFlag == "N") {
				frmTransferLanding.btnTransLndORFT.setVisibility(false);
			} else {
				frmTransferLanding.btnTransLndORFT.setEnabled(true);
				frmTransferLanding.btnTransLndORFT.setVisibility(true)
			}
			
			if (smartFlag == "N") {
				frmTransferLanding.btnTransLndSmart.setVisibility(false);
			} else {
				if (orftFlag == "N") {
					frmTransferLanding.btnTransLndORFT.setEnabled(false);
					frmTransferLanding.btnTransLndSmart.setEnabled(false);
					frmTransferLanding.hbxTransLndFee.setVisibility(false);
					frmTransferLanding.hbxFeeTransfer.setVisibility(true);
					makeWidgetsBelowTransferFeeButtonVisible(true);
				
					displayHbxNotifyRecipient(true);
					if(frmTransferLanding.txtTransLndSmsNEmail.isVisible || frmTransferLanding.txtTransLndSms.isVisible){
						frmTransferLanding.lineNotifyRecipientButton.setVisibility(true);
					}else{
						frmTransferLanding.lineNotifyRecipientButton.setVisibility(false);
					}
					
					frmTransferLanding.lblITMXFee.text = smartFee;
					frmTransferLanding.lblFeeTransfer.text = kony.i18n.getLocalizedString("TREnter_SMART");
					frmTransferLanding.lblFeeTransfer.skin = "lblGrey36px";
					displayP2PITMXFee(true, enteredAmount);
					
					var smartDateTime = smartDate.split(" ");
			    	if(smartDateTime.length > 1){
			    		frmTransferLanding.lblRecievedBy.text = smartDateTime[0];
						frmTransferLanding.lblRecievedByValue.text = smartDateTime[1];
			    	}
			    	
					frmTransferLanding.lineRecipientNote.setVisibility(true);
					frmTransferLanding.btnTransLndSmart.setVisibility(true);
					frmTransferLanding.btnTransLndORFT.setVisibility(true);
					frmTransferLanding.btnTransLndORFT.skin = "btnFeeTop";
					frmTransferLanding.btnTransLndSmart.skin = "btnFeeBottom";
					gblTrasORFT = 0;
					gblTransSMART = 1;
				}else{
					frmTransferLanding.btnTransLndSmart.setEnabled(true);
					frmTransferLanding.btnTransLndSmart.setVisibility(true);
					if(!frmTransferLanding.hbxTransLndFee.isVisible){//reset if smart only selected already and change to orft allowed bank
						gblTrasORFT = 0;
						gblTransSMART = 0;
						frmTransferLanding.btnTransLndORFT.skin = "btnFeeTop";
						frmTransferLanding.btnTransLndSmart.skin = "btnFeeBottom";
						frmTransferLanding.lblRecievedBy.text = "-";
						frmTransferLanding.lblRecievedByValue.text = "-";
					}else{
							if(gblTransSMART == 1 && !frmTransferLanding.hbxTranLandShec.isVisible){
								gblTransSMART =0;
								gblTrasORFT = 0;
								ehFrmTransferLanding_btnTransLndSmart_onClick();
							}else if(gblTrasORFT == 1 && !frmTransferLanding.hbxTranLandShec.isVisible){
								gblTrasORFT = 0;
								gblTransSMART = 0;
								ehFrmTransferLanding_btnTransLndORFT_onClick();
							}
					}
					frmTransferLanding.hbxTransLndFee.setVisibility(true);
					frmTransferLanding.hbxFeeTransfer.setVisibility(false);
				}
			}
			if (smartFlag == "N" && orftFlag == "N" && gblisTMB != gblTMBBankCD) {
				alert("selected To account is not eligible for transfers");
				frmTransferLanding.btnTranLandNext.setEnabled(false);
			} else{
				frmTransferLanding.btnTranLandNext.setEnabled(true);
			}
			
			frmTransferLanding.btnTransLndSmart.text = smartFee + " \n" + kony.i18n.getLocalizedString("TREnter_SMART");
			frmTransferLanding.btnTransLndORFT.text = orftFee + " \n" + kony.i18n.getLocalizedString("TREnter_ORFT");
		 	
			var currentDate = currentDate.toString();
			var dateTD = currentDate.substr(0, 2);
			var yearTD = currentDate.substr(6, 4);
			var monthTD = currentDate.substr(3, 2);
			
			var completeDate = yearTD + "-" + monthTD + "-" + dateTD;
			gblTransferDate = completeDate
			dismissLoadingScreen();
		} else {
			dismissLoadingScreen();

		}
		if (gblisTMB != gblTMBBankCD && gblTrasORFT == 0 && gblTransSMART == 0) {
			showAlert(kony.i18n.getLocalizedString("keyPleaseSelectTransferType"), kony.i18n.getLocalizedString("info"));
			setEnabledTransferLandingPage(true);
			return ;
		}
		afterTransferFeeContinueNext();
	}else{
		setEnabledTransferLandingPage(true);
	}
}

function onDuplicateTransferErrPopUp() {
	isFromEdit=true;
	frmTransferLanding.show();
	popGeneralMsg.dismiss();
}

function onClickCancelBtnTransferLandingMB(){
	gblfromCalender =false;
	transferFromMenu();
}

function onClickVbxEnterAmountFocus(){
	frmTransferLanding.txtTranLandAmt.setFocus(true);
}

function enableTransferTextBoxAmount(enabled){
	if(enabled){
		frmTransferLanding.txtTranLandAmt.setEnabled(true);
		frmTransferLanding.vbxTransferAmt.setEnabled(true);
		frmTransferLanding.txtTranLandAmt.skin = "txtBlueBold240";
		frmTransferLanding.txtTranLandAmt.focusSkin = "txtBlueBold240";
	}else{
		frmTransferLanding.txtTranLandAmt.setEnabled(false);
		frmTransferLanding.vbxTransferAmt.setEnabled(false);
		frmTransferLanding.txtTranLandAmt.skin = "txtGreyBold240";
		frmTransferLanding.txtTranLandAmt.focusSkin = "txtGreyBold240";
	}
}

function setEnabledTransferLandingPage(isEnabled){
	frmTransferLanding.vbox4751247744173.setEnabled(isEnabled);
}

function onBeginEditAccountNumber(){
	var accountNumber = frmTransferLanding.tbxAccountNumber.text.length;
	var maxTextLength = parseInt(frmTransferLanding.tbxAccountNumber.maxTextLength);
	if(isNotBlank(accountNumber)){
		if(accountNumber < maxTextLength){
			clearNotifyRecipientfields();
		}
	}
}

function recheckEnteredAcctInRecipientListForNotify() {
	var enteredAccountNumber = kony.string.replace(frmTransferLanding.tbxAccountNumber.text,"-","");
	for(var i=0;i < gblAccountListTmp.length;i++){
		var accounts = gblAccountListTmp[i]["Accounts"+i];
		for(var j=0;j < accounts.length ; j++){
			if((accounts[j].acctId == "0000" + enteredAccountNumber || accounts[j].acctId == enteredAccountNumber) && accounts[j].bankCde == gblisTMB){
				if(frmTransferLanding.hbxTranLandNotifyRec.isVisible){
					if(frmTransferLanding.btnTranLandSms.src == "mobile_blue.png"
						|| frmTransferLanding.btnTranLandEmail.src == "email_blue.png"){
						// data has been selected before change recipient name.
					}else{
						gblTrasSMS = 0;
						gblTransEmail = 0;
					}
				}
				break;
			}
		}
	}
			
	for(var i=0;i < gblOwnAccountsListTmp.length;i++){
		var bankCode = gblOwnAccountsListTmp[i].bankCde;
		if(!isNotBlank(bankCode)){
			bankCode = gblTMBBankCD; //TMB Default bank code = 11
		}
	
		if((gblOwnAccountsListTmp[i].acctId == "0000" + enteredAccountNumber || gblOwnAccountsListTmp[i].acctId == enteredAccountNumber) && bankCode == gblisTMB){
			gblTrasSMS = 1;
			gblTransEmail = 1;
			break;
		}
	}
		
	for(var i=0;i < gblDeletedOwnAccountsTmp.length;i++){
		if((gblDeletedOwnAccountsTmp[i].acctId == "0000" + enteredAccountNumber || gblDeletedOwnAccountsTmp[i].acctId == enteredAccountNumber) && gblTMBBankCD == gblisTMB){
			gblTrasSMS = 1;
			gblTransEmail = 1;
			break;
		}
	}
}