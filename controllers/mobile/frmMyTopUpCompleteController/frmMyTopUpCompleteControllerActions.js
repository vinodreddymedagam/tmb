define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnHdrMenu **/
    AS_Button_ea5f7e8d221042658bf24ca7bcc723e6: function AS_Button_ea5f7e8d221042658bf24ca7bcc723e6(eventobject) {
        var self = this;
        return handleMenuBtn.call(this);
    },
    /** onClick defined for hbxAdv **/
    AS_HBox_af7ee83c79d24e278a32b16c21875929: function AS_HBox_af7ee83c79d24e278a32b16c21875929(eventobject) {
        var self = this;
        return getCampaignResult.call(this);
    },
    /** onClick defined for btnCancelSpa **/
    AS_Button_j579ebe339b048f8b2cce96a5acbc538: function AS_Button_j579ebe339b048f8b2cce96a5acbc538(eventobject) {
        var self = this;
        if ((isMenuShown == false)) {
            frmMyTopUpList.show();
            TMBUtil.DestroyForm(frmMyTopUpComplete);
        } else {
            frmMyTopUpComplete.scrollboxMain.scrollToEnd();
            isMenuShown = false;
        }
    },
    /** onClick defined for btnAgreeSpa **/
    AS_Button_de8647a4d8574a7eadb397a782d60ba4: function AS_Button_de8647a4d8574a7eadb397a782d60ba4(eventobject) {
        var self = this;

        function SHOW_ALERT_ide_onClick_jcfefe968ae64cdf88647458cbf5aa94_True() {}
        TMBUtil.DestroyForm(frmAddTopUpBillerconfrmtn);
        if (addAnotherValidValue < kony.os.toNumber(GLOBAL_MAX_BILL_COUNT)) kony.print("addAnotherValidValue TRUE ------" + addAnotherValidValue);
        else kony.print("addAnotherValidValue FALSE------" + addAnotherValidValue);
        if ((addAnotherValidValue < kony.os.toNumber(GLOBAL_MAX_BILL_COUNT))) {
            frmAddTopUpToMB.imgAddedBiller.src = "";
            frmAddTopUpToMB.lblAddbillerName.text = "";
            frmAddTopUpToMB.lblAddedRef1.text = kony.i18n.getLocalizedString("keyRef1");
            frmAddTopUpToMB.lblAddedRef2.text = kony.i18n.getLocalizedString("keyRef2");
            frmAddTopUpToMB.show();
        } else {
            function SHOW_ALERT_ide_onClick_jcfefe968ae64cdf88647458cbf5aa94_Callback() {
                SHOW_ALERT_ide_onClick_jcfefe968ae64cdf88647458cbf5aa94_True();
            }
            kony.ui.Alert({
                "alertType": constants.ALERT_TYPE_ERROR,
                "alertTitle": "",
                "yesLabel": "Ok",
                "noLabel": "No",
                "message": kony.i18n.getLocalizedString("Valid_MoreThan50"),
                "alertHandler": SHOW_ALERT_ide_onClick_jcfefe968ae64cdf88647458cbf5aa94_Callback
            }, {
                "iconPosition": constants.ALERT_ICON_POSITION_LEFT
            });
        }
    },
    /** onClick defined for btnCancel **/
    AS_Button_fe93f97d94b747bb9fba86e93b6fd9a4: function AS_Button_fe93f97d94b747bb9fba86e93b6fd9a4(eventobject) {
        var self = this;
        if ((isMenuShown == false)) {
            frmMyTopUpList.show();
            TMBUtil.DestroyForm(frmMyTopUpComplete);
        } else {
            frmMyTopUpComplete.scrollboxMain.scrollToEnd();
            isMenuShown = false;
        }
    },
    /** onClick defined for btnAgree **/
    AS_Button_hbefe18ff4d9433da99dda72c0d69cc6: function AS_Button_hbefe18ff4d9433da99dda72c0d69cc6(eventobject) {
        var self = this;

        function SHOW_ALERT_ide_onClick_fe389cb3ca5c4989a2d61b7c0c5017a2_True() {}
        TMBUtil.DestroyForm(frmAddTopUpBillerconfrmtn);
        if (addAnotherValidValue < kony.os.toNumber(GLOBAL_MAX_BILL_COUNT)) kony.print("addAnotherValidValue TRUE ------" + addAnotherValidValue);
        else kony.print("addAnotherValidValue FALSE------" + addAnotherValidValue);
        if ((addAnotherValidValue < kony.os.toNumber(GLOBAL_MAX_BILL_COUNT))) {
            frmAddTopUpToMB.imgAddedBiller.src = "";
            frmAddTopUpToMB.lblAddbillerName.text = "";
            frmAddTopUpToMB.lblAddedRef1.text = kony.i18n.getLocalizedString("keyRef1");
            frmAddTopUpToMB.lblAddedRef2.text = kony.i18n.getLocalizedString("keyRef2");
            frmAddTopUpToMB.show();
        } else {
            function SHOW_ALERT_ide_onClick_fe389cb3ca5c4989a2d61b7c0c5017a2_Callback() {
                SHOW_ALERT_ide_onClick_fe389cb3ca5c4989a2d61b7c0c5017a2_True();
            }
            kony.ui.Alert({
                "alertType": constants.ALERT_TYPE_ERROR,
                "alertTitle": "",
                "yesLabel": "Ok",
                "noLabel": "No",
                "message": kony.i18n.getLocalizedString("Valid_MoreThan50"),
                "alertHandler": SHOW_ALERT_ide_onClick_fe389cb3ca5c4989a2d61b7c0c5017a2_Callback
            }, {
                "iconPosition": constants.ALERT_ICON_POSITION_LEFT
            });
        }
    },
    /** onClick defined for vbox4751247744173 **/
    AS_HBox_bbd5996108da49229199205bddeca37d: function AS_HBox_bbd5996108da49229199205bddeca37d(eventobject) {
        var self = this;
        return onClickForInnerBoxes.call(this);
    },
    /** preShow defined for frmMyTopUpComplete **/
    AS_Form_hdec96aafa4141338a4dab7917423682: function AS_Form_hdec96aafa4141338a4dab7917423682(eventobject, neworientation) {
        var self = this;
        return frmMyTopUpCompleteMenuPreshow.call(this);
    },
    /** postShow defined for frmMyTopUpComplete **/
    AS_Form_jd274eb0dc9e427aa517373b39e3f1fd: function AS_Form_jd274eb0dc9e427aa517373b39e3f1fd(eventobject, neworientation) {
        var self = this;
    },
    /** onDeviceBack defined for frmMyTopUpComplete **/
    AS_Form_e4c9d8f547c64950b6131fc5539a9b51: function AS_Form_e4c9d8f547c64950b6131fc5539a9b51(eventobject, neworientation) {
        var self = this;
    }
});