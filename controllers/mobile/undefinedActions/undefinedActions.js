define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnBack **/
    AS_Button_i19aba09ea3540778ec9a333cb8d979f: function frmUVTransReqHistoryDetails_back_onClick(eventobject) {
        return AS_Button_i19aba09ea3540778ec9a333cb8d979f(eventobject);
    }

    function AS_Button_i19aba09ea3540778ec9a333cb8d979f(eventobject) {
        var self = this;
        return onClickBackfrmUVTransReqHistoryDetails.call(this);
    },
    /** onDeviceBack defined for frmUVTransReqHistoryDetails **/
    AS_Form_bc8c8950206b4dc1b62f86daccc215a9: function frmUVTransReqHistoryDetails_onDeviceBack(eventobject) {
        return AS_Form_bc8c8950206b4dc1b62f86daccc215a9(eventobject);
    }

    function AS_Form_bc8c8950206b4dc1b62f86daccc215a9(eventobject) {
        var self = this;
        return disableBackButton.call(this);
    }
});