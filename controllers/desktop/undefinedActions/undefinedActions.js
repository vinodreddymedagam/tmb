define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for LinkSutability **/
    AS_Link_adaf0112a0a04ce4b413a454b44d3842: function onClickSuitabilityIBMenuEvent(eventobject) {
        return AS_Link_adaf0112a0a04ce4b413a454b44d3842(eventobject);
    }

    function AS_Link_adaf0112a0a04ce4b413a454b44d3842(eventobject) {
        var self = this;
        gblOrderFlow = false;
        getMFSuitibilityReviewDetailsIB();
    },
    /** onClick defined for LinkRedeem **/
    AS_Link_e38abafd2d7041969d493b9494a3b530: function onClickRedeemIBMenuEvent(eventobject) {
        return AS_Link_e38abafd2d7041969d493b9494a3b530(eventobject);
    }

    function AS_Link_e38abafd2d7041969d493b9494a3b530(eventobject) {
        var self = this;
        onClickIBRedeem();
    },
    /** onClick defined for LinkPurchase **/
    AS_Link_j17b87f332974e5d9171d69eecb865f8: function onClickPurchaseIBMenuEvent(eventobject) {
        return AS_Link_j17b87f332974e5d9171d69eecb865f8(eventobject);
    }

    function AS_Link_j17b87f332974e5d9171d69eecb865f8(eventobject) {
        var self = this;
        onClickIBPurchase();
    },
    /** onClick defined for btnMFFundPortflio **/
    AS_Button_e3b6508ebf204fdd89b25ddabe8b5213: function onclick_btnMFFundPortfolio_(eventobject) {
        return AS_Button_e3b6508ebf204fdd89b25ddabe8b5213(eventobject);
    }

    function AS_Button_e3b6508ebf204fdd89b25ddabe8b5213(eventobject) {
        var self = this;
        gblMFPortfolioType = "PT";
        frmIBMutualFundsPortfolio.btnMFFundPortflio.skin = "skinBtnBlueLeftRoundCorner";
        frmIBMutualFundsPortfolio.btnMFAutoSmartPortfolio.skin = "skinBtnWhiteRightRoundCorner";
        callMutualFundsSummary();
    },
    /** onClick defined for btnMFAutoSmartPortfolio **/
    AS_Button_cfc280f303504a7b81f82946e25a885f: function onClick_MFAutoSmartPortfolio_(eventobject) {
        return AS_Button_cfc280f303504a7b81f82946e25a885f(eventobject);
    }

    function AS_Button_cfc280f303504a7b81f82946e25a885f(eventobject) {
        var self = this;
        gblMFPortfolioType = "AP";
        frmIBMutualFundsPortfolio.btnMFFundPortflio.skin = "skinBtnWhiteLeftRoundCorner";
        frmIBMutualFundsPortfolio.btnMFAutoSmartPortfolio.skin = "skinBtnBlueRightRoundCorner";
        callMutualFundsSummary();
    },
    /** onSelection defined for cmbFilterby **/
    AS_ComboBox_h65c1b95f97d4be5830180b601c4c5d9: function frmIBMutualFundsPortfolio_onSelectionUnitHolder(eventobject) {
        return AS_ComboBox_h65c1b95f97d4be5830180b601c4c5d9(eventobject);
    }

    function AS_ComboBox_h65c1b95f97d4be5830180b601c4c5d9(eventobject) {
        var self = this;
        return setFilterUnitHolderNoComboboxSelectionChanged.call(this);
    },
    /** onClick defined for hbxFilterUnitholder **/
    AS_HBox_eaf1649931fa411fb886714191e1e197: function frmIBMutualFundsPortfolio_onSelectUnitHolderNo(eventobject) {
        return AS_HBox_eaf1649931fa411fb886714191e1e197(eventobject);
    }

    function AS_HBox_eaf1649931fa411fb886714191e1e197(eventobject) {
        var self = this;
    },
    /** onRowClick defined for segAccountDetails **/
    AS_Segment_ba02f5bd287345e9b624591297a200fd: function frmIBMutualFundsPortfolio_onSelectSegFund(eventobject, sectionNumber, rowNumber) {
        return AS_Segment_ba02f5bd287345e9b624591297a200fd(eventobject, sectionNumber, rowNumber);
    }

    function AS_Segment_ba02f5bd287345e9b624591297a200fd(eventobject, sectionNumber, rowNumber) {
        var self = this;
        return onClickProfolioSegment.call(this, null, frmIBMutualFundsPortfolio.segAccountDetails.selectedItems[0].lblunitHolderNumber, frmIBMutualFundsPortfolio.segAccountDetails.selectedItems[0].fundCode);
    },
    /** onClick defined for button474201253135915 **/
    AS_Button_ca48e571b441459888017d78cb377d7b: function btn_onClick_bulkmessage(eventobject) {
        return AS_Button_ca48e571b441459888017d78cb377d7b(eventobject);
    }

    function AS_Button_ca48e571b441459888017d78cb377d7b(eventobject) {
        var self = this;
        callBulkMessage();
        //frmIBEnterCrmidforS2S.show()
    },
    /** onClick defined for vbox474969373109364 **/
    AS_VBox_d8e3ac7084774856ac62fd108608a8fc: function AS_VBox_onclickVbox(eventobject) {
        return AS_VBox_d8e3ac7084774856ac62fd108608a8fc(eventobject);
    }

    function AS_VBox_d8e3ac7084774856ac62fd108608a8fc(eventobject) {
        var self = this;
        return onClickForInnerBoxes.call(this);
    },
    /** onClick defined for btnGotit **/
    AS_Button_c60bad179281426da0c70561b255a42a: function btn_I_Got_It_Action(eventobject) {
        return AS_Button_c60bad179281426da0c70561b255a42a(eventobject);
    }

    function AS_Button_c60bad179281426da0c70561b255a42a(eventobject) {
        var self = this;
        gblMFPortfolioType = "PT";
        gblUnitHolderNoSelected = "";
        frmIBMutualFundsPortfolio.btnMFFundPortflio.skin = "skinBtnBlueLeftRoundCorner";
        frmIBMutualFundsPortfolio.btnMFAutoSmartPortfolio.skin = "skinBtnWhiteRightRoundCorner";
        frmIBMutualFundsPortfolio.hBoxAutosmartPortfolio.setVisibility(false);
        frmIBMutualFundsPortfolio.hBoxNormalPortfolio.setVisibility(true);
        callMutualFundsSummary();
    },
    /** onClick defined for lblOrdToBProcess **/
    AS_Link_g9412f7484fb44ed96a8172c79a370fb: function AS_Link_g9412f7484fb44ed96a8172c79a370fb(eventobject) {
        var self = this;
        initialDateSlider.call(this);
        viewMFFullStatementIB.call(this, viewType = "O");
    },
    /** onClick defined for lnkFullStatement **/
    AS_Link_fd629ce1361b4bca86dfed82da49df63: function AS_Link_fd629ce1361b4bca86dfed82da49df63(eventobject) {
        var self = this;
        initialDateSlider.call(this);
        viewMFFullStatementIB.call(this, viewType = "F");
    },
    /** onClick defined for lnkTaxDoc **/
    AS_Link_bdeec8e4dd714ec39fc12b0faf473acc: function AS_Link_bdeec8e4dd714ec39fc12b0faf473acc(eventobject) {
        var self = this;
        return generateTaxDocPdf.call(this);
    },
    /** onClick defined for btnRedeem **/
    AS_Button_dc402f5293e942be8a42829a74d0fdf2: function AS_Button_dc402f5293e942be8a42829a74d0fdf2(eventobject) {
        var self = this;
        if (gblIsFundCodeNull) { // mki, MIB- 10003 = start
            showAlert(kony.i18n.getLocalizedString("MF_P_ERR_01001"), kony.i18n.getLocalizedString("info"));
        } else {
            onClickRedeemIB();
        } // mki, MIB- 10003 = End
    },
    /** onClick defined for btnPurchase **/
    AS_Button_e989ef8940ef4013b37c3c86f1e39a48: function AS_Button_e989ef8940ef4013b37c3c86f1e39a48(eventobject) {
        var self = this;
        if (gblIsFundCodeNull) { // mki, MIB- 10003 = start
            showAlert(kony.i18n.getLocalizedString("MF_P_ERR_01001"), kony.i18n.getLocalizedString("info"));
        } else {
            onClickPurchaseIB();
        } // mki, MIB- 10003 = End
    },
    /** preShow defined for frmIBMutualFundsPortfolio **/
    AS_Form_i36cb3d1e1b04d53b0f14b191fdf8ba7: function frmIBMutualFundsPortfolio_preshowevent(eventobject) {
        return AS_Form_i36cb3d1e1b04d53b0f14b191fdf8ba7(eventobject);
    }

    function AS_Form_i36cb3d1e1b04d53b0f14b191fdf8ba7(eventobject) {
        var self = this;
        return frmIBMutualFundsPortfolioPreShow.call(this);
    },
    /** postShow defined for frmIBMutualFundsPortfolio **/
    AS_Form_df51f66965d847af9dc7cee7e1075b1d: function AS_Form_df51f66965d847af9dc7cee7e1075b1d(eventobject) {
        var self = this;
        addIBMenu.call(this);
        setFocusOnBtnAccountSum.call(this, kony.application.getCurrentForm());
        deleteTagforChartIBMutualFund.call(this);
    }
});