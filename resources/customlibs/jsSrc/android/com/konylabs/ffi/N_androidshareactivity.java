package com.konylabs.ffi;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;
import com.konylabs.api.TableLib;
import com.konylabs.vm.LuaTable;



import com.kony.share.ShareOnAndroid;
import com.konylabs.libintf.Library;
import com.konylabs.libintf.JSLibrary;
import com.konylabs.vm.LuaError;
import com.konylabs.vm.LuaNil;


public class N_androidshareactivity extends JSLibrary {

 
	String[] methods = { };


 Library libs[] = null;
 public Library[] getClasses() {
 libs = new Library[1];
 libs[0] = new ShareOnAndroid();
 return libs;
 }



	public N_androidshareactivity(){
	}

	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		default:
			break;
		}
 
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "androidshareactivity";
	}


	/*
	 * return should be status(0 and !0),address
	 */
 


class ShareOnAndroid extends JSLibrary {

 
 
	public static final String onJSClickActivity = "onJSClickActivity";
 
 
	public static final String onJSClickActivityfbmsngr = "onJSClickActivityfbmsngr";
 
 
	public static final String onJSClickActivityline = "onJSClickActivityline";
 
 
	public static final String onJSClickActivityScreenShot = "onJSClickActivityScreenShot";
 
	String[] methods = { onJSClickActivity, onJSClickActivityfbmsngr, onJSClickActivityline, onJSClickActivityScreenShot };

	public Object createInstance(final Object[] params) {
 return new com.kony.share.ShareOnAndroid(
 (java.lang.String)params[0] , (java.lang.String)params[1] , (java.lang.String)params[2] , (java.lang.String)params[3] , (java.lang.String)params[4] , (java.lang.String)params[5] );
 }


	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		case 0:
 if (paramLen < 0 || paramLen > 1){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 ret = this.onJSClickActivity(params[0]
 );
 
 			break;
 		case 1:
 if (paramLen < 0 || paramLen > 1){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 ret = this.onJSClickActivityfbmsngr(params[0]
 );
 
 			break;
 		case 2:
 if (paramLen < 0 || paramLen > 1){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 ret = this.onJSClickActivityline(params[0]
 );
 
 			break;
 		case 3:
 if (paramLen < 0 || paramLen > 1){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 ret = this.onJSClickActivityScreenShot(params[0]
 );
 
 			break;
 		default:
			break;
		}
 
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "ShareOnAndroid";
	}

	/*
	 * return should be status(0 and !0),address
	 */
 
 
 	public final Object[] onJSClickActivity( Object self ){
 
		Object[] ret = null;
 ((com.kony.share.ShareOnAndroid)self).onJSClickActivity( );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] onJSClickActivityfbmsngr( Object self ){
 
		Object[] ret = null;
 ((com.kony.share.ShareOnAndroid)self).onJSClickActivityfbmsngr( );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] onJSClickActivityline( Object self ){
 
		Object[] ret = null;
 ((com.kony.share.ShareOnAndroid)self).onJSClickActivityline( );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] onJSClickActivityScreenShot( Object self ){
 
		Object[] ret = null;
 ((com.kony.share.ShareOnAndroid)self).onJSClickActivityScreenShot( );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
}

};
