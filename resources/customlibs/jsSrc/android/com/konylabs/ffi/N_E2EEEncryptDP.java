package com.konylabs.ffi;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;
import com.konylabs.api.TableLib;
import com.konylabs.vm.LuaTable;



import com.kony.tmbbank.e2eeDP.E2EEClientDP;
import com.konylabs.libintf.Library;
import com.konylabs.libintf.JSLibrary;
import com.konylabs.vm.LuaError;
import com.konylabs.vm.LuaNil;


public class N_E2EEEncryptDP extends JSLibrary {

 
 
	public static final String encryptDP = "encryptDP";
 
	String[] methods = { encryptDP };


 Library libs[] = null;
 public Library[] getClasses() {
 libs = new Library[0];
 return libs;
 }



	public N_E2EEEncryptDP(){
	}

	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		case 0:
 if (paramLen != 4){ return new Object[] {new Double(100),"Invalid Params"}; }
 java.lang.String hashAlgo0 = null;
 if(params[0] != null && params[0] != LuaNil.nil) {
 hashAlgo0 = (java.lang.String)params[0];
 }
 java.lang.String plaintText0 = null;
 if(params[1] != null && params[1] != LuaNil.nil) {
 plaintText0 = (java.lang.String)params[1];
 }
 java.lang.String pk0 = null;
 if(params[2] != null && params[2] != LuaNil.nil) {
 pk0 = (java.lang.String)params[2];
 }
 java.lang.String randomNumber0 = null;
 if(params[3] != null && params[3] != LuaNil.nil) {
 randomNumber0 = (java.lang.String)params[3];
 }
 ret = this.encryptDP( hashAlgo0, plaintText0, pk0, randomNumber0 );
 
 			break;
 		default:
			break;
		}
 
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "E2EEEncryptDP";
	}


	/*
	 * return should be status(0 and !0),address
	 */
 
 
 	public final Object[] encryptDP( java.lang.String inputKey0, java.lang.String inputKey1, java.lang.String inputKey2, java.lang.String inputKey3 ){
 
		Object[] ret = null;
 java.util.Vector retval = com.kony.tmbbank.e2eeDP.E2EEClientDP.encryptDP( inputKey0
 , inputKey1
 , inputKey2
 , inputKey3
 );
 LuaTable val = TableLib.convertToLuaTable(retval);
 
 			ret = new Object[]{val, new Double(0)};
 		return ret;
	}
 
};
