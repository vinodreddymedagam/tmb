package com.konylabs.ffi;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;
import com.konylabs.api.TableLib;
import com.konylabs.vm.LuaTable;



import tmb.kone.com.epid.ScanPassport;
import tmb.kone.com.epid.VerifyScreenActivity;
import com.konylabs.libintf.Library;
import com.konylabs.libintf.JSLibrary;
import com.konylabs.vm.LuaError;
import com.konylabs.vm.LuaNil;


public class N_scanEKYC extends JSLibrary {

 
 
	public static final String scanPassport = "scanPassport";
 
 
	public static final String verifyFace = "verifyFace";
 
	String[] methods = { scanPassport, verifyFace };


 Library libs[] = null;
 public Library[] getClasses() {
 libs = new Library[0];
 return libs;
 }



	public N_scanEKYC(){
	}

	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		case 0:
 if (paramLen != 7){ return new Object[] {new Double(100),"Invalid Params"}; }
 com.konylabs.vm.Function callback0 = null;
 if(params[0] != null && params[0] != LuaNil.nil) {
 callback0 = (com.konylabs.vm.Function)params[0];
 }
 Boolean isPassportScan0 = null;
 if(params[1] != null && params[1] != LuaNil.nil) {
 isPassportScan0 = (Boolean)params[1];
 }
 Boolean isNFCScan0 = null;
 if(params[2] != null && params[2] != LuaNil.nil) {
 isNFCScan0 = (Boolean)params[2];
 }
 Boolean isSimulateDG130 = null;
 if(params[3] != null && params[3] != LuaNil.nil) {
 isSimulateDG130 = (Boolean)params[3];
 }
 Double Timer0 = null;
 if(params[4] != null && params[4] != LuaNil.nil) {
 Timer0 = (Double)params[4];
 }
 Double NFCTimer0 = null;
 if(params[5] != null && params[5] != LuaNil.nil) {
 NFCTimer0 = (Double)params[5];
 }
 Boolean isScanOCR0 = null;
 if(params[6] != null && params[6] != LuaNil.nil) {
 isScanOCR0 = (Boolean)params[6];
 }
 ret = this.scanPassport( callback0, isPassportScan0, isNFCScan0, isSimulateDG130, Timer0, NFCTimer0, isScanOCR0 );
 
 			break;
 		case 1:
 if (paramLen != 3){ return new Object[] {new Double(100),"Invalid Params"}; }
 java.lang.String selifeDataParams1 = null;
 if(params[0] != null && params[0] != LuaNil.nil) {
 selifeDataParams1 = (java.lang.String)params[0];
 }
 com.konylabs.vm.Function callback1 = null;
 if(params[1] != null && params[1] != LuaNil.nil) {
 callback1 = (com.konylabs.vm.Function)params[1];
 }
 java.lang.String imageBase641 = null;
 if(params[2] != null && params[2] != LuaNil.nil) {
 imageBase641 = (java.lang.String)params[2];
 }
 ret = this.verifyFace( selifeDataParams1, callback1, imageBase641 );
 
 			break;
 		default:
			break;
		}
 
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "scanEKYC";
	}


	/*
	 * return should be status(0 and !0),address
	 */
 
 
 	public final Object[] scanPassport( com.konylabs.vm.Function inputKey0, Boolean inputKey1, Boolean inputKey2, Boolean inputKey3, Double inputKey4, Double inputKey5, Boolean inputKey6 ){
 
		Object[] ret = null;
 tmb.kone.com.epid.ScanPassport.startCameraActivity( (com.konylabs.vm.Function)inputKey0
 , inputKey1.booleanValue() , inputKey2.booleanValue() , inputKey3.booleanValue() , inputKey4.intValue() , inputKey5.intValue() , inputKey6.booleanValue() );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] verifyFace( java.lang.String inputKey0, com.konylabs.vm.Function inputKey1, java.lang.String inputKey2 ){
 
		Object[] ret = null;
 tmb.kone.com.epid.VerifyScreenActivity.verification( inputKey0
 , (com.konylabs.vm.Function)inputKey1
 , inputKey2
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
};
