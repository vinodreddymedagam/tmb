package com.konylabs.ffi;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;
import com.konylabs.api.TableLib;
import com.konylabs.vm.LuaTable;



import com.kony.tmbbank.util.ImageCompressor;
import com.konylabs.libintf.Library;
import com.konylabs.libintf.JSLibrary;
import com.konylabs.vm.LuaError;
import com.konylabs.vm.LuaNil;


public class N_ImageResizer extends JSLibrary {

 
 
	public static final String reduceImageSize = "reduceImageSize";
 
	String[] methods = { reduceImageSize };


 Library libs[] = null;
 public Library[] getClasses() {
 libs = new Library[0];
 return libs;
 }



	public N_ImageResizer(){
	}

	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		case 0:
 if (paramLen != 1){ return new Object[] {new Double(100),"Invalid Params"}; }
 java.lang.String encodedImageString0 = null;
 if(params[0] != null && params[0] != LuaNil.nil) {
 encodedImageString0 = (java.lang.String)params[0];
 }
 ret = this.reduceImageSize( encodedImageString0 );
 
 			break;
 		default:
			break;
		}
 
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "ImageResizer";
	}


	/*
	 * return should be status(0 and !0),address
	 */
 
 
 	public final Object[] reduceImageSize( java.lang.String inputKey0 ){
 
		Object[] ret = null;
 java.lang.String val = com.kony.tmbbank.util.ImageCompressor.compressImage( inputKey0
 );
 
 			ret = new Object[]{val, new Double(0)};
 		return ret;
	}
 
};
