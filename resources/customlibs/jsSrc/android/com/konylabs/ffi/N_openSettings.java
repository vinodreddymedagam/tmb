package com.konylabs.ffi;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;
import com.konylabs.api.TableLib;
import com.konylabs.vm.LuaTable;



import com.kony.settings.SettingsNavigation;
import com.konylabs.libintf.Library;
import com.konylabs.libintf.JSLibrary;
import com.konylabs.vm.LuaError;
import com.konylabs.vm.LuaNil;


public class N_openSettings extends JSLibrary {

 
 
	public static final String settings = "settings";
 
 
	public static final String securitySettings = "securitySettings";
 
 
	public static final String wifiSettings = "wifiSettings";
 
 
	public static final String pushSettings = "pushSettings";
 
	String[] methods = { settings, securitySettings, wifiSettings, pushSettings };


 Library libs[] = null;
 public Library[] getClasses() {
 libs = new Library[0];
 return libs;
 }



	public N_openSettings(){
	}

	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		case 0:
 if (paramLen != 0){ return new Object[] {new Double(100),"Invalid Params"}; }
 ret = this.settings( );
 
 			break;
 		case 1:
 if (paramLen != 0){ return new Object[] {new Double(100),"Invalid Params"}; }
 ret = this.securitySettings( );
 
 			break;
 		case 2:
 if (paramLen != 0){ return new Object[] {new Double(100),"Invalid Params"}; }
 ret = this.wifiSettings( );
 
 			break;
 		case 3:
 if (paramLen != 0){ return new Object[] {new Double(100),"Invalid Params"}; }
 ret = this.pushSettings( );
 
 			break;
 		default:
			break;
		}
 
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "openSettings";
	}


	/*
	 * return should be status(0 and !0),address
	 */
 
 
 	public final Object[] settings( ){
 
		Object[] ret = null;
 com.kony.settings.SettingsNavigation.launchSettings( );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] securitySettings( ){
 
		Object[] ret = null;
 com.kony.settings.SettingsNavigation.securitySettings( );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] wifiSettings( ){
 
		Object[] ret = null;
 com.kony.settings.SettingsNavigation.wifiSettings( );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] pushSettings( ){
 
		Object[] ret = null;
 com.kony.settings.SettingsNavigation.pushSettings( );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
};
