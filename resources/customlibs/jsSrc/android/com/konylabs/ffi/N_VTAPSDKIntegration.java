package com.konylabs.ffi;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;
import com.konylabs.api.TableLib;
import com.konylabs.vm.LuaTable;



import com.kony.tmbbank.vtapsdk.SoftTokenSdk;
import com.konylabs.libintf.Library;
import com.konylabs.libintf.JSLibrary;
import com.konylabs.vm.LuaError;
import com.konylabs.vm.LuaNil;


public class N_VTAPSDKIntegration extends JSLibrary {

 
	String[] methods = { };


 Library libs[] = null;
 public Library[] getClasses() {
 libs = new Library[1];
 libs[0] = new VTAPSdkAPI();
 return libs;
 }



	public N_VTAPSDKIntegration(){
	}

	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		default:
			break;
		}
 
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "VTAPSDKIntegration";
	}


	/*
	 * return should be status(0 and !0),address
	 */
 


class VTAPSdkAPI extends JSLibrary {

 
 
	public static final String getVtapInstance = "getVtapInstance";
 
 
	public static final String setHostName = "setHostName";
 
 
	public static final String setupVtap = "setupVtap";
 
 
	public static final String getProvisoningStatus = "getProvisoningStatus";
 
 
	public static final String initiateProvisoning = "initiateProvisoning";
 
 
	public static final String isUserRegistered = "isUserRegistered";
 
 
	public static final String createPin = "createPin";
 
 
	public static final String isRememberPINChecked = "isRememberPINChecked";
 
 
	public static final String checkPin = "checkPin";
 
 
	public static final String onResume = "onResume";
 
 
	public static final String onPause = "onPause";
 
 
	public static final String onDestroy = "onDestroy";
 
 
	public static final String init = "init";
 
 
	public static final String sendTroubleShootingLogs = "sendTroubleShootingLogs";
 
 
	public static final String getTroubleShootingID = "getTroubleShootingID";
 
 
	public static final String pushNotificationRegister = "pushNotificationRegister";
 
 
	public static final String pkiCertDownload = "pkiCertDownload";
 
 
	public static final String isPKIFunctionRegistered = "isPKIFunctionRegistered";
 
 
	public static final String vMessageAck = "vMessageAck";
 
 
	public static final String setPKIHostName = "setPKIHostName";
 
 
	public static final String isPKIFunctionPINRemembered = "isPKIFunctionPINRemembered";
 
 
	public static final String pkiFunctionAuthenticate = "pkiFunctionAuthenticate";
 
 
	public static final String vMessageDecrypt = "vMessageDecrypt";
 
 
	public static final String checkDeviceCompatibility = "checkDeviceCompatibility";
 
 
	public static final String vMessageDownload = "vMessageDownload";
 
	String[] methods = { getVtapInstance, setHostName, setupVtap, getProvisoningStatus, initiateProvisoning, isUserRegistered, createPin, isRememberPINChecked, checkPin, onResume, onPause, onDestroy, init, sendTroubleShootingLogs, getTroubleShootingID, pushNotificationRegister, pkiCertDownload, isPKIFunctionRegistered, vMessageAck, setPKIHostName, isPKIFunctionPINRemembered, pkiFunctionAuthenticate, vMessageDecrypt, checkDeviceCompatibility, vMessageDownload };

	public Object createInstance(final Object[] params) {
 return new com.kony.tmbbank.vtapsdk.SoftTokenSdk(
 );
 }


	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		case 0:
 if (paramLen < 0 || paramLen > 1){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 ret = this.getVtapInstance(params[0]
 );
 
 			break;
 		case 1:
 if (paramLen < 2 || paramLen > 3){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 java.lang.String vtapServerURL1 = null;
 if(params[0+inc] != null && params[0+inc] != LuaNil.nil) {
 vtapServerURL1 = (java.lang.String)params[0+inc];
 }
 java.lang.String provServerURL1 = null;
 if(params[1+inc] != null && params[1+inc] != LuaNil.nil) {
 provServerURL1 = (java.lang.String)params[1+inc];
 }
 ret = this.setHostName(params[0]
 ,vtapServerURL1
 ,provServerURL1
 );
 
 			break;
 		case 2:
 if (paramLen < 2 || paramLen > 3){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 com.konylabs.vm.Function callBack12 = null;
 if(params[0+inc] != null && params[0+inc] != LuaNil.nil) {
 callBack12 = (com.konylabs.vm.Function)params[0+inc];
 }
 com.konylabs.vm.Function callBack22 = null;
 if(params[1+inc] != null && params[1+inc] != LuaNil.nil) {
 callBack22 = (com.konylabs.vm.Function)params[1+inc];
 }
 ret = this.setupVtap(params[0]
 ,callBack12
 ,callBack22
 );
 
 			break;
 		case 3:
 if (paramLen < 0 || paramLen > 1){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 ret = this.getProvisoningStatus(params[0]
 );
 
 			break;
 		case 4:
 if (paramLen < 2 || paramLen > 3){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 java.lang.String aPin4 = null;
 if(params[0+inc] != null && params[0+inc] != LuaNil.nil) {
 aPin4 = (java.lang.String)params[0+inc];
 }
 java.lang.String tokenSerial4 = null;
 if(params[1+inc] != null && params[1+inc] != LuaNil.nil) {
 tokenSerial4 = (java.lang.String)params[1+inc];
 }
 ret = this.initiateProvisoning(params[0]
 ,aPin4
 ,tokenSerial4
 );
 
 			break;
 		case 5:
 if (paramLen < 1 || paramLen > 2){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 java.lang.String tokenSerial5 = null;
 if(params[0+inc] != null && params[0+inc] != LuaNil.nil) {
 tokenSerial5 = (java.lang.String)params[0+inc];
 }
 ret = this.isUserRegistered(params[0]
 ,tokenSerial5
 );
 
 			break;
 		case 6:
 if (paramLen < 1 || paramLen > 2){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 java.lang.String tokenSerial6 = null;
 if(params[0+inc] != null && params[0+inc] != LuaNil.nil) {
 tokenSerial6 = (java.lang.String)params[0+inc];
 }
 ret = this.createPin(params[0]
 ,tokenSerial6
 );
 
 			break;
 		case 7:
 if (paramLen < 1 || paramLen > 2){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 java.lang.String tokenSerial7 = null;
 if(params[0+inc] != null && params[0+inc] != LuaNil.nil) {
 tokenSerial7 = (java.lang.String)params[0+inc];
 }
 ret = this.isRememberPINChecked(params[0]
 ,tokenSerial7
 );
 
 			break;
 		case 8:
 if (paramLen < 1 || paramLen > 2){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 java.lang.String tokenSerial8 = null;
 if(params[0+inc] != null && params[0+inc] != LuaNil.nil) {
 tokenSerial8 = (java.lang.String)params[0+inc];
 }
 ret = this.checkPin(params[0]
 ,tokenSerial8
 );
 
 			break;
 		case 9:
 if (paramLen < 0 || paramLen > 1){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 ret = this.onResume(params[0]
 );
 
 			break;
 		case 10:
 if (paramLen < 0 || paramLen > 1){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 ret = this.onPause(params[0]
 );
 
 			break;
 		case 11:
 if (paramLen < 0 || paramLen > 1){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 ret = this.onDestroy(params[0]
 );
 
 			break;
 		case 12:
 if (paramLen < 0 || paramLen > 1){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 ret = this.init(params[0]
 );
 
 			break;
 		case 13:
 if (paramLen < 0 || paramLen > 1){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 ret = this.sendTroubleShootingLogs(params[0]
 );
 
 			break;
 		case 14:
 if (paramLen < 0 || paramLen > 1){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 ret = this.getTroubleShootingID(params[0]
 );
 
 			break;
 		case 15:
 if (paramLen < 4 || paramLen > 5){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 java.lang.String userId15 = null;
 if(params[0+inc] != null && params[0+inc] != LuaNil.nil) {
 userId15 = (java.lang.String)params[0+inc];
 }
 java.lang.String deviceId15 = null;
 if(params[1+inc] != null && params[1+inc] != LuaNil.nil) {
 deviceId15 = (java.lang.String)params[1+inc];
 }
 java.lang.String pushToken15 = null;
 if(params[2+inc] != null && params[2+inc] != LuaNil.nil) {
 pushToken15 = (java.lang.String)params[2+inc];
 }
 java.lang.String tokenSerial15 = null;
 if(params[3+inc] != null && params[3+inc] != LuaNil.nil) {
 tokenSerial15 = (java.lang.String)params[3+inc];
 }
 ret = this.pushNotificationRegister(params[0]
 ,userId15
 ,deviceId15
 ,pushToken15
 ,tokenSerial15
 );
 
 			break;
 		case 16:
 if (paramLen < 4 || paramLen > 5){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 Double functionId16 = null;
 if(params[0+inc] != null && params[0+inc] != LuaNil.nil) {
 functionId16 = (Double)params[0+inc];
 }
 java.lang.String messageId16 = null;
 if(params[1+inc] != null && params[1+inc] != LuaNil.nil) {
 messageId16 = (java.lang.String)params[1+inc];
 }
 java.lang.String messageType16 = null;
 if(params[2+inc] != null && params[2+inc] != LuaNil.nil) {
 messageType16 = (java.lang.String)params[2+inc];
 }
 java.lang.String tokenSerial16 = null;
 if(params[3+inc] != null && params[3+inc] != LuaNil.nil) {
 tokenSerial16 = (java.lang.String)params[3+inc];
 }
 ret = this.pkiCertDownload(params[0]
 ,functionId16
 ,messageId16
 ,messageType16
 ,tokenSerial16
 );
 
 			break;
 		case 17:
 if (paramLen < 1 || paramLen > 2){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 Double functionId17 = null;
 if(params[0+inc] != null && params[0+inc] != LuaNil.nil) {
 functionId17 = (Double)params[0+inc];
 }
 ret = this.isPKIFunctionRegistered(params[0]
 ,functionId17
 );
 
 			break;
 		case 18:
 if (paramLen < 1 || paramLen > 2){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 java.lang.String messageId18 = null;
 if(params[0+inc] != null && params[0+inc] != LuaNil.nil) {
 messageId18 = (java.lang.String)params[0+inc];
 }
 ret = this.vMessageAck(params[0]
 ,messageId18
 );
 
 			break;
 		case 19:
 if (paramLen < 1 || paramLen > 2){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 java.lang.String pkiServer19 = null;
 if(params[0+inc] != null && params[0+inc] != LuaNil.nil) {
 pkiServer19 = (java.lang.String)params[0+inc];
 }
 ret = this.setPKIHostName(params[0]
 ,pkiServer19
 );
 
 			break;
 		case 20:
 if (paramLen < 1 || paramLen > 2){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 Double functionId20 = null;
 if(params[0+inc] != null && params[0+inc] != LuaNil.nil) {
 functionId20 = (Double)params[0+inc];
 }
 ret = this.isPKIFunctionPINRemembered(params[0]
 ,functionId20
 );
 
 			break;
 		case 21:
 if (paramLen < 3 || paramLen > 4){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 java.lang.String messageId21 = null;
 if(params[0+inc] != null && params[0+inc] != LuaNil.nil) {
 messageId21 = (java.lang.String)params[0+inc];
 }
 java.lang.String dataToBeSigned21 = null;
 if(params[1+inc] != null && params[1+inc] != LuaNil.nil) {
 dataToBeSigned21 = (java.lang.String)params[1+inc];
 }
 Boolean reject21 = null;
 if(params[2+inc] != null && params[2+inc] != LuaNil.nil) {
 reject21 = (Boolean)params[2+inc];
 }
 ret = this.pkiFunctionAuthenticate(params[0]
 ,messageId21
 ,dataToBeSigned21
 ,reject21
 );
 
 			break;
 		case 22:
 if (paramLen < 1 || paramLen > 2){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 java.lang.String encryptedMsg22 = null;
 if(params[0+inc] != null && params[0+inc] != LuaNil.nil) {
 encryptedMsg22 = (java.lang.String)params[0+inc];
 }
 ret = this.vMessageDecrypt(params[0]
 ,encryptedMsg22
 );
 
 			break;
 		case 23:
 if (paramLen < 0 || paramLen > 1){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 ret = this.checkDeviceCompatibility(params[0]
 );
 
 			break;
 		case 24:
 if (paramLen < 3 || paramLen > 4){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 java.lang.String messageId24 = null;
 if(params[0+inc] != null && params[0+inc] != LuaNil.nil) {
 messageId24 = (java.lang.String)params[0+inc];
 }
 java.lang.String messageType24 = null;
 if(params[1+inc] != null && params[1+inc] != LuaNil.nil) {
 messageType24 = (java.lang.String)params[1+inc];
 }
 java.lang.String messageFlag24 = null;
 if(params[2+inc] != null && params[2+inc] != LuaNil.nil) {
 messageFlag24 = (java.lang.String)params[2+inc];
 }
 ret = this.vMessageDownload(params[0]
 ,messageId24
 ,messageType24
 ,messageFlag24
 );
 
 			break;
 		default:
			break;
		}
 
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "VTAPSdkAPI";
	}

	/*
	 * return should be status(0 and !0),address
	 */
 
 
 	public final Object[] getVtapInstance( Object self ){
 
		Object[] ret = null;
 Boolean val = new Boolean(((com.kony.tmbbank.vtapsdk.SoftTokenSdk)self).getVtapInstance( ));
 
 			ret = new Object[]{val, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] setHostName( Object self ,java.lang.String inputKey0
 ,java.lang.String inputKey1
 ){
 
		Object[] ret = null;
 ((com.kony.tmbbank.vtapsdk.SoftTokenSdk)self).setHostName( inputKey0
 , inputKey1
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] setupVtap( Object self ,com.konylabs.vm.Function inputKey0
 ,com.konylabs.vm.Function inputKey1
 ){
 
		Object[] ret = null;
 ((com.kony.tmbbank.vtapsdk.SoftTokenSdk)self).setupVtap( (com.konylabs.vm.Function)inputKey0
 , (com.konylabs.vm.Function)inputKey1
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] getProvisoningStatus( Object self ){
 
		Object[] ret = null;
 Boolean val = new Boolean(((com.kony.tmbbank.vtapsdk.SoftTokenSdk)self).getProvisoningStatus( ));
 
 			ret = new Object[]{val, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] initiateProvisoning( Object self ,java.lang.String inputKey0
 ,java.lang.String inputKey1
 ){
 
		Object[] ret = null;
 Double val = new Double(((com.kony.tmbbank.vtapsdk.SoftTokenSdk)self).initiateProvisoning( inputKey0
 , inputKey1
 ));
 
 			ret = new Object[]{val, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] isUserRegistered( Object self ,java.lang.String inputKey0
 ){
 
		Object[] ret = null;
 Boolean val = new Boolean(((com.kony.tmbbank.vtapsdk.SoftTokenSdk)self).isUserRegistered( inputKey0
 ));
 
 			ret = new Object[]{val, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] createPin( Object self ,java.lang.String inputKey0
 ){
 
		Object[] ret = null;
 Double val = new Double(((com.kony.tmbbank.vtapsdk.SoftTokenSdk)self).createPin( inputKey0
 ));
 
 			ret = new Object[]{val, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] isRememberPINChecked( Object self ,java.lang.String inputKey0
 ){
 
		Object[] ret = null;
 Boolean val = new Boolean(((com.kony.tmbbank.vtapsdk.SoftTokenSdk)self).isRememberPINChecked( inputKey0
 ));
 
 			ret = new Object[]{val, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] checkPin( Object self ,java.lang.String inputKey0
 ){
 
		Object[] ret = null;
 Double val = new Double(((com.kony.tmbbank.vtapsdk.SoftTokenSdk)self).checkPin( inputKey0
 ));
 
 			ret = new Object[]{val, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] onResume( Object self ){
 
		Object[] ret = null;
 ((com.kony.tmbbank.vtapsdk.SoftTokenSdk)self).onResume( );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] onPause( Object self ){
 
		Object[] ret = null;
 ((com.kony.tmbbank.vtapsdk.SoftTokenSdk)self).onPause( );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] onDestroy( Object self ){
 
		Object[] ret = null;
 ((com.kony.tmbbank.vtapsdk.SoftTokenSdk)self).onDestroy( );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] init( Object self ){
 
		Object[] ret = null;
 java.lang.Object val = ((com.kony.tmbbank.vtapsdk.SoftTokenSdk)self).init( );
 
 			ret = new Object[]{val, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] sendTroubleShootingLogs( Object self ){
 
		Object[] ret = null;
 Double val = new Double(((com.kony.tmbbank.vtapsdk.SoftTokenSdk)self).sendTroubleShootingLogs( ));
 
 			ret = new Object[]{val, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] getTroubleShootingID( Object self ){
 
		Object[] ret = null;
 java.lang.String val = ((com.kony.tmbbank.vtapsdk.SoftTokenSdk)self).getTroubleShootingID( );
 
 			ret = new Object[]{val, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] pushNotificationRegister( Object self ,java.lang.String inputKey0
 ,java.lang.String inputKey1
 ,java.lang.String inputKey2
 ,java.lang.String inputKey3
 ){
 
		Object[] ret = null;
 Boolean val = new Boolean(((com.kony.tmbbank.vtapsdk.SoftTokenSdk)self).pushNotificationRegister( inputKey0
 , inputKey1
 , inputKey2
 , inputKey3
 ));
 
 			ret = new Object[]{val, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] pkiCertDownload( Object self ,Double inputKey0
 ,java.lang.String inputKey1
 ,java.lang.String inputKey2
 ,java.lang.String inputKey3
 ){
 
		Object[] ret = null;
 Boolean val = new Boolean(((com.kony.tmbbank.vtapsdk.SoftTokenSdk)self).pkiCertDownload( inputKey0.intValue() , inputKey1
 , inputKey2
 , inputKey3
 ));
 
 			ret = new Object[]{val, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] isPKIFunctionRegistered( Object self ,Double inputKey0
 ){
 
		Object[] ret = null;
 Boolean val = new Boolean(((com.kony.tmbbank.vtapsdk.SoftTokenSdk)self).isPKIFunctionRegistered( inputKey0.intValue() ));
 
 			ret = new Object[]{val, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] vMessageAck( Object self ,java.lang.String inputKey0
 ){
 
		Object[] ret = null;
 Boolean val = new Boolean(((com.kony.tmbbank.vtapsdk.SoftTokenSdk)self).vMessageAck( inputKey0
 ));
 
 			ret = new Object[]{val, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] setPKIHostName( Object self ,java.lang.String inputKey0
 ){
 
		Object[] ret = null;
 ((com.kony.tmbbank.vtapsdk.SoftTokenSdk)self).setPKIHostName( inputKey0
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] isPKIFunctionPINRemembered( Object self ,Double inputKey0
 ){
 
		Object[] ret = null;
 Boolean val = new Boolean(((com.kony.tmbbank.vtapsdk.SoftTokenSdk)self).isPKIFunctionPINRemembered( inputKey0.intValue() ));
 
 			ret = new Object[]{val, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] pkiFunctionAuthenticate( Object self ,java.lang.String inputKey0
 ,java.lang.String inputKey1
 ,Boolean inputKey2
 ){
 
		Object[] ret = null;
 Double val = new Double(((com.kony.tmbbank.vtapsdk.SoftTokenSdk)self).pkiFunctionAuthenticate( inputKey0
 , inputKey1
 , inputKey2.booleanValue() ));
 
 			ret = new Object[]{val, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] vMessageDecrypt( Object self ,java.lang.String inputKey0
 ){
 
		Object[] ret = null;
 java.lang.String val = ((com.kony.tmbbank.vtapsdk.SoftTokenSdk)self).vMessageDecrypt( inputKey0
 );
 
 			ret = new Object[]{val, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] checkDeviceCompatibility( Object self ){
 
		Object[] ret = null;
 Double val = new Double(((com.kony.tmbbank.vtapsdk.SoftTokenSdk)self).checkDeviceCompatibility( ));
 
 			ret = new Object[]{val, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] vMessageDownload( Object self ,java.lang.String inputKey0
 ,java.lang.String inputKey1
 ,java.lang.String inputKey2
 ){
 
		Object[] ret = null;
 java.lang.String val = ((com.kony.tmbbank.vtapsdk.SoftTokenSdk)self).vMessageDownload( inputKey0
 , inputKey1
 , inputKey2
 );
 
 			ret = new Object[]{val, new Double(0)};
 		return ret;
	}
 
}

};
