package com.konylabs.ffi;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;
import com.konylabs.api.TableLib;
import com.konylabs.vm.LuaTable;



import com.kony.partners.MarshmallowBioHelper;
import com.kony.partners.SamsungBioHelper;
import com.konylabs.libintf.Library;
import com.konylabs.libintf.JSLibrary;
import com.konylabs.vm.LuaError;
import com.konylabs.vm.LuaNil;


public class N_AndroidTouchUtil extends JSLibrary {

 
	String[] methods = { };


 Library libs[] = null;
 public Library[] getClasses() {
 libs = new Library[2];
 libs[0] = new MarshmallowUtil();
 libs[1] = new SamsungUtil();
 return libs;
 }



	public N_AndroidTouchUtil(){
	}

	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		default:
			break;
		}
 
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "AndroidTouchUtil";
	}


	/*
	 * return should be status(0 and !0),address
	 */
 


class MarshmallowUtil extends JSLibrary {

 
 
	public static final String isFingerPrintSupported = "isFingerPrintSupported";
 
 
	public static final String startListening4Bio = "startListening4Bio";
 
 
	public static final String stopListening4Bio = "stopListening4Bio";
 
 
	public static final String hasRegisteredFinger = "hasRegisteredFinger";
 
	String[] methods = { isFingerPrintSupported, startListening4Bio, stopListening4Bio, hasRegisteredFinger };

	public Object createInstance(final Object[] params) {
 return new com.kony.partners.MarshmallowBioHelper(
 );
 }


	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		case 0:
 if (paramLen < 0 || paramLen > 1){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 ret = this.isFingerPrintSupported(params[0]
 );
 
 			break;
 		case 1:
 if (paramLen < 1 || paramLen > 2){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 com.konylabs.vm.Function callbackFunction1 = null;
 if(params[0+inc] != null && params[0+inc] != LuaNil.nil) {
 callbackFunction1 = (com.konylabs.vm.Function)params[0+inc];
 }
 ret = this.startListening4Bio(params[0]
 ,callbackFunction1
 );
 
 			break;
 		case 2:
 if (paramLen < 0 || paramLen > 1){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 ret = this.stopListening4Bio(params[0]
 );
 
 			break;
 		case 3:
 if (paramLen < 0 || paramLen > 1){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 ret = this.hasRegisteredFinger(params[0]
 );
 
 			break;
 		default:
			break;
		}
 
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "MarshmallowUtil";
	}

	/*
	 * return should be status(0 and !0),address
	 */
 
 
 	public final Object[] isFingerPrintSupported( Object self ){
 
		Object[] ret = null;
 java.lang.Boolean val = ((com.kony.partners.MarshmallowBioHelper)self).isFingerPrintSupported( );
 
 			ret = new Object[]{val, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] startListening4Bio( Object self ,com.konylabs.vm.Function inputKey0
 ){
 
		Object[] ret = null;
 ((com.kony.partners.MarshmallowBioHelper)self).startListening4Bio( (com.konylabs.vm.Function)inputKey0
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] stopListening4Bio( Object self ){
 
		Object[] ret = null;
 ((com.kony.partners.MarshmallowBioHelper)self).stopListening4Bio( );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] hasRegisteredFinger( Object self ){
 
		Object[] ret = null;
 java.lang.Boolean val = ((com.kony.partners.MarshmallowBioHelper)self).hasRegisteredFinger( );
 
 			ret = new Object[]{val, new Double(0)};
 		return ret;
	}
 
}



class SamsungUtil extends JSLibrary {

 
 
	public static final String hasRegisteredFinger = "hasRegisteredFinger";
 
 
	public static final String authenticateUsingFingerprint = "authenticateUsingFingerprint";
 
 
	public static final String isFingerPrintSupported = "isFingerPrintSupported";
 
 
	public static final String customizedDialogWithTransparency = "customizedDialogWithTransparency";
 
 
	public static final String stoplistening4Bio = "stoplistening4Bio";
 
 
	public static final String startlistening4Bio = "startlistening4Bio";
 
	String[] methods = { hasRegisteredFinger, authenticateUsingFingerprint, isFingerPrintSupported, customizedDialogWithTransparency, stoplistening4Bio, startlistening4Bio };

	public Object createInstance(final Object[] params) {
 return new com.kony.partners.SamsungBioHelper(
 );
 }


	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		case 0:
 if (paramLen < 0 || paramLen > 1){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 ret = this.hasRegisteredFinger(params[0]
 );
 
 			break;
 		case 1:
 if (paramLen < 1 || paramLen > 2){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 com.konylabs.vm.Function callBack1 = null;
 if(params[0+inc] != null && params[0+inc] != LuaNil.nil) {
 callBack1 = (com.konylabs.vm.Function)params[0+inc];
 }
 ret = this.authenticateUsingFingerprint(params[0]
 ,callBack1
 );
 
 			break;
 		case 2:
 if (paramLen < 0 || paramLen > 1){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 ret = this.isFingerPrintSupported(params[0]
 );
 
 			break;
 		case 3:
 if (paramLen < 1 || paramLen > 2){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 com.konylabs.vm.Function callback3 = null;
 if(params[0+inc] != null && params[0+inc] != LuaNil.nil) {
 callback3 = (com.konylabs.vm.Function)params[0+inc];
 }
 ret = this.customizedDialogWithTransparency(params[0]
 ,callback3
 );
 
 			break;
 		case 4:
 if (paramLen < 0 || paramLen > 1){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 ret = this.stoplistening4Bio(params[0]
 );
 
 			break;
 		case 5:
 if (paramLen < 0 || paramLen > 1){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 ret = this.startlistening4Bio(params[0]
 );
 
 			break;
 		default:
			break;
		}
 
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "SamsungUtil";
	}

	/*
	 * return should be status(0 and !0),address
	 */
 
 
 	public final Object[] hasRegisteredFinger( Object self ){
 
		Object[] ret = null;
 java.lang.Boolean val = ((com.kony.partners.SamsungBioHelper)self).hasRegisteredFinger( );
 
 			ret = new Object[]{val, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] authenticateUsingFingerprint( Object self ,com.konylabs.vm.Function inputKey0
 ){
 
		Object[] ret = null;
 ((com.kony.partners.SamsungBioHelper)self).authenticateUsingFingerprint( (com.konylabs.vm.Function)inputKey0
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] isFingerPrintSupported( Object self ){
 
		Object[] ret = null;
 java.lang.Boolean val = ((com.kony.partners.SamsungBioHelper)self).isFingerPrintSupported( );
 
 			ret = new Object[]{val, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] customizedDialogWithTransparency( Object self ,com.konylabs.vm.Function inputKey0
 ){
 
		Object[] ret = null;
 ((com.kony.partners.SamsungBioHelper)self).customizedDialogWithTransparency( (com.konylabs.vm.Function)inputKey0
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] stoplistening4Bio( Object self ){
 
		Object[] ret = null;
 ((com.kony.partners.SamsungBioHelper)self).stoplistening4Bio( );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] startlistening4Bio( Object self ){
 
		Object[] ret = null;
 Double val = new Double(((com.kony.partners.SamsungBioHelper)self).startlistening4Bio( ));
 
 			ret = new Object[]{val, new Double(0)};
 		return ret;
	}
 
}

};
