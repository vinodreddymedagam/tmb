var imgCNT = 0;flag = 0;
uploadImageSpa = {
						
    initializeWidget : function(parentNode, widgetModel)
    {
		var key1="";
		key1=kony.i18n.getLocalizedString("keychooseFile");
		var imageUpload =
	'<div id="form1">'+
	'<div>'+		   
    '<input id="fileToUpload" type="file" name="fileToUpload[]" onchange="fileSelected();" style="visibility:hidden;" />'+
    '</div>'+
    '<div>'+
    '<input type="button" id ="Choose File" value="'+kony.i18n.getLocalizedString("keychooseFile")+'" onclick="click1();" style="background-color:rgb(0,122,188); padding:8px 0; margin:0 0 10px 0; width:100%; color:#fff; font:normal 125% DB Ozone X Med; cursor: pointer; border-radius:5px; outline:0 none; border:0 none;"/>'+
    '</div>';
	if(kony.application.getCurrentForm().id == "frmeditMyProfile"){
	imageUpload+=
	'<div>'+
    '<input type="button" id ="Delete" value="'+kony.i18n.getLocalizedString("Receipent_delete")+'" onclick="DeleteImage();" style="background-color:rgb(0,122,188); padding:8px 0; margin:0 0 10px 0; width:100%; color:#fff; font:normal 125% DB Ozone X Med; cursor: pointer; border-radius:5px; outline:0 none; border:0 none;"/>'+
    '</div>';
	}
	imageUpload+=
	'<div>'+
    '<input type="button" id ="Cancel" value='+kony.i18n.getLocalizedString("keyCancelButton")+' onclick="CancelPopup();" style="background-color:rgb(0,122,188); padding:8px 0; margin:0 0 10px 0; width:100%; color:#fff; font:normal 125% DB Ozone X Med; cursor: pointer; border-radius:5px; outline:0 none; border:0 none;"/>'+
    '</div>';
		
		parentNode.innerHTML = imageUpload;
    },
	
    modelChange : function(widgetModel, propertyChanged, propertyValue){}
    
}

function click1()
{
  document.getElementById("fileToUpload").click();
}

function DeleteImage()
{
  frmeditMyProfile.imgprofpic.src = "avatar.png";
  profilePicFlagSPA = true;
  profilePicFlag = "picture";
  popUploadPicSpa.dismiss();
}

function CancelPopup()
{
  popUploadPicSpa.dismiss();
}


function fileSelected() {
            //alert("entered...");
            var file = document.getElementById('fileToUpload').files[0];
            if (file) {
                var fileSize = 0;
                if (file.size > 1024 * 1024)
                    fileSize = (Math.round(file.size * 5 * 100 / (1024 * 1024)) / 100).toString() + 'MB';
                else
                    fileSize = (Math.round(file.size * 100 / 1024) / 100).toString() + 'KB';

            }
			uploadFile();
			document.getElementById("fileToUpload").remove();
        }

         function uploadFile() {
            //alert("entered...");
            if(document.getElementById('fileToUpload').value == ""){
            	alert("Please select a file to upload.");
            }else{
            	/*var fd = new FormData();
	            var url = "https://" + appConfig.serverIp + ":"+appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/uploadFile";
	            fd.append("fileToUpload", document.getElementById('fileToUpload').files[0]);
	            if(kony.application.getCurrentForm().id=="frmIBCMEditMyProfile")
	            {
	              var fread=new FileReader();
	              fread.addEventListener("load",function (event) {
	              var binaryString = '',bytes = new Uint8Array(event.target.result),length = bytes.length;
                  for (var i = 0; i < length; i++) {
                     binaryString += String.fromCharCode(bytes[i]);
                     }  
                 
                  gblRcBase64List=window.btoa(binaryString);
                  //gblRcBase64List=kony.convertToBase64(binaryString);                
                  gblRcBase64List = gblRcBase64List.replace(/[\n\r\s\f\t\v]+/g, '');
                  frmIBCMEditMyProfile.imgprofpic.base64=gblRcBase64List;
                  popupDisplayPicOptions.dismiss();
                  //imageServiceCallIB();
                  //console.log("File contents: " + contents);
                                   });

                  fread.addEventListener("error",function(event) {
                  console.error("File could not be read! Code " + event.target.error.code);
                                                  });
                  fread.readAsArrayBuffer(document.getElementById('fileToUpload').files[0]);
                  
                  //gblRcBase64List =kony.convertToBase64(document.getElementById('fileToUpload').files[0]);
                  //alert(gblRcBase64List);
                  //alert("service called...");
                  
                }
                else{
	            var xhr = new XMLHttpRequest();
	            xhr.upload.addEventListener("progress", uploadProgress, false);
	            xhr.addEventListener("load", uploadComplete, false);
	            xhr.addEventListener("error", uploadFailed, false);
	            xhr.addEventListener("abort", uploadCanceled, false);
	            xhr.open("POST", url);
	            xhr.send(fd);}
	            */
		        var method = "post"; 
		        var url;
		        if (appConfig.secureServerPort != null) {
	        		//var url = "http://" + appConfig.serverIp + ":9080" + "/" + appConfig.middlewareContext + "/UploadServlet";
	        		url = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext +"/UploadServlet";
	       			 
	    		}
	    	    else {
	       			url = "https://" + appConfig.serverIp + "/" + appConfig.middlewareContext + "/UploadServlet";
	      			 
	   		    }
				//var url="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "UploadServlet";
			    var form = document.createElement("form");
			    form.setAttribute("method", method);
			    form.setAttribute("action", url);
				form.setAttribute("enctype", "multipart/form-data");
				form.setAttribute("encoding", "multipart/form-data");
				form.setAttribute("target","iframe_outs");
	            var hiddenField = document.getElementById('fileToUpload');
				var hiddenField1 = document.createElement("input");
	            hiddenField1.setAttribute("type", "hidden");
	            hiddenField1.setAttribute("name", "crmId");
	            hiddenField1.setAttribute("value", gblcrmId);
	            form.appendChild(hiddenField);
				form.appendChild(hiddenField1);
	            if(kony.application.getCurrentForm().id == "frmMyRecipientEditProfile" || kony.application.getCurrentForm().id == "frmMyRecipientAddProfile"){
	            	var hiddenField2 = document.createElement("input");
	            	hiddenField2.setAttribute("type", "hidden");
	            	hiddenField2.setAttribute("name", "personalizedId");
	            	hiddenField2.setAttribute("value", "temp");
	            	form.appendChild(hiddenField2);
	            }
			    document.body.appendChild(form);
				var iframe_out=document.createElement("iframe");
				iframe_out.name="iframe_outs";
				iframe_out.id="iframe_outs";
				iframe_out.setAttribute("src",url);
				if(document.getElementById('iframe_outs') !="" && document.getElementById('iframe_outs') !=null)
				{
				  var temp = document.getElementById('iframe_outs');
				  temp.parentNode.removeChild(temp);
				}
				document.body.appendChild(iframe_out);
				iframe_out.style.visibility="hidden";
				//gblFormString=form;
			    form.submit();
				//alert(document.getElementById('fileToUpload').files[0].name);
			                                                                          
				    profilePicFlag = "picture";
				    profilePicFlagSPA = false;
				    showLoadingScreen();
				    imgCNT = 0;
				    try{
                    	kony.timer.cancel("imagePicTime");
                    }catch(err ){}
				    kony.timer.schedule("imagePicTime",imagePicCallback ,5, true);
            }
        }
		
		function imagePicCallback()
        {
          imgCNT++;
		  var iframe = document.getElementById('iframe_outs');
          if(imgCNT == 18)
          {
            try{
                 	kony.timer.cancel("imagePicTime");
                 }catch(err ){}
			  imgCNT = 0;
			  showAlert(kony.i18n.getLocalizedString("keyImageSizeTooLarge"), "");
			  if(kony.application.getCurrentForm().id == "frmMyRecipientEditProfile" || kony.application.getCurrentForm().id == "frmMyRecipientAddProfile" ){
			  uploadRecipienttimerCallbackFailSpa();
			  }
			  else{
          	  uploadEditMyProfileCallbackFailSpa();
          	  }
          }
		  if(iframe.contentDocument.document != null && iframe.contentDocument.document !=""  && iframe.contentDocument.document !=undefined)
		  {
		    var temp = iframe.contentDocument.document.body.innerHTML;flag = 1;
		  }
          else if(iframe.contentWindow.document != null && iframe.contentWindow.document !=""  && iframe.contentWindow.document !=undefined)
          {
            var temp = iframe.contentWindow.document.body.innerHTML;flag = 1;
		  }
		  if(flag > 0)
		  {
            if(temp.search("Uploaded Filename")>= 0) 
            {
              imgCNT = 0;flag = 0;
              try{
                 	kony.timer.cancel("imagePicTime");
                 }catch(err ){}
                 
                   if(kony.application.getCurrentForm().id == "frmMyRecipientEditProfile" || kony.application.getCurrentForm().id == "frmMyRecipientAddProfile" ){
	                   try{
							kony.timer.cancel("upload1");
						}
							catch(e){
						}
						showLoadingScreen();
						kony.timer.schedule("upload1",uploadRecipienttimerCallbackSpa , 10, false);
                   
                   }
                   else{
				              try{
				                    kony.timer.cancel("uploadprofilepic");
				                 }catch(err ){}
				              kony.timer.schedule("uploadprofilepic",uploadEditMyProfileCallbackSpa ,10, false);
               		   }     
            }
			else if(temp.search("Upload")>= 0)
			{
			  imgCNT = 0; flag = 0;
			  showAlert(kony.i18n.getLocalizedString("keyImageSizeTooLarge"), ""); 
			  try{
                 	kony.timer.cancel("imagePicTime");
                 }catch(err ){}
			  if(kony.application.getCurrentForm().id == "frmMyRecipientEditProfile" || kony.application.getCurrentForm().id == "frmMyRecipientAddProfile"){
			 	 uploadRecipienttimerCallbackSpa();
			  }
			  else{
          	 	 uploadEditMyProfileCallbackSpa();
          	  }
			 
			}
		  }
    }
    
    function uploadProgress(evt) {
            if (evt.lengthComputable) {
                var percentComplete = Math.round(evt.loaded * 100 / evt.total);
                document.getElementById('progressNumber').innerHTML = percentComplete.toString() + '%';
                document.getElementById('prog').value = percentComplete;
            }
            else {
                document.getElementById('progressNumber').innerHTML = 'unable to compute';
            }
        }

        function uploadComplete(evt) {
            /* This event is raised when the server send back a response */
            gblRcBase64List = evt.target.responseText;
             if(gblAdd_Receipent_State == gblEXISTING_RC_EDITION){
            	startImageServiceCallRecipientsIB(null);
            }else if(gblAdd_Receipent_State == gblNEW_RC_ADDITION || gblAdd_Receipent_State == gblNEW_RC_ADDITION_PRECONFIRM){
            	// Dont call upload service here
				frmIBMyReceipentsAddContactManually.image210107168135.src = gblRcBase64List;
            }
        }

        function uploadFailed(evt) {
            alert("There was an error attempting to upload the file.");
        }

        function uploadCanceled(evt) {
            alert("The upload has been canceled by the user or the browser dropped the connection.");
        }