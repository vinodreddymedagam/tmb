vCoverflow = {
						
    initializeWidget : function(parentNode, widgetModel)
    {	
		var carouselWidget =
				'<link href="desktopweb/jslib/tparty/libraries/jQuery/skins.css" rel="stylesheet" type="text/css">'+
				'<style type="text/css">'+
				'@font-face{'+
						'font-family: DB Ozone X Bd;'+
						'src:url("desktopweb/DB Ozone X Bd.woff"),'+
						'src:url("desktopweb/DB Ozone X Bd.eot");'+
				'}'+
				'@font-face{'+
						'font-family: DB Ozone X;'+
						'src:url("desktopweb/DB Ozone X.woff"),'+
						'src:url("desktopweb/DB Ozone X.eot");'+
				'}'+
					'#callback-output {'+
						'height:250px;'+
						'overflow:scroll;'+
					'}'+
					'table {'+
						'border-collapse:collapse;'+
						'width: 100%;'+
						'height: 100%;'+
					'}'+
					'table {'+
						'border: 0px solid red;'+
						'border-collapse:collapse;'+
						'padding: 13% 0% 0% 6%;'+
						'margin: 0% 0% 0% 0%;'+
					'}'+
					
				'</style>'+
				'<div id="carousel">';
				var colorArray = ["#EE8080","#6FB3D6","#C1DB57","#F4ED26","#E28A3C"];
				var data = widgetModel["data"];
				
				var numberOfAccounts =  data.length;
				
				for(var i = 0, j = 0; i < numberOfAccounts; i++, ((j < colorArray.length - 1) ? (j++) : (j=0))){
						//carousel += '<li><span>Account Number:'+i+'<br/>Product:'+Product+'<br/>Remark: '+Remark+'</span></li>';
						if(data[i]["lblDream"]!=null)
						{
							carouselWidget += 
						'<span id="item-'+i+'" class = "carouselNormal" style="background-color:'+["#64A4CB"]+'" >'+
							'<table align="center" >'+
								'<tr >'+
									'<td align="center" style="margin:0% 0% 0% 0%;padding: 0% 0% 0% 0%">'+
										'<img src="desktopweb/images/'+data[i]["img1"]+'" float="left"/>'+
									'</td>'+
									'</tr>'+
								'<tr>'+
									'<td colspan="2">'+
										'<p id = "Dream" align="centre" style="margin:0% 0% 0% 0%">'+data[i]["lblDream"]+'</p>'+
									'</td>'+								
								'</tr>'+
							'</table>'+
							
						'</span>';
						}
						else
						{
							carouselWidget += 
						'<span id="item-'+i+'" class = "carouselNormal" style="background-color:'+colorArray[j]+'" >'+
							'<table align="center" >'+
								'<tr >'+
									'<td align="center" style="margin:0% 0% 0% 0%;padding: 0% 0% 0% 0%;height: 70px;width: 60px">'+
										'<img src="'+data[i]["img1"]+'" float="left" style="height:65px; width: 60px"/>'+
									'</td>'+
									'<td >'+
										'<p id = "accountType" align="left" style="margin:0% 0% 0% 0%;padding: 0% 0% 0% 0%">'+data[i]["lblCustName"]+'</p>'+
										'<p id = "amount" align="left" style="margin:0% 0% 0% 0%">'+data[i]["lblBalance"]+'</p>'+
										
										
									'</td>'+
								'</tr>'+
								'<tr>'+
									'<td colspan="2">'+
										'<p id = "accNo" align="left" style="margin:0% 0% 0% 0%;padding: 0% 0% 0% 4%">'+'<b>'+data[i]["lblACno"]+'</b> '+data[i]["lblActNoval"]+'</p>'+
										'<p id = "product" align="left" style="margin:0% 0% 0% 0%;padding: 0% 0% 0% 4%">'+'<b>'+data[i]["lblProduct"]+'</b> '+data[i]["lblProductVal"]+'</p>'+
										'<p id = "remFee" align="left" style="margin:0% 0% 0% 0%;padding: 0% 0% 0% 4%">'+'<b>'+data[i]["lblRemFee"]+'</b> '+data[i]["lblRemFeeval"]+'</p>'+
									'</td>'+								
								'</tr>'+
							'</table>'+
							
						'</span>';
						}
					
				}
			
				carouselWidget += '</div>';
					
			    parentNode.innerHTML = carouselWidget;
			    
			    if(numberOfAccounts == 1) {
					navigatorflag = true;
				} else {
					navigatorflag = true;
				}
			    
			$(document).ready(function () {
					var carousel = $("#carousel").waterwheelCarousel({
						navigateBtns: navigatorflag,
						childSelector: 'span',
						flankingItems: 2,
						orientation: "vertical",
						separation: 125,
						separationMultiplier: 2,
						sizeMultiplier: 0.8,
						opacityMultiplier:1,
						preloadImages: false,
						forcedImageWidth: '80%',
						forcedImageHeight: '36%',
						keyboardNav: true,
						activeClassName: 'carouselCenter',
						movingToCenter: function ($item) {
							$('#callback-output').prepend('movingToCenter: ' + $item.attr('id') + '<br/>');
							//alert('movingToCenter: ' + $item.attr('id'));
						},
						movedToCenter: function ($item) {
							$('#callback-output').prepend('movedToCenter: ' + $item.attr('id') + '<br/>');
							//alert('movedToCenter: ' + $item.attr('id'));
						},
						movingFromCenter: function ($item) {
							$('#callback-output').prepend('movingFromCenter: ' + $item.attr('id') + '<br/>');
						},
						movedFromCenter: function ($item) {
							$('#callback-output').prepend('movedFromCenter: ' + $item.attr('id') + '<br/>');
						},
						clickedCenter: function ($item) {
							$('#callback-output').prepend('clickedCenter: ' + $item.attr('id') + '<br/>');
							//alert('clickedCenter: ' + $item.attr('id'));
							widgetModel["selectedItem"]= Number($item.attr('id').match(/[0-9]{1,9}/g));
							gblCWSelectedItem = Number($item.attr('id').match(/[0-9]{1,9}/g));
							widgetModel["onSelect"].call();
						}
				});

				$('#prev').bind('click', function () {
					carousel.prev();
					return false;
				});

				$('#next').bind('click', function () {
					carousel.next();
					return false;
				});

				$('#reload').bind('click', function () {
					newOptions = eval("(" + $('#newoptions').val() + ")");
					carousel.reload(newOptions);
					return false;
				});
			});
    },
	
    modelChange : function(widgetModel, propertyChanged, propertyValue){}
}