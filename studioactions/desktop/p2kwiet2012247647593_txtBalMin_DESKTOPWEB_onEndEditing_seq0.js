function p2kwiet2012247647593_txtBalMin_DESKTOPWEB_onEndEditing_seq0(eventobject, changedtext) {
    if (frmIBSSApply.txtBalMin.text == "") {
        frmIBSSApply.txtBalMin.text = "0.00 " + kony.i18n.getLocalizedString("currencyThaiBaht");
    } else {
        frmIBSSApply.txtBalMin.text = commaFormatted(parseFloat(removeCommos(frmIBSSApply.txtBalMin.text)).toFixed(2)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
    }
}