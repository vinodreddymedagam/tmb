function p2kwiet2012247639197_frmIBBankAssuranceSummary_preshow_seq0(eventobject, neworientation) {
    frmIBBankAssuranceSummary.label449290336722095.text = kony.i18n.getLocalizedString("BA_Acc_Summary_Title");
    frmIBBankAssuranceSummary.lblnvestment.text = kony.i18n.getLocalizedString("BA_lbl_Total_Sum_Insured");
    frmIBBankAssuranceSummary.segAccountDetails.setData(updateLocaleDataSegBankAssurance(frmIBBankAssuranceSummary.segAccountDetails.data));
    // BA Details
    frmIBBankAssuranceSummary.BtnTaxDoc.text = kony.i18n.getLocalizedString("BA_Tax_doc");
    frmIBBankAssuranceSummary.btnPayBill.text = kony.i18n.getLocalizedString("PayBill");;
    frmIBBankAssuranceSummary.label449290336675636.text = kony.i18n.getLocalizedString("BA_lbl_Policy_details");
}