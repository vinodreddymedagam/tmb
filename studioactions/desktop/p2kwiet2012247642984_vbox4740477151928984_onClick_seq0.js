function p2kwiet2012247642984_vbox4740477151928984_onClick_seq0(eventobject) {
    if (frmIBePaymentExecutedTxn.lblBBPaymentValue.isVisible) {
        frmIBePaymentExecutedTxn.lblBBPaymentValue.setVisibility(false);
        frmIBePaymentExecutedTxn.lblHide.text = kony.i18n.getLocalizedString("keyUnhide");
    } else {
        frmIBePaymentExecutedTxn.lblBBPaymentValue.setVisibility(true);
        frmIBePaymentExecutedTxn.lblHide.text = kony.i18n.getLocalizedString("Hide");
    }
}