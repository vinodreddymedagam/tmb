function p2kwiet2012247642323_frmIBDebitCardActivateAssignPIN_postshow_seq0(eventobject, neworientation) {
    addIBMenu.call(this);
    if (kony.i18n.getCurrentLocale() == "th_TH") {
        frmIBDebitCardActivateAssignPIN.btnMenuMyAccountSummary.skin = "btnIBMenuMyAccountSummaryFocusThai";
    } else {
        frmIBDebitCardActivateAssignPIN.btnMenuMyAccountSummary.skin = "btnIBMenuMyAccountSummaryFocus";
    }
    if (maxLenPin == "6") {
        frmIBDebitCardActivateAssignPIN.lblEnterYourPIN.text = kony.i18n.getLocalizedString("keySet4DigitPin").replace("4", "6");
    } else {
        frmIBDebitCardActivateAssignPIN.lblEnterYourPIN.text = kony.i18n.getLocalizedString("keySet4DigitPin");
    }
}