function AS_Button_afbf780f817b4fa08f6cb7918fdf254a(eventobject) {
    function SHOW_ALERT_ide_onClick_acf0246a744246d4b8a16da9e9756a73_True() {}

    function SHOW_ALERT_ide_onClick_bcdf7eb9c7be4719a20077245316f95f_True() {}

    function SHOW_ALERT_ide_onClick_hd4e5439ff3949cebed2443185b4bc13_True() {}
    isFromLogin = true
    if ((!/^\s*$/.test(frmIBPreLogin.txtUserId.text))) {
        if ((frmIBPreLogin.txtPassword.text != "")) {
            if ((frmIBPreLogin.hboxCaptchaText.isVisible)) {
                if ((frmIBPreLogin.txtCaptchaText.text != "")) {
                    captchaValidation.call(this, frmIBPreLogin.txtCaptchaText.text);
                } else {
                    function SHOW_ALERT_ide_onClick_acf0246a744246d4b8a16da9e9756a73_Callback() {
                        SHOW_ALERT_ide_onClick_acf0246a744246d4b8a16da9e9756a73_True();
                    }
                    kony.ui.Alert({
                        "alertType": constants.ALERT_TYPE_ERROR,
                        "alertTitle": "Alert",
                        "yesLabel": "Yes",
                        "noLabel": "No",
                        "message": kony.i18n.getLocalizedString("keyCaptchaRequired"),
                        "alertHandler": SHOW_ALERT_ide_onClick_acf0246a744246d4b8a16da9e9756a73_Callback
                    }, {
                        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
                    });
                    frmIBPreLogin.txtCaptchaText.setFocus(true);
                }
            } else {
                IBLoginService.call(this);
            }
        } else {
            function SHOW_ALERT_ide_onClick_bcdf7eb9c7be4719a20077245316f95f_Callback() {
                SHOW_ALERT_ide_onClick_bcdf7eb9c7be4719a20077245316f95f_True();
            }
            kony.ui.Alert({
                "alertType": constants.ALERT_TYPE_ERROR,
                "alertTitle": "Alert",
                "yesLabel": "Yes",
                "noLabel": "No",
                "message": kony.i18n.getLocalizedString("keyPasswordRequired"),
                "alertHandler": SHOW_ALERT_ide_onClick_bcdf7eb9c7be4719a20077245316f95f_Callback
            }, {
                "iconPosition": constants.ALERT_ICON_POSITION_LEFT
            });
            frmIBPreLogin.txtPassword.setFocus(true);
        }
    } else {
        function SHOW_ALERT_ide_onClick_hd4e5439ff3949cebed2443185b4bc13_Callback() {
            SHOW_ALERT_ide_onClick_hd4e5439ff3949cebed2443185b4bc13_True();
        }
        kony.ui.Alert({
            "alertType": constants.ALERT_TYPE_ERROR,
            "alertTitle": "Alert",
            "yesLabel": "Yes",
            "noLabel": "No",
            "message": kony.i18n.getLocalizedString("keyLoginRequired"),
            "alertHandler": SHOW_ALERT_ide_onClick_hd4e5439ff3949cebed2443185b4bc13_Callback
        }, {
            "iconPosition": constants.ALERT_ICON_POSITION_LEFT
        });
        frmIBPreLogin.txtUserId.setFocus(true);
    }
}