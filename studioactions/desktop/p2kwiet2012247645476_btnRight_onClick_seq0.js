function p2kwiet2012247645476_btnRight_onClick_seq0(eventobject) {
    if (eventobject.skin == "btnIBNotHomeDelNormal" || eventobject.skin == "btnIBNotHomeDelFocus") {
        if (NotificationHomePageState == false) {
            populateNotificationsDeleteStateIB();
        } else {
            if (frmIBNotificationHome.segRequestHistory.selectedItems.length == 1) var msg = kony.i18n.getLocalizedString("keyNotSure") + " " + frmIBNotificationHome.segRequestHistory.selectedItems.length + " " + kony.i18n.getLocalizedString("keyMessages")
            else var msg = kony.i18n.getLocalizedString("keyNotSure") + " " + frmIBNotificationHome.segRequestHistory.selectedItems.length + " " + kony.i18n.getLocalizedString("keyMessages")
            popUpNotfnDeleteIB.lblNotfnMsg.text = msg;
            popUpNotfnDeleteIB.btnPopDeleteCancel.onClick = popupDeleteCancelIB;
            popUpNotfnDeleteIB.btnPopDeleteYEs.onClick = popupDeleteConfirmIB;
            popUpNotfnDeleteIB.show();
        }
    } else if (eventobject.skin == "btnDeleteActive") {
        //do nothing
    }
}