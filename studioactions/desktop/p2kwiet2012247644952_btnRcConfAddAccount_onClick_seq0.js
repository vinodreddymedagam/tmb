function p2kwiet2012247644952_btnRcConfAddAccount_onClick_seq0(eventobject) {
    if ((checkAccountCountBulkAddition() == MAX_BANK_ACCNT_BULK_ADD) || checkRcAccountLimitReached()) {
        return;
    } else {
        frmIBMyReceipentsAddBankAccnt.hboxAddAccntConf.setVisibility(false);
        frmIBMyReceipentsAddBankAccnt.hboxTMBSelectAccnt.setVisibility(true);
        clearDataOnRcBankAdditionForm();
    }
}