function p2kwiet2012247638530_frmIBActivateIBankingStep1_postshow_seq0(eventobject, neworientation) {
    frmIBActivateIBankingStep1.txtActivationCode.setFocus(true);
    curr_lang = kony.i18n.getCurrentLocale();
    if (curr_lang == "th_TH") {
        if (gblSetPwd) {
            frmIBActivateIBankingStep1.richtextActivationHelpText.text = GLOBAL_IB_REACTIVATION_HELP_TEXT_TH;
        } else {
            frmIBActivateIBankingStep1.richtextActivationHelpText.text = GLOBAL_IB_ACTIVATION_HELP_TEXT_TH;
        }
    } else {
        if (gblSetPwd) {
            frmIBActivateIBankingStep1.richtextActivationHelpText.text = GLOBAL_IB_REACTIVATION_HELP_TEXT_EN;
        } else {
            frmIBActivateIBankingStep1.richtextActivationHelpText.text = GLOBAL_IB_ACTIVATION_HELP_TEXT_EN;
        }
    }
    if (gblSetPwd) {
        frmIBActivateIBankingStep1.lblActivationCode.setVisibility(false);
        frmIBActivateIBankingStep1.hbxUserId.setVisibility(false);
    } else {
        frmIBActivateIBankingStep1.lblActivationCode.setVisibility(true);
        frmIBActivateIBankingStep1.hbxUserId.setVisibility(true);
    }
    frmIBActivateIBankingStep1.imgarrow1.setVisibility(false);
    frmIBActivateIBankingStep1.vbxRules.setVisibility(false);
    var isIE8 = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
    if (isIE8) {
        document.getElementById("frmIBActivateIBankingStep1_label507353026211739").style.paddingBottom = "1px";
    }
}