function p2kwiet2012247639687_frmIBBeepAndBillTandC_postshow_seq0(eventobject, neworientation) {
    registerForTimeOutIB.call(this);
    IBTnCTextBB.call(this);
    if (kony.i18n.getCurrentLocale() == "th_TH") frmIBBeepAndBillTandC.lblTnC.text = GLOBAL_TERMS_CONDITIONS_th_TH_1 + GLOBAL_TERMS_CONDITIONS_th_TH_2 + GLOBAL_TERMS_CONDITIONS_th_TH_3;
    else frmIBBeepAndBillTandC.lblTnC.text = GLOBAL_TERMS_CONDITIONS_en_US;
    gblEmailTCSelect = false;
}