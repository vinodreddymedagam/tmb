function p2kwiet2012247647022_txtPointsBeingRedeemed_DESKTOPWEB_onEndEditing_seq0(eventobject, changedtext) {
    checkRedeemPointsMoreAvailablePointsIB.call(this);
    frmIBPointRedemptionLanding.hbxPointsBeingRedeemed.skin = "hbxProper40px";
    var redeemPoints = frmIBPointRedemptionLanding.txtPointsBeingRedeemed.text;
    if (redeemPoints != "") {
        frmIBPointRedemptionLanding.txtPointsBeingRedeemed.text = commaFormattedPoints(Number(redeemPoints));
    }
}