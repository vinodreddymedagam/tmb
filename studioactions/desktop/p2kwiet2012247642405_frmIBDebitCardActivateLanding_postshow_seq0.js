function p2kwiet2012247642405_frmIBDebitCardActivateLanding_postshow_seq0(eventobject, neworientation) {
    addIBMenu.call(this);
    if (kony.i18n.getCurrentLocale() == "th_TH") frmIBDebitCardActivateLanding.btnMenuMyAccountSummary.skin = "btnIBMenuMyAccountSummaryFocusThai";
    else frmIBDebitCardActivateLanding.btnMenuMyAccountSummary.skin = "btnIBMenuMyAccountSummaryFocus";
    preOTPScreenForDebitCardActivate.call(this);
}