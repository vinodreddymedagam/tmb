function p2kwiet2012247640409_txtBPEditAmtValue_DESKTOPWEB_onEndEditing_seq0(eventobject, changedtext) {
    if (frmIBBillPaymentView.txtBPEditAmtValue.text == "") {
        frmIBBillPaymentView.txtBPEditAmtValue.text = "0.00 " + kony.i18n.getLocalizedString("currencyThaiBaht");
    } else {
        frmIBBillPaymentView.txtBPEditAmtValue.text = commaFormatted(parseFloat(removeCommos(frmIBBillPaymentView.txtBPEditAmtValue.text)).toFixed(2)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
    }
    frmIBBillPaymentView.hbxBPEditAmnt.skin = "hbox320pxpadding"
}