function p2kwiet2012247646427_btnNext_onClick_seq0(eventobject) {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    today = mm + '/' + dd + '/' + yyyy;
    frmIBOpenNewTermDepositAccConfirmation.lblOpeningDateVal.text = today;
    gblRetryCountRequestOTP = "0";
    gblRetryCountRequestOTP = "0";
    if (gblisDisToActTD == "N") {
        IBshowTDConfirmation();
    } else {
        if (frmIBOpenNewTermDepositAcc.lblToAcc.text != "" && frmIBOpenNewTermDepositAcc.lblToAcc.text != null) {
            IBshowTDConfirmation();
        } else {
            alert("Please Select an account for ToAccount")
        }
    }
}