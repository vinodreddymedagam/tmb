function p2kwiet2012247648709_frmIBTopUpConfirmation_preshow_seq0(eventobject, neworientation) {
    frmIBTopUpConfirmation.hbxNext.isVisible = true;
    frmIBTopUpConfirmation.hbxButn.isVisible = false;
    frmIBTopUpConfirmation.hbxOtpBox.isVisible = false;
    frmIBTopUpConfirmation.hbxOTPsnt.setVisibility(false);
    frmIBTopUpConfirmation.hbxTokenEntry.setVisibility(false);
    frmIBTopUpConfirmation.txtOtp.text = "";
    frmIBTopUpConfirmation.lblOTPinCurr.text = "";
    frmIBTopUpConfirmation.lblPlsReEnter.text = "";
}