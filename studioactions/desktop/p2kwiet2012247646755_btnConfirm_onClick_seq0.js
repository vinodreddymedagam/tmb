function p2kwiet2012247646755_btnConfirm_onClick_seq0(eventobject) {
    if ((frmIBOpenProdMailAddressEdit.hbxOtpBox.isVisible == true)) {
        validateOTPIBprofile.call(this);
    } else {
        if ((frmIBOpenProdMailAddressEdit.hbxcnf2.isVisible == true)) {
            onMyProfileAddressNextIB.call(this);
        } else {
            checkEditAddressChanged.call(this);
        }
    }
}