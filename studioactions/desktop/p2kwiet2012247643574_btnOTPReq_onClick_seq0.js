function p2kwiet2012247643574_btnOTPReq_onClick_seq0(eventobject) {
    if (frmIBFTrnsrEditCnfmtn.btnOTPReq.text == kony.i18n.getLocalizedString("keyRequest")) {
        reqOTPFT();
        //gblSwitchToken = true;
        gblTokenSwitchFlag = false;
    } else if (frmIBFTrnsrEditCnfmtn.btnOTPReq.text == kony.i18n.getLocalizedString("keySwitchToSMS")) {
        frmIBFTrnsrEditCnfmtn.lblOTP.text = kony.i18n.getLocalizedString("keyOTP");
        frmIBFTrnsrEditCnfmtn.lblMsgRequset.text = kony.i18n.getLocalizedString("keyotpmsgreq");
        frmIBFTrnsrEditCnfmtn.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
        gblSwitchToken = true;
        gblTokenSwitchFlag = false;
        reqOTPFT();
    }
}