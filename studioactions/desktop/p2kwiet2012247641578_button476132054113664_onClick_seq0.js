function p2kwiet2012247641578_button476132054113664_onClick_seq0(eventobject) {
    editbuttonflag = "profile";
    editConfirm = false;
    if (profileAddrFlagIB == "addr" || profileEmailFlag == true) {
        confirmEdit = true;
        selectState = frmIBCMEditMyProfile.btnProvince.selectedKeyValue[1];
        selectDist = frmIBCMEditMyProfile.btnDist.selectedKeyValue[1];
        selectSubDist = frmIBCMEditMyProfile.btnSubDist.selectedKeyValue[1];
        selectZip = frmIBCMEditMyProfile.btnPostalCode.selectedKeyValue[1];
    }
    frmIBCMEditMyProfile.hbxcnf1.setVisibility(false);
    frmIBCMEditMyProfile.hbxcnf2.setVisibility(false);
    frmIBCMEditMyProfile.hboxEdit.setVisibility(true);
    getIBEditProfileStatus.call(this);
    IBCMCommonMyProfilePreShow.call(this);
}