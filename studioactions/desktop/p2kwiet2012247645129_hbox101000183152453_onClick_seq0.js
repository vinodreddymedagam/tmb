function p2kwiet2012247645129_hbox101000183152453_onClick_seq0(eventobject) {
    frmIBMyReceipentsAddBankAccnt.hboxTMBSelectAccnt.isVisible = true;
    frmIBMyReceipentsAddBankAccnt.hboxAddAccntConf.isVisible = false;
    if (gblAdd_Receipent_State != gblEXISTING_RC_EDITION) {
        gblAdd_Receipent_State = gblNEW_RC_ADDITION_PRECONFIRM;
    }
    if (checkAccountCountBulkAddition() == MAX_BANK_ACCNT_BULK_ADD) {
        return;
    } else {
        if (checkAccountCountBulkAddition() == 1 && recipientAddFromTransfer) {
            return;
        }
        clearDataOnRcBankAdditionForm();
        frmIBMyReceipentsAddBankAccnt.show();
    }
}