function p2kwiet2012247641918_btnConfirm_onClick_seq0(eventobject) {
    function alert_onClick_99847201224768817_True() {}

    function alert_onClick_92526201224767475_True() {}

    function alert_onClick_51207201224761325_True() {}
    if ((gblSetPwd == false && frmIBCreateUserID.txtUserID.text == "")) {
        function alert_onClick_51207201224761325_Callback() {
            alert_onClick_51207201224761325_True();
        }
        kony.ui.Alert({
            "alertType": constants.ALERT_TYPE_ERROR,
            "alertTitle": kony.i18n.getLocalizedString("keyErrMsg"),
            "yesLabel": kony.i18n.getLocalizedString("keyYes"),
            "noLabel": "No",
            "message": kony.i18n.getLocalizedString("keyEnterUserId"),
            "alertHandler": alert_onClick_51207201224761325_Callback
        }, {
            "iconPosition": constants.ALERT_ICON_POSITION_LEFT
        });
        frmIBCreateUserID.txtUserID.setFocus(true)
    } else {
        if ((frmIBCreateUserID.txtPassword.text == "")) {
            function alert_onClick_92526201224767475_Callback() {
                alert_onClick_92526201224767475_True();
            }
            kony.ui.Alert({
                "alertType": constants.ALERT_TYPE_ERROR,
                "alertTitle": kony.i18n.getLocalizedString("keyErrMsg"),
                "yesLabel": kony.i18n.getLocalizedString("keyYes"),
                "noLabel": "No",
                "message": kony.i18n.getLocalizedString("keyEnterPassword"),
                "alertHandler": alert_onClick_92526201224767475_Callback
            }, {
                "iconPosition": constants.ALERT_ICON_POSITION_LEFT
            });
            frmIBCreateUserID.txtPassword.setFocus(true)
        } else {
            if ((frmIBCreateUserID.txtConfirmPassword.text == "")) {
                function alert_onClick_99847201224768817_Callback() {
                    alert_onClick_99847201224768817_True();
                }
                kony.ui.Alert({
                    "alertType": constants.ALERT_TYPE_ERROR,
                    "alertTitle": kony.i18n.getLocalizedString("keyErrMsg"),
                    "yesLabel": kony.i18n.getLocalizedString("keyYes"),
                    "noLabel": "No",
                    "message": kony.i18n.getLocalizedString("keyEnterConfirmPassword"),
                    "alertHandler": alert_onClick_99847201224768817_Callback
                }, {
                    "iconPosition": constants.ALERT_ICON_POSITION_LEFT
                });
                frmIBCreateUserID.txtConfirmPassword.setFocus(true)
            } else {
                ibUseridPasswordValidation.call(this);
            }
        }
    }
}