function p2kwiet2012247639415_btnConfirm_onClick_seq0(eventobject) {
    if ((frmIBBeepAndBillConfAndComp.btnConfirm.text == kony.i18n.getLocalizedString("keyConfirm"))) {
        if ((frmIBBeepAndBillConfAndComp.hbxBillersOTPContainer.isVisible == true || frmIBBeepAndBillConfAndComp.hbxToken.isVisible == true)) {
            verifyOTPBBComposite.call(this);
        } else {
            srvTokenSwitchingBB.call(this);
        }
    } else {
        isToAccSelected = false;
        getMyBillListIBBB()
            //frmIBBeepAndBillApply.show();
        frmIBBeepAndBillApply.imgAddBillerLogo.src = ""
        frmIBBeepAndBillApply.lblAddBillerCompCode.text = ""
        frmIBBeepAndBillApply.txtAddBillerNickName.text = "";
        frmIBBeepAndBillApply.txtAddBillerRef1.text = "";
        frmIBBeepAndBillApply.txtAddBillerRef2.text = "";
        frmIBBeepAndBillApply.tbxSearch.text = "";
    }
}