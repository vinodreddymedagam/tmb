function p2kwiet2012247646427_hbxPayingIntTo_onClick_seq0(eventobject) {
    if ((frmIBOpenNewTermDepositAcc.lblFromAccount.text != "" && frmIBOpenNewTermDepositAcc.lblFromAccount.text != null)) {
        frmIBOpenNewTermDepositAcc.vbxPayingTo.skin = "vboxLightBlue320px"
        frmIBOpenNewTermDepositAcc.vbxOpenAmount.skin = "NoSkin";
        frmIBOpenNewTermDepositAcc.imgPayInterest.setVisibility(true);
        frmIBOpenNewTermDepositAcc.imgOpenAmount.setVisibility(false);
        gblOnClickCoverflow = "";
        setDataforTermDepositPayingAccts.call(this, gblOpenActList);
    } else {
        alert("Please select from account");
    }
}