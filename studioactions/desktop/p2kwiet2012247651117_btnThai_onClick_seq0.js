function p2kwiet2012247651117_btnThai_onClick_seq0(eventobject) {
    langchange.call(this, "th_TH");
    hbxIBPreLogin.btnEng.skin = "btnEngLangOff";
    hbxIBPreLogin.btnThai.skin = "btnThaiLangOn";
    hbxIBPostLogin.btnEng.skin = "btnEngLangOff";
    hbxIBPostLogin.btnThai.skin = "btnThaiLangOn";
    if (!kony.appinit.isIE8) {
        frmIBPreLogin.txtUserId.placeholder = kony.i18n.getLocalizedString("keyUserIDPreLogin");
        frmIBPreLogin.txtPassword.placeholder = kony.i18n.getLocalizedString("keyPasswordPlaceHolder");
    }
}