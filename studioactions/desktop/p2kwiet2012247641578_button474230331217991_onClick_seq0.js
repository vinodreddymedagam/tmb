function p2kwiet2012247641578_button474230331217991_onClick_seq0(eventobject) {
    frmIBCMEditMyProfile.txtAddress1.setEnabled(true);
    frmIBCMEditMyProfile.txtAddress2.setEnabled(true);
    frmIBCMEditMyProfile.btnProvince.setEnabled(true);
    frmIBCMEditMyProfile.btnDist.setEnabled(true);
    frmIBCMEditMyProfile.btnSubDist.setEnabled(true);
    frmIBCMEditMyProfile.btnPostalCode.setEnabled(true);
    frmIBCMEditMyProfile.btnfbLogin.setEnabled(true);
    frmIBCMEditMyProfile.txtemailvalue.setEnabled(true);
    TMBUtil.DestroyForm(frmIBCMEditMyProfile);
    profilePicFlag = "";
    profilePicFlagIB = false;
    profileAddrFlagIB = "";
    profileEmailFlag = "";
    completeicon = false;
    editConfirm = false;
    frmIBCMEditMyProfile.hbxOtpBox.setVisibility(false);
    stateFlag = false;
    EditConf();
    frmIBCMMyProfile.lblRequestHeader.setVisibility(false);
    frmIBCMMyProfile.hbxListBox.setVisibility(false);
    frmIBCMMyProfile.hbxData.setVisibility(false);
    frmIBCMMyProfile.hbximage.setVisibility(true);
    frmIBCMMyProfile.lnkHeader.setVisibility(false);
    frmIBCMMyProfile.show();
}