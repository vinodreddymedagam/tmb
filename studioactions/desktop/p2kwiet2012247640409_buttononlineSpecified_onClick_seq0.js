function p2kwiet2012247640409_buttononlineSpecified_onClick_seq0(eventobject) {
    frmIBBillPaymentView.textboxOnlineTotalAmt.setEnabled(true);
    if (gblEditBillMethod == 3) {
        frmIBBillPaymentView.textboxOnlineTotalAmt.text = frmIBBillPaymentView.lblBPAmtValue.text;
    } else {
        frmIBBillPaymentView.textboxOnlineTotalAmt.text = commaFormatted(gblAmtFromService) + " " + kony.i18n.getLocalizedString("currencyThaiBaht"); //gblAmtFromService;
    }
    frmIBBillPaymentView.buttononlineSpecified.skin = "btnIBrndRightBlueNoWidth";
    frmIBBillPaymentView.buttononlineFull.skin = "btnIBrndleftGreyNoWidth";
}