function p2kwiet2012247650520_btnEndOnDate_onClick_seq0(eventobject) {
    futuredate.calOnDate.setVisibility(true);
    futuredate.txtAfterTimes.setVisibility(false);
    futuredate.lblAfterTimesInstruction.setVisibility(false);
    futuredate.btnEndAfter.skin = "btnNormal"
    futuredate.btnEndOnDate.skin = "btnFocusBlue"
    futuredate.btnEndNever.skin = "btnNormal"
}