function btn_I_Got_It_Action(eventobject) {
    return AS_Button_c60bad179281426da0c70561b255a42a(eventobject);
}

function AS_Button_c60bad179281426da0c70561b255a42a(eventobject) {
    gblMFPortfolioType = "PT";
    gblUnitHolderNoSelected = "";
    frmIBMutualFundsPortfolio.btnMFFundPortflio.skin = "skinBtnBlueLeftRoundCorner";
    frmIBMutualFundsPortfolio.btnMFAutoSmartPortfolio.skin = "skinBtnWhiteRightRoundCorner";
    frmIBMutualFundsPortfolio.hBoxAutosmartPortfolio.setVisibility(false);
    frmIBMutualFundsPortfolio.hBoxNormalPortfolio.setVisibility(true);
    callMutualFundsSummary();
}