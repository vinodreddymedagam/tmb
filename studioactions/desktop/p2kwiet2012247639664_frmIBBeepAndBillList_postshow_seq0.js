function p2kwiet2012247639664_frmIBBeepAndBillList_postshow_seq0(eventobject, neworientation) {
    addIBMenu.call(this);
    frmIBBeepAndBillList.lblSuggestedBiller.setVisibility(true)
    if (kony.i18n.getCurrentLocale() == "en_US") {
        frmIBBeepAndBillList.cbxSelectCat.masterData = gblBillerCategoriesBBIB;
    } else {
        frmIBBeepAndBillList.cbxSelectCat.masterData = gblBillerCategoriesBBIBTH;
    }
    populatefrmBBSelect(gblmasterBillerAndTopupBB);
    segConvenientServicesLoad.call(this);
    pagespecificSubMenu.call(this);
    frmIBBeepAndBillList.hboxScreenLevel2.setVisibility(false)
    frmIBBeepAndBillList.hbxImage.setVisibility(true)
    frmIBBeepAndBillList.tbxSearch.text = "";
}