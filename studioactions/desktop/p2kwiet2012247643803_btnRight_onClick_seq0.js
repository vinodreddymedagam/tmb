function p2kwiet2012247643803_btnRight_onClick_seq0(eventobject) {
    if (eventobject.skin == "btnIBNotHomeDelNormal" || eventobject.skin == "btnIBNotHomeDelFocus") {
        if (InboxHomePageState == false) {
            populateInboxDeleteStateIB();
        } else {
            //deleteNotificationMB();
            var msg = kony.i18n.getLocalizedString("keyIBNotificationDeletePart1") + " " + frmIBInboxHome.segRequestHistory.selectedItems.length + " " + kony.i18n.getLocalizedString("keyIBNotificationDeletePart2");
            popUpNotfnDeleteIB.lblNotfnMsg.text = msg;
            popUpNotfnDeleteIB.btnPopDeleteCancel.onClick = popupDeleteCancelInboxIB;
            popUpNotfnDeleteIB.btnPopDeleteYEs.onClick = popupDeleteConfirmInboxIB;
            popUpNotfnDeleteIB.show();
        }
    } else if (eventobject.skin == "btnDeleteActive") {
        //do nothing
    }
}