function p2kwiet2012247640468_frmIBChequeServiceConfirmation_preshow_seq0(eventobject, neworientation) {
    frmIBChequeServiceConfirmation.lblOTPinCurr.text = "";
    frmIBChequeServiceConfirmation.lblPlsReEnter.text = "";
    frmIBChequeServiceConfirmation.image247502979411375.src = glb_selectedData.img1;
    frmIBChequeServiceConfirmation.txtotp.text = ""
    gblChequeConfirm = "true"
    frmIBChequeServiceConfirmation.btnGoto.setVisibility(false);
    frmIBChequeServiceConfirmation.hbxBtn.setVisibility(true);
    frmIBChequeServiceConfirmation.hbxOtpBox.setVisibility(true);
    frmIBChequeServiceConfirmation.hbxSuccess.setVisibility(false);
    frmIBChequeServiceConfirmation.lblSuccess.setVisibility(false);
    frmIBChequeServiceConfirmation.lblBillersConfirm.text = kony.i18n.getLocalizedString("Confirmation");
}