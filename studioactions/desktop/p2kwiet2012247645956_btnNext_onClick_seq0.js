function p2kwiet2012247645956_btnNext_onClick_seq0(eventobject) {
    var amount = frmIBOpenNewSavingsAcc.txtOpenActSelAmt.text
    amount = amount.replace(/\s/g, '');
    frmIBOpenNewSavingsAcc.txtOpenActSelAmt.text = amount;
    gblRetryCountRequestOTP = "0";
    if ((gblOnClickCoverflow == true)) {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        today = mm + '/' + dd + '/' + yyyy;
        frmIBOpenNewSavingsAccConfirmation.lblOpeningDateVal.text = today;
        IBonClickNextselAct.call(this);
    } else {
        alert("Please Select an Account")
    }
}