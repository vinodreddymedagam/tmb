function p2kwiet2012247640409_btnBillpayDaily_onClick_seq0(eventobject) {
    frmIBBillPaymentView.lblBPEnding.setVisibility(true);
    frmIBBillPaymentView.hbxBPEndingBtns.setVisibility(true);
    frmIBBillPaymentView.lineSchBPClose.setVisibility(true);
    repeatScheduleFrequency.call(this, eventobject);
}