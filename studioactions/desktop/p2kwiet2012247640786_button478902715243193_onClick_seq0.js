function p2kwiet2012247640786_button478902715243193_onClick_seq0(eventobject) {
    if (frmIBCMChgMobNoTxnLimit.hbxChangeMobileNumber.isVisible) {
        IBNewMobNoOTPRequestService1();
    } else {
        IBTransLimitOTPRequestService();
    }
    frmIBCMChgMobNoTxnLimit.hbxTokenEntry.setVisibility(false);
    frmIBCMChgMobNoTxnLimit.hbxOTPEntry.setVisibility(true);
    frmIBCMChgMobNoTxnLimit.txtBxOTP.setFocus(true);
    frmIBCMChgMobNoTxnLimit.lblkeyOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
    frmIBCMChgMobNoTxnLimit.txtBxOTP.setFocus(true);
    gblTokenSwitchFlag = false;
    gblSwitchToken = true;
}