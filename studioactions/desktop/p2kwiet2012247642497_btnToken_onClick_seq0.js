function p2kwiet2012247642497_btnToken_onClick_seq0(eventobject) {
    frmIBDreamSAvingEdit.hbxToken.setVisibility(false);
    frmIBDreamSAvingEdit.hbxOTPEntry.setVisibility(true);
    frmIBDreamSAvingEdit.hbxOTPBankRef.isVisible = true;
    frmIBDreamSAvingEdit.hbxOTPsnt.isVisible = true;
    frmIBDreamSAvingEdit.lblOTPMsgReq.isVisible = true;
    frmIBDreamSAvingEdit.lblOTPMsgReq.text = kony.i18n.getLocalizedString("keyotpmsgreq");
    gblTokenSwitchFlag = false;
    gblSwitchToken = true;
    callOTPServiceDSMIB();
    frmIBDreamSAvingEdit.txtBxOTP.setFocus(true);
}