function p2kwiet2012247648709_btnSwitch2OTP_onClick_seq0(eventobject) {
    frmIBTopUpConfirmation.hbxTokenEntry.setVisibility(false);
    frmIBTopUpConfirmation.hbxOTPEntry.setVisibility(true);
    frmIBTopUpConfirmation.txtOtp.setFocus(true);
    gblTokenSwitchFlag = false;
    gblSwitchToken = true;
    frmIBTopUpConfirmation.label476047582115288.text = kony.i18n.getLocalizedString("keyotpmsgreq");
    startTopUpPaymentOTPRequestService.call(this);
}