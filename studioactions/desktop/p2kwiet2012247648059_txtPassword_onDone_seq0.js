function p2kwiet2012247648059_txtPassword_onDone_seq0(eventobject, changedtext) {
    function alert_onDone_45234201224768334_True() {}

    function alert_onDone_17956201224766841_True() {}

    function alert_onDone_9064201224766664_True() {}
    if ((frmIBPreLogin.txtUserId.text != "")) {
        if ((frmIBPreLogin.txtPassword.text != "")) {
            if ((frmIBPreLogin.hboxCaptchaText.isVisible)) {
                if ((frmIBPreLogin.txtCaptchaText.text != "")) {
                    captchaValidation.call(this, frmIBPreLogin.txtCaptchaText.text);
                } else {
                    function alert_onDone_45234201224768334_Callback() {
                        alert_onDone_45234201224768334_True();
                    }
                    kony.ui.Alert({
                        "alertType": constants.ALERT_TYPE_ERROR,
                        "alertTitle": "Alert",
                        "yesLabel": "Yes",
                        "noLabel": "No",
                        "message": "Field is mandatory",
                        "alertHandler": alert_onDone_45234201224768334_Callback
                    }, {
                        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
                    });
                    frmIBPreLogin.txtCaptchaText.setFocus(true);
                }
            } else {
                IBLoginService.call(this);
            }
        } else {
            function alert_onDone_17956201224766841_Callback() {
                alert_onDone_17956201224766841_True();
            }
            kony.ui.Alert({
                "alertType": constants.ALERT_TYPE_ERROR,
                "alertTitle": "Alert",
                "yesLabel": "Yes",
                "noLabel": "No",
                "message": "Field is mandatory",
                "alertHandler": alert_onDone_17956201224766841_Callback
            }, {
                "iconPosition": constants.ALERT_ICON_POSITION_LEFT
            });
            frmIBPreLogin.txtPassword.setFocus(true);
        }
    } else {
        function alert_onDone_9064201224766664_Callback() {
            alert_onDone_9064201224766664_True();
        }
        kony.ui.Alert({
            "alertType": constants.ALERT_TYPE_ERROR,
            "alertTitle": "Alert",
            "yesLabel": "Yes",
            "noLabel": "No",
            "message": "Field is mandatory",
            "alertHandler": alert_onDone_9064201224766664_Callback
        }, {
            "iconPosition": constants.ALERT_ICON_POSITION_LEFT
        });
        frmIBPreLogin.txtUserId.setFocus(true);
    }
}