function p2kwiet2012247649128_frmIBTopUpViewNEdit_postshow_seq0(eventobject, neworientation) {
    addIBMenu.call(this);
    frmIBTopUpViewNEdit.imgTMB.setVisibility(true);
    frmIBTopUpViewNEdit.comboboxOnlineAmt.setVisibility(false);
    //ticket 28094
    try {
        document.getElementById("calendarStartOn").setAttribute('readonly', "readonly");
        document.getElementById("calendarEndOn").setAttribute('readonly', "readonly");
    } catch (e) {}
    addNumberCheckListner.call(this, "txtTPEditAmtValue");
    pagespecificSubMenu.call(this);
}