function p2kwiet2012247645428_btnAddBillerSearch_onClick_seq0(eventobject) {
    frmIBMyTopUpsHome.segBillersList.setVisibility(false);
    frmIBMyTopUpsHome.lblMyBills.setVisibility(false);
    frmIBMyTopUpsHome.hbxMore.setVisibility(false);
    frmIBMyTopUpsHome.segSuggestedBillersList.removeAll();
    frmIBMyTopUpsHome.hbxSugBillersContainer.setVisibility(true);
    isSelectTopupEnabled = true;
    gblTopUpMore = 0;
    gblCustTopUpMore = 0;
    getMyTopUpSuggestListIB.call(this);
}