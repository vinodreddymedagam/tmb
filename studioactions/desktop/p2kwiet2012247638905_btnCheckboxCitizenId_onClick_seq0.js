function p2kwiet2012247638905_btnCheckboxCitizenId_onClick_seq0(eventobject) {
    if ((gblCIAnyIDRegisterAllowed == true)) {
        if ((frmIBAnyIDRegistration.btnCheckboxCitizenId.skin == "btnCheckFocTransparent")) {
            unRegisterAnyIDCitizenIdIB.call(this);
        } else {
            clickFromCitizenAnyID = true;
            onClickAccountForAnyIDRegistration.call(this);
        }
    } else {
        popupIBAnyIDHelpMessage.lblAnyIDHelpText.text = kony.i18n.getLocalizedString("MIB_P2PErr_LinkOtherCitizenID");
        popupIBAnyIDHelpMessage.show();
    }
}