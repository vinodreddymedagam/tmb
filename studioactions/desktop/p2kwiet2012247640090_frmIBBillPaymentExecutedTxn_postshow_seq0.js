function p2kwiet2012247640090_frmIBBillPaymentExecutedTxn_postshow_seq0(eventobject, neworientation) {
    addIBMenu.call(this);
    if (kony.i18n.getCurrentLocale() == "th_TH") {
        frmIBBillPaymentExecutedTxn.btnMenuMyActivities.skin = "btnIBMenuMyActivitiesFocusThai";
    } else {
        frmIBBillPaymentExecutedTxn.btnMenuMyActivities.skin = "btnIBMenuMyActivitiesFocus";
    }
}