function p2kwiet2012247641211_button478902715246365_onClick_seq0(eventobject) {
    frmIBCMConfirmation.label476047582115277.setVisibility(true);
    frmIBCMConfirmation.lblPhoneNo.setVisibility(true);
    frmIBCMConfirmation.lblBankRef.setVisibility(true);
    frmIBCMConfirmation.hbxTokenEntry.setVisibility(false);
    frmIBCMConfirmation.hbxOTPEntry.setVisibility(true);
    frmIBCMConfirmation.label476047582115288.text = kony.i18n.getLocalizedString("keyotpmsgreq");
    frmIBCMConfirmation.txtOTP.setFocus(true);
    requestOTPForChngUserId();
    gblTokenSwitchFlag = false;
    gblSwitchToken = true;
}