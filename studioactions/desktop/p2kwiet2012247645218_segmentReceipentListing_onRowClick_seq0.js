function p2kwiet2012247645218_segmentReceipentListing_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    //Store the selected Rc id
    gblselectedRcId = frmIBMyReceipentsEditAccountConf.segmentReceipentListing.selectedItems[0]["receipentID"];
    if (checkActualRcSegItemIndex()) {
        //Index done in method
    } else {
        globalSeletedRcIndex = frmIBMyReceipentsEditAccountConf.segmentReceipentListing.selectedIndex[1];
    }
    showLoadingScreenPopup();
    frmIBMyReceipentsAccounts.show();
    frmIBMyReceipentsAccounts.hboxMyReceipents.setVisibility(true);
    frmIBMyReceipentsAccounts.hboxReceipentAccount.setVisibility(false);
    //frmIBMyReceipentsAccounts.hboxTMBWelcome.setVisibility(false);
    //Code to set selected item name
    if (globalRcData[0][globalSeletedRcIndex].lblReceipentName == null) {
        frmIBMyReceipentsAccounts.lblRcName.text = "";
    } else {
        frmIBMyReceipentsAccounts.lblRcName.text = globalRcData[0][globalSeletedRcIndex].lblReceipentName;
    }
    if (frmIBMyReceipentsEditAccountConf.segmentReceipentListing.selectedItems[0]["mobileNumber"] == null || frmIBMyReceipentsEditAccountConf.segmentReceipentListing.selectedItems[0]["mobileNumber"] == "No Number") {
        frmIBMyReceipentsAccounts.lblRcMobileNo.text = "";
    } else {
        frmIBMyReceipentsAccounts.lblRcMobileNo.text = frmIBMyReceipentsEditAccountConf.segmentReceipentListing.selectedItems[0]["mobileNumber"];
    }
    if (frmIBMyReceipentsEditAccountConf.segmentReceipentListing.selectedItems[0]["emailId"] == null || frmIBMyReceipentsEditAccountConf.segmentReceipentListing.selectedItems[0]["emailId"] == "No Mail") {
        frmIBMyReceipentsAccounts.lblRcEmail.text = "";
    } else {
        frmIBMyReceipentsAccounts.lblRcEmail.text = frmIBMyReceipentsEditAccountConf.segmentReceipentListing.selectedItems[0]["emailId"];
    }
    if (frmIBMyReceipentsEditAccountConf.segmentReceipentListing.selectedItems[0]["facebookId"] == null) {
        frmIBMyReceipentsAccounts.lblRcFbId.text = "";
    } else {
        frmIBMyReceipentsAccounts.lblRcFbId.text = frmIBMyReceipentsEditAccountConf.segmentReceipentListing.selectedItems[0]["facebookId"];
    }
    if (frmIBMyReceipentsEditAccountConf.segmentReceipentListing.selectedItems[0]["imgReceipentPic"] == null) {
        frmIBMyReceipentsAccounts.imgReceipentProfile.src = "nouserimg.jpg";
    } else {
        frmIBMyReceipentsAccounts.imgReceipentProfile.src = frmIBMyReceipentsEditAccountConf.segmentReceipentListing.selectedItems[0]["imgReceipentPic"];
    }
}