function p2kwiet2012247647721_btnCancelOTP_onClick_seq0(eventobject) {
    frmIBSSApplyCnfrmtn.hbxDecisionOTP.setVisibility(false);
    frmIBSSApplyCnfrmtn.hbxOTP.setVisibility(false);
    frmIBSSApplyCnfrmtn.hbxCancelConf.setVisibility(true);
    frmIBSSApplyCnfrmtn.hbxToken.isVisible = false;
    frmIBSSApplyCnfrmtn.keyEnterToken.setVisibility(false);
}