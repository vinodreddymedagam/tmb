function p2kwiet2012247638227_frmIBAccntFullStatement_postshow_seq0(eventobject, neworientation) {
    addIBMenu.call(this);
    frmIBAccntFullStatement.enabledForIdleTimeout = true;
    if (kony.i18n.getCurrentLocale() == "th_TH") {
        frmIBAccntFullStatement.btnMenuMyAccountSummary.skin = "btnIBMenuMyAccountSummaryFocusThai";
    } else {
        frmIBAccntFullStatement.btnMenuMyAccountSummary.skin = "btnIBMenuMyAccountSummaryFocus";
    }
    viewAccntFullStatementIB.call(this);
}