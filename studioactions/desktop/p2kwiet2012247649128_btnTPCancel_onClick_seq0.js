function p2kwiet2012247649128_btnTPCancel_onClick_seq0(eventobject) {
    frmIBTopUpViewNEdit.imgTMB.setVisibility(true);
    frmIBTopUpViewNEdit.hboxExecute.skin = "hboxLightGrey320px"
    frmIBTopUpViewNEdit.hboxStartOn.skin = "hbox320pxpadding"
    frmIBTopUpViewNEdit.hboxRepeatAs.skin = "hboxLightGrey320px"
    frmIBTopUpViewNEdit.hboxEndOn.skin = "hbox320pxpadding"
    frmIBTopUpViewNEdit.lblTPStartOnDate.padding = [0, 0, 0, 0]
    frmIBTopUpViewNEdit.lblTPStartOnDateVal.padding = [0, 0, 0, 0]
    frmIBTopUpViewNEdit.lblTPRepeatAs.padding = [0, 0, 0, 0]
    frmIBTopUpViewNEdit.lblTPRepeatAsVal.padding = [0, 0, 0, 0]
    frmIBTopUpViewNEdit.lblTPEndOnDate.padding = [0, 0, 0, 0]
    frmIBTopUpViewNEdit.lblEndOnDateVal.padding = [0, 0, 0, 0]
    frmIBTopUpViewNEdit.lblTPExcute.padding = [0, 0, 0, 0]
    frmIBTopUpViewNEdit.lblTPExcuteVal.padding = [0, 0, 0, 0]
    cancelEditTPonEditPage.call(this);
}