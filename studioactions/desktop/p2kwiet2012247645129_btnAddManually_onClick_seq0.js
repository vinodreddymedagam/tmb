function p2kwiet2012247645129_btnAddManually_onClick_seq0(eventobject) {
    gblAdd_Receipent_State = gblNEW_RC_ADDITION;
    clearDataOnRcNewRecipientAdditionForm();
    frmIBMyReceipentsAddContactManually.hboxMyRecipient.setVisibility(true);
    frmIBMyReceipentsAddContactManually.hboxMyRecipientDetail.setVisibility(false);
    gblAdd_Receipent_State = gblNEW_RC_ADDITION;
    if (gblAdd_Receipent_State == gblEXISTING_RC_EDITION) {
        frmIBMyReceipentsAddContactManually.link50273743118078.setVisibility(false);
        frmIBMyReceipentsAddContactManually.link50273743118087.setVisibility(false);
        frmIBMyReceipentsAddContactManually.hbxOtpBox.setVisibility(true);
        frmIBMyReceipentsAddContactManually.label10107168132952.setVisibility(false);
        frmIBMyReceipentsAddContactManually.segmentRcAccounts.setVisibility(false);
    } else {
        //Clear existing data
        frmIBMyReceipentsAddContactManually.txtRecipientName.text = "";
        frmIBMyReceipentsAddContactManually.txtmobnum.text = "";
        frmIBMyReceipentsAddContactManually.txtemail.text = "";
        frmIBMyReceipentsAddContactManually.link50273743118078.setVisibility(true);
        frmIBMyReceipentsAddContactManually.link50273743118087.setVisibility(true);
        frmIBMyReceipentsAddContactManually.hbxOtpBox.setVisibility(false);
        frmIBMyReceipentsAddContactManually.label10107168132952.setVisibility(true);
        frmIBMyReceipentsAddContactManually.segmentRcAccounts.setVisibility(true);
    }
}