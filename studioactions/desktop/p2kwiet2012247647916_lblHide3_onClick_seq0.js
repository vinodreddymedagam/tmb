function p2kwiet2012247647916_lblHide3_onClick_seq0(eventobject) {
    if (frmIBSSSExcuteTrnsfrCmplete.lblBal2.isVisible) {
        frmIBSSSExcuteTrnsfrCmplete.lblBal2.isVisible = false;
        frmIBSSSExcuteTrnsfrCmplete.lblHide3.text = kony.i18n.getLocalizedString("keyUnhide");
    } else {
        frmIBSSSExcuteTrnsfrCmplete.lblBal2.isVisible = true;
        frmIBSSSExcuteTrnsfrCmplete.lblHide3.text = kony.i18n.getLocalizedString("Hide");
    }
}