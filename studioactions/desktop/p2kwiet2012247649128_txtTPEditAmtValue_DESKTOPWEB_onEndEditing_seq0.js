function p2kwiet2012247649128_txtTPEditAmtValue_DESKTOPWEB_onEndEditing_seq0(eventobject, changedtext) {
    if (frmIBTopUpViewNEdit.txtTPEditAmtValue.text == "") {
        frmIBTopUpViewNEdit.txtTPEditAmtValue.text = "0.00 " + kony.i18n.getLocalizedString("currencyThaiBaht");
    } else {
        frmIBTopUpViewNEdit.txtTPEditAmtValue.text = commaFormatted(parseFloat(removeCommos(frmIBTopUpViewNEdit.txtTPEditAmtValue.text)).toFixed(2)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
    }
    frmIBTopUpViewNEdit.hbxTPEditAmnt.skin = "hbox320pxpadding"
}