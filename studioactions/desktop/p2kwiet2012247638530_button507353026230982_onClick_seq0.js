function p2kwiet2012247638530_button507353026230982_onClick_seq0(eventobject) {
    if ((frmIBActivateIBankingStep1.imgarrow1.isVisible)) {
        frmIBActivateIBankingStep1.imgarrow1.setVisibility(false);
        frmIBActivateIBankingStep1.vbxRules.setVisibility(false);
        frmIBActivateIBankingStep1.hbxPassRules.setVisibility(false);
    } else {
        frmIBActivateIBankingStep1.imgarrow1.setVisibility(true);
        frmIBActivateIBankingStep1.vbxRules.setVisibility(true);
        frmIBActivateIBankingStep1.hbxPassRules.setVisibility(true);
        var deviceInfo = kony.os.deviceInfo();
        if (deviceInfo["hastouchsupport"] == true && deviceInfo["category"] == "Safari") frmIBActivateIBankingStep1.scrollPassRules.containerHeight = 15
            //frmIBActivateIBankingStep1.hbxPassRules.setVisibility(true);
    }
}