function p2kwiet2012247645428_btnAddMoreBiller_onClick_seq0(eventobject) {
    function alert_onClick_46620201224769084_True() {}
    if ((checkMaxBillerCountTopupIB())) {
        frmIBMyTopUpsHome.imgTMBLogo.setVisibility(false);
        //frmIBMyTopUpsHome.hbxTMBLogo.setVisibility(false);
        frmIBMyTopUpsHome.hbxBillersAddContainer.setVisibility(true);
        frmIBMyTopUpsHome.hbxBillersConfirmContainer.setVisibility(false);
        frmIBMyTopUpsHome.hbxBillersEditContainer.setVisibility(false);
        frmIBMyTopUpsHome.hbxBillersViewContainer.setVisibility(false);
        frmIBMyTopUpsHome.hbxCanConfBtnContainer.setVisibility(false);
        frmIBMyTopUpsHome.imgArrowAddBiller.setVisibility(true);
        frmIBMyTopUpsHome.imgArrowSegBiller.setVisibility(false);
        frmIBMyTopUpsHome.hbxBillersCompleteContainer.setVisibility(false);
        frmIBMyTopUpsHome.txtAddBillerNickName.text = "";
        frmIBMyTopUpsHome.txtAddBillerRef1.text = "";
        frmIBMyTopUpsHome.lblAddBillerName.text = "";
        frmIBMyTopUpsHome.imgAddBillerLogo.src = "";
        //frmIBMyTopUpsHome.imgAddBillerLogo.src="";
        frmIBMyTopUpsHome.lblAddBillerRef1.text = kony.i18n.getLocalizedString("keyRef1");
        frmIBMyTopUpsHome.txtAddBillerNickName.setEnabled(false);
        frmIBMyTopUpsHome.txtAddBillerRef1.setEnabled(false);
    } else {
        function alert_onClick_46620201224769084_Callback() {
            alert_onClick_46620201224769084_True();
        }
        kony.ui.Alert({
            "alertType": constants.ALERT_TYPE_ERROR,
            "alertTitle": "",
            "yesLabel": "Ok",
            "noLabel": "No",
            "message": "\"+kony.i18n.getLocalizedString(\"Valid_MoreThan50\")+\"",
            "alertHandler": alert_onClick_46620201224769084_Callback
        }, {
            "iconPosition": constants.ALERT_ICON_POSITION_LEFT
        });
    }
}