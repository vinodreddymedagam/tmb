function p2kwiet2012247647851_frmIBSSSExcuteTrnsfr_preshow_seq0(eventobject, neworientation) {
    kony.print("tokenValueForS2S... " + tokenValueForS2S);
    if (tokenValueForS2S != null) {
        kony.print("tokenValueForS2S not null...");
        if (tokenValueForS2S == "N") {
            kony.print("tokenValueForS2S value N..");
            gblOTPFlag = true;
            tokenFlagForS2S = false;
            kony.print("gblSSServieHours:" + gblSSServieHours);
            kony.print("tokenFlagForS2S:" + tokenFlagForS2S + "gblOTPFlag:" + gblOTPFlag); //txtBxOTP
            frmIBSSSExcuteTrnsfr.hboxBankRef.setVisibility(true);
            frmIBSSSExcuteTrnsfr.hbxOTPsnt.setVisibility(true);
            //frmIBSSSExcuteTrnsfr.lblkeyOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
            //frmIBSSSExcuteTrnsfr.lblOTPKey.text = kony.i18n.getLocalizedString("keyOTP");
            //frmIBSSSExcuteTrnsfr.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
            dismissLoadingScreenPopup();
        } else {
            kony.print("else tokenValueForS2S value Y..");
            gblOTPFlag = false;
            tokenFlagForS2S = true;
            kony.print("gblSSServieHours:" + gblSSServieHours);
            kony.print("tokenFlagForS2S:" + tokenFlagForS2S + "gblOTPFlag:" + gblOTPFlag);
            frmIBSSSExcuteTrnsfr.hboxBankRef.setVisibility(false);
            frmIBSSSExcuteTrnsfr.hbxOTPsnt.setVisibility(false);
            //frmIBSSSExcuteTrnsfr.lblkeyOTPMsg.text = kony.i18n.getLocalizedString("S2S_tokenId");
            //frmIBSSSExcuteTrnsfr.lblOTPKey.text = kony.i18n.getLocalizedString("S2S_Token");
            //frmIBSSSExcuteTrnsfr.btnOTPReq.text = kony.i18n.getLocalizedString("S2S_SwitchtoOTP");
            dismissLoadingScreenPopup();
        }
    } else {
        kony.print("else tokenValueForS2S Null..");
        gblOTPFlag = true;
        tokenFlagForS2S = false;
        kony.print("tokenFlagForS2S:" + tokenFlagForS2S + "gblOTPFlag:" + gblOTPFlag);
        kony.print("gblSSServieHours:" + gblSSServieHours);
        frmIBSSSExcuteTrnsfr.hboxBankRef.setVisibility(true);
        frmIBSSSExcuteTrnsfr.hbxOTPsnt.setVisibility(true);
        //frmIBSSSExcuteTrnsfr.lblkeyOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
        //frmIBSSSExcuteTrnsfr.lblOTPKey.text = kony.i18n.getLocalizedString("keyOTP");
        //frmIBSSSExcuteTrnsfr.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
        dismissLoadingScreenPopup();
    }
}