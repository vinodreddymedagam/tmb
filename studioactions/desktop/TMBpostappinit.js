function TMBpostappinit(params) {
    disableRightClick.call(this);
    IBcopyright_footer_display.call(this);
    IBAppTitle.call(this);
    flowSpa = false;
    if (gblUnavailableFlag) {
        return getIBStartupForm();
    } else {
        return frmIBPreLogin;
    }
}