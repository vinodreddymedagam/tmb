function p2kwiet2012247647346_frmIBPostLoginDashboard_postshow_seq0(eventobject, neworientation) {
    menuReset2.call(this);
    UiChangesOnLocaleChange();
    addIBMenu();
    footerPagesMenu();
    /* removing as per ENH_028 - IB Menu on Home Screen
    var isChrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1; 
    var isIE10= navigator.userAgent.match(/MSIE (\d+)/) != null && RegExp.$1 == "10";
    var isIE11= navigator.userAgent.match(/rv:11.0/i) != null;
    var isIE9= window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "9";
    if(isChrome){ 
    document.getElementById("frmIBPostLoginDashboard_btnMenuConvenientServices").style.marginTop="-25px";
    var head = document.getElementsByTagName('head')[0]; 
    var s = document.createElement('style'); 
    s.setAttribute('type', 'text/css'); 
    s.appendChild(document.createTextNode("#frmIBPostLoginDashboard_hbxTotalmenu td{vertical-align: top}")); 
    head.appendChild(s); 
    }else if(isIE10){
    document.getElementById("frmIBPostLoginDashboard_btnMenuAboutMe").style.marginTop="-88px";
    document.getElementById("frmIBPostLoginDashboard_btnMenuMyInbox").style.marginTop="-88px";
    var head = document.getElementsByTagName('head')[0]; 
    var s = document.createElement('style'); 
    s.setAttribute('type', 'text/css'); 
    s.appendChild(document.createTextNode("#frmIBPostLoginDashboard_hbxTotalmenu td{vertical-align: top}")); 
    head.appendChild(s); 
    }else if(isIE11){
    document.getElementById("frmIBPostLoginDashboard_btnMenuAboutMe").style.marginTop="-88px";
    document.getElementById("frmIBPostLoginDashboard_btnMenuMyInbox").style.marginTop="-88px";
    var head = document.getElementsByTagName('head')[0]; 
    var s = document.createElement('style'); 
    s.setAttribute('type', 'text/css'); 
    s.appendChild(document.createTextNode("#frmIBPostLoginDashboard_hbxTotalmenu td{vertical-align: top}")); 
    head.appendChild(s); 
    }else if(isIE9){
    document.getElementById("frmIBPostLoginDashboard_btnMenuAboutMe").style.marginTop="-88px";
    document.getElementById("frmIBPostLoginDashboard_btnMenuMyInbox").style.marginTop="-88px";
    var head = document.getElementsByTagName('head')[0]; 
    var s = document.createElement('style'); 
    s.setAttribute('type', 'text/css'); 
    s.appendChild(document.createTextNode("#frmIBPostLoginDashboard_hbxTotalmenu td{vertical-align: top}")); 
    head.appendChild(s); 
    }
    */
    LinkMyActivities = 0;
    clearCalendarData();
    showCalendar("", frmIBPostLoginDashboard);
    setHeaderPrePostLogin.call(this);
    resetFooterSkins.call(this);
}