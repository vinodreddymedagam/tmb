function p2kwiet2012247647770_btnCancel_onClick_seq0(eventobject) {
    frmIBSSSDetails.hbxViewTransferToNoFix.setVisibility(true);
    frmIBSSSDetails.hbxViewTrnFrmNoFxd.setVisibility(true);
    frmIBSSSDetails.hbxEditSSSFrom.setVisibility(false);
    frmIBSSSDetails.hbxEditSSSTo.setVisibility(false);
    frmIBSSSDetails.hbxEditCCBtns.setVisibility(false);
    frmIBSSSDetails.btnViewBack.setVisibility(true);
    frmIBSSApplyCnfrmtn.hbxToken.setVisibility(false);
    frmIBSSApplyCnfrmtn.keyEnterToken.setVisibility(false);
}