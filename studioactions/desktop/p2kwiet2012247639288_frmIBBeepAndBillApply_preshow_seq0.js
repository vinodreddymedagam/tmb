function p2kwiet2012247639288_frmIBBeepAndBillApply_preshow_seq0(eventobject, neworientation) {
    frmIBBeepAndBillApply.tbxSearch.placeholder = kony.i18n.getLocalizedString("Receipent_Search")
    frmIBBeepAndBillApply.tbxSearch.text = "";
    //getSelectBBCategoryService()
    if (kony.i18n.getCurrentLocale() == "en_US") {
        frmIBBeepAndBillApply.cbxSelectCat.masterData = gblBillerCategoriesBBIB;
    } else {
        frmIBBeepAndBillApply.cbxSelectCat.masterData = gblBillerCategoriesBBIBTH;
    }
}