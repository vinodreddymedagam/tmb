function p2kwiet2012247642558_btnEditDepositAccnt_onClick_seq0(eventobject) {
    gblCWSelectedItem = 0;
    if ((frmIBDreamSavingMaintenance.lblInterestRateVal.text == "0.00%")) {
        showAlert(kony.i18n.getLocalizedString("keyOpenActGenErr"), kony.i18n.getLocalizedString("info"));
    } else {
        clearDreamDataIB.call(this);
        checkUserStatusDSMIB.call(this);
    }
}