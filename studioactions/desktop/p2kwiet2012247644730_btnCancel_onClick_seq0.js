function p2kwiet2012247644730_btnCancel_onClick_seq0(eventobject) {
    frmIBMyBillersHome.imgTMBLogo.setVisibility(true);
    //frmIBMyBillersHome.hbxTMBLogo.setVisibility(true);
    frmIBMyBillersHome.hbxBillersAddContainer.setVisibility(false);
    frmIBMyBillersHome.hbxBillersConfirmContainer.setVisibility(false);
    frmIBMyBillersHome.hbxBillersEditContainer.setVisibility(false);
    frmIBMyBillersHome.hbxBillersViewContainer.setVisibility(false);
    frmIBMyBillersHome.segBillersList.setVisibility(true); //added to come back to biller list
    frmIBMyBillersHome.hbxSugBillersContainer.setVisibility(false);
    frmIBMyBillersHome.tbxSearch.text = "";
    frmIBMyBillersHome.lblMyBills.setVisibility(false);
    frmIBMyBillersHome.hbxBillersCompleteContainer.setVisibility(false);
    frmIBMyBillersHome.imgArrowAddBiller.setVisibility(false);
    frmIBMyBillersHome.imgArrowSegBiller.setVisibility(false);
    frmIBMyBillersHome.segBillersConfirm.removeAll(); //CLearing Confirm Seg data on click of cancel
    frmIBMyBillersHome.btnAddBiller.setEnabled(true); //Enabling add more since the segment is flushed
    frmIBMyBillersHome.btnConfirmBillerAdd.setEnabled(true); //Enabling add more since the segment is flushed
    frmIBMyBillersHome.segSuggestedBillersList.setEnabled(true); //Enabling add more since the segment is flushed
}