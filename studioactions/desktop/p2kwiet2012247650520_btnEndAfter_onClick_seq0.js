function p2kwiet2012247650520_btnEndAfter_onClick_seq0(eventobject) {
    futuredate.calOnDate.setVisibility(false);
    futuredate.txtAfterTimes.setVisibility(true);
    futuredate.lblAfterTimesInstruction.setVisibility(true);
    futuredate.btnEndAfter.skin = "btnFocusBlue"
    futuredate.btnEndOnDate.skin = "btnNormal"
    futuredate.btnEndNever.skin = "btnNormal"
}