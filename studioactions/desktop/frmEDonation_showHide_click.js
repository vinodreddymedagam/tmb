function frmEDonation_showHide_click(eventobject) {
    return AS_VBox_c2f23c55a97640089e1b41f9ea791691(eventobject);
}

function AS_VBox_c2f23c55a97640089e1b41f9ea791691(eventobject) {
    if (frmIBEDonationExecutedTxn.lblBBPaymentValue.isVisible) {
        frmIBEDonationExecutedTxn.lblBBPaymentValue.setVisibility(false);
        frmIBEDonationExecutedTxn.lblHide.text = kony.i18n.getLocalizedString("keyUnhide");
    } else {
        frmIBEDonationExecutedTxn.lblBBPaymentValue.setVisibility(true);
        frmIBEDonationExecutedTxn.lblHide.text = kony.i18n.getLocalizedString("Hide");
    }
}