function p2kwiet2012247648059_txtUserId_onDone_seq0(eventobject, changedtext) {
    function alert_onDone_72904201224761128_True() {}

    function alert_onDone_31468201224769550_True() {}

    function alert_onDone_55215201224764083_True() {}
    if ((frmIBPreLogin.txtUserId.text != "")) {
        if ((frmIBPreLogin.txtPassword.text != "")) {
            if ((frmIBPreLogin.hboxCaptchaText.isVisible)) {
                if ((frmIBPreLogin.txtCaptchaText.text != "")) {
                    captchaValidation.call(this, frmIBPreLogin.txtCaptchaText.text);
                } else {
                    function alert_onDone_72904201224761128_Callback() {
                        alert_onDone_72904201224761128_True();
                    }
                    kony.ui.Alert({
                        "alertType": constants.ALERT_TYPE_ERROR,
                        "alertTitle": "Alert",
                        "yesLabel": "Yes",
                        "noLabel": "No",
                        "message": "Field is mandatory",
                        "alertHandler": alert_onDone_72904201224761128_Callback
                    }, {
                        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
                    });
                    frmIBPreLogin.txtCaptchaText.setFocus(true);
                }
            } else {
                IBLoginService.call(this);
            }
        } else {
            function alert_onDone_31468201224769550_Callback() {
                alert_onDone_31468201224769550_True();
            }
            kony.ui.Alert({
                "alertType": constants.ALERT_TYPE_ERROR,
                "alertTitle": "Alert",
                "yesLabel": "Yes",
                "noLabel": "No",
                "message": "Field is mandatory",
                "alertHandler": alert_onDone_31468201224769550_Callback
            }, {
                "iconPosition": constants.ALERT_ICON_POSITION_LEFT
            });
            frmIBPreLogin.txtPassword.setFocus(true);
        }
    } else {
        function alert_onDone_55215201224764083_Callback() {
            alert_onDone_55215201224764083_True();
        }
        kony.ui.Alert({
            "alertType": constants.ALERT_TYPE_ERROR,
            "alertTitle": "Alert",
            "yesLabel": "Yes",
            "noLabel": "No",
            "message": "Field is mandatory",
            "alertHandler": alert_onDone_55215201224764083_Callback
        }, {
            "iconPosition": constants.ALERT_ICON_POSITION_LEFT
        });
        frmIBPreLogin.txtUserId.setFocus(true);
    }
}