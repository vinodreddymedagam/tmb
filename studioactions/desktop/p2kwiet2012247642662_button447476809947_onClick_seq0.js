function p2kwiet2012247642662_button447476809947_onClick_seq0(eventobject) {
    frmIBEditFutureBillPaymentPrecnf.labelkeyOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
    frmIBEditFutureBillPaymentPrecnf.labelkeyOTP.text = kony.i18n.getLocalizedString("keyOTP");
    frmIBEditFutureBillPaymentPrecnf.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
    if (gblEditBillMethod == 0) {
        frmIBBillPaymentView.hbxBPEditAmnt.setVisibility(true);
    }
    frmIBBillPaymentView.hbxBPVeiwAmnt.setVisibility(false);
    frmIBBillPaymentView.lblBPeditHrd.setVisibility(true);
    frmIBBillPaymentView.lblBPViewHrd.setVisibility(false);
    frmIBBillPaymentView.hbxBPViewHdr.setVisibility(false);
    frmIBBillPaymentView.buttonEdit.setVisibility(false);
    frmIBBillPaymentView.buttonDel.setVisibility(false);
    frmIBBillPaymentView.btnBPSchedule.setVisibility(true);
    frmIBBillPaymentView.hbxbtnReturn.setVisibility(false);
    frmIBBillPaymentView.hbxEditBtns.setVisibility(true);
    //frmIBBillPaymentView.txtBPEditAmtValue.text = "20.00 THB"
    //if(gblEditBillMethod == 0){
    // frmIBBillPaymentView.hbxOnlinePayment.setVisibility(false);
    // }else{
    // frmIBBillPaymentView.hbxBPEditAmnt.setVisibility(false);
    // }
    frmIBBillPaymentView.hbxBPAfterEditAvilBal.setVisibility(true);
    frmIBBillPaymentView.lblScheduleBillPayment.setVisibility(false);
    frmIBBillPaymentView.hbxEditBPFutureSchedule.setVisibility(false);
    frmIBEditFutureBillPaymentPrecnf.btnOTPReq.skin = btnIBREQotpFocus;
    frmIBBillPaymentView.show();
}