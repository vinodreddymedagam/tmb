function p2kwiet2012247648780_frmIBTopUpCW_preshow_seq0(eventobject, neworientation) {
    frmIBTopUpCW.hbxAmount.setVisibility(false);
    var today = new Date(GLOBAL_TODAY_DATE);
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    today = dd + '/' + mm + '/' + yyyy;
    frmIBTopUpCW.lblPayBillOnValue.text = today
    frmIBTopUpCW.txtAmount.setEnabled(false);
}