function p2kwiet2012247647851_switch2OTP_onClick_seq0(eventobject) {
    frmIBSSSExcuteTrnsfr.hbxTokenEntry.setVisibility(false);
    frmIBSSSExcuteTrnsfr.hbxSSSTrnsfrOTP.setVisibility(true);
    startReqOTPApplyServicesS2SIB();
    gblTokenSwitchFlag = false;
    gblSwitchToken = true;
    frmIBSSSExcuteTrnsfr.txtBxOTP.setFocus(true);
}