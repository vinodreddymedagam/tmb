function p2kwiet2012247644730_segBillersList_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    frmIBMyBillersHome.imgTMBLogo.setVisibility(false);
    //frmIBMyBillersHome.hbxTMBLogo.setVisibility(false);
    frmIBMyBillersHome.hbxBillersAddContainer.setVisibility(false);
    frmIBMyBillersHome.hbxBillersConfirmContainer.setVisibility(false);
    frmIBMyBillersHome.hbxOtpBox.setVisibility(false)
    frmIBMyBillersHome.hbxBillersEditContainer.setVisibility(false);
    frmIBMyBillersHome.hbxBillersViewContainer.setVisibility(true);
    frmIBMyBillersHome.hbxBillersCompleteContainer.setVisibility(false);
    frmIBMyBillersHome.imgArrowAddBiller.setVisibility(false);
    frmIBMyBillersHome.imgArrowSegBiller.setVisibility(false);
    frmIBMyBillersHome.imgEditComplete.setVisibility(false); //added img tick mark
    viewMybilldetail.call(this);
}