function p2kwiet2012247640897_btnIbCMHelp_onClick_seq0(eventobject) {
    if (gblCMUserIDRule == 0) {
        frmIBCMChgPwd.hbxPopUP.setVisibility(true);
        frmIBCMChgPwd.hbxCancelSaveOld.setVisibility(true);
        frmIBCMChgPwd.hbxPasswdStrength.setVisibility(false);
        frmIBCMChgPwd.label47428873338465.setVisibility(false);
        frmIBCMChgPwd.txtRetypePwd.setVisibility(false);
        gblCMUserIDRule = gblCMUserIDRule + 1
    } else if (gblCMUserIDRule == 1) {
        frmIBCMChgPwd.hbxPopUP.setVisibility(false);
        frmIBCMChgPwd.hbxCancelSaveOld.setVisibility(true);
        frmIBCMChgPwd.hbxPasswdStrength.setVisibility(true);
        frmIBCMChgPwd.label47428873338465.setVisibility(true);
        frmIBCMChgPwd.txtRetypePwd.setVisibility(true);
        gblCMUserIDRule = 0
    }
}