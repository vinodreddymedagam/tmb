function p2kwiet2012247647721_btnPreCancel_onClick_seq0(eventobject) {
    frmIBSSApply.txtBalMax.text = frmIBSSApplyCnfrmtn.lblAmntLmtMax.text
    frmIBSSApply.txtBalMin.text = frmIBSSApplyCnfrmtn.lblAmntLmtMin.text
    if (gbls2sEditFlag == "true") {
        frmIBSSApply.hboxLinkedAcct.setVisibility(true);
        frmIBSSApply.imgNoFixedAcct.src = frmIBSSApplyCnfrmtn.imgNoFix.src
        frmIBSSApply.lblCustName2.text = frmIBSSApplyCnfrmtn.lblCustName2.text
        frmIBSSApply.lblBal2.text = frmIBSSApplyCnfrmtn.lblBal2.text
            //frmIBSSApply.lblAcctNo.text=frmIBSSApplyCnfrmtn.lblAcc2.text 
        frmIBSSApply.lblAccNo2.text = frmIBSSApplyCnfrmtn.lblAccNo2.text
        frmIBSSApply.lblAccType2.text = frmIBSSApplyCnfrmtn.lblAccType2.text
        frmIBSSApply.show();
        frmIBSSApplyCnfrmtn.hbxToken.isVisible = false;
        frmIBSSApplyCnfrmtn.keyEnterToken.setVisibility(false);
    } else {
        frmIBSSApply.lblAcctNoValue.text = "";
        frmIBSSApply.hboxLinkedAcct.setVisibility(false);
        frmIBSSApply.imgNoFixedAcct.src = frmIBSSApplyCnfrmtn.imgNoFix.src
        frmIBSSApply.lblCustName2.text = frmIBSSApplyCnfrmtn.lblCustName2.text
        frmIBSSApply.lblBal2.text = frmIBSSApplyCnfrmtn.lblBal2.text
            //frmIBSSApply.lblAcctNo.text=frmIBSSApplyCnfrmtn.lblAcc2.text 
        frmIBSSApply.lblAccNo2.text = frmIBSSApplyCnfrmtn.lblAccNo2.text
        frmIBSSApply.lblAccType2.text = frmIBSSApplyCnfrmtn.lblAccType2.text
        s2sVerticalCoverFlow();
        frmIBSSApply.show();
        frmIBSSApplyCnfrmtn.hbxToken.isVisible = false;
        frmIBSSApplyCnfrmtn.keyEnterToken.setVisibility(false);
    }
}