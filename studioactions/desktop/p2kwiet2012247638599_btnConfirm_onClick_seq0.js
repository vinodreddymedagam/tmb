function p2kwiet2012247638599_btnConfirm_onClick_seq0(eventobject) {
    function alert_onClick_77301201224763925_True() {}

    function alert_onClick_62205201224769285_True() {}
    if ((frmIBActivateIBankingStep2Confirm.txtOTP.text != "")) {
        if ((kony.string.isNumeric(frmIBActivateIBankingStep2Confirm.txtOTP.text))) {
            if ((frmIBActivateIBankingStep2Confirm.txtOTP.text.length == gblOTPLENGTH)) {
                IBverifyOTP.call(this);
            } else {
                frmIBActivateIBankingStep2Confirm.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone"); //kony.i18n.getLocalizedString("invalidOTP"); //
                frmIBActivateIBankingStep2Confirm.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
                frmIBActivateIBankingStep2Confirm.hbxOTPincurrect.isVisible = true;
                frmIBActivateIBankingStep2Confirm.hbox507353026213385.isVisible = false;
                frmIBActivateIBankingStep2Confirm.hbox507353026213437.isVisible = false;
                frmIBActivateIBankingStep2Confirm.txtOTP.setFocus(true);
            }
        } else {
            function alert_onClick_77301201224763925_Callback() {
                alert_onClick_77301201224763925_True();
            }
            kony.ui.Alert({
                "alertType": constants.ALERT_TYPE_ERROR,
                "alertTitle": "",
                "yesLabel": kony.i18n.getLocalizedString("keyOK"),
                "noLabel": "No",
                "message": kony.i18n.getLocalizedString("keyIBOtpNumeric"),
                "alertHandler": alert_onClick_77301201224763925_Callback
            }, {
                "iconPosition": constants.ALERT_ICON_POSITION_LEFT
            });
        }
    } else {
        function alert_onClick_62205201224769285_Callback() {
            alert_onClick_62205201224769285_True();
        }
        kony.ui.Alert({
            "alertType": constants.ALERT_TYPE_ERROR,
            "alertTitle": "",
            "yesLabel": kony.i18n.getLocalizedString("keyOK"),
            "noLabel": "No",
            "message": kony.i18n.getLocalizedString("Receipent_alert_correctOTP"),
            "alertHandler": alert_onClick_62205201224769285_Callback
        }, {
            "iconPosition": constants.ALERT_ICON_POSITION_LEFT
        });
    }
}