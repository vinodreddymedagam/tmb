function p2kwiet2012247645740_hboxMonthsToDream_onClick_seq0(eventobject) {
    gbldreamCoverflow = true;
    amt = frmIBOpenNewDreamAcc.txtODTargetAmt.text;
    amt = amt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(",", "");
    if (amt == "" || amt == "0" || amt == "0.00") {
        frmIBOpenNewDreamAcc.txtODMnthSavAmtSave.setEnabled(false)
        frmIBOpenNewDreamAcc.txtODMyDreamSave.setEnabled(false);
        frmIBOpenNewDreamAcc.txtODMnthSavAmt.setEnabled(false);
        frmIBOpenNewDreamAcc.txtODMyDream.setEnabled(false);
    } else {
        frmIBOpenNewDreamAcc.txtODMnthSavAmtSave.setEnabled(true)
        frmIBOpenNewDreamAcc.txtODMyDreamSave.setEnabled(true);
        onclickMnthsave();
        frmIBOpenNewDreamAcc.image24573708914676.isVisible = true
        frmIBOpenNewDreamAcc.image2867539252331117.isVisible = false
        frmIBOpenNewDreamAcc.vboxMiddle.skin = "vbxRightColumn"
        frmIBOpenNewDreamAcc.vbxBPRight.skin = "vbxRightColumn"
        frmIBOpenNewDreamAcc.vbxBPMiddleRight.skin = "vbxMidRightMinHeight"
        frmIBOpenNewDreamAcc.hbxBPAll.skin = "hbxIBSizeLimiter";
    }
}