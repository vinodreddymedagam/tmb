function p2kwiet2012247648059_btnLogIn_onClick_seq0(eventobject) {
    function alert_onClick_32109201224767602_True() {}

    function alert_onClick_4439120122476418_True() {}

    function alert_onClick_91732201224763392_True() {}
    if ((frmIBTemplateLogin.txtUserId.text != "")) {
        if ((frmIBTemplateLogin.txtPassword.text != "")) {
            if ((frmIBTemplateLogin.hboxCaptchaText.isVisible)) {
                if ((frmIBTemplateLogin.txtCaptchaText.text != "")) {
                    captchaValidation.call(this, frmIBTemplateLogin.txtCaptchaText.text);
                } else {
                    function alert_onClick_32109201224767602_Callback() {
                        alert_onClick_32109201224767602_True();
                    }
                    kony.ui.Alert({
                        "alertType": constants.ALERT_TYPE_ERROR,
                        "alertTitle": "Alert",
                        "yesLabel": "Yes",
                        "noLabel": "No",
                        "message": "Field is mandatory",
                        "alertHandler": alert_onClick_32109201224767602_Callback
                    }, {
                        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
                    });
                    frmIBTemplateLogin.txtCaptchaText.setFocus(true);
                }
            } else {
                IBLoginService.call(this);
            }
        } else {
            function alert_onClick_4439120122476418_Callback() {
                alert_onClick_4439120122476418_True();
            }
            kony.ui.Alert({
                "alertType": constants.ALERT_TYPE_ERROR,
                "alertTitle": "Alert",
                "yesLabel": "Yes",
                "noLabel": "No",
                "message": "Field is mandatory",
                "alertHandler": alert_onClick_4439120122476418_Callback
            }, {
                "iconPosition": constants.ALERT_ICON_POSITION_LEFT
            });
            frmIBTemplateLogin.txtPassword.setFocus(true);
        }
    } else {
        function alert_onClick_91732201224763392_Callback() {
            alert_onClick_91732201224763392_True();
        }
        kony.ui.Alert({
            "alertType": constants.ALERT_TYPE_ERROR,
            "alertTitle": "Alert",
            "yesLabel": "Yes",
            "noLabel": "No",
            "message": "Field is mandatory",
            "alertHandler": alert_onClick_91732201224763392_Callback
        }, {
            "iconPosition": constants.ALERT_ICON_POSITION_LEFT
        });
        frmIBTemplateLogin.txtUserId.setFocus(true);
    }
}