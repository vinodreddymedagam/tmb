function p2kwiet2012247638530_btnConfirm_onClick_seq0(eventobject) {
    function alert_onClick_13004201224762039_True() {}

    function alert_onClick_2485020122476652_True() {}

    function alert_onClick_65422201224765184_True() {}
    if ((gblSetPwd == false && frmIBActivateIBankingStep1.txtActivationCode.text == "")) {
        function alert_onClick_65422201224765184_Callback() {
            alert_onClick_65422201224765184_True();
        }
        kony.ui.Alert({
            "alertType": constants.ALERT_TYPE_ERROR,
            "alertTitle": kony.i18n.getLocalizedString("info"),
            "yesLabel": kony.i18n.getLocalizedString("keyOK"),
            "noLabel": "No",
            "message": kony.i18n.getLocalizedString("keyIBEnterActivationCode"),
            "alertHandler": alert_onClick_65422201224765184_Callback
        }, {
            "iconPosition": constants.ALERT_ICON_POSITION_LEFT
        });
        frmIBActivateIBankingStep1.txtActivationCode.setFocus(true);
    } else {
        if ((frmIBActivateIBankingStep1.txtAccountNumber.text == "")) {
            function alert_onClick_2485020122476652_Callback() {
                alert_onClick_2485020122476652_True();
            }
            kony.ui.Alert({
                "alertType": constants.ALERT_TYPE_ERROR,
                "alertTitle": kony.i18n.getLocalizedString("info"),
                "yesLabel": kony.i18n.getLocalizedString("keyOK"),
                "noLabel": "No",
                "message": kony.i18n.getLocalizedString("keyIBEnterDepositAccountNo"),
                "alertHandler": alert_onClick_2485020122476652_Callback
            }, {
                "iconPosition": constants.ALERT_ICON_POSITION_LEFT
            });
            frmIBActivateIBankingStep1.txtAccountNumber.setFocus(true);
        } else {
            if ((frmIBActivateIBankingStep1.txtIDPass.text == "")) {
                function alert_onClick_13004201224762039_Callback() {
                    alert_onClick_13004201224762039_True();
                }
                kony.ui.Alert({
                    "alertType": constants.ALERT_TYPE_ERROR,
                    "alertTitle": kony.i18n.getLocalizedString("info"),
                    "yesLabel": kony.i18n.getLocalizedString("keyOK"),
                    "noLabel": "No",
                    "message": kony.i18n.getLocalizedString("keyIBEnterIDorPassportNo"),
                    "alertHandler": alert_onClick_13004201224762039_Callback
                }, {
                    "iconPosition": constants.ALERT_ICON_POSITION_LEFT
                });
                frmIBActivateIBankingStep1.txtIDPass.setFocus(true);
            } else {
                ibActivationValidatn.call(this);
                frmIBActivateIBankingStep1Confirm.lblAccNoVal.text = frmIBActivateIBankingStep1.txtAccountNumber.text;
                frmIBActivateIBankingStep1Confirm.lblIDNoVal.text = frmIBActivateIBankingStep1.txtIDPass.text;
            }
        }
    }
}