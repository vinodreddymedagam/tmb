function p2kwiet2012247642984_frmIBePaymentExecutedTxn_postshow_seq0(eventobject, neworientation) {
    addIBMenu.call(this);
    if (kony.i18n.getCurrentLocale() == "th_TH") {
        frmIBePaymentExecutedTxn.btnMenuMyActivities.skin = "btnIBMenuMyActivitiesFocusThai";
    } else {
        frmIBePaymentExecutedTxn.btnMenuMyActivities.skin = "btnIBMenuMyActivitiesFocus";
    }
}