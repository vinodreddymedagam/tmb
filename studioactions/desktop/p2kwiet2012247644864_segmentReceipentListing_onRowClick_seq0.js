function p2kwiet2012247644864_segmentReceipentListing_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    //Store the selected Rc id
    gblselectedRcId = frmIBMyReceipentsAccounts.segmentReceipentListing.selectedItems[0]["receipentID"];
    if (checkActualRcSegItemIndex()) {
        //Index done in method
    } else {
        globalSeletedRcIndex = frmIBMyReceipentsAccounts.segmentReceipentListing.selectedIndex[1];
    }
    //frmIBMyReceipentsAccounts.segmentReceipentListing.selectedIndex = globalSeletedRcIndex;
    frmIBMyReceipentsAccounts.hboxMyReceipents.setVisibility(true);
    frmIBMyReceipentsAccounts.hboxReceipentAccount.setVisibility(false);
    //frmIBMyReceipentsAccounts.hboxTMBWelcome.setVisibility(false);
    var RcName = globalRcData[0][globalSeletedRcIndex].lblReceipentName;
    var RcMob = frmIBMyReceipentsAccounts.segmentReceipentListing.selectedItems[0]["mobileNumber"];
    var RcMail = frmIBMyReceipentsAccounts.segmentReceipentListing.selectedItems[0]["emailId"];
    var RcFbId = frmIBMyReceipentsAccounts.segmentReceipentListing.selectedItems[0]["facebookId"];
    var RcPic = frmIBMyReceipentsAccounts.segmentReceipentListing.selectedItems[0]["imgReceipentPic"];
    resetRcSegData("1", "6");
    //Code to set selected item name
    if (RcName == null) {
        frmIBMyReceipentsAccounts.lblRcName.text = "";
    } else {
        frmIBMyReceipentsAccounts.lblRcName.text = RcName;
    }
    if (RcMob == null || RcMob == "No Number" || RcMob == undefined) {
        frmIBMyReceipentsAccounts.lblRcMobileNo.text = "";
    } else {
        frmIBMyReceipentsAccounts.lblRcMobileNo.text = RcMob;
    }
    if (RcMail == null || RcMail == "No Mail" || RcMail == undefined) {
        frmIBMyReceipentsAccounts.lblRcEmail.text = "";
    } else {
        frmIBMyReceipentsAccounts.lblRcEmail.text = RcMail;
    }
    if (RcFbId == null) {
        frmIBMyReceipentsAccounts.lblRcFbId.text = "";
    } else {
        frmIBMyReceipentsAccounts.lblRcFbId.text = RcFbId;
    }
    if (RcPic == null) {
        frmIBMyReceipentsAccounts.imgReceipentProfile.src = "nouserimg.jpg";
    } else {
        frmIBMyReceipentsAccounts.imgReceipentProfile.src = RcPic;
    }
    setDatatoReceipentBankList.call(this);
}