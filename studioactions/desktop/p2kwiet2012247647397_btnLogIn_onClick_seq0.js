function p2kwiet2012247647397_btnLogIn_onClick_seq0(eventobject) {
    function alert_onClick_94884201224764489_True() {}

    function alert_onClick_58010201224768544_True() {}

    function alert_onClick_64278201224764506_True() {}
    isFromLogin = true
    if ((!/^\s*$/.test(frmIBPreLogin.txtUserId.text))) {
        if ((frmIBPreLogin.txtPassword.text != "")) {
            if ((frmIBPreLogin.hboxCaptchaText.isVisible)) {
                if ((frmIBPreLogin.txtCaptchaText.text != "")) {
                    captchaValidation.call(this, frmIBPreLogin.txtCaptchaText.text);
                } else {
                    function alert_onClick_94884201224764489_Callback() {
                        alert_onClick_94884201224764489_True();
                    }
                    kony.ui.Alert({
                        "alertType": constants.ALERT_TYPE_ERROR,
                        "alertTitle": "Alert",
                        "yesLabel": "Yes",
                        "noLabel": "No",
                        "message": kony.i18n.getLocalizedString("keyCaptchaRequired"),
                        "alertHandler": alert_onClick_94884201224764489_Callback
                    }, {
                        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
                    });
                    frmIBPreLogin.txtCaptchaText.setFocus(true);
                }
            } else {
                IBLoginService.call(this);
            }
        } else {
            function alert_onClick_58010201224768544_Callback() {
                alert_onClick_58010201224768544_True();
            }
            kony.ui.Alert({
                "alertType": constants.ALERT_TYPE_ERROR,
                "alertTitle": "Alert",
                "yesLabel": "Yes",
                "noLabel": "No",
                "message": kony.i18n.getLocalizedString("keyPasswordRequired"),
                "alertHandler": alert_onClick_58010201224768544_Callback
            }, {
                "iconPosition": constants.ALERT_ICON_POSITION_LEFT
            });
            frmIBPreLogin.txtPassword.setFocus(true);
        }
    } else {
        function alert_onClick_64278201224764506_Callback() {
            alert_onClick_64278201224764506_True();
        }
        kony.ui.Alert({
            "alertType": constants.ALERT_TYPE_ERROR,
            "alertTitle": "Alert",
            "yesLabel": "Yes",
            "noLabel": "No",
            "message": kony.i18n.getLocalizedString("keyLoginRequired"),
            "alertHandler": alert_onClick_64278201224764506_Callback
        }, {
            "iconPosition": constants.ALERT_ICON_POSITION_LEFT
        });
        frmIBPreLogin.txtUserId.setFocus(true);
    }
}