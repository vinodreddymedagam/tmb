function p2kwiet2012247644098_frmIBMyAccnts_init_seq0(eventobject, neworientation) {
    rawdataArray = [];
    gblMyAccntAddTmpData = []; // Cache memory - global array
    HIDDEN_ACCT_VAL = "";
    gblAccntName = "";
    NON_TMB_ADD = 0;
    gblOverflowErrorFlag = false;
    gblDelAccnt = false;
    gblAddAccnt = false;
    gblDELAccntAdd = false; //To difrentiate edit or delete flow while adding account in editing flow
    gblEditTMBpersonalizedAcctId = ""
    gblEditTMBpersonalizedId = ""
    gblEditTMBbankCD = ""
    gblEditNTMBpersonalizedAcctId = ""
    gblEditNTMBpersonalizedId = ""
    gblEditNTMBbankCD = ""
    gblEditTMBhiddenAcctName = ""
    gblEditTMBpersonalizedAcctId1 = ""
    gblTMBBankCd = ""
    TMB_BANK_CD = ""; //TMB bannk ID value 
    gblPersonalizeID = "";
    gblAddAccntVarIB.call(this);
}