function p2kwiet2012247639578_frmIBBeepAndBillExecutedTxn_postshow_seq0(eventobject, neworientation) {
    addIBMenu.call(this);
    if (kony.i18n.getCurrentLocale() == "th_TH") {
        frmIBBeepAndBillExecutedTxn.btnMenuMyActivities.skin = "btnIBMenuMyActivitiesFocusThai";
    } else {
        frmIBBeepAndBillExecutedTxn.btnMenuMyActivities.skin = "btnIBMenuMyActivitiesFocus";
    }
}