function p2kwiet2012247649549_lblHide_onClick_seq0(eventobject) {
    if (frmIBTransferNowCompletion.lblBal.isVisible) {
        frmIBTransferNowCompletion.lblBal.setVisibility(false);
        frmIBTransferNowCompletion.lblHide.text = kony.i18n.getLocalizedString("keyUnhide");
    } else {
        frmIBTransferNowCompletion.lblBal.setVisibility(true);
        frmIBTransferNowCompletion.lblHide.text = kony.i18n.getLocalizedString("keyHideIB");
    }
}