function p2kwiet2012247640786_button474230331217989_onClick_seq0(eventobject) {
    if ((frmIBCMChgMobNoTxnLimit.hbxOtpBox.isVisible)) {
        if ((flagMobNum == "old")) {
            CompositeChangeMobileIB.call(this);
        } else {
            verifyChangeMobileNoUserOTP.call(this);
        }
    } else {
        if ((frmIBCMChgMobNoTxnLimit.hbxChangeMobileNumber.isVisible)) {
            IBvalidateMobNuProfileUI.call(this);
        } else {
            IBvalidateTransDetailProfReal.call(this);
        }
    }
}