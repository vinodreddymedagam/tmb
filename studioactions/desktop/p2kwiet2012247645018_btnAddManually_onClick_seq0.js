function p2kwiet2012247645018_btnAddManually_onClick_seq0(eventobject) {
    gblAdd_Receipent_State = gblNEW_RC_ADDITION;
    clearDataOnRcNewRecipientAdditionForm();
    showLoadingScreenPopup();
    frmIBMyReceipentsAddContactManually.show();
    dismissLoadingScreenPopup();
    TMBUtil.DestroyForm(frmIBMyReceipentsAddBankAccnt);
    TMBUtil.DestroyForm(frmIBMyReceipentsAddContactFB);
    TMBUtil.DestroyForm(frmIBMyReceipentsHome);
    gblAdd_Receipent_State = gblNEW_RC_ADDITION;
}