function p2kwiet2012247644730_frmIBMyBillersHome_postshow_seq0(eventobject, neworientation) {
    addIBMenu.call(this);
    segAboutMeLoad.call(this);
    pagespecificSubMenu.call(this);
    startDisplayBillerCategoryServiceIB.call(this);
    getMyBillListIB.call(this);
    kony.print("======================================================================================================================")
    frmIBMyBillersHome.lblBankRef.setVisibility(false);
    frmIBMyBillersHome.lblBankRefValue.setVisibility(false);
    frmIBMyBillersHome.lblOTPMsg.setVisibility(false);
    frmIBMyBillersHome.lblOTPMobileNo.setVisibility(false);
    frmIBMyBillersHome.txtotp.text = "";
    callServiceFlag = false;
    startIBBillTopUpProfileInqService();
    if ((gblAddBillerFromPay)) {
        frmIBMyBillersHome.segBillersConfirm.removeAll();
        frmIBMyBillersHome.imgTMBLogo.setVisibility(false);
        frmIBMyBillersHome.hbxBillersAddContainer.setVisibility(true);
        frmIBMyBillersHome.hbxBillersConfirmContainer.setVisibility(false);
        frmIBMyBillersHome.hbxBillersEditContainer.setVisibility(false);
        frmIBMyBillersHome.hbxBillersViewContainer.setVisibility(false);
        frmIBMyBillersHome.hbxCanConfBtnContainer.setVisibility(false);
        frmIBMyBillersHome.hbxBillersCompleteContainer.setVisibility(false);
        frmIBMyBillersHome.txtAddBillerNickName.text = "";
        frmIBMyBillersHome.txtAddBillerRef1.text = "";
        //frmIBMyBillersHome.lblAddBillerRef1.text=kony.i18n.getLocalizedString("keyRef1");
        //frmIBMyBillersHome.lblAddBillerRef2.text=kony.i18n.getLocalizedString("keyRef2");
        frmIBMyBillersHome.txtAddBillerRef2.text = "";
        //frmIBMyBillersHome.imgAddBillerLogo.src="empty.png"
        frmIBMyBillersHome.imgArrowAddBiller.setVisibility(true);
        frmIBMyBillersHome.imgArrowSegBiller.setVisibility(false);
        if (gblAddBillerButtonClicked) {
            frmIBMyBillersHome.lblAddBillerName.text = "";
            frmIBMyBillersHome.txtAddBillerNickName.setEnabled(false);
            frmIBMyBillersHome.txtAddBillerRef1.setEnabled(false);
            frmIBMyBillersHome.txtAddBillerRef2.setEnabled(false);
        }
    } else {}
    if (gblSuggestedBiller && !gblAddBillerFromPay) {
        //alert("inside f")
        frmIBMyBillersHome.hbxBillersAddContainer.setVisibility(true);
        frmIBMyBillersHome.imgTMBLogo.setVisibility(false);
        frmIBMyBillersHome.lblAddBillerName.text = "";
        frmIBMyBillersHome.imgAddBillerLogo.src = "";
        frmIBMyBillersHome.txtAddBillerNickName.text = "";
        frmIBMyBillersHome.txtAddBillerRef1.text = "";
        frmIBMyBillersHome.txtAddBillerRef2.text = "";
        frmIBMyBillersHome.lblMyBills.setVisibility(true);
        frmIBMyBillersHome.line588686174252372.setVisibility(false);
    }
    frmIBMyBillersHome.tbxSearch.setFocus(true)
}