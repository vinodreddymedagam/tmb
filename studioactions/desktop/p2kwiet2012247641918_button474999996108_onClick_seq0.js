function p2kwiet2012247641918_button474999996108_onClick_seq0(eventobject) {
    frmIBCreateUserID.vbxArrow.margin = [7, 0, 0, 0];
    frmIBCreateUserID.vbxRules.margin = [0, 30, 0, 0];
    var isIE8 = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
    if (isIE8) {
        document.getElementById("frmIBCreateUserID_imgarrow2_span").style.margin = "49px 0px 0px 0px";
        frmIBCreateUserID.vbxRules.margin = [0, 26, 0, 0];
    }
    if ((frmIBCreateUserID.imgarrow2.isVisible)) {
        frmIBCreateUserID.vbxRules.skin = "vbxBGGrey"
        frmIBCreateUserID.imgarrow1.setVisibility(false)
        frmIBCreateUserID.imgarrow2.setVisibility(false)
        frmIBCreateUserID.hbxPassRules.setVisibility(false);
        frmIBCreateUserID.hbxUserIdRules.setVisibility(false);
        frmIBCreateUserID.vbxRules.setVisibility(false);
    } else {
        frmIBCreateUserID.vbxRules.skin = "vbxBGGrey"
        frmIBCreateUserID.vbxRules.setVisibility(true);
        frmIBCreateUserID.imgarrow1.setVisibility(false)
        frmIBCreateUserID.imgarrow2.setVisibility(true)
        var deviceInfo = kony.os.deviceInfo();
        if (deviceInfo["hastouchsupport"] == true && deviceInfo["category"] == "Safari") frmIBCreateUserID.scrollPassRules.containerHeight = 30
        frmIBCreateUserID.hbxPassRules.setVisibility(true);
        frmIBCreateUserID.hbxUserIdRules.setVisibility(false);
        frmIBCreateUserID.hbxUserIDBubble.setVisibility(false);
        frmIBCreateUserID.hbxPasswordRulesBubble.setVisibility(false);
    }
}