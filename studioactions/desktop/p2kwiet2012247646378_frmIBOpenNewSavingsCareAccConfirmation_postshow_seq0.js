function p2kwiet2012247646378_frmIBOpenNewSavingsCareAccConfirmation_postshow_seq0(eventobject, neworientation) {
    addIBMenu.call(this);
    segConvenientServicesLoad.call(this);
    pagespecificSubMenu.call(this);
    addNumberCheckListner.call(this, "txtOTP");
    addNumberCheckListner.call(this, "tbxToken");
    defaultFormToShowConfirmation.call(this);
    syncIBOpenAcctLocale.call(this);
}