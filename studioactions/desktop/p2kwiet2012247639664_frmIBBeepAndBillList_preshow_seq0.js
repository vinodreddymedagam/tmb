function p2kwiet2012247639664_frmIBBeepAndBillList_preshow_seq0(eventobject, neworientation) {
    frmIBBeepAndBillList.lblSuggestedBiller.setVisibility(true)
    frmIBBeepAndBillList.tbxSearch.placeholder = kony.i18n.getLocalizedString("Receipent_Search")
    frmIBBeepAndBillList.lblErrorMsg.isVisible = false
    frmIBBeepAndBillList.tbxSearch.text = "";
}