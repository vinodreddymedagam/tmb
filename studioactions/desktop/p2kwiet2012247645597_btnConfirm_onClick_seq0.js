function p2kwiet2012247645597_btnConfirm_onClick_seq0(eventobject) {
    if ((frmIBOpenAccountContactKYC.hbxOtpBox.isVisible == true)) {
        validateOTPKYCAddressEmailChange.call(this);
    } else {
        if ((frmIBOpenAccountContactKYC.hbxcnf2.isVisible == true)) {
            onMyProfileAddressNextIB.call(this);
        } else {
            checkEditAddressChanged.call(this);
        }
    }
}