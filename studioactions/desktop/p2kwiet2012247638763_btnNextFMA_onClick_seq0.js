function p2kwiet2012247638763_btnNextFMA_onClick_seq0(eventobject) {
    kony.print("rawdataArray: " + rawdataArray);
    kony.print("rawdataArray length : " + rawdataArray.length);
    frmIBAddMyAccnt.hbxTokenEntry.setVisibility(false);
    frmIBAddMyAccnt.hbxOTPEntry.setVisibility(false);
    frmIBAddMyAccnt.segCnfrmtnStep2.widgetDataMap = {
        "lblAccntNameC2": "lblAccntName",
        "lblAccntNameC2val": "lblAccntNameVal",
        "lblAccntNumC2": "lblAccntNum",
        "lblAccntNumC2VAl": "lblAccntNumVal",
        "lblNNC2": "lblNN",
        "lblNNC2VAl": "lblNNValue",
        "lblASufxC2": "lblASufx",
        "lblASufxC2VAl": "lblASufxValue",
        "lblSt2BnkName": "lblBankName",
        "st2bankLogo": "bankLogo",
    };
    //frmIBAddMyAccnt.segCnfrmtnStep2.setData(rawdataArray);
    frmIBAddMyAccnt.segCnfrmtnStep2.setData(gblMyAccntAddTmpData);
    frmIBAddMyAccnt.hbxCnrmtnStep1.setVisibility(false);
    frmIBAddMyAccnt.hbxConfrmtnStep2.setVisibility(true);
    frmIBAddMyAccnt.hbxCompleteAccnt.setVisibility(false);
    frmIBAddMyAccnt.arrwImgSeg1.setVisibility(false);
    frmIBAddMyAccnt.arrwImgSeg2.setVisibility(false);
    frmIBAddMyAccnt.hbxTokenUser.setVisibility(false);
    frmIBAddMyAccnt.hbxOTP.setVisibility(false);
    frmIBAddMyAccnt.hbox44747680935256.setVisibility(false);
    frmIBAddMyAccnt.txtOTP.text = "";
    frmIBAddMyAccnt.txtOTP.setFocus(true);
    gblRetryCountRequestOTP = 0;
    frmIBAddMyAccnt.show();
    srvTokenSwitchingDummyMyAccounts.call(this);
}