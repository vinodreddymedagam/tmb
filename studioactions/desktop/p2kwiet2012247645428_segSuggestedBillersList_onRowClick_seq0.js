function p2kwiet2012247645428_segSuggestedBillersList_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    function alert_onRowClick_8948201224768266_True() {}

    function alert_onRowClick_73320201224764102_True() {}
    if ((kony.string.equals(gblOTPLockedForBiller, "04"))) {
        alert(kony.i18n.getLocalizedString("ECVrfyOTPErr"))
    } else {
        if ((checkIfBarcodeOnlyIBTopUp())) {
            if ((checkMaxBillerCountTopupIB())) {
                gblAddTopupButtonClicked = false;
                frmIBMyTopUpsHome.imgTMBLogo.setVisibility(false);
                frmIBMyTopUpsHome.hbxBillersAddContainer.setVisibility(true);
                frmIBMyTopUpsHome.hbxBillersConfirmContainer.setVisibility(false);
                frmIBMyTopUpsHome.hbxBillersEditContainer.setVisibility(false);
                frmIBMyTopUpsHome.hbxBillersViewContainer.setVisibility(false);
                frmIBMyTopUpsHome.hbxCanConfBtnContainer.setVisibility(false);
                frmIBMyTopUpsHome.imgArrowAddBiller.setVisibility(true);
                frmIBMyTopUpsHome.imgArrowSegBiller.setVisibility(false);
                frmIBMyTopUpsHome.hbxBillersCompleteContainer.setVisibility(false);
                frmIBMyTopUpsHome.txtAddBillerNickName.text = "";
                frmIBMyTopUpsHome.txtAddBillerRef1.text = "";
                frmIBMyTopUpsHome.txtAddBillerNickName.setEnabled(true);
                frmIBMyTopUpsHome.txtAddBillerRef1.setEnabled(true);
                loadAddTopupValues.call(this);
            } else {
                function alert_onRowClick_8948201224768266_Callback() {
                    alert_onRowClick_8948201224768266_True();
                }
                kony.ui.Alert({
                    "alertType": constants.ALERT_TYPE_ERROR,
                    "alertTitle": "",
                    "yesLabel": "Ok",
                    "noLabel": "No",
                    "message": kony.i18n.getLocalizedString("Valid_MoreThan50"),
                    "alertHandler": alert_onRowClick_8948201224768266_Callback
                }, {
                    "iconPosition": constants.ALERT_ICON_POSITION_LEFT
                });
            }
        } else {
            function alert_onRowClick_73320201224764102_Callback() {
                alert_onRowClick_73320201224764102_True();
            }
            kony.ui.Alert({
                "alertType": constants.ALERT_TYPE_ERROR,
                "alertTitle": "",
                "yesLabel": "Ok",
                "noLabel": "No",
                "message": kony.i18n.getLocalizedString("Valid_BarcodeOnly"),
                "alertHandler": alert_onRowClick_73320201224764102_Callback
            }, {
                "iconPosition": constants.ALERT_ICON_POSITION_LEFT
            });
        }
    }
}