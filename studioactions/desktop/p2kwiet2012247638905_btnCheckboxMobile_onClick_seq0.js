function p2kwiet2012247638905_btnCheckboxMobile_onClick_seq0(eventobject) {
    if ((gblMobileAnyIDRegisterAllowed == true)) {
        if ((frmIBAnyIDRegistration.btnCheckboxMobile.skin == "btnCheckFocTransparent")) {
            unRegisterAnyIDMobileIB.call(this);
        } else {
            clickFromCitizenAnyID = false;
            onClickAccountForAnyIDRegistration.call(this);
        }
    } else {
        popupIBAnyIDHelpMessage.lblAnyIDHelpText.text = kony.i18n.getLocalizedString("MIB_P2PErr_LinkOtherMobileNo");
        popupIBAnyIDHelpMessage.show();
    }
}