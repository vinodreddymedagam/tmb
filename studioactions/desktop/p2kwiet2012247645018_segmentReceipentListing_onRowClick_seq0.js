function p2kwiet2012247645018_segmentReceipentListing_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    //Store the selected Rc id
    gblselectedRcId = frmIBMyReceipentsAddContactFB.segmentReceipentListing.selectedItems[0]["receipentID"];
    if (checkActualRcSegItemIndex()) {
        //Index done in method
    } else {
        globalSeletedRcIndex = frmIBMyReceipentsAddContactFB.segmentReceipentListing.selectedIndex[1];
    }
    showLoadingScreenPopup();
    frmIBMyReceipentsAccounts.show();
    frmIBMyReceipentsAccounts.hboxMyReceipents.setVisibility(true);
    frmIBMyReceipentsAccounts.hboxReceipentAccount.setVisibility(false);
    //frmIBMyReceipentsAccounts.hboxTMBWelcome.setVisibility(false);
    //Code to set selected item name
    if (globalRcData[0][globalSeletedRcIndex].lblReceipentName == null) {
        frmIBMyReceipentsAccounts.lblRcName.text = "";
    } else {
        frmIBMyReceipentsAccounts.lblRcName.text = globalRcData[0][globalSeletedRcIndex].lblReceipentName;
    }
    if (frmIBMyReceipentsAddContactFB.segmentReceipentListing.selectedItems[0]["mobileNumber"] == null || frmIBMyReceipentsAddContactFB.segmentReceipentListing.selectedItems[0]["mobileNumber"] == "No Number") {
        frmIBMyReceipentsAccounts.lblRcMobileNo.text = "";
    } else {
        frmIBMyReceipentsAccounts.lblRcMobileNo.text = frmIBMyReceipentsAddContactFB.segmentReceipentListing.selectedItems[0]["mobileNumber"];
    }
    if (frmIBMyReceipentsAddContactFB.segmentReceipentListing.selectedItems[0]["emailId"] == null || frmIBMyReceipentsAddContactFB.segmentReceipentListing.selectedItems[0]["emailId"] == "No Mail") {
        frmIBMyReceipentsAccounts.lblRcEmail.text = "";
    } else {
        frmIBMyReceipentsAccounts.lblRcEmail.text = frmIBMyReceipentsAddContactFB.segmentReceipentListing.selectedItems[0]["emailId"];
    }
    if (frmIBMyReceipentsAddContactFB.segmentReceipentListing.selectedItems[0]["facebookId"] == null) {
        frmIBMyReceipentsAccounts.lblRcFbId.text = "";
    } else {
        frmIBMyReceipentsAccounts.lblRcFbId.text = frmIBMyReceipentsAddContactFB.segmentReceipentListing.selectedItems[0]["facebookId"];
    }
    if (frmIBMyReceipentsAddContactFB.segmentReceipentListing.selectedItems[0]["imgReceipentPic"] == null) {
        frmIBMyReceipentsAccounts.imgReceipentProfile.src = "nouserimg.jpg";
    } else {
        frmIBMyReceipentsAccounts.imgReceipentProfile.src = frmIBMyReceipentsAddContactFB.segmentReceipentListing.selectedItems[0]["imgReceipentPic"];
    }
}