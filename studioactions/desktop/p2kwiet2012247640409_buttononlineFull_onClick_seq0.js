function p2kwiet2012247640409_buttononlineFull_onClick_seq0(eventobject) {
    frmIBBillPaymentView.textboxOnlineTotalAmt.setEnabled(false);
    if (gblEditBillMethod == 3) {
        frmIBBillPaymentView.textboxOnlineTotalAmt.text = fullAmtLoan;
    } else {
        frmIBBillPaymentView.textboxOnlineTotalAmt.text = commaFormatted(fullAmtValue) + " " + kony.i18n.getLocalizedString("currencyThaiBaht"); //fullAmtValue;
    }
    frmIBBillPaymentView.buttononlineSpecified.skin = "btnIBrndrightGreyNowidth";
    frmIBBillPaymentView.buttononlineFull.skin = "btnIBrndleftblueNoWidth";
}