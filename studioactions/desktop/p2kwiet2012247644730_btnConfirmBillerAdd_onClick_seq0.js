function p2kwiet2012247644730_btnConfirmBillerAdd_onClick_seq0(eventobject) {
    function alert_onClick_49886201224767042_True() {}
    if ((kony.string.equals(gblOTPLockedForBiller, "04"))) {
        alert(kony.i18n.getLocalizedString("ECVrfyOTPErr"))
    } else {
        if ((checkMaxBillerCount())) {
            gblAddBillerFromPay = false;
            frmIBMyBillersHome.imgTMBLogo.setVisibility(false);
            kony.print("confirm add bill clicked");
            //frmIBMyBillersHome.hbxTMBLogo.setVisibility(false);
            frmIBMyBillersHome.hbxBillersAddContainer.setVisibility(true);
            frmIBMyBillersHome.hbxBillersConfirmContainer.setVisibility(false);
            frmIBMyBillersHome.hbxBillersEditContainer.setVisibility(false);
            frmIBMyBillersHome.hbxBillersViewContainer.setVisibility(false);
            frmIBMyBillersHome.hbxCanConfBtnContainer.setVisibility(false);
            frmIBMyBillersHome.hbxBillersCompleteContainer.setVisibility(false);
            frmIBMyBillersHome.lblAddBillerName.text = "";
            frmIBMyBillersHome.txtAddBillerNickName.text = "";
            frmIBMyBillersHome.txtAddBillerRef1.text = "";
            frmIBMyBillersHome.lblAddBillerRef1.text = kony.i18n.getLocalizedString("keyRef1");
            frmIBMyBillersHome.imgAddBillerLogo.src = "empty.png";
            frmIBMyBillersHome.imgArrowAddBiller.setVisibility(true);
            frmIBMyBillersHome.imgArrowSegBiller.setVisibility(false);
            frmIBMyBillersHome.txtAddBillerNickName.setEnabled(false);
            frmIBMyBillersHome.txtAddBillerRef1.setEnabled(false);
            frmIBMyBillersHome.lblAddBillerRef2.text = kony.i18n.getLocalizedString("keyRef2");
            frmIBMyBillersHome.txtAddBillerRef2.text = "";
            frmIBMyBillersHome.txtAddBillerRef2.setEnabled(false);
        } else {
            function alert_onClick_49886201224767042_Callback() {
                alert_onClick_49886201224767042_True();
            }
            kony.ui.Alert({
                "alertType": constants.ALERT_TYPE_ERROR,
                "alertTitle": "",
                "yesLabel": "Ok",
                "noLabel": "No",
                "message": kony.i18n.getLocalizedString("Valid_MoreThan50"),
                "alertHandler": alert_onClick_49886201224767042_Callback
            }, {
                "iconPosition": constants.ALERT_ICON_POSITION_LEFT
            });
        }
    }
}