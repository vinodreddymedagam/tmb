function p2kwiet2012247639508_frmIBBeepAndBillExecuteConfComp_preshow_seq0(eventobject, neworientation) {
    for (var i = 0; i < gblmasterBillerAndTopupBB.length; i++) {
        if (gblmasterBillerAndTopupBB[i]["BillerCompcode"] == gblBillerCompCodeBB) {
            if (kony.i18n.getCurrentLocale() == "th_TH") {
                frmIBBeepAndBillExecuteConfComp.lblRef1.text = gblmasterBillerAndTopupBB[i]["LabelReferenceNumber1TH"];
                frmIBBeepAndBillExecuteConfComp.lblRef2.text = gblmasterBillerAndTopupBB[i]["LabelReferenceNumber2TH"];
            } else {
                frmIBBeepAndBillExecuteConfComp.lblRef1.text = gblmasterBillerAndTopupBB[i]["LabelReferenceNumber1EN"];
                frmIBBeepAndBillExecuteConfComp.lblRef2.text = gblmasterBillerAndTopupBB[i]["LabelReferenceNumber2EN"];
            }
            if (kony.i18n.getCurrentLocale() == "en_US") {
                if (kony.i18n.getCurrentLocale() == "en_US") {
                    for (var j = 0; j < gblmasterBillerAndTopupBB[i]["BillerMiscData"].length; j++) {
                        if (gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref1.LABEL.EN") {
                            frmIBBeepAndBillExecuteConfComp.lblRef1.text = gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref2.LABEL.EN") {
                            frmIBBeepAndBillExecuteConfComp.lblRef2.text = gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscText"];
                        }
                    }
                }
            } else {
                for (var j = 0; j < gblmasterBillerAndTopupBB[i]["BillerMiscData"].length; j++) {
                    if (gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref1.LABEL.TH") {
                        frmIBBeepAndBillExecuteConfComp.lblRef1.text = gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscText"];
                    }
                    if (gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref2.LABEL.TH") {
                        frmIBBeepAndBillExecuteConfComp.lblRef2.text = gblmasterBillerAndTopupBB[i]["BillerMiscData"][j]["MiscText"];
                    }
                }
            }
            var tmpRef2BB = "N";
            for (var k = 0; k < gblmasterBillerAndTopupBB[i]["BillerMiscData"].length; k++) {
                if (gblmasterBillerAndTopupBB[i]["BillerMiscData"][k]["MiscName"] == "BB.IsRequiredRefNumber2") {
                    tmpRef2BB = gblmasterBillerAndTopupBB[i]["BillerMiscData"][k]["MiscText"];
                }
            }
            if (tmpRef2BB == "Y") {
                frmIBBeepAndBillExecuteConfComp.hbxRef2.setVisibility(true);
            } else {
                frmIBBeepAndBillExecuteConfComp.hbxRef2.setVisibility(false);
            }
        }
    }
}