function p2kwiet2012247641578_frmIBCMEditMyProfile_preshow_seq0(eventobject, neworientation) {
    frmIBCMEditMyProfile.lblOTPinCurr.text = "";
    frmIBCMEditMyProfile.lblPlsReEnter.text = "";
    frmIBCMEditMyProfile.hbxOtpBox.setVisibility(false);
    frmIBCMEditMyProfile.btnDist.setEnabled(false);
    frmIBCMEditMyProfile.btnSubDist.setEnabled(false);
    frmIBCMEditMyProfile.btnProvince.setEnabled(true);
    frmIBCMEditMyProfile.btnPostalCode.setEnabled(false);
    frmIBCMEditMyProfile.hbxTokenEntry.setVisibility(false);
    frmIBCMEditMyProfile.txtemailvalue.setFocus(true);
    frmIBCMEditMyProfile.image2474288733627.setVisibility(false);
    frmIBCMEditMyProfile.cimgprofpic.setVisibility(false);
    frmIBCMEditMyProfile.imgprofpic.setVisibility(false);
}