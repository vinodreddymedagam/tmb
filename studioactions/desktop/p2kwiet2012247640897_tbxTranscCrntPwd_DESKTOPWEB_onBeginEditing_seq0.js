function p2kwiet2012247640897_tbxTranscCrntPwd_DESKTOPWEB_onBeginEditing_seq0(eventobject, changedtext) {
    frmIBCMChgPwd.tbxTranscCrntPwd.skin = txtBottomBorderFocus;
    //frmIBCMChgPwd.hboxBubble.setVisibility(true);
    if (gblCMUserIDRule == 1) {
        frmIBCMChgPwd.hbxPopUP.setVisibility(false);
        frmIBCMChgPwd.hbxCancelSaveOld.setVisibility(true);
        frmIBCMChgPwd.hbxPasswdStrength.setVisibility(true);
        frmIBCMChgPwd.label47428873338465.setVisibility(true);
        frmIBCMChgPwd.txtRetypePwd.setVisibility(true);
        gblCMUserIDRule = 0
    }
}