function p2kwiet2012247644730_btnViewBillerEdit_onClick_seq0(eventobject) {
    if (getCRMLockStatus()) {
        curr_form = kony.application.getCurrentForm().id;
        popIBBPOTPLocked.show();
    } else {
        frmIBMyBillersHome.imgTMBLogo.setVisibility(false);
        frmIBMyBillersHome.hbxBillersAddContainer.setVisibility(false);
        frmIBMyBillersHome.hbxBillersConfirmContainer.setVisibility(false);
        frmIBMyBillersHome.hbxOtpBox.setVisibility(false)
        frmIBMyBillersHome.hbxBillersEditContainer.setVisibility(true);
        frmIBMyBillersHome.hbxBillersViewContainer.setVisibility(false);
        frmIBMyBillersHome.txtEditBillerNickName.text = frmIBMyBillersHome.lblViewBillerName.text;
        frmIBMyBillersHome.txtEditBillerNickName.setFocus(true)
    }
}