function p2kwiet2012247647951_buttonApplyNow_onClick_seq0(eventobject) {
    frmIBSSSTnC.hbxImage.margin = [0, 75, 0, 0];
    if (gblNFActiStats == "false") {
        showAlert(kony.i18n.getLocalizedString("S2S_OtherStatusMsg"), "");
    } else if (gblNFHidden == "false") {
        // we have to give a link on the alert
        kony.print("inside else gblNFHidden false");
        gblS2SHiddenStatus = true;
        popupDeleAccnt.lblConfoMsg.text = kony.i18n.getLocalizedString("noFixedHiddenStatus");
        popupDeleAccnt.hbox44914510567887.setVisibility(false);
        popupDeleAccnt.button44914510567703.text = kony.i18n.getLocalizedString("keyCancelButton"); //"Cancel";
        popupDeleAccnt.button44747680938122.text = kony.i18n.getLocalizedString("keyConfirm"); //"Continue";
        popupDeleAccnt.button44747680938122.onClick = addMyAccntFT;
        popupDeleAccnt.button44914510567703.onClick = ssDeleteCancelForIB;
        popupDeleAccnt.show();
    } else {
        /** Start of onclick of Apply Now button. This is used to navigate to terms and conditions view */
        frmIBSSSTnC.hbxSSSIntroHdr.setVisibility(false);
        frmIBSSSTnC.hbxTnCHdr.setVisibility(true);
        frmIBSSSTnC.lblSSSIntroData.setVisibility(false);
        //frmIBSSSTnC.lblSSSTnC.setVisibility(true);
        frmIBSSSTnC.hbxApplyNow.setVisibility(false);
        frmIBSSSTnC.hbxBtnsCnclAgree.setVisibility(true);
        frmIBSSSTnC.hboxTncContent.setVisibility(true);
        frmIBSSSTnC.lblTermAndCondition.setVisibility(true);
        loadTermsNConditionForS2SIB();
        /** End of onclick of Apply Now button */
    }
}