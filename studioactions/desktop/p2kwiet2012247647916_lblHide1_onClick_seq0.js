function p2kwiet2012247647916_lblHide1_onClick_seq0(eventobject) {
    if (frmIBSSSExcuteTrnsfrCmplete.lblAfterBal.isVisible) {
        frmIBSSSExcuteTrnsfrCmplete.lblAfterBal.isVisible = false;
        frmIBSSSExcuteTrnsfrCmplete.lblHide1.text = kony.i18n.getLocalizedString("keyUnhide");
    } else {
        frmIBSSSExcuteTrnsfrCmplete.lblAfterBal.isVisible = true;
        frmIBSSSExcuteTrnsfrCmplete.lblHide1.text = kony.i18n.getLocalizedString("Hide");
    }
}