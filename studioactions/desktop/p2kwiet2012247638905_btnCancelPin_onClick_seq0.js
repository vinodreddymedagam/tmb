function p2kwiet2012247638905_btnCancelPin_onClick_seq0(eventobject) {
    if ((frmIBAnyIDRegistration.hbxOtpBox.isVisible)) {
        frmIBAnyIDRegistration.hbxOtpBox.isVisible = false;
        enableDisableAnyIDAllClickableOnOTP(true);
    } else {
        navigatetoAnyIDBrief.call(this);
    }
}