function p2kwiet2012247647593_btnSave_onClick_seq0(eventobject) {
    /* start of on click of next button of frmIBSSApply*/
    var maxAmount = frmIBSSApply.txtBalMax.text;
    maxAmount = maxAmount.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim()
    var minAmount = frmIBSSApply.txtBalMin.text;
    minAmount = minAmount.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim()
    var lmtValid = validAmntLmtIB(maxAmount, minAmount);
    if (lmtValid) {
        frmIBSSApplyCnfrmtn.lblAmntLmtMax.text = frmIBSSApply.txtBalMax.text; //commaFormatted(maxAmount)+" "+kony.i18n.getLocalizedString("currencyThaiBaht");
        frmIBSSApplyCnfrmtn.lblAmntLmtMin.text = frmIBSSApply.txtBalMin.text; //commaFormatted(minAmount)+" "+kony.i18n.getLocalizedString("currencyThaiBaht")
        gblSSServieHours = "dummyABX";
        saveInputinSessionEditIB();
        s2sLinkedAcctForIBEditConfirmation(); //for IB
        s2sNoFixedAcctMappingForIBConfirm(); //for IB
        frmIBSSApplyCnfrmtn.hbxToken.setVisibility(false);
        frmIBSSApplyCnfrmtn.keyEnterToken.setVisibility(false);
    }
}