function p2kwiet2012247638763_btnSwitch2OTP_onClick_seq0(eventobject) {
    frmIBAddMyAccnt.hbxTokenEntry.setVisibility(false);
    frmIBAddMyAccnt.hbxOTPEntry.setVisibility(true);
    frmIBAddMyAccnt.label474165825167354.text = kony.i18n.getLocalizedString("keyotpmsgreq");
    frmIBAddMyAccnt.hbox450035142444916.setVisibility(true);
    frmIBAddMyAccnt.hbox44747680933513.setVisibility(true);
    frmIBAddMyAccnt.txtOTP.setFocus(true);
    gblTokenSwitchFlag = false;
    gblSwitchToken = true;
    IBreqOTPMYAccnts();
}