function p2kwiet2012247644560_frmIBMyActivities_postshow_seq0(eventobject, neworientation) {
    addIBMenu.call(this);
    document.getElementsByClassName("kcell kwt100 middlecenteralign")[3].style = "background-color:white;"
    document.getElementsByClassName("kcell kwt46 middlecenteralign")[0].style = "background-color:white;"
    document.getElementsByClassName("kcell kwt73 topleftalign")[0].style = "background-color:white;"
    if (kony.i18n.getCurrentLocale() == "th_TH") {
        frmIBMyActivities.btnMenuMyActivities.skin = "btnIBMenuMyActivitiesFocusThai";
    } else {
        frmIBMyActivities.btnMenuMyActivities.skin = "btnIBMenuMyActivitiesFocus";
    }
    document.getElementById("frmIBMyActivities_vbxcombo2").className = " kcell kwt28 ";
    document.getElementById("frmIBMyActivities_vbxcombo3").className = " ktable kwt100 ";
    document.getElementById("frmIBMyActivities_vboxCalDayView").className = " ktable kwt94 ";
    if (kony.i18n.getCurrentLocale() == "th_TH") {
        var isIE8 = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
        if (isIE8) {
            frmIBMyActivities.vbox588717954792719.containerWeight = 14;
        }
    }
    columnHeaderInDGHistory.call(this);
    getMonthCycleIB.call(this);
}