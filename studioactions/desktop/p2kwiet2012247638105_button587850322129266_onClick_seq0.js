function p2kwiet2012247638105_button587850322129266_onClick_seq0(eventobject) {
    frmIBBillPaymentView.hbxBPEditAmnt.setVisibility(false);
    frmIBBillPaymentView.hbxBPVeiwAmnt.setVisibility(true);
    frmIBBillPaymentView.lblBPeditHrd.setVisibility(false);
    frmIBBillPaymentView.lblBPViewHrd.setVisibility(true);
    frmIBBillPaymentView.hbxBPViewHdr.setVisibility(true);
    frmIBBillPaymentView.btnBPSchedule.setVisibility(false);
    frmIBBillPaymentView.hbxbtnReturn.setVisibility(true);
    frmIBBillPaymentView.hbxEditBtns.setVisibility(false);
    frmIBBillPaymentView.hbxBPAfterEditAvilBal.setVisibility(false);
    frmIBBillPaymentView.lblScheduleBillPayment.setVisibility(false);
    frmIBBillPaymentView.hbxEditBPFutureSchedule.setVisibility(false);
    frmIBBillPaymentView.arrowBPScheduleField.setVisibility(false);
    frmIBBillPaymentView.show();
}