function p2kwiet2012247648709_btnNext_onClick_seq0(eventobject) {
    frmIBTopUpConfirmation.hbxNext.isVisible = false;
    frmIBTopUpConfirmation.hbxButn.isVisible = true;
    frmIBTopUpConfirmation.hbxOtpBox.isVisible = true;
    frmIBTopUpConfirmation.txtOtp.setFocus(true);
    frmIBTopUpConfirmation.hbxOTPsnt.setVisibility(false);
    frmIBTopUpConfirmation.hbxRefNo.setVisibility(false);
    frmIBTopUpConfirmation.txtOtp.text = "";
    onTopUpPaymentNextIB.call(this);
}