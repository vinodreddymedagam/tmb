function frmIBEDonation_postShow_action(eventobject) {
    return AS_Form_feab0d55eda849d68e1c53eebdb53c04(eventobject);
}

function AS_Form_feab0d55eda849d68e1c53eebdb53c04(eventobject) {
    addIBMenu.call(this);
    if (kony.i18n.getCurrentLocale() == "th_TH") {
        frmIBEDonationExecutedTxn.btnMenuMyActivities.skin = "btnIBMenuMyActivitiesFocusThai";
    } else {
        frmIBEDonationExecutedTxn.btnMenuMyActivities.skin = "btnIBMenuMyActivitiesFocus";
    }
}