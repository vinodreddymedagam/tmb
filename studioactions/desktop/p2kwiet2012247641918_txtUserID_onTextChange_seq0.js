function p2kwiet2012247641918_txtUserID_onTextChange_seq0(eventobject, changedtext) {
    if (frmIBCreateUserID.txtUserID.text.length != 0) {
        frmIBCreateUserID.vbxRules.setVisibility(false);
        frmIBCreateUserID.hbxUserIDBubble.setVisibility(false);
    } else {
        frmIBCreateUserID.vbxRules.setVisibility(true);
        frmIBCreateUserID.hbxUserIDBubble.setVisibility(true);
    }
}