function p2kwiet2012247645274_segmentReceipentListing_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    //Store the selected Rc id
    gblselectedRcId = frmIBMyReceipentsHome.segmentReceipentListing.selectedItems[0]["receipentID"];
    if (checkActualRcSegItemIndex()) {
        //Index done in method
    } else {
        globalSeletedRcIndex = frmIBMyReceipentsHome.segmentReceipentListing.selectedIndex[1];
    }
    showLoadingScreenPopup();
    frmIBMyReceipentsAccounts.show();
    frmIBMyReceipentsAccounts.hboxMyReceipents.setVisibility(true);
    frmIBMyReceipentsAccounts.hboxReceipentAccount.setVisibility(false);
    //frmIBMyReceipentsAccounts.hboxTMBWelcome.setVisibility(false);
    //Code to set selected item name
    if (globalRcData[0][globalSeletedRcIndex].lblReceipentName == null) {
        frmIBMyReceipentsAccounts.lblRcName.text = "";
    } else {
        frmIBMyReceipentsAccounts.lblRcName.text = globalRcData[0][globalSeletedRcIndex].lblReceipentName;
    }
    if (frmIBMyReceipentsHome.segmentReceipentListing.selectedItems[0]["mobileNumber"] == "undefined" || frmIBMyReceipentsHome.segmentReceipentListing.selectedItems[0]["mobileNumber"] == undefined || frmIBMyReceipentsHome.segmentReceipentListing.selectedItems[0]["mobileNumber"] == null || frmIBMyReceipentsHome.segmentReceipentListing.selectedItems[0]["mobileNumber"] == 'No Number') {
        frmIBMyReceipentsAccounts.lblRcMobileNo.text = "";
    } else {
        frmIBMyReceipentsAccounts.lblRcMobileNo.text = frmIBMyReceipentsHome.segmentReceipentListing.selectedItems[0]["mobileNumber"];
    }
    if (frmIBMyReceipentsHome.segmentReceipentListing.selectedItems[0]["emailId"] == undefined || frmIBMyReceipentsHome.segmentReceipentListing.selectedItems[0]["emailId"] == null || frmIBMyReceipentsHome.segmentReceipentListing.selectedItems[0]["emailId"] == 'No Mail') {
        //frmIBMyReceipentsAccounts.lblRcEmail.text = "Not Available";
    } else {
        frmIBMyReceipentsAccounts.lblRcEmail.text = frmIBMyReceipentsHome.segmentReceipentListing.selectedItems[0]["emailId"];
    }
    if (frmIBMyReceipentsHome.segmentReceipentListing.selectedItems[0]["facebookId"] == null) {
        frmIBMyReceipentsAccounts.lblRcFbId.text = "";
    } else {
        frmIBMyReceipentsAccounts.lblRcFbId.text = frmIBMyReceipentsHome.segmentReceipentListing.selectedItems[0]["facebookId"];
    }
    if (frmIBMyReceipentsHome.segmentReceipentListing.selectedItems[0]["imgReceipentPic"] == null) {
        frmIBMyReceipentsAccounts.imgReceipentProfile.src = "nouserimg.jpg";
    } else {
        frmIBMyReceipentsAccounts.imgReceipentProfile.src = frmIBMyReceipentsHome.segmentReceipentListing.selectedItems[0]["imgReceipentPic"];
    }
}