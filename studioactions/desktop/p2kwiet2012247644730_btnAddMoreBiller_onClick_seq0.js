function p2kwiet2012247644730_btnAddMoreBiller_onClick_seq0(eventobject) {
    function alert_onClick_4116201224762584_True() {}
    if ((checkMaxBillerCount())) {
        frmIBMyBillersHome.imgTMBLogo.setVisibility(false);
        //frmIBMyBillersHome.hbxTMBLogo.setVisibility(false);
        frmIBMyBillersHome.hbxBillersAddContainer.setVisibility(true);
        frmIBMyBillersHome.hbxBillersConfirmContainer.setVisibility(false);
        frmIBMyBillersHome.hbxBillersEditContainer.setVisibility(false);
        frmIBMyBillersHome.hbxBillersViewContainer.setVisibility(false);
        frmIBMyBillersHome.hbxCanConfBtnContainer.setVisibility(false);
        frmIBMyBillersHome.hbxBillersCompleteContainer.setVisibility(false);
        frmIBMyBillersHome.lblAddBillerName.text = "";
        frmIBMyBillersHome.txtAddBillerNickName.text = "";
        frmIBMyBillersHome.txtAddBillerRef1.text = "";
        frmIBMyBillersHome.lblAddBillerRef1.text = kony.i18n.getLocalizedString("keyRef1");
        frmIBMyBillersHome.imgAddBillerLogo.src = "empty.png";
        frmIBMyBillersHome.imgArrowAddBiller.setVisibility(true);
        frmIBMyBillersHome.imgArrowSegBiller.setVisibility(false);
        frmIBMyBillersHome.txtAddBillerNickName.setEnabled(false);
        frmIBMyBillersHome.txtAddBillerRef1.setEnabled(false);
        frmIBMyBillersHome.lblAddBillerRef2.text = kony.i18n.getLocalizedString("keyRef2");
        frmIBMyBillersHome.txtAddBillerRef2.text = "";
        frmIBMyBillersHome.txtAddBillerRef2.setEnabled(false);
    } else {
        function alert_onClick_4116201224762584_Callback() {
            alert_onClick_4116201224762584_True();
        }
        kony.ui.Alert({
            "alertType": constants.ALERT_TYPE_ERROR,
            "alertTitle": "",
            "yesLabel": "Ok",
            "noLabel": "No",
            "message": "\"+kony.i18n.getLocalizedString(\"Valid_MoreThan50\")+\"",
            "alertHandler": alert_onClick_4116201224762584_Callback
        }, {
            "iconPosition": constants.ALERT_ICON_POSITION_LEFT
        });
    }
}