function p2kwiet2012247645428_segBillersList_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    frmIBMyTopUpsHome.imgTMBLogo.setVisibility(false);
    //frmIBMyTopUpsHome.hbxTMBLogo.setVisibility(false);
    frmIBMyTopUpsHome.hbxBillersAddContainer.setVisibility(false);
    frmIBMyTopUpsHome.hbxBillersConfirmContainer.setVisibility(false);
    frmIBMyTopUpsHome.hbxOtpBox.setVisibility(false)
    frmIBMyTopUpsHome.hbxBillersEditContainer.setVisibility(false);
    frmIBMyTopUpsHome.hbxBillersViewContainer.setVisibility(true);
    frmIBMyTopUpsHome.imgArrowAddBiller.setVisibility(false);
    frmIBMyTopUpsHome.imgArrowSegBiller.setVisibility(false);
    frmIBMyTopUpsHome.hbxBillersCompleteContainer.setVisibility(false);
    frmIBMyTopUpsHome.imgEditComplete.setVisibility(false); //added tick mark img
    viewMyTopUpdetail.call(this);
}