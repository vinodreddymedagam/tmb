function p2kwiet2012247641918_txtPassword_DESKTOPWEB_onBeginEditing_seq0(eventobject, changedtext) {
    if (kony.i18n.getCurrentLocale() == "en_US") {
        frmIBCreateUserID.image2448396637310883.src = "userid.png"
        frmIBCreateUserID.image2447324161940655.src = "password_rules.png"
    } else {
        frmIBCreateUserID.image2448396637310883.src = "userid_thai.png"
        frmIBCreateUserID.image2447324161940655.src = "password_rules_thai.png"
    }
    frmIBCreateUserID.hbxPassword.skin = "hbxIBbottomborderfocus"
    frmIBCreateUserID.imgarrow1.setVisibility(false);
    frmIBCreateUserID.imgarrow2.setVisibility(false);
    frmIBCreateUserID.hbxPassRules.setVisibility(false);
    frmIBCreateUserID.hbxUserIdRules.setVisibility(false);
    //frmIBCreateUserID.vbxRules.setVisibility(true)
    frmIBCreateUserID.vbxRules.skin = ""
    frmIBCreateUserID.hbxUserIDBubble.setVisibility(false)
        //frmIBCreateUserID.hbxPasswordRulesBubble.setVisibility(true)
    frmIBCreateUserID.vbxRules.margin = [0, 0, 0, 0];
    var countPwd = frmIBCreateUserID.txtPassword.text.length;
    if (countPwd <= 0) {
        frmIBCreateUserID.vbxRules.setVisibility(true);
        frmIBCreateUserID.hbxPasswordRulesBubble.setVisibility(true);
        document.getElementById("frmIBCreateUserID_hbxPasswordRulesBubble").style.margin = "23% 0% 0% 0%";
    } else {
        frmIBCreateUserID.vbxRules.setVisibility(false);
        frmIBCreateUserID.hbxPasswordRulesBubble.setVisibility(false);
    }
    var isIE8 = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
    if (isIE8) {
        document.getElementById("frmIBCreateUserID_hbxPasswordRulesBubble").style.margin = "20% 0% 0% 0%";
        frmIBCreateUserID.vbxRules.margin = [0, 0, 0, 0];
    }
}