function p2kwiet2012247640409_btnOnlineMin_onClick_seq0(eventobject) {
    frmIBBillPaymentView.btnOnlineSpecified.skin = btnIBgreyInactive;
    frmIBBillPaymentView.btnOnlineFull.skin = btnIBgreyInactive;
    frmIBBillPaymentView.btnOnlineMin.skin = btnIBgreyActive;
    frmIBBillPaymentView.textboxOnlineAmtvalue.setEnabled(false);
    frmIBBillPaymentView.textboxOnlineAmtvalue.text = minAmount;
}