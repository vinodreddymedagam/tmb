function p2kwiet2012247643574_btnB4T_onClick_seq0(eventobject) {
    if (varAvailBal == false) {
        frmIBFTrnsrEditCnfmtn.lblFTAvailBal.setVisibility(true);
        frmIBFTrnsrEditCnfmtn.lblFTAvailBalVal.setVisibility(true);
        varAvailBal = true;
        //frmIBFTrnsrEditCnfmtn.lblFTHide.text = "Hide"
        frmIBFTrnsrEditCnfmtn.lblFTHide.text = kony.i18n.getLocalizedString("Hide");
    } else if (varAvailBal == true) {
        frmIBFTrnsrEditCnfmtn.lblFTAvailBal.setVisibility(false);
        frmIBFTrnsrEditCnfmtn.lblFTAvailBalVal.setVisibility(false);
        varAvailBal = false;
        //frmIBFTrnsrEditCnfmtn.lblFTHide.text = "Show"
        frmIBFTrnsrEditCnfmtn.lblFTHide.text = kony.i18n.getLocalizedString("keyUnhide");
    }
}