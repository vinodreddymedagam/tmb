function p2kwiet2012247647916_lblHide_onClick_seq0(eventobject) {
    if (frmIBSSSExcuteTrnsfrCmplete.lblBeforeBalMsg.isVisible) {
        frmIBSSSExcuteTrnsfrCmplete.lblBeforeBalMsg.isVisible = false;
        frmIBSSSExcuteTrnsfrCmplete.lblHide.text = kony.i18n.getLocalizedString("keyUnhide");
        varbtnb4T = true;
    } else {
        frmIBSSSExcuteTrnsfrCmplete.lblBeforeBalMsg.isVisible = true;
        frmIBSSSExcuteTrnsfrCmplete.lblHide.text = kony.i18n.getLocalizedString("Hide");
        varbtnb4T = false;
    }
}