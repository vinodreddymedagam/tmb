function p2kwiet2012247645129_frmIBMyReceipentsAddContactManually_preshow_seq0(eventobject, neworientation) {
    frmIBMyReceipentsAddContactManually.lblOTPinCurr.text = "";
    frmIBMyReceipentsAddContactManually.lblPlsReEnter.text = "";
    validateSecurityForTransactionServiceRc();
    frmIBMyReceipentsAddContactManually.hboxMyRecipient.setVisibility(true);
    frmIBMyReceipentsAddContactManually.hboxMyRecipientDetail.setVisibility(false);
    if (gblAdd_Receipent_State == gblEXISTING_RC_EDITION) {
        frmIBMyReceipentsAddContactManually.link50273743118078.setVisibility(false);
        frmIBMyReceipentsAddContactManually.link50273743118087.setVisibility(false);
        frmIBMyReceipentsAddContactManually.label10107168132952.setVisibility(false);
        frmIBMyReceipentsAddContactManually.segmentRcAccounts.setVisibility(false);
        frmIBMyReceipentsAddContactManually.lblPlusSign.setVisibility(false);
        frmIBMyReceipentsAddContactManually.lblPlus.setVisibility(false);
        frmIBMyReceipentsAddContactManually.hbxOtpBox.setVisibility(false);
        frmIBMyReceipentsAddContactManually.textbox247428873338513.text = "";
        frmIBMyReceipentsAddContactManually.lblref.setVisibility(false);
        frmIBMyReceipentsAddContactManually.lblrefvalue.setVisibility(false);
        frmIBMyReceipentsAddContactManually.lblotpsent.setVisibility(false);
        frmIBMyReceipentsAddContactManually.lblotpmob.setVisibility(false);
        frmIBMyReceipentsAddContactManually.hbox45737278075167.setVisibility(false);
        frmIBMyReceipentsAddContactManually.button1010716813157.setVisibility(true);
    } else {
        frmIBMyReceipentsAddContactManually.link50273743118078.setVisibility(true);
        frmIBMyReceipentsAddContactManually.link50273743118087.setVisibility(true);
        frmIBMyReceipentsAddContactManually.hbxOtpBox.setVisibility(false);
        frmIBMyReceipentsAddContactManually.label10107168132952.setVisibility(true);
        frmIBMyReceipentsAddContactManually.segmentRcAccounts.setVisibility(true);
        frmIBMyReceipentsAddContactManually.lblPlusSign.setVisibility(true);
        frmIBMyReceipentsAddContactManually.lblPlus.setVisibility(true);
        frmIBMyReceipentsAddContactManually.lblBankRef.setVisibility(false);
        frmIBMyReceipentsAddContactManually.label476047582127701.setVisibility(false);
        frmIBMyReceipentsAddContactManually.label476047582115277.setVisibility(false);
        frmIBMyReceipentsAddContactManually.label476047582115279.setVisibility(false);
        frmIBMyReceipentsAddContactManually.hbox45737278075167.setVisibility(false);
        frmIBMyReceipentsAddContactManually.button1010716813157.setVisibility(true);
    }
}