function p2kwiet2012247644864_frmIBMyReceipentsAccounts_postshow_seq0(eventobject, neworientation) {
    addIBMenu.call(this);
    setDatatoReceipentsList.call(this, "6");
    doRcCacheRefresh.call(this);
    destroyAllForms.call(this);
    destroySavedBankData.call(this);
    setDatatoReceipentBankList.call(this);
    frmIBMyReceipentsAddContactManually.btnotp.skin = btn100;
    frmIBMyReceipentsAddContactManually.btnotp.focusSkin = btn100;
    frmIBMyReceipentsAddContactManually.btnotp.setEnabled(true);
    frmIBMyReceipentsAddContactFB.btnOTPReq.skin = btn100;
    frmIBMyReceipentsAddContactFB.btnOTPReq.focusSkin = btn100;
    frmIBMyReceipentsAddContactFB.btnOTPReq.setEnabled(true);
    isAddFb = false;
    pagespecificSubMenu.call(this);
}