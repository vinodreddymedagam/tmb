function p2kwiet2012247640897_txtPassword_DESKTOPWEB_onBeginEditing_seq0(eventobject, changedtext) {
    frmIBCMChgPwd.hbox47428873338443.skin = "hbxIBbottomborderfocus";
    if (gblCMUserIDRule == 1) {
        frmIBCMChgPwd.hbxPopUP.setVisibility(false);
        frmIBCMChgPwd.hbxCancelSaveOld.setVisibility(true);
        frmIBCMChgPwd.hbxPasswdStrength.setVisibility(true);
        frmIBCMChgPwd.label47428873338465.setVisibility(true);
        frmIBCMChgPwd.txtRetypePwd.setVisibility(true);
        gblCMUserIDRule = 0
    }
}