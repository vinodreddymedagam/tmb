function p2kwiet2012247640090_vbxHide_onClick_seq0(eventobject) {
    if (frmIBBillPaymentExecutedTxn.lblBBPaymentValue.isVisible) {
        frmIBBillPaymentExecutedTxn.lblBBPaymentValue.setVisibility(false);
        frmIBBillPaymentExecutedTxn.lblHide.text = kony.i18n.getLocalizedString("keyUnhide");
    } else {
        frmIBBillPaymentExecutedTxn.lblBBPaymentValue.setVisibility(true);
        frmIBBillPaymentExecutedTxn.lblHide.text = kony.i18n.getLocalizedString("Hide");
    }
}