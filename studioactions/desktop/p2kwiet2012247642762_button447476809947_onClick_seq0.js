function p2kwiet2012247642762_button447476809947_onClick_seq0(eventobject) {
    frmIBEditFutureTopUpPrecnf.labelkeyOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
    frmIBEditFutureTopUpPrecnf.labelkeyOTP.text = kony.i18n.getLocalizedString("keyOTP");
    frmIBEditFutureTopUpPrecnf.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
    frmIBTopUpViewNEdit.hbxTPVeiwAmnt.setVisibility(false);
    frmIBTopUpViewNEdit.lblTPeditHrd.setVisibility(true);
    frmIBTopUpViewNEdit.lblTPViewHrd.setVisibility(false);
    frmIBTopUpViewNEdit.hbxTPViewHdr.setVisibility(false);
    frmIBTopUpViewNEdit.buttonEdit.setVisibility(false);
    frmIBTopUpViewNEdit.buttonDel.setVisibility(false);
    frmIBTopUpViewNEdit.btnTPSchedule.setVisibility(true);
    frmIBTopUpViewNEdit.hbxbtnReturn.setVisibility(false);
    frmIBTopUpViewNEdit.hbxEditBtns.setVisibility(true);
    //frmIBBillPaymentView.txtBPEditAmtValue.text = "20.00 THB"
    frmIBTopUpViewNEdit.hbxTPAfterEditAvilBal.setVisibility(true);
    frmIBTopUpViewNEdit.lblScheduleTopUp.setVisibility(false);
    frmIBTopUpViewNEdit.hbxEditTPFutureSchedule.setVisibility(false);
    frmIBTopUpViewNEdit.show();
    if (gblEditBillMethodTP == 0) {
        frmIBTopUpViewNEdit.hbxTPEditAmnt.setVisibility(true);
    } else {
        frmIBTopUpViewNEdit.comboboxOnlineAmt.selectedKey = 0;
        frmIBTopUpViewNEdit.comboboxOnlineAmt.setVisibility(true);
    }
}