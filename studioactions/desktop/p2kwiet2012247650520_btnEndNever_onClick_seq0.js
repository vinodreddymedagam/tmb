function p2kwiet2012247650520_btnEndNever_onClick_seq0(eventobject) {
    futuredate.calOnDate.setVisibility(false);
    futuredate.txtAfterTimes.setVisibility(false);
    futuredate.lblAfterTimesInstruction.setVisibility(false);
    futuredate.btnEndAfter.skin = "btnNormal"
    futuredate.btnEndOnDate.skin = "btnNormal"
    futuredate.btnEndNever.skin = "btnFocusBlue"
}