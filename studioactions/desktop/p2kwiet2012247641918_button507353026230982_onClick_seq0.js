function p2kwiet2012247641918_button507353026230982_onClick_seq0(eventobject) {
    frmIBCreateUserID.vbxArrow.margin = [4, 0, 0, 0];
    frmIBCreateUserID.vbxRules.margin = [0, 0, 0, 0];
    var isIE8 = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
    if (isIE8) {
        document.getElementById("frmIBCreateUserID_imgarrow1_span").style.margin = "20px 0px 0px 0px";
        frmIBCreateUserID.vbxRules.margin = [0, 0, 0, 0];
    }
    if ((frmIBCreateUserID.imgarrow1.isVisible)) {
        frmIBCreateUserID.vbxRules.skin = "vbxBGGrey"
        frmIBCreateUserID.imgarrow1.setVisibility(false);
        frmIBCreateUserID.imgarrow2.setVisibility(false);
        frmIBCreateUserID.hbxPassRules.setVisibility(false);
        frmIBCreateUserID.hbxUserIdRules.setVisibility(false);
        frmIBCreateUserID.vbxRules.setVisibility(false);
    } else {
        frmIBCreateUserID.vbxRules.skin = "vbxBGGrey"
        frmIBCreateUserID.vbxRules.setVisibility(true);
        frmIBCreateUserID.imgarrow1.setVisibility(true)
        frmIBCreateUserID.imgarrow2.setVisibility(false)
        var deviceInfo = kony.os.deviceInfo();
        if (deviceInfo["hastouchsupport"] == true && deviceInfo["category"] == "Safari") frmIBCreateUserID.scrollUserIdRules.containerHeight = 30
        frmIBCreateUserID.hbxPassRules.setVisibility(false);
        frmIBCreateUserID.hbxUserIdRules.setVisibility(true);
        frmIBCreateUserID.hbxUserIDBubble.setVisibility(false);
        frmIBCreateUserID.hbxPasswordRulesBubble.setVisibility(false);
    }
}