function p2kwiet2012247638105_button5897308481150304_onClick_seq0(eventobject) {
    frmIBMyActivities.hbox58868446699428.setVisibility(true);
    frmIBMyActivities.lblactivity.setVisibility(true);
    frmIBMyActivities.hbxhistory.setVisibility(false);
    frmIBMyActivities.hbxfuture.setVisibility(false);
    frmIBMyActivities.hbxbottom.setVisibility(false);
    frmIBMyActivities.hbxbottomfuture.setVisibility(false);
    frmIBMyActivities.hbox589488823281264.setVisibility(false);
    frmIBMyActivities.hbxCalendarContainer.setVisibility(true);
    frmIBMyActivities.vboxCalDayView.setVisibility(false);
    //setting the right margin so that the hboxStatusDesc takes less width
    //and this will be similar when vboxCalDayView is visible 
    frmIBMyActivities.hboxStatusDesc.margin = [0, 0, 40, 0];
    frmIBMyActivities.vboxCalDayView.containerWeight = 0;
    frmIBMyActivities.btnfuture.skin = btnIBrndleftgrey;
    frmIBMyActivities.btnhistory.skin = btnIBrndleftgrey;
    frmIBMyActivities.btncalender.skin = btnIBrndrightblue;
    showCalendar("", frmIBMyActivities);
    frmIBMyActivities.show();
}