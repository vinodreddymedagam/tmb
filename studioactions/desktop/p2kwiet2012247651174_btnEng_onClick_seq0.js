function p2kwiet2012247651174_btnEng_onClick_seq0(eventobject) {
    langchange.call(this, "en_US");
    hbxIBPreLogin.btnEng.skin = "btnEngLangOn";
    hbxIBPreLogin.btnThai.skin = "btnThaiLangOff";
    hbxIBPostLogin.btnEng.skin = "btnEngLangOn";
    hbxIBPostLogin.btnThai.skin = "btnThaiLangOff";
}