function p2kwiet2012247651202_btnEngE8_onClick_seq0(eventobject) {
    langchange.call(this, "en_US");
    hbxiE8.btnEngE8.skin = "btnEngLangOn";
    hbxiE8.btnThaiE8.skin = "btnThaiLangOff";
    hbxiE8.btnEngE8.skin = "btnEngLangOn";
    hbxiE8.btnThaiE8.skin = "btnThaiLangOff";
    if (!kony.appinit.isIE8) {
        frmIBPreLogin.txtUserId.placeholder = kony.i18n.getLocalizedString("keyUserIDPreLogin");
        frmIBPreLogin.txtPassword.placeholder = kony.i18n.getLocalizedString("keyPasswordPlaceHolder");
    }
    hbxiE8.lnkChrome.text = kony.i18n.getLocalizedString("Chrome");
    hbxiE8.lnkSafari.text = kony.i18n.getLocalizedString("Safari");
    hbxiE8.lnkMozilla.text = kony.i18n.getLocalizedString("Mozilla");
    hbxiE8.lnkE.text = kony.i18n.getLocalizedString("Internet");
    hbxClose.btnClose.text = kony.i18n.getLocalizedString("Close");
    hbxiE8.lblDesc.text = kony.i18n.getLocalizedString("ie8Desc");
}