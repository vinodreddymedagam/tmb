function p2kwiet2012247647397_frmIBPreLogin_postshow_seq0(eventobject, neworientation) {
    isiPad = navigator.userAgent.match(/iPad/i) != null;
    frmIBPreLogin.txtUserId.setFocus(true);
    document.getElementById("frmIBPreLogin_hbxActivateImage").style.cursor = "pointer"
    if (gblFirstSiteAccess) {
        langchange("th_TH");
        gblFirstSiteAccess = false;
    }
    gblLoggedIn = false;
    if (!kony.appinit.isIE8) {
        frmIBPreLogin.txtUserId.placeholder = kony.i18n.getLocalizedString("keyUserIDPreLogin");
        frmIBPreLogin.txtPassword.placeholder = kony.i18n.getLocalizedString("keyPasswordPlaceHolder");
    }
    gblLocale = true;
    if (kony.i18n.getCurrentLocale() == "th_TH") {
        hbxFooterPrelogin.linkhotpromo.containerWeight = 11;
        hbxFooterPrelogin.linkFindTmb.containerWeight = 12;
        hbxFooterPrelogin.linkExchangeRate.containerWeight = 16;
        hbxFooterPrelogin.linkSiteTour.containerWeight = 10;
        hbxFooterPrelogin.link506459299592952.containerWeight = 11;
        hbxFooterPrelogin.linkTnC.containerWeight = 38;
        hbxIBPreLogin.btnEng.skin = "btnEngLangOff";
        hbxIBPreLogin.btnThai.skin = "btnThaiLangOn";
        hbxIBPostLogin.btnEng.skin = "btnEngLangOff";
        hbxIBPostLogin.btnThai.skin = "btnThaiLangOn";
    } else {
        hbxFooterPrelogin.linkhotpromo.containerWeight = 17;
        hbxFooterPrelogin.linkFindTmb.containerWeight = 12;
        hbxFooterPrelogin.linkExchangeRate.containerWeight = 17;
        hbxFooterPrelogin.linkSiteTour.containerWeight = 11;
        hbxFooterPrelogin.link506459299592952.containerWeight = 13;
        hbxFooterPrelogin.linkTnC.containerWeight = 30;
        hbxIBPreLogin.btnEng.skin = "btnEngLangOn";
        hbxIBPreLogin.btnThai.skin = "btnThaiLangOff";
        hbxIBPostLogin.btnEng.skin = "btnEngLangOn";
        hbxIBPostLogin.btnThai.skin = "btnThaiLangOff";
    }
    hbxFooterPrelogin.linkhotpromo.setVisibility(false);
    frmIBPreLogin.btnQRLogin.text = kony.i18n.getLocalizedString("IB_btnQR");
    if (window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8") {
        kony.print("this is IE8888 so underline will not work");
    } else {
        hbxFooterPrelogin.linkhotpromo.focusSkin = "footerLinkHPFoc"
        hbxFooterPrelogin.linkhotpromo.hoverSkin = "footerLinkHPFoc"
        hbxFooterPrelogin.linkFindTmb.focusSkin = "footerLinkFindFoc"
        hbxFooterPrelogin.linkFindTmb.hoverSkin = "footerLinkFindFoc"
        hbxFooterPrelogin.linkExchangeRate.focusSkin = "footerLinkERFoc"
        hbxFooterPrelogin.linkExchangeRate.hoverSkin = "footerLinkERFoc"
        hbxFooterPrelogin.linkSiteTour.focusSkin = "footerLinkSTFoc"
        hbxFooterPrelogin.linkSiteTour.hoverSkin = "footerLinkSTFoc"
        hbxFooterPrelogin.link506459299592952.focusSkin = "footerLinkContactFoc"
        hbxFooterPrelogin.link506459299592952.hoverSkin = "footerLinkContactFoc"
        hbxFooterPrelogin.linkTnC.focusSkin = "footerLinkTCFoc"
        hbxFooterPrelogin.linkTnC.hoverSkin = "footerLinkTCFoc"
        hbxFooterPrelogin.link445625918108451.focusSkin = "footerLinkCallTMBFoc"
        hbxFooterPrelogin.link445625918108451.hoverSkin = "footerLinkCallTMBFoc"
        hbxFooterPrelogin.linkWebChat.focusSkin = "footerLinkWebChatFoc"
        hbxFooterPrelogin.linkWebChat.hoverSkin = "footerLinkWebChatFoc"
        hbxFooterPrelogin.linkSecurity.focusSkin = "footerLinkSecurityFoc"
        hbxFooterPrelogin.linkSecurity.hoverSkin = "footerLinkSecurityFoc"
    }
    resetFooterSkins.call(this);
    setWebkitTextSecurity.call(this);
    if ((isMFEnabled == true)) {} else {
        campaignPreLoginService.call(this, "segCampaignImage", "frmIBPreLogin", "I");
    }
}