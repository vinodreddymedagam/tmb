function p2kwiet2012247643574_button447476809947_onClick_seq0(eventobject) {
    frmIBFTrnsrView.hbxFTEditAmnt.setVisibility(true);
    frmIBFTrnsrView.hbxFTVeiwAmnt.setVisibility(false);
    frmIBFTrnsrView.lblFTEditHdr.setVisibility(true);
    frmIBFTrnsrView.btnFTEdit.setVisibility(true);
    frmIBFTrnsrView.hbxFTViewHdr.setVisibility(false);
    frmIBFTrnsrView.btnReturnView.setVisibility(false)
    frmIBFTrnsrView.hbxEditBtns.setVisibility(true);
    //frmIBFTrnsrView.lblToAccntName.text = gblAccntName_ORFT;
    frmIBFTrnsrView.hbxEditFT.setVisibility(false);
    frmIBFTrnsrView.show();
    frmIBFTrnsrEditCnfmtn.btnOTPReq.skin = btnIBREQotpFocus;
    frmIBFTrnsrEditCnfmtn.btnOTPReq.setEnabled(true);
    frmIBFTrnsrView.lblFeeVal.text = frmIBFTrnsrEditCnfmtn.lblFeeVal.text;
    frmIBFTrnsrView.btnFTEditFlow.setVisibility(false);
    frmIBFTrnsrView.btnFTDel.setVisibility(false);
}