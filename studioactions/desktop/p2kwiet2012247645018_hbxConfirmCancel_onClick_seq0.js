function p2kwiet2012247645018_hbxConfirmCancel_onClick_seq0(eventobject) {
    if (gblAdd_Receipent_State != gblEXISTING_RC_EDITION) {
        gblAdd_Receipent_State = gblNEW_RC_ADDITION_PRECONFIRM;
    }
    if (checkAccountCountBulkAddition() == MAX_BANK_ACCNT_BULK_ADD) {
        return;
    } else {
        clearDataOnRcBankAdditionForm();
        frmIBMyReceipentsAddBankAccnt.show();
    }
}