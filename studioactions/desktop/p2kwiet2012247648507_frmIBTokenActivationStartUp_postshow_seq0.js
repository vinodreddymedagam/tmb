function p2kwiet2012247648507_frmIBTokenActivationStartUp_postshow_seq0(eventobject, neworientation) {
    addIBMenu.call(this);
    curr_form = kony.application.getCurrentForm();
    curr_form.segMenuOptions.setData([{
        "lblSegData": {
            "skin": "lblIBsegMenuFocus",
            "text": "My profile"
        }
    }, {
        "lblSegData": {
            "skin": "lblIBSegMenu",
            "text": "My Accounts"
        }
    }, {
        "lblSegData": {
            "skin": "lblIBSegMenu",
            "text": "Open New Account"
        }
    }, {
        "lblSegData": {
            "skin": "lblIBSegMenu",
            "text": "My Recipients"
        }
    }, {
        "lblSegData": {
            "skin": "lblIBSegMenu",
            "text": "My Bills"
        }
    }, {
        "lblSegData": {
            "skin": "lblIBSegMenu",
            "text": "My Top-Ups"
        }
    }]);
    gblMenuSelection = 1;
    IBCMCommonMyProfilePreShow.call(this);
}