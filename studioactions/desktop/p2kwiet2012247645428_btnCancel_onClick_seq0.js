function p2kwiet2012247645428_btnCancel_onClick_seq0(eventobject) {
    frmIBMyTopUpsHome.imgTMBLogo.setVisibility(true);
    //frmIBMyBillersHome.hbxTMBLogo.setVisibility(true);
    frmIBMyTopUpsHome.hbxBillersAddContainer.setVisibility(false);
    frmIBMyTopUpsHome.hbxBillersConfirmContainer.setVisibility(false);
    frmIBMyTopUpsHome.hbxBillersEditContainer.setVisibility(false);
    frmIBMyTopUpsHome.hbxBillersViewContainer.setVisibility(false);
    frmIBMyTopUpsHome.segBillersList.setVisibility(true); //added to come back to biller list
    frmIBMyTopUpsHome.hbxSugBillersContainer.setVisibility(false);
    frmIBMyTopUpsHome.tbxSearch.text = "";
    frmIBMyTopUpsHome.lblMyBills.setVisibility(false);
    frmIBMyTopUpsHome.hbxBillersCompleteContainer.setVisibility(false);
    frmIBMyTopUpsHome.imgArrowAddBiller.setVisibility(false);
    frmIBMyTopUpsHome.imgArrowSegBiller.setVisibility(false);
    frmIBMyTopUpsHome.segBillersConfirm.removeAll(); //CLearing Confirm Seg data on click of cancel
    frmIBMyTopUpsHome.btnAddBiller.setEnabled(true); //Enabling add more since the segment is flushed
    frmIBMyTopUpsHome.btnConfirmBillerAdd.setEnabled(true); //Enabling add more since the segment is flushed
    frmIBMyTopUpsHome.segSuggestedBillersList.setEnabled(true); //Enabling add more since the segment is flushed
}