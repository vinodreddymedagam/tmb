function p2kwiet2012247646427_frmIBOpenNewTermDepositAcc_preshow_seq0(eventobject, neworientation) {
    frmIBOpenNewTermDepositAcc.coverFlowTP.onSelect = onClickConnectTDCoverFlowIB;
    if (gblisDisToActTD == "N") {
        frmIBOpenNewTermDepositAcc.hbxPayingIntTo.setVisibility(false);
        frmIBOpenNewTermDepositAcc.line58877183059489.setVisibility(false);
        gblOnClickCoverflow = "TERMTOACCOUNT"
    } else {
        frmIBOpenNewTermDepositAcc.hbxPayingIntTo.setVisibility(true);
        frmIBOpenNewTermDepositAcc.line58877183059489.setVisibility(true);
    }
}