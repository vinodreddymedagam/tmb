function p2kwiet2012247645184_btnAddManually_onClick_seq0(eventobject) {
    gblAdd_Receipent_State = gblNEW_RC_ADDITION;
    clearDataOnRcNewRecipientAdditionForm();
    showLoadingScreenPopup();
    frmIBMyReceipentsAddContactManually.show();
    dismissLoadingScreenPopup();
    frmIBMyReceipentsAddContactManually.hboxMyRecipient.setVisibility(true);
    frmIBMyReceipentsAddContactManually.hboxMyRecipientDetail.setVisibility(false);
    gblAdd_Receipent_State = gblNEW_RC_ADDITION;
}