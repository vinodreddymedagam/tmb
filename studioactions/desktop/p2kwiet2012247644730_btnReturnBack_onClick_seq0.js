function p2kwiet2012247644730_btnReturnBack_onClick_seq0(eventobject) {
    if ((gblAddToMyBill)) {
        frmIBBillPaymentLP.show()
    } else {
        frmIBMyBillersHome.imgTMBLogo.setVisibility(true);
        //frmIBMyBillersHome.hbxTMBLogo.setVisibility(true);
        frmIBMyBillersHome.hbxBillersAddContainer.setVisibility(false);
        frmIBMyBillersHome.hbxBillersConfirmContainer.setVisibility(false);
        frmIBMyBillersHome.hbxBillersEditContainer.setVisibility(false);
        frmIBMyBillersHome.hbxBillersViewContainer.setVisibility(false);
        frmIBMyBillersHome.hbxSugBillersContainer.setVisibility(false);
        frmIBMyBillersHome.lblMyBills.setVisibility(false);
        frmIBMyBillersHome.hbxBillersCompleteContainer.setVisibility(false);
        frmIBMyBillersHome.imgArrowAddBiller.setVisibility(false);
        frmIBMyBillersHome.imgArrowSegBiller.setVisibility(false);
        //frmIBMyBillersHome.preShow();
        //frmIBMyBillersHome.postShow();
        preShowForBillers();
        postShowForBillers();
    }
}