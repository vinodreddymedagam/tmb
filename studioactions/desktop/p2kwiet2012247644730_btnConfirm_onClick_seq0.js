function p2kwiet2012247644730_btnConfirm_onClick_seq0(eventobject) {
    if ((checkMaxBillerCountOnConfirm())) {
        mapSeg.call(this);
        verifyOTPBillTopUp.call(this);
    } else {
        var BillerList = myBillerList;
        var ConfirmationList = frmIBMyBillersHome.segBillersConfirm.data;
        var maxBillAllowed = parseInt(GLOBAL_MAX_BILL_COUNT)
        var TotalBillerCanBeAdded = maxBillAllowed - BillerList.length;
        alert(kony.i18n.getLocalizedString("Valid_CantAddMore") + TotalBillerCanBeAdded + kony.i18n.getLocalizedString("keyBillersIB"));
    }
}