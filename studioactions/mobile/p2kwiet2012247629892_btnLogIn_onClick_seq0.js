function p2kwiet2012247629892_btnLogIn_onClick_seq0(eventobject) {
    function alert_onClick_10956201224765022_True() {}
    if ((!/^\s*$/.test(frmMBActivationIBLogin.txtUserId.text))) {
        if ((frmMBActivationIBLogin.txtPassword.text != "")) {
            if ((frmMBActivationIBLogin.hboxCaptchaText.isVisible)) {
                if ((frmMBActivationIBLogin.txtCaptchaText.text != "")) {
                    captchaValidationSpa.call(this, frmMBActivationIBLogin.txtCaptchaText.text);
                } else {
                    function alert_onClick_10956201224765022_Callback() {
                        alert_onClick_10956201224765022_True();
                    }
                    kony.ui.Alert({
                        "alertType": constants.ALERT_TYPE_ERROR,
                        "alertTitle": "Alert",
                        "yesLabel": "Yes",
                        "noLabel": "No",
                        "message": kony.i18n.getLocalizedString("keyCaptchaRequired"),
                        "alertHandler": alert_onClick_10956201224765022_Callback
                    }, {
                        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
                    });
                    frmMBActivationIBLogin.txtCaptchaText.setFocus(true);
                }
            } else {
                mbActivationIBLogin.call(this);
            }
        } else {
            alert(kony.i18n.getLocalizedString("keyPasswordRequired"));
            frmMBActivationIBLogin.txtPassword.setFocus(true);
        }
    } else {
        alert(kony.i18n.getLocalizedString("keyEnterUserId"));
        frmMBActivationIBLogin.txtUserId.setFocus(true);
    }
}