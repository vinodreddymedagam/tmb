function p2kwiet2012247637118_btnCancel_onClick_seq0(eventobject) {
    frmBillPaymentEdit.show();
    frmBillPaymentEdit.lblStartDate.text = gblTempTPStartOnDateMB
    frmBillPaymentEdit.lblEndOnDate.text = gblTempTPEndOnDateMB
    frmBillPaymentEdit.lblExecutetimes.text = gblTempTPRepeatAsMB
    repeatAsMB = gblTemprepeatAsTPMB
    endFreqSaveMB = gblTempendFreqSaveTPMB;
    if (repeatAsMB == "" || endFreqSaveMB == "") {
        if (gblEndingFreqOnLoadMB == "Never") {
            OnClickRepeatAsMB = "";
            OnClickEndFreqMB = "";
        } else if (gblOnLoadRepeatAsMB == "Once") {
            OnClickRepeatAsMB = repeatAsIB;
            OnClickEndFreqMB = endFreqSave;
        } else {
            OnClickRepeatAsMB = "";
            OnClickEndFreqMB = "";
        }
    } else {
        OnClickRepeatAsMB = repeatAsMB;
        OnClickEndFreqMB = endFreqSaveMB;
    }
}