function p2kwiet2012247637118_btnAfter_onClick_seq0(eventobject) {
    frmScheduleBillPayEditFuture.hboxtimes.setVisibility(true);
    frmScheduleBillPayEditFuture.lblNumberOfTimes.setVisibility(true);
    frmScheduleBillPayEditFuture.linetimes.setVisibility(true);
    frmScheduleBillPayEditFuture.tbxAfterTimes.keyBoardStyle = constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD;
    endingScheduleFrequencyMB.call(this, eventobject);
}