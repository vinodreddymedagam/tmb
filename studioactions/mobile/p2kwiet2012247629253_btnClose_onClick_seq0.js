function p2kwiet2012247629253_btnClose_onClick_seq0(eventobject) {
    if ((flowSpa == true)) {
        if (GBL_Fatca_Flow == "OAccounts") {
            if (gblFATCAUpdateFlag == "8" || gblFATCAUpdateFlag == "9" || gblFATCAUpdateFlag == "Z") {
                //TMBUtil.DestroyForm(frmAccountSummaryLanding);
                isMenuShown = false
                //frmAccountSummaryLanding = null;
                //frmAccountSummaryLandingGlobals();
                gblAccountTable = "";
                showLoadingScreen();
                callCustomerAccountService();
            } else {
                ivokeCustActInqForOpenAct();
                frmOpenActSelProd.scrollboxMain.scrollToEnd();
            }
        } else {
            showLoadingScreen();
            callCustomerAccountService();
        }
    } else {
        if (GBL_Fatca_Flow == "OAccounts") {
            if (gblFATCAUpdateFlag == "8" || gblFATCAUpdateFlag == "9" || gblFATCAUpdateFlag == "Z") {
                showAccuntSummaryScreen();
            } else {
                ivokeCustActInqForOpenAct();
            }
        } else if (GBL_Fatca_Flow == "ekycModule") { //mki,MIB-13686 start
            kony.store.removeItem("eKYCRegistrationFlag");
            kony.store.setItem("eKYCRegistrationFlag", "N");
            kony.application.exit();
        } else {
            showLoadingScreen();
            LoginProcessServExecMB("");
        }
    }
}