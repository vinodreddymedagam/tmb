function p2kwiet2012247637335_tbxExcludingBillerOneOld_Android_onEndEditing_seq0(eventobject, changedtext) {
    if (frmTopUp.tbxExcludingBillerOne.text == "") {
        frmTopUp.tbxExcludingBillerOne.text = "0.00 " + kony.i18n.getLocalizedString("currencyThaiBaht");
    } else {
        frmTopUp.tbxExcludingBillerOne.text = commaFormatted(parseFloat(removeCommos(frmTopUp.tbxExcludingBillerOne.text)).toFixed(2)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
    }
}