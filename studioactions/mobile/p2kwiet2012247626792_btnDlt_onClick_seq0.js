function p2kwiet2012247626792_btnDlt_onClick_seq0(eventobject, context) {
    gblTopupDelete = frmAddTopUpBillerconfrmtn.segConfirmationList.selectedIndex[1];
    frmAddTopUpBillerconfrmtn.segConfirmationList.removeAt(gblTopupDelete);
    var tmpLength = 0;
    if (frmAddTopUpBillerconfrmtn.segConfirmationList.data != null) tmpLength = frmAddTopUpBillerconfrmtn.segConfirmationList.data.length;
    else tmpLength = 0;
    if (tmpLength < GLOBAL_MAX_BILL_ADD) {
        frmMyTopUpList.button1010778103103263.setEnabled(true);
        enableAddButtonTopUpBillerConfirm();
        frmMyTopUpList.segSuggestedBillers.setEnabled(true);
    }
    if (tmpLength == 0) {
        TMBUtil.DestroyForm(frmAddTopUpToMB);
        frmMyTopUpList.show();
        gblFlagConfirmDataAddedMB = 0;
    }
}