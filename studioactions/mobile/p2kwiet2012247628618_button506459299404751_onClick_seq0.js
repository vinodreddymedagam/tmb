function p2kwiet2012247628618_button506459299404751_onClick_seq0(eventobject) {
    kony.print("user--" + isSignedUser);
    if (flowSpa) {
        TMBUtil.DestroyForm(frmContactusFAQMB);
    }
    if ((isSignedUser == true)) {
        contactUsDisplayForm.call(this);
    } else {
        frmContactUsMB.show();
        frmContactUsMB.hbxPostLogin.setVisibility(false);
        frmContactUsMB.hbxFeedback.setVisibility(false);
        frmContactUsMB.hbxContact.setVisibility(true)
        frmContactUsMB.line47592361418663.setVisibility(true);
        frmContactUsMB.line47592361418559.setVisibility(true);
        frmContactUsMB.hbxFAQ.setVisibility(true);
        frmContactUsMB.hbxFB.setVisibility(false);
        frmContactUsMB.hbxFindTMB.setVisibility(false);
        frmContactUsMB.lblheader.text = kony.i18n.getLocalizedString("keyContactUs");
    }
}