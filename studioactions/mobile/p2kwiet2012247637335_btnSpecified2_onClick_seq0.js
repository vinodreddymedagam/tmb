function p2kwiet2012247637335_btnSpecified2_onClick_seq0(eventobject) {
    fullSpecButtonColorSet.call(this, eventobject);
    frmBillPayment.tbxAmount.setVisibility(true);
    frmBillPayment.tbxAmount.setEnabled(true);
    frmBillPayment.tbxAmount.placeholder = commaFormatted(fullAmt) + kony.i18n.getLocalizedString("currencyThaiBaht");
    frmBillPayment.lblForFullPayment.setVisibility(false);
    frmBillPayment.tbxAmount.text = commaFormatted(fullAmt) + kony.i18n.getLocalizedString("currencyThaiBaht");
    gblFullPayment = false;
}