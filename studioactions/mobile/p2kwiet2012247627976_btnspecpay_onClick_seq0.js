function p2kwiet2012247627976_btnspecpay_onClick_seq0(eventobject) {
    frmBillPaymentEdit.btnspecpay.skin = btnScheduleRightFocus;
    frmBillPaymentEdit.btnminpay.skin = btnScheduleMid;
    frmBillPaymentEdit.btnfullpay.skin = btnScheduleLeft;
    //enable specified button and amt text
    frmBillPaymentEdit.txtamountvalue.setEnabled(true);
    frmBillPaymentEdit.txtamountvalue.text = frmBillPaymentView.lblamtvalue.text;
}