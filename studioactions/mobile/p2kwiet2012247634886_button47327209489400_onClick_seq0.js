function p2kwiet2012247634886_button47327209489400_onClick_seq0(eventobject) {
    if ((gblUserLockStatusMB == gblFinancialTxnMBLock)) {
        alertUserStatusLocked.call(this);
    } else {
        frmMyRecipientEditAccount.txtAccountNickName.text = frmMyRecipientViewAccount.lblNick.text;
        frmMyRecipientEditAccount.tbxAccName.text = frmMyRecipientViewAccount.lblAcctName.text;
        frmMyRecipientEditAccount.txtAccountNickName.text = frmMyRecipientViewAccount.lblNick.text;
        frmMyRecipientEditAccount.show();
    }
}