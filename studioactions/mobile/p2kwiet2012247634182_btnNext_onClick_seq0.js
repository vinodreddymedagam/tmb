function p2kwiet2012247634182_btnNext_onClick_seq0(eventobject) {
    frmMyRecipientAddProfile.tbxRecipientName.skin = txtNormalBG;
    frmMyRecipientAddProfile.tbxMobileNo.skin = txtNormalBG;
    frmMyRecipientAddProfile.tbxEmail.skin = txtNormalBG;
    frmMyRecipientAddProfile.tbxFbID.skin = txtNormalBG;
    //#ifdef j2me
    //#define preprocessdecision_onClick_12171201224769655_j2me_bb_bb10_winphone8_palm_android_iphone_winmobile_symbian_winmobile6x
    //#endif
    //#ifdef bb
    //#define preprocessdecision_onClick_12171201224769655_j2me_bb_bb10_winphone8_palm_android_iphone_winmobile_symbian_winmobile6x
    //#endif
    //#ifdef bb10
    //#define preprocessdecision_onClick_12171201224769655_j2me_bb_bb10_winphone8_palm_android_iphone_winmobile_symbian_winmobile6x
    //#endif
    //#ifdef winphone8
    //#define preprocessdecision_onClick_12171201224769655_j2me_bb_bb10_winphone8_palm_android_iphone_winmobile_symbian_winmobile6x
    //#endif
    //#ifdef palm
    //#define preprocessdecision_onClick_12171201224769655_j2me_bb_bb10_winphone8_palm_android_iphone_winmobile_symbian_winmobile6x
    //#endif
    //#ifdef android
    //#define preprocessdecision_onClick_12171201224769655_j2me_bb_bb10_winphone8_palm_android_iphone_winmobile_symbian_winmobile6x
    //#endif
    //#ifdef iphone
    //#define preprocessdecision_onClick_12171201224769655_j2me_bb_bb10_winphone8_palm_android_iphone_winmobile_symbian_winmobile6x
    //#endif
    //#ifdef winmobile
    //#define preprocessdecision_onClick_12171201224769655_j2me_bb_bb10_winphone8_palm_android_iphone_winmobile_symbian_winmobile6x
    //#endif
    //#ifdef symbian
    //#define preprocessdecision_onClick_12171201224769655_j2me_bb_bb10_winphone8_palm_android_iphone_winmobile_symbian_winmobile6x
    //#endif
    //#ifdef winmobile6x
    //#define preprocessdecision_onClick_12171201224769655_j2me_bb_bb10_winphone8_palm_android_iphone_winmobile_symbian_winmobile6x
    //#endif
    //#ifdef preprocessdecision_onClick_12171201224769655_j2me_bb_bb10_winphone8_palm_android_iphone_winmobile_symbian_winmobile6x
    if ((gblUserLockStatusMB == gblFinancialTxnMBLock)) {
        alertUserStatusLocked.call(this);
    } else {
        AddAccountList.length = 0;
        addNewRecipientAddNewAcc.call(this, eventobject);
    }
    //#endif
    //#ifdef spaip
    //#define preprocessdecision_onClick_95171201224765904_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
    //#endif
    //#ifdef spabbnth
    //#define preprocessdecision_onClick_95171201224765904_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
    //#endif
    //#ifdef spawinphone8
    //#define preprocessdecision_onClick_95171201224765904_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
    //#endif
    //#ifdef wap
    //#define preprocessdecision_onClick_95171201224765904_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
    //#endif
    //#ifdef spaan
    //#define preprocessdecision_onClick_95171201224765904_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
    //#endif
    //#ifdef spabb
    //#define preprocessdecision_onClick_95171201224765904_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
    //#endif
    //#ifdef spawindows
    //#define preprocessdecision_onClick_95171201224765904_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
    //#endif
    //#ifdef preprocessdecision_onClick_95171201224765904_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
    if ((gblIBFlowStatus == "04")) {
        alertUserStatusLocked.call(this);
    } else {
        AddAccountList.length = 0;
        addNewRecipientAddNewAcc.call(this, eventobject);
    }
    //#endif
}