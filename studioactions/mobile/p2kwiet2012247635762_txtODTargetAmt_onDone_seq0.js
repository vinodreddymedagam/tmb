function p2kwiet2012247635762_txtODTargetAmt_onDone_seq0(eventobject, changedtext) {
    var entAmt = frmOpenAcDreamSaving.txtODTargetAmt.text;
    entAmt = entAmt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(/,/g, "")
    var isCrtFormt;
    isCrtFormt = amountValidationMB(entAmt);
    if (!isCrtFormt) {
        showAlert(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), kony.i18n.getLocalizedString("info"));
        return false
    }
    var indexdot = entAmt.indexOf(".");
    var decimal = "";
    var remAmt = "";
    kony.print("indexdot" + indexdot);
    if (indexdot > 0) {
        decimal = entAmt.substr(indexdot);
        kony.print("decimal@@" + decimal);
        if (decimal.length > 3) {
            alert("Enter only 2 decimal values");
            return false;
        }
    }
    frmOpenAcDreamSaving.txtODMnthSavAmt.text = "";
    frmOpenAcDreamSaving.txtODMyDream.text = "";
}