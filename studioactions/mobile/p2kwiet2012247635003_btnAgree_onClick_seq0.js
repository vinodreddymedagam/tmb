function p2kwiet2012247635003_btnAgree_onClick_seq0(eventobject) {
    function alert_onClick_43566201224768490_True() {}
    frmMyTopUpEditScreens.txtEditName.skin = txtNormalBG;
    frmMyTopUpEditScreens.txtEditName.focusSkin = txtNormalBG;
    if ((checkDuplicateBillerNickCheck(frmMyTopUpEditScreens.txtEditName.text))) {
        editTopUpBillerValidation.call(this);
    } else {
        function alert_onClick_43566201224768490_Callback() {
            alert_onClick_43566201224768490_True();
        }
        kony.ui.Alert({
            "alertType": constants.ALERT_TYPE_ERROR,
            "alertTitle": "",
            "yesLabel": "Ok",
            "noLabel": "No",
            "message": kony.i18n.getLocalizedString("Valid_DuplicateNickname"),
            "alertHandler": alert_onClick_43566201224768490_Callback
        }, {
            "iconPosition": constants.ALERT_ICON_POSITION_LEFT
        });
    }
}