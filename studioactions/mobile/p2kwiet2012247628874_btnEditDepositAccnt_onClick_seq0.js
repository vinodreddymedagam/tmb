function p2kwiet2012247628874_btnEditDepositAccnt_onClick_seq0(eventobject) {
    if ((frmDreamSavingMB.lblInterestRateVal.text == "0.0%")) {
        showAlert(kony.i18n.getLocalizedString("keyOpenActGenErr"), kony.i18n.getLocalizedString("info"));
    } else {
        checkUserStatusDSM.call(this);
        clearTargetAmnt.call(this);
        oldTargetAmnt = frmDreamSavingMB.lblTargetAmount.text;
        oldTargetAmnt = oldTargetAmnt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "")
        oldMnthlyAmnt = frmDreamSavingMB.lblMnthlySavingAmntVal.text;
        oldMnthlyAmnt = oldMnthlyAmnt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "")
        oldRecurringDate = mnthlyTransferDate;
    }
}