function frmQRSuccess_onDoneEnterAmount(eventobject, changedtext) {
    return AS_TextField_hd5fec7250bf424c822a719fa96d02a9(eventobject, changedtext);
}

function AS_TextField_hd5fec7250bf424c822a719fa96d02a9(eventobject, changedtext) {
    if (frmQRSuccess.tbxEnterAmount.text == "" || frmQRSuccess.tbxEnterAmount.text == "0" || frmQRSuccess.tbxEnterAmount.text == "0.00") {
        alert(kony.i18n.getLocalizedString("keyPleaseEnterAmount"));
    } else {
        gblQRPayData["transferAmt"] = removeCommos(frmQRSuccess.tbxEnterAmount.text);
        invokeQRPaymentDefaultAccountServiceCalFee();
    }
}