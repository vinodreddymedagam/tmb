function p2kwiet2012247626636_frmAccountSummaryLanding_Android_onDeviceBack_seq0(eventobject, neworientation) {
    try {
        if (isMenuShown == false) {
            if (!gblTimerFlg) {
                showToastMsg();
                kony.timer.schedule("btnTimer", callback, 4, false);
                popGoback.show();
                gblTimerFlg = true;
                gblExitByBackBtn = true;
                resetValues();
            }
        }

        function callback() {
            popGoback.destroy();
            popGoback.dismiss();
            kony.timer.cancel("btnTimer");
            gblTimerFlg = false;
            kony.print("FPRINT making gblCallPrePost to true");
            gblCallPrePost = true;
        }
    } catch (Error) {
        alert("back btn");
    }
}