function p2kwiet2012247634003_button4733076528913_onClick_seq0(eventobject) {
    frmMyRecipientAddAcc.btnBanklist.text = kony.i18n.getLocalizedString("keyBank");
    frmMyRecipientAddAcc.txtAccNo.text = null;
    frmMyRecipientAddAcc.txtNickname.text = null;
    if (AddAccountList.length > 0) {
        frmMyRecipientAddAccConf.show();
    } else {
        if (isRecipientNew) {
            gblRequestFromForm = "addaccount";
            frmMyRecipientAddProfile.show();
        } else {
            frmMyRecipientDetail.show();
        }
    }
}