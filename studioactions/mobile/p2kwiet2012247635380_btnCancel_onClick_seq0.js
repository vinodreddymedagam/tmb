function p2kwiet2012247635380_btnCancel_onClick_seq0(eventobject) {
    frmOpenActSelProd.lblHdrTxt.text = kony.i18n.getLocalizedString("keyOpenAcc");
    frmOpenActSelProd.label47505874734422.text = kony.i18n.getLocalizedString("keyopenselectCategory");
    if (flowSpa) {
        frmOpenActSelProd.label473361467796309.text = kony.i18n.getLocalizedString("forUse");
        frmOpenActSelProd.label473361467796317.text = kony.i18n.getLocalizedString("forSave");
        frmOpenActSelProd.label473361467796313.text = kony.i18n.getLocalizedString("TermDepositMB");
    } else {
        frmOpenActSelProd.label47505874734002.text = kony.i18n.getLocalizedString("forUse");
        frmOpenActSelProd.label47505874734014.text = kony.i18n.getLocalizedString("forSave");
        frmOpenActSelProd.label47505874734008.text = kony.i18n.getLocalizedString("TermDepositMB");
    }
    setDataForUse();
    frmOpenActSelProd.show();
}