function p2kwiet2012247626788_btnCancelSpa_onClick_seq0(eventobject) {
    if ((isMenuShown == false)) {
        gblAddBillerFromPay = false;
        frmMyTopUpList.show();
        frmAddTopUpBillerconfrmtn.segConfirmationList.data = [];
        gblFlagConfirmDataAddedMB = 0;
        frmMyTopUpList.button1010778103103263.setEnabled(true);
        enableAddButtonTopUpBillerConfirm();
        frmMyTopUpList.segSuggestedBillers.setEnabled(true);
    } else {
        frmAddTopUpBillerconfrmtn.scrollboxMain.scrollToEnd();
        isMenuShown = false;
        gblAddBillerFromPay = false;
    }
}