function p2kwiet2012247634553_btnSearchRecipeints_onClick_seq0(eventobject) {
    var search = frmMyRecipientSelectContacts.textbox286685406430472.text
    frmMyRecipientSelectContacts.label475095112172022.setVisibility(false);
    frmMyRecipientSelectContacts.segMyRecipient.setVisibility(true);
    if (search != null && search != "") {
        if (kony.string.containsChars(frmMyRecipientSelectContacts.textbox286685406430472.text, charsArr)) {
            alert(kony.i18n.getLocalizedString("InvalidSearch"));
            return;
        }
    }
    var manualSearch = false;
    if (eventobject.id == "btnSearchRecipeints") {
        manualSearch = true;
    }
    if (search.length >= 3 || manualSearch) {
        var showList = new Array();
        var searchtxt = search;
        var j = 0;
        var regexp = new RegExp("(" + searchtxt + ")", "ig");
        for (var i = 0; i < ContactList1.length; i++) {
            if (ContactList1[i]["lblName"]) {
                if (regexp.test(ContactList1[i]["lblName"]) == true) {
                    showList[j] = ContactList1[i];
                    j++;
                }
            }
        }
        if (showList.length == 0 || showList.length == undefined) {
            frmMyRecipientSelectContacts.label475095112172022.setVisibility(true);
            frmMyRecipientSelectContacts.segMyRecipient.setVisibility(false);
            frmMyRecipientSelectContacts.label475095112172022.text = kony.i18n.getLocalizedString("keybillernotfound");
            //showAlertRcMB(kony.i18n.getLocalizedString("keybillernotfound"), kony.i18n.getLocalizedString("info"), "info")
        } else {
            frmMyRecipientSelectContacts.segMyRecipient.setData(showList);
        }
    } else {
        frmMyRecipientSelectContacts.segMyRecipient.setData(ContactList1);
    }
}