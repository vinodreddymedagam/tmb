function p2kwiet2012247634068_btnAgree_onClick_seq0(eventobject) {
    AddAccountList.length = 0;
    frmMyRecipientAddProfile.tbxEmail.text = "";
    frmMyRecipientAddProfile.tbxFbID.text = "";
    frmMyRecipientAddProfile.tbxMobileNo.text = "";
    frmMyRecipientAddProfile.tbxRecipientName.text = "";
    frmMyRecipientAddProfile.imgProfilePic.src = "avatar.png";
    gblAddMoreRcTrack = gblAddMoreRcTrack + 1;
    if ((gblAddMoreRcTrack + myRecipientsRs.length >= gblMAXRecipientCount)) {
        alert99LimitReached.call(this);
    } else {
        gblAddMoreFromManualCache.push(frmMyRecipientAddAccComplete.lblName.text);
        myRecipientsRs.push({
            "lblName": {
                "text": frmMyRecipientAddAccComplete.lblName.text
            }
        })
        frmMyRecipientAddProfile.show();
    }
}