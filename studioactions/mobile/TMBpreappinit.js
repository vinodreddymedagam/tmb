function TMBpreappinit(params) {
    try {
        kony.modules.loadFunctionalModule("preTemplates");
        kony.modules.loadFunctionalModule("preLoginModules");
        kony.modules.loadFunctionalModule("cardlessModule");
    } catch (err) {
        kony.print("Error loading functional modules");
    }
    defineTMBGlobals.call(this);
    preLoginLocaleSet();
    setAppErrorHandler();
    initializeMFSDK();
}