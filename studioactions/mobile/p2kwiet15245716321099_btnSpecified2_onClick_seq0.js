function p2kwiet15245716321099_btnSpecified2_onClick_seq0(eventobject) {
    fullSpecButtonColorSet.call(this, eventobject);
    frmBillPayment.tbxAmount.setVisibility(true);
    frmBillPayment.tbxAmount.setEnabled(true);
    frmBillPayment.tbxAmount.placeholder = "0.00";
    frmBillPayment.tbxAmount.setFocus(true);
    frmBillPayment.lblForFullPayment.setVisibility(false);
    frmBillPayment.tbxAmount.text = commaFormatted(fullAmt);
    gblFullPayment = false;
}