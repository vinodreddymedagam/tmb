function p2kwiet2012247633536_btnunbilled_onClick_seq0(eventobject) {
    frmMFFullStatementMB.segcredit.removeAll();
    frmMFFullStatementMB.btnunbilled.skin = "btnunbilledfoc";
    frmMFFullStatementMB.btnunbilled.focusSkin = "btnunbilledfoc";
    frmMFFullStatementMB.btnbilled.skin = "btnunbilled";
    frmMFFullStatementMB.cmbMFDates.selectedKey = 0;
    frmMFFullStatementMB.hbxbilled.setVisibility(false);
    gblViewType = "F";
    currentpageStmt = 1;
    setSortBtnSkinMBMF();
    frmMBMFAcctFullStatementPreShow();
}