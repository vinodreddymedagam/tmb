function p2kwiet2012247635060_button1010778103103263_onClick_seq0(eventobject) {
    function alert_onClick_71902201224763581_True() {}

    function alert_onClick_7489201224767698_True() {}
    if ((gblMyBillerTopUpBB == 2)) {
        TMBUtil.DestroyForm(frmAddTopUpToMB)
        gblBillerCompCodeBBMB = ""
        showAccountListBBMB.call(this);
    } else {
        //#ifdef j2me
        //#define preprocessdecision_onClick_29303201224768602_j2me_bb_bb10_winphone8_palm_android_winmobile_iphone_symbian_winmobile6x
        //#endif
        //#ifdef bb
        //#define preprocessdecision_onClick_29303201224768602_j2me_bb_bb10_winphone8_palm_android_winmobile_iphone_symbian_winmobile6x
        //#endif
        //#ifdef bb10
        //#define preprocessdecision_onClick_29303201224768602_j2me_bb_bb10_winphone8_palm_android_winmobile_iphone_symbian_winmobile6x
        //#endif
        //#ifdef winphone8
        //#define preprocessdecision_onClick_29303201224768602_j2me_bb_bb10_winphone8_palm_android_winmobile_iphone_symbian_winmobile6x
        //#endif
        //#ifdef palm
        //#define preprocessdecision_onClick_29303201224768602_j2me_bb_bb10_winphone8_palm_android_winmobile_iphone_symbian_winmobile6x
        //#endif
        //#ifdef android
        //#define preprocessdecision_onClick_29303201224768602_j2me_bb_bb10_winphone8_palm_android_winmobile_iphone_symbian_winmobile6x
        //#endif
        //#ifdef winmobile
        //#define preprocessdecision_onClick_29303201224768602_j2me_bb_bb10_winphone8_palm_android_winmobile_iphone_symbian_winmobile6x
        //#endif
        //#ifdef iphone
        //#define preprocessdecision_onClick_29303201224768602_j2me_bb_bb10_winphone8_palm_android_winmobile_iphone_symbian_winmobile6x
        //#endif
        //#ifdef symbian
        //#define preprocessdecision_onClick_29303201224768602_j2me_bb_bb10_winphone8_palm_android_winmobile_iphone_symbian_winmobile6x
        //#endif
        //#ifdef winmobile6x
        //#define preprocessdecision_onClick_29303201224768602_j2me_bb_bb10_winphone8_palm_android_winmobile_iphone_symbian_winmobile6x
        //#endif
        //#ifdef preprocessdecision_onClick_29303201224768602_j2me_bb_bb10_winphone8_palm_android_winmobile_iphone_symbian_winmobile6x
        if ((kony.string.equals(gblTranxLockedForBiller, "03"))) {
            //alert(kony.i18n.getLocalizedString("keyTranxPwdLocked"))
            popTransferConfirmOTPLock.show();
        } else {
            if ((checkMaxBillerCountMB())) {
                frmAddTopUpToMB.imgAddedBiller.src = "empty.png";
                frmAddTopUpToMB.lblAddbillerName.text = "";
                frmAddTopUpToMB.lblAddedRef1.text = kony.i18n.getLocalizedString("keyRef1");
                frmAddTopUpToMB.lblAddedRef2.text = kony.i18n.getLocalizedString("keyRef2");
                frmAddTopUpToMB.show();
            } else {
                function alert_onClick_7489201224767698_Callback() {
                    alert_onClick_7489201224767698_True();
                }
                kony.ui.Alert({
                    "alertType": constants.ALERT_TYPE_ERROR,
                    "alertTitle": "",
                    "yesLabel": "Ok",
                    "noLabel": "No",
                    "message": kony.i18n.getLocalizedString("Valid_MoreThan50"),
                    "alertHandler": alert_onClick_7489201224767698_Callback
                }, {
                    "iconPosition": constants.ALERT_ICON_POSITION_LEFT
                });
            }
        }
        //#endif
        //#ifdef spaip
        //#define preprocessdecision_onClick_68952201224763562_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
        //#endif
        //#ifdef spabbnth
        //#define preprocessdecision_onClick_68952201224763562_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
        //#endif
        //#ifdef spawinphone8
        //#define preprocessdecision_onClick_68952201224763562_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
        //#endif
        //#ifdef wap
        //#define preprocessdecision_onClick_68952201224763562_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
        //#endif
        //#ifdef spaan
        //#define preprocessdecision_onClick_68952201224763562_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
        //#endif
        //#ifdef spabb
        //#define preprocessdecision_onClick_68952201224763562_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
        //#endif
        //#ifdef spawindows
        //#define preprocessdecision_onClick_68952201224763562_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
        //#endif
        //#ifdef preprocessdecision_onClick_68952201224763562_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
        if ((kony.string.equals(gblIBFlowStatus, "04"))) {
            alert(kony.i18n.getLocalizedString("Receipent_OTPLocked"))
        } else {
            if ((checkMaxBillerCountMB())) {
                frmAddTopUpToMB.imgAddedBiller.src = "empty.png";
                frmAddTopUpToMB.lblAddbillerName.text = "";
                frmAddTopUpToMB.lblAddedRef1.text = kony.i18n.getLocalizedString("keyRef1");
                frmAddTopUpToMB.lblAddedRef2.text = kony.i18n.getLocalizedString("keyRef2");
                frmAddTopUpToMB.show();
            } else {
                function alert_onClick_71902201224763581_Callback() {
                    alert_onClick_71902201224763581_True();
                }
                kony.ui.Alert({
                    "alertType": constants.ALERT_TYPE_ERROR,
                    "alertTitle": "",
                    "yesLabel": "Ok",
                    "noLabel": "No",
                    "message": kony.i18n.getLocalizedString("Valid_MoreThan50"),
                    "alertHandler": alert_onClick_71902201224763581_Callback
                }, {
                    "iconPosition": constants.ALERT_ICON_POSITION_LEFT
                });
            }
        }
        //#endif
    }
}