function p2kwiet2012247634505_btnRight_onClick_seq0(eventobject) {
    onClickContactList.call(this);
    //#ifdef spaip
    //#define preprocessdecision_onClick_90487201224769429_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
    //#endif
    //#ifdef spabbnth
    //#define preprocessdecision_onClick_90487201224769429_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
    //#endif
    //#ifdef spawinphone8
    //#define preprocessdecision_onClick_90487201224769429_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
    //#endif
    //#ifdef wap
    //#define preprocessdecision_onClick_90487201224769429_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
    //#endif
    //#ifdef spaan
    //#define preprocessdecision_onClick_90487201224769429_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
    //#endif
    //#ifdef spabb
    //#define preprocessdecision_onClick_90487201224769429_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
    //#endif
    //#ifdef spawindows
    //#define preprocessdecision_onClick_90487201224769429_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
    //#endif
    //#ifdef preprocessdecision_onClick_90487201224769429_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
    if (myRecipientsRs.length == gblMAXRecipientCount) {
        //frmMyRecipients.button47428498625.setEnabled(false);
        frmMyRecipients.btnEmailto.setEnabled(false);
        frmMyRecipients.btnPhoto.setEnabled(false);
    } else {
        //frmMyRecipients.button47428498625.setEnabled(true);
        frmMyRecipients.btnEmailto.setEnabled(true);
        frmMyRecipients.btnPhoto.setEnabled(true);
    }
    //#endif
}