function AS_Button_487aef4348b2480daa66d21d943f2f34(eventobject) {
    //#ifdef spaip
    //#define CHANNEL_CONDITION_ide_onClick_382cbab02d754255acc8c02b1af85a9a_spaip_spabbnth_spawinphone8_spaan_spabb_spawindows
    //#endif
    //#ifdef spabbnth
    //#define CHANNEL_CONDITION_ide_onClick_382cbab02d754255acc8c02b1af85a9a_spaip_spabbnth_spawinphone8_spaan_spabb_spawindows
    //#endif
    //#ifdef spawinphone8
    //#define CHANNEL_CONDITION_ide_onClick_382cbab02d754255acc8c02b1af85a9a_spaip_spabbnth_spawinphone8_spaan_spabb_spawindows
    //#endif
    //#ifdef spaan
    //#define CHANNEL_CONDITION_ide_onClick_382cbab02d754255acc8c02b1af85a9a_spaip_spabbnth_spawinphone8_spaan_spabb_spawindows
    //#endif
    //#ifdef spabb
    //#define CHANNEL_CONDITION_ide_onClick_382cbab02d754255acc8c02b1af85a9a_spaip_spabbnth_spawinphone8_spaan_spabb_spawindows
    //#endif
    //#ifdef spawindows
    //#define CHANNEL_CONDITION_ide_onClick_382cbab02d754255acc8c02b1af85a9a_spaip_spabbnth_spawinphone8_spaan_spabb_spawindows
    //#endif
    //#ifdef CHANNEL_CONDITION_ide_onClick_382cbab02d754255acc8c02b1af85a9a_spaip_spabbnth_spawinphone8_spaan_spabb_spawindows
    if ((kony.i18n.getCurrentLocale() != "en_US")) {
        gblLang_flag = "en_US";
        //#ifdef android
        //kony.application.showLoadingScreen("LocBlock",kony.i18n.getLocalizedString("keyLocaleChangeEngMessage"), "center" , true, true, false);
        showLoadingScreen();
        //#endif
        //gblLang_flag = "en_US";
        //kony.i18n.setCurrentLocaleAsync("en_US", onSuccessLocaleChange, onFailureLocaleChange, "");
        frmMBTnC.lblTandCSpa.text = "";
        setLocaleEng();
        frmMBTnC.btnEngR.skin = btnOnFocus;
        frmMBTnC.btnThaiR.skin = btnOffFocus;
        frmMBTnCPreShow();
    } else {}
    //#endif
    //#ifdef android
    //#define CHANNEL_CONDITION_ide_onClick_a065c6a2df13406ca29317121455d747_android_iphone
    //#endif
    //#ifdef iphone
    //#define CHANNEL_CONDITION_ide_onClick_a065c6a2df13406ca29317121455d747_android_iphone
    //#endif
    //#ifdef CHANNEL_CONDITION_ide_onClick_a065c6a2df13406ca29317121455d747_android_iphone
    if ((kony.i18n.getCurrentLocale() != "en_US")) {
        gblLang_flag = "en_US";
        //#ifdef android
        //kony.application.showLoadingScreen("LocBlock",kony.i18n.getLocalizedString("keyLocaleChangeEngMessage"), "center" , true, true, false);
        showLoadingScreen();
        //#endif
        showLoadingScreen();
        frmMBTnC.lblTandCEng.text = "";
        frmMBTnC.hbxTandCEng.setVisibility(true);
        frmMBTnC.hbxTandCTh.setVisibility(false);
        setLocaleEng();
        frmMBTnC.btnEngR.skin = btnOnFocus;
        frmMBTnC.btnThaiR.skin = btnOffFocus;
        frmMBTnCPreShow();
    } else {}
    //#endif
}