function p2kwiet2012247632223_txtPointsBeingRedeemed_Android_onEndEditing_seq0(eventobject, changedtext) {
    checkRedeemPointsMoreAvailablePoints.call(this);
    var redeemPoints = frmMBPointRedemptionLanding.txtPointsBeingRedeemed.text;
    if (redeemPoints != "") frmMBPointRedemptionLanding.txtPointsBeingRedeemed.text = commaFormattedPoints(Number(redeemPoints));
    else frmMBPointRedemptionLanding.txtPointsBeingRedeemed.text = "";
}