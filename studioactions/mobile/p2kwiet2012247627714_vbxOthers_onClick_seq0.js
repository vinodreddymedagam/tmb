function p2kwiet2012247627714_vbxOthers_onClick_seq0(eventobject) {
    if ((gblActivityIds == "27" || gblActivityIds == "28" || gblActivityIds == "027" || gblActivityIds == "028")) {
        //#ifdef iphone
        //#define preprocessdecision_onClick_10924201224762476_iphone
        //#endif
        //#ifdef preprocessdecision_onClick_10924201224762476_iphone
        shareIntentmoreCall.call(this, "more", "BillPayment");
        //#endif
        //#ifdef android
        //#define preprocessdecision_onClick_1475201224764890_android
        //#endif
        //#ifdef preprocessdecision_onClick_1475201224764890_android
        shareIntentmoreCallandroid.call(this, "more", "BillPayment");
        //#endif
    } else {
        //#ifdef iphone
        //#define preprocessdecision_onClick_23405201224764154_iphone
        //#endif
        //#ifdef preprocessdecision_onClick_23405201224764154_iphone
        shareIntentmoreCall.call(this, "more", "TopupPayment");
        //#endif
        //#ifdef android
        //#define preprocessdecision_onClick_76203201224769788_android
        //#endif
        //#ifdef preprocessdecision_onClick_76203201224769788_android
        shareIntentmoreCallandroid.call(this, "more", "TopupPayment");
        //#endif
    }
}