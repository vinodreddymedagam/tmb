function p2kwiet2012247628805_lblTargetAmntVal_onDone_seq0(eventobject, changedtext) {
    if (frmDreamSavingEdit.lblTargetAmntVal.text == "0") {
        frmDreamSavingEdit.button86682510925150.setEnabled(false);
        frmDreamSavingEdit.button86682510925150.skin = "buttonCalDis";
    }
    frmDreamSavingEdit.lblTargetAmntVal.skin = "txtNormalBG";
    if (frmDreamSavingEdit.lblTargetAmntVal.text != "0") {
        frmDreamSavingEdit.button86682510925150.setEnabled(true);
        frmDreamSavingEdit.button86682510925150.skin = "btnCalculate";
    }
}