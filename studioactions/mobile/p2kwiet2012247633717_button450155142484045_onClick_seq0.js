function p2kwiet2012247633717_button450155142484045_onClick_seq0(eventobject) {
    myAccountListService.call(this);
    frmMyAccntConfirmationAddAccount.hbxftraddmore.setVisibility(false);
    frmMyAccntConfirmationAddAccount.hbxcnfrmftr.setVisibility(true);
    //
    TMBUtil.DestroyForm(frmMyAccntConfirmationAddAccount);
    // Clearing cache data
    NON_TMB_ADD = 0; // Non TMB accnt in cache - global var
    TOTAL_AC_ADD = 0; // Total accnt in cache - global var
    gblMyAccntAddTmpData = []; // Cache memory - global array
    //Navigate to add accnt form
    frmMyAccntAddAccount.show();
    //showfooter(kony.i18n.getLocalizedString("keyCancelButton"),kony.i18n.getLocalizedString("Next"),showMyAccntList,showConfirmationAddaccount);
}