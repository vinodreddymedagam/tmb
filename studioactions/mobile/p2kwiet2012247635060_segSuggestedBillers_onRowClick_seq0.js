function p2kwiet2012247635060_segSuggestedBillers_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    function alert_onRowClick_10203201224763559_True() {}
    if ((gblMyBillerTopUpBB == 2)) {
        TMBUtil.DestroyForm(frmAddTopUpToMB);
        onSelectSugBillerBB.call(this);
        showAccountListBBMB.call(this);
    } else {
        if ((gblBlockBillerTopUpAdd)) {
            kony.print("Adding biller is disabled");
        } else {
            if ((checkMaxBillerCountMB())) {
                onclickOfSuggestedBillersAdd.call(this);
            } else {
                function alert_onRowClick_10203201224763559_Callback() {
                    alert_onRowClick_10203201224763559_True();
                }
                kony.ui.Alert({
                    "alertType": constants.ALERT_TYPE_ERROR,
                    "alertTitle": "",
                    "yesLabel": "Ok",
                    "noLabel": "No",
                    "message": kony.i18n.getLocalizedString("Valid_MoreThan50"),
                    "alertHandler": alert_onRowClick_10203201224763559_Callback
                }, {
                    "iconPosition": constants.ALERT_ICON_POSITION_LEFT
                });
            }
        }
    }
}