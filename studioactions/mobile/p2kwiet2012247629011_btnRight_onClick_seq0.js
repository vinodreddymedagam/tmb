function p2kwiet2012247629011_btnRight_onClick_seq0(eventobject) {
    if (!frmEditFutureBillPaymentComplete.hboxSharelist.isVisible) {
        frmEditFutureBillPaymentComplete.hboximg.setVisibility(true);
        frmEditFutureBillPaymentComplete.hboxSharelist.setVisibility(true);
    } else {
        frmEditFutureBillPaymentComplete.hboximg.setVisibility(false);
        frmEditFutureBillPaymentComplete.hboxSharelist.setVisibility(false);
    }
}