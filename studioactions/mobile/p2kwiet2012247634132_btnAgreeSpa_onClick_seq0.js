function p2kwiet2012247634132_btnAgreeSpa_onClick_seq0(eventobject) {
    //#ifdef j2me
    //#define preprocessdecision_onClick_22942201224761944_j2me_bb_winphone8_palm_android_iphone_winmobile_symbian_winmobile6x
    //#endif
    //#ifdef bb
    //#define preprocessdecision_onClick_22942201224761944_j2me_bb_winphone8_palm_android_iphone_winmobile_symbian_winmobile6x
    //#endif
    //#ifdef winphone8
    //#define preprocessdecision_onClick_22942201224761944_j2me_bb_winphone8_palm_android_iphone_winmobile_symbian_winmobile6x
    //#endif
    //#ifdef palm
    //#define preprocessdecision_onClick_22942201224761944_j2me_bb_winphone8_palm_android_iphone_winmobile_symbian_winmobile6x
    //#endif
    //#ifdef android
    //#define preprocessdecision_onClick_22942201224761944_j2me_bb_winphone8_palm_android_iphone_winmobile_symbian_winmobile6x
    //#endif
    //#ifdef iphone
    //#define preprocessdecision_onClick_22942201224761944_j2me_bb_winphone8_palm_android_iphone_winmobile_symbian_winmobile6x
    //#endif
    //#ifdef winmobile
    //#define preprocessdecision_onClick_22942201224761944_j2me_bb_winphone8_palm_android_iphone_winmobile_symbian_winmobile6x
    //#endif
    //#ifdef symbian
    //#define preprocessdecision_onClick_22942201224761944_j2me_bb_winphone8_palm_android_iphone_winmobile_symbian_winmobile6x
    //#endif
    //#ifdef winmobile6x
    //#define preprocessdecision_onClick_22942201224761944_j2me_bb_winphone8_palm_android_iphone_winmobile_symbian_winmobile6x
    //#endif
    //#ifdef preprocessdecision_onClick_22942201224761944_j2me_bb_winphone8_palm_android_iphone_winmobile_symbian_winmobile6x
    if ((gblUserLockStatusMB == gblFinancialTxnMBLock)) {
        alertUserStatusLocked.call(this);
    } else {
        popTransactionPwd.btnPopTransactionCancel.onClick = transCancelAddAccount;
        popTransactionPwd.btnPopTransactionConf.onClick = addAccountRecipentCompletePre; //transConfirmAddAccount;;
        popTransactionPwd.tbxPopTransactionPwd.text = "";
        popTransactionPwd.lblPopTranscationMsg.text = kony.i18n.getLocalizedString("keyTransPwdMsg");
        popTransactionPwd.lblPopTransationPwd.text = kony.i18n.getLocalizedString("transPasswordSub");
        popTransactionPwd.btnPopTransactionCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
        popTransactionPwd.btnPopTransactionConf.text = kony.i18n.getLocalizedString("keyConfirm");
        popTransactionPwd.show()
    }
    //#endif
    //#ifdef spaip
    //#define preprocessdecision_onClick_16183201224767097_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
    //#endif
    //#ifdef spabbnth
    //#define preprocessdecision_onClick_16183201224767097_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
    //#endif
    //#ifdef spawinphone8
    //#define preprocessdecision_onClick_16183201224767097_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
    //#endif
    //#ifdef wap
    //#define preprocessdecision_onClick_16183201224767097_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
    //#endif
    //#ifdef spaan
    //#define preprocessdecision_onClick_16183201224767097_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
    //#endif
    //#ifdef spabb
    //#define preprocessdecision_onClick_16183201224767097_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
    //#endif
    //#ifdef spawindows
    //#define preprocessdecision_onClick_16183201224767097_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
    //#endif
    //#ifdef preprocessdecision_onClick_16183201224767097_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
    if ((gblIBFlowStatus == "04")) {
        alertUserStatusLocked.call(this);
    } else {
        var inputParams = {}
        spaChnage = "addrecipients"
        gblFlagTransPwdFlow = "addAccount"
        gblOTPFlag = true;
        locale = kony.i18n.getCurrentLocale();
        try {
            kony.timer.cancel("otpTimer")
        } catch (e) {
            kony.print("Timer doesnot exist");
        }
        if (isRecipientNew) {
            gblSpaChannel = "AddRecipient";
        } else {
            gblSpaChannel = "EditRecipient";
        }
        if (isRecipientNew == false) {
            if (locale == "en_US") {
                SpaEventNotificationPolicy = "MIB_EditRecipient_EN";
                SpaSMSSubject = "MIB_EditRecipient_EN";
            } else {
                SpaEventNotificationPolicy = "MIB_EditRecipient_TH";
                SpaSMSSubject = "MIB_EditRecipient_TH";
            }
        } else {
            if (locale == "en_US") {
                SpaEventNotificationPolicy = "MIB_AddRecipient_EN";
                SpaSMSSubject = "MIB_AddRecipient_EN";
            } else {
                SpaEventNotificationPolicy = "MIB_AddRecipient_TH";
                SpaSMSSubject = "MIB_AddRecipient_TH";
            }
        }
        var mobNo = frmMyRecipientAddAccConf.lblMobile.text;
        if (mobNo != null && mobNo != "" && mobNo != undefined) {
            mobNo = kony.string.replace(mobNo, "-", "");
            gblAccountDetails = "Mobile x" + mobNo.substring(6, 10);
        }
        gblToSpaAccountName = frmMyRecipientAddAccConf.lblName.text;
        gblAccountDetails = getAccountDetailMessageSpa(frmMyRecipientAddAccConf.segMyRecipientDetail.data) + " " + gblAccountDetails;
        saveRecipientDeatilsMB.call(this);
    }
    //#endif
}