function p2kwiet15245716321319_btnRight_onClick_seq0(eventobject) {
    if (frmBillPaymentComplete.hbxShareOption.isVisible) {
        frmBillPaymentComplete.hbxShareOption.isVisible = false;
        frmBillPaymentComplete.imgHeaderMiddle.src = "arrowtop.png";
    } else {
        frmBillPaymentComplete.hbxShareOption.isVisible = true;
        frmBillPaymentComplete.imgHeaderMiddle.src = "empty.png";
    }
}