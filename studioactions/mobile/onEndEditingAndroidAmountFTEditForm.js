function onEndEditingAndroidAmountFTEditForm(eventobject, changedtext) {
    return AS_TextField_dd4192a57a764d72b02d970ea8d7d495(eventobject, changedtext);
}

function AS_TextField_dd4192a57a764d72b02d970ea8d7d495(eventobject, changedtext) {
    if (frmMBFTEdit.txtEditAmnt.text == "") {
        frmMBFTEdit.txtEditAmnt.text = "0.00" + kony.i18n.getLocalizedString("currencyThaiBaht");
    } else {
        frmMBFTEdit.txtEditAmnt.text = commaFormatted(parseFloat(removeCommos(frmMBFTEdit.txtEditAmnt.text)).toFixed(2)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
    }
}