function p2kwiet2012247633717_button473361467792209_onClick_seq0(eventobject) {
    myAccountListService.call(this);
    TMBUtil.DestroyForm(frmMyAccntConfirmationAddAccount);
    if (flowSpa) {
        frmMyAccntConfirmationAddAccount.hbox473361467792157.isVisible = true;
        frmMyAccntConfirmationAddAccount.hbox473361467792205.isVisible = false;
    } else {
        frmMyAccntConfirmationAddAccount.hbxcnfrmftr.isVisible = true;
        frmMyAccntConfirmationAddAccount.hbxftraddmore.isVisible = false;
    }
    // Clearing cache data
    NON_TMB_ADD = 0; // Non TMB accnt in cache - global var
    TOTAL_AC_ADD = 0; // Total accnt in cache - global var
    gblMyAccntAddTmpData = []; // Cache memory - global array
    //Navigate to add accnt form
    frmMyAccntAddAccount.show();
    //showfooter(kony.i18n.getLocalizedString("keyCancelButton"),kony.i18n.getLocalizedString("Next"),showMyAccntList,showConfirmationAddaccount);
}