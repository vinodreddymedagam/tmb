function p2kwiet2012247626847_btnShow_onClick_seq0(eventobject) {
    if ((gblMyBillerTopUpBB == 2)) {
        frmMyTopUpList.lblSuggestedBillersText.setVisibility(true)
            //frmMyTopUpList.segBillersList.setData(gblCustomerBBMBInqRs);
        frmMyTopUpList.segBillersList.removeAll();
        frmMyTopUpList.lblHdrTxt.text = kony.i18n.getLocalizedString("keyBillPaymentSelectBill");
        showLoadingScreen();
        gblBBorBillers = false
        populateMySuggestListBBMB(gblmasterBillerAndTopupBBMB);
        getMyBillListMBBB.call(this);
    } else {
        if (flowSpa) {
            TMBUtil.DestroyForm(frmMyTopUpSelect);
            frmMyTopUpSelect.hboxMenuHeader = null;
            isMenuShown = false;
        }
        frmMyTopUpSelect.show();
    }
}