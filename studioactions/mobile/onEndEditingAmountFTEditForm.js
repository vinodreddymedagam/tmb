function onEndEditingAmountFTEditForm(eventobject, changedtext) {
    return AS_TextField_ba14749bc28f4059abed5974c13eb470(eventobject, changedtext);
}

function AS_TextField_ba14749bc28f4059abed5974c13eb470(eventobject, changedtext) {
    if (frmMBFTEdit.txtEditAmnt.text == "") {
        frmMBFTEdit.txtEditAmnt.text = "0.00" + kony.i18n.getLocalizedString("currencyThaiBaht");
    } else {
        frmMBFTEdit.txtEditAmnt.text = onDoneEditingAmountValue(frmMBFTEdit.txtEditAmnt.text) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
    }
}