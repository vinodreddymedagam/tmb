function p2kwiet2012247634837_btnNext_onClick_seq0(eventobject) {
    if (fl == 1) {
        previousSelectedItems["imgchbxbtn"] = "radiobtn2.png";
        if (previousSelectedItems["btnMobileNo1"]) {
            previousSelectedItems["btnMobileNo1"]["skin"] = "btnRadioFoc";
        }
        if (previousSelectedItems["btnMobileNo2"]) {
            previousSelectedItems["btnMobileNo2"]["skin"] = "btnRadio";
        }
        if (previousSelectedItems["btnMobileNo3"]) {
            previousSelectedItems["btnMobileNo3"]["skin"] = "btnRadio";
        }
        frmMyRecipientSelectMobile.segMyRecipient.setDataAt(previousSelectedItems, previousSelectedIndex);
        //frmMyRecipientSelectMobile.segMyRecipient.removeAt(previousSelectedIndex);
        //frmMyRecipientSelectMobile.segMyRecipient.addDataAt(previousSelectedItems, previousSelectedIndex);
    }
    previousSelectedIndex = false;
    previousSelectedItems = false;
    fl = null;
    if (requestFromForm == "editprofile") {
        if (selectedMobile) {
            frmMyRecipientEditProfile.tbxMobileNo.text = selectedMobile;
        } else {
            frmMyRecipientEditProfile.tbxMobileNo.text = "";
        }
        frmMyRecipientEditProfile.show();
    } else if (requestFromForm == "addprofile") {
        if (selectedMobile) {
            frmMyRecipientAddProfile.tbxMobileNo.text = selectedMobile;
        } else {
            frmMyRecipientAddProfile.tbxMobileNo.text = "";
        }
        gblRequestFromForm = "addprofile";
        frmMyRecipientAddProfile.show();
    }
    contactListToggle = true;
}