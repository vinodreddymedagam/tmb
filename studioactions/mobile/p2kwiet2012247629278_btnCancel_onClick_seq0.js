function p2kwiet2012247629278_btnCancel_onClick_seq0(eventobject) {
    function alert_onClick_6675201224764809_True() {
        if ((GblBillTopFlag)) {
            gblFirstTimeBillPayment = true;
            gblPaynow = true;
            undefined.show();
        } else {
            gblFirstTimeTopUp = true;
            frmTopUp.show();
        }
    }

    function alert_onClick_6675201224764809_False() {}

    function alert_onClick_6675201224764809_Callback(response) {
        if (response === true) {
            alert_onClick_6675201224764809_True();
        } else {
            alert_onClick_6675201224764809_False();
        }
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": "Cancel Transaction",
        "yesLabel": "Yes",
        "noLabel": "No",
        "message": "Do you really wish to cancel?",
        "alertHandler": alert_onClick_6675201224764809_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    });
}