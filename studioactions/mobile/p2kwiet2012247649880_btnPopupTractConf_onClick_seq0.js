function p2kwiet2012247649880_btnPopupTractConf_onClick_seq0(eventobject) {
    onpopconfirmaccnt.call(this);
    popAccntConfirmation.dismiss();
    hbxfooter.setVisibility(false);
    frmMyAccntConfirmationAddAccount.lblHdrTxt.text = kony.i18n.getLocalizedString("keylblComplete");
    frmMyAccntConfirmationAddAccount.btnAddHeader.setVisibility(false);
    hideDelete.call(this);
}