function p2kwiet2012247626847_button15633509701481_onClick_seq0(eventobject) {
    function alert_onClick_27922012247670_True() {}

    function alert_onClick_72719201224765217_True() {}
    frmAddTopUpToMB.txbNickName.skin = txtNormalBG;
    frmAddTopUpToMB.txbNickName.focusSkin = txtFocusBG;
    frmAddTopUpToMB.txtRef1.skin = txtNormalBG;
    frmAddTopUpToMB.txtRef1.focusSkin = txtFocusBG;
    if ((gblMyBillerTopUpBB == 2)) {
        if ((isMenuShown == false)) {
            if ((gblMyBillerTopUpBB == 2)) {
                onClickNextMBApplyBB.call(this);
                //#ifdef j2me
                //#define preprocessdecision_onClick_82011201224761700_j2me_bb_winphone8_palm_android_winmobile_iphone_symbian_winmobile6x
                //#endif
                //#ifdef bb
                //#define preprocessdecision_onClick_82011201224761700_j2me_bb_winphone8_palm_android_winmobile_iphone_symbian_winmobile6x
                //#endif
                //#ifdef winphone8
                //#define preprocessdecision_onClick_82011201224761700_j2me_bb_winphone8_palm_android_winmobile_iphone_symbian_winmobile6x
                //#endif
                //#ifdef palm
                //#define preprocessdecision_onClick_82011201224761700_j2me_bb_winphone8_palm_android_winmobile_iphone_symbian_winmobile6x
                //#endif
                //#ifdef android
                //#define preprocessdecision_onClick_82011201224761700_j2me_bb_winphone8_palm_android_winmobile_iphone_symbian_winmobile6x
                //#endif
                //#ifdef winmobile
                //#define preprocessdecision_onClick_82011201224761700_j2me_bb_winphone8_palm_android_winmobile_iphone_symbian_winmobile6x
                //#endif
                //#ifdef iphone
                //#define preprocessdecision_onClick_82011201224761700_j2me_bb_winphone8_palm_android_winmobile_iphone_symbian_winmobile6x
                //#endif
                //#ifdef symbian
                //#define preprocessdecision_onClick_82011201224761700_j2me_bb_winphone8_palm_android_winmobile_iphone_symbian_winmobile6x
                //#endif
                //#ifdef winmobile6x
                //#define preprocessdecision_onClick_82011201224761700_j2me_bb_winphone8_palm_android_winmobile_iphone_symbian_winmobile6x
                //#endif
                //#ifdef preprocessdecision_onClick_82011201224761700_j2me_bb_winphone8_palm_android_winmobile_iphone_symbian_winmobile6x
                if ((gblUserLockStatusMB == gblFinancialTxnMBLock)) {
                    alertUserStatusLocked.call(this);
                } else {
                    popTransactionPwd.btnPopTransactionCancel.onClick = transCancelAddAccount;
                    popTransactionPwd.btnPopTransactionConf.onClick = addTopupCompletePre; //transConfirmAddAccount;;
                    popTransactionPwd.tbxPopTransactionPwd.text = "";
                    popTransactionPwd.lblPopTranscationMsg.text = kony.i18n.getLocalizedString("keyTransPwdMsg");
                    popTransactionPwd.lblPopTransationPwd.text = kony.i18n.getLocalizedString("transPasswordSub");
                    popTransactionPwd.btnPopTransactionCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
                    popTransactionPwd.btnPopTransactionConf.text = kony.i18n.getLocalizedString("keyConfirm");
                    popTransactionPwd.tbxPopTransactionPwd.skin = tbxPopupBlue;
                    popTransactionPwd.tbxPopTransactionPwd.focusSkin = tbxPopupBlue;
                    popTransactionPwd.tbxPopTransactionPwd.text = "";
                    popTransactionPwd.tbxPopTransactionPwd.skin = tbxPopupBlue;
                    popTransactionPwd.lblPopTranscationMsg.skin = lblPopupLabelTxt;
                    popTransactionPwd.show()
                }
                //#endif
            } else {
                if ((checkDuplicateNicknameOnAddTopUp())) {
                    nicknameRef1Ref2ValidationCheck.call(this);
                } else {
                    function alert_onClick_72719201224765217_Callback() {
                        alert_onClick_72719201224765217_True();
                    }
                    kony.ui.Alert({
                        "alertType": constants.ALERT_TYPE_ERROR,
                        "alertTitle": "",
                        "yesLabel": "Yes",
                        "noLabel": "No",
                        "message": "Duplicate Nickname !",
                        "alertHandler": alert_onClick_72719201224765217_Callback
                    }, {
                        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
                    });
                }
            }
        } else {
            frmBBPaymentApply.scrollboxMain.scrollToEnd();
            isMenuShown = false;
        }
    } else {
        if ((isMenuShown == false)) {
            if ((frmAddTopUpToMB.txbNickName.text.length > 0 && isBillerSelected())) {
                if ((checkDuplicateNicknameOnAddTopUp())) {
                    addBillerTopupValidationsMB.call(this);
                } else {
                    function alert_onClick_27922012247670_Callback() {
                        alert_onClick_27922012247670_True();
                    }
                    kony.ui.Alert({
                        "alertType": constants.ALERT_TYPE_ERROR,
                        "alertTitle": "",
                        "yesLabel": "Ok",
                        "noLabel": "No",
                        "message": kony.i18n.getLocalizedString("Valid_DuplicateNickname"),
                        "alertHandler": alert_onClick_27922012247670_Callback
                    }, {
                        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
                    });
                }
            } else {
                if (!isBillerSelected()) {
                    alert(kony.i18n.getLocalizedString("KeyPlzSelBiller"));
                } else {
                    frmAddTopUpToMB.txbNickName.skin = txtErrorBG;
                    frmAddTopUpToMB.txbNickName.focusSkin = txtErrorBG;
                    alert(kony.i18n.getLocalizedString("Valid_BillerNicknameMandatory"));
                }
            }
        } else {
            frmAddTopUpToMB.scrollboxMain.scrollToEnd();
            isMenuShown = false;
        }
    }
}