var gblEDonation = [];
function frmEDonationPaymentConfirm_init_Action(){
  frmEDonationPaymentConfirm.preShow = frmEDonationPaymentConfirm_preShow_Action;
  frmEDonationPaymentConfirm.btnBackArrow.onClick = frmEDonationPaymentConfirm_btnBackArrow_Action;
  frmEDonationPaymentConfirm.btnBack.onClick = frmEDonationPaymentConfirm_btnBack_Action;
  frmEDonationPaymentConfirm.btnNext.onClick = frmEDonationPaymentConfirm_btnNext_Action;
  frmEDonationPaymentConfirm.lblChangeAcct.onTouchEnd = frmEDonationPaymentConfirm_lblChangeAcct_Action;
  frmEDonationPaymentConfirm.postShow = frmEDonationPaymentConfirm_postShow_Action;
  
  frmEDonationPaymentConfirm.onDeviceBack = disableBackButton;
}

function frmEDonationPaymentConfirm_preShow_Action(){
  var locale = kony.i18n.getCurrentLocale();
  changeStatusBarColor();
  frmEDonationPaymentConfirm.lblConfirmationHeader.text = kony.i18n.getLocalizedString("keyConfirmation");
  frmEDonationPaymentConfirm.lblFrom.text = kony.i18n.getLocalizedString("MB_eDFromAcct");
  if (locale == "en_US"){
        frmEDonationPaymentConfirm.lblFromAcctName.text = gblQRPayData["custAcctName"];
        if(gblQRPayData["billerCompCode"].length <= 4){
      		frmEDonationPaymentConfirm.lblBillerName.text = gblQRPayData["billerNameEN"];
      	} else {
      		frmEDonationPaymentConfirm.lblBillerName.text = gblQRPayData["toAccountName"];      	
      	}
  		frmEDonationPaymentConfirm.lblBillerCompCode.text = gblQRPayData["billerCompCode"];
  } else if (locale == "th_TH") {
        frmEDonationPaymentConfirm.lblFromAcctName.text = gblQRPayData["custAcctName"];
        if(gblQRPayData["billerCompCode"].length <= 4){
      		frmEDonationPaymentConfirm.lblBillerName.text = gblQRPayData["billerNameTH"];
      	} else {
      		frmEDonationPaymentConfirm.lblBillerName.text = gblQRPayData["toAccountName"];
      	}
  		frmEDonationPaymentConfirm.lblBillerCompCode.text = gblQRPayData["billerCompCode"];
  }
  frmEDonationPaymentConfirm.lblChangeAcct.text = kony.i18n.getLocalizedString("MB_eDChangeAcctLink");
  frmEDonationPaymentConfirm.lblTo.text = kony.i18n.getLocalizedString("MB_eDToBiller");
  frmEDonationPaymentConfirm.lblAmtDesc.text = kony.i18n.getLocalizedString("MB_eDAmountLab");
  frmEDonationPaymentConfirm.lblAmountTxt.text = Formatdecimalamount(commaFormatted(parseFloat(removeCommos(gblEDonation["amount"])).toFixed(2)));// + " " +kony.i18n.getLocalizedString("currencyThaiBaht");
  if(gblTaxDec == "T"){
    frmEDonationPaymentConfirm.lblTaxDeclared.setVisibility(true);
    frmEDonationPaymentConfirm.lblTaxDeclared.text = kony.i18n.getLocalizedString("MB_eDTax");
  }else{
    frmEDonationPaymentConfirm.lblTaxDeclared.setVisibility(false);
  }
  frmEDonationPaymentConfirm.lblPaymentDate.text = removeColonFromEnd(kony.i18n.getLocalizedString("keyPaymentDateTime"));
  frmEDonationPaymentConfirm.lblTransactionRefNo.text = removeColonFromEnd(kony.i18n.getLocalizedString("keyTransactionRefNo"));
  frmEDonationPaymentConfirm.transactionRefNoVal.text = gblbilPayRefNum;
  frmEDonationPaymentConfirm.flxSection2.setVisibility(false);
  frmEDonationPaymentConfirm.flxLineBox2.setVisibility(false);
  frmEDonationPaymentConfirm.flxSection3.setVisibility(false);
  frmEDonationPaymentConfirm.btnBack.text = kony.i18n.getLocalizedString("MB_eDBackBtn");
  frmEDonationPaymentConfirm.btnNext.text = kony.i18n.getLocalizedString("keyConfirm");
}

function frmEDonationPaymentConfirm_postShow_Action(){
  addAccessPinKeypad(frmEDonationPaymentConfirm);
  dismissLoadingScreen();
}

function frmEDonationPaymentConfirm_btnBackArrow_Action(){
  showLoadingScreen();
  frmDonationSelectAmount.show();
  dismissLoadingScreen();
}

function frmEDonationPaymentConfirm_btnBack_Action(){
  showLoadingScreen();
  frmDonationSelectAmount.show();
  dismissLoadingScreen();
}

function frmEDonationPaymentConfirm_btnNext_Action(){
  showLoadingScreen();
  validate_eDonation();
  dismissLoadingScreen();
}

function frmEDonationPaymentConfirm_lblChangeAcct_Action(){
  closeApprovalKeypad();
  frmEDonationOnclickSeletAccount();
}

function frmEDonationOnclickSeletAccount(){  //MKI,edonation change start
  
   if(isSignedUser){
      onSelectFromAccntEDonation();
    } else {
      
      gblQrSetDefaultAccnt = true;
	  
      if(frmMBPreLoginAccessesPin.FlexQuickBalanceContainer.left == "0%"){
        frmMBPreLoginAccessesPin.FlexQuickBalanceContainer.left="-100%";
        frmMBPreLoginAccessesPin.FlexLoginContainer.left="0%";
      }
      frmMBPreLoginAccessesPin.flexCrossmark.setVisibility(true);
      frmMBPreLoginAccessesPin.flexHeader.setVisibility(false);
      frmMBPreLoginAccessesPin.flexSwipeContainer.top = "20%"
      frmMBPreLoginAccessesPin.FlexContainer0b16eb3557e0043.setVisibility(false);
      frmMBPreLoginAccessesPin.imgQRHeaderLogo.setVisibility(true);
      frmMBPreLoginAccessesPin.LabelLoginForQR.setVisibility(true);
      frmMBPreLoginAccessesPin.LabelLoginForQR.text = kony.i18n.getLocalizedString("msgeLoginforQRTransaction");
      frmMBPreLoginAccessesPin.FlexLine.setVisibility(true);
      frmMBPreLoginAccessesPin.FlexLoginContainer.top = "9%"; 
	  showAccessPinScreenQR();
    }  //MKI,edonation change end
}

function getFeqLocale(){
  	if(gblScheduleRepeatBP == kony.i18n.getLocalizedString("keyDailyNE")){
	   	return kony.i18n.getLocalizedString("keyDaily");	   
	}else if(gblScheduleRepeatBP == kony.i18n.getLocalizedString("keyWeeklyNE")){
	   	return kony.i18n.getLocalizedString("keyWeekly");
	}else if(gblScheduleRepeatBP == kony.i18n.getLocalizedString("keyMonthlyNE")){
	   	return kony.i18n.getLocalizedString("keyMonthly");
	}else if(gblScheduleRepeatBP == kony.i18n.getLocalizedString("keyYearlyNE")){
	   	return kony.i18n.getLocalizedString("keyYearly");
	} else
		return kony.i18n.getLocalizedString("keyOnce");
}

function showEDonate(){
  gblEDonation = [];
  //invokeEDonationDefaultAccountService();
  //Set Value for Display
  frmEDonationPaymentConfirm.show();
}

function validate_eDonation() {
  var amount = gblqrToAccntBal.replace(/,/g, "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim();
  var serviceAmount = parseFloat(amount);
  var inputAmount = parseFloat(gblEDonation["amount"].replace(/,/g, "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim());
  kony.print("serviceAmount="+serviceAmount);
  kony.print("inputAmount="+inputAmount);
  if (inputAmount > 0.00) {
    if (inputAmount > serviceAmount) {
	  showAlertWithCallBack(kony.i18n.getLocalizedString("MB_eDLessAvailBal"), kony.i18n.getLocalizedString("info"));
      return false;
    } else {
      if (isSignedUser) {
        gblqrflow="postLogin";
        gblqrTransferAmnt = gblEDonation["amount"];
        kony.print("validateEDonation   showAccessPinScreenKeypad ");
        showAccessPinScreenKeypad();
      }else{
        gblqrflow="preLogin";
        gblqrTransferAmnt = gblEDonation["amount"];
        kony.print("validateEDonation PreLogin  showAccessPinScreenKeypad ");
        showAccessPinScreenKeypad();
      }
    }
  } else {
     showAlertWithCallBack(kony.i18n.getLocalizedString("MB_eDLessAvailBal"), kony.i18n.getLocalizedString("info"));
  	 return false;
  }
}

function verifyBillPaymentEdonation(tranPassword) {
    var inputParam = {};

    showLoadingScreen();
    inputParam["channel"] = "rc";
  	inputParam["EDonationFlag"] = "Y";

    inputParam["verifyPwdMB_appID"] = appConfig.appId;
    inputParam["verifyPwdMB_channel"] = "MB";
    inputParam["password"] = tranPassword;

    inputParam["verifyPwdMB_appID"] = appConfig.appId;
    inputParam["billerMethod"] = "0";

    inputParam["isPayNow"] = "true";
    inputParam["compCode"] = gblQRPayData["billerCompCode"]; //gblQRPayData["billerID"];

    inputParam["isPayPremium"] = "";
    inputParam["policyNumber"] = "";
    inputParam["NotificationAdd_source"] = "billpaymentnow";
    kony.print("$$$$ Till Here1");
    //var transferFee = frmBillPaymentConfirmationFuture.lblPaymentFeeValue.text;
    var transferAmount = gblqrTransferAmnt;

    var fromAcctID = gblQRPayData["fromAcctID"];
    var frmAcct = fromAcctID.replace(/-/g, "");
    var toAcct = gblQRPayData["toAcctIDNoFormat"];
    gblBillerMethod = 0;
    gblreccuringDisablePay = "Y";
    if (gblBillerMethod == 0) {

        var invoice;
        if (gblreccuringDisablePay == "Y") {
            invoice = gblQRPayData["ref2"]; //no ref 2 for topup?
        } else {
            invoice = ""; //workaround
        }
        inputParam["promptPayETEAdd_fromAcctNo"] = frmAcct;
        inputParam["promptPayETEAdd_toAcctNo"] = toAcct;
        inputParam["promptPayETEAdd_fromFIIdent"] = "";
        inputParam["promptPayETEAdd_transferAmt"] = transferAmount;
        inputParam["promptPayETEAdd_feeValue"] = gblQRPayData["fee"];
        inputParam["promptPayETEAdd_referenceNum1"] = gblQRPayData["ref1"];
        if(gbleDonationType == "QR_Type" || gbleDonationType == "Barcode_Type"){
		    if(gblTaxDec == "T"){
			    inputParam["promptPayETEAdd_referenceNum2"] = gblCustomerIDValue;
			}else{
			    inputParam["promptPayETEAdd_referenceNum2"] = "0";
			}
		} else {
	        inputParam["promptPayETEAdd_referenceNum2"] = gblQRPayData["ref2"];
	    }
        inputParam["promptPayETEAdd_EPAYCode"] = "";
        inputParam["promptPayETEAdd_CompCode"] = ""; //gblQRPayData["billerID"];
        inputParam["promptPayETEAdd_billPay"] = "billPay";
        inputParam["UsageLimit"] = "0";
        inputParam["promptPayETEAdd_appID"] = appConfig.appId;
        inputParam["promptPayETEAdd_channel"] = "MB";

    }

    kony.print("$$$$ Till Here2");
  if(gblQRPayData["fee"] == undefined || gblQRPayData["fee"] == "" || gblQRPayData["fee"] == "0.00"){
    var diplayFee = "0.00";
   }else{
    var diplayFee = gblQRPayData["fee"];
   }
  var lblRefTH1 = gblQRDefaultAccountResponse["labelRef1TH"];
      if(lblRefTH1 == "" || lblRefTH1 == undefined){
        lblRefTH1 = kony.i18n.getLocalizedString("Reference1");
      }
      var lblsearchRefTH1 = lblRefTH1.search(":");
      if(lblsearchRefTH1 == -1){
        var gblQrRef1LblTH1 = lblRefTH1+": "+gblQRPayData["ref1"];
      }else{
        var gblQrRef1LblTH1 = lblRefTH1+" "+gblQRPayData["ref1"];
      }
  
  var lblRefEN1 = gblQRDefaultAccountResponse["labelRef1EN"];
      if(lblRefEN1 == "" || lblRefEN1 == undefined){
        lblRefEN1 = kony.i18n.getLocalizedString("Reference1");
      }
      var lblsearchRefEN1 = lblRefEN1.search(":");
      if(lblsearchRefEN1 == -1){
        var gblQrRef1LblEN1 = lblRefEN1+": "+gblQRPayData["ref1"];
      }else{
        var gblQrRef1LblEN1 = lblRefEN1+" "+gblQRPayData["ref1"];
      }
  if(gblQRPayData["ref2"] == undefined || gblQRPayData["ref2"] == ""){
    var gblQrRef2Lbl2 = "";
  }else{
    var lblRefTH2 = gblQRDefaultAccountResponse["labelRef2TH"];
      if(lblRefTH2 == "" || lblRefTH2 == undefined){
        lblRefTH2 = kony.i18n.getLocalizedString("Reference2");
      }
      var lblsearchRefTH2 = lblRefTH1.search(":");
      if(lblsearchRefTH2 == -1){
        var gblQrRef1LblTH2 = lblRefTH2+": "+gblQRPayData["ref2"];
      }else{
        var gblQrRef1LblTH2 = lblRefTH2+" "+gblQRPayData["ref2"];
      }
  
  var lblRefEN2 = gblQRDefaultAccountResponse["labelRef2EN"];
      if(lblRefEN2 == "" || lblRefEN2 == undefined){
        lblRefEN2 = kony.i18n.getLocalizedString("Reference2");
      }
      var lblsearchRefEN2 = lblRefEN2.search(":");
      if(lblsearchRefEN2 == -1){
        var gblQrRef1LblEN2 = lblRefEN2+": "+gblQRPayData["ref2"];
      }else{
        var gblQrRef1LblEN2 = lblRefEN2+" "+gblQRPayData["ref2"];
      }
  }
  kony.print("diplayFee  "+diplayFee);
  if(gblQrRef1LblEN2 == "" || gblQrRef1LblEN2 == undefined){
    gblQrRef1LblEN2 = "";
    gblQrRef1LblTH2 = "";
  }
  kony.print("gblbilPayRefNum  "+gblbilPayRefNum);
  kony.print("gblQrRef1Lbl1  "+gblQrRef1LblEN2);
  kony.print("gblQrRef2Lbl2  "+gblQrRef1LblTH2);
  kony.print("gblQrRef1Lbl1  "+gblQrRef1LblEN1);
  kony.print("gblQrRef2Lbl2  "+gblQrRef1LblTH1);
    inputParam["NotificationAdd_channelId"] = "Mobile Banking";
    inputParam["NotificationAdd_customerName"] = gblCustomerName;
    inputParam["NotificationAdd_fromAcctNick"] = gblQRPayData["custName"];
    inputParam["NotificationAdd_fromAcctName"] = gblQRPayData["custAcctName"];
    inputParam["NotificationAdd_billerNick"] = "";
    inputParam["NotificationAdd_billerName"] = gblQRPayData["toAccountName"];
    inputParam["NotificationAdd_billerNameTH"] = gblQRPayData["toAccountName"];
    inputParam["NotificationAdd_ref1EN"] = gblQrRef1LblEN1;
    inputParam["NotificationAdd_ref1TH"] = gblQrRef1LblTH1;
    inputParam["NotificationAdd_ref2EN"] = gblQrRef1LblEN2;
    inputParam["NotificationAdd_ref2TH"] = gblQrRef1LblTH2;
    inputParam["NotificationAdd_amount"] = transferAmount;
    inputParam["NotificationAdd_fee"] = diplayFee + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
    inputParam["NotificationAdd_refID"] = gblbilPayRefNum;
    inputParam["NotificationAdd_memo"] = "";
    inputParam["NotificationAdd_deliveryMethod"] = "Email";
    inputParam["NotificationAdd_noSendInd"] = "0";
    inputParam["NotificationAdd_notificationType"] = "Email";
    inputParam["NotificationAdd_Locale"] = kony.i18n.getCurrentLocale();
    inputParam["NotificationAdd_notificationSubject"] = "billpay";
    inputParam["NotificationAdd_notificationContent"] = "billPaymentDone";
    inputParam["NotificationAdd_appID"] = appConfig.appId;
    inputParam["NotificationAdd_channel"] = "MB";
    inputParam["crmProfileMod_appID"] = appConfig.appId;
    inputParam["crmProfileMod_channel"] = "MB";
    inputParam["source"] = "billpaymentnow";
    inputParam["finActivityLog_billerRef1"] = gblQRPayData["ref1"];
    inputParam["finActivityLog_fromAcctName"] = gblQRPayData["accountName"];
    inputParam["finActivityLog_fromAcctNickname"] = gblQRPayData["custName"];
    inputParam["finActivityLog_toAcctName"] = gblQRPayData["toAccountName"];
    
    if(gblQRPayData["billerCompCode"] != undefined && gblQRPayData["billerCompCode"] != ""){
    	if(gblQRPayData["billerCompCode"].length <= 4){
    		inputParam["finActivityLog_toAcctNickname"] = gblQRPayData["toAccountName"].substr(0,14) + "(" + gblQRPayData["billerCompCode"] + ")";
    	}else{
    		inputParam["finActivityLog_toAcctNickname"] = gblQRPayData["toAccountName"].substr(0,10) + "(" + gblQRPayData["billerCompCode"].substring(0,3) + gblQRPayData["billerCompCode"].substring(gblQRPayData["billerCompCode"].length - 5) + ")";
    	}    	
    }else{
    	inputParam["finActivityLog_toAcctNickname"] = gblQRPayData["toAccountName"].substr(0,20);
    }
    
    if(gblTaxDec == "T"){
	    inputParam["finActivityLog_billerRef2"] = gblCustomerIDValue;
	}else{
	    inputParam["finActivityLog_billerRef2"] = "0";
	}

    inputParam["finActivityLog_availableBal"] = gblqrToAccntBal.replace(/,/g, "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim();
    inputParam["finActivityLog_finTxnRefId"] = gblbilPayRefNum;
    inputParam["finActivityLog_appID"] = appConfig.appId;
    inputParam["finActivityLog_channel"] = "MB";
    inputParam["finActivityLog_channelId"] = "02";
    inputParam["activityLog_appID"] = appConfig.appId;
    inputParam["activityLog_channel"] = "MB";
    inputParam["activityLog_channelId"] = "02";
    inputParam["activityLog_channelId"] = "02";
    inputParam["activityLog_activityFlexValues1"] = gblQRPayData["toAccountName"];
    //inputParam["activityLog_activityFlexValues2"] = "XXX-X-" + gblQRPayData["accountNoFomatted"].substring(6, 11) + "-X";
  	inputParam["activityLog_activityFlexValues2"] = gblQRPayData["accountNoFomatted"].replace(/-/g, "");
    inputParam["activityLog_activityFlexValues3"] = gblQRPayData["ref1"];
    //inputParam["activityLog_activityFlexValues4"] = gblqrTransferAmnt + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
  	inputParam["activityLog_activityFlexValues4"] = gblqrTransferAmnt;
	if(gblTaxDec == "T"){
      	inputParam["activityLog_activityFlexValues5"] = gblCustomerIDValue;
	    //inputParam["activityLog_activityFlexValues5"] = maskCitizenID(gblCustomerIDValue).toUpperCase();
	}else{
	    inputParam["activityLog_activityFlexValues5"] = "";
	}
    inputParam["serviceID"] = "ConfirmBillPaymentPromptPayService";
    inputParam["finActivityLog_billerCustomerName"] = gblQRPayData["toAccountName"] + "(" + gblQRPayData["billerCompCode"] + ")";
    if(gbleDonationType == "Barcode_Type"){
      	inputParam["EDonationFlag"] = "Y";
    }else{
    	inputParam["QRFlag"] = "Y";
    }
    invokeServiceSecureAsync("ConfirmBillPaymentPromptPayService", inputParam, callBackMBCompositeBillQRPay_donation);
}