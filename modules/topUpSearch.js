







function searchSuggestedTopUpPay() {
	search = "";
	// alert("Search Text "+ frmIBMyBillersHome.tbxSearch.text)
	search = frmIBTopUpLandingPage.txtSearchBox.text;
	if (search.length >= 3) {
		
		frmIBTopUpLandingPage.label1015334424150416.setVisibility(true);
		//frmIBBillPaymentLP.segSuggestedBillersList.setVisibility(true);
		isSearchedBiller = true;
		getTopUpLPSuggestListIB();
	} else {
		frmIBTopUpLandingPage.label1015334424150416.setVisibility(false);
		frmIBTopUpLandingPage.segSuggestedBiller.removeAll();
		onTopUpSelectMoreDynamicIB(MySelectBillSuggestListRs);
		//isSearchedBiller = false;
		//getBillerLPSuggestListIB();
	}
	//if (search.length < 3){
	//	onTopUpSelectMoreDynamicIB(MySelectBillSuggestListRs)
	//}
}

function getTopUpLPSuggestListIB() {
	if (isSearchedBiller)
		var inputParams = {
			billerName: search,
			IsActive: "1",
			BillerGroupType: gblGroupTypeTopup,
			flagBillerList : "IB"
			//clientDate: getCurrentDate()not working on IE 8
		};
	else
		var inputParams = {
			IsActive: "1",
			BillerGroupType: gblGroupTypeTopup,
			flagBillerList : "IB"
			// clientDate: getCurrentDate()not working on IE 8
		};
	showLoadingScreenPopup();
	invokeServiceSecureAsync("masterBillerInquiry", inputParams, getTopUpLPSuggestListIBsyncCallback);
}

function getTopUpLPSuggestListIBsyncCallback(status, callBackResponse) {
	if (status == 400) {
		if (callBackResponse["opstatus"] == "0") {
			var responseData = callBackResponse["MasterBillerInqRs"];
			if (responseData.length > 0) {
				setTopUpCollectionDataBillPay(callBackResponse["MasterBillerInqRs"]);
				// getMyBillListIB(); // calling customer Bill Inquiry	
				dismissLoadingScreenPopup();
			} else {
				var collectionData = [];
				setTopUpCollectionDataBillPay(collectionData)
				//frmIBTopUpLandingPage.label1015334424150416.setVisibility(false);
				frmIBTopUpLandingPage.segSuggestedBiller.removeAll();
				dismissLoadingScreenPopup();
				//alert(kony.i18n.getLocalizedString("keyNoSuggestBillers"));
			}
		}
	}
}

function setTopUpCollectionDataBillPay(collectionData) {
	var tmpRef2Len = "";
	var tmpRef2label = "";
	var tmpRef2Len = 0;
	var billername = "";
	var ref1label = "";
	var ref2label = "";
	var tempRecord = [];
	var tmpIsRef2Req = "";
	
	if (kony.i18n.getCurrentLocale() == "en_US") {
		billername = "BillerNameEN";
		ref1label = "LabelReferenceNumber1EN";
		ref2label = "LabelReferenceNumber2EN";
	} else if (kony.i18n.getCurrentLocale() == "th_TH") {
		billername = "BillerNameTH";
		ref1label = "LabelReferenceNumber1TH";
		ref2label = "LabelReferenceNumber2TH";
	}
	gblBillerLastSearchNoCat.length = 0;
	for (var i = 0; i < collectionData.length; i++) {
		if (collectionData[i]["BillerGroupType"] == gblGroupTypeTopup) {
			tmpIsRef2Req = collectionData[i]["IsRequiredRefNumber2Add"];
			if (tmpIsRef2Req == "Y" || tmpIsRef2Req == "y") {
				tmpRef2Len = collectionData[i]["Ref2Len"];
				tmpRef2label = collectionData[i][ref2label];
			} else {
				tmpRef2Len = 0;
				tmpRef2Label = "";
			}
			var imagesUrl = loadBillerIcons(collectionData[i]["BillerCompcode"]);
			var billerNameEN=collectionData[i]["BillerNameEN"] + " (" + collectionData[i]["BillerCompcode"] + ")";
			var billerNameTH=collectionData[i]["BillerNameTH"] + " (" + collectionData[i]["BillerCompcode"] + ")";
			
			tempRecord = {
				"lblSuggestedBiller": collectionData[i][billername] + "(" + collectionData[i]["BillerCompcode"] + ")",
				"lblSuggestedBillerEN": collectionData[i]["BillerNameEN"] + "(" + collectionData[i]["BillerCompcode"] + ")",
				"lblSuggestedBillerTH": collectionData[i]["BillerNameTH"] + "(" + collectionData[i]["BillerCompcode"] + ")",
				"BillerGroupType" : collectionData[i]["BillerGroupType"],
				"IsRequiredRefNumber2Add": collectionData[i]["IsRequiredRefNumber2Add"],
				"IsRequiredRefNumber2Pay": collectionData[i]["IsRequiredRefNumber2Pay"],
				"ToAccountKey": collectionData[i]["ToAccountKey"],
				"IsFullPayment" : collectionData[i]["IsFullPayment"],
				"btnAddSuggBiller": {
					"text": "Add",
					"skin": "btnIBaddsmall"
				},
				"BillerID": collectionData[i]["BillerID"],
				"BillerCompCode": collectionData[i]["BillerCompcode"],
                "BillerTaxID": collectionData[i]["BillerTaxID"],
				"Ref1Label": collectionData[i][ref1label],
				"Ref2Label": tmpRef2label,
				"Ref1EN" : collectionData[i]["LabelReferenceNumber1EN"],
				"Ref1TH" : collectionData[i]["LabelReferenceNumber1TH"],
				"EffDt": collectionData[i]["EffDt"],
				"imgSuggestedBiller": {
					"src": imagesUrl
				},
				"BarcodeOnly": collectionData[i]["BarcodeOnly"],
				"BillerMethod": collectionData[i]["BillerMethod"],
				"Ref1Len": collectionData[i]["Ref1Len"],
				"Ref2Len": tmpRef2Len,
				"BillerCategoryID": {
					"text": collectionData[i]["BillerCategoryID"],
				"TopupAmtMin" : collectionData[i]["TopupAmtMin"],
				"TopupAmtMax" : collectionData[i]["TopupAmtMax"]
				}
			}
			gblBillerLastSearchNoCat.push(tempRecord);
		}
	}
	sugTopUpCatChangeBillPay();
}

function sugTopUpCatChangeBillPay() {
	var showCatSuggestList = [];
	var tmpCatSelected = frmIBTopUpLandingPage.combobox1010778103113652.selectedKey;
	
	
	var j = 0;
	var tmp = "";
	var lengthBillersSuggestList = gblBillerLastSearchNoCat.length;
	if (null != tmpCatSelected && tmpCatSelected != 0) {
		for (j = 0, i = 0; i < lengthBillersSuggestList; i++) {
			
			if (gblBillerLastSearchNoCat[i].BillerCategoryID.text == tmpCatSelected) {
				showCatSuggestList[j] = gblBillerLastSearchNoCat[i];
				
				j++;
			}
		}
		gblTopUpSelectFormDataHolderIB = showCatSuggestList;
		onTopUpSelectMoreDynamicIB(gblTopUpSelectFormDataHolderIB);
		//frmIBMyBillersHome.segSuggestedBillersList.setData(showCatSuggestList);
	} else {
		onTopUpSelectMoreDynamicIB(gblBillerLastSearchNoCat);
		//frmIBMyBillersHome.segSuggestedBillersList.setData(gblBillerLastSearchNoCat);
	}
}

function onTopUpSelectMoreDynamicIB(gblTopUpSelectFormDataHolderIB) {
	
	var currentForm = kony.application.getCurrentForm();
	
	
	var maxLength = kony.os.toNumber(GLOBAL_LOAD_MORE); //configurable parameter also initilaize gblTopUpMore to max length
	
	gblTopUpMore = gblTopUpMore + maxLength;
	
	var newData = [];
	var totalData = gblTopUpSelectFormDataHolderIB; //data from static population 
	
	var totalLength = totalData.length;
	
	if (totalLength == 0) {
		frmIBTopUpLandingPage.label1015334424150416.setVisibility(false);
		frmIBTopUpLandingPage.segSuggestedBiller.removeAll();
	} else {
		frmIBTopUpLandingPage.label1015334424150416.setVisibility(true);
	}
	// if (totalLength > gblTopUpMore) {
	//     frmIBBillPaymentLP.lblBPSgstdBillerList.setVisibility(true);
	//    var endPoint = gblTopUpMore;
	//} else {
	// 	frmIBBillPaymentLP.lblBPSgstdBillerList.setVisibility(true);
	var endPoint = totalLength;
	//}
	for (i = 0; i < endPoint; i++) {
		
		newData.push(gblTopUpSelectFormDataHolderIB[i])
	}
	frmIBTopUpLandingPage.segSuggestedBiller.data = newData;
	dismissLoadingScreenPopup();
	frmIBTopUpLandingPage.txtSearchBox.setFocus(true);
}

function onMyCustTopUpMoreDynamicIB(gblCustTopUpSelectFormDataHolderIB) {
	
	var currentForm = kony.application.getCurrentForm();
	
	
	var maxLength = kony.os.toNumber(GLOBAL_LOAD_MORE); //configurable parameter also initilaize gblCustTopUpMore to max length
	
	gblCustTopUpMore = gblCustTopUpMore + maxLength;
	
	var newData = [];
	var totalData = gblCustTopUpSelectFormDataHolderIB; //data from static population 
	
	var totalLength = totalData.length;
	
	if (totalLength == 0) {
		frmIBTopUpLandingPage.label1015334424150416.setVisibility(false);
		frmIBTopUpLandingPage.segBiller.removeAll();
	} else {
		frmIBTopUpLandingPage.label1015334424150416.setVisibility(true);
	}
	if (totalLength > gblCustTopUpMore) {
		// currentForm.hbxMore.setVisibility(true);
		var endPoint = gblCustTopUpMore;
	} else {
		//currentForm.hbxMore.setVisibility(false);
		var endPoint = totalLength;
	}
	for (i = 0; i < endPoint; i++) {
		
		newData.push(gblCustTopUpSelectFormDataHolderIB[i])
	}
	frmIBTopUpLandingPage.segBiller.data = newData;
}

function searchTopUpBillsIB() {
	var showSearchList = [];
	var showCatList = [];
	var searchCustomerData = [];
	var searchMasterData = [];
	var lowerCaseName = "";
	var showSearchSuggestList = [];
	search = "";
	// alert("Search Text "+ frmIBMyBillersHome.tbxSearch.text)
	search = frmIBTopUpLandingPage.txtSearchBox.text;
	var tmpCatSelected = frmIBTopUpLandingPage.combobox1010778103113652.selectedKey;
	var j = 0;
	
	var lengthBillersList = MySelectBillListRs.length;
	
	if (search.length >= 3) {
		
		frmIBTopUpLandingPage.label1015334424150416.setVisibility(true);
		var searchtxt = search.toLowerCase();
		var lowerCaseNameBiller = "";
		// var regexp = new RegExp("(" + searchtxt + ")", "ig");
		for (j = 0, i = 0; i < lengthBillersList; i++) {
			if (MySelectBillListRs[i]["lblBillerName"]) {
				
				lowerCaseName = MySelectBillListRs[i]["lblBillerName"].toLowerCase();
				// added biller name search to fix DEF1512
				lowerCaseNameBiller = MySelectBillListRs[i]["BillerCompCode"].toLowerCase();
				//if (regexp.test(tmpBillerData[i]["lblBillerNickname"]["text"]) == true) {
				// if (lowerCaseName.contains(searchtxt)) {
				//if (kony.string.startsWith(lowerCaseName, searchtxt, true)) {
				if (lowerCaseName.indexOf(searchtxt)>= 0 || lowerCaseNameBiller.indexOf(searchtxt) >= 0) {	
					showSearchList[j] = MySelectBillListRs[i];
					j++;
				}
			}
		}
		searchCustomerData = showSearchList;
	} else {
		searchCustomerData = MySelectBillListRs;
		//frmIBTopUpLandingPage.label1015334424150416.setVisibility(false);
		//frmIBTopUpLandingPage.segSuggestedBiller.removeAll();
		// onTopUpSelectMoreDynamicIB(MySelectBillSuggestListRs)
	}
	
	if (null != tmpCatSelected && tmpCatSelected != 0) {
		var searchLen = searchCustomerData.length;
		for (j = 0, i = 0; i < searchLen; i++) {
			tmp = searchCustomerData[i].BillerCategoryID
			
			if (tmpCatSelected == tmp) {
				
				showCatList[j] = searchCustomerData[i];
				j++;
			}
		}
		searchCustomerData = showCatList;
	} else {
		showCatList = searchCustomerData;
	}
	//frmIBMyBillersHome.segSuggestedBillersList.setData(showSearchSuggestList);
	onMyCustTopUpMoreDynamicIB(showCatList);
	//frmIBMyBillersHome.segBillersList.setData(showCatList);
}

function searchMyTopUpsAndSuggestedTopUpsIB() {

	var searchMyBillList = [];
	var searchSuggestedBillList = [];
	var searchText = frmIBTopUpLandingPage.txtSearchBox.text;
	var searchValue = ""; 
	
	//MyBills Search
	if(MySelectBillListRs != null && MySelectBillListRs.length > 0) { // Atleast one biller should be avilable
		if (isNotBlank(searchText) && searchText.length >= 3) { //Atleast 3 characters should be entered in search box
			
			searchText = searchText.toLowerCase();
			
			for (j = 0, i = 0; i < MySelectBillListRs.length; i++) {
				
				searchValue = MySelectBillListRs[i].lblBillerName.toLowerCase() 
								+ "~" + MySelectBillListRs[i].BillerCompCode.toLowerCase() 
								+ "~" + MySelectBillListRs[i].Compcode.toLowerCase() 
								+ "~" + MySelectBillListRs[i].lblRef1Value;
				
				if (searchValue.indexOf(searchText) > -1) {
					if(frmIBTopUpLandingPage.combobox1010778103113652.selectedKey == "0" || MySelectBillListRs[i].BillerCategoryID == frmIBTopUpLandingPage.combobox1010778103113652.selectedKey) {
						searchMyBillList[j] = MySelectBillListRs[i];
						j++;
					}
				}
			}//For Loop End

		} else { //searchText condition End
			// Search text is less than 3 characters hence show all My Bills
			if(frmIBTopUpLandingPage.combobox1010778103113652.selectedKey == "0") {
				searchMyBillList = MySelectBillListRs;
			} else {
				for (j = 0, i = 0; i < MySelectBillListRs.length; i++) {
					if(MySelectBillListRs[i].BillerCategoryID == frmIBTopUpLandingPage.combobox1010778103113652.selectedKey) {
						searchMyBillList[j] = MySelectBillListRs[i];
						j++;
					}
				}
			}
		}
	}
	frmIBTopUpLandingPage.segBiller.setData(searchMyBillList);
	//Suggested Biller Search 
	if(MySelectBillSuggestListRs != null && MySelectBillSuggestListRs.length > 0) { // Atleast one biller should be avilable
		if (isNotBlank(searchText) && searchText.length >= 3) { //Atleast 3 characters should be entered in search box
			
			searchText = searchText.toLowerCase();
			
			for (j = 0, i = 0; i < MySelectBillSuggestListRs.length; i++) {
				
				var searchValue = MySelectBillSuggestListRs[i].lblSuggestedBiller.toLowerCase() 
									+ "~" + MySelectBillSuggestListRs[i].BillerCompCode.toLowerCase();
				
				if (searchValue.indexOf(searchText) > -1) {
					if(frmIBTopUpLandingPage.combobox1010778103113652.selectedKey == "0" || MySelectBillSuggestListRs[i].BillerCategoryID.text == frmIBTopUpLandingPage.combobox1010778103113652.selectedKey) {
						searchSuggestedBillList[j] = MySelectBillSuggestListRs[i];
						j++;
					}
				}
			}//For Loop End

		} else { //searchText condition End
			// Search text is less than 3 characters hence show all My Bills
			if(frmIBTopUpLandingPage.combobox1010778103113652.selectedKey == "0") {
				searchSuggestedBillList = MySelectBillSuggestListRs;
			} else {
				for (j = 0, i = 0; i < MySelectBillSuggestListRs.length; i++) {
					if(MySelectBillSuggestListRs[i].BillerCategoryID.text == frmIBTopUpLandingPage.combobox1010778103113652.selectedKey) {
						searchSuggestedBillList[j] = MySelectBillSuggestListRs[i];
						j++;
					}
				}
			}
		}
	}
	
	frmIBTopUpLandingPage.segSuggestedBiller.setData(searchSuggestedBillList);
	
	if(searchMyBillList.length == 0 && searchSuggestedBillList.length == 0){
		//frmSelectBiller.segMyBills.setVisibility(false);
		frmIBTopUpLandingPage.lblMsg.setVisibility(true);
		frmIBTopUpLandingPage.lblMsg.text = kony.i18n.getLocalizedString("keybillernotfound");
	}else{
		frmIBTopUpLandingPage.lblMsg.setVisibility(false);
	}
	if(searchMyBillList.length == 0){
		frmIBTopUpLandingPage.label1015334424150383.setVisibility(false);
	}else{
		frmIBTopUpLandingPage.label1015334424150383.setVisibility(true);
	}
	if(searchSuggestedBillList.length == 0){
		frmIBTopUpLandingPage.label1015334424150416.setVisibility(false);
	}else{
		frmIBTopUpLandingPage.label1015334424150416.setVisibility(true);
	}
}