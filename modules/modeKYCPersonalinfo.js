// Dropdown Related Functions-------------------- Start

function setPersonalInfoMasterDataforDropDown(){
  var selectedFlag = false;
  var selectedStatus="";
  var PersonalInfoMasterLabel = false;
  var PersonalInfoMasterLabelObj = null;
  var ContetnData = [];
  if(gblPersonalInfoMasterSelectedDropDownId == "flxMaritalStatus"){
    PersonalInfoMasterLabelObj = frmMBeKYCPersonalInform.lblMaritalStatusDesc ;
    ContetnData = gblPersonalInfoMasterCollectionData.MaritalStatusDS;
  }else if(gblPersonalInfoMasterSelectedDropDownId == "flxEducation"){
    PersonalInfoMasterLabelObj = frmMBeKYCPersonalInform.lblEducationDesc ;
    ContetnData = gblPersonalInfoMasterCollectionData.EducationDS;    
  }else if(gblPersonalInfoMasterSelectedDropDownId == "flxOccupation"){
    PersonalInfoMasterLabelObj = frmMBeKYCPersonalInform.lblOccupationDesc ;
    ContetnData = gblPersonalInfoMasterCollectionData.OccupationDS;
  }else if(gblPersonalInfoMasterSelectedDropDownId == "flxOccupationAdditional"){
    PersonalInfoMasterLabelObj = frmMBeKYCPersonalInform.lblOccupationAdditionalDesc ;
    ContetnData = gblPersonalInfoMasterCollectionData.OccupationAdditionalDS;
  }else if(gblPersonalInfoMasterSelectedDropDownId == "flxIncome"){
    PersonalInfoMasterLabelObj = frmMBeKYCPersonalInform.lblIncomeDesc ;
    ContetnData = gblPersonalInfoMasterCollectionData.IncomeRangeDS;
  }else if(gblPersonalInfoMasterSelectedDropDownId == "flxSourceofIncome"){
    PersonalInfoMasterLabelObj = frmMBeKYCPersonalInform.lblSourceofIncomeDesc ;
    ContetnData = gblPersonalInfoMasterCollectionData.SourceOfIncomeDS;
  }else if(gblPersonalInfoMasterSelectedDropDownId == "flxcountryofIncome"){
    PersonalInfoMasterLabelObj = frmMBeKYCPersonalInform.lblcountryofIncomeDesc ;
    ContetnData = gblPersonalInfoMasterCollectionData.CountryDS;
  }else if(gblPersonalInfoMasterSelectedDropDownId == "flxObjectiveofAccount"){
    PersonalInfoMasterLabelObj = frmMBeKYCPersonalInform.lblObjectiveofAccountDesc ;
    ContetnData = gblPersonalInfoMasterCollectionData.ObjectiveOfAccountDS;
  }else if(gblPersonalInfoMasterSelectedDropDownId == "flxsecndNationality"){
    PersonalInfoMasterLabelObj = frmMBeKYCPersonalInform.lblsecndNationalityDesc ;
    ContetnData = gblPersonalInfoMasterCollectionData.NationalityDS;
  }

//   if(PersonalInfoMasterLabelObj.isVisible) {
//     selectedFlag = true;
//     kony.print("getEmployeeStatus @@ already");
//     selectedStatus = PersonalInfoMasterLabelObj.text;
//   }
  setPersonalInfoMasterDropDownDatainSeg(ContetnData,selectedFlag,selectedStatus,"SCI_COUNTRY");
}
function setPersonalInfoMasterDropDownDatainSeg(content,selectedFlag,selectedStatus, flowName) {
  var locale = kony.i18n.getCurrentLocale();
  var currForm =kony.application.getCurrentForm();
  var segData = [];

   for (var i = 0; i < content.length; i++) {
    kony.print("getcontent @@ in loop");
    var entryName = content[i].varDesc;
    var entryCode = content[i].varCode;
    var entryID = content[i].varCode;
    var temprowData = {};
    var dataLocal = content[i].languageCode ;
    if (selectedFlag && selectedStatus == entryName) {
      kony.print("getcontent @@ matched");
      temprowData = {
        "lblName": entryName,
        "lblSegRowLine": ".",
        "imgTick": "tick_icon.png",
        "entryID": entryID,
        "lblEntryCode": entryCode,
        "flxSegLoanGeneric": {
          "skin": flexGreyBG
        }
      };
      segData.push(temprowData);
      kony.print("getcontent @@ pushed selected");
    } else {
      temprowData = {
        "lblName": entryName,
        "lblSegRowLine": ".",
        "imgTick": "",
        "entryID": entryID,
        "lblEntryCode": entryCode,
        "flxSegLoanGeneric": {
          "skin": flexWhiteBG
        }
      };
      if (locale == dataLocal) {
        segData.push(temprowData);
      } 

    }
  }
  frmMBeKYCPersonalInform.segDyna.removeAll();

  if(gblPersonalInfoMasterSelectedDropDownId == "flxMaritalStatus"){
    frmMBeKYCPersonalInform.lblDynamic.text = kony.i18n.getLocalizedString("eKYC_MaritalStatus");
  }else if(gblPersonalInfoMasterSelectedDropDownId == "flxEducation"){
    frmMBeKYCPersonalInform.lblDynamic.text = kony.i18n.getLocalizedString("eKYC_PersonalInfo_education");
  }else if(gblPersonalInfoMasterSelectedDropDownId == "flxOccupation"){
    frmMBeKYCPersonalInform.lblDynamic.text = kony.i18n.getLocalizedString("Occupation_textbox");
  }else if(gblPersonalInfoMasterSelectedDropDownId == "flxOccupationAdditional"){
    frmMBeKYCPersonalInform.lblDynamic.text = kony.i18n.getLocalizedString("eKYC_OccAdditional");
  }else if(gblPersonalInfoMasterSelectedDropDownId == "flxIncome"){
    frmMBeKYCPersonalInform.lblDynamic.text = kony.i18n.getLocalizedString("eKYC_PersonalInfo_Income");
  }else if(gblPersonalInfoMasterSelectedDropDownId == "flxSourceofIncome"){
    frmMBeKYCPersonalInform.lblDynamic.text = kony.i18n.getLocalizedString("Kyc_lbl_sourceofIncome");
  }else if(gblPersonalInfoMasterSelectedDropDownId == "flxcountryofIncome"){
    frmMBeKYCPersonalInform.lblDynamic.text = kony.i18n.getLocalizedString("eKYC_lbCountryofSourceIncome");
  }else if(gblPersonalInfoMasterSelectedDropDownId == "flxObjectiveofAccount"){
    frmMBeKYCPersonalInform.lblDynamic.text = kony.i18n.getLocalizedString("eKYC_lbObjOfAcct");
  }else if(gblPersonalInfoMasterSelectedDropDownId == "flxsecndNationality"){
    frmMBeKYCPersonalInform.lblDynamic.text = kony.i18n.getLocalizedString("eKYC_secondNationality");
  }

  frmMBeKYCPersonalInform.segDyna.onRowClick = segPersonalInfoMasterSourceofIncomeonRowClick;
  frmMBeKYCPersonalInform.btnPopUpClosePopUp.onClick = btnCloseMasterDataPopUponClick;
  frmMBeKYCPersonalInform.segDyna.setData(segData);
  frmMBeKYCPersonalInform.flxDynamicPopUp.setVisibility(true);
  frmMBeKYCPersonalInform.footers[0].isVisible =false;
  frmMBeKYCPersonalInform.headers[0].isVisible =false;
  dismissLoadingScreen();

}
function segPersonalInfoMasterSourceofIncomeonRowClick (){
  var PersonalInfoMasterName = frmMBeKYCPersonalInform.segDyna.selectedRowItems[0].lblName;  	
  var PersonalInfoMasterCode = frmMBeKYCPersonalInform.segDyna.selectedRowItems[0].lblEntryCode;  	

  frmMBeKYCPersonalInform.flxDynamicPopUp.setVisibility(false);
  frmMBeKYCPersonalInform.footers[0].isVisible =true;
  frmMBeKYCPersonalInform.headers[0].isVisible =true;
  if(gblPersonalInfoMasterSelectedDropDownId == "flxMaritalStatus"){
    frmMBeKYCPersonalInform.lblMaritalStatus.isVisible = false;
    frmMBeKYCPersonalInform.lblMaritalStatusHeader.isVisible = true;
    frmMBeKYCPersonalInform.lblMaritalStatusDesc.isVisible = true;  	
    frmMBeKYCPersonalInform.lblMaritalCode.text = PersonalInfoMasterCode;
    frmMBeKYCPersonalInform.lblMaritalStatusDesc.text = PersonalInfoMasterName;	
    frmMBeKYCPersonalInform.flxMaritalStatus.skin =lFboxLoan ;
  }else if(gblPersonalInfoMasterSelectedDropDownId == "flxEducation"){
    frmMBeKYCPersonalInform.lblEducation.isVisible = false;
    frmMBeKYCPersonalInform.lblEducationHeader.isVisible = true;
    frmMBeKYCPersonalInform.lblEducationDesc.isVisible = true;  	
    frmMBeKYCPersonalInform.lblEducationCode.text = PersonalInfoMasterCode;
    frmMBeKYCPersonalInform.lblEducationDesc.text = PersonalInfoMasterName;	
    frmMBeKYCPersonalInform.flxEducation.skin =lFboxLoan ;
  }else if(gblPersonalInfoMasterSelectedDropDownId == "flxOccupation"){
    frmMBeKYCPersonalInform.lblOccupation.isVisible = false;
    frmMBeKYCPersonalInform.lblOccupationHeader.isVisible = true;
    frmMBeKYCPersonalInform.lblOccupationDesc.isVisible = true;  	
    frmMBeKYCPersonalInform.lblOccupationCode.text = PersonalInfoMasterCode;
    frmMBeKYCPersonalInform.lblOccupationDesc.text = PersonalInfoMasterName;	
    frmMBeKYCPersonalInform.flxOccupation.skin =lFboxLoan ;
    var riskOccupationData = gblPersonalInfoMasterCollectionData.RiskOccupationDS;
    var isRiskOccupationEnable = false;
    for(var i=0; i < riskOccupationData.length; i++){
      if(PersonalInfoMasterCode === riskOccupationData[i].varCode)
        {
          isRiskOccupationEnable = true;
          break;
        }
    }
    
    frmMBeKYCPersonalInform.flxOccupationAdditional.isVisible = isRiskOccupationEnable ;
    
  }else if(gblPersonalInfoMasterSelectedDropDownId == "flxOccupationAdditional"){
    frmMBeKYCPersonalInform.lblOccupationAdditional.isVisible = false;
    frmMBeKYCPersonalInform.lblOccupationAdditionalHeader.isVisible = true;
    frmMBeKYCPersonalInform.lblOccupationAdditionalDesc.isVisible = true;  	
    frmMBeKYCPersonalInform.lblOccupationAdditionalCode.text = PersonalInfoMasterCode;
    frmMBeKYCPersonalInform.lblOccupationAdditionalDesc.text = PersonalInfoMasterName;	
    frmMBeKYCPersonalInform.flxOccupationAdditional.skin =lFboxLoan ;
  }else if(gblPersonalInfoMasterSelectedDropDownId == "flxIncome"){
    frmMBeKYCPersonalInform.lblIncome.isVisible = false;
    frmMBeKYCPersonalInform.lblIncomeHeader.isVisible = true;
    frmMBeKYCPersonalInform.lblIncomeDesc.isVisible = true;  	
    frmMBeKYCPersonalInform.lblIncomeCode.text = PersonalInfoMasterCode;
    frmMBeKYCPersonalInform.lblIncomeDesc.text = PersonalInfoMasterName;	
    frmMBeKYCPersonalInform.flxIncome.skin =lFboxLoan ;
  }else if(gblPersonalInfoMasterSelectedDropDownId == "flxSourceofIncome"){
    frmMBeKYCPersonalInform.lblSourceofIncome.isVisible = false;
    frmMBeKYCPersonalInform.lblSourceofIncomeHeader.isVisible = true;
    frmMBeKYCPersonalInform.lblSourceofIncomeDesc.isVisible = true;  	
    frmMBeKYCPersonalInform.lblSourceofIncomeCode.text = PersonalInfoMasterCode;
    frmMBeKYCPersonalInform.lblSourceofIncomeDesc.text = PersonalInfoMasterName;	
    frmMBeKYCPersonalInform.flxSourceofIncome.skin =lFboxLoan ;
  }else if(gblPersonalInfoMasterSelectedDropDownId == "flxcountryofIncome"){
    frmMBeKYCPersonalInform.lblcountryofIncome.isVisible = false;
    frmMBeKYCPersonalInform.lblcountryofIncomeHeader.isVisible = true;
    frmMBeKYCPersonalInform.lblcountryofIncomeDesc.isVisible = true;  	
    frmMBeKYCPersonalInform.lblcountryofIncomeCode.text = PersonalInfoMasterCode;
    frmMBeKYCPersonalInform.lblcountryofIncomeDesc.text = PersonalInfoMasterName;	
    frmMBeKYCPersonalInform.flxcountryofIncome.skin =lFboxLoan ;
  }else if(gblPersonalInfoMasterSelectedDropDownId == "flxObjectiveofAccount"){
    frmMBeKYCPersonalInform.lblObjectiveofAccount.isVisible = false;
    frmMBeKYCPersonalInform.lblObjectiveofAccountHeader.isVisible = true;
    frmMBeKYCPersonalInform.lblObjectiveofAccountDesc.isVisible = true;  	
    frmMBeKYCPersonalInform.lblObjectiveofAccountCode.text = PersonalInfoMasterCode;
    frmMBeKYCPersonalInform.lblObjectiveofAccountDesc.text = PersonalInfoMasterName;	
    frmMBeKYCPersonalInform.flxObjectiveofAccount.skin =lFboxLoan ;
    var isOtherobjTxtEnable =false;
    if(PersonalInfoMasterCode === eKYCconfigObject.OtherObjectiveOfAccount.code){
       isOtherobjTxtEnable = true;
    }
    
    frmMBeKYCPersonalInform.FlextxtOtherObjAcc.isVisible =  isOtherobjTxtEnable;
    
  }else if(gblPersonalInfoMasterSelectedDropDownId == "flxsecndNationality"){
    frmMBeKYCPersonalInform.lblsecndNationality.isVisible = false;
    frmMBeKYCPersonalInform.lblsecndNationalityHeader.isVisible = true;
    frmMBeKYCPersonalInform.lblsecndNationalityDesc.isVisible = true;  	
    frmMBeKYCPersonalInform.lblsecndNationalityCode.text = PersonalInfoMasterCode;
    frmMBeKYCPersonalInform.lblsecndNationalityDesc.text = PersonalInfoMasterName;	
    frmMBeKYCPersonalInform.flxsecndNationality.skin =lFboxLoan ;
  }  
}
function getPersonalInfoDatafromService(){
  var input_param = {};
  invokeServiceSecureAsync("getEkycMasterData", input_param, getPersonalInfoDatafromServiceCallback);
}
function getPersonalInfoDatafromServiceCallback(status, result){
  if (status == 400) {
    try{
      if (result.opstatus == "0") {                                       
        gblPersonalInfoMasterCollectionData =result;  
        frmMBeKYCPersonalInform.show();
        setDefaultSecondNationality();
       // setPersonalInfoMasterDataforDropDown();
      }else {        
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        return false;
      }
      dismissLoadingScreen();
    }catch(e){
      alert("Exception in callbackOfgetPersonalInfoFields, e : "+e);
      dismissLoadingScreen();
    }
  }
}
// Dropdown Related Functions-------------------- End

// Events Related Functions-------------------- Start
function onClickPersonalInfoMasterDropDown(dropDownType){ 
  var dropDownTypeVal = "";
  //frmMBeKYCContactInfo.tbxSearch.text = "";
  if(null !== dropDownType && null !== dropDownType.id){
    dropDownTypeVal = dropDownType.id;
    gblPersonalInfoMasterSelectedDropDownId = dropDownTypeVal;
  }
  var categoryCode = "SCI_COUNTRY";
  if( gblPersonalInfoMasterCollectionData !== null && gblPersonalInfoMasterCollectionData !== ""  ){
    setPersonalInfoMasterDataforDropDown();
  } 
}
function btnCloseMasterDataPopUponClick (){
  frmMBeKYCPersonalInform.flxDynamicPopUp.setVisibility(false);
  frmMBeKYCPersonalInform.footers[0].isVisible =true;
  frmMBeKYCPersonalInform.headers[0].isVisible =true;
}
function onclickbtnsaluation(obj){
  var btnName = "";
  if(obj.id == "btnMiss"){
    btnName = frmMBeKYCPersonalInform.btnMiss.text;
    //frmMBeKYCPersonalInform.lblSaluationValue = gblPersonalInfoMasterCollectionData.TitleDS[frmMBeKYCPersonalInform.btnMiss.text].varDesc;
    frmMBeKYCPersonalInform.btnMiss.skin = btnWhiteFont160BlueBGCenterBtn;
	frmMBeKYCPersonalInform.btnMr.skin = btnBlueFont160WhiteBG ;
	frmMBeKYCPersonalInform.btnMrs.skin = btnBlueFont160WhiteBG ;
  }else if(obj.id == "btnMr"){
    //frmMBeKYCPersonalInform.lblSaluationValue = gblPersonalInfoMasterCollectionData.TitleDS[frmMBeKYCPersonalInform.btnMr.text].varDesc;
    frmMBeKYCPersonalInform.lblSaluationValue = "1";
    frmMBeKYCPersonalInform.btnMr.skin = btnWhiteFont160BlueBGLeftRndCorner;
	frmMBeKYCPersonalInform.btnMiss.skin = btnBlueFont160WhiteBG ;
	frmMBeKYCPersonalInform.btnMrs.skin = btnBlueFont160WhiteBG ;
  }else if(obj.id == "btnMrs"){
    //frmMBeKYCPersonalInform.lblSaluationValue = gblPersonalInfoMasterCollectionData.TitleDS[frmMBeKYCPersonalInform.btnMrs.text].varDesc;
    frmMBeKYCPersonalInform.lblSaluationValue = "1";
    frmMBeKYCPersonalInform.btnMrs.skin = btnWhiteFont160BlueBGRightRndCorner;
	frmMBeKYCPersonalInform.btnMiss.skin = btnBlueFont160WhiteBG ;
	frmMBeKYCPersonalInform.btnMr.skin = btnBlueFont160WhiteBG ;
  }
  for(var i=0 ; i<= gblPersonalInfoMasterCollectionData.TitleDS.length;i++){
    if(btnName == gblPersonalInfoMasterCollectionData.TitleDS[i].varDesc){
      frmMBeKYCPersonalInform.lblSaluationValue.text = gblPersonalInfoMasterCollectionData.TitleDS[i].varCode;
      return;
    }
  }
}
function onClickBtnHeaderbackfrmMBeKYCPersonalInfo(){
  if(frmMBeKYCContactInfo.tbxAddressNo.text !== undefined && frmMBeKYCContactInfo.tbxAddressNo.text !== null && frmMBeKYCContactInfo.tbxAddressNo.text !== "" ){
  	frmMBeKYCContactInfo.show();
  }else{
    navigatefrmMBeKYCContactInfo();
  }
  
}
function preshowfrmMBeKYCPersonalinfo(){
  setHeaderContentToForm(kony.i18n.getLocalizedString("eKYC_titlePersonalInfo"),onClickBtnHeaderbackfrmMBeKYCPersonalInfo,true,"3");
//   setFooterTwobtnToForm(kony.i18n.getLocalizedString("Back"), kony.i18n.getLocalizedString("Next"),onClickBtnHeaderbackfrmMBeKYCPersonalInfo , onclickBtnNextfrmMBeKYCPersonalInfo);
  setFooterTwobtnToForm(kony.i18n.getLocalizedString("Back"),kony.i18n.getLocalizedString("Next"),"",onClickBtnHeaderbackfrmMBeKYCPersonalInfo,onclickBtnNextfrmMBeKYCPersonalInfo,null,true,false);
  
 linkFunctionEventsfrmMBeKYCPersonalInfo();
  
  var local = kony.i18n.getCurrentLocale();
  //frmMBeKYCPersonalInform.btnMr.text = gblPersonalInfoMasterCollectionData.TitleDS.
  var tempTitleData =[];
  for(var i=0;i<gblPersonalInfoMasterCollectionData.TitleDS.length;i++){
    if(local == gblPersonalInfoMasterCollectionData.TitleDS[i].languageCode ){
      tempTitleData.push(gblPersonalInfoMasterCollectionData.TitleDS[i].varDesc);
    }
  }
  frmMBeKYCPersonalInform.btnMr.text = tempTitleData[0];
  frmMBeKYCPersonalInform.btnMiss.text = tempTitleData[1];  
  frmMBeKYCPersonalInform.btnMrs.text = tempTitleData[2];
  dismissLoadingScreen();
}
function linkFunctionEventsfrmMBeKYCPersonalInfo(){
 
  frmMBeKYCPersonalInform.onDeviceBack = disableBackButton;
  frmMBeKYCPersonalInform.flxMain.isVisible=true;
  frmMBeKYCPersonalInform.flxDynamicPopUp.isVisible = false;
  frmMBeKYCPersonalInform.footers[0].isVisible =true;
  frmMBeKYCPersonalInform.headers[0].isVisible =true;
  frmMBeKYCPersonalInform.btnMiss.onClick = onclickbtnsaluation;
  frmMBeKYCPersonalInform.btnMr.onClick = onclickbtnsaluation;
  frmMBeKYCPersonalInform.btnMrs.onClick = onclickbtnsaluation;
  
  frmMBeKYCPersonalInform.flxMaritalStatus.onClick = onClickPersonalInfoMasterDropDown;
  frmMBeKYCPersonalInform.flxEducation.onClick = onClickPersonalInfoMasterDropDown;
  frmMBeKYCPersonalInform.flxOccupation.onClick = onClickPersonalInfoMasterDropDown;
  frmMBeKYCPersonalInform.flxOccupationAdditional.onClick = onClickPersonalInfoMasterDropDown;
  frmMBeKYCPersonalInform.flxIncome.onClick = onClickPersonalInfoMasterDropDown;
  frmMBeKYCPersonalInform.flxSourceofIncome.onClick = onClickPersonalInfoMasterDropDown;
  frmMBeKYCPersonalInform.flxcountryofIncome.onClick = onClickPersonalInfoMasterDropDown;
  frmMBeKYCPersonalInform.flxObjectiveofAccount.onClick = onClickPersonalInfoMasterDropDown;
  
  frmMBeKYCPersonalInform.FlextxtOtherObjAcc.onClick = onFlexClickActivateTextboxPersonalInfo;
  frmMBeKYCPersonalInform.tbxOtherObjAcc.onDone = onTextChangePersonalInfo;
  frmMBeKYCPersonalInform.tbxOtherObjAcc.onEndEditing = onTextChangePersonalInfo;
  frmMBeKYCPersonalInform.tbxOtherObjAcc.onTextChange = isValidAddress;
  
  frmMBeKYCPersonalInform.flxsecndNationality.onClick = onClickPersonalInfoMasterDropDown;
}
function onFlexClickActivateTextboxPersonalInfo(obj){
  switch(obj.id){
    case "FlextxtOtherObjAcc":
      onFlexClickActivateTextboxforEditing(frmMBeKYCPersonalInform.lblMainOtherObjAcc,frmMBeKYCPersonalInform.lblOtherObjAcc,
                                           frmMBeKYCPersonalInform.tbxOtherObjAcc);  
      break;
  }
}
function onTextChangePersonalInfo(obj){
  switch(obj.id){
    case "tbxBuildingNo":
      OnTextEditingDoneEmptyCheck(frmMBeKYCPersonalInform.lblMainOtherObjAcc,frmMBeKYCPersonalInform.lblOtherObjAcc,frmMBeKYCPersonalInform.tbxOtherObjAcc);    
      break;
  }
}
function onclickBtnNextfrmMBeKYCPersonalInfo(){
  var isValidMandatoryField = isMandatoryFieldsvalidate();
  if(isValidMandatoryField){
    savePersonalInfoDataInDB();
    //showeMarketConsent();
  }
}
function savePersonalInfoDataInDB(){
  var input_param ={};
  //input_param.citizenId = gblEncryptedCitizenID;
  input_param.maritalStatus = frmMBeKYCPersonalInform.lblMaritalCode.text;
  input_param.education =   frmMBeKYCPersonalInform.lblEducationCode.text ;
  input_param.occupation =  frmMBeKYCPersonalInform.lblOccupationCode.text;
  input_param.occupationAdditional =  frmMBeKYCPersonalInform.lblOccupationAdditionalCode.text;
  input_param.income = frmMBeKYCPersonalInform.lblIncomeCode.text;
  input_param.sourceIncome =  frmMBeKYCPersonalInform.lblSourceofIncomeCode.text;  
  input_param.countryIncome =  frmMBeKYCPersonalInform.lblcountryofIncomeCode.text;
  input_param.accountObjective =  frmMBeKYCPersonalInform.lblObjectiveofAccountCode.text;
  input_param.OtherObjectiveAccount =  frmMBeKYCPersonalInform.tbxOtherObjAcc.text;
  input_param.otherNationality =  frmMBeKYCPersonalInform.lblsecndNationalityCode.text;
  input_param.title =  frmMBeKYCPersonalInform.lblSaluationValue.text;
  
  invokeServiceSecureAsync("saveUpdateEKycPersonalInfo", input_param, callBacksavPersonalInfoDataInDB);
}
function callBacksavPersonalInfoDataInDB(status, result){
  if(status == 400){
    if(result.opstatus == "0"){
      if(result.errCode == "22_001"){
        popGeneralMsg.lblMsg.text = kony.i18n.getLocalizedString("eKYC_msgCannotOpenAcct");
        popGeneralMsg.btnClose.text = kony.i18n.getLocalizedString("keyClose");
        popGeneralMsg.btnClose.onClick = closePopGeneralMsgekycPersonalInfo;
        popGeneralMsg.show();
      }
      else{
        showeMarketConsent();
      }
    }
  }
}

function closePopGeneralMsgekycPersonalInfo(){
  popGeneralMsg.dismiss();
  kony.store.removeItem("eKYCRegistrationFlag");
  kony.store.setItem("eKYCRegistrationFlag", "N");
  kony.application.exit();
}
function frmMBeKYCPersonalinfoInit(){
  frmMBeKYCPersonalInform.preShow = preshowfrmMBeKYCPersonalinfo;
}
// Events Related Functions-------------------- End
function isMandatoryFieldsvalidate(){
  var IsAllMandatryChecked = true;
  
  if(frmMBeKYCPersonalInform.lblMaritalStatus.isVisible){
    IsAllMandatryChecked =false;
    frmMBeKYCPersonalInform.flxMaritalStatus.skin =lFboxLoanIncorrect ;
    frmMBeKYCPersonalInform.flxMain.scrollToWidget(frmMBeKYCPersonalInform.lblMaritalStatus);
  }else if(frmMBeKYCPersonalInform.lblEducation.isVisible){
    IsAllMandatryChecked =false;
    frmMBeKYCPersonalInform.flxEducation.skin =lFboxLoanIncorrect ;
    frmMBeKYCPersonalInform.flxMain.scrollToWidget(frmMBeKYCPersonalInform.flxEducation);
  }else if(frmMBeKYCPersonalInform.lblOccupation.isVisible){
    IsAllMandatryChecked =false;
    frmMBeKYCPersonalInform.flxOccupation.skin =lFboxLoanIncorrect ;
    frmMBeKYCPersonalInform.flxMain.scrollToWidget(frmMBeKYCPersonalInform.flxOccupation);
  }else if(frmMBeKYCPersonalInform.flxOccupationAdditional.isVisible && frmMBeKYCPersonalInform.lblOccupationAdditional.isVisible){
    IsAllMandatryChecked =false;
    frmMBeKYCPersonalInform.flxOccupationAdditional.skin =lFboxLoanIncorrect ;
    frmMBeKYCPersonalInform.flxMain.scrollToWidget(frmMBeKYCPersonalInform.flxOccupationAdditional);
  }else if(frmMBeKYCPersonalInform.lblIncome.isVisible){
    IsAllMandatryChecked =false;
    frmMBeKYCPersonalInform.flxIncome.skin =lFboxLoanIncorrect ;
    frmMBeKYCPersonalInform.flxMain.scrollToWidget(frmMBeKYCPersonalInform.flxIncome);
  }else if(frmMBeKYCPersonalInform.lblSourceofIncome.isVisible){
    IsAllMandatryChecked =false;
    frmMBeKYCPersonalInform.flxSourceofIncome.skin =lFboxLoanIncorrect ;
    frmMBeKYCPersonalInform.flxMain.scrollToWidget(frmMBeKYCPersonalInform.flxSourceofIncome);
  }else if(frmMBeKYCPersonalInform.lblcountryofIncome.isVisible){
    IsAllMandatryChecked =false;
    frmMBeKYCPersonalInform.flxcountryofIncome.skin =lFboxLoanIncorrect ;
    frmMBeKYCPersonalInform.flxMain.scrollToWidget(frmMBeKYCPersonalInform.flxcountryofIncome);
  }else if(frmMBeKYCPersonalInform.lblObjectiveofAccount.isVisible){
    IsAllMandatryChecked =false;
    frmMBeKYCPersonalInform.flxObjectiveofAccount.skin =lFboxLoanIncorrect ;
    frmMBeKYCPersonalInform.flxMain.scrollToWidget(frmMBeKYCPersonalInform.flxObjectiveofAccount);    
  }else if(frmMBeKYCPersonalInform.FlextxtOtherObjAcc.isVisible && (frmMBeKYCPersonalInform.tbxOtherObjAcc.text === null || 
                                                                    frmMBeKYCPersonalInform.tbxOtherObjAcc.text === "")){
    IsAllMandatryChecked =false;
    frmMBeKYCPersonalInform.FlextxtOtherObjAcc.skin =lFboxLoanIncorrect ;
    frmMBeKYCPersonalInform.flxMain.scrollToWidget(frmMBeKYCPersonalInform.FlextxtOtherObjAcc);    
  }
//   else if(frmMBeKYCPersonalInform.lblsecndNationality.isVisible){
//     IsAllMandatryChecked =false;
//     frmMBeKYCPersonalInform.flxsecndNationality.skin =lFboxLoanIncorrect ;
//     frmMBeKYCPersonalInform.flxMain.scrollToWidget(frmMBeKYCPersonalInform.flxsecndNationality);
//   }
  return IsAllMandatryChecked;
}
//Country Dropdown Related Functions-------------------- Start
function setDefaultSecondNationality(){
  var countryCode= eKYCconfigObject.DefaultSecondNationality.NationalityCode;
  var countryName= "";
  var locale = kony.i18n.getCurrentLocale();
  for(var i=0; i < gblPersonalInfoMasterCollectionData.NationalityDS.length ; i++){
    if(countryCode == gblPersonalInfoMasterCollectionData.NationalityDS[i].varCode && locale === gblPersonalInfoMasterCollectionData.NationalityDS[i].languageCode){
      countryName = gblPersonalInfoMasterCollectionData.NationalityDS[i].varDesc ;
      break;
    }
  }
  CountrySelectedDropDownId= "flxsecndNationality";
  setPersonalInfoListPlaceHolderVisibility(false);
  frmMBeKYCPersonalInform.lblsecndNationalityCode.text = countryCode;
  frmMBeKYCPersonalInform.lblsecndNationalityDesc.text = countryName;

}
function setPersonalInfoListPlaceHolderVisibility(IsVisibleBlueText){
  if(CountrySelectedDropDownId== "flxsecndNationality"){
    frmMBeKYCPersonalInform.lblsecndNationality.isVisible = IsVisibleBlueText;
    frmMBeKYCPersonalInform.lblsecndNationalityHeader.isVisible = !IsVisibleBlueText;
    frmMBeKYCPersonalInform.lblsecndNationalityDesc.isVisible = !IsVisibleBlueText;      
  }

}