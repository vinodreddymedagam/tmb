







/*
 * Method CallBillPaymentAddService returns the status of service
 *
 */

function callBillPaymentAddServiceMB() {
	var toAccType;
	var toAcctNo = frmSelectBiller.segMyBills.selectedItems[0]["ToAccountKey"];
	toAcctNo = removeHyphenIB(toAcctNo);
	var billerMethod = frmSelectBiller.segMyBills.selectedItems[0]["billerMethod"];
	var billerGroupType = frmSelectBiller.segMyBills.selectedItems[0]["billerGroupType"];
	GBLFINANACIALACTIVITYLOG.toAcctId = frmSelectBiller.segMyBills.selectedItems[0]["ToAccountKey"];
	
	
	var transferFee = frmBillPaymentConfirmationFuture.lblPaymentFeeValue.text;
	var transferAmount = parseFloat(removeCommos(frmBillPaymentConfirmationFuture.lblAmountValue.text)); //+parseFloat(transferFee)
	
	
	if (toAcctNo.length == 14) {
		toAccType = "SDA";
	} else {
		fourthDigit = toAcctNo.charAt(3);
		if (fourthDigit == "2" || fourthDigit == "7" || fourthDigit == "9") {
			toAccType = "SDA";
			toAcctNo = "0000" + toAcctNo;
		} else {
			toAccType = "DDA";
		}
	}
	var invoice;
	if (gblreccuringDisableAdd == "Y") {
		invoice = frmBillPaymentConfirmationFuture.lblRef2Value.text;
	} else {
		invoice = "XXX"; //workaround
	}
	var accTypeCode;
	if (toAccType == "DDA") {
		accTypeCode = "0000";
	} else {
		accTypeCode = "0200";
	}
	var TMB_BANK_FIXED_CODE = "0001";
	var TMB_BANK_CODE_ADD = "0011";
	var fiident = TMB_BANK_CODE_ADD + TMB_BANK_FIXED_CODE + "0" + toAcctNo[0] + toAcctNo[1] + toAcctNo[2] + accTypeCode;
	inputParam = {};
	//inputParam = {};
	var text = frmBillPaymentConfirmationFuture.lblBillerNameCompCode.text;
	var compcode = text.substring(text.indexOf("(") + 1, text.indexOf(")"));
	var fromAcctNo = frmBillPaymentConfirmationFuture.lblAccountNum.text;
	fromAcctNo = removeHyphenIB(fromAcctNo);
	var fromAccType;
	if (fromAcctNo.length == 14) {
		fromAccType = "SDA";
	} else {
		fourthDigit = fromAcctNo.charAt(3);
		if (fourthDigit == "2" || fourthDigit == "7" || fourthDigit == "9") {
			fromAccType = "SDA";
			fromAcctNo = "0000" + fromAcctNo;
		} else {
			fromAccType = "DDA";
		}
	}
	var tranCode;
	if (fromAccType == "DDA") {
		tranCode = "8810";
	} else {
		tranCode = "8820";
	}
	var frmFiident;
	if (GblBillTopFlag) {
      var selectedItem = frmBillPayment.segSlider.selectedIndex[1];
      var selectedData = frmBillPayment.segSlider.data[selectedItem];
		frmFiident = selectedData.fromFIIdent
	} else {
		
      var selectedItem = frmTopUp.segSlider.selectedIndex[1];
      var selectedData = frmTopUp.segSlider.data[selectedItem];
		frmFiident = selectedData.fromFIIdent
	}
	inputParam["fromAcctNo"] = fromAcctNo;
	inputParam["fromAcctTypeValue"] = fromAccType;
	inputParam["toAcctNo"] = toAcctNo;
	inputParam["toAcctTypeValue"] = toAccType;
	inputParam["frmFiident"] = frmFiident;
	inputParam["transferAmt"] = transferAmount.toFixed(2); //frmBillPayment.lblForFullPayment.text  ;//need to know this value;//"amt+payment fee"
	inputParam["tranCode"] = tranCode;
	inputParam["toFIIdent"] = fiident;
	inputParam["billPmtFee"] = parseFloat(transferFee);
	inputParam["pmtRefIdent"] = frmBillPaymentConfirmationFuture.lblRef1Value.text;
	inputParam["invoiceNum"] = frmBillPaymentConfirmationFuture.lblRef2Value.text;
	inputParam["PostedDt"] = getTodaysDate();
	inputParam["EPAYCode"] = "EPYS";
	inputParam["channelName"] = "MB";
	inputParam["CompCode"] = compcode;
	inputParam["billPay"] = "billPay";
	
	invokeServiceSecureAsync("billpaymentAdd", inputParam, BillPaymentAddServiceCallBack);
}
//function callBillPaymentOnlineInqService(){
//				inputParam ={};
//			inputParam["SvcProvider"] ="AIS";
//			inputParam["ChannelName"] = "INTERNET";
//			inputParam["RqUID"] = " ";
//			inputParam["TrnId"] = " ";
//			inputParam["EffDt"] = "";
//			inputParam["BankId"] = "011";
//			inputParam["BranchId"] = "0001";
//			inputParam["TellerId"] = "0001";
//			inputParam["Ref1"] = frmBillPayment.lblRef1Value.text;
//			inputParam["Ref2"] = frmBillPayment.lblRef2.text;
//			inputParam["Ref3"] = " ";
//			inputParam["Ref4"] = "";
//			inputParam["InterRegionFee"] = "0.00";
//			invokeServiceSecureAsync("OnlinePaymentAdd", inputParam, BillPaymentOnlinePaymentAddServiceCallBack);
//
//
//
//}
/*
 * Method callOnlinePaymentAddService()
 *
 */


function TopUpPaymentOnlinePaymentAddServiceCallBack(status, resulttable) {
	
	if (status == 400) {
		
		
		if (resulttable["opstatus"] == 0) {
			
			// fetching To Account Name for TMB Inq
			var StatusCode = resulttable["StatusCode"];
			var Severity = resulttable["Severity"];
			var StatusDesc = resulttable["StatusDesc"];
			dismissLoadingScreen();
			
			if (StatusCode != "0") {
				alert("" + resulttable["errMsg"])
				dismissLoadingScreen();
				return false;
			} else {
				callTopUpPaymentCrmProfileUpdateService();
			}
		} else {
			dismissLoadingScreen();
			alert(" " + resulttable["errMsg"]);
			/**dismissLoadingScreen();**/
		}
	}
}
/*
 * Method CallBillPaymentTransferService
 *
 */

/*
 * CallBack of BillPaymentAddService
 *
 */

function TopUpBillPaymentAddServiceCallBack(status, resulttable) {
	
	if (status == 400) {
		
		
		if (resulttable["opstatus"] == 0) {
			
			// fetching To Account Name for TMB Inq
			var StatusCode = resulttable["StatusCode"];
			var Severity = resulttable["Severity"];
			var StatusDesc = resulttable["StatusDesc"];
			dismissLoadingScreen();
			
			if (StatusCode != "0") {
				alert("" + resulttable["errMsg"])
				return false;
			} else {
				callTopUpPaymentCrmProfileUpdateService();
			}
		} else {
			dismissLoadingScreen();
			alert(" " + resulttable["errMsg"]);
			/**dismissLoadingScreen();**/
		}
	}
}
/*
 * CallBack of BillPaymentTransferAddServiceCallBack
 *
 *
 */

function TopUpPaymentTransferAddServiceCallBack(status, resulttable) {
	
	if (status == 400) {
		
		
		if (resulttable["opstatus"] == 0) {
			
			// fetching To Account Name for TMB Inq
			var StatusCode = resulttable["StatusCode"];
			var Severity = resulttable["Severity"];
			var StatusDesc = resulttable["StatusDesc"];
			var fee = resulttable["transferFee"];
			
			if (StatusCode != "0") {
				dismissLoadingScreen();
				alert(resulttable["errMsg"]);
				return false;
			} else {
				callTopUpPaymentCrmProfileUpdateService();
			}
		} else {
			dismissLoadingScreen();
			alert(" " + resulttable["errMsg"]);
			/**dismissLoadingScreen();**/
		}
	}
}
/*
 * Method BillPaymentCRMProfileUpdateService
 *
 */

function callTopUpPaymentCrmProfileUpdateService() {
	inputParam = {};
	inputParam["actionType"]="0";
	if(gblPaynow){
		inputParam["ebAccuUsgAmtDaily"] = (UsageLimit + totalBillPayAmt).toFixed(2);
	}else {
		inputParam["ebAccuUsgAmtDaily"] = UsageLimit;
		
	}
	
	invokeServiceSecureAsync("crmProfileMod", inputParam, callTopUpPaymentCrmProfileUpdateServiceCallBack);
}
/*
 * CallBack of BillPaymentCRMProfileUpdateService
 *
 */

function callTopUpPaymentCrmProfileUpdateServiceCallBack(status, resulttable) {
	
	if (status == 400) {
		var activityTypeID = "";
		var errorCode = "";
		var activityStatus = "";
		var deviceNickName = "";
		var activityFlexValues5 = "";
		var activityFlexValues1 = frmBillPaymentConfirmationFuture.lblBillerNameCompCode.text.substring(0, frmBillPaymentConfirmationFuture.lblBillerNameCompCode.text.indexOf("("));
		var activityFlexValues2 = frmBillPaymentConfirmationFuture.lblAccountNum.text;
		var activityFlexValues3 = frmBillPaymentConfirmationFuture.lblRef1Value.text;
		var amt = parseFloat(frmBillPaymentConfirmationFuture.lblAmountValue.text);
		var fee = parseFloat(frmBillPaymentConfirmationFuture.lblPaymentFeeValue.text);
		var activityFlexValues4 = amt.toString() + "+" + fee.toString();
		var logLinkageId = "";
		if (gblPaynow) {
			activityTypeID = "030";
		} else {
			activityTypeID = "031";
		}
		
		
		if (resulttable["opstatus"] == 0) {
			
			// fetching To Account Name for TMB Inq
			var StatusCode = resulttable["StatusCode"];
			var Severity = resulttable["Severity"];
			var StatusDesc = resulttable["StatusDesc"];
			dismissLoadingScreen();
			
			activityStatus = "01";
			gotoBillPaymentCompleteMB(resulttable);
			callBillPaymentNotificationAddService();// not working
		} else {
			activityStatus = "02";
			alert(" " + resulttable["errMsg"]);
			/**dismissLoadingScreen();**/
		}
		activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1,
			activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId)
			if(gblPaynow){
		setFinancialActivityLogForTopupMB(frmBillPaymentConfirmationFuture.lblTxnNumValue.text,
			frmBillPaymentConfirmationFuture.lblAmountValue.text, frmBillPaymentConfirmationFuture.lblPaymentFeeValue.text,
			activityStatus);
			}
	}
}
/*
 * Method callTopUpPaymentNotificationAddService
 *
 */

function setFinancialActivityLogForTopupMB(finTxnRefId, finTxnAmount, finTxnFee, finTxnStatus) {
	GBLFINANACIALACTIVITYLOG.finTxnRefId = finTxnRefId;
	var activityType = "030"
	
	var tranCode;
	var fromAccType = frmBillPaymentConfirmationFuture.lblAccountName.text;
	if (fromAccType == "DDA") {
		tranCode = "8810";
	} else {
		tranCode = "8820";
	}
	GBLFINANACIALACTIVITYLOG.activityTypeId = activityType
	//02 is for mobile banking
	GBLFINANACIALACTIVITYLOG.channelId = "02"
	var fromAccount = frmBillPaymentConfirmationFuture.lblAccountNum.text;
	GBLFINANACIALACTIVITYLOG.fromAcctId = fromAccount.replace(/-/g, "");
	//GBLFINANACIALACTIVITYLOG.toAcctId = "";
	GBLFINANACIALACTIVITYLOG.toBankAcctCd = "11";
	GBLFINANACIALACTIVITYLOG.finTxnAmount = parseFloat(removeCommos(finTxnAmount)).toFixed(2);
	GBLFINANACIALACTIVITYLOG.txnCd = tranCode;
	GBLFINANACIALACTIVITYLOG.finTxnFee = parseFloat(removeCommos(finTxnFee)).toFixed(2);
	var availableBal = removeCommos(frmBillPaymentConfirmationFuture.lblBalBeforePayValue.text);
	availableBal = parseFloat(availableBal)
	
	if (finTxnStatus == "01")
		availableBal = availableBal - parseFloat(removeCommos(finTxnAmount)) + parseFloat(removeCommos(finTxnFee));
	GBLFINANACIALACTIVITYLOG.finTxnBalance = availableBal.toFixed(2);
	// 01 for success 02 for fail
	GBLFINANACIALACTIVITYLOG.finTxnStatus = finTxnStatus;
	GBLFINANACIALACTIVITYLOG.smartFlag = "N";
	GBLFINANACIALACTIVITYLOG.fromAcctName = frmBillPaymentConfirmationFuture.lblAccUserName.text;
	GBLFINANACIALACTIVITYLOG.toAcctName = frmBillPaymentConfirmationFuture.lblBillerNameCompCode.text.split("(")[0];
	GBLFINANACIALACTIVITYLOG.toAcctNickname = frmBillPaymentConfirmationFuture.lblBillerNickname.text;
	GBLFINANACIALACTIVITYLOG.errorCd = "0000";
	var yearPadd = new Date()
		.getFullYear()
		.toString()
		.substring(2, 2);
	var refNumber = finTxnRefId.substring(2, finTxnRefId.length - 2)
	GBLFINANACIALACTIVITYLOG.eventId = yearPadd + refNumber;
	GBLFINANACIALACTIVITYLOG.billerBalance = "0";
	GBLFINANACIALACTIVITYLOG.txnType = "005";
	GBLFINANACIALACTIVITYLOG.billerRef1 = frmBillPaymentConfirmationFuture.lblRef1Value.text;
	GBLFINANACIALACTIVITYLOG.billerRef2 = "";
	GBLFINANACIALACTIVITYLOG.billerCustomerName = frmBillPaymentConfirmationFuture.lblBillerNickname.text;
	GBLFINANACIALACTIVITYLOG.fromAcctNickname = frmBillPaymentConfirmationFuture.lblAccountName.text;//"Testnick";
	GBLFINANACIALACTIVITYLOG.finTxnMemo = frmBillPaymentConfirmationFuture.lblMyNoteValue.text;
	GBLFINANACIALACTIVITYLOG.tellerId = "4";
	//GBLFINANACIALACTIVITYLOG.txnDescription = "done";
	GBLFINANACIALACTIVITYLOG.finLinkageId = "1";
	GBLFINANACIALACTIVITYLOG.billerCommCode = frmSelectBiller["segMyBills"]["selectedItems"][0]["BillerCompCode"].text;;
	GBLFINANACIALACTIVITYLOG.finSchduleRefId = "SU" + kony.string.sub(finTxnRefId, 2, finTxnRefId.length);
	
	financialActivityLogServiceCall(GBLFINANACIALACTIVITYLOG);
}
