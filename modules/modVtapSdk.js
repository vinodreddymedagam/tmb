VTAPSdkAPIObject = null;
vTapInstancestatus = null;
dataTobeAuthenticate = {};
gblVtapTroubleShootInfo = {};
/****************************************************************
 * Name    : initVTapSDK()
 * Author  : TMB Bank Ltd. 
 * Purpose : To initalize the softtoken SDK and get the instance
 * 			of softtoken ffi
 ****************************************************************/
function initVTapSDK() {
    kony.print("VTapSDK : init JS Call In Start init function");
  var vtapInstance = false;
    try {
      		//#ifdef android
			//Creates an object of class 'VTAPSdkAPI'
			VTAPSdkAPIObject = new VTAPSDKIntegration.VTAPSdkAPI();

			if (kony.sdk.isNullOrUndefined(VTAPSdkAPIObject)) {
            	kony.print("VTapSDK : init Unable to create a instance of softtoken sdk ffi");
            } else {
            	vtapInstance = getVtapInstance();
            	kony.print("VTapSDK : initVTapSDK ");
            }
			//#endif

            //#ifdef iphone
			vtapInstance = getVtapInstance();
			//#endif
    } catch (e) {
         //sendTroubleShootingLogsvKey();
         kony.print("VTapSDK : " + "getting the error in init function: " + e.message);
    }
    kony.print("VTapSDK : " + "JS Call In End init function");
  return vtapInstance;
}



/****************************************************************
 * Name    : getVtapInstance()
 * Author  : TMB Bank Ltd. 
 * Purpose : To create the VTAP Instance
 ****************************************************************/
function getVtapInstance() {
    try {
        //#ifdef android
        kony.print("VTapSDK : Start getVtapInstance function: ");
		vTapInstancestatus = VTAPSdkAPIObject.getVtapInstance();
		//#endif
		
		//#ifdef iphone
        kony.print("VTapSDK : Start getVtapInstance function iphone");
      	vTapInstancestatus = VTAPSDKIntegrationIOS.getVtapInstance();
		//#endif
	} catch (e) {
       // sendTroubleShootingLogsvKey();
        kony.print("VTapSDK : " + "getVtapInstanceST: " + e.message);
    }
  
    kony.print("VTapSDK : End getVtapInstance function: " + vTapInstancestatus);
  return vTapInstancestatus;
}


function setHostNameVtap(vtapServerURL, provServerURL) {
    kony.print("VTapSDK : Start setHostname function vtapServerURL: " + vtapServerURL + ", provServerURL : "+provServerURL+", vtap status is: " +vTapInstancestatus);
    try {
    	if (vTapInstancestatus == true) {
    		gblVtapTroubleShootInfo.vtapInstance = "VTap Instance Creation Success";
        	//#ifdef android
			VTAPSdkAPIObject.setHostName(vtapServerURL, provServerURL);
			//#endif
			
			//#ifdef iphone
			VTAPSDKIntegrationIOS.setHostName(vtapServerURL, provServerURL);
			//#endif
	    	setupVTapST();
	    }else{
          gblVtapTroubleShootInfo.vtapInstance = "VTap Instance Creation Failed";
	     	// vTap instance failed
			 //sendTroubleShootingLogsvKey();
			 kony.print("VTapSDK : " + " vTap instance failed ");
	    }
        
    } catch (e) {
        kony.print("VTapSDK : " + "setHostNameST: " + e.message);
    }
    kony.print("VTapSDK : setHostname function End");
}


/****************************************************************
 * Name    : setupVTapST()
 * Author  : TMB Bank Ltd. 
 * Purpose : Need to do the setup process in the V-Key SDK and , 
 * 			hence internally V-Key SDK will do all the necessary 
 * 			initialization and we get the status of setUP Process.
 ****************************************************************/
function setupVTapST() {
    kony.print("VTapSDK : JS Call In Start setupVTap function");
    try {
        //#ifdef android
		VTAPSdkAPIObject.setupVtap(setupCallbackST,setupThreatsCallback);
		//gblSoftTokenData["vKeyDebugInfo"] = VTAPSdkAPIObject.getTroubleShootingID();
		//#endif
		
		//#ifdef iphone
      	kony.print("VTapSDK : JS Call In End setupVTap function");
		VTAPSDKIntegrationIOS.setupVTap(setupCallbackST, setupThreatsCallback);	
     	//#endif

      kony.print("VTapSDK : JS Call In End setupVTap function");
    } catch (e) {
       // sendTroubleShootingLogsvKey();
        kony.print("VTapSDK : Exception setupVTapST: " + e.message);
    }
    
}


/****************************************************************
 * Name    : setupThreatsCallback(threat)
 * Author  : TMB Bank Ltd/Aravinthan. 
 * Purpose : This method will get the setupStatus from the V-Key SDK, 
 * 			if setUp is succsus, we can go ahead for provisioning else 
 * 			get the fallback mechanism.
 ****************************************************************/
 
function setupThreatsCallback(strThreat)
{
	kony.print("VTapSDK : " + " The device is getting the threat " + strThreat);
}

/****************************************************************
 * Name    : setupCallbackST(status)
 * Author  : TMB Bank Ltd. 
 * Purpose : This method will get the setupStatus from the V-Key SDK, 
 * 			if setUp is succsus, we can go ahead for provisioning else 
 * 			get the fallback mechanism.
 ****************************************************************/
function setupCallbackST(status) {
    kony.print("VTapSDK : JS Call In Start setupCallback function status is: " + status);
        kony.print("VTapSDK : setupVtap status is: " + status);
        if(status == "40200" || status==40200){
          gblVtapInitialized = true;
          gblVtapTroubleShootInfo.setupVtap = "VTap setupVtap Success";
        	kony.print("VTapSDK : setupVtap status succuss: ");
		//#ifdef android
// 		if(gblPayLoadVtap != null && gblPayLoadVtap != undefined) {
// 			 kony.print("gblVtapInitialized gblPayLoadVtap is not empty");
// 			handleVtapPushNotification(gblPayLoadVtap);
// 			gblPayLoadVtap = null;
// 		}
		//#endif
         }else if(status == 40206){
           gblVtapInitialized = false;
           gblVtapTroubleShootInfo.setupVtap = "VTap setupVtap Failed Due to Permissions";
        	kony.print("VTapSDK : setupVtap status is fail due to permissions, hence calling setupVtap again: "); 
           setHostNameVtap(gblVtapServerURL, gblProvServerURL); 
           setPKIHostName(gblPkiServerURL);
         }else{
           gblVtapInitialized = false;
           gblVtapTroubleShootInfo.setupVtap = "VTap setupVtap Failed, Status :"+status;
        	kony.print("VTapSDK : setupVtap status is fail: "); 
         }
}


/****************************************************************
 * Name    : initiateProvisoning()
 * Author  : TMB Bank Ltd. 
 * Purpose : This method will take the apin and firmware from the middleware and do the provisoning.
 ****************************************************************/

function initiateProvisoning(tokenAPIN, tokenSerial) {
  var result = false;
    try {
      	kony.print(" initiateProvisoning > tokenAPIN is : " + tokenAPIN);
      kony.print(" initiateProvisoning > tokenSerial is : " + tokenSerial);
        //#ifdef android
      	createVtapObjIfNullForAndroid();
		var provisoningStatusCode = VTAPSdkAPIObject.initiateProvisoning(tokenAPIN, tokenSerial) + "";
		//#endif
		
		//#ifdef iphone
		var provisoningStatusCode = VTAPSDKIntegrationIOS.initiateProvisoning(tokenAPIN,tokenSerial)+"";
		//#endif
        kony.print(" initiateProvisoning > provisoningStatusCode is " + provisoningStatusCode);
       gblVtapTroubleShootInfo.provisoningStatusCode = provisoningStatusCode;
        if (provisoningStatusCode == 40600) {
          kony.print("initiateProvisoning > provisoningStatusCode success");
           result = isUserRegistered(tokenSerial); 
        } else if(provisoningStatusCode == 40606){
          	kony.print(" initiateProvisoning provisoningStatusCode failed Due to Invalid Customer Data");
	        showUVActivationFail("Step 10: V-Key : Provisioning Failed");
          //	unAssignTSAndGetAPINandTS(tokenSerial);
        } else {
          	kony.print(" initiateProvisoning provisoningStatusCode failed ");
	        showUVActivationFail("Step 11: V-Key : Provisioning Failed");
        }
         kony.print("initiateProvisoning isUserRegistered result :"+result);
      	if(result) {
          	kony.print(" provisioning done setting PKIHostName and Register Push Notification");
          	//updateUVStatusText("(50%)");
          updateUVStatusText("(75%)");
          	gblVtapTroubleShootInfo.provisioningDone = "true";
			callPushNotificationRegister(); 
        }
        kony.print(" in end initiateProvisoning: ");
    } catch (e) {
          //sendTroubleShootingLogsvKey();
          kony.print(" initiateProvisoning " + e.message);
      	  showUVActivationFail("Step 12: V-Key : Provisioning Failed");
    }
}

function isUserRegistered(tokenSerial) {
    kony.print("VTapSDK : JS Call In Start isUserRegistered function tokenSerial : "+tokenSerial);
    var userRegisterStatus = 0;   
  	var result = false;
    try {
          //#ifdef android
      		createVtapObjIfNullForAndroid();
			userRegisterStatus = VTAPSdkAPIObject.isUserRegistered(tokenSerial);
			//#endif
			
			//#ifdef iphone
			userRegisterStatus = VTAPSDKIntegrationIOS.isUserRegistered(tokenSerial);
			//#endif
            kony.print("VTapSDK : " + " isUserRegistered value is: " + userRegisterStatus);
      		gblVtapTroubleShootInfo.userRegisterStatus = userRegisterStatus;
            if (!userRegisterStatus) {
              
               kony.print("User is Registered False");
                var createPinStatus = createPin(tokenSerial) + "";
              kony.print("createPinStatus : "+createPinStatus);
              gblVtapTroubleShootInfo.createPinStatus = createPinStatus;
              if(createPinStatus == 40700) {
                  result = isRememberPINChecked(tokenSerial);
                  kony.print("isRememberPINChecked : "+result);
                gblVtapTroubleShootInfo.rememberPinStatus = result;
                  if(!result) {
                    var checkPinStatus = checkPin(tokenSerial) + "";
                     kony.print("checkPinStatus : "+checkPinStatus);
                    gblVtapTroubleShootInfo.checkPinStatus = checkPinStatus;
					result = true;
                  }
              }
            
        } else {
          result = true;
          kony.print("User Registered True ");
        }
       kony.print("VTapSDK:" + "JS Call In End isUserRegistered function result:"+result)
    } catch (e) {
        kony.print("VTapSDK :" + "isUserRegistered Exception: " + e.message);
    }
   ;
  return result;
}



function createPin(tokenSerial) {
    kony.print("VTapSDK" + "JS Call In Start createPin function");
    var pinStatus = null;
    
    try {
           //#ifdef android
      		createVtapObjIfNullForAndroid();
			pinStatus = VTAPSdkAPIObject.createPin(tokenSerial) + "";
			//#endif
			
			//#ifdef iphone
           	pinStatus = VTAPSDKIntegrationIOS.createPin(tokenSerial)+"";
			//#endif
            kony.print("VTapSDK:" + "pinStatus value is: " + pinStatus);
        return pinStatus;
    } catch (e) {
        kony.print("VTapSDK: " + "createPin: " + e.message);
    }
    kony.print("VTapSDK: " + "JS Call In End createPin function");
}

function isRememberPINChecked(tokenSerial) {
    kony.print("VTapSDK:" + "JS Call In Start isRememberPINChecked function");
    var isRememberPinCheckedStatus = false;
    
    try {
          //#ifdef android
      		createVtapObjIfNullForAndroid();
			isRememberPinCheckedStatus = VTAPSdkAPIObject.isRememberPINChecked(tokenSerial);
			//#endif
			
			//#ifdef iphone
           	isRememberPinCheckedStatus = VTAPSDKIntegrationIOS.isRememberPINChecked(tokenSerial);
			//#endif
        kony.print("VTapSDK:" + "isRememberPinCheckedStatus value is: " + isRememberPinCheckedStatus);
        
    } catch (e) {
        kony.print("VTapSDK:" + " Exception in isRememberPINChecked " + e.message);
    }
    kony.print("VTapSDK:" + "JS Call In End isRememberPINChecked function");
  return isRememberPinCheckedStatus;
}



function checkPin(tokenSerial) {
    kony.print("VTapSDK:" + "JS Call In Start checkPin function");
    var checkPinStatus = null;
    
    try {
    
           //#ifdef android
      		createVtapObjIfNullForAndroid();
			checkPinStatus = VTAPSdkAPIObject.checkPin(tokenSerial);
			//#endif
			
			//#ifdef iphone
           	checkPinStatus = VTAPSDKIntegrationIOS.checkPin(tokenSerial);
			//#endif
        kony.print("VTapSDK:" + "checkPinStatus value is: " + checkPinStatus);
        return checkPinStatus;
    } catch (e) {
        kony.print("VTapSDK:" + "checkPin" + e.message);
    }
    kony.print("VTapSDK:" + "JS Call In End checkPin function");
}


function onResume_softtoken() {
    try {
        kony.print("Before Softtoken onResume called");
         //#ifdef android
			VTAPSdkAPIObject.onResume();
			//#endif
				
		   //#ifdef iphone
           //TODO : Need to implement
			//#endif
        kony.print("After Softtoken onResume called");
    } catch (e) {
        kony.print("VTapSDK:" + "onResume_softtoken" + e.message);
    }
}

function onPause_softtoken() {
    try {
          kony.print("Before Softtoken onPause called");
           
           //#ifdef android
			VTAPSdkAPIObject.onPause();
			//#endif
				
		   //#ifdef iphone
           //TODO : Need to implement
			//#endif
        kony.print("After Softtoken onPause called");
    } catch (e) {
        kony.print("VTapSDK:" + "onPause_softtoken" + e.message);
    }
}

function onDestroy_softtoken() {
    try {
        kony.print("Before Softtoken onDestroy called");
        
          //#ifdef android
			VTAPSdkAPIObject.onDestroy();
			//#endif
				
		   //#ifdef iphone
           //TODO : Need to implement
			//#endif
        kony.print("After Softtoken onDestroy called");
    } catch (e) {
        kony.print("VTapSDK:" + "onDestroy_softtoken" + e.message);
    }
}



function callSetHostNamevTap(){
	try{
		if (vTapInstancestatus == true) {
        	if (!kony.sdk.isNullOrUndefined(gblSoftTokenData["vTapURL"])) {
                setHostNameST(gblSoftTokenData["vTapURL"]);
            } else {
                kony.print("VTapSDK:" + "gblVtapurl is empty");
            }
	    }else{
	     	// vTap instance failed
			 kony.print("VTapSDK:" + "vTap instance failedy");
	    }
	}catch(e){
		 kony.print("VTapSDK:" + "callSetHostNamevTap" + e.message);
	}
}


function sendTroubleShootingLogsvKey() {
	try{
      	  var logStatus = "";
          //#ifdef android
      		createVtapObjIfNullForAndroid();
          	logStatus = VTAPSdkAPIObject.sendTroubleShootingLogs();
         //#endif
         
         //#ifdef iphone
           logStatus = VTAPSDKIntegrationIOS.sendTroubleShootingLogs();
         //#endif
        kony.print("VTapSDK:" + "Send Trouble Shooting Logs: " + logStatus);
	}catch(e){
		kony.print("VTapSDK:" + "sendTroubleShootingLogsvKey" + e.message);
	}
}

function getVKeyTroubleShootingId() {
	try{
		var troubleShootLogID="";
          //#ifdef android
          troubleShootLogID= VTAPSdkAPIObject.getTroubleShootingID();
         //#endif
         
         //#ifdef iphone
          troubleShootLogID= VTAPSDKIntegrationIOS.getTroubleshootingId();
         //#endif
         kony.print("VTapSDK:" + "Trouble Shooting ID: " + troubleShootLogID);
		return troubleShootLogID;
	}catch(e){
		kony.print("VTapSDK:" + "getVKeyTroubleShootingId" + e.message);
	}
}


/**
 * @function
 *
 */
function pushNotificationRegister(userId, deviceId, pushToken, tokenSerial) {
  	kony.print("Inside pushNotificationRegister for Android...");
	var pushStatus=false;
  try{
   		//#ifdef android
    	kony.print("Inside pushNotificationRegister before call : ");
    	createVtapObjIfNullForAndroid();
         pushStatus=VTAPSdkAPIObject.pushNotificationRegister(userId, deviceId, pushToken, tokenSerial);    	
         //#endif
       	// gblSoftTokenData["pushRegisterStatus"] = pushStatus +"";  
    	//#ifdef iphone
        pushStatus = VTAPSDKIntegrationIOS.pushNotificationRegister(userId,deviceId,pushToken,tokenSerial);
		//#endif
		gblVtapTroubleShootInfo.pushRegisterStatus = pushStatus;
    	kony.print("Inside pushNotificationRegister after pushStatus : "+pushStatus);
  	}catch(e){
		kony.print("VTAPSDK : Excpetion in pushNotificationRegister >>"+e.message);
	}	
  	return pushStatus;
}

/**
 * @function
 *
 * @param pkiServerURL 
 */
function setPKIHostName(pkiServerURL) {
  		kony.print("Inside setPKIHostName for Android...");
  try{
        if(kony.sdk.isNullOrUndefined(pkiServerURL)){
          kony.print("PKI url is null from service, hence adding from JS");
          pkiServerURL = gblPkiServerURL; 
        }
  		//#ifdef android
         VTAPSdkAPIObject.setPKIHostName(pkiServerURL);
         //#endif
    	//#ifdef iphone
    	kony.print("IOS PKI url from JS");
        VTAPSDKIntegrationIOS.setPKIHostName(pkiServerURL);
		//#endif
    	//Calling directly on the FFI/SDK IOS
	}catch(e){
		kony.print("VTAPSDK : Excpetion in setPKIHostName >>"+e.message);
	}	 
}

function pkiCertDownload(functionId, messageId, messageType, tokenSerial) {
  	kony.print("Inside pkiCertDownload ...tokenSerial:"+tokenSerial);
  var result = false;
  try {
   		//#ifdef android
    	  createVtapObjIfNullForAndroid();
          result = VTAPSdkAPIObject.pkiCertDownload(functionId, messageId, messageType, tokenSerial);
         //#endif
    	//#ifdef iphone
    	 result = VTAPSDKIntegrationIOS.pkiCertDownload(functionId,messageId,messageType,tokenSerial);
    	//#endif
    		kony.print("Inside pkiCertDownload result>>"+result);
  		}catch(e){
			kony.print("VTAPSDK : Excpetion in pkiCertDownload >>"+e.message);
		}	
  	return result;
}

/**
 * @function
 *
 * @param String messageId 
 */
function vMessageAck(messageId) {
	kony.print("Inside vMessageAck for Android...");
  	var result = false;
  	try {
      
   		//#ifdef android
      	createVtapObjIfNullForAndroid();
         result = VTAPSdkAPIObject.vMessageAck(messageId);
         //#endif
      //#ifdef iphone
     	result = VTAPSDKIntegrationIOS.vMessageAck(messageId);
      //#endif
      kony.print("Inside vMessageAck result>>"+result);
      
  	}catch(e){
			kony.print("VTAPSDK : Excpetion in vMessageAck >>"+e.message);
	}	
  	return result;
}

function checkDeviceCompatibility() {
	kony.print("Inside checkDeviceCompatibility...");
  	updateUVStatusText("(12%)");
    var result = false;
  	var resultCode="";
  	try {
   		//#ifdef android
      	createVtapObjIfNullForAndroid();
        resultCode = VTAPSdkAPIObject.checkDeviceCompatibility();
      	//#endif
      	//#ifdef iphone
      	resultCode = VTAPSDKIntegrationIOS.checkDeviceCompatibility();
      	//#endif
 		 kony.print("Inside checkDeviceCompatibility resultCode : "+resultCode);
      	  gblVtapTroubleShootInfo.deviceCheckStatus = "checkDeviceCompatibility :"+resultCode;
          if(resultCode == "40300" || resultCode == "40301" || resultCode == "40302") {
            	result = true;
          }
         
      	
  	}catch(e){
		kony.print("VTAPSDK : Excpetion in checkDeviceCompatibility >>"+e.message);
	}
  	return result;
}

function isPKIFunctionPINRemembered(functionId) {
	kony.print("Inside isPKIFunctionPINRemembered for Android check..."+functionId);
  	var result = false;
  	try {
      
   		//#ifdef android
      	createVtapObjIfNullForAndroid();
        result = VTAPSdkAPIObject.isPKIFunctionPINRemembered(functionId);
        //#endif
      
      	//#ifdef iphone
      	result = VTAPSDKIntegrationIOS.isPKIFunctionPINRemembered(functionId);
      	//#endif
      
      kony.print("Inside isPKIFunctionPINRemembered result>>"+result);
      
  	}catch(e){
			kony.print("VTAPSDK : Excpetion in isPKIFunctionPINRemembered >>"+e.message);
	}	
  	return result;
}


function pkiFunctionAuthenticate(messageId, dataToBeSigned, reject) {
	kony.print("Inside pkiFunctionAuthenticate ...");
  	var result = false;
  	try {
      
   		//#ifdef android
      		createVtapObjIfNullForAndroid();
         	var authStatus = VTAPSdkAPIObject.pkiFunctionAuthenticate(messageId, dataToBeSigned, reject);
      	//#endif
      
      	//#ifdef iphone
      		var authStatus = VTAPSDKIntegrationIOS.pkiFunctionAuthenticate(messageId,dataToBeSigned,reject);
      	//#endif
       		kony.print("Inside pkiFunctionAuthenticate authStatus>>"+authStatus);    	
      		if(authStatus == 41102 || authStatus == 41160) {
          		result = true;
        	}
         
      kony.print("Inside pkiFunctionAuthenticate result>>"+result);
      
  	}catch(e){
			kony.print("VTAPSDK : Excpetion in pkiFunctionAuthenticate >>"+e.message);
	}	
  	return result;
}

/**
 * @function
 *
 * @param String messageId 
 */
function vMessageDownload(messageId, messageType, messageFlag) {
	kony.print("Inside vMessageDownload for Android...");
  	var result = false;
  	try {
      
   		//#ifdef android
      	createVtapObjIfNullForAndroid();
         result = VTAPSdkAPIObject.vMessageDownload(messageId, messageType, messageFlag);
         //#endif
      	//#ifdef iphone
      	 result = VTAPSDKIntegrationIOS.vMessageDownload(messageId,messageType,messageFlag);
		
        //#endif
      kony.print("Inside vMessageDownload result>>"+result);
      
  	}catch(e){
			kony.print("VTAPSDK : Excpetion in vMessageDownload >>"+e.message);
	}	
  	return result;
}


function createVtapObjIfNullForAndroid() {
  kony.print("VTAPSDK:Checking VTAPSdkAPIObject is null or not");
    if (kony.sdk.isNullOrUndefined(VTAPSdkAPIObject)) {
      kony.print("VTAPSDK: VTAPSdkAPIObject is null hence creating new object");
      initVTapSDK();
    }
  
}