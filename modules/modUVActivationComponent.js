function getAPINandTS(){
  kony.print("Inside getAPINandTS for Android");
  var inputParam = {};
  invokeServiceSecureAsync("uvAssignApinTSFirmware", inputParam, callBackgetAPINandTS)
}

function callBackgetAPINandTS(status, resulttable) {
	if (status == 400) {
       kony.print("Inside callBackgetAPINandTS for Android");
		if (resulttable["opstatus"] == 0) {
          	
          gblTokenSerial = resulttable["tokenSerial"];
          gblTokenAPIN = resulttable["aPin"];
		  
          gblVtapTroubleShootInfo.tokenSerial = gblTokenSerial;	
          gblVtapTroubleShootInfo.tokenAPin = gblTokenAPIN;
          
           kony.print("Inside callBackgetAPINandTS for Android gblTokenSerial>>"+gblTokenSerial);
          kony.print("Inside callBackgetAPINandTS for Android gblTokenAPIN>>"+gblTokenAPIN);
          //updateUVStatusText("(38%)");
          updateUVStatusText("(50%)");
          initiateProvisoning(gblTokenAPIN, gblTokenSerial);
          
        } else {
          	gblVtapTroubleShootInfo.uvactivation = "fail";
          	showUVActivationFail("Step 9: V-Key: TS & APIN Service Failed");
			return false;
        }
    }
}

function unAssignTSAndGetAPINandTS(oldTokenSerial){
  kony.print("Inside unAssignTSAndGetAPINandTS");
  showLoadingScreen();
  var inputParam = {};
  inputParam.oldTokenSerial = oldTokenSerial;
  inputParam.tokenProvisioned = "TRUE";
  invokeServiceSecureAsync("uvAssignApinTSFirmware", inputParam, callBackgetAPINandTS)
}


/**
 * @function
 *
 */
function callPushNotificationRegister() {
  	var ksID = kony.store.getItem("ksid");
  	kony.print("Inside callPushNotificationRegister ksID:"+ksID);
  	gblVtapTroubleShootInfo.ksId = ksID;
    var pushToken = kony.store.getItem("kpnssubcriptiontoken");
  kony.print("Inside callPushNotificationRegister pushToken:"+pushToken);
  	if(isNotBlank(ksID) && isNotBlank(pushToken)) {	
      	var ksIdandTS = ksID + "~" + gblTokenSerial; // To handle ksid duplicate issue
  		var pushStatus = pushNotificationRegister(ksIdandTS, "", pushToken, gblTokenSerial);
      	if(pushStatus) {
           //updateUVStatusText("(62%)");
          gblVtapTroubleShootInfo.uvActivation = "Done";
          kony.print("UV Activation completed successfully from VTAP >>>");
          updateUVStatusText("100%");
          showUVActivationSuccess(true);
        } else {
         	kony.print("Inside pushNotificationRegister Failed:");
          	showUVActivationFail("Step 13: V-Key : SubscriptionToken Register Failed");
        }
    } else {
      kony.print("Inside callPushNotificationRegister ksID is null:");
      showUVActivationFail("Step 14: V-Key : SubscriptionToken Register Failed");
    }
}


/**
 * @function
 *
 * @param payLoad 
 */
function handleVtapPushNotification(payload){
  gblUVPushContent = "";
  if(!isNotBlank(payload)) {
     	kony.print("VTAPSDK : payload is empty");
    	return false;
  }
  
 var pushVMsg = JSON.parse(JSON.stringify(payload));
  kony.print("VTAPSDK : handleVtapPushNotification pushVMsg :"+pushVMsg);
     
    if(isNotBlank(pushVMsg)) {
	 //#ifdef iphone
      var alertObj = JSON.parse(JSON.stringify(pushVMsg["alert"]));
  	  kony.print("VTAPSDK : handleVtapPushNotification Alert Action :"+alertObj["action"]);
      if(GLOBAL_UV_STATUS_FLAG == "ON" && alertObj["action"] != undefined){
        kony.print("VTAPSDK : handleVtapPushNotification alertObj :"+alertObj);
         var actionObj = JSON.parse(JSON.stringify(alertObj["action"]));
        kony.print("VTAPSDK : handleVtapPushNotification actionObj :"+actionObj);
        var msgObj = JSON.parse(actionObj);
  	 }else{
       	kony.print("iPhone: Normal Push Notification Case >>>");

        if(isNotBlank(pushVMsg["payloadMsg"])){
				showPopUpPush(pushVMsg["payloadMsg"]);
        }
       	return;
     }
    //#else

      if(GLOBAL_UV_STATUS_FLAG == "ON" && pushVMsg["message"] != undefined){
		 var msgObj = JSON.parse(pushVMsg["message"]);
	  }else{
        kony.print("Android: Normal Push Notification Case >>>");
        if(isNotBlank(pushVMsg["payloadMsg"])){
			showPopUpPush(pushVMsg["payloadMsg"]);
        }
        
		return;
	  }
    //#endif

       kony.print("VTAPSDK : handleVtapPushNotification msgObj :"+msgObj);
      var messageId = msgObj["messageId"];
      var messageType = msgObj["messageType"];
      kony.print("VTAPSDK : handleVtapPushNotification messageId :"+messageId);
      kony.print("VTAPSDK : handleVtapPushNotification messageType :"+messageType);
      
	//	if(messageType == "ASP_CERT" ||messageType == "SMP_CERT" || messageType == "AUTH") {
			gblUVPushContent = pushVMsg;
		/*
          	if(messageType == "ASP_CERT" && isNotBlank(gblTokenSerial)) {
              	updateUVStatusText("(75%)");
              	gblVtapTroubleShootInfo.receivedAspCertPush = "true";
          		var aspCertDownloadStatus = pkiCertDownload(0, messageId, messageType, gblTokenSerial);
              	gblVtapTroubleShootInfo.aspCertDownloadStatus = "ASP_CERT download status :"+aspCertDownloadStatus;
              if(aspCertDownloadStatus){
                  	kony.print("ASP_CERT download successfully from VTAP >>>");
                	updateUVStatusText("(87%)");
                }else{
                  kony.print("ASP_CERT download Failed from VTAP >>>");
                  showUVActivationFail("Step 15: V-Key: ASP_CERT download Failed");
                }
            } else if(messageType == "SMP_CERT" && isNotBlank(gblTokenSerial)){
              	updateUVStatusText("(100%)");
              	gblVtapTroubleShootInfo.receivedSMPCertPush = "true";
              	var smpCertDownloadStatus = pkiCertDownload(1, messageId, messageType, gblTokenSerial);
              	gblVtapTroubleShootInfo.smpCertDownloadStatus = "SMP_CERT download status :"+smpCertDownloadStatus;
              	if(smpCertDownloadStatus){
                  	gblVtapTroubleShootInfo.uvActivation = "Done";
                  	kony.print("UV Activation completed successfully from VTAP >>>");
                  	updateUVStatusText("");
                  	showUVActivationSuccess(true);
                } else {
                  kony.print("SMP_CERT download Failed from VTAP >>>");
                  showUVActivationFail("Step 16: V-Key: SMP_CERT download Failed");
                }
            } else
              */
            if(messageType == "AUTH"){
              	kony.print("AUTH Push from VTAP >>>");
              	showLoadingScreen();
              	setUpVTapSDK();

				kony.timer.schedule("uvPushTimer", handleUVPushTimer, 0.5, true);	
              
              	kony.print("After uvPushTimer");
            }
		//}
        
  } else {
    kony.print("Payload incorrect >>> ")
   // showUVActivationFail();
  }
  
}


function handleUVPushTimer() {
  	  kony.print("VTAPSDK : Inside handleUVPushTimer gblVtapInitialized:"+gblVtapInitialized);
      if(gblVtapInitialized) {
          kony.print("In callback of uvPushTimer");
          kony.timer.cancel("uvPushTimer");
          gblFromUVReject = false;
          frmUVApprovalMB.show();
         kony.print("After frmUVApprovalMB show");
          dismissLoadingScreen();
      }
}

function handleUVPushMessage(pushContent) {
        //#ifdef iphone
    var alertObj = JSON.parse(JSON.stringify(pushContent["alert"]));
  kony.print("VTAPSDK : handleUVPushMessage alertObj :"+alertObj);
     var actionObj = JSON.parse(JSON.stringify(alertObj["action"]));
  kony.print("VTAPSDK : handleVtapPushNotification actionObj :"+actionObj);
  var contentArr = JSON.parse(actionObj);
    //#else
     var contentArr =  JSON.parse(pushContent["message"]);
    //#endif
  kony.print("VTAPSDK : handleUVPushMessage contentArr:"+contentArr);
            var messageId = contentArr["messageId"];
            var messageType = contentArr["messageType"];
  kony.print("VTAPSDK : handleUVPushMessage  , messageId:"+messageId);
  kony.print("VTAPSDK : handleUVPushMessage  , messageType:"+messageType);
            //Authentication
            gblVtapTroubleShootInfo.approvalPush = "Approval Push Received messageType :"+messageType;
            // if(vMessageAck(messageId)) {
            vMessageAck(messageId);
            var passType = contentArr["passType"];
            kony.print("VTAPSDK : handleUVPushMessage vMessageAck Success , passType:"+passType);
            gblVtapTroubleShootInfo.passType = "Approval Push Received passType :"+passType;
            var msgToDisplay = "";
            if(passType == 1) {
              //Show Details Screen
              var msgJson = contentArr["notifyMsg"];
              msgToDisplay = JSON.parse(msgJson);
              showUVPushDetails(msgToDisplay);
            } else if(passType == 2){
              var messageFlag = contentArr["msgFlag"];
              kony.print("VTAPSDK : handleVtapPushNotification vMessageAck Success , passType = 2, messageFlag:"+messageFlag);
              var resultJson = vMessageDownload(messageId, messageType, messageFlag);
              kony.print("msgToDisplay resultJson : "+resultJson);
              var jsonObj = JSON.parse(resultJson);
              kony.print("msgToDisplay jsonObj : "+JSON.stringify(jsonObj));
              var base64Str = jsonObj.data;
              kony.print("msgToDisplay base64Json : "+base64Str);
              msgToDisplay = base64ToJson(base64Str);

              kony.print("msgToDisplay JSON : "+JSON.stringify(msgToDisplay));

              dataTobeAuthenticate.messageId = messageId;
              dataTobeAuthenticate.dataToBeSigned = jsonObj.dataToBeSigned;

              kony.print(" dataTobeAuthenticate JSON : "+JSON.stringify(dataTobeAuthenticate));

              showUVPushDetails(msgToDisplay);
            }

}

/**
* Please uncomment the bleow methods for SIT & UAT Build release
*/
/*
function handleVtapPushNotification(payload){
  var pushContent="";
  gblUVPushContent = "";
  
  if (kony.string.startsWith(gblDeviceInfo.name, "iPhone", true)){
    pushContent = payload["payloadMsg"];
  }
  else
  {
    pushContent = payload["payloadMsg"];
  }
  
  kony.print("VTAPSDK : handleVtapPushNotification pushContent :"+pushContent);
  if(isNotBlank(pushContent)) {
    	gblUVPushContent = pushContent;
    	var contentArr = pushContent.split("@#");
    	var pushType = contentArr[0];
    	
    	kony.print("VTAPSDK : handleVtapPushNotification pushType :"+pushType);
    	
    	if(pushType == "VTAP") {
          	var messageId = contentArr[2];
    		var messageType = contentArr[3];
          	kony.print("VTAPSDK : handleVtapPushNotification messageId :"+messageId);
          	kony.print("VTAPSDK : handleVtapPushNotification messageType :"+messageType);
          	if(messageType == "ASP_CERT" && isNotBlank(gblTokenSerial)) {
              	updateUVStatusText("Downloading ASP Certificate");
              	gblVtapTroubleShootInfo.receivedAspCertPush = "true";
          		var aspCertDownloadStatus = pkiCertDownload(0, messageId, messageType, gblTokenSerial);
              	gblVtapTroubleShootInfo.aspCertDownloadStatus = "ASP_CERT download status :"+aspCertDownloadStatus;
              if(aspCertDownloadStatus){
                  	kony.print("ASP_CERT download successfully from VTAP >>>");
                	updateUVStatusText("Successfully Downloaded ASP Cert");
                }else{
                  kony.print("ASP_CERT download Failed from VTAP >>>");
                  showUVActivationFail();
                }
            } else if(messageType == "SMP_CERT" && isNotBlank(gblTokenSerial)){
              	updateUVStatusText("Downloading SMP Certificate");
              	gblVtapTroubleShootInfo.receivedSMPCertPush = "true";
              	var smpCertDownloadStatus = pkiCertDownload(1, messageId, messageType, gblTokenSerial);
              	gblVtapTroubleShootInfo.smpCertDownloadStatus = "SMP_CERT download status :"+smpCertDownloadStatus;
              	if(smpCertDownloadStatus){
                  	gblVtapTroubleShootInfo.uvActivation = "Done";
                  	kony.print("UV Activation completed successfully from VTAP >>>");
                  	updateUVStatusText("Successfully Downloaded SMP Cert, UV Activated successfully");
                  	showUVActivationSuccess();
                } else {
                  kony.print("SMP_CERT download Failed from VTAP >>>");
                  showUVActivationFail();
                }
            } else if(messageType == "AUTH"){
              	kony.print("AUTH Push from VTAP >>>");
      			frmUVApprovalMB.show();
            }
        }
  } else {
    kony.print("Payload incorrect >>> ")
    showUVActivationFail();
  }
  
}

function handleUVPushMessage(pushContent) {
  		
            var contentArr = pushContent.split("@#");
            var messageId = contentArr[2];
            var messageType = contentArr[3];
            //Authentication
            gblVtapTroubleShootInfo.approvalPush = "Approval Push Received messageType :"+messageType;
            // if(vMessageAck(messageId)) {
            vMessageAck(messageId);
            var passType = contentArr[7];
            kony.print("VTAPSDK : handleVtapPushNotification vMessageAck Success , passType:"+passType);
            gblVtapTroubleShootInfo.passType = "Approval Push Received passType :"+passType;
            var msgToDisplay = "";
            if(passType == 1) {
              //Show Details Screen
              var msgJson = contentArr[8];
              msgToDisplay = JSON.parse(msgJson);
              showUVPushDetails(msgToDisplay);
            } else if(passType == 2){
              var messageFlag = contentArr[5];
              kony.print("VTAPSDK : handleVtapPushNotification vMessageAck Success , passType = 2, messageFlag:"+messageFlag);
              var resultJson = vMessageDownload(messageId, messageType, messageFlag);
              kony.print("msgToDisplay resultJson : "+resultJson);
              var jsonObj = JSON.parse(resultJson);
              kony.print("msgToDisplay jsonObj : "+JSON.stringify(jsonObj));
              var base64Str = jsonObj.data;
              kony.print("msgToDisplay base64Json : "+base64Str);
              msgToDisplay = base64ToJson(base64Str);

              kony.print("msgToDisplay JSON : "+JSON.stringify(msgToDisplay));

              dataTobeAuthenticate.messageId = messageId;
              dataTobeAuthenticate.dataToBeSigned = jsonObj.dataToBeSigned;

              kony.print(" dataTobeAuthenticate JSON : "+JSON.stringify(dataTobeAuthenticate));

              showUVPushDetails(msgToDisplay);
            }

}
*/
/**
 * @function
 *
 */
function setUpVTapSDK() {
  kony.print("VTAPSDK : setUpVTapSDK GLOBAL_UV_STATUS_FLAG >>>"+GLOBAL_UV_STATUS_FLAG);
  if(GLOBAL_UV_STATUS_FLAG == "ON"){
       
          kony.print("VTAPSDK : Inside setUpVTapSDK gblVtapInitialized:"+gblVtapInitialized);
          if(gblVtapInitialized) {
              kony.print("VTAPSDK : Already setUpVTapSDK Initialized");
              return;
          }
    
        var status = initVTapSDK();
     	
        kony.print("VTAPSDK : Inside setUpVTapSDK  status>>>"+status);
        
    	if(status) {
          setHostNameVtap(gblVtapServerURL, gblProvServerURL);  
          setPKIHostName(gblPkiServerURL);
        } else {
          //Show Error Message
        }
  	}
}

/**
 * @function
 * This is the function to start UV Activation flow
 */
function setUpProvisioning() {
  	  kony.print("Inside setUpProvisioning ...");
      if(GLOBAL_UV_STATUS_FLAG == "ON") {
         if(gblVtapInitialized) {
              showUVLoading();
              //if(checkDeviceCompatibility()) { //Check Device Compatibility
                  updateUVStatusText("(25%)");
                 // kony.print("Inside CheckDeviceCompatibility is Success...");
                  getAPINandTS();  
         /*     } else {
                  //Handle Error case
                  kony.print("Inside CheckDeviceCompatibility is Failed ...");
                  showUVActivationFail("Step 8: V-Key: CheckDevice Compatibility Failed");
              }*/
        } else {
             //Handle Error case
              kony.print("Inside Vtap Setup is Failed ...");
              showUVActivationFail("Step 18: V-Key: Vtap Setup is Failed");
        }
      }
}


/**
 * @function
 *
 */
function setUpPKIAuthntication(messageId, dataToBeSigned, reject) {
      	kony.print("Inside setUpPKIAuthntication  ...");
        kony.print("Inside setUpPKIAuthntication messageId...:"+messageId);
  		kony.print("Inside setUpPKIAuthntication dataToBeSigned...:"+dataToBeSigned);
          if(isPKIFunctionPINRemembered(0)) {
            kony.print("Inside setUpPKIAuthntication isPKIFunctionPINRemembered Success");
			var result = pkiFunctionAuthenticate(messageId, dataToBeSigned, reject)
			 kony.print("Inside setUpPKIAuthntication pkiFunctionAuthenticate Result ..."+result);
            return result;
          }
  	return false;
}

/**
 * @function
 *
 * @param base64String 
 */
function base64ToJson(base64String) {
  kony.print("base64ToJson  base64String>>>> "+base64String);
  try{
     	var base64Decode = decodeBase64(base64String);
  		kony.print("base6dDecode >>>> "+base64Decode);
  		return JSON.parse(base64Decode);
  }catch(e) {
    kony.print("base6dDecode Exception >>>> "+e.message);
  }
 
  return base64String;
}


function showUVPushDetails(msgToDisplay){
  if(msgToDisplay !== undefined) {
      kony.print("Inside showUVPushDetails >>> "+JSON.stringify(msgToDisplay));
      var pushTxnData = msgToDisplay.transaction;
      if(pushTxnData !== undefined) {
            gblUVTransactionData.pushTxnData = pushTxnData;
            kony.print("Inside showUVPushDetails pushTxnData>>> "+JSON.stringify(msgToDisplay.transaction));
        	gblVtapTroubleShootInfo.finalPushToDisplay = "Received JSON data to display on Approval Screen :";
           	showUVApprovalDetails();
        	populateUVTransactionDetailsDisplay(pushTxnData);
      } else {
        	hideUVApprovalDetails();
      }
  } else {
    	kony.print("Push Payload Incorrect");
    	hideUVApprovalDetails();
  }
  
}


function hideUVApprovalDetails() {
  	frmUVApprovalMB.flexBody.setVisibility(false);
    frmUVApprovalMB.flexFooter.setVisibility(false);
}


function showUVApprovalDetails() {
  	frmUVApprovalMB.flexBody.setVisibility(true);
    frmUVApprovalMB.flexFooter.setVisibility(true);
}


function showUVLoading() {
    gblConfirmLoadingIndicatorCount = gblUVLoadingIndicatorTimeOut;
  	kony.print("Show UV Loading Screen");
  	dismissLoadingScreen();
	if( gblActivationCurrentForm == "cardlessWithdraw" || gblActivationCurrentForm == "TMBConfirm"){
		frmTMBConfirmLoadingScreen.lblHdrTxt.text = kony.i18n.getLocalizedString("LD02_txtTittle");
	}else{
		frmTMBConfirmLoadingScreen.lblHdrTxt.text = kony.i18n.getLocalizedString("LD01_txtTittle");
	}
  	frmTMBConfirmLoadingScreen.lblActiveCodeError.text = kony.i18n.getLocalizedString("LD01_txtTopic");
   	frmTMBConfirmLoadingScreen.show();
    try{
		//frmTMBConfirmLoadingScreen.lblRemainingTime.text = "Remaining Time:" + gblConfirmLoadingIndicatorCount;
        kony.timer.schedule("uvLoadingTimer" , showUVLoadingWithTimer, 1, true);

    }catch(e){
      kony.print("Exception in UV timer"+e.message)
    }
}


function showUVLoadingWithTimer(){
	kony.print("gblConfirmLoadingIndicatorCount"+gblConfirmLoadingIndicatorCount);
   
  	if(gblConfirmLoadingIndicatorCount > 0){
		gblConfirmLoadingIndicatorCount--;
      	//frmTMBConfirmLoadingScreen.lblRemainingTime.text = "Remaining Time:" + gblConfirmLoadingIndicatorCount;
       	
    }else{
      	cancelScheduledTimer("uvLoadingTimer");
     	showUVActivationFail("Step 17: UV registration process taking long time");
    }
}
function showUVActivationFail(failedStep) {
  
  if(GLOBAL_UV_STATUS_FLAG == "ON"){
  
  	 kony.print("Show UV Activation Fail");
  	 //uvactivationFlow - UV Activity log purpose
     uvactivationFlow(failedStep,"Failed");
  	 cancelScheduledTimer("uvLoadingTimer");
    gblUVregisterMB = "N";
  	if(isSignedUser){
     	frmUVFailureScreen.btnLater.onClick = onClickUVActFailureScreenLater;
     	frmUVFailureScreen.btnTryAgain.onClick = setUpProvisioning;
       //#ifdef android  
      frmUVFailureScreen.onDeviceBack = doNothing;
      //#endif

      	frmUVFailureScreen.lblHdrTxt.text = kony.i18n.getLocalizedString("TCF_txtTittle");
		frmUVFailureScreen.lblActiveCodeError.text = kony.i18n.getLocalizedString("TCF_txtFailedTopic");
		frmUVFailureScreen.CopylblActiveCodeError0d1a7cbedbeea47.text = kony.i18n.getLocalizedString("TCF_txtFailedDesc");
		frmUVFailureScreen.btnLater.text = kony.i18n.getLocalizedString("TCF_btnLater");
		frmUVFailureScreen.btnTryAgain.text = kony.i18n.getLocalizedString("TCF_btnTry");
      
  		frmUVFailureScreen.show();
    	}else{
      		showUVActivationSuccess(false);	
    	}
	}
}


function onClickUVActFailureScreenLater() {
  	kony.print("Show UV Activation Success >>isSignedUser>>"+isSignedUser);
  	
  	cancelScheduledTimer("uvLoadingTimer");
    if(isSignedUser){
        callCustomerAccountService();	
    }else{
        callServiceStartMB();
    }
  	
}


function showUVActivationSuccess(status) {
  
  	if(GLOBAL_UV_STATUS_FLAG == "ON"){
  	kony.print("Show UV Activation Success >>isSignedUser>>"+isSignedUser);
  //uvactivationFlow - UV Activity log purpose
  if(status == true || status == "true"){
   uvactivationFlow("","Success");
  }
  if(gblStartClickFromActivationMB) {
    gblStartClickFromActivationMB = false;
  	cancelScheduledTimer("uvLoadingTimer");
  	dismissLoadingScreen();
    if(isSignedUser){
      //#ifdef android
      	frmUVActivationSuccessful.onDeviceBack = doNothing;
      //#endif
        frmUVActivationSuccessful.show() ;	
    }else{
            if(gblRMOpenAcct){
                SuccessPineKYC(gblRMOpenAcctresult);
            }else{
               //#ifdef android
                frmRegActivationSuccess.onDeviceBack = doNothing;
                //#endif
              frmRegActivationSuccess.destroy();
              frmRegActivationSuccess.show();
            }
  		}
	}
  }
}


function updateUVStatusText(currentStatus) {
  
  /*var statusDots = frmTMBConfirmLoadingScreen.lblRemainingTime.text;
  if(!isNotBlank(statusDots)) {
    	statusDots = "";
  }
  frmTMBConfirmLoadingScreen.lblRemainingTime.text = statusDots + "*";
  */
  frmTMBConfirmLoadingScreen.lblRemainingTime.text="";
  frmTMBConfirmLoadingScreen.lblRemainingTime.text = currentStatus;
}

function frmTMBConfirmLoadingScreenInit(){
  //#ifdef android
  frmTMBConfirmLoadingScreen.onDeviceBack = doNothing;
   //#endif
}

function uvactivationFlow(failedStep,activationStatus){
  kony.print("Inside uvactivationFlow for activity log");
  try{
    var inputParam = {};
  	var troubleShoottingId = getVKeyTroubleShootingId();
    if(troubleShoottingId != null && troubleShoottingId != undefined &&  troubleShoottingId != ""){
        kony.print("Inside uvactivationFlow >> troubleShoottingId"+troubleShoottingId);
        inputParam['troubleShootingId'] = troubleShoottingId;
        inputParam['activationStatus'] = activationStatus;
        inputParam['failedStep'] = failedStep;
        invokeServiceSecureAsync("UVActivationFlow", inputParam, uvactivationFlowCallBack);
      }
    }catch(e){
      kony.print("VTAPSDK : Excpetion in vMessageDownload >>"+e.message);
    }
}

function uvactivationFlowCallBack(status, resulttable) {
	if (status == 400) {
         kony.print("Inside uvactivationFlowCallBack...");
      
       kony.print("GLOBAL_UV_STATUS_FLAG >>>"+GLOBAL_UV_STATUS_FLAG);
          if(GLOBAL_UV_STATUS_FLAG == "ON"){
      	 		sendTroubleShootingLogsvKey();
    		}
      	 
    }
}