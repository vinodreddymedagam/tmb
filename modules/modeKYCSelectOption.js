//Type your code here
function initfrmMBeKYCIntroduction(){
  frmMBeKYCIntroduction.preShow=preShowfrmMBeKYCIntroduction;
  frmMBeKYCIntroduction.onDeviceBack= doNothing; 
}
  
 function preShowfrmMBeKYCIntroduction(){
   var locale = kony.i18n.getCurrentLocale();
   var nfcFlag = isNFCEnabled();
   gblIALFlow = "";
   frmMBeKYCIntroduction.lblPersonalIdentificationSub2.setVisibility(false);
   var locale = kony.i18n.getCurrentLocale();
   frmMBeKYCIntroduction.lblexpiryDays.setVisibility(false);
   checkStatuseKYCIntroduction();
   frmMBeKYCIntroduction.lblPersonalIdentificationSub2.text =  kony.i18n.getLocalizedString("eKYC_stepIdentificationCIDPassportSelfie2");
   var frmekycStatusCheckTitle = kony.i18n.getLocalizedString("eKYC_TitleOpenAccount");
   if(locale == "en_US"){
     var frmekycStatusCheckTitle = frmekycStatusCheckTitle +" "+"TMB All Free";
   }
   setHeaderContentToForm(frmekycStatusCheckTitle,btnBackeKYCIntro,true,gbleKYCStep);
   if(gbleKYCStep==="1"){
//      setFooterTwobtnToForm(kony.i18n.getLocalizedString("btnCancel"),kony.i18n.getLocalizedString("btnNext"),btnTnCBackeKYC,navigateToGetYourId);
     setFooterTwobtnToForm(kony.i18n.getLocalizedString("btnCancel"),kony.i18n.getLocalizedString("btnNext"),"",btnTnCBackeKYC,navigateToGetYourId,null,true,false);
     if(nfcFlag !== null){
       setFooterTwobtnToForm(kony.i18n.getLocalizedString("btnCancel"),kony.i18n.getLocalizedString("btnNext"),"",btnTnCBackeKYC,navigateToGetYourId,null,true,false);
     }else{
       setFooterTwobtnToForm(kony.i18n.getLocalizedString("btnCancel"),kony.i18n.getLocalizedString("btnNext"),"",btnTnCBackeKYC,navigateCitizedIDflow,null,true,false);
     }
   }else if(gbleKYCStep==="2"){
//      setFooterTwobtnToForm(kony.i18n.getLocalizedString("btnCancel"),kony.i18n.getLocalizedString("btnNext"),btnTnCBackeKYC,onNextclickfrmMBeKYCFaceRecg);
     setFooterTwobtnToForm("","",kony.i18n.getLocalizedString("btnNext"),null,null,onNextclickfrmMBeKYCFaceRecg,false,true);
     setHeaderContentToForm(frmekycStatusCheckTitle,null,false,gbleKYCStep);
   }else if(gbleKYCStep==="3"){
//      setFooterTwobtnToForm(kony.i18n.getLocalizedString("btnCancel"),kony.i18n.getLocalizedString("btnNext"),btnTnCBackeKYC,btnNexteKYCIntro);
     setHeaderContentToForm(frmekycStatusCheckTitle,null,false,gbleKYCStep);
     frmMBeKYCIntroduction.lblexpiryDays.setVisibility(true);
     var lblexpiredays = kony.i18n.getLocalizedString("eKYC_msgApplicationExpiryDate") + gblEKYCFlowExpireDate; //gblEKYCFlowExpireDays;
     frmMBeKYCIntroduction.lblexpiryDays.text = lblexpiredays;
     setFooterTwobtnToForm(kony.i18n.getLocalizedString("eKYC_btnLater"),kony.i18n.getLocalizedString("btnNext"),"",popupAlertAuthentication,onClickbtnNexteKYCIntro,null,true,false);
   }else{
     setFooterTwobtnToForm(kony.i18n.getLocalizedString("btnCancel"),kony.i18n.getLocalizedString("btnNext"),btnTnCBackeKYC,btnNexteKYCIntro);
   }
   frmMBeKYCIntroduction.lblPersonalIdentification.text = kony.i18n.getLocalizedString("eKYC_stepIdentification");
   var nfcFlag = isNFCEnabled();
   if(null !== nfcFlag){
     if(locale == "th_TH"){
       frmMBeKYCIntroduction.lblPersonalIdentificationSub2.setVisibility(true);    
     }else{
       frmMBeKYCIntroduction.lblPersonalIdentificationSub2.setVisibility(false);
     }
   		frmMBeKYCIntroduction.lblPersonalIdentificationSub.text = kony.i18n.getLocalizedString("eKYC_stepIdentificationCIDPassportSelfie");  
   }else{
     frmMBeKYCIntroduction.lblPersonalIdentificationSub.text = kony.i18n.getLocalizedString("eKYC_stepIdentificationCIDSelfie");
     frmMBeKYCIntroduction.lblPersonalIdentificationSub2.setVisibility(false);
   }
   
   frmMBeKYCIntroduction.lblRegistration.text = kony.i18n.getLocalizedString("eKYC_stepRegistration");
   frmMBeKYCIntroduction.lblRegistrationSub.text = kony.i18n.getLocalizedString("eKYC_stepRegistrationDetails");
   frmMBeKYCIntroduction.lblAuthentication.text = kony.i18n.getLocalizedString("eKYC_stepAuthentication");
   frmMBeKYCIntroduction.lblAuthenticationSub.text = kony.i18n.getLocalizedString("eKYC_stepAuthenticationDetails");
 } 
 
function navigateCitizedIDflow(){
   frmMBeKYCIDInfo.show();
}

function popupAlertAuthentication(){
   var msg = kony.i18n.getLocalizedString("eKYC_msgInformExpiryDate");
   msg = msg+" "+gblEKYCFlowExpireDate;
   var title = kony.i18n.getLocalizedString("info");
   var okk = kony.i18n.getLocalizedString("keyYes");
   var Noo = kony.i18n.getLocalizedString("keyNo");
   showAlertWithYesNoHandler(msg,title,okk,Noo,doNothing,navSelectOption);
}

function navSelectOption(){
  gblIALFlow = "";
  gblUpdateIALFlow = false;
  frmeKYCGetYourIdReady.show();
  btnBackeKYCSelectDoc();
}

function checkStatuseKYCIntroduction(){
  if(gbleKYCStep==="1"){
    frmMBeKYCIntroduction.lblIntroStep1.text="1";
    frmMBeKYCIntroduction.lblIntroStep1.skin="skinlblblueTMBfont";
    frmMBeKYCIntroduction.lblIntroStep2.skin="skinpigreyborderLone";
    frmMBeKYCIntroduction.lblIntroStep3.skin="skinpigreyborderLone";
    frmMBeKYCIntroduction.lblIntroStep2.text="2";
    frmMBeKYCIntroduction.lblIntroStep3.text="3";
    frmMBeKYCIntroduction.lblPersonalIdentification.skin="lblBlue48px";
    frmMBeKYCIntroduction.lblPersonalIdentificationSub.skin="lblBlue40px";
    frmMBeKYCIntroduction.lblPersonalIdentificationSub2.skin="lblBlue40px";
    frmMBeKYCIntroduction.lblRegistration.skin="lblLighGrey200";
    frmMBeKYCIntroduction.lblRegistrationSub.skin="lblLighGreyNew160";
    frmMBeKYCIntroduction.lblAuthentication.skin="lblLighGrey200";
    frmMBeKYCIntroduction.lblAuthenticationSub.skin="lblLighGreyNew160";
  }else if(gbleKYCStep==="2"){
    frmMBeKYCIntroduction.lblIntroStep1.text="A";
    frmMBeKYCIntroduction.lblIntroStep1.skin="skinlblgreenTMBfont";
    frmMBeKYCIntroduction.lblIntroStep2.skin="skinlblblueTMBfont";
    frmMBeKYCIntroduction.lblIntroStep3.skin="skinpigreyborderLone";
    frmMBeKYCIntroduction.lblIntroStep2.text="2";
    frmMBeKYCIntroduction.lblIntroStep3.text="3";
    frmMBeKYCIntroduction.lblPersonalIdentification.skin="lblLighGrey200";
    frmMBeKYCIntroduction.lblPersonalIdentificationSub.skin="lblLighGreyNew160";
    frmMBeKYCIntroduction.lblPersonalIdentificationSub2.skin="lblLighGreyNew160";
    frmMBeKYCIntroduction.lblRegistration.skin="lblBlue48px";
    frmMBeKYCIntroduction.lblRegistrationSub.skin="lblBlue40px";
    frmMBeKYCIntroduction.lblAuthentication.skin="lblLighGrey200";
    frmMBeKYCIntroduction.lblAuthenticationSub.skin="lblLighGreyNew160";
  }else if(gbleKYCStep==="3"){
    frmMBeKYCIntroduction.lblIntroStep1.text="A";
    frmMBeKYCIntroduction.lblIntroStep1.skin="skinlblgreenTMBfont";
    frmMBeKYCIntroduction.lblIntroStep2.skin="skinlblgreenTMBfont";
    frmMBeKYCIntroduction.lblIntroStep3.skin="skinlblblueTMBfont";
    frmMBeKYCIntroduction.lblIntroStep2.text="A";
    frmMBeKYCIntroduction.lblIntroStep3.text="3";
    frmMBeKYCIntroduction.lblPersonalIdentification.skin="lblLighGrey200";
    frmMBeKYCIntroduction.lblPersonalIdentificationSub.skin="lblLighGreyNew160";
    frmMBeKYCIntroduction.lblPersonalIdentificationSub2.skin="lblLighGreyNew160";
    frmMBeKYCIntroduction.lblRegistration.skin="lblLighGrey200";//"lblGrey48px";
    frmMBeKYCIntroduction.lblRegistrationSub.skin="lblLighGreyNew160";
    frmMBeKYCIntroduction.lblAuthentication.skin="lblBlue48px";
    frmMBeKYCIntroduction.lblAuthenticationSub.skin="lblBlue40px";
  }else if(gbleKYCStep==="4"){
    frmMBeKYCIntroduction.lblIntroStep1.text="A";
    frmMBeKYCIntroduction.lblIntroStep1.skin="skinlblgreenTMBfont";
    frmMBeKYCIntroduction.lblIntroStep2.skin="skinlblgreenTMBfont";
    frmMBeKYCIntroduction.lblIntroStep3.skin="skinlblgreenTMBfont";
    frmMBeKYCIntroduction.lblIntroStep2.text="A";
    frmMBeKYCIntroduction.lblIntroStep3.text="A";
    frmMBeKYCIntroduction.lblPersonalIdentification.skin="lblGrey48px";
    frmMBeKYCIntroduction.lblPersonalIdentificationSub.skin="lblLighGreyNew160";
    frmMBeKYCIntroduction.lblPersonalIdentificationSub2.skin="lblLighGreyNew160";
    frmMBeKYCIntroduction.lblRegistration.skin="lblGrey48px";
    frmMBeKYCIntroduction.lblRegistrationSub.skin="lblLighGreyNew160";
    frmMBeKYCIntroduction.lblAuthentication.skin="lblGrey48px";
    frmMBeKYCIntroduction.lblAuthenticationSub.skin="lblLighGreyNew160";
  }
}
function initfrmMBeKYCCitezenIDInfo(){
  frmMBeKYCCitezenIDInfo.preShow=preShowfrmMBeKYCCitezenIDInfo;
  frmMBeKYCCitezenIDInfo.onDeviceBack=doNothing;
}

function EkycCurrentDate() {
	var day = new Date();
	var dd = day.getDate();
	var mm = day.getMonth() + 1;
	var yyyy = day.getFullYear();
	var datearray = [dd, mm, yyyy, "0", "0", "0"];
	return datearray;
}

function preShowfrmMBeKYCCitezenIDInfo(){
  var currentdatearray = EkycCurrentDate();
  var dateformatee = "dd MMM yyyy";
//   frmMBeKYCCitezenIDInfo.calIssueDate.dateComponents = currentdatearray;
//   frmMBeKYCCitezenIDInfo.calExpireDate.dateComponents = currentdatearray;
  frmMBeKYCCitezenIDInfo.calIssueDate.dateFormat = dateformatee;
  frmMBeKYCCitezenIDInfo.imgCheck.onTouchEnd = disableExpireDate;

  if(kony.os.deviceInfo().name == "iphone" || kony.os.deviceInfo().name == "iPhone"){
  frmMBeKYCCitezenIDInfo.switchToggleIOS.onSlide = toggelExpireDateDisable;

  }
  frmMBeKYCCitezenIDInfo.calExpireDate.dateFormat = dateformatee;
  frmMBeKYCCitezenIDInfo.calIssueDate.validEndDate = currentdatearray;
  frmMBeKYCCitezenIDInfo.calExpireDate.validStartDate = currentdatearray;
  frmMBeKYCCitezenIDInfo.txtLaserCode.setVisibility(false);
  frmMBeKYCCitezenIDInfo.lblsubheader.text=getLocalizedString("eKYC_msgEnterIssueExpiryDate");
  frmMBeKYCCitezenIDInfo.lbldateofIssue.text = getLocalizedString("IssueDate");
  frmMBeKYCCitezenIDInfo.lblExpireDate.text = getLocalizedString("ExpiredDate");
  frmMBeKYCCitezenIDInfo.imgIDCard.src="ekyc_id_card.png";
  frmMBeKYCCitezenIDInfo.lblNoExpire.text=getLocalizedString("CID_NeverExpired");
  if(gblIALFlow == "frmEKYCIdpSetting" || gblIALFlow == "frmEKYCIdpDetailsList"){
    LoadLaserCodeFromIALUpdateFlow();
  }else{
    frmMBeKYCCitezenIDInfo.flexLineUp.setVisibility(true);
    frmMBeKYCCitezenIDInfo.FlexIssueDate.setVisibility(true);
    frmMBeKYCCitezenIDInfo.Line1.setVisibility(true);
    frmMBeKYCCitezenIDInfo.FlexExpireDate.setVisibility(true);
    frmMBeKYCCitezenIDInfo.FlexExpireDateToggle.setVisibility(true);
    frmMBeKYCCitezenIDInfo.Line3.setVisibility(true);
    frmMBeKYCCitezenIDInfo.Line3.setVisibility(true);
    frmMBeKYCCitezenIDInfo.Line2.setVisibility(true);
    setHeaderContentToForm(kony.i18n.getLocalizedString("eKYC_TitleEnterInfofromCID"),NavfrmeKYCGetYourIdReady,true,gbleKYCStep);
    setFooterSinglebtnToForm(kony.i18n.getLocalizedString("btnNext"),onClicknextCitizenID);
  } 
}

function disableExpireDate(){
  if(null !== frmMBeKYCCitezenIDInfo.imgCheck.src && frmMBeKYCCitezenIDInfo.imgCheck.src == "chkbox_uncheck.png"){
    frmMBeKYCCitezenIDInfo.FlexExpireDate.setEnabled(false);
    frmMBeKYCCitezenIDInfo.FlexExpireDate.skin = "flexGreyBGLoan";
    frmMBeKYCCitezenIDInfo.imgCheck.src="chkbox_checked.png";
  }else{
    frmMBeKYCCitezenIDInfo.FlexExpireDate.setEnabled(true);
    frmMBeKYCCitezenIDInfo.FlexExpireDate.skin = "flexWhiteBG";
    frmMBeKYCCitezenIDInfo.imgCheck.src="chkbox_uncheck.png";
  }
}

function toggelExpireDateDisable(){
  if(frmMBeKYCCitezenIDInfo.switchToggleIOS.selectedIndex === 1){
    frmMBeKYCCitezenIDInfo.FlexExpireDate.setEnabled(false);
    frmMBeKYCCitezenIDInfo.FlexExpireDate.skin = "flexGreyBGLoan";
  }else{
    frmMBeKYCCitezenIDInfo.FlexExpireDate.setEnabled(true);
    frmMBeKYCCitezenIDInfo.FlexExpireDate.skin = "flexWhiteBG";
  }
}

function NavfrmeKYCGetYourIdReady(){
  frmeKYCGetYourIdReady.flxBody.setVisibility(true);
  frmeKYCGetYourIdReady.flxSelectPrefferedDocument.setVisibility(false);
  frmeKYCGetYourIdReady.flxHoldYourPassport.setVisibility(false);
  frmeKYCGetYourIdReady.flxPassportExpired.setVisibility(false);
  frmeKYCGetYourIdReady.show();
}

function onClicknextCitizenID(){
 if (frmMBeKYCCitezenIDInfo.calIssueDate.date ==="" && frmMBeKYCCitezenIDInfo.calExpireDate.date === "") {
        var dateErrMsg = kony.i18n.getLocalizedString("eKYC_msgEmptyfield") + " " + kony.i18n.getLocalizedString("IssueDate");
        showAlert(dateErrMsg, kony.i18n.getLocalizedString("info"));
    }else if(frmMBeKYCCitezenIDInfo.calIssueDate.date === ""){
         var dateErrMsg = kony.i18n.getLocalizedString("eKYC_msgEmptyfield") + " " + kony.i18n.getLocalizedString("IssueDate");
         showAlert(dateErrMsg, kony.i18n.getLocalizedString("info"));
    }else if (frmMBeKYCCitezenIDInfo.calIssueDate.date === frmMBeKYCCitezenIDInfo.calExpireDate.date) {
		if(null !== frmMBeKYCCitezenIDInfo.imgCheck.src && frmMBeKYCCitezenIDInfo.imgCheck.src !== "chkbox_checked.png"){
			showAlert(kony.i18n.getLocalizedString("eKYC_msgIncorrectIssuedDate"), kony.i18n.getLocalizedString("info"));
		}else{
         navEnterLaserCode();
    }
    }else if(frmMBeKYCCitezenIDInfo.calExpireDate.date === ""){
        if(null !== frmMBeKYCCitezenIDInfo.imgCheck.src && frmMBeKYCCitezenIDInfo.imgCheck.src !== "chkbox_checked.png"){
         var dateErrMsg = kony.i18n.getLocalizedString("eKYC_msgEmptyfield") + " " + kony.i18n.getLocalizedString("ExpiredDate");
         showAlert(dateErrMsg, kony.i18n.getLocalizedString("info"));
        }else {
         navEnterLaserCode();
    }
    } else {
         navEnterLaserCode();
    }
}

function navEnterLaserCode(){
  frmMBeKYCCitezenIDInfo.txtLaserCode.setVisibility(true);
  frmMBeKYCCitezenIDInfo.txtLaserCode.setFocus(true);
//   frmMBeKYCCitezenIDInfo.txtLaserCode.text="";
  var currentdatearray = eKYCCurrentDate();
  //frmMBeKYCCitezenIDInfo.calIssueDate.dateComponents = currentdatearray;
  frmMBeKYCCitezenIDInfo.calExpireDate.validStartDate = currentdatearray;
  frmMBeKYCCitezenIDInfo.calIssueDate.validEndDate = currentdatearray;
  
//    var EndDate = loanAssignedDate(contractEndDate);
//    frmLoanWorkInfo.calEndDate.dateComponents = EndDate;
  //frmMBeKYCCitezenIDInfo.calIssueDate.
  gblPrevLen=0;
  frmMBeKYCCitezenIDInfo.txtLaserCode.maxTextLength = 14;
  frmMBeKYCCitezenIDInfo.txtLaserCode.onTextChange=ontextchangeLaserCode;
  frmMBeKYCCitezenIDInfo.txtLaserCode.onDone=onDonetxtLaserCode;
  frmMBeKYCCitezenIDInfo.lblsubheader.text=getLocalizedString("eKYC_msgPlsEnterSerialNo");
  frmMBeKYCCitezenIDInfo.imgIDCard.src="ekyc_enter_serial.png";
  frmMBeKYCCitezenIDInfo.flexLineUp.setVisibility(false);
  frmMBeKYCCitezenIDInfo.FlexIssueDate.setVisibility(false);
  frmMBeKYCCitezenIDInfo.Line1.setVisibility(false);
  frmMBeKYCCitezenIDInfo.FlexExpireDate.setVisibility(false);
  frmMBeKYCCitezenIDInfo.FlexExpireDateToggle.setVisibility(false);
  frmMBeKYCCitezenIDInfo.Line3.setVisibility(false);
  frmMBeKYCCitezenIDInfo.Line2.setVisibility(false);
  if(!(gblIALFlow == "frmEKYCIdpSetting" || gblIALFlow == "frmEKYCIdpDetailsList")){
    setHeaderContentToForm(kony.i18n.getLocalizedString("eKYC_TitleEnterCIDSerialNumber"),backbtnEnterSerialNo,true,gbleKYCStep);
  }
  setFooterSinglebtnToForm(kony.i18n.getLocalizedString("btnNext"),showfrmMBeKYCFaceRecg);
  frmMBeKYCFaceRecg.flexBodyScroll.setVisibility(true);
  frmMBeKYCFaceRecg.flxFaceRecognitionCheck.setVisibility(false);
}

function backbtnEnterSerialNo(){
  if(eKYCData.scanType === "CI"){
//     resetGetYourIdReadyDetails();
//     frmeKYCGetYourIdReady.show();
    frmMBeKYCIDInfo.show();
  }else{
    frmMBeKYCCitezenIDInfo.txtLaserCode.setVisibility(false);
    frmMBeKYCCitezenIDInfo.txtLaserCode.text = ""; 
    frmMBeKYCCitezenIDInfo.lblsubheader.text = getLocalizedString("eKYC_msgEnterIssueExpiryDate");
    frmMBeKYCCitezenIDInfo.imgIDCard.src = "ekyc_id_card.png";
    frmMBeKYCCitezenIDInfo.FlexIssueDate.setVisibility(true);
    frmMBeKYCCitezenIDInfo.Line1.setVisibility(true);
    frmMBeKYCCitezenIDInfo.FlexExpireDate.setVisibility(true);
    frmMBeKYCCitezenIDInfo.FlexExpireDateToggle.setVisibility(true);
    frmMBeKYCCitezenIDInfo.Line2.setVisibility(true);
    setHeaderContentToForm(kony.i18n.getLocalizedString("eKYC_TitleEnterInfofromCID"), NavfrmeKYCGetYourIdReady, true, gbleKYCStep);
    setFooterSinglebtnToForm(kony.i18n.getLocalizedString("btnNext"), onClicknextCitizenID);
    }
}

function eKYCCurrentDate() {
	var day = new Date();
	var dd = day.getDate();
	var mm = day.getMonth() + 1;
	var yyyy = day.getFullYear();
	var datearray = [dd, mm, yyyy];
	return datearray;
}

function ontextchangeLaserCode() {
    var txt = frmMBeKYCCitezenIDInfo.txtLaserCode.text;
    if (txt == null) return false;
    var numChars = txt.length;
    var temp = "";
    var i, txtLen = numChars;
    var currLen = numChars;
    if (gblPrevLen < currLen) {
        for (i = 0; i < numChars; ++i) {
            if (txt[i] != '-') {
                temp = temp + txt[i];
            } else {
                txtLen--;
            }
        }
        var iphenText = "";
        for (i = 0; i < txtLen; i++) {
            iphenText += temp[i];
            if (i == 2 || i == 9) {
                iphenText += '-';
            }
        }
        frmMBeKYCCitezenIDInfo.txtLaserCode.text = iphenText;
    }
    gblPrevLen = currLen;
}

function onDonetxtLaserCode(){
  var txtVal=frmMBeKYCCitezenIDInfo.txtLaserCode.text.trim();
  if(txtVal ===null ||txtVal=== ""){
    showAlertWithCallBack(kony.i18n.getLocalizedString("Msg_eKycPlsEnterCIDSerialNo"), kony.i18n.getLocalizedString("info"),lasercodeFocus);
    return false;
  }else if(txtVal.length<14){
    showAlertWithCallBack(kony.i18n.getLocalizedString("Msg_eKycIncorrectCIDSerialNo"), kony.i18n.getLocalizedString("info"),lasercodeFocus);
    return false;
  }
  return true;
}

function lasercodeFocus(){
  frmMBeKYCCitezenIDInfo.txtLaserCode.setEnabled(true);
  frmMBeKYCCitezenIDInfo.txtLaserCode.setFocus(true);
}

function onEditCitiZenIDeKYC(txt) {
	if (txt == null) return false;
	var noChars = txt.length;
	var temp = "";
	var i, txtLen = noChars;
	var currLen = noChars;
	
	if (gblPrevLen < currLen) {
		for (i = 0; i < noChars; ++i) {
			if (txt[i] != '-') {
				temp = temp + txt[i];
			} else {
				txtLen--;
			}
			
		}
		var iphenText = "";
		for (i = 0; i < txtLen; i++) {
			iphenText += temp[i];
			if (i == 0 || i == 4 || i == 9 || i == 11) {
				iphenText += '-';
			}
		}
      frmMBeKYCCitezenIDInfo.txtLaserCode.text=iphenText;
		//return ;
	}
	gblPrevLen = currLen;
}
