/*
*************************************************************************************
		Module	: ibActivationConfirm
		Author  : Kony
		Purpose : Invoke KonyCRMProfileInquiry service
****************************************************************************************
*/

function ibActivationConfirm(actionCodeFlow, nationID, isCitizen, isPassport) {
	showLoadingScreenPopup();
	inputParam = {};
	gActivationCode = frmIBActivateIBankingStep1.txtActivationCode.text;
	var acctNum = frmIBActivateIBankingStep1.txtAccountNumber.text.replace(/\-/g, "");
	inputParam["actionCode"] = actionCodeFlow;
	inputParam["activationCode"] = gActivationCode;
	inputParam["accountNumber"] = acctNum;
	inputParam["channelId"] = "01";
	inputParam["activationCounter"] = gblActivationCounter;
	inputParam["idCounter"] = gblIdCounter;
	if (isCitizen) {
		inputParam["idType"] = "CI";
	} else if (isPassport) {
		inputParam["idType"] = "PP";
	}
	inputParam['accountStatusCode'] = '';
	inputParam['acctId'] = '';
	inputParam['acctType'] = '';
	inputParam['currencyCode'] = '';
	inputParam['fiIdent'] = "";
	inputParam['rmId'] = "";
	inputParam["idNumber"] = nationID;
	invokeServiceSecureAsync("KonyCRMProfileInquiry", inputParam, callBackOnClickActivationIB)
}
/*
*************************************************************************************
		Module	: callBackOnClickActivationIB
		Author  : Kony
		Purpose : Capturing the result and showing Confirmation screen
****************************************************************************************
*/

function callBackOnClickActivationIB(status, resulttable) {
	if (status == 400) {
	var successFlag = false;
		if (resulttable["opstatus"] == 0) {
			gblPHONENUMBER = resulttable["mobileNo"];
			//gblRefNum=resulttable["BankRefNO"]
			//gblTokenNum = resulttable["tokenUUID"];
			gblEmailId = resulttable["emailId"];
			//gblcrmId = resulttable["crmId"];
			gblIBFlowStatus = resulttable["ibFlowStatusIdRs"];
			gblMBFlowStatus = resulttable["mbFlowStatusIdRs"];
			//gblCustomerStatus = resulttable["customerStatusId"];
			gblActionCode = resulttable["actnCode"];
			//frmMBActiConfirm.show();
			frmIBActivateIBankingStep1Confirm.lblPhNoVal.text = maskingIB(gblPHONENUMBER);
			successFlag = true;
			if(gblIBFlowStatus=="09")
			{
				dismissLoadingScreenPopup();
				showAlertIB(kony.i18n.getLocalizedString("ECCRMPrfInqErr00002"), kony.i18n.getLocalizedString("info"));
				return false;
			}
			frmIBActivateIBankingStep1Confirm.show();
			dismissLoadingScreenPopup();
		} else {
			if (resulttable["errCode"] == "KonyDvMgmtErr00001") {
				dismissLoadingScreenPopup();
				showAlertIB(kony.i18n.getLocalizedString("ECKonyDvMgmtErr00001"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (resulttable["errCode"] == "AppSrvErr00001") {
				dismissLoadingScreenPopup();
				showAlertIB(kony.i18n.getLocalizedString("ECAppSrvErr00001"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (resulttable["errCode"] == "AcctValErr00001") {
				dismissLoadingScreenPopup();
				showAlertIB(kony.i18n.getLocalizedString("keyECAcctValErr00001"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (resulttable["errCode"] == "AcctValErr00002") {
				dismissLoadingScreenPopup();
				showAlertIB(kony.i18n.getLocalizedString("keyECAcctValErr00001"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (resulttable["errCode"] == "MIBInqErr00001") {
				dismissLoadingScreenPopup();
				showAlertIB(kony.i18n.getLocalizedString("ECMIBInqErr00001"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (resulttable["errCode"] == "MIBInqErr00002") {
				dismissLoadingScreenPopup();
				//activityLogServiceCall("005","MIBInqErr00002","02");
				showAlertIB(kony.i18n.getLocalizedString("ECMIBInqErr00003"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (resulttable["errCode"] == "MIBInqIDErr00001" || resulttable["errCode"] == "MIBInqIDErr00002" ||
				resulttable["errCode"] == "MIBInqIDErr00003") {
				if (resulttable["errCode"] == "MIBInqIDErr00001") {
					gblIdCounter = "1";
				} else if (resulttable["errCode"] == "MIBInqIDErr00002") {
					gblIdCounter = "2";
				} else if (resulttable["errCode"] == "MIBInqIDErr00003") {
					gblIdCounter = "3";
				}
				dismissLoadingScreenPopup();
				showAlertIB(kony.i18n.getLocalizedString("ECMIBInqErr00003"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (resulttable["errCode"] == "MIBInqErr00004") {
				gblIdCounter = "0";
				dismissLoadingScreenPopup();
				if (gblSetPwd == true) {
					popupIBTempPwd.lblText.text = kony.i18n.getLocalizedString("keyIncorectReActInfo");
					popupIBTempPwd.show();
				} else {
					showAlertIB(kony.i18n.getLocalizedString("ECMIBInqErr00004"), kony.i18n.getLocalizedString("info"));
				}
				return false;
			} else if (resulttable["errCode"] == "MIBInqErr00005") {
				dismissLoadingScreenPopup();
				showAlertIB(kony.i18n.getLocalizedString("ECMIBInqErr00005"), kony.i18n.getLocalizedString("info"));
				return false;
			}else if (resulttable["errCode"] == "MIBInqErr00007") {
				dismissLoadingScreenPopup();
				showAlertIB(kony.i18n.getLocalizedString("ECMIBInqErr00007"), kony.i18n.getLocalizedString("info"));
				return false;
			}else if (resulttable["errCode"] == "CRMPrfInqErr00001") {
				dismissLoadingScreenPopup();
				showAlertIB(kony.i18n.getLocalizedString("ECCRMPrfInqErr00001"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (resulttable["errCode"] == "CRMPrfInqErr00002") {
				dismissLoadingScreenPopup();
				showAlertIB(kony.i18n.getLocalizedString("ECCRMPrfInqErr00002"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (resulttable["errCode"] == "VrfyOTPCtrErr00001" || resulttable["errCode"] == "VrfyOTPCtrErr00002" ||
				resulttable["errCode"] == "VrfyOTPCtrErr00003") {
				if (resulttable["errCode"] == "VrfyOTPCtrErr00001") {
					gblActivationCounter = "1";
				} else if (resulttable["errCode"] == "VrfyOTPCtrErr00002") {
					gblActivationCounter = "2";
				} else if (resulttable["errCode"] == "VrfyOTPCtrErr00003") {
					gblActivationCounter = "3";
				}
				dismissLoadingScreenPopup();
				showAlertIB(kony.i18n.getLocalizedString("ECVrfyOTPErr00001"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (resulttable["errCode"] == "VrfyOTPErr00002") {
				gblActivationCounter = "0";
				dismissLoadingScreenPopup();
				//activityLogServiceCall("005","VrfyOTPErr00002","02");
				if (gblSetPwd == true) {
					popupIBTempPwd.lblText.text = kony.i18n.getLocalizedString("keyIncorectReActInfo");
					popupIBTempPwd.show();
				} else {
					showAlertIB(kony.i18n.getLocalizedString("ECVrfyOTPErr00002"), kony.i18n.getLocalizedString("info"));
				}
				
				return false;
			} else if (resulttable["errCode"] == "VrfyOTPErr00003") {
				dismissLoadingScreenPopup();
				//activityLogServiceCall("005","VrfyOTPErr00002","02");
				showAlertIB(kony.i18n.getLocalizedString("ECVrfyOTPErr00003"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (resulttable["errCode"] == "VrfyOTPErr00004") {
				dismissLoadingScreenPopup();
				//activityLogServiceCall("005","VrfyOTPErr00002","02");
				showAlertIB(kony.i18n.getLocalizedString("ECVrfyOTPErr00004"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (resulttable["errCode"] == "VrfyOTPErr00005") {
				dismissLoadingScreenPopup();
				//activityLogServiceCall("005","VrfyOTPErr00002","02");
				showAlertIB(kony.i18n.getLocalizedString("ECVrfyOTPErr00005"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (resulttable["errCode"] == "KonyDvMgmtErr00002") {
				dismissLoadingScreenPopup();
				showAlertIB(kony.i18n.getLocalizedString("ECKonyDvMgmtErr00002"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (resulttable["errCode"] == "JavaErr00001") {
				dismissLoadingScreenPopup();
				showAlertIB(kony.i18n.getLocalizedString("ECJavaErr00001"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (resulttable["errCode"] == "VrfyTknErr00001") {
				dismissLoadingScreenPopup();
				showAlert(kony.i18n.getLocalizedString("ECVrfyTknErr00001"), kony.i18n.getLocalizedString("info"));
				return false;
			}else if (resulttable["errCode"] == "VrfyTknErr00002") {
				dismissLoadingScreenPopup();
				showAlert(kony.i18n.getLocalizedString("ECVrfyTknErr00002"), kony.i18n.getLocalizedString("info"));
				return false;
			}else if (resulttable["errCode"] == "VrfyTknErr00003") {
				dismissLoadingScreenPopup();
				showAlert(kony.i18n.getLocalizedString("ECVrfyTknErr00003"), kony.i18n.getLocalizedString("info"));
				return false;
			}else {
				dismissLoadingScreenPopup();
				showAlertIB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
				//activityLogServiceCall("005","VrfyOTPErr00002","02");
				return false;
			}
		}
	/*	if (successFlag) {
			if (gblActionCode == "11") {
				activityLogServiceCall("004", "", "00", "", "Success", "Success", "Success", "", "", "");
			} else if (gblActionCode == "12") {
				activityLogServiceCall("015", "", "00", "", "Success", "Success", "Success", "", "", "");
			}	
		} else {
			if (gblActionCode == "11") {
				activityLogServiceCall("004", "", "02", "", "Fail", "Fail", "Fail", "", "", "");
			} else if (gblActionCode == "12") {
				activityLogServiceCall("015", "", "02", "", "Fail", "Fail", "Fail", "", "", "");
			}
		}
		*/
		dismissLoadingScreenPopup();
	}
}
/*
*************************************************************************************
		Module	: IBreqOTP
		Author  : Kony
		Purpose : Invoke RequestOTPKony service
****************************************************************************************
*/

function IBreqOTP() {
	showLoadingScreenPopup();
    frmIBActivateIBankingStep2Confirm.lblOTPinCurr.text = " ";//kony.i18n.getLocalizedString("invalidOTP"); //
    frmIBActivateIBankingStep2Confirm.lblPlsReEnter.text = " "; 
    frmIBActivateIBankingStep2Confirm.hbxOTPincurrect.isVisible = false;
    frmIBActivateIBankingStep2Confirm.hbox507353026213437.isVisible = true;
    frmIBActivateIBankingStep2Confirm.hbox507353026213385.isVisible = true;
    frmIBActivateIBankingStep2Confirm.txtOTP.setFocus(true);
	var locale = kony.i18n.getCurrentLocale();
	var eventNotificationPolicy;
	var SMSSubject;
	if (gblActionCode == "11") {
		if (locale == "en_US") {
			eventNotificationPolicy = "MIB_OTPActivate_EN";
			SMSSubject = "MIB_OTPActivateIB_EN";
		} else {
			eventNotificationPolicy = "MIB_OTPActivate_TH";
			SMSSubject = "MIB_OTPActivateIB_TH";
		}
	} else if (gblActionCode == "12") {
		if (locale == "en_US") {
			eventNotificationPolicy = "MIB_OTPResetPWD_EN";
			SMSSubject = "MIB_OTPResetPWDIB_EN";
		} else {
			eventNotificationPolicy = "MIB_OTPResetPWD_TH";
			SMSSubject = "MIB_OTPResetPWDIB_TH";
		}
	}else if (gblActionCode == "13") {
		if (locale == "en_US") {
			eventNotificationPolicy = "MIB_OTPUnlockIB_EN";
			SMSSubject = "MIB_OTPUnlockIB_EN";
		} else {
			eventNotificationPolicy = "MIB_OTPUnlockIB_TH";
			SMSSubject = "MIB_OTPUnlockIB_TH";
		}
	}
	var inputParam = {};
	inputParam["bankRef"] = "";
	inputParam["batchNo"] = "";
	//inputParam["channel"] = "";
	inputParam["eventNotificationId"] = eventNotificationPolicy;
	inputParam["inputSeedString"] = "";
	//inputParam["mobileNo"] = gblPHONENUMBER; //"0865832721" Mobile num VIT offshore;//Will be taken directly from MIB XPRESS 
	inputParam["param"] = "";
	inputParam["entries"] = "";
	inputParam["key"] = "";
	inputParam["value"] = "";
	inputParam["policyId"] = "";
	inputParam["productCode"] = "";
	inputParam["receipientName"] = "";
	inputParam["session"] = "";
	inputParam["smsSubject"] = SMSSubject;
	inputParam["storeId"] = "";
	inputParam["tokenUUID"] = "3443";
	inputParam["retryCounterRequestOTP"] = gblRetryCountRequestOTP;
	inputParam["Channel"] = "IB-ACTIVATION";
	
	invokeServiceSecureAsync("RequestOTPKony", inputParam, callBackIBreqOTP);
}
/*
*************************************************************************************
		Module	: callBackIBreqOTP
		Author  : Kony
		Purpose : Capturing the result and showing Confirmation screen
****************************************************************************************
*/

function callBackIBreqOTP(status, resulttable) {
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			gblOTPdisabletime = kony.os.toNumber(resulttable["requestOTPEnableTime"]) * 1000; // setTimeout function has input in milliseconds
			gblRetryCountRequestOTP = kony.os.toNumber(resulttable["retryCounterRequestOTP"]);
			gblOTPLENGTH = kony.os.toNumber(resulttable["otpLength"]);
			
			//alert(gblOTPdisabletime);
			var timout_id = setTimeout('OTPReqButton()', gblOTPdisabletime);
			//frmIBActivateIBankingStep2Confirm.lblOTPsent.setVisibility(false);
			frmIBActivateIBankingStep2Confirm.lblOTPsent.text= kony.i18n.getLocalizedString("keyotpmsg");
			frmIBActivateIBankingStep2Confirm.lblRefNo.text = resulttable["pac"];
			frmIBActivateIBankingStep2Confirm.lblOTPNumber.text = maskingIB(gblPHONENUMBER);
			frmIBActivateIBankingStep2Confirm.btnRequest.setEnabled(false);
			
			// frmIBActivateIBankingStep2Confirm.lblOTPNumber.text = maskingIB(gblPHONENUMBER);
			frmIBActivateIBankingStep2Confirm.lblOTPNumber.setVisibility(true);
           // frmIBActivateIBankingStep2Confirm.btnRequest.setEnabled(false);
			//frmIBActivateIBankingStep2Confirm.txtOTP.setFocus(true);
			
			frmIBActivateIBankingStep2Confirm.show();
			frmIBActivateIBankingStep2Confirm.txtOTP.maxTextLength = gblOTPLENGTH;
			dismissLoadingScreenPopup();
			frmIBActivateIBankingStep2Confirm.txtOTP.setFocus(true);
		} else {
			if (resulttable["errCode"] == "GenOTPRtyErr00002") {
				dismissLoadingScreenPopup();
				showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00002"), kony.i18n.getLocalizedString("info"));
				return false;
			}
			//frmIBActivateIBankingStep1Confirm.lblRefNo.text = resulttable["pac"];
			gblRetryCountRequestOTP = resulttable["retryCounterRequestOTP"];
			gblOTPdisabletime = kony.os.toNumber(resulttable["requestOTPEnableTime"]);
			gblOTPLENGTH = kony.os.toNumber(resulttable["otpLength"]);
			if (resulttable["errCode"] == "GenOTPRtyErr00001") {
				dismissLoadingScreenPopup();
				showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
				//gblOTPFlag = false;
				return false;
			} else {
				dismissLoadingScreenPopup();
				showAlertIB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
				//gblOTPFlag = false;
				return false;
			}
		}
		dismissLoadingScreenPopup();
	}
}
/*
*************************************************************************************
		Module	: IBverifyOTP
		Author  : Kony
		Purpose : Invoke verifyOTP service
****************************************************************************************
*/

function IBverifyOTP() {
	showLoadingScreenPopup();
	var inputParam = {};
	//inputParam["serviceID"] = "verifyOTP";
	inputParam["retryCounterVerifyOTP"] = gblVerifyOTPCounter;
	inputParam["otp"] = frmIBActivateIBankingStep2Confirm.txtOTP.text
	invokeServiceSecureAsync("verifyOTP", inputParam, callBackIBverifyOTP)
}
/*
*************************************************************************************
		Module	: callBackIBverifyOTP
		Author  : Kony
		Purpose : Capturing the result and showing Confirmation screen
****************************************************************************************
*/

function callBackIBverifyOTP(status, resulttable) {
	if (status == 400) {
	var successFlag = "Fail";
	var flag ="02";
		
		if (resulttable["opstatus"] == 0) {
			gblVerifyOTPCounter = "0";
			//gblShowPinPwdSecs = kony.os.toNumber(resulttable["showPinPwdSecs"]);
			//gblShowPwdNo = kony.os.toNumber(resulttable["showPinPwdCount"]);
			gblRetryCountRequestOTP = "0";
			dismissLoadingScreenPopup();
			successFlag = "Success";
			flag ="01";
			
			frmIBCreateUserID.show();
		} else if (resulttable["opstatus"] == 8005) {
			if (resulttable["errCode"] == "VrfyOTPErr00001") {
				gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
				//frmIBActivateIBankingStep2Confirm.lblOTPsent.text = kony.i18n.getLocalizedString("wrongOTP");//commented by swapna
                frmIBActivateIBankingStep2Confirm.lblOTPinCurr.text = "OTP is not correct.";//kony.i18n.getLocalizedString("invalidOTP"); //
                frmIBActivateIBankingStep2Confirm.lblPlsReEnter.text = "Please try again."; 
                frmIBActivateIBankingStep2Confirm.hbxOTPincurrect.isVisible = true;
                frmIBActivateIBankingStep2Confirm.hbox507353026213437.isVisible = false;
                frmIBActivateIBankingStep2Confirm.hbox507353026213385.isVisible = false;               
				frmIBActivateIBankingStep2Confirm.lblOTPNumber.setVisibility(false);
				frmIBActivateIBankingStep2Confirm.txtOTP.text = "";
     			dismissLoadingScreenPopup();
     			frmIBActivateIBankingStep2Confirm.txtOTP.setFocus(true);
			} else if (resulttable["errCode"] == "VrfyOTPErr00002") {
				gblVerifyOTPCounter = "0";
				dismissLoadingScreenPopup();
				if (gblSetPwd == true) {
					popIBBPOTPLockedNew.rchTxtOtpLock.text = kony.i18n.getLocalizedString("keyOtpLocked");
					popIBBPOTPLockedNew.show();
				} else {
					showAlertIB(kony.i18n.getLocalizedString("ECVrfyOTPErr"), kony.i18n.getLocalizedString("info"));
					frmIBActivateIBankingStep2Confirm.btnRequest.setEnabled(false);
					clearTimeout(timout_id);
					frmIBActivateIBankingStep2Confirm.btnConfirm.onClick = OTPLOCKERR;
					frmIBActivateIBankingStep2Confirm.btnRequest.skin = "btnIB158disabled";
					frmIBActivateIBankingStep2Confirm.btnRequest.setEnabled(false);
				}
				return false;
			} else if (resulttable["errCode"] == "VrfyOTPErr00003") {
				dismissLoadingScreenPopup();
				showAlertIB(kony.i18n.getLocalizedString("ECVrfyOTPErr00003"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (resulttable["errCode"] == "VrfyOTPErr00004") {
				dismissLoadingScreenPopup();
				showAlertIB(kony.i18n.getLocalizedString("ECVrfyOTPErr00004"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (resulttable["errCode"] == "VrfyOTPErr00005") {
				dismissLoadingScreenPopup();
				showAlertIB(kony.i18n.getLocalizedString("ECVrfyOTPErr00005"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (resulttable["errCode"] == "VrfyOTPErr00006") {
				dismissLoadingScreenPopup();
				showAlertIB(kony.i18n.getLocalizedString("ECVrfyOTPErr00006"), kony.i18n.getLocalizedString("info"));
				return false;
			} else {
				dismissLoadingScreenPopup();
				showAlertIB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
				return false;
			}
		} else {
		        frmIBActivateIBankingStep2Confirm.lblOTPinCurr.text = "";//kony.i18n.getLocalizedString("invalidOTP"); //
                frmIBActivateIBankingStep2Confirm.lblPlsReEnter.text = ""; 
                frmIBActivateIBankingStep2Confirm.hbxOTPincurrect.isVisible = false;
                frmIBActivateIBankingStep2Confirm.hbox507353026213437.isVisible = true;
                frmIBActivateIBankingStep2Confirm.hbox507353026213385.isVisible = true;
			dismissLoadingScreenPopup();
			showAlertIB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
			return false;
		}
	/*	if (gblActionCode == "11") {
			activityLogServiceCall("004", "", flag, "",  successFlag, successFlag, successFlag, "", "","");
		} else  if(gblActionCode == "12") {
			activityLogServiceCall("015", "", flag, "", successFlag, successFlag, successFlag, "", "","");
		} 
	*/
		
		dismissLoadingScreenPopup();
	}
}
function OTPLOCKERR(){
	showAlertIB(kony.i18n.getLocalizedString("ECVrfyOTPErr00003"),kony.i18n.getLocalizedString("info"));
}
function IBcreateUser() {
	//kony.application.showLoadingScreen("frmLoading", "",constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
	showLoadingScreenPopup();
	inputParam = {};
	inputParam["userStoreId"] = "";
	inputParam["loginId"] = frmIBCreateUserID.txtUserID.text;
	gblUserName = frmIBCreateUserID.txtUserID.text;
	inputParam["segmentId"] = "MIB";
	inputParam["ibPwd"] = frmIBCreateUserID.txtPassword.text;
	gblPassword = frmIBCreateUserID.txtPassword.text;
	inputParam["session"] = "";
	inputParam["username"] = "";
	invokeServiceSecureAsync("createUserIB", inputParam, callBackIBcreateUser)
	//}else{
	//frmConnectAccMB.show();
	//}
}

function callBackIBcreateUser(status, resulttable) {


    if (status == 400) {
        if (resulttable["errCode"] == "UseridErr") {
            dismissLoadingScreenPopup();
            showAlert(kony.i18n.getLocalizedString("keyUserIdMinRequirement"), kony.i18n.getLocalizedString("info"));
            return false;
        } else if (resulttable["errCode"] == "IBPwdError") {
            dismissLoadingScreenPopup();
            showAlert(kony.i18n.getLocalizedString("invalidPasswordIB"), kony.i18n.getLocalizedString("info"));
            return false;
        }
        if (resulttable["opstatus"] == 0) {
            //alert(resulttable["message"]);
            //if(resulttable["message"] != null)
            {
                if(gblEmailId != null && gblEmailId != undefined)				// prevents showing null in email textbox 
                	frmIBCustomerBasicInfo.txtEmail.text = gblEmailId;
                else
                	frmIBCustomerBasicInfo.txtEmail.text="";
                frmIBCustomerBasicInfo.show();
                dismissLoadingScreenPopup();
            }
            
        } else if(resulttable["opstatus"] == 1)
		{
		
		
		
		if (resulttable["errorKey"]!= null) {
			dismissLoadingScreenPopup();
			
            
            alert(kony.i18n.getLocalizedString("keyRepeatCharErr"));
            return false;
			}
		}else if(resulttable["opstatus"] == 8009 && resulttable["errCode"] == "validationErr"){
             if(resulttable["errorKey"] == "keyPassSettingGuidelinesIB"){
             	dismissLoadingScreenPopup();
             	showAlertIB(kony.i18n.getLocalizedString("invalidPasswordIB"), kony.i18n.getLocalizedString("info"));
             	frmIBCreateUserID.txtPassword.text = "";
             	frmIBCreateUserID.txtConfirmPassword.text = "";
             	frmIBCreateUserID.txtPassword.setFocus(true);
             	return false;
             }
             if(resulttable["errorKey"] == "keyWrongPwd"){
             	dismissLoadingScreenPopup();
             	showAlertIB(kony.i18n.getLocalizedString("invalidPasswordIB"), kony.i18n.getLocalizedString("info"));
             	frmIBCreateUserID.txtPassword.text = "";
             	frmIBCreateUserID.txtConfirmPassword.text = "";
             	frmIBCreateUserID.txtPassword.setFocus(true);
             	return false;
             }
		}
        else {
			
            if (resulttable["errCode"] == "1000") {
                dismissLoadingScreenPopup();
                showAlert(kony.i18n.getLocalizedString("ECCreateUserExits"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errCode"] == "40001") {
                dismissLoadingScreenPopup();
                showAlert(kony.i18n.getLocalizedString("ECCreateUserError"), kony.i18n.getLocalizedString("info"));
                return false;
            } else {
			
                dismissLoadingScreenPopup();
                if (resulttable["errMsg"] != null || resulttable["errMsg"] != "") {
                    showAlert(resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
                } else {
                    showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
                }
                return false;
            }
        }
    } else {
        
        if (resulttable["errorKey"] != undefined) {
		dismissLoadingScreenPopup();
            
            alert(kony.i18n.getLocalizedString("keyRepeatCharErr"));
        }
    }
}


function IBcrmProfileUpdate() {
	showLoadingScreenPopup();
	gblEmailId = kony.string.trim(frmIBCustomerBasicInfo.txtEmail.text);
	inputParam = {};
	//inputParam["rqUUId"] = "";
	//inputParam["channelName"] = "IB-INQ";
	//inputParam["ibUserId"] = gblUserName;
	var locale = kony.i18n.getCurrentLocale();
	if (locale == "en_US") {
		inputParam["languageCd"] = "EN";
	} else {
		inputParam["languageCd"] = "TH";
	}
	//inputParam["ebCustomerStatusID"] = "02";
	//inputParam["ibUserStatusId"] = "02";
	if (gblActionCode == "11") {
		inputParam["actionType"] = "24";
	}else if (gblActionCode == "12" || gblActionCode == "13"){
		inputParam["actionType"] = "29";
	}	
	if(frmIBCustomerBasicInfo.txtEmail.text != "")	
		inputParam["emailAddr"] = kony.string.trim(frmIBCustomerBasicInfo.txtEmail.text);
	invokeServiceSecureAsync("crmProfileModActivation", inputParam, callBackIBcrmProfileUpdateStep2)
}



function callBackIBcrmProfileUpdateStep2(status, resulttable) {
	if (status == 400) {
		if (resulttable["opstatus"] == 0  && resulttable["StatusCode"] == "0") {
				frmIBCustomerBasicInfo.lblAccntname.text = "";
				frmIBCustomerBasicInfo.lblAccntNo.text = "";
				frmIBCustomerBasicInfo.imgAccntIcon.setVisibility(false);
				gblUserLockStatusIB = resulttable["IBUserStatusID"];
				gblUserName = resulttable["IBUserID"];
				IBsendTnCMailPartyInq();
				
					
				dismissLoadingScreenPopup();
				frmIBFirstTimeActivationComplete.image2507382817236580.src = "iconcomplete.png";
				frmIBFirstTimeActivationComplete.label507382817236581.text = kony.i18n.getLocalizedString("keyIBFirstActiveSucces");
				frmIBFirstTimeActivationComplete.button507382817236587.setVisibility(true);
				frmIBFirstTimeActivationComplete.show();
		}else {
				dismissLoadingScreenPopup();
				frmIBFirstTimeActivationComplete.label507382817236579.text = kony.i18n.getLocalizedString("keyIBActivationFailureHeader");
				frmIBFirstTimeActivationComplete.image2507382817236580.src = "iconnotcomplete.png";
				frmIBFirstTimeActivationComplete.label507382817236581.text = kony.i18n.getLocalizedString("keyIBActivationFailureMessage");
				frmIBFirstTimeActivationComplete.button507382817236587.setVisibility(false);
				frmIBFirstTimeActivationComplete.show();
			//showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
		}
			/*if (resulttable["errCode"] == "JavaErr00001") {
				dismissLoadingScreenPopup();
				showAlertIB(kony.i18n.getLocalizedString("ECJavaErr00001"), kony.i18n.getLocalizedString("info"));
				return false;
			} else {
				
			}
		} else {
			showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
			dismissLoadingScreenPopup();
			return false;
		}*/
		dismissLoadingScreenPopup();
	}
}

function IBLoginViaActivation() {
	showLoadingScreenPopup();
	//IBuserID = frmIBPreLogin.txtUserId.text
	//if (IBuserID.length > 7 && IBuserID.length < 21 && IBuserID.match(/[a-z]/ig)){
	var inputParam = {};
	inputParam["loginId"] = gblUserName;
	inputParam["password"] = gblPassword;
	inputParam["password"] = encryptIBPwd(kony.string.trim(gblPassword));
	LoginFormName = frmIBPreLogin;
	gblLoginType = "Activation";
	invokeServiceSecureAsync("IBVerifyLoginEligibility", inputParam, callBackIBLoginService)
}

function onclickActivationCompleteStart() {
	initFATCAGlobalVars();
 	inputparam = {};
 	inputparam["activationCompleteFlag"] = "true";
 	inputparam["upgradeSkip"] = ""; 	//FATCA CR code
 	showLoadingScreenPopup();
	invokeServiceSecureAsync("customerAccountInquiry", inputparam, callBackActivationCompleteStart);
}

function callBackActivationCompleteStart(status, resulttable) {
	if (status == 400) {
 		if (resulttable["opstatus"] == 0) {
	 		gblMigration="true";
 		   var locale = kony.i18n.getCurrentLocale();
 		   frmIBPostLoginDashboard.lblAllHidden.setVisibility(false);
 		   
 		   //adding code here to fix the header issue on IB
            // margings code is from the i18nchnage.js
            if (kony.i18n.getCurrentLocale() == "th_TH")
            {
                hbxIBPostLogin.lnkLogOut.containerWeight = 9;
                hbxIBPostLogin.lnkLogOut.margin=[0,0,0,6];
                hbxIBPostLogin.label502795003206049.containerWeight = 10;
                hbxIBPostLogin.lnkLogOut.text = kony.i18n.getLocalizedString("keyIBLogout");
                hbxIBPostLogin.lnkHome.text = kony.i18n.getLocalizedString('keyIBHome');
                hbxIBPostLogin.lblLastLoginTime.text = kony.i18n.getLocalizedString("keyIBHeaderlblLastLogin");
                hbxIBPostLogin.lblName.text = gblCustomerNameTh;
                hbxIBPostLogin.btnEng.skin = "btnEngLangOff";
                hbxIBPostLogin.btnThai.skin = "btnThaiLangOn";
                //footer change code
                hbxFooterPrelogin.linkhotpromo.containerWeight = 11;
                hbxFooterPrelogin.linkFindTmb.containerWeight = 12;
                hbxFooterPrelogin.linkExchangeRate.containerWeight = 16;
                hbxFooterPrelogin.linkSiteTour.containerWeight = 10;
                hbxFooterPrelogin.link506459299592952.containerWeight = 11;
                hbxFooterPrelogin.linkTnC.containerWeight = 38;
                //language locale flip
                hbxFooterPrelogin.linkhotpromo.text = kony.i18n.getLocalizedString("keyIBFooterHotPromotions");
                hbxFooterPrelogin.linkFindTmb.text = kony.i18n.getLocalizedString("keyIBFooterFindTMB");
                hbxFooterPrelogin.linkExchangeRate.text = kony.i18n.getLocalizedString("keyIBFooterExchange Rates");
                hbxFooterPrelogin.linkSiteTour.text = kony.i18n.getLocalizedString("keyIBFooterSiteTour");
                hbxFooterPrelogin.linkTnC.text = kony.i18n.getLocalizedString("keyIBFooterTermsnConditions");
                hbxFooterPrelogin.linkSecurity.text = kony.i18n.getLocalizedString("keySecurityAdvices");
                hbxFooterPrelogin.linkWebChat.text = kony.i18n.getLocalizedString("keyIBFooterWebChat");
                hbxFooterPrelogin.link506459299592952.text = kony.i18n.getLocalizedString("keyContactUs");
            }
            else
            {
                hbxIBPostLogin.lnkLogOut.containerWeight = 5;
                hbxIBPostLogin.lnkLogOut.margin=[0,0,0,11];
                hbxIBPostLogin.label502795003206049.containerWeight = 14;
                hbxIBPostLogin.lnkLogOut.text = kony.i18n.getLocalizedString("keyIBLogout");
                hbxIBPostLogin.lnkHome.text = kony.i18n.getLocalizedString('keyIBHome');
                hbxIBPostLogin.lblLastLoginTime.text = kony.i18n.getLocalizedString("keyIBHeaderlblLastLogin");
                hbxIBPostLogin.lblName.text = gblCustomerName;
                hbxIBPostLogin.btnEng.skin = "btnEngLangOn";
                hbxIBPostLogin.btnThai.skin = "btnThaiLangOff";
                // footer chnage code
                hbxFooterPrelogin.linkhotpromo.containerWeight = 17;
                hbxFooterPrelogin.linkFindTmb.containerWeight = 12;
                hbxFooterPrelogin.linkExchangeRate.containerWeight = 17;
                hbxFooterPrelogin.linkSiteTour.containerWeight = 11;
                hbxFooterPrelogin.link506459299592952.containerWeight = 13;
                hbxFooterPrelogin.linkTnC.containerWeight = 30;
                //language locale flip
                hbxFooterPrelogin.linkhotpromo.text = kony.i18n.getLocalizedString("keyIBFooterHotPromotions");
                hbxFooterPrelogin.linkFindTmb.text = kony.i18n.getLocalizedString("keyIBFooterFindTMB");
                hbxFooterPrelogin.linkExchangeRate.text = kony.i18n.getLocalizedString("keyIBFooterExchange Rates");
                hbxFooterPrelogin.linkSiteTour.text = kony.i18n.getLocalizedString("keyIBFooterSiteTour");
                hbxFooterPrelogin.linkTnC.text = kony.i18n.getLocalizedString("keyIBFooterTermsnConditions");
                hbxFooterPrelogin.linkSecurity.text = kony.i18n.getLocalizedString("keySecurityAdvices");
                hbxFooterPrelogin.linkWebChat.text = kony.i18n.getLocalizedString("keyIBFooterWebChat");
                hbxFooterPrelogin.link506459299592952.text = kony.i18n.getLocalizedString("keyContactUs");
            }
            //added below if condition to implement MIB-961
            if(gblIsNewOffersExists){         	 
           		hbxFooterPrelogin.linkHotPromoImg.isVisible = true;
	        } else {
	        	hbxFooterPrelogin.linkHotPromoImg.isVisible = false;
	        }
            IBcopyright_footer_display();
			IBAppTitle();
			//
			if (resulttable.statusCode == "statusDesc") {
				var errMessage = kony.i18n.getLocalizedString("crmerrMsg");
				alert(errMessage)
			} else if (resulttable.statusCode == "errMsg") {
				var errMessage = kony.i18n.getLocalizedString("errMsg");
				alert(errMessage)
			}
			else if (resulttable.opstatus_hidden == 2){
			gblAccountTable = resulttable;
				gblLoggedIn = true;
			  dismissLoadingScreenPopup();
			   if (locale == "en_US")			      
			      frmIBPostLoginDashboard.lblUsername.text = gblCustomerName;		      
			    else
			  frmIBPostLoginDashboard.lblUsername.text = gblCustomerNameTh;	
			  frmIBPostLoginDashboard.lblAllHidden.setVisibility(true);
			  
			  //MIB-1069 ,MIB-1240
			  //frmIBPostLoginDashboard.lblAllHidden.text=kony.i18n.getLocalizedString("errMsgHidden"); 
			  frmIBPostLoginDashboard.lblAllHidden.text=kony.i18n.getLocalizedString("MB_ASTxt_NoAccount");	
			  frmIBPostLoginDashboard.lblAllHidden.setEnabled(true);
			  frmIBPostLoginDashboard.hbox149765754895937.onClick = onClickWhenNoAcctIB;
			  frmIBPostLoginDashboard.lblAllHidden.onClick = onClickWhenNoAcctIB;
			  //MIB-1069 ,MIB-1240
			  	    
			  createBarGraphIB(100, "lblBarGrey");
			  frmIBPostLoginDashboard.lblForUse.text = "0.00"+ kony.i18n.getLocalizedString("percent");
			  frmIBPostLoginDashboard.lblForsave.text ="0.00" + kony.i18n.getLocalizedString("percent");
			  frmIBPostLoginDashboard.lblFunds.text ="0.00" + kony.i18n.getLocalizedString("percent");
			  
			  frmIBPostLoginDashboard.lblTotalBal.text = "0.00"+ " "+ kony.i18n.getLocalizedString("currencyThaiBaht");
			  frmIBPostLoginDashboard.hbox867777585898845.isVisible=false;
			  frmIBPostLoginDashboard.segAccountDetails.removeAll()
			  
			  // added below if condition to implement MIB-1207- Any Id announcement page
			  // check the anyId annoucemnt flag and show the announcement page.
			  if(displayAnnoucementtoUser){
			  		displayAnnoucementtoUser = false;
			  		frmIBAnyIdAnnoucment.show();			
			  }else {
			  		frmIBPostLoginDashboard.show();
			  }	
			  //postLoginCLang();
			}
			 else {
				// code to get account summary
				frmIBAccntSummary.imgProfile.setVisibility(false);
				frmIBPostLoginDashboard.imgProfile.setVisibility(false);

				gblFinancialTxnIBLock = resulttable["enableMaintainenceFinancialTxnIB"];
				var count_dynamic_label = frmIBPostLoginDashboard.hbxBarGraph.ownchildrenref.push();
				var count_dynamic_label1 = frmIBAccntSummary.hbxBarGraph.ownchildrenref.push();
				for (i = 0; i < count_dynamic_label; i++)
					frmIBPostLoginDashboard.hbxBarGraph.removeAt(0);
				for (i = 0; i < count_dynamic_label1; i++)
					frmIBAccntSummary.hbxBarGraph.removeAt(0);
				gblAccountTable = resulttable;
				
				var forUse = 0 ;
				var forSave = 0 ;
				var forInvest = 0 ;
				
				if(isNotBlank(resulttable["forUse"]))
				 forUse = parseInt(resulttable["forUse"]);
				if(isNotBlank(resulttable["forSave"]))
				 forSave = parseInt(resulttable["forSave"]);
				if(isNotBlank(resulttable["forInvest"]))
				 forInvest = parseInt(resulttable["forInvest"]);

			    //alert("@@forInvest"+forInvest);
			    //forInvest = 10;
				
				if (forUse != 0) {
					createBarGraphIB(forUse, "lblBlueNew");
				}
				if (forSave != 0) {
					createBarGraphIB(forSave, "lblWhite");
				}
				if (forInvest != 0) {
					createBarGraphIB(forInvest, "lblBarGreen");
				}
				//MIB-1066
				if (forUse == 0 && forSave == 0 && forInvest == 0 ) {
					createBarGraphIB(100, "lblBarGrey");
				}
				//MIB-1066
				//alert("@@createBarGraphIB Ready ..");
				
				if(isNotBlank(resulttable["forUse"])) {
					frmIBPostLoginDashboard.lblForUse.text = resulttable["forUse"] + kony.i18n.getLocalizedString("percent");
				} else {
					frmIBPostLoginDashboard.lblForUse.text = "0.00" + kony.i18n.getLocalizedString("percent");				
				}
				if(isNotBlank(resulttable["forSave"])) {
					frmIBPostLoginDashboard.lblForsave.text = resulttable["forSave"] + kony.i18n.getLocalizedString("percent");
				} else {
					frmIBPostLoginDashboard.lblForsave.text = "0.00" + kony.i18n.getLocalizedString("percent");
				}
				if(isNotBlank(resulttable["forInvest"])) {
					frmIBPostLoginDashboard.lblFunds.text = resulttable["forInvest"] + kony.i18n.getLocalizedString("percent");
				} else {
					frmIBPostLoginDashboard.lblFunds.text = "0.00" + kony.i18n.getLocalizedString("percent");
				}
				if(isNotBlank(resulttable["totalBal"])) {
					frmIBPostLoginDashboard.lblTotalBal.text = resulttable["totalBal"] + " "+ kony.i18n.getLocalizedString("currencyThaiBaht");
				} else {
					frmIBPostLoginDashboard.lblTotalBal.text = "0.00 "+ kony.i18n.getLocalizedString("currencyThaiBaht");				
				}
					
				//alert("@@frmIBPostLoginDashboard finish");	
					
				if(isNotBlank(resulttable["forUse"])) {
					frmIBAccntSummary.lblForUse.text = resulttable["forUse"] + kony.i18n.getLocalizedString("percent");
				} else {
					frmIBAccntSummary.lblForUse.text = "0.00" + kony.i18n.getLocalizedString("percent");
				}	
				if(isNotBlank(resulttable["forSave"])) {
					frmIBAccntSummary.lblForsave.text = resulttable["forSave"] + kony.i18n.getLocalizedString("percent");
				} else {
					frmIBAccntSummary.lblForsave.text = "0.00" + kony.i18n.getLocalizedString("percent");				
				}

				if(isNotBlank(resulttable["forInvest"])) {
					frmIBAccntSummary.lblFunds.text = resulttable["forInvest"] + kony.i18n.getLocalizedString("percent");
				} else {
					frmIBAccntSummary.lblFunds.text = "0.00" + kony.i18n.getLocalizedString("percent");
				}
				
				if(resulttable["totalBal"]!=null) {
					frmIBAccntSummary.lblTotalBal.text = resulttable["totalBal"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
				} else {
					frmIBAccntSummary.lblTotalBal.text = "0.00 " + kony.i18n.getLocalizedString("currencyThaiBaht");
				}
				
				var list = [];
				var limited_list = [];
				gblCrmAccountNumbers = {};
				gblshorCutToAccounts=resulttable.shorCutToAccounts;
				var imageName ="";
				if(resulttable.custAcctRec != undefined){
				for (var i = 0; i < resulttable.custAcctRec.length; i++) {
					var obj = {};
					var accno = resulttable.custAcctRec[i].accId;
					kony.table.insert(gblCrmAccountNumbers, accno);
					imageName="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+resulttable["custAcctRec"][i]["ICON_ID"]+"&modIdentifier=PRODICON";
					/*if (resulttable["custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_DREAM_SAVING" || resulttable["custAcctRec"][i][
							"VIEW_NAME"] == "PRODUCT_CODE_NOFIXED" || resulttable["custAcctRec"][i]["VIEW_NAME"] ==
						"PRODUCT_CODE_SAVINGCARE")
						imageName = "prod_savingd.png"
					else if (resulttable["custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_CREDITCARD_TABLE")
						imageName = "prod_creditcard.png"
					else if (resulttable["custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_TD_TABLE")
						imageName = "prod_termdeposits.png"
					else if (resulttable["custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_NOFEESAVING_TABLE")
						imageName = "prod_nofee.png"
					else if (resulttable["custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_LOAN_HOMELOAN")
						imageName = "prod_homeloan.png"
					else if (resulttable["custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_OLDREADYCASH_TABLE" || resulttable[
							"custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_NEWREADYCASH_TABLE" || resulttable["custAcctRec"][i]["VIEW_NAME"] ==
						"PRODUCT_CODE_LOAN_CASH2GO")
						imageName = "prod_cash2go.png"
					else if (resulttable["custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_CURRENT_TABLE" || resulttable["custAcctRec"][
						i]["VIEW_NAME"] == "PRODUCT_CODE_SAVING_TABLE")
						imageName = "prod_currentac.png"
					else if (resulttable["custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_READYCASH_TABLE")
						imageName = "prod_cash2go.png"
				*/		
					if ((resulttable["custAcctRec"][i]["acctNickName"]) == null || (resulttable["custAcctRec"][i]["acctNickName"]) =='') {
						var sbStr = resulttable["custAcctRec"][i]["accId"];
			            var length = sbStr.length;
						if (resulttable["custAcctRec"][i]["accType"] == kony.i18n.getLocalizedString("Loan"))
				        sbStr = sbStr.substring(7, 11);
			             else
				        sbStr = sbStr.substring(length - 4, length);
						if (kony.i18n.getCurrentLocale() == "th_TH")
							obj.lblAccountName = resulttable.custAcctRec[i].ProductNameThai + " " + sbStr
						else
							obj.lblAccountName = resulttable.custAcctRec[i].ProductNameEng + " " + sbStr
						//resulttable["custAcctRec"][i]["acctNickName"] = obj.lblAccountName;	
							
					} else {
						obj.lblAccountName = resulttable["custAcctRec"][i]["acctNickName"];
					}
					obj.imgAccountSymbol = imageName;
					obj.imageRightArrow = "bg_arrow_right.png";
					//if(resulttable.custAcctRec[i].remainingNoFee != "") --- as per FS from Sujata
					if (resulttable["custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_NOFEESAVING_TABLE") {
						obj.lblRemainFree = kony.i18n.getLocalizedString("remFreeTran");
						obj.lblRemainFreeVal = resulttable.custAcctRec[i].remainingFee;
					}
					if (resulttable["custAcctRec"][i]["accType"] == kony.i18n.getLocalizedString("CreditCard"))
					{
						obj.lblBalanceVal = resulttable["custAcctRec"][i]["availableCreditBalDisplay"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht")
					}
					else
						obj.lblBalanceVal = resulttable["custAcctRec"][i]["availableBalDisplay"] + " " +  kony.i18n.getLocalizedString(
							"currencyThaiBaht")
					if (resulttable["custAcctRec"][i]["accType"] == kony.i18n.getLocalizedString("Loan"))
						obj.lblBalance = kony.i18n.getLocalizedString("OutstandingBalance");
					else if (resulttable["custAcctRec"][i]["accType"] == kony.i18n.getLocalizedString("termDeposit") || resulttable[
						"custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_NOFIXED")
						obj.lblBalance = kony.i18n.getLocalizedString("Balance");
					else if (resulttable["custAcctRec"][i]["accType"] == kony.i18n.getLocalizedString("CreditCard"))
						obj.lblBalance = kony.i18n.getLocalizedString("AvailableCreditLimit");
					else if (resulttable["custAcctRec"][i]["VIEW_NAME"] != "PRODUCT_CODE_NOFIXED" || resulttable["custAcctRec"][i][
						"accType"] == kony.i18n.getLocalizedString("Saving") || resulttable["custAcctRec"][i]["accType"] == kony.i18n.getLocalizedString(
						"Current"))
						obj.lblBalance = kony.i18n.getLocalizedString("Balance");
					list.push(obj);
					if(i<3) {
						limited_list.push(obj);
					}	
				}
				}
				
				//ENH_106_107 - Mutual Funds - MF Accounts Logic to add MF account in Customer Account Summary
					if(resulttable["mfAccountFlag"] == "true") {
						if(!gblHaveMutualFundsFlg){ gblHaveMutualFundsFlg = true; } 
						var mfObj = {};
						var randomnum = Math.floor((Math.random()*10000)+1); 
						imageName="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId=MF-Product&modIdentifier=MFLOGOS&rr="+randomnum;
						
						mfObj.imgAccountSymbol = imageName;
						mfObj.imageRightArrow = "bg_arrow_right.png";
						mfObj.lblAccountName = kony.i18n.getLocalizedString("MF_Acc_Sumary_Title");
						mfObj.lblRemainFree = kony.i18n.getLocalizedString("MF_lbl_Investment_value_h2");
						mfObj.lblBalanceVal = resulttable["mfTotalAmount"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
						mfObj.isMFAccount = "Y";
						mfObj.lblBalance = {
								"text":"",
	                    		"containerWeight": "10"
	                		 };
	                		 
						list.push(mfObj);
						limited_list.push(mfObj);
					}
					
					//ENH_108 - BA View Policy Details - Logic to add BA account in Customer Account Summary
					if(resulttable["baAccountFlag"] == "true") {
						if(!gblHaveBankAssuranceFlg){ gblHaveBankAssuranceFlg = true; } 
						var mfObj = {};
						var randomnum = Math.floor((Math.random()*10000)+1); 
						imageName="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId=BA_PROD_LOGO&modIdentifier=MFLOGOS&rr="+randomnum;
						
						mfObj.imgAccountSymbol = imageName;
						mfObj.imageRightArrow = "bg_arrow_right.png";
						mfObj.lblAccountName = kony.i18n.getLocalizedString("BA_Acc_Summary_Title");
						mfObj.lblRemainFree = kony.i18n.getLocalizedString("BA_lbl_Total_Sum_Insured")+":";
						
						var totalSumInsured = resulttable["totalSumInsured"];
						if(totalSumInsured != "-") {
							totalSumInsured = commaFormatted(parseFloat(totalSumInsured).toFixed(2)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
						} 
						mfObj.lblBalanceVal = totalSumInsured;
						mfObj.isBAAccount = "Y";
						mfObj.lblBalance = {
								"text":"",
	                    		"containerWeight": "10"
	                		 };
	                		 
						list.push(mfObj);
						limited_list.push(mfObj);
					}
					
			 	frmIBPostLoginDashboard.hbox867777585898845.isVisible=true;
				
				frmIBPostLoginDashboard.segAccountDetails.setData(limited_list);
				frmIBAccntSummary.segAccountDetails.setData(list);
				dismissLoadingScreenPopup();
				kony.application.dismissLoadingScreen();
				 if (locale == "en_US"){
			      frmIBAccntSummary.lblUsername.text=gblCustomerName;
			      frmIBPostLoginDashboard.lblUsername.text = gblCustomerName;
			      }
			    else{
			     frmIBPostLoginDashboard.lblUsername.text = gblCustomerNameTh;
			     frmIBAccntSummary.lblUsername.text=gblCustomerNameTh;
			     }
				gblLoggedIn = true;

				var currFormId = kony.application.getCurrentForm().id;
				
				if(currFormId == "frmIBPreLogin") {
					//invokeMasterBillerInqService();
					gblCalledMasterBillerService = 0;
					dismissLoadingScreenPopup();
	                //frmIBPostLoginDashboard.show();
					// added below if condition to implement MIB-1207- Any Id announcement page
					// check the anyId annoucemnt flag and show the announcement page.
					if (displayAnnoucementtoUser) {
						displayAnnoucementtoUser = false;
						frmIBAnyIdAnnoucment.show();
					} else {
						frmIBPostLoginDashboard.show();
					}
				}
				else {
					dismissLoadingScreenPopup();
					kony.application.dismissLoadingScreen();
					//frmIBPostLoginDashboard.show();
					// added below if condition to implement MIB-1207- Any Id announcement page
					// check the anyId annoucemnt flag and show the announcement page.
					if (displayAnnoucementtoUser) {
						displayAnnoucementtoUser = false;
						frmIBAnyIdAnnoucment.show();
					} else {
						frmIBPostLoginDashboard.show();
					}
					frmIBAccntSummary.imgProfile.setVisibility(true);
					frmIBPostLoginDashboard.imgProfile.setVisibility(true);
					//postLoginCLang();
				}
			}
		} else {
			if (resulttable["opstatus"] == 1) {
				
				if(resulttable["migrateduser"]=="false")
				{
					//alert("login page");
					gblMigration="false";
					//IBLogoutService();
					frmIBPreLogin.show();
				}
				else
				{
					showAlertIB(kony.i18n.getLocalizedString("accPwdNotValid"), kony.i18n.getLocalizedString("info"));
				}
				//loginFailUpdate();
				dismissLoadingScreenPopup();
				return false;
			} 
			else {
				if(resulttable["errMsg"] != undefined)
					alert(" " + resulttable["errMsg"]);
				else
					alert(kony.i18n.getLocalizedString("keyUndefinedError"));	
				dismissLoadingScreenPopup();
				return false;
			}
		}
	}
}

function showfrmIBPostLoginDashboard(){
	frmIBPostLoginDashboard.show();
}

function resetIBPassword() {
	showLoadingScreenPopup();
	var password_inputparam = {};
	password_inputparam["userID"] = frmIBCreateUserID.txtUserID.text;
	gblUserName = frmIBCreateUserID.txtUserID.text;
	password_inputparam["loginId"] = "IB_Pwd";
	password_inputparam["newPassword"] = frmIBCreateUserID.txtPassword.text; //map to transation password of form
	gblPassword = frmIBCreateUserID.txtPassword.text;
	password_inputparam["segIdVal"] = "MIB"
	invokeServiceSecureAsync("resetPasswordIB", password_inputparam, resetIBPasswordCallBack);
}

function resetIBPasswordCallBack(status, resulttable) {
	if (status == 400) {
		if (resulttable["opstatus"] == 0){
			if (resulttable["resetPasswordResponse"] != null){
					modifyUserForUnlockOTP();
			}
		}else if(resulttable["opstatus"] == 8009 && resulttable["errCode"] == "validationErr"){
             if(resulttable["errorKey"] == "keyPassSettingGuidelinesIB"){
             	dismissLoadingScreenPopup();
             	showAlertIB(kony.i18n.getLocalizedString("invalidPasswordIB"), kony.i18n.getLocalizedString("info"));
             	frmIBCreateUserID.txtPassword.text = "";
             	frmIBCreateUserID.txtConfirmPassword.text = "";
             	frmIBCreateUserID.txtPassword.setFocus(true);
             	return false;
             }
             if(resulttable["errorKey"] == "keyWrongPwd"){
             	dismissLoadingScreenPopup();
             	showAlertIB(kony.i18n.getLocalizedString("invalidPasswordIB"), kony.i18n.getLocalizedString("info"));
             	frmIBCreateUserID.txtPassword.text = "";
             	  	frmIBCreateUserID.txtConfirmPassword.text = "";
             	frmIBCreateUserID.txtPassword.setFocus(true);
             	return false;
             }
		}
		else{
				if (resulttable["opstatus"] == 8005)
					if(resulttable["errMsg"] != undefined)
						alert(resulttable["errMsg"]);
					else
						alert(kony.i18n.getLocalizedString("keyUndefinedError"));	
				dismissLoadingScreenPopup();
		}
	}
}

function IBsendTnCMailPartyInq() {

	
	var inputparam = {};
	inputparam["fIIdent"] = "";
	inputparam["rqUUId"] = "";
	inputparam["partyIdentValue"] = "";
	invokeServiceSecureAsync("partyInquiry", inputparam, IBsendTnCMailPartyInqCallBack);
}
function IBsendTnCMailPartyInqCallBack(status,resulttable )
{
	if (status == 400) 
	{
		//var isError = showCommonAlertIB(resulttable)
		//if (isError) return false;
		if (resulttable["opstatus"] == 0) 
		{
			gblCustomerName = resulttable["customerName"];
			gblCustomerNameTh=resulttable["customerNameTH"];
			gblEmailId = resulttable["emailId"];
			if(gblActionCode == "11" || gblActionCode == "04" || gblActionCode == "12" || gblActionCode == "13") {
 				onClickEmailTnC('TermsAndConditions');	
 			}
 			if(gblActionCode == "12"){
 			 var inputparam = {}
            	 
                // invokeServiceSecureAsync("partyInquiry", inputparam, rectivationIBCallBack);
 			}
		
		}
		else 
		{
			
		}
		
	}
}


/**
 * description
 * @returns {}
 */
function modifyUserForUnlockOTP() {
    var inputParams = {}
	inputParams["userId"] = frmIBCreateUserID.txtUserID.text;
	inputParams["status"] = "Activated";
	invokeServiceSecureAsync("modifyUserToUnlockOTP", inputParams, callBackmodifyUserForUnlockOTP);
    
}
function callBackmodifyUserForUnlockOTP(status,resultTable) {
	if(status == 400){
		if (resultTable["opstatus"] == 0) {
			IBcrmProfileUpdate();
		}else{
			
			if(resulttable["errMsg"] != undefined)
				alert(resulttable["errMsg"]);
			else
				alert(kony.i18n.getLocalizedString("keyUndefinedError"));	
			dismissLoadingScreenPopup();
		}
	}
	
	
}

function accountsummaryLangToggleIB() {
	showLoadingScreenPopup();
	inputparam = {};
	//inputparam["crmID"]=gblcrmId;
	inputparam["rqUUId"] = "";
	inputparam["bankCd"] = gblTMBBankCD;
	inputparam["activationCompleteFlag"] = "true";
	invokeServiceSecureAsync("customerAccountInquiry", inputparam, accountsummaryLangToggleIBcallback);
}

function accountsummaryLangToggleIBcallback(status, resulttable) {
	if (status == 400) {
		
		var locale = kony.i18n.getCurrentLocale();
		if (resulttable["opstatus"] == 0) {
			gblshorCutToAccounts=resulttable.shorCutToAccounts;
			frmIBAccntSummary.lblAllHidden.setVisibility(false)
			
			if (resulttable.statusCode == "statusDesc") {
				var errMessage = kony.i18n.getLocalizedString("crmerrMsg");
				alert(errMessage)
			} else if (resulttable.statusCode == "errMsg") {
				var errMessage = kony.i18n.getLocalizedString("errMsg");
				alert(errMessage)
			}
			else if (resulttable.opstatus_hidden == 2) {
			gblAccountTable = resulttable;
			  dismissLoadingScreenPopup();
			  //MIB-1066
			  createBarGraphIB(100, "lblBarGrey");
			  //MIB-1066
			  frmIBAccntSummary.lblForUse.text = "0.00" + kony.i18n.getLocalizedString("percent");
			  frmIBAccntSummary.lblForsave.text = "0.00" + kony.i18n.getLocalizedString("percent");
			  frmIBAccntSummary.lblFunds.text ="0.00" + kony.i18n.getLocalizedString("percent");
			  frmIBAccntSummary.lblTotalBal.text = "0.00" + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
			  if (locale == "en_US")
			  frmIBAccntSummary.lblUsername.text=gblCustomerName;
			  else
			  frmIBAccntSummary.lblUsername.text=gblCustomerNameTh;
			  frmIBAccntSummary.lblAllHidden.setVisibility(true);
			  //MIB-1069
			  //frmIBAccntSummary.lblAllHidden.text=kony.i18n.getLocalizedString("errMsgHidden");
			  frmIBAccntSummary.lblAllHidden.text=kony.i18n.getLocalizedString("MB_ASTxt_NoAccount");
			  //frmIBPostLoginDashboard.lblAllHidden
			  frmIBAccntSummary.lblAllHidden.setEnabled(true);
			  frmIBAccntSummary.hbox1434108470126789.onClick = onClickWhenNoAcctIB;
			  frmIBAccntSummary.lblAllHidden.onClick = onClickWhenNoAcctIB;
			  //MIB-1069
			  frmIBAccntSummary.segAccountDetails.removeAll();
			  resetdetailspage();
			    			   
			  frmIBAccntSummary.hbox449290336674865.setVisibility(false);
			  frmIBAccntSummary.hbxAccntDetails.setVisibility(false);
			  frmIBAccntSummary.line449290336723181.setVisibility(false);
			  frmIBAccntSummary.hboxApplySendToSave.setVisibility(false);
			  frmIBAccntSummary.lnkDreamSaving.setVisibility(false);
			   
			  var imageTmb = new kony.ui.Image2({
			        "id": "imageTmb",
			        "isVisible": true,
			        "src": "tmb_logo_blue_new.png",
			        "imageWhenFailed": null,
			        "imageWhileDownloading": null
			   }, {
			        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
			        "margin": [0, 35, 0, 0],
			        "padding": [0, 0, 0, 0],
			        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
			        "referenceWidth": null,
			        "referenceHeight": null,
			        "marginInPixel": false,
			        "paddingInPixel": false,
			        "containerWeight": 100
			   }, {
			        "toolTip": null
			   });
			    
			   if(frmIBAccntSummary.imageTmb == undefined){	  
			   	 frmIBAccntSummary.vbox449290336673324.add(imageTmb);
			   }
			   frmIBAccntSummary.show();
			}
			 else {
				// code to get account summary
				frmIBAccntSummary.imgProfile.setVisibility(false);
				frmIBPostLoginDashboard.imgProfile.setVisibility(false);
				
				gblFinancialTxnIBLock = resulttable["enableMaintainenceFinancialTxnIB"];
				var count_dynamic_label = frmIBPostLoginDashboard.hbxBarGraph.ownchildrenref.push();
				var count_dynamic_label1 = frmIBAccntSummary.hbxBarGraph.ownchildrenref.push();
				for (i = 0; i < count_dynamic_label; i++)
					frmIBPostLoginDashboard.hbxBarGraph.removeAt(0);
				for (i = 0; i < count_dynamic_label1; i++)
					frmIBAccntSummary.hbxBarGraph.removeAt(0);
				gblAccountTable = resulttable;
				
				var forUse = 0;
				var forSave = 0;
				var forInvest = 0;
				
				if(isNotBlank(resulttable["forUse"]))
					forUse = parseInt(resulttable["forUse"]);
				
				if(isNotBlank(resulttable["forSave"]))
					forSave = parseInt(resulttable["forSave"]);
				
				if(isNotBlank(resulttable["forInvest"]))
					forInvest = parseInt(resulttable["forInvest"]);
			    
			    //forInvest = 10.00;
			    //alert("@@forInvest : "+forInvest);
				
				if (forUse != 0) {
					createBarGraphIB(forUse, "lblBlueNew");
				}
				if (forSave != 0) {
					createBarGraphIB(forSave, "lblWhite");
				}
				if (forInvest != 0) {
					createBarGraphIB(forInvest, "lblBarGreen");
				}
				
				//MIB-1066
				if (forUse == 0 && forSave == 0 && forInvest == 0) {
					createBarGraphIB(100, "lblBarGrey");	
				}
				//MIB-1066
				
			//	alert("@@createBarGraphIB finish");
				
				if(isNotBlank(resulttable["forUse"])) {
					frmIBPostLoginDashboard.lblForUse.text = resulttable["forUse"] + kony.i18n.getLocalizedString("percent");
				} else {
					frmIBPostLoginDashboard.lblForUse.text = "0.00" + kony.i18n.getLocalizedString("percent");				
				}
				if(isNotBlank(resulttable["forSave"])) {
					frmIBPostLoginDashboard.lblForsave.text = resulttable["forSave"] + kony.i18n.getLocalizedString("percent");
				} else {
					frmIBPostLoginDashboard.lblForsave.text = "0.00" + kony.i18n.getLocalizedString("percent");
				}
				if(isNotBlank(resulttable["forInvest"])) {
					frmIBPostLoginDashboard.lblFunds.text = resulttable["forInvest"] + kony.i18n.getLocalizedString("percent");
				} else {
					frmIBPostLoginDashboard.lblFunds.text = "0.00" + kony.i18n.getLocalizedString("percent");
				}
				if(isNotBlank(resulttable["totalBal"])) {
					frmIBPostLoginDashboard.lblTotalBal.text = resulttable["totalBal"] + " "+ kony.i18n.getLocalizedString("currencyThaiBaht");
				} else {
					frmIBPostLoginDashboard.lblTotalBal.text = "0.00 "+ kony.i18n.getLocalizedString("currencyThaiBaht");				
				}
				
				//alert("@@frmIBPostLoginDashboard finish");	
					
				if(isNotBlank(resulttable["forUse"])) {
					frmIBAccntSummary.lblForUse.text = resulttable["forUse"] + kony.i18n.getLocalizedString("percent");
				} else {
					frmIBAccntSummary.lblForUse.text = "0.00" + kony.i18n.getLocalizedString("percent");
				}	
				if(isNotBlank(resulttable["forSave"])) {
					frmIBAccntSummary.lblForsave.text = resulttable["forSave"] + kony.i18n.getLocalizedString("percent");
				} else {
					frmIBAccntSummary.lblForsave.text = "0.00" + kony.i18n.getLocalizedString("percent");				
				}

				if(isNotBlank(resulttable["forInvest"])) {
					frmIBAccntSummary.lblFunds.text = resulttable["forInvest"] + kony.i18n.getLocalizedString("percent");
				} else {
					frmIBAccntSummary.lblFunds.text = "0.00" + kony.i18n.getLocalizedString("percent");
				}
				
				if(resulttable["totalBal"]!=null) {
					frmIBAccntSummary.lblTotalBal.text = resulttable["totalBal"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
				} else {
					frmIBAccntSummary.lblTotalBal.text = "0.00 " + kony.i18n.getLocalizedString("currencyThaiBaht");
				}
				var list = [];
				var imageName = "";
				for (var i = 0; i < resulttable.custAcctRec.length; i++) {
					var obj = {};
					var accno = resulttable.custAcctRec[i].accId;
					
					imageName="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+resulttable["custAcctRec"][i]["ICON_ID"]+"&modIdentifier=PRODICON";
					/*if (resulttable["custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_DREAM_SAVING" || resulttable["custAcctRec"][i][
							"VIEW_NAME"] == "PRODUCT_CODE_NOFIXED" || resulttable["custAcctRec"][i]["VIEW_NAME"] ==
						"PRODUCT_CODE_SAVINGCARE")
						imageName = "prod_savingd.png"
					else if (resulttable["custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_CREDITCARD_TABLE")
						imageName = "prod_creditcard.png"
					else if (resulttable["custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_TD_TABLE")
						imageName = "prod_termdeposits.png"
					else if (resulttable["custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_NOFEESAVING_TABLE")
						imageName = "prod_nofee.png"
					else if (resulttable["custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_LOAN_HOMELOAN")
						imageName = "prod_homeloan.png"
					else if (resulttable["custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_OLDREADYCASH_TABLE" || resulttable[
							"custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_NEWREADYCASH_TABLE" || resulttable["custAcctRec"][i]["VIEW_NAME"] ==
						"PRODUCT_CODE_LOAN_CASH2GO")
						imageName = "prod_cash2go.png"
					else if (resulttable["custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_CURRENT_TABLE" || resulttable["custAcctRec"][
						i]["VIEW_NAME"] == "PRODUCT_CODE_SAVING_TABLE")
						imageName = "prod_currentac.png"
					else if (resulttable["custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_READYCASH_TABLE")
						imageName = "prod_cash2go.png"
				*/		
					if ((resulttable["custAcctRec"][i]["acctNickName"]) == null || (resulttable["custAcctRec"][i]["acctNickName"]) ==
						'') {

						var sbStr = resulttable["custAcctRec"][i]["accId"];
			            var length = sbStr.length;
						if (resulttable["custAcctRec"][i]["accType"] == kony.i18n.getLocalizedString("Loan"))
				        sbStr = sbStr.substring(7, 11);
			             else
				        sbStr = sbStr.substring(length - 4, length);
						if (kony.i18n.getCurrentLocale() == "th_TH")
							obj.lblAccountName = resulttable.custAcctRec[i].ProductNameThai + " " + sbStr
						else
							obj.lblAccountName = resulttable.custAcctRec[i].ProductNameEng + " " + sbStr
						//resulttable["custAcctRec"][i]["acctNickName"] = obj.lblAccountName;	
							
					} else {
						obj.lblAccountName = resulttable["custAcctRec"][i]["acctNickName"];
					}
					obj.imgAccountSymbol = imageName;
					obj.imageRightArrow = "bg_arrow_right.png";
					//if(resulttable.custAcctRec[i].remainingNoFee != "") --- as per FS from Sujata
					if (resulttable["custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_NOFEESAVING_TABLE") {
						obj.lblRemainFree = kony.i18n.getLocalizedString("remFreeTran");
						obj.lblRemainFreeVal = resulttable.custAcctRec[i].remainingFee;
					}
					if (resulttable["custAcctRec"][i]["accType"] == kony.i18n.getLocalizedString("CreditCard"))
					{
						obj.lblBalanceVal = resulttable["custAcctRec"][i]["availableCreditBalDisplay"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht")
					}
					else
						obj.lblBalanceVal = resulttable["custAcctRec"][i]["availableBalDisplay"] + " " + kony.i18n.getLocalizedString(
							"currencyThaiBaht")
					if (resulttable["custAcctRec"][i]["accType"] == kony.i18n.getLocalizedString("Loan"))
						obj.lblBalance = kony.i18n.getLocalizedString("OutstandingBalance");
					else if (resulttable["custAcctRec"][i]["accType"] == kony.i18n.getLocalizedString("termDeposit") || resulttable[
						"custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_NOFIXED")
						obj.lblBalance = kony.i18n.getLocalizedString("Balance");
					else if (resulttable["custAcctRec"][i]["accType"] == kony.i18n.getLocalizedString("CreditCard"))
						obj.lblBalance = kony.i18n.getLocalizedString("AvailableCreditLimit");
					else if (resulttable["custAcctRec"][i]["VIEW_NAME"] != "PRODUCT_CODE_NOFIXED" || resulttable["custAcctRec"][i][
						"accType"] == kony.i18n.getLocalizedString("Saving") || resulttable["custAcctRec"][i]["accType"] == kony.i18n.getLocalizedString(
						"Current"))
						obj.lblBalance = kony.i18n.getLocalizedString("Balance");
					list.push(obj);
				}
				
				//ENH_106_107 - Mutual Funds - MF Accounts Logic to add MF account in Customer Account Summary
				if(resulttable["mfAccountFlag"] == "true") {
						if(!gblHaveMutualFundsFlg){ gblHaveMutualFundsFlg = true;} 
						var mfObj = {};
						var randomnum = Math.floor((Math.random()*10000)+1); 
						imageName="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId=MF-Product&modIdentifier=MFLOGOS&rr="+randomnum;
						
						mfObj.imgAccountSymbol = imageName;
						mfObj.imageRightArrow = "bg_arrow_right.png";
						mfObj.lblAccountName = kony.i18n.getLocalizedString("MF_Acc_Sumary_Title");
						mfObj.lblRemainFree = kony.i18n.getLocalizedString("MF_lbl_Investment_value_h2");
						mfObj.lblBalanceVal = resulttable["mfTotalAmount"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
						mfObj.isMFAccount = "Y";
						mfObj.lblBalance = {
								"text":"",
	                    		"containerWeight": "10"
	                		 };
	                		 
						list.push(mfObj);
				}
				
				//ENH_108 - BA View Policy Details - Logic to add BA account in Customer Account Summary
					if(resulttable["baAccountFlag"] == "true") {
						if(!gblHaveBankAssuranceFlg){ gblHaveBankAssuranceFlg = true; } 
						var mfObj = {};
						var randomnum = Math.floor((Math.random()*10000)+1); 
						imageName="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId=BA_PROD_LOGO&modIdentifier=MFLOGOS&rr="+randomnum;
						
						mfObj.imgAccountSymbol = imageName;
						mfObj.imageRightArrow = "bg_arrow_right.png";
						mfObj.lblAccountName = kony.i18n.getLocalizedString("BA_Acc_Summary_Title");
						mfObj.lblRemainFree = kony.i18n.getLocalizedString("BA_lbl_Total_Sum_Insured")+":";
						
						var totalSumInsured = resulttable["totalSumInsured"];
						if(totalSumInsured != "-") {
							totalSumInsured = commaFormatted(parseFloat(totalSumInsured).toFixed(2)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
						} 
						mfObj.lblBalanceVal = totalSumInsured;
						mfObj.isBAAccount = "Y";
						mfObj.lblBalance = {
								"text":"",
	                    		"containerWeight": "10"
	                		 };
	                		 
						list.push(mfObj);
					}
					
				gblIndex = frmIBAccntSummary.segAccountDetails.selectedIndex;
				
				frmIBPostLoginDashboard.segAccountDetails.setData(list);
				frmIBAccntSummary.segAccountDetails.setData(list);
				dismissLoadingScreenPopup();
				kony.application.dismissLoadingScreen();
				 if (locale == "en_US"){
			      frmIBAccntSummary.lblUsername.text=gblCustomerName;
			      frmIBPostLoginDashboard.lblUsername.text = gblCustomerName;
			      }
			    else{
			     frmIBPostLoginDashboard.lblUsername.text = gblCustomerNameTh;
			     frmIBAccntSummary.lblUsername.text=gblCustomerNameTh;
			     }
				
				
				//gblLoggedIn = "true";
				frmIBAccntSummary.segAccountDetails.selectedIndex = gblIndex;
				//imagestrch issue code
				var randomnum = Math.floor((Math.random()*10000)+1); 
            	gblMyProfilepic="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=Y&personalizedId=&billerId=&dummy=" +randomnum;
				frmIBPostLoginDashboard.imgProfile.imageScaleMode=constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO;
				frmIBAccntSummary.imgProfile.imageScaleMode=constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO;
				frmIBPostLoginDashboard.imgProfile.src = gblMyProfilepic;
				frmIBAccntSummary.imgProfile.src = gblMyProfilepic;
				hbxIBPostLogin.imgProfilePic.src = gblMyProfilepic ;
				
				frmIBAccntSummary.show();
				try{
					kony.timer.cancel("imagedisplaycheck");
				}
				catch(e){
				}
				kony.timer.schedule("imagedisplaycheck", imageDsplayQuick, 5, false)
				if(kony.i18n.getCurrentLocale() == "th_TH")
					frmIBAccntSummary.btnMenuMyAccountSummary.skin="btnIBMenuMyAccountSummaryFocusThai";
				else
					frmIBAccntSummary.btnMenuMyAccountSummary.skin="btnIBMenuMyAccountSummaryFocus";
				frmIBAccntSummary.segMenuOptions.removeAll();
				frmIBAccntSummary.segAccountDetails.selectedIndex = gblIndex; // To retain index so that we can come back to same account from Edit beneficiary and other flows.
				showIBAccountDetails();
			}
		} else {
			if (resulttable["opstatus"] == 1) {
				showAlertIB(kony.i18n.getLocalizedString("accPwdNotValid"), kony.i18n.getLocalizedString("info"));
				//loginFailUpdate();
				dismissLoadingScreenPopup();
				return false;
			} 
			 /*else if (resulttable["opstatus"] == 2){
			  dismissLoadingScreenPopup();
			  frmIBAccntSummary.lblForUse.text = "0" + kony.i18n.getLocalizedString("percent");
			  frmIBAccntSummary.lblForsave.text = "0" + kony.i18n.getLocalizedString("percent");
		      frmIBAccntSummary.lblTermDeposit.text ="0" + kony.i18n.getLocalizedString("percent");
			  frmIBAccntSummary.lblTotalBal.text = "0.00" + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
			  frmIBAccntSummary.lblUsername.text=gblCustomerName;
			  frmIBAccntSummary.show()
			}*/
			else {
				if(resulttable["errmsg"] != undefined)
					alert(" " + resulttable["errMsg"]);
				else 
					alert(kony.i18n.getLocalizedString("keyUndefinedError"));
				dismissLoadingScreenPopup();
				return false;
			}
		}
	}
}

function imageDsplayQuick()
{
frmIBAccntSummary.imgProfile.setVisibility(true);
}

function syncIBAccountDetailsUpdate()
{
	if (kony.application.getCurrentForm().id == "frmIBPostLoginDashboard") 
	{
		menuReset2();
		onclickActivationCompleteStart();
	}
	if (kony.application.getCurrentForm().id == "frmIBAccntSummary")
	{	
		frmIBAccntSummary.imgProfile.setVisibility(false);
		accountsummaryLangToggleIB();
		if(kony.i18n.getCurrentLocale() == "th_TH")
			frmIBAccntSummary.btnMenuMyAccountSummary.skin="btnIBMenuMyAccountSummaryFocusThai";
		else
			frmIBAccntSummary.btnMenuMyAccountSummary.skin="btnIBMenuMyAccountSummaryFocus";
			
		
		//showIBAccountDetails();
		
	}	
}

function onClickActLink(rch){
	
	var onclickLink = rch.toString();
	if(rch["href"] !=null){
		gblFindTMBViewTypeIB = "MapView";
		gblLatitudeIB = "";
		gblLongitudeIB = "";
		//finTMBfooterLinks();
		onClickFindATMIB();
	}else{
		
	}
}

function onClickWhenNoAcctIB(){
   //alert("onClickWhenNoAcctIB");
   frmIBMyAccnts.show();
	srvGetBankListIB();
	if(getCRMLockStatus()){	
	curr_form=kony.application.getCurrentForm().id;
		popIBBPOTPLocked.show();
	}else{
	frmIBMyAccnts.hbxTMBImg.setVisibility(false);
	//frmIBMyAccnts.imgArrow.setVisibility(true);
	
	
	frmIBMyAccnts.vbox447417227428995.skin="vbxRightColumn15px";
	frmIBMyAccnts.hboxAddNewAccnt.setVisibility(true);
	frmIBMyAccnts.hbxViewAccnt.setVisibility(false);
	frmIBMyAccnts.hbxEditAccntNN.setVisibility(false);
	frmIBMyAccnts.lblOtherBankHdr.setVisibility(false);
	if(frmIBMyAccnts.segOtherBankAccntsList.data!=null && frmIBMyAccnts.segOtherBankAccntsList.data!=undefined && frmIBMyAccnts.segOtherBankAccntsList.data.length>0)
	frmIBMyAccnts.lblOtherBankHdr.setVisibility(true);


	gblMyAccntAddTmpData.length = 0;
	frmIBAddMyAccnt.segAccntDetails.removeAll();
	NON_TMB_ADD = 0;
	
	frmIBMyAccnts.lblCreditCardNum.setVisibility(false);
	frmIBMyAccnts.lblsuffix.setVisibility(false);
	frmIBMyAccnts.txtbxSuffix.setVisibility(false);

	frmIBMyAccnts.hbxCreditCardNum.setVisibility(false);
	frmIBMyAccnts.hbxAccntNum.setVisibility(true);
	frmIBMyAccnts.lblAccntNum.setVisibility(true);
	frmIBMyAccnts.txtbxOtherAccnt.setVisibility(false);
	frmIBMyAccnts.cmbobxAccntType.setVisibility(false);
	frmIBMyAccnts.lineComboAccntype.setVisibility(false);
	//frmIBMyAccnts.arrwImgSeg1.setVisibility(false);
//	frmIBMyAccnts.arrwImgSeg2.setVisibility(false);
	frmIBMyAccnts.hbxAccountName.setVisibility(false);
	
	frmIBMyAccnts.tbxAccountName.text = "";

	frmIBMyAccnts.txtAccNum1.text = "" 
	frmIBMyAccnts.txtAccNum2.text = "" 
	frmIBMyAccnts.txtAccNum3.text = ""
	frmIBMyAccnts.txtAccNum4.text = ""
	frmIBMyAccnts.txtNickNAme.text = ""
	frmIBMyAccnts.txtbxOtherAccnt.text = ""
	frmIBMyAccnts.cmbobxBankType.selectedKey = "key1"
	frmIBMyAccnts.cmbobxBankType.setVisibility(true);
	frmIBMyAccnts.line363582120273807.setVisibility(false);
	
	}
	
	frmIBMyAccnts.lblMyAccunts.setVisibility(true);	
	frmIBMyAccnts.lblMyAccunts.text = kony.i18n.getLocalizedString("keyMyAccountsIB");
	
	frmIBMyAccnts.hbxTMBImg.setVisibility(false);
	if(frmIBMyAccnts.imgArrow != undefined){
		frmIBMyAccnts.imgArrow.setVisibility(true);
	}
	frmIBMyAccnts.hboxAddNewAccnt.setVisibility(true);
	frmIBMyAccnts.hbxViewAccnt.setVisibility(false);
	frmIBMyAccnts.hbxEditAccntNN.setVisibility(false);


	gblMyAccntAddTmpData.length = 0;
	frmIBAddMyAccnt.segAccntDetails.removeAll();
	NON_TMB_ADD = 0;

	frmIBMyAccnts.lblCreditCardNum.setVisibility(false);
	frmIBMyAccnts.lblsuffix.setVisibility(false);
	frmIBMyAccnts.txtbxSuffix.setVisibility(false);

	frmIBMyAccnts.hbxCreditCardNum.setVisibility(false);
	frmIBMyAccnts.hbxAccntNum.setVisibility(true);
	frmIBMyAccnts.lblAccntNum.setVisibility(true);
	frmIBMyAccnts.txtbxOtherAccnt.setVisibility(false);
	frmIBMyAccnts.cmbobxAccntType.setVisibility(false);
	frmIBMyAccnts.lineComboAccntype.setVisibility(false);
	frmIBMyAccnts.arrwImgSeg1.setVisibility(false);
	frmIBMyAccnts.arrwImgSeg2.setVisibility(false);

	frmIBMyAccnts.txtAccNum1.text = "" 
	frmIBMyAccnts.txtAccNum2.text = "" 
	frmIBMyAccnts.txtAccNum3.text = ""
	frmIBMyAccnts.txtAccNum4.text = ""
	frmIBMyAccnts.txtNickNAme.text = ""
	frmIBMyAccnts.txtbxOtherAccnt.text = ""
	frmIBMyAccnts.cmbobxBankType.selectedKey = "key1"
	frmIBMyAccnts.cmbobxBankType.setVisibility(true);
}

