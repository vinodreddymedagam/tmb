function frmMBRequestNewPinMenuPreshow(){
	if (gblCallPrePost) 
	{
		frmMBRequestNewPin.imgCard.src = frmMBManageCard.imgCard.src;
		frmMBRequestNewPin.lblCardNumber.skin = getCardNoSkin(frmMBRequestNewPin.imgCard.src); //MKI, MIB-12137 start
      	if(frmMBRequestNewPin.imgCard.src.toLowerCase().includes("debittmbwave")){      
        	frmMBRequestNewPin.lblCardAccountName.skin = "lblBlackCard22px";
        }
        else{         
            frmMBRequestNewPin.lblCardAccountName.skin = "lblWhiteCard22px";
        } //MKI, MIB-12137 end
		frmMBRequestNewPin.lblCardNumber.text = frmMBManageCard.lblCardNumber.text;
		frmMBRequestNewPin.lblCardAccountName.text = frmMBManageCard.lblCardAccountName.text;
		frmMBRequestNewPin.flxBody.scrollToWidget(frmMBRequestNewPin.lblRequestPinTitle);
		//TODO : Change address from service
		//frmMBRequestNewPin.rchAddress.text = "32 soi Bangwak 150 Bangwak Road, Bangpai Bangkea Bangkok 10100 Thailand";
		
		frmMBRequestNewPinLocalePreshow();
	}
}

function frmMBRequestNewPinLocalePreshow(){
	changeStatusBarColor();
	frmMBRequestNewPin.lblRequestPinHeaderTitle.text = kony.i18n.getLocalizedString("RequestPIN_Title");
	frmMBRequestNewPin.lblRequestPinTitle.text = kony.i18n.getLocalizedString("RequestPIN_Topic");
	frmMBRequestNewPin.lblChangeAddrTitle.text = kony.i18n.getLocalizedString("RequestPIN_InstructionSentTo");
	frmMBRequestNewPin.lblChangeAddrLink.text = kony.i18n.getLocalizedString("RequestPIN_WantChangeAdd");
	frmMBRequestNewPin.btnBack.text = kony.i18n.getLocalizedString("Back");
	frmMBRequestNewPin.btnConfirm.text = kony.i18n.getLocalizedString("CAV07_btnConfirm");
	
}

function frmMBRequestNewPinMenuPostshow(){
    if (gblCallPrePost) {
        // Add the new code in the post show please add here

    }
    assignGlobalForMenuPostshow();
    addAccessPinKeypad(frmMBRequestNewPin);
}

function frmMBRequestPinFailurePreshow(){
	if (gblCallPrePost) 
	{
		frmMBRequestPinFailureLocalePreshow();
	}
}

function frmMBRequestPinFailureLocalePreshow(){
	changeStatusBarColor();
	//keyManageCreditCard
	frmMBRequestPinFailure.lblHeader.text = kony.i18n.getLocalizedString("CAV08_keyTitle");
	if(frmMBRequestPinFailure.flxtryagain.isVisible){
       frmMBRequestPinFailure.lblTryAgain.text = kony.i18n.getLocalizedString("btnTryAgain");
    }
	frmMBRequestPinFailure.lblCallcenternum.text = kony.i18n.getLocalizedString("DBI03_Call1558");
	frmMBRequestPinFailure.lblManageCards.text = kony.i18n.getLocalizedString("CCA05_BtnActOtherCard");
	frmMBRequestPinFailure.rchAddress.text = kony.i18n.getLocalizedString("NewPIN_ResultError");
	//NewPIN_ResultError
}

function frmMBRequestPinFailurePostshow(){
	/*if(!frmMBRequestPinFailure.flxtryagain.isVisible){
       frmMBRequestPinFailure.flxtryagain.setVisibility(true);
    }*/
}

function frmMBManageCardMenuPostshow(){
    if (gblCallPrePost) {
        // Add the new code in the post show please add here
		
    }
    assignGlobalForMenuPostshow();
}


function frmMBRequestPinSuccessPreshow(){
	if (gblCallPrePost) 
	{
		frmMBRequestPinSuccessLocalePreshow();
	}
}

function frmMBRequestPinSuccessLocalePreshow(){
	changeStatusBarColor();
	frmMBRequestPinSuccess.flxBody.scrollToWidget(frmMBRequestPinSuccess.flxCompleteImg);
	frmMBRequestPinSuccess.lblHeader.text = kony.i18n.getLocalizedString("CAV08_keyTitle");
	frmMBRequestPinSuccess.lblAvailableCashDesc.text = kony.i18n.getLocalizedString("NewPIN_ResultDesc");
	frmMBRequestPinSuccess.rchPinDesc.text = kony.i18n.getLocalizedString("NewPIN_Instruction");
	frmMBRequestPinSuccess.lblManageCards.text = kony.i18n.getLocalizedString("CCA05_BtnActOtherCard");
	
}

function frmMBRequestPinSuccessPostshow(){
    if (gblCallPrePost) {
        // Add the new code in the post show please add here

    }
    assignGlobalForMenuPostshow();
}