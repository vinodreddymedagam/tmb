//const for term condition flow
var TNC_FLOW_TMB_APPLY_LOAN = "TMB_NEW_LOAN";

//service & service call back

function callServiceReadUTFFileLoan() {
    var input_param = {};
    var locale = kony.i18n.getCurrentLocale();
    if (locale == "en_US") {
        input_param["localeCd"] = "en_US";
    } else {
        input_param["localeCd"] = "th_TH";
    }
    var moduleKey = "";
  if (gblMBNewTncFlow == TNC_FLOW_TMB_APPLY_LOAN) {
        if(gblLoanAppType =="27"){
          moduleKey = "TMBPersonalLoan"; 
        }else {
          moduleKey = "TMBCreditLoan"; 
        }
         //TMBBlockCreditCard  TMBApplyLoan
    }
    input_param["moduleKey"] = moduleKey;
    showLoadingScreen();
    invokeServiceSecureAsync("readUTFFile", input_param, callBackServiceReadUTFFileforLoan);

}

function frmMBNewTnCLoanReloadCB(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
            frmMBNewTnCLoan.btnRight.setVisibility(true);
            frmMBNewTnCLoan.btnnext.text = kony.i18n.getLocalizedString("keyAgreeButton");
            frmMBNewTnCLoan.btnback.text = kony.i18n.getLocalizedString("Back");
            frmMBNewTnCLoan.richtextTnC.text = result["fileContent"];
            frmMBNewTnCLoan.lblDescSubTitle.setFocus(true);
            dismissLoadingScreen();
        } else {
            dismissLoadingScreen();
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}

function callBackServiceReadUTFFileforLoan(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
        	kony.print("success");
            frmMBNewTnCLoan.btnRight.setVisibility(true);
            //frmMBNewTnCLoan.btnnext.text = kony.i18n.getLocalizedString("keyAgreeButton");
            //frmMBNewTnCLoan.btnback.text = kony.i18n.getLocalizedString("Back");
            frmMBNewTnCLoan.richtextTnC.text = result["fileContent"];
            //frmMBNewTnCLoan.lblDescSubTitle.setFocus(true);
            dismissLoadingScreen();
            frmMBNewTnCLoan.show();
        } else {
            dismissLoadingScreen();
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}
function callServiceTCEMailServiceLoan(moduleKey) {
    //call notification add service for email with the tnc keyword
    showLoadingScreen()
    var inputparam = {};
    inputparam["channelName"] = "Mobile Banking";
    inputparam["channelID"] = "02";
    inputparam["notificationType"] = "Email"; // always email
    inputparam["phoneNumber"] = gblPHONENUMBER;
    inputparam["mail"] = gblEmailId;
    inputparam["customerName"] = gblCustomerName;
    inputparam["localeCd"] = kony.i18n.getCurrentLocale();
    inputparam["moduleKey"] = moduleKey;
    //inputparam["fileNameOpen"] = fileNameOpen;
    invokeServiceSecureAsync("TCEMailService", inputparam, callbackServiceTCEMailServiceLoan);
}

function callbackServiceTCEMailServiceLoan(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
            var StatusCode = result["StatusCode"];
            var Severity = result["Severity"];
            var StatusDesc = result["StatusDesc"];
            if (StatusCode == "0") {
                showAlert(kony.i18n.getLocalizedString("keytermOpenAcnt"), kony.i18n.getLocalizedString("info"));
                dismissLoadingScreen();
            } else {
                dismissLoadingScreen();
                return false;
            }
        } else {
            dismissLoadingScreen();
        }
    }
}

// event

function onClickBtnNextfrmMBNewTnCLoan() {
 if (gblMBNewTncFlow == TNC_FLOW_TMB_APPLY_LOAN) {
      onclickTnCLoanBtnNext();
    }else if(gblMBNewTncFlow === "eKYC_OPEN_ACCT"){
       btnTnCNexteKYC();
    }else if(gblMBNewTncFlow === "TMB_EKYC_IDP_FLOW"){
       frmMBNewTncApplyEkycIdpClickBtnNext();
    }
}

function onClickBtnBackfrmMBNewTnCLoan() {
    if (gblMBNewTncFlow == TNC_FLOW_TMB_APPLY_LOAN) {
         onclickTnCLoanBtnBack();
    }else if(gblMBNewTncFlow === "eKYC_OPEN_ACCT"){
       btnTnCBackeKYC();
    }else if(gblMBNewTncFlow === "TMB_EKYC_IDP_FLOW"){
       lunchSettingIDPPage();
    }
}

function onClickBtnEmailfrmMBNewTnCLoan() {
    popupConfrmDelete.lblheader.text = kony.i18n.getLocalizedString("TncInfoHeader");
    popupConfrmDelete.btnpopConfDelete.text = kony.i18n.getLocalizedString("keyOK");
    popupConfrmDelete.btnPopupConfCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
    popupConfrmDelete.btnpopConfDelete.onClick = onClickPopUpOkBtnEmailfrmMBNewTnCLoan;
    popupConfrmDelete.lblPopupConfText.text = kony.i18n.getLocalizedString("keytermOpenAcnt");
    popupConfrmDelete.show();
}

function onClickPopUpOkBtnEmailfrmMBNewTnCLoan() {
    popupConfrmDelete.dismiss();
   if (gblMBNewTncFlow == TNC_FLOW_TMB_APPLY_LOAN) {
     if(gblLoanAppType =="27"){
       	 callServiceTCEMailServiceLoan("TMBPersonalLoan");
        }else {
          callServiceTCEMailServiceLoan("TMBCreditLoan");
        }
        
    }
}

function onClickBtnPdffrmMBNewTnCLoan() {
   if (gblMBNewTncFlow == TNC_FLOW_TMB_APPLY_LOAN) {
      if(gblLoanAppType =="27"){
       	 onClickDownloadTnC("pdf", "TMBPersonalLoan");
        }else {
          onClickDownloadTnC("pdf", "TMBCreditLoan");
        }
    }
}

function onClickBtnRightfrmMBNewTnCLoan() {
	if (frmMBNewTnCLoan.flxSaveCamEmail.isVisible == false) {
		frmMBNewTnCLoan.flxSaveCamEmail.setVisibility(true);
		frmMBNewTnCLoan.flexBodyScroll.minHeight = "87%";
		frmMBNewTnCLoan.btnRight.skin="btnShareNewOpenAc";
	}else{
		frmMBNewTnCLoan.flxSaveCamEmail.setVisibility(false);
		frmMBNewTnCLoan.flexBodyScroll.minHeight = "100%";
		frmMBNewTnCLoan.btnRight.skin="btnRightShare"
	}
}

/*frmMBNewTnC.kl*/
function initfrmMBNewTnCLoan(){
  frmMBNewTnCLoan.preShow=frmMBNewTnCLoanMenuPreshow;
  frmMBNewTnCLoan.postShow=frmMBNewTnCLoanMenuPostshow;
  frmMBNewTnCLoan.onDeviceBack=onDeviceSpaback;
  
}

function frmMBNewTnCLoanMenuPreshow() {
	changeStatusBarColor();
    if (gblCallPrePost) {
        frmMBNewTnCLoan.lblDescSubTitle.text = kony.i18n.getLocalizedString("keyTermsNConditions");
        frmMBNewTnCLoan.btnCancel.text = kony.i18n.getLocalizedString("MF_RDM_33");
        frmMBNewTnCLoan.btnNext.text = kony.i18n.getLocalizedString("keyAgreeButton"); //MF_RDM_34
        frmMBNewTnCLoan.flxSaveCamEmail.setVisibility(false);
        frmMBNewTnCLoan.btnRight.onClick =onClickBtnRightfrmMBNewTnCLoan;
      	frmMBNewTnCLoan.flexEmail.onTouchEnd=onClickBtnEmailfrmMBNewTnCLoan;
      	frmMBNewTnCLoan.flexPdf.onTouchEnd =onClickBtnPdffrmMBNewTnCLoan;
        frmMBNewTnCLoan.btnMainMenu.onClick =onClickBtnBackfrmMBNewTnCLoan;
        
       	frmMBNewTnCLoan.btnNext.onClick =onClickBtnNextfrmMBNewTnCLoan;
        frmMBNewTnCLoan.btnCancel.onClick = onClickBtnBackfrmMBNewTnCLoan;
        frmMBNewTnCLoan.flexBodyScroll.minHeight = "100%";
        frmMBNewTnCLoan.btnRight.skin = "btnRightShare";
      	 if (gblMBNewTncFlow == TNC_FLOW_TMB_APPLY_LOAN) {
    	     frmMBNewTnCLoan.lblHdrTxt.text = kony.i18n.getLocalizedString("LoanTC_Title");
        } 
        
        //frmMBNewTnCLoan.flexBodyScroll.setContentOffset({"x":"0dp", "y":"0dp"});
    }
}

function navigateBackToSubProducts(){
  gblconsentApproved=false;
  //frmLoanProductList.show();
  navigateBackToSubProductsRefreshData();
}

function navigateBackToSubProductsRefreshData(){
  		kony.print("gblSelectedLoanProduct--->"+gblSelectedLoanProduct);
      if (gblSelectedLoanProduct == "VP" || gblSelectedLoanProduct == "MS" || gblSelectedLoanProduct == "VM" || gblSelectedLoanProduct == "VT") { //  credit card
    	 onClickCreditCards();
    }else if(gblSelectedLoanProduct == "RC01" || gblSelectedLoanProduct == "C2G01"){ // personal Loan
   		 onClickPersonalLoan();
    }
}


function frmMBNewTnCLoanMenuPostshow() {
    if (gblCallPrePost) {

    }
    assignGlobalForMenuPostshow();
   //frmMBNewTnCLoan.flexBodyScroll.setContentOffset({"x":"0dp", "y":"0dp"});
}
