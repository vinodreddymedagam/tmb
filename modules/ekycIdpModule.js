function loadEKYCIDPLandingPage(){
  showLoadingScreen();
  var inputParams ={};
  inputParams["moduleName"] = "getAutRequestSettings";
  gblRTPDetailsSortBy = 1;
  invokeServiceSecureAsync("IDPApprovalFlow", inputParams, loadIDPApprovalFlowCallBack);
}
function loadIDPApprovalFlowCallBack(status, resulttable){
   if (status == 400) {
     if(resulttable["opstatus"] == 0){
       gblIDVerificationFlag = resulttable["ekycOpenAcctConsent"];
       gblEykcFlag = resulttable["ekycFlag"];
       gblCurrentIAL = resulttable["iAlLevel"];
       gblPartyStatusCode = resulttable["partyStatusCode"];
       gblUpdateIAlCount = parseFloat(resulttable["getNoUpdateIAl"]);
       if(gblIDVerificationFlag != "Y"){
         frmEKYCIdpNoActive.show();
       }else{
         gblIdpList = resulttable["EKYCIDPConsents"];
         frmEKYCIdpDetailsList.show();
       }
     }else{
       showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
     }
   }else{
     showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
   }
  dismissLoadingScreen();
}
function frmEKYCIdpNoActive_init_action(){
  frmEKYCIdpNoActive.btnGotIt.onClick = showAccountSummaryFromMenu;
  frmEKYCIdpNoActive.btnMainMenu.onClick = handleMenuBtn;
  frmEKYCIdpNoActive.btnSetting.onClick = frmEKYCIdpNoActive_btnSetting_function;
  frmEKYCIdpNoActive.onDeviceBack = disableBackButton;
  frmEKYCIdpNoActive.preShow = frmEKYCIdpNoActive_preShow_function;
  frmEKYCIdpNoActive.postShow = frmEKYCIdpNoActive_postShow_function;
}
function frmEKYCIdpNoActive_preShow_function(){
  showLoadingScreen();
  frmEKYCIdpNoActive.lblHeader.text = kony.i18n.getLocalizedString("eKYC_titleIdPConsent");
  frmEKYCIdpNoActive.lblTitle.text = kony.i18n.getLocalizedString("eKYC_msgOnOffIdPconsent");
  frmEKYCIdpNoActive.lblDesc.text = kony.i18n.getLocalizedString("eKYC_msgIdPConsentDetails");
  frmEKYCIdpNoActive.lblSettingDesc.text = kony.i18n.getLocalizedString("eKYC_msgHowToSettingIdPConsent");
  frmEKYCIdpNoActive.btnGotIt.text = kony.i18n.getLocalizedString("eKYC_btnGoHome");
}
function frmEKYCIdpNoActive_postShow_function(){
  rotateImages(frmEKYCIdpNoActive.flexArrow,45);
  dismissLoadingScreen();
}
function frmEKYCIdpNoActive_btnSetting_function(){
  lunchSettingIDPPage();
}