function frmCallmeNow_init_Action(){
  frmCallmeNow.btnMenuIcon.onClick = onBackfrmLoanApplicationStatus;
  frmCallmeNow.preShow = frmCallmeNow_PreShow_Action;
  frmCallmeNow.postShow = frmCallmeNow_PostShow_Action;
  frmCallmeNow.onDeviceBack = disableBackButton;
  frmCallmeNow.btnCancel.onClick = onBackfrmLoanApplicationStatus;
  frmCallmeNow.callMeNowBrowser.onFailure = frmCallmeNow_CallMeNowBrowser_OnFailure_Action;
  frmCallmeNow.callMeNowBrowser.onSuccess = frmCallmeNow_CallMeNowBrowser_OnSuccess_Action;
}

function frmCallmeNow_PreShow_Action(){
  frmCallmeNow.btnCancel.text = kony.i18n.getLocalizedString("CAV06_btnCancel");
  lunchCallMeNow();
}

function frmCallmeNow_PostShow_Action(){

}

function frmCallmeNow_CallMeNowBrowser_OnFailure_Action(){
  dismissLoadingScreen();
}

function frmCallmeNow_CallMeNowBrowser_OnSuccess_Action(){
  dismissLoadingScreen();
}

function lunchCallMeNow(){
  //var url = "https://uattmbbank.tau2904.com/dev/tmb/mib";
     var url = gblCallMeNowUrl;
  var current_locale = kony.i18n.getCurrentLocale();
  var locale = "EN";
  var customername = "";
  var name = "";
  var surname = "";
  var productService = gblproductService;
  if (current_locale == "en_US") {
    locale = "EN";
    customername = gblCustomerName.split(" ");
    name = customername[0];
    surname = customername[customername.length - 1];
  } else {
    locale = "TH";
    customername = gblCustomerNameTh.split(" ");
    name = MyBase64.encode(customername[0]);
    surname = MyBase64.encode(customername[customername.length - 1]);
  }
  var payload = {
    "lang": locale,
    "product": productService,
    "first_name": name,
    "last_name": surname,
    "mobile": gblPHONENUMBER
  };
  var headersConf = {
    "Content-Type": "application/x-www-form-urlencoded",
    "Touch-App": "DropLeadForm",
    "TOUCH-Ref1": encryptPayload(JSON.stringify(payload))
  };
  kony.print("Bee Before request: "+JSON.stringify(payload));
  kony.print("Bee Encrypt request: "+JSON.stringify(encryptPayload(JSON.stringify(payload))));
  frmCallmeNow.callMeNowBrowser.requestURLConfig = {
    URL: url,
    requestMethod: constants.BROWSER_REQUEST_METHOD_GET,
    headers: headersConf
  };
}

function openCallMeNowPage(productType,isTopbrass){
  if(productType == "PL"){
    frmCallmeNow.lblHdrTxt.text = kony.i18n.getLocalizedString("Loan_LTitle");
  }else{
    frmCallmeNow.lblHdrTxt.text = kony.i18n.getLocalizedString("Loan_CCTitle");
  }
  if(isTopbrass){
    gblproductService = "mib-dl-cctopbrass";
    frmCallmeNow.btnCancel.onClick = navigateToLoanProductList;
    frmCallmeNow.btnMenuIcon.onClick = navigateToLoanProductList;
  }else{
    gblproductService = "mib-dl-ccother";
    frmCallmeNow.btnCancel.onClick = onBackfrmLoanApplicationStatus;
    frmCallmeNow.btnMenuIcon.onClick = onBackfrmLoanApplicationStatus;
  }
  showLoadingScreen();
  frmCallmeNow.show();
}
function navigateToLoanProductList(){
  getLoanSubmissionData(gblLoanAppType);
}