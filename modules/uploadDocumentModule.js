function frmUploadManyFile_Init_Action(){
  frmUploadManyFile.preShow = frmUploadManyFile_PreShow_Action;
  frmUploadManyFile.postShow = frmUploadManyFile_PostShow_Action;
  frmUploadManyFile.btnBackUploadDocuments.onClick = setStoreImage;
  frmUploadManyFile.onDeviceBack = disableBackButton;
  frmUploadManyFile.btnPopUpClose.onClick = frmUploadManyFile_btnPopUpClose_onClick;
  frmUploadManyFile.cameraObj.onCapture = cameraCheckBeforeUploadDocument;
  frmUploadManyFile.cameraObj.onTouchStart = activeCaptureCamera;
  frmUploadManyFile.cameraObj.onTouchEnd = InActiveCaptureCamera;
}

function activeCaptureCamera(){
  frmUploadManyFile.flxNewDocument.skin = "flexGreyBGLoan";
}

function InActiveCaptureCamera(){
  frmUploadManyFile.flxNewDocument.skin = "flexWhiteBG";
}

function frmUploadManyFile_PreShow_Action(){
  showLoadingScreen();
  frmUploadManyFile.flxProgress.setVisibility(false);
  frmUploadManyFile.lblFileName.text = "";
  frmUploadManyFile.imgSelectedFile.src = "uploadalbum.png";
  frmUploadManyFile.lblDocumentNew.text = kony.i18n.getLocalizedString("AddNewFile");
  frmUploadManyFile.btnPopUpClose.setVisibility(true);
  frmUploadManyFile.lblUploadDocuments.text = kony.i18n.getLocalizedString("UploadTitle");
}

function frmUploadManyFile_PostShow_Action(){
  dismissLoadingScreen();
}
function frmUploadManyFile_PopupClose_Action(){
  popDelImageUpload.dismiss();
}  
function frmUploadManyFile_PopupConfirm_Action(){
  deleteImageService();
}  
function showConfirmDialogBox(){
  popDelImageUpload.lblDeleteConfirmation.text = kony.i18n.getLocalizedString("ConfirmDeleteFile");
  popDelImageUpload.btnPopDeleteCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
  popDelImageUpload.btnPopDeleteYEs.text = kony.i18n.getLocalizedString("keyYes");
  popDelImageUpload.show();
}
function frmUploadManyFile_btnPopUpClose_onClick(){
  frmUploadManyFile.flxProgress.setVisibility(false);
  resetView();
}
function opencameraForUploadDocument(){
  try {
    gblEditPhotoSource = "png";
    resetView();
    frmUploadManyFile.flxProgress.setVisibility(true);
    var maximumUploadSize = 960;
    var imgBase64 = "";
    var rawBytesString = frmUploadManyFile.cameraObj.rawBytes;
    if (rawBytesString == null) {
      alert(kony.i18n.getLocalizedString("KeyImageNotSelected"));
      return;
    }
    gbluploadObjTmp = {};
    gbluploadObjTmp.fileName = "01" + "_" + getDocumentFormatName() + "_" + gblLoanAppRefNo + "_" + gblContentLoanUpload;
    gbluploadObjTmp.rawbytes = rawBytesString;
    imgBase64 = kony.convertToBase64(rawBytesString);
    if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPod touch") {
      imgBase64 = imgBase64.replace(/[\n\r\s\f\t\v]+/g, '');
      callUploadService(imgBase64);
    } else {
      var base64ImageScale = "";
      imgBase64 = ImageConverter.compressImage(imgBase64,1,30);
      imgBase64 = imgBase64.replace(/[\n\r\s\f\t\v]+/g, '');
      callUploadService(imgBase64);
    }
  } catch (err) {
    alert(kony.i18n.getLocalizedString("KeyMediaGalleryError"));
  }
}
// function openGalleryForUploadDocument() {
//   gblEditPhotoSource = "png";
// //   gblUploadDocumentList = frmUploadManyFile.uploadList.selectedRowItems[0];
//   try {
//     function onselectioncallback(rawbytes) {
//       if (rawbytes == null) {
//         alert(kony.i18n.getLocalizedString("KeyImageNotSelected"));
//         return;
//       }
//       var capturedImg = kony.image.createImage(rawbytes);
//       kony.print("Upload:) imageWidth" + capturedImg.getImageWidth());
//       kony.print("Upload:) imageHeight" + capturedImg.getImageHeight());
//       capturedImg.compress(0.7);
//       var capturedBytes  = capturedImg.getImageAsRawBytes(); 
//       var highSize = capturedImg.getImageWidth()> capturedImg.getImageHeight()?
// 					capturedImg.getImageWidth() : capturedImg.getImageHeight();
//       if(highSize >=2048){ //Maximum should not greater than 2048px
//         var scaleRatio = highSize/20480;
//         capturedImg.scale(scaleRatio);
//       }
//       capturedImg.compress(0.3);
//       var capturedBytesThumbnail  = capturedImg.getImageAsRawBytes(); 
//       gbluploadObjTmp = {};
//       var imgBase64 = "";
//       var imgBase64Thumbnail = "";
//       imgBase64 = kony.convertToBase64(capturedBytes);
//       imgBase64Thumbnail = kony.convertToBase64(capturedBytesThumbnail);
//       var curSize = (imgBase64.length - 814)/1.37;

//       if (curSize > 5242880) {
//         alert("" + kony.i18n.getLocalizedString("UploadError"));
//         return;
//       }
//       gbluploadObjTmp.fileName = "01" + "_" + getDocumentFormatName() + "_" + gblLoanAppRefNo + "_" + gblContentLoanUpload;
//       imgBase64 = imgBase64.replace(/[\n\r\s\f\t\v]+/g, '');
//       gbluploadObjTmp.imageBase64 = imgBase64Thumbnail;
//       callUploadService(imgBase64);
//     }
//     var querycontext = {
//       mimetype: "image/*"
//     };
//     returnStatus = kony.phone.openMediaGallery(onselectioncallback, querycontext);
//   } catch (err) {
//     alert(kony.i18n.getLocalizedString("KeyMediaGalleryError"));
//   }
// }
function resetView(){
  frmUploadManyFile.flxContainerBlue.left = "-100%";
  frmUploadManyFile.flxLoading.setVisibility(false);
  frmUploadManyFile.imgSelectedFile.setVisibility(false);
}
function callUploadService(imgBase64){
  showLoadingScreen();
  resetView();
  frmUploadManyFile.flxContainerBlue.left = "-100%";
  frmUploadManyFile.flxProgress.setVisibility(true);
  frmUploadManyFile.lblFileName.text = gbluploadObjTmp.fileName;
  frmUploadManyFile.flxLoading.setVisibility(true);
  frmUploadManyFile.imgSelectedFile.setVisibility(false);
  var inputParam = {};
  inputParam["actionId"] = "preSave";  // "Save" -> Submit All Documents
  inputParam["actionType"] = "upload";
  inputParam["imageType"] = "PNG";
  inputParam["appRefNo"] = gblLoanAppRefNo;
  inputParam["fileName"] = gbluploadObjTmp.fileName;
  inputParam["baseImg"] = imgBase64;
  invokeServiceSecureAsync("ImageFTPService", inputParam, uploadDocumentsMBCallback);
  frmUploadManyFile.flxContainerBlue.animate(
    kony.ui.createAnimation({"100":{"stepConfig":{"timingFunction":kony.anim.EASE}, "left":"-5%"}}),
    {"delay":0.1,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":2},
    {"animationEnd":disableBackButton }
  );
}

function uploadDocumentsMBCallback(status, callBackResponse) {
  dismissLoadingScreen();
  frmUploadManyFile.btnPopUpClose.setVisibility(true);
  if (status == 400) {
    gblUploadDocumentList.push(gbluploadObjTmp);
    gbluploadObjTmp = {};
    frmUploadManyFile.flxProgress.setVisibility(false);
    loadSegmentUploadByType();
  } else {
    gbluploadObjTmp = {};
    showAlertWithCallBack(kony.i18n.getLocalizedString("UploadError"), kony.i18n.getLocalizedString("info"), loadSegmentUploadByType);
  }
}
function showUploadDocumentByType(){
  gblContentLoanUpload = "";
  gblContentListIndex = 0;
  gblUploadDocumentList = [];
  var selectedIndex = frmUploadDocuments.uploadList.selectedIndex[1];
  var contentID = gblUploadCheckList[selectedIndex].documentDescriptionEN;
  var contentIndex = gblUploadCheckList[selectedIndex].refEntryCode;
  frmUploadManyFile.lblDocumentName.text = contentID;
  gblContentLoanUpload = contentIndex;
  gblContentListIndex = selectedIndex;
  getStoreImage();
  loadSegmentUploadByType();
  frmUploadManyFile.show();
}
function loadSegmentUploadByType(){
  try{
    frmUploadManyFile.uploadList.removeAll();
    var uploadCheckListData = [];
    for(var j = 0; j < gblUploadDocumentList.length ; j++){
      var uploadObj = {};
      var uploadCheckListRow = gblUploadDocumentList[j];
      var imageLogo = uploadCheckListRow.rawbytes;
      var buttonAction = "delete_icon.png";
      var uploadName= uploadCheckListRow.fileName;
      kony.print(uploadName.length);
      var buttonAction = "delete_icon.png";
      uploadObj = {
        "imgUploadLogo": {"rawBytes":imageLogo},
        "lblDocumentName":uploadName,
        "lblSegRowLine":"------",
        "imgUpld":buttonAction,
        "checkMask":{"isVisible":true},
        "flxViewClick":{"onClick":previewImageUploadFile},
        "flxAction":{"onClick":eventHandleButtonAddOrDelete}
      };
      uploadCheckListData.push(uploadObj);
    }
    frmUploadManyFile.uploadList.widgetDataMap = {imgUploadLogo: "imgUploadLogo", lblDocumentName: "lblDocumentName", 
                                                  lblSegRowLine: "lblSegRowLine", imgUpld: "imgUpld",
                                                  checkMask:"checkMask",flxViewClick:"flxViewClick",
                                                  flxAction:"flxAction",imgUploadLogoNew:"imgUploadLogoNew",
                                                  lblDocumentNew:"lblDocumentNew",imgUpNew:"imgUpNew",
                                                  flxNewDocument:"flxNewDocument"};
    frmUploadManyFile.uploadList.setData(uploadCheckListData);
  }catch(e){
    kony.print("Exception : "+e);
  }
}
function previewImageUploadFile(a,b){
  var selectItem = frmUploadManyFile.uploadList.selectedRowItems[0];
  resetView();
  frmUploadManyFile.flxProgress.setVisibility(true);
  frmUploadManyFile.btnPopUpClose.setVisibility(true);
  frmUploadManyFile.flxLoading.setVisibility(false);
  frmUploadManyFile.imgSelectedFile.setVisibility(true);
  frmUploadManyFile.imgSelectedFile.rawBytes = selectItem.imgUploadLogo.rawBytes;
}
function eventHandleButtonAddOrDelete(){
  var selectItem = frmUploadManyFile.uploadList.selectedRowItems[0];
  if(selectItem.imgUpld == "icon_plus.png"){
    openGalleryForUploadDocument();
  }else{
    showConfirmDialogBox();
  }
}
function deleteImageService(){
  showLoadingScreen();
  var selectItem = frmUploadManyFile.uploadList.selectedRowItems[0];
  var fileName = selectItem.lblDocumentName;
  var inputParam = {};
  inputParam["actionId"] = "preSave";
  inputParam["actionType"] = "delete";
  inputParam["appRefNo"] = gblLoanAppRefNo;
  inputParam["fileName"] = fileName;
  invokeServiceSecureAsync("ImageFTPService", inputParam, uploadDocumentsDeleteMBCallback);
}
function uploadDocumentsDeleteMBCallback(status, callBackResponse){
  dismissLoadingScreen();
  popDelImageUpload.dismiss();
  if (status == 400) {
    var imageIndex = frmUploadManyFile.uploadList.selectedIndex[1];
    gblUploadDocumentList.splice(imageIndex, 1);
    loadSegmentUploadByType();
  } else {
    showAlertWithCallBack(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"), loadSegmentUploadByType);
  }
}
function createAddList(){
  var lblDocumentName = "";
  if("th_TH" == kony.i18n.getCurrentLocale()){
    lblDocumentName= "เพิ่มเอกสารใหม่";
  }else{
    lblDocumentName = "Add New";
  }
  var obj = {
    "imgUploadLogo": "icon_picture.png",
    "lblDocumentName":lblDocumentName,
    "lblSegRowLine":"------",
    "imgUpld":"icon_plus.png"
  };
  return obj;
}

function getDocumentFormatName(){
  var dateFormatName = "";
  var d = new Date();
  dateFormatName = "" +pad(d.getFullYear().toString().substr(2,2))+pad(d.getMonth()+1) +pad(d.getDate()) + pad(d.getHours()) +pad(d.getMinutes()) + pad(d.getSeconds());
  return dateFormatName;
}

function getStoreImage(){
  if(typeof(gblUploadCheckList[gblContentListIndex]) != "undefined"){
    if(typeof(gblUploadCheckList[gblContentListIndex].imageDataSet[gblContentLoanUpload]) != "undefined"){
      gblUploadDocumentList =  gblUploadCheckList[gblContentListIndex].imageDataSet[gblContentLoanUpload];
    }
  }
}
function setStoreImage(){
  gblUploadCheckList[gblContentListIndex].imageDataSet[gblContentLoanUpload] = gblUploadDocumentList;
  gblUploadDocumentList = [];
  returnToUploadList();
}

function pad(n) {
  return (n < 10) ? ("0" + n) : n;
}
function cameraCheckBeforeUploadDocument(){
  var options = {isAccessModeAlways:true};
  var result = kony.application.checkPermission(kony.os.RESOURCE_CAMERA,options);
  if(result.status == kony.application.PERMISSION_DENIED){ //Indicates permission denied
    if(result.canRequestPermission){
      kony.application.requestPermission(resourceConfig, permissionStatusCallback);
    }
    else{
      var basicConfig = {
        alertType : constants.ALERT_TYPE_CONFIRMATION,
        message : kony.i18n.getLocalizedString("MB_BPErr_NotAllow"),
        alertHandler : permissionAlertHandler
      }
      kony.ui.Alert(basicConfig);
    }
  }
  else if(result.status == kony.application.PERMISSION_GRANTED){
    opencameraForUploadDocument();
  }
  else if(result.status == kony.application.PERMISSION_RESTRICTED){
    alert(kony.i18n.getLocalizedString("MB_BPErr_NotAllow"));
  }
  function permissionAlertHandler(resp){
    if(resp)
      kony.application.openApplicationSettings();
  }
  function permissionStatusCallback(response){
    if(response.status == kony.os.PERMISSION_GRANTED){
      opencameraForUploadDocument();
    }
    else if(response.status == kony.os.PERMISSION_DENIED){
      var basicConfig = {
        alertType : constants.ALERT_TYPE_CONFIRMATION,
        message : kony.i18n.getLocalizedString("MB_BPErr_NotAllow"),
        alertHandler : permissionAlertHandler
      }
      kony.ui.Alert(basicConfig);
    }
  }
}


