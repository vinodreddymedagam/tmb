var unavailable_types_IB = {
  UNKNOWN: "IB_UNKNOWN",
  MAINTENANCE: "IB_MAINTENANCE",
  SERVER_DOWN: "IB_SERVER_DOWN"
};

gblUnavailableTypeIB = null;
gblUnavailableJsonDataIB = null;
isTimeoutCheckServerDownIB = false;
isTimeoutCheckMaintenanceIB = false;
doneCheckIBUnavailable = false;

function checkIBUnavailable(showFlag) {
  kony.print("#### checkIBUnavailable");
  showLoadingScreenPopup();
  resetCheckIBUnavailable();
  gblUnavailableTypeIB = unavailable_types_IB.UNKNOWN;
  doneCheckIBUnavailable = false;
  checkIBServiceDown();
  if (isNotBlank(showFlag) && showFlag) {
    frmIBUnavailable.show();  
  }
}

function checkIBServiceDown() {
  if (!doneCheckIBUnavailable) {
	
	cancelCheckIBServiceDownID();
	
	kony.print("#### checkIBServiceDown");
    kony.timer.schedule("checkIBServiceDownID", timeoutCheckIBServiceDown, gblTimeoutServerDownInSecond, false);

    var inputparams = {};

    //checking service down?
    invokeServiceSecureAsync("checkConnection", inputparams, function(checkConnectionStatus, checkConnectionResulttable) {
      if (!doneCheckIBUnavailable) {
        if (checkConnectionStatus === 400 && checkConnectionResulttable.opstatus === 0) {
          doneCheckIBUnavailable = true;  
          resetCheckIBUnavailable();
          gblUnavailableTypeIB = null;
          kony.print("#### checkConnection resulttable > " + JSON.stringify(checkConnectionResulttable));
          showLoadingScreenPopup();
          loadResourceBundleIB(function(loadResourceBundleIBStatus, loadResourceBundleIBGetContentUpdate) {
            getIBStartupForm().show();
            callbackgetphrases(loadResourceBundleIBStatus, loadResourceBundleIBGetContentUpdate);
            gblCalledi18nFlagIB = true;
            dismissLoadingScreenPopup();
          });
        } else {
          checkIBMaintenance();
        }
      }
    });
  }  
}

function checkIBMaintenance() {
  if (!doneCheckIBUnavailable)   {
    
    cancelCheckIBServiceDownID();
	cancelCheckIBMaintenanceID();
	
	kony.print("#### checkIBMaintenance");
    kony.timer.schedule("checkIBMaintenanceID", timeoutCheckIBMaintenance, gblTimeoutMaintenanceInSecond, false);

    var maintenance_url = "https://" + appConfig.serverIp + gblMaintenanceURI;
    var request = new kony.net.HttpRequest();
    request.onReadyStateChange = checkIBMaintenanceStateChange;
    request.timeout = parseInt(gblTimeoutMaintenanceInSecond * 1000);
    kony.print("#### checkIBMaintenance URL =>" + maintenance_url);
    request.open(constants.HTTP_METHOD_POST, maintenance_url, true);
    request.setRequestHeader("Content-Type", "application/json");
    var postdata = {};
    request.send(postdata);
  }  
}

function checkIBMaintenanceStateChange() {
  if (!doneCheckIBUnavailable)   {  
    if (this.readyState == constants.HTTP_READY_STATE_UNSENT) {
      kony.print("#### HTTP_READY_STATE_UNSENT " + new Date());
    } else if (this.readyState == constants.HTTP_READY_STATE_OPENED) {
      kony.print("#### HTTP_READY_STATE_OPENED " + new Date());
    } else if (this.readyState == constants.HTTP_READY_STATE_HEADERS_RECEIVED) {
      kony.print("#### HTTP_READY_STATE_HEADERS_RECEIVED " + new Date());
    } else if (this.readyState == constants.HTTP_READY_STATE_LOADING) {
      kony.print("#### HTTP_READY_STATE_LOADING " + new Date());
    } else if (this.readyState == constants.HTTP_READY_STATE_DONE) {
      kony.print("#### HTTP_READY_STATE_DONE " + new Date());
      if (this.status == 200 && this.response) {
        if (typeof(this.response) === 'string') {
          try {
            gblUnavailableJsonDataIB = JSON.parse(this.response);
          }catch(e){
            kony.print("#### Error JSON.parse " + e.stack);
            gblUnavailableJsonDataIB = null;
          }
        }else{
          gblUnavailableJsonDataIB = this.response;
        }
        kony.print("#### jsonResponse = " + JSON.stringify(gblUnavailableJsonDataIB));
      } else {
        kony.print("#### request.status = " + this.status);
        gblUnavailableJsonDataIB = null;
      }
      showIBUnavailable();
    }
  }  
}

function showIBUnavailable() {
  doneCheckIBUnavailable = true;
  cancelCheckIBServiceDownID();
  cancelCheckIBMaintenanceID();  
  if (isNotBlank(gblUnavailableJsonDataIB) && "Y" === gblUnavailableJsonDataIB.maintenance_flag) {
    gblUnavailableTypeIB = unavailable_types_IB.MAINTENANCE;
  }else if (isNotBlank(gblUnavailableJsonDataIB) && "N" === gblUnavailableJsonDataIB.maintenance_flag) {
    gblUnavailableTypeIB = unavailable_types_IB.SERVER_DOWN;
  } else {
    //TODO: should handle this case
    gblUnavailableTypeIB = unavailable_types_IB.SERVER_DOWN;
  }
  frmIBUnavailable.show();
  dismissLoadingScreenPopup();
}

function timeoutCheckIBServiceDown() {
  kony.print("#### timeoutCheckIBServiceDown");
  isTimeoutCheckServerDownIB = true;
  checkIBMaintenance();
}

function timeoutCheckIBMaintenance() {
  kony.print("#### timeoutCheckIBMaintenance");
  isTimeoutCheckMaintenanceIB = true;
  showIBUnavailable();
}

function getIBStartupForm() {
  var startupForm = frmIBPreLogin;
  if (isNotBlank(gblUnavailableTypeIB)) {
    startupForm = frmIBUnavailable;
  } else {
    startupForm = frmIBPreLogin;
  }
  return startupForm;
}

function frmIBUnavailableInit() {
  frmIBUnavailable.preShow = frmIBUnavailablePreShow;
  frmIBUnavailable.postShow = frmIBUnavailablePostShow;
  frmIBUnavailable.flxMain.onClick = frmIBUnavailableflxMainOnClick;
}

function frmIBUnavailablePreShow() {
  kony.print("#### frmIBUnavailablePreShow gblUnavailableTypeIB = " + gblUnavailableTypeIB);

  if (!isNotBlank(gblUnavailableTypeIB) || gblUnavailableTypeIB == unavailable_types_IB.UNKNOWN) {
    frmIBUnavailable.imgBG.src = "splash.png";
    frmIBUnavailable.imgBG.skin = "noSkinImage";
    frmIBUnavailable.lblUrl.text = "";
  } else if (gblUnavailableTypeIB == unavailable_types_IB.MAINTENANCE) {
    if (isNotBlank(gblUnavailableJsonDataIB) && isNotBlank(gblUnavailableJsonDataIB.IB)) {
      if (isNotBlank(gblUnavailableJsonDataIB.IB.maintenance_image_url)) {
        frmIBUnavailable.imgBG.src = getUnavailableImageSRC(gblUnavailableJsonDataIB.IB.maintenance_image_url);
      }
      if (isNotBlank(gblUnavailableJsonDataIB.IB.maintenance_link_url)) {
        frmIBUnavailable.lblUrl.text = gblUnavailableJsonDataIB.IB.maintenance_link_url;
        frmIBUnavailable.imgBG.skin = "imgCursor";
      }
    }
  } else if (gblUnavailableTypeIB == unavailable_types_IB.SERVER_DOWN) {
    if (isNotBlank(gblUnavailableJsonDataIB) && isNotBlank(gblUnavailableJsonDataIB.IB)) {
      if (isNotBlank(gblUnavailableJsonDataIB.IB.serverdown_image_url)) {
        frmIBUnavailable.imgBG.src = getUnavailableImageSRC(gblUnavailableJsonDataIB.IB.serverdown_image_url);
      }
      if (isNotBlank(gblUnavailableJsonDataIB.IB.serverdown_link_url)) {
        frmIBUnavailable.lblUrl.text = gblUnavailableJsonDataIB.IB.serverdown_link_url;
        frmIBUnavailable.imgBG.skin = "imgCursor";
      }
    }
  }
}

function frmIBUnavailablePostShow() {
  kony.print("frmIBUnavailablePostShow");
  if (isNotBlank(gblUnavailableTypeIB) && gblUnavailableTypeIB == unavailable_types_IB.UNKNOWN){
    showLoadingScreenPopup();
  }else{
    dismissLoadingScreenPopup();
	try{
		//change current url 
		window.history.pushState("", "", "/");
	}catch(e){
	}
  }  
}

function frmIBUnavailableflxMainOnClick() {
  if (isNotBlank(frmIBUnavailable.lblUrl.text)) {
    kony.print("#### frmIBUnavailable openURL " + frmIBUnavailable.lblUrl.text);
    kony.application.openURL(frmIBUnavailable.lblUrl.text);
  } else {
    kony.print("#### frmIBUnavailable.lblUrl.text blank (no event)");
  }
}
function resetCheckIBUnavailable(){
  gblUnavailableJsonDataIB = null;
  isTimeoutCheckServerDownIB = false;
  isTimeoutCheckMaintenanceIB = false;
  cancelCheckIBServiceDownID();
  cancelCheckIBMaintenanceID();
}
function cancelCheckIBServiceDownID(){
  try {
    kony.timer.cancel("checkIBServiceDownID");
  }catch(e){

  }  
}
function cancelCheckIBMaintenanceID(){
  try {
    kony.timer.cancel("checkIBMaintenanceID");
  }catch(e){

  }
}

function isInIBStartupForm(){
  var result = false;
  var currForm = kony.application.getCurrentForm();
  if (isNotBlank(currForm) && isNotBlank(currForm.id) && currForm.id == "frmIBUnavailable") {
    result = true;
  }
  return result;
}