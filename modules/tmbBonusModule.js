var gblMenuSource = "";
function TMBBonusMenuEligibleCheck(){
  if(gblTMBBonusflag != "ON"){
  	frmMenu.segMore.removeAt(1);
  }else{
  	if(gblReferCD == "" || gblReferCD == null){
    	frmMenu.segMore.removeAt(1);
    }
  }
}

function TMBBonusShortcutEligibleCheck(){
//   if(gblTMBBonusflag != "ON"){
//   	frmAccountSummaryLanding.btnTmbBonus.setVisibility(false);
//   }else{
//   	if(gblReferCD != "" && gblReferCD != null){
//     	frmAccountSummaryLanding.btnTmbBonus.setVisibility(true);
//     }else{
//     	frmAccountSummaryLanding.btnTmbBonus.setVisibility(false);
//     }
//   }
}
function callActivityLogForTMBBonus(status){
    var inputParam ={};
	inputParam["tmbBonussource"] = gblMenuSource;
  	inputParam["statusTMBBonus"] = status;
	invokeServiceSecureAsync("tmbBonusService", inputParam, noAction);
}
function noAction(status, callBackResponse){
  return;
}
function setScheduleTimeoutAlert(){
  	GLOBAL_KONY_IDLE_TIMEOUT = "5";
  	registerForTimeOut();
	try{
		kony.timer.cancel("idleTMBBonusCheck");
	}
	catch(e){
	}
	kony.timer.schedule("idleTMBBonusCheck", callBackAlertTimeOut, 240, false)
    invokeServiceSecureAsync("crmProfileInq", {}, noAction);
}

function callBackAlertTimeOut(){
  	var currentForm = kony.application.getCurrentForm().id;
  	if(currentForm == "frmLoyaltylanding"){
      	var KeyTitle = "";//"Sorry" kony.i18n.getLocalizedString("keyOK");
        var keyMsg = kony.i18n.getLocalizedString("MB_PointSessionTimeout"); //ErrAndNoFinPrint
        var keyContinue = kony.i18n.getLocalizedString("Btn_Continue");
        var keyCancel = kony.i18n.getLocalizedString("keyXferBtnCancel");
       //Defining basicConf parameter for alert
        var basicConf = {
            message: keyMsg,
            alertType: constants.ALERT_TYPE_CONFIRMATION,
            alertTitle: KeyTitle,
            yesLabel: keyContinue,
            noLabel: keyCancel,
            alertHandler: callBackShowContinueSession
        };
        //Defining pspConf parameter for alert
        var pspConf = {};
        //Alert definition
        var infoAlert = kony.ui.Alert(basicConf, pspConf);
      	GLOBAL_KONY_IDLE_TIMEOUT = "1";
      	registerForTimeOut();
      	GLOBAL_KONY_IDLE_TIMEOUT = "5";
    }

	return;
}

function callBackAlertLoadFail(){
  	callActivityLogForTMBBonus("02");
  	dismissLoadingScreen();
	var KeyTitle = "";//"Sorry" kony.i18n.getLocalizedString("keyOK");
    var keyMsg = kony.i18n.getLocalizedString("MB_PointNotConnect"); //ErrAndNoFinPrint
    var keyContinue = kony.i18n.getLocalizedString("Btn_Retry");
    var keyCancel = kony.i18n.getLocalizedString("keyXferBtnCancel");
   //Defining basicConf parameter for alert
	var basicConf = {
		message: keyMsg,
		alertType: constants.ALERT_TYPE_CONFIRMATION,
		alertTitle: KeyTitle,
		yesLabel: keyContinue,
		noLabel: keyCancel,
		alertHandler: callBackShowRetryPage
	};
	//Defining pspConf parameter for alert
	var pspConf = {};
	//Alert definition
	var infoAlert = kony.ui.Alert(basicConf, pspConf);
	return;
}
function callBackShowRetryPage(response){
	if(response == true){
		frmLoyaltylanding.loyaltyBrowser.reload();
	}else{
     	showAccountSummaryFromMenu();
    }
}
function callBackShowContinueSession(response){
	if(response == true){
		setScheduleTimeoutAlert();
	}else{
     	onLogoutPopUpConfrm();
    }
}
function onSuccessTMBWoWLoading(){
  	callActivityLogForTMBBonus("01");
	dismissLoadingScreen();
  	frmLoyaltylanding.loyaltyBrowser.onSuccess = noAction;
}


function launchTMBBonus() {
    showLoadingScreen();
    var current_locale = kony.i18n.getCurrentLocale();
    var locale = "EN";
        var refID = gblReferCD;
        if (current_locale == "en_US") {
            locale = "EN";
        } else {
            locale = "TH";
        }
        setScheduleTimeoutAlert();
        var name = gblCustomerNameTh.split(" ")[0]; //kony.convertToBase64(gblCustomerNameTh.split(" ")[0])
        var encodeName = "";
        try {
            encodeName = MyBase64.encode(name);
        } catch (e) {
            encodeName = "";
        }
        kony.print(JSON.stringify({
            "RefID": refID,
            "FirstnameTH": encodeName,
            "FirstnameEN": gblCustomerName.split(" ")[0],
            "Language": locale
        }));
        var payloadObjEnc = encryptPayload(JSON.stringify({
            "RefID": refID,
            "FirstnameTH": encodeName,
            "FirstnameEN": gblCustomerName.split(" ")[0],
            "Language": locale
        }));
        //var payloadObj = JSON.stringify({"RefID":refID,"FirstnameTH":encodeName,"FirstnameEN":gblCustomerName.split(" ")[0],"Language":locale});
        var url = gblTMBBonusURL;
        var headersConf = {
            "Authorization": gblTMBBonusAuthorization,
            "payload": payloadObjEnc,
            "Content-Type": "application/x-www-form-urlencoded"
        };
        frmLoyaltylanding.loyaltyBrowser.requestURLConfig = {
            URL: url,
            requestMethod: constants.BROWSER_REQUEST_METHOD_GET,
            headers: headersConf
        }
}


var MyBase64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(input){var output="";var chr1,chr2,chr3,enc1,enc2,enc3,enc4;var i=0;input=MyBase64._utf8_encode(input);while(i<input.length){chr1=input.charCodeAt(i++);chr2=input.charCodeAt(i++);chr3=input.charCodeAt(i++);enc1=chr1>>2;enc2=((chr1&3)<<4)|(chr2>>4);enc3=((chr2&15)<<2)|(chr3>>6);enc4=chr3&63;if(isNaN(chr2)){enc3=enc4=64;}else if(isNaN(chr3)){enc4=64;}
output=output+this._keyStr.charAt(enc1)+this._keyStr.charAt(enc2)+this._keyStr.charAt(enc3)+this._keyStr.charAt(enc4);}
return output;},decode:function(input){var output="";var chr1,chr2,chr3;var enc1,enc2,enc3,enc4;var i=0;input=input.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(i<input.length){enc1=this._keyStr.indexOf(input.charAt(i++));enc2=this._keyStr.indexOf(input.charAt(i++));enc3=this._keyStr.indexOf(input.charAt(i++));enc4=this._keyStr.indexOf(input.charAt(i++));chr1=(enc1<<2)|(enc2>>4);chr2=((enc2&15)<<4)|(enc3>>2);chr3=((enc3&3)<<6)|enc4;output=output+String.fromCharCode(chr1);if(enc3!=64){output=output+String.fromCharCode(chr2);}
if(enc4!=64){output=output+String.fromCharCode(chr3);}}
output=MyBase64._utf8_decode(output);return output;},_utf8_encode:function(string){string=string.replace(/\r\n/g,"\n");var utftext="";for(var n=0;n<string.length;n++){var c=string.charCodeAt(n);if(c<128){utftext+=String.fromCharCode(c);}
else if((c>127)&&(c<2048)){utftext+=String.fromCharCode((c>>6)|192);utftext+=String.fromCharCode((c&63)|128);}
else{utftext+=String.fromCharCode((c>>12)|224);utftext+=String.fromCharCode(((c>>6)&63)|128);utftext+=String.fromCharCode((c&63)|128);}}
return utftext;},_utf8_decode:function(utftext){var string="";var i=0;var c=c1=c2=0;while(i<utftext.length){c=utftext.charCodeAt(i);if(c<128){string+=String.fromCharCode(c);i++;}
else if((c>191)&&(c<224)){c2=utftext.charCodeAt(i+1);string+=String.fromCharCode(((c&31)<<6)|(c2&63));i+=2;}
else{c2=utftext.charCodeAt(i+1);c3=utftext.charCodeAt(i+2);string+=String.fromCharCode(((c&15)<<12)|((c2&63)<<6)|(c3&63));i+=3;}}
return string;}}

function encryptPayload(plainText) {
  var iv = "e675f725e675f725";
  var salt = "a1b2c3d4e5f61234";
  var iteration_couter = 1023;
  var password = "tmb1234@test";  //"1afc06aa242190566105ab3f971056e3e4d5471b36a4667ea5b8a2eca318959e"
  var encryptText = "";

  var key = CryptoJS.PBKDF2(password, CryptoJS.enc.Utf8.parse(salt), {keySize: 256/32, iterations: iteration_couter });
  encryptText = CryptoJS.AES.encrypt(plainText, key, {iv: CryptoJS.enc.Utf8.parse(iv)}).ciphertext.toString(CryptoJS.enc.Base64);
		kony.print("Encrypted : " + encryptText );

  return encryptText;

}