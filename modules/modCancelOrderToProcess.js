Trnsaction_Type="R";
OrderDateTime="2017/07/20 16:07";
//Type your code here

/**
 * @function
 *
 */
function showMFCancelOrder(eventObject) {

    var index = eventObject.id.split("buttonCancel");
    var selectedindex = parseInt(index[1]);
    var indexOfSelectedIndex = OrderToBeProcessDS[selectedindex];
	frmMFcancelOrderToProcess.flexLoadinGif.setVisibility(false);
  	OrderDateTime=OrderToBeProcessDS[selectedindex]["OrderDate"];
	var orderDate = OrderToBeProcessDS[selectedindex]["OrderDate"].split(" ");
    var date = orderDate[0];
    frmMFcancelOrderToProcess.lblCancelOderHeader.text=kony.i18n.getLocalizedString("key_cancelorder");       
    //alert("selectedindex: "+selectedindex);
    frmMFcancelOrderToProcess.lblkey1.text = kony.i18n.getLocalizedString("MF_lbl_Order_date");
    frmMFcancelOrderToProcess.lblvalue1.text = date;
    frmMFcancelOrderToProcess.lblkey2.text = kony.i18n.getLocalizedString("key_fundCode")+":";
    frmMFcancelOrderToProcess.lblvalue2.text = frmMFFullStatementNFundSummary.lblFundName.text;
	 var locale = kony.i18n.getCurrentLocale();
     var tranTypeHub = "TranTypeHubEN";
     var statusHub = "StatusHubEN";
     var channelHub = "ChannelHubEN";
     if (locale == "th_TH") {
     	tranTypeHub = "TranTypeHubTH";
     	statusHub = "StatusHubTH";
     	channelHub = "ChannelHubTH";
     }
  	if(indexOfSelectedIndex["TranTypeHubEN"]=="BUY"){
      Trnsaction_Type="P";
    }else{
      Trnsaction_Type="R";
    }
    var amount = commaFormatted(OrderToBeProcessDS[selectedindex]["Amount"]);
     if (parseInt(OrderToBeProcessDS[selectedindex]["Amount"]) == 0 && parseInt(OrderToBeProcessDS[selectedindex]["Unit"]) != 0) {
         amount = verifyDisplayUnit(OrderToBeProcessDS[selectedindex]["Unit"]);
    }
    frmMFcancelOrderToProcess.lblkey3.text =  kony.i18n.getLocalizedString("MF_lbl_Amount");
    frmMFcancelOrderToProcess.lblvalue3.text = amount;
    frmMFcancelOrderToProcess.lblkey4.text = kony.i18n.getLocalizedString("MF_lbl_Transaction_type");
    frmMFcancelOrderToProcess.lblvalue4.text = indexOfSelectedIndex[tranTypeHub];


    frmMFcancelOrderToProcess.lblkey5.text = kony.i18n.getLocalizedString("MF_thr_Status")+":";
    frmMFcancelOrderToProcess.lblvalue5.text = indexOfSelectedIndex[statusHub];

    frmMFcancelOrderToProcess.lblkey6.text = kony.i18n.getLocalizedString("MF_lbl_Channel");
    frmMFcancelOrderToProcess.lblvalue6.text =  indexOfSelectedIndex[channelHub];
    frmMFcancelOrderToProcess.lblkey7.text = kony.i18n.getLocalizedString("MF_TransactionRefNo");
    frmMFcancelOrderToProcess.lblvalue7.text = indexOfSelectedIndex.OrderReference;
    frmMFcancelOrderToProcess.btnclose.onClick = closeOderToProcess;
    frmMFcancelOrderToProcess.btnCancelOrder.onClick=onClickCancelOrder;
    frmMFcancelOrderToProcess.btnCancelOrder.text=kony.i18n.getLocalizedString("key_cancelorder");
    frmMFcancelOrderToProcess.show();
    gblCallPrePost = false;
    
    
}

function onClickCancelOrder() {
    if (gblUserLockStatusMB == "03") {
        showTranPwdLockedPopup();
    } else {
        //gblMFOrder = {};
        //gblMFEventFlag = MF_EVENT_REDEEM;
        MBcallCancelValidation();
        //showCancelPwdPopup();
        //gblMFOrder["orderType"] = MF_ORD_TYPE_REDEEM; 
    }
}

function MBcallCancelValidation() {
    var inputParam = {};

    inputParam["unitHolderNo"] = removeHyphenIB(gblUnitHolderNumber); //"110053000033";//
  	inputParam["isInitalLog"] = "Y";
  	inputParam["orderDateTime"] = OrderDateTime;
  	inputParam["orderType"] = Trnsaction_Type;
    kony.print(" in MBcallCancelValidation");
     showLoadingScreen();
   	var platformChannel = gblDeviceInfo.name;
  	if(platformChannel == "thinclient"){
      inputParam["fundHouseCode"] =frmIBMutualFundsPortfolio.segAccountDetails.selectedItems[0].FundHouseCode;// "UOBAMTH";
      inputParam["transactionRef"] = dataOfSelectedIndex.OrderReference;
      invokeServiceSecureAsync("MFCancelOrderCompositeJavaService", inputParam, CancelValidationCallBackIB);
    }else{
      
   	  inputParam["fundHouseCode"] = frmMFSummary.segAccountDetails.selectedItems[0]["fundHouseCode"]; //"70/30-D LTF";
      inputParam["transactionRef"] = frmMFcancelOrderToProcess.lblvalue7.text;
      invokeServiceSecureAsync("MFCancelOrderCompositeJavaService", inputParam, CancelValidationCallBackMB);
    }
}

function CancelValidationCallBackMB(status, resulttable) {
     kony.print(" in call back MBcallCancelValidation");
    if (status == 400) {
    	dismissLoadingScreen();
        if (resulttable["opstatus"] == 0 || resulttable["opstatus"] == "0") {
             kony.print(" in call back success MBcallCancelValidation");
            	
          showCancelPwdPopup();
        }else if(resulttable["opstatus"] == 1005 || resulttable["opstatus"] == "1005"){
          showAlert( kony.i18n.getLocalizedString("MF_Cancel_cuttoff"), kony.i18n.getLocalizedString("info"));
        }else{
          showAlert("status : " + status + " errMsg: " + resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
        }

    }else{
    	dismissLoadingScreen();
    }

}


function showCancelPwdPopup() {
    var lblText = kony.i18n.getLocalizedString("transPasswordSub");
    popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = tbxPopupBlue;
    //dismissLoadingScreen();
   if(gblAuthAccessPin == true){
       kony.print("show Access pin popup");
       showAccesspinPopup();
    }else{
       showOTPPopup(lblText, "", "", onClickCancelConfirmPop, 3);
    }
}

function onClickCancelConfirmPop() {
     
   if (isNotBlank(popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text)) {

        popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("keyTransPwdMsg");
        popupTractPwd.lblPopupTract7.skin = lblPopupLabelTxt;
        popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = tbxPopupBlue;
        popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = tbxPopupBlue;
        popupTractPwd.hbxPopupTranscPwd.skin = tbxPopupBlue;
		//popupTractPwd.dismiss();
		showLoadingScreen();
      	
        callCancelConfirmCompositeService();
        
    } else {
        setTransPwdFailedError(kony.i18n.getLocalizedString("emptyMBTransPwd"));
        return false;
    }
}

function callCancelConfirmCompositeService() {
  var inputParam = {};

  inputParam["unitHolderNo"] = removeHyphenIB(gblUnitHolderNumber); //"110053000033";//

  inputParam["isInitalLog"] = "N";
  inputParam["orderType"] = Trnsaction_Type;
  inputParam["orderDateTime"] = OrderDateTime;

  var platformChannel = gblDeviceInfo.name;
  if(platformChannel == "thinclient"){
    var otpText = popUpCancelorder.txtBxOTP.text;
    if (otpText != null) {
      otpText = otpText.trim();
    } else {
      alert(kony.i18n.getLocalizedString("Receipent_alert_correctOTP"));
    }
    if (otpText == "") {
      alert(kony.i18n.getLocalizedString("Receipent_alert_correctOTP"));
    }else{
      inputParam["password"] =otpText; 
      inputParam["fundHouseCode"] =frmIBMutualFundsPortfolio.segAccountDetails.selectedItems[0].FundHouseCode;// "UOBAMTH";
      inputParam["transactionRef"] = dataOfSelectedIndex.OrderReference;
      kony.application.showLoadingScreen("frmLoading", "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, false, null);
      invokeServiceSecureAsync("MFCancelOrderCompositeJavaService", inputParam, mfCancelOrderCompositeCallBackIB);

    }
  }else{
    inputParam["password"] =  gblNum;  //popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text; 
    inputParam["fundHouseCode"] = frmMFSummary.segAccountDetails.selectedItems[0]["fundHouseCode"]; //"70/30-D LTF";
    inputParam["transactionRef"] = frmMFcancelOrderToProcess.lblvalue7.text;
    kony.application.showLoadingScreen("frmLoading", "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, false, null);
    invokeServiceSecureAsync("MFCancelOrderCompositeJavaService", inputParam, mfCancelOrderCompositeCallBackMB);
  }

}

function onclicktryagain(){
	frmMFFullStatementNFundSummary.show();
	showFullSatement();
}

function callMFTimer() {
    try {
        var platformChannel = gblDeviceInfo.name;
        if (platformChannel == "thinclient") {
            popUpCancelorder.HBoxImage.top = "40%";
            popUpCancelorder.containerheight = "60";
            popUpCancelorder.HBoxImage.setVisibility(true);
            popUpCancelorder.ImgComplete.src = "bloom.gif";
        } else {
            frmMFcancelOrderToProcess.flexLoadinGif.setVisibility(true);
            frmMFcancelOrderToProcess.flxFullBody.setVisibility(false);
            //frmMFcancelOrderToProcess.imagegif.src="bloom.gif";
        }
        kony.timer.schedule("MFCancelTimer", callbackMFTimer, 5, false);

    } catch (e) {
        kony.timer.cancel("MFCancelTimer");
    }
}

function callbackMFTimer() {
    try {
        var platformChannel = gblDeviceInfo.name;

        if (platformChannel == "thinclient") {

            showLoadingScreen();
            callOrderToBeProcessServiceIB();
            popUpCancelorder.HBoxImage.setVisibility(false);
            popUpCancelorder.containerheight = "80";
            popUpCancelorder.dismiss();
            dismissLoadingScreen();

        } else {
            frmMFcancelOrderToProcess.flexLoadinGif.setVisibility(false);
            frmMFcancelOrderToProcess.flxFullBody.setVisibility(true);
            frmMFFullStatementNFundSummary.show();
            showFullSatement();
        }
        kony.timer.cancel("MFCancelTimer");
    } catch (e) {
        kony.timer.cancel("MFCancelTimer");
    }
}

function mfCancelOrderCompositeCallBackMB(status, resulttable) {
    dismissLoadingScreen();
    if (status == 400) {
        if (resulttable["opstatus"] == 0 || resulttable["opstatus"] == "0") {
            kony.print("mfCancelOrderCompositeCallBackMB success");
          	popupTractPwd.dismiss();
            onClickCancelAccessPin();
          	frmMFcancelOrderToProcess.lbltransCancelComplete.text="Transaction Cancelation Completed";
          	callMFTimer();
        } else if (resulttable["errCode"] == "VrfyAcPWDErr00001" || resulttable["errCode"] == "VrfyAcPWDErr00002") {
            popupEnterAccessPin.lblWrongPin.setVisibility(true);
            kony.print("invalid pin"); //To do : set red skin to enter access pin
            resetAccessPinImg(resulttable["badLoginCount"]);
            return false;
        }else if (resulttable["errCode"] == "VrfyAcPWDErr00003") {
            onClickCancelAccessPin();
            gotoUVPINLockedScreenPopUp(); 
            return false;
        } else if (resulttable["errCode"] == "VrfyTxPWDErr00001" || resulttable["errCode"] == "VrfyTxPWDErr00002") {
            setTransPwdFailedError(kony.i18n.getLocalizedString("invalidTxnPwd"));
        }else if (resulttable["errCode"] == "VrfyTxPWDErr00003") {
            showTranPwdLockedPopup();
        }else if(resulttable["opstatus"] == 1005 || resulttable["opstatus"] == "1005"){
          showAlert( kony.i18n.getLocalizedString("MF_Cancel_cuttoff"), kony.i18n.getLocalizedString("info"));
		}else if (resulttable["opstatus"] == 1015 || resulttable["opstatus"] == "1015") {
          	kony.print("mfCancelOrderCompositeCallBackMB failed");
          	popupTractPwd.dismiss();
            onClickCancelAccessPin();
           	frmMFcancelOrderFailed.btnTryAgain.onClick = onclicktryagain;
  			frmMFcancelOrderFailed.btnCallCC.onClick = CallTheNumber;
          	frmMFcancelOrderFailed.ImageTransStatus.src="icon_alert.png";
          	frmMFcancelOrderFailed.lblTransStatus.text=kony.i18n.getLocalizedString("MF_Cancel_Warn");
            frmMFcancelOrderFailed.btnTryAgain.setVisibility(false);

          	frmMFcancelOrderFailed.btnclose.onClick=onclicktryagain;
            frmMFcancelOrderFailed.show();
        } else if (resulttable["opstatus"] == 1001 || resulttable["opstatus"] == "1001") {
          	kony.print("mfCancelOrderCompositeCallBackMB failed");
          	popupTractPwd.dismiss();
            onClickCancelAccessPin();
          	frmMFcancelOrderFailed.btnTryAgain.onClick = onclicktryagain;
  			frmMFcancelOrderFailed.btnCallCC.onClick = CallTheNumber;
          	frmMFcancelOrderFailed.ImageTransStatus.src="icon_notcomplete.png";
          	frmMFcancelOrderFailed.lblTransStatus.text=kony.i18n.getLocalizedString("MF_Cancel_Fail");
            frmMFcancelOrderFailed.btnTryAgain.setVisibility(true);

          	frmMFcancelOrderFailed.btnclose.onClick=onclicktryagain;
            frmMFcancelOrderFailed.show();
        } else if (resulttable["opstatus"] == 8005) {
            dismissLoadingScreen();
            if (resulttable["errCode"] == "VrfyOTPErr00001") {
                gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
                dismissLoadingScreen();
                showAlert("" + kony.i18n.getLocalizedString("invalidOTP"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00002") {
                dismissLoadingScreen();
                showAlert("" + kony.i18n.getLocalizedString("ECVrfyOTPErr"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00005") {
                dismissLoadingScreen();
                showAlert("" + kony.i18n.getLocalizedString("KeyTokenSerialNumError"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00006") {
                dismissLoadingScreen();
                gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
                showAlert("" + resulttable["errMessage"], kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00004") {
                dismissLoadingScreen();
                showAlert("" + resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
                return false;
            } else {
                dismissLoadingScreen();
                showAlert("" + kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                return false;
            }
        }
    } else {
        dismissLoadingScreen();
        popupTractPwd.dismiss();
        onClickCancelAccessPin();
        if (status == 300) {
            showAlert(kony.i18n.getLocalizedString("Receipent_alert_Error"), kony.i18n.getLocalizedString("info"));
        } else {
            showAlert("status : " + status + " errMsg: " + resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
        }
    }
      
} 
/***
 * @function
 *
 */
function closeOderToProcess() {
	frmMFFullStatementNFundSummary.flexFundDetails.setVisibility(false);
	frmMFFullStatementNFundSummary.FlexMonthTab.setVisibility(true);
	frmMFFullStatementNFundSummary.FlexOrderToProcess.setVisibility(true);
    frmMFFullStatementNFundSummary.FlexOrderToProcesssegBg.setVisibility(true);
    //frmMFFullStatementNFundSummary.FlexLoadingFullSatement.setVisibility(false);
    frmMFFullStatementNFundSummary.flexLoading.setVisibility(false);
    frmMFFullStatementNFundSummary.show();
    
}
function callOrderToBeProcessServiceMB() {
    var inputParam = {};
  	inputParam["unitHolderNo"] = removeHyphenIB(gblUnitHolderNumber); //"110053000033";//
    inputParam["fundCode"] = gblFundCode; //"70/30-D LTF";
    inputParam["fundHouseCode"] = frmMFSummary.segAccountDetails.selectedItems[0]["fundHouseCode"];
   kony.application.showLoadingScreen("frmLoading", "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, false, null);
    invokeServiceSecureAsync("MFFundCutoffTimefValidation", inputParam, callOrderToBeProcessServiceCallBackMB);									
    //alert("in start callOrderToBeProcessServiceMB");
   // invokeServiceSecureAsync("MFUHAccountDetailInq", inputParam, callOrderToBeProcessServiceCallBackMB);
}

function callOrderToBeProcessServiceCallBackMB(status, resulttable) {
    frmMFFullStatementNFundSummary.FlexMonthTab.setVisibility(true);
    kony.print("status"+ status);
    viewMFFullStmt(startDate, endDate, "1");
    if (status == 400) {
        if (resulttable["opstatus"] == 0 || resulttable["opstatus"] == "0") {
               // alert("OrderToBeProcessDS "+JSON.stringify(resulttable["OrderToBeProcessDS"]));
                if (resulttable["OrderToBeProcessDS"].length == 0) {
                    frmMFFullStatementNFundSummary.FlexMonthTab.top = "0dp";
                    //setData();
                    frmMFFullStatementNFundSummary.FlexOrderToProcess.setVisibility(false);
                    frmMFFullStatementNFundSummary.FlexOrderToProcesssegBg.setVisibility(false);
                    return;
                } else {
                    frmMFFullStatementNFundSummary.FlexOrderToProcess.setVisibility(true);
                    frmMFFullStatementNFundSummary.FlexOrderToProcesssegBg.setVisibility(true);
                  	var locale = kony.i18n.getCurrentLocale();
                    var nooftransaction = kony.i18n.getLocalizedString("key_ordertoprocess_count");
                    if (locale == "en_US" && (resulttable["OrderToBeProcessDS"].length == 1 || resulttable["OrderToBeProcessDS"].length == "1")) {
                        nooftransaction = replaceAll(nooftransaction, "orders", "order");
                    }
                    nooftransaction = replaceAll(nooftransaction, "{count}", resulttable["OrderToBeProcessDS"].length);
                    frmMFFullStatementNFundSummary.lblOdertoProcess.text = nooftransaction;
                    OrderToBeProcessDS = resulttable["OrderToBeProcessDS"];
                    addrowforOrderToProcess();
                }
			  kony.application.dismissLoadingScreen();
        } else {
            kony.application.dismissLoadingScreen();
            // dismissLoadingScreen();

        }
    } else {
        kony.application.dismissLoadingScreen();
        //dismissLoadingScreen();
    }
}

//Type your code here

/**
 * @function
 *
 */
function addrowforOrderToProcess() {
    try {
        frmMFFullStatementNFundSummary.FlexOrderToProcesssegBg.removeAll();
        index = OrderToBeProcessDS.length;
        kony.print("OrderToBeProcessDS " +JSON.stringify(OrderToBeProcessDS));
        //alert("OrderToBeProcessDS " +JSON.stringify(OrderToBeProcessDS));
        frmMFFullStatementNFundSummary.FlexOrderToProcesssegBg.setVisibility(true);
        //frmMFFullStatementNFundSummary.FlexOrderToProcesssegBg.layoutType = kony.flex.FLOW_VERTICAL;
        for (var i = 0; i < index; i++) {

            var orderDate = OrderToBeProcessDS[i]["OrderDate"].split(" ");
            var date = orderDate[0];
            var time = orderDate[1];
            //var amount=OrderToBeProcessDS[i]["Amount"];
            var amount = commaFormatted(OrderToBeProcessDS[i]["Amount"]);
            if (parseInt(OrderToBeProcessDS[i]["Amount"]) == 0 && parseInt(OrderToBeProcessDS[i]["Unit"]) != 0) {
                amount = verifyDisplayUnit(OrderToBeProcessDS[i]["Unit"]);
            }
			var locale = kony.i18n.getCurrentLocale();
            var tranTypeHub = "TranTypeHubEN";
                if (locale == "th_TH") {
                    tranTypeHub = "TranTypeHubTH";
                }
            var showbtncancel = false;
            if (OrderToBeProcessDS[i]["enbCancel"] == "Y") {
                showbtncancel = true;
            }

            var lblorderDateVal = new kony.ui.Label({
                "id": "lblorderDateVal" + i,
                "width": "21%",
                "height": "36dp",
                "left": "15dp",
                "zIndex": 1,
                "isVisible": true,
                "text": date,
                "skin": "lblBlack18pxRegualr"
            }, {
                "padding": [0, 0, 0, 0],
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "marginInPixel": false,
                "paddingInPixel": false,
                "containerWeight": 0
            }, {
                "textCopyable": false
            });
          	
            var lblTimeVal = new kony.ui.Label({
                "id": "lblTimeVal" + i,
                "width": "12%",
                "height": "36dp",
                "zIndex": 1,
                "isVisible": true,
                "text": time,
                "skin": "lblBlack18pxRegualr"
            }, {
                "padding": [0, 0, 0, 0],
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "marginInPixel": false,
                "paddingInPixel": false,
                "containerWeight": 0
            }, {
                "textCopyable": false
            });
          var lbltransVal = new kony.ui.Label({
                "id": "lbltransVal" + i,
                "width": "12%",
                "height": "36dp",
                "zIndex": 1,
                "isVisible": true,
                "text": OrderToBeProcessDS[i][tranTypeHub],
                "skin": "lblBlack18pxRegualr"
            }, {
                "padding": [0, 0, 0, 0],
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "marginInPixel": false,
                "paddingInPixel": false,
                "containerWeight": 0
            }, {
                "textCopyable": false
            });
            var lblAmountVal = new kony.ui.Label({
                "id": "lblAmountVal" + i,
                "width": "21%",
                "height": "100.0%",
                "zIndex": 1,
                "isVisible": true,
                "text": amount,
                "skin": "lblBlack18pxRegualr"
            }, {
                "padding": [0, 0, 0, 0],
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "marginInPixel": false,
                "paddingInPixel": false,
                "containerWeight": 0
            }, {
                "textCopyable": false
            });
            var buttonCancel = new kony.ui.Button({
                "id": "buttonCancel" + i,
                "top": "0dp",
                "width": "18%",
                "height": "36dp",
                "left": "5dp",
                "zIndex": 1,
                "isVisible": showbtncancel,
                "text": kony.i18n.getLocalizedString("keyCancelButton"),
                "onClick": showMFCancelOrder,
                "skin": "btnBlue160",
                "focusSkin": "btnBlue160"
            }, {
                "padding": [0, 0, 0, 0],
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "displayText": true,
                "marginInPixel": false,
                "paddingInPixel": false,
                "containerWeight": 0
            }, {});
            var FlexmainOrederProcessed = new kony.ui.FlexContainer({
                "id": "FlexmainOrederProcessed" + i,
                "top": "3dp",
                "left": "0dp",
                "width": "100%",
                "height": "36dp",
                //"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "zIndex": 1,
                "isVisible": true,
                "clipBounds": true,
                "Location": "[0,0]",
                "skin": "flexwhite",
                "layoutType": kony.flex.FLOW_HORIZONTAL
            }, {
                "padding": [0, 0, 0, 0]
            }, {});

            var flexLine = new kony.ui.FlexContainer({
                "id": "flexLine" + i,
                "top": "0dp",
                "left": "5dp",
                "width": "86%",
                "height": "1dp",
                //"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "zIndex": 1,
                "isVisible": true,
                "clipBounds": true,
                "Location": "[0,0]",
                "skin": "flexgreyLine",
                "layoutType": kony.flex.FLOW_HORIZONTAL
            }, {
                "padding": [2, 2, 2, 2]
            }, {});
            FlexmainOrederProcessed.setDefaultUnit(kony.flex.DP);
            FlexmainOrederProcessed.add(lblorderDateVal, lblTimeVal, lbltransVal, lblAmountVal,buttonCancel);
           if(i==index-1){
            	frmMFFullStatementNFundSummary.FlexOrderToProcesssegBg.add(FlexmainOrederProcessed);
            }else{
            	frmMFFullStatementNFundSummary.FlexOrderToProcesssegBg.add(FlexmainOrederProcessed, flexLine);
            }
        }
    } catch (e) {
		kony.print("error addrowforOrderToProcess message :" + e.message);
        kony.print("error addrowforOrderToProcess :" + e.stack);
    }
}