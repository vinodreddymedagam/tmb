/*
 * Function on frmMBManageCard
 */
function frmMBManageCardMenuPreshow(){
	//#ifdef android    
    //mki, mib-10789 Start
    // The below code snippet is to fix the whilte label on some screens
    frmMBManageCard.flxManageCardOptions.showFadingEdges  = false;
    //#endif  
    //mki, mib-10789 End
  	
  	frmMBManageCardLocalePreshow();
	preShowfrmMBManageCard();
  
    
}

function frmMBManageCardLocalePreshow(){
	changeStatusBarColor();
	if(gblSelectedCard["cardType"] == "Debit Card"){
		frmMBManageCard.lblManageCardTitle.text = kony.i18n.getLocalizedString("DBA_ManageDebitCard")
		frmMBManageCard.lblBlockCard.text = kony.i18n.getLocalizedString("DBA_BlockCard");
        frmMBManageCard.lblChangeDebitLimit.text =kony.i18n.getLocalizedString("DBL_ChangeLimit");
  	} else {
  		if(gblSelectedCard["cardType"] == "Credit Card"){
  			frmMBManageCard.lblManageCardTitle.text = kony.i18n.getLocalizedString("keyManageCreditCard")
  		} else if(gblSelectedCard["cardType"] == "Ready Cash"){
  			frmMBManageCard.lblManageCardTitle.text = kony.i18n.getLocalizedString("keyManageReadyCashCard")
		}
		frmMBManageCard.lblBlockCard.text = kony.i18n.getLocalizedString("btnBlockCard");
  	}
	
	frmMBManageCard.lblCashAdvance.text = kony.i18n.getLocalizedString("btnRequestCashAdvance");
	frmMBManageCard.lblChangePin.text = kony.i18n.getLocalizedString("CCP01_Instruction");
	frmMBManageCard.lblFullStm.text = kony.i18n.getLocalizedString("btnSeeFullStatement");
	frmMBManageCard.lblRequestPin.text = kony.i18n.getLocalizedString("CRP01_Instruction");
	frmMBManageCard.lblWaitPin.text = kony.i18n.getLocalizedString("CRP01_Instruction");
	frmMBManageCard.lblWaitPINDesc.text = kony.i18n.getLocalizedString("msgNotAllowRequestNewPIN");
	frmMBManageCard.buttonBack.text = kony.i18n.getLocalizedString("Back");
	frmMBManageCard.lblLinkApplysoGood.text = kony.i18n.getLocalizedString("keyApplySoGooODLink");
	var cardNoSkin = "lblDarkGrey36px";
	var gblDeviceInfo = kony.os.deviceInfo();

	var screenwidth = gblDeviceInfo["deviceWidth"];
	
	if(screenwidth > 640) {
		frmMBManageCard.lblWaitPINDesc.skin = "lblDarkGrey36px";
	} else {
		frmMBManageCard.lblWaitPINDesc.skin = "lblDarkGray150";
	}
}

function preShowfrmMBManageCard(){
	
  	
	if(gblSelectedCard["cardType"] == "Debit Card"){
		frmMBManageCard.flxChangeLimitForCC.setVisibility(false);	
        frmMBManageCard.btnBlockCard.onTouchStart = frmMBManageCardOnTouchStart;
  		frmMBManageCard.btnBlockCard.onTouchEnd = frmMBManageCardOnTouchEnd;
        
      	kony.print("@@@@@@gblSelectedCard allowsetLimit@@@@@"+gblSelectedCard["allowSetLimit"]);
      if(gblSelectedCard["allowSetLimit"] == "Y"){
        frmMBManageCard.flxChangeDebitLimit.setVisibility(true);
        frmMBManageCard.btnChangeDebitLimit.onClick = showfrmMBManageChangeDebitCardLimit ;
      }else{
        frmMBManageCard.flxChangeDebitLimit.setVisibility(false);
      }
      
		frmMBManageCard.lblManageCardTitle.text = kony.i18n.getLocalizedString("DBA_ManageDebitCard")
		frmMBManageCard.flxRequestPIN.setVisibility(false);
		frmMBManageCard.flxChangePIN.setVisibility(false);
		frmMBManageCard.flxCashAdvance.setVisibility(false);
		frmMBManageCard.flxBlockCard.setVisibility(true);
		frmMBManageCard.flxFullStm.setVisibility(false);
      	frmMBManageCard.flexLinkApplysoGood.setVisibility(false);
		frmMBManageCard.flxWaitPin.setVisibility(false);

  	} else {
  		
  		//for hover
  		frmMBManageCard.btnBlockCard.onTouchStart = frmMBManageCardOnTouchStart;
  		frmMBManageCard.btnBlockCard.onTouchEnd = frmMBManageCardOnTouchEnd;
  		
  		frmMBManageCard.btnChangePin.onTouchStart = frmMBManageCardOnTouchStart;
  		frmMBManageCard.btnChangePin.onTouchEnd = frmMBManageCardOnTouchEnd;
  		
  		frmMBManageCard.btnRequestPin.onTouchStart = frmMBManageCardOnTouchStart;
  		frmMBManageCard.btnRequestPin.onTouchEnd = frmMBManageCardOnTouchEnd;
  		
		frmMBManageCard.btnCashAdvance.onTouchStart = frmMBManageCardOnTouchStart;
  		frmMBManageCard.btnCashAdvance.onTouchEnd = frmMBManageCardOnTouchEnd;
  		
  		frmMBManageCard.btnFullStm.onTouchStart = frmMBManageCardOnTouchStart;
  		frmMBManageCard.btnFullStm.onTouchEnd = frmMBManageCardOnTouchEnd;
      
      	frmMBManageCard.btnLinkApplysoGood.onTouchStart = frmMBManageCardOnTouchStart;
  		frmMBManageCard.btnLinkApplysoGood.onTouchEnd = frmMBManageCardOnTouchEnd;
  		
  		if(gblSelectedCard["cardType"] == "Credit Card"){
  			frmMBManageCard.lblManageCardTitle.text = kony.i18n.getLocalizedString("keyManageCreditCard");
          //Added change usage limit of credit card
		  frmMBManageCard.flxChangeLimitForCC.setVisibility(true);
          frmMBManageCard.lblChangeLimitForCC.text = kony.i18n.getLocalizedString("CC_menuUsageLimit");
          frmMBManageCard.btnChangeLimitForCC.onClick = navigateToCreditUsageLimt;
  		} else if(gblSelectedCard["cardType"] == "Ready Cash"){
  			frmMBManageCard.lblManageCardTitle.text = kony.i18n.getLocalizedString("keyManageReadyCashCard");
            frmMBManageCard.flxChangeLimitForCC.setVisibility(false);
		}
  		frmMBManageCard.flxBlockCard.setVisibility(true);
		frmMBManageCard.flxChangePIN.setVisibility(true);
		frmMBManageCard.flxCashAdvance.setVisibility(true);
		frmMBManageCard.flxFullStm.setVisibility(true);
        frmMBManageCard.flxChangeDebitLimit.setVisibility(false);
      	kony.print("gblSelectedCardapplysogood"+gblSelectedCard["applySoGoodFlag"]);
      	if(gblSelectedCard["applySoGoodFlag"] == "Y"){
			frmMBManageCard.flexLinkApplysoGood.setVisibility(true);
		} else {
			frmMBManageCard.flexLinkApplysoGood.setVisibility(false);
		}
		if(gblSelectedCard["allowRequestNewPin"] == "N"){
			frmMBManageCard.flxRequestPIN.setVisibility(false);
			frmMBManageCard.flxWaitPin.setVisibility(true);
		} else {
			frmMBManageCard.flxRequestPIN.setVisibility(true);
			frmMBManageCard.flxWaitPin.setVisibility(false);
		}
  	}
  	var cardNoSkin = getCardNoSkin(frmMBManageCard.imgCard.src); //MKI, MIB-12137 start
	frmMBManageCard.lblCardNumber.skin = cardNoSkin; 
  	if(frmMBManageCard.imgCard.src.toLowerCase().includes("debittmbwave")){      
        	frmMBManageCard.lblCardAccountName.skin = "lblBlackCard22px";
	}
    else{         
        	frmMBManageCard.lblCardAccountName.skin = "lblWhiteCard22px";
    } //MKI, MIB-12137 end
}

function frmMBManageCardOnTouchStart(eventobject){
	if (eventobject.id == "btnBlockCard") {
		frmMBManageCard.flxBlockCard.skin = "flexGreyBGBlueBorder";
	}else if (eventobject.id == "btnChangePin") {
		frmMBManageCard.flxChangePIN.skin = "flexGreyBGBlueBorder";
	}else if (eventobject.id == "btnRequestPin") {
		frmMBManageCard.flxRequestPIN.skin = "flexGreyBGBlueBorder";
	}else if (eventobject.id == "btnCashAdvance") {
		frmMBManageCard.flxCashAdvance.skin = "flexGreyBGBlueBorder";
	}else if (eventobject.id == "btnFullStm") {
		frmMBManageCard.flxFullStm.skin = "flexGreyBGBlueBorder";
	}else if (eventobject.id == "btnLinkApplysoGood") {
		frmMBManageCard.flexLinkApplysoGood.skin = "flexGreyBGBlueBorder";
	}
}
function frmMBManageCardOnTouchEnd(eventobject){
	if (eventobject.id == "btnBlockCard") {
		frmMBManageCard.flxBlockCard.skin = "flexWhiteBGBlueBorder";
	}else if (eventobject.id == "btnChangePin") {
		frmMBManageCard.flxChangePIN.skin = "flexWhiteBGBlueBorder";
	}else if (eventobject.id == "btnRequestPin") {
		frmMBManageCard.flxRequestPIN.skin = "flexWhiteBGBlueBorder";
	}else if (eventobject.id == "btnCashAdvance") {
		frmMBManageCard.flxCashAdvance.skin = "flexWhiteBGBlueBorder";
	}else if (eventobject.id == "btnFullStm") {
		frmMBManageCard.flxFullStm.skin = "flexWhiteBGBlueBorder";
	}else if (eventobject.id == "btnLinkApplysoGood") {
		frmMBManageCard.flexLinkApplysoGood.skin = "flexWhiteBGBlueBorder";
	}
}

function getCallbackShowManageCard(status, resulttable){
    var cardType = "";
    gblManageCardFlow = kony.application.getCurrentForm().id;
    if (status == 400) {
    	dismissLoadingScreen();
	    if (resulttable["opstatus"] == 0) {
	    	if(resulttable["errCode"] == undefined) {
	    		gblCardList = resulttable;
		        if (resulttable["creditCardRec"].length > 0) {
		            gblSelectedCard = resulttable["creditCardRec"][0]; 
				}     
		    	if (resulttable["readyCashRec"].length > 0) {
		            gblSelectedCard = resulttable["readyCashRec"][0];
		        }  
		        setGblSelectedCardData();
		        gotoManageCard(gblSelectedCard);
	    	} else {
		    	showAlert(resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
	            return false;
	    	}
        } else{
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    } else{
        dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        return false;
    }
}
function onClickBackManageListCard(){
	if(gblManageCardFlow == "frmMBCardList") {
		if(gblisCardActivationCompleteFlow) {
			frmCardActivationDetails.destroy();
			frmCardActivationComplete.destroy();
			gblisCardActivationCompleteFlow = false;
			preShowMBCardList();
		} else {
			frmMBCardList.show();
		}
	} else if(gblManageCardFlow == "frmMBListDebitCard") {
		if(gblisCardActivationCompleteFlow) {
			frmCardActivationDetails.destroy();
			frmCardActivationComplete.destroy();
			gblisCardActivationCompleteFlow = false;
			callDebitCardInqService();
		} else {
			frmMBListDebitCard.show();
		}
	} else if(gblManageCardFlow == "frmAccountDetailsMB") {
		showAccountDetails({"id":"hboxSwipe"+gblIndex});
	} else if(gblManageCardFlow == "frmMBCreditCardListDeeplink") {
		getCustomerCardListForDeeplink();
	} else {
		preShowMBCardList();
	}
}

