/*****************************************************************
 *     Name    : onClickOfBack
 *     Module  : Touch ID
 *     Author  : Kony IT Services
 *     Purpose : Function to Navigate to pre login form
 *     Params  : None
 * 	   Changes :
 ******************************************************************/

function onClickOfBack() {
kony.print("FPRINT CALLING HERE7");
    frmMBPreLoginAccessesPin.show();
}

/*****************************************************************
 *     Name    : onClickChangeTouchIcon
 *     Module  : Touch ID
 *     Author  : Kony IT Services
 *     Purpose : Function to use flip the skin Touch and Back in prelogin form
 *     Params  : None
 * 	   Changes :
 ******************************************************************/

function onClickChangeTouchIcon() {
    gblSuccess = false;
    frmMBPreLoginAccessesPin.btnTouchnBack.skin = "btnkeypaddel";
    frmMBPreLoginAccessesPin.btnTouchnBack.focusSkin = "btnkeypaddelfoc";
	//fix MIB-9975
	accessPinOnClickLogin();
}

/*****************************************************************
 *     Name    : getPin
 *     Module  : Touch ID
 *     Author  : Kony IT Services
 *     Purpose : Function to use get number from keypad
 *     Params  : yes
 * 	   Changes :
 ******************************************************************/
function getPin(obj) {
    var temp;
    var caseVar = obj.id;
    
    switch(caseVar) {
	    case "btnOne":
	    	temp = "1";
	     break;
	    case "btnTwo":
	    	temp = "2";
    	break;
    	case "btnThree":
    		temp = "3";
    	break;
    	case "btnFour":
    		temp = "4";
    	break;
    	case "btnFive":
    		temp = "5";
    	break;
    	case "btnSix":
    		temp = "6";
    	break;
    	case "btnSeven":
    		 temp = "7";
    	break;
    	case "btnEight":
    		 temp = "8";
    	break;	
    	case "btnNine":
    		 temp = "9";
    	break;
    	case "btnZero":
    		 temp = "0";
    	break;			
    }
    
   /* if (obj.id == "btnOne") {
        temp = "1";
    } else if (obj.id == "btnTwo") {
        temp = "2";
    } else if (obj.id == "btnThree") {
        temp = "3";
    } else if (obj.id == "btnFour") {
        temp = "4";
    } else if (obj.id == "btnFive") {
        temp = "5";
    } else if (obj.id == "btnSix") {
        temp = "6";
    } else if (obj.id == "btnSeven") {
        temp = "7";
    } else if (obj.id == "btnEight") {
        temp = "8";
    } else if (obj.id == "btnNine") {
        temp = "9";
    } else if (obj.id == "btnZero") {
        temp = "0";
    }*/

    if (gblNum.length < 6) {
        gblNum = gblNum + temp;

    }
    if (gblNum.length == 6) {
        
        accsPwdValidatnLogin(gblNum.trim());
    }


   /* if (gblNum.length == 6) //|| gblTouchflag == true
    {
        accsPwdValidatnLogin(gblNum.trim());
    }*/
}

/*****************************************************************
 *     Name    : onClickOfKeypad
 *     Module  : Touch ID
 *     Author  : Kony IT Services
 *     Purpose : Function use to fill  small circles when we enter Pin
 *     Params  : None
 * 	   Changes :
 ******************************************************************/

function onClickOfKeypad() {

    if (gblPinCount < 6) {
        gblPinCount++;
    }
	switch(gblPinCount) {
	    case 1:
	        frmMBPreLoginAccessesPin.imgone.src = "key_sel.png";
	       // frmMBPreLoginAccessesPin.imgTwo.src = "key_default.png";
            //frmMBPreLoginAccessesPin.imgThree.src = "key_default.png";
            //frmMBPreLoginAccessesPin.imgFour.src = "key_default.png";
            //frmMBPreLoginAccessesPin.imgFive.src = "key_default.png";
            //frmMBPreLoginAccessesPin.imgSix.src = "key_default.png";
	        break;
	   case 2:
	        frmMBPreLoginAccessesPin.imgTwo.src = "key_sel.png";
	        break;
	   case 3:
	        frmMBPreLoginAccessesPin.imgThree.src = "key_sel.png";
	        break;
	    case 4:
	        frmMBPreLoginAccessesPin.imgFour.src = "key_sel.png";
	        break;
	    case 5:
	         frmMBPreLoginAccessesPin.imgFive.src = "key_sel.png";
	        break;
	    case 6:
	        frmMBPreLoginAccessesPin.imgSix.src = "key_sel.png";
	        break;            
	             
	} 
   /* if (gblPinCount <= 6) {
        if (gblPinCount == 0) {
            frmMBPreLoginAccessesPin.imgone.src = "key_default.png";
            frmMBPreLoginAccessesPin.imgTwo.src = "key_default.png";
            frmMBPreLoginAccessesPin.imgThree.src = "key_default.png";
            frmMBPreLoginAccessesPin.imgFour.src = "key_default.png";
            frmMBPreLoginAccessesPin.imgFive.src = "key_default.png";
            frmMBPreLoginAccessesPin.imgSix.src = "key_default.png";
        } else if (gblPinCount == 1) {

            frmMBPreLoginAccessesPin.imgone.src = "key_sel.png";
            frmMBPreLoginAccessesPin.imgTwo.src = "key_default.png";
            frmMBPreLoginAccessesPin.imgThree.src = "key_default.png";
            frmMBPreLoginAccessesPin.imgFour.src = "key_default.png";
            frmMBPreLoginAccessesPin.imgFive.src = "key_default.png";
            frmMBPreLoginAccessesPin.imgSix.src = "key_default.png";

        } else if (gblPinCount == 2) {
            frmMBPreLoginAccessesPin.imgone.src = "key_sel.png";
            frmMBPreLoginAccessesPin.imgTwo.src = "key_sel.png";
            frmMBPreLoginAccessesPin.imgThree.src = "key_default.png";
            frmMBPreLoginAccessesPin.imgFour.src = "key_default.png";
            frmMBPreLoginAccessesPin.imgFive.src = "key_default.png";
            frmMBPreLoginAccessesPin.imgSix.src = "key_default.png";

        } else if (gblPinCount == 3) {
            frmMBPreLoginAccessesPin.imgone.src = "key_sel.png";
            frmMBPreLoginAccessesPin.imgTwo.src = "key_sel.png";
            frmMBPreLoginAccessesPin.imgThree.src = "key_sel.png";
            frmMBPreLoginAccessesPin.imgFour.src = "key_default.png";
            frmMBPreLoginAccessesPin.imgFive.src = "key_default.png";
            frmMBPreLoginAccessesPin.imgSix.src = "key_default.png";

        } else if (gblPinCount == 4) {
            frmMBPreLoginAccessesPin.imgone.src = "key_sel.png";
            frmMBPreLoginAccessesPin.imgTwo.src = "key_sel.png";
            frmMBPreLoginAccessesPin.imgThree.src = "key_sel.png";
            frmMBPreLoginAccessesPin.imgFour.src = "key_sel.png";
            frmMBPreLoginAccessesPin.imgFive.src = "key_default.png";
            frmMBPreLoginAccessesPin.imgSix.src = "key_default.png";

        } else if (gblPinCount == 5) {
            frmMBPreLoginAccessesPin.imgone.src = "key_sel.png";
            frmMBPreLoginAccessesPin.imgTwo.src = "key_sel.png";
            frmMBPreLoginAccessesPin.imgThree.src = "key_sel.png";
            frmMBPreLoginAccessesPin.imgFour.src = "key_sel.png";
            frmMBPreLoginAccessesPin.imgFive.src = "key_sel.png";
            frmMBPreLoginAccessesPin.imgSix.src = "key_default.png";

        } else if (gblPinCount == 6) {
            frmMBPreLoginAccessesPin.imgone.src = "key_sel.png";
            frmMBPreLoginAccessesPin.imgTwo.src = "key_sel.png";
            frmMBPreLoginAccessesPin.imgThree.src = "key_sel.png";
            frmMBPreLoginAccessesPin.imgFour.src = "key_sel.png";
            frmMBPreLoginAccessesPin.imgFive.src = "key_sel.png";
            frmMBPreLoginAccessesPin.imgSix.src = "key_sel.png";

        }
    }*/

}

function onClickTouchButtonPrelogin(){
	var returnedValue = "NO";
    if(gblDeviceInfo["name"] == "iPhone")
    {
      onClickCheckTouchIdAvailableUsingKonyAPI();
     // returnedValue = TouchId.isTouchIDAvailable("iPhone");
    }
    if (gblDeviceInfo["name"] == "iPhone" && gblPinCount == 0 && isAuthUsingTouchSupportedUsingKonyAPI() && isTouchIDEnabled()) {
       //Creates an object of class 'TouchIdLogin
       // var TouchIdLoginObject = new TouchId.TouchIdLogin();
       //show loading indicator
       showLoadingScreen();
       //var localStr = ""; 
       //if(kony.i18n.getCurrentLocale() == "en_US"){
       //   localStr = kony.i18n.getLocalizedString("keyTouchCancel");//"Or press cancel to login with Access PIN";
       // } else { 
          //localStr = "ใส่รหัสผ่าน iPhone หรือ กด ยกเลิก เพื่อใส่รหัสผ่าน TMB Touch";
       // localStr = kony.i18n.getLocalizedString("keyTouchCancel");//"ใส่รหัสผ่านเครื่อง/กดยกเลิกใส่รหัสTMB Touch";
        //} 
        //Invokes method 'authenicateButtonTapped' on the object
       // TouchIdLoginObject.authenicateButtonTapped(onTouchIdScan,localStr);
		authUsingTouchIDUsingKonyAPI();
    } 
	dismissLoadingScreen();
}


/*****************************************************************
 *     Name    : onClickTounchandBack
 *     Module  : Touch ID
 *     Author  : Kony IT Services
 *     Purpose : Function to  unfill small circles when we click on back button
 *     Params  : None
 * 	   Changes :
 ******************************************************************/
 /*tooManyAtmFlag=kony.store.getItem("tooManyAtmFlag");
 alert("tooManyAtmFlag before="+tooManyAtmFlag);
 if(tooManyAtmFlag==null || tooManyAtmFlag==undefined){
	 tooManyAtmFlag=false;
 	kony.store.setItem("tooManyAtmFlag",tooManyAtmFlag);
 }
 alert("tooManyAtmFlag after="+tooManyAtmFlag);*/
 tooManyAtmFlag=false;
 
//COmmmented below unused function
/*
function onClickTounchandBack() {

    //if(gblDeviceInfo["name"] == "iPhone" && gblPinCount == 0 && gblTouchCount <= 2 && onClickCheckTouchIdAvailable()){
   
    var returnedValue = "NO";
    if(gblDeviceInfo["name"] == "iPhone")
    {
      onClickCheckTouchIdAvailable();
      returnedValue = TouchId.isTouchIDAvailable("iPhone");
    }
    if (gblDeviceInfo["name"] == "iPhone" && gblPinCount == 0 && returnedValue == "YES" && isTouchIDEnabled()) {
      if(frmMBPreLoginAccessesPin.btnTouchnBack.skin == "btntouchiconstudio5"){
        //Creates an object of class 'TouchIdLogin
        var TouchIdLoginObject = new TouchId.TouchIdLogin();
        //show loading indicator
        showLoadingScreen();
       var localStr = ""; 
       if(kony.i18n.getCurrentLocale() == "en_US"){
          localStr = kony.i18n.getLocalizedString("keyTouchCancel");//"Or press cancel to login with Access PIN";
        } else { 
          //localStr = "ใส่รหัสผ่าน iPhone หรือ กด ยกเลิก เพื่อใส่รหัสผ่าน TMB Touch";
          localStr = kony.i18n.getLocalizedString("keyTouchCancel");//"ใส่รหัสผ่านเครื่อง/กดยกเลิกใส่รหัสTMB Touch";
        } 
        //Invokes method 'authenicateButtonTapped' on the object
        TouchIdLoginObject.authenicateButtonTapped(onTouchIdScan,localStr);
      }
    }else if (gblDeviceInfo["name"] == "android"){
		kony.print("FPRINT: on FP button skin:"+frmMBPreLoginAccessesPin.btnTouchnBack.skin);
		if(frmMBPreLoginAccessesPin.btnTouchnBack.skin == "btntouchiconstudio5"){
			//frmMBPreLoginAccessesPin.show();	
			kony.print("FPRINT toggling true");
			showFPFlex(true);
			fpdefault();
			fpFlag=true;
			if(!tooManyAtmFlag){
				authUsingTouchID();
			}
		}else {
		kony.print("FPRINT toggling false1");
			showFPFlex(false);
        	onClickPinBack();
    	}

	}  else {
	kony.print("FPRINT toggling flase2");
		showFPFlex(false);
        onClickPinBack();
    }
}
*/

/*****************************************************************
 *     Name    : onTouchIdScan
 *     Module  : Touch ID
 *     Author  : Kony IT Services
 *     Purpose : Function post to error message
 *     Params  : None
 * 	   Changes :
 ******************************************************************/
function onTouchIdScan(result) {
    //alert("in onTouchIdscan" + result["status"]);

    if (result != undefined && result != null) {

        //authenticateUser();
        if (result["status"] == "Success") {
            //alert("Success");
            gblSuccess = true;
            showLoadingScreen();
            swipeLoginSuccessSkinChange();
            accsPwdValidatnLogin(gblNum);
        } else {
        
          // dismiss loading indicator
            kony.application.dismissLoadingScreen();
            gblSuccess = false;
            var errMsg = result["errorDescription"];
            if (errMsg == "LAErrorUserFallback") {
                //TouchID lock. navigate to Access Pin form
                //showAlertTouchErrCode(kony.i18n.getLocalizedString("userFallback"), kony.i18n.getLocalizedString("info"))
            } else if (errMsg == "LAErrorUserCancel") {
                //navigate to Access Pin 
                //showAlert(kony.i18n.getLocalizedString("userCancel"), kony.i18n.getLocalizedString("info")) // temprory commented
            } else if (errMsg == "LAErrorAuthenticationFailed") {
                //showAlertTouchErrCode(kony.i18n.getLocalizedString("userAuthenticationFailed"), kony.i18n.getLocalizedString("info"))// temprory commented
                //alert("Login failed. Please try again or press 'Cancel' to  enter your Access PIN");
            } else if(errMsg == "LAErrorTouchIDNotAvailable"){
                //showAlertTouchErrCode(kony.i18n.getLocalizedString("userTouchIDNotAvailable"), kony.i18n.getLocalizedString("info"))// temprory commented
                // alert("Login failed. Please try again or press 'Cancel' to  enter your Access PIN")       
            } else if(errMsg == "LAErrorTouchIDNotEnrolled"){
                //showAlertTouchErrCode(kony.i18n.getLocalizedString("userTouchIDNotAvailable"), kony.i18n.getLocalizedString("info"));// temprory commented
            } else if(errMsg == "LAErrorPasscodeNotSet"){
                //showAlertTouchErrCode(kony.i18n.getLocalizedString("userTouchIDNotAvailable"), kony.i18n.getLocalizedString("info"));
            }
        }
    }else{
      // Dismiss loading indicator
      kony.application.dismissLoadingScreen();
      alert("Unable to login using Touch ID");
    }

}
/*****************************************************************
 *     Name    : onClickPinBack
 *     Module  : Touch ID
 *     Author  : Kony IT Services
 *     Purpose : Function to  unfill small circles when we click on back button
 *     Params  : None
 * 	   Changes :
 ******************************************************************/
function onClickPinBack() {

    if (gblPinCount > 0) {
        gblPinCount--;
        var pinLen = gblNum;
        var updatedPin = gblNum.substring(0, pinLen.length - 1)
        gblNum = updatedPin;
    }

  switch(gblPinCount) {
	    case 0:
	        frmMBPreLoginAccessesPin.imgone.src = "key_default.png";
	        //frmMBPreLoginAccessesPin.imgTwo.src = "key_default.png";
	        //frmMBPreLoginAccessesPin.imgThree.src = "key_default.png";
	        //frmMBPreLoginAccessesPin.imgFour.src = "key_default.png";
	        //frmMBPreLoginAccessesPin.imgFive.src = "key_default.png";
	        //frmMBPreLoginAccessesPin.imgSix.src = "key_default.png";
	        //alert("isEnable ---->"+isEnable);
	       /* var returnedValue = "NO";
	        if(gblDeviceInfo["name"] == "iPhone"){
	          returnedValue = TouchId.isTouchIDAvailable("iPhone");
	        }
	        */
	        if (gblDeviceInfo["name"] == "iPhone" && isAuthUsingTouchSupportedUsingKonyAPI() && isTouchIDEnabled()) {
	            //gblNum="";
	            if(gblDeviceInfo["model"]=="iPhone X"){
                    frmMBPreLoginAccessesPin.btnTouchnBack.skin = "btnFaceId";
                    frmMBPreLoginAccessesPin.btnTouchnBack.focusSkin = "btnFaceId";
                }else{
                    frmMBPreLoginAccessesPin.btnTouchnBack.skin = "btntouchiconstudio5";
                    frmMBPreLoginAccessesPin.btnTouchnBack.focusSkin = "btntouchiconfoc";
                }
	        } else if(gblDeviceInfo["name"] == "android" && isAuthUsingTouchSupported() && isTouchIDEnabled()){   //else if(gblDeviceInfo["name"] == "android" && isAuthUsingTouchSupported() && hasRegisteredFinger() && isTouchIDEnabled()){
	        	kony.print("FPRINT isUserStatusActive="+isUserStatusActive);
	        	if(isUserStatusActive){
	        		frmMBPreLoginAccessesPin.btnTouchnBack.skin = "btntouchiconstudio5";
	           		frmMBPreLoginAccessesPin.btnTouchnBack.focusSkin = "btntouchiconfoc";
	            }else{
	            	frmMBPreLoginAccessesPin.btnTouchnBack.skin = "btnkeypaddel";
    				frmMBPreLoginAccessesPin.btnTouchnBack.focusSkin = "btnkeypaddelfoc";
	            }
	        }
		break;
    	case 1:
	        //frmMBPreLoginAccessesPin.imgone.src = "key_sel.png";
	        frmMBPreLoginAccessesPin.imgTwo.src = "key_default.png";
	       // frmMBPreLoginAccessesPin.imgThree.src = "key_default.png";
	        //frmMBPreLoginAccessesPin.imgFour.src = "key_default.png";
	        //frmMBPreLoginAccessesPin.imgFive.src = "key_default.png";
	        //frmMBPreLoginAccessesPin.imgSix.src = "key_default.png";
		break;
    	case 2:
	       // frmMBPreLoginAccessesPin.imgone.src = "key_sel.png";
	        //frmMBPreLoginAccessesPin.imgTwo.src = "key_sel.png";
	        frmMBPreLoginAccessesPin.imgThree.src = "key_default.png";
	       // frmMBPreLoginAccessesPin.imgFour.src = "key_default.png";
	        //frmMBPreLoginAccessesPin.imgFive.src = "key_default.png";
	        //frmMBPreLoginAccessesPin.imgSix.src = "key_default.png";
		break;
   		case 3:
	        //frmMBPreLoginAccessesPin.imgone.src = "key_sel.png";
	        //frmMBPreLoginAccessesPin.imgTwo.src = "key_sel.png";
	        //frmMBPreLoginAccessesPin.imgThree.src = "key_sel.png";
	        frmMBPreLoginAccessesPin.imgFour.src = "key_default.png";
	       // frmMBPreLoginAccessesPin.imgFive.src = "key_default.png";
	        //frmMBPreLoginAccessesPin.imgSix.src = "key_default.png";
		break;
    	case 4:
	        //frmMBPreLoginAccessesPin.imgone.src = "key_sel.png";
	        //frmMBPreLoginAccessesPin.imgTwo.src = "key_sel.png";
	        //frmMBPreLoginAccessesPin.imgThree.src = "key_sel.png";
	        //frmMBPreLoginAccessesPin.imgFour.src = "key_sel.png";
	        frmMBPreLoginAccessesPin.imgFive.src = "key_default.png";
	        //frmMBPreLoginAccessesPin.imgSix.src = "key_default.png";
		break;
    	case 5:
	       // frmMBPreLoginAccessesPin.imgone.src = "key_sel.png";
	        //frmMBPreLoginAccessesPin.imgTwo.src = "key_sel.png";
	        //frmMBPreLoginAccessesPin.imgThree.src = "key_sel.png";
	        //frmMBPreLoginAccessesPin.imgFour.src = "key_sel.png";
	        //frmMBPreLoginAccessesPin.imgFive.src = "key_sel.png";
	        frmMBPreLoginAccessesPin.imgSix.src = "key_default.png";
		break;
    }
    /*if (gblPinCount == 0) {
        frmMBPreLoginAccessesPin.imgone.src = "key_default.png";
        frmMBPreLoginAccessesPin.imgTwo.src = "key_default.png";
        frmMBPreLoginAccessesPin.imgThree.src = "key_default.png";
        frmMBPreLoginAccessesPin.imgFour.src = "key_default.png";
        frmMBPreLoginAccessesPin.imgFive.src = "key_default.png";
        frmMBPreLoginAccessesPin.imgSix.src = "key_default.png";
        //alert("isEnable ---->"+isEnable);
        var returnedValue = "NO";
        
        if(gblDeviceInfo["name"] == "iPhone"){
          returnedValue = TouchId.isTouchIDAvailable("iPhone");
        }
        
        if (gblDeviceInfo["name"] == "iPhone" && returnedValue == "YES" && isTouchIDEnabled()) {
            //gblNum="";
            frmMBPreLoginAccessesPin.btnTouchnBack.skin = "btntouchiconstudio5";
            frmMBPreLoginAccessesPin.btnTouchnBack.focusSkin = "btntouchiconfoc";
        }

    } else if (gblPinCount == 1) {

        frmMBPreLoginAccessesPin.imgone.src = "key_sel.png";
        frmMBPreLoginAccessesPin.imgTwo.src = "key_default.png";
        frmMBPreLoginAccessesPin.imgThree.src = "key_default.png";
        frmMBPreLoginAccessesPin.imgFour.src = "key_default.png";
        frmMBPreLoginAccessesPin.imgFive.src = "key_default.png";
        frmMBPreLoginAccessesPin.imgSix.src = "key_default.png";

    } else if (gblPinCount == 2) {
        frmMBPreLoginAccessesPin.imgone.src = "key_sel.png";
        frmMBPreLoginAccessesPin.imgTwo.src = "key_sel.png";
        frmMBPreLoginAccessesPin.imgThree.src = "key_default.png";
        frmMBPreLoginAccessesPin.imgFour.src = "key_default.png";
        frmMBPreLoginAccessesPin.imgFive.src = "key_default.png";
        frmMBPreLoginAccessesPin.imgSix.src = "key_default.png";

    } else if (gblPinCount == 3) {
        frmMBPreLoginAccessesPin.imgone.src = "key_sel.png";
        frmMBPreLoginAccessesPin.imgTwo.src = "key_sel.png";
        frmMBPreLoginAccessesPin.imgThree.src = "key_sel.png";
        frmMBPreLoginAccessesPin.imgFour.src = "key_default.png";
        frmMBPreLoginAccessesPin.imgFive.src = "key_default.png";
        frmMBPreLoginAccessesPin.imgSix.src = "key_default.png";

    } else if (gblPinCount == 4) {
        frmMBPreLoginAccessesPin.imgone.src = "key_sel.png";
        frmMBPreLoginAccessesPin.imgTwo.src = "key_sel.png";
        frmMBPreLoginAccessesPin.imgThree.src = "key_sel.png";
        frmMBPreLoginAccessesPin.imgFour.src = "key_sel.png";
        frmMBPreLoginAccessesPin.imgFive.src = "key_default.png";
        frmMBPreLoginAccessesPin.imgSix.src = "key_default.png";

    } else if (gblPinCount == 5) {
        frmMBPreLoginAccessesPin.imgone.src = "key_sel.png";
        frmMBPreLoginAccessesPin.imgTwo.src = "key_sel.png";
        frmMBPreLoginAccessesPin.imgThree.src = "key_sel.png";
        frmMBPreLoginAccessesPin.imgFour.src = "key_sel.png";
        frmMBPreLoginAccessesPin.imgFive.src = "key_sel.png";
        frmMBPreLoginAccessesPin.imgSix.src = "key_default.png";

    } else if (gblPinCount == 6) {
        frmMBPreLoginAccessesPin.imgone.src = "key_sel.png";
        frmMBPreLoginAccessesPin.imgTwo.src = "key_sel.png";
        frmMBPreLoginAccessesPin.imgThree.src = "key_sel.png";
        frmMBPreLoginAccessesPin.imgFour.src = "key_sel.png";
        frmMBPreLoginAccessesPin.imgFive.src = "key_sel.png";
        frmMBPreLoginAccessesPin.imgSix.src = "key_sel.png";

    }*/
}
/*****************************************************************
 *     Name    : CallTheNumber
 *     Module  : Touch ID
 *     Author  : Kony IT Services
 *     Purpose : Function use to navigate the dail pad and allow to dail the user.
 *     Params  : None
 * 	   Changes :
 ******************************************************************/
function CallTheNumber() {
    try {
        var number = "1558";
        kony.phone.dial(number);
    } catch (err) {
        alert("error in dial:: " + err);
    }
}


/*****************************************************************
 *     Name    : onClickCheckTouchIdAvailable
 *     Module  : Touch ID
 *     Author  : Kony IT Services
 *     Purpose : Function use to verify Touch future available or not in device.
 *     Params  : None
 * 	   Changes :
 ******************************************************************/

function onClickCheckTouchIdAvailable() {
    gblTouchDevice = false;
    kony.print("FPRINT: GOWRI: onClickCheckTouchIdAvailable has called ");
    if(gblDeviceInfo["name"] == "iPhone"){ //checking FP support for Iphone
	    var returnedValue = TouchId.isTouchIDAvailable("iPhone");
	    if(returnedValue == "YES"){
	       gblTouchDevice = true;
	     }else if(returnedValue == "LAErrorTouchIDNotEnrolled"){
	       gblTouchDevice = true;
	     }else{
	       gblTouchDevice = false;
	     }
	 }else if(gblDeviceInfo["name"] == "android"){	//checking FP support for Android
	 	kony.print("FPRINT: CHECCKNG FP SUPPORT FOR ANDROID");
	 	//&& hasRegisteredFinger()
	 	if(isAuthUsingTouchSupported()){
	 		gblTouchDevice = true;
	 	}else{
	 		gblTouchDevice = false;
	 	}
	 }   
	 kony.print("FPRINT: FP SUPPORT IS="+gblTouchDevice);
    return gblTouchDevice;
}

function isTouchIDEnabled(){
    if(gblTouchDevice == true) {
      var usestouchFlag = kony.store.getItem("usesTouchId");
     // alert("touchFlag--111true->"+usestouchFlag);
	  if(usestouchFlag == null){
	     // alert("touchFlag--iffff->"+usestouchFlag)
	      kony.store.setItem("usesTouchId", "N");
	      usestouchFlag = "N";
	      return false;	      
	  }else{
	      //usestouchFlag = kony.store.getItem("usesTouchId");
	      if(usestouchFlag == "Y")
	         return true;
	      else
	         return false;   
	  }
	}
}

function resetValues()
{
   if(gblDeviceInfo["name"] == "thinclient") return false;
	 if(kony.net.isNetworkAvailable(constants.NETWORK_TYPE_ANY)){
	 	if (!gblExitByBackBtn){
            frmMBPreLoginAccessesPin.lblEnterPin.text = kony.i18n.getLocalizedString("tryagain");
        }else{
    		gblExitByBackBtn = false;
    	}
     }
     frmMBPreLoginAccessesPin.lblEnterPin.text = kony.i18n.getLocalizedString("enterpin");
     if(!gblFromLogout){
       frmMBPreLoginAccessesPin.lblEnterPin.skin = "lblBlackMed150NewRed";
       kony.print("gblFromLogout = true ");
     }
      //gblFromLogout = false; 
     gblFPLoginSvsCalled = false; 
     gblPinCount = 0;
     gblNum = "";
     frmMBPreLoginAccessesPin.imgone.src = "key_default.png";
     frmMBPreLoginAccessesPin.imgTwo.src = "key_default.png";
     frmMBPreLoginAccessesPin.imgThree.src = "key_default.png";
     frmMBPreLoginAccessesPin.imgFour.src = "key_default.png";
     frmMBPreLoginAccessesPin.imgFive.src = "key_default.png";
     frmMBPreLoginAccessesPin.imgSix.src = "key_default.png";
	 gblIsLoading = false;
  	 gblMBLoggingIn = false;
     var returnedValue = "NO";
    /* if(gblDeviceInfo["name"] == "iPhone"){
       returnedValue = TouchId.isTouchIDAvailable("iPhone");
     }*/
     if(gblDeviceInfo["name"] == "iPhone" && isAuthUsingTouchSupportedUsingKonyAPI()  && gblPinCount == 0 && isTouchIDEnabled()){
        if(gblDeviceInfo["model"]=="iPhone X"){
            frmMBPreLoginAccessesPin.btnTouchnBack.skin = "btnFaceId";
            frmMBPreLoginAccessesPin.btnTouchnBack.focusSkin = "btnFaceId";
        }else{
            frmMBPreLoginAccessesPin.btnTouchnBack.skin = "btntouchiconstudio5";
            frmMBPreLoginAccessesPin.btnTouchnBack.focusSkin = "btntouchiconfoc";
        }
     }else if(gblDeviceInfo["name"] == "android" && isAuthUsingTouchSupportedUsingKonyAPI() && isTouchIDEnabled()){
    	if(isUserStatusActive){
	        		frmMBPreLoginAccessesPin.btnTouchnBack.skin = "btntouchiconstudio5";
	           		frmMBPreLoginAccessesPin.btnTouchnBack.focusSkin = "btntouchiconfoc";
        }else{
          frmMBPreLoginAccessesPin.btnTouchnBack.skin = "btnkeypaddel";
          frmMBPreLoginAccessesPin.btnTouchnBack.focusSkin = "btnkeypaddelfoc";
        }
	 }else{
        onClickChangeTouchIcon();
     }
}

imgEDSelcted=1;

function imgEDToggle(){
	kony.print("FPRINT: imgED.src="+frmFPSetting.imgED.src);
	if(frmFPSetting.imgED.src=="android_unsel.png"){
		frmFPSetting.imgED.src="android_sel.png" ;
		imgEDSelected=0;
	}else{
		frmFPSetting.imgED.src="android_unsel.png" ;
		imgEDSelected=1;
	}
}

function onImgEDTouch(){
		imgEDToggle();
		var selectedKey=imgEDSelected;
		kony.print("FPRINT: IN onRdBtnTouchSel selectedKey="+selectedKey);
		//var selectedKey = frmTouchIdSettings.chkBoxGrp.selectedIndex;
		if(selectedKey == 0){ //ON
		kony.print("FPRINT: RADIO BUTTON ON");
		//&& hasRegisteredFinger()
		//if(isAuthUsingTouchSupported() && hasRegisteredFinger()){
          if(isAuthUsingTouchSupportedUsingKonyAPI()){
			kony.print("FPRINT: SUCESS!");
			kony.print("FPRINT: ENABLING TOUCH LOGIN");
			onEnableTouchLogin();
		}else{
			kony.print("FPRINT: FAILED123 isAuthUsingTouchSupported and hasRegisteredFinger");
			//showAlertTouchMessageFPAndroid(kony.i18n.getLocalizedString("ErrAndNoFinPrint"), "");
			showAlertTouchMessageFPAndroid(kony.i18n.getLocalizedString("ErrAndNoFinPrint"), "");
			kony.print("FPRINT: ErrAndNoFinPrint="+kony.i18n.getLocalizedString("ErrAndNoFinPrint"));
		}

          /* var returnedValue = TouchId.isTouchIDAvailable("iPhone");
          if(returnedValue == "LAErrorTouchIDNotEnrolled"){
            showAlertTouchMessage(kony.i18n.getLocalizedString("keyTouchMessage"), "Sorry")

            setEnableDisableTouchLogin();
          }else {
             onEnableTouchLogin();
          }*/
       }else { // Off
		kony.print("FPRINT: RADIO BUTTON OFF");
		   onConfirmGetDeviceID();
		}
} 

function onClickToggle(){
	//alert("selected keys "+frmTouchIdSettings.chkBoxGrp.selectedKeys);
	//alert("selected keyVal "+frmTouchIdSettings.chkBoxGrp.selectedKeyValues);
	var selectedKey = frmTouchIdSettings.chkBoxGrp.selectedIndex;
		if(selectedKey == 0){ //ON
           //var returnedValue = TouchId.isTouchIDAvailable("iPhone");
          
          if(!isAuthUsingTouchSupportedUsingKonyAPI()){
            showAlertTouchMessage(kony.i18n.getLocalizedString("keyTouchMessage"), "Sorry")

            setEnableDisableTouchLogin();
          }else {
             onEnableTouchLogin();
          }
       }else { // Off
		   onConfirmGetDeviceID();
		}
}


/*****************************************************************
 *     Name    : showAlertTouchMessageFPAndroid
 *     Module  : Touch ID
 *     Author  : Kony IT Services
 *     Purpose : Function Touch message
 *     Params  : None
 * 	   Changes :
 ******************************************************************/
function showAlertTouchMessageFPAndroid(keyMsg, KeyTitle) {
	//var KeyTitle = "";//"Sorry" kony.i18n.getLocalizedString("keyOK");
  //  var keyMsg = kony.i18n.getLocalizedString("keyTouchMessage"); //ErrAndNoFinPrint
   // var okk = kony.i18n.getLocalizedString("keyOK");
    var keyCancelBtn = kony.i18n.getLocalizedString("keyCancelButton");
    var keySettingsBtn = kony.i18n.getLocalizedString("MIB_P2PViewID");
   //Defining basicConf parameter for alert
	var basicConf = {
		message: keyMsg,
		alertType: constants.ALERT_TYPE_CONFIRMATION,
		alertTitle: KeyTitle,
		yesLabel: keySettingsBtn,
		noLabel: keyCancelBtn,
		alertHandler: callBackShowAlertTouchMessageFPAndroid
	};
	//Defining pspConf parameter for alert
	var pspConf = {};
	//Alert definition
	var infoAlert = kony.ui.Alert(basicConf, pspConf);
    function handleok(response){}
    
	return;
}
	
function callBackShowAlertTouchMessageFPAndroid(response){
kony.print("In side call back callBackShowAlertTouchMessage");

	frmFPSetting.imgED.src="android_unsel.png" ;
	imgEDSelected=1;
	if(response == true){
		kony.print("Navigate to device settings screen");	
		
		openSettings.settings(); //MarshmallowPermissionChecks.NavigateToSettings();
	}
}


/*****************************************************************
 *     Name    : showAlertTouchMessage
 *     Module  : Touch ID
 *     Author  : Kony IT Services
 *     Purpose : Function Touch message
 *     Params  : None
 * 	   Changes :
 ******************************************************************/
function showAlertTouchMessage(keyMsg, KeyTitle) {
	var KeyTitle = "Sorry"//kony.i18n.getLocalizedString("keyOK");
   // var keyMsg = kony.i18n.getLocalizedString("keyTouchMessage");
    var okk = kony.i18n.getLocalizedString("keyOK");
    
	//Defining basicConf parameter for alert
	var basicConf = {
		message: keyMsg,
		alertType: constants.ALERT_TYPE_INFO,
		alertTitle: KeyTitle,
		yesLabel: okk,
		noLabel: "",
		alertHandler: callBackShowAlertTouchMessage
	};
	//Defining pspConf parameter for alert
	var pspConf = {};
	//Alert definition
	var infoAlert = kony.ui.Alert(basicConf, pspConf);
    function handleok(response){}
    
	return;
}
	
function callBackShowAlertTouchMessage(){
kony.print("In side call back callBackShowAlertTouchMessage");
	frmFPSetting.imgED.src="android_unsel.png" ;
	imgEDSelected=1;

}

function onClickForgotPin(){
	gblSetPwd = true;
	gblTouchShow = true;
	frmMBanking.imgActivation1.src="reactivation_option_1.png";
	frmMBanking.imgActivation2.src="reactivation_option_2.png";
	frmMBanking.imgActivation3.src="reactivation_option_3.png";
	frmMBanking.lblActivation1.skin="lblBlueActivation";
	frmMBanking.lblActivation2.skin="lblBlueActivation";
	frmMBanking.lblActivation3.skin="lblBlueActivation";
	frmMBanking.vbxActivationAllOptions.skin="vbxBgWhite";
	frmMBanking.imgActivationArrow1.src="navarrowblue.png";
	frmMBanking.imgActivationArrow2.src="navarrowblue.png";
	frmMBanking.imgActivationArrow3.src="navarrowblue.png";
	frmMBanking.show();
}

function swipeLoginSuccessSkinChange()
{
	frmMBPreLoginAccessesPin.imgone.src = "key_sel.png";
    frmMBPreLoginAccessesPin.imgTwo.src = "key_sel.png";
    frmMBPreLoginAccessesPin.imgThree.src = "key_sel.png";
    frmMBPreLoginAccessesPin.imgFour.src = "key_sel.png";
    frmMBPreLoginAccessesPin.imgFive.src = "key_sel.png";
    frmMBPreLoginAccessesPin.imgSix.src = "key_sel.png";
}



function loginTouchIdMobileBanking(initVecValue,touchCode,serverTime) {
  gblMBLoggingIn = true;
  
  var inputParam = {};
  kony.print("KONYTOUCHID: Inside loginTouchIdMobileBanking >>>");
  var secKeyVal = decrypt(initVecValue); //glbInitVector
  secKeyVal = secKeyVal.substring(secKeyVal.length-10, secKeyVal.length);

  //var inputStr = secKeyVal.trim() + GBL_UNIQ_ID + serverTime.trim() + touchCode.trim();
  //Need unencrypted device to fix MIB-7138 - R76_MB_Kony7.3Migration_Login - Cannot login with TouchID
  var inputStr = secKeyVal.trim() + gblDeviceIDUnEncrypt + serverTime.trim() + touchCode.trim();
  //alert("secKeyVal-->"+secKeyVal.trim()+" GBL_UNIQ_ID--->"+GBL_UNIQ_ID+" serverTime--->"+serverTime.trim()+" touchCode--->"+touchCode.trim())
  var deviceIdHash = kony.crypto.createHash("sha256", inputStr); 
  //alert("deviceIdHash---->"+deviceIdHash);
  //*******old inputParam******
  channelId = "02";
  var pushRegID=kony.store.getItem("kpnssubcriptiontoken");
  if(pushRegID!=null && pushRegID!="" && pushRegID!=undefined && pushRegID!="undefined")
  {
    inputParam["subscriptionToken"]=pushRegID;
  }
  if (GLOBAL_MB_CHANNEL != null && GLOBAL_MB_CHANNEL != "") {
    channelId = GLOBAL_MB_CHANNEL;
  }
  inputParam["loginChannel"] = channelId;
  inputParam["nodeNum"] = "1"
  if (gFromConnectAccount == true) {
    inputParam["password"] = glbAccessPin
  } else {
    inputParam["password"] = gblNum;//frmAfterLogoutMB.tbxAccessPIN.text;
    gblNum ="";
    //frmAfterLogoutMB.tbxAccessPIN.text = "";
  }
  inputParam["activityTypeId"] = "Login";
  kony.print("KONYTOUCHID: loginTouchIdMobileBanking gblSuccess >>>"+gblSuccess);		
  if(gblSuccess == true || gblSuccess == "true"){
    inputParam["loginTypeTouch"] = "true";
  }
  else{ 
    inputParam["loginTypeTouch"] = "false";
  }
  //*******old inputParam******
  inputParam["deviceId2"] = deviceIdHash;
  inputParam["osDeviceId"] = getDeviceID();
  inputParam["deviceOS"] = getDeviceOS();
  inputParam["deviceId"] = getDeviceID();
  invokeServiceSecureAsync("LoginCompositeService", inputParam, callBackMBLoginAuth);
}


function getKeyForLoginTouch(){
			var inputParam = {};
      	  	invokeServiceSecureAsync("RetrieveDeviceKey", inputParam, callBackGetSecKey)
}

function callBackGetSecKey(status, resulttable){
    	if (status == 400) {
			 if(resulttable["opstatus"] == 0){
			 	encryptSecretKey(resulttable["secretKey"], glbInitVector);
			 	loginTouchIdMobileBanking(glbInitVector,gblTouchCodeTouch,gblServerTimeTouch);
			 }else{
			 	    resetValues();
					kony.application.dismissLoadingScreen();
					 var errorMessage = resulttable["errMsg"];
					var middlewareErrorMessage = resulttable["errmsg"];
					if(errorMessage == null || errorMessage == undefined || errorMessage == "" ){
						if(middlewareErrorMessage != null && middlewareErrorMessage != undefined && middlewareErrorMessage != "" ){
						    errorMessage =  middlewareErrorMessage;
						}else{
							errorMessage = kony.i18n.getLocalizedString("ECGenOTPRtyErr00001");
						}
					}
			       alert(""+errorMessage);
			 }
    				
    				
    	}
}
