







/*
************************************************************************
            Name    : loadResourceBundle()
            Author  : 
            Date    : 
            Purpose : To update the Resource Bundle 
        	Input params: 
       		Output params:
        	ServiceCalls: loadResourceBundle
       		Called From : pre app init 
************************************************************************
*/

function loadResourceBundle(customCallback) {
  	kony.print("usage >> start loadResourceBundle "+new Date().getTime());
  
        //#ifdef iphone
	        startSecurityValidationIphone();
        //#endif
  
        //#ifdef android
 	       intialize();
        //#endif
  
	var updatedOn = kony.i18n.getLocalizedString("keyphrasesUpdatedOn");
    kony.print("keyphrasesUpdatedOn:-------->"+updatedOn);
    var currDt = new Date();
    var updatedOnDt = "";
    gblFirtTimeLaunch = false;
    kony.print("index of updated on dt: "+updatedOn.indexOf('-'));
    if(isNotBlank(updatedOn) && updatedOn.indexOf('-') != -1){
      updatedOnDt = updatedOn.substr(0,updatedOn.indexOf('-'));
      kony.print("updatedOnDt---------->"+updatedOnDt);
      if(kony.string.startsWith(updatedOn, "01-JAN-13")){
        gblFirtTimeLaunch = true;
      }
    }
    kony.print("Todays date is: "+currDt.getDate()); //ToDo: if currDt is greater than keyphrasesUpdatedOn date then call getPhrases
	var i18n_inputparam = {};
	i18n_inputparam["timestamp"] = updatedOn;


	i18n_inputparam["localeId"] = kony.i18n.getCurrentLocale();
	i18n_inputparam["platform1"] = "M";	

     var getContentUpdate = kony.store.getItem("getContentUpdate"); 
     if(getContentUpdate != null)
       gblCallPhrasesFlag = getContentUpdate["CallPhrasesOnInitFlag"];
  
      //kony.print("gblCallPhrasesFlag: "+gblCallPhrasesFlag); 
     if(gblCallPhrasesFlag == true || gblCallPhrasesFlag == "true"){
         gblgetPhrasesCalled = true;
        //showLoadingScreen();  //not req at launch
        var status = invokeServiceSecureAsync("NewGetPhrases", i18n_inputparam, isNotBlank(customCallback) ? customCallback : callbackgetphrasesMB);  
     }else{
        if(gblFirtTimeLaunch || kony.os.toNumber(currDt.getDate()) != kony.os.toNumber(updatedOnDt) || getContentUpdate == null){
        kony.print("call getPhrases ------------");
          gblgetPhrasesCalled = true;
          //showLoadingScreen();  //not req at launch
        var status = invokeServiceSecureAsync("NewGetPhrases", i18n_inputparam, isNotBlank(customCallback) ? customCallback : callbackgetphrasesMB);
     } else {       
        kony.print("No need to call getPhrases ------------");
        kony.print("Call initGlobalVars ---");
        //showLoadingScreen();  //not req at launch
        initGlobalVars(getContentUpdate);
      }  
     }
  
     
   //var status = invokeServiceSecureAsync("getPhrases", i18n_inputparam, isNotBlank(customCallback) ? customCallback : callbackgetphrasesMB);
	
}

function initGlobalVars(getContentUpdate) {
	kony.print(">> initGlobalVars");
	dynamicCampaignBrowser();
	initGlobalsFromTPhrases(getContentUpdate);  
    var getEncrKeyFromDevice = kony.store.getItem("encrytedText");

	if (getContentUpdate["Results"] != null) {

		//#ifdef android
		
		if (getEncrKeyFromDevice != null) {
			//TMBUtil.DestroyForm(frmAfterLogoutMB);
             gblApplicationLaunch = true;   
            var currForm = kony.application.getCurrentForm();
			if (currForm != null && currForm.id == "frmMBPreLoginAccessesPin") { //frmAfterLogoutMB
				
				//Reverted Commented FFI Code
				//if (isAuthUsingTouchSupported() && hasRegisteredFinger() && isTouchIDEnabled()) {
					//Commented KONY API code
				if(isAuthUsingTouchSupported() && isTouchIDEnabled()){
					kony.print("FPRINT CALLING TOUCH POPUP");
					kony.print("FPRINT AT APPLAUNCH");
					//showLoadingScreen(); //not req at launch
					GBL_FLOW_ID_CA = 13;
					getUniqueID();
				} else {
					//dismissLoadingScreen();  //not req at launch
				}
			}
		} else {
             //dismissLoadingScreen();  //not req at launch
			//TMBUtil.DestroyForm(frmMBanking);
            var currForm = kony.application.getCurrentForm();
			if (currForm != null && currForm.id == "frmMBanking") {
				
				frmMBanking.show();
			}
		}
		//#endif
      
         if(isNotBlank(getContentUpdate["updatestatus"]))
            gblAppUpdateAvlble = true;
          else
            gblAppUpdateAvlble = false;
             

		checkUpgrades(getContentUpdate["updatestatus"], getContentUpdate["updateurl"]);
	}
     //dismissLoadingScreen();  //not req at launch
	kony.print("usage >> end initGlobalVars " + new Date().getTime());
}

function loadResourceBundleIB(customCallback) {
	//var deviceInfo = kony.os.deviceInfo();
	//if (gblDeviceInfo == null) {
	//gblDeviceInfo = kony.os.deviceInfo();
	//}
	if (gblDeviceInfo["name"] == "thinclient") {
		if(document.getElementById("popupIBLoading") == null)
			showIBLoadingScreen();
	} else {
		showLoadingScreen();
		registerPushCallBacks();	//for push notification purpose
	}
	var i18n_inputparam = {};
	var getcurAppLocale = kony.store.getItem("curAppLocale");
	var updatedOn = "";		// have to send all the data
	i18n_inputparam["timestamp"] = updatedOn;
	if (getcurAppLocale != null) {
		kony.i18n.setCurrentLocaleAsync(getcurAppLocale["appLocale"], onsuccess(), onfailure(), "");
		//setLocaleEng();     			
	}
	i18n_inputparam["localeId"] = kony.i18n.getCurrentLocale();	
	//#ifdef android
		i18n_inputparam["platform1"] = "M";
	//#endif

	//#ifdef iphone
		i18n_inputparam["platform1"] = "M";
	//#endif

	//#ifdef desktopweb
		i18n_inputparam["platform1"] = "D";	
	//#endif	
	
	var status = invokeServiceSecureAsync("getPhrases", i18n_inputparam, isNotBlank(customCallback) ? customCallback : callbackgetphrases);
}


function callbackgetphrases(status, getContentUpdate) {
	if (status == 400) {
		if (getContentUpdate["opstatus"] == 0) {
			dynamicCampaignBrowser();
			//localStorage.clear();
		    localStorage.removeItem("TMB_en_US");
			localStorage.removeItem("TMB_th_TH");
          
          	//For Maintenance Checking Function
			gblTimeoutNetworkInSecond = getValueOrDefault(getContentUpdate["TIMEOUT_NETWORK_IN_SECOND"], 3);  
			gblTimeoutServerDownInSecond = getValueOrDefault(getContentUpdate["TIMEOUT_SERVERDOWN_IN_SECOND"], 30);
			gblTimeoutMaintenanceInSecond = getValueOrDefault(getContentUpdate["TIMEOUT_MAINTENANACE_IN_SECOND"], 30);          
          
          	  //QR LOGIN
              gblTotalTimeVar= getContentUpdate["QR_EXPIRY_TIME"];//180;
              gblTimeVar1=getContentUpdate["QR_TIME_VARIATION1"];//20;
              gblTimeVar2=getContentUpdate["QR_TIME_VARIATION2"];//10;
              gblTimeVar3=getContentUpdate["QR_TIME_VARIATION3"];//5;
              gblNumTimeVar1=getContentUpdate["NUM_TIMES_VARIATION1"];//2;
              gblNumTimeVar2=getContentUpdate["NUM_TIMES_VARIATION2"];//2;
kony.print(" IN 1 gblTotalTimeVar="+gblTotalTimeVar+"gblTimeVar1="+gblTimeVar1+"gblTimeVar2="+gblTimeVar2+"gblTimeVar3="+gblTimeVar3+"gblNumTimeVar1="+gblNumTimeVar1+"gblNumTimeVar2="+gblNumTimeVar2);
//alert("IN 1gblTotalTimeVar="+gblTotalTimeVar+"gblTimeVar1="+gblTimeVar1+"gblTimeVar2="+gblTimeVar2+"gblTimeVar3="+gblTimeVar3+"gblNumTimeVar1="+gblNumTimeVar1+"gblNumTimeVar2="+gblNumTimeVar2);
			
			//MIB-4884-Allow special characters for My Note and Note to recipient field
			GBL_ALLOW_SPECIAL_CHAR_KEY_NAMES=getContentUpdate["ALLOW_SPECIAL_CHAR_KEY_NAMES"];
			GBL_VALID_CHARS_MY_NOTE_NOW=getContentUpdate["VALID_CHARS_MY_NOTE_NOW"];
			GBL_VALID_CHARS_MY_NOTE_FUTURE=getContentUpdate["VALID_CHARS_MY_NOTE_FUTURE"];
			
			gblSMART_FREE_TRANS_CODES=getContentUpdate["SMART_FREE_TRANS_CODES"];
			gblALL_SMART_FREE_TRANS_CODES=getContentUpdate["ALL_SMART_FREE_TRANS_CODES"];				
			gblORFT_FREE_TRANS_CODES=getContentUpdate["ORFT_FREE_TRANS_CODES"];
            gblTO_ACC_P2P_FEE_WAIVE_CODES=getContentUpdate["TO_ACC_P2P_FEE_WAIVE_PROD_CODES"];
			gblITMX_FREE_TRANS_CODES=getContentUpdate["ITMX_FEE_TRANS_CODES"];
			GLOBAL_ERROR_CODES_FOR_TOPUP = getContentUpdate["ERROR_CODES_FOR_TOPUP"];	
			GLOBAL_PDF_DOWNLOAD_URL_en_US = getContentUpdate["GLOBAL_PDF_DOWNLOAD_URL_en_US"]
			GLOBAL_IMAGE_DOWNLOAD_URL_en_US = getContentUpdate["GLOBAL_IMAGE_DOWNLOAD_URL_en_US"]
			GLOBAL_PDF_DOWNLOAD_URL_th_TH = getContentUpdate["GLOBAL_PDF_DOWNLOAD_URL_th_TH"]
			GLOBAL_IMAGE_DOWNLOAD_URL_th_TH = getContentUpdate["GLOBAL_IMAGE_DOWNLOAD_URL_th_TH"]
			GLOBAL_KONY_IDLE_TIMEOUT = getContentUpdate["KONY_IDLE_TIMEOUT"];
			GLOBAL_MB_CHANNEL = getContentUpdate["MB_CHANNEL_ID"];
			GLOBAL_IB_CHANNEL = getContentUpdate["IB_CHANNEL_ID"];
			GLOBAL_ACCT_NUM_POSITION = getContentUpdate["ACCT_NUM_POSITION"];
			GLOBAL_NUM_AT_POS_CA = getContentUpdate["NUM_AT_POS_CA"];
			GLOBAL_NUM_AT_POS_TD = getContentUpdate["NUM_AT_POS_TD"];
			GLOBAL_NUM_AT_POS_SA = getContentUpdate["NUM_AT_POS_SA"].replace(/([.*+?^=!:${}()|\[\]\/\\])/g,"");
			gblShowPinPwdSecs = kony.os.toNumber(getContentUpdate["showPinPwdSecs"]);
			gblShowPwdNo = kony.os.toNumber(getContentUpdate["showPinPwdCount"]);
			GLOBAL_INI_MAX_LIMIT_AMT = getContentUpdate["INI_MAX_LIMIT_AMT"];
			GLOBAL_CEILING_LIMIT = getContentUpdate["CEILING_LIMIT"];
			GLOBAL_MAX_LIMIT_HIST = getContentUpdate["EB_MAX_LIMIT_AMT_HIST"];
			var getcurAppLocale = kony.store.getItem("curAppLocale");
			//FATCA TnC Add
			GLOBAL_FATCA_TNC_en_US = getContentUpdate["FATCA_TERMS_CONDITIONS_ENGLISH"];
			GLOBAL_FATCA_TNC_th_TH = getContentUpdate["FATCA_TERMS_CONDITIONS_THAI"];
			
			GLOBAL_BLOCKED_TRANS = getContentUpdate["TRANS_PWD_BLOCKED"];
			GLOBAL_BLOCKED_ACCESS = getContentUpdate["ACCESS_PWD_BLOCKED"];
			GLOBAL_BLOCKED_USERIDS = getContentUpdate["USER_ID_BLOCKED"];
			GLOBAL_LOAD_MORE = getContentUpdate["GLOBAL_LOAD_MORE"];
			GLOBAL_MAX_BILL_COUNT = getContentUpdate["MAX_BILL_COUNT"];
			GLOBAL_MAX_BILL_ADD = getContentUpdate["MAX_BILL_ADD"];
			BILLER_LOGO_URL = getContentUpdate["BILLER_LOGO_URL"];
			GLOBAL_NICKNAME_LENGTH = getContentUpdate["NICKNAME_MAX_LENGTH_OPEN"];
			GLOBAL_DREAM_DESC_MAXLENGTH = getContentUpdate["DREAM_DESC_MAX_LENGTH"];
			DEFAULT_SERVICE_TIMEOUT = kony.os.toNumber(getContentUpdate["DEFAULT_SERVICE_TIMEOUT"]);
			GLOBAL_OPENACT_STARTTIME = getContentUpdate["OPENACT_STARTTIME"];
			GLOBAL_OPENACT_ENDTIME = getContentUpdate["OPENACT_ENDTIME"];
			GLOBAL_EDITDREAM_STARTTIME = getContentUpdate["EDIT_DREAM_STARTTIME"];
			GLOBAL_EDITDREAM_ENDTIME = getContentUpdate["EDIT_DREAM_ENDTIME"];
			GLOBAL_ACTIVATION_HELP_TEXT_EN = getContentUpdate["MB_ACTIVATION_CODE_HELP_TEXT_EN"];
			GLOBAL_ACTIVATION_HELP_TEXT_TH = getContentUpdate["MB_ACTIVATION_CODE_HELP_TEXT_TH"];
			GLOBAL_REACTIVATION_HELP_TEXT_EN = getContentUpdate["MB_REACTIVATION_CODE_HELP_TEXT_EN"];
			GLOBAL_REACTIVATION_HELP_TEXT_TH = getContentUpdate["MB_REACTIVATION_CODE_HELP_TEXT_TH"];
			//E-STATEMENT
			GLOBAL_E_STATEMENT_PRODUCTS = getContentUpdate["E_STATEMENT_PRODUCTS"];
			GLOBAL_E_STATEMENT_IMAGES = getContentUpdate["E_STATEMENT_IMAGES"];
			Gbl_Image_Size_Limit = getContentUpdate["Max_FileSize"];
			if(getContentUpdate["Start_Digs_MobileNum"] != null && getContentUpdate["Start_Digs_MobileNum"] != undefined && getContentUpdate["Start_Digs_MobileNum"] != "")
				Gbl_StartDigsMobileNum = getContentUpdate["Start_Digs_MobileNum"];
			else 	Gbl_StartDigsMobileNum = "09,08,06";    // Initialising with default.
			Gbl_StartDigsMobileNum.split(',');
			if(getContentUpdate["FATCA_MAX_SKIP_COUNTER"] != null && getContentUpdate["FATCA_MAX_SKIP_COUNTER"] != undefined && getContentUpdate["FATCA_MAX_SKIP_COUNTER"] != "")
				GLOBAL_FATCA_MAX_SKIP_COUNT = parseInt(getContentUpdate["FATCA_MAX_SKIP_COUNTER"]);
			else 	GLOBAL_FATCA_MAX_SKIP_COUNT = 3;
			GLOBAL_JOINT_CODES = getContentUpdate["JOINT_ACCNT_CODES"];
			GLOBAL_MB_APPLY_CODES = getContentUpdate["APPLY_MB_STATUS_CODES"];
			Gbl_Prod_Code_MEACC = getContentUpdate["Prod_Code_MEACC"];
			GLOBAL_IB_ACTIVATION_HELP_TEXT_EN = getContentUpdate["IB_ACTIVATION_HELP_TEXT_EN"];
			GLOBAL_IB_ACTIVATION_HELP_TEXT_TH = getContentUpdate["IB_ACTIVATION_HELP_TEXT_TH"];
			GLOBAL_IB_REACTIVATION_HELP_TEXT_EN = getContentUpdate["IB_REACTIVATION_HELP_TEXT_EN"];
			GLOBAL_IB_REACTIVATION_HELP_TEXT_TH = getContentUpdate["IB_REACTIVATION_HELP_TEXT_TH"];
			firstTimeActivationPopUp = getContentUpdate["FirstTimeActivation_Popup"];
			GLOBAL_TODAY_DATE = getContentUpdate["TODAY_DATE"]; //To show the server date on Calendar
			//ENH_209 start
			GLOBAL_Exchange_Rates_Link_EN=getContentUpdate["Exchange_Rates_Link_EN"];
			GLOBAL_Exchange_Rates_Link_TH=getContentUpdate["Exchange_Rates_Link_TH"];
			//ENH_209 end
			//ENH_230 start
			GLOBAL_SecurityAdvice_Link_EN=getContentUpdate["SecurityAdvice_Link_EN"];
			GLOBAL_SecurityAdvice_Link_TH=getContentUpdate["SecurityAdvice_Link_TH"];
			//ENH_230 end
			
			// Online Payment based on MWA
			GLOBAL_ONLINE_PAYMENT_VERSION_MWA=getContentUpdate["ONLINE_PAYMENT_VERSION_MWA"];
          	//UV Status Flag
            GLOBAL_UV_STATUS_FLAG = getContentUpdate["UV_STATUS_FLAG"];
          	//handling login QR button based on UV status flag
          frmIBPreLogin.btnQRLogin.setVisibility(true);
            /*if (GLOBAL_UV_STATUS_FLAG == "ON") {
	           frmIBPreLogin.btnQRLogin.setVisibility(true);
	          }else{
	            frmIBPreLogin.btnQRLogin.setVisibility(false);
            }*/
          
            //RTP Enable Flag
            GLOBAL_RTP_ENABLE = getContentUpdate["RTP_ENABLE"];
            //MF Temenos ON/OFF Support Flag
  			GBL_MF_TEMENOS_ENABLE_FLAG = getContentUpdate["MF_TERMINOS_FLAG"];
          
           //MF Suitability ON/OFF Support Flag
  			GBL_MF_SUITABILITY_FLAG = getContentUpdate["MF_SUITABILITY_FLAG"];
  			
  			//Email Pattern
  			GBL_EMAIL_REG_PATTERN = getContentUpdate["EMAIL_REG_PATTERN"];

			GLOBAL_LOGINQR_PARAMID = getContentUpdate["LOGINQR_PARAMID"];
          
           // Online Bill Payment for PEA
  			gblEA_BILLER_COMP_CODES = getContentUpdate["EA_BILLER_COMPCODES"];
  			gblPEA_BILLER_COMP_CODE = getContentUpdate["PEA_BILLER_COMPCODE"];
			// MF migration Change : -- Moving campaign prelogin service call to here..
			if(isMFEnabled == true && !flowSpa && kony.application.getCurrentForm().id == "frmIBPreLogin")
			{
				campaignPreLoginService("segCampaignImage","frmIBPreLogin","I");
			}
			if (getContentUpdate["Results"] != null){
				if(getContentUpdate["Results"][0]["EnglishPhrases"].length > 0){
					try{
						//kony.i18n.updateResourceBundle(getContentUpdate["Results"][0]["EnglishPhrases"][0], "en_US");
						setTimeout(function(){kony.i18n.updateResourceBundle(getContentUpdate["Results"][0]["EnglishPhrases"][0], "en_US")},5000);
					}catch(i18nError){
						alert("Exception While getting updateResourceBundle english  : "+i18nError );
					}
				}		
				if(getContentUpdate["Results"][1]["ThaiPhrases"].length > 0) {
					try{
						//kony.i18n.updateResourceBundle(getContentUpdate["Results"][1]["ThaiPhrases"][0], "th_TH");
						setTimeout(function(){kony.i18n.updateResourceBundle(getContentUpdate["Results"][1]["ThaiPhrases"][0], "th_TH")},2000);
					}catch(i18nError){
						alert("Exception While getting updateResourceBundle thai  : "+i18nError );
					}
				}
				//kony.i18n.updateResourceBundle(getContentUpdate["Results"][0]["EnglishPhrases"][0], "en_US");
				//kony.i18n.updateResourceBundle(getContentUpdate["Results"][1]["ThaiPhrases"][0], "th_TH");
				
				//#ifdef android
					var getEncrKeyFromDevice = kony.store.getItem("encrytedText");
					if (getEncrKeyFromDevice != null) {
						//TMBUtil.DestroyForm(frmAfterLogoutMB);
						if (kony.application.getCurrentForm() == frmMBPreLoginAccessesPin) { //frmAfterLogoutMB
							frmMBPreLoginAccessesPin.show(); //frmAfterLogoutMB
						}
					} else {
						//TMBUtil.DestroyForm(frmMBanking);
						if (kony.application.getCurrentForm() == frmMBanking) {
							frmMBanking.show();
						}
					}
				//#endif
			}
			frmIBPreLogin.btnQRLogin.text = kony.i18n.getLocalizedString("IB_btnQR");
		} else {
			
		}
		dismissLoadingScreen();
	}
}

function callbackgetphrasesMB(status, getContentUpdate) {
	kony.print(">> callbackgetphrasesMB");
	if (status == 400) {
       gblgetPhrasesCalled = false;
        // dismissLoadingScreen(); //not req at launch
		if (getContentUpdate["opstatus"] == 0) {
            //Store the getPhrases response in device store
           kony.store.setItem("getContentUpdate", getContentUpdate);
           dynamicCampaignBrowser();
           initGlobalsFromTPhrases(getContentUpdate);
          
			if (getContentUpdate["Results"] != null) {
				if (getContentUpdate["Results"][0]["EnglishPhrases"].length > 0) {
					try {
						    kony.print("updating resourcebundle in eng");
							kony.i18n.updateResourceBundle(getContentUpdate["Results"][0]["EnglishPhrases"][0], "en_US");
						//}
					} catch (i18nError) {
						alert("Exception While getting updateResourceBundle english  : " + i18nError);
					}
				}

				if (getContentUpdate["Results"][1]["ThaiPhrases"].length > 0) {
					try {
						    kony.print("updating resourcebundle in thai");
							kony.i18n.updateResourceBundle(getContentUpdate["Results"][1]["ThaiPhrases"][0], "th_TH");
						//}
					} catch (i18nError) {
						alert("Exception While getting updateResourceBundle thai  : " + i18nError);
					}
				}

				//kony.i18n.updateResourceBundle(getContentUpdate["Results"][0]["EnglishPhrases"][0], "en_US");
				//kony.i18n.updateResourceBundle(getContentUpdate["Results"][1]["ThaiPhrases"][0], "th_TH");
				//#ifdef android
				var getEncrKeyFromDevice = kony.store.getItem("encrytedText");
				if (getEncrKeyFromDevice != null) {
					//TMBUtil.DestroyForm(frmAfterLogoutMB);
					if (kony.application.getCurrentForm() == frmMBPreLoginAccessesPin) { //frmAfterLogoutMB
						//dismissLoadingScreen();
						//frmMBPreLoginAccessesPin.show(); //frmAfterLogoutMB

						//Reverted Commented FFI Code
						//if (isAuthUsingTouchSupported() && hasRegisteredFinger() && isTouchIDEnabled()) {
							//Commented KONY API code
						if(isAuthUsingTouchSupported() && isTouchIDEnabled()){
							kony.print("FPRINT CALLING TOUCH POPUP");
							kony.print("FPRINT AT APPLAUNCH");
							//showLoadingScreen();  //not req at launch
							GBL_FLOW_ID_CA = 13;
							getUniqueID();
						} else {
							//dismissLoadingScreen(); //not req at launch
						}
					}
				} else {
					//TMBUtil.DestroyForm(frmMBanking);
                    dismissLoadingScreen();
					if (kony.application.getCurrentForm() == frmMBanking) {
						//dismissLoadingScreen();  //not req at launch
						frmMBanking.show();
					}
				}
				//#endif
                
                if(isNotBlank(getContentUpdate["updatestatus"])){
                   gblAppUpdateAvlble = true;
                   kony.print("gblAppUpdateAvlble is set true");
                }else{
                  gblAppUpdateAvlble = false;
                }
                  
              
				checkUpgrades(getContentUpdate["updatestatus"], getContentUpdate["updateurl"]);
			}
			//#ifdef iphone
			if (!isSignedUser) {
				showTouchIdPopupUsingKonyAPI();
			}
			//#endif

		} else {
			//dismissLoadingScreen(); //not req at launch
		}
	}
	kony.print("usage >> end loadResourceBundle " + new Date().getTime());
}

function checkUpgrades(updatestatus, updateurl) {
	if(updatestatus != null) {
		//global variable
		GLOBAL_UPDATE_URL = updateurl;
		if(updatestatus == "optional") { 
	    	//window.Alert(getLocaleSpecificText("update_available"), updateAvailableAlerthandler, "confirmation", getLocaleSpecificText("btn_confirm"),getLocaleSpecificText("btn_cancel"),getLocaleSpecificText("alert_citi_mobile"))
			var message = kony.i18n.getLocalizedString("optional_upgrade_message");
			if( message == null || message == "") {
				message = "A new version of TMB Mobile application has been released. Choose OK to update the application or Cancel to continue with sign on.";
			}

			var title = kony.i18n.getLocalizedString("optional_upgrade_title");
			var yeslbl = kony.i18n.getLocalizedString("optional_upgrade_yes_label");
			var nolbl = kony.i18n.getLocalizedString("optional_upgrade_no_label");
			
			if(title == null || title == "") title = "Upgrade Information";
			if(yeslbl == null || yeslbl == "") yeslbl = "Upgrade";
			if(nolbl == null || nolbl == "") nolbl = "Cancel";
			
			var basicConf = {
				message: message,
				alertType: constants.ALERT_TYPE_CONFIRMATION,
				alertTitle: title,
				yesLabel: yeslbl,
				noLabel: nolbl,
				alertHandler: callbackOptionalUpgrade
			};
			
			//Defining pspConf parameter for alert
			var pspConf = {};
			
			//Alert definition
			var infoAlert = kony.ui.Alert(basicConf, pspConf);
	    } else if(updatestatus == "mandatory") {
			var message = kony.i18n.getLocalizedString("mandatory_upgrade_message");
			if( message == null || message == "") {
				message = "Your current application is running an outdated version. The application will now exit and direct you to update site.";
			}
			
			var title = kony.i18n.getLocalizedString("mandatory_upgrade_title");
			var yeslbl = kony.i18n.getLocalizedString("mandatory_upgrade_yes_label");
			var nolbl = kony.i18n.getLocalizedString("mandatory_upgrade_no_label");
			
			if(title == null || title == "") title = "Upgrade Error";
			if(yeslbl == null || yeslbl == "") yeslbl = "OK";
			if(nolbl == null || nolbl == "") nolbl = "Exit";
			
			var basicConf = {
				message: message,
				alertType: constants.ALERT_TYPE_CONFIRMATION,
				alertTitle: title,
				yesLabel: yeslbl,
				noLabel: nolbl,
				alertHandler: callbackMandatoryUpgrade
			};
			
			//Defining pspConf parameter for alert
			var pspConf = {};
			
			//Alert definition
			var infoAlert = kony.ui.Alert(basicConf, pspConf);
	    }
	}
}


function callbackMandatoryUpgrade(response) {
	if (response == true) {
		//alert("Need to implement the code");
		//GLOBAL_UPDATE_URL now available
		if(GLOBAL_UPDATE_URL != null && GLOBAL_UPDATE_URL != "" && GLOBAL_UPDATE_URL != undefined) {
			//window.open(GLOBAL_UPDATE_URL);
	        //frmFBLogin.browserFB.clearHistory();
	        //TMBUtil.DestroyForm(frmFBLogin);
		    
		    //frmFBLogin.browserFB.url = GLOBAL_UPDATE_URL;
		    //frmFBLogin.show();
		    if (gblDeviceInfo["name"] == "android") {
		    	  var PlayStoreLinkObject = new Linker.PlayStoreLink();
   				  PlayStoreLinkObject.displayPlayStore("com.TMBTOUCH.PRODUCTION");
		    }
		    else
		    {
		    	kony.application.openURL(GLOBAL_UPDATE_URL);
		    }
		    kony.application.exit();
		} else {
			var message = "";
			//if(kony.os.deviceInfo().name == "android") {
			//#ifdef android
				message = kony.i18n.getLocalizedString("upgrade_from_google_store"); 
				if(message == null || message == "") message = "Please update the application from Google play store.";
			//#else
				message = kony.i18n.getLocalizedString("upgrade_from_apple_store"); 
				if(message == null || message == "") message = "Please update the application from Apple iStore.";
			//#endif
			alert(message);
		}
	} else {
		kony.application.exit();
	}
}

function callbackOptionalUpgrade(response) {
	if (response == true) {
		//alert("Need to implement the code");
		if(GLOBAL_UPDATE_URL != null && GLOBAL_UPDATE_URL != "" && GLOBAL_UPDATE_URL != undefined) {
		  //window.open(GLOBAL_UPDATE_URL);
            //frmFBLogin.browserFB.clearHistory();
            //TMBUtil.DestroyForm(frmFBLogin);
            
            //frmFBLogin.browserFB.url = GLOBAL_UPDATE_URL;
            //frmFBLogin.show();
            
           	if (gblDeviceInfo["name"] == "android") {
		    	  var PlayStoreLinkObject = new Linker.PlayStoreLink();
   				  PlayStoreLinkObject.displayPlayStore("com.TMBTOUCH.PRODUCTION");
		    }
		    else
		    {
		    	kony.application.openURL(GLOBAL_UPDATE_URL);
		    }
		} else {
		    var message = "";
            //if(kony.os.deviceInfo().name == "android") {
			//#ifdef android
                message = kony.i18n.getLocalizedString("upgrade_from_google_store"); 
                if(message == null || message == "") message = "Please update the application from Google play store.";
            //} else {
			//#else
                message = kony.i18n.getLocalizedString("upgrade_from_apple_store"); 
                if(message == null || message == "") message = "Please update the application from Apple iStore.";
            //#endif
            alert(message);
		}
	} else {
		//Do Nothing, so that exit form will be shown as is.
	}
}

gblBrwCmpObject = "";
gblLnkCmpObject = "";


function dynamicCampaignBrowser(){
      gblBrwCmpObject= "";
      gblLnkCmpObject= "";
      gblVbxCmp= "";
      
      gblBrwCmpObject = new kony.ui.Browser({
        "id": "gblBrwCmpObject",
        "text": "",
        "isVisible": true,
        "htmlString": "",
        "detectTelNumber": false
    }, {
        "margin": [0, 0, 0, 0],
        "containerHeight": null,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 54
    }, {});
    
    gblVbxCmp = new kony.ui.Box({
        "id": "gblVbxCmp",
        "isVisible": true,
        "orientation": constants.BOX_LAYOUT_VERTICAL
    }, {
        "containerWeight": 100,
        "margin": [0, 0, 0, 0],
        "padding": [0, 0, 0, 0],
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "vExpand": false,
        "hExpand": true,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {});
    //gblVbxCmp.add(gblBrwCmpObject, gblLnkCmpObject);
}

function setDefaultUVSectionDisplayValue(){
  	gblUVDetailsDisplaySectionB = ["M01","M02","M03","M04","M05","M06","M07","M08","M09","M10","M11","M12","M13","M14","M15","M36"];
    gblUVDetailsDisplaySectionC = ["M01","M02","M03","M04","M05","M06","M07","M08","M09","M10","M11","M12","M13","M14","M15","M36"];
    gblUVDetailsDisplaySectionD = ["M01","M02","M03","M04","M05","M06","M07","M08","M09","M10","M11","M12","M36"];
    gblUVDetailsDisplaySectionE = ["M16","M17","M18","M19","M20","M21","M22","M23","M24","M25","M26","M27","M28","M29","M30","M31","M32","M33","M34","M35", "M37"];
    gblUVTransTypeTransfer = ["M01","M02","M03","M04","M05","M06","M07","M10"];
    gblUVTransTypeBillPay = ["M08","M11","M36"];
    gblUVTransTypeTopUp = ["M09","M12"];
    gblUVTransTypeOpenAcct = ["M13","M14"];
    gblUVTransTypeWithdraw = ["M15"];
    gblUVTransTypeProfile = ["M16","M17","M18","M19","M20","M21","M22","M23","M24"];
    gblUVTransTypePromptPay = ["M25","M26","M27"];
    gblUVTransTypeCardMangt = ["M28","M29","M30","M31"];
    gblUVTransTypeCheque = ["M32"];
    gblUVTransTypeMutualFund = ["M33","M34"];
    gblUVTransTypeAuth = ["M35", "M37"];
}


function getUpdatedPhrases(){ 
 var updatedOn = kony.i18n.getLocalizedString("keyphrasesUpdatedOn");
 kony.print("keyphrasesUpdatedOn:-------->"+updatedOn);
 gblFirtTimeLaunch = false;
   if(isNotBlank(updatedOn) && updatedOn.indexOf('-') != -1){
      updatedOnDt = updatedOn.substr(0,updatedOn.indexOf('-'));
      kony.print("updatedOnDt---------->"+updatedOnDt);
      if(kony.string.startsWith(updatedOn, "01-JAN-13")){
        gblFirtTimeLaunch = true;
      }
    }
 var i18n_inputparam = {};
 i18n_inputparam["timestamp"] = updatedOn;


	i18n_inputparam["localeId"] = kony.i18n.getCurrentLocale();
	
	//#ifdef android
		i18n_inputparam["platform1"] = "M";
	//#endif

	//#ifdef iphone
		i18n_inputparam["platform1"] = "M";
	//#endif
  showLoadingScreen();
  var status = invokeServiceSecureAsync("NewGetPhrases", i18n_inputparam, callbacknewgetphrasesMB);
}
  
  
 function callbacknewgetphrasesMB(status, getContentUpdate) {
   if (status == 400) {
         dismissLoadingScreen();
		if (getContentUpdate["opstatus"] == 0) {
            //Store the getPhrases response in device store
           kony.store.setItem("getContentUpdate", getContentUpdate);
           initGlobalsFromTPhrases(getContentUpdate);			
			if (getContentUpdate["Results"] != null) {
				if (getContentUpdate["Results"][0]["EnglishPhrases"].length > 0) {
					try {
						    kony.print("updating resourcebundle in eng");
							kony.i18n.updateResourceBundle(getContentUpdate["Results"][0]["EnglishPhrases"][0], "en_US");
						//}
					} catch (i18nError) {
						alert("Exception While getting updateResourceBundle english  : " + i18nError);
					}
				}

				if (getContentUpdate["Results"][1]["ThaiPhrases"].length > 0) {
					try {
						    kony.print("updating resourcebundle in thai");
							kony.i18n.updateResourceBundle(getContentUpdate["Results"][1]["ThaiPhrases"][0], "th_TH");
						//}
					} catch (i18nError) {
						alert("Exception While getting updateResourceBundle thai  : " + i18nError);
					}
				}

			}

		} else {
			dismissLoadingScreen();
		}
      try{		
        dismissLoadingScreen();
        kony.print("Before showing login form");
        if(kony.store.getItem("encrytedText") != null) {
			 //TMBUtil.DestroyForm(frmMBPreLoginAccessesPin);
             gblApplicationLaunch = true;
			 frmMBPreLoginAccessesPin.show(); //frmAfterLogoutMB
		 } else {
               frmMBanking.show();
          }
	    }catch(e){
          kony.print("Error occured while showing login"+e.message);
   	  }
	}
 }
 
 function initGlobalsFromTPhrases(getContentUpdate){
     kony.print("inside initGlobalsFromTPhrases start");
           gblCallPhrasesFlag = getContentUpdate["CallPhrasesOnInitFlag"];
			//dynamicCampaignBrowser();
			gblRedirect = false;
			gblrefcontweight = "";
			gblrefcontwidth = "";
			gblcontheight = "";
			
			//For Maintenance Checking Function
			gblTimeoutNetworkInSecond = getValueOrDefault(getContentUpdate["TIMEOUT_NETWORK_IN_SECOND"], 3);  
			gblTimeoutServerDownInSecond = getValueOrDefault(getContentUpdate["TIMEOUT_SERVERDOWN_IN_SECOND"], 30);
			gblTimeoutMaintenanceInSecond = getValueOrDefault(getContentUpdate["TIMEOUT_MAINTENANACE_IN_SECOND"], 30);
            
			//MIB-4884-Allow special characters for My Note and Note to recipient field
			GBL_ALLOW_SPECIAL_CHAR_KEY_NAMES = getContentUpdate["ALLOW_SPECIAL_CHAR_KEY_NAMES"];		
			GLOBAL_PDF_DOWNLOAD_URL_en_US = getContentUpdate["GLOBAL_PDF_DOWNLOAD_URL_en_US"]
			GLOBAL_IMAGE_DOWNLOAD_URL_en_US = getContentUpdate["GLOBAL_IMAGE_DOWNLOAD_URL_en_US"]
			GLOBAL_PDF_DOWNLOAD_URL_th_TH = getContentUpdate["GLOBAL_PDF_DOWNLOAD_URL_th_TH"]
			GLOBAL_IMAGE_DOWNLOAD_URL_th_TH = getContentUpdate["GLOBAL_IMAGE_DOWNLOAD_URL_th_TH"]
            kony.print("after download vars");
				
			GLOBAL_MB_CHANNEL = getContentUpdate["MB_CHANNEL_ID"];
			GLOBAL_IB_CHANNEL = getContentUpdate["IB_CHANNEL_ID"];
			GLOBAL_ACCT_NUM_POSITION = getContentUpdate["ACCT_NUM_POSITION"];
			GLOBAL_NUM_AT_POS_CA = getContentUpdate["NUM_AT_POS_CA"];
			GLOBAL_NUM_AT_POS_TD = getContentUpdate["NUM_AT_POS_TD"];
			GLOBAL_NUM_AT_POS_SA = getContentUpdate["NUM_AT_POS_SA"].replace(/([.*+?^=!:${}()|\[\]\/\\])/g,"");
			gblShowPinPwdSecs = kony.os.toNumber(getContentUpdate["showPinPwdSecs"]);
			gblShowPwdNo = kony.os.toNumber(getContentUpdate["showPinPwdCount"]);
			gblShowPwd = kony.os.toNumber(getContentUpdate["showPinPwdCount"]);
			gblShowPinPwdSecsFinal = kony.os.toNumber(getContentUpdate["showPinPwdSecs"]);
			gblShowPwdNoFinal = kony.os.toNumber(getContentUpdate["showPinPwdCount"]);
			gblShowPwdFinal = kony.os.toNumber(getContentUpdate["showPinPwdCount"]);
			
			var getcurAppLocale = kony.store.getItem("curAppLocale");
			GLOBAL_TERMS_CONDITIONS_th_TH_1 = getContentUpdate["TERMS_CONDITIONS_THAI_1"];
			GLOBAL_TERMS_CONDITIONS_th_TH_2 = getContentUpdate["TERMS_CONDITIONS_THAI_2"];
			GLOBAL_TERMS_CONDITIONS_th_TH_3 = getContentUpdate["TERMS_CONDITIONS_THAI_3"];
			GLOBAL_TERMS_CONDITIONS_en_US = getContentUpdate["TERMS_CONDITIONS_ENGLISH"];
			

			GLOBAL_BLOCKED_TRANS = getContentUpdate["TRANS_PWD_BLOCKED"];
			GLOBAL_BLOCKED_ACCESS = getContentUpdate["ACCESS_PWD_BLOCKED"];
			GLOBAL_BLOCKED_USERIDS = getContentUpdate["USER_ID_BLOCKED"];
            kony.print("after blocked vars");
			
			DEFAULT_SERVICE_TIMEOUT = kony.os.toNumber(getContentUpdate["DEFAULT_SERVICE_TIMEOUT"]);
			GLOBAL_OPENACT_STARTTIME = getContentUpdate["OPENACT_STARTTIME"];
			GLOBAL_OPENACT_ENDTIME = getContentUpdate["OPENACT_ENDTIME"];
             kony.print("after timeout vars");
			
			GLOBAL_ACTIVATION_HELP_TEXT_EN = getContentUpdate["MB_ACTIVATION_CODE_HELP_TEXT_EN"];
			GLOBAL_ACTIVATION_HELP_TEXT_TH = getContentUpdate["MB_ACTIVATION_CODE_HELP_TEXT_TH"];
			GLOBAL_REACTIVATION_HELP_TEXT_EN = getContentUpdate["MB_REACTIVATION_CODE_HELP_TEXT_EN"];
			GLOBAL_REACTIVATION_HELP_TEXT_TH = getContentUpdate["MB_REACTIVATION_CODE_HELP_TEXT_TH"];
            kony.print("After activation vars");

			//QR LOGIN
			gblTotalTimeVar = getContentUpdate["QR_EXPIRY_TIME"]; //180;
			gblTimeVar1 = getContentUpdate["QR_TIME_VARIATION1"]; //20;
			gblTimeVar2 = getContentUpdate["QR_TIME_VARIATION2"]; //10;
			gblTimeVar3 = getContentUpdate["QR_TIME_VARIATION3"]; //5;
			gblNumTimeVar1 = getContentUpdate["NUM_TIMES_VARIATION1"]; //2;
			gblNumTimeVar2 = getContentUpdate["NUM_TIMES_VARIATION2"]; //2;
          	KPNS_REFRESH = getContentUpdate["KPNS_REFRESH"];
			kony.print(" IN 2 gblTotalTimeVar=" + gblTotalTimeVar + "gblTimeVar1=" + gblTimeVar1 + "gblTimeVar2=" + gblTimeVar2 + "gblTimeVar3=" + gblTimeVar3 + "gblNumTimeVar1=" + gblNumTimeVar1 + "gblNumTimeVar2=" + gblNumTimeVar2);
			gblLogiflowOptimisation = getContentUpdate["SWITCH_LOGIN_OPT_FLAG"];
          	//alert("IN 2gblTotalTimeVar="+gblTotalTimeVar+"gblTimeVar1="+gblTimeVar1+"gblTimeVar2="+gblTimeVar2+"gblTimeVar3="+gblTimeVar3+"gblNumTimeVar1="+gblNumTimeVar1+"gblNumTimeVar2="+gblNumTimeVar2);
			//E-STATEMENT
			GLOBAL_E_STATEMENT_PRODUCTS = getContentUpdate["E_STATEMENT_PRODUCTS"];
			GLOBAL_E_STATEMENT_IMAGES = getContentUpdate["E_STATEMENT_IMAGES"];
			
			
			GBL_TOUCH_ID_STATUS_COUNT = getContentUpdate["TOUCH_ID_COUNT"];
			gblQuickBalanceEnable = getContentUpdate["QUICK_BALANCE_ENABLE"];
			gblShowMyQEGenerator = getContentUpdate["SHOW_MYQR_GENERATOR"];
			gblShowlblQRDisclaimer = getContentUpdate["SHOW_QRDisclaimer"];
			gblShowQRPayslip = getContentUpdate["SHOW_QRPAYSLIP"];
			
			//MF Temenos ON/OFF Support Flag
  			GBL_MF_TEMENOS_ENABLE_FLAG = getContentUpdate["MF_TERMINOS_FLAG"];
  			//MF Suitability ON/OFF Support Flag
  			GBL_MF_SUITABILITY_FLAG = getContentUpdate["MF_SUITABILITY_FLAG"];
  			
  			//Email Pattern
  			GBL_EMAIL_REG_PATTERN = getContentUpdate["EMAIL_REG_PATTERN"];
			
			gblEDonationAmounts = getContentUpdate["E-DONATION_AMOUNTS"];
   			gblEDonationMaxLimiAmount = parseInt(getContentUpdate["E-DONATION_MAX_AMOUNT"]);
   			gblEDMaxLimitAmt = parseInt(getContentUpdate["E-DONATION_MAX_AMOUNT"]);
			//E-KYC status flag check
           GLOBAL_EKYC_ENABLE_FLAG=getContentUpdate["CHECK_EKYC_ENABLE_FLAG"];
           GLOBAL_EKYC_CID_FLAG=getContentUpdate["EKYC_CID_FLAG"];
   			GLOBAL_NDID_FLAG=getContentUpdate["NDID_FLAG"];
   
			//UV Status Flag
			GLOBAL_UV_STATUS_FLAG = getContentUpdate["UV_STATUS_FLAG"];
			//RTP Enable Flag
			GLOBAL_RTP_ENABLE = getContentUpdate["RTP_ENABLE"];
			gblUVLoadingIndicatorTimeOut = getContentUpdate["UV_ACTIVATION_LOADING_TIME"];
			GLOBAL_LOGINQR_PARAMID = getContentUpdate["LOGINQR_PARAMID"];
			kony.print("GLOBAL_UV_STATUS_FLAG >>>" + GLOBAL_UV_STATUS_FLAG);
			//Setting UP Vtap SDK
			if (GLOBAL_UV_STATUS_FLAG == "ON") {
				gblVtapServerURL = getContentUpdate["VKeyVtapServer_URL"];
				gblProvServerURL = getContentUpdate["VKeyProvisioningServer_URL"];
				gblPkiServerURL = getContentUpdate["VkeyPKIServer_URL"];
            
            	/*
				//#ifdef iphone
            		kony.print("INSIDE SetupVTAPSDK iPhone......");
					setUpVTapSDK();
				//#endif
              */
            	
			}
        

			/*if (gblDeviceInfo["name"] == "thinclient") {
				kony.application.showLoadingScreen("frmLoading", "", constants.LOADING_SCREEN_POSITION_FULL_SCREEN, true, false, null);
			} else { */
              //kony.print("showing loading indicator???");
				//showLoadingScreen();
				// registerPushCallBacks();	//for push notification purpose
			//}

			if (getContentUpdate["MENUCONFIG"] != null || getContentUpdate["MENUCONFIG"] != undefined) {
				gblMenuConfig = getContentUpdate["MENUCONFIG"];
			} else {
				gblMenuConfig = "";
			}
			//kony.print("gblQuickBalanceEnable:::::"+gblQuickBalanceEnable)
			if (gblQuickBalanceEnable != "ON") {

				showQuickBalanceButton();
			}/*else{
              	frmMBPreLoginAccessesPin.FlexContainerDefault.setVisibility(true);
              	frmMBPreLoginAccessesPin.FlexContainerTwo.setVisibility(false);
            }*/
		//	if (isMFEnabled == true && flowSpa)
		//		campaignPreLoginService("imgMBPreLogin", "frmSPALogin", "M");		
			

			GLOBAL_IB_ACTIVATION_HELP_TEXT_EN = getContentUpdate["IB_ACTIVATION_HELP_TEXT_EN"];
			GLOBAL_IB_ACTIVATION_HELP_TEXT_TH = getContentUpdate["IB_ACTIVATION_HELP_TEXT_TH"];
			GLOBAL_IB_REACTIVATION_HELP_TEXT_EN = getContentUpdate["IB_REACTIVATION_HELP_TEXT_EN"];
			GLOBAL_IB_REACTIVATION_HELP_TEXT_TH = getContentUpdate["IB_REACTIVATION_HELP_TEXT_TH"];
			firstTimeActivationPopUp = getContentUpdate["FirstTimeActivation_Popup"];
			//var deviceInfo = kony.os.deviceInfo();
			GLOBAL_TODAY_DATE = getContentUpdate["TODAY_DATE"];
			//ENH_209 start
			GLOBAL_Exchange_Rates_Link_EN = getContentUpdate["Exchange_Rates_Link_EN"];
			GLOBAL_Exchange_Rates_Link_TH = getContentUpdate["Exchange_Rates_Link_TH"];
			//ENH_209 end
			//ENH_230 start
			
			MBTnC = false;
			//ENH_230 end		

			gblUVDetailsDisplaySectionB = getContentUpdate["UV_TRANS_TYPE_DISPLAY_SECT_B"];
			gblUVDetailsDisplaySectionC = getContentUpdate["UV_TRANS_TYPE_DISPLAY_SECT_C"];
			gblUVDetailsDisplaySectionD = getContentUpdate["UV_TRANS_TYPE_DISPLAY_SECT_D"];
			gblUVDetailsDisplaySectionE = getContentUpdate["UV_TRANS_TYPE_DISPLAY_SECT_E"];
			gblUVTransTypeTransfer = getContentUpdate["UV_TRANS_TYPE_TRANSFER"];
			gblUVTransTypeBillPay = getContentUpdate["UV_TRANS_TYPE_BILLPAY"];
			gblUVTransTypeTopUp = getContentUpdate["UV_TRANS_TYPE_TOPUP"];
			gblUVTransTypeOpenAcct = getContentUpdate["UV_TRANS_TYPE_OPENACCT"];
			gblUVTransTypeWithdraw = getContentUpdate["UV_TRANS_TYPE_WITHDRAW"];
			gblUVTransTypeProfile = getContentUpdate["UV_TRANS_TYPE_PROFILE"];
			gblUVTransTypePromptPay = getContentUpdate["UV_TRANS_TYPE_PROMPTPAY"];
			gblUVTransTypeCardMangt = getContentUpdate["UV_TRANS_TYPE_CARDMNG"];
			gblUVTransTypeCheque = getContentUpdate["UV_TRANS_TYPE_CHEQUE"];
			gblUVTransTypeMutualFund = getContentUpdate["UV_TRANS_TYPE_MUTUALFUND"];
			gblUVTransTypeAuth = getContentUpdate["UV_TRANS_TYPE_AUTHEN"];
   
            //Account Summary page refresh interval time in seconds
            GLOBAL_ACCOUNT_SUMMARY_REFRESH_INTERVAL = getContentUpdate["ACCOUNT_SUMMARY_REFRESH_INTERVAL"];
    
            //Get Phrases flag
            GLOBAL_GET_CAMPAIGN_FLAG = getContentUpdate["GET_CAMPAIGN_FLAG"];
			
			//For MasterBiller service call Enable/Disable
            GET_MASTER_BILLER_REFRESH_FLAG = getContentUpdate["GET_MASTER_BILLER_REFRESH_FLAG"];
			
			//For MyBiller service call Enable/Disable
            GET_MY_BILLER_REFRESH_FLAG = getContentUpdate["GET_MY_BILLER_REFRESH_FLAG"];
    

			if (!isNotBlank(gblUVDetailsDisplaySectionB) || !isNotBlank(gblUVDetailsDisplaySectionC)
				 || !isNotBlank(gblUVDetailsDisplaySectionD) || !isNotBlank(gblUVDetailsDisplaySectionE)) {
				kony.print(">> setDefaultUVSectionDisplayValue");
				setDefaultUVSectionDisplayValue();
			}
      kony.print("inside initGlobalsFromTPhrases end");
 }




/*
 * Call GetPhrases once per day
*/
function callGetPhrasesOnForeground() {
    var updatedOn = kony.i18n.getLocalizedString("keyphrasesUpdatedOn");
    var currDt = new Date();
    var updatedOnDt = "";
    kony.print("index of updated on dt: " + updatedOn.indexOf('-'));
    if (isNotBlank(updatedOn) && updatedOn.indexOf('-') != -1) {
        updatedOnDt = updatedOn.substr(0, updatedOn.indexOf('-'));
        kony.print("updatedOnDt---------->" + updatedOnDt);
    }

    var i18n_inputparam = {};
    i18n_inputparam["timestamp"] = updatedOn;
    i18n_inputparam["localeId"] = kony.i18n.getCurrentLocale();
    i18n_inputparam["platform1"] = "M";
    var getContentUpdate = kony.store.getItem("getContentUpdate");
    if (getContentUpdate != null)
        gblCallPhrasesFlag = getContentUpdate["CallPhrasesOnInitFlag"];

    if (kony.os.toNumber(currDt.getDate()) != kony.os.toNumber(updatedOnDt) || getContentUpdate == null) {
        kony.print("call getPhrases onforeground------------");
        //showLoadingScreen();
        var status = invokeServiceSecureAsync("NewGetPhrases", i18n_inputparam, callbackgetphrasesonforeground);
    }
}

function callbackgetphrasesonforeground(status, getContentUpdate) {
    if (status == 400) {
        if (getContentUpdate["opstatus"] == 0) {
          dismissLoadingScreen();
            kony.store.setItem("getContentUpdate", getContentUpdate);
            initGlobalsFromTPhrases(getContentUpdate);
            if (getContentUpdate["Results"] != null) {
                if (getContentUpdate["Results"][0]["EnglishPhrases"].length > 0) {
                    try {
                        kony.print("updating resourcebundle in eng");
                        kony.i18n.updateResourceBundle(getContentUpdate["Results"][0]["EnglishPhrases"][0], "en_US");
                        //}
                    } catch (i18nError) {
                        alert("Exception While getting updateResourceBundle english  : " + i18nError);
                    }
                }

                if (getContentUpdate["Results"][1]["ThaiPhrases"].length > 0) {
                    try {
                        kony.print("updating resourcebundle in thai");
                        kony.i18n.updateResourceBundle(getContentUpdate["Results"][1]["ThaiPhrases"][0], "th_TH");
                        //}
                    } catch (i18nError) {
                        alert("Exception While getting updateResourceBundle thai  : " + i18nError);
                    }
                }
            }

        }
    }
}



