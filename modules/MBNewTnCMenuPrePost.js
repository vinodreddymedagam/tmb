/*frmMBNewTnC.kl*/

function frmMBNewTnCMenuPreshow() {
  	//#ifdef android    
  	//mki, mib-10789 Start
	// The below code snippet is to fix the whilte label on some screens
	frmMBNewTnC.flexBodyScroll.showFadingEdges  = false;
	//#endif  
	//mki, mib-10789 End
	changeStatusBarColor();
    if (gblCallPrePost) {
        frmMBNewTnC.lblDescSubTitle.text = kony.i18n.getLocalizedString("keyTermsNConditions");
        frmMBNewTnC.btnback.text = kony.i18n.getLocalizedString("Back");
        frmMBNewTnC.btnnext.text = kony.i18n.getLocalizedString("keyAgreeButton");
        frmMBNewTnC.flxSaveCamEmail.setVisibility(false);
        frmMBNewTnC.flexBodyScroll.minHeight = "100%";
        frmMBNewTnC.btnRight.skin = "btnRightShare";
        if (gblMBNewTncFlow == TNC_FLOW_TMB_BLOCK_CREDIT_CARD) {
            frmMBNewTnC.lblHdrTxt.text = kony.i18n.getLocalizedString("titleBlockCreditCard");
            //callServiceReadUTFFile(true);
        } else if (gblMBNewTncFlow == TNC_FLOW_TMB_NEW_DEBIT_CARD) {
            frmMBNewTnC.lblHdrTxt.text = kony.i18n.getLocalizedString("titleIssueNewDebitCard");
        } else if (gblMBNewTncFlow == TNC_FLOW_TMB_DEBIT_CARD_ACTIVATION) {
            frmMBNewTnC.lblHdrTxt.text = kony.i18n.getLocalizedString("keyTermsNConditions");
            frmMBNewTnC.btnnext.text = kony.i18n.getLocalizedString("CAV03_btnAgree");
            frmMBNewTnC.btnback.text = kony.i18n.getLocalizedString("Back");
        }
        frmMBNewTnC.flexBodyScroll.setContentOffset({"x":"0dp", "y":"0dp"});
    }
}

function frmMBNewTnCMenuPostshow() {
    if (gblCallPrePost) {

    }
    assignGlobalForMenuPostshow();
    frmMBNewTnC.flexBodyScroll.setContentOffset({"x":"0dp", "y":"0dp"});
}