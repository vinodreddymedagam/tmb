gblFATCAAnswers = {};
FatcaQues_EN = {};
FatcaQues_TH = {};
gblInfoFlow = false;
/**
 * Function to show FATCA info for '0' flag customers
 */

function clearFatcaGlobal()
{
  GBL_Fatca_Flow = "";
  gblFATCAUpdateFlag = "";
  gblFATCASkipCounter = "";
  gblFATCAUpdateError = false;
} 
function showFACTAInfo() {
  kony.print("showFACTAInfo...entered");
  if(GBL_Fatca_Flow == "login" || GBL_Fatca_Flow == "pushnote") {
    frmFATCATnC.btnAgree.skin = "btnBlueClose";
    frmFATCATnC.btnAgree.focusSkin = "btnBlueClose";
  }
  else {
    frmFATCATnC.btnAgree.skin = "btnBlueClose";
    frmFATCATnC.btnAgree.focusSkin = "btnBlueClose";
  }
  frmFATCATnC.lblGoToBranch.text = kony.i18n.getLocalizedString("keyFATCATnCInfo");
  frmFATCATnC.btnAgree.text = kony.i18n.getLocalizedString("Next");
  frmFATCATnC.hbxBtnSkipNextMB.setVisibility(false);
  frmFATCATnC.hbxSkipNext.setVisibility(true);
  frmFATCATnC.hbxTnC.setVisibility(false);
  frmFATCATnC.hbxAgreeTandC.setVisibility(false);
  frmFATCATnC.hbxBtnClose.setVisibility(false);
  frmFATCATnC.lblError.setVisibility(false);
  frmFATCATnC.hbxGoToBranch.setVisibility(true);
  frmFATCATnC.hbxImage.setVisibility(true);

  frmFATCATnC.hbxImage.margin = [0,12,0,2];
  frmFATCATnC.imgSucessFail.src = "info.png";
  gblInfoFlow = true;
  dismissLoadingScreen();
  kony.print("showFACTAInfo...done ");
  frmFATCATnC.show();
}

/**
 * Function called when click on close button of FATCA info pop Up
 */
function onClickFATCAInfoClose() {
  if(gblFATCAUpdateError === true) {
    // navigate to account summary
    FATCAGeneralPopup.dismiss();
    showAccountSummaryFromMenu();
  }
  else {

    frmFATCATnC.btnSkip.skin = "btnDisabledGray";
    frmFATCATnC.btnSkip.focusSkin = "btnDisabledGray";
    frmFATCATnC.btnSkip.setEnabled(false);
    FATCAGeneralPopup.dismiss();
  }
}

/**
 * Function of service call to get terms and conditions for FATCA forms		To be reomved 
 */
function getMBFATCATermsAndCondition() {
  gblInfoFlow = false;
  setFATCAMBTermsAndConditions();
}

/*	Function to set terms and conditions.	*/

function setFATCAMBTermsAndConditions() {
  if(kony.i18n.getCurrentLocale() == "en_US")
    frmFATCATnC.rTextTandCFATCA.text = GLOBAL_FATCA_TNC_en_US;
  else
    frmFATCATnC.rTextTandCFATCA.text = GLOBAL_FATCA_TNC_th_TH;
  frmFATCATnC.hbxTnC.setVisibility(true);
  frmFATCATnC.hbxAgreeTandC.setVisibility(true);
  frmFATCATnC.hbxBtnClose.setVisibility(false);
  frmFATCATnC.hbxGoToBranch.setVisibility(false);
  frmFATCATnC.hbxImage.setVisibility(false);
  frmFATCATnC.lblError.setVisibility(false);		
  frmFATCATnC.hbxBtnSkipNextMB.setVisibility(true);
  frmFATCATnC.btnNext.setEnabled(false);
  frmFATCATnC.hbxSkipNext.setVisibility(false);
  frmFATCATnC.hbxImage.margin = [0,30,0,2];
  dismissLoadingScreen();
  frmFATCATnC.show();
}

/*	Function to change the locale of questions and repopulate.	*/

function setQuestionLocaleFATCAMB() {
  var locale = kony.i18n.getCurrentLocale();
  var segData = frmFATCAQuestionnaire1.segQuestion.data;
  var fatcaques = {};

  if(locale == "en_US")
    fatcaques = FatcaQues_EN;
  else  
    fatcaques = FatcaQues_TH;
  //frmFATCAQuestionnaire1.segQuestion.removeAll();	
  for(var i=0; i<segData.length; i++) {
    var text = "";
    var help = "";
    var isVisible = true;
    var quesCode = segData[i]["quesCode"];
    kony.print("seg ques code : " + quesCode);
    kony.print("seg ques code : " + fatcaques[quesCode]);
    var ques = fatcaques[quesCode];
    var arr = ques.split("|||");
    text = arr[0].trim();
    if(arr[1] != undefined)
      help = arr[1].trim();
    else
      help = "";
    if(help == "") {
      isVisible = false;
    }
    segData[i]["help"] = help;
    segData[i]["lblRadioNo"] = kony.i18n.getLocalizedString("keyFATCANo");
    segData[i]["lblRadioYes"] = kony.i18n.getLocalizedString("keyFATCAYes");
    segData[i]["rTextQues"] = text;
    segData[i]["btnQHelp"]["isVisible"] = isVisible;
  }
  kony.print(segData);
  //frmFATCAQuestionnaire1.segQuestion.removeAll();	
  frmFATCAQuestionnaire1.segQuestion.setData(segData);
  if(gblFATCAPage == 1)
    frmFATCAQuestionnaire1PreShow();
  else
    frmFATCAQuestionnaire2PreShow();
  dismissLoadingScreen();
}

/*	Function to initiaize global answer variable.	*/

function setFATCAMBGoToBranchInfoMessage() {
  frmFATCATnC.hbxImage.setVisibility(true);
  frmFATCATnC.lblError.setVisibility(true);
  frmFATCATnC.hbxLanguage.setVisibility(true);
  frmFATCATnC.lblHdrTxt.text = "FATCA";
  frmFATCATnC.imgSucessFail.src = "mes2.png";
  if(GBL_Fatca_Flow == "login" || GBL_Fatca_Flow == "pushnote")
    frmFATCATnC.lblGoToBranch.text = kony.i18n.getLocalizedString("keyFATCAError_001");
  else if (GBL_Fatca_Flow == "ekycModule"){ //mki,MIB-13686
    frmFATCATnC.lblGoToBranch.text = kony.i18n.getLocalizedString("eKYC_msgNotpassFATCA");
  }else {
    if( Object.keys(gblFATCAAnswers).length === 0  )
      frmFATCATnC.lblGoToBranch.text = kony.i18n.getLocalizedString("keyFATCAError_003");
    else
      frmFATCATnC.lblGoToBranch.text = kony.i18n.getLocalizedString("keyFATCAError_005");
  }	
  frmFATCATnC.hbxTnC.setVisibility(false);
  frmFATCATnC.hbxAgreeTandC.setVisibility(false);
  frmFATCATnC.hbxSkipNext.setVisibility(false);
  frmFATCATnC.hbxBtnSkipNextMB.setVisibility(false);
  frmFATCATnC.hbxImage.margin = [0,12,0,2];
  frmFATCATnC.hbxGoToBranch.setVisibility(true);
  frmFATCATnC.hbxBtnClose.setVisibility(true);
  frmFATCATnC.show();
}

function setFATCAMBFormFinishInfoMessage() {


  frmFATCATnC.hbxImage.setVisibility(true);
  frmFATCATnC.hbxGoToBranch.setVisibility(true);
  frmFATCATnC.hbxBtnClose.setVisibility(true);
  frmFATCATnC.hbxLanguage.setVisibility(true);
  frmFATCATnC.lblError.setVisibility(false);
  frmFATCATnC.hbxTnC.setVisibility(false);
  frmFATCATnC.hbxAgreeTandC.setVisibility(false);
  frmFATCATnC.hbxBtnSkipNextMB.setVisibility(false);
  frmFATCATnC.imgSucessFail.src = "completeico.png";
  frmFATCATnC.lblHdrTxt.text = kony.i18n.getLocalizedString("keylblComplete");	
  if(GBL_Fatca_Flow == "login" || GBL_Fatca_Flow == "pushnote")
    frmFATCATnC.lblGoToBranch.text = kony.i18n.getLocalizedString("keyFATCAError_002");
  else
    frmFATCATnC.lblGoToBranch.text = kony.i18n.getLocalizedString("keyFATCAError_006");
  frmFATCATnC.hbxImage.margin = [0,12,0,2];
  frmFATCATnC.show();

}

function onClickFATCATnCAgreeBtn(eventObject) {
  if(eventObject.skin == "btnCheck") {
    eventObject.skin = "btnCheckFoc";
    frmFATCATnC.btnNext.skin = "btnBlueClose";
    frmFATCATnC.btnNext.focusSkin = "btnBlueClose";
    frmFATCATnC.btnNext.setEnabled(true);
  }
  else {
    eventObject.skin = "btnCheck";
    frmFATCATnC.btnNext.skin = "btnDisabledGrayLarge";
    frmFATCATnC.btnNext.focusSkin = "btnDisabledGrayLarge";
    frmFATCATnC.btnNext.setEnabled(false);
  }
}

function onClickFATCATnCNext() {
  if(frmFATCATnC.btnAgreeCheck.skin == "btnCheckFoc") {
    gblFATCAPage = 1;
    getFATCAQuestionsServiceCall();
  }
}

function navigateFATCAfromEKYC(){ //mki, 
  GBL_Fatca_Flow = "ekycModule";
  gblFATCAPage = 1;
  gblFATCAUpdateFlag = "N";
  getFATCAQuestionsServiceCall();
}

function getFATCAQuestionsServiceCall() {
  showLoadingScreen();
  var inputparams = {};
  inputparams["page"] = gblFATCAPage + "";
  invokeServiceSecureAsync("getFATCAQuestions", inputparams, getFATCAQuestionsServiceCallBack);
}

function getFATCAQuestionsServiceCallBack(status, resulttable) {
  if (status == 400) {
    if (resulttable["opstatus"] == 0) {
      var segData = [];
      kony.print("getFATCAQuestions-output: " + JSON.stringify(resulttable));
      var locale = kony.i18n.getCurrentLocale();
      var fatques = "";
      FatcaQues_EN = {};
      FatcaQues_TH = {};
      if( GBL_Fatca_Flow != "ekycModule"){ //mki,MIB-13686
        frmFATCATnC.btnAgreeCheck.skin = "btnCheck";
        frmFATCATnC.btnNext.skin = "btnDisabledGrayLarge";
        frmFATCATnC.btnNext.focusSkin = "btnDisabledGrayLarge";
        frmFATCATnC.btnNext.hoverSkin = "btnDisabledGrayLarge";
      }

      frmFATCAQuestionnaire1.segQuestion.removeAll();
      if(gblFATCAPage == 2) {
        frmFATCAQuestionnaire1.lblStep2.text = kony.i18n.getLocalizedString("keyStep2Label");
        frmFATCAQuestionnaire1.lblStep1.text = kony.i18n.getLocalizedString("keyStep1Label");
        frmFATCAQuestionnaire1.lblStep2.skin = "lblBlackSmall";
        frmFATCAQuestionnaire1.lblStep1.skin = "lblWhiteSmall";
        frmFATCAQuestionnaire1.lblStep1.setVisibility(true);
        frmFATCAQuestionnaire1.lblStep2.setVisibility(true);		
        frmFATCAQuestionnaire1.imgStep1.src = "step2_1.png";
        frmFATCAQuestionnaire1.imgStep2.src = "step2_2_sel.png";
      }	
      var quesListOrderArr = resulttable["FatcaQuesOrder"].split(",");
      FatcaQues_EN = resulttable["FatcaQues_EN"][0];
      FatcaQues_TH = resulttable["FatcaQues_TH"][0];
      kony.print("" + JSON.stringify(FatcaQues_EN));
      kony.print("" + JSON.stringify(FatcaQues_TH));
      if(locale == "en_US")
        fatques = "FatcaQues_EN";
      else
        fatques = "FatcaQues_TH";
      for(var i=0; i<quesListOrderArr.length; i++) {
        var temp = {};
        var text = "";
        var help = "";
        var isVisible = true;
        var ques = "";
        var quesCode = quesListOrderArr[i].trim();
        if(((locale == "en_US") && resulttable["FatcaQues_EN"][0].hasOwnProperty(quesCode)) || ((locale == "th_TH") && resulttable["FatcaQues_TH"][0].hasOwnProperty(quesCode))) {
          if(locale == "en_US") 
            ques =  resulttable["FatcaQues_EN"][0][quesCode];
          else
            ques =  resulttable["FatcaQues_TH"][0][quesCode];	
          var arr = ques.split("|||");
          text = arr[0].trim();
          if(arr[1] != undefined)
            help = arr[1].trim();
          else
            help = "";
        }
        if(help == "") {
          isVisible = false;
        }
        temp = {
          "rTextQues": text,
          "lblRadioNo": kony.i18n.getLocalizedString("keyFATCANo").trim(),
          "lblRadioYes": kony.i18n.getLocalizedString("keyFATCAYes").trim(),
          "btnRadioNo": {
            skin: "btnRadioFoc"
          },
          "btnRadioYes": {
            skin: "btnRadio"
          },
          "btnQHelp":{
            isVisible : isVisible ,
            widgetAlignment : constants.WIDGET_ALIGN_TOP_RIGHT
          },
          "help": help,
          "quesCode": quesCode
        };
        segData.push(temp);
      }	
      kony.print(segData);
      frmFATCAQuestionnaire1.segQuestion.setData(segData);

      var segData1 = frmFATCAQuestionnaire1.segQuestion.data;
      for(i=0; i<segData1.length; i++) {
        gblFATCAAnswers[segData1[i]["quesCode"]] = "N";
      }			

      //ENH_221 : Mark 'No' as default answer in FATCA flow 
      frmFATCAQuestionnaire1.btnFATCAQues1Next.skin = "btnBlueClose";
      frmFATCAQuestionnaire1.btnFATCAQues1Next.focusSkin = "btnBlueClose";
      frmFATCAQuestionnaire1.btnFATCAQues1Next.setEnabled(true);

      kony.print("gblFATCAPage: " + gblFATCAPage);
      if(gblFATCAPage == 2) {
        frmFATCAQuestionnaire2PreShow();
      }
      else {
        frmFATCAQuestionnaire1PreShow();
      }
      frmFATCAQuestionnaire1.show();
    }
    dismissLoadingScreen();
  }
  dismissLoadingScreen();
}

/**
 * Function to show the help of partcular questions
 */
function showFATCAQuesInfo() {
  var selectedIndex = frmFATCAQuestionnaire1.segQuestion.selectedIndex[1];
  kony.print("showing fatca question help: " + selectedIndex);
  var help = frmFATCAQuestionnaire1.segQuestion.data[selectedIndex]["help"];
  kony.print("help: " + help);
  FATCAHelpPopup.lblHead.text = help;
  FATCAHelpPopup.btnNext.text = kony.i18n.getLocalizedString("keyClose");
  FATCAHelpPopup.show();
}

function onClickFATCARadioBtn(eventObject) {
  var selectedIndex = frmFATCAQuestionnaire1.segQuestion.selectedIndex[1];
  kony.print("on click of radio btn - " + selectedIndex);
  var segData = frmFATCAQuestionnaire1.segQuestion.data[selectedIndex];
  var temp = {};
  kony.print("Seg Data : " + segData);
  var clicked = 0;
  if(segData["btnRadioYes"]["skin"] == "btnRadioFoc")
    clicked = 1;
  else if(segData["btnRadioNo"]["skin"] == "btnRadioFoc")
    clicked = 2;
  else
    clicked = 0;
  kony.print("clicked: " + clicked);

  if(eventObject.id == "btnRadioNo") {
    segData["btnRadioNo"]["skin"] = "btnRadioFoc";
    if(clicked == 1)
      segData["btnRadioYes"]["skin"] = "btnRadio";	
  }
  else if(eventObject.id == "btnRadioYes") {
    segData["btnRadioYes"]["skin"] = "btnRadioFoc";
    if(clicked == 2)
      segData["btnRadioNo"]["skin"] = "btnRadio";
  }
  frmFATCAQuestionnaire1.segQuestion.setDataAt(segData,selectedIndex);

  gblFATCAUpdateFlag = "N";
  var segData1 = frmFATCAQuestionnaire1.segQuestion.data;
  for(i=0; i<segData1.length; i++) {
    if(segData1[i]["btnRadioYes"]["skin"] == "btnRadioFoc") {
      if(gblFATCAPage == 1)
        gblFATCAUpdateFlag = "9";
      //else if(gblFATCAPage == 2) //mki,MIB-13686
      // gblFATCAUpdateFlag = "8"; //mki,MIB-13686
      gblFATCAAnswers[segData1[i]["quesCode"]] = "Y";
    }
    else if(segData1[i]["btnRadioNo"]["skin"] == "btnRadioFoc") {
      gblFATCAAnswers[segData1[i]["quesCode"]] = "N";
    }
    else {
      break;
    }
  }
  if(i == segData1.length) {
    frmFATCAQuestionnaire1.btnFATCAQues1Next.skin = "btnBlueClose";
    frmFATCAQuestionnaire1.btnFATCAQues1Next.focusSkin = "btnBlueClose";
    frmFATCAQuestionnaire1.btnFATCAQues1Next.setEnabled(true);
  }
}


/**
 * Function to update FATCA in submission of answers in first or second page
 */
function populateFATCAPageTwo() {
  //mki,MIB-13686
  /*
  if(gblFATCAUpdateFlag == "8" || gblFATCAUpdateFlag == "9") {
    MBFATCAUpdateServiceCall();
  }
  else {
    if(gblFATCAPage == 1) {
      gblFATCAPage = 2;
      getFATCAQuestionsServiceCall();
    }
    else {
      MBFATCAUpdateServiceCall();
    }
  }*/
  //if(gblFATCAUpdateFlag == "9") {
  MBFATCAUpdateServiceCall();
  //}  
}

function MBFATCAUpdateServiceCall() {
  kony.print("In update service call :" + gblFATCAUpdateFlag);
  showLoadingScreen();
  var inputparams = {};
  if(GBL_Fatca_Flow == "ekycModule"){
    inputparams["AnswerKey"] = JSON.stringify(gblFATCAAnswers);    
	invokeServiceSecureAsync("ekycFatcaUpdateActivityLog", inputparams, MBFATCAUpdateServiceCallBack);
  }else{
    inputparams["AnswerKey"] = JSON.stringify(gblFATCAAnswers);
    inputparams["fatcaFlow"] = GBL_Fatca_Flow;
    invokeServiceSecureAsync("FatcaUpdate", inputparams, MBFATCAUpdateServiceCallBack);
  }
}

function MBFATCAUpdateServiceCallBack(status, resulttable) {
  if (status == 400) {
    if (resulttable["opstatus"] == 0) {
      kony.print("In update service call callback");
      kony.print("FATCA-update result: " + resulttable);
      if(GBL_Fatca_Flow !== "ekycModule"){
        gblFATCAUpdateFlag = resulttable["FATCAFlag"];	//To be commented when service responds
      }
      
      if( GBL_Fatca_Flow == "ekycModule" && gblFATCAUpdateFlag == "N"){ //mki,MIB-13686
        //navigatefrmMBeKYCContactInfo();
        emailVerificationMB("eKYC");
      }else if(resulttable["FATCAFlag"] == "8" || resulttable["FATCAFlag"] == "9" || (GBL_Fatca_Flow == "ekycModule" && gblFATCAUpdateFlag !== "N")) {
        setFATCAMBGoToBranchInfoMessage();
      }else {
        setFATCAMBFormFinishInfoMessage();
      }
    }
    else {
      // create a custom pop-up and on close button navigate to account summary page
      gblFATCAUpdateError = true;
      FATCAGeneralPopup.lblHead.text = kony.i18n.getLocalizedString("ECGenericError");
      FATCAGeneralPopup.btnClose.text = kony.i18n.getLocalizedString("keyClose");
      FATCAGeneralPopup.show();
      // alert(kony.i18n.getLocalizedString("ECGenericError"));
    }
    dismissLoadingScreen();
  }
  dismissLoadingScreen();
}