function frmMBRTPDetailsListInit(){
  frmMBRTPDetailsList.btnSortbyArrow.onClick = onClickRTPDetailsSort;
  frmMBRTPDetailsList.segRTPDetails.onRowClick = onRowClickRTPList;
  frmMBRTPDetailsList.preShow = preShowFrmMBRTPDetailsList
}

function setfrmMBRTPDetailsListFadingEdges(){
                //#ifdef android    
                //mki, mib-10789 Start
                // The below code snippet is to fix the whilte label on some screens
                frmMBRTPDetailsList.flxScrlSegRTPDetails.showFadingEdges  = false;
                //#endif  
                //mki, mib-10789 End
}

function invokeRTPDetails(){
  try{
    rtp_log("@@@ In invokeRTPDetails() @@@");
    showLoadingScreen();
    var inputParams = {};
    rtp_log("@@@gblRTPDetailsSortBy:::"+gblRTPDetailsSortBy);
    if(gblRTPDetailsSortBy == 2){
      rtp_log("--- Sorting By ExpiryDate ---");
      inputParams["sortBy"] = "Expiry";
    }
    if(gblConsentIndividualFlag && !gblConsentBusinessFlag){
      inputParams["rtpProduct"] = "RTPSC"; //for Transfer/Consumer
    }else if(!gblConsentIndividualFlag && gblConsentBusinessFlag){
      inputParams["rtpProduct"] = "RTPBP"; //for BillPayment/Corporate
    }else if(!gblConsentIndividualFlag && !gblConsentBusinessFlag){ //Skipping RTPInq service when both consents flags are false.
      rtp_log("------ Skipping RTPInq Service ------");
      gblRTPBadgeCount = 0;
      dismissLoadingScreen();
      showNoActiveRTP();
      return false; 
    }
    invokeServiceSecureAsync("RTPInq", inputParams, onScuccessRTPInq);
  }catch(e){
    rtp_log("@@@ In invokeRTPDetails() Exception:::"+e);
  }
}

function onScuccessRTPInq(status, resulttable){
  try{
    rtp_log("@@@ In onScuccessRTPInq() @@@");
    rtp_log("@@@ resulttable::::"+JSON.stringify(resulttable));
    if(status == 400 && resulttable["opstatus"] == 0){
		if (resulttable.hasOwnProperty("RTPActivityDS") && resulttable["RTPActivityDS"].length > 0) {
          frmMBRTPDetailsList.segRTPDetails.onClick = onRowClickRTPList;
          //Assinging RTP Badgecount to global varible.
          gblRTPBadgeCount = resulttable["TotalRecord"];
          frmMBRTPDetailsList.segRTPDetails.removeAll();
          setRTPDetails(resulttable["RTPActivityDS"]);
          frmMBRTPDetailsList.show();
          dismissLoadingScreen();
        }else{
          gblRTPBadgeCount = 0;
          dismissLoadingScreen();
          showNoActiveRTP();
        }
	}else{
		dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
	}
  }catch(e){
    rtp_log("@@@ In onScuccessRTPInq() Exception:::"+e);
  }
}

function setRTPDetails(rtpListData){
    try{
      rtp_log("@@@ In setRTPDetails() @@@");
      gblRTPPrevSelIndex = null;
      var rtpData = rtpListData;
      var rtpSegData = [];
      /*frmMBRTPDetailsList.segRTPDetails.widgetDataMap = {imgRTPIcon: "imgRTPIcon", lblAccountName: "lblAccountName", lblRefNum: "lblRefNum",
                                                          lblDescription: "lblDescription", lblDate: "lblDate", lblAmount: "lblAmount",
                                                           flxImage: "flxImage", flxDelete: "flxDelete", imgClose: "imgClose"};*/

      //Assigning Gesture for Delete row functionality.
      var setSwipe = {fingers:1, swipedistance:50, swipevelocity:25};
      frmMBRTPDetailsList.segRTPDetails.rowTemplate.addGestureRecognizer(2, setSwipe, rtpDetailsSwipeCallBack);
      var lblDueDateIsvisible = true;
      for(var i = 0; i < rtpData.length; i++ ){
        var dueDateVal = "";
        var dueDateSkin = "lblGrey36px";
        var imageLogo = "";
        var nearToExp = rtpData[i].numOfExpiryDays;
        //R - Billpay(Corporate), C - Transfer(Customer)
        if(rtpData[i].RTPProduct == "RTPBP"){ //for BillPayment/Corporate
          imageLogo = "icon_store.png";
          lblDueDateIsvisible = true;
          if(isNotBlank(nearToExp) && kony.os.toNumber(nearToExp) <= 3){  
            dueDateSkin = "lblRed36px";
          }
        }else if(rtpData[i].RTPProduct == "RTPSC"){ //for Transfer/Consumer
          imageLogo = "icon_firend.png";
          if(isNotBlank(nearToExp) && kony.os.toNumber(nearToExp) <= 3){  
            dueDateSkin = "lblRed36px";
            lblDueDateIsvisible = true;
          }else{
            lblDueDateIsvisible = false;
          }
        }
        var ref1Val = "";
        if(isNotBlank(rtpData[i].Reference1)){
          ref1Val = rtpData[i].Reference1;
        }
        var ref2Val = "";
        if(isNotBlank(rtpData[i].Reference2)){
          ref2Val = rtpData[i].Reference2;
        }
        var ref3Val = "";
        if(isNotBlank(rtpData[i].Reference3)){
          ref3Val = rtpData[i].Reference3;
        }
        var ref4Val = "";
        if(isNotBlank(rtpData[i].Reference4)){
          ref4Val = rtpData[i].Reference4;
        }
        var ref5Val = "";
        if(isNotBlank(rtpData[i].Reference5)){
          ref5Val = rtpData[i].Reference5;
        }
        var tempData = {
            imgRTPIcon: {src: imageLogo},
            lblAccountName: truncatStirngTo15Char(rtpData[i].SenderAcctName),
            lblRefNum: kony.i18n.getLocalizedString("MB_RTPReflab") + " " + rtpData[i].RTPTranRef,
            lblDescription: rtpData[i].Memo,
            lblDate: {text: kony.i18n.getLocalizedString("MB_RTPDDab") + " " + rtpData[i].ExpiryDateNoTime, skin: dueDateSkin,isVisible : lblDueDateIsvisible},
            lblAmount: commaFormatted(parseFloat(rtpData[i].TranAmt).toFixed(2)), // + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
            flxDelete: {onClick: deleteRTPSelRow},
            flxImage: {left: "2%"},
            createDate : rtpData[i].CreateDate,
            expiryDate: rtpData[i].ExpiryDate,
          	expiryDateNoTime: rtpData[i].ExpiryDateNoTime,
            imgClose: "close_white.png",
            RTPTranRef: rtpData[i].RTPTranRef,
            SenderAcctName: rtpData[i].SenderAcctName,
            ReceiverAcctNo: rtpData[i].ReceiverAcctNo,
            SenderAcctNo: rtpData[i].SenderAcctNo,
            RTPProduct: rtpData[i].RTPProduct,
            SenderBankCode: rtpData[i].SenderBankCode,
            SenderProxyID: rtpData[i].SenderProxyID,
            SenderProxyType: rtpData[i].SenderProxyType,
            IsRTPConsumer: rtpData[i].IsRTPConsumer,
            ReceiverProxyID: rtpData[i].ReceiverProxyID,
            //SenderAcctName: rtpData[i].SenderAcctName RTPType 
            ReceiverAcctDispName: rtpData[i].ReceiverAcctDispName,
            ReceiverAcctName: rtpData[i].ReceiverAcctName,
            ReceiverBankCode: rtpData[i].ReceiverBankCode,
            ReceiverProxyType: rtpData[i].ReceiverProxyType,
            RTPType: rtpData[i].RTPType,
            Reference1: ref1Val,
         	Reference2: ref2Val,
            Reference3: ref3Val,
            Reference4: ref4Val,
            Reference5: ref5Val
          };
        rtpSegData.push(tempData);
      }
      gblRTPListData = rtpSegData;
      frmMBRTPDetailsList.segRTPDetails.setData(rtpSegData);
      rtp_log("------END setRTPDetails()-----");
    }catch(e){
      	rtp_log("@@@ In setRTPDetails() Exception:::"+e);
    }
}

function setRTPListonChangeLocale(){
  try{
    rtp_log("@@@ In setRTPListonChangeLocale() @@@");
    var rtpList = frmMBRTPDetailsList.segRTPDetails.data;
    if(rtpList != null && rtpList != "" && rtpList != undefined){
      frmMBRTPDetailsList.segRTPDetails.removeAll();
      gblRTPListData = null;
      for(var i = 0; i < rtpList.length; i++ ){
        rtpList[i].lblRefNum = kony.i18n.getLocalizedString("MB_RTPReflab") + " " + rtpList[i].RTPTranRef;
        rtpList[i].lblDate.text = kony.i18n.getLocalizedString("MB_RTPDDab") + " " + rtpList[i].expiryDateNoTime;
      }
      frmMBRTPDetailsList.segRTPDetails.setData(rtpList);
      gblRTPListData = rtpList;
    }
  }catch(e){
    rtp_log("@@@ In setRTPListonChangeLocale() Exception:::"+e);
  }
}

function rtpDetailsSwipeCallBack(widgetRef, gestureInfo, context){
  try{
    rtp_log("@@@ In rtpDetailsSwipeCallBack() @@@");
    var rowIndex = context.rowIndex;
    var data = frmMBRTPDetailsList.segRTPDetails.data[rowIndex];
    rtp_log("@@@gblRTPPrevSelIndex:::"+gblRTPPrevSelIndex+"||RowIndex:::"+rowIndex);
    if(gblRTPPrevSelIndex != null){
      if(gblRTPPrevSelIndex != rowIndex){
        var prevData = frmMBRTPDetailsList.segRTPDetails.data[gblRTPPrevSelIndex];
        prevData["flxImage"].left = "2%";
        frmMBRTPDetailsList.segRTPDetails.setDataAt(prevData, gblRTPPrevSelIndex);
      }
    }
    
    var flexLeft = "";
    if(gestureInfo.swipeDirection == 1){
    	flexLeft = "-35%";
    }else if(gestureInfo.swipeDirection == 2){
    	flexLeft = "2%";
    }else{
    	return;   
    }  
    data["flxImage"].left = flexLeft;
    frmMBRTPDetailsList.segRTPDetails.setDataAt(data, rowIndex);
    gblRTPPrevSelIndex = rowIndex;
  }catch(e){
    rtp_log("@@@ Exception in SwipeCallBack:::"+e);
  }
}

function truncatStirngTo15Char(textVal){
  if(textVal !== null && textVal !== "" & textVal !== undefined){
    var textLen = textVal.length;
    if(textLen > 15){
      textVal = textVal.substring(0, 15)+"...";
    }
  }
  return textVal;
}

// ----------------- RTP Delete Functionality -----------------
function deleteRTPSelRow(){
  try{
    rtp_log("@@@ In deleteRTPSelRow() @@@");
    showLoadingScreen();
    var selRow = frmMBRTPDetailsList.segRTPDetails.selectedIndex[1];
    var deleteData = frmMBRTPDetailsList.segRTPDetails.data[selRow];
    
    if(isNotBlank(selRow) && deleteData != null && deleteData != "" && deleteData != undefined ){
      if(isNotBlank(deleteData.RTPTranRef) && isNotBlank(deleteData.SenderAcctName)){
        var inputParams = {};
        inputParams["rtpTranRef"] = deleteData.RTPTranRef;
        inputParams["rtpStatus"] = "05";
        inputParams["rtpSenderName"] = deleteData.SenderAcctName;
        invokeServiceSecureAsync("RTPUpdate", inputParams, 
            function(status, resulttable){
              try{
                rtp_log("@@@ Delete resulttable::::"+JSON.stringify(resulttable));
                if(status == 400 && resulttable["opstatus"] == 0){
                  frmMBRTPDetailsList.segRTPDetails.removeAt(selRow);
                  gblRTPPrevSelIndex = null;
                  updategblRTPListData(deleteData.RTPTranRef);
                  updateRTPBadgeCount();
                  rtp_log("@@@SegDataLen:::"+gblRTPListData.length);
                  if(isNotBlank(gblRTPListData.length) && gblRTPListData.length == 0){
                    displayfrmMBRTPNoActiveList();
                  }
                  dismissLoadingScreen();
                }else{
                  dismissLoadingScreen();
                  showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
                }
              }catch(e){
                rtp_log("@@@ In Delete Row() Exception:::"+e);
              }
            });
      }
    }
  }catch(e){
    rtp_log("@@@ Exception in deleteRTPSelRow:::"+e);
    dismissLoadingScreen();
  }
}

function updateRTPBadgeCount(){
  if(isNotBlank(gblRTPBadgeCount) && gblRTPBadgeCount > 0){
    gblRTPBadgeCount = gblRTPBadgeCount-1;
  }
}

function updategblRTPListData(rtpTranRef){
  try{
    rtp_log("@@@ In updategblRTPListData() @@@");
    var postion = null;
    if(isNotBlank(rtpTranRef) && gblRTPListData != null && gblRTPListData != "" && gblRTPListData != undefined){
      for(var i = 0; i < gblRTPListData.length; i++ ){
        if(gblRTPListData[i].RTPTranRef == rtpTranRef){
          rtp_log("---- Found deleted item -----");
          postion = i;
        }
      }
      rtp_log("@@@Delete postion:::"+postion);
      if(postion != null){
        gblRTPListData.splice(postion, 1);
      }
    }
  }catch(e){
    rtp_log("@@@ In updategblRTPListData() Exception:::"+e);
  }
}


// -----------------------RTP Sorting Functionality--------------------.
function onClickRTPDetailsSort(){
  if(gblRTPDetailsSortBy == 1){
    frmMBRTPDetailsList.imgMostRecent.setVisibility(true);
  	frmMBRTPDetailsList.imgNearlyExpires.setVisibility(false);
  	frmMBRTPDetailsList.imgSenderName.setVisibility(false);
  }else if(gblRTPDetailsSortBy == 2){
    frmMBRTPDetailsList.imgMostRecent.setVisibility(false);
  	frmMBRTPDetailsList.imgNearlyExpires.setVisibility(true);
  	frmMBRTPDetailsList.imgSenderName.setVisibility(false);
  }else if(gblRTPDetailsSortBy == 3){
    frmMBRTPDetailsList.imgMostRecent.setVisibility(false);
  	frmMBRTPDetailsList.imgNearlyExpires.setVisibility(false);
  	frmMBRTPDetailsList.imgSenderName.setVisibility(true);
  }
  frmMBRTPDetailsList.flxSortPop.setVisibility(true);
}

function onCloseRTPSortPopup(){
  try{
    rtp_log("@@@ In onCloseRTPSortPopup()@@@");
    if(gblRTPPrevSelIndex !== null && gblRTPPrevSelIndex !== "" && gblRTPPrevSelIndex !== undefined){
      var swipeData = frmMBRTPDetailsList.segRTPDetails.data[gblRTPPrevSelIndex];
      if(swipeData !== null && swipeData !== "" && swipeData !== undefined){
        swipeData["flxImage"].left = "2%";
        frmMBRTPDetailsList.segRTPDetails.setDataAt(swipeData, gblRTPPrevSelIndex);
      }
    }
    frmMBRTPDetailsList.flxSortPop.setVisibility(false);
  }catch(exp){
    rtp_log("@@@ In onCloseRTPSortPopup() Exception:::"+exp);
  }
}

function preShowFrmMBRTPDetailsList(){
  	frmMBRTPDetailsList.lblHeader.text = kony.i18n.getLocalizedString("MB_RTPTittle");
  	
  	if(gblRTPDetailsSortBy == 1){
      frmMBRTPDetailsList.imgMostRecent.setVisibility(true);
      frmMBRTPDetailsList.imgNearlyExpires.setVisibility(false);
      frmMBRTPDetailsList.imgSenderName.setVisibility(false);
      frmMBRTPDetailsList.lblSortByVal.text = kony.i18n.getLocalizedString("MB_RTPRecentlab");
      frmMBRTPDetailsList.flxSortPop.setVisibility(false);
    }else if(gblRTPDetailsSortBy == 2){
      frmMBRTPDetailsList.imgMostRecent.setVisibility(false);
      frmMBRTPDetailsList.imgNearlyExpires.setVisibility(true);
      frmMBRTPDetailsList.imgSenderName.setVisibility(false);
      frmMBRTPDetailsList.lblSortByVal.text = kony.i18n.getLocalizedString("MB_RTPExplab");
      frmMBRTPDetailsList.flxSortPop.setVisibility(false);
      //onClickRTPSortNearlyExp();
    }else if(gblRTPDetailsSortBy == 3){
       	onClickRTPSortSenderName();
    }
  	//frmMBRTPDetailsList.lblSortByVal.text = kony.i18n.getLocalizedString("MB_RTPRecentlab");
  	frmMBRTPDetailsList.lblSortByText.text = kony.i18n.getLocalizedString("MB_RTPSortlab");
	frmMBRTPDetailsList.lblSotrtBy.text = kony.i18n.getLocalizedString("MB_RTPSortlab");
  	frmMBRTPDetailsList.lblMostRecent.text = kony.i18n.getLocalizedString("MB_RTPRecentlab");
  	frmMBRTPDetailsList.lblNearlyExpires.text = kony.i18n.getLocalizedString("MB_RTPExplab");
  	frmMBRTPDetailsList.lblSenderName.text = kony.i18n.getLocalizedString("MB_RTPSenderlab");
  	setRTPListonChangeLocale();
  	setfrmMBRTPDetailsListFadingEdges();
}

function onClickRTPSortMostRecent(){
  	gblRTPDetailsSortBy = 1;
  	frmMBRTPDetailsList.imgMostRecent.setVisibility(true);
  	frmMBRTPDetailsList.imgNearlyExpires.setVisibility(false);
  	frmMBRTPDetailsList.imgSenderName.setVisibility(false);
  	frmMBRTPDetailsList.lblSortByVal.text = kony.i18n.getLocalizedString("MB_RTPRecentlab");
  	frmMBRTPDetailsList.flxSortPop.setVisibility(false);
  	//RTPDataSortyByMostRecent();
  	invokeRTPDetails();
}
function onClickRTPSortNearlyExp(){
  	gblRTPDetailsSortBy = 2;
  	frmMBRTPDetailsList.imgMostRecent.setVisibility(false);
  	frmMBRTPDetailsList.imgNearlyExpires.setVisibility(true);
  	frmMBRTPDetailsList.imgSenderName.setVisibility(false);
  	frmMBRTPDetailsList.lblSortByVal.text = kony.i18n.getLocalizedString("MB_RTPExplab");
  	frmMBRTPDetailsList.flxSortPop.setVisibility(false);
  	//RTPDataSortyByNearlyExpire();
  	invokeRTPDetails();
}
function onClickRTPSortSenderName(){
  	gblRTPDetailsSortBy = 3;
  	frmMBRTPDetailsList.imgMostRecent.setVisibility(false);
  	frmMBRTPDetailsList.imgNearlyExpires.setVisibility(false);
  	frmMBRTPDetailsList.imgSenderName.setVisibility(true);
  	frmMBRTPDetailsList.lblSortByVal.text = kony.i18n.getLocalizedString("MB_RTPSenderlab");
  	frmMBRTPDetailsList.flxSortPop.setVisibility(false);
  	RTPDataSortyBySenderName();
}

function RTPDataSortyByMostRecent(){
  try{
    rtp_log("@@@ In RTPDataSortyByMostRecent() @@@");
    var rtpData = gblRTPListData;
  	if(rtpData !== null && rtpData !== "" && rtpData !== undefined){
      rtpData.sort(function(a,b){
        var date1= new Date(a.createDate);
        var date2 = new Date(b.createDate);
        if (date1 > date2) return -1;
  		if (date1 < date2) return 1;
        return 0; 
        //return new Date(a.createDate).getTime() - new Date(b.createDate).getTime();
      });
      frmMBRTPDetailsList.segRTPDetails.removeAll();
      frmMBRTPDetailsList.segRTPDetails.setData(rtpData);
    }
  }catch(e){
    rtp_log("@@@ In RTPDataSortyByMostRecent() Exception:::"+e);
  }
}

function RTPDataSortyByNearlyExpire(){
  try{
    rtp_log("@@@ In RTPDataSortyByNearlyExpire() @@@");
    var rtpData = gblRTPListData; 
  	if(rtpData !== null && rtpData !== "" && rtpData !== undefined){
      rtpData.sort(function(a,b){
        var date1= new Date(a.expiryDate);
        var date2 = new Date(b.expiryDate);
        if (date1 > date2) return 1;
  		if (date1 < date2) return -1;
        return 0;
        //return new Date(a.expiryDate).getTime() - new Date(b.expiryDate).getTime();
      });
      frmMBRTPDetailsList.segRTPDetails.removeAll();
      frmMBRTPDetailsList.segRTPDetails.setData(rtpData);
    }
  }catch(e){
    rtp_log("@@@ In RTPDataSortyByNearlyExpire() Exception:::"+e);
  }
}

function RTPDataSortyBySenderName(){
  	rtp_log("@@@ In RTPDataSortyBySenderName() @@@");
  	var rtpData = gblRTPListData; 
  	if(rtpData !== null && rtpData !== "" && rtpData !== undefined){
      kony.table.sort(rtpData,"lblAccountName");
      frmMBRTPDetailsList.segRTPDetails.removeAll();
      frmMBRTPDetailsList.segRTPDetails.setData(rtpData);
    }
}


//----------------- RTP List onRow Click -----------------
function onRowClickRTPList(){
    GblBillTopFlag = true;
    try{
      rtp_log("@@@ In onRowClickRTPList() @@@---selectedRowItems---");
      var selectedRowDetails = frmMBRTPDetailsList.segRTPDetails.selectedRowItems;
      rtp_log("@@@ Selected Row:::"+JSON.stringify(selectedRowDetails));
      if(selectedRowDetails != null && selectedRowDetails != "" && selectedRowDetails != undefined){
        //       if (selectedRowDetails[0].IsRTPConsumer == "C") {
        //         onClickforTransfer(selectedRowDetails);
        //       } else if (selectedRowDetails[0].IsRTPConsumer == "R") {
        //         onClickifBillPay(selectedRowDetails);
        //       }
        onClickTransferBillPay(selectedRowDetails);
      }
    }catch(e){
      rtp_log("@@@ In onRowClickRTPList() Exception:::"+e);
    }
}

/*function onClickforTransfer(transferData)
{
  rtp_log("transferData" +JSON.stringify(transferData));
  rtp_log("transferData.ReceiverAcctNo[]" +transferData[0].ReceiverAcctNo);
  //rtp_log("transferData.ReceiverAcctNo" +transferData.ReceiverAcctNo);
  if(transferData != null || transferData != undefined){
  var resultTable = {
         "ReceiverAcctNo": transferData[0].ReceiverAcctNo,
		"RTPTranRef": transferData[0].RTPTranRef,
		"SenderBankCode": transferData[0].SenderBankCode,
		"SenderProxyID": transferData[0].SenderProxyID,
		"SenderProxyType": transferData[0].SenderProxyType,
		"TranAmt": transferData[0].lblAmount,
    	"refNo" :transferData[0].RTPTranRef,
    	"reqUser" :  transferData[0].SenderAcctName
        //"opstatus":"0"
      };
      //resultTable["TranAmt"] = gblPushData["amount"];
  validatingTFPushdata(resultTable);
     // executeRTPTransfer(resultTable);
  }
}*/

function onClickTransferBillPay(transferData)
{     
  rtp_log("transferData" +JSON.stringify(transferData));
  if(transferData != null || transferData != undefined){
	var resultTable = {
     	 "refNo" :transferData[0].RTPTranRef,
    	"reqUser" :  transferData[0].SenderAcctName,
       "ReceiverAcctNo": transferData[0].ReceiverAcctNo,
		"RTPTranRef": transferData[0].RTPTranRef,
		"SenderBankCode": transferData[0].SenderBankCode,
		"SenderProxyID": transferData[0].SenderProxyID,
		"SenderProxyType": transferData[0].SenderProxyType,
		"TranAmt": transferData[0].lblAmount,
    	"SenderAcctName" :  transferData[0].SenderAcctName,
      	"SenderAcctNo" :  transferData[0].SenderAcctNo,
		"ReceiverProxyID" :  transferData[0].ReceiverProxyID,
      	"RTPProduct" :  transferData[0].RTPProduct,
     	"ExpiryDate" :  transferData[0].expiryDate,
      	"ReceiverAcctDispName" :  transferData[0].ReceiverAcctDispName,
      	"SenderAcctName" :  transferData[0].SenderAcctName,
      	"Memo" :  transferData[0].lblDescription,
     	"ReceiverAcctDispName" : transferData[0].ReceiverAcctDispName,
        "ReceiverAcctName" : transferData[0].ReceiverAcctName,
        "ReceiverBankCode" : transferData[0].ReceiverBankCode,
        "ReceiverProxyType" : transferData[0].ReceiverProxyType, 
      	"RTPType" : transferData[0].RTPType,
      	"Reference1" : transferData[0].Reference1,
      	"Reference2" : transferData[0].Reference2,
        "Reference3" : transferData[0].Reference3,
        "Reference4" : transferData[0].Reference4,
        "Reference5" : transferData[0].Reference5
       // "opstatus":"0"
      };
 	 validatingRTPPushdata(resultTable);
  }
}

