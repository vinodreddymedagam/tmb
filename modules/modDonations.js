  //Type your code here
  gblMasterDonationList = [];
  gblDonationSegementData = [];

  function frmDonationIntroductionInit() {
      frmDonationIntroduction.btnStart.onClick = getEDonationDataList;
      frmDonationIntroduction.flexClose.onClick = getEDonationDataList;
      frmDonationIntroduction.onDeviceBack = doNothing;
  }

  function frmDonationSelectBillersInit() {
      frmDonationSelectBillers.btnSearchBiller.onClick = showHeaderSearchTextBox;
      frmDonationSelectBillers.segDonationBillers.onRowClick = showDonationSelectAmount;
      frmDonationSelectBillers.btnCancelSearch.onClick = hideDonationSearchTextBox;
      frmDonationSelectBillers.flexIntrodunctionLink.onClick = showIntroductionPopup;
      frmDonationSelectBillers.flexClose.onClick = showIntroductionPopup;
      frmDonationSelectBillers.txtBillerSearch.onTextChange = searchDonationBillers;
      frmDonationSelectBillers.onDeviceBack = doNothing;
  }

  function getEDonationDataList() {
      //TODO Add e-donation flag
      showLoadingScreen();
      var inputParams = {
          IsActive: "1",
          BillerGroupType: 0,
          BillerCategoryID: 27, // For eDonations only
          clientDate: getCurrentDate(),
          flagBillerList: "MB"
      };
      invokeServiceSecureAsync("masterBillerInquiry", inputParams, eDonationInqCallback);
  }

  function eDonationInqCallback(status, callBackResponse) {
      if (status == 400) {
          if (callBackResponse["opstatus"] == "0") {
              gblMasterDonationList = [];
              gblMasterDonationList =  callBackResponse["MasterBillerInqRs"];
              var responseData = callBackResponse["MasterBillerInqRs"];
              if (responseData.length > 0) {
                  createEDonationSegment(callBackResponse["MasterBillerInqRs"]);
              }
          } else {
              dismissLoadingScreen();
              alert(kony.i18n.getLocalizedString("ECGenericError"));
          }
      } else {
          if (status == 300) {
              dismissLoadingScreen();
              alert(kony.i18n.getLocalizedString("ECGenericError"));
          }
      }
  }
 function showdonationBillers(){
   frmDonationSelectBillers.show();
 }

  function createEDonationSegment(collectiondata) {
      var locale = kony.i18n.getCurrentLocale();
      var billerGroup = ""
      var billerName = "";
      var donationBillerData = [];
      frmDonationSelectBillers.segDonationBillers.removeAll();
      for (var i = 0; i < 20; i++) {
          var imagesUrl = "";
          imagesUrl = loadBillerIcons(collectiondata[i]["BillerCompcode"]);
          //
          if (kony.string.startsWith(locale, "en", true) == true) {
              billerGroup = collectiondata[i]["BillerNameEN"] + " (" + collectiondata[i]["BillerCompcode"] + ")";
              billerName = collectiondata[i]["BillerNameEN"];
          } else {
              billerGroup = collectiondata[i]["BillerNameTH"] + " (" + collectiondata[i]["BillerCompcode"] + ")";
              billerName = collectiondata[i]["BillerNameTH"];
          }
          var billerSegment = {
              "lblBillerName": billerName,
              "lblBillerGroup": billerGroup,
              "BillerTaxID": "BillerTaxID",
              "lblRef1Value": "lblRef1Value",
              "BillerCompCode": collectiondata[i]["BillerCompcode"],
              "imgBillerLogo": {
                  "src": imagesUrl,

              }, "imgFav": {
                  "src": "icon_fav_active.png",

              }, "flexFav":{
                  "onClick" :addDonationBillerToFav
                }
          }
          donationBillerData.push(billerSegment);
      }
      gblDonationSegementData = donationBillerData; // THis global variable will be used when reset the search text area or less then 3 charecters 

      frmDonationSelectBillers.segDonationBillers.setData(donationBillerData);
      frmDonationSelectBillers.show();
      dismissLoadingScreen();
  }

  function searchDonationBillers() {
      var filteredData = [];
      var searchText = frmDonationSelectBillers.txtBillerSearch.text;
      var segmentData = gblDonationSegementData;
      var searchValue = "";
      if (segmentData.length > 0) {
          if (searchText.length > 3) {
              searchText = searchText.toLowerCase();
              kony.print("searchText-->"+searchText);
              for (j = 0, i = 0; i < segmentData.length; i++) {
              kony.print("-->"+segmentData[i].BillerCompCode.toLowerCase());
              kony.print("-->"+segmentData[i].BillerTaxID.toLowerCase());
              kony.print("-->"+segmentData[i].lblBillerName.toLowerCase());
              kony.print("-->"+segmentData[i].lblBillerName.toLowerCase());
              searchValue =  segmentData[i].lblBillerName.toLowerCase() 
                                  + "~" + segmentData[i].BillerCompCode.toLowerCase() 
                                  + "~" + segmentData[i].BillerTaxID
                                  + "~" + segmentData[i].lblRef1Value;
              kony.print("searchValue-->"+searchValue);
              kony.print("searchValue-->"+searchValue.indexOf(searchText));
              if (searchValue.indexOf(searchText) > -1) {
                          frmDonationSelectBillers.segDonationBillers.isVisible = true;
                          frmDonationSelectBillers.lblNoresults.isVisible = false;
                          filteredData[j] = segmentData[i];
                          j++;
				   kony.print("filteredData-->"+filteredData);
                   
                  }
              }
            if(filteredData.length >0){
              frmDonationSelectBillers.segDonationBillers.setData(filteredData)
            }else{
                    frmDonationSelectBillers.segDonationBillers.isVisible = false;
                    frmDonationSelectBillers.lblNoresults.isVisible = true;
            }
          }else{
            frmDonationSelectBillers.segDonationBillers.setData(gblDonationSegementData)
            frmDonationSelectBillers.segDonationBillers.isVisible = true;
            frmDonationSelectBillers.lblNoresults.isVisible = false;
          }

      }
  }

  function addDonationBillerToFav(event,rowClicked){
    var index = rowClicked["rowIndex"];
    kony.print("index-->"+index)
    kony.print("index-->"+gblMasterDonationList[index]);
    kony.print("index-->"+frmDonationSelectBillers.segDonationBillers.data[index]);
  }
  function showHeaderSearchTextBox() {
      frmDonationSelectBillers.flexSearchBox.isVisible = true;
      frmDonationSelectBillers.txtBillerSearch.text = "";
      frmDonationSelectBillers.txtBillerSearch.setFocus(true);
      frmDonationSelectBillers.lblHeader.isVisible = false;
      frmDonationSelectBillers.btnCancelSearch.isVisible = true;
      frmDonationSelectBillers.btnSearchBiller.isVisible = false;
  }

  function hideDonationSearchTextBox() {
      frmDonationSelectBillers.flexSearchBox.isVisible = false;
      frmDonationSelectBillers.txtBillerSearch.text = "";
      frmDonationSelectBillers.lblHeader.isVisible = true;
      frmDonationSelectBillers.btnCancelSearch.isVisible = false;
      frmDonationSelectBillers.btnSearchBiller.isVisible = true;
      frmDonationSelectBillers.segDonationBillers.isVisible = true;
      frmDonationSelectBillers.lblNoresults.isVisible = false;
      frmDonationSelectBillers.segDonationBillers.setData(gblDonationSegementData);

  }

  function showDonationSelectAmount() {
      var selectedRowDetails = frmDonationSelectBillers.segDonationBillers.selectedRowItems;
      var locale = kony.i18n.getCurrentLocale();
      if (locale == "en_US"){
      		if(gblQRPayData["billerCompCode"].length <= 4){
            	frmDonationSelectAmount.lblFoundationName.text = gblQRPayData["billerNameEN"];
            } else {
             	frmDonationSelectAmount.lblFoundationName.text = gblQRPayData["toAccountName"];           
            }
      } else if (locale == "th_TH") {
      		if(gblQRPayData["billerCompCode"].length <= 4){
            	frmDonationSelectAmount.lblFoundationName.text = gblQRPayData["billerNameTH"];
            } else {
            	frmDonationSelectAmount.lblFoundationName.text = gblQRPayData["toAccountName"];
            }
      }
      frmDonationSelectAmount.lblBillerFoundationGrp.text = selectedRowDetails[0]["lblBillerGroup"];
      frmDonationSelectAmount.show();
  }

  function showIntroductionPopup() {
      if (frmDonationSelectBillers.flexIntroduction.isVisible) {
          frmDonationSelectBillers.flexIntroduction.isVisible = false;
      } else {
          frmDonationSelectBillers.flexIntroduction.isVisible = true;
      }
  }