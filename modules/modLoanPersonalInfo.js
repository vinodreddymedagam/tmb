//gblPersonalInfo = {};
//gblPersonalInfo.savePI = false;

function initEventOfPersonalInfo(){
  frmLoanPersonalInfo.onDeviceBack = disableBackButton;
  frmLoanPersonalInfo.preShow = preshowOfPersonalInfo;
  //frmLoanPersonalInfo.postShow = getPersonalInfoFields;
  
  frmLoanPersonalInfo.btnBack.onClick =  settingSavedValueInPI;
	
  //frmLoanPersonalInfo.flxImgThaiSalution.onClick = onClickPersonalDropDown;
  //frmLoanPersonalInfo.flxImgTitleType.onClick = onClickDropDownIcons;
  
  frmLoanPersonalInfo.flxThaiSaluation.onClick = onClickPersonalDropDown;
  frmLoanPersonalInfo.flxTitleType.onClick = onClickDropDownIcons;
  
 //#ifdef android
  if(null !== frmLoanPersonalInfo.imgCheck){
    frmLoanPersonalInfo.imgCheck.onTouchEnd =  handleCheckUnCheck; 
  }
  //#endif
  
  //#ifdef iphone
  frmLoanPersonalInfo.switchEstmt.onSlide = switchEstmtInPersonalInfo;
  //#endif
  
  //frmLoanPersonalInfo.lblEStatementVal.onTextChange = handleSaveBtnPersonalInfo;
  
//   frmLoanPersonalInfo.flxAddress.onClick = addressFlexOnclick;
  
//   frmLoanPersonalInfo.flxAddessWithData.onClick = addressFlexOnclick;
//   frmLoanPersonalInfo.flxAddress.onClick = editContactAddress;
  
  frmLoanPersonalInfo.btnEditContactPencel.onClick = editContactAddress;
//   frmLoanPersonalInfo.flxMobileNoInfo.onClick = editMobileNum;
  
  frmLoanPersonalInfo.flxResidentialStatus.onClick = onClickDropDownIcons;
  
  frmLoanPersonalInfo.flxSorceOfIncome.onClick =  onClickPersonalDropDown;
  
  frmLoanPersonalInfo.btnCloseSIPopUp.onClick = btnCloseSIPopUponClick;
  frmLoanPersonalInfo.btnNext.onClick = onClickOfDoneBtnPersonal;
  
  frmLoanPersonalInfo.flxAddNo.onClick = flxAddNoOnClick;
  
  frmLoanPersonalInfo.flxBuildingNo.onClick = flxBuildingNoOnClick;
  frmLoanPersonalInfo.tbxAddrVal.onTextChange = onTextChangeBuildNoPersonal;
  frmLoanPersonalInfo.tbxAddrVal.onBeginEditing = flexAddressEmptyCheck;
  
  frmLoanPersonalInfo.tbxBuildingNo.onTextChange = onTextChangeBuildNoPersonal;
  
  frmLoanPersonalInfo.tbxBuildingNo.onBeginEditing = flexAddressEmptyCheck;
     
  frmLoanPersonalInfo.FlexFloorNo.onClick = flxFloorNoOnClick;
  
  frmLoanPersonalInfo.tbxFloorNo.onTextChange = onTextChangeBuildNoPersonal;
  
  frmLoanPersonalInfo.tbxFloorNo.onBeginEditing = flexAddressEmptyCheck;
  
	frmLoanPersonalInfo.flxSoi.onClick = flxSoiOnClick;
  
  frmLoanPersonalInfo.tbxSoi.onTextChange = onTextChangeBuildNoPersonal;
  
  //frmLoanPersonalInfo.tbxSoi.onDone = onTextChangeBuildNoPersonal;
  frmLoanPersonalInfo.tbxSoi.onBeginEditing = flexAddressEmptyCheck;
    
  frmLoanPersonalInfo.FlexRoad.onClick = flxRoadOnClick;
  
  frmLoanPersonalInfo.tbxRoad.onTextChange = onTextChangeBuildNoPersonal;
  //frmLoanPersonalInfo.tbxRoad.onDone = onTextChangeBuildNoPersonal;
  frmLoanPersonalInfo.tbxRoad.onBeginEditing = flexAddressEmptyCheck;
  
  frmLoanPersonalInfo.FlexMoo.onClick = flxMooOnClick;
  
  frmLoanPersonalInfo.tbxMoo.onTextChange = onTextChangeBuildNoPersonal;
  //frmLoanPersonalInfo.tbxMoo.onDone = onTextChangeBuildNoPersonal;
  frmLoanPersonalInfo.tbxMoo.onBeginEditing = flexAddressEmptyCheck;
  
  frmLoanPersonalInfo.flxSubDistrict.onClick = flxSubDistrictOnClick;
  frmLoanPersonalInfo.tbxSubDistrict.onTextChange = handleSaveBtnContactAddPopUp; //searchDistrictOnChange;
  //frmLoanPersonalInfo.tbxSubDistrict.onDone = searchDistrictOnChange;
  frmLoanPersonalInfo.tbxSubDistrict.onBeginEditing = flexAddressEmptyCheck;
  
  frmLoanPersonalInfo.segResult.onRowClick = handleAddressSegRowClick;
  
  frmLoanPersonalInfo.tbxDistrict.onTextChange = handleSaveBtnContactAddPopUp;
  frmLoanPersonalInfo.tbxProvince.onTextChange = handleSaveBtnContactAddPopUp;
  frmLoanPersonalInfo.tbxZipCode.onTextChange = handleSaveBtnContactAddPopUp;
  
  //frmLoanPersonalInfo.btnMobilePopUpClose.onClick = onClickOfPopUpClosePeronsal;
  
  //Contact Number pop events
//   frmLoanPersonalInfo.flxMobileNumber.onClick = flxContactOnTouchEvent;
  
//   frmLoanPersonalInfo.txtMobileNumber.onTextChange = handleSaveBtnMobileNumberPopUp;
//  frmLoanPersonalInfo.txtMobileNumber.onDone = onTextChangeMobileNoPersonal;
//   frmLoanPersonalInfo.txtMobileNumber.onBeginEditing = enableContactNumberField;
    
//    frmLoanPersonalInfo.flxHomeNo.onClick = flxContactOnTouchEvent;
//   frmLoanPersonalInfo.txtHomeNo.onTextChange = handleSaveBtnMobileNumberPopUp;
//  frmLoanPersonalInfo.txtHomeNo.onDone = onTextChangeMobileNoPersonal;
//   frmLoanPersonalInfo.txtHomeNo.onBeginEditing = flexAddressEmptyCheck;
    
//   frmLoanPersonalInfo.flxExtNo.onClick = flxContactOnTouchEvent;
  
//   frmLoanPersonalInfo.txtExtNo.onTextChange = handleSaveBtnMobileNumberPopUp;
//  frmLoanPersonalInfo.txtExtNo.onDone = onTextChangeMobileNoPersonal;
//   frmLoanPersonalInfo.txtExtNo.onBeginEditing = onTextChangeMobileNoPersonal;
   
  
  frmLoanPersonalInfo.btnSaveContactNo.onClick = contactPopUpSaveClick;
  
  //frmLoanPersonalInfo.BtnPopUpClosePopUp.onClick = onClickOfPopUpClosePeronsal;
  
  frmLoanPersonalInfo.flxSourceOfIncomePopUp.onClick = disableBackButton;
  frmLoanPersonalInfo.flxAddressPopUp.onClick = disableBackButton;
  frmLoanPersonalInfo.tbxSearch.onTextChange = searchloanworkinginfo;
  
}

function editContactAddress(){
  try {
          kony.modules.loadFunctionalModule("myProfileModule");               
       } catch (err) {
           kony.print("Error loading functional modules");
           return false;
       }
  
  try{
//  frmViewMyProfilePreShow();
  TMBUtil.DestroyForm(frmeditMyProfiles);
  editbuttonflag="profile";
  gblLoanNav = "popup";
  viewloanprofileServiceCall();
//   if(gblLoanNav == "popup"){
//     frmeditMyProfile.hbox475868836198148.setVisibility(false);
//     frmeditMyProfile.hbox475124774143.setVisibility(false);
//     frmeditMyProfile.hbxArrow.setVisibility(false);
//     frmeditMyProfile.hbox475868836197589.setVisibility(false);
//     frmeditMyProfile.hbox475868836198148.setVisibility(false);
//     frmeditMyProfile.hboxClosebtn.setVisibility(true);
//     frmeditMyProfile.hboxTitleHeader.setVisibility(true);
//   }else{
//     frmeditMyProfile.hbox475868836198148.setVisibility(true);
//     frmeditMyProfile.hbox475124774143.setVisibility(true);
//     frmeditMyProfile.hbxArrow.setVisibility(true);
//     frmeditMyProfile.hbox475868836197589.setVisibility(true);
//     frmeditMyProfile.hbox475868836198148.setVisibility(true);
//     frmeditMyProfile.hboxClosebtn.setVisibility(false);
//     frmeditMyProfile.hboxTitleHeader.setVisibility(false);
//   }
//   frmeditMyProfile.show();
  }catch(e){
    alert("Exception in MyProfilePopup, e : "+e);
  }
}


function viewloanprofileServiceCall() {
	gblRetryCountRequestOTP = "0";
    //nulltheGlobalsMyProfile();
    var inputParams = {};
    //inputParams["deviceID"] = GBL_UNIQ_ID ;
  	//fix MIB-8330: Device Name wrong displaying on My Profile
  	inputParams["deviceID"] = gblDeviceIDUnEncrypt; 
    invokeServiceSecureAsync("MyProfileViewCompositeService", inputParams, viewloanprofileServiceCallBack);
}

function viewloanprofileServiceCallBack(status, resulttable) {
    if (status == 400) //success response
    {
        var subDistBan = kony.i18n.getLocalizedString("gblsubDtPrefixThaiB");
		var subDistNotBan = kony.i18n.getLocalizedString("gblsubDtPrefixThai") + ".";
		var distBan = kony.i18n.getLocalizedString("gblDistPrefixThaiB");
		var distNotBan = kony.i18n.getLocalizedString("gblDistPrefixThai") + ".";
        
        
        
        
        if (resulttable["opstatus"] == 0) {
               dismissLoadingScreen();
                if (resulttable["ebMaxLimitAmtCurrent"] != null) {
                    
                    gblEBMaxLimitAmtCurrent = resulttable.ebMaxLimitAmtCurrent;
                    gblEBMaxLimitAmtCurrentOld = gblEBMaxLimitAmtCurrent;
                    frmMyProfiles.lblDailytransLimitvalue.text = ProfileCommFormat(gblEBMaxLimitAmtCurrent)+ " " +kony.i18n.getLocalizedString("currencyThaiBaht");
                }
                if (resulttable["ebMaxLimitAmtHist"] != null) {
                    
                    gblEBMaxLimitAmtHist = resulttable.ebMaxLimitAmtHist;
                }
                if (resulttable["ebMaxLimitAmtRequest"] != null) {
                    
                    gblEBMaxLimitAmtReq = resulttable.ebMaxLimitAmtRequest;
                }
                if (resulttable["emailAddr"] != null) {
                    
                    gblEmailAddr = resulttable.emailAddr;
                    gblEmailAddrOld = gblEmailAddr;
                    frmMyProfiles.lblEmailVal.text = gblEmailAddr;
                }
                
                gblFacebookId = resulttable.fbUsername;
                gblFacebookIdOld = gblFacebookId;
                frmMyProfiles.lblfbidstudio1.text = gblFacebookId;//Modified by Studio Viz
                
                gblFBCode = resulttable["facebookId"];
                if (resulttable["ebTxnLimitAmt"] != null) {
                    
                    gblEbTxnLimitAmt = resulttable.ebTxnLimitAmt;
                    gblEbTxnLimitAmtOld = gblEbTxnLimitAmt;
                }
				if(resulttable["deviceNickName"]!=null){
					gblDeviceNickName = resulttable["deviceNickName"];
            		
            		frmMyProfiles.lblMyDeviceVal.text = gblDeviceNickName;
				}			
				if(resulttable["ibUserStatusId"] != null)
					ibUserStatusId = resulttable.ibUserStatusId;
				if(resulttable["mbUserStatusId"] != null)
					mbUserStatusId = resulttable.mbUserStatusId;
				if(resulttable["ibStatusFlag"] != null)
					ibStatusFlag = resulttable.ibStatusFlag;
				if(resulttable["mbStatusFlag"] != null)
					mbStatusFlag = resulttable.mbStatusFlag;
				if(resulttable["s2sBusinessHrsFlag"] != null)
					s2sBusinessHrsFlag = resulttable.s2sBusinessHrsFlag;
				if(resulttable["s2sStartTime"] != null)
					s2sStartTime = resulttable.s2sStartTime;
				if(resulttable["s2sEndTime"] != null)
					s2sEndTime = resulttable.s2sEndTime;
				if(resulttable["S2SstatusCode"] != null)
					s2sstatuscode = resulttable.S2SstatusCode;
				if(resulttable["S2SstatusDesc"] != null)
					s2sstatusdesc = resulttable.S2SstatusDesc;		
                
                gblCustomerName = resulttable["customerName"];
                gblCustomerNameTh = resulttable["customerNameTH"];
                customerName = resulttable["customerName"];
                if (kony.i18n.getCurrentLocale() == "th_TH") {
                    frmMyProfiles.lblProfileDescription.text = gblCustomerNameTh;
                } else {
                    frmMyProfiles.lblProfileDescription.text = gblCustomerName;
                }
                for (var i = 0; i < resulttable["ContactNums"].length; i++) {
                    var PhnType = resulttable["ContactNums"][i]["PhnType"];
                    
                    if (PhnType != null && PhnType != "" && resulttable["ContactNums"][i]["PhnNum"] != undefined) {
                        if (PhnType == "Mobile") {
                            if ((resulttable["ContactNums"][i]["PhnNum"] != null) && (resulttable["ContactNums"][i]["PhnNum"] != "" && resulttable["ContactNums"][i]["PhnNum"] != undefined)) {
                                gblPHONENUMBER = resulttable["ContactNums"][i]["PhnNum"];
                                gblPHONENUMBEROld = resulttable["ContactNums"][i]["PhnNum"];
                                
                                frmMyProfiles.lblMobileVal.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
                                GblMobileAL = HidePhnNum(gblPHONENUMBER);
                            }
                        }
                    }
                }
                for (var i = 0; i < resulttable["Persondata"].length; i++) {
                    var tempAddrtype = resulttable["Persondata"][i]["AddrType"];
                    if (tempAddrtype == "Primary") {
                        if (resulttable["Persondata"][i]["AddrType"] != null && resulttable["Persondata"][i]["AddrType"] != "" && resulttable["Persondata"][i]["AddrType"] != undefined) {
                            //if(resulttable["Persondata"][i]["addr3"]){}
                            if (resulttable["Persondata"][i]["addr3"] != null || resulttable["Persondata"][i]["addr3"] != "" || resulttable["Persondata"][i]["addr3"] != undefined) {
                                var adr3 = resulttable["Persondata"][i]["addr3"];
                                
                                var reg = / {1,}/;
                                var tempArr = [];
                                tempArr = adr3.split(reg);
                                
                                //if (resulttable["Persondata"][i]["City"] != null && resulttable["Persondata"][i]["City"] != "" && resulttable["Persondata"][i]["City"] != undefined) 
								{
                                   if (tempArr[0] != null && tempArr[1] != null && tempArr[0] != "" && tempArr[1] != "" && tempArr[0] != undefined && tempArr[1] != undefined) {
                                        if (resulttable["Persondata"][i]["City"] != null && resulttable["Persondata"][i]["City"] != "" && resulttable["Persondata"][i]["City"] != undefined) {
                                            if(tempArr[0].indexOf(subDistBan, 0) >= 0)
												gblsubdistrictValue = tempArr[0].substring(4);
											else  if(tempArr[0].indexOf(subDistNotBan, 0) >= 0) 
												gblsubdistrictValue = tempArr[0].substring(2);
											else 	gblsubdistrictValue = "";	
											if(tempArr[1].indexOf(distBan, 0) >= 0)  	 
												gbldistrictValue = tempArr[1].substring(3);
											else   if(tempArr[1].indexOf(distNotBan, 0) >= 0) 
												gbldistrictValue = tempArr[1].substring(2); 
											else 	gbldistrictValue = "";
                                        } 
                                    } else {
                                        gblStateValue = "";
                                        gblsubdistrictValue = "";
                                        gbldistrictValue = "";
                                    }
                                    gblAddress1Value = resulttable["Persondata"][i]["addr1"];
                                    gblAddress2Value = resulttable["Persondata"][i]["addr2"];
									if(resulttable["Persondata"][i]["City"] != null && resulttable["Persondata"][i]["City"] != "" && resulttable["Persondata"][i]["City"] != undefined)
										gblStateValue = resulttable["Persondata"][i]["City"];
									else	gblStateValue = "";
									if(resulttable["Persondata"][i]["PostalCode"] != null && resulttable["Persondata"][i]["PostalCode"] != "" && resulttable["Persondata"][i]["PostalCode"] != undefined)
										gblzipcodeValue = resulttable["Persondata"][i]["PostalCode"];
									else 	
										gblzipcodeValue = resulttable["Persondata"][i]["PostalCode"];
									if(resulttable["Persondata"][i]["CountryCodeValue"] != null && resulttable["Persondata"][i]["CountryCodeValue"] != "" && resulttable["Persondata"][i]["CountryCodeValue"] != undefined)	
										gblcountryCode = resulttable["Persondata"][i]["CountryCodeValue"];
									else	gblcountryCode = "";
                                    if (tempArr[0] == "" || tempArr[0] == undefined) {
                                        tempArr[0] = "";
                                    }
                                    if (tempArr[1] == "" || tempArr[1] == undefined) {
                                        tempArr[1] = "";
                                    }
                                    gblViewsubdistrictValue = tempArr[0];
                                    gblViewdistrictValue = tempArr[1];
                                    gblnewStateValue = resulttable["Persondata"][i]["StateProv"];
                                    frmeditMyProfile.txtAddress1.text = gblAddress1Value;
                                    frmeditMyProfile.txtAddress2.text = gblAddress2Value;
                                  
                                  	frmMyProfiles.lblContactVal.text = gblAddress1Value + " " + gblAddress2Value + " " + gblViewsubdistrictValue + " " + gblViewdistrictValue + " " + gblStateValue + " " + gblzipcodeValue + " " + gblcountryCode;
                                    frmeditMyProfiles.txtAddress1.text = gblAddress1Value;
                                    frmeditMyProfiles.txtAddress2.text = gblAddress2Value;
                                  	frmeditMyProfiles.txtSubDistrict.text = gblsubdistrictValue;
                                  	frmeditMyProfiles.lblDistrictVal.text = gbldistrictValue;
                                  	frmeditMyProfiles.lblProvinceVal.text = gblStateValue;
                                  	frmeditMyProfiles.lblZipCodeVal.text = gblzipcodeValue;
                                    gblnotcountry = false;
                                    if (resulttable["Persondata"][i]["CountryCodeValue"] != kony.i18n.getLocalizedString("Thailand")) {
                                        gblnotcountry = true;
                                    } else {
                                        gblnotcountry = false;
                                    }
                                    gblAddress = frmMyProfiles.lblContactVal.text;
                                }
                            }
                        }
                    } else if (tempAddrtype == "Registered") {
                        
                        //alert("address3 of the person data"+resulttable["Persondata"][i]["addr3"]);
                        if (resulttable["Persondata"][i]["AddrType"] != null && resulttable["Persondata"][i]["AddrType"] != "" && resulttable["Persondata"][i]["AddrType"] != undefined) {
                            if (resulttable["Persondata"][i]["addr3"] != null || resulttable["Persondata"][i]["addr3"] != "" || resulttable["Persondata"][i]["addr3"] != undefined) {
                                var adr3 = resulttable["Persondata"][i]["addr3"];
                                var reg = / {1,}/;
                                var tempArr = [];
                                tempArr = adr3.split(reg);
                                //if (resulttable["Persondata"][i]["City"] != null || resulttable["Persondata"][i]["City"] != "") 
								
                                    if (tempArr[0] != null && tempArr[1] != null && tempArr[0] != "" && tempArr[1] != "" && tempArr[0] != undefined && tempArr[1] != undefined) {
                                        if (resulttable["Persondata"][i]["City"] == kony.i18n.getLocalizedString("BangkokThaiValueProfile")) {
                                            gblregsubdistrictValue = tempArr[0];
                                            gblregdistrictValue = tempArr[1];
                                        } else {
                                            gblregsubdistrictValue = tempArr[0];
                                            gblregdistrictValue = tempArr[1];
                                        }
                                    }
                                 else {
                                    gblregStateValue = "";
                                    gblregsubdistrictValue = tempArr[0];
                                    gblregdistrictValue = tempArr[1];
                                }
                                gblregAddress1Value = resulttable["Persondata"][i]["addr1"];
                                gblregAddress2Value = resulttable["Persondata"][i]["addr2"];
								if(resulttable["Persondata"][i]["City"] != null && resulttable["Persondata"][i]["City"] != "" && resulttable["Persondata"][i]["City"] != undefined)
									gblregStateValue = resulttable["Persondata"][i]["City"];
								else gblregStateValue = "";
								if(resulttable["Persondata"][i]["PostalCode"] != null && resulttable["Persondata"][i]["PostalCode"] != "" && resulttable["Persondata"][i]["PostalCode"] != undefined)
									gblregzipcodeValue = resulttable["Persondata"][i]["PostalCode"];
								else 	gblregzipcodeValue = "";
								if(resulttable["Persondata"][i]["CountryCodeValue"] != null && resulttable["Persondata"][i]["CountryCodeValue"] != "" && resulttable["Persondata"][i]["CountryCodeValue"] != undefined)	
									gblregcountryCode = resulttable["Persondata"][i]["CountryCodeValue"];
                                else gblregcountryCode = "";									
                                frmMyProfiles.lblRegisterAddressVal.text = gblregAddress1Value + " " + gblregAddress2Value + " " + gblregsubdistrictValue + " " + gblregdistrictValue + " " + " " + gblregStateValue + " " + " " + gblregzipcodeValue + " " + " " + gblregcountryCode;
                            }
                        }
                    }
                }
            }
        } 
  getIBMBEditProfileStatus();
}


// function editMobileNum(){
//   try {
//           kony.modules.loadFunctionalModule("myProfileModule");               
//        } catch (err) {
//            kony.print("Error loading functional modules");
//            return false;
//        }
  
//   try{
//   gblLoanNav = "popup";
//   frmChangeMobNoTransLimitMB.show();
//   }catch(e){
//     alert("Exception in editMobileNoPopup, e : "+e);
//   }
// }

function onTextChangeMobileNoPersonal(){
  handleSaveBtnMobileNumberPopUp();
  onDoneContactPopUp();
}

function onTextChangeBuildNoPersonal(obj){
  handleSaveBtnContactAddPopUp();
  //flexAddressEmptyCheck(obj);
}

function navigateToLoanPersonalInfo(){
  frmLoanPersonalInfo.show();	
}

function onClickOfPopUpClosePeronsal(){
  //frmLoanPersonalInfo.show();
}

// function onClickOfDoneBtnPersonal(){
//   	gblPersonalInfo.savePI = true;
//   	//nextJSONPreparation();
//   frmLoanApplicationForm.show();
// }

function onClickOfDoneBtnPersonal(){
  try{	
  	// need to pass below values to updateCustomerInfo service
  	//titleTypeCode , thaiSalutationCode, residentType, emailStatementFlag, sourceFromCountry
     // thaiName, thaiSurName, engSurName, engName
  var emailStatementFlag = "Y";
    if(gblDeviceInfo["name"] == "android"){
        if(null !== frmLoanPersonalInfo.imgCheck.src && frmLoanPersonalInfo.imgCheck.src == "chkbox_uncheck.png"){
        	emailStatementFlag = "N";
      }
    }else{
      if(frmLoanPersonalInfo.switchEstmt.selectedIndex==1){
           emailStatementFlag = "N";
       }
    }
    var inputParams = {
 		titleTypeCode : "G",
 		thaiSalutationCode : frmLoanPersonalInfo.lblSalutationEntryCode.text,
 		residentType : frmLoanPersonalInfo.lblResidenceCode.text,
 		emailStatementFlag : emailStatementFlag,
      	sourceFromCountry : frmLoanPersonalInfo.lblSrcIncomCountryCode.text,
      	thaiName : frmLoanPersonalInfo.lblThaiNameVal.text,
      	thaiSurName : frmLoanPersonalInfo.lblThaiSurNameVal.text,
      	engSurName : frmLoanPersonalInfo.lblEngSurNameVal.text,
      	engName : frmLoanPersonalInfo.lblEngNameVal.text,
        email : frmLoanPersonalInfo.lblEmailWithData.text,
        mobileNo : gblPHONENUMBER,
        address : frmLoanPersonalInfo.lblContactAddressInfo.text,
		caId : gblCaId,
        personalInfoSavedFlag : "Y"
      	
	};
    kony.print("inputParams of updateCustInfoJavaSerice : "+JSON.stringify(inputParams));
	showLoadingScreen();
	invokeServiceSecureAsync("updateCustInfoJavaService", inputParams, callbackOfSavePersonalInfoFields);
  }catch(e){
    alert("Exception in onClickOfDoneBtnPersonal function , "+e);
  }
}

function callbackOfSavePersonalInfoFields(status, result){
    try{
      if (result.opstatus == "0") {
        dismissLoadingScreen();
        kony.print("result : "+JSON.stringify(result));
        
        gblPersonalInfo.savePI = true;
        callbackOfgetPersonalInfoFields(status, result);
        //frmLoanApplicationForm.show();
      }else {
        dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        return false;
      }
    }catch(e){
    alert("Exception in callbackOfSavePersonalInfoFields, e : "+e);
      dismissLoadingScreen();
  }
  dismissLoadingScreen();
}

function switchEstmtInPersonalInfo(){
    if(frmLoanPersonalInfo.switchEstmt.selectedIndex == 1){
      frmLoanPersonalInfo.lblEStatementVal.text = getLocalizedString("keyNo"); //"NO";
    }else if(frmLoanPersonalInfo.switchEstmt.selectedIndex === 0){
      frmLoanPersonalInfo.lblEStatementVal.text = getLocalizedString("keyYes"); //"YES";
    }
}  

function settingSavedValueInPI(){
  try{
    navigateBackToApplicationForm();
    
    frmLoanPersonalInfo.lblSalVal.text = gblPersonalInfo.thaiSalTH;
    frmLoanPersonalInfo.lblSalutationEntryCode.text = gblPersonalInfo.thaiSalutationCode;

    frmLoanPersonalInfo.lblResidenceCode.text = gblPersonalInfo.residentTypeCode;
    frmLoanPersonalInfo.lblResidentStatusVal.text = gblPersonalInfo.residentTypeTH;

    frmLoanPersonalInfo.lblSrcIncomCountryCode.text = gblPersonalInfo.countryOfIncomeCode;
    var locale = kony.i18n.getCurrentLocale();
    if("th_TH" === locale){
      frmLoanPersonalInfo.lblSourceCountry.text = gblPersonalInfo.countryOfIncomeTH;
    }else{
      frmLoanPersonalInfo.lblSourceCountry.text = gblPersonalInfo.countryOfIncomeEN;
    }
	
    var estatementVal = getLocalizedString("keyYes");
    if(gblPersonalInfo.emailStatementFlag !== undefined){
      if("N" === result.emailStatementFlag){
          estatementVal = getLocalizedString("keyNo");
      }
    }
    kony.print("###  estatementVal : "+estatementVal);
    frmLoanPersonalInfo.lblEStatementVal.text = estatementVal
    
    //#ifdef android
    if(null !== frmLoanPersonalInfo.imgCheck){
      if(frmLoanPersonalInfo.lblEStatementVal.text == getLocalizedString("keyNo")){
        frmLoanPersonalInfo.imgCheck.src = "chkbox_uncheck.png";
      }else{
        frmLoanPersonalInfo.imgCheck.src = "chkbox_checked.png";
      }
    }  
    //#endif
    
    //#ifdef iphone
    if(frmLoanPersonalInfo.lblEStatementVal.text == getLocalizedString("keyNo")){
      frmLoanPersonalInfo.switchEstmt.selectedIndex = 1;
    }else{
      frmLoanPersonalInfo.switchEstmt.selectedIndex = 0;
    }
    //#endif

  }catch(e){
    kony.print("exception occured in settingSavedValueInPI, "+e);
  }
}  

function navigateBackToApplicationForm(){
  frmLoanApplicationForm.show();
}

function addressFlexOnclick() {
  //frmLoanPersonalInfo.flxHeader.setVisibility(true);
  //alert(frmLoanPersonalInfo.flxHeader.isVisible);
  frmLoanPersonalInfo.btnBack.onClick = onClickOfBackInContactAddress;
  frmLoanPersonalInfo.lblApplicationForm.text = kony.i18n.getLocalizedString("ContactAddress");
  frmLoanPersonalInfo.flxAddressPopUp.setVisibility(true);
//   frmLoanPersonalInfo.flxMain.setVisibility(false);
  if(frmLoanPersonalInfo.flxAddessWithData.isVisible) {
    openAddressPopUpWithEnteredData();
  } else {
    openAddressPopUpWithOutData();
  }
  frmLoanPersonalInfo.flxAddNo.setFocus(true);
}

function onClickOfBackInContactAddress(){
  frmLoanPersonalInfo.btnBack.onClick = navigateBackToApplicationForm;
  frmLoanPersonalInfo.flxMain.setVisibility(true);
  frmLoanPersonalInfo.flxAddressPopUp.setVisibility(false);
  frmLoanPersonalInfo.lblApplicationForm.text = kony.i18n.getLocalizedString("ApplicationForm_Title");
  frmLoanPersonalInfo.flxsaluation.setFocus(true);
  //alert("Set focus to false");
}


function openAddressPopUpWithEnteredData() {
  //Added for Pop Clicks
  if(frmLoanPersonalInfo.flxAddessWithData.isVisible) {
//     frmLoanPersonalInfo.tbxAddrVal.text = frmLoanPersonalInfo.lblAddWithData.text;
//     frmLoanPersonalInfo.tbxBuildingNo.text = frmLoanPersonalInfo.lblBuildingWithData.text;
//     frmLoanPersonalInfo.tbxFloorNo.text = frmLoanPersonalInfo.lblFloorVal.text;
//     frmLoanPersonalInfo.tbxMoo.text = "";
//     frmLoanPersonalInfo.tbxSoi.text = "";
//     frmLoanPersonalInfo.tbxRoad.text = frmLoanPersonalInfo.lblRoadNoWithdata.text;

//     frmLoanPersonalInfo.tbxFloorNo.text = frmLoanPersonalInfo.lblRoadNoWithdata.text;
//     frmLoanPersonalInfo.tbxMoo.text = frmLoanPersonalInfo.lblRoadNoWithdata.text;
//     frmLoanPersonalInfo.tbxSoi.text =frmLoanPersonalInfo.lblRoadNoWithdata.text;
//     frmLoanPersonalInfo.tbxRoad.text = frmLoanPersonalInfo.lblRoadNoWithdata.text;
  }
}

function openAddressPopUpWithOutData(){
  try{
  frmLoanPersonalInfo.lblMainAddressNo.setVisibility(true);
  frmLoanPersonalInfo.lblMainBuildingNo.setVisibility(true);
  frmLoanPersonalInfo.lblMainFloorNo.setVisibility(true);
  frmLoanPersonalInfo.lblMainMoo.setVisibility(true);
  frmLoanPersonalInfo.lblMainSoi.setVisibility(true);
  frmLoanPersonalInfo.lblMainRoadNo.setVisibility(true);
  frmLoanPersonalInfo.lblSubDisctricHeader.setVisibility(true);  
  frmLoanPersonalInfo.btnSaveAddr.onClick  = null;
  frmLoanPersonalInfo.btnSaveAddr.skin ="btnGreyBGNoRound";
  frmLoanPersonalInfo.btnSaveAddr.focusSkin ="btnGreyBGNoRound";
  frmLoanPersonalInfo.lblAddressNo.setVisibility(false);
  frmLoanPersonalInfo.tbxAddrVal.setVisibility(false);
  frmLoanPersonalInfo.lblBuildingNo.setVisibility(false);
  frmLoanPersonalInfo.tbxBuildingNo.setVisibility(false);
  frmLoanPersonalInfo.lblFloorNo.setVisibility(false);
  frmLoanPersonalInfo.tbxFloorNo.setVisibility(false);
  frmLoanPersonalInfo.lblMoo.setVisibility(false);
  frmLoanPersonalInfo.tbxMoo.setVisibility(false);
  frmLoanPersonalInfo.lblSoi.setVisibility(false);
  frmLoanPersonalInfo.tbxSoi.setVisibility(false);
  frmLoanPersonalInfo.lblRoad.setVisibility(false);
  frmLoanPersonalInfo.tbxRoad.setVisibility(false);
  frmLoanPersonalInfo.lblSubDistrict.setVisibility(false);
  frmLoanPersonalInfo.tbxSubDistrict.setVisibility(false);  
  frmLoanPersonalInfo.tbxBuildingNo.text= "";
  frmLoanPersonalInfo.tbxAddrVal.text="";
  frmLoanPersonalInfo.tbxFloorNo.text="";
  frmLoanPersonalInfo.tbxMoo.text="";
  frmLoanPersonalInfo.tbxRoad.text="";
  frmLoanPersonalInfo.tbxSoi.text="";
  frmLoanPersonalInfo.tbxSubDistrict.text = "";
    frmLoanPersonalInfo.tbxDistrict.text = "";
    frmLoanPersonalInfo.tbxProvince.text = "";
    frmLoanPersonalInfo.tbxZipCode.text = "";
  }catch(e){
    alert("Exception in openAddressPopUpWithOutData, e : "+e);
  }
}

function flexAddressEmptyCheck(textObj) {
  //alert("textObj : "+textObj);
  var arry = ["tbxAddrVal","tbxBuildingNo","tbxFloorNo","tbxSoi","tbxMoo","tbxRoad","tbxSubDistrict"];
  var tbxVal = textObj.id;
  var index = arry.indexOf(tbxVal);
  arry.splice(index, 1);
 // alert("arry : "+arry);
  if(arry !== null && arry.length > 0) {
    for(var i = 0; i < arry.length; i++) {
      var val = frmLoanPersonalInfo[arry[i]].text;
      if(arry[i] === "tbxAddrVal") {
        if(val === "") {
          frmLoanPersonalInfo.lblMainAddressNo.isVisible=true;
          frmLoanPersonalInfo.lblAddressNo.isVisible=false;
          frmLoanPersonalInfo.tbxAddrVal.isVisible=false;
          //frmLoanPersonalInfo.tbxAddrVal.text = "";
          //frmLoanPersonalInfo.tbxAddrVal.setFocus(false);
        }

      } else if (arry[i] === "tbxBuildingNo") {
        if(val === "") {
          frmLoanPersonalInfo.lblMainBuildingNo.isVisible=true;
          frmLoanPersonalInfo.lblBuildingNo.isVisible=false;
          frmLoanPersonalInfo.tbxBuildingNo.isVisible=false;
          //frmLoanPersonalInfo.tbxBuildingNo.text = "";
          //frmLoanPersonalInfo.tbxBuildingNo.setFocus(false);
        }

      } else if (arry[i] === "tbxFloorNo") {
        if(val === "") {
          frmLoanPersonalInfo.lblMainFloorNo.isVisible=true;
          frmLoanPersonalInfo.lblFloorNo.isVisible=false;
          frmLoanPersonalInfo.tbxFloorNo.isVisible=false;
          //frmLoanPersonalInfo.tbxFloorNo.text = "";
          //frmLoanPersonalInfo.tbxFloorNo.setFocus(false);
        }

      } else if (arry[i] === "tbxSoi") {
        if(val === "") {
          frmLoanPersonalInfo.lblMainSoi.isVisible=true;
          frmLoanPersonalInfo.lblSoi.isVisible=false;
          frmLoanPersonalInfo.tbxSoi.isVisible=false;
          //frmLoanPersonalInfo.tbxSoi.text = "";
          //frmLoanPersonalInfo.tbxSoi.setFocus(false);
        }

      } else if (arry[i] === "tbxRoad") {
        if(val === "") {
          frmLoanPersonalInfo.lblMainRoadNo.isVisible=true;
          frmLoanPersonalInfo.lblRoad.isVisible=false;
          frmLoanPersonalInfo.tbxRoad.isVisible=false;
          //frmLoanPersonalInfo.tbxRoad.text = "";
          //frmLoanPersonalInfo.tbxRoad.setFocus(false);
        }

      } else if (arry[i] === "tbxMoo") {
        if(val === "") {
          frmLoanPersonalInfo.lblMainMoo.isVisible=true;
          frmLoanPersonalInfo.lblMoo.isVisible=false;
          frmLoanPersonalInfo.tbxMoo.isVisible=false;
          //frmLoanPersonalInfo.tbxMoo.text = "";
          //frmLoanPersonalInfo.tbxMoo.setFocus(false);
        }

      } else if (arry[i] === "tbxSubDistrict") {
        if(val === "") {
          frmLoanPersonalInfo.lblSubDistrict.isVisible=false;
          frmLoanPersonalInfo.lblSubDisctricHeader.isVisible=true;
          frmLoanPersonalInfo.tbxSubDistrict.isVisible=false;
          //frmLoanPersonalInfo.tbxSubDistrict.setFocus(false);
        }

      }
    }
  }
  frmLoanPersonalInfo[tbxVal].setFocus(true);
}

function handleSaveBtnContactAddPopUp(){
  var buildingNo = frmLoanPersonalInfo.tbxBuildingNo.text;
  var tbxAddrVal = frmLoanPersonalInfo.tbxAddrVal.text;
  var tbxFloorNo = frmLoanPersonalInfo.tbxFloorNo.text;
  var tbxMoo = frmLoanPersonalInfo.tbxMoo.text;
  var tbxRoad = frmLoanPersonalInfo.tbxRoad.text;
  var tbxSoi = frmLoanPersonalInfo.tbxSoi.text;
  var tbxSubDistrict = frmLoanPersonalInfo.tbxSubDistrict.text;
  var tbxDistrict = frmLoanPersonalInfo.tbxDistrict.text;
  var tbxProvince = frmLoanPersonalInfo.tbxProvince.text;
  var tbxZipCode = frmLoanPersonalInfo.tbxZipCode.text;

  if(isNotBlank(buildingNo) && isNotBlank(tbxAddrVal) && isNotBlank(tbxFloorNo) && 
     isNotBlank(tbxMoo) && isNotBlank(tbxRoad) && isNotBlank(tbxSoi) && 
     isNotBlank(tbxSubDistrict) && isNotBlank(tbxDistrict) && isNotBlank(tbxProvince) &&
     isNotBlank(tbxZipCode) && tbxZipCode.length == 5)
  {
    frmLoanPersonalInfo.btnSaveAddr.setEnabled(true);
    frmLoanPersonalInfo.btnSaveAddr.skin ="btnBlueBGNoRound";
    frmLoanPersonalInfo.btnSaveAddr.focusSkin ="btnBlueBGNoRound";
    frmLoanPersonalInfo.btnSaveAddr.onClick  = popUpAddressSaveClick;
  } else{
    frmLoanPersonalInfo.btnSaveAddr.setEnabled(false);
    frmLoanPersonalInfo.btnSaveAddr.skin ="btnGreyBGNoRound";
    frmLoanPersonalInfo.btnSaveAddr.focusSkin ="btnGreyBGNoRound";
  }
}

function enableAddressAutoFocus () {
  var arrayLabel = ["tbxAddrVal","tbxBuildingNo","tbxFloorNo","tbxSoi","tbxMoo","tbxRoad"];
  flexAddressEmptyCheck(arrayLabel);
}


function flxAddNoOnClick(){
  frmLoanPersonalInfo.lblMainAddressNo.setVisibility(false);
  frmLoanPersonalInfo.lblAddressNo.setVisibility(true);
  frmLoanPersonalInfo.tbxAddrVal.setVisibility(true);
  frmLoanPersonalInfo.tbxAddrVal.text = isNotBlank(frmLoanPersonalInfo.tbxAddrVal.text) ? "" : frmLoanPersonalInfo.tbxAddrVal.text;
  frmLoanPersonalInfo.tbxAddrVal.setFocus(true);

}

function flxBuildingNoOnClick(){
  frmLoanPersonalInfo.lblMainBuildingNo.setVisibility(false);
  frmLoanPersonalInfo.lblBuildingNo.setVisibility(true);
  frmLoanPersonalInfo.tbxBuildingNo.setVisibility(true);
  frmLoanPersonalInfo.tbxBuildingNo.text = isNotBlank(frmLoanPersonalInfo.tbxBuildingNo.text) ? "" : frmLoanPersonalInfo.tbxBuildingNo.text;
  frmLoanPersonalInfo.tbxBuildingNo.setFocus(true);
}

function flxFloorNoOnClick(){
  frmLoanPersonalInfo.lblMainFloorNo.setVisibility(false);
  frmLoanPersonalInfo.lblFloorNo.setVisibility(true);
  frmLoanPersonalInfo.tbxFloorNo.setVisibility(true);
  frmLoanPersonalInfo.tbxFloorNo.text = isNotBlank(frmLoanPersonalInfo.tbxFloorNo.text) ? "" : frmLoanPersonalInfo.tbxFloorNo.text;
  frmLoanPersonalInfo.tbxFloorNo.setFocus(true);
}

function flxSoiOnClick(){
  frmLoanPersonalInfo.lblMainSoi.setVisibility(false);
  frmLoanPersonalInfo.lblSoi.setVisibility(true);
  frmLoanPersonalInfo.tbxSoi.setVisibility(true);
  frmLoanPersonalInfo.tbxSoi.text = isNotBlank(frmLoanPersonalInfo.tbxSoi.text) ? "" : frmLoanPersonalInfo.tbxSoi.text;
  frmLoanPersonalInfo.tbxSoi.setFocus(true);
}

function flxRoadOnClick(){
  frmLoanPersonalInfo.lblMainRoadNo.setVisibility(false);
  frmLoanPersonalInfo.lblRoad.setVisibility(true);
  frmLoanPersonalInfo.tbxRoad.setVisibility(true);
  frmLoanPersonalInfo.tbxRoad.text = isNotBlank(frmLoanPersonalInfo.tbxRoad.text) ? "" : frmLoanPersonalInfo.tbxRoad.text;
  frmLoanPersonalInfo.tbxRoad.setFocus(true);
}

function flxMooOnClick(){
  frmLoanPersonalInfo.lblMainMoo.setVisibility(false);
  frmLoanPersonalInfo.lblMoo.setVisibility(true);
  frmLoanPersonalInfo.tbxMoo.setVisibility(true);
  frmLoanPersonalInfo.tbxMoo.text = isNotBlank(frmLoanPersonalInfo.tbxMoo.text) ? "" : frmLoanPersonalInfo.tbxMoo.text;
  frmLoanPersonalInfo.tbxMoo.setFocus(true);
}

function flxSubDistrictOnClick(){
  frmLoanPersonalInfo.lblSubDisctricHeader.setVisibility(false);
  frmLoanPersonalInfo.lblSubDistrict.setVisibility(true);
  frmLoanPersonalInfo.tbxSubDistrict.setVisibility(true);
  frmLoanPersonalInfo.tbxSubDistrict.text = isNotBlank(frmLoanPersonalInfo.tbxSubDistrict.text) ? "" : frmLoanPersonalInfo.tbxSubDistrict.text;
  frmLoanPersonalInfo.tbxSubDistrict.setFocus(true);
}

function popUpAddressSaveClick(){
 try{
  var buildingNo = frmLoanPersonalInfo.tbxBuildingNo.text;
  var tbxAddrVal = frmLoanPersonalInfo.tbxAddrVal.text;
  var tbxFloorNo = frmLoanPersonalInfo.tbxFloorNo.text;
  var tbxMoo = frmLoanPersonalInfo.tbxMoo.text;
  var tbxRoad = frmLoanPersonalInfo.tbxRoad.text;
  var tbxSoi = frmLoanPersonalInfo.tbxSoi.text;
//   frmLoanPersonalInfo.lblAddWithData.text =tbxAddrVal;
//   frmLoanPersonalInfo.lblBuildingWithData.text =buildingNo;
//   frmLoanPersonalInfo.lblRoadNoWithdata.text = tbxRoad;
  
  var tbxSubDistrict = frmLoanPersonalInfo.tbxSubDistrict.text;
  var tbxDistrict = frmLoanPersonalInfo.tbxDistrict.text;
  var tbxProvince = frmLoanPersonalInfo.tbxProvince.text;
  var tbxZipCode = frmLoanPersonalInfo.tbxZipCode.text;
  
  var contactAddressInfo = buildingNo+ " " +tbxAddrVal+ " " +tbxFloorNo+ " " +tbxMoo+ " " +tbxRoad+ " " +tbxSoi+ " " +tbxSubDistrict+ " " +tbxDistrict+ " " +tbxProvince+ " " +tbxZipCode;
   
  frmLoanPersonalInfo.lblContactAddressInfo.text =  contactAddressInfo;
//   frmLoanPersonalInfo.lblFloorVal.text = tbxFloorNo;
//   frmLoanPersonalInfo.lblSubDistrictMainVal.text =tbxSubDistrict;
//   frmLoanPersonalInfo.lblDistrictMainVal.text = tbxDistrict;
//   frmLoanPersonalInfo.lblProvinceMainVal.text =tbxProvince;
//   frmLoanPersonalInfo.lblZipCodeMainVal.text =tbxZipCode;
 
  gblPersonalInfo.ContactAdress = {"lblAddNoVal" : tbxAddrVal,"lblBuildingVal" : buildingNo, "lblRoadNoVal" : tbxRoad,
                      "tbxSoi" : tbxSoi, "tbxMoo" : tbxMoo, "tbxFloorNo" : tbxFloorNo,
                     "tbxSubDistrict" : tbxSubDistrict, "tbxDistrict" : tbxDistrict,
                     "tbxProvince" : tbxProvince, "tbxZipCode" : tbxZipCode,};
  
  frmLoanPersonalInfo.flxMain.setVisibility(true);
   frmLoanPersonalInfo.flxAddressPopUp.setVisibility(false);
  frmLoanPersonalInfo.flxAddress.setVisibility(false);
  frmLoanPersonalInfo.flxAddessWithData.setVisibility(true);
  
  frmLoanPersonalInfo.lblApplicationForm.text = kony.i18n.getLocalizedString("ApplicationForm_Title");
   handleSaveBtnPersonalInfo();
   frmLoanPersonalInfo.flxAddessWithData.setFocus(true);
 }catch(e){
   kony.print("Exception in popUpAddressSaveClick : "+e);
 }
}

function preshowOfPersonalInfo() {
  try{
  frmLoanPersonalInfo.lblApplicationForm.text = getLocalizedString("ApplicationForm_Title");
  frmLoanPersonalInfo.lblPersonalInfo.text = getLocalizedString("Personal_Info");

  frmLoanPersonalInfo.lblTitleTypeHead.text = getLocalizedString("TitleType");
  frmLoanPersonalInfo.lblThaiSalHead.text = getLocalizedString("ThaiSalutation");

  frmLoanPersonalInfo.lblThaiName.text = getLocalizedString("ThaiName");
  frmLoanPersonalInfo.lblThaiSurName.text = getLocalizedString("ThaiSurname");
  frmLoanPersonalInfo.lblEngName.text = getLocalizedString("EngName");
  frmLoanPersonalInfo.lblEngSurName.text = getLocalizedString("EngSurname");

  frmLoanPersonalInfo.lblAddrWithDataTitle.text = getLocalizedString("EditProfile_msg");
  frmLoanPersonalInfo.lblEmailWithDataTitle.text = getLocalizedString("Email_label");
  frmLoanPersonalInfo.lblContactAddrWithData.text = getLocalizedString("ContactAddress");
  frmLoanPersonalInfo.lblMobileNumWithData.text = getLocalizedString("MobileNo_label");

  frmLoanPersonalInfo.lblResidentialBlue.text = getLocalizedString("Residential_Status_label");
  frmLoanPersonalInfo.lblResidentailsBlack.text = getLocalizedString("Residential_Status_label");
  frmLoanPersonalInfo.lblIncomeCountry.text = getLocalizedString("SourceofIncome");

  frmLoanPersonalInfo.lblEstmt.text = getLocalizedString("EStatement");
    
  frmLoanPersonalInfo.lblInformationMsg.text = kony.i18n.getLocalizedString("keybillernotfound");
    frmLoanPersonalInfo.tbxSearch.placeholder = kony.i18n.getLocalizedString("KeySearch");
    frmLoanPersonalInfo.btnNext.text = kony.i18n.getLocalizedString("SaveandContinue");
    
  frmLoanPersonalInfo.lblResidentStatusVal.setVisibility(false);
  frmLoanPersonalInfo.lblResidentialBlue.setVisibility(true);
  
  var residentVal = frmLoanPersonalInfo.lblResidentStatusVal.text;
    if(undefined != residentVal && residentVal.length > 0){
      frmLoanPersonalInfo.lblResidentialBlue.setVisibility(false);
		frmLoanPersonalInfo.lblResidentailsBlack.setVisibility(true);
		frmLoanPersonalInfo.lblResidentStatusVal.setVisibility(true);
    }else{
      frmLoanPersonalInfo.lblResidentialBlue.setVisibility(true);
		frmLoanPersonalInfo.lblResidentailsBlack.setVisibility(false);
		frmLoanPersonalInfo.lblResidentStatusVal.setVisibility(false);
    }
    
  frmLoanPersonalInfo.flxMain.setVisibility(true);
  kony.print("preshow of personal Info , gblPersonalInfo.savePI : "+gblPersonalInfo.savePI);
    if(gblPersonalInfo.savePI){
    frmLoanPersonalInfo.btnNext.skin = "btnBlue28pxLoan2";
  frmLoanPersonalInfo.btnNext.focusSkin="btnBlueBGNoRound150pxLoan";
  frmLoanPersonalInfo.btnNext.onClick = onClickOfDoneBtnPersonal;
    frmLoanPersonalInfo.lblcircle.skin = "skinpiGreenLineLone";
    frmLoanPersonalInfo.flxcircle.skin="flxGreenCircleLoan";
    
      //#ifdef android
      if(gblPersonalInfo.emailStatementFlag == "Y"){
         frmLoanPersonalInfo.imgCheck.src = "chkbox_checked.png";
         frmLoanPersonalInfo.lblEStatementVal.text = getLocalizedString("keyYes");
      }else if(gblPersonalInfo.emailStatementFlag == "N"){
         frmLoanPersonalInfo.imgCheck.src = "chkbox_uncheck.png";
         frmLoanPersonalInfo.lblEStatementVal.text = getLocalizedString("keyNo");
      } 
     //#else
      if(gblPersonalInfo.emailStatementFlag == "Y"){
         frmLoanPersonalInfo.lblEStatementVal.text = getLocalizedString("keyYes");
         frmLoanPersonalInfo.switchEstmt.selectedIndex = 0;
      }else if(gblPersonalInfo.emailStatementFlag == "N"){
         frmLoanPersonalInfo.lblEStatementVal.text = getLocalizedString("keyNo");
         frmLoanPersonalInfo.switchEstmt.selectedIndex = 1;
      }
      //#endif
  }else{
    frmLoanPersonalInfo.btnNext.skin = "btnGreyBGNoRound";
  frmLoanPersonalInfo.btnNext.focusSkin="btnGreyBGNoRound";
  frmLoanPersonalInfo.btnNext.onClick = null;
  frmLoanPersonalInfo.lblcircle.skin = "skinpiorangeLineLone";
  frmLoanPersonalInfo.flxcircle.skin="flexGreyBGLoan";
 
  }
  

  kony.print("gblPersonalInfo.ContactAdress : "+JSON.stringify(gblPersonalInfo.ContactAdress));

  kony.print("calling openPIwithData");
  //openPIwithData();
    handleSaveBtnPersonalInfo();
    frmLoanPersonalInfo.flxSourceOfIncomePopUp.setVisibility(false);
    frmLoanPersonalInfo.flxAddressPopUp.setVisibility(false);
  }catch(e){
    kony.print("Exception ocurred in preshowOfPersonalInfo : "+e);
  } 
}
//TamBol



function handleSaveBtnMobileNumberPopUp() {
  var saveInd;
  /* var textValues = ["txtMobileNumber","txtHomeNo","txtExtNo"];
  for(var i = 0; i < textValues.length; i++) {
	var val = frmLoanPersonalInfo[textValues[i]].text;
    if(val === "") {
      saveInd = false;
      break;
    } else {
      saveInd = true;
    }
  }*/

  var mobileNumber = frmLoanPersonalInfo.txtMobileNumber.text;
  var homeNo = frmLoanPersonalInfo.txtHomeNo.text;
  var extNo = frmLoanPersonalInfo.txtExtNo.text;
  if(isNotBlank(mobileNumber) && isNotBlank(homeNo) && isNotBlank(extNo)) {
    saveInd = true;
  } else {
    saveInd = false;
  }

  if(saveInd === true) {
    frmLoanPersonalInfo.btnSaveContactNo.skin = "btnBlueBGNoRound";
    frmLoanPersonalInfo.btnSaveContactNo.focusSkin = "btnBlueBGNoRound";
    frmLoanPersonalInfo.btnSaveContactNo.setEnabled(true);
  } else {
    frmLoanPersonalInfo.btnSaveContactNo.skin = "btnGreyBGNoRoundLoan";
    frmLoanPersonalInfo.btnSaveContactNo.focusSkin = "btnGreyBGNoRoundLoan";
    frmLoanPersonalInfo.btnSaveContactNo.setEnabled(false);
  }
}

function onDoneContactPopUp() {
  var arryList = ["txtMobileNumber","txtHomeNo","txtExtNo"];
  if(frmLoanPersonalInfo.txtMobileNumber.text === "")
    flexAddressEmptyCheck(arryList);
}

function enableContactNumberField() {
  var arryList = ["txtHomeNo", "txtExtNo"];
  flexAddressEmptyCheck(arryList);
}

function enableContactFloorFields() {
  var arryList = ["txtMobileNumber", "txtExtNo"];
  flexAddressEmptyCheck(arryList);
}

function onDoneContactMooNoPopUp() {
  var arryList = ["txtMobileNumber","txtHomeNo","txtExtNo"];
  if(frmLoanPersonalInfo.txtExtNo.text === "")
    flexAddressEmptyCheck(arryList);
}

function enableContactMooField() {
  var arryList = ["txtMobileNumber", "txtHomeNo"];
  flexAddressEmptyCheck(arryList);
}

function contactPopUpSaveClick(){
  gblMobileData = {};
  var mobileNumber = frmLoanPersonalInfo.txtMobileNumber.text;
  var homeNo = frmLoanPersonalInfo.txtHomeNo.text;
  var extNo = frmLoanPersonalInfo.txtExtNo.text;
  gblMobileData = {"mobileNumber" : mobileNumber,"homeNo" : homeNo, "extNo" : extNo};
  frmLoanPersonalInfo.lblMobileNo.text = gblMobileData.mobileNumber;
  frmLoanPersonalInfo.flxMobilePopUp.setVisibility(true);
}

function flxContactOnTouchEvent(obj) {
  var arryLables = obj.widgets();
  frmLoanPersonalInfo[arryLables[0].id].isVisible=true;
  frmLoanPersonalInfo[arryLables[1].id].isVisible=true;
  frmLoanPersonalInfo[arryLables[1].id].setFocus(true);
  frmLoanPersonalInfo[arryLables[1].id].text = frmLoanPersonalInfo[arryLables[1].id].text === "" ? "" : frmLoanPersonalInfo[arryLables[1].id].text;
  frmLoanPersonalInfo[arryLables[2].id].isVisible=false;
}

function onChangeContactFocusEvent(obj) {
  var arryList = ["txtMobileNumber", "txtHomeNo", "txtExtNo"];
  //onChangeFocusEvent(arryList);
  for(var i = 0; i < arryList.length; i++) {
    var val = frmLoanPersonalInfo[arryList[i]].text;
    if(val === "") {
      flexAddressEmptyCheck(arryList);
    }
  }
}

function handleSaveBtnPersonalInfo() {
  var confirmSkin = "";
  var txtArry = ["lblSalVal","lblSourceCountry","lblEmailWithData","lblEStatementVal","lblResidentStatusVal","lblMobileNoInfo","lblContactAddressInfo"];
  for(var i = 0; i < txtArry.length; i++){
     var val ="";
        if(frmLoanPersonalInfo[txtArry[i]].text!=null){
           val = frmLoanPersonalInfo[txtArry[i]].text.trim();
        }
    if(val !== undefined && val === "") {
      confirmSkin = "btnGreyBGNoRound";
      break;
    } else {
      if(frmLoanPersonalInfo.flxAddessWithData.isVisible){
        confirmSkin = "btnBlue28pxLoan2";
      }
    }
  }
  if(frmLoanPersonalInfo.lblResidentialBlue.isVisible){
    confirmSkin = "btnGreyBGNoRound";
  }
  //if(confirmSkin === "btnBlueBGNoRound150pxLoan" && Object.keys(gblPersonalInfo.ContactAdress).length > 0 && Object.keys(gblMobileData).length > 0 && Object.keys(gblResidentStatus).length > 0) { 
  if(confirmSkin === "btnBlue28pxLoan2") { 
    frmLoanPersonalInfo.flxcircle.skin="flxGreenCircleLoan";
    frmLoanPersonalInfo.lblcircle.skin = "skinpiGreenLineLone";
    frmLoanPersonalInfo.btnNext.skin = confirmSkin;
    frmLoanPersonalInfo.btnNext.focusSkin = "btnBlueBGNoRound150pxLoan";
    //nextJSONPreparation();
    frmLoanPersonalInfo.btnNext.onClick = onClickOfDoneBtnPersonal;
  } else {
      frmLoanPersonalInfo.btnNext.onClick = null;
    frmLoanPersonalInfo.btnNext.skin = "btnGreyBGNoRound";
    frmLoanPersonalInfo.btnNext.focusSkin="btnGreyBGNoRound";
    frmLoanPersonalInfo.lblcircle.skin = "skinpiorangeLineLone";
    frmLoanPersonalInfo.flxcircle.skin="slFbox";
  }
}

function nextJSONPreparation() {
  try {
    frmLoanApplicationForm.lblPersonalCircle.skin = "skinpiGreenLineLone";    
    frmLoanApplicationForm.flxPersonalLblCircle.skin="flxGreenCircleLoan";
    gblPersonalInfo = {"lblSalVal" : frmLoanPersonalInfo.lblSalVal.text,
                       "lblThaiNameVal" : frmLoanPersonalInfo.lblThaiNameVal.text,
                       "lblThaiSurNameVal" : frmLoanPersonalInfo.lblThaiSurNameVal.text,
                       "lblEngNameVal" : frmLoanPersonalInfo.lblEngNameVal.text,
                       "lblEngSurNameVal" : frmLoanPersonalInfo.lblEngSurNameVal.text,
                       "lblSourceCountry" : frmLoanPersonalInfo.lblSourceCountry.text,
                       "lblEmailVal" : frmLoanPersonalInfo.lblEmailVal.text,
                       "lblEStatementVal" : frmLoanPersonalInfo.lblEStatementVal.text,
                       "lblResidentStatusVal" : frmLoanPersonalInfo.lblResidentStatusVal.text,
                       "lblMobileNo" : frmLoanPersonalInfo.lblMobileNo.text
                      };
    kony.print("nextJSONPreparation : "+JSON.stringify(gblPersonalInfo));
  } catch(e) {
    alert("Exception in nextJSONPreparation : "+e);
  }
}

function callbackOfSaluationDropDowns(status, result){
  if (status == 400) {
    try{
      if (result.opstatus == "0") {
        //dismissLoadingScreen();
        kony.print("result value = "+ JSON.stringify(result));
        var employeeStatus = result["SALUTATION"];
          gblloanCollectionData =result["SALUTATION"]; 
        gblLoanSearchFlow = "SALUTATION";
        var selectedFlag=false;
        var selectedStatus="";
        if(frmLoanPersonalInfo.lblSalVal.isVisible === true) {
          selectedFlag=true;
          kony.print("getEmployeeStatus @@ already");
          selectedStatus = frmLoanPersonalInfo.lblSalVal.text;
        }
        setDropDownDatainSeg(employeeStatus,selectedFlag,selectedStatus,"SALUTATION");
      }else {
        dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        return false;
      }
    }catch(e){
    alert("Exception in callbackOfgetPersonalInfoFields, e : "+e);
  }
  }
}

function callbackOfSciCountryDropDowns(status, result){
  if (status == 400) {
    try{
      if (result.opstatus == "0") {
        //dismissLoadingScreen();
        kony.print("result value = "+ JSON.stringify(result));
        var employeeStatus = result["SCI_COUNTRY"];
          gblloanCollectionData =result["SCI_COUNTRY"]; 
         gblLoanSearchFlow = "SCI_COUNTRY";
        var selectedFlag=false;
        var selectedStatus="";
        if(frmLoanPersonalInfo.lblSourceCountry.isVisible === true) {
          selectedFlag=true;
          kony.print("getEmployeeStatus @@ already");
          selectedStatus = frmLoanPersonalInfo.lblSourceCountry.text;
        }
        setDropDownDatainSeg(employeeStatus,selectedFlag,selectedStatus,"SCI_COUNTRY");
      }else {
        dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        return false;
      }
    }catch(e){
    alert("Exception in callbackOfgetPersonalInfoFields, e : "+e);
  }
  }
}

function onClickPersonalDropDown(dropDownType){
  
  var dropDownTypeVal = "";
  frmLoanPersonalInfo.tbxSearch.text = "";
  if(null !== dropDownType && null !== dropDownType.id){
    dropDownTypeVal = dropDownType.id;
  }
  kony.print("dropDownTypeVal : "+dropDownTypeVal);
  var categoryCode = "";
   if("flxThaiSaluation" === dropDownTypeVal){
	    categoryCode = "SALUTATION";
  }else if("flxSorceOfIncome" === dropDownTypeVal){
    categoryCode = "SCI_COUNTRY";
  }
  if("" !== categoryCode){
  	getDropdwonByCategary(categoryCode);  
  }
  kony.print("categoryCode : "+categoryCode);
}

function onClickDropDownIcons(dropDownType){
  try{
  showLoadingScreen();
   // alert("dropDownType : "+dropDownType);
  var dropDownObj = [];
  var objArray = [];
  var locale = kony.i18n.getCurrentLocale();
  var dropDownTypeVal = "";
  var lblName = "";  
  
    gblloanCollectionData = [];
          gblLoanSearchFlow = "";  
       frmLoanPersonalInfo.flxSearch.setVisibility(false);
       frmLoanPersonalInfo.tbxSearch.text = "";
    
  if(null !== dropDownType && null !== dropDownType.id){
    dropDownTypeVal = dropDownType.id;
  }
  kony.print("gblPersonalInfo"+ JSON.stringify(gblPersonalInfo));
   frmLoanPersonalInfo.segSourceofIncome.removeAll();  
  if("flxThaiSaluation" === dropDownTypeVal){
    dropDownObj = gblPersonalInfo.SALUTATION;
    frmLoanPersonalInfo.lblSIHeading.text = kony.i18n.getLocalizedString("ThaiSalutation");
    lblName = "lblSalVal";
    frmLoanPersonalInfo.btnCloseSIPopUp.onClick = btnCloseSIPopUponClick;
    frmLoanPersonalInfo.segSourceofIncome.onRowClick = segThaiSalutionOnRowClick;
  }else if("flxResidentialStatus" === dropDownTypeVal){
    kony.print("RESIDENT_TYP"+ JSON.stringify(gblPersonalInfo.RESIDENT_TYP));
    dropDownObj = gblPersonalInfo.RESIDENT_TYP;
    frmLoanPersonalInfo.lblSIHeading.text = kony.i18n.getLocalizedString("Residential_Status_label");
    lblName = "lblResidentStatusVal";
    frmLoanPersonalInfo.btnCloseSIPopUp.onClick = btnCloseSIPopUponClickscroldown;
    frmLoanPersonalInfo.segSourceofIncome.onRowClick = segResindentialOnRowClick;
  }else if("flxSorceOfIncome" === dropDownTypeVal){
        dropDownObj = gblPersonalInfo.SCI_COUNTRY;
    frmLoanPersonalInfo.lblSIHeading.text = kony.i18n.getLocalizedString("SourceofIncome");
    lblName = "lblSourceCountry";
    frmLoanPersonalInfo.btnCloseSIPopUp.onClick = btnCloseSIPopUponClickscroldown;
    frmLoanPersonalInfo.segSourceofIncome.onRowClick = segSourceofIncomeonRowClick;
  }
    entryCode
      //alert("dropDownObj : "+JSON.stringify(dropDownObj));
      if(null !== dropDownObj && dropDownObj.length > 0){
        dropDownObj.sort(dynamicSortLoan("entryName2"));
		var entryCode = "";
        for(var j = 0; j < dropDownObj.length ; j++){
          var obj = {};
          entryCode = dropDownObj[j].entryCode !== undefined ? dropDownObj[j].entryCode : "";
          if("th_TH" == locale){
            obj = {
              "lblName": dropDownObj[j].entryName2,
              "lblSegRowLine":".",  "imgTick":"empty.png", "lblEntryCode" : entryCode,
              "flxSegLoanGeneric":{"skin":flexWhiteBG}
            };
          }else{
            obj = {
              "lblName": dropDownObj[j].entryName2,
              "lblSegRowLine":".",  "imgTick":"empty.png", "lblEntryCode" : entryCode,
              "flxSegLoanGeneric":{"skin":flexWhiteBG}
            };
          }
          objArray.push(obj);
        }
      }
    
    var segData = [];
 var selectedStatus="";
if (frmLoanPersonalInfo[lblName].isVisible === true) {
	kony.print("getEmployeeStatus @@ already");
	selectedStatus = frmLoanPersonalInfo[lblName].text;
}
	for (var i = 0; i < objArray.length; i++) {
		kony.print("getEmployeeStatus @@ in loop");
		if (selectedStatus == objArray[i].lblName) {
			kony.print("getEmployeeStatus @@ matched");
			var temprowData = {
				"lblName": selectedStatus,
				"lblSegRowLine": ".",
				"imgTick": "tick_icon.png",
				"flxSegLoanGeneric": {
					"skin": flexGreyBG
				}
			};
			segData.push(temprowData);
			kony.print("getEmployeeStatus @@ pushed selected");
		} else {
			segData.push(objArray[i]);
		}
	}

  frmLoanPersonalInfo.segSourceofIncome.setData(segData);
    
    frmLoanPersonalInfo.flxSourceOfIncomePopUp.setVisibility(true);
//     frmLoanPersonalInfo.flxMain.setVisibility(false);

    dismissLoadingScreen();

  }catch(e){
    alert("Exception in onClickDropDownIcons function, dropDownTypeVal : "+dropDownTypeVal+" , e : "+e);
    dismissLoadingScreen();
  }
}

/*
function imgArrowSourceOfCountryonTouchEnd (){
 try{
  frmLoanPersonalInfo.flxSourceOfIncomePopUp.setVisibility(true);
  frmLoanPersonalInfo.flxMain.setVisibility(false);
  frmLoanPersonalInfo.lblSIHeading.text = kony.i18n.getLocalizedString("SourceofIncome");
  frmLoanPersonalInfo.segSourceofIncome.removeAll();

  //frmLoanPersonalInfo.segSourceofIncome.widgetDataMap= {lblName:"lblName"}; 

 var employeeStatus = [{"lblName":"Field no 1","lblSegRowLine":".",  "imgTick":"empty.png", "flxSegLoanGeneric":{"skin":flexWhiteBG}},
                        {"lblName":"Field no 2","lblSegRowLine":".",  "imgTick":"empty.png", "flxSegLoanGeneric":{"skin":flexWhiteBG}},
                        {"lblName":"Field no 3","lblSegRowLine":".",  "imgTick":"empty.png", "flxSegLoanGeneric":{"skin":flexWhiteBG}}
                       ];
  var segData = [];
  if(frmLoanPersonalInfo.lblSourceCountry.isVisible === true) {
   kony.print("getEmployeeStatus @@ already");
   var selectedStatus = frmLoanPersonalInfo.lblSourceCountry.text;
   
   for(var i=0 ; i<employeeStatus.length ; i++){
     kony.print("getEmployeeStatus @@ in loop");
     if(selectedStatus == employeeStatus[i].lblName){
       kony.print("getEmployeeStatus @@ matched");
        var temprowData = {
	                        "lblName": selectedStatus,
          					"lblSegRowLine":".", 
	                        "imgTick": "tick_icon.png",
	                        "flxSegLoanGeneric": {
	                            "skin": flexGreyBG
	                        }
	                    };
       segData.push(temprowData);
       kony.print("getEmployeeStatus @@ pushed selected");
     }else{
       segData.push(employeeStatus[i]);
     }
   }
  }

  frmLoanPersonalInfo.segSourceofIncome.setData(segData);
  frmLoanPersonalInfo.segSourceofIncome.onRowClick = segSourceofIncomeonRowClick;
 }catch(e){
   kony.print("Exception : "+e);
 }
}

function onClickThaiSalutionsIcon (){
  frmLoanPersonalInfo.flxSourceOfIncomePopUp.setVisibility(true);
  frmLoanPersonalInfo.flxMain.setVisibility(false);
  frmLoanPersonalInfo.lblSIHeading.text = kony.i18n.getLocalizedString("ThaiSalutation");
  frmLoanPersonalInfo.segSourceofIncome.removeAll();

  //frmLoanPersonalInfo.segSourceofIncome.widgetDataMap= {lblName:"lblName"}; 

  var employeeStatus = [{"lblName":"Field no 1","lblSegRowLine":".",  "imgTick":"empty.png", "flxSegLoanGeneric":{"skin":flexWhiteBG}},
                        {"lblName":"Field no 2","lblSegRowLine":".",  "imgTick":"empty.png", "flxSegLoanGeneric":{"skin":flexWhiteBG}},
                        {"lblName":"Field no 3","lblSegRowLine":".",  "imgTick":"empty.png", "flxSegLoanGeneric":{"skin":flexWhiteBG}}
                       ];
  var segData = [];
  if(frmLoanPersonalInfo.lblSalVal.isVisible === true) {
   kony.print("getEmployeeStatus @@ already");
   var selectedStatus = frmLoanPersonalInfo.lblSalVal.text;
   
   for(var i=0 ; i<employeeStatus.length ; i++){
     kony.print("getEmployeeStatus @@ in loop");
     if(selectedStatus == employeeStatus[i].lblName){
       kony.print("getEmployeeStatus @@ matched");
        var temprowData = {
	                        "lblName": selectedStatus,
          					"lblSegRowLine":".", 
	                        "imgTick": "tick_icon.png",
	                        "flxSegLoanGeneric": {
	                            "skin": flexGreyBG
	                        }
	                    };
       segData.push(temprowData);
       kony.print("getEmployeeStatus @@ pushed selected");
     }else{
       segData.push(employeeStatus[i]);
     }
   }
  }
  frmLoanPersonalInfo.segSourceofIncome.setData(segData);
  frmLoanPersonalInfo.segSourceofIncome.onRowClick = segThaiSalutionOnRowClick;
}

function onClickTitleTypeIcon (){
  frmLoanPersonalInfo.flxSourceOfIncomePopUp.setVisibility(true);
  frmLoanPersonalInfo.flxMain.setVisibility(false);
  frmLoanPersonalInfo.lblSIHeading.text = kony.i18n.getLocalizedString("TitleType");
  frmLoanPersonalInfo.segSourceofIncome.removeAll();

  //frmLoanPersonalInfo.segSourceofIncome.widgetDataMap= {lblName:"lblName"}; 

  var employeeStatus = [{"lblName":"Field no 1","lblSegRowLine":".",  "imgTick":"empty.png", "flxSegLoanGeneric":{"skin":flexWhiteBG}},
                        {"lblName":"Field no 2","lblSegRowLine":".",  "imgTick":"empty.png", "flxSegLoanGeneric":{"skin":flexWhiteBG}},
                        {"lblName":"Field no 3","lblSegRowLine":".",  "imgTick":"empty.png", "flxSegLoanGeneric":{"skin":flexWhiteBG}}
                       ];
  var segData = [];
  if(frmLoanPersonalInfo.lblTitleTypeVal.isVisible === true) {
   kony.print("getEmployeeStatus @@ already");
   var selectedStatus = frmLoanPersonalInfo.
   .text;
   
   for(var i=0 ; i<employeeStatus.length ; i++){
     kony.print("getEmployeeStatus @@ in loop");
     if(selectedStatus == employeeStatus[i].lblName){
       kony.print("getEmployeeStatus @@ matched");
        var temprowData = {
	                        "lblName": selectedStatus,
          					"lblSegRowLine":".", 
	                        "imgTick": "tick_icon.png",
	                        "flxSegLoanGeneric": {
	                            "skin": flexGreyBG
	                        }
	                    };
       segData.push(temprowData);
       kony.print("getEmployeeStatus @@ pushed selected");
     }else{
       segData.push(employeeStatus[i]);
     }
   }
  }

  frmLoanPersonalInfo.segSourceofIncome.setData(segData);
  frmLoanPersonalInfo.segSourceofIncome.onRowClick = segTitleTypeOnRowClick;
}

function onClickResindentialIcon (){
  frmLoanPersonalInfo.flxSourceOfIncomePopUp.setVisibility(true);
  frmLoanPersonalInfo.flxMain.setVisibility(false);
  frmLoanPersonalInfo.lblSIHeading.text = kony.i18n.getLocalizedString("Residential_Status_label");
  frmLoanPersonalInfo.segSourceofIncome.removeAll();

  //frmLoanPersonalInfo.segSourceofIncome.widgetDataMap= {lblName:"lblName"}; 

  var employeeStatus = [{"lblName":"Field no 1","lblSegRowLine":".",  "imgTick":"empty.png", "flxSegLoanGeneric":{"skin":flexWhiteBG}},
                        {"lblName":"Field no 2","lblSegRowLine":".",  "imgTick":"empty.png", "flxSegLoanGeneric":{"skin":flexWhiteBG}},
                        {"lblName":"Field no 3","lblSegRowLine":".",  "imgTick":"empty.png", "flxSegLoanGeneric":{"skin":flexWhiteBG}}
                       ];
  var segData = [];
  if(frmLoanPersonalInfo.lblResidentStatusVal.isVisible === true) {
   kony.print("getEmployeeStatus @@ already");
   var selectedStatus = frmLoanPersonalInfo.lblResidentStatusVal.text;
   
   for(var i=0 ; i<employeeStatus.length ; i++){
     kony.print("getEmployeeStatus @@ in loop");
     if(selectedStatus == employeeStatus[i].lblName){
       kony.print("getEmployeeStatus @@ matched");
        var temprowData = {
	                        "lblName": selectedStatus,
          					"lblSegRowLine":".", 
	                        "imgTick": "tick_icon.png",
	                        "flxSegLoanGeneric": {
	                            "skin": flexGreyBG
	                        }
	                    };
       segData.push(temprowData);
       kony.print("getEmployeeStatus @@ pushed selected");
     }else{
       segData.push(employeeStatus[i]);
     }
   }
  }

  frmLoanPersonalInfo.segSourceofIncome.setData(segData);
  frmLoanPersonalInfo.segSourceofIncome.onRowClick = segResindentialOnRowClick;
}
*/

function btnCloseSIPopUponClick (){

  frmLoanPersonalInfo.flxMain.setVisibility(true);  
  frmLoanPersonalInfo.flxSourceOfIncomePopUp.setVisibility(false);
  frmLoanPersonalInfo.flxsaluation.setFocus(true);
  
}

function btnCloseSIPopUponClickscroldown (){

  frmLoanPersonalInfo.flxMain.setVisibility(true);  
  frmLoanPersonalInfo.flxSourceOfIncomePopUp.setVisibility(false);
  frmLoanPersonalInfo.flxMain.scrollToEnd(true);
  
}

  
function segSourceofIncomeonRowClick (){
    var lbltext = frmLoanPersonalInfo.segSourceofIncome.selectedRowItems[0].lblName;
  	
  frmLoanPersonalInfo.flxMain.setVisibility(true); 
  
  frmLoanPersonalInfo.flxSourceOfIncomePopUp.setVisibility(false);
  
    //frmLoanPersonalInfo.lblSourceOfIncomeMain.isVisible = false;
    //frmLoanPersonalInfo.lblIncomeCountry.isVisible = true;
   // frmLoanPersonalInfo.lblSourceCountry.isVisible = true;
  	frmLoanPersonalInfo.lblSrcIncomCountryCode.text = frmLoanPersonalInfo.segSourceofIncome.selectedRowItems[0].lblEntryCode;
    frmLoanPersonalInfo.lblSourceCountry.text = lbltext;
    frmLoanPersonalInfo.flxMain.scrollToEnd(true);
    handleSaveBtnPersonalInfo();
    frmLoanPersonalInfo.flxsaluation.setFocus(true);
  }

function segThaiSalutionOnRowClick (){
    var lbltext = frmLoanPersonalInfo.segSourceofIncome.selectedRowItems[0].lblName;
    frmLoanPersonalInfo.flxMain.setVisibility(true);  
  frmLoanPersonalInfo.flxSourceOfIncomePopUp.setVisibility(false);
    frmLoanPersonalInfo.lblSalVal.isVisible = true;
    frmLoanPersonalInfo.lblSalVal.text = lbltext;
  frmLoanPersonalInfo.lblSalutationEntryCode.text = frmLoanPersonalInfo.segSourceofIncome.selectedRowItems[0].lblEntryCode;
  handleSaveBtnPersonalInfo();
  frmLoanPersonalInfo.flxsaluation.setFocus(true);
  }

// function segTitleTypeOnRowClick (){
//     var lbltext = frmLoanPersonalInfo.segSourceofIncome.selectedRowItems[0].lblName;

//     frmLoanPersonalInfo.flxMain.setVisibility(true);  
//   frmLoanPersonalInfo.flxSourceOfIncomePopUp.setVisibility(false);
//     frmLoanPersonalInfo.lblTitleTypeVal.isVisible = true;
//     frmLoanPersonalInfo.lblTitleTypeVal.text = lbltext;
  
//   frmLoanPersonalInfo.lblTitleTypeEntryCode.text = frmLoanPersonalInfo.segSourceofIncome.selectedRowItems[0].lblEntryCode;
//   frmLoanPersonalInfo.lblSalVal.text = "";
//   frmLoanPersonalInfo.lblSalutationEntryCode.text = "";
//   frmLoanPersonalInfo.flxThaiSaluation.setEnabled(true);
//   handleSaveBtnPersonalInfo();
//   frmLoanPersonalInfo.flxsaluation.setFocus(true);
//   }

function segResindentialOnRowClick (){
    var lbltext = frmLoanPersonalInfo.segSourceofIncome.selectedRowItems[0].lblName;
    frmLoanPersonalInfo.flxMain.setVisibility(true);  
  frmLoanPersonalInfo.flxSourceOfIncomePopUp.setVisibility(false);
  	frmLoanPersonalInfo.lblResidentialBlue.setVisibility(false);
    frmLoanPersonalInfo.lblResidentailsBlack.isVisible = true;
   frmLoanPersonalInfo.lblResidentStatusVal.isVisible = true;
    frmLoanPersonalInfo.lblResidentStatusVal.text = lbltext;
  frmLoanPersonalInfo.lblResidenceCode.text = frmLoanPersonalInfo.segSourceofIncome.selectedRowItems[0].lblEntryCode;
  frmLoanPersonalInfo.flxMain.scrollToEnd(true);
   handleSaveBtnPersonalInfo();
  frmLoanPersonalInfo.flxResidentialStatus.setFocus(true);
  }

function handleCheckUnCheck(){
  try{
    if(null !== frmLoanPersonalInfo.imgCheck){
      if(frmLoanPersonalInfo.imgCheck.src == "chkbox_checked.png"){
        frmLoanPersonalInfo.imgCheck.src = "chkbox_uncheck.png";
        frmLoanPersonalInfo.lblEStatementVal.text = getLocalizedString("keyNo"); //"NO";

      }else{
        frmLoanPersonalInfo.imgCheck.src = "chkbox_checked.png";
        frmLoanPersonalInfo.lblEStatementVal.text = getLocalizedString("keyYes"); //"YES";
      }  
    }

  }catch(e){
	kony.print("Exception in handleCheckUnCheck : "+e);
    //alert("Exception in handleCheckUnCheck : "+e);
  }

}

function savePIClick(){
  
}


function ShowLoanApplicationForm() {
  gblPersonalInfo.savePI = true;
  //nextJSONPreparation();
  frmLoanApplicationForm.show();
}



function searchDistrictOnChange(){
  var districtArry = ["HUAI KHAN LAEN >> WISET CHAI CHAN >> ANG THONG >> 14110","HUA TAPHAN >> WISET CHAI CHAN >> ANG THONG >> 14110",
                      "KHOK KONG >> CHANUMAN >> AMNAT CHAROEN >> 37210","KHOK SAN >> CHANUMAN >> AMNAT CHAROEN >> 37210",
                      "CHAMLONG >> SAWAENG HA >> ANG THONG >> 14150","CHAN LAN >> PHANA >> AMNAT CHAROEN >> 37180",
                     "CHANUMAN >> CHANUMAN >> AMNAT CHAROEN >> 37210"];
  var searchString = frmLoanPersonalInfo.tbxSubDistrict.text;
  kony.print("searchString ## "+searchString);
  var districtNameLowerCase = "";
  if(isNotBlank(searchString) && searchString.length >= 3){
    searchString = searchString.toLowerCase();
    var temp = [];
    for(var i=0; i<districtArry.length; i ++){

      districtNameLowerCase = districtArry[i].toLowerCase();

      if(districtNameLowerCase.indexOf(searchString) >= 0){
        	temp.push({"lblResult":districtNameLowerCase});

      }

    }
    if(temp.length > 0){
      frmLoanPersonalInfo.segResult.data = temp;
      frmLoanPersonalInfo.flxSearchResults.setVisibility(true);
    }else{
      frmLoanPersonalInfo.segResult.removeAll();
      frmLoanPersonalInfo.flxSearchResults.setVisibility(false);
    }
  }
}

function handleAddressSegRowClick(){
  var seletedItem = frmLoanPersonalInfo.segResult.selectedRowItems[0];
  kony.print("seletedItem : "+seletedItem);
  kony.print("search string : "+seletedItem.lblResult);
  
  var searchStr = seletedItem.lblResult;
  var searchSplit = searchStr.split(">>");
  kony.print("searchSplit.length : "+searchSplit.length);
  if(searchSplit.length == 4){
    frmLoanPersonalInfo.tbxSubDistrict.text = searchSplit[0].trim();
    frmLoanPersonalInfo.tbxDistrict.text = searchSplit[1].trim();
    frmLoanPersonalInfo.tbxProvince.text = searchSplit[2].trim();
    frmLoanPersonalInfo.tbxZipCode.text = searchSplit[3].trim();
  }else{
    
  }
  frmLoanPersonalInfo.segResult.removeAll();
  frmLoanPersonalInfo.flxSearchResults.setVisibility(false);
  handleSaveBtnContactAddPopUp();
}

