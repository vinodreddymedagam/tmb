function loginAuthNew(uniqueID) {
  showLoadingScreen();
  
  //Setting last refresh time for AS page
  gblLastRefresh = Math.round(new Date().getTime()/1000);
  
  //Account Summary landing page get destroyed just ones in whole app flow.
  TMBUtil.DestroyForm(frmAccountSummaryLanding);
  kony.print("In New Login Optimisation Code loginAuthNew%%%%%%%%")
  inputParam = {};
  inputParam["deviceId"] = uniqueID;
  kony.print("FPRINT: uniqueID is deviceId= " + uniqueID);
  var pushRegID = kony.store.getItem("kpnssubcriptiontoken");
  if (pushRegID != null && pushRegID != "" && pushRegID != undefined && pushRegID != "undefined") {
    inputParam["subscriptionToken"] = pushRegID;
  }
  //inputParam["osDeviceId"] = kony.os.deviceInfo().deviceid;
  // modifying the code for kony device id.
  inputParam["osDeviceId"] = getDeviceID();
  //kony.print("FPRINT: getDeviceID= "+getDeviceID);
  channelId = "02";
  if (GLOBAL_MB_CHANNEL != null && GLOBAL_MB_CHANNEL != "") {
    channelId = GLOBAL_MB_CHANNEL;
  }
  inputParam["loginChannel"] = channelId;
  inputParam["nodeNum"] = "1";
  kony.print("FPRINT: gFromConnectAccount=" + gFromConnectAccount);
  if (gFromConnectAccount == true) {
    inputParam["password"] = glbAccessPin
    kony.print("FPRINT: glbAccessPin=" + glbAccessPin);
  } else {
    inputParam["password"] = gblNum; //frmAfterLogoutMB.tbxAccessPIN.text;
    kony.print("FPRINT: gblNum=" + gblNum);
    gblNum = "";
    //frmAfterLogoutMB.tbxAccessPIN.text = "";
  }
  inputParam["activityTypeId"] = "Login";
  gblLoginType = "Login";

  if (gblSuccess == true || gblSuccess == "true") {
    inputParam["loginTypeTouch"] = "true";
  } else {
    inputParam["loginTypeTouch"] = "false";
  }
  inputParam["deviceOS"] = getDeviceOS();
  var RTPAnnounceDeviceFlag = kony.store.getItem("RTPAnnounceFlag")
  inputParam["RTPAnnounceDeviceFlag"] = RTPAnnounceDeviceFlag;
  var  registerWithAnyId = kony.store.getItem("registerWithAnyId");
  inputParam["registerWithAnyId"] = registerWithAnyId;
  //Hashing deviceId  
  //var secKeyVal = decrypt(glbInitVector);
  //alert("secKeyVal--->"+secKeyVal);

  //Account Summary cache related flags 
  isCacheFlow = false;
  pinFlow = true;
  isLoginFlow = true;
  kony.print("gblChangeLanguage: "+gblChangeLanguage);
  if(gblChangeLanguage == true){
    var locale = kony.i18n.getCurrentLocale();
    kony.print("locale in LoginCompositeSingleService @@: "+locale);
    if (locale == "en_US") {
      inputParam["languageCd"] = "EN";
    } else {
      inputParam["languageCd"] = "TH";
    }
  }
  //kony.print("inputParam in LoginCompositeSingleService @@: "+inputParam);
  invokeServiceSecureAsync("LoginCompositeSingleService", inputParam, callBackMBLoginAuthNew)
}
/*
Optimisation of the Login Flow. Below function called on done of entering 6 password fields
*/
function loginTouchIdMobileBankingnew(initVecValue, touchCode, serverTime) {
  kony.print("In New Login Optimisation Code loginTouchIdMobileBankingnew%%%%%%%%")
  //Account Summary landing page get destroyed just ones in whole app flow.
  TMBUtil.DestroyForm(frmAccountSummaryLanding);
  try {
    gblMBLoggingIn = true;
    
    //Setting last refresh time for AS page
    gblLastRefresh = Math.round(new Date().getTime()/1000);

    var inputParam = {};
    kony.print("KONYTOUCHID: Inside loginTouchIdMobileBanking >>>");
    var secKeyVal = decrypt(initVecValue); //glbInitVector
    secKeyVal = secKeyVal.substring(secKeyVal.length - 10, secKeyVal.length);

    //var inputStr = secKeyVal.trim() + GBL_UNIQ_ID + serverTime.trim() + touchCode.trim();
    //Need unencrypted device to fix MIB-7138 - R76_MB_Kony7.3Migration_Login - Cannot login with TouchID
    var inputStr = secKeyVal.trim() + gblDeviceIDUnEncrypt + serverTime.trim() + touchCode.trim();
    //alert("secKeyVal-->"+secKeyVal.trim()+" GBL_UNIQ_ID--->"+GBL_UNIQ_ID+" serverTime--->"+serverTime.trim()+" touchCode--->"+touchCode.trim())
    var deviceIdHash = kony.crypto.createHash("sha256", inputStr);
    //alert("deviceIdHash---->"+deviceIdHash);
    //*******old inputParam******
    channelId = "02";
    var pushRegID = kony.store.getItem("kpnssubcriptiontoken");
    if (pushRegID != null && pushRegID != "" && pushRegID != undefined && pushRegID != "undefined") {
      inputParam["subscriptionToken"] = pushRegID;
    }
    if (GLOBAL_MB_CHANNEL != null && GLOBAL_MB_CHANNEL != "") {
      channelId = GLOBAL_MB_CHANNEL;
    }
    inputParam["loginChannel"] = channelId;
    inputParam["nodeNum"] = "1"
    if (gFromConnectAccount == true) {
      inputParam["password"] = glbAccessPin
    } else {
      inputParam["password"] = gblNum; //frmAfterLogoutMB.tbxAccessPIN.text;
      gblNum = "";
      //frmAfterLogoutMB.tbxAccessPIN.text = "";
    }
    inputParam["activityTypeId"] = "Login";
    kony.print("KONYTOUCHID: loginTouchIdMobileBanking gblSuccess >>>" + gblSuccess);
    if (gblSuccess == true || gblSuccess == "true") {
      inputParam["loginTypeTouch"] = "true";
    } else {
      inputParam["loginTypeTouch"] = "false";
    }
    //*******old inputParam******
    inputParam["deviceId2"] = deviceIdHash;
    inputParam["osDeviceId"] = getDeviceID();
    inputParam["deviceId"] = getDeviceID();
    inputParam["deviceOS"] = getDeviceOS();
    
     kony.print("gblChangeLanguage: "+gblChangeLanguage);
  if(gblChangeLanguage == true){
    var locale = kony.i18n.getCurrentLocale();
    kony.print("locale in LoginCompositeSingleService @@: "+locale);
    if (locale == "en_US") {
      inputParam["languageCd"] = "EN";
    } else {
      inputParam["languageCd"] = "TH";
    }
  }


    var time = 1000;

    //AS cache related flags
    isCacheFlow = true;
    allowOtherOpr = false;
    pinFlow = false;
    isLoginFlow = true;

    var  registerWithAnyId = kony.store.getItem("registerWithAnyId");
    inputParam["registerWithAnyId"] = registerWithAnyId;
    var resulttable = kony.store.getItem("cachedResulttableASLoginFlow");
    var RTPAnnounceDeviceFlag = kony.store.getItem("RTPAnnounceFlag")
    inputParam["RTPAnnounceDeviceFlag"] = RTPAnnounceDeviceFlag;
    if((gblRTPBillPayPush == true || gblTransferPush  == true) || (null !== resulttable && (resulttable["registerWithAnyId"] == "N" || (resulttable["userPromptPayRegistered"] == "Y" && !(RTPAnnounceDeviceFlag == "Y")))))   
      gblAnnouncementFlow = true;
    
    //Revert cache flow
    gblAnnouncementFlow = true;

    if((null !== resulttable && null !== resulttable["accountSummary"] && null !== resulttable["accountSummary"][0]["custAcctRec"]) && gblAnnouncementFlow == false && !isNotBlank(gbl3dTouchAction)){
      kony.print("load acctsummary from cache");
      callBackMBLoginAuthNew(400, resulttable);

      //Back to non cache login flow  
      isCacheFlow = false;

      invokeServiceSecureAsync("LoginCompositeSingleService", inputParam, callBackMBLoginAuthNew);
    }else{

      //Failback scenario 
      pinFlow = true;
      isCacheFlow = false;
      allowOtherOpr = true;

      invokeServiceSecureAsync("LoginCompositeSingleService", inputParam, callBackMBLoginAuthNew);
    }



  } catch (e) {
    kony.print("Error in loginTouchIdMobileBankingnew "+e);
  }

}
/*
Calling this new function
*/
function callBackMBLoginAuthNew(status, resulttable) {
  kony.print("In New Login Optimisation Code callBackMBLoginAuthNew%%%%%%%%")
  kony.print("FPRINT: gblStartClickFromActivationMB=" + gblStartClickFromActivationMB);
  kony.print("FPRINT: status=" + status);
  kony.print("FPRINT: resulttable[opstatus]=" + resulttable["opstatus"]);
  kony.print("FPRINT: resulttable[deviceStatus]=" + resulttable["deviceStatus"]);
  kony.print("FPRINT: resulttable[errMsg]=" + resulttable["errMsg"]);
  kony.print("FPRINT: resulttable[errCode]=" + resulttable["errCode"]);
  gblnewLoginCompositeresult=resulttable;
  gblFromLogout = false; 
  if (status == 400) {
    
    
    if(isNotBlank(resulttable["errCode"]) ){
     
        if(resulttable["errCode"] == "XPErr0001" ){
          
				kony.print("FPRINT 1 SETTING isUserStatusActive TO FALSE");
                kony.print("FPRINT 1 setting isUserStatusActive to false");
             	kony.store.setItem("isUserStatusActive", false);
				isUserStatusActive=false;
				onClickChangeTouchIcon();	
				popupAccesesPinLocked.lblPopupConfText.text = kony.i18n.getLocalizedString("acclockmsg");
				popupAccesesPinLocked.btnPopupConfCancel.text= kony.i18n.getLocalizedString("keyCancelButton");
				popupAccesesPinLocked.btnCallTMB.text= kony.i18n.getLocalizedString("keyCall");
				popupAccesesPinLocked.show();
				//showAlert(kony.i18n.getLocalizedString("accLock"), kony.i18n.getLocalizedString("info"));
                resetValues();
				kony.application.dismissLoadingScreen();
				return false;
          
        }
       
     }
    
    
    
    loadFunctionalModulesAsync("cardMgmtModule");
    //loadFunctionalModulesAsync("postLoginCardLimitModules");
     
    
    gblStartClickFromActivationMB = false;
    //if(!gblStartClickFromActivationMB){
    /*	gblStartClickFromActivationMB=false;
			kpns_log("callBackMBLoginAuth: call case gblStartClickFromActivationMB");
			registerAppWithPushNotificationsInfrastructure();
		} else {
          */
    frmMenu.destroy();
    var ksid = kony.store.getItem("ksid");
    kony.print("KSID Val is" + ksid);
    var ksidUpdated = kony.store.getItem("ksidUpdated");
    //var is_updated_kpns = kony.store.getItem("is_updated_kpns");
    // subscription token or code
    //Date Check
    var currentDate=kony.os.date("dd/mm/yyyy");
    var savedKsidDate= kony.store.getItem("ksidsavedDate");
    var pushRegID = kony.store.getItem("kpnssubcriptiontoken");
    var kpns_refreshtime = "";
    kony.print("Value of KPNS Refresh is"+KPNS_REFRESH);
    if(isNotBlank(KPNS_REFRESH)){
      kpns_refreshtime = KPNS_REFRESH;
    }else{
      kpns_refreshtime = "15";
    }      	
    if(isNotBlank(savedKsidDate)){
      var dateDiff= kony.os.compareDates(currentDate, savedKsidDate, "dd/mm/yyyy")
      if(dateDiff>=kpns_refreshtime){
        kony.print("Call KPNS after certain refresh interval");
        registerAppWithPushNotificationsInfrastructure();
      }
    }else{
      kony.print("KPNS Called First Time");
      if (!isNotBlank(ksidUpdated) || !isNotBlank(pushRegID)) {
        kony.print("callBackMBLoginAuth: call case ksid is null or pushRegID is null");
        registerAppWithPushNotificationsInfrastructure();
      }  
    }       

    kony.print("KSID updated Val is" + kony.store.getItem("ksid"));
    //}
    if (resulttable["opstatus"] == 1234 || resulttable["opstatus"] == "1234") {
      kony.application.dismissLoadingScreen();
      checkUpgrades(resulttable["updatestatus"], resulttable["updateurl"]);
      resetValues();
      return false;

    } else if (resulttable["opstatus"] == 0) {

      //Account Summary cache balance default value
      if(isCacheFlow == true && null != resulttable["accountSummary"][0]["custAcctRec"]){ 
        for(var i = 0; i < resulttable["accountSummary"][0]["custAcctRec"].length; i++){
          resulttable["accountSummary"][0]["custAcctRec"][i].availableBalDisplay = "....";
          if (resulttable["mfTotalAmount"] != null)
            resulttable["mfTotalAmount"] = "....";
          if ( resulttable["totalSumInsured"] != null)
             resulttable["totalSumInsured"] = "....";  
        }

      }  else{
        if(resulttable["accountSummary"] != null || resulttable["accountSummary"] != undefined)
          kony.store.setItem("cachedResulttableASLoginFlow",resulttable); 
      }
     
      /**
            	Device Store of Surrogate ID 
            **/

      if (null == tmbRetriveFromDevice("trusteerIdFromClient")) {
        if (null != resulttable["trusteerIdFromClient"] && undefined != resulttable["trusteerIdFromClient"])
          tmbSaveOnDevice("trusteerIdFromClient", resulttable["trusteerIdFromClient"])
          }


      gblShowAnyIDRegistration = resulttable["setyourID"];
      gblCustomerIDType = resulttable["customerIDType"];
      gblCustomerIDValue = resulttable["customerIDValue"];
      gbllastMbLoginSucessDateEng = resulttable["lastMbLoginSucessDateEng"];
      gbllastMbLoginSucessDateThai = resulttable["lastMbLoginSucessDateThai"];
      gblEykcFlag = resulttable["EKYCFlag"];
      gblCurrentIAL = resulttable["IALLevel"];
      gblPartyStatusCode = resulttable["PartyStatusCode"];

      if (resulttable["deviceStatus"] == "0") {
        showAlert(kony.i18n.getLocalizedString("ECKonyDeviceErr00002"), kony.i18n.getLocalizedString("info"));
        resetValues();
        kony.application.dismissLoadingScreen();
        return false;
      } else if (resulttable["deviceStatus"] == "2") {
        showAlert("Device is blocked", kony.i18n.getLocalizedString("info"));
        resetValues();
        kony.application.dismissLoadingScreen();
        return false;
      } else if (resulttable["deviceStatus"] == "3") {
        showAlert("Device is cancelled", kony.i18n.getLocalizedString("info"));
        resetValues();
        kony.application.dismissLoadingScreen();
        return false;
      } else if (resulttable["errMsg"] == "Password is verified") {
        var time = resulttable["time"];
        GBL_TIMEFOR_LOGIN = resulttable["time"]
        registerForTimeOut();
        gblTouchStatus = resulttable["usesTouchId"];
        //gblActivationFlow = true;
        kony.print("FPRINT: Password is verified ");
        if (gblTouchStatus == undefined || gblTouchStatus == "null" || gblTouchStatus == null || gblTouchStatus == "" || gblTouchStatus == "N") {
          gblTouchStatus = "N";
          kony.store.setItem("usesTouchId", "N");
        } else if (gblTouchStatus == "Y") {
          kony.store.setItem("usesTouchId", "Y");
        }
        isUserStatusActive = true;
        kony.store.setItem("isUserStatusActive",isUserStatusActive);
        kony.print("FPRINT: gblTouchStatus=" + gblTouchStatus);
        setPostLoginGlobalVars(resulttable);
        loginPProcessCallBackNew(resulttable);

      } else if (resulttable["errMsg"] == "Password Locked" || resulttable["errCode"] == "E10105") {
        resetValues();
        onClickChangeTouchIcon();
        kony.print("FPRINT SETTING isUserStatusActive TO FALSE");
        isUserStatusActive = false;
        kony.store.setItem("isUserStatusActive",isUserStatusActive);
        popupAccesesPinLocked.lblPopupConfText.text = kony.i18n.getLocalizedString("acclockmsg");
        popupAccesesPinLocked.btnPopupConfCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
        popupAccesesPinLocked.btnCallTMB.text = kony.i18n.getLocalizedString("keyCall");
        popupAccesesPinLocked.show();
        //alert(" " + kony.i18n.getLocalizedString("keyAccessPwdLocked"));
        kony.application.dismissLoadingScreen();
        kony.store.removeItem("cachedResulttableASLoginFlow");
        return false;
      } else if (resulttable["errMsg"] == "User is already login to application") {
        resetValues();
        kony.application.dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECUserLoggedIn"), kony.i18n.getLocalizedString("info"));
        frmMBPreLoginAccessesPin.show(); //As per existing behaviour from R67 app. QA expectation
        kony.store.removeItem("cachedResulttableASLoginFlow");
        return false;
      } else {
        resetValues();
        //showAlertRcMB(kony.i18n.getLocalizedString("keyWrongPwd"),kony.i18n.getLocalizedString("info"), "info");
        kony.application.dismissLoadingScreen();
        return false;
      }
    } else if (resulttable["errMsg"] == "User is already login to application") {
      resetValues();
      kony.application.dismissLoadingScreen();
      showAlert(kony.i18n.getLocalizedString("ECUserLoggedIn"), kony.i18n.getLocalizedString("info"));
      frmMBPreLoginAccessesPin.show(); //As per existing behaviour from R67 app. QA expectation
      kony.store.removeItem("cachedResulttableASLoginFlow");
      return false;
    } else {

      var errorMessage = resulttable["errMsg"];
      var middlewareErrorMessage = resulttable["errmsg"];

      kony.application.dismissLoadingScreen();

      if ((resulttable["opstatus"] == -1 || resulttable["opstatus"] == "-1") && (errorMessage == "deviceIdEnhancementUnlock" || middlewareErrorMessage == "deviceIdEnhancementUnlock")) {

        showAlertDeviceIdTouchMessage(kony.i18n.getLocalizedString("deviceIdEnhancementUnlock"), kony.i18n.getLocalizedString("deviceIDUnlock"));

      } else {

        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));

      }

      resetValues();
      return false;

    }
  } else {
    resetValues();

  }
}

/*
New LoginProcessExecute service
*/
function loginPProcessCallBackNew(resulttable) {
  kony.print("In New Login Optimisation Code loginPProcessCallBackNew%%%%%%%%")
  //crmprofileinq result set
  displayAnnoucementtoUser = false;
  gblReferCD = resulttable["referCD"];
  /*	if(resulttable["MBFlag"] == "Y" && resulttable["IBFlag"] == "Y" && resulttable["ATMFlag"] == "Y"){
            gblUVregisterMB = "Y";
          }else{
            gblUVregisterMB = "N";
          }
          */
  if (resulttable["opstatuscrm"] == 0 && resulttable["opstatusPartyInq"] == 0) {
    gblFBCode = resulttable["facebookId"];

    //MIB-1207
    if (resulttable["setyourID"] != undefined && resulttable["setyourID"] != null) {
      gblShowAnyIDRegistration = resulttable["setyourID"]; // value taken from tmb.properties file
      //gblShowAnyIDRegistration =false;
    }
    if (resulttable["anyIdAnnouncementCount"] != undefined && resulttable["anyIdAnnouncementCount"] != null) {
      gblAnyIdAnnouceCount = parseInt(resulttable["anyIdAnnouncementCount"]); // value taken from tmb.properties file
    }
    // retriving the count from store
    //if(isNotBlank(kony.store.getItem("AnyIdAnnoceShowedCount"))){
    //				gblAnyIdAnnoceShowedCount=kony.store.getItem("AnyIdAnnoceShowedCount");
    //				gblAnyIdAnnoceShowedCount=parseInt(gblAnyIdAnnoceShowedCount);
    //			}
    //			else
    //			{
    //				gblAnyIdAnnoceShowedCount=gblAnyIdCount;
    //			}
    if (resulttable["registerWithAnyId"] != undefined && resulttable["registerWithAnyId"] != null) {
      gblRegisterWithAnyId = resulttable["registerWithAnyId"];
      kony.store.setItem("registerWithAnyId",gblRegisterWithAnyId);
      // posible values N or Y
      // user should not register with any id, and announcement showed count should be less than max count.
      //if(gblRegisterWithAnyId == "N" && (gblAnyIdAnnoceShowedCount < gblAnyIdAnnouceCount)){
      if (gblRegisterWithAnyId == "N") {
        //TODO show the frmRegAnyIdAnnouncement form and handle the navigations
        displayAnnoucementtoUser = true;
      } else {
        displayAnnoucementtoUser = false;
      }
    }
    gblMutualFundService = resulttable["mutualfundMaintanace"];
    SWITCH_CAL_TRANSFER_FEE = resulttable["SWITCH_CAL_TRANSFER_FEE"];
    if (resulttable["userPromptPayRegistered"] != undefined && resulttable["userPromptPayRegistered"] != null) {
      var registerPromptPay = resulttable["userPromptPayRegistered"];
      if (registerPromptPay == "Y") {
        isRTPCustomer = true;

        var RTPAnnounceDeviceFlag = kony.store.getItem("RTPAnnounceFlag")
        if (RTPAnnounceDeviceFlag == "Y") { //already shown to user
          displayRTPAnnoucement = false;
        } else {
          displayRTPAnnoucement = true;
        }
      } else {
        isRTPCustomer = false;
        displayRTPAnnoucement = false;
      }
    } else {
      isRTPCustomer = false;
      displayRTPAnnoucement = false;
    }

    //below code is added to implement MIB-945
    gblIsNewOffersExists = false;
    if (undefined != resulttable["unreadMyOffers"] && null != resulttable["unreadMyOffers"]) {
      //unreadMyOffers is Y means user has new offers
      //alert("unreadMyOffers : "+resulttable["unreadMyOffers"]);
      if ("Y" == resulttable["unreadMyOffers"]) {
        gblIsNewOffersExists = true;
      }
    }

    if (resulttable["errCode"] == "XPErr0001") {

      //showFPFlex(false);
      kony.print("FPRINT SETTING isUserStatusActive TO FALSE");
      isUserStatusActive = false;
      onClickChangeTouchIcon();
      popupAccesesPinLocked.lblPopupConfText.text = kony.i18n.getLocalizedString("acclockmsg");
      popupAccesesPinLocked.btnPopupConfCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
      popupAccesesPinLocked.btnCallTMB.text = kony.i18n.getLocalizedString("keyCall");
      popupAccesesPinLocked.show();
      //showAlert(kony.i18n.getLocalizedString("accLock"), kony.i18n.getLocalizedString("info"));
      resetValues();
      kony.application.dismissLoadingScreen();
      return false;
    } else if (resulttable["errCode"] == "XPErr0002") {
      onClickChangeTouchIcon();
      kony.print("FPRINT SETTING isUserStatusActive TO FALSE");
      isUserStatusActive = false;
      popupAccesesPinLocked.lblPopupConfText.text = kony.i18n.getLocalizedString("acclockmsg");
      popupAccesesPinLocked.btnPopupConfCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
      popupAccesesPinLocked.btnCallTMB.text = kony.i18n.getLocalizedString("keyCall");
      popupAccesesPinLocked.show();
      //showAlert(kony.i18n.getLocalizedString("accLock"), kony.i18n.getLocalizedString("info"));
      resetValues();
      kony.application.dismissLoadingScreen();
      return false;
    } else {
      gblCustomerStatus = resulttable["ebCustomerStatusId"]
      gblIBFlowStatus = resulttable["ibUserStatusIdTr"]
      gblMBFlowStatus = resulttable["mbFlowStatusIdRs"]
      gblUserLockStatusMB = resulttable["mbFlowStatusIdRs"];
      //updating the user locked status to globalvariable "gblUserLockStatusMB" 		

      if (gblSuccess == true && (gblUserLockStatusMB == "05" || gblUserLockStatusMB == "06")) {
        onClickChangeTouchIcon();
        kony.print("FPRINT SETTING isUserStatusActive TO FALSE");
        isUserStatusActive = false;
        popupAccesesPinLocked.lblPopupConfText.text = kony.i18n.getLocalizedString("acclockmsg");
        popupAccesesPinLocked.btnPopupConfCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
        popupAccesesPinLocked.btnCallTMB.text = kony.i18n.getLocalizedString("keyCall");
        popupAccesesPinLocked.show();
        resetValues();
        //showAlert(kony.i18n.getLocalizedString("accLock"), kony.i18n.getLocalizedString("info"));
        return false;
      }


      var list;
      if (resulttable["languageCd"] != null) {
        var langFromCRM = resulttable["languageCd"].toLowerCase();
        var storeAppLocale = kony.store.getItem("curAppLocale");
        kony.print("storeAppLocale@@: "+storeAppLocale);
        if(isNotBlank(storeAppLocale)){
          var langFromDeviceStore = storeAppLocale["appLocale"];
          kony.print("langFromCRM@@: "+langFromCRM);
          kony.print("langFromDeviceStore@@: "+langFromDeviceStore);
          if (kony.string.startsWith(langFromDeviceStore, langFromCRM, true) == false) {
            kony.print("langFromDeviceStore containsChars@@: "+langFromDeviceStore);
            if (kony.string.startsWith(langFromDeviceStore, "en", true) == true) {
              kony.print("langFromDeviceStore is EN@@: ");
              list = {
                appLocale: "en_US"
              }
              setLocaleEng();
            }else{
              kony.print("langFromDeviceStore is TH@@: ");
              list = {
                appLocale: "th_TH"
              }
              setLocaleTH();
            }

          }else{
            if (resulttable["languageCd"] == "TH") {
              list = {
                appLocale: "th_TH"
              }
              setLocaleTH();
            } else {
              list = {
                appLocale: "en_US"
              }
              setLocaleEng();
            }
          }
        }else{
          if (resulttable["languageCd"] == "TH") {
            list = {
              appLocale: "th_TH"
            }
            setLocaleTH();
          } else {
            list = {
              appLocale: "en_US"
            }
            setLocaleEng();
          }
        }
      } else {
        list = {
          appLocale: "en_US"
        }
        setLocaleEng();
      }
      kony.store.removeItem("curAppLocale");
      kony.store.setItem("curAppLocale", list);

      var getcurAppLocale = kony.store.getItem("curAppLocale");
      if (getcurAppLocale["appLocale"] != null) {
        if (getcurAppLocale["appLocale"] == "en_US") {
          //setLocaleEng();
        } else if (getcurAppLocale["appLocale"] == "th_TH") {
          //setLocaleTH();
        }
      }
      if (resulttable["languageCd"] == null) {
        if (getcurAppLocale["appLocale"] != null) {
          kony.i18n.setCurrentLocaleAsync(getcurAppLocale["appLocale"], onsuccess(), onfailure(), "");
        }

      }
      //partyinq result set
      if (resulttable["opstatusPartyInq"] == 0) {
        gblCustomerName = resulttable["customerName"];
        gblCustomerFullNameEn = resulttable["customerFullNameEn"];
        gblCustomerNameTh = resulttable["customerNameTH"];
        gblCustomerFullNameTh = resulttable["CustomerFullNameTH"];
        
        gblCustomerAge = resulttable["birthDate"];
        //for (var i = 0; i < resulttable["ContactNums"].length; i++) {
        //if (resulttable["ContactNums"][i]["PhnType"] == 'Mobile')
        gblPHONENUMBER = resulttable["PhnNum"];
        gblCustomerIDType = resulttable["customerIDType"];
        if (gblCustomerIDType == "CI") {
          gblCustomerIDValue = resulttable["customerIDValue"];
          gblCustomerPassport = "";
        }else if(gblCustomerIDType == "PP"){
          gblCustomerPassport = resulttable["customerIDValue"];
          gblCustomerIDValue = "";
  	    }
        gblCustomerDob = resulttable["birthDate"];
        
        //Storing Encrypted CitizenID/Passport, URL and Encrypted Type for Loan Status tracking
        if(isNotBlank(resulttable["customerIDOrPPEncr"])){
          gblEncryptedCitizenID = resulttable["customerIDOrPPEncr"];
        }
        if(isNotBlank(resulttable["customerMobileNumberEncrpt"])){
          gblEncryptedPhoneNumber = resulttable["customerMobileNumberEncrpt"];
        }
        if(isNotBlank(resulttable["LoanStatusURL"])){
          gblLoanStatusURL = resulttable["LoanStatusURL"];
        }
        if(isNotBlank(resulttable["LoanStatusEncryptType"])){
          gblLoanStatusEncryptType = resulttable["LoanStatusEncryptType"];
        }
        
        kony.print("gblCustomerName login opt@@: "+gblCustomerName);
  		kony.print("gblCustomerNameTh login opt@@: "+gblCustomerNameTh);
  		kony.print("gblCustomerDob login opt@@: "+gblCustomerDob);
  		kony.print("gblCustomerIDType login opt@@: "+gblCustomerIDType);
  		kony.print("gblCustomerIDValue login opt@@: "+gblCustomerIDValue);
  		kony.print("gblCustomerPassport login opt@@: "+gblCustomerPassport);
        
		setupInsertIOParam(resulttable);
		
        //}
        //frmAccountSummaryLanding.lblAccntHolderName.text = resulttable["customerName"];
        cleanPreorPostForms();
        if (gblPushNotificationFlag) {
          GBL_Fatca_Flow = "pushnote";
          FatcaFlowNew(resulttable);
        } else {

          //Login composite service will return the messages count
          //unreadInboxMessagesMBLogin();
          if (resulttable["statusInboxUnread"] == "1") {
            gblUnreadCount = "0";
          } else if (resulttable["statusInboxUnread"] == "0") {
            gblUnreadCount = resulttable["unreadInboxUnread"];
          } else {
            gblUnreadCount = "0";
          }
          if (resulttable["statusMessagesUnread"] == "1") {
            gblMessageCount = "0";
          } else if (resulttable["statusMessagesUnread"] == "0") {
            gblMessageCount = resulttable["messagesUnread"];
          } else {
            gblMessageCount = "0";
          }
          var badgeCount = parseInt(gblUnreadCount) + parseInt(gblMessageCount);
          gblMyInboxTotalCountMB = badgeCount;
          if (kony.string.equalsIgnoreCase("iphone", gblDeviceInfo.name) || kony.string.equalsIgnoreCase("iPad", gblDeviceInfo.name) || kony.string.equalsIgnoreCase("iPod touch", gblDeviceInfo.name)) {
            kony.application.setApplicationBadgeValue(badgeCount);
          }
          GBL_Fatca_Flow = "login";
          FatcaFlowNew(resulttable);
          //callCustomerAccountService();
        }
      } else {
        alert(" " + resulttable["errMsg"]);
        kony.application.dismissLoadingScreen();
        return false;
      }
    }
  } else {
    //alert(" " + resulttable["errMsg"]);
    // Hard coding alert msg  for debugging prod issue 51680 - Unknown error while Re activation of User.
    // As the issue is occuring intermediate, to debug and capture the logs this alert msg is coded.
    //alert("Sorry for inconvinience caused");
    resetValues();
    var errorMessage = kony.i18n.getLocalizedString("ECGenOTPRtyErr00001");
    alert(""+errorMessage);
    kony.application.dismissLoadingScreen();
    return false;
  }

}
/*
New Function for fatca Flow
*/

function FatcaFlowNew(resulttable) {
  kony.print("In New Login Optimisation Code FatcaFlowNew%%%%%%%%")
  kony.print("FPRINT: IN FatcaFlow");
  TMBUtil.DestroyForm(frmFATCAQuestionnaire1);
  TMBUtil.DestroyForm(frmFATCATnC);
  gblFATCAAnswers = {};
  FatcaQues_EN = {};
  FatcaQues_TH = {};
  //GLOBAL_FATCA_MAX_SKIP_COUNT = 5;
  //gblFATCASkipCounter = parseInt(resulttable["FATCASkipCounter"]);
  gblLoginType = "";
  //resulttable["FatcaFlag"] = "9";	//Comment this line when service response comes
  if (resulttable["FatcaFlag"] == "0") {
    dismissLoadingScreen();
    gblFATCAUpdateFlag = "0";
    showFACTAInfo();
  } else if (resulttable["FatcaFlag"] == "8" || resulttable["FatcaFlag"] == "9" ||
             resulttable["FatcaFlag"] == "P" || resulttable["FatcaFlag"] == "R" || resulttable["FatcaFlag"] == "X") {
    dismissLoadingScreen();
    gblFATCAUpdateFlag = "Z";
    setFATCAMBGoToBranchInfoMessage();
  } else /*if(resulttable["FATCAFlag"] == "N" || resulttable["FATCAFlag"] == "I" || resulttable["FATCAFlag"] == "U") */ {
    //showLoadingScreen();
    kony.print("FPRINT: Calling LoginProcessServExecMB");
    LoginProcessServExecMBNew("", resulttable);
  }
}

function LoginProcessServExecMBNew(upgradeSkip, resulttable) {
  kony.print("In New Login Optimisation Code LoginProcessServExecMBNew%%%%%%%%")
  kony.print("FPRINT: Calling getValidTouchCounter");
  touchflag = getValidTouchCounter();
  kony.print("FPRINT: touchflag=" + touchflag);
  kony.print("FPRINT: gblNewCrmId=" + gblNewCrmId);
  //We show frmRegistrationPopup for new CrmId also.
  if (touchflag && gblTouchStatus == "N" && gblActivationFlow == true) {
    isSignedUser = true;
    if (gblDeviceInfo["name"] == "iPhone") {
      frmTouchIdIntermediateLogin.show();
    } else {
      frmRegistrationPopup.show();
    }
    gblActivationFlow = false;
    dismissLoadingScreen();
  } else {
    if (GBL_Fatca_Flow == "pushnote") {
      gblPushNotificationFlag = false;
      pushNotificationAllow(gblPayLoadKpns, upgradeSkip);
    } else {
      //Adding code to remove the dynamically added menu widgets if any and add them again
      inboxclickedClicked = 1;
      settingsClicked = 1;
      moreClicked = 1;
      if(frmMenu.flexdynamicContainer != null)
      frmMenu.flexdynamicContainer.removeAll();
      // frmAccountSummaryLanding.show();
      customerAccountCallUpdatedBack(resulttable);

    }
  }
}
/*
New function with the call back from Composite service
*/

function customerAccountCallUpdatedBack(resulttable) {
  try{
    kony.print("In New Login Optimisation Code customerAccountCallUpdatedBack%%%%%%%%")
    //Account Summary cache balance default value
    if(isCacheFlow == true && null != resulttable["accountSummary"][0]["custAcctRec"]){ 
      for(var i = 0; i < resulttable["accountSummary"][0]["custAcctRec"].length; i++){
        resulttable["accountSummary"][0]["custAcctRec"][i].availableBalDisplay = "....";
        if (resulttable["mfTotalAmount"] != null)
          resulttable["mfTotalAmount"] = "....";
        if ( resulttable["totalSumInsured"] != null)
          resulttable["totalSumInsured"] = "....";  
      }
    }else{
      if(resulttable["accountSummary"] != null || resulttable["accountSummary"] != undefined)
        kony.store.setItem("cachedResulttableASLoginFlow",resulttable); 
    }

    //Profile picture current version
    gblProfilePicCurrentVersion = resulttable["profileImageStatus"];

    var accountSummaryResp = "";
    if (resulttable["accountSummary"] != null || resulttable["accountSummary"] != undefined) {
      accountSummaryResp = resulttable["accountSummary"][0];

      //Sets AS BG image in Occasions.
      setASBGImageOrColor(accountSummaryResp["isSpecialOccasion"]); 

      kony.print("The values are" + accountSummaryResp["opstatus"] + "&&&" + accountSummaryResp["statusCode"])
      if (accountSummaryResp["opstatus"] == 0) {
        gblshorCutToAccounts=accountSummaryResp.shorCutToAccounts;
        ITMX_TRANSFER_EWALLET_ENABLE = accountSummaryResp["ITMX_TRANSFER_EWALLET_ENABLE"];

        //for transfers fee calculations global variables
        gblORFTRange1Lower = accountSummaryResp.ORFTRange1Lower;
        gblORFTRange1Higher= accountSummaryResp.ORFTRange1Higher;
        gblORFTRange2Lower = accountSummaryResp.ORFTRange2Lower;
        gblORFTRange2Higher= accountSummaryResp.ORFTRange2Higher;
        gblORFTSPlitFeeAmnt1 = accountSummaryResp.ORFTSPlitFeeAmnt1;
        gblORFTSPlitFeeAmnt2 = accountSummaryResp.ORFTSPlitFeeAmnt2;

        gblSMARTRange1Higher = accountSummaryResp.SMARTRange1Higher;
        gblSMARTRange1Lower = accountSummaryResp.SMARTRange1Lower;
        gblSMARTRange2Higher = accountSummaryResp.SMARTRange2Higher;
        gblSMARTRange2Lower = accountSummaryResp.SMARTRange2Lower;
        gblSMARTSPlitFeeAmnt1 = accountSummaryResp.SMARTSPlitFeeAmnt1;
        gblSMARTSPlitFeeAmnt2 = accountSummaryResp.SMARTSPlitFeeAmnt2;

        gblALL_SMART_FREE_TRANS_CODES = accountSummaryResp["ALL_SMART_FREE_TRANS_CODES"];	
        gblSMART_FREE_TRANS_CODES = accountSummaryResp["SMART_FREE_TRANS_CODES"];	
        gblORFT_FREE_TRANS_CODES = accountSummaryResp["ORFT_FREE_TRANS_CODES"];
        gblORFT_ALL_FREE_TRANS_CODES = accountSummaryResp["ALL_ORFT_FREE_TRANS_CODES"];	

        gblMaxTransferORFT = accountSummaryResp.ORFTTransLimit;
        gblMaxTransferSMART = accountSummaryResp.SMARTTransLimit;
        gblTransORFTSplitAmnt = accountSummaryResp.ORFTTransSplitAmnt;
        gblTransSMARTSplitAmnt = accountSummaryResp.SMARTTransAmnt;
        gblLimitORFTPerTransaction = accountSummaryResp.ORFTTransSplitAmnt;
        gblLimitSMARTPerTransaction = accountSummaryResp.SMARTTransAmnt;
        setToAccPromptPayGlobalVars(accountSummaryResp);        
        //Transfers Fee code

        if (accountSummaryResp["statusCode"] != 0) {
          alert(kony.i18n.getLocalizedString("ECGenericError"));
          cleanPreorPostForms();
          dismissLoadingScreen();
        } else if (accountSummaryResp["statusCode"] == -1) {
          //var errMessage = kony.i18n.getLocalizedString("errMsg");
          if(accountSummaryResp["errMsg"]=="100" ||accountSummaryResp["errMsg"]==100){
            alert(kony.i18n.getLocalizedString("ECGenericError"));
          }else{
            alert(accountSummaryResp["errMsg"]);
          }
          cleanPreorPostForms();		
          dismissLoadingScreen();
        }else if(accountSummaryResp["opstatus_hidden"] == 2){
          var crmIdIm= accountSummaryResp["crmId"];
          if(accountSummaryResp["shorCutToAccounts"] != undefined && accountSummaryResp["shorCutToAccounts"] != null){
            gblshorCutToAccounts=accountSummaryResp["shorCutToAccounts"];
          }
          isSignedUser = true;
          /*
          gblMyProfilepic = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=Y&personalizedId=&billerId=";
          if(gblMyProfilepic == null)
            frmAccountSummaryLanding.imgProfile.src="avatar1.png";
          else
            frmAccountSummaryLanding.imgProfile.src = gblMyProfilepic;*/

          //MIB-1069
          //frmAccountSummaryLanding.lblHiddenAccount.text=kony.i18n.getLocalizedString("errMsgHidden");
          frmAccountSummaryLanding.lblHiddenAccount.text=kony.i18n.getLocalizedString("MB_ASTxt_NoAccount");
          frmAccountSummaryLanding.lblAllHidden.setEnabled(true)
          frmAccountSummaryLanding.hbox454820850134417.onClick = onClickWhenNoAcctMB;

         /* frmAccountSummaryLanding.lblForUse.text = "0.00" + kony.i18n.getLocalizedString("percent");
          frmAccountSummaryLanding.lblForsave.text = "0.00" + kony.i18n.getLocalizedString("percent");
          if(flowSpa) {
            frmAccountSummaryLanding.label4751247744357.text = kony.i18n.getLocalizedString("trmDeposit");
            frmAccountSummaryLanding.lblFunds.text = "0.00" + kony.i18n.getLocalizedString("percent");
          } else {
            frmAccountSummaryLanding.label4751247744357.text = kony.i18n.getLocalizedString("KeyForInvest");
            frmAccountSummaryLanding.lblFunds.text = "0.00" + kony.i18n.getLocalizedString("percent");
          }

          frmAccountSummaryLanding.lblBalanceValue.text = "0.00 " + kony.i18n.getLocalizedString("currencyThaiBaht");
          */
          cleanPreorPostForms();
          //if cmp internal link set other than account summary then we do not show account summary form.
          //isCmpFlowFrmActivation set to true, if cmp internal link set	
          //alert("11 in account summuary js , isCmpFlowFrmActivation : "+isCmpFlowFrmActivation);
          if(isCmpFlowFrmActivation){
            if(gblCampaignDataEN == 'frmAccountSummary' || gblCampaignDataTH == 'frmAccountSummary'){
              //activaition complete screen banner interal link is set as account summary.
              //frmAccountSummaryLanding.show();
              showAccountSummaryNew();
            }else {
              //activaition complete screen banner interal link is NOT set as account summary.
              //so calling corresponding internal link function
              getCampaignResult();
            }
            isCmpFlowFrmActivation = false;
          }else{
            //to show anyId announcement page while login and actiivation complete screen.
            //gblShowAnyIDRegistration set to true, if any Id is turn ON
            //displayAnnoucementtoUser set to true, if user is not registered with ANYID with TMB
            if("transfer" == gbl3dTouchAction){
              gbl3dTouchAction="";
              transferFromMenu();	//moving to Transfer flow	
            }else if("billpay" == gbl3dTouchAction){
              gbl3dTouchAction="";
              callBillPaymentFromMenu();//moving to Bill Pay here
            }else if("topup" == gbl3dTouchAction){
              gbl3dTouchAction="";
              callTopUpFromMainMenu();//moving to Top Up here
            }else if(gblQuickBalanceFromLogin){
              onClickQuickBalanceMenu();
            }else if(gblPreLoginBillPayFlow){
              gblPreLoginBillPayFlow = false;
              callBillPaymentFromQR();
            }else if(gblQrSetDefaultAccnt){
              gblQrSetDefaultAccnt = false;
              onSelectFromAccntQRPay();
            }else if(gblGoDirectlyToPrompPay){
              gblGoDirectlyToPrompPay = false;
              menuSetUserIDonClick();
            }else if(gblShowAnyIDRegistration && displayAnnoucementtoUser) {
              frmRegAnyIdAnnouncement.imgAnydAnnouPage.src = "";
              frmRegAnyIdAnnouncement.show();
            }else if(displayRTPAnnoucement) {
              startUpRTPDisplay();
            }else if (gblTransferPush || gblRTPBillPayPush ) {
              inquiryRTPPush();
            }else if(gblDeeplinkExternalAccess){
              showLoadingScreen();
              deepLinkAccess(gblContentDeeplinkData);
              gblDeeplinkExternalAccess = false;
              gblContentDeeplinkData = "";
            }else{
              //forceAccountSummaryRefresh();
              //frmAccountSummaryLanding.show();
              showAccountSummaryNew();
            }
          }
          TMBUtil.DestroyForm(frmMBPreLoginAccessesPin); //frmAfterLogoutMB
          dismissLoadingScreen();
        }else{
          gblFinancialTxnMBLock=accountSummaryResp["enableMaintainenceFinancialTxnMB"];
          //setting the Date value from the Login response
          if(accountSummaryResp["TODAY_DATE"] != null)
            GLOBAL_TODAY_DATE = accountSummaryResp["TODAY_DATE"];
          kony.print("@@Above gblRTPBadgeCount"+accountSummaryResp["RTPTotalRecordsCount"]);
          //Assigning RTP Badge count to Global variable.
          gblRTPBadgeCount = accountSummaryResp["RTPTotalRecordsCount"];
          gblAccountTable = accountSummaryResp;
          setRTPBadgeCountNew();
          //createNoOfRowsDynamically(accountSummaryResp);
          //New Account Summary Flow.
          gblXCord = "";
          gblYCord = "";
          gblAccountSummaryData = accountSummaryResp;
          setProfilePicBalance();
          setAccountSummaryData();

          var gblPlatformName = gblDeviceInfo.name;
          if(isCacheFlow == false)
            loggingDeviceStoreData();

          isSignedUser = true;
          var deviceId = getDeviceID();

          //After loading My Account Summary Page content is loaded..
          if(gblNotificationFor == "bb" || gblNotificationFor == "BB"){
            //go to Beep and Bill flow:
            customerPaymentStatusInquiryForBB(gblPayLoadKpns); 	
            gblNotificationFor = "randomXY";
          }else if(gblNotificationFor == "s2s" || gblNotificationFor == "S2S"){
            //showLoadingScreen();
            //go to Send To Save Execution Flow
            gblExeS2S = "true";
            gblSSExcuteCnfrm = "randomXC"; // to control flow
            gblNotificationFor = "randomXC"
            getIBMBStatus();
            gblNotificationFor = "randomXY";//just to make sure that app is coming from notificaton or regular
          }else{
            //if cmp internal link set other than account summary then we do not show account summary form.
            //isCmpFlowFrmActivation set to true, if cmp internal link set	
            if(isCmpFlowFrmActivation){
              if(gblCampaignDataEN == 'frmAccountSummary' || gblCampaignDataTH == 'frmAccountSummary'){
                //activaition complete screen banner interal link is set as account summary.
                //frmAccountSummaryLanding.show();
                showAccountSummaryNew();
              }else {
                //activaition complete screen banner interal link is NOT set as account summary.
                //so calling corresponding internal link function
                getCampaignResult();
              }
              isCmpFlowFrmActivation = false;
            }else{
              //to show anyId announcement page while login and actiivation complete screen.
              //gblShowAnyIDRegistration set to true, if any Id is turn ON
              //displayAnnoucementtoUser set to true, if user is not registered with ANYID with TMB  
              if(isCacheFlow == false){
                if("transfer" == gbl3dTouchAction){
                  gbl3dTouchAction="";
                  transferFromMenu();	//moving to Transfer flow	
                }else if("billpay" == gbl3dTouchAction){
                  gbl3dTouchAction="";
                  callBillPaymentFromMenu();//moving to Bill Pay here
                }else if("topup" == gbl3dTouchAction){
                  gbl3dTouchAction="";
                  callTopUpFromMainMenu();//moving to Top Up here
                }else if(gblQuickBalanceFromLogin){
                  onClickQuickBalanceMenu();
                }else if(gblGoDirectlyToPrompPay){
                  gblGoDirectlyToPrompPay = false;
                  menuSetUserIDonClick();
                }else if(gblPreLoginBillPayFlow){
                  gblPreLoginBillPayFlow = false;
                  callBillPaymentFromQR();
                }else if(gblQrSetDefaultAccnt){
                  gblQrSetDefaultAccnt = false;
                  if(gbleDonationType == "QR_Type"){
                    onSelectFromAccntEDonation();
                  }else{
                    onSelectFromAccntQRPay();
                  }
                }else if (gblShowAnyIDRegistration && displayAnnoucementtoUser) {
                  frmRegAnyIdAnnouncement.imgAnydAnnouPage.src = "";
                  frmRegAnyIdAnnouncement.show();
                }else if (displayRTPAnnoucement) {
                  startUpRTPDisplay();
                }else if (gblTransferPush || gblRTPBillPayPush ) {
                  inquiryRTPPush();
                }else if(gblDeeplinkExternalAccess){
                  showLoadingScreen();
                  deepLinkAccess(gblContentDeeplinkData);
                  gblDeeplinkExternalAccess = false;
                  gblContentDeeplinkData = "";
                }else{
                  //forceAccountSummaryRefresh();
                  //frmAccountSummaryLanding.show();
                  showAccountSummaryNew();
                }
              }else{
                //forceAccountSummaryRefresh();
                //frmAccountSummaryLanding.show();
                showAccountSummaryNew();
              }
            }
            if(isCacheFlow == false)
              allowOtherOpr = true;
            // below condition is called on click of return btn of s2s activation complete screen
            if (gblConfOrComp == true) {
              gblConfOrComp = false; // to control UI of s2s apply flow
              TMBUtil.DestroyForm(frmSSConfirmation);
            }
            dismissLoadingScreen();
          }
          accountDetailsClear();
          //TMBUtil.DestroyForm(frmAfterLogoutMB);
          kony.application.dismissLoadingScreen();
          GBL_MALWARE_FOR_RISK_FLAG = true;
          collectRiskData();
        }
      }else{
        kony.application.dismissLoadingScreen();
      }
      //endAccountSummaryAnimation();
    }
  }catch(e){
    kony.print("Exception in account summary "+e);
  }
}
