var gblPOWcustNME = null;
var gblPOWtransXN = null;
var gblPOWchannel = null;
var gblPOWstateTrack = false;
var gblDeviceInfo = null;
var KonyDMObject = null;
var imageDataEN=[];
var imageDataTH=[];
var bannerDataEN=[];
var bannerDataTH=[];
var bannerFlag="";
var androidFileType="png";
mfClient=null;
bbServiceObj=null;
idealTimeout = true;

/*
************************************************************************
            Name    : hideUsername()
            Author  : Shubhanshu Yadav
            Date    : January 02, 2013
            Purpose : Formats the username to show only the last 4 digits.
        Input params: accoutNumber
       Output params: na
        ServiceCalls: nil
        Called From : adddatainseg() function
************************************************************************
 */


/*
 ***********************************************************************
 * Name : getHeader
 * Author : Nishant Kumar
 * Date : April 2 2013
 * Purpose : Generate custom header from template header "hdrCommon"
 * Params: menuCallBack,lbltext,btnSkin,btnFocusSkin, btnCallBack (if not required pass 0)
 * ServiceCalls: nil
 * Called From : preShow of each form
 **********************************************************************
 */

function getHeader(menuCallBack, lbltext, btnSkin, btnFocusSkin, btnCallBack) {
	//Individual widgets can be accessed from template header "hdrCommon"
	if (menuCallBack != 0)
		hbxHdrCommon.btnHdrMenu.onClick = menuCallBack;
	//Reset label width,skin and alignment
	hbxHdrCommon.lblHdrTxt.skin = "lblHeader";
	hbxHdrCommon.lblHdrTxt.containerWeight = 70;
	//hbxHdrCommon.lblHdrTxt.margin = [0,3,0,0];
	//hbxHdrCommon.lblHdrTxt.contentAlignment = constants.CONTENT_ALIGN_CENTER;
	if (lbltext != 0)
		hbxHdrCommon.lblHdrTxt.text = lbltext;
	else
		hbxHdrCommon.lblHdrTxt.text = " ";
	if (btnSkin != 0 && btnCallBack != 0) {
		hbxHdrCommon.btnRight.isVisible = true;
		hbxHdrCommon.btnRight.skin = btnSkin;
		hbxHdrCommon.btnRight.focusSkin = btnFocusSkin;
		hbxHdrCommon.btnRight.onClick = btnCallBack;
	} else {
		hbxHdrCommon.btnRight.isVisible = false;
		//hbxHdrCommon.lblHdrTxt.containerWeight = 75;
	}
	// Default arrow position is middle
	// If you don't want arrowtop, make the src as "empty.png"
	// If you are customizing images in "hbxArrow" that line or method must be placed/called after getHeader() 
	hbxArrow.imgHeaderMiddle.src = "arrowtop.png";
	hbxArrow.imgHeaderRight.src = "empty.png";
}


function mobileMethod() {
	//alert("i am here");
	frmMyProfiles.show();
}

function checkMBUserStatus() {
	if (gblUserLockStatusMB == "03") {
		//showAlert(kony.i18n.getLocalizedString("keyErrTxnPasslock"), kony.i18n.getLocalizedString("info"));
		dismissLoadingScreen();
		popTransferConfirmOTPLock.show();
		return false;
	}
	return true;
}


function getServerImagePath(img){
	var randomnum = Math.floor((Math.random() * 10000) + 1);
		var imgStr = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext 
									+ "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+img
									+"&modIdentifier=PRODUCTPACKAGEIMG&dummy=" + randomnum;

return 	imgStr;										
}


/**
 * description
 * @returns {}
 */
/*
function invokeServiceSecureAsync(serviceID, inputParam, callBackFunction) {
	if (appConfig.secureServerPort != null) {
		//var url = "http://" + appConfig.serverIp + ":9080" + "/" + appConfig.middlewareContext + "/MWServlet";
		var url = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext +"/MWServlet";
	} else {
		var url = "https://" + appConfig.serverIp + "/" + appConfig.middlewareContext + "/MWServlet";
	}
	var sessionIdKey = "cacheid";
	inputParam.appID = appConfig.appId;
	inputParam.appver = appConfig.appVersion;
	inputParam["serviceID"] = serviceID;
	if (gblDeviceInfo.length == 0) {
		gblDeviceInfo = kony.os.deviceInfo();
	}
	if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "android") {
		inputParam["channel"] = "rc";
	} else {
		inputParam["channel"] = "wap";
	}
	inputParam["platform"] = gblDeviceInfo.name;
	inputParam[sessionIdKey] = sessionID;
	if (globalhttpheaders) {
		if (inputParam["httpheaders"]) {
			inputParam.httpheaders = mergeHeaders(inputParam.httpheaders, globalhttpheaders);
		} else {
			inputParam["httpheaders"] = globalhttpheaders;
		};
	};
	var connHandle = kony.net.invokeServiceAsync(url, inputParam, callBackFunction, null, DEFAULT_SERVICE_TIMEOUT);
	return connHandle;
}*/
/*
************************************************************************
            Name    : change i18n
            Author  : Gaurav Mandal	
            Date    : April 09, 2013
            Purpose : Changes the locale of the app
        Input params: na
       Output params: na
        ServiceCalls: nil
        Called From : language toggle buttons
************************************************************************
 */
// Sets app locale based on device locale at the startup

function startupLocale() {
	//#ifdef android
		var getcurAppLocale = kony.store.getItem("curAppLocale");
		kony.print("getcurAppLocale:"+getcurAppLocale);
  		if (getcurAppLocale != null) {
		  kony.print("inside locale null");	
          kony.i18n.setCurrentLocaleAsync(getcurAppLocale["appLocale"], onsuccess(), onfailure(), "");
		} else {
			if (kony.i18n.getCurrentDeviceLocale() == "th_TH") {
			kony.i18n.setCurrentLocaleAsync("th_TH", onsuccess(), onfailure(), "");
			}
		}
	
	//#endif
}

function onsuccess() {
	
}

function onfailure() {
	
}

// To set the locale for prelogin flow 
function preLoginLocaleSet() {
  kony.ds.remove("LicenseDisableFlag");
    var getcurAppLocale = kony.store.getItem("curAppLocale");
    if (isSignedUser == false) {

        if (getcurAppLocale != null) {
            kony.i18n.setCurrentLocaleAsync(getcurAppLocale["appLocale"], onsuccesspreCallback, onfailurepreCallback, "");            
        } else {
            var deviceLanguage = kony.i18n.getCurrentDeviceLocale();
            kony.print("deviceLanguage:" + deviceLanguage);
            if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPhone Simulator" || gblDeviceInfo["name"] == "iPad") {
                deviceLanguage = deviceLanguage.substring(0, 2);
            } else {
                deviceLanguage = deviceLanguage.language; // android
            }
            kony.print("deviceLanguage ssss:" + deviceLanguage);
            if (kony.string.startsWith(deviceLanguage, "th", true)) {
                kony.print("inside TH set");
                kony.i18n.setCurrentLocaleAsync("th_TH", onsuccesspreCallback, onfailurepreCallback, "");
            } else {
                kony.print("inside EN set");
                kony.i18n.setCurrentLocaleAsync("en_US", onsuccesspreCallback, onfailurepreCallback, "");
            }
        }

    }
}


function onsuccesspreCallback() {
	kony.print("onsuccesspreCallback");
}

function onfailurepreCallback() {
	kony.print("onfailurepreCallback");
}
//To change the locale to THAI from button inside forms.

function setLocaleTH() {
    if(!flowSpa){
      if (kony.i18n.isLocaleSupportedByDevice("th_TH")) {
        kony.i18n.setCurrentLocaleAsync("th_TH", onsuccesscallback, onfailurecallback, "")
      } else {
        //just for testin purposes in emulator
        //kony.i18n.setCurrentLocaleAsync("ru_RU", onsuccesscallback, onfailurecallback, "");
      }
    } else {
      kony.i18n.setCurrentLocaleAsync("th_TH", onsuccesscallback, onfailurecallback, "")
    }
}
//To change the locale to ENGLISH from button inside forms.

function setLocaleEng() {
	//kony.application.showLoadingScreen("localeBlock","", "centre", true, true, false);
	kony.i18n.setCurrentLocaleAsync("en_US", onsuccesscallback, onfailurecallback, "");
}

function onsuccesscallback() {
	
	var currForm = kony.application.getCurrentForm();
	//currForm.destroy();
	//deviceInfo = kony.os.deviceInfo();
	var type = gblDeviceInfo["type"];
	var category = gblDeviceInfo["category"];
	if(currForm!=frmMBPreLoginAccessesPin && currForm!=frmFATCAQuestionnaire1)
	{
			currForm.show();
		}
		else if(currForm==frmFATCAQuestionnaire1)
		{
			setQuestionLocaleFATCAMB();
			currForm.show();
	}
	
	kony.application.dismissLoadingScreen();
	if (type == "spa" || category == "android") {
		if (gblLang_flag == "en_US") {
			//alert("ENG");
			//kony.application.getCurrentForm().lblLocale.text = "en_US";
			frmMBTnC.btnEngR.skin = "btnOnFocus";
			frmMBTnC.btnThaiR.skin = "btnOffFocus";
		}
		if (gblLang_flag == "th_TH") {
			//alert("THAI");
			//kony.application.getCurrentForm().lblLocale.text = "th_TH";
			frmMBTnC.btnEngR.skin = "btnOnNormal";
			frmMBTnC.btnThaiR.skin = "btnOffNorm";
		}
	}
}

function onfailurecallback() {
	
	return 0;
}


function preShow(){
  if(kony.os.deviceInfo().name == "android"){
   // frmDreamCalculator.scrollbox156816275924507.containerHeight = 100 ;//60
   // frmDreamCalculator.scrollbox156816275924507.containerHeightReference = constants.HEIGHT_BY_DEVICE_REFERENCE;
    frmDreamCalculator.hbox86682510926373.containerHeight = 80;
    frmDreamCalculator.scrollbox156816275924507.containerHeightReference = constants.HEIGHT_BY_DEVICE_REFERENCE;
    frmDreamCalculator.bounces = false;
    frmDreamCalculator.scrollbox156816275924507.bounces=false;
  }
}



/**
 * description
 * @returns {}
 */

function registerForTimeOut() {
	idleTimeOut = 5;
	//alert("In registerforTimeout");
	if (GLOBAL_KONY_IDLE_TIMEOUT != null && GLOBAL_KONY_IDLE_TIMEOUT != undefined) {
		
		idleTimeOut = kony.os.toNumber(GLOBAL_KONY_IDLE_TIMEOUT);
	}
	
	
	try{
	kony.application.unregisterForIdleTimeout();
	}
	catch(e){
	}
	try{
		kony.timer.cancel("idletimeoutcheck");
	}
	catch(e){
	}
	kony.timer.schedule("idletimeoutcheck", invokeidletimeout, 1, false)
	//alert(idleTimeOut);
	

}
function invokeidletimeout(){
	
	kony.application.registerForIdleTimeout(idleTimeOut, onIdleTimeOutCallBack);
	
}
/**
 * On idle time out, this function will be called.if activated user navigates to login screen else to frmBanking screen
 * @returns {}
 */

function onIdleTimeOutCallBack() {
    try {
        kony.print("on idle timeout logout");
        //alert("in idle timeout")
        isSignedUser = false;
        gblDeeplinkExternalAccess = false;
        gblContentDeeplinkData = "";
        gblFPLoginSvsCalled = false;
        resetTransferRTPPush();
        GBL_Time_Out = "true";
        var getEncrKeyFromDevice = kony.store.getItem("encrytedText");
        //disMissAllPopUps();
        if (getEncrKeyFromDevice != null) {
            /** calling this to remove the userid created while activation from ECAS*/

            gblTouchShow = false;
            clearGlobalVariables();
            //spaFormGlobalsCall();
            cleanPreorPostForms();
            kony.application.dismissLoadingScreen();
            idealTimeout = false;
            invokeLogoutService();
            gblFromLogout = true;
            resetMBPreloginaccesspinUI();
             resetValues();
            frmMBPreLoginAccessesPin.show();
            kony.application.dismissLoadingScreen();            
        } else {
          	if(GLOBAL_EKYC_ENABLE_FLAG === "true" || GLOBAL_EKYC_ENABLE_FLAG == true){  
              invokeLogoutService();
              //resetValues();
              //gblFromLogout = true;
               preshowfrmeKYCStartUp();
               frmeKYCStartUp.show();
              kony.application.dismissLoadingScreen();   
            }else{
              popupTractPwd.dismiss(); //DEF14068 Fix
           	  ActivationMBViaIBLogoutService(); // Fix for DEF1052
            //frmMBanking.show();   
            }
        }
    } catch (e) {
        //alert("wrong in trigger")
    }
}


function invokeDeleteUserServiceTimeOut(){
	
	inputParam = {};
	inputParam["segmentId"] = "MIB";
	
	invokeServiceSecureAsync("deleteUser", inputParam, deleteUserFromECASCallBack);
}
/*
************************************************************************
            Name    : callDummy 
            Author  : Gayathri
            Date    : April 16, 2013
            Purpose : On click of form back button
        Input params: na
       Output params: na
        ServiceCalls: nil
        Called From : AccountSummaryLanding.
************************************************************************
 */

function callDummy() {}
/*
************************************************************************
            Name    : disableBackButton 
            Author  : Gaurav Mandal
            Date    : April 20, 2013
            Purpose : On click of form back button
        Input params: na
       Output params: na
        ServiceCalls: nil
        Called From : All forms onDeviceBack property
************************************************************************
 */
function closeApplicationBackBtn(){
	try{
		 kony.application.exit();
      }
   	catch(Error)
      {
         alert("Exception While getting exiting the application  : "+Error);
      }
}


function showToastMsg(){
	//Creates an object of class 'ToastMessager'
	//var ToastMessagerObject = new Toast.ToastMessager();
	//Invokes method 'showToastMessage' on the object

	//ToastMessagerObject.showToastMessage(kony.i18n.getLocalizedString("MIB_BackbuttonMSG"), 1);
    
    var msg = kony.i18n.getLocalizedString("MIB_BackbuttonMSG");
    var toastMsg = new kony.ui.Toast({text:msg, duration:constants.SHORT});
    toastMsg.show();
}

function disableBackButton() {}
gblApplicationLaunch = false;
// Below code to hide the default loading indicator 
kony.application.setApplicationBehaviors({"hideDefaultLoadingIndicator":true});


function showStartUp() {
  	kony.print("usage >> start showStartUp "+new Date().getTime());
	
	//deviceInfo = kony.os.deviceInfo();
	if(gblDeviceInfo.name == "android"){
	     kony.application.setApplicationProperties({
			    "statusBarColor": "007ABC",
			    "statusBarForegroundColor": "007ABC"
			});	
	}
	flowSpa = false;
	//Code fix for defect Ticket51503. Earlier we were saving accessPIN in deviceDB during login.
	//So we are making that to empty and removing that from DeviceDB
	kony.store.setItem("userPIN", "");
	try {
		kony.store.removeItem("userPIN");
	} catch (err) {}

	var getEncrKeyFromDevice = kony.store.getItem("encrytedText");
	kony.print("getEncrKeyFromDevice in ds store: " + getEncrKeyFromDevice);
	var isActive = kony.store.getItem("isUserStatusActive");
	kony.print("is user Active: " + isActive);
    // to fix issue on j2 prime
	/*if (gblDeviceInfo.name == "android") {
		onClickCheckTouchIdAvailable();
	} else if (gblDeviceInfo["name"] == "iPhone") {
		onClickCheckTouchIdAvailableUsingKonyAPI();
	}*/

	// kony.print("Condition values: "+gblDeviceInfo.name +","+isAuthUsingTouchSupported() + ", "+hasRegisteredFinger()+","+isTouchIDEnabled());
	if (getEncrKeyFromDevice != null) {
    /*     if (gblDeviceInfo.name == "android") {
	       	onClickCheckTouchIdAvailable();
	    } 
		kony.print("getEncrKeyFromDevice is not null");
        //MIB-14053: Use Kony API for Touch ID
       //Commenting below code as same code called in form post show
		if (gblDeviceInfo.name == "android" && isAuthUsingTouchSupported() && hasRegisteredFinger() && isTouchIDEnabled()) {
			kony.print("inside touch id if block :" + isActive);
			//kony.print("FPRINT AT APPLAUNCH");
			//showLoadingScreen();
			//GBL_FLOW_ID_CA = 13;
			//getUniqueID();
			if (isActive) {
				//kony.print("FPRINT IN isUserStatusActive TO TRUE");
				isUserStatusActive = true;
				//kony.print("FPRINT USER STATUS IS ACTIVE");
				frmMBPreLoginAccessesPin.btnTouchnBack.skin = "btntouchiconstudio5";
				frmMBPreLoginAccessesPin.btnTouchnBack.focusSkin = "btntouchiconfoc";
				showFPFlex(true);
				fpdefault();
				closeQuickBalance();
			}
		} */

		/*if (gblDeviceInfo["name"] == "iPhone" && !isSignedUser) {
			kony.print("Before showing Touch ID Popup");
			showTouchIdPopupUsingKonyAPI();

			}*/
			startup = frmMBPreLoginAccessesPin;
			gblCurrentForm = "frmMBPreLoginAccessesPin";
		} else {
			if(kony.string.equalsIgnoreCase("iphone", gblDeviceInfo.name) || kony.string.equalsIgnoreCase("iPad", gblDeviceInfo.name) || kony.string.equalsIgnoreCase("iPod touch", gblDeviceInfo.name)){
			kony.application.setApplicationBadgeValue("");
			}
			  if(GLOBAL_EKYC_ENABLE_FLAG ==null || GLOBAL_EKYC_ENABLE_FLAG==""){
                startup=frmeKYCSplash;
                frmeKYCSplash.postShow=postshowfrmeKYCStartUp;
              }
              else if(GLOBAL_EKYC_ENABLE_FLAG === "true" || GLOBAL_EKYC_ENABLE_FLAG == true){     
                  var isRegistredUser = kony.store.getItem("eKYCRegistrationFlag");
                  if (isRegistredUser != null && isRegistredUser == "Y") {
                    startup=frmeKYCSplash;
                	callloginekycService();
                  }else{
                    refresheKYCStartup();
                    startup=frmeKYCStartUp;
                  }
//                  setCurrentLocale();
//                  startup=frmeKYCStartUp;
//                  refresheKYCStartup();
              }else{
                  startup = frmMBanking;
                  gblCurrentForm = "frmMBanking";
              }
		}
		if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPod touch" || gblDeviceInfo["name"] == "iPhone Simulator"){
		//Creates an object of class 'BarScan'(iPhone Barcode FFI)
		gblApplicationLaunch = true; // Use this variable in preshow access pin screen to show loading indicator.
		BarScanObject = new iphoneBarcode.BarScan();
	}
	/*if (gblUnavailableFlag && !gblIsBackgroundEvent) {
	if (!currentFormIsFrmUVApprovalMB()) {
	startup = getMBStartupForm();
	gblCurrentForm = startup.id;
	}
	}*/
	//}
    //startupLocale()
	initFATCAGlobalVars();
    loadPostLoginModules();
    intiliseCacheAtStartup(); //Fix for MIB-12085
	isSignedUser = false;    
	return startup;
}

function ekyctimercallback(){
  kony.print("timer done for 5 sec ");
  var getContentUpdate = kony.store.getItem("getContentUpdate"); 
  dismissLoadingScreen();
  postshowfrmeKYCStartUp();
//   if(getContentUpdate != null){
//   		var isRegistredUser = kony.store.getItem("eKYCRegistrationFlag");
//   		if (isRegistredUser != null && isRegistredUser == "Y") {
//     		callloginekycService();
//   		}else{
//              refresheKYCStartup();
//         }
//   }else{
//     postshowfrmeKYCStartUp();
//   }
  
}
function openMBActivationForm(){
  //startup = frmMBanking;
  gblCurrentForm = "frmMBanking";
  frmMBanking.show();
}

// function preshowfrmeKYCStartUp(){
//    var getContentUpdate = kony.store.getItem("getContentUpdate"); 
//      if(getContentUpdate != null){
//        		 refresheKYCStartup();
//      }else{
//        frmeKYCStartUp.postShow=postshowfrmeKYCStartUp;
//      }
// }

function  postshowfrmeKYCStartUp(){
 	 var getContentUpdate = kony.store.getItem("getContentUpdate"); 
     if(getContentUpdate == null){
       	showLoadingScreen();
        	kony.print("stop for 5 sec ");
              try{
                kony.timer.cancel("ekyctimer");
              }
              catch(e){
              }
              kony.timer.schedule("ekyctimer", ekyctimercallback, 5, false)
       }else{
         //refresheKYCStartup();
         //frmeKYCStartUp.show();
         	 	var isRegistredUser = kony.store.getItem("eKYCRegistrationFlag");
  				if (isRegistredUser != null && isRegistredUser == "Y") {
    			  callloginekycService();
  				}else{
                  refresheKYCStartup();
                  frmeKYCStartUp.show();
                }
       } 
}

function refresheKYCStartup(){
   gbleKYCVerifyAccessPin = "";
  var isRegistredUser = kony.store.getItem("eKYCRegistrationFlag");
  if (isRegistredUser != null && isRegistredUser == "Y") {
    var keyOpenAct=kony.i18n.getLocalizedString("eKYC_btnContinueOpenAccount");
    var keyOpenActPrdctKey = keyOpenAct;// + " " + productName;
    frmeKYCStartUp.btneKYCOpenacct.text=keyOpenActPrdctKey;
    frmeKYCStartUp.btneKYCOpenacct.onClick=loginAccounteKYC;
     //callloginekycService();
  }else{
    var keyOpenAct=kony.i18n.getLocalizedString("eKYC_btnOpenAccount");
    var keyOpenActPrdctKey = keyOpenAct;// + " " + productName;
    
    frmeKYCStartUp.btneKYCOpenacct.text=keyOpenActPrdctKey;
    //frmeKYCStartUp.btneKYCOpenacct.onClick=onclickOpenAccounteKYC;
    frmeKYCStartUp.btneKYCOpenacct.onClick=btnOpenAccountEKYC;
  }
  
   frmeKYCStartUp.btnLangChange.onClick=changeLocaleOneKYC;
   frmeKYCStartUp.btnOpenActivation.onClick=openMBActivationForm;
   frmeKYCStartUp.btnLangChange.text=kony.i18n.getLocalizedString("eKYC_ChangeLang");
  
//   var keyOpenAct=kony.i18n.getLocalizedString("eKYC_btnOpenAccount");
//   keyOpenAct=replaceAll(keyOpenAct, "<ProductName>", productName);
  
 
   frmeKYCStartUp.btnOpenActivation.text=kony.i18n.getLocalizedString("eKYC_btnActivateTMBtouch");
   frmeKYCStartUp.lblCustomer.text=kony.i18n.getLocalizedString("eKYC_lbNewCustomer");
   frmeKYCStartUp.lblExitingUser.text=kony.i18n.getLocalizedString("eKYC_lbExistingCustomer");
}
function loginAccounteKYC(){
   loadFunctionalModuleSync("eKYCOpenAcct");
   gbleKYCVerifyAccessPin = "EKYC_LOGIN";
   frmMBeKYCAccessPin.lblsetUpPin.setVisibility(false);
   frmMBeKYCAccessPin.lblsubHeader.bottom="0%";
   frmMBeKYCAccessPin.show();
  //1-2. Setting up VTAP SDK
    setUpVTapSDK();
}

function onclickOpenAccounteKYC(){
  loadFunctionalModuleSync("eKYCOpenAcct");
  gblMBNewTncFlow="eKYC_OPEN_ACCT";
   readUTFFileServiceeKYC();
  //1-1. Setting up VTAP SDK
    setUpVTapSDK();
}

function btnOpenAccountEKYC() {
  // frmMBeKYCIntroduction.btnNext.onClick=btnNexteKYCIntro;
  var nfcFlag = isNFCEnabled();
  if (null === nfcFlag) {
    if (GLOBAL_EKYC_CID_FLAG === "ON" && GLOBAL_NDID_FLAG === "ON") {
     	onclickOpenAccounteKYC();     
    }else{
       showAlert(kony.i18n.getLocalizedString("eKYC_msgDeviceNotSupporteKYCflow"), kony.i18n.getLocalizedString("info"));
    }
    
  } else {
    if (checkNFCSupprt()) {
			onclickOpenAccounteKYC();
    } 
  }
}

// This method will be used to load the specific module by passing module name. 
function loadFunctionalModulesAsync(moduleName){
   kony.print();
     var fmlsuccesscalback = function (modulename) {
        //Now we cn access objects defined in module2
       kony.print("loaded module "+modulename+" successfully");
    }
    var fmlerrorcalback = function (modulename, errorcode) {
        kony.print("error : " + errorcode + " in module - " + modulename);
    }
      try {
        kony.modules.loadFunctionalModuleAsync(moduleName,fmlsuccesscalback,fmlerrorcalback);
        
    } catch (err) {
        kony.print("Error loading functional modules"+err.message);
        kony.print("Error message"+err);
    }
  
}


function loadPostLoginModules(){
    //kony.print();
     var fmlsuccesscalback = function (modulename) {
        //Now we cn access objects defined in module2
       kony.print("loaded module "+modulename+" successfully");
    }
    var fmlerrorcalback = function (modulename, errorcode) {
        kony.print("error : " + errorcode + " in module - " + modulename);
    }
      try {
        kony.modules.loadFunctionalModuleAsync("postLoginModules",fmlsuccesscalback,fmlerrorcalback);
        kony.modules.loadFunctionalModuleAsync("postLoginSeg",fmlsuccesscalback,fmlerrorcalback);
        kony.modules.loadFunctionalModuleAsync("postLoginPopups",fmlsuccesscalback,fmlerrorcalback);
        
    } catch (err) {
        kony.print("Error loading functional modules"+err.message);
        kony.print("Error message"+err);
    }

}


function loadFunctionalModuleSync(moduleName){
  try {
        kony.modules.loadFunctionalModule(moduleName);               
     } catch (err) {
       kony.print("Error loading functional modules");
    }
}

/**
 * description
 * @returns {}
 */
 
 function onClickDownloadPDFCommon(fullUrl){
 
 			if(gblDeviceInfo.name == "android"){
			if(KonyDMObject == null)
	 			KonyDMObject = new KonyandroidUtils.KonyDM();
				showLoadingScreen();
	 			KonyDMObject.downloadFile(
					/**String*/ fullUrl, 
					/**Function*/ callbackDownload);		
			}else{
			
			if(flowSpa && (gblDeviceInfo.category == "android"))
			{
			
			try {
		            var cwForm = document.createElement("form");
		        	cwForm.setAttribute('method',"post");
		        	cwForm.setAttribute('action',fullUrl);
		        	if(document.getElementsByTagName('body')!= null && document.getElementsByTagName('body')[0]!= null)
		        	{
		            	document.getElementsByTagName('body')[0].appendChild(cwForm);
		        	}
		        	kony.application.dismissLoadingScreen();            
		        	cwForm.submit();
	    		} catch(ex){
	       			
	    		}
			
			
			}else
			{
				kony.application.openURL(fullUrl);
			}
			}
 
 }

function onClickDownloadPDF() {
	gblLang_flag = kony.i18n.getCurrentLocale();
	androidFileType = "pdf";
	if (gblLang_flag == "en_US") {
		if (GLOBAL_PDF_DOWNLOAD_URL_en_US != null && GLOBAL_PDF_DOWNLOAD_URL_en_US.length > 0) {
			
			onClickDownloadPDFCommon(GLOBAL_PDF_DOWNLOAD_URL_en_US);
						
		} else {
			alert(kony.i18n.getLocalizedString("pdf_download_error"));
		}
	} else {
		if (GLOBAL_PDF_DOWNLOAD_URL_th_TH != null && GLOBAL_PDF_DOWNLOAD_URL_th_TH.length > 0) {
		
			onClickDownloadPDFCommon(GLOBAL_PDF_DOWNLOAD_URL_th_TH);
			
		} else {
			alert(kony.i18n.getLocalizedString("pdf_download_error"));
		}
	}
}

//e.g filetype=pdf or png filename=FirstTimeActivation lookup tmb_tnc.properties
function onClickDownloadTnC(filetype, filename) {
	gblLang_flag = kony.i18n.getCurrentLocale();
	androidFileType = filetype;
	//var pdfimageurl = "http://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext +"/pdforimagerender";
	var pdfimageurl = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext +"/pdforimagerender";
	filename = replaceAndSymbol(filename);//adding this condtion for DEF212
	var  uriquery = "?localID="+encodeURIComponent(gblLang_flag)+"&filetype="+encodeURIComponent(filetype)+"&filename="+encodeURIComponent(filename);
	onClickDownloadPDFCommon(pdfimageurl+uriquery);
	//alert(kony.i18n.getLocalizedString("pdf_download_error"));
}

/*replaceAndSymbol function will replace & symbol with And string
 * @param, fileName
 * @return replacedAndString
 */
function replaceAndSymbol(fileName){
 var newFileName = fileName;
 if(undefined != newFileName && null != newFileName){
    if(newFileName.indexOf("&") != -1){
      //newFileName = newFileName.replace("&","And")
      newFileName = newFileName.replace(/&/g,"And");
     }
 }
 return newFileName;
}


function onClickEmailTnC(tnckeyword) {
	//call notification add service for email with the tnc keyword
	//alert("tnckeyword::"+tnckeyword);
	var inputparam = {};
	inputparam["channelName"] = "Internet Banking";
	inputparam["channelID"] = "01";
	inputparam["notificationType"] = "Email"; // always email
	inputparam["phoneNumber"] = gblPHONENUMBER;
	inputparam["mail"] = gblEmailId;
	inputparam["customerName"] = gblCustomerName;
	inputparam["localeCd"] = kony.i18n.getCurrentLocale();
	if(tnckeyword == undefined || tnckeyword == "") {
		inputparam["moduleKey"] = "TermsAndConditions";
	} else if(gblActionCode == "12" ||gblActionCode == "13"){
	inputparam["moduleKey"] = "TermsAndConditionsReactivation";
	}else {
		inputparam["moduleKey"] = tnckeyword;
	}
	
	invokeServiceSecureAsync("TCEMailService", inputparam, callBackNotificationAddService);
}


/*
************************************************************************
            Name    : showLoadingScreen 
            Author  : Developer
            Date    : May 3, 2013
            Purpose : On click of Next button or service call button
        	Input params: na
        	Output params: Blocks UI
        	ServiceCalls: nil
        	Called From : According need 
************************************************************************
 */
 
function showLoadingScreen() {
  gblIsLoading = true;
  if (gblDeviceInfo.name == "thinclient")
        if (document.getElementById("__loadingScreenDiv") != null)
            if (document.getElementById("__loadingScreenDiv").style['cssText'].length != 76) { //to prevent loading screen from being called again and again
                kony.application.showLoadingScreen("frmLoading", "", constants.LOADING_SCREEN_POSITION_FULL_SCREEN, true, false, null);
            } else {
                kony.print("Loading indicator is already shown");
            } else
        kony.application.showLoadingScreen("frmLoading", "", constants.LOADING_SCREEN_POSITION_FULL_SCREEN, true, false, null);

    else if (gblDeviceInfo.name == "android") {
        kony.application.showLoadingScreen("frmLoading", "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
    } else
        kony.application.showLoadingScreen("frmLoading", "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, {
            shouldShowLabelInBottom: "false",
            separatorHeight: 10
        });
}

function showLoadingScreenGif()
{
  var currForm=kony.application.getCurrentForm();
  if(currForm.id=="frmMFCompleteMB")
  {
    frmMFCompleteMB.flexLoadinGif.setVisibility(true);
  }
  else if(currForm.id=="frmMFcancelOrderToProcess")
  {
    frmMFcancelOrderToProcess.flexLoadinGif.setVisibility(true);
  }
}

function showLoadingScreenGifSuccess()
{
  try{
  	kony.timer.cancel("animtimer");
  }
  catch(e){
  }
  kony.timer.schedule("animtimer", callbackanimtimer, 6, false)
  
}

function callbackanimtimer()
{
  var currForm=kony.application.getCurrentForm();
  if(currForm.id=="frmMFCompleteMB")
  {
    frmMFCompleteMB.flexLoadinGif.setVisibility(false);
  }else if(currForm.id=="frmMFcancelOrderToProcess")
  {
    frmMFcancelOrderToProcess.flexLoadinGif.setVisibility(false);
  }
}

function dismissLoadingScreenGif()
{
  try{
  	kony.timer.cancel("animtimer");
  }
  catch(e){
  }
  kony.timer.schedule("animtimer", callbackanimtimer, 6, false)
  /*var currForm=kony.application.getCurrentForm();
  if(currForm.id=="frmMFConfirmMB")
  {
    frmMFConfirmMB.flexLoadinGif.setVisibility(false);
  }*/
  
}
/*
************************************************************************
            Name    : dismissLoadingScreen
            Author  : Developer
            Date    : May 3, 2013
            Purpose : to dismiss the loading screen before loading next screen
        	Input params: na
        	Output params: dismiss the loading screen
        	ServiceCalls: nil
        	Called From : According need 
************************************************************************
 */

function dismissLoadingScreen() {
  	gblIsLoading = false;
	if(gblDeviceInfo["category"]=="IE" && gblDeviceInfo["version"]=="8"){
			popupIBLoading.dismiss();
	}else{
		kony.application.dismissLoadingScreen();
	}
}
/*
************************************************************************

	Module	: amountValidation
	Author  : Kony
	Purpose : Checking if the enter data contains only "0","1", "2", "3", "4", "5", "6", "7", "8", "9", ",", "."

************************************************************************
 */

function amountValidation(amount) {
	if (amount != null && amount != "") {
		if (kony.string.containsOnlyGivenChars(amount, ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", ",", "."])) {
			amount = amount.replace(",", "");
			amount = amount.replace(".", "");
			//ADDED os.tonumber(amount)== nil TO CHECK IF THE USER ONLY ENTER "." or ","
			if (kony.os.toNumber(amount) <= 0 || kony.os.toNumber(amount) == null) {
				return false;
			} else
				return true;
		} else
			return false;
	} else
		return false;
}

/*
************************************************************************

	Module	: amountFormatting
	Author  : Azure Mayank
	Purpose : Formatting the value of amount.

************************************************************************
 */

function amountFormat(dataAmount) {
    try {
		var amount = dataAmount.toString();
        kony.print("inside amountFormat 1");
        var newAmount = "0", newDecimalAmount = ".00" + kony.i18n.getLocalizedString("currencyThaiBaht");
        kony.print("inside amountFormat 2");
        if (amount != null && amount != "" && amount != "0.00" && amount != "undefined") {
            kony.print("inside amountFormat 3");
            amount = commaFormatted(amount);
            var splitAmount = amount.split('.', 2);
            kony.print("inside amountFormat 4");
            newAmount = splitAmount[0];
            kony.print("inside amountFormat 5");
            newDecimalAmount = "." + ((typeof(splitAmount[1]) == "undefined") ? "00" : splitAmount[1]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
            kony.print("inside amountFormat6");
        }
        var formattedAmount = [newAmount, newDecimalAmount];
        kony.print("formattedAmount = " + JSON.stringify(formattedAmount));
        return formattedAmount;
    } catch (e) {
        kony.print("amountFormat error:" + e.message);
    }
}

function Formatdecimalamount(dataAmount) {
    try {
		var amount = dataAmount.toString();
        kony.print("inside amountFormat 1");
        var newAmount = "0", newDecimalAmount = ".00" + kony.i18n.getLocalizedString("currencyThaiBaht");
        kony.print("inside amountFormat 2");
        if (amount != null && amount != "" && amount != "0.00" && amount != "undefined") {
            kony.print("inside amountFormat 3");
            var splitAmount = amount.split('.', 2);
            kony.print("inside amountFormat 4  "+splitAmount);
            newAmount = splitAmount[0];
            kony.print("inside amountFormat 5  "+newAmount);
            newDecimalAmount = "." + ((typeof(splitAmount[1]) == "undefined") ? "00" : splitAmount[1]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
            kony.print("inside amountFormat6  "+newDecimalAmount);
        }
        var formattedAmount = newAmount+newDecimalAmount;
        kony.print("formattedAmount = " + formattedAmount);
        return formattedAmount;
    } catch (e) {
        kony.print("amountFormat error:" + e.message);
    }
}
/*
************************************************************************

	Module	: showAlert
	Author  : Kony
	Purpose : Common alert (AlertMessage,Alert type,Alert Title,Yes Label, No Label,Alert Handler)

****/

function showAlertForSplitTransfers(alertMsg, type, title, yesLbl, noLbl, callBackHandler) {
	var basicConf = {
		message: alertMsg,
		alertType: type,
		alertTitle: title,
		yesLabel: yesLbl,
		noLabel: noLbl,
		alertHandler: callBackHandler
	};
	//Defining pspConf parameter for alert
	var pspConf = {};
	kony.ui.Alert(basicConf, pspConf);
}
/*
************************************************************************

	Module	: commaFormatted
	Author  : Sujata
	Purpose : It will format amount with comma.

****/

function commaFormatted(amount) {
	
	var minus = '';
	
	if(amount == "" || amount == null || amount == undefined)
       return "";

	
	if(amount.indexOf(".")==0){
		amount="0"+amount;
	}
	amount = amount.concat(".00")
	
	if(parseFloat(amount) < 0) {
		minus = '-';
	}
	
	var delimiter = ","; // replace comma if desired
	amount = new String(amount);
	var a = amount.split('.', 2)
	var d = a[1];
	if(d.length==1){
		d=d+"0"
	}
	var i = parseInt(a[0]);
	if (isNaN(i)) {
		return '';
	}
	
	i = Math.abs(i);
	var n = new String(i);
	var a = [];
	while (n.length > 3) {
		var nn = n.substr(n.length - 3);
		a.unshift(nn);
		n = n.substr(0, n.length - 3);
	}
	if (n.length > 0) {
		a.unshift(n);
	}
	n = a.join(delimiter);
	if (d.length < 1) {
		amount = n;
	} else {
		amount = n + '.' + d;
	}
	amount = minus + amount;
	return amount;
}

/*
************************************************************************
            Name    : removeMenu()
            Author  : Shubhanshu Yadav
            Date    : June 07, 2013
            Purpose : To remove the dynamic menu on hide of the form.
        Input params: Nil
       Output params: Nil
        ServiceCalls: nil
        Called From : onHide of the form
************************************************************************
 */

function removeMenu() {
	/*if(!flowSpa){
	var currentForm = kony.application.getCurrentForm();
	if ((isMenuRendered == true) || (isMenuShown == true)) {
		while (currentForm.vbox47407564342877.widgets()
			.length !== 0) {
			currentForm.vbox47407564342877.removeAt(0);
		}
		currentForm.vboxLeft.removeAt(0);
	}
	}*/
}
/*
************************************************************************
	Module	: financialActivityLogServiceCall
	Author  : Kony
	Purpose : Calling the financial activity log java service 
************************************************************************/

function financialActivityLogServiceCall(CRMFinancialActivityLogAddRequest) {
	inputParam = {};
	//inputParam["ipAddress"] = ""; 
	inputParam["crmId"] = CRMFinancialActivityLogAddRequest.crmId;
	inputParam["finTxnRefId"] = CRMFinancialActivityLogAddRequest.finTxnRefId;
	inputParam["finTxnDate"] = CRMFinancialActivityLogAddRequest.finTxnDate;
	inputParam["activityTypeId"] = CRMFinancialActivityLogAddRequest.activityTypeId;
	inputParam["txnCd"] = CRMFinancialActivityLogAddRequest.txnCd;
	inputParam["tellerId"] = CRMFinancialActivityLogAddRequest.tellerId;
	inputParam["txnDescription"] = CRMFinancialActivityLogAddRequest.txnDescription;
	inputParam["finLinkageId"] = CRMFinancialActivityLogAddRequest.finLinkageId;
	inputParam["channelId"] = CRMFinancialActivityLogAddRequest.channelId;
	inputParam["fromAcctId"] = CRMFinancialActivityLogAddRequest.fromAcctId;
	inputParam["toAcctId"] = CRMFinancialActivityLogAddRequest.toAcctId;
	inputParam["toBankAcctCd"] = CRMFinancialActivityLogAddRequest.toBankAcctCd;
	inputParam["finTxnAmount"] = CRMFinancialActivityLogAddRequest.finTxnAmount;
	inputParam["finTxnFee"] = CRMFinancialActivityLogAddRequest.finTxnFee;
	inputParam["finTxnBalance"] = CRMFinancialActivityLogAddRequest.finTxnBalance;
	inputParam["finTxnMemo"] = CRMFinancialActivityLogAddRequest.finTxnMemo;
	inputParam["billerRef1"] = CRMFinancialActivityLogAddRequest.billerRef1;
	inputParam["billerRef2"] = CRMFinancialActivityLogAddRequest.billerRef2;
	inputParam["noteToRecipient"] = CRMFinancialActivityLogAddRequest.noteToRecipient;
	inputParam["recipientMobile"] = CRMFinancialActivityLogAddRequest.recipientMobile;
	inputParam["recipientEmail"] = CRMFinancialActivityLogAddRequest.recipientEmail;
	inputParam["finTxnStatus"] = CRMFinancialActivityLogAddRequest.finTxnStatus;
	inputParam["smartFlag"] = CRMFinancialActivityLogAddRequest.smartFlag;
	inputParam["clearingStatus"] = CRMFinancialActivityLogAddRequest.clearingStatus;
	inputParam["finSchduleRefId"] = CRMFinancialActivityLogAddRequest.finSchduleRefId;
	inputParam["billerCommCode"] = CRMFinancialActivityLogAddRequest.billerCommCode;
	inputParam["fromAcctName"] = CRMFinancialActivityLogAddRequest.fromAcctName;
	inputParam["fromAcctNickname"] = CRMFinancialActivityLogAddRequest.fromAcctNickname;
	inputParam["toAcctName"] = CRMFinancialActivityLogAddRequest.toAcctName;
	inputParam["toAcctNickname"] = CRMFinancialActivityLogAddRequest.toAcctNickname;
	inputParam["billerCustomerName"] = CRMFinancialActivityLogAddRequest.billerCustomerName;
	inputParam["billerBalance"] = CRMFinancialActivityLogAddRequest.billerBalance;
	inputParam["activityRefId"] = CRMFinancialActivityLogAddRequest.activityRefId;
	inputParam["eventId"] = CRMFinancialActivityLogAddRequest.eventId;
	inputParam["errorCd"] = CRMFinancialActivityLogAddRequest.errorCd;
	inputParam["txnType"] = CRMFinancialActivityLogAddRequest.txnType;
	inputParam["tdInterestAmount"] = CRMFinancialActivityLogAddRequest.tdInterestAmount;
	inputParam["tdTaxAmount"] = CRMFinancialActivityLogAddRequest.tdTaxAmount;
	inputParam["tdPenaltyAmount"] = CRMFinancialActivityLogAddRequest.tdPenaltyAmount;
	inputParam["tdNetAmount"] = CRMFinancialActivityLogAddRequest.tdNetAmount;
	inputParam["tdMaturityDate"] = CRMFinancialActivityLogAddRequest.tdMaturityDate;
	inputParam["remainingFreeTxn"] = CRMFinancialActivityLogAddRequest.remainingFreeTxn;
	inputParam["toAccountBalance"] = CRMFinancialActivityLogAddRequest.toAccountBalance;
	inputParam["sendToSavePoint"] = CRMFinancialActivityLogAddRequest.sendToSavePoint;
	inputParam["openTdTerm"] = CRMFinancialActivityLogAddRequest.openTdTerm;
	inputParam["openTdInterestRate"] = CRMFinancialActivityLogAddRequest.openTdInterestRate;
	inputParam["openTdMaturityDate"] = CRMFinancialActivityLogAddRequest.openTdMaturityDate;
	inputParam["affiliatedAcctNickname"] = CRMFinancialActivityLogAddRequest.affiliatedAcctNickname;
	inputParam["affiliatedAcctId"] = CRMFinancialActivityLogAddRequest.affiliatedAcctId;
	inputParam["beneficialFirstname"] = CRMFinancialActivityLogAddRequest.beneficialFirstname;
	inputParam["beneficialLastname"] = CRMFinancialActivityLogAddRequest.beneficialLastname;
	inputParam["relationship"] = CRMFinancialActivityLogAddRequest.relationship;
	inputParam["percentage"] = CRMFinancialActivityLogAddRequest.percentage;
	inputParam["finFlexValues1"] = CRMFinancialActivityLogAddRequest.finFlexValues1;
	inputParam["finFlexValues2"] = CRMFinancialActivityLogAddRequest.finFlexValues2;
	inputParam["finFlexValues3"] = CRMFinancialActivityLogAddRequest.finFlexValues3;
	inputParam["finFlexValues4"] = CRMFinancialActivityLogAddRequest.finFlexValues4;
	inputParam["finFlexValues5"] = CRMFinancialActivityLogAddRequest.finFlexValues5;
	inputParam["dueDate"] = CRMFinancialActivityLogAddRequest.dueDate;
	inputParam["beepAndBillTxnId"] = CRMFinancialActivityLogAddRequest.beepAndBillTxnId;
	inputParam["openProdCode"] = CRMFinancialActivityLogAddRequest.openProdCode;
	invokeServiceSecureAsync("financialActivityLog", inputParam, callBackFinancialActivityLog)
}
/*
************************************************************************

	Module	: FianncialActivityLogServiceCall
	Author  : Kony
	Purpose : Calling the financial activity log java service

************************************************************************/

function callBackFinancialActivityLog(status, resulttable) {
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			
		}
	}
}
/*
************************************************************************

	Module	: activityLogServiceCall
	Author  : Kony
	Purpose : Calling the activity log java service and sending the ipAddress,activityDate,appSessionID from preprocessor

************************************************************************/

function activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1,
	activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId) {
	
}

/**
 * Author  : Nishant Kumar
 * Date    : May 05, 2013
 * Purpose : sorting array based on given key
 * Input params: array, key
 * Output params: No
 * ServiceCalls: No
 * Called From : utility
 */

function sortByKey(array, key) {
	return array.sort(function (a, b) {
		var x = a[key];
		var y = b[key];
		return ((x < y) ? -1 : ((x > y) ? 1 : 0));
	});
}

/**
 * description
 * @returns {}
 */

function setFocusOnTB() {
	switch (gblTxtFocusFlag) {
	case 1:
		frmMBsetPasswd.txtTempAccess.setFocus(true);
		gblTxtFocusFlag = 2;
		break;
	case 2:
		frmMBsetPasswd.txtAccessPwd.setFocus(true);
		gblTxtFocusFlag = 1;
		break;
	case 3:
		frmMBsetPasswd.txtTemp.setFocus(true);
		gblTxtFocusFlag = 4;
		break;
	case 4:
		frmMBsetPasswd.txtTransPass.setFocus(true);
		gblTxtFocusFlag = 3;
		break;
	}
}

function disMissAllPopUps() {
	AccntTransPwd.dismiss();
	BillerNotAvailable.dismiss();
	BillPayTopUpConf.dismiss();
	DeletePop.dismiss();
	helpMessage.dismiss();
	popAccessPinBubble.dismiss();
	popAccntConfirmation.dismiss();
	popAddrCmboBox.dismiss();
	popBankList.dismiss();
	popBPTransactionPwd.dismiss();
	popBubble.dismiss();
	popDelRecipient.dismiss();
	popDelRecipientProfile.dismiss();
	popDelTopUp.dismiss();
	popForgotPass.dismiss();
	popProfilePic.dismiss();
	popRecipientBankList.dismiss();
	popTransactionPwd.dismiss();
	popTransferConfirmOTPLock.dismiss();
	popTransfersTDAccount.dismiss();
	popupBubblePasswordRules.dismiss();
	popupBubbleUserId.dismiss();
	popupConfirmation.dismiss();
	popupConfrmYes.dismiss();
	popUpLogout.dismiss();
	popUpMyBillers.dismiss();
	popUpTermination.dismiss();
	popupTractPwd.dismiss();
	popupActivationHelp.dismiss();
	popAllowOrNot.dismiss();
	popATMBranch.dismiss();
	popDreamSaving.dismiss();
	popEditBillPayTransactionPwd.dismiss();
	popExgRateDisclaimer.dismiss();
	popFeedBack.dismiss();
	popHelpMessageTrans.dismiss();
	popInboxSortBy.dismiss();
	popInboxSortBy2.dismiss();
	popOtpSpa.dismiss();
	popPromotions.dismiss();
	popS2SAmntHelp.dismiss();
	popSelDate.dismiss();
	popUpCallCancel.dismiss();
	popupEditBPDele.dismiss();
	popupFTdel.dismiss();
	popUploadPic.dismiss();
	popUploadPicSpa.dismiss();
	popUpNotfnDelete.dismiss();
	//popUpPayBill.dismiss();
	popUpProvinceData.dismiss();
	popupSpaForgotPassword.dismiss();
	popUpTopUpAmount.dismiss();
	//TAGCPServerDetailsPopup.dismiss();
	TMBCallPopup.dismiss();
	popUpTouchIdTransPwd.dismiss();
	popupAddToMyRecipient.dismiss();
    popupAddToMyBills.dismiss();
    popGeneralMsg.dismiss();
}


function showLoadingPopUpScreen() {
	//kony.application.showLoadingScreen(frmLoading, "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
	//#ifdef android
		kony.application.showLoadingScreen("frmLoading", "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, {
			transparencybehindloadingscreen: 20
		});
	//else if (kony.os.deviceInfo().name != "iPhone")
	//#else
		//#ifndef iphone
			kony.application.showLoadingScreen("frmLoading", "", constants.LOADING_SCREEN_POSITION_FULL_SCREEN, true, false, null);
		//else
		//#else
		kony.application.showLoadingScreen("frmLoading", "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, {
			shouldShowLabelInBottom: "false",
			separatorHeight: 10
		});
		//#endif
	//#endif
}
/*
************************************************************************
            Name    : emailProcessForTC()
            Author  : Dileep Somajohassula
            Date    : August 22, 2013
            Purpose : To trigger a mail when first time activations is done
        ServiceCalls: partyInquiry
        Called From : End of Device activation flow 
************************************************************************
 */

function emailProcessForTC() {
	
	
	if (gblEmailTCSelect && (gblActionCode != "22")){
		if (gblTCEmailTriggerFlag) {
			
			showLoadingScreen();
			var inputparam = {}
			
			invokeServiceSecureAsync("partyInquiry", inputparam, FstTmeActivatnPartySrviceCallback);
		}
	}
}

function FstTmeActivatnPartySrviceCallback(status, resulttable) {
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			showLoadingScreen();
			var inputparam = {};		
			gblCustomerName = resulttable["customerName"];
			//gblEmailId = resulttable["emailId"];
			inputparam["channelName"] = "Mobile Banking";
			inputparam["channelID"] = "02";
			inputparam["notificationType"] = "Email"; // always email
			inputparam["phoneNumber"] = gblPHONENUMBER;
			inputparam["mail"] = gblEmailId;
			inputparam["customerName"] = gblCustomerName;
			inputparam["localeCd"] = kony.i18n.getCurrentLocale();
			//if(tnckeyword == undefined || tnckeyword == "") {
			if(gblActionCode == "12" ||gblActionCode == "13"){
			inputparam["moduleKey"] = "TermsAndConditionsReactivation";
			}else if(gblActionCode == "23"){
			inputparam["moduleKey"] = "TermsAndConditionsAddDevice";
			}
			else{
				inputparam["moduleKey"] = "TermsAndConditions";
			}
			
			invokeServiceSecureAsync("TCEMailService", inputparam, callBackNotificationAddMBForMail)
		}
		dismissLoadingScreen();
	}
	dismissLoadingScreen();
}

function callBackNotificationAddMBForMail(status, resulttable) {
	
	if (status == 400) {
		
		
		if (resulttable["opstatus"] == 0) {
			
			dismissLoadingScreen();
		}
		dismissLoadingScreen();
	}
	dismissLoadingScreen();
}

function showLoadingScreenWithNoIndicator() {
	if (gblDeviceInfo.name == "thinclient"){
		kony.application.showLoadingScreen("frmLoadingNoIndicator", "", constants.LOADING_SCREEN_POSITION_FULL_SCREEN, true, false, null);
		}
	else if (gblDeviceInfo.name == "android"){
		kony.application.showLoadingScreen("frmLoadingNoIndicator", "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, false, null);
		}
	else
		kony.application.showLoadingScreen("frmLoadingNoIndicator", "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, false, {
			shouldShowLabelInBottom: "false",
			separatorHeight: 10
		});
}

function loggingDeviceStoreData() {
      	var inputParams = {};
		inputParams["methodId"] = "logDeviceStoreDataNew";
		inputParams["storeData"] = "";
		var status = invokeServiceSecureAsync("logDeviceStoreDataNew", inputParams, callbackLogDeviceStoreData);
}

function callbackLogDeviceStoreData(status, resulttable) {
	if (status == 400) {
		//for MENU Offers Flag
      	if (undefined != resulttable["unreadMyOffers"] && null != resulttable["unreadMyOffers"]) {
            //unreadMyOffers is Y means user has new offers
            //alert("unreadMyOffers : "+resulttable["unreadMyOffers"]);
            if ("Y" == resulttable["unreadMyOffers"]) {
                gblIsNewOffersExists = true;
            }
        }
      	if (undefined != resulttable["statusInboxUnread"] && null != resulttable["statusInboxUnread"]) {
            if (resulttable["statusInboxUnread"] == "1") {
                gblUnreadCount = "0";
            } else if (resulttable["statusInboxUnread"] == "0") {
              	if (undefined != resulttable["unreadInboxUnread"] && null != resulttable["unreadInboxUnread"]) {
                   gblUnreadCount = resulttable["unreadInboxUnread"];
                }else{
                  gblUnreadCount = "0";
                }               
            } else {
                gblUnreadCount = "0";
            }
        }
      
      	if (undefined != resulttable["statusMessagesUnread"] && null != resulttable["statusMessagesUnread"]){
            if (resulttable["statusMessagesUnread"] == "1") {
                gblMessageCount = "0";
            } else if (resulttable["statusMessagesUnread"] == "0") {
              	if (undefined != resulttable["messagesUnread"] && null != resulttable["messagesUnread"]) {
                	gblMessageCount = resulttable["messagesUnread"];
                }else{
                  	gblMessageCount = "0";
                }
                
            } else {
                gblMessageCount = "0";
            }
        }
      	/*gblRTPBadgeCount = resulttable["RTPTotalRecordsCount"];
      	kony.print("@@rtpcountnew123@@@"+gblRTPBadgeCount);
        setRTPBadgeCountNew();*/
        var badgeCount = parseInt(gblUnreadCount) + parseInt(gblMessageCount);
        gblMyInboxTotalCountMB = badgeCount;
        if (kony.string.equalsIgnoreCase("iphone", gblDeviceInfo.name) || kony.string.equalsIgnoreCase("iPad", gblDeviceInfo.name) || kony.string.equalsIgnoreCase("iPod touch", gblDeviceInfo.name)) {
        	kony.application.setApplicationBadgeValue(badgeCount);
        }
        //Commenting below code as FATCASkipCounter is not used anywhere
        /*if (undefined != resulttable["FATCASkipCounter"] && null != resulttable["FATCASkipCounter"]){
        	gblFATCASkipCounter = parseInt(resulttable["FATCASkipCounter"]);
        } */
         
        //Assigning RTP Badge count to Global variable.
         /* gblRTPBadgeCount = resulttable["RTPTotalRecordsCount"];
          setRTPBadgeCount(); */
      	
	} else {
		
	}
}


function printTermsNConditions() {
	 var tncTitle = kony.i18n.getLocalizedString("keyTermsNConditions");
	 if(tncTitle=="Terms & Conditions")
	 {
	 	tncTitle = "TermsandConditions";
	 }
	 
	 var tncContent = "";
	 
	 if(kony.application.getCurrentForm().id == "frmIBMigratedUserStep1"){
	  tncContent = frmIBMigratedUserStep1.lblTnC.text;
	 }else{
	  tncContent = frmIBActivationTandC.lblTnC.text;
	 }
	
	 showPopup(tncTitle, tncContent);
}
//function printDiv(title, control) {
//	 showPopup($(title, control).html());
//}

function showPopup(title, data) {
	 var mywindow = window.open('', title, 'height=400,width=600');
	 mywindow.document.write('<html><head><title>Terms & Conditions</title>');
	 /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
	 mywindow.document.write('</head><body><center>' + title + '</center><font><div overflow-y: auto; overflow-x: hidden; margin: 0%; padding: 0%; height: 328px;>');
	 mywindow.document.write(data);
	 mywindow.document.write('</div></font></body></html>');
	 mywindow.document.close();
	 mywindow.focus();
	 mywindow.print();
	 mywindow.close();
	 return true;
}

var gblFBLoginSameThreadTrack = false;

function postOnWall() {
		var prevform = kony.application.getPreviousForm();
		var currform = kony.application.getCurrentForm();
		//gblcalled=false;
		gblPOWstateTrack=false;
		if(gblFBCode == null || gblFBCode==""){
			if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPod touch" || gblDeviceInfo["name"] == "android" || flowSpa==true){
					if(gblPOWstateTrack == true){
						prevform.show();
						//gblPOWstateTrack = false;
					}
					else{
						invokeFacebookSetUp();
					}
				}
				else{
					if(gblFBIBStateTrack == true){
						prevform.show();
					}
					else{
						startIBFBPOWService();
						//gblFBLoginSameThreadTrack = true;
					}
				}
		
		}else{
				showLoadingScreen();
				var inputParam = {}
				inputParam["locale"] = kony.i18n.getCurrentLocale();
				inputParam["custNME"] = gblPOWcustNME;
				inputParam["transXN"] = gblPOWtransXN;
				inputParam["channel"] = gblPOWchannel;
				invokeServiceSecureAsync("fbpostmessageonwall", inputParam, postOnWallCallback);
				if(gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPod touch" || gblDeviceInfo["name"] == "android"){
					//MB does not require same thread execution of frmFBLogin.show()
				}
				else{
					/*
					var myVar = setTimeout(function(){if(gblFBLoginSameThreadTrack == true)clearTimeout(myVar);},18000);
					if(gblFBLoginSameThreadTrack == true)
						startIBFBPOWService();
					gblFBLoginSameThreadTrack = false;
					*/
				}
			}
	}


function postOnWallAfter() {
	showLoadingScreen();
	var inputParam = {}
	inputParam["locale"] = kony.i18n.getCurrentLocale();
	inputParam["custNME"] = gblPOWcustNME;
	inputParam["transXN"] = gblPOWtransXN;
	inputParam["channel"] = gblPOWchannel;
	var fbinputs={};
	invokeServiceSecureAsync("getfbUserCode", fbinputs, getfbcodecalback);
	invokeServiceSecureAsync("fbpostmessageonwall", inputParam, postOnWallCallback);
	if(gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPod touch" || gblDeviceInfo["name"] == "android"){
		//MB does not require same thread execution of frmFBLogin.show()
	}
	else{
		/*
		var myVar = setTimeout(function(){if(gblFBLoginSameThreadTrack == true)clearTimeout(myVar);},18000);
		if(gblFBLoginSameThreadTrack == true)
			startIBFBPOWService();
		gblFBLoginSameThreadTrack = false;
		*/
	}
}

function postOnWallCallback(status, getFriends){
	
	dismissLoadingScreen();
	if(status == 400)
	{
		//gblFBCode=getFriends["facebookCode"];
		var prevform = kony.application.getPreviousForm();
		var currform = kony.application.getCurrentForm();
		if(getFriends["invokeServlet"] == "yes"){
			if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPod touch" || gblDeviceInfo["name"] == "android" || flowSpa==true){
				if(gblPOWstateTrack == true){
					prevform.show();
					//gblPOWstateTrack = false;
				}
				else{
					invokeFacebookSetUp();
				}
			}
			else{
				if(gblFBIBStateTrack == true){
					prevform.show();
				}
				else{
					startIBFBPOWService();
					//gblFBLoginSameThreadTrack = true;
				}
			}
		}
		else{
			if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPod touch" || gblDeviceInfo["name"] == "android" || flowSpa==true){
				if(gblPOWstateTrack == true)
					prevform.show();
				else
					currform.show();
			}
			else{
				if(gblFBIBStateTrack == true){
					prevform.show();
				}
				else{
					//DO NOTHING
				}
			}
			//if(getFriends["status"] == "success"){
			//	showAlertRcMB(kony.i18n.getLocalizedString("keyPOWSuccess"), kony.i18n.getLocalizedString("info"), "info")
			//}
			//else if(getFriends["status"] == "fail"){
			//	showAlertRcMB(kony.i18n.getLocalizedString("keyPOWFail"), kony.i18n.getLocalizedString("info"), "info")
			//}
			if(getFriends["status"] == "success"){
				showAlertRcMB(kony.i18n.getLocalizedString("keyPOWSuccess"), kony.i18n.getLocalizedString("info"), "info")
			}
			else if(getFriends["status"] == "authenticationerror"){
				showAlertRcMB(kony.i18n.getLocalizedString("keyPOWFail"), kony.i18n.getLocalizedString("info"), "info")
			}
			else if(getFriends["status"] == "duplicate"){
				showAlertRcMB(kony.i18n.getLocalizedString("keyPOWDuplicate"), kony.i18n.getLocalizedString("info"), "info")
				//showAlertRcMB("This status update is identical to the last one you posted. Try posting something different, or delete your previous update.", kony.i18n.getLocalizedString("info"), "info")
			}
		}
		gblPOWstateTrack = false;
		gblFBIBStateTrack = false;
	}
	
}

/**
 * @function
 *
 * @param status 
 * @param resulttable 
 */
function mbRenderFileCallbackfunction(status, resulttable) {
    
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            var filePath = resulttable["ouputfilepath"];
            var fileType = resulttable["filetype"];
            var outputtemplatename = resulttable["outputtemplatename"];
            //  var url ="https://dev.tau2904.com:443/middleware/RenderGenericPdfImageServlet";
            url = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/RenderGenericPdfImageServlet";
            var  uriquery = "filename="+encodeURIComponent(filePath)+"&filetype="+encodeURIComponent(fileType)+"&outputtemplatename="+encodeURIComponent(outputtemplatename);
            var totalURL = url + "?" + uriquery;
            if(kony.application.getCurrentForm() == frmAccountStatementMB){
		   	 	var accId=gblAccountTable["custAcctRec"][gblIndex]["accId"];
	            var activitytypeId;
		   	 	if(fileType == "pdf"){
		   	 		activitytypeId="084";
		   	 	}
		   	 		//activityLogServiceCall(activitytypeId, "", "01", "", accId, frmAccountStatementMB.lblname.text, gaccType, "", "", "");
	   	 	}
            if(flowSpa && (gblDeviceInfo.category == "android")) 
            {
           
            	
	            try {
		            var cwForm = document.createElement("form");
		        	cwForm.setAttribute('method',"post");
		        	cwForm.setAttribute('action',totalURL);
		        	if(document.getElementsByTagName('body')!= null && document.getElementsByTagName('body')[0]!= null)
		        	{
		            	document.getElementsByTagName('body')[0].appendChild(cwForm);
		        	}
		        	kony.application.dismissLoadingScreen();            
		        	cwForm.submit();
	    		} catch(ex){
	       			
	    		}
	    		
	    		//kony.application.openURL(totalURL);
    		} else {
	    		if ((gblDeviceInfo.name == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPod touch") && fileType == "png")
				{
					var myCustomString = (kony.i18n.getLocalizedString("keySaveasImage") + "#!(#&)!" + kony.i18n.getLocalizedString("keySaveImageMessage") + "#!(#&)!" + GLOBAL_KONY_IDLE_TIMEOUT + "#!(#&)!" + totalURL );
					saveImage.save(myCustomString);
	 			}else if(gblDeviceInfo.name == "android"){
	 			
	 			//Calling Android ffi funcion for downloading pdf or image file.
	 			if(KonyDMObject == null){
	 			KonyDMObject = new KonyandroidUtils.KonyDM();
	 			}
	 			if(fileType == "png"){
	 				androidFileType = "png";
	 			}else{
	 				androidFileType = "pdf";
	 			}
	 			showLoadingScreen();
	 			KonyDMObject.downloadFile(
					/**String*/ totalURL, 
					/**Function*/ callbackDownload);
	 			
	 			} 	 			
	 			else {
					dismissLoadingScreenPopup();
	    			kony.application.openURL(totalURL);
	    		}
    		}

        }
    }
}

function callbackDownload(result) {
    dismissLoadingScreen();
    var downloadStr = "";
    if (androidFileType == "pdf") {
        downloadStr = "" + kony.i18n.getLocalizedString("keySavePDFMessage");
         dismissLoadingScreen();
         showAlertWithCallBack(downloadStr, kony.i18n.getLocalizedString("info"), removeSpecificImgFromServer);
        // alert(downloadStr);
       // showAlert(downloadStr, kony.i18n.getLocalizedString("info"));
    } else if (androidFileType == "png") {
        downloadStr = "" + kony.i18n.getLocalizedString("keySaveImageMessage");
        dismissLoadingScreen();
        //showAlert(downloadStr, kony.i18n.getLocalizedString("info"));
        showAlertWithCallBack(downloadStr, kony.i18n.getLocalizedString("info"), removeSpecificImgFromServer);
        //invokeServiceSecureAsync("removeImgFrmServerTmp", inputParam, callBackRemoveImg)
    } else {
        downloadStr = "" + kony.i18n.getLocalizedString("keySaveImageMessage");
        //	alert(downloadStr);
        showAlert(downloadStr, kony.i18n.getLocalizedString("info"));
    }
}
function removeSpecificImgFromServer() {
    //alert("isSignedUser : "+isSignedUser);
    if(isSignedUser){
    	var inputParam = {};
    	invokeServiceSecureAsync("removeSpecificImgFromServer", inputParam, callBackRemoveImg);
    }   
}
function callBackRemoveImg() {
}
function ibRenderFileCallbackfunction(status, resulttable) {
    
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            var filePath = resulttable["ouputfilepath"];
            var fileType = resulttable["filetype"];
            var outputtemplatename = resulttable["outputtemplatename"];
            var url = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/RenderGenericPdfImageServlet";
            var uriquery = "filename="+encodeURIComponent(filePath)+"&filetype="+encodeURIComponent(fileType)+"&outputtemplatename="+encodeURIComponent(outputtemplatename);
            var finalURL = url + "?" + uriquery;
            dismissLoadingScreenPopup();
             if(kony.application.getCurrentForm() == frmIBAccntFullStatement){
		   	 	var accId=gblAccountTable["custAcctRec"][gblIndex]["accId"];
		   	 	var activitytypeId;
		   	 	if(fileType == "pdf"){
		   	 		activitytypeId="084";
		   	 	}else if(fileType == "csv"){
		   	 		activitytypeId="085";
		   	 	}
		   	 	//	activityLogServiceCall(activitytypeId, "", "01", "", accId,
	 			//	frmIBAccntFullStatement.lblname.text, gaccType, "", "", "");
		   	 }
		   	 
		
	   	 if(fileType == "print"){
    		kony.application.openURL(finalURL);
    		
	   	 } else 
	   	 {
	   	 	if((navigator.userAgent.match(/iPhone/i)) ||  
				(navigator.userAgent.match(/iPad/i)) || 
				(navigator.userAgent.match(/iPod/i))) 
	   	 	{
	   	 		kony.application.openURL(finalURL);
	   	 		
	   	 	}
	   	 	else
	   	 	{
	            try {
		            var cwForm = document.createElement("form");
		        	cwForm.setAttribute('method',"post");
		        	cwForm.setAttribute('action',finalURL);
		        	
		        	//cwForm.setAttribute("accept", "application/pdf");	        	
		        	//cwForm.setAttribute("enctype", "multipart/form-data");        	
		        	
		        	
		        	if(document.getElementsByTagName('body')!= null && document.getElementsByTagName('body')[0]!= null)
		        	{
		        	   document.getElementsByTagName('body')[0].appendChild(cwForm);		        	   
		        	}
		        	kony.application.dismissLoadingScreen();		        		        	         
		        	cwForm.submit();

                 
                 
	    		} catch(ex){
	       			
	    		} 
    		}// end try catch 
	   	 } // for pdf and img 
    		
        } else {// END : opstatus check
			dismissLoadingScreenPopup();
		}
    } // END : status check end
}

function showAlertForUniqueGenFail(message)
{
	 var alert_seq0_act1 = kony.ui.Alert({
	        "message":message,
	        "alertType": constants.ALERT_TYPE_INFO,
	        "alertTitle": "",
	        "yesLabel": "Close",
	        "noLabel": "",
	        "alertIcon": "",
	        "alertHandler": onCallBackUniqueFailure
	    }, {});
}
function onCallBackUniqueFailure()
{
	try{
         kony.application.exit();
      }
   catch(Error)
      {
         
      }
}

function onConfirmationAlertCallBack(response) {
	GBL_Session_LOCK=true;
    if (response == true) {
    	//update session for session transaction lock
    } else {
    	onCallBackUniqueFailure();
    }
};

function showAlertForSecValidation(message) {
    var alert_seq0_act0 = kony.ui.Alert({
        "message": message,
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": "",
        "yesLabel": "Yes",
        "noLabel": "No",
        "alertIcon": "",
        "alertHandler": onConfirmationAlertCallBack
    }, {});
};



function callServiceToLogRiskInfo()
{
				
		var inputParam ={}; 
		///var deviceInfo = kony.os.deviceInfo();
		//if (deviceInfo["name"] == "iPhone" || deviceInfo["name"] == "iPhone Simulator") {
		//#ifdef iphone
			inputParam["deviceRiskScore"] = GBL_RiskScoreIph;
			inputParam["osType"] = "2";
		//#else
			//#ifdef android
			//}else if (deviceInfo["name"] == "android"){
				inputParam["deviceRiskScore"] =GBL_Risk_DataTo_Log;
				inputParam["osType"] = "1";
			//#endif
		//#endif
		//}
		if(GBL_MalWare_Names == "" || GBL_MalWare_Names == null){
			inputParam["malwareResult"] = "0";	
		}else{
			inputParam["malwareResult"] = "1";
		}
		inputParam["malwareName"]=GBL_MalWare_Names;
		inputParam["rootedFalg"]=GBL_rootedFlag.toString();
		inputParam["deviceUNId"]=GBL_UNIQ_ID;
		inputParam["sessionLock"]=GBL_Session_LOCK.toString();
		
		invokeServiceSecureAsync("updateRiskData", inputParam, callBackUpdateRiskData)
}
function callBackUpdateRiskData(status,resulttable){
	if(status == 400){
		
	}
}
function collectRiskData(){
		/**
		 * The below code commented as part of R76 Sprint 
		 * MIB-6454 Analysis of Trusteer code flow
		 */
		
		/**
		*
		//if (deviceInfo["name"] == "iPhone" || deviceInfo["name"] == "iPhone Simulator") {
		//#ifdef iphone
			startRiskDataCollForIphone();
		//}else if (deviceInfo["name"] == "android"){
		//#else
			//#ifdef android
				startRiskDataCollForAndroid();
			//#endif
		//#endif
		//}
		**/
}

function passingProdDescTnCEmail()
{
	prodDescTnC = gblFinActivityLogOpenAct["prodDesc"]; 
	prodDescTnC = prodDescTnC.replace(/\s+/g, '');
}

function TextBoxErrorSkin(textBoxName, skinName)
{
textBoxName.skin = skinName
}
/**
 * description
 * returns {}
 */
function showActivationIncompletePage() {
    frmMBActiComplete.image250285458166.src = "iconnotcomplete.png";
    frmMBActiComplete.label50285458167.text = kony.i18n.getLocalizedString("keyActFail");
    frmMBActiComplete.btnStart.setVisibility(false);
   // frmMBActiComplete.show();
  frmRegActivationSuccess.destroy();
  activationCompleteShow();
}

function onClickLinkActMB(rch){
	 if(rch["href"] !=null){
		  gblLatitude = "";
		  gblLongitude = "";
		 
		    gblCurrenLocLat = "";
		    gblCurrenLocLon = "";
		    provinceID = "";
		    districtID = "";
		    gblProvinceData = [];
		    gblDistrictData = [];
		    frmATMBranch.btnCombProvince.text = kony.i18n.getLocalizedString("FindTMB_Province");
		    frmATMBranch.btnCombDistrict.text =  kony.i18n.getLocalizedString("FindTMB_District");
		    frmATMBranch.txtKeyword.text = "";
		  onClickATM();
		  popupActivationHelp.dismiss();
	 }else{
	  	
	 }
}
function dontAllowNonNeumaric(textbozId){
	var isIE8 = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
	if(!isIE8){
		var len=textbozId.text.length;
		var lastChar=textbozId.text.charAt(len-1);
	    if (!kony.string.isNumeric(lastChar)) 
			textbozId.text = textbozId.text.substring(0,len-1);
	}		
 }
 
 
function otplength(){
   var otplen = frmIBActivateIBankingStep2Confirm.txtOTP.text.length;
   if(otplen > 6){
   //alert("length is-->"+otplen);
       frmIBActivateIBankingStep2Confirm.txtOTP.text = frmIBActivateIBankingStep2Confirm.txtOTP.text.substring(0,6);
   }
 } 

function allowOnlyAlphaNumeric(textbozId) {
     var len = textbozId.text.length;
    var lastChar = textbozId.text.charAt(len - 1);
    
    if (!lastChar.match(/^[a-zA-Z0-9- ]*$/)) {
       textbozId.text = textbozId.text.substring(0, len - 1);
    } 
}



function firstNameAlpNumValidation(text){
      var pat1 = /^[A-Za-z\u0E00-\u0E7F\-]+$/
      var isValidFirstName = pat1.test(text);     
      return isValidFirstName;
}
function lastNameAlpNumValidation(text){
      var pat1 = /^[A-Za-z\u0E00-\u0E7F\s-]+$/
      var isValidLastName = pat1.test(text);
      return isValidLastName;
}

function handleOTPLockedIB(callBackResponse){
	gblOTPLockedForBiller = "04";
	gblUserLockStatusIB="04";
	gblRcOpLockStatus=true;
	if(callBackResponse["IBUserStatusID"]!= null || callBackResponse["IBUserStatusID"] != undefined)
		gblUserLockStatusIB= callBackResponse["IBUserStatusID"];
	popIBBPOTPLocked.show();
 }
 

function handleOTPLockedIBNew(){
	popIBBPOTPLockedNew.dismiss();
	if (gblSetPwd == true) {	
	 	frmIBPreLogin.show();
	} else {
		dynamicMenu_btnMenuMyAccountSummary_onClick_seq0();
		frmIBPostLoginDashboard.show();
	}
 }
 
 /*
************************************************************************
            Name    : DisableFadingEdges()
            Author  : Ankit Chandra
            Date    : March 14, 2014
            Purpose : To remove the fading edges of the form in android.
        Input params: CurrentformId
       Output params: Nil
        ServiceCalls: nil
        Called From : preShow of the form
************************************************************************
 */
 
 
 function DisableFadingEdges(CurrentformId)
 {
 	//#ifdef android
 		CurrentformId.sboxRight.showFadingEdges = false ;
		//CurrentformId.scrollboxLeft.showFadingEdges = false;
		CurrentformId.scrollboxMain.showFadingEdges = false;
 	//#endif
 	
 }
 
 function dontAllowNonNeumaricSPAMB(textbozId){

        var len = textbozId.text.length;
        while(len >= 1 && (!kony.string.isNumeric(textbozId.text.charAt(len - 1)))){
        textbozId.text = textbozId.text.substring(0, len - 1);
        len = len - 1;
        }
 }
 
 function findColonLastString(text){
 
 	if(text == undefined || text == null)
 		return false;
 	
 	var len =text.trim().length; 
 	if(text.trim().charAt(len - 1) == ':')
 		return false;
 	else
 		return true;
 }
 
 function onDeviceBck()
 {
   	frmBillPaymentEdit.show();

	frmBillPaymentEdit.lblStartDate.text = gblTempTPStartOnDateMB
	frmBillPaymentEdit.lblEndOnDate.text = gblTempTPEndOnDateMB
	frmBillPaymentEdit.lblExecutetimes.text = gblTempTPRepeatAsMB
	
	repeatAsMB = gblTemprepeatAsTPMB
	endFreqSaveMB = gblTempendFreqSaveTPMB;


	if (repeatAsMB == "" || endFreqSaveMB == "") {
		if (gblEndingFreqOnLoadMB == "Never") {
			OnClickRepeatAsMB = "";
			OnClickEndFreqMB = "";
		}else if(gblOnLoadRepeatAsMB == "Once"){
			OnClickRepeatAsMB = repeatAsIB;
			OnClickEndFreqMB = endFreqSave;
		}else {
			OnClickRepeatAsMB = "";
			OnClickEndFreqMB = "";
		}
	} else {
		OnClickRepeatAsMB = repeatAsMB;
		OnClickEndFreqMB = endFreqSaveMB;
	}
 }
 
 function allowOnlyNumbers(evt) {
	var e = evt && evt.type == 'keydown' ? evt : window.event;
	
    if (e) {
        var charCode = (e.which) ? e.which : e.keyCode;
        
        if (charCode != 46 && charCode != 110 && charCode != 190 && charCode > 31 && (charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105)) {
              e.preventDefault();
        }
        if (e.shiftKey) {
			e.preventDefault();
		}
      }
}

function allowOnlyNumbersZeroToNine(evt) {
	var e = evt && evt.type == 'keydown' ? evt : window.event;
	
    if (e) {
        var charCode = (e.which) ? e.which : e.keyCode;
        if (charCode != 8 &&(charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105)) {
              e.preventDefault();
        }
        if (e.shiftKey) {
			e.preventDefault();
		}
      }
}

function addNumberCheckListner(widjet) {
    var isIE8 = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
	if(!isIE8){
	    var elementID = kony.application.getCurrentForm().id + "_" + widjet
	    
	    var tNode = document.getElementById(elementID);
	    if (tNode) {
	        tNode.addEventListener('keydown', allowOnlyNumbers, false);
	    }
    }
}

function addNumberCheckListnerZeroToNine(widjet) {
    var isIE8 = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
	if(!isIE8){
	    var elementID = kony.application.getCurrentForm().id + "_" + widjet
	    
	    var tNode = document.getElementById(elementID);
	    if (tNode) {
	        tNode.addEventListener('keydown', allowOnlyNumbersZeroToNine, false);
	    }
    }
}

function getfbcodecalback(status,callBack){
	if(status==400){
		
		if(callBack["fbuserCode"] != "notfound")
			gblFBCode=callBack["fbuserCode"]
		else
			gblFBCode="";
		
	}
}

function appendColon(input){
	if(input != null && input != undefined && (input.trim().charAt(input.trim().length-1) == ':' || input.trim()=="")){
		if(input.trim().charAt(input.trim().length-2) == ':'){
			return input.substring(0,input.trim().length-1);
		}
		return input;
	}else{		
		return input+':';
	}
}

function removeColonFromEnd(input){
	//Common method to remove any number of ':' from the end of an input string
	if(undefined == input || null == input)input="";
	if(input.trim().charAt(input.trim().length-1) == ':'){
		return removeColonFromEnd(input.substring(0,input.trim().length-1));
	}else{		
		return input;
	}
}


function SPAcopyright_text_display() {
	curr_lang = kony.i18n.getCurrentLocale();
	var date = new Date();
	copyright_year = date.getFullYear()
	if (curr_lang == "th_TH") {
		frmSPALogin.lblFooter.text = kony.i18n.getLocalizedString("keyIBFooterCopyrightFirstHalf") + " " + (Number(
			copyright_year) + 543) + " " + kony.i18n.getLocalizedString("keyIBFooterCopyrightSecondHalf");
	} else
		frmSPALogin.lblFooter.text = kony.i18n.getLocalizedString("keyIBFooterCopyrightFirstHalf") + " " +
			copyright_year + ". " + kony.i18n.getLocalizedString("keyIBFooterCopyrightSecondHalf");
}
//function to check for Tab/Blank spaces RegEX

function hasWhiteSpace(s) {
  return /\s/g.test(s);
}

function maskCreditCard(creditCardNo) {
    var formatedCCNo = "";
    if(undefined != creditCardNo && null != creditCardNo){
    	if ("" != creditCardNo && 16 == creditCardNo.length) {
	        var length = creditCardNo.length;
	        
	        var firstFour = creditCardNo.substring(0, 4);
	        var fiveSix = creditCardNo.substring(4, 6);
	        var lastFour = creditCardNo.substring(12,16);
	        var formatedValue = firstFour + "-" + fiveSix + "XX-XXXX-" + lastFour;
	        formatedCCNo = formatedValue;
	        
	    } else{
	    	formatedCCNo = creditCardNo;
	    }
    } else{
    	formatedCCNo = creditCardNo;
    }
    return formatedCCNo;
}

function iphone6check() {
	if (flowSpa) {
        if ((navigator.userAgent.match(/iPhone/i))) {
            var iphonescreenwidth = screen.width;
            if (iphonescreenwidth >= 375) {
                isIphone6=true;
            } else {
                isIphone6=false;
            }
        }
		else{
			isIphone6=false;
		}
    } else if (gblDeviceInfo.name == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPod touch") {
        if( gblDeviceInfo.model.indexOf("6") >= 0){
         	isIphone6MB=true;
        } else {
            isIphone6MB=false;
        }
    }else{
        isIphone6=false;
        isIphone6MB=false;
    }
}

function commonMBPostShow(){
       var headerhbx=document.getElementsByClassName("hboxHeader");
       if(headerhbx!= undefined && headerhbx[0] != undefined && headerhbx!= null)
       headerhbx[0].style.margin="0% 0% -1% 0%";
       
       if(kony.application.getCurrentForm().id == "frmBillPayment"){
              var headerhbx=document.getElementsByClassName("hboxHeader");
              if(headerhbx!= undefined && headerhbx[1] != undefined && headerhbx!= null)
                     headerhbx[1].style.margin="0% 0% -1% 0%";
       }
       if(kony.application.getCurrentForm().id == "frmATMBranch"){
              var headerhbx=document.getElementsByClassName("hbxAccBgBlue");
              if(headerhbx!= undefined && headerhbx[0] != undefined && headerhbx!= null)
              headerhbx[0].style.margin="0% 0% -1% 0%";
       }
}

function loadMyBillPage(){
	gblBillerFromMenu = true;
    getMyBillSuggestListIB();
    //frmIBMyBillersHome.show();
    frmIBMyBillersHome.segMenuOptions.setData([{
        "lblSegData": {
            "skin": "lblIBSegMenu",
            "text": kony.i18n.getLocalizedString("keyMyProfile")
        }
    }, {
        "lblSegData": {
            "skin": "lblIBSegMenu",
            "text": kony.i18n.getLocalizedString("keyMyAccountsIB")
        }
    }, {
        "lblSegData": {
            "skin": "lblIBSegMenu",
            "text": kony.i18n.getLocalizedString("kelblOpenNewAccount")
        }
    }, {
        "lblSegData": {
            "skin": "lblIBSegMenu",
            "text": kony.i18n.getLocalizedString("keyMyRecipients")
        }
    }, {
        "lblSegData": {
            "skin": "lblIBsegMenuFocus",
            "text": kony.i18n.getLocalizedString("keyMyBills")
        }
    }, {
        "lblSegData": {
            "skin": "lblIBSegMenu",
            "text": kony.i18n.getLocalizedString("myTopUpsMB")
        }
    }]);
    if (kony.i18n.getCurrentLocale() != "th_TH") frmIBMyBillersHome.btnMenuAboutMe.skin = "btnIBMenuAboutMeFocus";
    else frmIBMyBillersHome.btnMenuAboutMe.skin = "btnIBMenuAboutMeFocusThai";
}

function OTPhbxBox_skinChange_onBeginEditing(hbxId){
	hbxId.skin = "hbxOtpTextField";
}

function OTPhbxBox_skinChange_onEndEditing(hbxId){
	hbxId.skin = "hbxOtpTextFieldNormal";
}

function AvgYears( startDate , endDate , dateDifference )
{ 
	num = endDate.getFullYear() - startDate.getFullYear();
	if((num *365 + count_Years(startDate , endDate) - dateDifference) <= 0)
		return num;
	else return num - 1;	
}	
		
function isLeapY( year , startDate , endDate )
{
  var tDate = parseDate("29/02/2014");
  tDate.setYear(year);
  if( ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0) )
  {
     if( tDate > startDate && tDate < endDate )
		return true;
  }
  return false;
}		
		
		
function count_Years(startDate , endDate)
{
	var cnt = 0;
	for(i = startDate.getFullYear() ; i <= endDate.getFullYear() ; i++)
	{
		if(isLeapY(i , startDate , endDate ))
			cnt ++;
	}
	return cnt;
}

function PreencodeAccntNumbers(ValfrmAccnt)
{
	if (ValfrmAccnt == null) return false;
	if(ValfrmAccnt.length==14)
	{
		if(ValfrmAccnt.substring(0,4)=="0000")
		{
			return encodeAccntNumbers(ValfrmAccnt.substring(4,ValfrmAccnt.length));
		}

	}
    return encodeAccntNumbers(ValfrmAccnt);
}

//Common method to show hide loading indicator for both IB and MB
function showDismissLoadingMBIB(isShow){
	if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "android" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPod touch") {
		if(isShow)
        	showLoadingScreen();
        else
        	dismissLoadingScreen();	
    } else {
    	if(isShow)
        	showLoadingScreenPopup();
        else
        	dismissLoadingScreenPopup();	
    }
}

 function frmIBPreLoginGlobalsNonIE8() {
	 hbxIBPreLogin.setVisibility(true);
	 hbxiE8.setVisibility(false);
	 hbxClose.setVisibility(false);
	 frmIBPreLogin.txtUserId.setEnabled(true);
	 frmIBPreLogin.txtPassword.setEnabled(true);
};
function chekcingIE8()
{
	 chkIE8 = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
	 if(chkIE8) 
	 {
		 hbxIBPreLogin.setVisibility(false);
		 hbxiE8.setVisibility(true);
		 hbxClose.setVisibility(true);
		// frmIBPreLoginGlobalsforIE8();
		 frmIBPreLogin.txtUserId.setEnabled(false);
		 frmIBPreLogin.txtPassword.setEnabled(false);
	}
}

function formatCitizenID(unformattedCitizenID){
	if(null == unformattedCitizenID)return "";
	if(unformattedCitizenID.length != 13)return unformattedCitizenID;
	var iddata = unformattedCitizenID;
	var iphenText = "";
	var noniphendata = "";
	for (i = 0; i < iddata.length; i++)
		if (iddata[i] != "-") noniphendata += iddata[i];
	iddata = noniphendata;
	for (i = 0; i < iddata.length; i++) {
		iphenText += iddata[i];
		if (i == 0 || i == 4 || i == 9 || i == 11) {
			iphenText += '-';
		}
	}
	return iphenText;
}

function formatEWalletNumber(eWalletNumber){
	if (eWalletNumber != "") {
		var hyphenText = "";
		for (i = 0; i < eWalletNumber.length; i++) {
			hyphenText += eWalletNumber[i];
			if (i == 2) {
				hyphenText += '-';
			}
		}
	}
	return hyphenText;
}

function formatMobileNumber(mobileNumber){
	if (mobileNumber != "") {
		var hyphenText = "";
		for (i = 0; i < mobileNumber.length; i++) {
			hyphenText += mobileNumber[i];
			if (i == 2 || i == 5) {
				hyphenText += '-';
			}
		}
	}
	return hyphenText;
}

function formatLoanLandlineNumber(txt){
  if (txt == null) return false;
	var noChars = txt.length;
	var temp = "";
	var i, txtLen = noChars;
	var currLen = noChars;
	
	if (gblPrevLen < currLen) {
		for (i = 0; i < noChars; ++i) {
			if (txt[i] != '-') {
				temp = temp + txt[i];
			} else {
				txtLen--;
			}
			
		}
		var iphenText = "";
		for (i = 0; i < txtLen; i++) {
			iphenText += temp[i];
			if (i == 1 || i == 4) {
				iphenText += '-';
			}
		}
		
		//if (flowSpa) {
			//frmMBActivation.txtIDPass.text = iphenText;
			//
		//} else {
			frmLoanWorkInfo.tbxOfficeNo.text = iphenText;
		//}
	}
  gblPrevLen = currLen;
/*	if (landLineNumber != "") {
		var hyphenText = "";
		for (i = 0; i < landLineNumber.length; i++) {
			hyphenText += landLineNumber[i];
			if (i == 1 || i == 5) {
				hyphenText += '-';
			}
		}
	}
  
	return hyphenText;*/
}

function formatLoanMobileNumber(txt){
  if (txt == null) return false;
	var noChars = txt.length;
	var temp = "";
	var i, txtLen = noChars;
	var currLen = noChars;
	
	if (gblPrevLen < currLen) {
		for (i = 0; i < noChars; ++i) {
			if (txt[i] != '-') {
				temp = temp + txt[i];
			} else {
				txtLen--;
			}
			
		}
		var iphenText = "";
		for (i = 0; i < txtLen; i++) {
			iphenText += temp[i];
			if (i == 2 || i == 5) {
				iphenText += '-';
			}
		}
		
		//if (flowSpa) {
			//frmMBActivation.txtIDPass.text = iphenText;
			//
		//} else {
			frmLoanWorkInfo.tbxOfficeNo.text = iphenText;
		//}
	}
  gblPrevLen = currLen;
/*	if (landLineNumber != "") {
		var hyphenText = "";
		for (i = 0; i < landLineNumber.length; i++) {
			hyphenText += landLineNumber[i];
			if (i == 1 || i == 5) {
				hyphenText += '-';
			}
		}
	}
  
	return hyphenText;*/
}

//below function for non-casa accounts.
function noActiveActs()
{
	var noCasaAct = false;
	var nonCASAAct = 0;
	if (null != gblAccountTable)
	{
		if(null !=gblAccountTable.custAcctRec){
            for(var i=0; i < gblAccountTable.custAcctRec.length;i++)
            {	
                if(null!=gblAccountTable["custAcctRec"][i]["acctStatus"]){
                    var accountStatus = gblAccountTable["custAcctRec"][i]["acctStatus"];
                    if(accountStatus.indexOf("Active") == -1){
                        nonCASAAct = nonCASAAct + 1;
                    }
                }
            }
             	
		if(nonCASAAct==gblAccountTable.custAcctRec.length)
			noCasaAct=true;
        } 
	}
	return noCasaAct;
}

//Below function is to truncate extra decimals in amount(amount digits should be less than 18 else it will roundoff) 
function fixedToTwoDecimal(amount) {
	if(isNotBlank(amount)) {
	    amount = removeCommos(amount);
	    amount = Number(amount);
	    amount = amount.toFixedDown(2);
	}
    return amount+"";
}
function fixedToNDecimal(amount, n) {
	if(isNotBlank(amount)) {
	    amount = removeCommos(amount);
	    amount = Number(amount);
	    amount = amount.toFixedDown(n);
	}
    return amount+"";
}

Number.prototype.toFixedDown = function(amount) {
    var re = new RegExp("(\\d+\\.\\d{" + amount + "})(\\d)"),
        m = this.toString().match(re);
    return m ? parseFloat(m[1]) : this.valueOf();
}

function shortenBillerName(str, len)
{ 	
	
	len = 100; //MIB-5334 - Allow customer to see full biller name on TMB Touch
	var newValue = str;
    var actlength =str.length;
    if(actlength>len)
    {
    	var cmpCode= str.substring(actlength-6, actlength);
    	var res = str.substring(0, actlength-6);
    	var newlength =res.length;
    	var shorten= res.substring(0,len);
    	newValue=shorten+".."+cmpCode;
    }
    else
    {
    	newValue=str;
    }
    
    return newValue;
}

function replaceAll(str, find, replaceStr) {
  return str.replace(new RegExp(find, 'g'), replaceStr);
}


//Sorting Array of Json Objects

// generic comparison function
cmp = function(x, y){
    return x > y ? 1 : x < y ? -1 : 0; 
};

function sortArrayOfJSONObjects(jsonArray,keyArray,order){
	jsonArray.sort(function(a, b){
    var arr1=[];
   	var arr2=[];
    for(var key in keyArray){
    	var val1=cmp(a[keyArray[key]], b[keyArray[key]]);
        var val2=cmp(b[keyArray[key]], a[keyArray[key]]);
        if(order){
        	arr1.push(val1);
        	arr2.push(val2);
        }else{
            //note the minus, for descending order
        	arr1.push(-val1);
        	arr2.push(-val2);      
        }
    }
    return cmp( arr1,arr2);
	});
}


function equals ( x, y ) {
    // If both x and y are null or undefined and exactly the same
    if ( x === y ) {
        return true;
    }

    // If they are not strictly equal, they both need to be Objects
    if ( ! ( x instanceof Object ) || ! ( y instanceof Object ) ) {
        return false;
    }

    // They must have the exact same prototype chain, the closest we can do is
    // test the constructor.
    if ( x.constructor !== y.constructor ) {
        return false;
    }

    for ( var p in x ) {
        // Inherited properties were tested using x.constructor === y.constructor
        if ( x.hasOwnProperty( p ) ) {
            // Allows comparing x[ p ] and y[ p ] when set to undefined
            if ( ! y.hasOwnProperty( p ) ) {
                return false;
            }

            // If they have the same strict value or identity then they are equal
            if ( x[ p ] === y[ p ] ) {
                continue;
            }

            // Numbers, Strings, Functions, Booleans must be strictly equal
            if ( typeof( x[ p ] ) !== "object" ) {
                return false;
            }

            // Objects and Arrays must be tested recursively
            if ( !equals( x[ p ],  y[ p ] ) ) {
                return false;
            }
        }
    }

    for ( p in y ) {
        // allows x[ p ] to be set to undefined
        if ( y.hasOwnProperty( p ) && ! x.hasOwnProperty( p ) ) {
            return false;
        }
    }
    return true;
}

function maskAccount(AcctNo) {
	if (isNotBlank(AcctNo)) {
		AcctNo = kony.string.replace(AcctNo, "-", "");
		if (AcctNo.length == 10) {
			AcctNo = "xxx-x-"+AcctNo.substring(4, 9)+"-x";
		}else if (AcctNo.length == 14){
			AcctNo = "xxx-x-"+AcctNo.substring(8, 13)+"-x";
		}
	}
	return AcctNo;
}

function maskCitizenID(citizenID) {
	if (isNotBlank(citizenID)) {
		citizenID = kony.string.replace(citizenID, "-", "");
		if (citizenID.length == 13) {
			citizenID = "x-xxxx-xxxx"+citizenID.substring(9, 10)+"-"+citizenID.substring(10, 12)+"-"+citizenID.substring(12, 13);
		}
	}
	return citizenID;
}

function maskeWalletID(eWalletID) {
	if (isNotBlank(eWalletID)) {
		eWalletID = kony.string.replace(eWalletID, "-", "");
		if (eWalletID.length == 15) {
			eWalletID = "xxx-xxxxxxxx"+eWalletID.substring(11, 15);
		}
	}
	return eWalletID;
}

function maskMobileNumber(mobileNo) {
	if (isNotBlank(mobileNo)) {
		mobileNo = kony.string.replace(mobileNo, "-", "");
		if (mobileNo.length == 10) {
			mobileNo = "xxx-xxx-"+mobileNo.substring(6, 10);
		}
	}
	return mobileNo;
}

function checkReleaseTransferLandingMB(){
	var currentForm = kony.application.getCurrentForm().id;
	if(isNotBlank(currentForm) && kony.string.equals(currentForm, "frmTransferLanding")){
		setEnabledTransferLandingPage(true);
	}
}

function callNative(mobileno) {
	try {
		//showLoadingScreen();
       kony.phone.dial(mobileno);
		//	dismissLoadingScreen();
      
	  /*	if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad") {
			IphoneCall.dial(mobileno);
			dismissLoadingScreen();
		}else if (gblDeviceInfo["name"] == "android"){
			kony.phone.dial(mobileno);
			dismissLoadingScreen();
		} */
	}catch(err){
		kony.print("error callNative: "+mobileno+", error: "+err);
	}
}

// Start : MIB-4884-Allow special characters for My Note and Note to recipient field
function replaceHtmlTagChars(inputString) {
 if(isNotBlank(inputString)) {
	inputString = replaceAll(inputString, "<" , "&lt;");
	inputString = replaceAll(inputString, ">" , "&gt;");
 }
 return inputString;
}

 function replaceHtmlTagsInObject(inputParam) {
	 var keyNames = GBL_ALLOW_SPECIAL_CHAR_KEY_NAMES.split(',');
	 var keyName = "";
	for (i in keyNames) {
		keyName = keyNames[i];
		if (inputParam.hasOwnProperty(keyName)) {
			inputParam[keyName] = replaceHtmlTagChars(inputParam[keyName]);
		}
	}
	
	return inputParam;
 }
 
 function checkSpecialCharMyNote(myNote) {
 	if(isNotBlank(myNote)) {
 		return myNote.indexOf("<%") > -1;
 	}
 	
 	return false;
 }
 
function decodeHtmlTagChars(inputString) {
	 if(isNotBlank(inputString)) {
		inputString = replaceAll(inputString, "&lt;" , "<");
		inputString = replaceAll(inputString, "&gt;" , ">");
	 }
	 return inputString;
}

function checkEmoji(inputData) {
	//Do not remove below commented code, keep it for future reference
	//var patternEmoji = /(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|[\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|[\ud83c[\ude32-\ude3a]|[\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])/;
	//return patternEmoji.test(inputData);
	return !validateSpecialChars(inputData);
}

function validateSpecialChars(inputData) {
	var patternEmojiNow =  new RegExp(GBL_VALID_CHARS_MY_NOTE_NOW);
	var patternEmojiFuture =  new RegExp(GBL_VALID_CHARS_MY_NOTE_FUTURE);
	
	if(gblPaynow) {
		return patternEmojiNow.test(inputData);
	} else {
		return patternEmojiFuture.test(inputData);
	}
}

function notAllowEmojiChars(eventObj) {
	var enteredText = eventObj.text;
	if(isNotBlank(enteredText)) {
		if(checkEmoji(enteredText)) {
			eventObj.text = gblPrevTextNoEmoji;
		} else {
			gblPrevTextNoEmoji = enteredText;
		}
	} else {
		gblPrevTextNoEmoji = "";
	}
}

function assignTextNoEmoji(eventObj) {
	gblPrevTextNoEmoji = eventObj.text;
}
 // End : MIB-4884-Allow special characters for My Note and Note to recipient field
 
function changeStatusBarColor(){
	try {
		kony.application.getCurrentForm().statusBarColor =  "007ABC00";
	} catch (e) {
		// todo: handle exception
	}
} 

function productionNameTrun(productName){
	var prodName =  "";
	if(isNotBlank(productName) && productName.length > 31){
		prodName = productName.substring(0, 29);
		prodName = prodName + "..";
		return prodName;
	}
	return productName;
}

function branchNameTrun(branchName){
	var bracName =  "";
	if(isNotBlank(branchName) && branchName.length > 26){
		bracName = branchName.substring(0, 24);
		bracName = bracName + "..";
		return bracName;
	}
	return branchName;
}

/**
 * @function
 *
 * @param base64String 
 */
function convertBase64ToRawBytes(base64String) {
  
       try{
            	base64String = kony.convertToRawBytes(base64String);
          } catch(e) {
            	kony.print("KONYTOUCHID: Exception base64String >>>"+e);
            	kony.print("KONYTOUCHID: Exception Name base64String >>>"+e.name);
            	kony.print("KONYTOUCHID: Exception Message base64String >>>"+e.message);
          }
  
  return base64String;
}

function formatAccount(acctNo) {
	if (isNotBlank(acctNo)) {
		var firstThree = "";
		var digitFour = "";
		var fiveToNine = "";
		var lastCharacter = "";		
		if (acctNo.length == 10){
			acctNo = kony.string.replace(acctNo, "-", "");
			firstThree = acctNo.substring(0, 3);
			digitFour = acctNo.substring(3, 4);
			fiveToNine = acctNo.substring(4, 9);
			lastCharacter = acctNo.substring(9, 10);
			acctNo = firstThree + "-" + digitFour + "-" + fiveToNine + "-" + lastCharacter;
		}else if (acctNo.length == 14){
			acctNo = kony.string.replace(acctNo, "-", "");
			firstThree = acctNo.substring(4, 7);
			digitFour = acctNo.substring(7, 8);
			fiveToNine = acctNo.substring(8, 13);
			lastCharacter = acctNo.substring(13, 14);
			acctNo = firstThree + "-" + digitFour + "-" + fiveToNine + "-" + lastCharacter;
		}		
	}
	return acctNo;
}

/**

The below method return Boolean true when the device using the mobile Data
	Else it returns false
**/
function isNetworkType3G(){
  
   var flag3Gnetwork =false;
   if(kony.net.getActiveNetworkType() == constants.NETWORK_TYPE_3G) {
      flag3Gnetwork = true;
   }
  return flag3Gnetwork;
}

/**

The below method return Boolean true when the application using the internet connectivity
	Else it returns false
**/
function isActiveNetworkAvailable(){
  
  return kony.net.isNetworkAvailable(constants.NETWORK_TYPE_ANY);

}


function navigatetoWIFISettings(){
  
  //#ifdef iphone
	   kony.application.openApplicationSettings();
   //#endif
  
   //#ifdef android
 	 	openSettings.wifiSettings(); //MarshmallowPermissionChecks.navigatoWiFISettings();
  //#endif
  //frmActivationAttention.flexSettingsPopup.setVisibility(false);
}

//Checking customer enabled or disabled push notification on device (both Android/iOS)
function arePushNotificationsEnabled(){
  	kony.print(">>> isPushNotificationsEnabled");
  	//This method will return true/fase,
  	return PushNotificationUtils.areNotificationsEnabled();
}

//Navigate to push notification setting (Package Name or bundle identifier is com.kone.TMB)
function navigatetoPushNotificationSettings(){
  	kony.print(">>> navigatetoPushNotificationSettings");
	PushNotificationUtils.navigateToSettings();//openSettings.pushSettings(); 
}



/**
 * @function
 *
 * @param dateValue, format should be yyyy-MM-dd HH:mm:ss else it will not work
 */
function formatDateForUV(dateValue){
  
  if(isNotBlank(dateValue)) {
        var dateStr = dateValue.split(" ")[0];
    	var timeStr = dateValue.split(" ")[1];
        
  		var dd = dateStr.substr(8, 2); 
        var mm = dateStr.substr(5, 2);
        var yyyy = dateStr.substr(0, 4);

        var hh_mm =  timeStr.substr(0, 5);

       dateFromInput = dd+"/"+mm +"/"+ yyyy +" "+hh_mm;

      return dateFromInput;
  }else{
    return dateValue;
  }
}

//Below function is not working as expected for iPhone waiting for plugin from PF, hence backup
function formatDateForUVBkp(dateValue){
  		//#ifdef iphone
        dateValue=dateformatteriPhone(dateValue);
        //#endif
        var dateFromInput = new Date(dateValue);
        
  		var dd = dateFromInput.getDate(); 
        var mm = dateFromInput.getMonth() + 1;
        var yyyy = dateFromInput.getFullYear();

        var hh =  dateFromInput.getHours();
        var min =  dateFromInput.getMinutes();

        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }

        if (hh < 10) {
              hh = '0' + hh;
          }
        if (min < 10) {
            min = '0' + min;
        }

       dateFromInput = dd+"/"+mm +"/"+ yyyy +" "+hh+":"+min;

      return dateFromInput;
}


//Below function not working as expected
function dateformatteriPhone(iPhonedate){
  kony.print("in Common function")
  try{
  var str = iPhonedate;
  var res = str.split(" ");
  res[0]=res[0]+"T";
  res[1]=res[1]+"Z";
  iPhonedate= res[0]+res[1];
  kony.print("expectedExpire iphone>>> "+iPhonedate);
  return iPhonedate;
  }catch(e){
    kony.print("Issue with date");
  }  
  
}

function formatDateFullDesc(dateToFormat){

  var dateArray = dateToFormat.split("/");
  var day = dateArray[0];
  var month = dateArray[1];
  var month = parseInt(month);
  var year = dateArray[2];
  var dateToDisplay = "";
  kony.print("day @@:"+day);
  var monthArray = [kony.i18n.getLocalizedString("keyCalendarJan"), kony.i18n.getLocalizedString("keyCalendarFeb"), kony.i18n.getLocalizedString("keyCalendarMar"), 
                    kony.i18n.getLocalizedString("keyCalendarApr"),kony.i18n.getLocalizedString("keyCalendarMay"), kony.i18n.getLocalizedString("keyCalendarJun"),
                    kony.i18n.getLocalizedString("keyCalendarJul"), kony.i18n.getLocalizedString("keyCalendarAug"),kony.i18n.getLocalizedString("keyCalendarSep"),
                    kony.i18n.getLocalizedString("keyCalendarOct"), kony.i18n.getLocalizedString("keyCalendarNov"), kony.i18n.getLocalizedString("keyCalendarDec")];
  for(var i=1;i<=12; i++){
    kony.print("inside for @@: "+i);
    if(i.toString() == month){
      kony.print("inside if match found @@: ");
      month = monthArray[i-1];
      break;
    }
  }
  kony.print("month @@:"+month);
  if(kony.i18n.getCurrentLocale() == "th_TH"){
    var yearToDisplay = parseInt(year);
    kony.print("in thai yearToDisplay @@: "+yearToDisplay);
    yearToDisplay = yearToDisplay + 543;
    kony.print("in thai yearToDisplay 543 added @@: "+yearToDisplay);
    yearToDisplay = yearToDisplay.toString();
    kony.print("in thai yearToDisplay to string @@: "+yearToDisplay);
    dateToDisplay = day+" "+month+" "+yearToDisplay;
    kony.print("in thai date to display @@: "+dateToDisplay);
  }else{
    dateToDisplay = day+" "+month+" "+year;
    kony.print("in english date to display @@: "+dateToDisplay);
  }
kony.print("end date format ");
  return dateToDisplay;
}

function setAppErrorHandler(){
  kony.print("inside setAppErrorHandler");
  kony.lang.setUncaughtExceptionHandler(uncaughtExceptionHandler);      
}

function uncaughtExceptionHandler(exceptionObject) {
    // Converting exception object into a readable string
    var exceptionString = "";
    var deviceDetails = "";
  
    if(gblDeviceInfo == null)
      gblDeviceInfo = kony.os.deviceInfo();
   
    deviceDetails = gblDeviceInfo["name"]+", "+gblDeviceInfo["model"]+", "+gblDeviceInfo["version"];
    
    if("stack" in exceptionObject){
        exceptionString =  exceptionObject.stack;
    }
    if("message" in exceptionObject){
      //  exceptionString += " : " + exceptionObject.message;
      kony.print("exceptionObject.message: "+exceptionObject.message);
    }
    
    //Logging the exception string to console
    kony.print("Unhandled TMBUI Exception:" + exceptionString);       
}

function getLocalizedString(key){
  return kony.i18n.getLocalizedString(key);
}

//Function for confirmation alert with success & failure callbacks
function showAlertWithYesNoHandler(keyMsg,KeyTitle,yes,no,successHandler, failHandler){

	//Defining basicConf parameter for alert
	var basicConf = {
		message: keyMsg,
		alertType: constants.ALERT_TYPE_CONFIRMATION,
		alertTitle: KeyTitle,
		yesLabel: yes,
		noLabel:no,
		alertHandler:function(response) {
                if (response == true) {
                    successHandler();
                } else {
					failHandler();                    	
                }
        }
	};
	//Defining pspConf parameter for alert
	var pspConf = {};
	//Alert definition
	var infoAlert = kony.ui.Alert(basicConf, pspConf);
}


function commaFormattedOpenAct(amount) {
	if (amount == '0.00' || amount == 0) {
		return '0.00';
	}else{
	    //checking for if ZERO exists at first left place
	    for (i = 0; i < amount.length; i++) {
	        if (amount.charAt(0) == '0') {
	            amount = amount.substring(1, amount.length);
	        } else {
	            amount = amount;
	        }
	    }
	    var ifCommaExists = amount.indexOf(",");
	    var newvalue;
	    amount = amount.trim();
	    if (ifCommaExists != -1) {
	        amount = amount.replace(/,/g, "");
	    }
	
	    if (amount.indexOf(".") > 0) {
	        var amountFormat = amount.split(".");
	
	        if (amountFormat[1].length == 1) {
	            amount = amount + "0";
	        } else if (amountFormat[1].length == 0) {
	            amount = amountFormat[0] + ".00";
	        }
	
	    } else {
	        if (amount == "") {
	            amount = 0;
	        }
	        amount = amount + ".00"
	    }
	
	    var decimalPart = "." + amount.split('.')[1];
	    amount = amount.split('.')[0];
	
	
	    while (amount.length > 3) {
	        decimalPart = "," + amount.substr(amount.length - 3, 3) + decimalPart;
	        amount = amount.substr(0, amount.length - 3);
	    }
	    newvalue = amount + decimalPart;
    }
    return newvalue;
}

/**
 * Method to dismiss the loading screen
 * @returns {}
 */

function dismissLoadingScreenPopup() {
	if(gblDeviceInfo["category"]=="IE" && gblDeviceInfo["version"]=="8"){
			popupIBLoading.dismiss();
	}else{
		kony.application.dismissLoadingScreen(); // Adding this code to fix iPad hang issues.Please comment this and uncomment above line if any issue is there
	}
}


function showAlertWithCallBack(keyMsg, KeyTitle, callBack) {
	var okk = kony.i18n.getLocalizedString("keyOK");
	//Defining basicConf parameter for alert
	var basicConf = {
		message: keyMsg,
		alertType: constants.ALERT_TYPE_INFO,
		alertTitle: KeyTitle,
		yesLabel: okk,
		noLabel: "",
		alertHandler: callBack
	};
	//Defining pspConf parameter for alert
	var pspConf = {};
	//Alert definition
	var infoAlert = kony.ui.Alert(basicConf, pspConf);
}


/**
 * Method to add - to account numbers
 * @param {} txt
 * @returns {}
 */

function encodeAccntNumbers(txt) {
	if (txt == null) return false;
	
	if(txt.length == 10){
		return addHyphenIB(txt);
	}
	else{
		return txt
	}
	var numChars = txt.length;
	var temp = "";
	var i, txtLen = numChars;
	var currLen = numChars;
	for (i = 0; i < numChars; ++i) {
		if (txt[i] != '-') {
			temp = temp + txt[i];
		} else {
			txtLen--;
		}
	}
	var iphenText = "";
	for (i = 0; i < txtLen; i++) {
		iphenText += temp[i];
		if (i == 2 || i == 3 || i == 8) {
			iphenText += '-';
		}
	}
	return iphenText;
}


/**
 * Method to show the loading screen
 * @returns {}
 */

function showLoadingScreenPopup() {
	if(gblDeviceInfo["category"]=="IE" && gblDeviceInfo["version"]=="8")
	{
		if(document.getElementById("popupIBLoading") == null)
		popupIBLoading.show();
		
	}
	else
	{
		kony.application.showLoadingScreen("frmLoading", "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null); // Adding this code to fix iPad hang issues.Please comment this and uncomment above 2 lines if any issue is there
	}
}


var TMBUtil = new function(){

	var controller = {};
	
	// To call this function: TMBUtil.DestroyForm(formObj)
	controller.DestroyForm = function(frmObj){
		frmObj.info = {};
		frmObj.destroy();
	}
	
	return controller;
}

function dynamicSort(property) {
	var sortOrder = 1;
	if (property[0] === "-") {
		sortOrder = -1;
		property = property.substr(1, property.length - 1);
	}
	return function (a, b) {
		var result = (a[property]["text"].toLowerCase() < b[property]["text"].toLowerCase()) ? -1 : (a[property]["text"].toLowerCase() > b[property]["text"].toLowerCase()) ? 1 : 0;
		return result * sortOrder;
	}
}

function dynamicSortLoan(property) {
    var sortOrder = 1;
    if(property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }
    return function (a,b) {
        var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        return result * sortOrder;
    }
}

function dynamicSortOther(property) {
	var sortOrder = 1;
	if (property[0] === "-") {
		sortOrder = -1;
		property = property.substr(1, property.length - 1);
	}
	return function (a, b) {
		var result = (a[property].toLowerCase() < b[property].toLowerCase()) ? -1 : (a[property].toLowerCase() > b[property].toLowerCase()) ? 1 : 0;
		return result * sortOrder;
	}
}

function getCRMLockStatus() {
if(gblUserLockStatusIB==gblFinancialTxnIBLock)
	gblRcOpLockStatus=true;
else{
gblRcOpLockStatus=false;
}
return gblRcOpLockStatus;
}


function invokeCommonIBLogger(str1) {
	if (gblLOG_DEBUG) {
		
	}
}

function getBraketsIfEmptyForAmount(amount){
  var formattedVal = "[-]";
  if(amount !== undefined && amount !== ""){
    return commaFormatted(amount) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
  }else{
    return formattedVal;
  }
}

function getBraketsIfEmptyForAccount(accountNo){
  var formattedVal = "[-]";
  if(accountNo !== undefined && accountNo !== ""){
    return formatAccountNo(accountNo);
  }else{
    return formattedVal;
  }
}

function getBraketsIfEmptyForMobileNo(mobileNo){
  var formattedVal = "[-]";
  if(mobileNo !== undefined && mobileNo !== ""){
    return maskMobileNumber(mobileNo);
  }else{
    return formattedVal;
  }
}

function isIphone5Dimensions(){ // This will return true for iphone 5, 5s, SE
	var deviceInfo = kony.os.deviceInfo();
	var screenwidth = deviceInfo["deviceWidth"];
	var screenheight = deviceInfo["deviceHeight"];
	var isIphone5Dimensions=false;
	//#ifdef iphone
		if (screenwidth <= 730 && screenheight < 1280) {
			isIphone5Dimensions=true;
		}
		
	//#endif
   return isIphone5Dimensions;
}


function formatBillNumber(billNo) {
	if(gblPEA_BILLER_COMP_CODE.indexOf(gblCompCode) != -1 && isNotBlank(billNo) && billNo.length >= 6) {
		billNo = billNo.substring(4,6) + "/" + billNo.substring(0,4);
	}
  return billNo;
}

function getBillNoLabel(billNo, lblBillNo) {
	if(gblPEA_BILLER_COMP_CODE.indexOf(gblCompCode) != -1) {
        if(isNotBlank(billNo)) {
          	lblBillNo = kony.i18n.getLocalizedString("PEA_Electricity_Of");
        } else {
          	lblBillNo = kony.i18n.getLocalizedString("PEA_DisConn_Penality_Fee");
        }
	}
  return lblBillNo;
}

//This funtion will not allow the user to enter the special characters in textboxs.
function notAllowSpecialChars(eventObj){
  try{
    var fieldValue = eventObj.text;
    var patternEmojiNow =  new RegExp(GBL_VALID_CHARS_MY_NOTE_NOW);
    if(!patternEmojiNow.test(fieldValue)){
      kony.print("---Entered special characters---");
      var textLen = fieldValue.length;
      fieldValue = fieldValue.substring(0, parseInt(textLen)-1);
      eventObj.text = fieldValue;
    }
  }catch(e){
    kony.print("@@@ In validateSpecialChar() Exception:::" + e);
  }
}

function isNFCEnabled(){
	var nfcStatus = null;
	//#ifdef iphone
	return	nfcStatus;
	//#endif
	//#ifdef android
   var KonyMain = java.import("com.konylabs.android.KonyMain");
   var context = KonyMain.getActivityContext();

   var adapterClass = java.import("android.nfc.NfcAdapter");
   var adapter = adapterClass.getDefaultAdapter(context);

   if (adapter !== null) {
     kony.print("Is Enabled : "+adapter.isEnabled());
     nfcStatus = adapter.isEnabled();
   }
  return nfcStatus;
  //#endif
}

function checkNFCSupprt(){
  var nfcFlag = isNFCEnabled();
  
  if(null !== nfcFlag){
   //nfcFlag = nfcFlag+"";
    if(true === nfcFlag){
    	//alert("NFC is ON");
      return true;
  	}else{
//       alert("NFC is OFF, Please on the NFC in settings");
      var msg = kony.i18n.getLocalizedString("eKYC_msgAllowNFC");
      var title = kony.i18n.getLocalizedString("info");
      var okk = kony.i18n.getLocalizedString("notificationAllow");
      var Noo = kony.i18n.getLocalizedString("btnDeny");
//       showAlertNFCNotEnable(msg, title);
      showAlertWithYesNoHandler(msg,title,okk,Noo,NavigateNFCSettings, doNothing);
    }
  }else{
    //alert("NFC is not supported in this device");
    return false;
  }
}

function NavigateNFCSettings() {
   openSettings.settings();
}


function showAlert(keyMsg, KeyTitle) {
	var okk = kony.i18n.getLocalizedString("keyOK");
	//Defining basicConf parameter for alert
	var basicConf = {
		message: keyMsg,
		alertType: constants.ALERT_TYPE_INFO,
		alertTitle: KeyTitle,
		yesLabel: okk,
		noLabel: "",
		alertHandler: handle2
	};
	//Defining pspConf parameter for alert
	var pspConf = {};
	//Alert definition
	var infoAlert = kony.ui.Alert(basicConf, pspConf);

	function handle2(response) {}
	return;
}

function showAlertRcMB(keyMsg, KeyTitle, alertType) {
  var okk = kony.i18n.getLocalizedString("keyOK");
  //Defining basicConf parameter for alert
  if(alertType == "info"){
    var type = constants.ALERT_TYPE_INFO;
  }
  if(alertType == "error"){
    var type = constants.ALERT_TYPE_ERROR;
  }
  var basicConf = {
    message: keyMsg,
    alertType: type,
    alertTitle: KeyTitle,
    yesLabel: okk,
    noLabel: "",
    alertHandler: handle2
  };
  //Defining pspConf parameter for alert
  var pspConf = {};
  //Alert definition
  var infoAlert = kony.ui.Alert(basicConf, pspConf);

  function handle2(response) {}
  return;
}