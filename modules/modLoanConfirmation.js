//Type your code here
function showLoanConfirmation(){
  invokeCustomerInfoByCaID();
  //dataPopulate();
  //frmApplicationCfrm.show();
}
function initLoanApplicationConfirm(){
  frmApplicationCfrm.postShow=postShowOfApplicationConfirm;
  frmApplicationCfrm.preShow=preShowOfApplicationConfirm;
  frmApplicationCfrm.onDeviceBack=doNothing;
  frmApplicationCfrm.btnCancel.onClick= handleLoanConfirmBack;
}

function handleLoanConfirmBack(){
  //showAlertWithYesNoHandler(getLocalizedString("ConfirmCancelFile"),"Confirmation",getLocalizedString("keyOK"),getLocalizedString("keyCancelButton"),navigateBackToSubProducts,doNothing);
  popupConfrmExitUpload.btnpopConfDelete.onClick = frmUploadDocuments_btnCancel_Action;
  showConfirmToBackPopup();
}

function postShowOfApplicationConfirm(){
   addAccessPinKeypad(frmApplicationCfrm);
   frmApplicationCfrm.btnConfirm.onClick=showAccessPinScreenKeypad;
}
function preShowOfApplicationConfirm() {
  	frmApplicationCfrm.btnBack.onClick= returnToUploadList;
    frmApplicationCfrm.flxBranch.setVisibility(false);
	frmApplicationCfrm.lblHeader.text = kony.i18n.getLocalizedString("Title");
	frmApplicationCfrm.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
	frmApplicationCfrm.btnConfirm.text = kony.i18n.getLocalizedString("TRConfirm_BtnConfirm");
	frmApplicationCfrm.lblProductHead.text = kony.i18n.getLocalizedString("Application_detail");
	frmApplicationCfrm.lblProductNameHead.text = kony.i18n.getLocalizedString("Product");
	frmApplicationCfrm.lblAppNoHead.text = kony.i18n.getLocalizedString("Application_no");
	frmApplicationCfrm.lblProductDescHead.text = kony.i18n.getLocalizedString("ApplyProduct");
	frmApplicationCfrm.lblApplyTypeHead.text = kony.i18n.getLocalizedString("Apply_type");
	frmApplicationCfrm.lblLoanAmontHead.text = kony.i18n.getLocalizedString("Loan_Amount");
	frmApplicationCfrm.lblLoanTenorHead.text = kony.i18n.getLocalizedString("Loan_Tenor");
	frmApplicationCfrm.lblCustomerInfoHead.text = kony.i18n.getLocalizedString("Customer_Information");
	frmApplicationCfrm.lblIDTypeHead.text = kony.i18n.getLocalizedString("ID_Type");
	frmApplicationCfrm.lblIDHead.text = kony.i18n.getLocalizedString("ID");
	frmApplicationCfrm.lblIssueCountryHead.text = kony.i18n.getLocalizedString("Issue_Country");
	frmApplicationCfrm.lblIssueDateHead.text = kony.i18n.getLocalizedString("Issue_date");
	frmApplicationCfrm.lblExpyDateHead.text = kony.i18n.getLocalizedString("Expired_date");
	frmApplicationCfrm.lblCusNameThaiHead.text = kony.i18n.getLocalizedString("Customer_TH_Name");
	frmApplicationCfrm.lblCusNameEngHead.text = kony.i18n.getLocalizedString("Customer_EN_Name");
	frmApplicationCfrm.lblDOBHead.text = kony.i18n.getLocalizedString("Date_of_birth");
	frmApplicationCfrm.lblPOBHead.text = kony.i18n.getLocalizedString("Birth_Place");
	frmApplicationCfrm.lblHighEducationHead.text = kony.i18n.getLocalizedString("Highest_Education");
	frmApplicationCfrm.lblSourceofIncomeHead.text = kony.i18n.getLocalizedString("Source_of_Income");
	frmApplicationCfrm.lblNationalityHead.text = kony.i18n.getLocalizedString("Nationality");
	frmApplicationCfrm.lblSecondNationalityHead.text = kony.i18n.getLocalizedString("2nd_Nationality");
	frmApplicationCfrm.lblMaritalStatusHead.text = kony.i18n.getLocalizedString("Marital_status");
	frmApplicationCfrm.lblContactInfo.text = kony.i18n.getLocalizedString("Contact_Information");
	frmApplicationCfrm.lblMobileNoHead.text = kony.i18n.getLocalizedString("Mobile_No");
	frmApplicationCfrm.lblEmailAddrsHead.text = kony.i18n.getLocalizedString("Email_Address");
	frmApplicationCfrm.lblStatementStatusHead.text = kony.i18n.getLocalizedString("Statement_status");
	frmApplicationCfrm.lblMailDeliveryHead.text = kony.i18n.getLocalizedString("Mail_Delivery");
	frmApplicationCfrm.lblCardDeliveryHead.text = kony.i18n.getLocalizedString("Card_Delivery");
	frmApplicationCfrm.lblContactAddrs.text = kony.i18n.getLocalizedString("Contact_Address");
	frmApplicationCfrm.lblAddressNumberHead.text = kony.i18n.getLocalizedString("Address_no");
	frmApplicationCfrm.lblMOOHead.text = kony.i18n.getLocalizedString("Moo");
	frmApplicationCfrm.lblSOIHead.text = kony.i18n.getLocalizedString("Soi");
	frmApplicationCfrm.lblRoadHead.text = kony.i18n.getLocalizedString("Road");
	frmApplicationCfrm.lblSubDistrictHead.text = kony.i18n.getLocalizedString("Sub_District");
	frmApplicationCfrm.lblDistrictHead.text = kony.i18n.getLocalizedString("District");
	frmApplicationCfrm.lblProvinceHead.text = kony.i18n.getLocalizedString("Province");
	frmApplicationCfrm.lblPostalHead.text = kony.i18n.getLocalizedString("Postal");
	frmApplicationCfrm.lblTelephoneNoHead.text = kony.i18n.getLocalizedString("Telephone_no");
	frmApplicationCfrm.lblExtNoHead.text = kony.i18n.getLocalizedString("Ext_no");
	frmApplicationCfrm.lblLivingStatusHead.text = kony.i18n.getLocalizedString("Living_status");
	frmApplicationCfrm.lblWorkingInfo.text = kony.i18n.getLocalizedString("Working_Information");
	frmApplicationCfrm.lblOccupationHead.text = kony.i18n.getLocalizedString("Occupation");
	frmApplicationCfrm.lblPositionHead.text = kony.i18n.getLocalizedString("Position");
	frmApplicationCfrm.lblCompanyHead.text = kony.i18n.getLocalizedString("Company");
	frmApplicationCfrm.lblDurationEmployHead.text = kony.i18n.getLocalizedString("Duration_employ");
	frmApplicationCfrm.lblEmployContractHead.text = kony.i18n.getLocalizedString("Employ_Contract");
	frmApplicationCfrm.lblStartDOEHead.text = kony.i18n.getLocalizedString("Start_date_employ");
	frmApplicationCfrm.lblEndDOEHead.text = kony.i18n.getLocalizedString("End_date_employ");
	frmApplicationCfrm.lblIncomeInfo.text = kony.i18n.getLocalizedString("Income_Information");
	frmApplicationCfrm.lblMonthlyIncomeHead.text = kony.i18n.getLocalizedString("Monthly_Income");
    frmApplicationCfrm.lblColaAllowance.text = kony.i18n.getLocalizedString("COLA");
	frmApplicationCfrm.lblBonusPerYearHead.text = kony.i18n.getLocalizedString("Bonus_per_year");
	frmApplicationCfrm.lblOtherIncomeHead.text = kony.i18n.getLocalizedString("Other_Income");
	//frmApplicationCfrm.lblIncomeTypeHead.text = kony.i18n.getLocalizedString("Income_Type");
	frmApplicationCfrm.lblBankHead.text = kony.i18n.getLocalizedString("Bank");
	frmApplicationCfrm.lblAccountNumberHead.text = kony.i18n.getLocalizedString("Account_no");
	frmApplicationCfrm.lblPersonalLoanOnly.text = kony.i18n.getLocalizedString("Receive_by");
	frmApplicationCfrm.lblPersonalLoanOnlyDesc.text = kony.i18n.getLocalizedString("Receive_by_content");
	frmApplicationCfrm.lblBankNameHead.text = kony.i18n.getLocalizedString("Bank");
	frmApplicationCfrm.lblAccountNameHead.text = kony.i18n.getLocalizedString("Account_name_label");
	frmApplicationCfrm.lblAcctNumberHead.text = kony.i18n.getLocalizedString("Account_no");
	frmApplicationCfrm.lblBranchHead.text = kony.i18n.getLocalizedString("Branch");
	frmApplicationCfrm.lblPayment.text = kony.i18n.getLocalizedString("Payment");
	frmApplicationCfrm.lblDirectDebitAccntHead.text = kony.i18n.getLocalizedString("Direct_debit_account_no");
	frmApplicationCfrm.lblDirectDebitAccntNameHead.text = kony.i18n.getLocalizedString("Direct_debit_account_name");
    frmApplicationCfrm.lblDeclaredIncome.text = kony.i18n.getLocalizedString("Declared_Income");
    frmApplicationCfrm.lblAvgSixMonthCash.text = kony.i18n.getLocalizedString("Average_Cash_Inflow");
    frmApplicationCfrm.lblShareHolderPerc.text = kony.i18n.getLocalizedString("Shareholder_Percentage");
	//frmApplicationCfrm.lblPaymentDesc.text = kony.i18n.getLocalizedString("Condition_Payment");
	
	//dataPopulate();
}

function dataPopulate(customerInfo) {
	try {
		var locale = kony.i18n.getCurrentLocale();
		//product
		frmApplicationCfrm.lblAppNo.text = customerInfo["appRefNo"];
		if (locale == "th_TH") {
			frmApplicationCfrm.lblProductDesc.text = customerInfo["productDescTH"];
		} else {
			frmApplicationCfrm.lblProductDesc.text = customerInfo["productDescEN"];
		}
		frmApplicationCfrm.lblApplyType.text = customerInfo["appTypeDesc"];
		gblLoanType = customerInfo["appType"];
		frmApplicationCfrm.lblLoanAmont.text = commaFormatted(customerInfo["LoanInfoDS"][0]["limitApplied"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
        frmApplicationCfrm.lblLoanTenor.text = customerInfo["LoanInfoDS"][0]["tenure"];

		//customer info
		if (undefined != customerInfo["idType1"] && "CI" === customerInfo["idType1"]) {
			frmApplicationCfrm.lblIDType.text = getLocalizedString("MB_eDCI");
			frmApplicationCfrm.lblID.text = formatCitizenID(customerInfo["idNo1"]);
		} else {
			frmApplicationCfrm.lblIDType.text = getLocalizedString("Passportno");
			frmApplicationCfrm.lblID.text = customerInfo["idNo1"];
		}

		frmApplicationCfrm.lblID.text = formatCitizenID(customerInfo["idNo1"]);
		
		if (locale == "th_TH") {
			frmApplicationCfrm.lblIssueCountry.text = customerInfo["issueCountryNameTH"];
			frmApplicationCfrm.lblPOB.text = customerInfo["birthPlaceTH"];
			frmApplicationCfrm.lblSourceofIncome.text = customerInfo["srcFromCountryTH"];
			frmApplicationCfrm.lblNationality.text = customerInfo["nationalityTH"];
          if(isNotBlank(customerInfo["nationality2TH"])){
            frmApplicationCfrm.lblSecondNationality.text = customerInfo["nationality2TH"];
          }else{
            frmApplicationCfrm.lblSecondNationality.text = "[-]";
          }
			frmApplicationCfrm.lblIssueDate.text = customerInfo["issuedDateFmtTH"];
			frmApplicationCfrm.lblExpyDate.text = customerInfo["expiryDateFmtTH"];
			frmApplicationCfrm.lblDOB.text = customerInfo["birthDateFmtTH"];

			frmApplicationCfrm.lblOccupation.text = customerInfo["empOccupationTH"];
			frmApplicationCfrm.lblPosition.text = customerInfo["profCodeTH"];
			frmApplicationCfrm.lblHighEducation.text = customerInfo["educationLevelTH"];
			frmApplicationCfrm.lblLivingStatus.text = customerInfo["residentTypeTH"];
          	frmApplicationCfrm.lblMaritalStatus.text = customerInfo["maritalStatusTH"];
		} else {
			frmApplicationCfrm.lblIssueCountry.text = customerInfo["issueCountryNameEN"];
			frmApplicationCfrm.lblPOB.text = customerInfo["birthPlaceEN"];
			frmApplicationCfrm.lblSourceofIncome.text = customerInfo["srcFromCountryEN"];
			frmApplicationCfrm.lblNationality.text = customerInfo["nationalityEN"];
          if(isNotBlank(customerInfo["nationality2EN"])){
            frmApplicationCfrm.lblSecondNationality.text = customerInfo["nationality2EN"];
          }else{
            frmApplicationCfrm.lblSecondNationality.text = "[-]";
          }		
			//frmApplicationCfrm.lblMaritalStatus.text =customerInfo["maritalStatusEN"];
			frmApplicationCfrm.lblIssueDate.text = customerInfo["issuedDateFmtEN"];
			frmApplicationCfrm.lblExpyDate.text = customerInfo["expiryDateFmtEN"];
			frmApplicationCfrm.lblDOB.text = customerInfo["birthDateFmtEN"];

			frmApplicationCfrm.lblOccupation.text = customerInfo["empOccupationEN"];
			frmApplicationCfrm.lblPosition.text = customerInfo["profCodeEN"];
			frmApplicationCfrm.lblHighEducation.text = customerInfo["educationLevelEN"];
			frmApplicationCfrm.lblLivingStatus.text = customerInfo["residentTypeEN"];
          	frmApplicationCfrm.lblMaritalStatus.text = customerInfo["maritalStatusEN"];
		}
		kony.print("employmentName : " + customerInfo["employmentName"]);

		frmApplicationCfrm.lblCompany.text = customerInfo["employmentName"];

		frmApplicationCfrm.lblCusNameThai.text = customerInfo["custTHName"]; //+ " "+customerInfo["thaiSurName"]; //gblPersonalInfo.txtThaiName + " " + gblPersonalInfo.txtThaiSurName;
		frmApplicationCfrm.lblCusNameEng.text = customerInfo["custENName"]; //+" "+customerInfo["engSurName"];  //gblPersonalInfo.txtEngName + " " + gblPersonalInfo.txtEngSurName;

		//frmApplicationCfrm.lblHighEducation.text =customerInfo["educationLevel"]; // gblHighestEducation.status;

		//contact info
		frmApplicationCfrm.lblMobileNo.text = getBraketsIfEmptyForMobileNo(customerInfo["mobileNo"]); // gblMobileData.mobileNumber;
		frmApplicationCfrm.lblEmailAddrs.text = customerInfo["email"]; // gblPersonalInfo.txtEmail;
		if (undefined != customerInfo["emailStatementFlag"] && "Y" === customerInfo["emailStatementFlag"]) {
			frmApplicationCfrm.lblStatementStatus.text = getLocalizedString("keyYes");
		} else {
			frmApplicationCfrm.lblStatementStatus.text = getLocalizedString("keyNo");
		}

		//contact address
		kony.print(JSON.stringify(customerInfo["addresses"]));

		kony.print("homeAddressLine1 : " + customerInfo["homeAddressLine1"] + " , homeAddressLine2 : " + customerInfo["homeAddressLine2"]);
		frmApplicationCfrm.lblHomeAddressLine1.text = customerInfo["homeAddressLine1"];
		frmApplicationCfrm.lblHomeAddressLine2.text = customerInfo["homeAddressLine2"] + " " + kony.i18n.getLocalizedString("Thailand");
      
        frmApplicationCfrm.lblWorkAddressLine1.text = customerInfo["officeAddressLine1"];
		frmApplicationCfrm.lblWorkAddressLine2.text = customerInfo["officeAddressLine2"];

		frmApplicationCfrm.lblTelephoneNo.text = customerInfo["empTelephoneNo"];
		frmApplicationCfrm.lblExtNo.text = customerInfo["empTelephoneExtNo"];

		var years = customerInfo["employmentYear"];
		var months = customerInfo["employmentMonth"];
		frmApplicationCfrm.lblDurationEmploy.text = years + " " + kony.i18n.getLocalizedString("Years_textbox") + " " + months + " " + kony.i18n.getLocalizedString("Months_textbox");

		//Income Info

		if (customerInfo["employmentStatus"].trim() == "01") {
			frmApplicationCfrm.flxEmployContract.setVisibility(true);
			frmApplicationCfrm.flxStartDOE.setVisibility(true);
			frmApplicationCfrm.flxEndDOE.setVisibility(true);
          	frmApplicationCfrm.lblStartDOE.text = customerInfo["contractStartDateFmt"];
			frmApplicationCfrm.lblEndDOE.text = customerInfo["contractEndDateFmt"];
          if(customerInfo["contractEmployedFlag"] == "Y"){
            frmApplicationCfrm.lblEmployContract.text = kony.i18n.getLocalizedString("Contract_Employed_title");
          }else if(customerInfo["contractEmployedFlag"] == "N"){
            frmApplicationCfrm.lblEmployContract.text = kony.i18n.getLocalizedString("Employee_title");
            frmApplicationCfrm.lblStartDOE.text = "[-]";
			frmApplicationCfrm.lblEndDOE.text = "[-]";
          }
			frmApplicationCfrm.flxDeclaredIncome.setVisibility(false);
			frmApplicationCfrm.flxAvgSixMonths.setVisibility(false);
			frmApplicationCfrm.flxShareHolderPerc.setVisibility(false);
			frmApplicationCfrm.flxMonthlyIncome.setVisibility(true);
			frmApplicationCfrm.flxColaAllowance.setVisibility(true);
			frmApplicationCfrm.flxBonusPerYear.setVisibility(true);
			frmApplicationCfrm.flxOtherIncome.setVisibility(true);
			frmApplicationCfrm.lblMonthlyIncome.text = commaFormatted(customerInfo["incomeBasicSalary"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
// 			frmApplicationCfrm.lblBonusPerYear.text = commaFormatted(customerInfo["bonusYearly"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
            frmApplicationCfrm.lblBonusPerYear.text = getBraketsIfEmptyForAmount(customerInfo["bonusYearly"]);
// 			frmApplicationCfrm.lblOtherIncome.text = commaFormatted(customerInfo["incomeOtherIncome"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
            frmApplicationCfrm.lblOtherIncome.text = getBraketsIfEmptyForAmount(customerInfo["incomeExtraOther"]);
			//frmApplicationCfrm.lblIncomeType.text = kony.i18n.getLocalizedString("Employee_label");
// 			frmApplicationCfrm.lblColaAllowanceInfo.text = commaFormatted(customerInfo["incomeExtraOther"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
            frmApplicationCfrm.lblColaAllowanceInfo.text = getBraketsIfEmptyForAmount(customerInfo["incomeOtherIncome"]);
		} else {
			frmApplicationCfrm.flxEmployContract.setVisibility(false);
			frmApplicationCfrm.flxStartDOE.setVisibility(false);
			frmApplicationCfrm.flxEndDOE.setVisibility(false);

			frmApplicationCfrm.flxDeclaredIncome.setVisibility(true);
			frmApplicationCfrm.flxAvgSixMonths.setVisibility(true);
			frmApplicationCfrm.flxShareHolderPerc.setVisibility(true);
			frmApplicationCfrm.flxMonthlyIncome.setVisibility(false);
			frmApplicationCfrm.flxColaAllowance.setVisibility(false);
			frmApplicationCfrm.flxBonusPerYear.setVisibility(false);
			frmApplicationCfrm.flxOtherIncome.setVisibility(false);
			frmApplicationCfrm.lblDeclaredIncomeInfo.text = getBraketsIfEmptyForAmount(customerInfo["incomeDeclared"]);
			frmApplicationCfrm.lblAvgSixMonthCashInfo.text = getBraketsIfEmptyForAmount(customerInfo["incometotalLastMthCreditAcct1"]);
			frmApplicationCfrm.lblShareHolderPercInfo.text = customerInfo["incomeSharedHolderPercent"];
			//frmApplicationCfrm.lblIncomeType.text = kony.i18n.getLocalizedString("Self_Employed_label");
		}
        var accountNo = customerInfo["incomeBankAccoutNumber"];
        if(accountNo.length > 10){
           var formattedAccountno = accountNo;
        }else{
           var formattedAccountno = formatAccountNo(accountNo);
        }
		frmApplicationCfrm.lblAccountNumber.text = formattedAccountno;
		if (locale == "th_TH") {
			frmApplicationCfrm.lblBank.text = gblLoanIncomeInformation["bankNameTH"];
		} else {
			frmApplicationCfrm.lblBank.text = gblLoanIncomeInformation["bankNameEN"];
		}
		//Income Info End;
		if (gblLoanType !== "") {
          if (gblLoanType === "PL") {
            frmApplicationCfrm.flxOtherLoanDetails.isVisible = true;
            frmApplicationCfrm.lblOtherLoan.text = kony.i18n.getLocalizedString("Other_Loan_label");
            //frmApplicationCfrm.lblOtherLoanVal.text = customerInfo["loanWithOtherBank"]
            frmApplicationCfrm.lblConsiderLoanlabel.text = kony.i18n.getLocalizedString("Consider_Loan_label");
            //frmApplicationCfrm.lblConsiderLoanlabelVal.text = customerInfo["considerLoanWithOtherBank"]
            if(customerInfo["LoanInfoDS"][0].loanWithOtherBank=="1"){
              frmApplicationCfrm.lblOtherLoanVal.text  =  kony.i18n.getLocalizedString("Select_None");
            }else if(customerInfo["LoanInfoDS"][0].loanWithOtherBank=="2"){
              frmApplicationCfrm.lblOtherLoanVal.text  =  kony.i18n.getLocalizedString("Select_One_label");
            }else if(customerInfo["LoanInfoDS"][0].loanWithOtherBank=="3"){
              frmApplicationCfrm.lblOtherLoanVal.text  =  kony.i18n.getLocalizedString("Select_More_than_three_label");
            }

            if(customerInfo["LoanInfoDS"][0].considerLoanWithOtherBank=="1"){
              frmApplicationCfrm.lblConsiderLoanlabelVal.text  =  kony.i18n.getLocalizedString("Select_None");
            }else if(customerInfo["LoanInfoDS"][0].considerLoanWithOtherBank=="2"){
              frmApplicationCfrm.lblConsiderLoanlabelVal.text  =  kony.i18n.getLocalizedString("Select_One_label");
            }else if(customerInfo["LoanInfoDS"][0].considerLoanWithOtherBank=="3"){
              frmApplicationCfrm.lblConsiderLoanlabelVal.text  =  kony.i18n.getLocalizedString("Select_More_than_three_label");
            }
            if (customerInfo["product"] !== null && customerInfo["product"] == "RC") {
              //frmApplicationCfrm.lblPaymentDesc.text=kony.i18n.getLocalizedString("Condition_Payment_RC");
              frmApplicationCfrm.flxLoanAmont.setVisibility(false);
              frmApplicationCfrm.flxLoanTenor.setVisibility(false);
              kony.print("Hiding the Account to Receive Loan for RC");
              frmApplicationCfrm.flxPersonalLoanOnly.setVisibility(false);
              frmApplicationCfrm.flxPersonalLoanOnlyContainer.setVisibility(false);
              frmApplicationCfrm.flxMailDelivery.setVisibility(true);
			  frmApplicationCfrm.flxCardDelivery.setVisibility(true);
            } else {
              frmApplicationCfrm.lblBankName.text = customerInfo["bankNameDisplay"];
              frmApplicationCfrm.lblAccountName.text = customerInfo["LoanInfoDS"][0]["disburstAccountNameFull"];
              frmApplicationCfrm.lblAcctNumber.text = formatAccountNo(customerInfo["LoanInfoDS"][0]["disburstAccountNo"]);
              frmApplicationCfrm.lblBranch.text ="[-]";// customerInfo["LoanInfoDS"][0]["accountName"];

              frmApplicationCfrm.flxPersonalLoanOnly.isVisible = true;
              frmApplicationCfrm.flxPersonalLoanOnlyContainer.isVisible = true;
              frmApplicationCfrm.lblPaymentDesc.isVisible = false;
              frmApplicationCfrm.flxCardDelivery.isVisible = false;
              frmApplicationCfrm.flxMailDelivery.setVisibility(true);
              frmApplicationCfrm.flxLoanAmont.setVisibility(true);
              frmApplicationCfrm.flxLoanTenor.setVisibility(true);
            }
            //frmApplicationCfrm.flxLoanAmont.isVisible=true;
            //frmApplicationCfrm.flxLoanTenor.isVisible=true;
          } else if (gblLoanType === "CC") {

				frmApplicationCfrm.flxCardDelivery.isVisible = true;
            	frmApplicationCfrm.flxMailDelivery.setVisibility(true);
				
				//frmApplicationCfrm.lblMailDelivery.text =customerInfo["mailingPreference"];
				//frmApplicationCfrm.lblCardDelivery.text = customerInfo["cardDelivery"];
				frmApplicationCfrm.flxPersonalLoanOnly.isVisible = false;
				frmApplicationCfrm.flxPersonalLoanOnlyContainer.isVisible = false;
				frmApplicationCfrm.flxOtherLoanDetails.isVisible = false;
				frmApplicationCfrm.flxLoanAmont.isVisible = false;
				frmApplicationCfrm.flxLoanTenor.isVisible = false;
				//frmApplicationCfrm.lblPaymentDesc.text=kony.i18n.getLocalizedString("Condition_Payment_CC")
			}
		}
      if (locale == "th_TH") {
          frmApplicationCfrm.lblMailDelivery.text = customerInfo["mailPrefTH"];
          frmApplicationCfrm.lblCardDelivery.text = customerInfo["cardDeliveryAddTH"];
      } else {
          frmApplicationCfrm.lblMailDelivery.text = customerInfo["mailPrefEN"];
          frmApplicationCfrm.lblCardDelivery.text = customerInfo["cardDeliveryAddEN"];
      }
      kony.print("mailPreference : "+customerInfo["mailPreference"]+" , mailPrefTH : "+customerInfo["mailPrefTH"]+" , mailPrefEN : "+customerInfo["mailPrefEN"]
                 +" , cardDeviliverAdd : "+customerInfo["cardDeviliverAdd"]+" , cardDeliveryAddTH : "+customerInfo["cardDeliveryAddTH"]
                 +" , cardDeliveryAddEN : "+customerInfo["cardDeliveryAddEN"]);

		if (customerInfo["LoanInfoDS"][0]["paymentMethod"].trim() === "1") {
			frmApplicationCfrm.flxLetterofConcent.setVisibility(true);
			frmApplicationCfrm.lblPaymentval.setVisibility(false);
			frmApplicationCfrm.flxPaymentContainer.isVisible = true;
			if (gblLoanType === "PL") {
              	frmApplicationCfrm.lblDirectDebitAccnt.text = formatAccountNo(customerInfo["LoanInfoDS"][0]["paymentAccountNo"]);
				frmApplicationCfrm.lblDirectDebitAccntName.text = customerInfo["LoanInfoDS"][0]["paymentAccountNameFull"];
			} else {
				frmApplicationCfrm.lblDirectDebitAccnt.text = formatAccountNo(customerInfo["LoanInfoDS"][0].debitAccountNo);
				frmApplicationCfrm.lblDirectDebitAccntName.text = customerInfo["LoanInfoDS"][0].paymentAccountNameFull;
			}
		} else {
          	frmApplicationCfrm.flxLetterofConcent.setVisibility(false);
          	if (gblLoanType === "PL") {
                  frmApplicationCfrm.flxLetterofConcent.setVisibility(true);
             }
			frmApplicationCfrm.lblPaymentval.setVisibility(true);
			frmApplicationCfrm.lblPayment.text = kony.i18n.getLocalizedString("Payment");
			frmApplicationCfrm.flxPaymentContainer.isVisible = false;
		}
      
        if(customerInfo["ncbConsentFlag"] == "Y"){
        frmApplicationCfrm.lblConcentTag.text = kony.i18n.getLocalizedString("NCB_Model_Accept");
        }else{
        frmApplicationCfrm.lblConcentTag.text = kony.i18n.getLocalizedString("NCB_Model_NotAccept");
        }

		frmApplicationCfrm.rtxtLetterCompleteDesc.text = customerInfo["fileContent"];
		frmApplicationCfrm.rtxtOwnerLetterCompleteDesc.text = customerInfo["fileConsentContent"];
		frmApplicationCfrm.lblPaymentDesc.text=customerInfo["payMethodCriteriaText"];
		
	} catch (e) {
		alert("Exception at dataPopulate" + e);
	}
}

function invokeCustomerInfoByCaID(){
    var inputParam={};
  inputParam["caID"] =  gblCaId;
   var locale = kony.i18n.getCurrentLocale();
    if (locale == "en_US") {
        inputParam["localeCd"] = "en_US";
    } else {
        inputParam["localeCd"] = "th_TH";
    }
  //inputParam["module"] = "3";  
 inputParam["applicationProduct"]=gblSelectedLoanProduct;
  showLoadingScreen();
  invokeServiceSecureAsync("GetLoanConfirmDetails", inputParam, callbacksearchCustomerInfoByCaID);
} 
function callbacksearchCustomerInfoByCaID(status,result){
  if (status == 400) {
    if (result["opstatus"] === 0 ||result["opstatus"] == "0") {
      dismissLoadingScreen();
      dataPopulate(result);
      frmApplicationCfrm.show();
    } 
    
  } 
  dismissLoadingScreen();
}

function invokeLoanConfirmation(txnPwd){
    var inputParam={};
  inputParam["loginModuleId"] = "MB_TxPwd";
  inputParam["password"] =  encryptData(txnPwd);  
    var locale = kony.i18n.getCurrentLocale();
    if (locale == "en_US") {
        inputParam["localeCd"] = "en_US";
    } else {
        inputParam["localeCd"] = "th_TH";
    }
  inputParam["caID"]=gblCaId;
  var moduleKey="";
  if(gblLoanAppType =="27"){
      moduleKey = "TMBPersonalLoan"; 
  }else {
      moduleKey = "TMBCreditLoan"; 
  }
  inputParam["moduleKey"] = moduleKey;
  showLoadingScreen();
  invokeServiceSecureAsync("LoanSubmitApplicationJavaService", inputParam, callbackLoanConfirmation);
} 
function showLoanComplete(){
        frmApplicationFormComplete.btnCancel1.onClick  =navigateBackToSubProducts;
//       frmApplicationFormComplete.btnStatusTrackinglink.onClick  =checkLoanStatus();
      frmApplicationFormComplete.lblApplicationForm.text = kony.i18n.getLocalizedString("CUL02_txtTitle");
      frmApplicationFormComplete.btnCancel1.text = kony.i18n.getLocalizedString("GotoLoan");
      frmApplicationFormComplete.show();
}
function callbackLoanConfirmation(status,result){
  if (status == 400) {
    if (result["opstatus"] === 0 ||result["opstatus"] == "0") {
      var locale = kony.i18n.getCurrentLocale();
      dismissLoadingScreen();
      closeApprovalKeypad();
      frmApplicationFormComplete.lblSuccesandcompleteicon.text = "A";
      frmApplicationFormComplete.lblSuccesandcompleteicon.skin = "skinIconFontCompleteLone";
      frmApplicationFormComplete.ImgInComplete.setVisibility(false);
      frmApplicationFormComplete.lblSuccesandcompleteicon.setVisibility(true);
      if(locale == "th_TH"){
         var productName=result.productDescTH;
      }else if(locale == "en_US"){
        var productName=result.productDescEN;
      }
      var successcode = kony.i18n.getLocalizedString("Loan_Result_Desc_Success");
      if(locale == "th_TH"){
         successcode = replaceAll(successcode, "<Product Name TH>", productName);
      }else if(locale == "en_US"){
         successcode = replaceAll(successcode, "<Product Name EN>", productName);
      }
      frmApplicationFormComplete.lblTrackingMessage.text = successcode;
	  showLoanComplete();
    }else if (result["opstatus"] === 2 || result["opstatus"] == "2") {
//       frmApplicationFormComplete.lblSuccesandcompleteicon.text = "X";
//       frmApplicationFormComplete.lblSuccesandcompleteicon.skin = "skinIconFontINCompleteLone";
      frmApplicationFormComplete.ImgInComplete.setVisibility(true);
      frmApplicationFormComplete.lblSuccesandcompleteicon.setVisibility(false);
      frmApplicationFormComplete.lblTrackingMessage.text = kony.i18n.getLocalizedString("Loan_Result_Desc_Err");
       showLoanComplete();
       dismissLoadingScreen();
    }else if (result["opstatus"] === 1 || result["opstatus"] == "1") {
      
      if(result["errCode"] == "E10020" || result["errMsg"] == "Wrong Password" ){
        //Wrong password Entered 

        resetKeypadApproval();
        var badLoginCount = result["badLoginCount"];
        var incorrectPinText = kony.i18n.getLocalizedString("PIN_Incorrect");
        incorrectPinText = incorrectPinText.replace("{rem_attempt}", gblTotalPinAttempts - badLoginCount);
        frmApplicationCfrm.lblForgotPin.text = incorrectPinText;
        frmApplicationCfrm.lblForgotPin.skin = "lblBlackMed150NewRed";
        frmApplicationCfrm.lblForgotPin.onTouchEnd = doNothing;
        dismissLoadingScreen();

      }else if(result["errCode"] == "E10403" || result["errMsg"] == "Password Locked" ){
        // password Locked 
        closeApprovalKeypad();
        dismissLoadingScreen();
        //frmMBManageDebitCardFailure.rchAddress.text = kony.i18n.getLocalizedString("ChangeDBL_Failed");
        gotoUVPINLockedScreenPopUp();

      }else {
        dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        return false;
      }
    } else {
        dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        return false;
      }
  }else {
      dismissLoadingScreen();
      showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
      return false;
    }

}