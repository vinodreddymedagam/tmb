// mutual fund switch services

function MFFundValidationFOrTargetFund(){
  try{
  var inputParam = {};
  showLoadingScreen();

  inputParam["unitHolderNo"] = frmMFSwitchLanding.lblPortfolioCode.text;
  if (gblMFSwitchOrder["orderType"] == MF_ORD_TYPE_SWITCH) {
    inputParam["orderType"] = MF_EVENT_SWI;   
  }    
  inputParam["fundHouseCode"] = gblMFSwitchOrder["targetFundHouseCode"]
  inputParam["fundCode"] = gblMFSwitchOrder["targetFundCode"];
  invokeServiceSecureAsync("MFSwitchFundValidation", inputParam, MFFundValidationFOrTargetFundCallBack);
  }catch(e){
    kony.print("error in MFFundValidationFOrTargetFund "+e);
  }
}

function MFFundValidationFOrTargetFundCallBack(status,resulttable){
  try{
  if (status == 400) {
    dismissLoadingScreen();
    if ( (resulttable["opstatus"] == 0) ) {
      var targetFundRulesData = resulttable;
      gblMFSwitchOrder["suitabilityScore"]=targetFundRulesData["suitabilityScore"];
      gblMFSwitchOrder["expiryDays"]=targetFundRulesData["suitabilityExpireDate"];
      gblMFSwitchOrder["overAge"] = targetFundRulesData["overAge"];
      

      if(targetFundRulesData["effectiveDate"] != undefined &&    targetFundRulesData["transactionDate"] != undefined) {
        effectiveDate = targetFundRulesData["effectiveDate"].substring(0,10);
        transactionDate = targetFundRulesData["transactionDate"].substring(0,10);
        gblMFSwitchOrder["effectiveDate"] = effectiveDate;
        gblMFSwitchOrder["transactionDate"] = transactionDate;
      }
      if((gblMFEventFlag == MF_EVENT_SWI || gblMFEventFlag == MF_EVENT_SWO) &&     targetFundRulesData["suitabilityExpireFlag"] == 'Y' ) {
        gblIsExpiry = true;
        var yyyyMMdd = targetFundRulesData["suitabilityExpireDate"].split("-");
        var expireDate = yyyyMMdd[2] + "/" + yyyyMMdd[1] + "/" + yyyyMMdd[0];
        getMFSuitibilityReviewExpiry(expireDate);
      } else if(targetFundRulesData["allowTransFlag"] == '3' || gblMFSwitchData["allowTransFlag"] == '3'){ //MKI, MIB-12551
        showAlert(kony.i18n.getLocalizedString("MFSW01-003"), kony.i18n.getLocalizedString("info")); //MKI, MIB-12551
      }else if(targetFundRulesData["allowTransFlag"] != '0' || gblMFSwitchData["allowTransFlag"] != '0'){
        showAlert(kony.i18n.getLocalizedString("MFSW01-002"), kony.i18n.getLocalizedString("info"));
      } else if(targetFundRulesData["fundCode"] == "" || gblMFSwitchData["fundCode"] == "") {
        showAlert("Cannot check fund suitability", kony.i18n.getLocalizedString("info"));
      } else if(effectiveDate != transactionDate){ 
        popupConfirmMutualFundOrder(callBackOnConfirmOverTimeOrderSwitch);
      }else {
        MutualFundTnC();
      }
    } 
    else {
      if ( (resulttable["errCode"] == 21) ) { //MKI, MIB - 10144 start
        dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("MF_P_ERR_01004"), kony.i18n.getLocalizedString("info"));
      }
      else if ( (resulttable["errCode"] == 22) ) { //MKI,MIB-11357 start
        dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("MF_ERR_UHNO_NOTALLOW"), kony.i18n.getLocalizedString("info"));
      } //MKI,MIB-11357 End
      else if ( (resulttable["errCode"] == 1) ) {
        dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("MF_P_ERR_01005"), kony.i18n.getLocalizedString("info"));
      }
      else{
        dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
      } //MKI, MIB - 10144 End
    }
  } else {
    dismissLoadingScreen();
  }	
  }catch(e){
     kony.print("error in MFFundValidationFOrTargetFundCallBack "+e);
  }

}




function saveToSessionSwitchMFOrder(){
  try{
  kony.print("saveToSessionSwitchMFOrder  start");
  showLoadingScreen();	
  inputParam = {};
  inputParam["amount"] = gblMFSwitchOrder["amount"];
  inputParam["orderType"] = MF_EVENT_SWI;
  inputParam["fundCode"] = gblMFSwitchOrder["sourceFundCode"];
  inputParam["unitHolderNo"] = gblMFSwitchOrder["sourceUnitHolderNo"];
  inputParam["fundShortName"] = gblMFSwitchOrder["sourceFundShortName"];
  inputParam["FundHouseCode"] = gblMFSwitchOrder["sourceFundHouseCode"];
  inputParam["FundNameEn"] = gblMFSwitchOrder["sourceFundNameEn"];
  inputParam["FundNameTh"] = gblMFSwitchOrder["sourceFundNameTh"];
  inputParam["targetFundCode"] = gblMFSwitchOrder["targetFundCode"];
  inputParam["targetFundNameTh"] = gblMFSwitchOrder["targetFundNameTh"];
  inputParam["targetFundNameEn"] = gblMFSwitchOrder["targetFundNameEn"];
  inputParam["targetFundHouseCode"] = gblMFSwitchOrder["targetFundHouseCode"];
  inputParam["targetUnitHolderNo"] = gblMFSwitchOrder["targetUnitHolderNo"];
  inputParam["targetFundShortName"] =gblMFSwitchOrder["targetFundShortName"];
  inputParam["effectiveDate"] =gblMFSwitchOrder["effectiveDate"];
  inputParam["transactionDate"] =gblMFSwitchOrder["transactionDate"] ;
  if(gblMFSwitchOrder["redeemUnit"] == ORD_UNIT_ALL) {
    inputParam["redeemType"] = ORD_UNIT_UNIT;
  } else {
    inputParam["redeemType"] = gblMFSwitchOrder["redeemUnit"];
  }

  invokeServiceSecureAsync("MFSwitchOrderValidationService", inputParam, MFSwitchOrderValidationServiceCallBack);
  kony.print("saveToSessionSwitchMFOrder  end");
  }catch(e){
    kony.print("error in saveToSessionSwitchMFOrder "+e);
  }
}

function MFSwitchOrderValidationServiceCallBack(status, resulttable) {
  try{
  if (status == 400) {
    dismissLoadingScreen();
    if (resulttable["opstatus"] == 0) {
      kony.print("MFSwitchOrderValidationServiceCallBack  start");
      frmMFSwitchConfirmMB.show();  
    }
  }
  }catch(e){
    kony.print("error in MFSwitchOrderValidationServiceCallBack "+e);
  }
}

function MBconfirmSwitchMFOrderCompositeJavaService(pwd){
  try{
  var inputParam = {};
  showLoadingScreen();	
  inputParam["channel"] = "rc";
  inputParam["password"] = pwd;
  inputParam["verifyPwd_retryCounterVerifyOTP"] = "rc";
  inputParam["verifyPwdMB_appID"] = appConfig.appId;
  inputParam["verifyPwdMB_channel"] = "MB";

  inputParam["verifyPwd_tokenUUID"] = appConfig.appId;
  inputParam["fundHouseCode"] = gblMFSwitchOrder["sourceFundHouseCode"];
  inputParam["fundCode"] = gblMFSwitchOrder["sourceFundCode"];
  inputParam["unitHolderNo"] = gblMFSwitchOrder["sourceUnitHolderNo"];
  if(gblMFOrder["redeemUnit"] == "A"){
    inputParam["redeemType"] = "U";
  } else { 
    inputParam["redeemType"] = gblMFSwitchOrder["redeemUnit"];
  }
  inputParam["redeemAmount"] =gblMFSwitchOrder["amount"];
  inputParam["suitabilityScore"] =gblMFSwitchOrder["suitabilityScore"];
  inputParam["expiryDays"] =gblMFSwitchOrder["expiryDays"];
  inputParam["orderType"] = gblMFSwitchOrder["orderType"];
  inputParam["targetfundHouseCode"] =gblMFSwitchOrder["targetFundHouseCode"];;
  inputParam["targetfundCode"] = gblMFSwitchOrder["targetFundCode"];;
  inputParam["crmProfileMod_channel"] = "MB";
  inputParam["crmProfileMod_appID"] = appConfig.appId;
  inputParam["mforder_fundHouseCode"] = gblMFSwitchOrder["targetFundHouseCode"];;
  inputParam["fundShortName"] = gblMFSwitchOrder["sourceFundShortName"];
  inputParam["serviceID"] = "MFOrderCompositeService";
  invokeServiceSecureAsync("MFSwitchOrderService", inputParam, MBconfirmSwitchMFOrderCompositeJavaServiceCallBack);
  }catch(e){
    kony.print("error in MBconfirmSwitchMFOrderCompositeJavaService "+e);
  }
}


function MBconfirmSwitchMFOrderCompositeJavaServiceCallBack(status,resulttable){
  try{

  if (status == 400) {
    kony.print("result is ::: "+JSON.stringify(resulttable));
    if (resulttable["opstatus"] == 0) {
      dismissLoadingScreen();
      
      popupTractPwd.dismiss();
      onClickCancelAccessPin();

      /*  if (GBL_MF_TEMENOS_ENABLE_FLAG != "ON" && gblMFOrder["gblMFSwitchOrder"] == "TMBAM" && resulttable["errMsg"] != "OK"){
        return;;				
      }else{*/				
      if (gblMFSwitchOrder["fundHouseCode"] == "TMBAM"){


        gblMFSwitchOrder["refID"] = resulttable["transactionRef"];
        gblMFSwitchOrder["orderTime"] = resulttable["transactionDate"].substring(10, resulttable["transactionDate"].lenght);
        temptransDate = resulttable["transactionDate"]; //YYYY-MM-DD
        tempYear = temptransDate.substring(0,4);
        tempMonth = temptransDate.substring(5,7);
        tempDate = temptransDate.substring(8,10);
        transDate = tempMonth + "/" + tempDate + "/" + tempYear;
        gblMFSwitchOrder["orderDate"] = transDate; //formatDateMF(transDate);

        tempeffectiveDate = resulttable["effectiveDate"]; //YYYY-MM-DD
        tempYear = tempeffectiveDate.substring(0,4);
        tempMonth = tempeffectiveDate.substring(5,7);
        tempDate = tempeffectiveDate.substring(8,10);
        effectiveDate = tempMonth + "/" + tempDate + "/" + tempYear;
        gblMFSwitchOrder["effectiveDate"] = effectiveDate; //formatDateMF(effectiveDate);
        
        frmMFSwitchCompleteMB.lbltranref.text = resulttable["transactionRef"];
        frmMFSwitchCompleteMB.show();
        showLoadingScreenGif();
        dismissLoadingScreenGif();
      }else{
        
        frmMFSwitchCompleteMB.lbltranref.text = resulttable["transactionRef"];
        frmMFSwitchCompleteMB.show();
        showLoadingScreenGif();
        dismissLoadingScreenGif();
      }
    } else if (resulttable["opstatus"] == "-1") {
      dismissLoadingScreen();
      popupTractPwd.dismiss();
      onClickCancelAccessPin();
      frmMFSwitchFailedMB.show()
    } else if (resulttable["opstatus"] == 1001) {
      dismissLoadingScreen();
      popupTractPwd.dismiss();
      onClickCancelAccessPin();
      frmMFSwitchFailedMB.show()
    } else if (resulttable["errCode"] == "VrfyAcPWDErr00001" || resulttable["errCode"] == "VrfyAcPWDErr00002") {
      dismissLoadingScreen();
      popupEnterAccessPin.lblWrongPin.setVisibility(true);
      resetAccessPinImg(resulttable["badLoginCount"]);

    }else if (resulttable["errCode"] == "VrfyAcPWDErr00003") {
      dismissLoadingScreen();
      onClickCancelAccessPin();
      gotoUVPINLockedScreenPopUp();
    } else if (resulttable["errCode"] == "VrfyTxPWDErr00001" || resulttable["errCode"] == "VrfyTxPWDErr00002") {
      dismissLoadingScreen();
      setTransPwdFailedError(kony.i18n.getLocalizedString("invalidTxnPwd"));
    }else if (resulttable["errCode"] == "VrfyTxPWDErr00003") {
      dismissLoadingScreen();
      showTranPwdLockedPopup();
    }else if (resulttable["opstatus"] == 8005) {
      popupTractPwd.dismiss();
      onClickCancelAccessPin();
      dismissLoadingScreen();
      if (resulttable["errCode"] == "VrfyOTPErr00001") {
        gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];

        alert("" + kony.i18n.getLocalizedString("invalidOTP"));
        //return false;
      } else if (resulttable["errCode"] == "VrfyOTPErr00002") {

        alert("" + kony.i18n.getLocalizedString("ECVrfyOTPErr"));
        //startRcCrmUpdateProfilBPIB("04");
        //return false;
      } else if (resulttable["errCode"] == "VrfyOTPErr00005") {

        alert("" + kony.i18n.getLocalizedString("KeyTokenSerialNumError"));
        //return false;
      } else if (resulttable["errCode"] == "VrfyOTPErr00006") {

        gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
        alert("" + resulttable["errMessage"]);
        //return false;
      }else if (resulttable["errCode"] == "VrfyOTPErr00004") {

        alert("" + resulttable["errMsg"]);
        //return false;
      }else{

        alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
        //return false;
      }
      frmMFSwitchFailedMB.show()
    } else {
      popupTractPwd.dismiss();
      onClickCancelAccessPin();
      dismissLoadingScreen();

      if (resulttable["opstatus"] == 1 && resulttable["errCode"] == "ERRDUPTR0001") {
        alert(kony.i18n.getLocalizedString("keyErrDuplicatemt"));
      }
      var errorMsgTop = resulttable["errMsg"];
      if(resulttable["errMsg"] == null || resulttable["errMsg"] == undefined || resulttable["errMsg"] == ""){
        errorMsgTop = kony.i18n.getLocalizedString("ECGenOTPRtyErr00001");
        alert(errorMsgTop);
      }

      if(resulttable["isServiceFailed"] == "true"){
        gblMyBillerTopUpBB = 0;
        alert("Service Failed");
      }
      frmMFSwitchFailedMB.show()
    }
  } else {
    if (status == 300) {
      popupTractPwd.dismiss();
      onClickCancelAccessPin();
      dismissLoadingScreen();
      alert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"));
      frmMFSwitchFailedMB.show()
    }else{
      dismissLoadingScreen();
      popupTractPwd.dismiss();
      onClickCancelAccessPin();
      frmMFSwitchFailedMB.show()
    }
  }
  }catch(e){
    kony.print("exception in complete screen is :: "+e);
  }
}