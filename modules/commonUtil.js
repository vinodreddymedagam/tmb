var tknid = ""; //////Added for CSFR Token ID/////////
serviceOpMap = {

  //TMBMIB_MAPPING
  "QRCodeGenerator":"TMBMIBService0",
  "KMSSubUnSubscribeReg":"TMBMIBService0",
  "BOTQRCodeGenerator":"TMBMIBService0",
  "QRCodeApproval":"TMBMIBService0",  
  "QRLoginScanBussiRulesChk":"TMBMIBService0",
  "QRSessionPolling":"TMBMIBService0", 
  "QRCodeFlagUpdate":"TMBMIBService0", 
  "CleanQRCode":"TMBMIBService0", 
  "IBVerifyQRLoginEligibility":"TMBMIBService0",
  "ekycFatcaUpdateActivityLog":"TMBMIBService0",
  "activationActivityLog":"TMBMIBService0",
  "ActivityLoggingForActivationReactivation":"TMBMIBService0",
  "addDevice":"TMBMIBService0",
  "addDreamSavingCareDataOpenAct":"TMBMIBService0",
  "addMyRecipientAfterTransferComplete":"TMBMIBService0",
  "addressModifyCompositeJavaService":"TMBMIBService0",
  "AddServiceRequest":"TMBMIBService0",
  "anyIDBusinessHrsCheck":"TMBMIBService0",
  "AnyIDInqComposite":"TMBMIBService0",
  "anyIDRegisterComposite":"TMBMIBService0",
  "applySoGoodCalculateInterest":"TMBMIBService0",
  "applySoGooDJavaService":"TMBMIBService0",
  "ATMLocatorService":"TMBMIBService0",
  "atmPINverifyOTPComposite":"TMBMIBService0",
  "BeepAndBillApplyJavaService":"TMBMIBService0",
  "beepAndBillData":"TMBMIBService0",
  "beepAndBillerValidation":"TMBMIBService0",
  "BeepAndBillExecuteJavaService":"TMBMIBService0",
  "billerValidation":"TMBMIBService0",
  "billPaymentValidationService":"TMBMIBService0",
  "CalendarInquiry":"TMBMIBService0",
  "CampaignFeedback":"TMBMIBService0",
  "cancelServiceRequest":"TMBMIBService0",
  "changePasswordComposite":"TMBMIBService0",
  "changeUserIDComposite":"TMBMIBService0",
  "checkAccountExistAccnt":"TMBMIBService0",
  "checkEligibilityOfP2P":"TMBMIBService0",
  "checkIBMBStatus":"TMBMIBService0",
  "checkOnUsPromptPayinq":"TMBMIBService0",
  "checkOpenActBusinessHours":"TMBMIBService0",
  "checkS2SBusinessHours":"TMBMIBService0",
  "checkS2SBusinessHoursBeforeConfirm":"TMBMIBService0",
  "checkTDBussinessHours":"TMBMIBService0",
  "checkUserExistRelInfoTable":"TMBMIBService0",
  "chgAccPinTxnPwdCompositeService":"TMBMIBService0",
  "chgMobileNumberCompositeService":"TMBMIBService0",
  "chgTransLimitCancelCompositeService":"TMBMIBService0",
  "chgTransLimitCompositeService":"TMBMIBService0",
  "CollectCampaignMetric":"TMBMIBService0",
  "ConfirmBillAddService":"TMBMIBService0",
  "ConfirmBillPaymentService":"TMBMIBService0",
  "ConfirmBillPaymentPromptPayService":"TMBMIBService0",
  "confirmPicUpload":"TMBMIBService0",
  "crmAccountModKony":"TMBMIBService0",
  "crmAllAccountsInquiryKony":"TMBMIBService0",
  "crmBankAccountInquiryKony":"TMBMIBService0",
  "CRMFinActivityInquiry":"TMBMIBService0",
  "crmOtherBankAccountInquiryKony":"TMBMIBService0",
  "crmProfileEnquiry":"TMBMIBService0",
  "crmProfileUpdateKony":"TMBMIBService0",
  "deleteCRMFinancialActivityLog":"TMBMIBService0",
  "deviceEnquiry":"TMBMIBService0",
  "DreamSavingEnquiry":"TMBMIBService0",
  "duplicateSessionCheckMB":"TMBMIBService0",
  "editDreamActBusinessHours":"TMBMIBService0",
  "EditDreamSavingCheck":"TMBMIBService0",
  "editDreamSavingComposite":"TMBMIBService0",
  "EditFutureTransferCompositeService":"TMBMIBService0",
  "eStmtDetailsCompositeJavaService":"TMBMIBService0",
  "eStmtModifyCompositeJavaService":"TMBMIBService0",
  "ExecuteMyAccountAddService":"TMBMIBService0",
  "ExecuteMyRecipientAccountEditDeleteService":"TMBMIBService0",
  "ExecuteMyRecipientAddService":"TMBMIBService0",
  "ExecuteMyRecipientDeleteService":"TMBMIBService0",
  "executeTransfer":"TMBMIBService0",
  "ExecuteTransferValidationService":"TMBMIBService0",
  "fbdelinkservice":"TMBMIBService0",
  "fbpostmessageonwall":"TMBMIBService0",
  "financialActivityLog":"TMBMIBService0",
  "ForceChangePasswordExecute":"TMBMIBService0",
  "FutureBillPaymentEditExecute":"TMBMIBService0",
  "generateImagePdf":"TMBMIBService0",
  "generatePdfImage":"TMBMIBService0",
  "generatePdfImageForActStmts":"TMBMIBService0",
  "generatePdfImageForMFFullStmts":"TMBMIBService0",
  "generateTransferPDF":"TMBMIBService0",
  "generateTransferRefNo":"TMBMIBService0",
  "getAccntTransStmt":"TMBMIBService0",
  "getAccountList":"TMBMIBService0",
  "getAllTxnsFromCCStmntInq":"TMBMIBService0",
  "getBankDetails":"TMBMIBService0",
  "getBankList":"TMBMIBService0",
  "getBillerCompCode":"TMBMIBService0",
  "getBMessage":"TMBMIBService0",
  "GetCalendarData":"TMBMIBService0",
  "GetCampaign":"TMBMIBService0",
  "getCaptchaOnRefresh":"TMBMIBService0",
  "getCardRewards":"TMBMIBService0",
  "getCRMAccountByCRMIdAndAcctId":"TMBMIBService0",
  "GetDistrictID":"TMBMIBService0",
  "getDreamActInfo":"TMBMIBService0",
  "getDreamDesc":"TMBMIBService0",
  "getDreamSavingTargets":"TMBMIBService0",
  "getDremTargetId":"TMBMIBService0",
  "GetExchangeRateData":"TMBMIBService0",
  "getFATCAQuestions":"TMBMIBService0",
  "getfbUserCode":"TMBMIBService0",
  "getFriends":"TMBMIBService0",
  "getGuidAndVersion":"TMBMIBService0",
  "getHolydayFlag":"TMBMIBService0",
  "GetHotPromotion":"TMBMIBService0",
  "getInboxMessagesUnreadCount":"TMBMIBService0",
  "getinboxunreadcount":"TMBMIBService0",
  "getInterestRate":"TMBMIBService0",
  "getITMXTransferFee":"TMBMIBService0",
  "GetLatestTransactions":"TMBMIBService0",
  "getMFFullStamentAllRecords":"TMBMIBService0",
  "getMibRecipientsForOnUs":"TMBMIBService0",
  "getMinMaxAmountVals":"TMBMIBService0",
  "GetOpenProductCode":"TMBMIBService0",
  "getPersonalizedAcctName":"TMBMIBService0",
  "getPhrases":"TMBMIBService0",
  "GetProductSorting":"TMBMIBService0",
  "GetProvinceID":"TMBMIBService0",
  "getRecipients":"TMBMIBService0",
  "getRecipientsForTransfer":"TMBMIBService0",
  "GetRef1Ref2ValidationMethodBB":"TMBMIBService0",
  "GetServerDateTime":"TMBMIBService0",
  "GetServiceRequestStatus":"TMBMIBService0",
  "getTokenActivationCode":"TMBMIBService0",
  "getTransactionDetails":"TMBMIBService0",
  "getTransactionTypes":"TMBMIBService0",
  "getTransferFee":"TMBMIBService0",
  "GetTxnTypes":"TMBMIBService0",
  "imageDeleteService":"TMBMIBService0",
  "imageUploadService":"TMBMIBService0",
  "inboxdelete":"TMBMIBService0",
  "inboxdetails":"TMBMIBService0",
  "inboxhistory":"TMBMIBService0",
  "inboximportant":"TMBMIBService0",
  "inquiryCustomerAccounts":"TMBMIBService0",
  "logDeviceStoreData":"TMBMIBService0",
  "LoginCompositeService":"TMBMIBService0",
  "LoginProcessServiceExecute":"TMBMIBService0",
  "LoginProcessServiceExecuteIB":"TMBMIBService0",
  "LoginTouchId":"TMBMIBService0",
  "logOutTMB":"TMBMIBService0",
  "MBLoginAuth":"TMBMIBService0",
  "menuOptionEnableTokenActivation":"TMBMIBService0",
  "MyAccountDeleteService":"TMBMIBService0",
  "MyAccountEditService":"TMBMIBService0",
  "MyprofileAddressDistrictJavaService":"TMBMIBService0",
  "MyprofileAddressProvinceJavaService":"TMBMIBService0",
  "MyprofileAddressSubDistrictJavaService":"TMBMIBService0",
  "MyprofileAddressZipcodeJavaService":"TMBMIBService0",
  "MyProfileModifyCompositeJavaService":"TMBMIBService0",
  "MyProfileViewCompositeService":"TMBMIBService0",
  "MyProfileViewCompositeServiceKYCOpenAcc":"TMBMIBService0",
  "newChangeMobileComposite":"TMBMIBService0",
  "notificationdelete":"TMBMIBService0",
  "notificationdetails":"TMBMIBService0",
  "notificationhistory":"TMBMIBService0",
  "notificationimportant":"TMBMIBService0",
  "notificationmarkread":"TMBMIBService0",
  "openAcctContactDetailsCompositeJavaService":"TMBMIBService0",
  "openActConfirmCompositeService":"TMBMIBService0",
  "pointRedeemBousinessHrs":"TMBMIBService0",
  "readUTFFile":"TMBMIBService0",
  "receipentAddBankAccntService":"TMBMIBService0",
  "receipentAllDetailsService":"TMBMIBService0",
  "receipentDeleteService":"TMBMIBService0",
  "receipentEditDelUpdateBankAccntService":"TMBMIBService0",
  "receipentEditService":"TMBMIBService0",
  "receipentgetBankService":"TMBMIBService0",
  "receipentListingService":"TMBMIBService0",
  "receipentNewAddService":"TMBMIBService0",
  "receipentNewAddwithAccntService":"TMBMIBService0",
  "receipentSetFavBankAccntService":"TMBMIBService0",
  "receipentSetFavService":"TMBMIBService0",
  "RedeemPointCompositeService":"TMBMIBService0",
  "removeImgFrmServerTmp":"TMBMIBService0",
  "removeSpecificImgFromServer":"TMBMIBService0",
  "RequestGenActCodeApplyExecute":"TMBMIBService0",
  "requestHistory":"TMBMIBService0",
  "RetrieveDeviceKey":"TMBMIBService0",
  "saveAddAccountDetails":"TMBMIBService0",
  "saveAnyIDRegistrationSession":"TMBMIBService0",
  "SaveChangeLimitParamsInSession":"TMBMIBService0",
  "saveCustomerSatisfactionScore":"TMBMIBService0",
  "saveEStatementSession":"TMBMIBService0",
  "SaveOpenAccountTxn":"TMBMIBService0",
  "SaveParamsForChangePwdUserId":"TMBMIBService0",
  "SaveParamsForTokenActivation":"TMBMIBService0",
  "saveRedeemPointsTxn":"TMBMIBService0",
  "SaveStopChequeParamsInSession":"TMBMIBService0",
  "selectProdCategoryForOpenAcc":"TMBMIBService0",
  "SendToSaveApplyConfirm":"TMBMIBService0",
  "sendToSaveApplyValidationService":"TMBMIBService0",
  "SendToSaveEditConfirm":"TMBMIBService0",
  "SendToSaveExecuteConfirm":"TMBMIBService0",
  "setPasswordComposite":"TMBMIBService0",
  "soGooodBusinessHrs":"TMBMIBService0",
  "stopChequePaymentComposite":"TMBMIBService0",
  "targetIDDreamSaving":"TMBMIBService0",
  "tokenSwitching":"TMBMIBService0",
  "TopupBillPaymentCompositeService":"TMBMIBService0",
  "TouchIdStatusUpdate":"TMBMIBService0",
  "updateCRMFinancialActivityLog":"TMBMIBService0",
  "updateDevice":"TMBMIBService0",
  "updateDeviceNameEmail":"TMBMIBService0",
  "updateDeviceNickName":"TMBMIBService0",
  "updateEditDreamSaveInfo":"TMBMIBService0",
  "updatefeedback":"TMBMIBService0",
  "updateRiskData":"TMBMIBService0",
  "updateTokenStatus":"TMBMIBService0",
  "updateTouchIdStatus":"TMBMIBService0",
  "validateCrmStatus":"TMBMIBService0",
  "verifyAddDevice":"TMBMIBService0",
  "verifyOTPActivateMBUsingIB":"TMBMIBService0",
  "verifyOTPKony":"TMBMIBService0",
  "welcomeMessage":"TMBMIBService0",
  "cardLimitSettingSaveToSession":"TMBMIBService0",
  "cardLimitSettingCompositeService":"TMBMIBService0",
  "changePassword":"TMBMIBService1",
  "resetPassword":"TMBMIBService1",
  "resetPasswordIB":"TMBMIBService1",
  "verifyPassword":"TMBMIBService1",
  "FBFriendsListingService":"TMBMIBService10",
  "AccntTransStmt":"TMBMIBService11",
  "AcctTrnInquiryCalendar":"TMBMIBService11",
  "billerCategoryInquiry":"TMBMIBService11",
  "billPaymentInquiry":"TMBMIBService11",
  "CCstatement":"TMBMIBService11",
  "crmProfileInqForS2S":"TMBMIBService11",
  "CustMobileNoMod":"TMBMIBService11",
  "customerAccountInquiryCalendar":"TMBMIBService11",
  "customerBBAdd":"TMBMIBService11",
  "customerBBDelete":"TMBMIBService11",
  "customerBBInquiry":"TMBMIBService11",
  "customerBBPaymentAdd":"TMBMIBService11",
  "CustomerBBPaymentStatusInquiry":"TMBMIBService11",
  "customerBillAdd":"TMBMIBService11",
  "customerBillDelete":"TMBMIBService11",
  "customerBillInquiry":"TMBMIBService11",
  "customerBillUpdate":"TMBMIBService11",
  "CustomerRiskLevel":"TMBMIBService11",
  "depositAOXferAdd":"TMBMIBService11",
  "doLiquidityInq":"TMBMIBService11",
  "FatcaUpdate":"TMBMIBService11",
  "issueCardOpenAccount":"TMBMIBService11",
  "masterBillerInquiry":"TMBMIBService11",
  "masterBillerInquiryForEditBP":"TMBMIBService11",
  "NotificationAddSms":"TMBMIBService11",
  "onlinePaymentAdd":"TMBMIBService11",
  "onlinePaymentInq":"TMBMIBService11",
  "onlinePaymentInqForEditBP":"TMBMIBService11",
  "partyUpdateAddress":"TMBMIBService11",
  "partyUpdateEmailAddr":"TMBMIBService11",
  "partyUpdateMyProfile":"TMBMIBService11",
  "RetailLiquidityAdd":"TMBMIBService11",
  "RetailLiquidityDelete":"TMBMIBService11",
  "RetailLiquidityInquiry":"TMBMIBService11",
  "RetailLiquidityMod":"TMBMIBService11",
  "RetailLiquidityTransferAdd":"TMBMIBService11",
  "RetailLiquidityTransferInquiry":"TMBMIBService11",
  "returnChequeInquiry":"TMBMIBService11",
  "s2sCustomerAccInq":"TMBMIBService11",
  "TDStatement":"TMBMIBService11",
  "timeDepositStatementInquiryCalendar":"TMBMIBService11",
  "doPmtAdd":"TMBMIBService12",
  "doPmtCan":"TMBMIBService12",
  "doPmtInq":"TMBMIBService12",
  "doPmtInqForEditBP":"TMBMIBService12",
  "doPmtMod":"TMBMIBService12",
  "fetchTransactionInstructions":"TMBMIBService12",
  "RecurringFundTransinq":"TMBMIBService12",
  "RecurringFundTransModelUpdate":"TMBMIBService12",
  "stopChequePayment":"TMBMIBService12",
  "deleteUser":"TMBMIBService13",
  "modifyUserToUnlockOTP":"TMBMIBService13",
  "generateOTPWithUser":"TMBMIBService14",
  "verifyPasswordEx":"TMBMIBService14",
  "verifyPasswordS2S":"TMBMIBService14",
  "verifyTokenEx":"TMBMIBService14",
  "LoanStatement":"TMBMIBService15",
  "generateOTPForChaneMobileNumber":"TMBMIBService16",
  "verifyPasswordBB":"TMBMIBService16",
  "SubscribeKPNS":"TMBMIBService17",
  "checkBiller":"TMBMIBService18",
  "assignPin":"TMBMIBService19",
  "verifyPin":"TMBMIBService19",
  "findUserByIdInSegment":"TMBMIBService2",
  "MFUHAccountSummaryInq":"TMBMIBService20",
  "MFUHAccountDetailInq":"TMBMIBService21",
  "MFUHFullStmntInquiry":"TMBMIBService22",
  "InitOnlineATMPIN":"TMBMIBService23",
  "BAGetPolicyDetails":"TMBMIBService24",
  "BAGetPolicyList":"TMBMIBService24",
  "BAPayPolicyPremium":"TMBMIBService24",
  "BAReqTaxDocument":"TMBMIBService24",
  "MFTaxDocumentView":"TMBMIBService25",
  "decryptData":"TMBMIBService26",
  "initDPParam":"TMBMIBService26",
  "verifyCVVE2E":"TMBMIBService27",
  "AnyIDInq":"TMBMIBService28",
  "AnyIDInqXpress":"TMBMIBService29",
  "createUser":"TMBMIBService3",
  "createUserIB":"TMBMIBService3",
  "modifyUpdateUser":"TMBMIBService3",
  "modifyUser":"TMBMIBService3",
  "anyIdRegisterSearch":"TMBMIBService30",
  "anyIDRegistrationETEAdd":"TMBMIBService31",
  "anyIDRegistrationETEDel":"TMBMIBService31",
  "anyIDRegistrationETEMod":"TMBMIBService31",
  "anyIDAccountInq":"TMBMIBService32",
  "updateMobile":"TMBMIBService33",
  "promptPayInq":"TMBMIBService34",
  "promptPayAdd":"TMBMIBService35",
  "generateOTP":"TMBMIBService4",
  "RequestOTPKony":"TMBMIBService4",
  "verifyOTP":"TMBMIBService4",
  "verifyToken":"TMBMIBService4",
  "AccountWithdrawInq":"TMBMIBService5",
  "billpaymentAdd":"TMBMIBService5",
  "cardVerifyInq":"TMBMIBService5",
  "CCCampaignStmtInq":"TMBMIBService5",
  "CCEstmtUpdate":"TMBMIBService5",
  "CCInstlAddCalculate":"TMBMIBService5",
  "CCRedempAdd":"TMBMIBService5",
  "ContactUsNotification":"TMBMIBService5",
  "creditcardDetailsInq":"TMBMIBService5",
  "creditcardDetailsInqNonSec":"TMBMIBService5",
  "crmAccountEnquiry":"TMBMIBService5",
  "crmProfileInq":"TMBMIBService5",
  "crmProfileInqWithUserId":"TMBMIBService5",
  "crmProfileMod":"TMBMIBService5",
  "crmProfileModActivation":"TMBMIBService5",
  "crmProfileModWithUserId":"TMBMIBService5",
  "customerAccountInquiry":"TMBMIBService5",
  "customerBBAccountInquiry":"TMBMIBService5",
  "debitCardInq":"TMBMIBService5",
  "EStmtMaintInq":"TMBMIBService5",
  "EStmtMaintUpdate":"TMBMIBService5",
  "fundTransferInq":"TMBMIBService5",
  "LoanAccountAddrAdd":"TMBMIBService5",
  "LoanAccountAddrUpdate":"TMBMIBService5",
  "NotificationAdd":"TMBMIBService5",
  "ORFTAdd":"TMBMIBService5",
  "ORFTInq":"TMBMIBService5",
  "partyInquiry":"TMBMIBService5",
  "partyInquiryForEstmt":"TMBMIBService5",
  "TCEMailService":"TMBMIBService5",
  "tDDetailinq":"TMBMIBService5",
  "TransferAdd":"TMBMIBService5",
  "updateLogoutTimeInCRM":"TMBMIBService5",
  "GetCustomerStatus":"TMBMIBService6",
  "ReqGenActivationCode":"TMBMIBService6",
  "depositAccountInquiry":"TMBMIBService7",
  "depositAccountInquiryNonSec":"TMBMIBService7",
  "doLoanAcctInq":"TMBMIBService7",
  "doLoanAcctInqNonSec":"TMBMIBService7",
  "doRecXferInq":"TMBMIBService7",
  "getCaptcha":"TMBMIBService8",
  "IBVerifyLoginEligibility":"TMBMIBService0",
  "KonyCRMProfileInquiry":"TMBMIBService0",
  "VerifyLoginEligibility":"TMBMIBService0",
  "IBVerifyLoginEligibilityOld":"TMBMIBService9",
  "KonyCRMProfileInquiryOld":"TMBMIBService9",
  "VerifyLoginEligibilityOld":"TMBMIBService9",
  "beneficiaryMod":"TMBMIBService29",
  "beneficiaryInq":"TMBMIBService29",
  "beneficiaryAdd":"TMBMIBService29",
  "quickBalanceSetting":"TMBMIBService0",
  "quickBalance":"TMBMIBService0",
  "GetSubdistrictforContactInfo":"TMBMIBService0",
  "getEkycMasterData":"TMBMIBService0",
  "getEKycCustomerInfo":"TMBMIBService0",
  "saveUpdateEKycCustomerInfo":"TMBMIBService0",
  "saveUpdateEKycPersonalInfo":"TMBMIBService0",
  "getBeneficiaryRelationships":"TMBMIBService0",
  "saveEditBeneficiarySession":"TMBMIBService0",
  "verifyCardNumber":"TMBMIBService0",
  "verifyCitizenId":"TMBMIBService0",
  "verifyCardTransPwdComposite":"TMBMIBService0",
  "assignPinCardActivation":"TMBMIBService19",
  "verifyCVVE2ECardActivation":"TMBMIBService27",
  "cardDetailInq":"TMBMIBService11",
  "customerCardList":"TMBMIBService0",
  "customerAccountRelationInquiry":"TMBMIBService5",
  "cardDetailsAdd":"TMBMIBService11",
  "requestNewPinCompositeService":"TMBMIBService0",
  "cashAdvanceCompositeService":"TMBMIBService0",
  "cashAdvanceSaveToSession":"TMBMIBService0",
  "cashAdvanceBusinessHrs":"TMBMIBService0",
  "getCardBlockReasons":"TMBMIBService0",
  "creditCardStatusInq":"TMBMIBService5",
  "creditCardBlockReissue":"TMBMIBService5",
  "blockDebitCardService":"TMBMIBService5",
  "blockCardCompositeService":"TMBMIBService0",
  "getOfferCardDetails":"TMBMIBService0",
  "changePinVerify":"TMBMIBService19",
  "changePinAssign":"TMBMIBService19",
  "changeAssignPinCompositeService":"TMBMIBService0",
  "changeVerifyPinCompositeService":"TMBMIBService0",
  "changePinVerifyPwdService":"TMBMIBService0",
  "reissueCardActivityLog":"TMBMIBService0",
  "reissueCardCompositeService":"TMBMIBService0",
  "cashChillChill":"TMBMIBService5",
  "interestRateInquiry":"TMBMIBService40",
  "MFSuitabilityInq":"TMBMIBService41",
  "MFSuitabilityInqNew":"TMBMIBService41", 
  "MFFundRuleInq":"TMBMIBService42",
  "MFHolidayCalendarInq":"TMBMIBService43",
  "MFFundValidation":"TMBMIBService0",
  "TMBMIBService44":"mfBillPaymentAdd",
  "TMBMIBService45":"mfTMBAMAdd",
  "MFOrderCompositeJavaService":"TMBMIBService0",
  "MFOrderValidationService":"TMBMIBService0",
  "MFOrderValidationService_test":"TMBMIBService0",
  "BatchMutualFundJavaService":"TMBMIBService0",
  "MFFundCutoffTimefValidation":"TMBMIBService0",
  "MFCancelOrderCompositeJavaService":"TMBMIBService0",
  "checkConnection":"TMBMIBService0",
  "IBVerifyLoginEligibilityNew": "TMBMIBService0",
  "verifyPasswordLogin": "TMBMIBService1",
  "verifyOTPNoUser": "TMBMIBService1",
  "SuitQuestionnaireETEInq": "MFSuitabilityServices",
  "CalcSuitabilityETEInq": "MFCalcSuitabilityInqService",
  "CalcSuitabilityETEInqNew": "MFCalcSuitabilityInqService",
  "MFFundListInq": "TMBMIBService47",
  "requestAccessCode": "TMBMIBService46",
  "rejectReason": "TMBMIBService46",
  "loginQRScan": "TMBMIBService46",
  "reSendUVPushnotification": "TMBMIBService46",
  "getAssignedToken": "TMBMIBService48",
  "assignTokenActivation": "TMBMIBService48",
  "unassignTokenReact": "TMBMIBService48",
  "getAPINActivation": "TMBMIBService48",
  "QRPaymentDefaultAccount":"TMBMIBService0",
  "QRPaymentDefaultAccountSettings":"TMBMIBService0",
  "QRPaymentValidation":"TMBMIBService0",
  "uvAssignApinTSFirmware":"TMBMIBService0",
  "uvTransactionHistory": "TMBMIBService0",
  "tmbBonusService": "TMBMIBService0",
  "GenerateQRTransaction":"TMBMIBService0",
  "deleteAllPreviousAssociations":"TMBMIBService0",
  "loginWithAccessPinJavaService":"TMBMIBService0",
  "UVMaintInq":"TMBMIBService5",
  "getRequestAccessCode":"TMBMIBService0",
  "verifyReqAccessCodeTransPwdComposite":"TMBMIBService0",
  "UVActivationFlow":"TMBMIBService0",
  "getRTPConsents":"TMBMIBService0",
  "updateRTPConsents":"TMBMIBService0",
  "cardlessWithdrawDailyLimitverify":"TMBMIBService0",
  "PromptPayETEAdd":"TMBMIBService34",
  "crcVerifier":"TMBMIBService0",
  "RTPInq":"RTPServices",
  "RTPUpdate":"RTPServices",
  "LoginCompositeSingleService":"TMBMIBService0",
  "SuitQuestionnaireENTH":"TMBMIBService0",
  "logDeviceStoreDataNew":"TMBMIBService0",
  "NewGetPhrases":"TMBMIBService0",
  "RTPBillPayValidationCompositeJavaService":"TMBMIBService0",
  "errorReporter":"TMBMIBService0",
  "promptPayTrnInq":"TMBMIBService29",
  "QRPaymentInquiry":"TMBMIBService0",
  "getBase64ProfilePicture":"TMBMIBService0",
  "cardOperInqComposite": "TMBMIBService0",
  "loanStatusTrackingDetails":"TMBMIBService0",
  "verifyLetterofConsentService":"TMBMIBService0",
  "uploadImageFiletoFTP":"TMBMIBService0",
  "eDonationInqComposite":"TMBMIBService0",
  "creditCardLimitSettingInq":"TMBMIBService0",
  "creditCardLimitSettingCompositeService":"TMBMIBService0",
  "getPersonalInfoFields":"TMBMIBService0",
  "GetLoanPaymentMethod" :"TMBMIBService0",
  "updateFacilityInfo" :"LoanUpdateFacilityInfo",
  "searchApplicationInfoByCaID" :"LoanSubGetApplicationInfo",
  "LoanSubmitApplicationJavaService" :"TMBMIBService0",
  "searchCustomerInfoByCaID":"LoanSubGetCustomerInfo",
  "updateCustInfoJavaService":"TMBMIBService0",
  "getFacilityInfo" :"LoanSubGetFacilityInfo",
  "ImageFTPService":"TMBMIBService0",
  "searchChecklistInfoByCaID":"LoanSubmissionGetChecklistInfo",
  "updateIncompleteDocApplication":"LoanUpdateIncompleteDocApp",
  "GetLoanConfirmDetails":"GetLoanConfirmDetailsComposite",
  "getCreditCardInfo" :"LoanSubGetCreditCardInfo",
  "LoanFacilityCreditCardInfoJavaService":"TMBMIBService0",
  "updateCreditCardInfo" :"LoanUpdateCreditCardInfo",
  "notificationInquiry" :"TMBMIBService52",
  "notificationUpdate" :"TMBMIBService52",
  "GetLoanReceiveMethod" :"TMBMIBService0",
  "validateDOEDOBService":"TMBMIBService0",
  "eKYCCreateRMOpenAcctCompositeService":"TMBMIBService0",
  "EKYCOne2OneVerification":"EKYCOne2OneVerification",
  "ekycPartyDOPAJavaService": "TMBMIBService0",
  "RTPSenderOnUs": "SaveAlert",
  "RTPSenderOffUs": "SaveAlert",
  "EKYCSetAccessPinJavaService":"TMBMIBService0",
  "EKYCLoginJavaService":"TMBMIBService0",
  "CalRiskPersonal":"CalRiskPersonal",
  "verifyPromptPayReceipt" :"VerifyPromptPayReceipt",
  "saveAlertCompositeService":"TMBMIBService0",
  "saveAlertPromptPayInq":"TMBMIBService0",
  "saveAlertList":"TMBMIBService0",
  "RequestOTPEmail":"TMBMIBService4",
  "verifyOTPEmail":"TMBMIBService4",
  "emailVerification":"TMBMIBService0",
  "myProfileChangeImage":"TMBMIBService0",
  "getIdentityProviders":"IdPService",
  "getIdentity":"IdPService",
  "getRequestStatus":"IdPService",
  "IDPApprovalFlow":"TMBMIBService0",
  "getBiometricData":"ProspectService",
  "partyUpdateEkycIAL":"TMBMIBService11",
  "eKYCIdpResult":"TMBMIBService0",
  "eKYCIdpRequestStatus":"TMBMIBService0",
  "validateSourceAndGetTargetFunds":"MFServices",
  "MFSwitchOrderValidationService":"MFSwitchFundValidation",
  "MFSwitchFundValidation":"MFSwitchFundValidation",
  "MFSwitchOrderService":"TMBMIBService0"
};

gblTourFlag = false;
serviceCheckFlag = false;

function commonServiceInvoker(serviceID, inputParam, callBackFunction) {
 
    var sessionIdKey = "cacheid";
    inputParam.appID = appConfig.appId;
    inputParam.appver = appConfig.appVersion;
    inputParam["serviceID"] = serviceID;
    inputParam["locale"] = kony.i18n.getCurrentLocale();
    inputParam["app_name"] = "TMBUI";
 
    if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "android" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPod touch") {
        inputParam["channel"] = "rc";

        var trusteerIdFromClient = tmbRetriveFromDevice("trusteerIdFromClient");
        if (trusteerIdFromClient == null) trusteerIdFromClient = "";
        inputParam["trusteerIdFromClient"] = trusteerIdFromClient;

    } else {
        localStorage.removeItem("deviceID");
        inputParam["channel"] = "wap";
    }
    inputParam["platform"] = gblDeviceInfo.name;
    inputParam[sessionIdKey] = sessionID;
    inputParam["tknid"] = tknid; //////Added for CSFR Token ID/////////

    if (globalhttpheaders) {
        if (inputParam["httpheaders"]) {
            inputParam.httpheaders = mergeHeaders(inputParam.httpheaders, globalhttpheaders);
        } else {
            inputParam["httpheaders"] = globalhttpheaders;
        };
    };
    var vChannel = inputParam["channel"];
    //inputParam["httpconfig"] = {"timeout": DEFAULT_SERVICE_TIMEOUT};


    // Start : MIB-4884-Allow special characters for My Note and Note to recipient field
    var allowSpecialCharServices = "executeTransfer,ConfirmBillPaymentService,ConfirmBillPaymentPromptPayService,TopupBillPaymentCompositeService,EditFutureTransferCompositeService,FutureBillPaymentEditExecute, openActConfirmCompositeService";
    if (allowSpecialCharServices.indexOf(serviceID) >= 0) {
        inputParam = replaceHtmlTagsInObject(inputParam);
    }
    // End : MIB-4884-Allow special characters for My Note and Note to recipient field

    if (serviceID == "imageUploadService") {
        var connHandle = callMF(serviceID, inputParam, callBackBasicFunction);

    } else if ((serviceID == "generatePdfImage" || serviceID == "generatePdfImageForActStmts" || (serviceID == "CRMFinActivityInquiry" && inputParam["savefunctn"] != null && inputParam["savefunctn"] == "forsave")) && vChannel != "rc") {
        if (navigator.userAgent.indexOf('Safari') == -1 && navigator.userAgent.indexOf('Chrome') == -1 && navigator.userAgent.indexOf('CriOS') == -1) {
            showAlertPopBlockerInfo(url, inputParam, callBackBasicFunction);
        } else {
            var connHandle = callMF(serviceID, inputParam, callBackBasicFunction);
        }
    } else {
        var connHandle = callMF(serviceID, inputParam, callBackBasicFunction);
    }

    function callMF(serviceID, inputParam, callBackBasicFunction) {
        try {
            var headers = {};
            var options = {};
            var vChannel = inputParam["channel"];

            if (DEFAULT_SERVICE_TIMEOUT === null || DEFAULT_SERVICE_TIMEOUT === "" || DEFAULT_SERVICE_TIMEOUT === undefined) {
                DEFAULT_SERVICE_TIMEOUT = 180000;
            }

            if (vChannel == "wap") {
                options = {
                    "xmlHttpRequestOptions": {
                        "enableWithCredentials": true,
                        "timeoutIntervalForRequest": DEFAULT_SERVICE_TIMEOUT,
                        "timeoutIntervalForResource": DEFAULT_SERVICE_TIMEOUT
                    }
                };
            } else {
                options = {
                    "httpRequestOptions": {
                        "enableWithCredentials": true,
                        "timeoutIntervalForRequest": DEFAULT_SERVICE_TIMEOUT,
                        "timeoutIntervalForResource": DEFAULT_SERVICE_TIMEOUT
                    }
                };
            }
            if (serviceID == "SubscribeKPNS") {
                headers["Content-Type"] = "application/json";
            }
            headers["appVers"] = appConfig.appVersion;
            appConfig.serverIp = gblServerIp;//"mibdev1.tau2904.com";
            appConfig.secureServerPort = gblSecurePort;//"443";
            var serviceName = serviceOpMap[serviceID];
            bbServiceObj = manualMFClientObject.getIntegrationService(serviceName);
            bbServiceObj.invokeOperation(serviceID, headers, inputParam, function(result) {
                //success
                gblIntegrationFailureCount = 0; //reset value

                kony.print("success callMF: serviceID=" + serviceID + ",inputParam=" + JSON.stringify(inputParam) + ",result=" + JSON.stringify(result));

                callBackBasicFunction(400, result);
            }, function(result) {

                kony.print("error callMF: serviceID=" + serviceID + ",inputParam=" + JSON.stringify(inputParam) + ",result=" + JSON.stringify(result));

                //error
                kony.print("====>Operation failed commonServiceInvoker");
                //alert(kony.i18n.getLocalizedString("genErrorWifiOff"));
                if (result["opstatus"] == 1013 || result["opstatus"] == 1011 || result["opstatus"] == 1000 || result["opstatus"] == 1012) {
                    //    isMFsdkinit=false;
                    resetValues();
                    checkReleaseTransferLandingMB();
                     
                        kony.print("genErrorWifiOff : ====>Operation failed commonServiceInvoker");
                        dismissLoadingScreen();
                        if (serviceID != "logDeviceStoreDataNew") {
                            alert(kony.i18n.getLocalizedString("genErrorWifiOff"));
                        }
                    
                } else if (null != result["errCode"] && undefined != result["errCode"] && "K898989" == result["errCode"]) {

                    //alert(kony.i18n.getLocalizedString("ECGenericError"));
                    dismissLoadingScreen();
                    if (vChannel == "rc") {
                        resetValues();
                        clearGlobalVariables();
                        spaFormGlobalsCall();
                        cleanPreorPostForms();
                        var getEncrKeyFromDevice = kony.store.getItem("encrytedText");
                        if (getEncrKeyFromDevice != null) {
                            frmMBPreLoginAccessesPin.show();
                        } else {
                            frmMBanking.show();
                        }

                    } else {
                        frmIBPreLogin.show();
                    }
                } else {
                    callBackBasicFunction(400, result);
                }

            }, options);


        } catch (e) {
            if (e.code == s.AUTH_FAILURE || e.code == Errors.INIT_FAILURE || e.code == Errors.INTEGRATION_FAILURE) {
                // initialization and authorization is not complete
                // sleep for try again
                // isMFsdkinit=false;
                kony.print("====>Trying to invoke service while SDK is not initialized yet! commonServiceInvoker " + e.stack);
                checkReleaseTransferLandingMB();                
                    kony.print("genErrorWifiOff : ====>Trying to invoke service while SDK is not initialized yet! commonServiceInvoker");
                    dismissLoadingScreen();
                    alert(kony.i18n.getLocalizedString("genErrorWifiOff"));
                
            }
        }
    }




    /**
     * function checks for valid session
     * @returns {}
     */
    function callBackBasicFunction(status, resultTable) {
        try {
            if (resultTable["tknid"] != null) {
                tknid = resultTable["tknid"]; //////Added for CSFR Token ID/////////
                kony.print("tknid: "+tknid);
            }
            if (resultTable["TODAY_DATE"] != null)
                GLOBAL_TODAY_DATE = resultTable["TODAY_DATE"];
            var isSessionDestroyed = false;
            if (resultTable["opstatus"] == 6001) {
                isSessionDestroyed = true;
            } else if (resultTable["opstatus"] == 1011 || resultTable["opstatus"] == 1012 || resultTable["opstatus"] == 1000 || resultTable["errcode"] == 1012 || resultTable["errcode"] == 1011 || resultTable["errcode"] == 1012 || resultTable["errcode"] == 1000 || resultTable["opstatus"] == 1013 || resultTable["opstatus"] == 8004 || resultTable["opstatus"] == 8006) { //wifi off
                //isSessionDestroyed = true;
                if (resultTable["opstatus"] == 1013 || resultTable["opstatus"] == 8004 || resultTable["opstatus"] == 8006) //MW returnedinvalid JSON
                {

                    if (serviceID == "FatcaUpdate") {
                        if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPod touch" || gblDeviceInfo["name"] == "iPhone Simulator" || gblDeviceInfo["name"] == "android" || flowSpa) {


                            gblFATCAUpdateError = true;
                            FATCAGeneralPopup.lblHead.text = kony.i18n.getLocalizedString("ECGenericError");
                            FATCAGeneralPopup.btnClose.text = kony.i18n.getLocalizedString("keyClose");
                            FATCAGeneralPopup.show();

                        } else {
                            gblFATCAUpdateError = true;
                            popupIBFATCAInfo.hbxInfoSkipNext.setVisibility(false);
                            popupIBFATCAInfo.hbxInfoClose.setVisibility(true);
                            popupIBFATCAInfo.btnInfoClose.text = kony.i18n.getLocalizedString("keyFATCAClose");
                            popupIBFATCAInfo.lblText.text = kony.i18n.getLocalizedString("ECGenericError");
                            popupIBFATCAInfo.show();

                        }
                    } else {
                        alert(kony.i18n.getLocalizedString("ECJavaErr00001"));
                        checkReleaseTransferLandingMB();
                    }
                } else {
                    kony.print("genErrorWifiOff : ====> callBackBasicFunction");
                    alert(kony.i18n.getLocalizedString("genErrorWifiOff"));
                    checkReleaseTransferLandingMB();
                }
                if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPod touch" || gblDeviceInfo["name"] == "android") {
                    resetValues();
                }

                //var deviceInf = kony.os.deviceInfo();
                if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPod touch" || gblDeviceInfo["name"] == "iPhone Simulator" || gblDeviceInfo["name"] == "android" || (gblDeviceInfo["name"] == "thinclient" && gblDeviceInfo["type"] == "spa")) {
                    if (gblDeviceInfo["name"] == "thinclient" && gblDeviceInfo["type"] == "spa") {
                        if (navigator.userAgent.match(/Mobile Safari/i) || navigator.userAgent.match(/iPhone/i)) {
                            dismissLoadingScreen();
                        } else {
                            dismissLoadingScreenPopup();
                        }
                    } else {
                        dismissLoadingScreen();
                    }
                } else {
                    dismissLoadingScreenPopup();
                }
                return false;
            } else if (resultTable["opstatus"] == 0 && resultTable["errCode"] == "K898989") {
                dismissIBLoadingScreen();
                if (flowSpa) {
                    isMenuShown = false;
                    isSignedUser = false;
                    frmSPALogin = null;
                    frmSPALoginGlobals();
                }
                isSessionDestroyed = true;
            } else if (resultTable["opstatus"] == 0 && resultTable["errCode"] == "K898990") {
                dismissIBLoadingScreen();
                alert("You are not authorized to invoke this Service, please contact TMB HelpDesk");
                checkReleaseTransferLandingMB();
            } else if (resultTable["opstatus"] == 0 && resultTable["errCode"] == "K899091") {
                dismissIBLoadingScreen();
                isSessionDestroyed = true;
                alert("CSRF Security threat found (May be service failure too!!!)");
                checkReleaseTransferLandingMB();
            } else {
                //dismissIBLoadingScreen();
                //have to handle other conditons wifiOff//default error message,...
            }
            if (isSessionDestroyed) {
                //var deviceInfo = kony.os.deviceInfo();
                dismissIBLoadingScreen();
                if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPod touch" || gblDeviceInfo["name"] == "iPhone Simulator" || gblDeviceInfo["name"] == "android") {
                    var getEncrKeyFromDevice = kony.store.getItem("encrytedText");
                    if (getEncrKeyFromDevice != null) {
                        if (kony.application.getCurrentForm().id != "frmMBPreLoginAccessesPin") { //frmAfterLogoutMB
                            isSignedUser = false;
                            //frmAfterLogoutMB
                            //Code changed for IOS9 upgrade
                            clearGlobalVariables();
                            spaFormGlobalsCall();
                            cleanPreorPostForms();
                            gblTouchShow = false;
                            frmMBPreLoginAccessesPin.show();
                        } else {
                            if (serviceCheckFlag) {
                                if (serviceID != "updateRiskData" && serviceID != "logOutTMB") {
                                    alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
                                    checkReleaseTransferLandingMB();
                                }
                            }
                            dismissLoadingScreen();
                            return false;
                        }
                    } else {
                        frmMBanking.show();
                    }
                } else if (gblDeviceInfo["name"] == "thinclient" && gblDeviceInfo["type"] == "spa") {
                    //
                    //frmSPALogin.show();
                    //} else {
                    if (flowSpa) {
                        frmSPALogin.show();
                        if (resultTable["errCode"] == "K898989") {
                            clearGlobalVariables();
                            spaFormGlobalsCall();
                            cleanPreorPostForms();
                            checkReleaseTransferLandingMB();
                            dismissLoadingScreen();
                        }
                    } else {
                        frmIBPreLogin.show();
                    }
                }
            } else {
                callBackFunction(status, resultTable);
            }

        } catch (e) {
            if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPod touch" || gblDeviceInfo["name"] == "iPhone Simulator" || gblDeviceInfo["name"] == "android" || (gblDeviceInfo["name"] == "thinclient" && gblDeviceInfo["type"] == "spa")) {
                if (gblDeviceInfo["name"] == "thinclient" && gblDeviceInfo["type"] == "spa") {
                    if (navigator.userAgent.match(/Mobile Safari/i) || navigator.userAgent.match(/iPhone/i)) {
                        dismissLoadingScreen();
                    } else {
                        dismissLoadingScreenPopup();
                    }
                } else {
                    dismissLoadingScreen();
                }
            } else {
                dismissLoadingScreenPopup();
            }
            return false;
        }

        return connHandle;
    }
}  

function invokeServiceSecureAsync(serviceID, inputParam, callBackFunction) {
    if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "android" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPod touch") {


        var trusteerIdFromClient = tmbRetriveFromDevice("trusteerIdFromClient");
        if (trusteerIdFromClient == null) trusteerIdFromClient = "";
        inputParam["trusteerIdFromClient"] = trusteerIdFromClient;

    }
    if (kony.net.isNetworkAvailable(constants.NETWORK_TYPE_ANY)) {

        var appServiceDocument = serviceDocObject;

        if (!isMFsdkinit && gblDeviceInfo["name"] != "thinclient") {
            //isMFsdkinit=true;
            manualMFClientObject = new kony.sdk();
            kony.sdk.skipAnonymousLoginCall(true); // for stopping "/login" call service
            var serviceDocResult = manualMFClientObject.initWithServiceDoc(configObject.keys.APP_KEY, configObject.keys.APP_SECRET, appServiceDocument);
            kony.print("serviceDocResult: " + serviceDocResult);
            if (serviceDocResult === true) {
                isMFsdkinit = true;
                kony.print("Anonymous Login Success");
                commonServiceInvoker(serviceID, inputParam, callBackFunction);
            } else {
                //alert("Anonymous Login Failure");
                kony.print("Anonymous Login Failure");
                kony.print("====>Failed to initialize as client of Mobile Fabric. MB");
                isMFsdkinit = false;
                checkReleaseTransferLandingMB();
                kony.print("genErrorWifiOff : ====>Failed to initialize as client of Mobile Fabric. MB");
                dismissLoadingScreen();
                if (serviceID != "logDeviceStoreDataNew") {
                        alert(kony.i18n.getLocalizedString("genErrorWifiOff"));
                }                
            }
        } else if (!isMFsdkinit && isMFEnabled && gblDeviceInfo["name"] == "thinclient") {
            manualMFClientObject = new kony.sdk();
            kony.sdk.skipAnonymousLoginCall(true); // for stopping "/login" call service
            var serviceDocResult = manualMFClientObject.initWithServiceDoc(appkey, appsecret, appServiceDocument);
            kony.print("serviceDocResult: " + serviceDocResult);
            if (serviceDocResult === true) {
                isMFsdkinit = true;
                kony.print("====>Successfully initialized as client of Mobile Fabric. IB");
                //loadResourceBundleIB();
                commonServiceInvoker(serviceID, inputParam, callBackFunction);
            } else {
                kony.print("serviceDocResult: " + serviceDocResult);
                kony.print("====>Failed to initialize as client of Mobile Fabric. IB");
                isMFsdkinit = false;
                checkReleaseTransferLandingMB();
                if (gblUnavailableFlag && isInIBStartupForm() && serviceID == "checkConnection") {
                    checkIBMaintenance();
                } else {
                    doneCheckIBUnavailable = true;
                    resetCheckIBUnavailable();
                    kony.print("genErrorWifiOff : ====>Failed to initialize as client of Mobile Fabric. IB");
                    dismissLoadingScreen();
                    alert(kony.i18n.getLocalizedString("genErrorWifiOff"));
                }
            }
            //appMFPostInitIB();

        } else {
            commonServiceInvoker(serviceID, inputParam, callBackFunction);
        }
    } else {
        kony.print("====>Failed to initialize as client of Mobile Fabric. No Internet");
        // MIB-6016
        if (serviceID == "LoginCompositeService") {
            resetValues();
        }
        //isMFsdkinit=false;
        //alert("Login failure" + JSON.stringify(error));
        checkReleaseTransferLandingMB();        
        kony.print("genErrorWifiOff : ====>Failed to initialize as client of Mobile Fabric. No Internet");
        dismissLoadingScreen();
        alert(kony.i18n.getLocalizedString("genErrorWifiOff"));
        
    }
}


/*function campaignBannerCount() {
   // var deviceInfo = kony.os.deviceInfo();

    if (gblCampaginForm.length > 0) {
        var inputParams = {
            cmpCode: gblCampaignFlag.split("-")[3],
            deviceID: gblDeviceInfo["deviceid"],
            deviceModel: gblDeviceInfo["name"],
            deviceOS: gblDeviceInfo["version"],
            browser:gblDeviceInfo["category"],
            clickChannel: gblCampaignFlag.split("-")[2],
            clickScreen: gblCampaginForm.split("-")[0],
            responseFlagBanner: gblCampaignFlag.split("-")[0],
            noOfClickBanner: gblClickCount,
            noOfDisplayBanner: gblViewCount,
            responseFlagInbox: "Y",
            noOfDisplayInbox: "0",
            responseFlagHotpromo: "Y",
            noOfClickHotpromo: gblPromotionsClickCount,
            noOfDisplayHotpromo: gblPromotionsViewCount,
            recIndicator:"D",
            campaignsMetrics: JSON.stringify(campaignData)
        };
        invokeServiceSecureAsync("CollectCampaignMetric", inputParams, campaignCountServiceCallBack);
    }else if (campaignData!= null && campaignData["CampaignsMetrics"] != undefined){
    	 var inputParams = {
            cmpCode: "",
            deviceID: gblDeviceInfo["deviceid"],
            deviceModel: gblDeviceInfo["name"],
            deviceOS: gblDeviceInfo["version"],
            browser:gblDeviceInfo["category"],
            clickChannel: "02",
            clickScreen: "",
            responseFlagBanner: "",
            noOfClickBanner: gblClickCount,
            noOfDisplayBanner: gblViewCount,
            responseFlagInbox: "Y",
            noOfDisplayInbox: "0",
            responseFlagHotpromo: "Y",
            noOfClickHotpromo: gblPromotionsClickCount,
            noOfDisplayHotpromo: gblPromotionsViewCount,
            recIndicator:"D",
            campaignsMetrics: JSON.stringify(campaignData)
        };
        invokeServiceSecureAsync("CollectCampaignMetric", inputParams, campaignCountServiceCallBack);
    }else {
    	 invokeLogoutService();
    }

} */


/*function campaignCountServiceCallBack(status, callbackResponse) {

    if (status == 400) {
        if (callbackResponse["opstatus"] == 0) {

        	gblCampaignFlag = "";
		    //gblCampaignData = "";
		    gblLastClicked = "";
		    gblCampaginForm = "";
		    gblCampaignImgEN = "";
		    gblCampaignImgTH = "";
		    gblCampaignFlag = "";
		    gblCampaignDataEN = "";
		    gblCampaignDataTH = "";
		    gblViewCount = 0;
		    gblClickCount = 0;
		    //gblCampaignTotalData = "";
		    campaignData = {};
		    invokeLogoutService();
        } else {
        	invokeLogoutService();
        }
    } else {
    	invokeLogoutService();
    }
}*/

/*function campaignBannerIBCount() {
    //var deviceInfo = kony.os.deviceInfo();
	showLoadingScreenPopup();
    if (gblCampaginForm.length > 0) {
        var inputParams = {
            cmpCode: gblCampaignFlag.split("-")[3],
            deviceID: gblDeviceInfo["deviceid"],
            deviceModel: gblDeviceInfo["name"],
            deviceOS: gblDeviceInfo["version"],
            browser:gblDeviceInfo["category"],
            clickChannel: gblCampaignFlag.split("-")[2],
            clickScreen: gblCampaginForm.split("-")[0],
            responseFlagBanner: gblCampaignFlag.split("-")[0],
            noOfClickBanner: gblClickCount,
            noOfDisplayBanner: gblViewCount,
            responseFlagInbox: "Y",
            noOfDisplayInbox: "0",
            responseFlagHotpromo: "Y",
            noOfClickHotpromo: gblPromotionsClickCount,
            noOfDisplayHotpromo: gblPromotionsViewCount,
            recIndicator:"D",
            campaignsMetrics: JSON.stringify(campaignData)
        };
        invokeServiceSecureAsync("CollectCampaignMetric", inputParams, campaignCountServiceIBCallBack);
    }else if (campaignData != null && campaignData["CampaignsMetrics"] != undefined){
    	 var inputParams = {
            cmpCode: "",
            deviceID: gblDeviceInfo["deviceid"],
            deviceModel: gblDeviceInfo["name"],
            deviceOS: gblDeviceInfo["version"],
            browser:gblDeviceInfo["category"],
            clickChannel: "01",
            clickScreen: "",
            responseFlagBanner: "",
            noOfClickBanner: gblClickCount,
            noOfDisplayBanner: gblViewCount,
            responseFlagInbox: "Y",
            noOfDisplayInbox: "0",
            responseFlagHotpromo: "Y",
            noOfClickHotpromo: gblPromotionsClickCount,
            noOfDisplayHotpromo: gblPromotionsViewCount,
            recIndicator:"D",
            campaignsMetrics: JSON.stringify(campaignData)
        };
        invokeServiceSecureAsync("CollectCampaignMetric", inputParams, campaignCountServiceIBCallBack);
    }

     else {
    	 IBLogoutService();
    }

} */


/*function campaignCountServiceIBCallBack(status, callbackResponse) {

    if (status == 400) {
        if (callbackResponse["opstatus"] == 0) {

        	gblCampaignFlag = "";
		    //gblCampaignData = "";
		    gblLastClicked = "";
		    gblCampaginForm = "";
		    gblCampaignImgEN = "";
		    gblCampaignImgTH = "";
		    gblCampaignFlag = "";
		    gblCampaignDataEN = "";
		    gblCampaignDataTH = "";
		    gblViewCount = 0;
		    gblClickCount = 0;
		    //gblCampaignTotalData = "";
		    campaignData = {};
		    IBLogoutService();
        } else {
        	IBLogoutService();
        }
    } else {
    	IBLogoutService();
    }
} */


function clearGlobalVariables() {
  try {
    //PLEASE UPDATE THIS WHEN ADDING NEW GLOBAL VARIABLES
    //gblLangFlipTO=false;
    mySelectBillerListMB = [];
    mySelectBillerSuggestListMB = [];
    mySuggestedTopUpList = [];
    gblDeviceContactList = [];
    gblLangPrefIB = false;
    gblLangPrefMB = "";
    gblIBRetryCountRequestOTP["chngIBPwd"] = "0";
    gblIBRetryCountRequestOTP["chngIBUserId"] = "0";
    gblMyAccntNickNme = "";
    //	gblMyAccntPersonalizedAcctId = "";
    //	gblMyAccntBankCD = "";
    sessionID = "";
    gblUserName = "";
    gblOTPFlag = false;
    gblSTATUS = true;
    gblACTCODEVALID = true;
    gblOTPSTATUS = true;
    gblLang_flag = "en_US";
    gblPHONENUMBER = "9987651234";
    gblBANKREF = "";
    gblOTPLENGTH = 0;
    gblflag = 0;
    gblPrevLen = 0;
    gblChangePWDFlag = 0;
    gblShowPwdNo = gblShowPwdNoFinal;
    gblShowPwd = gblShowPwdFinal;
    gblAddOrAuth = 0;
    gblAccountTable = {};
    gblIndex = "";
    gblApplyServices = 0;
    gblActivationCode = "";
    gblTokenNum = "";
    gblSetPwd = false;
    glbAccessPin = "";
    gblcrmId = "";
    gblEmailId = "";
    gblTestAddDevice = false;
    gaccType = "";
    gblTrasORFT = 0;
    gblTransSMART = 0;
    gblTransEmail = 0;
    gblTrasSMS = 0;
    gblAckFlage = "true";
    gblIBFlowStatus = "";
    gblMBFlowStatus = "";
    gblCustomerStatus = "";
    gblRMID = "";
    gblCitizenID = "";
    glbTrasactionPwd = "";
    gbltranFromSelIndex = {};
    gblNoOfFromAcs = 0;
    gblTransferToRecipientData = {};
    gblTransferToRecipientCache = {};
    gblFirstTimeBillPayment = true;
    gblPaynow = true;
    gblScheduleRepeatBP = "Once";
    gblFirstTimeTopUp = true;
    gblPromptPayBillerAcctName = "";
    gblLocale = false;
    gblBPScheduleFirstShow = true;
    gblSwipeFlag = 0;
    GblBillTopFlag = true;
    gblVerifyOTPCounter = "0";
    gblShowPinPwdSecs = gblShowPinPwdSecsFinal;
    Locale = 0;
    gblPrevLen = 0;
    gblAdd_Receipent_State = 0;
    gblNEW_RC_ADDITION = 1;
    gblEXISTING_RC_EDITION = 2;
    gblNEW_RC_ADDITION_PRECONFIRM = 3;
    gblnewRcPhNumber = "";
    gblFB_SINGLE_SELECTION = 100;
    gblfbSelectionState = 0;
    gblOTPdisabletime = 60000;
    gblSelectedAccountIndex = 0;
    gblTrasSMS = "0";
    gblTransEmail = "0";
    gblXferSmart = "0";
    gblXferOrft = "0";
    gblselectedRcId = "";
    gblRC_QA_TEST_VAL = 1;
    gblEXISTING_RC_BANKACCNT_EDITION = 4;
    gblLOG_DEBUG = true;
    gblEXISTING_RC_BANKACCNT_ADDITION = 5;
    gblMenuSelection = 0;
    numberOfAccounts = 5;
    gblCMHide = "0";
    gblCMMore = "0";
    gblOTPLENGTH = 0;
    gblVerifyOTPCounter = "0";
    gblTopUpPaynow = true;
    gblBpBalHideUnhide = true;
    gblSelectedAccBal = 0;
    gblCMUserIDRule = "0";
    gblIdCounter = "0";
    gblActivationCounter = "0";
    gblOnClickReq = false;
    gblRetryCountRequestOTP = "0";
    gblActionCode = "0";
    gblRtyCtrVrfyAxPin = "0";
    gblRtyCtrVrfyTxPin = "0";
    gblScheduleEndBP = "none";
    gblTopupDelete = 0;
    gblRefreshRcCache = false;
    gblRcValidateAccnt = true;
    gblStartBPDate = "";
    gblEndBPDate = "";
    gblGiveToConfirmationLabel = "";
    gblTopUpType = 0;
    gblTopUpMore = 0;
    gblRcEnableCache = true;
    gblLoginCount = "0";
    gbltimestampfirsthit = "";
    gblPenalty = false;
    gblFullPayment = true;
    gblMode = "0";
    gblFlagMenu = "";
    gblMyBillerTopUpBB = 0;
    gblConfOrComp = false;
    gblisTMB = "";
    gbltdFlag = "";
    gblsplitAmt = {};
    gblsplitFee = {};
    gblMaxTransferORFT = "";
    gblMaxTransferSMART = "";
    gblLimitORFTPerTransaction = "";
    gblLimitSMARTPerTransaction = "";
    gblUVregisterMB = "N";
    gblcwselectedData = "";
    gblMobNoTransLimitFlag = true;
    gblAddressFlag = 1;
    gblTransferRefNo = "";
    gblCustomerName = "";
    gblCustomerFullNameEn="";
    gblCustomerFullNameTh="";
    gblTMBBankCD = "11";
    gblAddressFlagIB = 1;
    gblDefaultAccountNum = "";
    gblRcOpLockStatus = false;
    gblNFActiStats = "false";
    gblNFOpnStats = "false";
    gblNFHidden = "false";
    gblVerifyOTP = 0;
    GblsaveProfileAddr = false;
    gblCRMProfileData = {};
    gbltranRecip = 0;
    gblIBsaveProfileAddr = false;
    gblIBAddressFlag = "1";
    gblIBLocale = false;
    gblORFTInqData = {};
    gblTransferDate = "";
    gblTDDateIB = "";
    gbTDAmtIB = "";
    gblSplitAckImg = {};
    gblSplitCnt = 0;
    gblRef1ScanValue = 0;
    gblRef2ScanValue = 0;
    gblScanAmount = 0;
    gblScanCompCode = "";
    gblRef1LabelScan = "";
    gblRef2LabelScan = "";
    gblBarCodeOnly = "";
    gblAmountPaymentFlag = "";//MKI,MIB-12109
    gblSplitStatusInfo = {};
    gblBillerCategoryGroupType = "BILLPAY";
    gblTopupCategoryGroupType = "TOPUP";
    gblCustomerBillerID = "0";
    var gblCustomerBillerIDMB = "0";
    gblGroupTypeBiller = "0";
    gblGroupTypeTopup = "1";
    gblBillerMethod = "0";
    gblBillerNameScan = "";
    gblAddToMyBill = false;
    gblMyProfileAddressFlag = "none";
    gblTxtFocusFlag = 0;
    gblScheduleTransfers = false;
    gblCrmAccountNumbers = {};
    gblSSTrnMinofMax = 40000.00;
    gblSSTrnsLmtMin = 20000.00;
    gblSSTrnsLmtMax = 70000.00;
    gblSSApply = "false";
    gblStmntSessionFlag = "1";
    gblEBMaxLimitAmtCurrent = "";
    glbInitVector = "";
    gblBillerAndTopUpCategoryGroupType = "BOTH";
    gblSSTrnMaxofMin = 40000.00;
    gblSSExcuteCnfrm = "false";
    gblSSServieHours = "false ";
    gblLoggedIn = false;
    gblsearchtxt = "";
    gblEmailAddr = "";
    gblFacebookId = "";
    gblLinkedAcct = "";
    gblEbTxnLimitAmt = "";
    gblmyProfileAddrState = "";
    gblmyProfileAddrDistrict = "";
    gblmyProfileAddrSubDistrict = "";
    gblmyProfileAddrZipCode = "";
    gblRegAddress2 = "";
    gblRegAddress3 = "";
    gblConAddress2 = "";
    gblConAddress3 = "";
    gblCurrentPage = "0";
    gblIsPrevLink = false;
    gblIsNextLink = false;
    gblUpdateProfileFlag = "";
    gblEBMaxLimitAmtHist = "";
    gblzipcodeValue = "10990";
    gblsubdistrictValue = "Champon";
    gbldistrictValue = "chatuchak";
    gblStateValue = "Bangkok";
    gblUserName = "";
    gblPassword = "";
    gblCWSelectedItem = 0;
    gblXferPhoneNo = "";
    gblXferEmail = "";
    gblPhoneNumberReq = "";
    gblTDDepositNo = {};
    gblShowConfComplete = true;
    gblTCEmailTriggerFlag = false;
    gblEmailTCSelect = false;
    gblSSLinkedStatus = "false";
    gblSelProduct = "";
    gblTopUpfirstTime = true;
    gbls2sEditFlag = "false";
    gblSSFromAccStatus = "false";
    gblSSToAccStatus = "false";
    gblPreshow = "0";
    gblSelRel = "0";
    times = "";
    gblXferRecImg = "";
    gblScannedNickname = "";
    gblXferTDAmt = {};
    gblXferTDdate = {};
    gblS2SHiddenStatus = "false";
    gblBlockBillerTopUpAdd = false;
    gblBalAfterXfer = "";
    gblFundXferData = {};
    gblDeviceNickName = "";
    BankRefId = "";
    localeChange = "0";
    gblAddress1Value = "123/456";
    gblAddress2Value = "Phaholyothin Road";
    gblExeS2S = "false";
    gblregAddress1Value = "";
    gblregAddress2Value = "";
    gblregStateValue = "";
    gblregzipcodeValue = "";
    gblregsubdistrictValue = "";
    gblregdistrictValue = "";
    gblspaSelIndex = {};
    LinkMyActivities = "0";
    gblOrftAddRs = {};
    gblIBEditNickNameValue = "";
    gblEditNickNameValue = "";
    gblDreamMnths = "";
    gblFirstSiteAccess = false;
    //gblEditServiceHours = false;
    //myProfileFlag = false;
    gblSortBy = "txnDate";
    gblSortOrder = "DESC";
    gblIBTxnRefID = "";
    gblmbSpatransflow = {};
    gblXerSplitData = {};
    gblOpenActList = {};
    gblDateReset = "0";
    gblDepositAcclist = {};
    gblFinancialTxnIBLock = "0";
    gblFinancialTxnMBLock = "0";
    gblBillpaymentNoFee = false;
    gblXferLnkdAccnts = "";
    gblPartyInqOARes = {};
    gblNotificationFor = "falseX ";
    gblActivitiesNvgn = "";
    gblPayLoadKpns = {};
    gblsegID = "";
    gblisDisToActTD = "";
    gblMaxOpenAmt = "";
    gblMinOpenAmt = "";
    gblEmailAddrOld = "";
    //isFirstTimeBBMB = true;
    gblOnClickCoverflow = "";
    gblOTPReqLimit = 0;
    gblFromTermActs = {};
    gblTOTermActs = {};
    gblFinActivityLogOpenAct = {};
    gblTDDateFlag = false;
    gblWithdrawalTotInterest = 0;
    gblWithdrawalTaxAmt = 0;
    gblOpenActBenList = {};
    gblUserLockStatusIB = 0;
    gblDreamSelData = {};
    gblcarouselwidgetflow = "Normal";
    gblTokenActivationFlag = false;
    gblStopChequeOTPFlag = "";
    gblOTPDream = "";
    gblTokenSwitchFlag = false;
    gblSwitchToken = false;
    gblCampaignResp = "";
    gblCampaignFlag = "";
    //gblCampaignData = "";
    gblLastClicked = "";
    gblCampaginForm = "";
    gblCampaignImgEN = "";
    gblCampaignImgTH = "";
    //gblCampaignFlag = "";
    gblCampaignDataEN = "";
    gblCampaignDataTH = "";
    //begin MIB-1988
    gblHtmlStringEN = "";
    gblHtmlStringTH = "";
    //end MIB-1988
    gblViewCount = 0;
    gblClickCount = 0;
    //gblCampaignTotalData = "";
    campaignData = {};
    Recp_category = 0;
    gblVerifyToken = 0;
    gblMYInboxCount = 0;
    var cryptoP = "";
    var cryptoQ = "";
    var cPubModulo = "";
    var cryptoPrivateVal = "";
    var cPubExp = "";
    cSerPub = "";
    gblTourFlag=false;
    gblCampaignURLEN="";
    gblCampaignURLTH="";
    gblTouchActSum="N";
    gblrewardsData=[];
    gblSelectedReward=-1;
    gblrefcontweight = "";
    gblrefcontwidth = "";
    gblcontheight = "";
    gblBASummaryData = "";
    gblBAPolicyDetailsData = "";
    gblMFSummaryData = "";
    gblOpenAccountFlow = false;
    gblOpenActBusinessHrs = false;
    gblOfficeAddrOpenAc = "";
    gblMyOffersDetails=false;

    //added below global variable to implement MIB-920
    isCmpFlow = false;
    gblProdCode = "";
    gblProudctDetails = {};
    ProdDetails = {};
    gblOpenAcctNormalSavings = "";
    gblOpenAcctSavingsCare = "";
    gblOpenAcctTermDeposit = "";
    gblOpenAcctDreamSaving = "";
    gblForUserProdCodes = "";
    displayAnnoucementtoUser=false;
    isSignedUser = false;
    gblIsNewOffersExists = false;
    gblOfferDetailsObj = {};

    //Clearing Any ID global variables

    //For MB Any ID
    gblMBOtherBankLinked=false;
    gblCIOtherBankLinked=false;
    gblMBTMBLinked=false;
    gblCITMBLinked=false;
    userCIChanged=false;
    userMBChanged=false;
    agreeButton = false;
    gblServiceMBAct="";
    gblServiceCIAct="";
    fromTnC=false;
    noEligibleActs=false;
    singleAct=false;
    rawDataTMB = [];
    anyIDProdFeature_image_EN="";
    anyIDProdFeature_image_TH="";
    gblCIanyIDActName="";
    gblCIanyIDActNameTH="";
    gblanyIDActName="";
    gblanyIDActNameTH="";
    gblAnyIDimageName="";
    gblanyIDActID="";
    gblCIAndroidSrvVal=false;
    gblCIiPhoneSrvVal=false;
    gblMBAndroidSrvVal="";
    gblMBiPhoneSrvVal="";
    gblselectedChckBox="";
    isMBRegister=false;
    isCIRegister=false;
    isMBDeregister=false;
    isfirstCallToMasterBiller = false;
    isCIDeregister=false;
    gblAccountsAnyID=[];
    gblMBActsForLocaleChange=[];
    gblCIActsForLocaleChange=[];
    gblCIAccountsEN="";
    gblCIAccountsTH="";
    gblLinkOtherBankLangStr = [];
    gblMBLinkOtherBankLangStr= [];
    fromBack=false;
    gblLocalfromDeregister=false;
    gblAnyIDAccountTable=[];

    //For IB Any ID
    gblCIAnyID=false;
    gblMobileAnyID=false;
    localCIAnyID=false;
    localMobileAnyID=false;
    gblCIAnyIDRegisterAllowed=true;
    gblMobileAnyIDRegisterAllowed=true;
    gblCIAnyIDRegisteredAccount={};
    gblMobileAnyIDRegisteredAccount={};
    gblSelectedAnyIDMobileAccount={};
    gblSelectedAnyIDCitizenAccount={};
    gblAnyIDInqData={};
    gblAnyIDAccountTable=[];

    //Clearing Any ID global variables

    //For MB Any ID
    gblMBOtherBankLinked=false;
    gblCIOtherBankLinked=false;
    gblMBTMBLinked=false;
    gblCITMBLinked=false;
    userCIChanged=false;
    userMBChanged=false;
    agreeButton = false;
    gblServiceMBAct="";
    gblServiceCIAct="";
    fromTnC=false;
    noEligibleActs=false;
    singleAct=false;
    rawDataTMB = [];
    anyIDProdFeature_image_EN="";
    anyIDProdFeature_image_TH="";
    gblCIanyIDActName="";
    gblCIanyIDActNameTH="";
    gblanyIDActName="";
    gblanyIDActNameTH="";
    gblAnyIDimageName="";
    gblanyIDActID="";
    gblCIAndroidSrvVal=false;
    gblCIiPhoneSrvVal=false;
    gblMBAndroidSrvVal="";
    gblMBiPhoneSrvVal="";
    gblselectedChckBox="";
    isMBRegister=false;
    isCIRegister=false;
    isMBDeregister=false;
    isCIDeregister=false;
    gblAccountsAnyID=[];
    gblMBActsForLocaleChange=[];
    gblCIActsForLocaleChange=[];
    gblCIAccountsEN="";
    gblCIAccountsTH="";
    gblLinkOtherBankLangStr = [];
    gblMBLinkOtherBankLangStr= [];
    fromBack=false;
    gblLocalfromDeregister=false;
    gblAnyIDAccountTable=[];

    //For IB Any ID
    gblCIAnyID=false;
    gblMobileAnyID=false;
    localCIAnyID=false;
    localMobileAnyID=false;
    gblCIAnyIDRegisterAllowed=true;
    gblMobileAnyIDRegisterAllowed=true;
    gblCIAnyIDRegisteredAccount={};
    gblMobileAnyIDRegisteredAccount={};
    gblSelectedAnyIDMobileAccount={};
    gblSelectedAnyIDCitizenAccount={};
    gblAnyIDInqData={};
    gblAnyIDAccountTable=[];

    isRefreshPressed = false;
    searchEligibleContactList = [];
    deviceContactRefreshServerValue = 1;
    gblSelectBillerCategoryID = "0";
    gblBillerPresentInMyBills = false;
    gblMyBillList = [];
    gblDisplayBalanceBillPayment = true;
    mySuggestedTopUpList = [];
    gblUploadDocumentList = [];
	gblRunningNumber = "01";
    gblContentLoanUpload = "";
	gblContentListIndex = 0;
	gbluploadObjTmp = {};
	gblUploadCheckList = [];
	gblIncompleteDoc = false;
    gblproductService = "";
    gblIdpList = [];

    //Added by Vijay for Top Up UI Redesign
    mySuggestedTopUpList = [];
    if (gbl3dTouchAction != "quickbalance") {
      gbl3dTouchAction = "";
    }
    //

    gblfromCalender = false;

    mySelectBillerSuggestListMB = [];
    gblQuickBalanceFromLogin=false;
    gblQrSetDefaultAccnt=false;

    gblActivityNiceName = "";
    dynamicMenuLoaded=false;
    inboxclickedClicked=0;
    settingsClicked=0;
    moreClicked=0;
    gblStartClickFromActivationMB=false;

    //block credit card
    gblMBNewTncFlow = "";
    gblStatementFlow = "";
    
    setDefaultUVSectionDisplayValue();// Setting all UV globals in one function

    gblMBLoggingIn = false;
	gblIsLoading = false;
    gblFlexPostMenucreated=false;
    gblnewLoginCompositeresult="";
    
    // Loan App related globals
  	gblCustomerPassport = "";
  	gblCustomerDob = "";
    gblLoanFullAddressSearch = [];
	gblLoanFullAddressSearchForEditAddress = [];
    // To Acc Prompt Pay globals
    gbltoAccPromptPayRange1Lower = "";
    gbltoAccPromptPayRange1Higher= "";
    gbltoAccPromptPayRange2Lower = "";
    gbltoAccPromptPayRange2Higher= "";
    gbltoAccPromptPaySPlitFeeAmnt1 = "";
    gbltoAccPromptPaySPlitFeeAmnt2 = "";
    gblMaxTransfertoAccPromptPay = "";
    gblTranstoAccPromptPaySplitAmnt = "";
    gblLimittoAccPromptPayPerTransaction = "";
    gbleDonationType = "";
    gblDonationNormalFlow = false;
    gblQRPayData = [];
    gblLoanNav = "";
    gblLoanIncomeInformation = [];
    gblIncomeInvalidField = null;
    gblPersonalInfo = [];
    gblbankInfo = "";
    gblEDMaxLimitAmt = 1000000;
    gblCaId = "";
    gblChosenTab = "";
    gblLoanAppRefNo = "";
    gblServiceType = "";
    gblCardRef = "";
    eKYCData = {};
    gblEnabledLoan = "";
    gblLoanFullAddressSearchForEditAddress = [];
  	gblAddrSearchLang = "";
	gblEditAddressFlow = "";
    gblIDVerificationFlag = "N";
    gblCurrentAAL = 2.3;
    gblIALFlow = "";
    gblUpdateIAlCount = 0;
    gblAllowUpdateIAl = 1;
	gblMaximumLoanUploadDocumentSize = 960;
	gblMFEventFlag = "";
    gblMFSwitchData = {};
    gblMFSwitchOrder = {}; 
    //PLEASE CALL MODULE SPECIFIC CLEARGLOBALVARIABLE METHODS HERE
    //MB
    if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPod touch" || gblDeviceInfo["name"] == "android" || flowSpa == true) {
      onClickCancelAccessPin();
      clearGVNotificationInboxMB();
      resetTransferRTPPush();
      gblLoanApplicationList = [];
      gblLoanAppType = "";
      gblDeeplinkExternalAccess = false;
  	  gblContentDeeplinkData = "";
      clearPointRedemptionLandingOnLogout();
      frmAccountDetailsMB.segDebitCard.removeAll();
      frmAccountDetailsMB.segMaturityDisplay.removeAll();
      TMBUtil.DestroyForm(frmAccountDetailsMB);
      frmAccountStatementMB.segcredit.removeAll();
      TMBUtil.DestroyForm(frmAccountStatementMB);
      TMBUtil.DestroyForm(frmAccountStatementMBDummy);
      TMBUtil.DestroyForm(frmAccountSummaryLanding);
      TMBUtil.DestroyForm(frmAccTrcPwdInter);
      TMBUtil.DestroyForm(frmAddNewBeepnBillMB);
      if (!flowSpa) {
        frmAddTopUpBiller.segSlider.removeAll();
        frmAddTopUpToMB.segSlider.removeAll();
        frmBBApplyBeepAndBillPayment.segSlider.removeAll();
        //frmBBPaymentApply.segSlider.removeAll();
        frmBillPayment.segSlider.removeAll();
        //frmConnectAccMB.segSlider.removeAll();
        frmDreamSavingEdit.segSlider.removeAll();
        frmMBApplyBeepAndBillPayment.segSlider.removeAll();
        frmMBMyActivities.segDayView.removeAll();
        //frmMBMyActivities.segSliderCalendar.removeAll()
        //frmMyFreqUsedAcc.segSlider.removeAll();
        frmOpenAccountSavingCareMB.segSlider.removeAll();
        frmOpenAccTermDeposit.segSliderTDFrom.removeAll();
        frmOpenAcDreamSaving.segSliderOpenDream.removeAll();
        frmOpnActSelAct.segNSSlider.removeAll();
        frmSSService.segSlider.removeAll();
        frmTopUp.segSlider.removeAll();
        frmTransferLanding.segTransFrm.removeAll();
      }
      TMBUtil.DestroyForm(frmAddTopUpBiller);
      frmAddTopUpBillerconfrmtn.segConfirmationList.removeAll();
      TMBUtil.DestroyForm(frmAddTopUpBillerconfrmtn);
      TMBUtil.DestroyForm(frmAddTopUpToMB);
      TMBUtil.DestroyForm(frmAddTopUpToMB);
      //TMBUtil.DestroyForm(frmAfterLogoutMB);
      TMBUtil.DestroyForm(frmApplyInternetBankingConfirmation);
      TMBUtil.DestroyForm(frmApplyInternetBankingMB);
      TMBUtil.DestroyForm(frmApplyMBConfirmationSPA);
      TMBUtil.DestroyForm(frmApplyMBSPA);
      frmApplyServicesMB.segServiceList.removeAll();
      TMBUtil.DestroyForm(frmApplyServicesMB);
      TMBUtil.DestroyForm(frmAppTour);
      TMBUtil.DestroyForm(frmATMBranch);
      TMBUtil.DestroyForm(frmATMBranchesDetails);
      frmATMBranchList.segATMListDetails.removeAll();
      TMBUtil.DestroyForm(frmATMBranchList);

      TMBUtil.DestroyForm(frmBBApplyBeepAndBillPayment);
      TMBUtil.DestroyForm(frmBBApplyNow);
      //TMBUtil.DestroyForm(frmBBComplete);
      TMBUtil.DestroyForm(frmBBConfirmAndComplete);
      //TMBUtil.DestroyForm(frmBBConfirmAndCompleteCalendar);
      //TMBUtil.DestroyForm(frmBBConfirmation);
      //TMBUtil.DestroyForm(frmBBExecute);
      TMBUtil.DestroyForm(frmBBExecuteConfirmAndComplete);
      //TMBUtil.DestroyForm(frmBBExecuteConfirmAndCompleteCalendar);
      //frmBBList.segBillersList.removeAll();
      // frmBBList.segSuggestedBillers.removeAll();
      //  TMBUtil.DestroyForm(frmBBList);
      TMBUtil.DestroyForm(frmBBMyBeepAndBill);
      // frmBBMyBeepAndBillSearch.segBillersList.removeAll();
      // frmBBMyBeepAndBillSearch.segSuggestedBillers.removeAll();
      //  TMBUtil.DestroyForm(frmBBMyBeepAndBillSearch);
      // TMBUtil.DestroyForm(frmBBMyBill);
      // TMBUtil.DestroyForm(frmBBPaymentApply);
      //frmBBSelectBill.segSelectList.removeAll();
      //TMBUtil.DestroyForm(frmBBSelectBill);
      //TMBUtil.DestroyForm(frmBeepAndBillApplyNow);
      //frmBeepAndBillConfirmation.segBBConfirm.removeAll();
      // TMBUtil.DestroyForm(frmBeepAndBillConfirmation);
      TMBUtil.DestroyForm(frmBillPayment);
      TMBUtil.DestroyForm(frmBillPaymentComplete);
      TMBUtil.DestroyForm(frmBillPaymentCompleteCalendar);
      TMBUtil.DestroyForm(frmBillPaymentConfirmationFuture);
      TMBUtil.DestroyForm(frmBillPaymentEdit);
      TMBUtil.DestroyForm(frmBillPaymentView);
      TMBUtil.DestroyForm(frmBlank);

      TMBUtil.DestroyForm(frmChangeMobNoTransLimitMB);
      TMBUtil.DestroyForm(frmCMChgAccessPin);
      TMBUtil.DestroyForm(frmCMChgPwdSPA);
      TMBUtil.DestroyForm(frmCMChgTransPwd);

      TMBUtil.DestroyForm(frmConfirmationarchived);
      TMBUtil.DestroyForm(frmConnectAccMB);
      TMBUtil.DestroyForm(frmContactUsCompleteScreenMB);
      TMBUtil.DestroyForm(frmContactusFAQMB);
      TMBUtil.DestroyForm(frmContactUsMB);
      TMBUtil.DestroyForm(frmDreamCalculator);
      TMBUtil.DestroyForm(frmDreamSavingEdit);
      TMBUtil.DestroyForm(frmDreamSavingMB);
      TMBUtil.DestroyForm(frmEditFutureBillPaymentComplete);
      TMBUtil.DestroyForm(frmEditFutureBillPaymentConfirm);
      TMBUtil.DestroyForm(frmeditMyProfile);
      frmExchangeRate.segExchangeRates.removeAll();
      TMBUtil.DestroyForm(frmExchangeRate);
      //   TMBUtil.DestroyForm(frmFBLogin);
      TMBUtil.DestroyForm(frmFBProfileLogin);
      TMBUtil.DestroyForm(frmFeedbackComplete);
      frmFullStatement.segment2502794999720.removeAll();
      TMBUtil.DestroyForm(frmFullStatement);
      TMBUtil.DestroyForm(frmGeneratecrmid);
      TMBUtil.DestroyForm(frmInboxDetails);
      frmInboxHome.segMyRecipient.removeAll();
      TMBUtil.DestroyForm(frmInboxHome);
      TMBUtil.DestroyForm(frmMBAccLocked);
      TMBUtil.DestroyForm(frmMBActiComplete);
      TMBUtil.DestroyForm(frmMBActiConfirm);
      TMBUtil.DestroyForm(frmMBActivation);
      //TMBUtil.DestroyForm(frmMBanking);
      //TMBUtil.DestroyForm(frmMBankingSpa);
      TMBUtil.DestroyForm(frmMBApplyBeepAndBillPayment);
      TMBUtil.DestroyForm(frmMBFTEdit);
      TMBUtil.DestroyForm(frmMBFTEditCmplete);
      TMBUtil.DestroyForm(frmMBFTEditCmpleteCalendar);
      TMBUtil.DestroyForm(frmMBFTEditCnfrmtn);
      TMBUtil.DestroyForm(frmMBFTSchduleOldFt);
      TMBUtil.DestroyForm(frmMBFtSchedule);
      TMBUtil.DestroyForm(frmMBFTView);
      TMBUtil.DestroyForm(frmMBFTViewCalendar);
      TMBUtil.DestroyForm(frmMBMyActivities);
      TMBUtil.DestroyForm(frmMBsetPasswd);
      TMBUtil.DestroyForm(frmMBSetuseridSPA);
      TMBUtil.DestroyForm(frmMBTnC);
      TMBUtil.DestroyForm(frmMyAccntAddAccount);
      frmMyAccntConfirmationAddAccount.segBankAccnt.removeAll();
      TMBUtil.DestroyForm(frmMyAccntConfirmationAddAccount);
      TMBUtil.DestroyForm(frmMyAccountEdit);
      frmMyAccountList.segOtherBankAccntDetails.removeAll();
      frmMyAccountList.segTMBAccntDetails.removeAll();
      TMBUtil.DestroyForm(frmMyAccountList);
      TMBUtil.DestroyForm(frmMyAccountView);

      TMBUtil.DestroyForm(frmMyProfile);
      frmMyProfileReqHistory.segRequestHistory.removeAll();
      TMBUtil.DestroyForm(frmMyProfileReqHistory);
      TMBUtil.DestroyForm(frmMyRecipientAddAcc);
      frmMyRecipientAddAccComplete.segMyRecipientDetail.removeAll();
      TMBUtil.DestroyForm(frmMyRecipientAddAccComplete);
      frmMyRecipientAddAccConf.segMyRecipientDetail.removeAll();
      TMBUtil.DestroyForm(frmMyRecipientAddAccConf);
      TMBUtil.DestroyForm(frmMyRecipientAddProfile);
      TMBUtil.DestroyForm(frmMyRecipientAddProfileComp);
      frmMyRecipientDetail.segMyRecipientDetail.removeAll();
      TMBUtil.DestroyForm(frmMyRecipientDetail);
      TMBUtil.DestroyForm(frmMyRecipientEditAccComplete);
      TMBUtil.DestroyForm(frmMyRecipientEditAccount);
      TMBUtil.DestroyForm(frmMyRecipientEditProfile);
      TMBUtil.DestroyForm(frmMyRecipientEditProfileComp);
      frmMyRecipients.segMyRecipient.removeAll();
      TMBUtil.DestroyForm(frmMyRecipients);
      frmMyRecipientSelectContacts.segMyRecipient.removeAll();
      TMBUtil.DestroyForm(frmMyRecipientSelectContacts);
      frmMyRecipientSelectContactsComp.segment24733076528972.removeAll();
      TMBUtil.DestroyForm(frmMyRecipientSelectContactsComp);
      frmMyRecipientSelectContactsConf.segMyRecipient.removeAll();
      TMBUtil.DestroyForm(frmMyRecipientSelectContactsConf);
      frmMyRecipientSelectFacebook.segMyRecipient.removeAll();
      TMBUtil.DestroyForm(frmMyRecipientSelectFacebook);
      TMBUtil.DestroyForm(frmMyRecipientSelectFBComp);
      frmMyRecipientSelectFBConf.segment24733076528972.removeAll();
      TMBUtil.DestroyForm(frmMyRecipientSelectFBConf);
      frmMyRecipientSelectFbID.segMyRecipient.removeAll();
      TMBUtil.DestroyForm(frmMyRecipientSelectFbID);
      frmMyRecipientSelectMobile.segMyRecipient.removeAll();
      TMBUtil.DestroyForm(frmMyRecipientSelectMobile);
      TMBUtil.DestroyForm(frmMyRecipientViewAccount);
      frmMyTopUpComplete.segComplete.removeAll();
      TMBUtil.DestroyForm(frmMyTopUpComplete);
      frmMyTopUpCompleteCalendar.segComplete.removeAll();
      TMBUtil.DestroyForm(frmMyTopUpCompleteCalendar);
      TMBUtil.DestroyForm(frmMyTopUpEditScreens);
      frmMyTopUpList.segBillersList.removeAll();
      frmMyTopUpList.segSuggestedBillers.removeAll();
      TMBUtil.DestroyForm(frmMyTopUpList);
      frmMyTopUpSelect.segSelectList.removeAll();
      TMBUtil.DestroyForm(frmMyTopUpSelect);
      frmMyTransferRecipient.segMyRecipient.removeAll();
      TMBUtil.DestroyForm(frmMyTransferRecipient);
      TMBUtil.DestroyForm(frmNotificationDetails);
      frmNotificationHome.segMyRecipient.removeAll();
      TMBUtil.DestroyForm(frmNotificationHome);
      TMBUtil.DestroyForm(frmOpenAccountNSConfirmation);
      TMBUtil.DestroyForm(frmOpenAccountNSConfirmationCalendar);
      TMBUtil.DestroyForm(frmOpenAccountSavingCareMB);
      frmOpenAccTermDeposit.segToTDAct.removeAll();
      TMBUtil.DestroyForm(frmOpenAccTermDeposit);
      TMBUtil.DestroyForm(frmOpenAcDreamSaving);
      TMBUtil.DestroyForm(frmOpenActDSAck);
      TMBUtil.DestroyForm(frmOpenActDSAckCalendar);
      TMBUtil.DestroyForm(frmOpenActDSConfirm);
      TMBUtil.DestroyForm(frmOpenActSavingCareCnfNAck);
      TMBUtil.DestroyForm(frmOpenActSavingCareCnfNAckCalendar);
      frmOpenActSelProd.segOpenActSelProd.removeAll();
      TMBUtil.DestroyForm(frmOpenActSelProd);
      TMBUtil.DestroyForm(frmOpenActTDAck);
      TMBUtil.DestroyForm(frmOpenActTDAckCalendar);
      TMBUtil.DestroyForm(frmOpenActTDConfirm);
      TMBUtil.DestroyForm(frmOpenProdDetnTnC);
      TMBUtil.DestroyForm(frmOpnActSelAct);
      TMBUtil.DestroyForm(frmPostLoginMenu);
      frmPostLoginMenuNew.SegMainMenu.removeAll();
      frmPostLoginMenuNew.segment247410543438941.removeAll();
      TMBUtil.DestroyForm(frmPostLoginMenuNew);
      TMBUtil.DestroyForm(frmPromotion);
      TMBUtil.DestroyForm(frmPromotionDetails);
      frmRequestHistory.segReqHistory.removeAll();
      TMBUtil.DestroyForm(frmRequestHistory);
      TMBUtil.DestroyForm(frmSchedule);
      TMBUtil.DestroyForm(frmScheduleBillPayEditFuture);
      TMBUtil.DestroyForm(frmScheduleTransfer);
      frmSelectBiller.segMyBills.removeAll();
      frmSelectBiller.segSuggestedBillers.removeAll();
      TMBUtil.DestroyForm(frmSelectBiller);
      TMBUtil.DestroyForm(frmSPABlank);
      //TMBUtil.DestroyForm(frmSPALogin);
      TMBUtil.DestroyForm(frmSPATnC);
      TMBUtil.DestroyForm(frmSpaTokenactivationstartup);
      TMBUtil.DestroyForm(frmSpaTokenConfirmation);
      TMBUtil.DestroyForm(frmSSConfirmation);
      TMBUtil.DestroyForm(frmSSSApply);
      TMBUtil.DestroyForm(frmSSService);
      TMBUtil.DestroyForm(frmSSServiceED);
      TMBUtil.DestroyForm(frmSSSExecute);
      TMBUtil.DestroyForm(frmSSSExecuteCalendar);
      TMBUtil.DestroyForm(frmSSTnC);
      frmTimeDepositAccoutDetails.segment24751247741539.removeAll();
      TMBUtil.DestroyForm(frmTimeDepositAccoutDetails);
      frmTMBTransAck.segment2101271281126460.removeAll();
      TMBUtil.DestroyForm(frmTMBTransAck);
      TMBUtil.DestroyForm(frmTopUp);
      frmTranfersToRecipents.segTransferToRecipients.removeAll();
      TMBUtil.DestroyForm(frmTranfersToRecipents);
      frmTransferConfirm.segTransCnfmSplit.removeAll();
      TMBUtil.DestroyForm(frmTransferConfirm);
      //    TMBUtil.DestroyForm(frmTransferGeneratePDF);
      TMBUtil.DestroyForm(frmTransferLanding);
      TMBUtil.DestroyForm(frmTransfersAck);
      frmTransfersAckCalendar.segTransAckSplit.removeAll();
      TMBUtil.DestroyForm(frmTransfersAckCalendar);
      TMBUtil.DestroyForm(frmTransferStartuptest);
      TMBUtil.DestroyForm(frmViewTopUpBiller);
      TMBUtil.DestroyForm(frmMenu);
      //Popups
      AccntTransPwd.destroy();
      BillerNotAvailable.destroy();
      BillPayTopUpConf.destroy();
      DeletePop.destroy();
      helpMessage.destroy();
      popAccessPinBubble.destroy();
      popAccntConfirmation.destroy();
      popAddrCmboBox.destroy();
      popBankList.destroy();
      popBPTransactionPwd.destroy();
      popBubble.destroy();
      popDelRecipient.destroy();
      popDelRecipientProfile.destroy();
      popDelTopUp.destroy();
      popForgotPass.destroy();
      popProfilePic.destroy();
      popRecipientBankList.destroy();
      popTransactionPwd.destroy();
      popTransferConfirmOTPLock.destroy();
      popTransfersTDAccount.destroy();
      popupBubblePasswordRules.destroy();
      popupBubbleUserId.destroy();
      popupConfirmation.destroy();
      popupConfrmYes.destroy();
      popUpLogout.destroy();
      popUpMyBillers.destroy();
      popUpTermination.destroy();
      popupTractPwd.destroy();
      popupActivationHelp.destroy();
      popAllowOrNot.destroy();
      popATMBranch.destroy();
      popDreamSaving.destroy();
      popEditBillPayTransactionPwd.destroy();
      popExgRateDisclaimer.destroy();
      popFeedBack.destroy();
      popHelpMessageTrans.destroy();
      popInboxSortBy.destroy();
      popInboxSortBy2.destroy();
      popOtpSpa.destroy();
      popPromotions.destroy();
      popS2SAmntHelp.destroy();
      popSelDate.destroy();
      popUpCallCancel.destroy();
      popupEditBPDele.destroy();
      popupFTdel.destroy();
      popUploadPic.destroy();
      popUploadPicSpa.destroy();
      popUpNotfnDelete.destroy();
      //popUpPayBill.destroy();
      popUpProvinceData.destroy();
      popupSpaForgotPassword.destroy();
      popUpTopUpAmount.destroy();
      popupAddToMyRecipient.destroy();
      popupAddToMyBills.destroy();
      popGeneralMsg.destroy();
      TAGCPServerDetailsPopup.destroy();
      TMBCallPopup.destroy();
      popDelImageUpload.destroy();
      popupConfrmExitUpload.destroy();
      //IB
    } else {
      clearGVNotificationInboxIB();
      TMBUtil.DestroyForm(frmApplyMbviaIBConf);
      TMBUtil.DestroyForm(frmApplyMBviaIBConfirmation);
      TMBUtil.DestroyForm(frmApplyMBviaIBStep1);
      TMBUtil.DestroyForm(frmCD4Testing);
      TMBUtil.DestroyForm(frmIBAccntFullStatement);
      TMBUtil.DestroyForm(frmIBAccntStatementDummy);
      frmIBAccntSummary.segAccountDetails.removeAll();
      frmIBAccntSummary.segDebitCard.removeAll();
      frmIBAccntSummary.segMaturityDisplay.removeAll();
      TMBUtil.DestroyForm(frmIBAccntSummary);
      frmIBAccountSummary.segment2502646048174117.removeAll();
      frmIBAccountSummary.segMyAccounts.removeAll();
      TMBUtil.DestroyForm(frmIBAccountSummary);
      TMBUtil.DestroyForm(frmIBActivateIBankingStep1);
      TMBUtil.DestroyForm(frmIBActivateIBankingStep1Confirm);
      TMBUtil.DestroyForm(frmIBActivateIBankingStep2Confirm);
      TMBUtil.DestroyForm(frmIBActivationTandC);
      frmIBAddMyAccnt.segAccntDetails.removeAll();
      frmIBAddMyAccnt.segCmplete.removeAll();
      frmIBAddMyAccnt.segCnfrmtnStep2.removeAll();
      frmIBAddMyAccnt.segOtherBListFAA.removeAll();
      frmIBAddMyAccnt.segTMBAccntListFAA.removeAll();
      TMBUtil.DestroyForm(frmIBAddMyAccnt);
      frmIBATMBranch.segATMListDetails.removeAll();
      TMBUtil.DestroyForm(frmIBATMBranch);
      frmIBBeepAndBillApply.segBillersList.removeAll();
      frmIBBeepAndBillApply.segSuggestedBillersList.removeAll();
      TMBUtil.DestroyForm(frmIBBeepAndBillApply);
      TMBUtil.DestroyForm(frmIBBeepAndBillApplyCW);
      TMBUtil.DestroyForm(frmIBBeepAndBillConfAndComp);
      //TMBUtil.DestroyForm(frmIBBeepAndBillConfirmation);
      TMBUtil.DestroyForm(frmIBBeepAndBillExecuteConfComp);
      //TMBUtil.DestroyForm(frmIBBeepAndBillExecutedTxn);
      frmIBBeepAndBillList.segBillersList.removeAll();
      frmIBBeepAndBillList.segSuggestedBillersList.removeAll();
      TMBUtil.DestroyForm(frmIBBeepAndBillList);
      //TMBUtil.DestroyForm(frmIBBeepAndBillLP);
      TMBUtil.DestroyForm(frmIBBeepAndBillTandC);
      TMBUtil.DestroyForm(frmIBBillPaymentCompletenow);
      TMBUtil.DestroyForm(frmIBBillPaymentConfirm);
      TMBUtil.DestroyForm(frmIBBillPaymentCW);
      TMBUtil.DestroyForm(frmIBBillPaymentExecutedTxn);
      frmIBBillPaymentLP.segBPBillsList.removeAll();
      frmIBBillPaymentLP.segBPSgstdBillerList.removeAll();
      TMBUtil.DestroyForm(frmIBBillPaymentLP);
      TMBUtil.DestroyForm(frmIBBillPaymentView);
      TMBUtil.DestroyForm(frmIBChequeServiceConfirmation);
      TMBUtil.DestroyForm(frmIBChequeServiceReturnedChequeLanding);
      TMBUtil.DestroyForm(frmIBChequeServiceStopChequeAck);
      TMBUtil.DestroyForm(frmIBChequeServiceStopChequeLanding);
      TMBUtil.DestroyForm(frmIBChequeServiceViewReturnedCheque);
      TMBUtil.DestroyForm(frmIBCMChgMobNoTxnLimit);
      TMBUtil.DestroyForm(frmIBContactUs);
      TMBUtil.DestroyForm(frmIBCMChgPwd);

      TMBUtil.DestroyForm(frmIBCMChngUserID);
      TMBUtil.DestroyForm(frmIBCMConfirmation);
      TMBUtil.DestroyForm(frmIBCMConfirmationPwd);
      //TMBUtil.DestroyForm(frmIBCMConfirmEdit);
      TMBUtil.DestroyForm(frmIBCMEditMyProfile);

      frmIBCMMyProfile.segRequestHistory.removeAll();
      TMBUtil.DestroyForm(frmIBCMMyProfile);
      TMBUtil.DestroyForm(frmIBCommonMenu);
      TMBUtil.DestroyForm(frmIBCreateUserID);
      TMBUtil.DestroyForm(frmIBCustomerBasicInfo);
      TMBUtil.DestroyForm(frmIBDreamSAvingEdit);
      TMBUtil.DestroyForm(frmIBDreamSavingMaintenance);
      TMBUtil.DestroyForm(frmIBEditFutureBillPaymentPrecnf);
      TMBUtil.DestroyForm(frmIBEditFutureTopUpPrecnf);
      //TMBUtil.DestroyForm(frmIBEditGeneralInfo); - was giving error in logout
      frmIBEnterCrmidforS2S.segShowCrmId.removeAll();
      TMBUtil.DestroyForm(frmIBEnterCrmidforS2S);
      TMBUtil.DestroyForm(frmIBEnterCustomerIdLogin);
      TMBUtil.DestroyForm(frmIBePaymentExecutedTxn);
      TMBUtil.DestroyForm(frmIBExchangeRates);
      frmIBExecutedTransaction.segSplitDet.removeAll();
      TMBUtil.DestroyForm(frmIBExecutedTransaction);
      TMBUtil.DestroyForm(frmIBFBLogin);
      TMBUtil.DestroyForm(frmIBFirstTimeActivationComplete);
      TMBUtil.DestroyForm(frmIBFirstTimeUserSelection);
      TMBUtil.DestroyForm(frmIBFTrnsrEditCnfmtn);
      TMBUtil.DestroyForm(frmIBFTrnsrView);
      frmIBInboxHome.segRequestHistory.removeAll();
      TMBUtil.DestroyForm(frmIBInboxHome);
      //TMBUtil.DestroyForm(frmIBLogoutLanding);
      frmIBMyAccnts.segOtherBankAccntsList.removeAll();
      frmIBMyAccnts.segTMBAccntsList.removeAll();
      TMBUtil.DestroyForm(frmIBMyAccnts);
      frmIBMyActivities.datagridFT.removeAll();
      frmIBMyActivities.datagridhistory.removeAll();
      TMBUtil.DestroyForm(frmIBMyActivities);
      frmIBMyBillersHome.segAddBillerComplete.removeAll();
      frmIBMyBillersHome.segBillersConfirm.removeAll();
      frmIBMyBillersHome.segBillersList.removeAll();
      frmIBMyBillersHome.segSuggestedBillersList.removeAll();
      TMBUtil.DestroyForm(frmIBMyBillersHome);
      TMBUtil.DestroyForm(frmIBMyProfile);
      frmIBMyReceipentsAccounts.segmentRcAccounts.removeAll();
      frmIBMyReceipentsAccounts.segmentReceipentListing.removeAll();
      TMBUtil.DestroyForm(frmIBMyReceipentsAccounts);
      frmIBMyReceipentsAddBankAccnt.segementTobeAddedAccnts.removeAll();
      frmIBMyReceipentsAddBankAccnt.segmentReceipentListing.removeAll();
      TMBUtil.DestroyForm(frmIBMyReceipentsAddBankAccnt);
      frmIBMyReceipentsAddContactFB.segmentReceipentListing.removeAll();
      frmIBMyReceipentsAddContactFB.segmentSelectRc.removeAll();
      frmIBMyReceipentsAddContactFB.segmentSelectRcConf.removeAll();
      TMBUtil.DestroyForm(frmIBMyReceipentsAddContactFB);
      frmIBMyReceipentsAddContactManually.segmentRcAccounts.removeAll();
      frmIBMyReceipentsAddContactManually.segmentReceipentListing.removeAll();
      TMBUtil.DestroyForm(frmIBMyReceipentsAddContactManually);
      frmIBMyReceipentsAddContactManuallyConf.segmentRcAccounts.removeAll();
      frmIBMyReceipentsAddContactManuallyConf.segmentReceipentListing.removeAll();
      TMBUtil.DestroyForm(frmIBMyReceipentsAddContactManuallyConf);
      frmIBMyReceipentsEditAccountConf.segementTobeAddedAccntsConf.removeAll();
      frmIBMyReceipentsEditAccountConf.segmentReceipentListing.removeAll();
      TMBUtil.DestroyForm(frmIBMyReceipentsEditAccountConf);
      frmIBMyReceipentsFBContactConf.segementTobeAddedFBConf.removeAll();
      frmIBMyReceipentsFBContactConf.segmentReceipentListing.removeAll();
      TMBUtil.DestroyForm(frmIBMyReceipentsFBContactConf);
      frmIBMyReceipentsHome.segmentReceipentListing.removeAll();
      TMBUtil.DestroyForm(frmIBMyReceipentsHome);
      //TMBUtil.DestroyForm(frmIBMyTopUpHome);
      frmIBMyTopUpsHome.segAddBillerComplete.removeAll();
      frmIBMyTopUpsHome.segBillersConfirm.removeAll();
      frmIBMyTopUpsHome.segBillersList.removeAll();
      frmIBMyTopUpsHome.segSuggestedBillersList.removeAll();
      TMBUtil.DestroyForm(frmIBMyTopUpsHome);
      frmIBNotificationHome.segRequestHistory.removeAll();
      TMBUtil.DestroyForm(frmIBNotificationHome);
      frmIBOpenActSelProd.segOpenActSelProd.removeAll();
      TMBUtil.DestroyForm(frmIBOpenActSelProd);
      TMBUtil.DestroyForm(frmIBOpenNewDreamAcc);
      TMBUtil.DestroyForm(frmIBOpenNewDreamAccComplete);
      TMBUtil.DestroyForm(frmIBOpenNewDreamAccConfirmation);
      TMBUtil.DestroyForm(frmIBOpenNewSavingsAcc);
      TMBUtil.DestroyForm(frmIBOpenNewSavingsAccComplete);
      TMBUtil.DestroyForm(frmIBOpenNewSavingsAccConfirmation);
      TMBUtil.DestroyForm(frmIBOpenNewSavingsCareAcc);
      TMBUtil.DestroyForm(frmIBOpenNewSavingsCareAccComplete);
      TMBUtil.DestroyForm(frmIBOpenNewSavingsCareAccConfirmation);
      TMBUtil.DestroyForm(frmIBOpenNewTermDepositAcc);
      TMBUtil.DestroyForm(frmIBOpenNewTermDepositAccComplete);
      TMBUtil.DestroyForm(frmIBOpenNewTermDepositAccConfirmation);
      TMBUtil.DestroyForm(frmIBOpenProdDetnTnC);
      frmIBPostLoginDashboard.segAccountDetails.removeAll();
      //frmIBPostLoginDashboard.segMenuOptions.removeAll(); 		--removing as per ENH_028 - IB Menu on Home Screen
      TMBUtil.DestroyForm(frmIBPostLoginDashboard);
      //TMBUtil.DestroyForm(frmIBPreLogin);
      TMBUtil.DestroyForm(frmIBSSApply);
      TMBUtil.DestroyForm(frmIBSSApplyCmplete);
      TMBUtil.DestroyForm(frmIBSSApplyCnfrmtn);
      TMBUtil.DestroyForm(frmIBSSSDetails);
      TMBUtil.DestroyForm(frmIBSSSExcuteTrnsfr);
      TMBUtil.DestroyForm(frmIBSSSExcuteTrnsfrCmplete);
      TMBUtil.DestroyForm(frmIBSSSTnC);
      TMBUtil.DestroyForm(frmIBSTSExecutedTxn);
      TMBUtil.DestroyForm(frmIBTOADreamExecutedTxn);
      TMBUtil.DestroyForm(frmIBTOASavingsCareExecutedTxn);
      TMBUtil.DestroyForm(frmIBTOASavingsExecutedTxn);
      TMBUtil.DestroyForm(frmIBTOATDExecutedTxn);
      TMBUtil.DestroyForm(frmIBTokenActivationPage);
      TMBUtil.DestroyForm(frmIBTopUpComplete);
      TMBUtil.DestroyForm(frmIBTopUpConfirmation);
      TMBUtil.DestroyForm(frmIBTopUpCW);
      TMBUtil.DestroyForm(frmIBTopUpExecutedTxn);
      frmIBTopUpLandingPage.segBiller.removeAll();
      frmIBTopUpLandingPage.segSuggestedBiller.removeAll();
      TMBUtil.DestroyForm(frmIBTopUpLandingPage);
      TMBUtil.DestroyForm(frmIBTopUpViewNEdit);
      frmIBTranferLP.segXferRecipentsList.removeAll();
      TMBUtil.DestroyForm(frmIBTranferLP);
      TMBUtil.DestroyForm(frmIBTransferCustomWidgetLP);
      TMBUtil.DestroyForm(frmIBTransferGeneratePDF);
      frmIBTransferNowCompletion.segSplitDet.removeAll();
      TMBUtil.DestroyForm(frmIBTransferNowCompletion);
      frmIBTransferNowConfirmation.segSplitDet.removeAll();
      TMBUtil.DestroyForm(frmIBTransferNowConfirmation);
      frmIBTransferTemplate.segMenuOptions.removeAll();
      TMBUtil.DestroyForm(frmIBTransferTemplate);
      TMBUtil.DestroyForm(frmTokenActivation);
      TMBUtil.DestroyForm(frmMBLoanKYC);
      TMBUtil.DestroyForm(frmChangeLanguage);
    }
  }
  catch(Err) {

  }
}

function replaceCommon(str, source, destination) {

  var strNew = "";
  var patt = new RegExp(source, "g")
  if (str != null) {
    strNew = str.replace(patt, destination);
  }

  return strNew;
}


function webchatURL() {
  var locale = kony.i18n.getCurrentLocale();

  if (gblLoggedIn == true || gblLoggedIn == "true") {


    var firstname = "";
    var lastname = "";
    postChatAttributes  = true;
    //egainWebChat(firstname,lastname,postChatAttributes);
    if (locale == "en_US") {
      firstname = gblCustomerName.split(" ")[0];
      lastname = gblCustomerName.split(" ")[1];
      /*if (gblCustomerName.split(" ").length > 3) {
                lastname = gblCustomerName.split(" ")[2] + "" + gblCustomerName.split(" ")[3];
            } else {
                lastname = gblCustomerName.split(" ")[2];
            }*/
      // kony.application.openURL(kony.i18n.getLocalizedString("WEBCHAT_POSTLOGIN").trim() + "?name=" + firstname + "&lastname=" + lastname + "&email=" + gblEmailId + "&phone=" + gblPHONENUMBER + "&subject=1")
    } else {
      firstname = gblCustomerNameTh.split(" ")[0];
      lastname = gblCustomerNameTh.split(" ")[1];
      /* if (gblCustomerNameTh.split(" ").length > 3) {

                lastname = gblCustomerNameTh.split(" ")[2] + "" + gblCustomerNameTh.split(" ")[3];
            } else {

                lastname = gblCustomerNameTh.split(" ")[2];
            }*/
      //kony.application.openURL(kony.i18n.getLocalizedString("WEBCHAT_POSTLOGIN").trim() + "?name=" + firstname + "&lastname=" + lastname + "&email=" + gblEmailId + "&phone=" + gblPHONENUMBER + "&subject=1")
    }
    egainWebChat(firstname,lastname,postChatAttributes);

  } else {
    postChatAttributes  = false;

    egainWebChat(firstname,lastname,postChatAttributes);
    //egainChat.openHelp();
    //kony.application.openURL(kony.i18n.getLocalizedString("WEBCHAT_PRELOGIN").trim() + "?language=" + locale)
  }
}
function egainWebChat(firstname,lastname,postChatAttributes)
{ 
  var egainChat = {};

  egainChat.egainChatParameters = { first_name: firstname, last_name: lastname, email_address: gblEmailId, phone_number: gblPHONENUMBER };
  //Set to true to enable posting attributes to templates.
  egainChat.postChatAttributes  = postChatAttributes;

  egainChat.eglvchathandle = null;
  // egainChat.liveServerURL = "http://10.201.83.160/system";
  egainChat.liveServerURL = kony.i18n.getLocalizedString("WEBCHAT_LIVESERVICE");
  openHelp(egainChat);
  /*To be called by client website. All the parameters specified in eGainLiveConfig must be set here.*/
  storeChatParameters(attributeName, attributeValue,egainChat); 
  writeIframeIfRequired(egainChat);
}
function storeChatParameters(attributeName, attributeValue,egainChat)
{
  egainChat.egainChatParameters[attributeName] = attributeValue;
}

openHelp = function(egainChat) {
  var domainRegex = /^((?:https?:\/\/)?(?:www\.)?([^\/]+))/i;
  try{
    if( egainChat.eglvchathandle != null && egainChat.eglvchathandle.closed == false ){
      egainChat.eglvchathandle.focus();
      return;
    }
  }
  catch(err){}
  var refererName = "";
  refererName = encodeURIComponent(refererName);
  var refererurl = encodeURIComponent(document.location.href);
  var hashIndex = refererurl.lastIndexOf('#');
  if(hashIndex != -1){
    refererurl = refererurl.substring(0,hashIndex);
  }
  var eglvcaseid = (/eglvcaseid=[0-9]*/gi).exec(window.location.search);
  var vhtIds = '';
  if(typeof EGAINCLOUD != "undefined" && EGAINCLOUD.Account.getAllIds)
  {
    var ids = EGAINCLOUD.Account.getAllIds();
    vhtIds = '&aId=' + ids.a + '&sId=' + ids.s +'&uId=' + ids.u;
  }
  var EG_CALL_Q = window.EG_CALL_Q || [];
  EG_CALL_Q.push( ["enableTracker", true] );
  var webchaturldb = kony.i18n.getLocalizedString("MIB_WEBCHAT_EGAIN");
  var eGainChatUrl;
  var curr_lang = kony.i18n.getCurrentLocale();
  if (curr_lang == "th_TH") 
    eGainChatUrl = webchaturldb+'&postChatAttributes='+egainChat.postChatAttributes+'&eglvrefname='+refererName+'&'+eglvcaseid+vhtIds;
  else
    eGainChatUrl = webchaturldb+'&postChatAttributes='+egainChat.postChatAttributes+'&eglvrefname='+refererName+'&'+eglvcaseid+vhtIds;	
  var domain = domainRegex.exec(eGainChatUrl)[0];
  if( window.navigator.userAgent.indexOf("Trident") != -1 && egainChat.postChatAttributes ) {
    var win = document.getElementById('egainChatDomainFrame');
    win.contentWindow.postMessage(JSON.stringify(egainChat.egainChatParameters), domain);
  }
  if( (eGainChatUrl + refererurl).length <= 2000)
    eGainChatUrl += '&referer='+refererurl;
  var params ="resizable=yes,scrollbars=yes,toolbar=no";

  if (egainChat.postChatAttributes)
    storeChatParameters('eGainChatIdentifier','true',egainChat);
  egainChat.eglvchathandle = window.open( eGainChatUrl,'',params)
  /*Message posted to the child window every second until it sends a message in return. This is done as we can not be sure when the mssage listener will be set in the child window.*/
  if( window.navigator.userAgent.indexOf("Trident") == -1 && egainChat.postChatAttributes ) {
    var messagePostInterval = setInterval(function(){
      var message = egainChat.egainChatParameters;
      egainChat.eglvchathandle.postMessage(message, domain);
    },1000);
    window.addEventListener('message',function(event) {
      if(event.data.chatParametersReceived) {
        clearInterval(messagePostInterval);
      }
    },false);
  }
}
writeIframeIfRequired = function(egainChat) {
  if (typeof egainChat !== "undefined") {
    if(egainChat.postChatAttributes  &&  window.navigator.userAgent.indexOf("Trident") != -1 ) {
      var iframe = document.createElement('iframe');
      iframe.src=egainChat.liveServerURL+'/web/view/live/customer/storeparams.html';
      iframe.style.display = 'none';
      iframe.name = 'egainChatDomainFrame';
      iframe.id = 'egainChatDomainFrame';
      document.body.appendChild(iframe);
    }
  }
}

function setApplicationCallbacks()
{
  try {
    kony.print(">>===============setApplicationCallbacks start======="+new Date());
    var callbacksObj = {onactive:onactiveEvent,oninactive:oninactiveEvent,onbackground:onbackgroundEvent,onforeground:onforegroundEvent,onappterminate:onappterminateEvent};
    kony.application.setApplicationCallbacks(callbacksObj);
    kony.print("<<===============setApplicationCallbacks executed======="+new Date());
  }catch(e){
	kony.print("Error setApplicationCallbacks ==="+e.stack);
  }
}
//onactive function 
function onactiveEvent()
{
	kony.print("===============onactiveEvent=======");
     var form = kony.application.getCurrentForm();
    if (form != null && form != undefined) {
      if (form.id == "frmMBPreLoginAccessesPin") {
        if(gblgetPhrasesCalled == false)
           callGetPhrasesOnForeground();       
    }        
   }
}

//oninactive function
function oninactiveEvent()
{
	kony.print("===============oninactiveEvent=======");
}

gblIsBackgroundEvent = false;
//onbackground function 
function onbackgroundEvent()
{
    //No need of listener as we are using kony API for finger print
   /* if(gblDeviceInfo.model!="SM-N910C"){
                stopFingerPrintListening();
    } */
	kony.print("===============onbackgroundEvent=======");
	gblIsBackgroundEvent = true;
}

//onforeground function
function onforegroundEvent()
{
	kony.print("===============onforegroundEvent=======");
	gblIsBackgroundEvent = false;    
  	frmscanqrLandingStartCamera();
  //recheckMBUnavailable();
    var form = kony.application.getCurrentForm();
    if (form != null && form != undefined) {
      if (form.id == "frmMBPreLoginAccessesPin") {
        if(gblgetPhrasesCalled == false)
           callGetPhrasesOnForeground();
        
        //MIB-14053: Use Kony API for Touch ID Not required as we are using kony API
       /* if(gblDeviceInfo.model!="SM-N910C"){
        startFingerPrintListening();
      } */
    } else if (form.id == "frmENotificationSettings") {
     	  foreGroundOnENotificaions();
    }         
   }
}

//onappterminate function
function onappterminateEvent()
{
	kony.print("===============onappterminateEvent=======");
}
function onforegroundApp(){
  var getEncrKeyFromDevice = kony.store.getItem("encrytedText");
  //if (getEncrKeyFromDevice != null && !isSignedUser)
  //	frmAfterLogoutMB.show()
}
/*function genKeyForFile(){
	var randomNumb = Math.floor((Math.random() * 10000) + 1);
	seed(randomNumb);
	genckey("1");
	genKeyTest();
}*/

/*function genKeyTest(){
	var encWorks = false; 
	var testStr = "testPay";
	var loopCount = 0;
	while (!encWorks && loopCount < 5) {
		var testEncStr = enc(cPubModulo.toString(), cPubExp.toString(), testStr);

		var testDecStr =dec(testEncStr, cryptoPrivateVal.toString(), cryptoP.toString(), cryptoQ.toString());

		if(testDecStr == testStr){
			encWorks = true;

		}else{

			genckey("1");
			loopCount = loopCount+1;
		}

	}
}*/

/*function sortObject(o) {
    var sorted = {},
    key, a = [];

    for (key in o) {
    	if (o.hasOwnProperty(key)) {
    		a.push(key);
    	}
    }

    a.sort();

    for (key = 0; key < a.length; key++) {
    	sorted[a[key]] = o[a[key]];
    }
    return sorted;
}*/


//function to call the form globals to resolve the MENU issue

function spaFormGlobalsCall()
{		
  if(flowSpa)
  {
    TMBUtil.DestroyForm(frmAccountDetailsMB);
    TMBUtil.DestroyForm(frmAccountStatementMB);
    TMBUtil.DestroyForm(frmAccountStatementMBDummy);
    TMBUtil.DestroyForm(frmAccountSummaryLanding);
    TMBUtil.DestroyForm(frmAccTrcPwdInter);
    TMBUtil.DestroyForm(frmAddNewBeepnBillMB);
    TMBUtil.DestroyForm(frmAddTopUpBiller);
    TMBUtil.DestroyForm(frmAddTopUpBillerconfrmtn);
    TMBUtil.DestroyForm(frmAddTopUpToMB);
    //TMBUtil.DestroyForm(frmAfterLogoutMB);
    TMBUtil.DestroyForm(frmApplyInternetBankingMB);
    TMBUtil.DestroyForm(frmApplyMBConfirmationSPA);
    TMBUtil.DestroyForm(frmApplyMBSPA);
    TMBUtil.DestroyForm(frmApplyServicesMB);
    TMBUtil.DestroyForm(frmAppTour);
    TMBUtil.DestroyForm(frmATMBranch);
    TMBUtil.DestroyForm(frmATMBranchesDetails);
    TMBUtil.DestroyForm(frmATMBranchList);
    TMBUtil.DestroyForm(frmBBApplyBeepAndBillPayment);
    TMBUtil.DestroyForm(frmBBApplyNow); 
    TMBUtil.DestroyForm(frmBBConfirmAndComplete);
    TMBUtil.DestroyForm(frmBBExecuteConfirmAndComplete);
    TMBUtil.DestroyForm(frmBBMyBeepAndBill);
    TMBUtil.DestroyForm(frmBillPayment);
    TMBUtil.DestroyForm(frmBillPaymentComplete);
    TMBUtil.DestroyForm(frmBillPaymentCompleteCalendar);
    TMBUtil.DestroyForm(frmBillPaymentConfirmationFuture);
    TMBUtil.DestroyForm(frmBillPaymentEdit);
    TMBUtil.DestroyForm(frmBillPaymentView);
    TMBUtil.DestroyForm(frmBlank);
    TMBUtil.DestroyForm(frmChangeMobNoTransLimitMB);
    TMBUtil.DestroyForm(frmCMChgAccessPin);
    TMBUtil.DestroyForm(frmCMChgPwdSPA);
    TMBUtil.DestroyForm(frmCMChgTransPwd);
    TMBUtil.DestroyForm(frmConfirmationarchived);
    TMBUtil.DestroyForm(frmConnectAccMB);
    TMBUtil.DestroyForm(frmContactUsCompleteScreenMB);
    TMBUtil.DestroyForm(frmContactusFAQMB);
    TMBUtil.DestroyForm(frmContactUsMB);
    TMBUtil.DestroyForm(frmDreamCalculator);
    TMBUtil.DestroyForm(frmDreamSavingEdit);
    TMBUtil.DestroyForm(frmDreamSavingMB);
    TMBUtil.DestroyForm(frmEditFutureBillPaymentComplete);
    TMBUtil.DestroyForm(frmEditFutureBillPaymentConfirm);
    TMBUtil.DestroyForm(frmeditMyProfile);
    TMBUtil.DestroyForm(frmExchangeRate);
    //   TMBUtil.DestroyForm(frmFBLogin);
    TMBUtil.DestroyForm(frmFBProfileLogin);
    TMBUtil.DestroyForm(frmFeedbackComplete);
    TMBUtil.DestroyForm(frmFullStatement);
    TMBUtil.DestroyForm(frmGeneratecrmid);
    TMBUtil.DestroyForm(frmInboxDetails);
    TMBUtil.DestroyForm(frmInboxHome);
    TMBUtil.DestroyForm(frmMBAccLocked);
    TMBUtil.DestroyForm(frmMBActiComplete);
    TMBUtil.DestroyForm(frmMBActiConfirm);
    TMBUtil.DestroyForm(frmMBActivation);
    TMBUtil.DestroyForm(frmMBApplyBeepAndBillPayment);
    TMBUtil.DestroyForm(frmMBFTEdit);
    TMBUtil.DestroyForm(frmMBFTEditCmplete);
    TMBUtil.DestroyForm(frmMBFTEditCmpleteCalendar);
    TMBUtil.DestroyForm(frmMBFTEditCnfrmtn);
    TMBUtil.DestroyForm(frmMBFTSchduleOldFt);
    TMBUtil.DestroyForm(frmMBFtSchedule);
    TMBUtil.DestroyForm(frmMBFTView);
    TMBUtil.DestroyForm(frmMBFTViewCalendar);
    TMBUtil.DestroyForm(frmMBMyActivities);
    TMBUtil.DestroyForm(frmMBsetPasswd);
    TMBUtil.DestroyForm(frmMBSetuseridSPA);
    TMBUtil.DestroyForm(frmMBTnC);
    TMBUtil.DestroyForm(frmMyAccntAddAccount);
    TMBUtil.DestroyForm(frmMyAccntConfirmationAddAccount);
    TMBUtil.DestroyForm(frmMyAccountEdit);
    TMBUtil.DestroyForm(frmMyAccountList);
    TMBUtil.DestroyForm(frmMyAccountView);
    TMBUtil.DestroyForm(frmMyProfile);
    TMBUtil.DestroyForm(frmMyProfileReqHistory);
    TMBUtil.DestroyForm(frmMyRecipientAddAcc);
    TMBUtil.DestroyForm(frmMyRecipientAddAccComplete);
    TMBUtil.DestroyForm(frmMyRecipientAddAccConf);
    TMBUtil.DestroyForm(frmMyRecipientAddProfile);
    TMBUtil.DestroyForm(frmMyRecipientAddProfileComp);
    TMBUtil.DestroyForm(frmMyRecipientDetail);
    TMBUtil.DestroyForm(frmMyRecipientEditAccComplete);
    TMBUtil.DestroyForm(frmMyRecipientEditAccount);
    TMBUtil.DestroyForm(frmMyRecipientEditProfile);
    TMBUtil.DestroyForm(frmMyRecipientEditProfileComp);
    TMBUtil.DestroyForm(frmMyRecipients);
    TMBUtil.DestroyForm(frmMyRecipientSelectContacts);
    TMBUtil.DestroyForm(frmMyRecipientSelectContactsComp);
    TMBUtil.DestroyForm(frmMyRecipientSelectContactsConf);
    TMBUtil.DestroyForm(frmMyRecipientSelectFacebook);
    TMBUtil.DestroyForm(frmMyRecipientSelectFBComp);
    TMBUtil.DestroyForm(frmMyRecipientSelectFBConf);
    TMBUtil.DestroyForm(frmMyRecipientSelectFbID);
    TMBUtil.DestroyForm(frmMyRecipientSelectMobile);
    TMBUtil.DestroyForm(frmMyRecipientViewAccount);
    TMBUtil.DestroyForm(frmMyTopUpComplete);
    TMBUtil.DestroyForm(frmMyTopUpCompleteCalendar);
    TMBUtil.DestroyForm(frmMyTopUpEditScreens);
    TMBUtil.DestroyForm(frmMyTopUpList);
    TMBUtil.DestroyForm(frmMyTopUpSelect);
    TMBUtil.DestroyForm(frmMyTransferRecipient);
    TMBUtil.DestroyForm(frmNotificationDetails);
    TMBUtil.DestroyForm(frmNotificationHome);
    TMBUtil.DestroyForm(frmOpenAccountNSConfirmation);
    TMBUtil.DestroyForm(frmOpenAccountNSConfirmationCalendar);
    TMBUtil.DestroyForm(frmOpenAccountSavingCareMB);
    TMBUtil.DestroyForm(frmOpenAccTermDeposit);
    TMBUtil.DestroyForm(frmOpenAcDreamSaving);
    TMBUtil.DestroyForm(frmOpenActDSAck);
    TMBUtil.DestroyForm(frmOpenActDSAckCalendar);
    TMBUtil.DestroyForm(frmOpenActDSConfirm);
    TMBUtil.DestroyForm(frmOpenActSavingCareCnfNAck);
    TMBUtil.DestroyForm(frmOpenActSavingCareCnfNAckCalendar);
    TMBUtil.DestroyForm(frmOpenActSelProd);
    TMBUtil.DestroyForm(frmOpenActTDAck);
    TMBUtil.DestroyForm(frmOpenActTDAckCalendar);
    TMBUtil.DestroyForm(frmOpenActTDConfirm);
    TMBUtil.DestroyForm(frmOpenProdDetnTnC);
    TMBUtil.DestroyForm(frmOpnActSelAct);
    TMBUtil.DestroyForm(frmPostLoginMenu);
    TMBUtil.DestroyForm(frmPostLoginMenuNew);
    TMBUtil.DestroyForm(frmPromotion);
    TMBUtil.DestroyForm(frmPromotionDetails);
    TMBUtil.DestroyForm(frmRequestHistory);
    TMBUtil.DestroyForm(frmSchedule);
    TMBUtil.DestroyForm(frmScheduleBillPayEditFuture);
    TMBUtil.DestroyForm(frmScheduleTransfer);
    TMBUtil.DestroyForm(frmSelectBiller);
    TMBUtil.DestroyForm(frmSPABlank);
    //TMBUtil.DestroyForm(frmSPALogin);
    TMBUtil.DestroyForm(frmSPATnC);
    TMBUtil.DestroyForm(frmSpaTokenactivationstartup);
    TMBUtil.DestroyForm(frmSpaTokenConfirmation);
    TMBUtil.DestroyForm(frmSSConfirmation);
    TMBUtil.DestroyForm(frmSSSApply);
    TMBUtil.DestroyForm(frmSSService);
    TMBUtil.DestroyForm(frmSSServiceED);
    TMBUtil.DestroyForm(frmSSSExecute);
    TMBUtil.DestroyForm(frmSSSExecuteCalendar);
    TMBUtil.DestroyForm(frmSSTnC);
    TMBUtil.DestroyForm(frmTimeDepositAccoutDetails);
    TMBUtil.DestroyForm(frmTMBTransAck);
    TMBUtil.DestroyForm(frmTopUp);
    TMBUtil.DestroyForm(frmTranfersToRecipents);
    TMBUtil.DestroyForm(frmTransferConfirm);
    //     TMBUtil.DestroyForm(frmTransferGeneratePDF);
    TMBUtil.DestroyForm(frmTransferLanding);
    TMBUtil.DestroyForm(frmTransfersAck);
    TMBUtil.DestroyForm(frmTransfersAckCalendar);
    //TMBUtil.DestroyForm(frmTransferStartuptest);
    TMBUtil.DestroyForm(frmViewTopUpBiller);
  }
}

function isNotBlank(inputData) {
	try {
		inputData = inputData + "";
		if (inputData != null && inputData != "null" && inputData != undefined && inputData != "undefined" && inputData.trim() != "")
			return true;
		else
			return false;
	} catch (e) {
		kony.print("Error isNotBlank " + e.stack);
		return isNotBlankObject(inputData);
	}
}

function isNotBlankObject(inputDataObject) {
	try {
		for (var prop in inputDataObject) {
			if (inputDataObject.hasOwnProperty(prop))
				return true;
		}
		return false;
	} catch (e) {
		kony.print("Error isNotBlankObject " + e.stack);
		return false;
	}
}

//Encryption
function encryptData(plainText) {
  var iv = "e675f725e675f725";
  var salt = "a1b2c3d4e5f61234";
  var iterationCount = 1023;
  var passPhrase = "KONY";

  var encrypt = plainText;

  //#ifdef android
  encrypt = Encrypt.encryptDataWithPhrase(plainText,passPhrase);
  //#else
  //#ifdef iphone
  if(isNotBlank(plainText)) { 
    var key = CryptoJS.PBKDF2(passPhrase, CryptoJS.enc.Utf8.parse(salt), {keySize: 256/32, iterations: iterationCount });
    encrypt = CryptoJS.AES.encrypt(plainText, key, {iv: CryptoJS.enc.Utf8.parse(iv)}).ciphertext.toString(CryptoJS.enc.Base64);
  }
  //#endif
  //#endif

  return encrypt;
}


function encryptCard(plainText) {
  var iv = "e675f725e675f725";
  var salt = "a1b2c3d4e5f61234";
  var iteration_couter = 1023;
  var password = gblAESKey;  //"1afc06aa242190566105ab3f971056e3e4d5471b36a4667ea5b8a2eca318959e"
  var encrypt = plainText;

  //#ifdef android
  encrypt = Encrypt.encryptDataWithPhrase(plainText,password);
  //#else
  //#ifdef iphone
  if(isNotBlank(plainText)) {
    var key = CryptoJS.PBKDF2(password, CryptoJS.enc.Utf8.parse(salt), {keySize: 256/32, iterations: iteration_couter });
    encrypt = CryptoJS.AES.encrypt(plainText, key, {iv: CryptoJS.enc.Utf8.parse(iv)}).ciphertext.toString(CryptoJS.enc.Base64);

  }
  //#endif
  //#endif

  return encrypt;

}

//Code to remove kony i18n generated new lines with browser native new lines
function newLineTextIB(alertText) {
  if(alertText.indexOf("\\n") > -1) {
    alertText = alertText.replace(/\\n/g, "\n");
  } 

  if(alertText.indexOf("<br/>") > -1) {
    alertText = alertText.replace(/<br\/>/g, "\n");
  } 

  if(alertText.indexOf("<br>") > -1) {
    alertText = alertText.replace(/<br>/g, "\n");
  } 

  return alertText;
}

function getCopyRightText(){
  curr_lang = kony.i18n.getCurrentLocale();
  var date = new Date();
  var copyRightText="";
  copyright_year = date.getFullYear()
  if (curr_lang == "th_TH") {
    copyRightText = kony.i18n.getLocalizedString("keyIBFooterCopyrightFirstHalf") + " " + (Number(
      copyright_year) + 543) + " " + kony.i18n.getLocalizedString("keyIBFooterCopyrightSecondHalf");
  } else {
    copyRightText = kony.i18n.getLocalizedString("keyIBFooterCopyrightFirstHalf") + " " +
      copyright_year + ". " + kony.i18n.getLocalizedString("keyIBFooterCopyrightSecondHalf");	 
  }
  return copyRightText;
}


function onClickSecurityAdvices(){
  if (kony.i18n.getCurrentLocale() == "en_US"){
    kony.application.openURL(GLOBAL_SecurityAdvice_Link_EN)
  } else {
    kony.application.openURL(GLOBAL_SecurityAdvice_Link_TH)
  }		
  //Commented for ENH_209
  /*try{

		showLoadingScreen();
		inputParams = {};
		invokeServiceSecureAsync("GetExchangeRateData", inputParams, callBackExchangeRates)
	}catch(e){	

	}*/
}

function setCurrentLocale(){
  var deviceLanguage = kony.i18n.getCurrentDeviceLocale();
		var getcurAppLocale = kony.store.getItem("curAppLocale");
		if (getcurAppLocale != null) {
			if (gblDeviceInfo["name"] != "thinclient" && gblDeviceInfo["name"] == "android") {
				kony.i18n.setCurrentLocaleAsync(getcurAppLocale["appLocale"], onsuccesscallback, onfailurecallback, "");
			} else if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPhone Simulator") {
				kony.i18n.setCurrentLocaleAsync(getcurAppLocale["appLocale"], onsuccess(), onfailure(), "");
			}
		} else {
			if (gblDeviceInfo["name"] != "thinclient" && gblDeviceInfo["name"] == "android") {
				//For Android, we are getting a table of i18n information where as for iPhone, we are getting simple string hence the logic is chagned.
				if (kony.string.startsWith(deviceLanguage.language, "th", true)) {
					kony.i18n.setCurrentLocaleAsync("th_TH", onsuccesscallback, onfailurecallback, "");
				} else {
					kony.i18n.setCurrentLocaleAsync("en_US", onsuccesscallback, onfailurecallback, "");
				}
			} else if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPhone Simulator") {
				if (kony.string.startsWith(deviceLanguage, "th", true)) {
					kony.i18n.setCurrentLocaleAsync("th_TH", onsuccess(), onfailure(), "");
				} else {
					kony.i18n.setCurrentLocaleAsync("en_US", onsuccess(), onfailure(), "");
				}
			}
		}
}


function appMFPostInit() {
	try {
		pre_init(); 
		kony.print("appPreInit()---->START");
        navigationStack = [];
        setApplicationCallbacks();
        registerPushCallBacks();
		kony.print("appPreInit()---->STOP");
      
		appConfig.middlewareContext = "services";
		appConfig.serverIp = gblServerIp;//"mibdev1.tau2904.com";
		appConfig.secureServerPort = gblSecurePort;//"443";
		kony.print("gblUnavailableFlag = " + gblUnavailableFlag);
		kony.print("gblIsBackgroundEvent = " + gblIsBackgroundEvent);
				
	//	if (gblUnavailableFlag && !gblIsBackgroundEvent) {
	//		checkMBUnavailable(false);
	//	} else {
			if (kony.net.isNetworkAvailable(constants.NETWORK_TYPE_ANY)) {
				loadResourceBundle();
			} else {
				isMFsdkinit = false;
				dismissLoadingScreen();
				kony.print("====>Failed to initialize as client of Mobile Fabric. before call loadResourceBundle");
				//alert("Login failure" + JSON.stringify(error));
				alert(kony.i18n.getLocalizedString("genErrorWifiOff"));
			}
	} catch (e) {
		dismissLoadingScreen();
		kony.print("====>Exception in appPreInit MB:" + e.stack);
	}
}



function appMFPostInitIB(){
  try{
    kony.print("appPreInit()---->START");

    navigationStack=[];
    kony.print("appPreInit()---->STOP");

    appConfig.middlewareContext="services";
    mfClient = new kony.sdk();
    kony.sdk.skipAnonymousLoginCall(true);// for stopping "/login" call service
    kony.sdk.setXdomainLibPath("https://" + appConfig.serverIp +"/authService/resources/js/xdomain.min.js");
    var fullServerIp = "https://" + appConfig.serverIp;
    kony.sdk.setXdomainSlaves({fullServerIp:"/xdomain"});
    //if (gblUnavailableFlag) {
    //  	checkIBUnavailable(false);
   // }else{
    if (kony.net.isNetworkAvailable(constants.NETWORK_TYPE_ANY)) {
    	loadResourceBundleIB();  
    }else{
       isMFsdkinit = false;
	   dismissLoadingScreen();
	   kony.print("====>Failed to initialize as client of Mobile Fabric. before call loadResourceBundle");
	   //alert("Login failure" + JSON.stringify(error));
	   alert(kony.i18n.getLocalizedString("genErrorWifiOff"));
    }
  }catch(e){
    dismissLoadingScreen();
    kony.print("====>Exception in appPreInit IB:" + e.stack);
  }

}



function validCicsTranCode(cicstrancode) {

  if(isNotBlank(cicstrancode) && (cicstrancode.charAt(0) == '1' || cicstrancode.charAt(0) == '2')) {
    return true;
  } else {
    return false;
  }
}

function hasClass(el, className) {
  if(isNotBlank(el)){
    if (el.classList)
      return el.classList.contains(className)
      else
        return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'))
        }
}

function addClass(el, className) {
  if(isNotBlank(el)){  
    if (el.classList)
      el.classList.add(className)
      else if (!hasClass(el, className)) el.className += " " + className
        } 
}

function removeClass(el, className) {
  if(isNotBlank(el)){ 
    if (el.classList)
      el.classList.remove(className)
      else if (hasClass(el, className)) {
        var reg = new RegExp('(\\s|^)' + className + '(\\s|$)')
        el.className=el.className.replace(reg, ' ')
      }
  }
}

function removeBGColor (obj) {
  if(isNotBlank(obj)){
    if (obj.style.removeProperty) {
      obj.style.removeProperty ("background-color");
    } 
    else {
      obj.style.removeAttribute ("backgroundColor");
    }
  }
}

function removeBGImage (obj) {
  if(isNotBlank(obj)){	
    if (obj.style.removeProperty) {
      obj.style.removeProperty ("background-image");
    } 
    else {
      obj.style.removeAttribute ("backgroundImage");
    }
  }
}

function setWebkitTextSecurity (){
  var isChromeN = navigator.userAgent.toLowerCase().indexOf('chrome') > -1; 
  var isEdge = navigator.userAgent.toLowerCase().indexOf('edge') > -1; 	
  var chromeVersion = getChromeVersion();
  if(navigator.vendor!=undefined){
    var isVendorGoogle = navigator.vendor.toLowerCase().indexOf('google') > -1; 	
    if(isChromeN && chromeVersion >= 29 && isVendorGoogle && !isEdge){
      frmIBPreLogin_txtPassword.setAttribute("type","text");
      addClass(frmIBPreLogin_txtPassword , "webkitTextSecurity");
    }
  }else{
    if(isChromeN && chromeVersion >= 29 && !isEdge){
      frmIBPreLogin_txtPassword.setAttribute("type","text");
      addClass(frmIBPreLogin_txtPassword , "webkitTextSecurity");
    }
  }	
}

function getChromeVersion () {     
  var raw = navigator.appVersion.match(/Chrom(e|ium)\/([0-9]+)\./);

  return raw ? parseInt(raw[2], 10) : false;
}

function getChromeVersionFromUserAgent () {     
  var raw = navigator.userAgent.match(/Chrom(e|ium)\/([0-9]+)\./);

  return raw ? parseInt(raw[2], 10) : false;
}

function getChromeAppleWebKit () {     
  var raw = navigator.userAgent.match(/AppleWebKit\/([0-9]+)\./);

  return raw ? parseInt(raw[1], 10) : false;
}

function processAppService(params) {
	kony.print("usage >> start processAppService " + new Date().getTime());
    kony.print("processAppService launchmode =" + params["launchmode"]);
  	gblDeeplinkExternalAccess = false;
	gblContentDeeplinkData = "";
    if (params["launchmode"] == 3 ) {
        var quickActionItem = params["launchparams"]["quickactionitem"];
        var getEncrKeyFromDevice = kony.store.getItem("encrytedText");
        var startForm = frmMBanking;
        if (null != getEncrKeyFromDevice) {
            startForm = frmMBPreLoginAccessesPin;
        }
        if ("undefined" == typeof quickActionItem) {
          	  try {
              	if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPhone Simulator") {
                    if("undefined" != typeof params["launchparams"]["URL"] ){
                        var urlString = params["launchparams"]["URL"];
                        if(urlString.startsWith("insert")){
                          insertIOFunction.initFunction(urlString);
                          if (isSignedUser) {
                          	startForm = kony.application.getCurrentForm();
                          }
                        }else{
                          startForm = deepLinkAccess(params);
                        }
                      	return startForm;
                    }
                }else{
                    startForm = deepLinkAccess(params);
                    return startForm;
                }
              }catch(e){
                kony.print("Error invoking service"+e.stack);
              }
        } else
        if (quickActionItem) {
            gbl3dTouchAction = quickActionItem["id"];
            if (quickActionItem["id"] == "quickbalance") {
                gblTouchShow = false;
                if (isSignedUser == true) {
                    frmMBPreLoginAccessesPinPreShow();
                    return onLogoutPopUpConfrm();
                } else {
                    startForm = frmMBPreLoginAccessesPin;
                }
                return startForm;
            } else if (quickActionItem["id"] == "transfer") {
                if (isSignedUser == true) {
                    showLoadingScreen();
                    gbl3dTouchAction = "";
                    transferFromMenu();
                    startForm = frmPreTransferMB2;
                }
                return startForm;
            } else if (quickActionItem["id"] == "billpay") {
                if (isSignedUser == true) {
                    showLoadingScreen();
                    gbl3dTouchAction = "";
                    callBillPaymentFromMenu();
                    startForm = frmSelectBillerLanding;
                }
                return startForm;
            } else if (quickActionItem["id"] == "topup") {
                if (isSignedUser == true) {
                     showLoadingScreen();
				}
			}
		}
	}/*else if (params["launchmode"] == 2) {
    	
    	 if(GLOBAL_UV_STATUS_FLAG == "ON"){
           	startForm = frmBlank;
         } else {
            var getEncrKeyFromDevice = kony.store.getItem("encrytedText");
            startForm = frmMBanking;
            if(null != getEncrKeyFromDevice){
              startForm = frmMBPreLoginAccessesPin;
            }
         }		
		kony.print("AppService Push case, returning  frmBlank");
		return startForm;
	}*/
}


function getValueOrDefault(value, defaultValue){
  return isNotBlank(value) ? value : defaultValue;
}

function frmscanqrLandingStartCamera(){
  try {
    var currForm = kony.application.getCurrentForm();
    if (isNotBlank(currForm) && isNotBlank(currForm.id)) {
      if(currForm.id == "frmscanqrLanding"){
        frmscanqrLanding.customWidgetAndroid.startCamera();
      }
    }      
  }catch(e){
    kony.print("Error frmscanqrLandingStartCamera "+e.stack);
  }
}

function getTodaysDate() {
	// var today = new Date();
	var today = new Date(GLOBAL_TODAY_DATE);
	var dd = today.getDate();
	var mm = today.getMonth() + 1; //January is 0!
	var yyyy = today.getFullYear();
	if (dd < 10) dd = '0' + dd
	if (mm < 10) mm = '0' + mm
	today = yyyy + "-" + mm + "-" + dd
	return today;
}

/**
	This is common function, which is used to save in the Devie with Encripted Value.
	
**/
function tmbSaveOnDevice(dsSaveKey,dsSaveValue){
  
  	if(null !=dsSaveKey && "" !=dsSaveKey ){
	
      	kony.store.setItem(dsSaveKey, dsSaveValue);
      
    }
}

/**
	This is common function invoked to retrive any data which is saved in the Device, it will decrypt and returned plain text.
**/
function tmbRetriveFromDevice(dsRetiveKey){
 	
    var valueReturned = "";
  
    if(null !=dsRetiveKey && "" !=dsRetiveKey ){
	
      	valueReturned = kony.store.getItem(dsRetiveKey);
      
    }
  return valueReturned;
  
}

function deepLinkAccess(params){
  try{
    kony.print("@@@ In deepLinkAccess() @@@");
    var formControl = frmMBanking;
    if (null != kony.store.getItem("encrytedText")) {
    	formControl = frmMBPreLoginAccessesPin;
    }
    var urlSchema = params["launchparams"];
    var action = urlSchema["eventActions"];
    if (action == "openSavingAccount") { //Pass                
      loadFunctionalModuleSync("openAcctModule");
      if (!isSignedUser) {
        gblDeeplinkExternalAccess = true;
        gblContentDeeplinkData = params;
      }else{
        showLoadingScreen();
        gbl3dTouchAction = "";
        openMoreSave = true;
        OnClickOpenNewAccount();
        formControl = kony.application.getCurrentForm();
        dismissLoadingScreen();
      }
    }else if (action == "openSavingCareAccount") { //Pass                
      loadFunctionalModuleSync("openAcctModule");
      if (!isSignedUser) {
        gblDeeplinkExternalAccess = true;
        gblContentDeeplinkData = params;
      }else{
        showLoadingScreen();
        gbl3dTouchAction = "";
        gblProdCode = "211";
        isCmpFlow = true;
        OnClickOpenNewAccount();
        formControl = kony.application.getCurrentForm();
        dismissLoadingScreen();
      }
    }else if (action == "openAllFreeAccount") { //Pass                 
      loadFunctionalModuleSync("openAcctModule");
      if (!isSignedUser) {
        gblDeeplinkExternalAccess = true;
        gblContentDeeplinkData = params;
      }else{
        gbl3dTouchAction = "";
        gblProdCode = "225";
        isCmpFlow = true;
        OnClickOpenNewAccount();
        formControl = kony.application.getCurrentForm();
        dismissLoadingScreen();
      }
    }else if (action == "openNoFixedAccount") { //Pass                  
      loadFunctionalModuleSync("openAcctModule");
      if (!isSignedUser) {
        gblDeeplinkExternalAccess = true;
        gblContentDeeplinkData = params;
      }else{
        gbl3dTouchAction = "";
        gblProdCode = "221";
        isCmpFlow = true;
        OnClickOpenNewAccount();
        formControl = kony.application.getCurrentForm();
        dismissLoadingScreen();
      }
    }else if (action == "myMutualFunds") { //Pass                 
      loadFunctionalModuleSync("mutualFundModule");
      if (!isSignedUser) {
        gblDeeplinkExternalAccess = true;
        gblContentDeeplinkData = params;
      }else{
        gbl3dTouchAction = "";
        if(glblResulttable["mfAccountFlag"] == "true"){
          MBcallMutualFundsSummary();
          formControl = kony.application.getCurrentForm();
        }else{
          alert(kony.i18n.getLocalizedString("keyUserDontHaveMutualFund"));
          formControl = kony.application.getCurrentForm();
          if (kony.application.getCurrentForm().id == "frmMBPreLoginAccessesPin"){
            //frmAccountSummaryLanding.show();
            //formControl = frmAccountSummaryLanding;
            formControl = frmAccountSummary;
          	showAccountSummaryNewWithRefresh();
          }
        }
        dismissLoadingScreen();
      }
    }else if (action == "promptPayRegistration") { //Pass
      if (!isSignedUser) {
        gblDeeplinkExternalAccess = true;
        gblContentDeeplinkData = params;
      }else{
        gbl3dTouchAction = "";
        isCmpFlow = true;
        frmMBAnyIdRegTnC.show();
        dismissLoadingScreen();
        formControl = frmMBAnyIdRegTnC;
      }
    }else if (action == "appTour") { //pass                
      loadFunctionalModuleSync("appTourModule");
      gbl3dTouchAction = "";
      menuAppTouronClick();
      if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPhone Simulator") {
        formControl = frmAppTour;
      }else{
        formControl = kony.application.getCurrentForm();
      }
      dismissLoadingScreen();
    }else if (action == "tmbWOW") { // pass
      if (!isSignedUser) {
        gblDeeplinkExternalAccess = true;
        gblContentDeeplinkData = params;
      }else{
        gbl3dTouchAction = "";
        onClickTMBWow();
        formControl = frmLoyaltylanding;
        dismissLoadingScreen();
      }
    }else if (action == "tmbNewLoan") { // pass
      kony.print("DeepLink flow > tmbNewLoan");
      try {
        kony.modules.loadFunctionalModule("loanModule");
      }catch (err) {
        kony.print("Error loading functional module > loanModule");
        return false;
      }

      if (!isSignedUser) {
        gblDeeplinkExternalAccess = true;
        gblContentDeeplinkData = params;
      }else{
        if(gblEnabledLoan == "Y"){
		  gblCurrentLocaleBeforeLoan = kony.i18n.getCurrentLocale();
          gbl3dTouchAction = "";
          if(gblDeeplinkExternalAccess) {
            frmGetNewLoan.show();
          }
          formControl = frmGetNewLoan;
        }else{
          formControl = kony.application.getCurrentForm();
        }
        dismissLoadingScreen();
      }
    }else if (action == "tmbPersonalLoanProdList") { // pass
      kony.print("DeepLink flow > tmbPersonalLoanProdList");
      try {
        kony.modules.loadFunctionalModule("loanModule");
      } catch (err) {
        kony.print("Error loading functional module > loanModule");
        return false;
      }

      if (!isSignedUser) {
        gblDeeplinkExternalAccess = true;
        gblContentDeeplinkData = params;
      }else{
        if(gblEnabledLoan == "Y"){
		  gblCurrentLocaleBeforeLoan = kony.i18n.getCurrentLocale();
          gbl3dTouchAction = "";
          gblDeepLinkFlow = true;
          onClickPersonalLoan();
        }else{
          gbl3dTouchAction = "";
          gblDeepLinkFlow = false;
        }
        formControl = kony.application.getCurrentForm();
        dismissLoadingScreen();
      }
    }else if (action == "tmbLoanCCProdList") { // pass
      kony.print("DeepLink flow > tmbLoanCCProdList");
      try {
        kony.modules.loadFunctionalModule("loanModule");
      } catch (err) {
        kony.print("Error loading functional module > loanModule");
        return false;
      }

      if (!isSignedUser) {
        gblDeeplinkExternalAccess = true;
        gblContentDeeplinkData = params;
      }else{
        if(gblEnabledLoan == "Y"){
		 gblCurrentLocaleBeforeLoan = kony.i18n.getCurrentLocale();
          gbl3dTouchAction = "";
          gblDeepLinkFlow = true;
          onClickCreditCards();
        }else{
          gbl3dTouchAction = "";
          gblDeepLinkFlow = false;
        }
        formControl = kony.application.getCurrentForm();
        dismissLoadingScreen();
      }
    }else if (action == "tmbCash2GoLoanDetails") { // pass
      kony.print("DeepLink flow > tmbCash2GoLoanDetails");
      try {
        kony.modules.loadFunctionalModule("loanModule");
      } catch (err) {
        kony.print("Error loading functional module > loanModule");
        return false;
      }
      if (!isSignedUser) {
        gblDeeplinkExternalAccess = true;
        gblContentDeeplinkData = params;
      }else{
		gblCurrentLocaleBeforeLoan = kony.i18n.getCurrentLocale();
        gbl3dTouchAction = "tmbCash2GoLoanDetails";
        gblDeepLinkFlow = true;
        onClickPersonalLoan();
        formControl = kony.application.getCurrentForm();
        dismissLoadingScreen();
      }
    }else if (action == "tmbFeedback") { // pass
      kony.print("DeepLink flow > tmbFeedback");
      try {
        kony.modules.loadFunctionalModule("contactUsModule");
      }catch (err) {
        kony.print("Error loading functional module > contactUsModule");
        return false;
      }
      if (!isSignedUser) {
        gblDeeplinkExternalAccess = true;
        gblContentDeeplinkData = params;
      }else{
        gbl3dTouchAction = "";
        kony.print("DeepLink flow > tmbFeedback 1");
        gblDeepLinkFlow = true;
        if(gblDeeplinkExternalAccess) {
          frmContactUsMB.show();
        }
        formControl = frmContactUsMB;
        dismissLoadingScreen();
      }
    }else if (action == "tmbCreditCards") { // pass
      kony.print("DeepLink flow > tmbCreditCards");
      try {
        loadFunctionalModuleSync("cardMgmtModule");
      }catch (err) {
        kony.print("Error loading functional module > cardMgmtModule");
        return false;
      }
      if (!isSignedUser) {
        gblDeeplinkExternalAccess = true;
        gblContentDeeplinkData = params;
      }else{
		gblCurrentLocaleBeforeLoan = kony.i18n.getCurrentLocale();
        var cardAction = urlSchema["cardAction"];
        gbl3dTouchAction = "";
        kony.print("DeepLink flow > tmbCreditCards");
        gblDeepLinkFlow = true;
        openCreditCardDeeplink(cardAction);
        if(kony.application.getCurrentForm().id == "frmMBPreLoginAccessesPin"){
          //formControl = frmAccountSummaryLanding;
          //frmAccountSummaryLanding.show();
          formControl = frmAccountSummary;
          showAccountSummaryNewWithRefresh();
        }else{
          formControl = kony.application.getCurrentForm();
        }
      }
    }else if (action == "tmbApplyEStatement") { // pass
      kony.print("DeepLink flow > tmbApplyEStatement");
      if (!isSignedUser) {
        gblDeeplinkExternalAccess = true;
        gblContentDeeplinkData = params;
      }else{
        gbl3dTouchAction = "";
        kony.print("DeepLink flow > tmbCreditCards");
        gblDeepLinkFlow = true;
        kony.print("Before checking for apply estatement"+isShowEStatementMenu())
        if(isShowEStatementMenu()){
          kony.print("Eligible for estatement")
          menuMBestmtBousinessHrsonClick();
          formControl = kony.application.getCurrentForm();
        }else{
          kony.print("Not eligible for estatement")
          //formControl = frmAccountSummaryLanding;
          //frmAccountSummaryLanding.show();
          formControl = frmAccountSummary;
          showAccountSummaryNewWithRefresh();
        }
      }
    }
	  if(gblCurrentLocaleBeforeLoan != "" &&  formControl.id != "frmAccountSummaryLanding"){
          //gblCurrentLocaleBeforeLoan = kony.i18n.getCurrentLocale();
          	kony.i18n.setCurrentLocaleAsync("th_TH", function() {
            return formControl;
        }, function() {}, "");
        }else{
          return formControl;
        }
    //return formControl;
  }catch(e){
    kony.print("@@@ In deepLinkAccess() Exception:::"+e);
  }
}

function setupInsertIOParam(resulttable){
  	try{
  		var inputDataList = "";
        inputDataList+="FatcaFlag@@@"+resulttable["FatcaFlag"] + "__";
      	inputDataList+="Digital_Cust_Flag@@@"+resulttable["EKYCFlag"] + "__";
        
        var today = new Date();
        var dateSprit = gblCustomerDob.split("/");
        var birthDate = new Date(dateSprit[2],parseInt(dateSprit[1]) -1,dateSprit[0]);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        inputDataList+="attr001@@@"+age;
        
        for(var i = 0; i < resulttable["accountSummary"][0]["custAcctRec"].length; i++){
          	var customerAcctRec = resulttable["accountSummary"][0]["custAcctRec"][i];
          	var iProductID = customerAcctRec["productID"];
          	var iAccType = customerAcctRec["accType"];
          
          	if(inputDataList.indexOf("actType"+ iAccType) == -1)
        		inputDataList+="__actType"+ iAccType + "@@@Y";
          
          	if(inputDataList.indexOf("prdType"+ iProductID) == -1)
          		inputDataList+="__prdType"+ iProductID + "@@@Y";
        }
      
      //#ifdef android
      		insertIOAndroid.setVisitor(gblReferCD, "", inputDataList);
		//#endif

		//#ifdef iphone
			insertIOFunction.setParams(inputDataList);
          	insertIOFunction.setVisitorId(gblReferCD);
		//#endif
       	
    }catch(e){
    	kony.print("error data sending: "+e);
  	}
}

function isNullorEmpty(value){
  if(((value === null) || (value === "null") || (value === "") || (value === undefined))){
    return true;
  }else{
    return false;
  }
  return false;
}

/*frmPromotion*/

function frmPromotionMenuPreshow() {
    if (gblCallPrePost) {
        frmPromotion.scrollboxMain.scrollToEnd();
        DisableFadingEdges.call(this, frmPromotion);
    }
}

function frmPromotionMenuPostshow() {
    if (gblCallPrePost) {
        frmPromotion.scrollboxMain.scrollToEnd();
        gblIndex = -1;
        isMenuShown = false;
    }
    assignGlobalForMenuPostshow();
}

/*frmPromotionDetails*/

function frmPromotionDetailsMenuPreshow() {
    if (gblCallPrePost) {
        frmPromotionDetails.scrollboxMain.scrollToEnd();
        DisableFadingEdges.call(this, frmPromotionDetails);
    }
}

function frmPromotionDetailsMenuPostshow() {
    if (gblCallPrePost) {
        setPromotionsTimer.call(this);
    }
    assignGlobalForMenuPostshow();
}

function addDashAc(txt) {
	if(txt.length != 10)
	return txt;
	var i = txt.substr(0, 3);
	var j = txt.substr(3, 1);
	var k = txt.substr(4, 5);
	var l = txt.substr(9);
	var acc = i + "-" + j + "-" + k + "-" + l;
	var len = acc.length;
	if(acc.charAt(len-1) == '-'){
		return acc.substr(0, len-1);
	}
	else{
		return acc;
	}
}
