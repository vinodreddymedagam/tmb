function isAuthUsingTouchSupportedUsingKonyAPI() {
	kony.print("KONYTOUCHID : Inside isAuthUsingTouchSupported");
  try{
    var status = kony.localAuthentication.getStatusForAuthenticationMode(constants.LOCAL_AUTHENTICATION_MODE_TOUCH_ID);
	kony.print("isAuthUsingTouchSupported --- status: " + status);
	if (status == 5000) {
		kony.print("KONYTOUCHID :  status 5000");
		return true;
	} else {
		kony.print("KONYTOUCHID :  status not 5000");
		return false;
	}
  }catch(e){
    kony.print(" Exception "+e);
    return false;
  }
}

function authUsingTouchIDUsingKonyAPI()
{
   kony.print("KONYTOUCHID :  authUsingTouchID Call"); 
  var config = {"promptMessage" : kony.i18n.getLocalizedString("keyTouchCancel")};
	kony.localAuthentication.authenticate(
	constants.LOCAL_AUTHENTICATION_MODE_TOUCH_ID,authUsingTouchIDCallBack,config);
}

function authUsingTouchIDCallBack(status,message)
{
  
  kony.print("KONYTOUCHID :  authUsingTouchIDCallBack status : "+status);
  
	if(status == 5000)
	{
      kony.print("KONYTOUCHID: SUCCESS 1");
      //#ifdef android
      fpdefault();
      isTouchBeingUsedForLogin = true;
      kony.print("KONYTOUCHID: SUCCESS 2");
      loginVA();
      kony.print("KONYTOUCHID: SUCCESS 3");
      //stopListening();
      //#else
          //#ifdef iphone
      		kony.print("iPhone Kony Touch Id");
                 gblSuccess = true;
                showLoadingScreen();
                swipeLoginSuccessSkinChange();
                accsPwdValidatnLogin(gblNum);
           //#endif
       //#endif
           
	}

  else if(status == 5001)
      {
     	//For Android
         //#ifdef android
         kony.print("KONYTOUCHID :  write logic to display try again, status : "+status);
        
        kony.print("FPRINT No Match!!! Authentication Failed.");
        txt=kony.i18n.getLocalizedString("noMatch");
        kony.print("FPRINT txt="+txt);
        width="70";
        fpFailed(txt,width,true);
        
        //#endif
        
      }else if(status == 5002 || 5003){
        //do nothing : Cancel by user
      } 
  	else
	{
		var messg = status+": "+message;
        kony.print(messg);
		kony.ui.Alert({message: messg,
		alertType: constants.ALERT_TYPE_INFO,
		yesLabel:"Close"},
		{});
        
	}
  
   if(status == 5004)
      {
        kony.print("KONYTOUCHID: Status Too Many Attempts : "+ status);
     	//For Android
        //#ifdef android
         	tooManyAtmFlag = true;
         	kony.print("KONYTOUCHID :  Too Many Attempts, status : "+status);
        
					kony.print("FPRINT Sensor is stopped working due to TOO many attempts. Please try after some time.");
					
					var blockedSeconds = 30;
					var callback = function() {
						blockedSeconds--;
						var txt1=kony.i18n.getLocalizedString("tryAgain")+" " ;
						var txt2=" "+kony.i18n.getLocalizedString("seconds");
						txt=txt1 + blockedSeconds + txt2; //54
						kony.print("FPRINT txt="+txt);
						width="80";
						fpFailed(txt,width,false);
						if(blockedSeconds <= 0) {
							kony.timer.cancel("touch");
							tooManyAtmFlag=false;
							kony.store.setItem("TIMESTAMP1", -1);
							fpdefault(); //45
							kony.print("FPRINT: 1 DONE BLOCK SECONDS");
                            //MIB-14053: Use Kony API for Touch ID
							authUsingTouchIDUsingKonyAPI(); //authUsingTouchID();
						}
						 
					};
					
					var t1=kony.store.getItem("TIMESTAMP1");
					var blockTime=30; //30 secs
					var callTimer=false;
					if(t1==null){
						t1=-1;
						kony.store.setItem("TIMESTAMP1", t1);
					}

					if(t1!=-1){
						var currTime=new Date();
						kony.print("FPRINT TIMESTAMP2="+currTime);
						var t2=currTime.getTime();
						var diff=-1;
						diff = Math.floor((t2 - t1) / 1000);
						if(diff>0 && diff<blockTime){
							callTimer=true;
							blockedSeconds=blockTime-diff;
						}
					}

					if(tooManyAtmFlag || callTimer){
						
                      		var stmp1=new Date();
						  	kony.print("FPRINT TIMESTAMP1="+stmp1);
						  	kony.store.setItem("TIMESTAMP1", stmp1.getTime());
							kony.timer.schedule("touch", callback, 1, true);
					  }
					kony.print("FPRINT: 2 DONE BLOCK SECONDS");
                  	
       	 	//#endif
      }
}

function stopListeningUsingKonyAPI(){
  kony.print("KONYTOUCHID: cancelAuthentication Start");	
  
  kony.localAuthentication.cancelAuthentication();
  
  kony.print("KONYTOUCHID: cancelAuthentication End");	
}	


function onClickCheckTouchIdAvailableUsingKonyAPI() {
    gblTouchDevice = false;
    kony.print("KONYTOUCHID: onClickCheckTouchIdAvailable has called ");
  
    if(isAuthUsingTouchSupportedUsingKonyAPI()) {
          gblTouchDevice = true;
    }
       
	 kony.print("KONYTOUCHID: FP SUPPORT IS="+gblTouchDevice);
    return gblTouchDevice;
}

/**
 * @function
 *
 */
function showTouchIdPopupUsingKonyAPI() {
    kony.print("KONYTOUCHID: showTouchIdPopup Start");
    showFPFlex(false);
    var isActive = kony.store.getItem("isUserStatusActive");
    isUserStatusActive = isActive;
    if (gblDeviceInfo["name"] == "iPhone") {
        //alert("onClickCheckTouchIdAvailable--->"+onClickCheckTouchIdAvailable());

        var currentFormId = kony.application.getCurrentForm().id;
        onClickCheckTouchIdAvailableUsingKonyAPI();
        if (isAuthUsingTouchSupportedUsingKonyAPI() && isTouchIDEnabled() && (isActive === true || isActive == "true")) {
          	 if(gblDeviceInfo["model"]=="iPhone X"){
               frmMBPreLoginAccessesPin.btnTouchnBack.skin = "btnFaceId";
               frmMBPreLoginAccessesPin.btnTouchnBack.focusSkin = "btnFaceId";
             }else{
               frmMBPreLoginAccessesPin.btnTouchnBack.skin = "btntouchiconstudio5";
               frmMBPreLoginAccessesPin.btnTouchnBack.focusSkin = "btntouchiconfoc";
             }		     
             if (gblApplicationLaunch == true) {               
                if (gblPinCount == 0 && isMenuShown == false && currentFormId != "frmPreMenu" && gbl3dTouchAction != "quickbalance" && isActive === true) {
                    showLoadingScreen();
                  kony.print("gblTouchShow" + gblTouchShow + "gblAppUpdateAvlble" + gblAppUpdateAvlble);
                    if (gblTouchShow == true && gblAppUpdateAvlble == false) {
                         kony.print("calling fp auth");
                         authUsingTouchIDUsingKonyAPI();
                    } else {
                        gblTouchShow = true;
                        kony.application.dismissLoadingScreen();
                    }
                }
                //onClickTounchandBack()        
            } 
        } else {
            onClickChangeTouchIcon();
        }        
    } else if (gblDeviceInfo["name"] == "android") {
        kony.print("FPRINT: IN showTouchIdPopup ANDRIOD gblFromLogout=" + gblFromLogout);
        onClickCheckTouchIdAvailable();
        if (isAuthUsingTouchSupported() && isTouchIDEnabled()) {
            if (languageFlip) {
                kony.print("FPRINT DO NOTHING LANGUAGE FLIP");
                languageFlip = false;
            } else {
                if (gblFromLogout) {
                    if (gblSuccess == true || gblSuccess == "true") {
                        frmMBPreLoginAccessesPin.btnTouchnBack.skin = "btntouchiconstudio5";
                        frmMBPreLoginAccessesPin.btnTouchnBack.focusSkin = "btntouchiconfoc";
                        showFPFlex(true);
                        fpdefault();
                    } else {
                        frmMBPreLoginAccessesPin.btnTouchnBack.skin = "btntouchiconstudio5";
                        frmMBPreLoginAccessesPin.btnTouchnBack.focusSkin = "btntouchiconfoc";
                        showFPFlex(false);
                    }
                    gblFromLogout = false;
                    gotUserStatus = true;
                    isUserStatusActive = true;
                } else {
                    kony.print("FPRINT isMFsdkinit=" + isMFsdkinit);
                    kony.print("FPRINT isUserStatusActive=" + isUserStatusActive);
                    if (isUserStatusActive == true) { //After chkUserStatus(getDeviceID())
                        kony.print("FPRINT gotUserStatus true and isUserStatusActive true");
                        frmMBPreLoginAccessesPin.btnTouchnBack.skin = "btntouchiconstudio5";
                        frmMBPreLoginAccessesPin.btnTouchnBack.focusSkin = "btntouchiconfoc";
                        showFPFlex(true);
                        fpdefault();
                        //closeQuickBalance();
                    } else {
                        kony.print("FPRINT gotUserStatus true and isUserStatusActive false");
                        kony.print("FPRINT USER STATUS IS INACTIVE");
                        showFPFlex(false);
                        onClickChangeTouchIcon();
                    }
                }

            }
        } else {
            showFPFlex(false);
            onClickChangeTouchIcon();
        }
    }
    dismissLoadingScreen();
}


function onClickTounchandBackUsingKonyAPI() {
    kony.print("KONY TOUCH ID >>>");
    //if(gblDeviceInfo["name"] == "iPhone" && gblPinCount == 0 && gblTouchCount <= 2 && onClickCheckTouchIdAvailable()){
    var isActive = kony.store.getItem("isUserStatusActive");
    var returnedValue = "NO";
    if (gblDeviceInfo["name"] == "iPhone") {
        onClickCheckTouchIdAvailableUsingKonyAPI();
        //returnedValue = TouchId.isTouchIDAvailable("iPhone");
		//if ( gblPinCount == 0 && isAuthUsingTouchSupportedUsingKonyAPI() && isTouchIDEnabled() && (isActive === true || isActive == "true")) {
         if (frmMBPreLoginAccessesPin.btnTouchnBack.skin == "btntouchiconstudio5" || frmMBPreLoginAccessesPin.btnTouchnBack.skin == "btnFaceId") {
            showLoadingScreen();
            authUsingTouchIDUsingKonyAPI();
       } else {
        kony.print("FPRINT toggling flase2");
        //showFPFlex(false);
        onClickPinBack();
      }
    } else if (gblDeviceInfo["name"] == "android") {
        kony.print("FPRINT: on FP button skin:" + frmMBPreLoginAccessesPin.btnTouchnBack.skin);
        if (frmMBPreLoginAccessesPin.btnTouchnBack.skin == "btntouchiconstudio5") {
            //frmMBPreLoginAccessesPin.show();	
            kony.print("FPRINT toggling true");
            gblLoginTab = "FP";
            showFPFlex(true);
            fpdefault();
            fpFlag = true;
          //MIB-14053: Use Kony API for Touch ID.  Already called in showFPFlex
            /*if (!tooManyAtmFlag) {
                authUsingTouchIDUsingKonyAPI(); //authUsingTouchID();
            }*/
        } else {
            kony.print("FPRINT toggling false1");
            showFPFlex(false);
            onClickPinBack();
        }
    } /*else {
        kony.print("FPRINT toggling flase2");
        showFPFlex(false);
        onClickPinBack();
    } */
    dismissLoadingScreen();
}