function showfrmCheckList (){
   segDataSetInChkList();
  frmCheckList.show();
  
}

function initfrmCheckList(){
  
  //frmCheckList.preShow=preshowfrmCheckList;
  frmCheckList.postShow=posthowfrmCheckList;
  frmCheckList.onDeviceBack=onDeviceSpaback;
  
  frmCheckList.flxInfoPopup.onTouchEnd = onDeviceSpaback;
}
function posthowfrmCheckList(){
   kony.print("posthowfrmCheckList  in  ");
  if (gblCallPrePost) {
        //..
    }
    assignGlobalForMenuPostshow();
}

function preshowfrmCheckList(){
  //changeStatusBarColor();
  //if (gblCallPrePost) {
    try{
     
       kony.print("preshowfrmCheckList  in  ");
      frmCheckList.lblSubProductName.text = frmLoanProductDetails.lblProduceHeader.text;
      kony.print("Product name : "+frmLoanProductDetails.lblProduceHeader.text);
      if(frmLoanProductDetails.imgLoanProduct.src == "cashtogogif.gif"){
        frmCheckList.imgCheckList.src = "cashtogo.png";
      }else{
        frmCheckList.imgCheckList.src = frmLoanProductDetails.imgLoanProduct.src;
      }
      kony.print("Header : "+kony.i18n.getLocalizedString("LoanCheckList"));
      kony.print("work emp title : "+kony.i18n.getLocalizedString("Work_Employed_title"));
      kony.print("Employee_title : "+kony.i18n.getLocalizedString("Employee_title"));
      kony.print("Self_Employed : "+kony.i18n.getLocalizedString("Self_Employed"));
      kony.print("DocPrepare_title : "+kony.i18n.getLocalizedString("DocPrepare_title"));
      kony.print("Document_title : "+kony.i18n.getLocalizedString("Document_title"));
      
      frmCheckList.lblProductDetailsHeader.text = kony.i18n.getLocalizedString("LoanCheckList");
      frmCheckList.lblWorking.text = kony.i18n.getLocalizedString("Work_Employed_title");
      frmCheckList.btnEmployee.text = kony.i18n.getLocalizedString("Employee_title");
      frmCheckList.btnSelfEmployee.text = kony.i18n.getLocalizedString("Self_Employed");
      frmCheckList.lblPrepraseDocs.text = kony.i18n.getLocalizedString("DocPrepare_title");

      frmCheckList.lblInformation.text = kony.i18n.getLocalizedString("Document_title");

      frmCheckList.btnCancelCheck.text = kony.i18n.getLocalizedString("MF_RDM_33");
      frmCheckList.btnNextCheck.text = kony.i18n.getLocalizedString("MF_RDM_6");
      //frmCheckList.btnNextCheck.setEnabled(false);
      frmCheckList.btnNextCheck.onClick=onclickTncLoan;
//       frmCheckList.SegDataCheckList.onRowClick = imageCheckListOnclick;
      frmCheckList.btnshowInfo.onClick = showCheckListHelp;
      frmCheckList.btnBackProDetl.onClick = onClickCancelfrmCheckList;
	  frmCheckList.btnCancelCheck.onClick = onclickBackfrmCheckList;
    //frmCheckList.btnNextCheck.skin="btnGreyBGNoRoundLoan";
      //frmCheckList.imgCheckList.src=frmProductLoanDetails.imgProduct.src;
      frmCheckList.btnEmployee.onClick = onclickEmployee;
      frmCheckList.btnSelfEmployee.onClick = onclickSelfEmployee;
      frmCheckList.btnEmployee.onClick = onclickEmployee;
      frmCheckList.btnSelfEmployee.skin="btnTabWhiteRightRound";
      frmCheckList.btnEmployee.skin = "btnBlueTabBGNoRound";
      
      //segDataSetInChkList();
      onclickEmployee();
      
      var imgProductSrc = frmLoanProductDetails.imgLoanProduct.src;
      kony.print("imgProductSrc : "+imgProductSrc);
      var lblProdDesc = "";
      if (imgProductSrc === "sosmart.png") {
			lblProdDesc = kony.i18n.getLocalizedString("TMB_So_Smart_Description");
        } else if (imgProductSrc === "sofast.png") {
			lblProdDesc = kony.i18n.getLocalizedString("TMB_So_Fast_Description");
        } else if (imgProductSrc === "topbrass.png") {
			lblProdDesc = kony.i18n.getLocalizedString("TMB_Royal_Top_Brass_Description");
        } else if (imgProductSrc === "sochill.png") {
			lblProdDesc = kony.i18n.getLocalizedString("TMB_So_Chill_Description");
        }else if (imgProductSrc === "ready.png") {
			lblProdDesc = kony.i18n.getLocalizedString("TMB_Ready_Cash_Description");
        }else if (imgProductSrc === "cashtogogif.gif") {
			lblProdDesc = kony.i18n.getLocalizedString("TMB_Cash2Go_Description");
        }
      kony.print("lblProdDesc : "+lblProdDesc);
      frmCheckList.lblSubProdDesc.text = lblProdDesc;
      frmCheckList.btnNextCheck.setEnabled(true);
//       frmCheckList.btnNextCheck.onClick=onclickTncLoan;
      frmCheckList.btnNextCheck.skin="btnBlueBGNoRound150pxLoan";
    }catch(e){
      kony.print("Exception in preshow of Check List  "+e);
    }
  //}
}
function onclickBackfrmCheckList(){
  frmLoanProductList.show();
}

function onClickCancelfrmCheckList(){
  frmMBLoanKYC.show();
}

// function imageChecklist(){
//   var checkBxCount = 0;
//   var length=frmCheckList.SegDataCheckList.data.length;
//   var data = frmCheckList.SegDataCheckList.data;
// 	//alert("@@@ data "+data);
//   for(var i=0;i<length;i++){
    
//     if(data[i].ImageCheckList.skin === "skinpiblueLineLone"){
//       checkBxCount++;
//     }
//   }
  
//   if(checkBxCount === 0){
//     frmCheckList.btnNextCheck.setEnabled(true);
//     frmCheckList.btnNextCheck.onClick=onclickTncLoan;
//      frmCheckList.btnNextCheck.skin="btnBlueBGNoRound150pxLoan";
//   }else{
    
//     frmCheckList.btnNextCheck.setEnabled(false);
//      frmCheckList.btnNextCheck.onClick="";
//     frmCheckList.btnNextCheck.skin="btnGreyBGNoRoundLoan";
//   }
  
//   if(selVal.ImageCheckList=="iconcheckmargreenwhite.png" && selVal.ImageCheckList=="iconcheckmargreenwhite.png" && selVal.ImageCheckList=="iconcheckmargreenwhite.png" ){
//     frmCheckList.btnNextCheck.skin="btnBlueBGNoRound150pxLoan";
//     frmCheckList.btnNextCheck.onClick=onclickTncLoan;
//   }
  
// }

// function imageCheckListOnclick(){
  
//   selVal=frmCheckList.SegDataCheckList.selectedRowItems[0];
//   var selIndex = frmCheckList.SegDataCheckList.selectedRowIndex[1];
//   //alert("@@@ selIndex "+selIndex);
//   //alert("selVal --- "+JSON.stringify(selVal));
//   var val = "";
//   if(selVal.ImageCheckList.skin=="skinpiblueLineLone"){
//     val = {ImageCheckList : {skin:"skinpibluechkedLineLone"}, lblChecklistVal : selVal.lblChecklistVal,
//            lblLineSeparator :{ text : "  "}, lblLineSeparatorAndroid : { text : "  "}};    
//   }else if(selVal.ImageCheckList.skin=="skinpibluechkedLineLone"){
//     val = {ImageCheckList : {skin:"skinpiblueLineLone"}, lblChecklistVal : selVal.lblChecklistVal,
//           lblLineSeparator : { text : "  "}, lblLineSeparatorAndroid : { text : "  "}};
//   }
//   frmCheckList.SegDataCheckList.setDataAt(val, selIndex); 
//   imageChecklist();
// }

function showCheckListHelp(){
  	frmCheckList.btnPopUpCls.onClick = onclickbtnPopUpCls;
  	//frmCheckList.flxInfoPopup.setVisibility(true);
  	frmCheckList.flxInfoPopup.setVisibility(true);
  	//frmCheckList.flxInfoPopup.left = 0+"%";
  	
  	frmCheckList.flxInfoPopup.onTouchEnd = onDeviceSpaback;
  
  	//assignCheckListHelp();
  	callServicegetChkList();
}

function onclickbtnPopUpCls(){
  frmCheckList.flxInfoPopup.setVisibility(false);
  frmCheckList.FlexScrollContainer0bda564969b5044.setVisibility(true);
}

function onclickEmployee(){
  frmCheckList.btnEmployee.skin = "btnBlueTabBGNoRound";
  frmCheckList.btnEmployee.focusSkin="btnBlueTabBGNoRound";
  kony.print("Emp Skin : "+frmCheckList.btnEmployee.skin);
  frmCheckList.btnSelfEmployee.skin="btnTabWhiteRightRound";
  frmCheckList.btnSelfEmployee.focusSkin="btnTabWhiteRightRound";
  kony.print("SelfEmp Skin : "+frmCheckList.btnSelfEmployee.skin);
  frmCheckList.FlexContainerRoundCorners.skin="slFboxBrd0pxLoan";
  segDataSetInChkList();
}

function onclickSelfEmployee(){
 frmCheckList.btnEmployee.skin = "btnWhiteTabBGNoRound";
 frmCheckList.btnEmployee.focusSkin = "btnWhiteTabBGNoRound";
  kony.print("Emp Skin : "+frmCheckList.btnEmployee.skin);
 frmCheckList.btnSelfEmployee.skin = "btnTabBlueRightRound";
 frmCheckList.btnSelfEmployee.focusSkin = "btnTabBlueRightRound";
  kony.print("SelfEmp Skin : "+frmCheckList.btnSelfEmployee.skin);
 frmCheckList.FlexContainerRoundCorners.skin = "slFboxBrd0pxLoan";
 segDataSetInChkList();
}

function segDataSetInChkList(){
  kony.print("frmCheckList.btnEmployee.skin   "+frmCheckList.btnEmployee.skin);
  kony.print("Setting check list data to segment");  
  kony.print("2nd lable : "+kony.i18n.getLocalizedString("CL_Lastest_payslip"));
  kony.print("3rd lable : "+kony.i18n.getLocalizedString("CL_BookBank"));
//   frmCheckList.SegDataCheckList.widgetDataMap = {ImageCheckList:"ImageCheckList",lblChecklistVal:"lblChecklistVal"};
    if(frmCheckList.btnEmployee.skin == "btnBlueTabBGNoRound"){
//       frmCheckList.SegDataCheckList.data = [
// 					{ImageCheckList: {skin: "skinpibluechkedLineLone"},lblChecklistVal:kony.i18n.getLocalizedString("CL_CopyofIDCard"),template:flxCheckListTemp}, 
// 					{ImageCheckList: {skin: "skinpibluechkedLineLone"},lblChecklistVal:kony.i18n.getLocalizedString("CL_Lastest_payslip"),template:flxCheckListTemp},  
// 					{ImageCheckList: {skin: "skinpibluechkedLineLone"},lblChecklistVal:kony.i18n.getLocalizedString("CL_BookBank"),template:flxCheckListTemp}
// 				 ];
      frmCheckList.lblChecklistVal.text = kony.i18n.getLocalizedString("CL_CopyofIDCard");
      frmCheckList.lblChecklistVal2.text = kony.i18n.getLocalizedString("CL_Lastest_payslip");
      frmCheckList.lblChecklistVal3.text = kony.i18n.getLocalizedString("CL_BookBank");
      frmCheckList.flxCheckListTemp3.height = "60dp";
    }else{
//    frmCheckList.SegDataCheckList.data = [
// 					{ImageCheckList: {skin: "skinpibluechkedLineLone"},lblChecklistVal:kony.i18n.getLocalizedString("CL_CopyofIDCard"),template:flxCheckListTemp}, 
// 					{ImageCheckList: {skin: "skinpibluechkedLineLone"},lblChecklistVal:kony.i18n.getLocalizedString("CL_CopyofCertificate"),template:flxCheckListTemp},  
// 					{ImageCheckList: {skin: "skinpibluechkedLineLone"},lblChecklistVal:kony.i18n.getLocalizedString("CL_BookBankForBusiness"),template:flxCheckListTemp}
// 				 ];
      frmCheckList.lblChecklistVal.text = kony.i18n.getLocalizedString("CL_CopyofIDCard");
      frmCheckList.lblChecklistVal2.text = kony.i18n.getLocalizedString("CL_CopyofCertificate");
      frmCheckList.lblChecklistVal3.text = kony.i18n.getLocalizedString("CL_BookBankForBusiness");
	  
	  var locale = kony.i18n.getCurrentLocale();
		if (locale == "th_TH") {
			frmCheckList.flxCheckListTemp3.height = "80dp";
		} else {
			frmCheckList.flxCheckListTemp3.height = "60dp";
		}      
    }
	
//   frmCheckList.btnNextCheck.setEnabled(false);
     //frmCheckList.btnNextCheck.onClick="";
//     frmCheckList.btnNextCheck.skin="btnGreyBGNoRoundLoan";
}

function callServicegetChkList() {
    var input_param = {};
    var locale = kony.i18n.getCurrentLocale();
    if (locale == "en_US") {
        input_param.localeCd = "en_US";
    } else {
        input_param.localeCd = "th_TH";
    }
    input_param.moduleKey = "ReadLetterofContent";
  	var contentType = "EMP";
  	if(frmCheckList.btnEmployee.skin  !== "btnBlueTabBGNoRound"){
      contentType = "SELF_EMP";
    }
  	input_param["empType"] = contentType;
  	frmCheckList.richTxtCheckListHelp.text = "";
    showLoadingScreen();
    invokeServiceSecureAsync("readUTFFile", input_param, callbackshowCheckListHelp);
}

function callbackshowCheckListHelp(status, result){
  if (status == 400) {
    if (result.opstatus === 0) {
		kony.print("Content : "+result["fileContent"]);
      frmCheckList.richTxtCheckListHelp.text = result["fileContent"];
      dismissLoadingScreen();
    } else {
      dismissLoadingScreen();
      showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
      return false;
    }
    frmCheckList.flxInfoPopup.setVisibility(true);
  }

}

function assignCheckListHelp(){
  //var locale = kony.i18n.getCurrentLocale();
  var totalCheckList = "";
  kony.print("skin : "+frmCheckList.btnEmployee.skin);
  kony.print("emply CheckListHelp : "+kony.i18n.getLocalizedString("CL_EmpCheckListHelp"));
  if(frmCheckList.btnEmployee.skin  === "btnBlueTabBGNoRound"){
    totalCheckList = kony.i18n.getLocalizedString("CL_EmpCheckListHelp");
  }else{
    totalCheckList = kony.i18n.getLocalizedString("CL_SelfEmpChkListHelp"); 
  }

  kony.print("totalCheckList : "+totalCheckList);
  frmCheckList.richTxtCheckListHelp.text = totalCheckList;

  frmCheckList.flxInfoPopup.setVisibility(true);
}
