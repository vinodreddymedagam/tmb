//Type your code here
gblCardlesswithdrawEndTime = 300;
gblSelectActiveCode = 0;
gblActiveCodes = 0;
gblUsedCodes = 0;
gblExpiredCodes = 0;
gblNoCodesToShow = false;
gblNextIndex = 0;
glbCLW = "";
gblRemainingSeconds = 10800;
gblUniqId = 0;
gblCardlessWithDrawFromAcct = "";
gblUVCustomerForTesting = true;
gblCLWActiveCodes = [];
gblCLWUsedCodes = [];
gblCLWExpiredCodes = [];

function assignePageSkintoTutorialSeg() {
    frmTutorialForCardlessWithdraw.segCardlesswithdrawActivation.pageSkin = noSkinSegmentRow;
}

function setActiveCodesToSegment(result) {
    var segData = [];
    var rowData = {};
    frmCardlesswithdrawLanding.segActiveCodes.widgetDataMap = {
        lblCode: "lblCode",
        lblAccountName: "lblAccountName",
        lblAccountNo: "lblAccountNo",
        lblRequestedTime: "lblRequestedTime",
        lblRemainingTime: "lblRemainingTime",
        lblRemainingSeconds: "lblRemainingSeconds",
        lblStartTime: "lblStartTime",
        lblEndTime: "lblEndTime",
        imgTimer: "imgTimer",
        lblTransactionId: "lblTransactionId",
        lblExpireDate: "lblExpireDate"
    };
    for (var i = 0; i < result.length; i++) {
		//var amount = parseInt(result[i]["AMOUNT"]);
		//amount  = commaFormattedTransfer(amount+"") + ".00 " + kony.i18n.getLocalizedString("currencyThaiBaht");
        rowData = {
            lblCode: result[i]["CODE"],
            lblAccountName: result[i]["ACCOUNT_NAME"] + " : " + commaFormattedTransfer(result[i]["AMOUNT"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
            lblAccountNo: result[i]["ACCOUNT_NO"],
            lblRequestedTime: result[i]["CREATED_DATE"],
            lblRemainingTime: secondsToHmsinSegment(result[i]["EXPIRE_TIME"]),
            lblRemainingSeconds: result[i]["EXPIRE_TIME"],
            lblStartTime: result[i]["START_TIME"],
            lblEndTime: result[i]["END_TIME"],
            imgTimer: "timerunning.png",
            lblTransactionId: result[i]["TRANSACTION_ID"],
            lblExpireDate: result[i]["EXPIRE_DATE"],
            lblAccountNameOnly: result[i]["ACCOUNT_NAME"],
            lblAmountOnly: commaFormattedTransfer(result[i]["AMOUNT"])+ " " + kony.i18n.getLocalizedString("currencyThaiBaht")
        };
        segData.push(rowData);
    }
    frmCardlesswithdrawLanding.segActiveCodes.setData(segData);
}

function setUsedCodesToSegment(result) {
    var segData = [];
    var rowData = {};
    /*  frmCardlesswithdrawLanding.segUsedCodes.widgetDataMap ={
					lblCode: "lblCode",
			       	lblAccountName:"lblAccountName",
					lblAccountNo:"lblAccountNo",
					lblRequestedTime: "lblRequestedTime",
					lblRemainingTime:"lblRemainingTime",
					lblRemainingSeconds:"lblRemainingSeconds",
					lblStartTime:"lblStartTime",
					lblEndTime:"lblEndTime",
					imgTimer:"imgTimer"
   }*/
    for (var i = 0; i < result.length; i++) {
        rowData = {
            lblCode: result[i]["CODE"],
            lblAccountName: result[i]["ACCOUNT_NAME"] + " : " + commaFormattedTransfer(result[i]["AMOUNT"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
            lblAccountNo: result[i]["ACCOUNT_NO"],
            lblRequestedTime: result[i]["CREATED_DATE"],
            //lblRemainingTime: secondsToHmsinSegment(result[i]["EXPIRE_TIME"]),
            lblRemainingTime: "0",
            lblRemainingSeconds: result[i]["EXPIRE_TIME"],
            lblStartTime: result[i]["START_TIME"],
            lblEndTime: result[i]["END_TIME"],
            imgTimer: "timeup.png"
        };
        segData.push(rowData);
    }
    frmCardlesswithdrawLanding.segUsedCodes.setData(segData);
}

function setExpiredCodesToSegment(result) {
    var segData = [];
    var rowData = {};
    frmCardlesswithdrawLanding.segExpiredCodes.widgetDataMap = {
        lblCode: "lblCode",
        lblAccountName: "lblAccountName",
        lblAccountNo: "lblAccountNo",
        lblRequestedTime: "lblRequestedTime",
        lblRemainingTime: "lblRemainingTime",
        lblRemainingSeconds: "lblRemainingSeconds",
        lblStartTime: "lblStartTime",
        lblEndTime: "lblEndTime",
        imgTimer: "imgTimer"
    }
    for (var i = 0; i < result.length; i++) {
        rowData = {
            lblCode: result[i]["CODE"],
            lblAccountName: result[i]["ACCOUNT_NAME"] + " : " + commaFormattedTransfer(result[i]["AMOUNT"]) + "" + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
            lblAccountNo: result[i]["ACCOUNT_NO"],
            lblRequestedTime: result[i]["CREATED_DATE"],
            //lblRemainingTime: secondsToHmsinSegment(result[i]["EXPIRE_TIME"]),
            lblRemainingTime: "0",
            lblRemainingSeconds: result[i]["EXPIRE_TIME"],
            lblStartTime: result[i]["START_TIME"],
            lblEndTime: result[i]["END_TIME"],
            imgTimer: "timeup.png"
        };
        segData.push(rowData);
    }
    frmCardlesswithdrawLanding.segExpiredCodes.setData(segData);
}

function onClickBackfrmCardlesswithdrawLanding() {}

function showcloseCardlessRequestInfoPopup() {
    var frmName = kony.application.getCurrentForm();
    if (frmName.id != "frmTutorialForCardlessWithdraw") {
        gblNextIndex = 0;
        var gblDeviceInfomenu = kony.os.deviceInfo();
        frmTutorialForCardlessWithdraw.segCardlesswithdrawActivation.selectedRowIndex = [0, 0];
        // 	var screenwidth = gblDeviceInfomenu["deviceWidth"];
        //     if(screenwidth<750)
        //     {
        //     	frmMenu.flexNormal.setVisibility(false);
        //     	frmMenu.flexSmall.setVisibility(true);
        //     }
        //     else
        //     {
        //     	frmMenu.flexNormal.setVisibility(true);
        //     	frmMenu.flexSmall.setVisibility(false);
        //     }
        var locale = kony.i18n.getCurrentLocale();
        if (locale == "en_US") {} else {}
        frmTutorialForCardlessWithdraw.btnActivate.isVisible = false;
        frmTutorialForCardlessWithdraw.btnNext.isVisible = true;
        frmTutorialForCardlessWithdraw.flexFooterLine.isVisible = true;
        frmTutorialForCardlessWithdraw.show();
    } else {
        kony.application.getPreviousForm().show();
    }
}

function frmTutorialForCardlessWithdrawInit() {
    frmTutorialForCardlessWithdraw.segCardlesswithdrawActivation.pageSkin = noSkinSegmentRow;
    frmTutorialForCardlessWithdraw.flexCrossmark.onClick = showcloseCardlessRequestInfoPopup;
    frmTutorialForCardlessWithdraw.btnActivate.onClick = showcloseCardlessRequestInfoPopup;
    frmTutorialForCardlessWithdraw.btnNext.onClick = onClickCardlessWithdrawNext;
    frmTutorialForCardlessWithdraw.onDeviceBack = doNothing;
}

function getTutorialForCardlessWithdrawActivation() {}
getTutorialForCardlessWithdrawActivation.prototype.paginationSwitch = function(sectionIndex, rowIndex) {
    try {
        rowIndex = parseFloat(rowIndex);
        frmCardlesswithdrawActivation.segCardlesswithdrawActivation.selectedIndex = [0, rowIndex];
    } catch (e) {}
    gblNextIndex = rowIndex;
    if (rowIndex == 2) {
        // frmCardlesswithdrawActivation.segCardlesswithdrawActivation.pageOffDotImage = "";
        //      frmCardlesswithdrawActivation.segCardlesswithdrawActivation.pageOnDotImage = "";
        frmCardlesswithdrawActivation.btnActivate.isVisible = true;
        frmCardlesswithdrawActivation.btnNext.isVisible = false;
        frmCardlesswithdrawActivation.flexFooterLine.isVisible = false;
    } else {
        frmCardlesswithdrawActivation.btnActivate.isVisible = false;
        frmCardlesswithdrawActivation.btnNext.isVisible = true;
        frmCardlesswithdrawActivation.flexFooterLine.isVisible = true;
    }
}

function getTutorialForCardlessWithdraw() {}
getTutorialForCardlessWithdraw.prototype.paginationSwitch = function(sectionIndex, rowIndex) {
    try {
        rowIndex = parseFloat(rowIndex);
        frmTutorialForCardlessWithdraw.segCardlesswithdrawActivation.selectedIndex = [0, rowIndex];
    } catch (e) {}
    gblNextIndex = rowIndex;
    if (rowIndex == 2) {
        // frmCardlesswithdrawActivation.segCardlesswithdrawActivation.pageOffDotImage = "";
        //      frmCardlesswithdrawActivation.segCardlesswithdrawActivation.pageOnDotImage = "";
        frmTutorialForCardlessWithdraw.btnActivate.isVisible = true;
        frmTutorialForCardlessWithdraw.btnNext.isVisible = false;
        frmTutorialForCardlessWithdraw.flexFooterLine.isVisible = false;
    } else {
        frmTutorialForCardlessWithdraw.btnActivate.isVisible = false;
        frmTutorialForCardlessWithdraw.btnNext.isVisible = true;
        frmTutorialForCardlessWithdraw.flexFooterLine.isVisible = true;
    }
}

function frmTutorialForCardlessWithdrawPreshow() {
    gblNextIndex = 0;
    assignePageSkintoTutorialSeg();
    setDataToCardlessActivationTutorial(frmTutorialForCardlessWithdraw);
}


function onClickbtnWithdrawMoney() {
    loadFromAccountForCardlessWD();
    frmCardlessWithdrawSelectAccount.txtAmountVal.text = "";
    //setDataFromAccountsCardLessWithdraw();
    //frmCardlesswithdrawConfirmation.show();
}


function onClickActiveCode() {
    frmCardlesswithdrawComplete.btnDone.isVisible = false;
    frmCardlesswithdrawComplete.flexFooterLine.isVisible = true;
    frmCardlesswithdrawComplete.flexFooter.top = "-4%";
    frmCardlesswithdrawComplete.lblAccessCode.text = frmCardlesswithdrawLanding.segActiveCodes.selectedRowItems[0]["lblCode"];
    gblSelectActiveCode = frmCardlesswithdrawLanding.segActiveCodes.selectedRowItems[0];
    gblRemainingSeconds = frmCardlesswithdrawLanding.segActiveCodes.selectedRowItems[0]["lblRemainingSeconds"];
    frmCardlesswithdrawComplete.lblTimer.text = secondsToHms(gblRemainingSeconds);
    if (gblUniqId != 0 && gblUniqId != undefined) {
        kony.timer.cancel(gblUniqId)
    }
    var uniqueIdGen = Math.random().toString(36).substr(2, 9) + "";
    gblUniqId = uniqueIdGen;
    if (gblRemainingSeconds != 0) {
        var timer = kony.timer.schedule(uniqueIdGen, setTimerToActiveCode, 1, true);
    }
    frmCardlesswithdrawComplete.lblAccountName.text = frmCardlesswithdrawLanding.segActiveCodes.selectedRowItems[0]["lblAccountNameOnly"];
    frmCardlesswithdrawComplete.lblAmount.text = frmCardlesswithdrawLanding.segActiveCodes.selectedRowItems[0]["lblAmountOnly"];
    frmCardlesswithdrawComplete.lblAccountNumber.text = formatAccountNumberOpenAct(frmCardlesswithdrawLanding.segActiveCodes.selectedRowItems[0]["lblAccountNo"]);
    frmCardlesswithdrawComplete.lblStartTime.text = frmCardlesswithdrawLanding.segActiveCodes.selectedRowItems[0]["lblStartTime"];
    frmCardlesswithdrawComplete.lblEndTime.text = frmCardlesswithdrawLanding.segActiveCodes.selectedRowItems[0]["lblEndTime"];
    frmCardlesswithdrawComplete.lblTransactionId.text = frmCardlesswithdrawLanding.segActiveCodes.selectedRowItems[0]["lblTransactionId"];
    frmCardlesswithdrawComplete.lblExpireFulldate.text = frmCardlesswithdrawLanding.segActiveCodes.selectedRowItems[0]["lblExpireDate"];
    frmCardlesswithdrawComplete.btnBack.isVisible = true;
    frmCardlesswithdrawComplete.show();
}

function frmCardlesswithdrawLandingInit() {
    frmCardlesswithdrawLanding.btnBack.onClick = showAccountSummaryFromMenu;
    //frmCardlesswithdrawLanding.btnBack.onClick =   onClickCardlessWithdrawBack;
    frmCardlesswithdrawLanding.segActiveCodes.onRowClick = onClickActiveCode;
    frmCardlesswithdrawLanding.btnInfo.onClick = showcloseCardlessRequestInfoPopup;
    frmCardlesswithdrawLanding.btnWithdrawMoney.onClick = onClickbtnWithdrawMoney;
    frmCardlesswithdrawLanding.onDeviceBack = doNothing;
}


function setAllCodesDataToSegment() {
    uvTransactionHistoryCallBack(gblResultCLW);
    /* () if(gblActiveCodes != null && gblActiveCodes.length >0 ){
        setActiveCodesToSegment();
        gblNoActiveCodes = false;
       
     }
      if(gblUsedCodes != null && gblUsedCodes.length >0 ){
        setUsedCodesToSegment();
        gblNoActiveCodes = false;
       
     }
     
      if(gblExpiredCodes != null && gblExpiredCodes.length >0 ){
        setExpiredCodesToSegment();
        gblNoActiveCodes = false;
       
     }
     if(gblNoCodesToShow){
       frmCardlesswithdrawLanding.flexBody.isVisible = false;
       frmCardlesswithdrawLanding.flexNoCodeNote.isVisible = true;
     }*/
}

function frmCardlesswithdrawLandingPreshow() {
    frmCardlesswithdrawLanding.lblHdrTxt.text =kony.i18n.getLocalizedString("MB_CWTitle");
    frmCardlesswithdrawLanding.Label0afbfbdf5f7ff4c.text =kony.i18n.getLocalizedString("MB_CWText1");
  	frmCardlesswithdrawLanding.lblActiveCodeError.text =kony.i18n.getLocalizedString("MB_CWCannotRequestCode");
    frmCardlesswithdrawLanding.lblActiveActiveWithdrawHeader.text =kony.i18n.getLocalizedString("MB_CW_ActiveWRTitle");
  	frmCardlesswithdrawLanding.lblUsedWithdrawHeader.text =kony.i18n.getLocalizedString("MB_CW_UsedWRTitle");
    frmCardlesswithdrawLanding.lblExpiredCOdesHeader.text =kony.i18n.getLocalizedString("MB_CW_ExpWRTitle");
    frmCardlesswithdrawLanding.lblNoCodes.text = kony.i18n.getLocalizedString("MB_CW_NoAct");
    frmCardlesswithdrawLanding.btnWithdrawMoney.text = kony.i18n.getLocalizedString("MB_RequestBtn");
     //#ifdef android    
    // The below code snippet is to fix the whilte label on some screens
       frmCardlesswithdrawLanding.FlexScrollContainer0ea78177032bd44.showFadingEdges  = false;
                //#endif  
    frmCardlesswithdrawLanding.btnInfo.right = "13dp";
	frmCardlesswithdrawLanding.btnInfo.height = "60%";
	frmCardlesswithdrawLanding.btnInfo.centerY = "52%";
	frmCardlesswithdrawLanding.btnInfo.width = "8%";
    gblNextIndex = 0;
    // setAllCodesDataToSegment();
}

function frmCardlesswithdrawLandingPostshow() {
    //frmCardlesswithdrawComplete.destroy(); 
    //frmCardlesswithdrawConfirmation.destroy();
    // frmCardlesswithdrawTermincateCode.show();
}


function onClickCardlessWithdrawNext() {
    //setDataFromAccountsCardLessWithdraw();
    var currForm = kony.application.getCurrentForm();
    gblNextIndex += 1;
    currForm.segCardlesswithdrawActivation.selectedRowIndex = [0, gblNextIndex];
    // if(currForm.segCardlesswithdrawActivation.selectedItems !== null){
    //var imageSrc = currForm.segCardlesswithdrawActivation.selectedItems[0]['imageTutorial'];
    //}
    // if(imageSrc == "clwadone.png"){
    if (gblNextIndex == 2) {
        // frmCardlesswithdrawActivation.segCardlesswithdrawActivation.pageOffDotImage = "";
        //      frmCardlesswithdrawActivation.segCardlesswithdrawActivation.pageOnDotImage = "";
        currForm.btnActivate.isVisible = true;
        currForm.btnNext.isVisible = false;
        currForm.flexFooterLine.isVisible = false;
    } else {
        currForm.btnActivate.isVisible = false;
        currForm.btnNext.isVisible = true;
        currForm.flexFooterLine.isVisible = true;
    }
}
/* function onClickCardlessWithdrawBack() {
    //setDataFromAccountsCardLessWithdraw();
    var currForm = kony.application.getCurrentForm();
    gblNextIndex -= 1;
    if (gblNextIndex <= 1) {
        showAccountSummaryFromMenu();
    } else {
        currForm.segCardlesswithdrawActivation.selectedRowIndex = [0, gblNextIndex];
        // if(currForm.segCardlesswithdrawActivation.selectedItems !== null){
        //var imageSrc = currForm.segCardlesswithdrawActivation.selectedItems[0]['imageTutorial'];
        //}
        // if(imageSrc == "clwadone.png"){
        if (gblNextIndex != 3) {
            // frmCardlesswithdrawActivation.segCardlesswithdrawActivation.pageOffDotImage = "";
            //      frmCardlesswithdrawActivation.segCardlesswithdrawActivation.pageOnDotImage = "";
            currForm.btnActivate.isVisible = false;
            currForm.btnNext.isVisible = true;
            currForm.flexFooterLine.isVisible = true;
        } else {
            currForm.btnActivate.isVisible = true;
            currForm.btnNext.isVisible = false;
            currForm.flexFooterLine.isVisible = false;
        }
    }
}*/
function onClickCardlessWithdrawBack() {
    showAccountSummaryFromMenu()
        //onClickCardlessWithdrawBack();
}


function showCLActivateButton() {
    //var modScroll = new getTutorialForCardlessWithdraw();
    //modScroll.paginationSwitch( sectionIndex, rowIndex );
}

function showCardLessWithDrawLanding() {
    var currForm = kony.application.getCurrentForm();
    if (currForm.id == "frmCardlessWithdrawSelectAccount") {
        gblCardlessFromSelAcctId = "";
        glb_accId = "";
    }
    gblNextIndex = 0;
    uvTransactionHistory()
        //frmCardlesswithdrawLanding.show();
}

function activationProvisioning() {
    if (gblUVregisterMB == "N") {
        gblActivationCurrentForm = "cardlessWithdraw";
        if (gblActivationWithWifi != true) {
            frmActivationAttention.show();
        } else {
            gblRetryCountRequestOTP = "0";
            mobileNumberFromUserDevice = "";
            onClickUVAttentionNext();
        }
    } else {
        showCardLessWithDrawLanding();
    }
}

function frmCardlesswithdrawActivationInit() {
    frmCardlesswithdrawActivation.btnNext.onClick = onClickCardlessWithdrawNext;
    frmCardlesswithdrawActivation.btnBack.onClick = onClickCardlessWithdrawBack;
    frmCardlesswithdrawActivation.btnActivate.onClick = activationProvisioning;
    // frmCardlesswithdrawActivation.segCardlesswithdrawActivation.onSwipe = showCLActivateButton;
    frmCardlesswithdrawActivation.onDeviceBack = doNothing;
}

function frmCardlesswithdrawActivationPreshow() {
    frmCardlesswithdrawActivation.segCardlesswithdrawActivation.pageSkin = noSkinSegmentRow;
    frmCardlesswithdrawActivation.lblHdrTxt.text = kony.i18n.getLocalizedString("MB_CWTitle")
    setDataToCardlessActivationTutorial(frmCardlesswithdrawActivation);
    frmCardlesswithdrawActivation.segCardlesswithdrawActivation.selectedIndex = [0, 0];
    gblNextIndex = 0;
}

function setDataToCardlessActivationTutorial(frmName){
  var data = [{
            "CopyLabel0a6ae87640b1b46": kony.i18n.getLocalizedString("key_CLWR_1"),
            "Label0e0c8fd24d22945": kony.i18n.getLocalizedString("key_CLWR_Header1"),
            "imageTutorial": "clwa1.png",
            "richTextActication": kony.i18n.getLocalizedString("key_CLWR_1")
        }, {
            "CopyLabel0a6ae87640b1b46": kony.i18n.getLocalizedString("key_CLWR_2"),
            "Label0e0c8fd24d22945": kony.i18n.getLocalizedString("key_CLWR_Header2"),
            "imageTutorial": "clwa2.png",
            "richTextActication": kony.i18n.getLocalizedString("key_CLWR_2")
        }, {
            "CopyLabel0a6ae87640b1b46": kony.i18n.getLocalizedString("key_CLWR_4"),
            "Label0e0c8fd24d22945": kony.i18n.getLocalizedString("key_CLWR_Header4"),
            "imageTutorial": "clwa4.png",
            "richTextActication": kony.i18n.getLocalizedString("key_CLWR_4")
        }]
  frmName.segCardlesswithdrawActivation.removeAll();
  frmName.segCardlesswithdrawActivation.setData(data);
  
}
function destroyCardlessWithDrawForms() {
    frmCardlesswithdrawActivation.destroy();
    frmCardlesswithdrawComplete.destroy();
    frmCardlesswithdrawConfirmation.destroy();
    frmCardlesswithdrawLanding.destroy();
    frmCardlesswithdrawTermincateCode.destroy();
    frmCardlessWithdrawSelectAccount.destroy();
    frmTutorialForCardlessWithdraw.destroy();
}

function frmCardlesswithdrawActivationPostshow() {
    /* frmCardlesswithdrawComplete.destroy(); 
   frmCardlesswithdrawConfirmation.destroy();
   frmCardlesswithdrawLanding.destroy();
   frmCardlesswithdrawTermincateCode.destroy();
  frmCardlessWithdrawSelectAccount.destroy(); */
}

function frmCardlesswithdrawTermincateCodePreshow() {}

function frmCardlesswithdrawTermincateCodePostshow() {}

function frmCardlesswithdrawTermincateCodeInit() {
    frmCardlesswithdrawTermincateCode.btnGotoAcctSummary.onClick = showAccountSummaryFromMenu;
    frmCardlesswithdrawTermincateCode.btnRequestNewPin.onClick = showCardLessWithDrawLanding;
    frmCardlesswithdrawTermincateCode.onDeviceBack = doNothing;
}

function showPreviousFormInCLW() {
    frmCardlessWithdrawSelectAccount.segNSSlider.selectedIndex = gblCardlessFromSelIndex;
    gblCardlessFromSelIndex = frmCardlessWithdrawSelectAccount.segNSSlider.selectedIndex;
    frmCardlessWithdrawSelectAccount.txtAmountVal.text = frmCardlesswithdrawConfirmation.lblAmount.text.replace(".00"+kony.i18n.getLocalizedString("currencyThaiBaht"), "") + "";
    frmCardlessWithdrawSelectAccount.show();
    showFocusTOAmountCLW();
}

function confirmCardlessWithdraw() {
    frmCardlesswithdrawComplete.btnDone.isVisible = true;
    frmCardlesswithdrawComplete.show();
}

function frmCardlesswithdrawConfirmationInit() {
    frmCardlesswithdrawConfirmation.preShow = frmCardlesswithdrawConfirmationPreshow;
    frmCardlesswithdrawConfirmation.postShow = frmCardlesswithdrawConfirmationPostshow;
    frmCardlesswithdrawConfirmation.btnConfirm.onClick = showTxnPasswordPopupRequestAccessCodeConfimation; //UVAccessCodeInfo;
    frmCardlesswithdrawConfirmation.btnCancel.onClick = navigatetoTheSource;
    frmCardlesswithdrawConfirmation.btnBack.onClick = showPreviousFormInCLW;
    frmCardlesswithdrawConfirmation.onDeviceBack = doNothing;
    frmCardlesswithdrawConfirmation.btnInfo.onClick = showcloseCardlessRequestInfoPopup;
    frmCardlesswithdrawConfirmation.btnInfo.right = "13dp";
	frmCardlesswithdrawConfirmation.btnInfo.height = "60%";
	frmCardlesswithdrawConfirmation.btnInfo.centerY = "52%";
	frmCardlesswithdrawConfirmation.btnInfo.width = "8%";
    //  frmCardlesswithdrawConfirmation.flexCrossmark.onClick = showcloseCardlessRequestInfoPopup;
}

function navigatetoTheSource() {
    var prevForm = gblCardlessFlowCurrentForm;
    if (prevForm.id == "frmAccountSummaryLanding" || prevForm.id == "frmCardlessWithdrawSelectAccount") {
        showAccountSummaryFromMenu();
    } else {
        prevForm.show();
    }
}

function frmCardlesswithdrawCompletePreshow() {
    frmCardlesswithdrawComplete.richtextTutorial.text = "<label style='color:#979797'>" + kony.i18n.getLocalizedString("MB_CWCodeIns") + "</label><label style='color:#007ABC'>" + kony.i18n.getLocalizedString("MB_CWIntroLink") + "</label>";
    frmCardlesswithdrawComplete.lblEnterPinNote.text = kony.i18n.getLocalizedString("MB_CWTimerIns");
    frmCardlesswithdrawComplete.lblValidFromKey.text = kony.i18n.getLocalizedString("MB_CWCodeValidFrom");
    frmCardlesswithdrawComplete.lblValiduntilKey.text = kony.i18n.getLocalizedString("MB_CWCodeValidTo");
    frmCardlesswithdrawComplete.lblFindTMB.text = kony.i18n.getLocalizedString("MenuFindTMB");
    frmCardlesswithdrawComplete.lblHdrTxt.text = kony.i18n.getLocalizedString("MB_CWFinishTitle");
    frmCardlesswithdrawComplete.btnTernmicateCode.text = kony.i18n.getLocalizedString("MB_CWTerminateTitle");
    frmCardlesswithdrawComplete.btnDone.text = kony.i18n.getLocalizedString("MB_CWFinishBtn");
	frmCardlesswithdrawComplete.CopylblAccountName0fef10c0159944c.text = kony.i18n.getLocalizedString("MB_CWAmt");
}

function secondsToHms(d) {
    d = Number(d);
    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);
    return ('0' + h).slice(-2) + ":" + ('0' + m).slice(-2) + ":" + ('0' + s).slice(-2);
}

function secondsToHmsinSegment(d) {
    d = Number(d);
    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);
    //return ('0' + h).slice(-2) + kony.i18n.getLocalizedString("HH") + ". " + ('0' + m).slice(-2) + kony.i18n.getLocalizedString("MM");
    return ('0' + m).slice(-2) + kony.i18n.getLocalizedString("MM");
}


function frmCardlesswithdrawCompletePostshow() {
    var locale = kony.i18n.getCurrentLocale();
    if (locale == "en_US") {
        frmCardlesswithdrawComplete.imgLocation.zIndex = "32%"
    } else {
        frmCardlesswithdrawComplete.imgLocation.zIndex = "24%"
    }
}

function setTimerToActiveCode() {
    if (parseInt(gblRemainingSeconds) <= parseInt(gblCardlesswithdrawEndTime)) {
        frmCardlesswithdrawComplete.lblTimer.skin = "lblRed200pxDigitNew"
    } else {
        frmCardlesswithdrawComplete.lblTimer.skin = "lblBlue200pxDigit"
    }
    if (gblRemainingSeconds >= 0) {
        frmCardlesswithdrawComplete.lblTimer.text = secondsToHms(gblRemainingSeconds--);
    }
}

function showApplicationPreviosForm() {
    var prevForm = kony.application.getPreviousForm();
    prevForm.show();
}

function frmCardlesswithdrawCompleteInit() {
    frmCardlesswithdrawComplete.preShow = frmCardlesswithdrawCompletePreshow;
    frmCardlesswithdrawComplete.postShow = frmCardlesswithdrawCompletePostshow;
    frmCardlesswithdrawComplete.btnDone.onClick = uvTransactionHistory;
    frmCardlesswithdrawComplete.btnTernmicateCode.onClick = UVAccessCodeCancel;
    frmCardlesswithdrawComplete.flexAtm.onClick = onClickATM;
    
    frmCardlesswithdrawComplete.lblFindTMB.onClick = onClickATM;
    frmCardlesswithdrawComplete.btnInfo.onClick = showcloseCardlessRequestInfoPopup;
    frmCardlesswithdrawComplete.flexRichtext.onClick = showcloseCardlessRequestInfoPopup;
    frmCardlesswithdrawComplete.btnBack.onClick = uvTransactionHistory;
    frmCardlesswithdrawComplete.onDeviceBack = doNothing;
}

function setDataFromAccountsCardLessWithdraw() {
    var ifAccounts = true;
    dsSelActSelIndex = [0, 0];
    dsSelActLength = 0;
    ifAccounts = setDataforOpenFromActs(frmCardlessWithdrawSelectAccount.segNSSlider);
    if (ifAccounts == false) {
        return false;
    }
    frmCardlessWithdrawSelectAccount.segNSSlider.selectedIndex = dsSelActSelIndex;
    dsSelActLength = frmCardlessWithdrawSelectAccount.segNSSlider.data.length - 1;
    frmCardlessWithdrawSelectAccount.show();
    showFocusTOAmountCLW();
}

function frmCardlessWithdrawSelectAccountPreshowNew() {
    changeStatusBarColor();
    if (gblPlatformName == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblPlatformName == "iPhone Simulator") frmCardlessWithdrawSelectAccount.HBox0f513ccee027441.isVisible = false;
    else frmCardlessWithdrawSelectAccount.HBox0f513ccee027441.isVisible = true;
    gblCardlessFlowCurrentForm = frmCardlessWithdrawSelectAccount;
    frmCardlessWithdrawSelectAccount.lblHeadertxt.text = kony.i18n.getLocalizedString("MB_CWSelectAcctTitle");
    frmCardlessWithdrawSelectAccount.lblSelectAccountNote.text = kony.i18n.getLocalizedString("MB_CWMsg");
    frmCardlessWithdrawSelectAccount.txtAmountVal.text = "";
}

function frmCardlessWithdrawSelectAccountPostshow() {}

function frmCardlesswithdrawConfirmationPreshow() {
  frmCardlesswithdrawConfirmation.lblHdrTxt.text = kony.i18n.getLocalizedString("MB_CWCFTitle");
  frmCardlesswithdrawConfirmation.Label0b9e42afaed6440.text = kony.i18n.getLocalizedString("MB_CWFromAcctTitle");
  frmCardlesswithdrawConfirmation.CopylblAccountName0fef10c0159944c.text = kony.i18n.getLocalizedString("MB_CWAmt");
  frmCardlesswithdrawConfirmation.CopyLabel0de54cd944f3941.text = kony.i18n.getLocalizedString("MB_CWAmtInsHeader");
  frmCardlesswithdrawConfirmation.CopyLabel0c98755e572b04d.text = kony.i18n.getLocalizedString("MB_CWAmtInsGuide");
  frmCardlesswithdrawConfirmation.btnConfirm.text = kony.i18n.getLocalizedString("MB_CWApproveBtn");
  frmCardlesswithdrawConfirmation.btnCancel.text = kony.i18n.getLocalizedString("MB_CWCancelLink");
  
}


function frmCardlesswithdrawConfirmationPostshow() {
    frmCardlesswithdrawConfirmation.lblAccountNo.text = gblCardlessWithDrawFromAcct["lblActNoval"];
    frmCardlesswithdrawConfirmation.lblAccountName.text = gblCardlessWithDrawFromAcct["lblCustName"];
    frmCardlesswithdrawComplete.btnBack.isVisible = false;
    addAccessPinKeypad(frmCardlesswithdrawConfirmation); 
}

function showCardlessConfirmationScreen() {
    //TO DO Call a service for log the initial 
    if (validateAmtCardlessWD()) {
        /* var activityTypeID = "150";
         var errorCode = "";
         var activityStatus = "Initial";
         var deviceNickName = "";
         var activityFlexValues1 = gblCardlessWithDrawFromAcct["lblCustName"];
         var activityFlexValues2 = "0000"; //CardlessCode
         var activityFlexValues3 = dateFormatForDisplay(new Date().getTime().toString()); //Old Future Date + New Future Date (Edit case)
         var activityFlexValues4 = "";
         var activityFlexValues5 = "";
         var logLinkageId = "";
         activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1, activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId);*/
        frmCardlesswithdrawConfirmation.lblAmount.text = frmCardlessWithdrawSelectAccount.txtAmountVal.text + ".00"
        // +"" +  + kony.i18n.getLocalizedString("currencyThaiBaht");
        frmCardlesswithdrawConfirmation.lblAmount.text = frmCardlesswithdrawConfirmation.lblAmount.text + kony.i18n.getLocalizedString("currencyThaiBaht")
        frmCardlesswithdrawConfirmation.show();
    }
}

function onRowClickCardLessSelectAccl() {
    gblCardlessWithDrawFromAcct = frmCardlessWithdrawSelectAccount.segNSSlider.selectedItems[0];
    gblCardlessFromSelIndex = frmCardlessWithdrawSelectAccount.segNSSlider.selectedIndex;
}

function frmCardlessWithdrawSelectAccountInit() {
    frmCardlessWithdrawSelectAccount.preShow = frmCardlessWithdrawSelectAccountPreshow;
    frmCardlessWithdrawSelectAccount.postShow = frmCardlessWithdrawSelectAccountPostshow;
    frmCardlessWithdrawSelectAccount.btnNext.onClick = showCardlessConfirmationScreen;
    frmCardlessWithdrawSelectAccount.btnBack.onClick = showCardLessWithDrawLanding;
    frmCardlessWithdrawSelectAccount.segNSSlider.onRowClick = onRowClickCardLessSelectAccl;
    frmCardlessWithdrawSelectAccount.btnInfo.onClick = showcloseCardlessRequestInfoPopup;
    frmCardlessWithdrawSelectAccount.onDeviceBack = doNothing;
	frmCardlessWithdrawSelectAccount.txtAmountVal.onTextChange = ontextChangeAmountCardlesswithdraw;
	//frmCardlessWithdrawSelectAccount.txtAmountVal.onDone = onDoneAmountCardlesswithdraw;
}

function ontextChangeAmountCardlesswithdraw() {
    var enteredAmount = frmCardlessWithdrawSelectAccount.txtAmountVal.text;
    if (isNotBlank(enteredAmount)) {
        enteredAmount = kony.string.replace(enteredAmount, ",", "");
        if (isNotBlank(enteredAmount) && enteredAmount.length > 0 && parseFloat(enteredAmount, 10) == 0) {} else {
            frmCardlessWithdrawSelectAccount.txtAmountVal.text = commaFormattedTransfer(enteredAmount);
        }
    }
}

function onDoneAmountCardlesswithdraw() {
    var enteredAmount = frmCardlessWithdrawSelectAccount.txtAmountVal.text;
    if (isNotBlank(enteredAmount)) {
       frmCardlessWithdrawSelectAccount.txtAmountVal.text = frmCardlessWithdrawSelectAccount.txtAmountVal.text + kony.i18n.getLocalizedString("currencyThaiBaht")
    }
}




function formatAmountWithCommasandBhat(amount) {
    var enteredAmount = amount+"";
    if (isNotBlank(enteredAmount)) {
        enteredAmount = kony.string.replace(enteredAmount, ",", "");
        if (isNotBlank(enteredAmount) && enteredAmount.length > 0 && parseFloat(enteredAmount, 10) == 0) {
            return amount;
        } else {
            return commaFormattedTransfer(enteredAmount);
        }
    }
}

function UVAccessCodeInfo() {
    try {
        var input_param = {};
        input_param["mode"] = "R";
        input_param["accountNumber"] = replaceAll(gblCardlessWithDrawFromAcct["accountNum"], "-", "");
        input_param["accountName"] = gblCardlessWithDrawFromAcct["lblCustName"];
        input_param["locale"] = kony.i18n.getCurrentLocale();
        kony.print("------------>"+frmCardlessWithdrawSelectAccount.txtAmountVal.text);
        var amount = replaceAll(frmCardlessWithdrawSelectAccount.txtAmountVal.text,",","") + ".00"
        kony.print("------------>"+amount);
        input_param["amount"] = amount;
        //  input_param["accountType"] = "SDA";
        // input_param["mobileNumber"] = "0804005575";
        /* input_param["moduleKey"] = moduleKey;
        input_param["moduleKey"] = moduleKey;
        input_param["moduleKey"] = moduleKey;
        input_param["moduleKey"] = moduleKey; */
        showLoadingScreen();
        kony.print("------------>" + input_param)
        invokeServiceSecureAsync("getRequestAccessCode", input_param, UVAccessCodeInfoCallBack);
    } catch (err) {}
}

function UVAccessCodeInfoCallBack(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
            dismissLoadingScreen();
            /*  var result = {
        "EXPIRE_DATE":"2017-07-11 03:00:00.0",
        "expTime":"360",
        "CREATED_DATE":"12 August 2017 12:00",
        "STATUS":"0",
        "AC_REQUEST_ID":"1",
        "accessCode":"3210",
        "START_TIME":"10:00",
      	"END_TIME":"13:00",
      	"ACCOUNT_NO":"155-965-4569",
      	"ACCOUNT_NAME":"TMB All Free 1235"} */
            frmCardlesswithdrawComplete.btnDone.isVisible = true;
            frmCardlesswithdrawComplete.flexFooterLine.isVisible = false;
            frmCardlesswithdrawComplete.flexFooter.top = "1%";
            frmCardlesswithdrawComplete.lblAccessCode.text = result["accessCode"];
            gblRemainingSeconds = result["expTime"];
            frmCardlesswithdrawComplete.lblTimer.text = secondsToHms(gblRemainingSeconds);
            frmCardlesswithdrawComplete.lblAccountName.text = frmCardlesswithdrawConfirmation.lblAccountName.text;
            frmCardlesswithdrawComplete.lblAccountNumber.text = frmCardlesswithdrawConfirmation.lblAccountNo.text;
            frmCardlesswithdrawComplete.lblStartTime.text = result["START_TIME"];
            frmCardlesswithdrawComplete.lblEndTime.text = result["END_TIME"];
            frmCardlesswithdrawComplete.lblTransactionId.text = result["transactionID"];
            frmCardlesswithdrawComplete.lblExpireFulldate.text = result["EXPIRE_DATE"];
            if (gblUniqId != 0 && gblUniqId != undefined) {
                kony.timer.cancel(gblUniqId)
            }
            var uniqueIdGen = Math.random().toString(36).substr(2, 9) + "";
            gblUniqId = uniqueIdGen;
            if (gblRemainingSeconds != 0) {
                var timer = kony.timer.schedule(uniqueIdGen, setTimerToActiveCode, 1, true);
            }
            frmCardlesswithdrawComplete.lblAmount.text = frmCardlesswithdrawConfirmation.lblAmount.text
            frmCardlesswithdrawComplete.show();
        } else {
            dismissLoadingScreen();
            showAlert(kony.i18n.getLocalizedString("MB_CWFailRequest"), kony.i18n.getLocalizedString("info"));
        }
    }
}

function UVAccessCodeCancel() {
    try {
        var input_param = {};
        input_param["mode"] = "C";
        input_param["transactionId"] = frmCardlesswithdrawComplete.lblTransactionId.text;
        input_param["accessCode"] = frmCardlesswithdrawComplete.lblAccessCode.text;
        input_param["tranDate"] = frmCardlesswithdrawComplete.lblExpireFulldate.text;
        input_param["accountNumber"] = frmCardlesswithdrawComplete.lblAccountNumber.text
        showLoadingScreen();
        // UVAccessCodeCancelCallBack()
        invokeServiceSecureAsync("getRequestAccessCode", input_param, UVAccessCodeCancelCallBack);
    } catch (err) {}
}

function UVAccessCodeCancelCallBack(status, result) {
    kony.print("result---" + result)
    kony.print("status---" + status)
    kony.print("opstatus---" + result["opstatus"])
    if (status == 400) {
        if (result["opstatus"] == 0) {
            dismissLoadingScreen();
            frmCardlesswithdrawTermincateCode.show();
        } else {
			if(result["errCode"] == "01003"){
				dismissLoadingScreen();
				showAlert(kony.i18n.getLocalizedString("msg_Deactivate"), kony.i18n.getLocalizedString("info"));
			}else if(result["errCode"] == "01004"){
				dismissLoadingScreen();
				showAlert(kony.i18n.getLocalizedString("msg_Used"), kony.i18n.getLocalizedString("info"));
			}else if(result["errCode"] == "01005"){
				dismissLoadingScreen();
				showAlert(kony.i18n.getLocalizedString("msg_Expired"), kony.i18n.getLocalizedString("info"));
			}else{			
				dismissLoadingScreen();
				showAlert(kony.i18n.getLocalizedString("MB_CWFailDelete"), kony.i18n.getLocalizedString("info"));
			}
        }
    } else {
        showAlert(kony.i18n.getLocalizedString("MB_CWFailDelete"), kony.i18n.getLocalizedString("info"));
        dismissLoadingScreen();
    }
}


function uvTransactionHistory() {
    var input_param = {};
    input_param["historyType"] = "ACCEECODES";
    showLoadingScreen();
    invokeServiceSecureAsync("uvTransactionHistory", input_param, uvTransactionHistoryCallBack);
}

function checkUVActiveCode() {
    var input_param = {};
    input_param["historyType"] = "ACCEECODES";
    showLoadingScreen();
    invokeServiceSecureAsync("uvTransactionHistory", input_param, checkUVActiveCodeCallBack);
}

function checkUVActiveCodeCallBack(status, result) {
    var hasActive = false;
    if (status == 400) {
        if (result["opstatus"] == 0) {
            result = result["Result"];
            if (result.length > 0) {
                for (var i = 0; i < result.length; i++) {
                    if (result[i]["STATUS"] == "0") {
                        hasActive = true;
                        break;
                    }
                }
            }
        }
    }
    if (hasActive) {
        alert(kony.i18n.getLocalizedString("MB_CWCannotRequestCode"));
        dismissLoadingScreen();
    } else {
        loadFromAccountForCardlessWD(glb_accId);
    }
}


function uvTransactionHistoryCallBack(status, result) {
   //var result = {"StatusCode":"0","Result":[{"AMOUNT":"500.00","EXPIRE_DATE":"2017-09-01 22:42:53.687","EXPIRE_TIME":"10136","CREATED_DATE":"01 September 2017 07:42","CUSTOMER_NAME":"MADHU GULLAPALLY","ACCOUNT_NO":"0322607821","ACCOUNT_NAME":"TMB All Free Ac 7821","TRANSACTION_ID":"f0226b01-5d58-4897-976c-5104c09aa5a8","END_TIME":"10:42","START_TIME":"07:42","STATUS":"0","AC_REQUEST_ID":"52","CODE":"0077"}],"httpStatusCode":0,"opstatus":"0","tknid":"765F6D1F3D8B078924FE29FE5B61C89ED10CC6239F142D4BDCB4117813ABD0F9"}
    /* var result = {
        "StatusCode": "0",
        "Result": [],
        "httpStatusCode": 0,
        "opstatus": "0",
        "tknid": "765F6D1F3D8B078924FE29FE5B61C89ED10CC6239F142D4BDCB4117813ABD0F9"
    } */
    gblCLWActiveCodes = [];
    gblCLWUsedCodes = [];
    gblCLWExpiredCodes = [];
    frmCardlesswithdrawLanding.segActiveCodes.removeAll();
    frmCardlesswithdrawLanding.segExpiredCodes.removeAll();
    frmCardlesswithdrawLanding.segUsedCodes.removeAll();
    frmCardlesswithdrawLanding.lblActiveActiveWithdrawHeader.isVisible = false;
    frmCardlesswithdrawLanding.lblUsedWithdrawHeader.isVisible = false;
    frmCardlesswithdrawLanding.lblExpiredCOdesHeader.isVisible = false;
    frmCardlesswithdrawLanding.segActiveCodes.isVisible = false;
    frmCardlesswithdrawLanding.segExpiredCodes.isVisible = false;
    frmCardlesswithdrawLanding.segUsedCodes.isVisible = false;
    frmCardlesswithdrawLanding.flexEndOfCodes.isVisible = false;
    frmCardlesswithdrawLanding.flexNoCodeNote.isVisible = true;
    frmCardlesswithdrawLanding.FlexLine1.height = "1px";
    frmCardlesswithdrawLanding.btnWithdrawMoney.isVisible = true;
    frmCardlesswithdrawLanding.lblActiveCodeError.isVisible = false;
    if (status == 400) {
        if (result["opstatus"] == 0) {
            result = result["Result"];
            if (result.length > 0) {
                frmCardlesswithdrawLanding.flexNoCodeNote.isVisible = false;
                //frmCardlesswithdrawLanding.FlexLine1.isVisible = true;
                frmCardlesswithdrawLanding.FlexLine1.height = "10dp";
                for (var i = 0; i < result.length; i++) {
                    if (result[i]["STATUS"] == "0") {
                        gblCLWActiveCodes.push(result[i]);
                    } else if (result[i]["STATUS"] == "1") {
                        gblCLWUsedCodes.push(result[i]);
                    } else if (result[i]["STATUS"] == "2" || result[i]["STATUS"] == "3") {
                        gblCLWExpiredCodes.push(result[i]);
                    }
                }
                kony.print("gblCLWActiveCodes" + gblCLWActiveCodes)
                kony.print("gblCLWUsedCodes" + gblCLWUsedCodes)
                kony.print("gblCLWExpiredCodes" + gblCLWExpiredCodes)
                if (gblCLWActiveCodes.length > 0) {
                    setActiveCodesToSegment(gblCLWActiveCodes);
                    frmCardlesswithdrawLanding.lblActiveActiveWithdrawHeader.isVisible = true;
                    frmCardlesswithdrawLanding.segActiveCodes.isVisible = true;
                    if (gblCLWActiveCodes.length >= 1) {
                        frmCardlesswithdrawLanding.btnWithdrawMoney.isVisible = false;
                        frmCardlesswithdrawLanding.lblActiveCodeError.isVisible = true;
                    } else {
                        frmCardlesswithdrawLanding.btnWithdrawMoney.isVisible = true;
                        frmCardlesswithdrawLanding.lblActiveCodeError.isVisible = false;
                    }
                }
                if (gblCLWUsedCodes.length > 0) {
                    setUsedCodesToSegment(gblCLWUsedCodes);
                    frmCardlesswithdrawLanding.lblUsedWithdrawHeader.isVisible = true;
                    frmCardlesswithdrawLanding.segUsedCodes.isVisible = true;
                    frmCardlesswithdrawLanding.flexEndOfCodes.isVisible = true;
                }
                if (gblCLWExpiredCodes.length > 0) {
                    setExpiredCodesToSegment(gblCLWExpiredCodes);
                    frmCardlesswithdrawLanding.lblExpiredCOdesHeader.isVisible = true;
                    frmCardlesswithdrawLanding.segExpiredCodes.isVisible = true;
                    frmCardlesswithdrawLanding.flexEndOfCodes.isVisible = true;
                }
                frmCardlesswithdrawLanding.flexNoCodeNote.isVisible = false;
                //frmCardlesswithdrawLanding.FlexLine1.isVisible = true;
                frmCardlesswithdrawLanding.FlexLine1.height = "10dp";
                if (gblCLWActiveCodes.length == 0) {
                    if (gblCLWUsedCodes.length == 0) {
                        if (gblCLWExpiredCodes.length == 0) {
                            frmCardlesswithdrawLanding.lblActiveActiveWithdrawHeader.isVisible = false;
                            frmCardlesswithdrawLanding.lblUsedWithdrawHeader.isVisible = false;
                            frmCardlesswithdrawLanding.lblExpiredCOdesHeader.isVisible = false;
                            frmCardlesswithdrawLanding.segActiveCodes.isVisible = false;
                            frmCardlesswithdrawLanding.segExpiredCodes.isVisible = false;
                            frmCardlesswithdrawLanding.segUsedCodes.isVisible = false;
                            frmCardlesswithdrawLanding.flexNoCodeNote.isVisible = true;
                            frmCardlesswithdrawLanding.FlexLine1.height = "1px";
                            frmCardlesswithdrawLanding.btnWithdrawMoney.isVisible = true;
                        }
                    }
                }
            } else {
                frmCardlesswithdrawLanding.lblActiveActiveWithdrawHeader.isVisible = false;
                frmCardlesswithdrawLanding.lblUsedWithdrawHeader.isVisible = false;
                frmCardlesswithdrawLanding.lblExpiredCOdesHeader.isVisible = false;
                frmCardlesswithdrawLanding.segActiveCodes.isVisible = false;
                frmCardlesswithdrawLanding.segExpiredCodes.isVisible = false;
                frmCardlesswithdrawLanding.segUsedCodes.isVisible = false;
                frmCardlesswithdrawLanding.btnWithdrawMoney.isVisible = true;
                frmCardlesswithdrawLanding.flexNoCodeNote.isVisible = true;
                frmCardlesswithdrawLanding.FlexLine1.height = "1px";
                frmCardlesswithdrawLanding.lblActiveCodeError.isVisible = false;
                //gblUVCustomerForTesting = true;
            }
            dismissLoadingScreen();
            frmCardlesswithdrawLanding.show();
        } else {
            showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
            dismissLoadingScreen();
        }
    }
}

function onClickOfCardlessWithDrawMenu() {
    try {
      
        //3. Setting up VTAP SDK
   		setUpVTapSDK();
		if(isSignedUser) {
			gblCardlessFromSelAcctId = "";
			glb_accId = "";
			gblIndex = "";
			//gblUVregisterMB = "Y"
			if (gblUVregisterMB == "Y") {
				uvTransactionHistory();
			} else {
				callUVMaintInqService(callBackUVMaintInqServiceForCardLessWD);
			}
		} else{
			gotoLoginOrActivation("");
		}
    } catch (e) {
        //alert(e.message);
      	kony.print("Error onClickOfCardlessWithDrawMenu :"+e.message);
    }
}

function callUVMaintInqService(callBackUVMaintInqService) {
    // call service UV Approval
    showLoadingScreen();
    var inputParam = {};
    invokeServiceSecureAsync("UVMaintInq", inputParam, callBackUVMaintInqService);
}

function callBackUVMaintInqServiceForCardLessWD(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == "0") {
            dismissLoadingScreen();
            if (resulttable["CustServFlag"] == "Y" && resulttable["MBFlag"] == "Y" && resulttable["IBFlag"] == "Y" && resulttable["ATMFlag"] == "Y") {
                gblUVregisterMB = "Y";
                uvTransactionHistory();
            } else {
                gblUVregisterMB = "N";
                frmCardlesswithdrawActivation.show();
            }
        } else {
            dismissLoadingScreen();
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        }
    }
}

function callBackUVMaintInqServiceForCardLessWDShortcut(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == "0") {
            dismissLoadingScreen();
            if (resulttable["CustServFlag"] == "Y" && resulttable["MBFlag"] == "Y" && resulttable["IBFlag"] == "Y" && resulttable["ATMFlag"] == "Y") {
                gblUVregisterMB = "Y";
                checkUVActiveCode();
            } else {
                gblUVregisterMB = "N";
                frmCardlesswithdrawActivation.show();
            }
        } else {
            dismissLoadingScreen();
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        }
    }
}

function callBackUVMaintInqServiceForTMBConfirmList(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == "0") {
            dismissLoadingScreen();
            if (resulttable["CustServFlag"] == "Y" && resulttable["MBFlag"] == "Y" && resulttable["IBFlag"] == "Y" && resulttable["ATMFlag"] == "Y") {
                gblUVregisterMB = "Y";
                callUVPushTxnHistory();
            } else {
                gblUVregisterMB = "N";
                gblActivationCurrentForm = "TMBConfirm"
                frmUVActivateNow.show();
            }
        } else {
            dismissLoadingScreen();
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        }
    }
}

function validateAmtCardlessWD() {
    //alert(gblCardlessWithDrawFromAcct)
    var availBalance = gblCardlessWithDrawFromAcct["lblBalance"];
    availBalance = replaceAll(availBalance, kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    availBalance = replaceAll(availBalance, ",", "");
    var amount = replaceAll(frmCardlessWithdrawSelectAccount.txtAmountVal.text,",","");
    var maxAmountErrorMsg = kony.i18n.getLocalizedString("CW_msgMaxAmt").replace("{maximum value}", gblCardlessWithDrawMaxAmt);
    if (amount == "" || parseInt(amount) == 0) {
        showAlertWithCallBack(kony.i18n.getLocalizedString("CW_msgNoAmt"), kony.i18n.getLocalizedString("info"), showFocusTOAmountCLW);
        return false;
    } else if (parseInt(amount) % 100 != 0) {
        showAlertWithCallBack(kony.i18n.getLocalizedString("CW_msgBankNote"), kony.i18n.getLocalizedString("info"), showFocusTOAmountCLW);
        return false;
    } else if (parseInt(amount) > parseInt(replaceAll(gblCardlessWithDrawMaxAmt,",",""))) {
        showAlertWithCallBack(maxAmountErrorMsg, kony.i18n.getLocalizedString("info"), showFocusTOAmountCLW);
        return false;
    } else if (parseInt(amount) > parseInt(availBalance)) {
        showAlertWithCallBack(kony.i18n.getLocalizedString("CW_msgBalanceNotEnough"), kony.i18n.getLocalizedString("info"), showFocusTOAmountCLW);
        return false;
    } else {
        //crmProfileInqDailyLimitRequest();
        cardlessWithdrawDailyLimitRequest();
    }
}

function showFocusTOAmountCLW() {
    frmCardlessWithdrawSelectAccount.txtAmountVal.setFocus(true);
}

function showTxnPasswordPopupRequestAccessCodeConfimation() {
    var refNo = "";
    var mobNO = "";
   if(gblAuthAccessPin == true){
     showAccesspinPopup();
   }else{
     var lblText = kony.i18n.getLocalizedString("transPasswordSub");    
     showOTPPopup(lblText, refNo, mobNO, verifyPwdForRequestAccessCodeConfimation, 3); 
   }
    
}

function verifyPwdForRequestAccessCodeConfimation(tranPassword) {
    if (tranPassword == null || tranPassword == '') {
        setTransPwdFailedError(kony.i18n.getLocalizedString("emptyMBTransPwd"));
        return false;
    } else {
        showLoadingScreen();
        if(gblAuthAccessPin != true){
          popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("keyTransPwdMsg");
          popupTractPwd.lblPopupTract7.skin = lblPopupLabelTxt;
          popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = tbxPopupBlue;
          popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = tbxPopupBlue;
          popupTractPwd.hbxPopupTranscPwd.skin = tbxPopupBlue;  
        }
        
        var inputParam = {};
        var locale = kony.i18n.getCurrentLocale();
        inputParam["password"] = tranPassword;
        inputParam["notificationAdd_appID"] = appConfig.appId;
        inputParam["verifyPwdMB_loginModuleId"] = "MB_TxPwd";
        inputParam["verifyPwdMB_retryCounterVerifyAccessPin"] = "0";
        inputParam["verifyPwdMB_retryCounterVerifyTransPwd"] = "0";
        inputParam["locale"] = locale;
        inputParam["mode"] = "R";
        inputParam["accountNumber"] = replaceAll(gblCardlessWithDrawFromAcct["accountNum"], "-", "");
        inputParam["accountName"] = gblCardlessWithDrawFromAcct["lblCustName"];
        var amount = replaceAll(frmCardlessWithdrawSelectAccount.txtAmountVal.text,",","") +".00"
        inputParam["amount"] = amount;
        invokeServiceSecureAsync("verifyReqAccessCodeTransPwdComposite", inputParam, verifyPwdForRequestAccessCodeConfimationCallBack);
    }
}

function verifyPwdForRequestAccessCodeConfimationCallBack(status, resulttable) {
    if (status == 400) {
        dismissLoadingScreen();
        if (resulttable["errCode"] == "VrfyTxPWDErr00001" || resulttable["errCode"] == "VrfyTxPWDErr00002") {
            setTransPwdFailedError(kony.i18n.getLocalizedString("invalidTxnPwd"));
            return false;
        } else if (resulttable["errCode"] == "VrfyTxPWDErr00003") {
            showTranPwdLockedPopup();
            return false;
        } else if (resulttable["errCode"] == "VrfyAcPWDErr00001" || resulttable["errCode"] == "VrfyAcPWDErr00002") {
          popupEnterAccessPin.lblWrongPin.setVisibility(true);
          resetAccessPinImg(resulttable["badLoginCount"]);
          return false;
        } else if (resulttable["errCode"] == "VrfyAcPWDErr00003"){
          onClickCancelAccessPin();
          gotoUVPINLockedScreenPopUp();
          return false;
        } else if (resulttable["opstatus"] == 8005) {
            dismissLoadingScreen();
            if (resulttable["errCode"] == "VrfyOTPErr00001") {
                gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
                dismissLoadingScreen();
                alert("" + kony.i18n.getLocalizedString("invalidOTP"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00002") {
                dismissLoadingScreen();
                alert("" + kony.i18n.getLocalizedString("ECVrfyOTPErr"));
                //startRcCrmUpdateProfilBPIB("04");
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00005") {
                dismissLoadingScreen();
                alert("" + kony.i18n.getLocalizedString("KeyTokenSerialNumError"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00006") {
                dismissLoadingScreen();
                gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
                alert("" + resulttable["errMessage"]);
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00004") {
                dismissLoadingScreen();
                alert("" + resulttable["errMsg"]);
                return false;
            } else {
                dismissLoadingScreen();
                popupTractPwd.dismiss();
                onClickCancelAccessPin();
                gblPasswordSucces = false;
                showAlert(kony.i18n.getLocalizedString("MB_CWFailRequest"), kony.i18n.getLocalizedString("info"));
            }
        } else if (resulttable["opstatus"] == 0) {
            //Do success
            popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("keyTransPwdMsg");
            popupTractPwd.lblPopupTract7.skin = lblPopupLabelTxt;
            popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = tbxPopupBlue;
            popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = tbxPopupBlue;
            popupTractPwd.hbxPopupTranscPwd.skin = tbxPopupBlue;
            popupTractPwd.dismiss();
            onClickCancelAccessPin();
            UVAccessCodeInfoCallBack(status, resulttable)
        } else {
            popupTractPwd.dismiss();
            onClickCancelAccessPin();
            gblPasswordSucces = false;
            showAlert(kony.i18n.getLocalizedString("MB_CWFailRequest"), kony.i18n.getLocalizedString("info"));
        }
    }
}

function crmProfileInqDailyLimitRequest() {
    var inputParam = {}
    showLoadingScreenPopup();
    invokeServiceSecureAsync("crmProfileInq", inputParam, callBackCrmProfileInqDailyLimitRequest)
}

function callBackCrmProfileInqDailyLimitRequest(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            var StatusCode = resulttable["statusCode"];
            if (StatusCode != 0) {
                if (resulttable["errMsg"] != undefined) {
                    dismissLoadingScreenPopup();
                    alert(resulttable["errMsg"]);
                    return false;
                } else {
                    dismissLoadingScreenPopup();
                    alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
                    return false;
                }
            }
            var DailyLimit = parseFloat(resulttable["ebMaxLimitAmtCurrent"].toString());
            var UsageLimit;
            if (resulttable["ebAccuUsgAmtDaily"] == "" && resulttable["ebAccuUsgAmtDaily"].length == 0) {
                UsageLimit = 0;
            } else {
                UsageLimit = parseFloat(resulttable["ebAccuUsgAmtDaily"].toString());
            }
            var channelLimit = DailyLimit - UsageLimit;
            if (channelLimit < 0) {
                channelLimit = 0;
            }
            kony.print("DailyLimit" + DailyLimit + ",UsageLimit" + UsageLimit);
            var dailylimitExceedMsg = kony.i18n.getLocalizedString("CW_msgDailyLimit")
            dailylimitExceedMsg = dailylimitExceedMsg.replace("{remaining daily limit}", commaFormatted(fixedToTwoDecimal(channelLimit) + ""));
            var amtEntered = frmCardlessWithdrawSelectAccount.txtAmountVal.text;
            kony.print("amtEntered" + amtEntered);
            var transferAmt = parseFloat(amtEntered);
            kony.print("transferAmt" + transferAmt);
            if (channelLimit < transferAmt) {
                dismissLoadingScreenPopup();
                showAlertWithCallBack(dailylimitExceedMsg, kony.i18n.getLocalizedString("info"), showFocusTOAmountCLW);
                return false;
            } else {
                dismissLoadingScreenPopup();
               
                frmCardlesswithdrawConfirmation.lblAmount.text = frmCardlessWithdrawSelectAccount.txtAmountVal.text + ".00" + kony.i18n.getLocalizedString("currencyThaiBaht")
                frmCardlesswithdrawConfirmation.show();
                return true;
            }
        } else {
            dismissLoadingScreenPopup();
            if (resulttable["opstatus"] == "8005") {
                showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errMsg"] != undefined) {
                alert(resulttable["errMsg"]);
                return false;
            } else {
                alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
                return false;
            }
        }
    }
}

function cardlessWithdrawDailyLimitRequest() {
    var inputParam = {}
    showLoadingScreenPopup();
    inputParam["accountNumber"] = replaceAll(gblCardlessWithDrawFromAcct["accountNum"], "-", "");
    inputParam["accountName"] = gblCardlessWithDrawFromAcct["lblCustName"];
    var amount = frmCardlessWithdrawSelectAccount.txtAmountVal.text + ".00"
	amount = replaceAll(amount,",","");
    inputParam["amount"] = amount;
    invokeServiceSecureAsync("cardlessWithdrawDailyLimitverify", inputParam, callBackCardlessWithdrawDailyLimitRequest)
}

function callBackCardlessWithdrawDailyLimitRequest(status, resulttable) {
    kony.print("CardLess : status>>" + status);
    if (status == 400) {
        kony.print("CardLess : opstatus>>" + resulttable["opstatus"]);
        if (resulttable["opstatus"] == 0) {
            var DailyLimit = parseFloat(resulttable["ebMaxLimitAmtCurrent"].toString());
            var UsageLimit;
            if (resulttable["ebAccuUsgAmtDaily"] == "" && resulttable["ebAccuUsgAmtDaily"].length == 0) {
                UsageLimit = 0;
            } else {
                UsageLimit = parseFloat(resulttable["ebAccuUsgAmtDaily"].toString());
            }
            var channelLimit = DailyLimit - UsageLimit;
            if (channelLimit < 0) {
                channelLimit = 0;
            }
            kony.print("DailyLimit" + DailyLimit + ",UsageLimit" + UsageLimit);
            var dailylimitExceedMsg = kony.i18n.getLocalizedString("CW_msgDailyLimit")
            dailylimitExceedMsg = dailylimitExceedMsg.replace("{remaining daily limit}", commaFormatted(fixedToTwoDecimal(channelLimit) + ""));
            var amtEntered = frmCardlessWithdrawSelectAccount.txtAmountVal.text;
            kony.print("amtEntered" + amtEntered);
            var transferAmt = parseFloat(amtEntered);
            kony.print("transferAmt" + transferAmt);
            if (channelLimit < transferAmt) {
                dismissLoadingScreenPopup();
                showAlertWithCallBack(dailylimitExceedMsg, kony.i18n.getLocalizedString("info"), showFocusTOAmountCLW);
                return false;
            } else {
                dismissLoadingScreenPopup();
                frmCardlesswithdrawConfirmation.lblAmount.text = frmCardlessWithdrawSelectAccount.txtAmountVal.text + ".00" + kony.i18n.getLocalizedString("currencyThaiBaht")
                
                frmCardlesswithdrawConfirmation.show();
                return true;
            }
        } else {
            dismissLoadingScreenPopup();
            if (resulttable["opstatus"] == "8005") {
                showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errMsg"] != undefined) {
                alert(resulttable["errMsg"]);
                return false;
            } else {
                alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
                return false;
            }
        }
    }
}