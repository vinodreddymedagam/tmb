animation = true;

function animateAccountSummary() {
  if(animation === false){
    frmAccountSummaryLanding["flxSkeletonScrollWhite"].animate(getAnimationObject(),animConfig(),{});
    frmAccountSummaryLanding.flxSkeletonScroll.isVisible = true;
    frmAccountSummaryLanding.flxSkeletonScrollWhite.isVisible = true;
  }

}

function getAnimationObject()
{
  //alert("animation");
  var animDefinition = {
    "0": {
      "stepConfig": {
        "timingFunction": kony.anim.EASE
      },
      "backgroundColor": "ffffff",
      "opacity": 0
    },
    "100": {
      "stepConfig": {
        "timingFunction": kony.anim.EASE
      },
      "backgroundColor": "ffffff",
      "opacity": 0.5
    }
  };
  animDef = kony.ui.createAnimation(animDefinition);
  return animDef;
}

function animConfig(){
  var config = {
    "delay": 0,
    "iterationCount": 1000,
    "fillMode": kony.anim.FILL_MODE_NONE,
    "duration": 0.30,
    "direction": kony.anim.DIRECTION_ALTERNATE
  };
  return config;
}

function endAccountSummaryAnimation(){
  frmAccountSummaryLanding.flxSkeletonScroll.isVisible = false;
  frmAccountSummaryLanding.flxSkeletonScrollWhite.isVisible = false;
  // frmAccountSummaryLanding.destroy();
}




