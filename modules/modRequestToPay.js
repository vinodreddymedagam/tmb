gblAcceptIndividual = false;
gblAcceptBusiness = false;
gblConsentIndividualFlag = false;
gblConsentBusinessFlag = false;
gblRTPDetailsSortBy = 0;
gblIsRTPFirstTime = true;
gblIsRTPStartUp = false;
gblIsRTPBillersCategory = false;

//easy for debug about RTP
function rtp_log(msg){
	kony.print("RTP: "+msg);
}

function frmMBRTPSettingsInit(){
  frmMBRTPSettings.flxAcceptFriend.onClick = onClickRTPAcceptFriend;
  frmMBRTPSettings.flxAcceptBusiness.onClick = onClickRTPAcceptBusiness;
  frmMBRTPSettings.preShow = preShowfrmMBRTPSettings;
}

function onClickRTPAcceptFriend(){
  if(frmMBRTPSettings.imgFriendCheck.src == "radio_uncheck.png"){
    frmMBRTPSettings.imgFriendCheck.src = "radio_check.png";
    gblAcceptIndividual = true;
  }else{
    frmMBRTPSettings.imgFriendCheck.src = "radio_uncheck.png";
    gblAcceptIndividual = false;
  }
}

function onClickRTPAcceptBusiness(){
  if(frmMBRTPSettings.imgBusinessCheck.src == "radio_uncheck.png"){
    frmMBRTPSettings.imgBusinessCheck.src = "radio_check.png";
    gblAcceptBusiness = true;
    //frmMBRTPSettings.flxConsent.setVisibility(true);
  }else{
    frmMBRTPSettings.imgBusinessCheck.src = "radio_uncheck.png";
    gblAcceptBusiness = false;
    frmMBRTPSettings.flxConsent.setVisibility(false);
  }
}

function preShowfrmMBRTPSettings(){
  	frmMBRTPSettings.lbRPTTitle.text = kony.i18n.getLocalizedString("MB_RTPSettingTittle");
  	frmMBRTPSettings.lblAcceptFriend.text = kony.i18n.getLocalizedString("MB_RTPSettingPDesc");
  	frmMBRTPSettings.lblAcceptBusiness.text = kony.i18n.getLocalizedString("MB_RTPSettingCDesc1");
  	frmMBRTPSettings.lblConsent.text  = kony.i18n.getLocalizedString("MB_RTPSettingCDesc2");
  	frmMBRTPSettings.btnRegister.text  = kony.i18n.getLocalizedString("MB_RTPBillerSetbtn");
  	frmMBRTPSettings.btnDoLater.text = kony.i18n.getLocalizedString("MB_RTPBillerLater");
}

function onClickRTPConsentSettingBtn(){
  gblIsRTPFirstTime = false;
  displayFrmMBRTPSettings();
}

function onClickRTPSettingBtn(){
  gblIsRTPFirstTime = true;
  //checkUserPushNotificationEnable(displayFrmMBRTPSettings);
  displayFrmMBRTPSettings();
}

function displayFrmMBRTPSettings(){
  rtp_log("@@@ In displayFrmMBRTPSettings()@@");
  rtp_log("@@@ gblConsentIndividualFlag:::"+gblConsentIndividualFlag);
  rtp_log("@@@ gblConsentBusinessFlag:::"+gblConsentBusinessFlag);
  if(gblConsentIndividualFlag){
    frmMBRTPSettings.imgFriendCheck.src = "radio_check.png";
  }else{
    frmMBRTPSettings.imgFriendCheck.src = "radio_uncheck.png";
  }
  if(gblConsentBusinessFlag){
    frmMBRTPSettings.imgBusinessCheck.src = "radio_check.png";
    //frmMBRTPSettings.flxConsent.setVisibility(true);
  }else{
    frmMBRTPSettings.imgBusinessCheck.src = "radio_uncheck.png";
  }
  frmMBRTPSettings.show();
}

function onClickRTPGotItBtn(){
  gblIsRTPFirstTime = true;
  kony.store.setItem("RTPIntroFlag", "Y");
  checkRTPConditionToDisplayScreen();
}

function onClickRTPLaterLink(){
  checkRTPConditionToDisplayScreen();
}

function checkRTPConditionToDisplayScreen(){
  rtp_log("@@@ In checkRTPConditionToDisplayScreen()@@");
  var RTPIntroFlag = kony.store.getItem("RTPIntroFlag");
  rtp_log("RTPIntroFlag :::"+RTPIntroFlag+"||gblIsRTPFirstTime:::"+gblIsRTPFirstTime);

  if(RTPIntroFlag == "Y" && gblIsRTPFirstTime){
    rtp_log("--- Calling RTPInq ---");
    invokeRTPDetails();
    /*if(gblAcceptIndividual || gblAcceptBusiness){
      invokeRTPDetails();
    }else{
      displayfrmMBRTPNoActiveList();
    }*/
  }else{
    preShowFrmMBRTPAcceptConsent();
    rtp_log("--- RTPAcceptConsent Fist time ---");
    frmMBRTPAcceptConsent.show();
    dismissLoadingScreen();
    gblIsRTPFirstTime = true;
    kony.store.setItem("RTPIntroFlag", "Y");
  }
}

function displayfrmMBRTPNoActiveList(){
  rtp_log("@@@ In displayfrmMBRTPNoActiveList()@@");
  // accept individual
  if(gblConsentIndividualFlag){
    frmMBRTPNoActiveList.imgPersonStatus.src = "icon_answer_choose.png";
  }else{
    frmMBRTPNoActiveList.imgPersonStatus.src = "icon_checked_grey.png";
  }
  // accept business
  if(gblConsentBusinessFlag){
    frmMBRTPNoActiveList.imgBizStatus.src = "icon_answer_choose.png";
  }else{
    frmMBRTPNoActiveList.imgBizStatus.src = "icon_checked_grey.png";
  }
  frmMBRTPNoActiveList.show();
  dismissLoadingScreen();
  rtp_log("---displayfrmMBRTPNoActiveList() END---");
}

function onClickRPTBackBtnSettings(){
  checkRTPConditionToDisplayScreen();
}

function preShowFrmMBRTPAcceptConsent(){
  rtp_log("@@@ In preShowFrmMBRTPAcceptConsent() @@@");
  rtp_log("@@@ gblConsentIndividualFlag :::"+gblConsentIndividualFlag);
  rtp_log("@@@ gblConsentBusinessFlag :::"+gblConsentBusinessFlag);
  
  frmMBRTPAcceptConsent.lblHeader.text = kony.i18n.getLocalizedString("MB_RTPTittle");
  if(gblConsentIndividualFlag || gblConsentBusinessFlag){
    frmMBRTPAcceptConsent.flxFooter.setVisibility(true);
    frmMBRTPAcceptConsent.imgAcceptConsent.src = "accept_consent.png";
    frmMBRTPAcceptConsent.btnGotIt.text = kony.i18n.getLocalizedString("MB_RTPOkbtn");
    frmMBRTPAcceptConsent.lblTitle.text = kony.i18n.getLocalizedString("MB_RTPEnableMsg1");
    frmMBRTPAcceptConsent.lblDesc.text = kony.i18n.getLocalizedString("MB_RTPEnableMsg2");
    frmMBRTPAcceptConsent.lblSettingDesc1.text = kony.i18n.getLocalizedString("MB_RTPDisableMsg31");
    frmMBRTPAcceptConsent.lblSettingDesc2.text = kony.i18n.getLocalizedString("MB_RTPEnableMsg32");
    frmMBRTPAcceptConsent.lblSettingDesc3.text = kony.i18n.getLocalizedString("MB_RTPEnableMsg33");

  }else{
    frmMBRTPAcceptConsent.flxFooter.setVisibility(false);
    frmMBRTPAcceptConsent.imgAcceptConsent.src = "accept_consent.png";
    frmMBRTPAcceptConsent.lblTitle.text = kony.i18n.getLocalizedString("MB_RTPDisableMsg1");
    frmMBRTPAcceptConsent.lblDesc.text = kony.i18n.getLocalizedString("MB_RTPDisableMsg2");
    frmMBRTPAcceptConsent.lblSettingDesc1.text = kony.i18n.getLocalizedString("MB_RTPDisableMsg31");
    frmMBRTPAcceptConsent.lblSettingDesc2.text = kony.i18n.getLocalizedString("MB_RTPDisableMsg32");
    frmMBRTPAcceptConsent.lblSettingDesc3.text = kony.i18n.getLocalizedString("MB_RTPDisableMsg33");
  }
}

// On Back of RTP Settings.
function onClickRTPBackArrowBtnSetting(){
  if(gblAcceptBusiness || gblAcceptIndividual ){
    	checkUserPushNotificationEnable(checkNotificationSettingRTPCallback);
  }else{
   		checkNotificationSettingRTPCallback();
  }
}
function checkNotificationSettingRTPCallback(){
  rtp_log("@@@ In onClickRTPBackArrowBtnSetting() @@@");
  rtp_log("gblAcceptBusiness :::"+gblAcceptBusiness);
  rtp_log("gblAcceptIndividual :::"+gblAcceptIndividual);
  rtp_log("gblConsentIndividualFlag :::"+gblConsentIndividualFlag);
  rtp_log("gblConsentBusinessFlag :::"+gblConsentBusinessFlag);
  showLoadingScreen(); 
  if((gblConsentIndividualFlag != gblAcceptIndividual) || (gblConsentBusinessFlag != gblAcceptBusiness)){
    updateRTPConsentFlag();
  }else{
    checkRTPConditionToDisplayScreen();
  }
}
function onClickRTPIconFromAccountSum(){
  getRTPConsentFlag();
}

function getRTPConsentFlag(){
  var inputParam = {};
  showLoadingScreen(); //V:load
  invokeServiceSecureAsync("getRTPConsents", inputParam, callBackGetRTPConsentsFlag);
}

function callBackGetRTPConsentsFlag(status,resulttable){
  try{
    rtp_log("@@@ In callBackGetRTPConsentsFlag() @@@");
    if(status == 400){
      if(resulttable["opstatus"] == 1 || resulttable["opstatus"] == "1"){
        kony.print("show info screen");
        dismissLoadingScreen();
        showPayAlert();
      }else if (resulttable["opstatus"] == 0) {
        rtp_log("@@@BPConsent:::"+resulttable["BPConsent"]);
        rtp_log("@@@CTConsent:::"+resulttable["CTConsent"]);
        if(resulttable["BPConsent"] == "Y")
          gblAcceptBusiness = true;
        else
          gblAcceptBusiness = false

        if(resulttable["CTConsent"] == "Y")  
            gblAcceptIndividual=true;
        else
          gblAcceptIndividual = false;

        gblConsentIndividualFlag = gblAcceptIndividual;
        gblConsentBusinessFlag = gblAcceptBusiness;
        checkRTPConditionToDisplayScreen();
      }else{
        dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        return false;
      }
    }else{
      dismissLoadingScreen();
      showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
      return false;
    }
  }catch(e){
    rtp_log("@@@ In callBackGetRTPConsentsFlag() Exception:::"+e);
  }
}
function checkNotificationSetting(){
   checkUserPushNotificationEnable(updateRTPConsentFlag);
}
function updateRTPConsentFlag(){
  rtp_log("@@@ In updateRTPConsentFlag() @@@");
  showLoadingScreen();
  var inputParam = {};
  rtp_log("updateRTPConsentFlag gblAcceptBusiness >>>"+gblAcceptBusiness);
  rtp_log("updateRTPConsentFlag gblAcceptIndividual >>>"+gblAcceptIndividual);

  if(gblAcceptIndividual) {
    inputParam["ctConsent"] = "Y";
  } else {
    inputParam["ctConsent"] = "N";
  }

  if(gblAcceptBusiness) {
    inputParam["bpConsent"] = "Y";
  } else {
    inputParam["bpConsent"] = "N";
  }
  rtp_log("@@@gblIsRTPStartUp:::"+gblIsRTPStartUp);

  if(gblIsRTPStartUp) {
    inputParam["rtpScreen"] = "RTPPopup"; 
  }else{
    inputParam["rtpScreen"] = "RTPList";
  }
  rtp_log("inputParam >>>"+JSON.stringify(inputParam));
  invokeServiceSecureAsync("updateRTPConsents", inputParam, callBackUpdateRTPConsentsFlag);
}

function callBackUpdateRTPConsentsFlag(status,resulttable){
  rtp_log("@@@ In callBackUpdateRTPConsentsFlag() @@@");
  if(status == 400){
    if(resulttable["opstatus"] == 0){
      rtp_log("@@@gblIsRTPStartUp:::"+gblIsRTPStartUp);
      gblConsentIndividualFlag = gblAcceptIndividual;
      gblConsentBusinessFlag = gblAcceptBusiness;
      
      if(gblIsRTPStartUp){
        goToAcctSummryFromRTPBell();
      }else{	
        checkRTPConditionToDisplayScreen();
      }
    }else{
      if(gblIsRTPStartUp){
        showAlertWithCallBack(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"), goToAcctSummryFromRTPBell);
        return false;
      }else{
        //If promptpay not registered
        if(resulttable["errCode"] == "TS8873" || resulttable["errMsg"] == "NOT REGISTERED" ){ 
          rtp_log("---- PromptPay not Registered. ----");
          dismissLoadingScreen();
          showAlertWithCallBack(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"), showNoActiveRTP);
          return false;
        }else{
          dismissLoadingScreen();
          showAlertWithCallBack(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"), invokeRTPDetails);
          return false;
        }
      }
    }
  }else{
    if(gblIsRTPStartUp){
      showAlertWithCallBack(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"), goToAcctSummryFromRTPBell);
      return false;
    }else{
      dismissLoadingScreen();
      showAlertWithCallBack(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info") ,invokeRTPDetails);
      return false;
    }
  } 
}

function showNoActiveRTP(){
  displayfrmMBRTPNoActiveList();
  rtp_log("@@@ In showNoActiveRTP() @@@");
  rtp_log("@@@gblConsentIndividualFlag:::"+gblConsentIndividualFlag);
  rtp_log("@@@gblConsentBusinessFlag:::"+gblConsentBusinessFlag);
  if(gblConsentIndividualFlag || gblConsentBusinessFlag){
    frmMBRTPNoActiveList.lblTitle.text = kony.i18n.getLocalizedString("MIB_NoActiveRTPDesc1_enable");
    frmMBRTPNoActiveList.lblDesc.text = kony.i18n.getLocalizedString("MIB_NoActiveRTPDesc2_enable");
  }else{
    frmMBRTPNoActiveList.lblTitle.text = kony.i18n.getLocalizedString("MIB_NoActiveRTPDesc1_disable");
  	frmMBRTPNoActiveList.lblDesc.text = kony.i18n.getLocalizedString("MIB_NoActiveRTPDesc2_disable");
  }
}

function goToAcctSummryFromRTPBell(){
  gblIsRTPStartUp = false;
  if(gblLogiflowOptimisation=="ON"){
    customerAccountCallUpdatedBack(gblnewLoginCompositeresult);
  }else{
    callCustomerAccountService("");
  }
}

function preShowFrmMBRTPNoActiveList(){
  	frmMBRTPNoActiveList.lblHeader.text = kony.i18n.getLocalizedString("MB_RTPTittle");
  	if(!gblAcceptIndividual && !gblAcceptBusiness){
    	frmMBRTPNoActiveList.lblTitle.text = kony.i18n.getLocalizedString("MIB_NoActiveRTPDesc1_disable");
      	frmMBRTPNoActiveList.lblDesc.text = kony.i18n.getLocalizedString("MIB_NoActiveRTPDesc2_disable");
    }else{
      	frmMBRTPNoActiveList.lblTitle.text = kony.i18n.getLocalizedString("MIB_NoActiveRTPDesc1_enable");
      	frmMBRTPNoActiveList.lblDesc.text = kony.i18n.getLocalizedString("MIB_NoActiveRTPDesc2_enable");
    }
  	frmMBRTPNoActiveList.lblSettingDesc1.text = kony.i18n.getLocalizedString("MB_RTPDisableMsg31");
  	frmMBRTPNoActiveList.lblSettingDesc2.text = kony.i18n.getLocalizedString("MB_RTPDisableMsg32");
  	frmMBRTPNoActiveList.lblSettingDesc3.text = kony.i18n.getLocalizedString("MB_RTPDisableMsg33");
  	frmMBRTPNoActiveList.btnGoToAcctSum.text = kony.i18n.getLocalizedString("Result_urlTxn14");
}

function preShowFrmMBRTPStartUp(){
  	frmMBRTPStartUp.lblTitle.text = kony.i18n.getLocalizedString("MB_RTPIntrMsg1");
  	frmMBRTPStartUp.lblNoteText.text = kony.i18n.getLocalizedString("MB_RTPIntrMsg2");
  	frmMBRTPStartUp.lblAcceptFriend.text = kony.i18n.getLocalizedString("MB_RTPIntrConCheck");
  	frmMBRTPStartUp.lblAcceptBusiness.text = kony.i18n.getLocalizedString("MB_RTPIntrCorCheck");
  	frmMBRTPStartUp.btnGetStarted.text = kony.i18n.getLocalizedString("keyOK");
  	if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPhone Simulator"){
      setRTPBellSound();
    }
}

function onClickRTPStartUpCheckBoxIndividual(){
  	if(frmMBRTPStartUp.imgFriend.src == "radio_uncheck.png"){
      	frmMBRTPStartUp.imgFriend.src = "radio_check.png";
      	gblAcceptIndividual = true;
    }else{
      	frmMBRTPStartUp.imgFriend.src = "radio_uncheck.png";
      	gblAcceptIndividual = false;
    }
}

function onClickRTPStartUpCheckBoxBusiness(){
 	if(frmMBRTPStartUp.imgBusiness.src == "radio_uncheck.png"){
      	frmMBRTPStartUp.imgBusiness.src = "radio_check.png";
      	gblAcceptBusiness = true;
    }else{
      	frmMBRTPStartUp.imgBusiness.src = "radio_uncheck.png";
      	gblAcceptBusiness = false;
    }
}

function startUpRTPDisplay(){
  	displayRTPAnnoucement = false;
  	gblIsRTPStartUp = true;
  	frmMBRTPStartUp.imgBusiness.src = "radio_check.png";
    gblAcceptBusiness = true;
    frmMBRTPStartUp.imgFriend.src = "radio_check.png";
    gblAcceptIndividual = true;
    createMediaObjForBellRing();
}

function createMediaObjForBellRing(){
  try{
    rtp_log("@@@ In createMediaObjForBellRing() 123@@@");
    var filePath = "";
    if(gblDeviceInfo["name"] == "android"){
      filePath = kony.io.FileSystem.getDataDirectoryPath() + "/doorbell_ring_freetone.mp3";
      rtp_log("*** copyBundledRawFileTo API overrides the destination file with new one***");
      kony.io.FileSystem.copyBundledRawFileTo("doorbell_ring_freetone.mp3", filePath);
    }else{
      filePath = kony.io.FileSystem.getApplicationDirectoryPath() + "/doorbell_ring_freetone.mp3";
    }
    rtp_log("@@@ filePath:::"+filePath);
    var fileObj = new kony.io.File(filePath);
	gblMediaObj =  kony.media.createFromFile(fileObj);
    if(gblMediaObj !== null){
      frmMBRTPStartUp.show();
      kony.store.setItem("RTPAnnounceFlag", "Y");
    }
  }catch(exp){
    rtp_log("@@@ In createMediaObjForBellRing() Exception:::"+exp);
  }
}

function setRTPBellSound(){
  if(gblMediaObj != null){
    rtp_log("-----Playing Bell Sound-------");
    gblMediaObj.play(1);
  }
}

function setRTPBellSoundforAndroid(){
  if(gblMediaObj != null && gblDeviceInfo["name"] == "android"){
    rtp_log("-----Playing Bell Sound---Android----");
    gblMediaObj.play(1);
  }
}

function onClickRTPBillerSettingsCategory(){
  	gblIsRTPBillersCategory = true;
  	callBillerTopUpCategoryService();
}

function isRTPEnable(){
  rtp_log("isRTPCustomer>>>>>>>>>>>>>>>>"+isRTPCustomer);
  rtp_log("GLOBAL_RTP_ENABLE>>>>>>>>>>>>>>>>"+GLOBAL_RTP_ENABLE);
  if(GLOBAL_RTP_ENABLE == "ON" && isRTPCustomer) {
    return true;
  }else{
    return false;
  }
}

