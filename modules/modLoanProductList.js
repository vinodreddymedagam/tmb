//Type your code here
gblSelectedLoanProduct = "";

function showLoanLandingScreen() {
    frmGetNewLoan.show();
}

function showLoanProductListScreen() {
    frmLoanProductList.show();
}

function showCreditCardProducts() {
    setDatatoLoansProductList();
    frmLoanProductList.lblHeader.text = kony.i18n.getLocalizedString("Loan_CCTitle");
  	frmLoanProductList.show();
}

function showPersonalLoanProducts() {
    setDatatoLoansProductList();
    frmLoanProductList.lblHeader.text = kony.i18n.getLocalizedString("Loan_LTitle");
	frmLoanProductList.show();
}

function frmLoanProductListPreshow() {}

function frmLoanProductListPostshow() {}

function frmLoanProductListInit() {
    frmLoanProductList.preShow = frmLoanProductListPreshow;
    frmLoanProductList.postShow = frmLoanProductListPostshow;
    frmLoanProductList.onDeviceBack = doNothing;
    frmLoanProductList.btnBack.onClick = showLoanLandingScreen;
}

function setDatatoLoansProductList() {
    frmLoanProductList.segCreditCard.removeAll();
    var segData = []
    var resultLoanProducts = gblLoanApplicationList;
    var btnStatusText = "";
    var btnStatusSkin = btnLoanCCBlue;
    var lblActiveIsVisible = false;
    var ImgActiveIsVisible = false;
    var imgProductSrc = "";
  	var imgProductSrcToDisplay = "";
	var lblProductDescription = "";
    var loanProductName = "";
  	var lblProductDescWidth = "98%";
  	var btnStatusHeight = "35dp";
  	var flexActiveWidth = "30%"
    for (var i = 0; i < resultLoanProducts.length; i++) {
        // This if else is to check the status of the loan and depends on that we will show button text(apply,continue....)
      	var lblProductDescWidth = "98%";
        var btnStatusHeight = "35dp";
        var flexActiveWidth = "30%"
        if (resultLoanProducts[i]["currentLoanStatus"] == 1) {
            lblActiveIsVisible = false;
            btnStatusText = kony.i18n.getLocalizedString("Loan_Continue");
            btnStatusSkin =btnLoanCCBlueBGWhieFont ; //btnLoanCCBlue
        } else if (resultLoanProducts[i]["currentLoanStatus"] == 2) {
            lblActiveIsVisible = false;
            btnStatusText = kony.i18n.getLocalizedString("Loan_Apply");
            btnStatusSkin = btnLoanCCBlueBGWhieFont;
        } else if (resultLoanProducts[i]["currentLoanStatus"] == 3) {
          	lblActiveIsVisible = false;
            btnStatusText = kony.i18n.getLocalizedString("Loan_CheckStatus");
            btnStatusSkin = btnLoanCCBlueBGWhieFont;
            kony.print("device type in loans-->"+isIphone5Dimensions())
            if(isIphone5Dimensions() && kony.i18n.getCurrentLocale() != "th_TH" ){ // This condition is to fix MIB-13957. it applies for iphone 5,5s and SE
                btnStatusHeight = "50dp"
            }
        } else if (resultLoanProducts[i]["currentLoanStatus"] == 4) {
            btnStatusText = kony.i18n.getLocalizedString("ActiveStatus");
            btnStatusSkin = btnLoanCCBlueBGWhieFont;
          	lblProductDescWidth = "66%";
            lblActiveIsVisible = true;
           if(isIphone5Dimensions()){ // This condition is to fix MIB-13957. it applies for iphone 5,5s and SE
                lblProductDescWidth = "63%"
                flexActiveWidth = "37%"
            }
        }else if (resultLoanProducts[i]["currentLoanStatus"] == 5) {
          	lblActiveIsVisible = false;
            btnStatusText = kony.i18n.getLocalizedString("UploadDoc_Btn");
            btnStatusSkin = btnLoanCCBlueBGWhieFont;
        }
        if (resultLoanProducts[i]["productCode"] == "VM") {
            imgProductSrc = "sosmart.png";
          	imgProductSrcToDisplay = "sosmarthalf.png"
			lblProductDescription = kony.i18n.getLocalizedString("TMB_So_Smart_Description");
        } else if (resultLoanProducts[i]["productCode"] == "VP") {
            imgProductSrc = "sofast.png";
          	imgProductSrcToDisplay = "sofasthalf.png"
			lblProductDescription = kony.i18n.getLocalizedString("TMB_So_Fast_Description");
        } else if (resultLoanProducts[i]["productCode"] == "VT") {
            imgProductSrc = "topbrass.png";
          	imgProductSrcToDisplay = "topbrasshalf.png"
			lblProductDescription = kony.i18n.getLocalizedString("TMB_Royal_Top_Brass_Description");
        } else if (resultLoanProducts[i]["productCode"] == "MS") {
            imgProductSrc = "sochill.png";
          	imgProductSrcToDisplay = "sochillhalf.png"
			lblProductDescription = kony.i18n.getLocalizedString("TMB_So_Chill_Description");
        }else if (resultLoanProducts[i]["productCode"] == "RC01") {
            imgProductSrc = "ready.png";
          	imgProductSrcToDisplay = "readyhalf.png"
			lblProductDescription = kony.i18n.getLocalizedString("TMB_Ready_Cash_Description");
        }else if (resultLoanProducts[i]["productCode"] == "C2G01") {
            imgProductSrc = "cashtogo.png";
          	imgProductSrcToDisplay = "cashtogohalf.png"
			lblProductDescription = kony.i18n.getLocalizedString("TMB_Cash2Go_Description");
        }
        if (kony.i18n.getCurrentLocale() == "th_TH") {
            loanProductName = resultLoanProducts[i]["loanProductNameTH"];
        } else {
            loanProductName = resultLoanProducts[i]["loanProductNameEN"];
        }
        var aaaaa = "btnStatus"+resultLoanProducts[i]["productCode"];
        frmLoanProductList.segCreditCard.widgetDataMap = {

                  "lblProductName": "lblProductName",

                  "lblProductDesc": "lblProductDesc",

                  "ImageCrediCard": "ImageCrediCard",

                  "btnStatus": "btnStatus",

                  "lblActive": "lblActive",
                  "ImgActive":"ImgActive",
          			"flexCardDesc":"flexCardDesc",
                  "ImageCrediCardToDisplay" : "ImageCrediCardToDisplay",
          		"flexActive" : "flexActive"

                  //"btnPayBillComplete": "btnPayBillComplete"

                };
        rowData = {
            lblProductName: loanProductName,
            lblProductDesc: {
                text: lblProductDescription
            },
          	flexCardDesc: {
                width: lblProductDescWidth
            },
            flexActive: {
                width: flexActiveWidth
            },
            ImageCrediCard: {
                src: imgProductSrc,
              	isVisible: false
            },
            ImageCrediCardToDisplay: {
                src: imgProductSrcToDisplay
            },
            btnStatus: {
				id : "btnStatus"+resultLoanProducts[i]["productCode"],
                text: btnStatusText,
                skin: btnStatusSkin,
                height : btnStatusHeight,
              	focusSkin: btnLoanCCBlue,
                onClick: onClickProductStatus,
                currentLoanStatus: resultLoanProducts[i]["currentLoanStatus"], // To differentianate what is the status(active or apply or ..)
                productCode: resultLoanProducts[i]["productCode"], // To get the product detail to show product detail screen.
                isVisible: !(lblActiveIsVisible)
            },
            lblActive: {
                text: kony.i18n.getLocalizedString("ActiveStatus"),
                isVisible: lblActiveIsVisible
            },
            ImgActive: {
                src: "completeico.png",
                isVisible: lblActiveIsVisible
            }
        };
        segData.push(rowData);
    }
    frmLoanProductList.segCreditCard.setData(segData);
}


function onClickProductStatus(event,rowClicked) {
	var code = gblLoanApplicationList[rowClicked["rowIndex"]]["productCode"];
  	gblCaId = gblLoanApplicationList[rowClicked["rowIndex"]]["caId"];
  	gblLoanAppRefNo = gblLoanApplicationList[rowClicked["rowIndex"]]["appRefNo"];
  	defineFacilityGlobals(); // Define global variable for facility information page
  	if(typeof(gblCaId) == "undefined"){
      kony.print("gblCaId =" + gblCaId);
      kony.print("gblCaId type =" + typeof(gblCaId));
      gblCaId = "";
    }
  	if(typeof(gblLoanAppRefNo) == "undefined"){
      kony.print("gblLoanAppRefNo =" + gblLoanAppRefNo);
      kony.print("gblLoanAppRefNo type =" + typeof(gblLoanAppRefNo));
      gblLoanAppRefNo = "";
    }
    gblSelectedLoanProduct = "";
    gblSelectedLoanProduct = code;
  	kony.print("@@@LoanProductButton:::"+event["text"]);
  	if(code == "VT"){
      openCallMeNowPage("CC",true);
      return false;
    }
    if (event["text"] == kony.i18n.getLocalizedString("Loan_Apply")) {
        setDatatoProductDetail(code);
    }
  	if(event["text"] == kony.i18n.getLocalizedString("Loan_CheckStatus")){
      kony.print("---Navigating to Loan Status----");
      checkLoanStatus();
    }
  	if(event["text"] == kony.i18n.getLocalizedString("UploadDoc_Btn")){
      kony.print("---Navigating to Upload Document----");
      showUploadDcumentPage(true);
    }
    if (event["text"] == kony.i18n.getLocalizedString("Loan_Continue")) {
      showfrmLoanApplication();
    }
    if (event["status"] == "0") {
        // Go to product screen
    }
    if (event["status"] == "0") {
        // Go to product screen
    }
    frmFacilityInformation.destroy();
}


function setDatatoPersonalProductList() {
    frmLoanProductList.segPersonalLoan.removeAll();
    var segData = []
    for (var i = 0; i < 12; i++) {
        var btnStatusText = "";
        var btnStatusSkin = btnLoanCCBlue;
        var lblActiveIsVisible = false;
        var ImgActiveIsVisible = false;
        var imgProductSrc = ""
        rowData = {
            lblProductName: "So Good TMB",
            lblProductDesc: "Avail maximum discount at all bangkok stores",
            imgProduct: {
                src: imgProductSrc
            },
            btnStatus: {
                text: btnStatusText,
                skin: btnStatusSkin,
                onClick: onClickProductStatus,
                status: "0", // To differentianate what is the status(active or apply or ..)
                ID: "tmbsochill" // To get the product detail to show product detail screen.
            },
            lblActive: {
                text: kony.i18n.getLocalizedString("ActiveStatus"),
                isVisible: lblActiveIsVisible
            },
            ImgActive: {
                src: "",
                isVisible: lblActiveIsVisible
            }
        };
        segData.push(rowData);
    }
    frmLoanProductList.segPersonalLoan.setData(segData);
}