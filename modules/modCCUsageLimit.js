gblMinresultval = null;
gblMaxresultval = null;
gblSliderUsagelimitVal = null;

function navigateToCreditUsageLimt(){
  //frmMBCCUsageLimit.show();
  invokeCreditCardDetailsService();
}

function invokeCreditCardDetailsService(){
    var inputParam = {};
	showLoadingScreen();
	kony.print("invokeCreditCardDetailsService >>");
  	kony.print("gblCACardRefNumber >> "+gblCACardRefNumber);
  	inputParam["cardRefId"] = gblCACardRefNumber;
  
  	invokeServiceSecureAsync("creditCardLimitSettingInq", inputParam, callBackInvokeCreditCardDetailsService);
}

function callBackInvokeCreditCardDetailsService(status, resulttable){
  try{
  	if (status == 400) {
        kony.print("Printing the result for callBackInvokeCreditCardDetailsService : "+JSON.stringify(resulttable));
        if (resulttable.opstatus == 0 ||resulttable.opstatus == "0") {
           dismissLoadingScreen();
            if(resulttable["changeLimtReq"]=="Y"){
               var msg=getLocalizedString("CC_msgNotValid");
                msg=msg.replace("{expiry_date}", resulttable["expireDt"]);
                msg=msg.replace("{old_limit}", commaFormattedTransfer(resulttable["oldLimit"]));
				showAlert(msg, getLocalizedString("info"));
            }else{
               setCCUsageLimit(resulttable);
              frmMBCCUsageLimit.show();
            }
          
        } else if (resulttable.opstatus == -1 || resulttable.opstatus == "-1") {
            if(resulttable.errCode == "buzError"){
              	dismissLoadingScreen();
              	 var CA_StartTime = resulttable["StartTime"];
			 	 var CA_EndTime = resulttable["EndTime"];
             	 var errorMsg = kony.i18n.getLocalizedString("Card_Err_ServiceHour"); 
				 errorMsg = errorMsg.replace("{service_hours}", CA_StartTime + " - " + CA_EndTime);
                 showAlert(errorMsg, kony.i18n.getLocalizedString("info"));
                 return false;
            }  
        }else{
          	dismissLoadingScreen();
        	showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        }
    }

  }catch(e){
    kony.print("Exception in callBackInvokeCreditCardDetailsService:"+e);
  }
}
function setCCUsageLimit(resulttable){
  var lblMinlblVal = parseFloat(resulttable["minLimit"]);
  gblMinresultval = Math.ceil(lblMinlblVal);
  var lblMaxlblVal = parseInt(resulttable["maxLimit"]);
  gblMaxresultval = lblMaxlblVal;
  var lblUsageLimitlblVal = parseInt(resulttable["usageLimit"]);
  gblSliderUsagelimitVal = lblUsageLimitlblVal;
  var lblAvailToSpendlblVal = parseInt(resulttable["availToSpend"]);
  var num = gblMinresultval;
  var floor=Math.floor(num/1000) *1000;
  var roundedlblMinlblVal = floor;
  frmMBCCUsageLimit.lblCardLimitVal.text = commaFormattedTransfer(lblMaxlblVal+"")+ " " + kony.i18n.getLocalizedString("currencyThaiBaht");
  frmMBCCUsageLimit.lblUsageLimitVal.text = commaFormattedTransfer(lblUsageLimitlblVal+"")+ " " + kony.i18n.getLocalizedString("currencyThaiBaht");
  frmMBCCUsageLimit.lblAvailToSpendVal.text =  commaFormattedTransfer(lblAvailToSpendlblVal+"")+ " " + kony.i18n.getLocalizedString("currencyThaiBaht");
  frmMBCCUsageLimit.lblPrevAmount.text = frmMBCCUsageLimit.lblAvailToSpendVal.text;
  frmMBCCUsageLimit.lblMinVal.text = commaFormattedTransfer(gblMinresultval+"") +"";
  frmMBCCUsageLimit.lblMaxVal.text = commaFormattedTransfer(lblMaxlblVal+"")+"";
  frmMBCCUsageLimit.sliderCardLimit.step = 1000;
  frmMBCCUsageLimit.sliderCardLimit.selectedValue = gblSliderUsagelimitVal;//parseFloat("45000");
  frmMBCCUsageLimit.sliderCardLimit.min = roundedlblMinlblVal;
  frmMBCCUsageLimit.sliderCardLimit.max = parseInt(resulttable["maxLimit"]);
  
  frmMBCCUsageLimit.lblPrevUsageLimitVal.text = frmMBCCUsageLimit.lblUsageLimitVal.text;
  frmMBCCUsageLimit.lblLimitVal.text = commaFormattedTransfer(lblUsageLimitlblVal+"");
  if(frmMBCCUsageLimit.sliderCardLimit.selectedValue == gblMinresultval){
        frmMBCCUsageLimit.sliderCardLimit.selectedValue = roundedlblMinlblVal;
  }
  //alert("frmMBCCUsageLimit.lblPrevUsageLimitVal.text : "+frmMBCCUsageLimit.lblPrevUsageLimitVal.text);
  
//    kony.print("minin" + commaFormattedTransfer(lblMinlblVal) + "a" );
//    kony.print("maxax" + commaFormattedTransfer(lblMaxlblVal) + "b" );
 
}

function initMBCCUsageLimit(){
  //frmMBCCUsageLimit.btnBack.onClick = navigateBackToCardManageCard;
  frmMBCCUsageLimit.btnBack.onClick = handleMenuBtn;
  frmMBCCUsageLimit.onDeviceBack = onDeviceSpaback;
  frmMBCCUsageLimit.btnCancel.onClick = navigateCancelToCardManageCard;
  
  frmMBCCUsageLimit.btnConfirm.onClick = navigateConfirmToCardManageCard;
  
  frmMBCCUsageLimit.preShow = preShowOfCCUsageLimit;
  
  frmMBCCUsageLimit.postShow = postShowOfCCUsageLimit;
  frmMBCCUsageLimit.sliderCardLimit.onSlide = onSlideEventCreditLimit;
  
  frmMBCCUsageLimit.btnMinus.onClick = onClickSliderMinusCC;
  frmMBCCUsageLimit.btnPlus.onClick = onClickSliderPlusCC;
}

function onClickSliderMinusCC(sliderVal){
  try{
    kony.print("@@@ In onClickSliderMinusCC @@@");
    var lblMinlblVal = gblMinresultval;
    var num = lblMinlblVal;
    var floor=Math.floor(num/1000) *1000;
    var roundedlblMinlblVal = floor;
    var currentAmt = frmMBCCUsageLimit.lblLimitVal.text;
    var minAmount = roundedlblMinlblVal;
    if(isNotBlank(currentAmt)){
      currentAmt = parseFloat(removeCommos(currentAmt));
      minAmount = parseFloat(removeCommos(minAmount));
      if(currentAmt <= minAmount || currentAmt <= lblMinlblVal){
        frmMBCCUsageLimit.lblLimitVal.text = commaFormattedTransfer(lblMinlblVal + ""); //commaFormattedTransfer(selVal+"");
        frmMBCCUsageLimit.btnMinus.skin = "sknBtnMinusWhite";
        frmMBCCUsageLimit.btnMinus.setEnabled(false);
        
      }else{
        currentAmt = currentAmt - 1000;
      	kony.print("sliderVal : "+sliderVal);
        if("X" !== sliderVal){
        	kony.print("test point 1: ");	
          frmMBCCUsageLimit.sliderCardLimit.selectedValue = currentAmt;  
        }
        kony.print("@@@currentAmt:::"+currentAmt+" , minAmount : "+minAmount);
        frmMBCCUsageLimit.lblLimitVal.text = commaFormattedTransfer(currentAmt+"");
        frmMBCCUsageLimit.btnMinus.skin = "sknBtnMinusBlue";
        frmMBCCUsageLimit.btnMinus.setEnabled(true);
       
      }
       frmMBCCUsageLimit.btnPlus.skin = "sknBtnPlusBlue";
	   frmMBCCUsageLimit.btnPlus.setEnabled(true);
      onSlideMangeCreditLimit();
    }
  }catch(e){
    kony.print("@@@ In onClickSliderMinus() Exception:::"+e);
  }
}

function onClickSliderPlusCC(sliderVal){
  try{
    kony.print("@@@ In onClickSliderPlusCC @@@");
    var currentAmt = frmMBCCUsageLimit.sliderCardLimit.selectedValue;
    var maxAmount = frmMBCCUsageLimit.lblMaxVal.text;
    var lblMinlblVal = gblMinresultval;
    var num = lblMinlblVal;
    var floor=Math.floor(num/1000) *1000;
    var roundedlblMinlblVal = floor;
    var minAmount = roundedlblMinlblVal;
    kony.print("@@CurrentAmt:::"+currentAmt+"||@@MaxAmt:::"+maxAmount);
    if(isNotBlank(currentAmt)){
      currentAmt = parseFloat(removeCommos(currentAmt));
      maxAmount = parseFloat(removeCommos(maxAmount));
      
      if(currentAmt < maxAmount){
        currentAmt = currentAmt + 1000;
       kony.print("sliderVal : "+sliderVal);
        if("X" !== sliderVal){
          	kony.print("Test point 2");
        	frmMBCCUsageLimit.sliderCardLimit.selectedValue = currentAmt;  
        }
        if(currentAmt > maxAmount){
          currentAmt = maxAmount;
        }
      	frmMBCCUsageLimit.lblLimitVal.text = commaFormattedTransfer(currentAmt+"");
        frmMBCCUsageLimit.btnPlus.skin = "sknBtnPlusBlue";
        frmMBCCUsageLimit.btnPlus.setEnabled(true);
      }else{
        frmMBCCUsageLimit.btnPlus.skin = "sknBtnPlusWhite";
        frmMBCCUsageLimit.btnPlus.setEnabled(false);
      }
      frmMBCCUsageLimit.btnMinus.skin = "sknBtnMinusBlue";
		frmMBCCUsageLimit.btnMinus.setEnabled(true);
      kony.print("@@CurrentAmt:::"+currentAmt+"||@@MaxAmt:::"+maxAmount);
      onSlideMangeCreditLimit();
    }
  }catch(e){
   kony.print("@@@ In onClickSliderPlusCC() Exception:::"+e);
  }
}

function onSlideEventCreditLimit(){
  var newUsageAmt = frmMBCCUsageLimit.sliderCardLimit.selectedValue;
  var oldUsageAmt = frmMBCCUsageLimit.lblCardLimitVal.text;
  //var oldUsageAmtVal = getAmountValueFromAmtWithBaht(frmMBCCUsageLimit.lblUsageLimitVal.text);
  var oldUsageAmtVal = getAmountValueFromAmtWithBaht(frmMBCCUsageLimit.lblPrevUsageLimitVal.text);
  
  var lblMinlblVal = gblMinresultval;
    var num = lblMinlblVal;
    var floor=Math.floor(num/1000) *1000;
    var roundedlblMinlblVal = floor;
    var currentAmt = frmMBCCUsageLimit.lblLimitVal.text;
    var minAmount = roundedlblMinlblVal;
  
   var maxAmount = frmMBCCUsageLimit.lblMaxVal.text;
   
  
     
  kony.print("newUsageAmt : "+newUsageAmt+" , oldUsageAmt  : "+oldUsageAmt+" , oldUsageAmtVal : "+oldUsageAmtVal);
  if(parseFloat(removeCommos(oldUsageAmt)) < parseFloat(removeCommos(newUsageAmt))){
    onClickSliderPlusCC("X");
  }else{
    onClickSliderMinusCC("X");
  }
  
  maxAmount = parseFloat(removeCommos(maxAmount));
  minAmount = parseInt(removeCommos(minAmount));
  
  if(newUsageAmt == minAmount){
     frmMBCCUsageLimit.lblLimitVal.text = commaFormattedTransfer(lblMinlblVal + "");
     frmMBCCUsageLimit.btnMinus.skin = "sknBtnMinusWhite";
     frmMBCCUsageLimit.btnMinus.setEnabled(false);
  }else{
     frmMBCCUsageLimit.btnMinus.skin = "sknBtnMinusBlue";
     frmMBCCUsageLimit.btnMinus.setEnabled(true);
    }
  
  if(newUsageAmt == maxAmount){
      frmMBCCUsageLimit.btnPlus.skin = "sknBtnPlusWhite";
        frmMBCCUsageLimit.btnPlus.setEnabled(false);
  }
  kony.print("newUsageAmt : "+newUsageAmt+ "minAmount : "+minAmount+ "maxAmount : "+maxAmount);
}

function onSlideMangeCreditLimit(){
  try{
    var lblMinlblVal = gblMinresultval;
    var num = lblMinlblVal;
    var floor=Math.floor(num/1000) *1000;
    var roundedlblMinlblVal = floor;
    var currentAmt = frmMBCCUsageLimit.lblLimitVal.text;
    var minAmount = roundedlblMinlblVal;
    var newUsageAmt = frmMBCCUsageLimit.sliderCardLimit.selectedValue;
    kony.print("newUsageAmt : "+newUsageAmt);
    if(newUsageAmt == minAmount){
       frmMBCCUsageLimit.lblLimitVal.text = commaFormattedTransfer(lblMinlblVal+"");
    
       frmMBCCUsageLimit.lblUsageLimitVal.text = commaFormattedTransfer(lblMinlblVal+"")+" "+getLocalizedString("currencyThaiBaht");
      
    }else{
       frmMBCCUsageLimit.lblLimitVal.text = commaFormattedTransfer(frmMBCCUsageLimit.sliderCardLimit.selectedValue+"");
    
       frmMBCCUsageLimit.lblUsageLimitVal.text = commaFormattedTransfer(newUsageAmt+"")+" "+getLocalizedString("currencyThaiBaht");
    }
    
    //Diff = new Usage Limit – Current Usage Limit
	//Usage Limit (display as current amount flag)
	//Value of Available to Spend = Available to Spend + Diff
    
    //var currUsageLimtAmt = getAmountValueFromAmtWithBaht(frmMBCCUsageLimit.lblUsageLimitVal.text);
    var currUsageLimtAmt = getAmountValueFromAmtWithBaht(frmMBCCUsageLimit.lblPrevUsageLimitVal.text);
    kony.print("currUsageLimtAmt : "+currUsageLimtAmt);
    
    var diff = newUsageAmt - currUsageLimtAmt;
    kony.print("diff : "+diff);
	var currentAvalToSpend = getAmountValueFromAmtWithBaht(frmMBCCUsageLimit.lblPrevAmount.text) + diff;
    kony.print("currentAvalToSpend : "+currentAvalToSpend);
   
    frmMBCCUsageLimit.lblAvailToSpendVal.text = commaFormattedTransfer(currentAvalToSpend+"")+" "+getLocalizedString("currencyThaiBaht");
        var lablelimitval = parseInt(removeCommos(frmMBCCUsageLimit.lblLimitVal.text));
        var availabletospend = parseFloat(frmMBCCUsageLimit.lblAvailToSpendVal.text);
    
    if (lablelimitval < gblMinresultval) {
            frmMBCCUsageLimit.lblLimitVal.text = commaFormattedTransfer(gblMinresultval+"");
        } else if (lablelimitval > gblMaxresultval) {
            frmMBCCUsageLimit.lblLimitVal.text = commaFormattedTransfer(gblMaxresultval+"");
        }
    
        if (frmMBCCUsageLimit.lblLimitVal.text == commaFormattedTransfer(lblMinlblVal + "")) {
             var differ = lablelimitval - currUsageLimtAmt;
	         availabletospend = getAmountValueFromAmtWithBaht(frmMBCCUsageLimit.lblPrevAmount.text) + differ;
             frmMBCCUsageLimit.lblAvailToSpendVal.text = commaFormattedTransfer(availabletospend+"")+" "+getLocalizedString("currencyThaiBaht");
        }
     var lbllimitval = parseInt(removeCommos(frmMBCCUsageLimit.lblLimitVal.text));
        if (lbllimitval == gblMinresultval) {
             frmMBCCUsageLimit.btnMinus.skin = "sknBtnMinusWhite";
             frmMBCCUsageLimit.btnMinus.setEnabled(false);
        }else{
             frmMBCCUsageLimit.btnMinus.skin = "sknBtnMinusBlue";
             frmMBCCUsageLimit.btnMinus.setEnabled(true);
        } 
        if(lbllimitval == gblMaxresultval){
            frmMBCCUsageLimit.btnPlus.skin = "sknBtnPlusWhite";
            frmMBCCUsageLimit.btnPlus.setEnabled(false);
        }
   frmMBCCUsageLimit.lblUsageLimitVal.text = commaFormattedTransfer(lbllimitval + "") + " " + getLocalizedString("currencyThaiBaht");
  }catch(e){
    kony.print("@@@ In onSlideMangeCreditLimit() Exception:::"+e);
  }  
}

function getAmountValueFromAmtWithBaht(amtWithbath){
  var cardLimtAmtTemp = amtWithbath;
    if(isNotBlank(cardLimtAmtTemp)){
      if(cardLimtAmtTemp.indexOf(" ")){
      	var spaceIndex = cardLimtAmtTemp.indexOf(" ");
  		//kony.print(str.substr(0,spaceIndex));
        var temp = cardLimtAmtTemp.substr(0,spaceIndex);
  		kony.print("Amt with format : "+temp);
        var actualAmtVal = removeCommos(temp);
  		kony.print("actualAmtVal : "+actualAmtVal);
        return actualAmtVal;
    	}
    }
  return amtWithbath;
}

function navigateBackToCardManageCard(){
  kony.print("Click on Back button");
  //frmMBManageCard.show();
}
  
function navigateCancelToCardManageCard(){
  kony.print("Click on Cancel button");
  frmMBManageCard.show();
}

function navigateConfirmToCardManageCard(){
  //navigateToCCUsageLimtComplate();
  try {
   //var usageLimitText = getAmountValueFromAmtWithBaht(frmMBCCUsageLimit.lblUsageLimitVal.text);
  var usageLimitText = getAmountValueFromAmtWithBaht(frmMBCCUsageLimit.lblPrevUsageLimitVal.text);
  var availToSpendText =  getAmountValueFromAmtWithBaht(frmMBCCUsageLimit.lblAvailToSpendVal.text);
  var availToSpend=parseInt(availToSpendText); 
  var currentUsagelimit = parseInt(removeCommos(frmMBCCUsageLimit.lblUsageLimitVal.text));
   if(parseInt(usageLimitText) == currentUsagelimit){
        showAlert(getLocalizedString("CUL_msgInvalidNoChange"), getLocalizedString("info"));
      } else if(availToSpend<0){
         showAlert(getLocalizedString("msgNegavailToSpend"), getLocalizedString("info"));
   } else{
		
		var topValCard = "-40%";
         frmMBCCUsageLimit.flxBody.animate(kony.ui.createAnimation({
        "100": {
            "top": topValCard,
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            }
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.5
    });
        
//         frmMBCCUsageLimit.flxBody.top = "-40%"  
       showAccessPinScreenKeypad();
    }   
  }
  catch(e){
    kony.print("@@@ In onSlideMangeCreditLimit() Exception:::"+e);
   }
}

function preShowOfCCUsageLimit(){
  
  var lblMinlblVal = gblMinresultval;
    var num = lblMinlblVal;
    var floor=Math.floor(num/1000) *1000;
    var roundedlblMinlblVal = floor;
    var currentAmt = frmMBCCUsageLimit.lblLimitVal.text;
    var minAmount = roundedlblMinlblVal;
  
  var newUsageAmt = parseInt(frmMBCCUsageLimit.sliderCardLimit.selectedValue);
  var maxAmount =parseInt( frmMBCCUsageLimit.sliderCardLimit.max);
    
  
  kony.print("newUsageAmt "+newUsageAmt);
  kony.print("maxAmount "+maxAmount);
  kony.print("minAmount "+minAmount);
  
  kony.print("preShowOfCCUsageLimit start");
  frmMBCCUsageLimit.flxSliderMain.showFadingEdges = false;
  frmMBCCUsageLimit.flxBody.top = "0%";
  frmMBCCUsageLimit.imgCard.src = frmMBManageCard.imgCard.src;
  frmMBCCUsageLimit.lblCardNumber.text = frmMBManageCard.lblCardNumber.text;
  frmMBCCUsageLimit.lblCardAccountName.text = frmMBManageCard.lblCardAccountName.text;
  
  kony.print("#### Title : "+getLocalizedString("CUL01_txtTitle"));
  frmMBCCUsageLimit.lblManageCardTitle.text = getLocalizedString("CUL01_txtTitle");
  frmMBCCUsageLimit.btnCancel.text = getLocalizedString("CUL01_btnCancel");
  frmMBCCUsageLimit.btnConfirm.text = getLocalizedString("CUL01_btnConfirm");
  frmMBCCUsageLimit.lblCashWDLimit.text = getLocalizedString("CUL01_txtDesc");
  frmMBCCUsageLimit.lblMinText.text = getLocalizedString("CUL01_txtMin");
  frmMBCCUsageLimit.lblMaxText.text = getLocalizedString("CUL01_txtMax");
  
  frmMBCCUsageLimit.sliderCardLimit.thumbOffset = 0;
  
//   frmMBCCUsageLimit.lblMinVal.text = commaFormattedTransfer("5000");
//   frmMBCCUsageLimit.lblMaxVal.text = commaFormattedTransfer("100000");
  
  frmMBCCUsageLimit.lblCardLimit.text = getLocalizedString("CUL01_txtCardLimit");
  frmMBCCUsageLimit.lblUsageLimit.text = getLocalizedString("CUL01_txtUsageLimit");
  frmMBCCUsageLimit.lblAvailToSpend.text = getLocalizedString("CUL01_txtAvail");
  
 // frmMBCCUsageLimit.lblCardLimitVal.text = "100,000.00"+ " " + kony.i18n.getLocalizedString("currencyThaiBaht");
//  frmMBCCUsageLimit.lblUsageLimitVal.text = "45,000.00"+ " " + kony.i18n.getLocalizedString("currencyThaiBaht");
  //frmMBCCUsageLimit.lblAvailToSpendVal.text = "35,000.00"+ " " + kony.i18n.getLocalizedString("currencyThaiBaht");
  //frmMBCCUsageLimit.lblPrevAmount.text = frmMBCCUsageLimit.lblAvailToSpendVal.text;
  
//   frmMBCCUsageLimit.btnMinus.skin = "sknBtnMinusBlue";
//   frmMBCCUsageLimit.btnMinus.setEnabled(true);
//   frmMBCCUsageLimit.btnPlus.skin = "sknBtnPlusBlue";
//   frmMBCCUsageLimit.btnPlus.setEnabled(true);
  try{
       if(newUsageAmt == minAmount){
      kony.print("minAmount equal");
         frmMBCCUsageLimit.lblLimitVal.text = commaFormattedTransfer(lblMinlblVal + "");
       frmMBCCUsageLimit.btnMinus.skin = "sknBtnMinusWhite";
       frmMBCCUsageLimit.btnMinus.setEnabled(false);
    }else{
       kony.print("minAmount not equal");
       frmMBCCUsageLimit.btnMinus.skin = "sknBtnMinusBlue";
       frmMBCCUsageLimit.btnMinus.setEnabled(true);
    }
    if(newUsageAmt == maxAmount){
       kony.print("maxAmount equal");
        frmMBCCUsageLimit.btnPlus.skin = "sknBtnPlusWhite";
          frmMBCCUsageLimit.btnPlus.setEnabled(false);
    }else{
        kony.print("maxAmount not equal");
      frmMBCCUsageLimit.btnPlus.skin = "sknBtnPlusBlue";
      frmMBCCUsageLimit.btnPlus.setEnabled(true);
    }
     if(gblDeviceInfo["name"] == "android"){
      frmMBCCUsageLimit.sliderCardLimit.thumbOffset = 0;
	  frmMBCCUsageLimit.sliderCardLimit.top = "8%";
	  frmMBCCUsageLimit.sliderCardLimit.width = "112%";
    }else{
//       frmMBCCUsageLimit.sliderCardLimit.top = "15%";
//       frmMBCCUsageLimit.sliderCardLimit.width = "80%";
    }
    frmMBCCUsageLimit.sliderCardLimit.selectedValue = gblSliderUsagelimitVal;
    if(frmMBCCUsageLimit.sliderCardLimit.selectedValue == gblMinresultval){
        frmMBCCUsageLimit.sliderCardLimit.selectedValue = roundedlblMinlblVal;
    }
  }catch(e){
    kony.print("Exception "+e);
  }
  
  
}

function postShowOfCCUsageLimit(){
  
  kony.print("postShowOfCCUsageLimit start");
  
  addAccessPinKeypad(frmMBCCUsageLimit);
  //TODO below two values we will get from service
  //frmMBCCUsageLimit.sliderCardLimit.min = parseFloat("5000");
  //frmMBCCUsageLimit.sliderCardLimit.max = parseFloat("100000");
//   frmMBCCUsageLimit.sliderCardLimit.step = 1000;
//   frmMBCCUsageLimit.sliderCardLimit.selectedValue = parseFloat("45000");

}


function navigateToCCUsageLimtComplate(tranPassword){
  kony.print("### tranPassword : "+tranPassword);
  
  var inputParam={};
  inputParam["loginModuleId"] = "MB_TxPwd";
  inputParam["password"] =  encryptData(tranPassword);  
    var locale = kony.i18n.getCurrentLocale();
    if (locale == "en_US") {
        inputParam["localeCd"] = "en_US";
    } else {
        inputParam["localeCd"] = "th_TH";
    }
   var inputAmt = frmMBCCUsageLimit.lblLimitVal.text;
   inputAmt = removeCommas(inputAmt);
   inputParam["cardRefId"] =gblCACardRefNumber ;
   inputParam["amount"] = inputAmt; //-- need to check which amt value send to service
  showLoadingScreen();
  invokeServiceSecureAsync("creditCardLimitSettingCompositeService", inputParam, callbackVerifyCCChangeLimit);
  //frmMBCCUsageLimitComplete.show();
}

function callbackVerifyCCChangeLimit(status,result){
  if (status == 400) {
    if (result["opstatus"] == 0) {
      
      dismissLoadingScreen();
      setfrmMBCCUsageLimitComplete(result);
      closeApprovalKeypad();
	  frmMBCCUsageLimitComplete.show();
    }else if (result["opstatus"] == 1) {
      if(result["errCode"] == "E10020" || result["errMsg"] == "Wrong Password" ){
        //Wrong password Entered 

        resetKeypadApproval();
        var badLoginCount = result["badLoginCount"];
        var incorrectPinText = kony.i18n.getLocalizedString("PIN_Incorrect");
        incorrectPinText = incorrectPinText.replace("{rem_attempt}", gblTotalPinAttempts - badLoginCount);
        frmMBCCUsageLimit.lblForgotPin.text = incorrectPinText;
        frmMBCCUsageLimit.lblForgotPin.skin = "lblBlackMed150NewRed";
        frmMBCCUsageLimit.lblForgotPin.onTouchEnd = doNothing;
        dismissLoadingScreen();

      }else if(result["errCode"] == "E10403" || result["errMsg"] == "Password Locked" ){
        // password Locked 
        closeApprovalKeypad();
        dismissLoadingScreen();
        //frmMBManageDebitCardFailure.rchAddress.text = kony.i18n.getLocalizedString("ChangeDBL_Failed");
        gotoUVPINLockedScreenPopUp();

      }else {
        dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        return false;
      }
    } else {
        dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        return false;
      }
  }else {
      dismissLoadingScreen();
      showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
      return false;
    }

  }
function setfrmMBCCUsageLimitComplete(result){
  if(result.statusChangeLimit=="I"){
  	kony.print("increased limit");
    frmMBCCUsageLimitComplete.lblSubHeader.text = getLocalizedString("CUL02_txtDesc01");
  }else{
    kony.print("Decreased limit");
    frmMBCCUsageLimitComplete.lblSubHeader.text = getLocalizedString("CUL02_txtDesc02");
  }
  var lblMaxlblVal = parseFloat(result["maxLimit"]).toFixed(2);
  var lblUsageLimitlblVal = parseFloat(result["usageLimit"]).toFixed(2);
  var lblAvailToSpendlblVal = parseFloat(result["availToSpend"]).toFixed(2);
  
  frmMBCCUsageLimitComplete.lblCardLimitVal.text = commaFormattedTransfer(lblMaxlblVal+"")+ " "+ kony.i18n.getLocalizedString("currencyThaiBaht");
  frmMBCCUsageLimitComplete.lblUsageLimitVal.text = commaFormattedTransfer(lblUsageLimitlblVal+"")+ " "+ kony.i18n.getLocalizedString("currencyThaiBaht");
  frmMBCCUsageLimitComplete.lblAvalToSpendVal.text = commaFormattedTransfer(lblAvailToSpendlblVal+"")+ " " + kony.i18n.getLocalizedString("currencyThaiBaht");
  frmMBCCUsageLimitComplete.lblRefNoVal.text = "CL01233455663";
}

function initOfFrmMBCCUsageLimtComplete(){
  kony.print("init method called");
  frmMBCCUsageLimitComplete.btnMainMenu.onClick = handleMenuBtn;
  frmMBCCUsageLimitComplete.onDeviceBack = onDeviceSpaback;
  frmMBCCUsageLimitComplete.btnManageMore.onClick = preShowMBCardList;
  frmMBCCUsageLimitComplete.btnHome.onClick = showAccountSummaryFromMenu; //To load AS page with refreshed data and without pre destroying.
  
  frmMBCCUsageLimitComplete.preShow = preShowOfMBCCUsageLimtComplete;
  frmMBCCUsageLimitComplete.postShow = posShowOfMBCCUsageLimtComplete;
  kony.print("init method end");
}

function preShowOfMBCCUsageLimtComplete(){
  kony.print("preshow of frmMBCCUsageLimitComplete");
  frmMBCCUsageLimitComplete.lblHdrTxt.text = getLocalizedString("CUL02_txtTitle");
  
  //if increase, add this i18: CUL02_txtDesc01  , if decrese add this i18n : CUL02_txtDesc02
  //frmMBCCUsageLimitComplete.lblSubHeader.text = getLocalizedString("CUL02_txtDesc01");
  
  //handle whether increase limit or decrease limit.
  var oldAvailLimit = frmMBCCUsageLimit.lblPrevAmount.text;
  var oldAvalToSpendAmt = getAmountValueFromAmtWithBaht(oldAvailLimit);
  kony.print("oldAvailLimit : "+oldAvailLimit+" , oldAvalToSpendAmt : "+oldAvalToSpendAmt);
  
  var newAvailLimit = frmMBCCUsageLimit.lblAvailToSpendVal.text;
  var newAvalToSpendAmt = getAmountValueFromAmtWithBaht(newAvailLimit);
  kony.print("newAvailLimit : "+newAvailLimit+" , newAvalToSpendAmt : "+newAvalToSpendAmt);
//   if(parseFloat(newAvalToSpendAmt)> parseFloat(oldAvalToSpendAmt)){
//   	kony.print("increased limit");
//     frmMBCCUsageLimitComplete.lblSubHeader.text = getLocalizedString("CUL02_txtDesc01");
//   }else{
//     kony.print("Decreased limit");
//     frmMBCCUsageLimitComplete.lblSubHeader.text = getLocalizedString("CUL02_txtDesc02");
//   }
  
  frmMBCCUsageLimitComplete.imgCard.src = frmMBCCUsageLimit.imgCard.src;
  frmMBCCUsageLimitComplete.lblCardNumber.text = frmMBCCUsageLimit.lblCardNumber.text;
  frmMBCCUsageLimitComplete.lblCardAccountName.text = frmMBCCUsageLimit.lblCardAccountName.text;
  
  frmMBCCUsageLimitComplete.lblCardLimit.text = getLocalizedString("CUL01_txtCardLimit");
  frmMBCCUsageLimitComplete.lblUsageLimit.text = getLocalizedString("CUL01_txtUsageLimit");
  frmMBCCUsageLimitComplete.lblAvalToSpend.text = getLocalizedString("CUL01_txtAvail");
  frmMBCCUsageLimitComplete.lblRefNo.text = getLocalizedString("CUL02_txtRef");
  
}

function posShowOfMBCCUsageLimtComplete(){
  kony.print("post show of the form");
  
//   frmMBCCUsageLimitComplete.lblCardLimitVal.text = frmMBCCUsageLimit.lblCardLimitVal.text;
//   frmMBCCUsageLimitComplete.lblUsageLimitVal.text = frmMBCCUsageLimit.lblUsageLimitVal.text;
//   frmMBCCUsageLimitComplete.lblAvalToSpendVal.text = frmMBCCUsageLimit.lblAvailToSpendVal.text;
//   frmMBCCUsageLimitComplete.lblRefNoVal.text = "CL01233455663";
  
  frmMBCCUsageLimitComplete.imgSuccessTick.setVisibility(true);
}