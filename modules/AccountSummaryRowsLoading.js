var productName;
var imageName;
var btnText;
var containerWt;  
var visibility_transfer=false;
var visibility_paybill=false;
var visibility_topup=false;
var visibility_paymycredit=false;
var visibility_paymyloan=false;
var visibility_cardless=false;
var hbxPercentage;
var hbxlayoutAlignment;
var hbxwidgetAlignment;
var hbxvisibilityRemainingFee;
var lblRemainingFeeVal;
var lblBaltext;
var onClickBtn;
var margin;
var spaPadding;

function onScrollEndDynamicRows(){
  try{
    var custAcctReclength = glblResulttable["custAcctRec"].length;
    loadRows(glbl_i,glbl_i+7,custAcctReclength);
    glbl_i =  glbl_i+ 7;
  }catch(e){
    kony.print("error in onScrollEndDynamicRows "+e);
  }
}

function loadRows(start, end, max_length){
  var locale = kony.i18n.getCurrentLocale();
  if(start > max_length)
    return;
  if(start < max_length && end > max_length)
    end = max_length;

  var i = glbl_i;

  try {
    resulttable = glblResulttable;
    if(resulttable["custAcctRec"] === undefined || resulttable["custAcctRec"] === null)
     alert("Sorry for inconvenience. Service is temporarily unavailable at this moment. Please try again.");
    if(resulttable["custAcctRec"] != undefined){    
      for (i = start; i < end; i++) {

        if (resulttable["custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_NOFEESAVING_TABLE") {
          hbxvisibilityRemainingFee = true;
          lblRemainingFeeVal = resulttable["custAcctRec"][i]["remainingFee"];
        } else {
          hbxvisibilityRemainingFee = false;
          lblRemainingFeeVal = "";
        }
        if (resulttable["custAcctRec"][i]["accType"] == kony.i18n.getLocalizedString("CreditCard"))
          availableBalace = commaFormatted(resulttable["custAcctRec"][i]["availableCreditBal"]) +" "+kony.i18n.getLocalizedString(
            "currencyThaiBaht")
          else
            availableBalace = resulttable["custAcctRec"][i]["availableBalDisplay"] +" "+ kony.i18n.getLocalizedString(
              "currencyThaiBaht")

            if (resulttable["custAcctRec"][i]["PayMyLoan"] == "Y") {			 		
              visibility_paymyloan = true;
              lblBaltext = kony.i18n.getLocalizedString("OutstandingBalance");
              //onClickBtnLoan = onClickLoanAcctSummary;
            }else{
              visibility_paymyloan = false;
            }			
        if (resulttable["custAcctRec"][i]["Transfer"] == "Y")
        {
          visibility_transfer = true;			
          lblBaltext = kony.i18n.getLocalizedString("Balance");
          //onClickBtnTran = onclickgetTransferFromAccountsFromAccSmry;
        }else{
          visibility_transfer = false;
        }		
        if (resulttable["custAcctRec"][i]["cardlessWithdraw"] == "Y")
        {
          if(GLOBAL_UV_STATUS_FLAG == "ON"){
            visibility_cardless = true;	
          }else{
            visibility_cardless = false;
          }		
        }else{
          visibility_cardless = false;
        }	
        if (resulttable["custAcctRec"][i]["PayMyCreditCard"] == "Y") {
          lblBaltext = kony.i18n.getLocalizedString("AvailableCreditLimit");
          //onClickBtnCred = onClickCreditAcctSummary;
          visibility_paymycredit = true;
        }else{
          visibility_paymycredit=false;
        }
        if (resulttable["custAcctRec"][i]["Paybill"] == "Y")
        {
          visibility_paybill = true;
          lblBaltext = kony.i18n.getLocalizedString("Balance");
          //onClickBtnBillPay = onclickgetBillPaymentFromAccountsFromAccSmry;
        }else{
          visibility_paybill = false;
        }
        if (resulttable["custAcctRec"][i]["Topup"] == "Y")
        {
          visibility_topup = true;
          lblBaltext = kony.i18n.getLocalizedString("Balance");
          //onClickBtnTopup = onclickgetTopUpFromAccountsFromAccSmry;
        }else{
          visibility_topup = false;
        }
        if (locale == "th_TH") {
          productName = resulttable["custAcctRec"][i]["ProductNameThai"];
        } else if (locale == "en_US") {
          productName = resulttable["custAcctRec"][i]["ProductNameEng"];
        }
        var nickName;
        if ((resulttable["custAcctRec"][i]["acctNickName"]) == null || (resulttable["custAcctRec"][i]["acctNickName"]) == '') {
          var sbStr = resulttable["custAcctRec"][i]["accId"];
          var length = sbStr.length;
          if (resulttable["custAcctRec"][i]["accType"] == kony.i18n.getLocalizedString("Loan"))
            sbStr = sbStr.substring(7, 11);
          else
            sbStr = sbStr.substring(length - 4, length);
          nickName = productName + " " + sbStr;
        }
        else{
          nickName=resulttable["custAcctRec"][i]["acctNickName"];
        }
        //var randomnum = Math.floor((Math.random() * 10000) + 1);
        //imageName="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+resulttable["custAcctRec"][i]["ICON_ID"]+"&modIdentifier=PRODICON" +randomnum;
        imageName=loadAccntProdImages(resulttable["custAcctRec"][i]["ICON_ID"]);
        kony.print("^^^^^^The Imagename is^^^^^^"+imageName)

        createDynamicFlex(i, imageName, nickName, lblBaltext, availableBalace, lblRemainingFeeVal, showAccountDetails);
      
        var SwipeContainer = new kony.ui.FlexContainer({
          "autogrowMode": kony.flex.AUTOGROW_NONE,
          "clipBounds": true,
          "height": "13%",
          "id": "SwipeContainer"+i,
          "isVisible": false,
          "layoutType": kony.flex.FLOW_HORIZONTAL,
          "left": "0dp",
          "skin": "flexGreyBG",
          "top": "0%",
          "width": "100%",
          "zIndex": 0
        }, {}, {});
        SwipeContainer.setDefaultUnit(kony.flex.DP);        

        var no_sections = 0;

        if(visibility_transfer==true)
          no_sections++;
        if(visibility_paybill==true)
          no_sections++;
        if(visibility_topup==true)
          no_sections++;
        if(visibility_cardless==true)
          no_sections++;
        if(visibility_paymycredit==true)
          no_sections++;
        if(visibility_paymyloan==true)
          no_sections++;


        var percentage = 0;
        var container_width = 0;
        if(no_sections == 1){
          container_width="94%";
          percentage = "3%";
        }          
        if(no_sections == 2){
          container_width="46%"
          percentage = "3%";
        } 
        if(no_sections == 3){
          container_width="30%"
          percentage = "3%";
        }
        if(no_sections == 4){
          container_width="22%"
          percentage = "3%";
        }          
        if(no_sections == 5){
          container_width="22%"
          percentage = "2%";
        }
          


        var align_first = 0;
        var set_percentage = "2%";
        if(visibility_transfer==true){
          if(align_first == 0)
            set_percentage = percentage;
          align_first = 1;
          SwipeContainer.add(setSwipeContainerData("SwipeInnerContainerTransfer"+i,set_percentage, "transfershortcut.png", kony.i18n.getLocalizedString("Transfer"), onclickgetTransferFromAccountsFromAccSmry, no_sections,container_width));
          set_percentage = "2%";
        }
        if(visibility_paybill==true){
          if(align_first == 0)
            set_percentage = percentage;
          align_first = 1;
          SwipeContainer.add(setSwipeContainerData("SwipeInnerContainerPaybill"+i,set_percentage, "paybillshortcut.png", kony.i18n.getLocalizedString("PayBill"), onclickgetBillPaymentFromAccountsFromAccSmry, no_sections,container_width));
          set_percentage = "2%";
        }
        if(visibility_topup==true){
          if(align_first == 0)
            set_percentage = percentage;
          align_first = 1;
          SwipeContainer.add(setSwipeContainerData("SwipeInnerContainertopup"+i,set_percentage, "topupshortcut.png", kony.i18n.getLocalizedString("TopUp"), onclickgetTopUpFromAccountsFromAccSmry, no_sections,container_width));
          set_percentage = "2%";
        }
        if(visibility_cardless==true){
          if(align_first == 0)
            set_percentage = percentage;
          align_first = 1;
          SwipeContainer.add(setSwipeContainerData("SwipeInnerContainerCardless"+i,set_percentage, "cardlessshortcut.png", kony.i18n.getLocalizedString("Withdraw"), onClickCardLessSwipeBtn, no_sections,container_width));
          set_percentage = "2%";
        }
        if(visibility_paymycredit==true){
          if(align_first == 0)
            set_percentage = percentage;
          align_first = 1;
          SwipeContainer.add(setSwipeContainerData("SwipeInnerContainerPaycredit"+i,set_percentage, "paybillshortcut.png", kony.i18n.getLocalizedString("PayBill"), onClickCreditAcctSummary, no_sections,container_width));
          set_percentage = "2%";
        }
        if(visibility_paymyloan==true){
          if(align_first == 0)
            set_percentage = percentage;
          align_first = 1;
          SwipeContainer.add(setSwipeContainerData("SwipeInnerContainerPaymyLoan"+i,set_percentage, "paybillshortcut.png", kony.i18n.getLocalizedString("PayBill"), onClickLoanAcctSummary, no_sections,container_width));
          set_percentage = "2%";
        }

        SwipeContainer.addGestureRecognizer(2, setSwipe, mySwipe);
        frmAccountSummaryLanding.vbox4751247744173.add(SwipeContainer);
        lineSeperator();
        
         //Loading MutualFund And BA accounts at last
         if(i == glblResulttable["custAcctRec"].length-1)
         loalMutualFundAndBA();    
      }
    }
  } catch(exception) {

  }
}

function setSwipeContainerData(id,left_margin, image, label, event, no_sections,container_width) {
var SwipeInnerContainer = new kony.ui.FlexContainer({
    "autogrowMode": kony.flex.AUTOGROW_NONE,
    "clipBounds": true,
    "height": "90%",
    "id": id,
    "isVisible": true,
    "layoutType": kony.flex.FREE_FORM,
    "left": left_margin,
    "onClick": event,
    "skin": "FlexAccntSummaryRounded",
    "top": "5%",
    "width": container_width,
    "zIndex": 0
  }, {}, {});

  SwipeInnerContainer.setDefaultUnit(kony.flex.DP);

  var SwipeImage = new kony.ui.Image2({
    "height": "80%",
    "id": "SwipeImageTransfer",
    "isVisible": true,
    "left": "11%",
    "skin": "slImage",
    "src": image,
    "top": "3%",
    "width": "80%",
    "zIndex": 0
  }, {
    "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
    "padding": [0, 0, 0, 0],
    "paddingInPixel": false
  }, {});

  var SwipeLabel = new kony.ui.Label({
    "height": "30%",
    "id": "SwipeLabelTransfer",
    "isVisible": true,
    "left": "5%",
    "skin": "SwipeLabel",
    "text": label,
    "textStyle": {
      "letterSpacing": 0,
      "strikeThrough": false
    },
    "top": "70%",
    "width": "90%",
    "zIndex": 0
  }, {
    "contentAlignment": constants.CONTENT_ALIGN_CENTER,
    "padding": [0, 0, 0, 0],
    "paddingInPixel": false
  }, {
    "textCopyable": false
  });

  SwipeInnerContainer.add(SwipeImage, SwipeLabel);

  return SwipeInnerContainer;

}

function createDynamicFlex(i, imageName, nickName, lblBaltext, availableBalace, lblRemainingFeeVal, event){
  try{
		if(hbxvisibilityRemainingFee){
		 var hbxAccountDetail;
		 if(event!=false){
			hbxAccountDetail = new kony.ui.FlexContainer({
			"autogrowMode": kony.flex.AUTOGROW_NONE,
			"height": "15%",
			"id": "hboxSwipe"+i,
			"isVisible": true,
			"layoutType": kony.flex.FLOW_HORIZONTAL,
			"left": "0dp",
			"skin": "slFbox",
			"top": "0dp",
			"width": "100%",
			"zIndex": 1,
			"onClick": event
		  }, {}, {});
		 }
		else{
		hbxAccountDetail = new kony.ui.FlexContainer({
			"autogrowMode": kony.flex.AUTOGROW_NONE,
			"height": "15%",
			"id": "hboxSwipe"+i,
			"isVisible": true,
			"layoutType": kony.flex.FLOW_HORIZONTAL,
			"left": "0dp",
			"skin": "slFbox",
			"top": "0dp",
			"width": "100%",
			"zIndex": 1
		  }, {}, {});
		}  
		hbxAccountDetail.setDefaultUnit(kony.flex.DP);
		var imgAccount = new kony.ui.Image2({
		  "height": "100%",
		  "id": "imgAccount" + i,
		  "isVisible": true,
		  "left": "0dp",
		  "skin": "slImage",
		  "src": imageName,
		  "top": "0dp",
		  "width": "15%",
		  "zIndex": 1
		}, {
		  "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
		  "padding": [0, 0, 0, 0],
		  "paddingInPixel": false
		}, {});

		var vboxAccontypeDetail = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "vboxAccontypeDetail"+i,
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "2%",
        "skin": "slFbox",
        "top": "0%",
        "width": "78%",
        "zIndex": 1
		}, {}, {});
		vboxAccontypeDetail.setDefaultUnit(kony.flex.DP);
		var lblAccountType = new kony.ui.Label({
        "height": "34%",
        "id": "lblAccountType"+i,
        "isVisible": true,
        "left": "0dp",
        "skin": "lblPopupBlack",
        "text": nickName,
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "100%",
        "zIndex": 1
		}, {
			"contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
			"padding": [0, 0, 0, 0],
			"paddingInPixel": false
		}, {
			"textCopyable": false
		});
		
		//remaining
		var hboxRemaining = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "33%",
        "id": "hboxRemaining"+i,
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
		}, {}, {});
		hboxRemaining.setDefaultUnit(kony.flex.DP);
		var lblRemaining = new kony.ui.Label({
        "id": "lblRemaining"+i,
        "isVisible": true,
        "skin": "lblBlackSmall",
        "text": kony.i18n.getLocalizedString("remFreeTran") + " :",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "65%",
        "zIndex": 1
		}, {
			"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
			"padding": [0, 2, 0, 0],
			"paddingInPixel": false
		}, {
			"textCopyable": false
		});
		var lblRemainingNo = new kony.ui.Label({
        "id": "lblRemainingNo"+i,
        "isVisible": true,
		"left": "0%",
        "skin": "lblBlueFont",
        "text": lblRemainingFeeVal,
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "7%",
        "zIndex": 1
		}, {
			"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
			"padding": [0, 0, 0, 0],
			"paddingInPixel": false
		}, {
			"textCopyable": false
		});
		hboxRemaining.add(lblRemaining, lblRemainingNo);
		//hbox balance
		var hboxBalance = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "35%",
        "id": "hboxBalance"+i,
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
		}, {}, {});
		hboxBalance.setDefaultUnit(kony.flex.DP);
		var lblBalance = new kony.ui.Label({
        "id": "lblBalance"+i,
        "isVisible": true,
        "left": "0dp",
        "skin": "lblBlackSmallBold",
        "text": lblBaltext,
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "30%",
        "zIndex": 1
		}, {
			"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
			"padding": [0, 2, 0, 0],
			"paddingInPixel": false
		}, {
			"textCopyable": false
		});
		var lblBalValue = new kony.ui.Label({
        "id": "lblBalValue"+i,
        "isVisible": true,
        "left": "0dp",
        "skin": "lblBlackLarge",
        "text": availableBalace,
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "70%",
        "zIndex": 1
		}, {
			"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
			"padding": [0, 0, 0, 0],
			"paddingInPixel": false
		}, {
			"textCopyable": false
		});
		hboxBalance.add(lblBalance, lblBalValue);
		vboxAccontypeDetail.add(lblAccountType, hboxRemaining, hboxBalance);
		var imgChevron = new kony.ui.Image2({
        "height": "100%",
        "id": "imgChevron"+i,
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "navarrow.png",
        "top": "0dp",
        "width": "5%",
        "zIndex": 1
		}, {
			"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
			"padding": [0, 0, 0, 0],
			"paddingInPixel": false
		}, {});
		hbxAccountDetail.add(imgAccount, vboxAccontypeDetail, imgChevron);		
	}else{
		var hbxAccountDetail;
		if(event!=false){
				hbxAccountDetail = new kony.ui.FlexContainer({
				"autogrowMode": kony.flex.AUTOGROW_NONE,
				"height": "13%",
				"id": "hboxSwipe"+i,
				"isVisible": true,
				"layoutType": kony.flex.FLOW_HORIZONTAL,
				"left": "0dp",
				"skin": "slFbox",
				"top": "0dp",
				"width": "100%",
				"zIndex": 1,
				"onClick": event
		  		}, {}, {});
		}
		else{
		hbxAccountDetail = new kony.ui.FlexContainer({
		 "autogrowMode": kony.flex.AUTOGROW_NONE,
		 "height": "15%",
		 "id": "hboxSwipe"+i,
		 "isVisible": true,
		 "layoutType": kony.flex.FLOW_HORIZONTAL,
		 "left": "0dp",
		 "skin": "slFbox",
		 "top": "0dp",
		 "width": "100%",
		 "zIndex": 1
		  }, {}, {});
		}
		hbxAccountDetail.setDefaultUnit(kony.flex.DP);
		var imgAccount = new kony.ui.Image2({
		  "height": "100%",
		  "id": "imgAccount" + i,
		  "isVisible": true,
		  "left": "0dp",
		  "skin": "slImage",
		  "src": imageName,
		  "top": "0dp",
		  "width": "15%",
		  "zIndex": 1
		}, {
		  "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
		  "padding": [0, 0, 0, 0],
		  "paddingInPixel": false
		}, {});

		var vboxAccontypeDetail = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "vboxAccontypeDetail"+i,
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "2%",
        "skin": "slFbox",
        "top": "0%",
        "width": "78%",
        "zIndex": 1
    }, {}, {});
		vboxAccontypeDetail.setDefaultUnit(kony.flex.DP);
		var lblAccountType = new kony.ui.Label({
        "centerY": "32%",
        "height": "38%",
        "id": "lblAccountType"+i,
        "isVisible": true,
        "left": "0dp",
        "right": "2%",
        "skin": "lblPopupBlack",
        "text": nickName,
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "98%",
        "zIndex": 1
		}, {
			"contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
			"padding": [0, 0, 0, 0],
			"paddingInPixel": false
		}, {
			"textCopyable": false
		});
		var hboxBalance = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40%",
        "id": "hboxBalance"+i,
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "right": "2%",
        "skin": "slFbox",
        "width": "98%",
        "zIndex": 1
		}, {}, {});
		hboxBalance.setDefaultUnit(kony.flex.DP);
		var lblBalance = new kony.ui.Label({
        "id": "lblBalance"+i,
        "isVisible": true,
        "left": "0dp",
        "skin": "lblBlackSmallBold",
        "text": lblBaltext,
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "42%",
        "zIndex": 1
		}, {
			"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
			"padding": [0, 2, 0, 0],
			"paddingInPixel": false
		}, {
			"textCopyable": false
		});
		var lblBalValue = new kony.ui.Label({
        "id": "lblBalValue"+i,
        "isVisible": true,
        "left": "0dp",
        "skin": "lblBlackLarge",
        "text": availableBalace,
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "58%",
        "zIndex": 1
		}, {
			"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
			"padding": [0, 0, 0, 0],
			"paddingInPixel": false
		}, {
			"textCopyable": false
		});
		hboxBalance.add(lblBalance, lblBalValue);		
		vboxAccontypeDetail.add(lblAccountType, hboxBalance);
		var imgChevron = new kony.ui.Image2({
        "height": "100%",
        "id": "imgChevron"+i,
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "navarrow.png",
        "top": "0dp",
        "width": "5%",
        "zIndex": 1
		}, {
			"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
			"padding": [0, 0, 0, 0],
			"paddingInPixel": false
		}, {});
		hbxAccountDetail.add(imgAccount, vboxAccontypeDetail, imgChevron);
	}
    if(event == showAccountDetails){
      var setSwipe = {
        fingers: 1,
        swipedistance: 105,
        swipevelocity: 75
      };
      var setupTblTap = {
        fingers: 1,
        taps: 1
      };


//       // due to SPA single tap platform issue we made it seperately.
//       if ((null != gblPlatformName) && (kony.string.equalsIgnoreCase("thinclient", gblPlatformName))) {
//         hbxAccountDetail.addGestureRecognizer(1, gblspasngleTap, showAccountDetails);
//         SwipeContainer.addGestureRecognizer(2, setSwipe, mySwipe);
//       } else { 
//         hbxAccountDetail.addGestureRecognizer(1, setupTblTap, showAccountDetails);
//       }
      hbxAccountDetail.addGestureRecognizer(2, setSwipe, mySwipe);
    } 
    frmAccountSummaryLanding.vbox4751247744173.add(hbxAccountDetail);
  }catch(e){
    kony.print("Exception in createDynamicFlex"+e);
  }

}


function lineSeperator(){
  var lineSeperator = new kony.ui.FlexContainer({
    "autogrowMode": kony.flex.AUTOGROW_NONE,
    "clipBounds": true,
    "height": "0.2%",
    "id": "lineSeperator",
    "isVisible": true,
    "layoutType": kony.flex.FREE_FORM,
    "left": "0%",
    "skin": "LineSeperator",
    "top": "0%",
    "width": "100%",
    "zIndex": 1
  }, {}, {});
  lineSeperator.setDefaultUnit(kony.flex.DP);
  lineSeperator.add();
  frmAccountSummaryLanding.vbox4751247744173.add(lineSeperator);

}

function loalMutualFundAndBA(){
   resulttable = glblResulttable;
   var i = resulttable["custAcctRec"].length;

    //ENH_106_107 - Mutual Funds - MF Accounts Logic to add MF account in Customer Account Summary
    if(!flowSpa && resulttable["mfAccountFlag"] == "true") {
      var availableBalace = resulttable["mfTotalAmount"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht");

        if(!gblHaveMutualFundsFlg){ gblHaveMutualFundsFlg = true; }
        hbxvisibilityRemainingFee = false;
        lblRemainingFeeVal = "";

        //availableBalace = resulttable["mfTotalAmount"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht");

        var nickName;
        nickName= kony.i18n.getLocalizedString("MF_Acc_Sumary_Title");
        var randomnum = Math.floor((Math.random() * 10000) + 1);
        //imageName="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId=MF-Product&modIdentifier=MFLOGOS&rr="+randomnum;
		var imagenameval = "mf_product";
      	imageName = loadAccntProdImages(imagenameval);

        lblBaltext = kony.i18n.getLocalizedString("MF_lbl_Investment_value_h2");

        createDynamicFlex(i, imageName, nickName, lblBaltext, availableBalace, lblRemainingFeeVal, showMFAccountDetails);//MKI, MIB-Mutual fund ON/OFF issue

        lineSeperator();
      
    }

    i++;
    //ENH_108 - BA View Policy Details - Logic to add BA account in Customer Account Summary
    if(!flowSpa && resulttable["baAccountFlag"] == "true") {
      var totalSumInsured = resulttable["totalSumInsured"];
        if(totalSumInsured != "-") {
          totalSumInsured = commaFormatted(parseFloat(totalSumInsured).toFixed(2)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
        } 
        if(!gblHaveBankAssuranceFlg){ gblHaveBankAssuranceFlg = true; }
        hbxvisibilityRemainingFee = false;
        lblRemainingFeeVal = "";

    var nickName;
    nickName= kony.i18n.getLocalizedString("BA_Acc_Summary_Title");
    var randomnum = Math.floor((Math.random() * 10000) + 1);
    //imageName="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId=BA_PROD_LOGO&modIdentifier=MFLOGOS&rr="+randomnum;
    var imagenameval = "ba_prod_logo";
    imageName = loadAccntProdImages(imagenameval);

    lblBaltext = kony.i18n.getLocalizedString("MF_lbl_Investment_value_h2");
    createDynamicFlex(i, imageName, nickName, lblBaltext, totalSumInsured, lblRemainingFeeVal, showBAAccountDetails);	
    lineSeperator();
  }
}

function showMFAccountDetails(){  
    loadFunctionalModuleSync("mutualFundModule"); 
    checkSuitabilityExpireMB(); 
}

function showBAAccountDetails()
{
    loadFunctionalModuleSync("BAssuranceModule");
    MBcallBAPolicyListService();
}

function loadBalancesDynamically(){
  try{
    var resulttable = glblResulttable;
    var availableBalace = "";
    
    if(resulttable["custAcctRec"] === undefined || resulttable["custAcctRec"] === null)
     alert("Sorry for inconvenience. Service is temporarily unavailable at this moment. Please try again.");
    
    if(resulttable != null && resulttable["custAcctRec"] != null){ 
      for(var i = 0; i < resulttable["custAcctRec"].length; i++){
        var balIndex = "lblBalValue" + (i);
        var remainingFeeIndex = "lblRemainingNo"+i;
        if (resulttable["custAcctRec"][i]["accType"] == kony.i18n.getLocalizedString("CreditCard"))
          availableBalace = commaFormatted(resulttable["custAcctRec"][i]["availableCreditBal"]) +" "+kony.i18n.getLocalizedString("currencyThaiBaht");
        else
          availableBalace = resulttable["custAcctRec"][i]["availableBalDisplay"] +" "+ kony.i18n.getLocalizedString("currencyThaiBaht");

        if (resulttable["custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_NOFEESAVING_TABLE") {
          hbxvisibilityRemainingFee = true;
          lblRemainingFeeVal = resulttable["custAcctRec"][i]["remainingFee"];
        } else {
          hbxvisibilityRemainingFee = false;
          lblRemainingFeeVal = "";
        }

       //If accounts are not created yet, break the loop.
      if(null == frmAccountSummaryLanding[balIndex])
     	break;
        frmAccountSummaryLanding[balIndex].text = availableBalace;
       if(hbxvisibilityRemainingFee == true && null != frmAccountSummaryLanding[remainingFeeIndex])
        frmAccountSummaryLanding[remainingFeeIndex].text = lblRemainingFeeVal;
      }
    }
      //Load Mutual Funds and BA balances.
      var i = resulttable["custAcctRec"].length;
      var balIndex = "lblBalValue" + (i);
      if(null == resulttable || null == frmAccountSummaryLanding[balIndex] || resulttable["mfAccountFlag"] != "true" || null == resulttable["mfTotalAmount"])
        return;

      var availableBalace = resulttable["mfTotalAmount"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht");

      lblRemainingFeeVal = "";
      frmAccountSummaryLanding[balIndex].text = availableBalace; 

      i++;

      balIndex = "lblBalValue" + (i);
      if(null == resulttable || null == frmAccountSummaryLanding[balIndex] || resulttable["baAccountFlag"] != "true" || null == resulttable["totalSumInsured"])
        return;

      var totalSumInsured = resulttable["totalSumInsured"];
      if(totalSumInsured != "-") {
        totalSumInsured = commaFormatted(parseFloat(totalSumInsured).toFixed(2)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
      } 

      frmAccountSummaryLanding[balIndex].text = totalSumInsured; 

  }catch(e){
      kony.print("Exception in loadBalancesDynamically" + e);
    }
} 
