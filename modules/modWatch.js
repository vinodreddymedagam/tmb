function pre_init() 
{ 
//alert("inside Pre_init method"); 

kony.application.setApplicationCallbacks({onwatchrequest:watchCallback}); 

} 

/*************************************************************************************
 * Function:cleanUp()
 * Description: function to CleanUp.
 * Author: Kony
 *************************************************************************************/
function cleanUp() {
    // alert("cleanUp"); 
    kony.print("cleaning up ...");
}
gblreplyObj = "";
gblWatchBGTaskId = "";

/*************************************************************************** 
* Name	: JSWcallBack() 
* Author	: UST 
* Purpose	: This function receives requestId on watchRequestCallBack, based on request id appropriate response is sent back. 
****************************************************************************/ 
function watchCallback(dict, replyObj) 
{ 
    gblreplyObj = replyObj;
    gblQuickBalanceResult = "";
	var taskID = kony.application.beginBackgroundTask("UniqueTaskID", cleanUp);
     gblWatchBGTaskId = taskID;
	//retDict = {}; 
	//retDict = {name: 'iPhone6s', iOS: '9.3.4'}; // make a new dictionary with two pairs 
    invokeQuickBalanceServiceWatch();
    //kony.timer.schedule("watchServiceTimer", sendResponsetoWatch, 1, true);
   /*  while (true) {
    if (gblQuickBalanceResult != "") {
      alert("111 sending response to watch");
      retDict["data"] = gblQuickBalanceResult; 
      alert("222 sending response to watch");
      replyObj.executeWithReply(retDict);
      kony.application.endBackgroundTask(taskID);
      break; 
    }
    } */
  
   /*  function sendResponsetoWatch(){
      alert("Coming to time callback")
      if(gblQuickBalanceResult != ""){
         alert("gblQuickBalanceResult-->"+gblQuickBalanceResult)
         retDict = gblQuickBalanceResult;
        replyObj.executeWithReply(retDict);
        kony.timer.cancel("watchServiceTimer");
        alert("Cancelled Timer");
        kony.application.endBackgroundTask(taskID);
      }else{
        alert("response not coming")
      }
      
    }*/
 // kony.application.endBackgroundTask(taskID);
}	

function invokeQuickBalanceServiceWatch() {
    var inputParam = {};
    inputParam["quickBalanceFlag"] = "true";
    inputParam["encryptDeviceId"] = getDeviceID();
    kony.print("watch-->"+inputParam["encryptDeviceId"]);
   // var retDict = {}
    //retDict["data"] = [{"acctStatusCode":"Active | ปกติ (Active)","bankCD":"","availableCreditBalDisplay":"0.00","availableBalDisplay":"11111111","ICON_ID":"ICON-02","persionlizedId":"","productID":"225","availableBal":"13502.90","remainingFee":"5","availableCreditBal":"0.00","defaultCurrentNickNameTH":"บัญชี ทีเอ็มบี ออลล์ ฟรี 3123","acctStatus":"Active | ปกติ (Active)","VIEW_NAME":"PRODUCT_CODE_SAVING_TABLE","accId":"00002742053123","accountNoFomatted":"274-2-05312-3","QuickBalanceEnabled":"02","defaultCurrentNickNameEN":"Sanddep Balanagu","ProductNameEng":"TMB All Free Account","partyAcctRelDesc":"PRIIND","openingMethod":"INT","persionlizeAcctId":"","accType":"SDA","accountName":"UAT34 IB34","SortId":"10003","crmId":"000000000000000000000000000000","fiident":"0011000102740200","ledgerBal":"13502.90","ProductNameThai":"บัญชี ทีเอ็มบี ออลล์ ฟรี"},{"acctStatusCode":"Active | ปกติ (Active)","bankCD":"","availableCreditBalDisplay":"0.00","availableBalDisplay":"222222222","ICON_ID":"ICON-02","persionlizedId":"","productID":"225","availableBal":"13502.90","remainingFee":"5","availableCreditBal":"0.00","defaultCurrentNickNameTH":"บัญชี ทีเอ็มบี ออลล์ ฟรี 3123","acctStatus":"Active | ปกติ (Active)","VIEW_NAME":"PRODUCT_CODE_SAVING_TABLE","accId":"00002742053123","accountNoFomatted":"274-2-05312-3","QuickBalanceEnabled":"02","defaultCurrentNickNameEN":"Swetha Balanagu","ProductNameEng":"TMB All Free Account","partyAcctRelDesc":"PRIIND","openingMethod":"INT","persionlizeAcctId":"","accType":"SDA","accountName":"UAT34 IB34","SortId":"10003","crmId":"000000000000000000000000000000","fiident":"0011000102740200","ledgerBal":"13502.90","ProductNameThai":"บัญชี ทีเอ็มบี ออลล์ ฟรี"}];
   // alert("calling service");
    invokeServiceSecureAsync("quickBalance", inputParam, callBackQuickBalanceWatch);
  //  return retDict;
}

  function callBackQuickBalanceWatch(status, resulttable) {
    try {
          var locale_app=kony.i18n.getCurrentLocale();
          var retDict = {};
        var  masterData = []
       // retDict["data"]  = [{"acctStatusCode":"Active | ปกติ (Active)","bankCD":"","availableCreditBalDisplay":"0.00","availableBalDisplay":"11111111","ICON_ID":"ICON-02","persionlizedId":"","productID":"225","availableBal":"13502.90","remainingFee":"5","availableCreditBal":"0.00","defaultCurrentNickNameTH":"บัญชี ทีเอ็มบี ออลล์ ฟรี 3123","acctStatus":"Active | ปกติ (Active)","VIEW_NAME":"PRODUCT_CODE_SAVING_TABLE","accId":"00002742053123","accountNoFomatted":"274-2-05312-3","QuickBalanceEnabled":"02","defaultCurrentNickNameEN":"Sanddep Balanagu","ProductNameEng":"TMB All Free Account","partyAcctRelDesc":"PRIIND","openingMethod":"INT","persionlizeAcctId":"","accType":"SDA","accountName":"UAT34 IB34","SortId":"10003","crmId":"000000000000000000000000000000","fiident":"0011000102740200","ledgerBal":"13502.90","ProductNameThai":"บัญชี ทีเอ็มบี ออลล์ ฟรี"},{"acctStatusCode":"Active | ปกติ (Active)","bankCD":"","availableCreditBalDisplay":"0.00","availableBalDisplay":"222222222","ICON_ID":"ICON-02","persionlizedId":"","productID":"225","availableBal":"13502.90","remainingFee":"5","availableCreditBal":"0.00","defaultCurrentNickNameTH":"บัญชี ทีเอ็มบี ออลล์ ฟรี 3123","acctStatus":"Active | ปกติ (Active)","VIEW_NAME":"PRODUCT_CODE_SAVING_TABLE","accId":"00002742053123","accountNoFomatted":"274-2-05312-3","QuickBalanceEnabled":"02","defaultCurrentNickNameEN":"Swetha Balanagu","ProductNameEng":"TMB All Free Account","partyAcctRelDesc":"PRIIND","openingMethod":"INT","persionlizeAcctId":"","accType":"SDA","accountName":"UAT34 IB34","SortId":"10003","crmId":"000000000000000000000000000000","fiident":"0011000102740200","ledgerBal":"13502.90","ProductNameThai":"บัญชี ทีเอ็มบี ออลล์ ฟรี"}];
     // retDict["data"] =  resulttable["custAcctRec"]; 
        if (resulttable["setQuickBalance"] == "true" || resulttable["setQuickBalance"] == true) {
       for (var i = 0; i < resulttable["custAcctRec"].length; i++) {
                        if ((resulttable["custAcctRec"][i]["acctNickName"]) == null || (resulttable["custAcctRec"][i]["acctNickName"]) == undefined) {
                            if (locale_app == "th_TH")
                             accountName = resulttable["custAcctRec"][i]["defaultCurrentNickNameTH"];
                            else
                             accountName = resulttable["custAcctRec"][i]["defaultCurrentNickNameEN"];
                        } else {

                            accountName = resulttable["custAcctRec"][i]["acctNickName"];
                        }
                        if ((resulttable["custAcctRec"][i]["accType"]) == "CCA") {
                            availBalLabel = kony.i18n.getLocalizedString("keyMBCreditlimit").replace(":", "");
                            availBalValue = resulttable["custAcctRec"][i]["availableCreditBalDisplay"];
                        } else {
                            availBalLabel = kony.i18n.getLocalizedString("Balance").replace(":", "");
                            availBalValue = resulttable["custAcctRec"][i]["availableBalDisplay"];
                        }
                        var tempRecord = {
                            "lblAccountNickName": accountName,
                            "lblAvailableBalanceValue": availBalValue.substring(0,availBalValue.indexOf(".")),
                            "lblAvailableBalanceValueDecimal": availBalValue.substring(availBalValue.indexOf("."),availBalValue.length),
                            "lblAvailBalText": availBalLabel
                        };
                        masterData.push(tempRecord);
                    }
          retDict["data"] =  masterData; 
          if (locale_app == "th_TH"){
        retDict["header1"] = "Want to see your balance?";
        retDict["header2"] = "ต้องการดูความสมดุลของคุณหรือไม่?";
      }else{
        retDict["header1"] = "Activate this feature in TMB Touch under settings,Quick Balance.";
        retDict["header2"] = "เปิดใช้คุณลักษณะนี้ใน TMB Touch ภายใต้การตั้งค่า Quick Balance";
      }
    }else{
      retDict["data"] =  []; 
      if (locale_app == "th_TH"){
         retDict["header1"] = "ต้องการดูความสมดุลของคุณหรือไม่?";
        retDict["header2"] = "เปิดใช้คุณลักษณะนี้ใน TMB Touch ภายใต้การตั้งค่า Quick Balance";
      }else{
        retDict["header1"] = "Want to see your balance?";
        retDict["header2"] = "Activate this feature in TMB Touch under settings,Quick Balance.";
       
      }
       
    }
      
      gblreplyObj.executeWithReply(retDict);
      kony.application.endBackgroundTask(gblWatchBGTaskId);
    	/* dismissLoadingScreen();
    	var locale_app=kony.i18n.getCurrentLocale();
        var accountName = "";
        var availBalLabel = "";
        var availBalValue = ""
        var masterData = [];
        if (status == 400) {
            if (resulttable["opstatus"] == "0.0" || resulttable["opstatus"] == 0) {
                if (resulttable["setQuickBalance"] == "true" || resulttable["setQuickBalance"] == true) {
                    var collectionData = resulttable["custAcctRec"];
                    
                    
                } else {
                    showQuickBalanceImage();
                }
            }           
        }*/
      
     // return retDict;
    } catch (e) {
        
    }
}

function handleQuickBalanceRequest(dict)
{
     var retDict = {}
	//var retDict = {};
	 GBL_FLOW_ID_CA = 13;
		//#ifdef iphone
			retDict = TrusteerDeviceId();
		//#endif

   // quickBalanceResult = {"SAVING_CARE_PRODUCT_CODES":"211,203","opstatus":"0","custAcctRec":[{"acctStatusCode":"Active | ปกติ (Active)","bankCD":"","availableCreditBalDisplay":"0.00","availableBalDisplay":"13,502.90","ICON_ID":"ICON-02","persionlizedId":"","productID":"225","availableBal":"13502.90","remainingFee":"5","availableCreditBal":"0.00","defaultCurrentNickNameTH":"บัญชี ทีเอ็มบี ออลล์ ฟรี 3123","acctStatus":"Active | ปกติ (Active)","VIEW_NAME":"PRODUCT_CODE_SAVING_TABLE","accId":"00002742053123","accountNoFomatted":"274-2-05312-3","QuickBalanceEnabled":"02","defaultCurrentNickNameEN":"TMB All Free Account 3123","ProductNameEng":"TMB All Free Account","partyAcctRelDesc":"PRIIND","openingMethod":"INT","persionlizeAcctId":"","accType":"SDA","accountName":"UAT34 IB34","SortId":"10003","crmId":"000000000000000000000000000000","fiident":"0011000102740200","ledgerBal":"13502.90","ProductNameThai":"บัญชี ทีเอ็มบี ออลล์ ฟรี"}],"enableMaintainenceFinancialTxnIB":"04","statusCode":"0","SAVING_CARE_MAX_BENEFICIARIES":"5","TODAY_DATE":"04/10/2018","openingDate":"10/04/2018","StatusDesc":"Success","enableMaintainenceFinancialTxnMB":"03","ITMX_TRANSFER_EWALLET_ENABLE":"true","crmId":"000000000000000000000000000000","tknid":"EF52731B5240D32E892F236471A19F702F2E946BED34BE83FF8C2CC0B6C35E53","httpresponse":{"headers":{"X-Android-Received-Millis":"1523349310778","Cache-Control":"no-store, no-cache, must-revalidate, max-age=2592000","X-Kony-Service-Message":"","Vary":"Accept-Encoding","X-Powered-By":"Servlet/3.0","Expires":"Thu, 10 May 2018 08:35:09 GMT","X-Android-Selected-Protocol":"http/1.1","X-Android-Sent-Millis":"1523349310345","Content-Language":"en-US","X-Kony-RequestId":"65d69ce4-47b2-4a93-9eab-5beaa2e3fe99","Connection":"Keep-Alive","Date":"Tue, 10 Apr 2018 08:35:09 GMT","X-Kony-Service-Opstatus":"0","Transfer-Encoding":"chunked","Content-Type":"text/plain;charset=UTF-8","X-Android-Response-Source":"NETWORK 200","Keep-Alive":"timeout=30, max=100","Pragma":"no-cache","Content-Encoding":"gzip"},"url":"https://mibdev1.tau2904.com/services/TMBMIBService5/customerAccountInquiry","responsecode":200}}; 
   //invokeQuickBalanceServiceWatch();
  
 // retDict["data"] = [{"acctStatusCode":"Active | ปกติ (Active)","bankCD":"","availableCreditBalDisplay":"0.00","availableBalDisplay":"11111111","ICON_ID":"ICON-02","persionlizedId":"","productID":"225","availableBal":"13502.90","remainingFee":"5","availableCreditBal":"0.00","defaultCurrentNickNameTH":"บัญชี ทีเอ็มบี ออลล์ ฟรี 3123","acctStatus":"Active | ปกติ (Active)","VIEW_NAME":"PRODUCT_CODE_SAVING_TABLE","accId":"00002742053123","accountNoFomatted":"274-2-05312-3","QuickBalanceEnabled":"02","defaultCurrentNickNameEN":"Sanddep Balanagu","ProductNameEng":"TMB All Free Account","partyAcctRelDesc":"PRIIND","openingMethod":"INT","persionlizeAcctId":"","accType":"SDA","accountName":"UAT34 IB34","SortId":"10003","crmId":"000000000000000000000000000000","fiident":"0011000102740200","ledgerBal":"13502.90","ProductNameThai":"บัญชี ทีเอ็มบี ออลล์ ฟรี"},{"acctStatusCode":"Active | ปกติ (Active)","bankCD":"","availableCreditBalDisplay":"0.00","availableBalDisplay":"222222222","ICON_ID":"ICON-02","persionlizedId":"","productID":"225","availableBal":"13502.90","remainingFee":"5","availableCreditBal":"0.00","defaultCurrentNickNameTH":"บัญชี ทีเอ็มบี ออลล์ ฟรี 3123","acctStatus":"Active | ปกติ (Active)","VIEW_NAME":"PRODUCT_CODE_SAVING_TABLE","accId":"00002742053123","accountNoFomatted":"274-2-05312-3","QuickBalanceEnabled":"02","defaultCurrentNickNameEN":"Swetha kasu","ProductNameEng":"TMB All Free Account","partyAcctRelDesc":"PRIIND","openingMethod":"INT","persionlizeAcctId":"","accType":"SDA","accountName":"UAT34 IB34","SortId":"10003","crmId":"000000000000000000000000000000","fiident":"0011000102740200","ledgerBal":"13502.90","ProductNameThai":"บัญชี ทีเอ็มบี ออลล์ ฟรี"}];
	return retDict;
  	
}

  




function assignGlobalForMenuPostshow() {
	gblCallPrePost = true;
	gblCurrentForm = kony.application.getCurrentForm().id;
	try{
		changeStatusBarColor();
		kony.application.getCurrentForm().scrollboxMain.scrollToEnd();
	}catch(error){
	}
}

//Type your code here