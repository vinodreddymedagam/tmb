MF_EVENT_PURCHASE = '1';
MF_EVENT_REDEEM = '2';
MF_EVENT_SWI = '3';
MF_EVENT_SWO = '4';


MF_ORD_TYPE_PURCHASE = "P";
MF_ORD_TYPE_REDEEM = "R";
MF_ORD_TYPE_SWITCH = "S";

ORD_UNIT_ALL = 'A';
ORD_UNIT_BAHT = 'M';
ORD_UNIT_UNIT = 'U';

MF_FUNDSUMMARY_FLOW = 1;
MF_FUNDDETAIL_FLOW = 2;

function onClickRedeem(){
  var APchar = gblUnitHolderNumber.substring(0, 2);
  kony.print("APchar  "+APchar);
  kony.print(" AS Prefix "+gblMFSummaryData["MF_UHO_ASP_PREFIX"]);
  var AS_prefix  = gblMFSummaryData["MF_UHO_ASP_PREFIX"];
  if(AS_prefix == APchar){
	alert(kony.i18n.getLocalizedString("MF_AS_Prefix_ERR"));
    return;
  }
  if(gblUserLockStatusMB == "03"){
    showTranPwdLockedPopup();
  }else{
    if( gblMBMFDetailsResulttable["MF_BusinessHrsFlag"]!=null && gblMBMFDetailsResulttable["MF_BusinessHrsFlag"]=="false"){
      var endTime = gblMBMFDetailsResulttable["MF_EndTime"];
      var startTime = gblMBMFDetailsResulttable["MF_StartTime"]; 
      var messageUnavailable = kony.i18n.getLocalizedString("ECGenericErrorNew");
      messageUnavailable = messageUnavailable.replace("{start_time}", startTime);
      messageUnavailable = messageUnavailable.replace("{end_time}", endTime);
      showAlert(messageUnavailable, kony.i18n.getLocalizedString("info"));
   } else {
    gblMFOrder = {};
    gblMFEventFlag = MF_EVENT_REDEEM;
    MBcallMutualFundRulesValidation();
    gblMFOrder["orderType"] = MF_ORD_TYPE_REDEEM; 
    gblMFOrder["orderFlow"] = MF_FUNDDETAIL_FLOW;
    gblMFOrder["fundNameTH"] = gblMBMFDetailsResulttable["FundNameTH"];
    gblMFOrder["fundNameEN"] = gblMBMFDetailsResulttable["FundNameEN"];
    gblMFOrder["fundShortName"] = gblFundShort;
  }
 }
}
function onClickPurchase(){
  var APchar = gblUnitHolderNumber.substring(0, 2);
  kony.print("APchar  "+APchar);
  kony.print(" AS Prefix "+gblMFSummaryData["MF_UHO_ASP_PREFIX"]);
  var AS_prefix  = gblMFSummaryData["MF_UHO_ASP_PREFIX"];
  if(AS_prefix == APchar){
	alert(kony.i18n.getLocalizedString("MF_AS_Prefix_ERR"));
    return;
  }
  if(gblUserLockStatusMB == "03"){
    showTranPwdLockedPopup();
  }else{
    if( gblMBMFDetailsResulttable["MF_BusinessHrsFlag"]!=null && gblMBMFDetailsResulttable["MF_BusinessHrsFlag"]=="false"){
       var endTime = gblMBMFDetailsResulttable["MF_EndTime"];
       var startTime = gblMBMFDetailsResulttable["MF_StartTime"]; 
       var messageUnavailable = kony.i18n.getLocalizedString("ECGenericErrorNew");
       messageUnavailable = messageUnavailable.replace("{start_time}", startTime);
       messageUnavailable = messageUnavailable.replace("{end_time}", endTime);
       showAlert(messageUnavailable, kony.i18n.getLocalizedString("info"));
     } else {
      var tempfundTypeB = gblMFOrder["fundTypeB"];
      gblMFOrder = {};
      gblMFOrder["fundTypeB"] = tempfundTypeB ;
      kony.print("mki gblMFOrder blank 3");
      gblMFEventFlag = MF_EVENT_PURCHASE;
      MBcallMutualFundRulesValidation();
      gblMFOrder["orderType"] = MF_ORD_TYPE_PURCHASE;
      gblMFOrder["orderFlow"] = MF_FUNDDETAIL_FLOW;
      gblMFOrder["fundNameTH"] = gblMBMFDetailsResulttable["FundNameTH"];
      gblMFOrder["fundNameEN"] = gblMBMFDetailsResulttable["FundNameEN"];
      gblMFOrder["fundShortName"] = gblFundShort;
  }
 }
}
function frmMFTncInitiatization(){
  frmMFTnc.preShow = frmMFTncPreShow;
  frmMFTnc.postShow = function (){
    animateIdAndSelfieImagesMF(frmMFTnc.flxArrow, 45);
  }
  frmMFTnc.btnNext.onClick = frmMFTncOnClickNext;
  frmMFTnc.btnCancel.onClick = onClickCancelMF;
  frmMFTnc.btnBack.onClick = frmMFTncOnClickBack;
  frmMFTnc.imgChkbox1.onTouchStart = frmMFTncOnClickAcceptWarning1;
  frmMFTnc.imgChkbox2.onTouchStart = frmMFTncOnClickAcceptWarning2;
  frmMFTnc.imgChkbox3.onTouchStart = frmMFTncOnClickAcceptWarning3; //MKI, MIB-11354new
  frmMFTnc.onDeviceBack = disableBackButton;
  frmMFTnc.LabelLinkFundFactSheet.onTouchStart = frmMFTncShowFundFactSheet // what is this?
}

function frmMFTncSetVisibleWarningContent(enableFlag, enable1con, enable2con, enable3con){ //MKI, MIB-11354 start
  if(enableFlag){
    frmMFTnc.flexWarning.setVisibility(true);
    frmMFTnc.imgChkbox1.src = "chkbox2.png";
    frmMFTnc.imgChkbox2.src = "chkbox2.png";
	frmMFTnc.imgChkbox3.src = "chkbox2.png";
    frmMFTnc.btnNext.setEnabled(false);
    frmMFTnc.flexTnCContent.height = "60%";
	frmMFTnc.btnNext.skin = "btnGreyBGNoRound";   
  } else {
    frmMFTnc.flexWarning.setVisibility(false);
    frmMFTnc.flexTnCContent.height = "87%";
    frmMFTnc.imgChkbox1.src = "chkbox.png";
    frmMFTnc.imgChkbox2.src = "chkbox.png";
	frmMFTnc.imgChkbox3.src = "chkbox.png";
    frmMFTnc.btnNext.setEnabled(true);
    frmMFTnc.btnNext.skin = "btnBlueBGNoRound";	   
  }
  frmMFTnc.FlexScoreCondition.setVisibility(enable1con);
  frmMFTnc.FlexFXCondition.setVisibility(enable2con);
  frmMFTnc.FlexAgeCondition.setVisibility(enable3con);
  if(gblMFEventFlag == MF_EVENT_PURCHASE){
    frmMFTnc.flexFundFactSheetLink.setVisibility(true);
  } else {
	frmMFTnc.flexFundFactSheetLink.setVisibility(false);
  }
} //MKI, MIB-11354 End

function frmMFTncPreShow() { //MKI, MIB-11354 start
    var accountRiskLevel = 0;
    var fundRiskLevel = 0;

    frmMFTnc.FlexScrollContainer0ib81cc9011714a.setContentOffset({
        "x": "0dp",
        "y": "0dp"
    });
    frmMFTnc.flexTnCContent.setContentOffset({
        "x": "0dp",
        "y": "0dp"
    });
    frmMFTnc.lblHeader.text = kony.i18n.getLocalizedString("MF_Tnc");
    frmMFTnc.LabelWarningHeader.text = kony.i18n.getLocalizedString("MF_Disclaimer");
    frmMFTnc.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
    frmMFTnc.btnNext.text = kony.i18n.getLocalizedString("keyAgreeButton");
    frmMFTnc.flxMsgNotification.isVisible = false;
  
  	if(gblMFEventFlag == MF_EVENT_SWI||gblMFEventFlag == MF_EVENT_SWO){
      frmMFTnc.LabelInform.text = kony.i18n.getLocalizedString("MF_Purchase_46");
      frmMFTnc.LabelLinkFundFactSheet.text = kony.i18n.getLocalizedString("MF_Purchase_47");
      var allowPurchaseRedeem = 'N';
      if (gblMFSwitchOrder["fxRisk"] == "Y") {
          allowPurchaseRedeem = 'N';
      } else {
          allowPurchaseRedeem = 'Y';
      }
    }else{
      frmMFTnc.LabelInform.text = kony.i18n.getLocalizedString("MF_Purchase_46");
      frmMFTnc.LabelLinkFundFactSheet.text = kony.i18n.getLocalizedString("MF_Purchase_47");
      var allowPurchaseRedeem = 'N';
      if (gblFundRulesData["fxRisk"] == "Y") {
          allowPurchaseRedeem = 'N';
      } else {
          allowPurchaseRedeem = 'Y';
      }
    }
  	
    if (gblMFEventFlag == MF_EVENT_PURCHASE) {
        accountRiskLevel = parseInt(gblFundRulesData["suitabilityScore"]);
        fundRiskLevel = parseInt(gblFundRulesData["riskRate"]);
        custAge = gblFundRulesData["overAge"];

	    accountRiskLevel = getAccountScoreMapping(accountRiskLevel); //MKI, MIB-11354
	    if(accountRiskLevel < fundRiskLevel) {
	      if (custAge == 'Y') {
	      	gblMFOrder["AcceptTerm1"] = false;
		  	gblMFOrder["AcceptTerm2"] = false;
		  } else {
	      	gblMFOrder["AcceptTerm1"] = false;
		  	gblMFOrder["AcceptTerm2"] = true;
		  }
	    } else {
	      	gblMFOrder["AcceptTerm1"] = true;
		  	gblMFOrder["AcceptTerm2"] = true;
	    }

        if (allowPurchaseRedeem == "N") {
            gblMFOrder["AcceptTerm3"] = false;
        } else {
            gblMFOrder["AcceptTerm3"] = true;
        }

        if (gblMFOrder["AcceptTerm1"] && gblMFOrder["AcceptTerm2"] && gblMFOrder["AcceptTerm3"]) {
            flagEnableWarningContent = false;
        } else {
            flagEnableWarningContent = true;
        }
        frmMFTncSetVisibleWarningContent(flagEnableWarningContent, !(gblMFOrder["AcceptTerm1"]), !(gblMFOrder["AcceptTerm2"]), !(gblMFOrder["AcceptTerm3"]));

        if (!gblMFOrder["AcceptTerm2"]) {
            frmMFTnc.LabelWarning1.text = kony.i18n.getLocalizedString("MF_Warning1");
            frmMFTnc.lblWarningAccept1.text = kony.i18n.getLocalizedString("MF_WarningBox1");

            frmMFTnc.LabelWarning2.text = kony.i18n.getLocalizedString("MF_Warning2");
            frmMFTnc.lblWarningAccept2.text = kony.i18n.getLocalizedString("MF_WarningBox2");
        }else if (!gblMFOrder["AcceptTerm1"]) {
            frmMFTnc.LabelWarning1.text = kony.i18n.getLocalizedString("MF_Warning1");
            frmMFTnc.lblWarningAccept1.text = kony.i18n.getLocalizedString("MF_WarningBox1");
        }

        if (!gblMFOrder["AcceptTerm3"]) {
            frmMFTnc.LabelWarning3.text = kony.i18n.getLocalizedString("MF_Warning3");
            frmMFTnc.lblWarningAccept3.text = kony.i18n.getLocalizedString("MF_WarningBox3");
        }

        setPurchaseStatusIndicator(frmMFTnc);
    } else if (gblMFEventFlag == MF_EVENT_REDEEM) {
        if (allowPurchaseRedeem != "Y") { 
          	gblMFOrder["AcceptTerm1"] = true;//MKI,MIB-12690 Start
            gblMFOrder["AcceptTerm2"] = false;
            gblMFOrder["AcceptTerm3"] = true;
            frmMFTncSetVisibleWarningContent(false, gblMFOrder["AcceptTerm1"], gblMFOrder["AcceptTerm2"], gblMFOrder["AcceptTerm3"]);//MKI,MIB-12690 End
            
            frmMFTnc.LabelWarning2.text = kony.i18n.getLocalizedString("MF_Warning2"); //MKI, MIB-11354New
            frmMFTnc.lblWarningAccept2.text = kony.i18n.getLocalizedString("MF_WarningBox2"); //MKI, MIB-11354New
        } else {
            frmMFTncSetVisibleWarningContent(false, false, false, false);
            gblMFOrder["AcceptTerm1"] = true;
            gblMFOrder["AcceptTerm2"] = true;
            gblMFOrder["AcceptTerm3"] = true;
        }
        setRedeemStatusIndicator(frmMFTnc);
    }else if(gblMFEventFlag == MF_EVENT_SWI||gblMFEventFlag == MF_EVENT_SWO){
      	accountRiskLevel = parseInt(gblMFSwitchOrder["suitabilityScore"]);
        fundRiskLevel = parseInt(gblMFSwitchOrder["riskRate"]);
        custAge = gblMFSwitchOrder["overAge"];

	    accountRiskLevel = getAccountScoreMapping(accountRiskLevel); //MKI, MIB-11354
	    if(accountRiskLevel < fundRiskLevel) {
	      if (custAge == 'Y') {
	      	gblMFSwitchOrder["AcceptTerm1"] = false;
		  	gblMFSwitchOrder["AcceptTerm2"] = false;
		  } else {
	      	gblMFSwitchOrder["AcceptTerm1"] = false;
		  	gblMFSwitchOrder["AcceptTerm2"] = true;
		  }
	    } else {
	      	gblMFSwitchOrder["AcceptTerm1"] = true;
		  	gblMFSwitchOrder["AcceptTerm2"] = true;
	    }

        if (allowPurchaseRedeem == "N") {
            gblMFSwitchOrder["AcceptTerm3"] = false;
        } else {
            gblMFSwitchOrder["AcceptTerm3"] = true;
        }

        if (gblMFSwitchOrder["AcceptTerm1"] && gblMFSwitchOrder["AcceptTerm2"] && gblMFSwitchOrder["AcceptTerm3"]) {
            flagEnableWarningContent = false;
        } else {
            flagEnableWarningContent = true;
        }
        frmMFTncSetVisibleWarningContent(flagEnableWarningContent, !(gblMFSwitchOrder["AcceptTerm1"]), !(gblMFSwitchOrder["AcceptTerm2"]), !(gblMFSwitchOrder["AcceptTerm3"]));

        if (!gblMFSwitchOrder["AcceptTerm2"]) {
            frmMFTnc.LabelWarning1.text = kony.i18n.getLocalizedString("MF_Warning1");
            frmMFTnc.lblWarningAccept1.text = kony.i18n.getLocalizedString("MF_WarningBox1");

            frmMFTnc.LabelWarning2.text = kony.i18n.getLocalizedString("MF_Warning2");
            frmMFTnc.lblWarningAccept2.text = kony.i18n.getLocalizedString("MF_WarningBox2");
        }else if (!gblMFSwitchOrder["AcceptTerm1"]) {
            frmMFTnc.LabelWarning1.text = kony.i18n.getLocalizedString("MF_Warning1");
            frmMFTnc.lblWarningAccept1.text = kony.i18n.getLocalizedString("MF_WarningBox1");
        }

        if (!gblMFSwitchOrder["AcceptTerm3"]) {
            frmMFTnc.LabelWarning3.text = kony.i18n.getLocalizedString("MF_Warning3");
            frmMFTnc.lblWarningAccept3.text = kony.i18n.getLocalizedString("MF_WarningBox3");
        }

        
      frmMFTnc.flexStepIndicator.setVisibility(false);
      frmMFTnc.flxMsgNotification.isVisible = true;
      frmMFTnc.flexFundFactSheetLink.setVisibility(false);
      
      
      
      //show new comment
      
    }
} //MKI, MIB-11354 End
function frmMFTncOnClickNext(){
  if(gblMFEventFlag == MF_EVENT_SWI || gblMFEventFlag == MF_EVENT_SWO){
    saveToSessionSwitchMFOrder();    
    
  }else{
  		MBcallMutualFundRulesValidationForAcceptTnc();
  }
}
function frmMFTncOnClickBack(){
  if(gblMFEventFlag == MF_EVENT_PURCHASE){
    frmMFSelectPurchaseFundMB.show();
  } else if(gblMFEventFlag == MF_EVENT_REDEEM){
    frmMFSelectRedeemFundMB.show();
  } else if (gblMFEventFlag == MF_EVENT_SWI||gblMFEventFlag == MF_EVENT_SWO){
    frmMFSwitchLanding.show();
  }
}
function frmMFTncSetEnableNext(){
  if(gblMFEventFlag == MF_EVENT_SWI||gblMFEventFlag == MF_EVENT_SWO){
    if(gblMFSwitchOrder["AcceptTerm1"] && gblMFSwitchOrder["AcceptTerm2"] && gblMFSwitchOrder["AcceptTerm3"]) {
    frmMFTnc.btnNext.setEnabled(true);
    frmMFTnc.btnNext.skin = "btnBlueBGNoRound";
  } else {
    frmMFTnc.btnNext.setEnabled(false);
    frmMFTnc.btnNext.skin = "btnGreyBGNoRound";
  }
  }else{
  if(gblMFOrder["AcceptTerm1"] && gblMFOrder["AcceptTerm2"] && gblMFOrder["AcceptTerm3"]) {
    frmMFTnc.btnNext.setEnabled(true);
    frmMFTnc.btnNext.skin = "btnBlueBGNoRound";
  } else {
    frmMFTnc.btnNext.setEnabled(false);
    frmMFTnc.btnNext.skin = "btnGreyBGNoRound";
  }
  }
}
function frmMFTncOnClickAcceptWarning1(){
  if(frmMFTnc.imgChkbox1.src == "chkbox2.png") {
    frmMFTnc.imgChkbox1.src = "chkbox.png";
    gblMFOrder["AcceptTerm1"] = true;
    gblMFSwitchOrder["AcceptTerm1"] = true;
  } else {
    frmMFTnc.imgChkbox1.src = "chkbox2.png";
    gblMFOrder["AcceptTerm1"] = false;
    gblMFSwitchOrder["AcceptTerm1"] = false;
    
  }

  frmMFTncSetEnableNext();

}
function frmMFTncOnClickAcceptWarning2(){
  if(frmMFTnc.imgChkbox2.src == "chkbox2.png") {
    frmMFTnc.imgChkbox2.src = "chkbox.png";
    gblMFOrder["AcceptTerm2"] = true;
    gblMFSwitchOrder["AcceptTerm2"] = true;
  } else {
    frmMFTnc.imgChkbox2.src = "chkbox2.png";
    gblMFOrder["AcceptTerm2"] = false;
    gblMFSwitchOrder["AcceptTerm2"] = false;
  }

  frmMFTncSetEnableNext();
}
function frmMFTncOnClickAcceptWarning3(){//MKI, MIB-11354 start
  if(frmMFTnc.imgChkbox3.src == "chkbox2.png") {
    frmMFTnc.imgChkbox3.src = "chkbox.png";
    gblMFOrder["AcceptTerm3"] = true;
    gblMFSwitchOrder["AcceptTerm3"] = true;
  } else {
    frmMFTnc.imgChkbox3.src = "chkbox2.png"; //MKI, MIB-11354new
    gblMFOrder["AcceptTerm3"] = false;
    gblMFSwitchOrder["AcceptTerm3"] = false;
  }

  frmMFTncSetEnableNext();
}//MKI, MIB-11354 End
function frmMFSelectPurchaseFundMBSetEnableEditFund(enableFlag){
  if(enableFlag){
    frmMFSelectPurchaseFundMB.btnSeleteFund.setVisibility(true);
    frmMFSelectPurchaseFundMB.ImageArrowSelFund.setVisibility(true);
    frmMFSelectPurchaseFundMB.btnSeleteUnitHolderNo.setVisibility(true);
    frmMFSelectPurchaseFundMB.ImageArrowSelUnitHolder.setVisibility(true);
  } else {
    frmMFSelectPurchaseFundMB.btnSeleteFund.setVisibility(false);
    frmMFSelectPurchaseFundMB.ImageArrowSelFund.setVisibility(false);
    frmMFSelectPurchaseFundMB.btnSeleteUnitHolderNo.setVisibility(false);
    frmMFSelectPurchaseFundMB.ImageArrowSelUnitHolder.setVisibility(false);
  }
}
function frmMFSelectPurchaseFundMBSetHideShow(step){	
  if(step == 1){
    frmMFSelectPurchaseFundMB.FlexFundDetails.setVisibility(false);
    frmMFSelectPurchaseFundMB.LabelSelectFund.setVisibility(true);
    frmMFSelectPurchaseFundMB.FlexContainerUnitHolderNo.setVisibility(false);
    frmMFSelectPurchaseFundMB.FlexContainerPaymentDetail.setVisibility(false);
    frmMFSelectPurchaseFundMB.btnNext.setEnabled(false);
    frmMFSelectPurchaseFundMB.btnNext.skin = "btnGreyBGNoRound";
    frmMFSelectPurchaseFundMB.FlexUnitHolderDetail.setVisibility(false);
    frmMFSelectPurchaseFundMB.LabelSelectUnitHolderno.setVisibility(true);
    frmMFSelectPurchaseFundMB.FlexErrorNoUnitHolderNo.setVisibility(false);

  } else if(step == 2){
    frmMFSelectPurchaseFundMB.FlexFundDetails.setVisibility(true);
    frmMFSelectPurchaseFundMB.LabelSelectFund.setVisibility(false);
    frmMFSelectPurchaseFundMB.FlexContainerUnitHolderNo.setVisibility(true);
    frmMFSelectPurchaseFundMB.FlexContainerPaymentDetail.setVisibility(false);
    frmMFSelectPurchaseFundMB.btnNext.setEnabled(false);
    frmMFSelectPurchaseFundMB.btnNext.skin = "btnGreyBGNoRound";
    frmMFSelectPurchaseFundMB.FlexUnitHolderDetail.setVisibility(false);
    frmMFSelectPurchaseFundMB.LabelSelectUnitHolderno.setVisibility(true);
    frmMFSelectPurchaseFundMB.FlexErrorNoUnitHolderNo.setVisibility(false);
  } else if(step == 3){
    frmMFSelectPurchaseFundMB.FlexFundDetails.setVisibility(true);
    frmMFSelectPurchaseFundMB.LabelSelectFund.setVisibility(false);
    frmMFSelectPurchaseFundMB.FlexContainerUnitHolderNo.setVisibility(true);
    frmMFSelectPurchaseFundMB.FlexContainerPaymentDetail.setVisibility(true);
    
    frmMFSelectPurchaseFundMB.btnNext.setEnabled(true);
    frmMFSelectPurchaseFundMB.btnNext.skin = "btnBlueBGNoRound";
    frmMFSelectPurchaseFundMB.FlexUnitHolderDetail.setVisibility(true);
    frmMFSelectPurchaseFundMB.LabelSelectUnitHolderno.setVisibility(false);
    frmMFSelectPurchaseFundMB.FlexErrorNoUnitHolderNo.setVisibility(false);
  }
}

function frmMFSelectPurchaseFundMBInitiatization(){
  frmMFSelectPurchaseFundMB.onHide = frmMFSelectPurchaseFundMBOnHide;
  frmMFSelectPurchaseFundMB.preShow = frmMFSelectPurchaseFundMBPreShow;
  frmMFSelectPurchaseFundMB.postShow = frmMFSelectPurchaseFundMBPostShowNew;//mki, MIB-10909
  frmMFSelectPurchaseFundMB.btnNext.onClick = frmMFSelectPurchaseFundMBOnClickNext;
  frmMFSelectPurchaseFundMB.btnBack.onClick = frmMFSelectPurchaseFundMBOnClickBack;
  frmMFSelectPurchaseFundMB.btnCancel.onClick = onClickCancelMF;
  frmMFSelectPurchaseFundMB.ButtonSelectFromAccount.onClick = onSelectFromAccntMufutalFund;
  frmMFSelectPurchaseFundMB.onDeviceBack = disableBackButton
  frmMFSelectPurchaseFundMB.TextBoxAmount.onTextChange = frmMFSelectPurchaseFundMBOnTextChangeAmount;
  frmMFSelectPurchaseFundMB.TextBoxAmount.onDone = frmMFSelectPurchaseFundMBOnDoneEditingAmount;
  frmMFSelectPurchaseFundMB.btnSeleteFund.onClick = showFundListForPurchase;
  frmMFSelectPurchaseFundMB.btnSeleteUnitHolderNo.onClick = showUnitHolderNoListForPurchase;
}
function frmMFSelectPurchaseFundMBPostShowNew() // mki. MIB-10909 - start
{
  if(gbliSTextBoxAmountFocusTrue)
    {
      gbliSTextBoxAmountFocusTrue = false;
      frmMFSelectPurchaseFundMB.TextBoxAmount.setFocus(true);
    }
} // mki. MIB-10909 - End
function showFundListForPurchase(){
  setFundListForPurchase("","");
  frmMFSeachFundPurchaseMB.show();
}
function showUnitHolderNoListForPurchase(){
  if(gblMFOrder["fundCode"] != ""){
    kony.print("MKI 12122 false");
    frmMFSelectPurchaseFundMB.CopyFlexContainer0ic09daab510d48.setVisibility(false); //MKI,MIB-12122
  	frmMFSelectPurchaseFundMB.CopyFlexContainer0da027117ae7642.setVisibility(false); //MKI,MIB-12122
    frmMFFilterUnitHolderNoMB.show();
  } else {
    alert("Please Select Fund First");
  }

}
function frmMFSelectPurchaseFundMBOnTextChangeAmount(){
  var enteredAmount = frmMFSelectPurchaseFundMB.TextBoxAmount.text;
  if(isNotBlank(enteredAmount)) {
    enteredAmount = kony.string.replace(enteredAmount, ",", "");
    if(!(isNotBlank(enteredAmount) && enteredAmount.length > 0 && parseInt(enteredAmount, 10) == 0)){
      frmMFSelectPurchaseFundMB.TextBoxAmount.text = commaFormattedTransfer(enteredAmount);
    }
  }
}

function frmMFSelectPurchaseFundMBOnDoneEditingAmount(){ 
  if (kony.string.containsChars(frmMFSelectPurchaseFundMB.TextBoxAmount.text, ["."]))
    frmMFSelectPurchaseFundMB.TextBoxAmount.text = frmMFSelectPurchaseFundMB.TextBoxAmount.text;
  else
    frmMFSelectPurchaseFundMB.TextBoxAmount.text = frmMFSelectPurchaseFundMB.TextBoxAmount.text + ".00";
  frmMFSelectPurchaseFundMB.TextBoxAmount.text = onDoneEditingAmountValue(frmMFSelectPurchaseFundMB.TextBoxAmount.text);

}

function frmMFSelectPurchaseFundMBPreShow(){
  var locale = kony.i18n.getCurrentLocale();
  var logo = "";
  var fundHouseCode = "";
  var randomnum;
 
  frmMFSelectPurchaseFundMB.lblHeader.text = kony.i18n.getLocalizedString("MF_Purchase_01");
  frmMFSelectPurchaseFundMB.LabelSelectFund.text = kony.i18n.getLocalizedString("MF_Purchase_03");
  frmMFSelectPurchaseFundMB.LabelSelectUnitHolderno.text = kony.i18n.getLocalizedString("MF_Purchase_22");
  frmMFSelectPurchaseFundMB.LabelPaymentDetail.text = kony.i18n.getLocalizedString("MF_Purchase_32");
  frmMFSelectPurchaseFundMB.LabelSelectAccount.text = kony.i18n.getLocalizedString("MF_Purchase_28");
  frmMFSelectPurchaseFundMB.LabelAmountB.text = kony.i18n.getLocalizedString("MF_Purchase_36");
  frmMFSelectPurchaseFundMB.lblTransactionOrdDate.text = kony.i18n.getLocalizedString("MF_Purchase_38");
  frmMFSelectPurchaseFundMB.LabelUnitHolderNumber.text = kony.i18n.getLocalizedString("MF_Purchase_26");
  frmMFSelectPurchaseFundMB.LabelAvailableBal.text = kony.i18n.getLocalizedString("MF_Purchase_34");
  frmMFSelectPurchaseFundMB.LabelErrorNotMatch.text = kony.i18n.getLocalizedString("MF_P_ERR_01001");
  
  frmMFSelectPurchaseFundMB.lblGetWowPoints.text = kony.i18n.getLocalizedString("GetWowPointsLabel");//MKI,MIB-11522 start
  frmMFSelectPurchaseFundMB.lblEveryWowPoints.text = kony.i18n.getLocalizedString("GetEveryPointsLabel");
  frmMFSelectPurchaseFundMB.lblBahtWowPoints.text = kony.i18n.getLocalizedString("GetBahtWowPointsLabel"); 
  frmMFSelectPurchaseFundMB.CopylblTransactionOrdDate0dd90b686980742.text = kony.i18n.getLocalizedString("wowPointsData"); 
  frmMFSelectPurchaseFundMB.CopylblTransactionOrdDate0i4ec1489f7d347.text = kony.i18n.getLocalizedString("wowPointsAmountData"); //MKI,MIB-11522 End
  kony.print("MKI 12122 1= "+gblMFOrder["avaiAmount"]);
  kony.print("MKI 12122 gblMFOrder[avaiAmount] != 0.00 ===="+gblMFOrder["avaiAmount"] != 0.00);
  if(gblMFOrder["avaiAmount"] != undefined && gblMFOrder["avaiAmount"] != 0.00){
    frmMFSelectPurchaseFundMB.CopyFlexContainer0ic09daab510d48.isVisible = true; //MKI,MIB-12122
    frmMFSelectPurchaseFundMB.CopyFlexContainer0da027117ae7642.isVisible = true; //MKI,MIB-12122           
  }
  else{
    frmMFSelectPurchaseFundMB.CopyFlexContainer0ic09daab510d48.isVisible = false; //MKI,MIB-12122
    frmMFSelectPurchaseFundMB.CopyFlexContainer0da027117ae7642.isVisible = false; //MKI,MIB-12122           
  }
  
  if (gblMFOrder["fundTypeB"] != undefined && gblMFOrder["fundTypeB"] != "" && gblMFOrder["fundTypeB"] != null) { //MKI,MIB-11522 start
        frmMFSelectPurchaseFundMB.flexWowPoints.isVisible = true;
  } else {
        frmMFSelectPurchaseFundMB.flexWowPoints.isVisible = false;
  } //MKI,MIB-11522 End
  if(gblMFOrder["orderFlow"] == MF_FUNDDETAIL_FLOW){
    frmMFSelectPurchaseFundMBSetEnableEditFund(false);
    frmMFSelectPurchaseFundMBSetHideShow(3); //Show All Widget
    randomnum = Math.floor((Math.random() * 10000) + 1);
    fundHouseCode = gblMBMFDetailsResulttable["FundHouseCode"];
    //logo = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext +
      "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+fundHouseCode+"&modIdentifier=MFLOGOS&rr="+randomnum;
	logo = loadMFIcon(fundHouseCode);
    frmMFSelectPurchaseFundMB.LabelFundCodeVal.text = gblMFOrder["fundShortName"];
    frmMFSelectPurchaseFundMB.LabelFundFullNameVal.text = locale == "th_TH" ? gblMBMFDetailsResulttable["FundNameTH"] : gblMBMFDetailsResulttable["FundNameEN"];
    frmMFSelectPurchaseFundMB.LabelUnitHolderNumberVal.text = gblMBMFDetailsResulttable["UnitHolderNo"];
    frmMFSelectPurchaseFundMB.ImageFund.src = logo;
    if(gblMFOrder["avaiAmount"] != undefined && gblMFOrder["avaiAmount"] != 0.00){
      frmMFSelectPurchaseFundMB.LabelSelectAccount.setVisibility(false);
      frmMFSelectPurchaseFundMB.FlexAvailableAmount.setVisibility(true);
      frmMFSelectPurchaseFundMB.LabelFromAccountNameVal.setVisibility(true);
     
      frmMFSelectPurchaseFundMB.LabelFromAccountNameVal.text = gblMFOrder["acctNickname"];
      frmMFSelectPurchaseFundMB.LabelAvailableBalanceVal.text = numberWithCommas(numberFormat(gblMFOrder["avaiAmount"], 2));
      frmMFSelectPurchaseFundMB.TextBoxAmount.text = onDoneEditingAmountValue(gblMFOrder["amount"]); 
    } else {
      frmMFSelectPurchaseFundMB.LabelSelectAccount.setVisibility(true);
      frmMFSelectPurchaseFundMB.FlexAvailableAmount.setVisibility(false);
      frmMFSelectPurchaseFundMB.LabelFromAccountNameVal.setVisibility(false);
      frmMFSelectPurchaseFundMB.LabelFromAccountNameVal.text = "";
      frmMFSelectPurchaseFundMB.TextBoxAmount.text = "0.00";
    }
  } else if(gblMFOrder["orderFlow"] == MF_FUNDSUMMARY_FLOW){
    frmMFSelectPurchaseFundMBSetEnableEditFund(true);
    if(gblMFOrder["fundCode"] == undefined || gblMFOrder["fundCode"] == ""){
      frmMFSelectPurchaseFundMBSetHideShow(1);

    } else if(gblMFOrder["fundCode"] != ""){
      if(gblMFOrder["unitHolderNo"] == undefined || gblMFOrder["unitHolderNo"] == ""){
        var noOfUnitHolderNo = setUnitHolderNumberListPurchase(gblMFOrder["fundHouseCode"], gblMFOrder["fundCode"], gblMFOrder["allotType"]);
        if(noOfUnitHolderNo >= 1){
          frmMFSelectPurchaseFundMBSetHideShow(2);
          randomnum = Math.floor((Math.random() * 10000) + 1);
          fundHouseCode = gblMFOrder["fundHouseCode"];
          //logo = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext +"/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+fundHouseCode+"&modIdentifier=MFLOGOS&rr="+randomnum;
		  logo = loadMFIcon(fundHouseCode);
          frmMFSelectPurchaseFundMB.ImageFund.src = logo;
          frmMFSelectPurchaseFundMB.LabelFundCodeVal.text = gblMFOrder["fundShortName"];
          frmMFSelectPurchaseFundMB.LabelFundFullNameVal.text = gblMFOrder["fundNickName"];
          if(noOfUnitHolderNo == 1) {
            var firstIndex = [0, 0];
            frmMFFilterUnitHolderNoMB.SegmentList.selectedIndex = firstIndex;
            frmMFFilterUnitHolderNoMBOnSeleteData();
          }

        } else if(noOfUnitHolderNo == 0){
          frmMFSelectPurchaseFundMBSetHideShow(1);
          frmMFSelectPurchaseFundMB.FlexErrorNoUnitHolderNo.setVisibility(true);
        } 
      } else if(gblMFOrder["fundCode"] != "" && gblMFOrder["unitHolderNo"] != ""){
        frmMFSelectPurchaseFundMBSetHideShow(3);
        frmMFSelectPurchaseFundMB.LabelUnitHolderNumberVal.text = formatUnitHolderNumer(gblMFOrder["unitHolderNo"]);
        if(gblMFOrder["avaiAmount"] != undefined && gblMFOrder["avaiAmount"] != 0.00){
          frmMFSelectPurchaseFundMB.LabelSelectAccount.setVisibility(false);
          frmMFSelectPurchaseFundMB.FlexAvailableAmount.setVisibility(true);
          frmMFSelectPurchaseFundMB.LabelFromAccountNameVal.setVisibility(true);
          
          frmMFSelectPurchaseFundMB.LabelFromAccountNameVal.text = gblMFOrder["acctNickname"];
          frmMFSelectPurchaseFundMB.LabelAvailableBalanceVal.text = numberWithCommas(numberFormat(gblMFOrder["avaiAmount"], 2));
          frmMFSelectPurchaseFundMB.TextBoxAmount.text = onDoneEditingAmountValue(gblMFOrder["amount"]); 
        } else {
          frmMFSelectPurchaseFundMB.LabelSelectAccount.setVisibility(true);
          frmMFSelectPurchaseFundMB.FlexAvailableAmount.setVisibility(false);
          frmMFSelectPurchaseFundMB.LabelFromAccountNameVal.setVisibility(false);
          frmMFSelectPurchaseFundMB.LabelFromAccountNameVal.text = "";
          frmMFSelectPurchaseFundMB.TextBoxAmount.text = "0.00";
        }
      }
    }
  } 
  frmMFSelectPurchaseFundMB.lblTransactionOrdDateVal.text = formatDateMF(GLOBAL_TODAY_DATE);

  frmMFSelectPurchaseFundMB.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
  frmMFSelectPurchaseFundMB.btnNext.text = kony.i18n.getLocalizedString("Next");

  setPurchaseStatusIndicator(frmMFSelectPurchaseFundMB);
}

function frmMFSelectPurchaseFundMBOnHide(){
  frmMFSelectPurchaseFundMB.scrollsToTop = true;
}

function frmMFSelectPurchaseFundMBOnClickNext(){
  checkValidAmountMB();
}
function frmMFSelectPurchaseFundMBOnClickBack(){
  if(gblMFOrder["orderFlow"] == MF_FUNDSUMMARY_FLOW){
    gblUnitHolderNoSelected = "";
    gblMFOrder["orderFlow"] = MF_FUNDDETAIL_FLOW;//MKI, MIB-10005
    frmMFSummary.show();
  } else if(gblMFOrder["orderFlow"] == MF_FUNDDETAIL_FLOW){
    frmMFFullStatementNFundSummary.show();
  }
}

function frmMFSelectRedeemFundMBInitiatization(){
  frmMFSelectRedeemFundMB.preShow = frmMFSelectRedeemFundMBPreShow;
  frmMFSelectRedeemFundMB.ButtonAll.onClick = frmMFSelectRedeemFundMBOnClickBtnAll;
  frmMFSelectRedeemFundMB.ButtonBaht.onClick = frmMFSelectRedeemFundMBOnClickBtnBaht;
  frmMFSelectRedeemFundMB.ButtonUnit.onClick = frmMFSelectRedeemFundMBOnClickBtnUnit;
  frmMFSelectRedeemFundMB.btnNext.onClick = frmMFSelectRedeemFundMBOnClickNext;
  frmMFSelectRedeemFundMB.btnCancel.onClick = onClickCancelMF;
  frmMFSelectRedeemFundMB.btnBack.onClick = frmMFSelectRedeemFundMBOnClickBack;
  frmMFSelectRedeemFundMB.TextBoxAmount.onTextChange = frmMFSelectRedeemFundMBOnTextChangeAmount;
  frmMFSelectRedeemFundMB.TextBoxAmount.onDone = frmMFSelectRedeemFundMBOnDoneEditingAmount;
  frmMFSelectRedeemFundMB.onDeviceBack = disableBackButton;
  frmMFSelectRedeemFundMB.btnSeleteFund.onClick = showFundListForRedeem;
}
function showFundListForRedeem(){
  setFundListForRedeem();
  frmMFSelectFundRedeemMB.show();
}
function frmMFSelectRedeemFundMBOnTextChangeAmount(){
  var enteredAmount = frmMFSelectRedeemFundMB.TextBoxAmount.text;
  if(isNotBlank(enteredAmount)) {
    enteredAmount = kony.string.replace(enteredAmount, ",", "");
    if(!(isNotBlank(enteredAmount) && enteredAmount.length > 0 && parseInt(enteredAmount, 10) == 0)){
      frmMFSelectRedeemFundMB.TextBoxAmount.text = commaFormattedTransfer(enteredAmount);
    }
  }
}

function frmMFSelectRedeemFundMBOnDoneEditingAmount(){
  if(gblMFOrder["redeemUnit"] == ORD_UNIT_BAHT){
    if (kony.string.containsChars(frmMFSelectRedeemFundMB.TextBoxAmount.text, ["."]))
      frmMFSelectRedeemFundMB.TextBoxAmount.text = frmMFSelectRedeemFundMB.TextBoxAmount.text;
    else
      frmMFSelectRedeemFundMB.TextBoxAmount.text = frmMFSelectRedeemFundMB.TextBoxAmount.text + ".00";
    frmMFSelectRedeemFundMB.TextBoxAmount.text = onDoneEditingAmountValue(frmMFSelectRedeemFundMB.TextBoxAmount.text);
  } else if(gblMFOrder["redeemUnit"] == ORD_UNIT_UNIT){
    if (kony.string.containsChars(frmMFSelectRedeemFundMB.TextBoxAmount.text, ["."]))
      frmMFSelectRedeemFundMB.TextBoxAmount.text = frmMFSelectRedeemFundMB.TextBoxAmount.text;
    else
      frmMFSelectRedeemFundMB.TextBoxAmount.text = frmMFSelectRedeemFundMB.TextBoxAmount.text + ".0000";
    frmMFSelectRedeemFundMB.TextBoxAmount.text = onDoneEditingUnitValue(frmMFSelectRedeemFundMB.TextBoxAmount.text);
  }

}
function onDoneEditingUnitValue(unit){
  if(!isNotBlank(unit) || parseFloat(unit) == 0){
    return "";
  }
  if(unit.indexOf(".") > -1) {
    return numberWithCommas(fixedToNDecimal(unit, 4));
  }
  return unit;
}
function frmMFSelectRedeemFundMBSetHideShow(setEnableAll){
  if(setEnableAll){
    frmMFSelectRedeemFundMB.FlexContainerShow.setVisibility(true);
    frmMFSelectRedeemFundMB.LabelSelectFund.setVisibility(false);
    frmMFSelectRedeemFundMB.FlexContainerDetail.setVisibility(true);
    frmMFSelectRedeemFundMB.FlexContainerTranDate.setVisibility(true);
    frmMFSelectRedeemFundMB.btnNext.setEnabled(true);
    frmMFSelectRedeemFundMB.btnNext.skin = "btnBlueBGNoRound";
    frmMFSelectRedeemFundMB.btnSeleteFund.setVisibility(false);
    frmMFSelectRedeemFundMB.ImageArrow.setVisibility(false);
  } else{
    frmMFSelectRedeemFundMB.FlexContainerShow.setVisibility(false);
    frmMFSelectRedeemFundMB.LabelSelectFund.setVisibility(true);
    frmMFSelectRedeemFundMB.FlexContainerDetail.setVisibility(false);
    frmMFSelectRedeemFundMB.FlexContainerTranDate.setVisibility(false);
    frmMFSelectRedeemFundMB.btnNext.setEnabled(false);
    frmMFSelectRedeemFundMB.btnNext.skin = "btnGreyBGNoRound";
    frmMFSelectRedeemFundMB.btnSeleteFund.setVisibility(true);
    frmMFSelectRedeemFundMB.ImageArrow.setVisibility(true);
  }
}
function frmMFSelectRedeemFundMBPreShow(){
  var locale = kony.i18n.getCurrentLocale();
  var prevForm = kony.application.getPreviousForm();
  var investmentValue = "";
  var unitF = 0.0;
  var ordUnitF = 0.0;
  var redeemableUnitF = 0.0;
  var fundHouseCode = "";
  var randomnum = 0;
  var logo = "";

  frmMFSelectRedeemFundMB.lblHeader.text = kony.i18n.getLocalizedString("MF_RDM_1");
  frmMFSelectRedeemFundMB.lblAvailableUnits.text = kony.i18n.getLocalizedString("MF_RDM_16");
  frmMFSelectRedeemFundMB.lblAvailableAmount.text = kony.i18n.getLocalizedString("MF_RDM_14");
  frmMFSelectRedeemFundMB.lblAmount.text = kony.i18n.getLocalizedString("MF_RDM_18");
  frmMFSelectRedeemFundMB.ButtonUnit.text = kony.i18n.getLocalizedString("MF_RDM_19");
  frmMFSelectRedeemFundMB.ButtonAll.text = kony.i18n.getLocalizedString("MF_RDM_20");
  frmMFSelectRedeemFundMB.ButtonBaht.text = kony.i18n.getLocalizedString("MF_RDM_21");
  frmMFSelectRedeemFundMB.lblTransactionOrdDate.text = kony.i18n.getLocalizedString("MF_RDM_25");

  if(gblMFOrder["orderFlow"] == MF_FUNDDETAIL_FLOW) {
    fundHouseCode = gblMBMFDetailsResulttable["FundHouseCode"];
    randomnum = Math.floor((Math.random() * 10000) + 1);
    //logo = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext +"/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+fundHouseCode+"&modIdentifier=MFLOGOS&rr="+randomnum;
    logo = loadMFIcon(fundHouseCode);
    frmMFSelectRedeemFundMB.ImageFund.src = logo;
    frmMFSelectRedeemFundMB.lblFundCodeVal.text = gblMFOrder["fundShortName"];
    frmMFSelectRedeemFundMB.lblUnitHolderNumber.text = gblMBMFDetailsResulttable["UnitHolderNo"];

    unitF = parseFloat(numberFormat(gblMBMFDetailsResulttable["Unit"], 4));
    if(gblFundRulesData["totalRedeemInProcess"] == undefined){
      ordUnitF = 0.00;
    } else {
      ordUnitF = numberFormat(gblFundRulesData["totalRedeemInProcess"], 4);
    }

    redeemableUnitF = unitF - ordUnitF;
    gblMFOrder["redeemableUnit"] = numberFormat(redeemableUnitF, 4);
    frmMFSelectRedeemFundMB.LabelAvailableUnitsVal.text = commaFormatted(numberFormat(unitF, 4));//MKI,MIB-12562

    investmentValue = commaFormatted(gblMBMFDetailsResulttable["InvestmentValue"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
    frmMFSelectRedeemFundMB.LabelAvailableAmountVal.text = investmentValue;

    if(gblMFOrder["redeemUnit"] == undefined || gblMFOrder["redeemUnit"] == ORD_UNIT_ALL){
      frmMFSelectRedeemFundMBOnClickBtnAll();
      frmMFSelectRedeemFundMB.TextBoxAmount.text = commaFormatted(numberFormat(redeemableUnitF, 4));//MKI,MIB-12562
    } else {
      if(gblMFOrder["redeemUnit"] == ORD_UNIT_UNIT){
        frmMFSelectRedeemFundMBOnClickBtnUnit();
        frmMFSelectRedeemFundMB.TextBoxAmount.text = onDoneEditingUnitValue(gblMFOrder["amount"]); 
      } else if(gblMFOrder["redeemUnit"] == ORD_UNIT_BAHT){
        frmMFSelectRedeemFundMBOnClickBtnBaht();
        frmMFSelectRedeemFundMB.TextBoxAmount.text = onDoneEditingAmountValue(gblMFOrder["amount"]); 
      }
    }
    frmMFSelectRedeemFundMB.lblTransactionOrdDateVal.text = formatDateMF(GLOBAL_TODAY_DATE);
    frmMFSelectRedeemFundMBSetHideShow(true);
  } else if(gblMFOrder["orderFlow"] == MF_FUNDSUMMARY_FLOW){
    frmMFSelectRedeemFundMB.LabelSelectFund.text = kony.i18n.getLocalizedString("MF_RDM_4");
    if(gblFundRulesData["fundCode"] != null && gblFundRulesData["fundCode"] != ""){
      frmMFSelectRedeemFundMBSetHideShow(true);
      frmMFSelectRedeemFundMB.btnSeleteFund.setVisibility(true);
      frmMFSelectRedeemFundMB.ImageArrow.setVisibility(true);
      fundHouseCode = gblMFOrder["fundHouseCode"];
      randomnum = Math.floor((Math.random() * 10000) + 1);
      //logo = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext +"/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+fundHouseCode+"&modIdentifier=MFLOGOS&rr="+randomnum;
      logo = loadMFIcon(fundHouseCode);
      frmMFSelectRedeemFundMB.ImageFund.src = logo;
      frmMFSelectRedeemFundMB.lblFundCodeVal.text = gblMFOrder["fundShortName"];
      frmMFSelectRedeemFundMB.lblUnitHolderNumber.text = formatUnitHolderNumer(gblMFOrder["unitHolderNo"]);

      unitF = parseFloat(numberFormat(gblMBMFDetailsResulttable["Unit"], 4));
      if(gblFundRulesData["totalRedeemInProcess"] == undefined){
        ordUnitF = 0.00;
      } else {
        ordUnitF = numberFormat(gblFundRulesData["totalRedeemInProcess"], 4);
      }
      redeemableUnitF = unitF - ordUnitF;
      gblMFOrder["redeemableUnit"] = numberFormat(redeemableUnitF, 4);
      frmMFSelectRedeemFundMB.LabelAvailableUnitsVal.text = commaFormatted(numberFormat(unitF, 4)); //MKI,MIB-12562

      investmentValue = commaFormatted(gblMBMFDetailsResulttable["InvestmentValue"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
      frmMFSelectRedeemFundMB.LabelAvailableAmountVal.text = investmentValue;

      if(gblMFOrder["redeemUnit"] == undefined || gblMFOrder["redeemUnit"] == ORD_UNIT_ALL){
        frmMFSelectRedeemFundMBOnClickBtnAll();
        frmMFSelectRedeemFundMB.TextBoxAmount.text = commaFormatted(numberFormat(redeemableUnitF, 4)); //MKI,MIB-12562
      } else {
        if(gblMFOrder["redeemUnit"] == ORD_UNIT_UNIT){
          frmMFSelectRedeemFundMBOnClickBtnUnit();
          frmMFSelectRedeemFundMB.TextBoxAmount.text = onDoneEditingUnitValue(gblMFOrder["amount"]); 
        } else if(gblMFOrder["redeemUnit"] == ORD_UNIT_BAHT){
          frmMFSelectRedeemFundMBOnClickBtnBaht();
          frmMFSelectRedeemFundMB.TextBoxAmount.text = onDoneEditingAmountValue(gblMFOrder["amount"]); 
        }
      }
      frmMFSelectRedeemFundMB.lblTransactionOrdDateVal.text = formatDateMF(GLOBAL_TODAY_DATE);

    } else {
      frmMFSelectRedeemFundMBSetHideShow(false);
    }
  }

  setRedeemStatusIndicator(frmMFSelectRedeemFundMB);
  frmMFSelectRedeemFundMB.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
  frmMFSelectRedeemFundMB.btnNext.text = kony.i18n.getLocalizedString("Next");
}

function frmMFSelectRedeemFundMBOnClickBtnAll(){
  gblMFOrder["redeemUnit"] = ORD_UNIT_ALL;
  frmMFSelectRedeemFundMB.TextBoxAmount.text = commaFormatted(gblMFOrder["redeemableUnit"]);
  frmMFSelectRedeemFundMB.TextBoxAmount.setEnabled(false);
  //frmMFSelectRedeemFundMB.TextBoxAmount.setVisibility(false);
  //frmMFSelectRedeemFundMB.LabelDescription.setVisibility(true);

  frmMFSelectRedeemFundMB.ButtonAll.skin = "btnBlueBGGreyRound20px";

  frmMFSelectRedeemFundMB.ButtonBaht.skin = "btnBlue20pxTransBackgrnd";
  frmMFSelectRedeemFundMB.ButtonUnit.skin = "btnBlue20pxTransBackgrnd";
}

function frmMFSelectRedeemFundMBOnClickBtnBaht(){
  gblMFOrder["redeemUnit"] = ORD_UNIT_BAHT;
  //frmMFSelectRedeemFundMB.TextBoxAmount.setVisibility(true);
  //frmMFSelectRedeemFundMB.LabelDescription.setVisibility(false);
  frmMFSelectRedeemFundMB.TextBoxAmount.setEnabled(true);
  frmMFSelectRedeemFundMB.TextBoxAmount.placeholder = "0.00";
  frmMFSelectRedeemFundMB.TextBoxAmount.text = "";
  frmMFSelectRedeemFundMB.TextBoxAmount.setFocus(true);

  frmMFSelectRedeemFundMB.ButtonBaht.skin = "btnBlueBGGreyRound20px";

  frmMFSelectRedeemFundMB.ButtonAll.skin = "btnBlue20pxTransBackgrnd";
  frmMFSelectRedeemFundMB.ButtonUnit.skin = "btnBlue20pxTransBackgrnd";

}

function frmMFSelectRedeemFundMBOnClickBtnUnit(){
  gblMFOrder["redeemUnit"] = ORD_UNIT_UNIT;
  //frmMFSelectRedeemFundMB.TextBoxAmount.setVisibility(true);
  //frmMFSelectRedeemFundMB.LabelDescription.setVisibility(false);
  frmMFSelectRedeemFundMB.TextBoxAmount.setEnabled(true);
  frmMFSelectRedeemFundMB.TextBoxAmount.placeholder = "0.0000";
  frmMFSelectRedeemFundMB.TextBoxAmount.text = "";
  frmMFSelectRedeemFundMB.TextBoxAmount.setFocus(true);

  frmMFSelectRedeemFundMB.ButtonUnit.skin = "btnBlueBGGreyRound20px";

  frmMFSelectRedeemFundMB.ButtonAll.skin = "btnBlue20pxTransBackgrnd";
  frmMFSelectRedeemFundMB.ButtonBaht.skin = "btnBlue20pxTransBackgrnd";

}

function frmMFSelectRedeemFundMBOnClickNext(){
  checkValidAmountMB();
}
function checkValidFundRuleMB(){
  try{
   kony.print("checkValidFundRuleMB ::: start "+ JSON.stringify(gblMFSwitchData));
  var amount = 0.0;
  if(gblMFSwitchData["effectiveDate"] != undefined &&    gblMFSwitchData["transactionDate"] != undefined) {
    effectiveDate = gblMFSwitchData["effectiveDate"].substring(0,10);
    transactionDate = gblMFSwitchData["transactionDate"].substring(0,10);
  }
  if((gblMFEventFlag == MF_EVENT_PURCHASE||gblMFEventFlag == MF_EVENT_SWI||gblMFEventFlag == MF_EVENT_SWO) && gblMFSwitchData["suitabilityExpireFlag"] == 'Y' ) {
    showAlert(kony.i18n.getLocalizedString("MF01_001"), kony.i18n.getLocalizedString("info"));
  } else if(gblMFSwitchData["allowTransFlag"] == '3'){ //MKI, MIB-12551
        showAlert(kony.i18n.getLocalizedString("MF_ERR_Overcutofftime"), kony.i18n.getLocalizedString("info")); //MKI, MIB-12551
  } 
  else if(gblMFSwitchData["allowTransFlag"] != '0'){
    showAlert(kony.i18n.getLocalizedString("MF_ERR_cutofftime"), kony.i18n.getLocalizedString("info"));
  } else if(gblMFEventFlag == MF_EVENT_REDEEM && 
            (gblMFSwitchData["allotType"] == '2' || gblMFSwitchData["allotType"] == '3')){
    showAlert(kony.i18n.getLocalizedString("MF_ERR_notallow_order"), kony.i18n.getLocalizedString("info"));
  } else if(gblMFSwitchData["fundCode"] == "") {
    showAlert("Cannot check fund suitability", kony.i18n.getLocalizedString("info"));
  } else if(effectiveDate != transactionDate){ 
    popupConfirmMutualFundOrder(callBackOnConfirmOTOrdSumFlow);
  } else {

    if(gblMFEventFlag == MF_EVENT_PURCHASE){
      if(frmMFSelectPurchaseFundMB.TextBoxAmount.text != ""){
        amount = parseFloat(removeCommos(frmMFSelectPurchaseFundMB.TextBoxAmount.text));
      }
      gblMFOrder["amount"] = numberFormat(amount, 2);
    }
    gblChannel = "MB";
    getMutualFundTnC();
     
  }
    kony.print("checkValidFundRuleMB ::: end");
  }catch(e){
     kony.print("checkValidFundRuleMB ::: error "+e);
  }
}
function callBackOnConfirmOTOrdSumFlow(){
  var amount = 0.0;
  popupConfirmation.dismiss();
  if(gblMFEventFlag == MF_EVENT_PURCHASE){
    if(frmMFSelectPurchaseFundMB.TextBoxAmount.text != ""){
      amount = parseFloat(removeCommos(frmMFSelectPurchaseFundMB.TextBoxAmount.text));
    }
    gblMFOrder["amount"] = numberFormat(amount, 2);
  }
  gblChannel = "MB";
  getMutualFundTnC();
}
function checkValidAmountMB(){
  if(gblMFEventFlag == MF_EVENT_PURCHASE){
    checkValidPurchaseAmountMB();
  } else if(gblMFEventFlag == MF_EVENT_REDEEM){
    checkValidRedeemAmountMB();
  }
}
function checkValidRedeemAmountMB(){
  var orderUnit = 0.00;
  var errMsg = "";
  var avaiUnit = parseFloat(gblMFOrder["redeemableUnit"]);
  var avaiAmount = parseFloat(removeCommos(gblMBMFDetailsResulttable["InvestmentValue"]));

  if(frmMFSelectRedeemFundMB.TextBoxAmount.text == ""){
    orderUnit = 0.00;
  } else {
    orderUnit = parseFloat(removeCommos(frmMFSelectRedeemFundMB.TextBoxAmount.text));
  }

  if(gblMBMFDetailsResulttable["Nav"] != ""){
    nav = parseFloat(removeCommos(gblMBMFDetailsResulttable["Nav"]));
    avaiAmount = parseFloat(gblMFOrder["redeemableUnit"]) * nav;
  }  

  if(frmMFSelectRedeemFundMB.ButtonUnit.skin == "btnBlueBGGreyRound20px"){
    gblMFOrder["amount"] = numberFormat(orderUnit, 4);
    errMsg = validateRedeemOrder(avaiUnit,avaiAmount, orderUnit, ORD_UNIT_UNIT,nav);
    gblMFOrder["redeemUnit"] = ORD_UNIT_UNIT;
  } else if(frmMFSelectRedeemFundMB.ButtonBaht.skin == "btnBlueBGGreyRound20px"){
    gblMFOrder["amount"] = numberFormat(orderUnit, 2);
    errMsg = validateRedeemOrder(avaiUnit,avaiAmount, orderUnit, ORD_UNIT_BAHT,nav);
    gblMFOrder["redeemUnit"] = ORD_UNIT_BAHT;
  } else{
    gblMFOrder["amount"] = numberFormat(orderUnit, 4);
    errMsg = validateRedeemOrder(avaiUnit,avaiAmount, orderUnit, ORD_UNIT_ALL,nav);
    gblMFOrder["redeemUnit"] = ORD_UNIT_ALL;
  }
  if(errMsg != ""){
    showAlert(errMsg, kony.i18n.getLocalizedString("info"));
    frmMFSelectRedeemFundMB.TextBoxAmount.text = ""; //mki, MIB 9746
    frmMFSelectRedeemFundMB.TextBoxAmount.setFocus(true);
  } else {

    if(gblMFOrder["orderFlow"] == MF_FUNDSUMMARY_FLOW){
      checkValidFundRuleMB();
    } else if(gblMFOrder["orderFlow"] == MF_FUNDDETAIL_FLOW){
      gblChannel = "MB";
      getMutualFundTnC();
    }

  }
}
function checkValidPurchaseAmountMB(){
  var amount = 0.0;
  var avaiAmount = 0.0;
  var errMsg = "";
  if(gblMFOrder["fromAccountNo"] == undefined || gblMFOrder["fromAccountNo"] == ""){
    showAlert(kony.i18n.getLocalizedString("MF01_002"), kony.i18n.getLocalizedString("info"));
  } else {
    if(frmMFSelectPurchaseFundMB.TextBoxAmount.text != ""){
      amount = parseFloat(removeCommos(frmMFSelectPurchaseFundMB.TextBoxAmount.text));
    }
    avaiAmount = gblMFOrder["avaiAmount"];

    if(amount > 0.0){
      errMsg = validatePurchaseOrder(amount, avaiAmount);

      if(errMsg != ""){
        showAlertWithCallBack(errMsg,kony.i18n.getLocalizedString("info"), callBackAmtFieldMF);
        frmMFSelectPurchaseFundMB.TextBoxAmount.text = ""; //mki, MIB 9746
        
      } else {
        gblMFOrder["amount"] = numberFormat(amount, 2);
        if(gblMFOrder["orderFlow"] == MF_FUNDSUMMARY_FLOW){
          checkValidFundRuleMB();
        } else if(gblMFOrder["orderFlow"] == MF_FUNDDETAIL_FLOW){
          gblChannel = "MB";
          getMutualFundTnC();
        }
      }

    } else {
      showAlertWithCallBack(kony.i18n.getLocalizedString("MF01_003"),kony.i18n.getLocalizedString("info"), callBackAmtFieldMF);
    }
  }
}
function callBackAmtFieldMF(){
  frmMFSelectPurchaseFundMB.TextBoxAmount.setFocus(true);
}
function frmMFSelectRedeemFundMBOnClickBack(){
  if(gblMFOrder["orderFlow"] == MF_FUNDSUMMARY_FLOW){
    gblUnitHolderNoSelected = "";
    frmMFSummary.show();
  } else if(gblMFOrder["orderFlow"] == MF_FUNDDETAIL_FLOW){
    frmMFFullStatementNFundSummary.show();
  }
}

function frmMFConfirmMBInitiatization(){
  frmMFConfirmMB.preShow = frmMFConfirmMBPreShow;
  frmMFConfirmMB.postShow = addAccessPinKeypad(frmMFConfirmMB);
  frmMFConfirmMB.btnConfirm.onClick = frmMFConfirmMBOnClickConfirm;
  frmMFConfirmMB.btnBack.onClick = frmMFConfirmMBOnClickBack;
  frmMFConfirmMB.btnCancel.onClick = onClickCancelMF;
  frmMFConfirmMB.onDeviceBack = disableBackButton;

}
function frmMFConfirmMBPreShow(){
  var amountUnit = "";
  var locale = kony.i18n.getCurrentLocale();
  frmMFConfirmMB.lblHeader.text = kony.i18n.getLocalizedString("keyConfirm");
  if(gblMFEventFlag == MF_EVENT_PURCHASE){ 
    setPurchaseStatusIndicator(frmMFConfirmMB);
    frmMFConfirmMB.imgMFicon.src = frmMFSelectPurchaseFundMB.ImageFund.src;
    frmMFConfirmMB.lblAmntlbl.text = kony.i18n.getLocalizedString("MF_PRC_lbl_Amount");
  } else if(gblMFEventFlag == MF_EVENT_REDEEM){
    setRedeemStatusIndicator(frmMFConfirmMB);
    frmMFConfirmMB.imgMFicon.src = frmMFSelectRedeemFundMB.ImageFund.src;
    frmMFConfirmMB.lblAmntlbl.text = kony.i18n.getLocalizedString("MF_RD_lbl_amount");
  }

  frmMFConfirmMB.lblUnitHolderNo.text = formatUnitHolderNumer(gblMFOrder["unitHolderNo"]);
  frmMFConfirmMB.lblMFName.text = gblMFOrder["fundShortName"];

  frmMFConfirmMB.lblFundDetail.text = (locale == "th_TH") ? gblMFOrder["fundNameTH"] : gblMFOrder["fundNameEN"];

  frmMFConfirmMB.LabelSelectAcct.text = kony.i18n.getLocalizedString("MF_PRC_lbl_Payment");
  frmMFConfirmMB.LabelTranOrdDate.text = kony.i18n.getLocalizedString("MF_LBL_TranDate");

  frmMFConfirmMB.LabelEffDate.text = kony.i18n.getLocalizedString("MF_LBL_EffectivDate");
  frmMFConfirmMB.LabeCutoff.text = kony.i18n.getLocalizedString("MF_RD_lbl_cutoff");
  frmMFConfirmMB.LabeCutoffVal.text = gblFundRulesData["timeNotAllow"].substring(0,5) + ":00";

  frmMFConfirmMB.LabelTranOrdDateVal.text = formatDateMF(GLOBAL_TODAY_DATE);
  tempDate = gblFundRulesData["effectiveDate"].substring(0,2)
  tempMonth = gblFundRulesData["effectiveDate"].substring(3,5);
  tempYear = gblFundRulesData["effectiveDate"].substring(6,10);
  effDate = tempMonth + "/" + tempDate + "/" + tempYear;
  frmMFConfirmMB.LabelEffDateVal.text = formatDateMF(effDate);

  frmMFConfirmMB.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
  frmMFConfirmMB.btnConfirm.text = kony.i18n.getLocalizedString("keyConfirm");

  if(gblMFOrder["redeemUnit"] == ORD_UNIT_ALL || gblMFOrder["redeemUnit"] == ORD_UNIT_UNIT) {
    amountUnit = kony.i18n.getLocalizedString("MF_RD_lbl_unit");
  } else {
    amountUnit = kony.i18n.getLocalizedString("currencyThaiBaht");
  }
  frmMFConfirmMB.lblAmntVal.text = commaFormatted(gblMFOrder["amount"]) + " " + amountUnit;

  if(gblMFEventFlag == MF_EVENT_PURCHASE){
    frmMFConfirmMB.FlexSelectAccount.setVisibility(true);
    frmMFConfirmMB.FlexSperatorLine1.setVisibility(true);
    frmMFConfirmMB.lblFromAccntNumVal.text = gblMFOrder["acctNickname"];
    frmMFConfirmMB.FlexMFSettleDate.setVisibility(false);
    frmMFConfirmMB.FlexMFSettleDateLine.setVisibility(false);

  } else {
    frmMFConfirmMB.FlexSelectAccount.setVisibility(false);
    frmMFConfirmMB.FlexSperatorLine1.setVisibility(false); 
    frmMFConfirmMB.FlexMFSettleDateLine.setVisibility(true);
    frmMFConfirmMB.FlexMFSettleDate.setVisibility(true);
    frmMFConfirmMB.LabelSettleDate.text = kony.i18n.getLocalizedString("MF_RDM_44");
    frmMFConfirmMB.LabelSettleDateVal.text = "T + " + gblFundRulesData["timeToRed"];
  }

}
function frmMFConfirmMBOnClickConfirm(){

  if(gblMFEventFlag == MF_EVENT_PURCHASE){
    MBmasterBillerInqMF();
  } else {
    saveToSessionMFOrder();
  }
}
function frmMFConfirmMBOnClickBack(){
  if(gblMFEventFlag == MF_EVENT_PURCHASE){
    frmMFTnc.show();

  } else if (gblMFEventFlag == MF_EVENT_REDEEM){
    frmMFSelectRedeemFundMB.show();
  }
}
function frmMFCompleteMBInitiatization(){
  frmMFCompleteMB.preShow = frmMFCompleteMBPreShow;
  frmMFCompleteMB.btnPortfolio.onClick = frmMFCompleteMBOnClickPortfolio;
  frmMFCompleteMB.btnMainMenu.onClick  = handleMenuBtn;
  frmMFCompleteMB.onDeviceBack = disableBackButton;
  frmMFCompleteMB.btnWowClose.onClick = onClickbtnWowClose;
   //#ifdef iphone
     frmMFCompleteMB.RichTxtGetWowCompleteScreen.maxHeight = "70%";
     frmMFCompleteMB.RichTxtGetWowCompleteScreen.skin=richTxtWhite80;
  //#endif
  //#ifdef android
  	frmMFCompleteMB.RichTxtGetWowCompleteScreen.maxHeight = "50Dp";
    frmMFCompleteMB.RichTxtGetWowCompleteScreen.skin=richTxtWhite160;
    
  //#endif
}                              

function onClickbtnWowClose(){
  frmMFCompleteMB.flexPopupWow.isVisible = false;
}
function frmMFCompleteMBPreShow(){
  var locale = kony.i18n.getCurrentLocale();
  frmMFCompleteMB.lblHeader.text = kony.i18n.getLocalizedString("keylblComplete");
  frmMFCompleteMB.flexWowUi.showFadingEdges  = false;
  frmMFCompleteMB.LabelSelectAcct.text = kony.i18n.getLocalizedString("MF_PRC_lbl_Payment");
  frmMFCompleteMB.LabelTranOrdDate.text = kony.i18n.getLocalizedString("MF_LBL_TranDate");
  frmMFCompleteMB.LabelEffDate.text = kony.i18n.getLocalizedString("MF_LBL_EffectivDate");
  frmMFCompleteMB.btnWowClose.text = kony.i18n.getLocalizedString("keyClose");
  frmMFCompleteMB.btnPortfolio.text = kony.i18n.getLocalizedString("MF_btn_back");
  frmMFCompleteMB.lblRefNo.text = kony.i18n.getLocalizedString("MF_Confrim_LBL_TransactionRefNo");
  
  frmMFCompleteMB.RichTxtGetWowCompleteScreen.text = kony.i18n.getLocalizedString("get30WowMessageCompletescreen"); //MKI,MIB-11522 
  
  if(gblMFEventFlag == MF_EVENT_PURCHASE){
    frmMFCompleteMB.imgMFicon.src = frmMFSelectPurchaseFundMB.ImageFund.src;
    frmMFCompleteMB.lblAmntlbl.text = kony.i18n.getLocalizedString("MF_PRC_lbl_Amount");
  } else if(gblMFEventFlag == MF_EVENT_REDEEM){
    frmMFCompleteMB.imgMFicon.src = frmMFSelectRedeemFundMB.ImageFund.src;
    frmMFCompleteMB.lblAmntlbl.text = kony.i18n.getLocalizedString("MF_RD_lbl_amount");
  }
  frmMFCompleteMB.lblUnitHolderNo.text = formatUnitHolderNumer(gblMFOrder["unitHolderNo"]);
  frmMFCompleteMB.lblMFName.text = gblMFOrder["fundShortName"];
  frmMFCompleteMB.lblFundDetail.text = locale == "th_TH" ? gblMFOrder["fundNameTH"] : gblMFOrder["fundNameEN"];
  //frmMFCompleteMB.lblAmntVal.text = frmMFConfirmMB.lblAmntVal.text;
  if(gblMFOrder["redeemUnit"] == ORD_UNIT_ALL || gblMFOrder["redeemUnit"] == ORD_UNIT_UNIT) {
    amountUnit = kony.i18n.getLocalizedString("MF_RD_lbl_unit");
  } else {
    amountUnit = kony.i18n.getLocalizedString("currencyThaiBaht");
  }
  frmMFCompleteMB.lblAmntVal.text = commaFormatted(gblMFOrder["amount"]) + " " + amountUnit;
  if(gblMFOrder["orderType"] == MF_ORD_TYPE_REDEEM){
    if(GBL_MF_TEMENOS_ENABLE_FLAG != "ON" && gblMFOrder["fundHouseCode"] == "TMBAM") {
      frmMFCompleteMB.FlexMFPaymentMethod.setVisibility(true);
      frmMFCompleteMB.FlexMFPaymentMethodLine.setVisibility(true);

      if(gblMFOrder["paymentType"] != undefined || gblMFOrder["paymentType"] != ""){
        frmMFCompleteMB.LabelPaymentMethod.text = kony.i18n.getLocalizedString("RD_lbl_method");
        if(gblMFOrder["paymentType"] == "TR") {
          frmMFCompleteMB.LabelPaymentMethodVal.text = kony.i18n.getLocalizedString("RD_lbl_transfer") + gblMFOrder["paymentDetail"];
        }else if (gblMFOrder["paymentType"] == "CQ"){
          frmMFCompleteMB.LabelPaymentMethodVal.text = kony.i18n.getLocalizedString("RD_lbl_cheque");
        }
      } else {
        frmMFCompleteMB.FlexMFPaymentMethod.setVisibility(false);
        frmMFCompleteMB.FlexMFPaymentMethodLine.setVisibility(false);
      }

    } else {
      frmMFCompleteMB.FlexMFPaymentMethod.setVisibility(false);
      frmMFCompleteMB.FlexMFPaymentMethodLine.setVisibility(false);
    }
    frmMFCompleteMB.FlexMFSelectAccount.setVisibility(false);
    frmMFCompleteMB.FlexMFSelectAccountLine.setVisibility(false);
    frmMFCompleteMB.FlexMFSettleDate.setVisibility(true);
    frmMFCompleteMB.FlexMFSettleDateLine.setVisibility(true);
    frmMFCompleteMB.LabelSettleDate.text = kony.i18n.getLocalizedString("MF_RDM_44");
    frmMFCompleteMB.LabelSettleDateVal.text = "T + " + gblFundRulesData["timeToRed"];
    setRedeemStatusIndicator(frmMFCompleteMB);
  } else if(gblMFOrder["orderType"] == MF_ORD_TYPE_PURCHASE){
    frmMFCompleteMB.FlexMFSelectAccount.setVisibility(true);
    frmMFCompleteMB.FlexMFSelectAccountLine.setVisibility(true);
    frmMFCompleteMB.LabelSelectAcctVal.text = gblMFOrder["acctNickname"];
    frmMFCompleteMB.FlexMFSettleDate.setVisibility(false);
    frmMFCompleteMB.FlexMFSettleDateLine.setVisibility(false);
    frmMFCompleteMB.FlexMFPaymentMethod.setVisibility(false);
    frmMFCompleteMB.FlexMFPaymentMethodLine.setVisibility(false);
    setPurchaseStatusIndicator(frmMFCompleteMB);
  }

  frmMFCompleteMB.lblRefNoVal.text = gblMFOrder["refID"];
  frmMFCompleteMB.LabelTranOrdDateVal.text = formatDateMF(gblMFOrder["orderDate"]) + " " + gblMFOrder["orderTime"];
  frmMFCompleteMB.LabelEffDateVal.text = formatDateMF(gblMFOrder["effectiveDate"]);
  if(gblMFOrder["fundTypeB"] != undefined && gblMFOrder["fundTypeB"] != "" && gblMFOrder["fundTypeB"] != null && parseInt(gblMFOrder["amount"]) >= 100000){ //MKI,MIB-11522 start
    frmMFCompleteMB.flexPopupWow.isVisible = true;
    gblMFOrder["fundTypeB"] = null;
  }else{
    frmMFCompleteMB.flexPopupWow.isVisible = false;
  } //MKI,MIB-11522 End
}
function frmMFCompleteMBOnClickPortfolio(){
  MBcallMutualFundsSummary();
}
function frmMFCompleteMBOnClickBack(){
  if(gblMFEventFlag == MF_EVENT_PURCHASE){
    frmMFSelectPurchaseFundMB.show();
  }
}

function onClickCancelMF(){
  if(gblMFOrder["orderFlow"] == MF_FUNDSUMMARY_FLOW){
    MBcallMutualFundsSummary();
  } else if(gblMFOrder["orderFlow"] == MF_FUNDDETAIL_FLOW){
    kony.print("mki gblMFOrder blank 4");
    gblMFOrder = {};
    frmMFFullStatementNFundSummary.show();
  }
}
function showMutualFundsPwdPopupForProcess() {
  var lblText = kony.i18n.getLocalizedString("transPasswordSub");
  var refNo = "";
  var mobNO = "";
  if(gblAuthAccessPin == true){
    showAccesspinPopup();
  }else{
    showLoadingScreen();
    showOTPPopup(lblText, refNo, mobNO, onClickMutualFundsConfirmPop, 3);  
  }
  
}

function onClickMutualFundsConfirmPop(tranPassword) {
  if (isNotBlank(popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text)) {
    showLoadingScreen(); 
    popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("keyTransPwdMsg");
    popupTractPwd.lblPopupTract7.skin = lblPopupLabelTxt;
    popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = tbxPopupBlue;
    popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = tbxPopupBlue;
    popupTractPwd.hbxPopupTranscPwd.skin = tbxPopupBlue;

    MBconfirmMFOrder(tranPassword);	


  } else{
    setTransPwdFailedError(kony.i18n.getLocalizedString("emptyMBTransPwd"));
    return false;
  }
}

function onSelectFromAccntMufutalFund() {
  showLoadingScreen();
  var inputParam = {}
  inputParam["billPayInd"] = "billPayInd";
  if(gblFundRulesData["allotType"] == '2' || gblFundRulesData["allotType"] == '3'){
     inputParam["ccBillPay"] = "Y";  
  }
  invokeServiceSecureAsync("customerAccountInquiry", inputParam, mutualFundCustomerAccountCallBack);
}

function mutualFundCustomerAccountCallBack(status, resulttable) {
  var selectedIndex = 0;
  if (status == 400) //success responce
  {
    if (resulttable["opstatus"] == 0) {
      /** checking for Soap status below  */
      var StatusCode = resulttable["statusCode"];

      //	if( resulttable["StatusCode"] !=null &&   resulttable["StatusCode"] == "0"){
      var fromData = []
      var j = 1
      var nonCASAAct = 0;
      gbltranFromSelIndex = [0, 0];

      for (var i = 0; i < resulttable.custAcctRec.length; i++) {
        var accountStatus = resulttable["custAcctRec"][i].acctStatus;
        if (accountStatus.indexOf("Active") == -1) {
          nonCASAAct = nonCASAAct + 1;
          //showAlertWithCallBack(kony.i18n.getLocalizedString("MB_StatusNotEligible"), kony.i18n.getLocalizedString("info"),onclickActivationCompleteStart);
          //return false;
        }
        if (accountStatus.indexOf("Active") >= 0) {
          var icon = "";
          var iconcategory = "";
          var CCImagetype  = resulttable.custAcctRec[i]["ProductImg"];
          if (resulttable.custAcctRec[i].personalisedAcctStatusCode == "02") {
            continue; //do not populate if account is deleted
          }
         // icon = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + "NEW_" + resulttable.custAcctRec[i]["ICON_ID"] + "&modIdentifier=PRODICON";
          icon=loadFromPalleteIcons(resulttable["custAcctRec"][i]["ICON_ID"]); //mki, mib-11372
          kony.print("^^^^^Mutual fund MB^^^^^"+icon);
          iconcategory = resulttable.custAcctRec[i]["ICON_ID"];
          if (iconcategory == "ICON-01" || iconcategory == "ICON-02"|| iconcategory == "ICON-03"|| iconcategory == "ICON-04"){
            if (iconcategory == "ICON-01" || iconcategory == "ICON-02") {
            	var temp = createSegmentRecordBills(resulttable.custAcctRec[i], hbxSliderNew1, icon)
            } else if (iconcategory == "ICON-03") {
              var temp = createSegmentRecordBills(resulttable.custAcctRec[i], hbxSliderNew2, icon)
            } else if (iconcategory == "ICON-04") {
                var temp = createSegmentRecordBills(resulttable.custAcctRec[i], hbxSliderNew3, icon)
            }
          } else if(iconcategory == "ICON-06" || iconcategory == "ICON-07"){
            kony.print("esult for 06 and 07 "+JSON.stringify(resulttable.custAcctRec[i]));
            if(CCImagetype == "SoSmart"){
              var temp = createSegmentRecordBills(resulttable.custAcctRec[i], hbxSliderSoSmart, icon)
            }else if(CCImagetype == "SoChill"){
              var temp = createSegmentRecordBills(resulttable.custAcctRec[i], hbxSliderSoChill, icon)
            }else if(CCImagetype == "SoFast"){
              var temp = createSegmentRecordBills(resulttable.custAcctRec[i], hbxSliderSofast, icon)
            }else if(CCImagetype == "RoyalTopBrass"){
              var temp = createSegmentRecordBills(resulttable.custAcctRec[i], hbxSliderTopBrass, icon)
            }else if(CCImagetype == "VisaDefault"){
              var temp = createSegmentRecordBills(resulttable.custAcctRec[i], hbxSliderDefaultVisa, icon)
            }else if(CCImagetype == "MasterDefault"){
              var temp = createSegmentRecordBills(resulttable.custAcctRec[i], hbxSliderDefaultMaster, icon)
            }else 
              var temp = createSegmentRecordBills(resulttable.custAcctRec[i], hbxSliderDefaultVisa, icon)
            }
           
          }
          
          // added for removing the joint account and others for SPA and MB
          var jointActXfer = resulttable.custAcctRec[i].partyAcctRelDesc;
          if (jointActXfer == "PRIJNT" || jointActXfer == "SECJNT" || jointActXfer == "OTHJNT" || jointActXfer == "SECJAN") {

          } else {

            kony.table.insert(fromData, temp[0])
          }
          j++;
        }
      gblNoOfFromAcs = fromData.length;
      for (var i = 0; i < gblNoOfFromAcs; i++) {
        if (glb_accId == fromData[i].accountNo) {
          selectedIndex = i;
          glb_accId = 0;
          break;
        }
      }
      if (selectedIndex != 0)
        gbltranFromSelIndex = [0, selectedIndex];
      else
        gbltranFromSelIndex = [0, 0];


      frmSelAccntMutualFund.segTransFrm.widgetDataMap = [];
      frmSelAccntMutualFund.segTransFrm.widgetDataMap = {
        lblAcntType: "lblAcntType",
        img1: "img1",
        lblCustName: "lblCustName",
        lblBalance: "lblBalance",
        lblActNoval: "lblActNoval",
        lblDummy: "lblDummy",
        lblSliderAccN2: "lblSliderAccN2",
        lblRemainFee: "lblRemainFee",
        lblRemainFeeValue: "lblRemainFeeValue"
      }
      frmSelAccntMutualFund.segTransFrm.data = [];
      //frmBillPayment.segSlider.data=fromData;
      if (nonCASAAct == resulttable.custAcctRec.length) {
        showAlertWithCallBack(kony.i18n.getLocalizedString("MB_StatusNotEligible"), kony.i18n.getLocalizedString("info"), onClickOfAccountDetailsBack);
        return false;
      }
      if (fromData.length == 0) {
        frmSelAccntMutualFund.segTransFrm.setVisibility(false);
        showAlertWithCallBack(kony.i18n.getLocalizedString("MB_CommonError_NoSA"), kony.i18n.getLocalizedString("info"), onClickOfAccountDetailsBack);
        return false;
      } else {
        if (gblDeviceInfo["name"] == "android") {
          if (fromData.length == 1) {
            frmSelAccntMutualFund.segTransFrm.viewConfig = {
              "coverflowConfig": {
                "rowItemRotationAngle": 0,
                "isCircular": false,
                "spaceBetweenRowItems": 10,
                "projectionAngle": 90,
                "rowItemWidth": 80
              }
            };
          } else {
            frmSelAccntMutualFund.segTransFrm.viewConfig = {
              "coverflowConfig": {
                "rowItemRotationAngle": 0,
                "isCircular": true,
                "spaceBetweenRowItems": 10,
                "projectionAngle": 90,
                "rowItemWidth": 80
              }
            };
          }
        }
        frmSelAccntMutualFund.segTransFrm.data = fromData;
        frmSelAccntMutualFund.segTransFrm.selectedIndex = gbltranFromSelIndex;
      }
      //billpaymentNoOfAccounts = fromData.length;
      dismissLoadingScreen();
      frmSelAccntMutualFund.LabelHeader.text = kony.i18n.getLocalizedString("MF_PRC_lbl_select_Account"); 
      frmSelAccntMutualFund.btnTransCnfrmCancel.text = kony.i18n.getLocalizedString("keyOK"); 

      frmSelAccntMutualFund.show();
    } else {
      dismissLoadingScreen();
      showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
    }
  } else {
    dismissLoadingScreen();
  }
}

function getMutualfundIndex() {}
getMutualfundIndex.prototype.paginationSwitch = function (sectionIndex, rowIndex) {

  var segdata = frmSelAccntMutualFund.segTransFrm.data;
  gbltdFlag = segdata[rowIndex]["tdFlag"];
  rowIndex = parseFloat(rowIndex);
  frmSelAccntMutualFund.segTransFrm.selectedIndex = [0, rowIndex];
  gbltranFromSelIndex = frmSelAccntMutualFund.segTransFrm.selectedIndex;
  //gblSelTransferFromAcctNo = frmSelAccntMutualFund.segTransFrm.selectedItems[0].accountNum;

	if(gbleDonationType == "QR_Type" || gbleDonationType == "Barcode_Type"){
      kony.print("Inside here QR");
      checkAmountQRPay_donation();
    }
}

function onClickCoverFlowMutualFundMB() {
  try{
    kony.print("previous form>>"+kony.application.getPreviousForm().id);
    if(kony.application.getPreviousForm().id == "frmSend2SaveCreateSmartRequest"){
        setAccountDataForSaveAlert();
    }else{
      var current
      var avaiLength = 0;
      var avaiAmount = "";
      gbltranFromSelIndex = frmSelAccntMutualFund.segTransFrm.selectedIndex;
      gbltdFlag = frmSelAccntMutualFund.segTransFrm.selectedItems[0].tdFlag;
      if(gbltdFlag == "CCA"){
        gblSelTransferFromAcctNo = frmSelAccntMutualFund.segTransFrm.selectedItems[0].accountNo;
      }else{
        gblSelTransferFromAcctNo = frmSelAccntMutualFund.segTransFrm.selectedItems[0].lblActNoval;
      }

      //toggleTDMaturityCombobox(gbltdFlag);
      frmMFSelectPurchaseFundMB.lblAccountNickName.text = frmSelAccntMutualFund.segTransFrm.selectedItems[0].lblCustName;
      //frmMFSelectPurchaseFundMB.LabelAvaiBalVal.text = frmSelAccntMutualFund.segTransFrm.selectedItems[0].lblBalance;

      avaiLength = frmSelAccntMutualFund.segTransFrm.selectedItems[0].lblBalance.length;
      avaiAmount = frmSelAccntMutualFund.segTransFrm.selectedItems[0].lblBalance.substring(0, avaiLength - 2);
      gblMFOrder["avaiAmount"] = parseFloat(removeCommos(avaiAmount));
      gblMFOrder["fromAccountNo"] = gblSelTransferFromAcctNo;
      gblMFOrder["acctNickname"] =frmSelAccntMutualFund.segTransFrm.selectedItems[0].lblCustName;
      gblMFOrder["acctType"] = frmSelAccntMutualFund.segTransFrm.selectedItems[0].lblAcntType

      frmMFSelectPurchaseFundMB.LabelAvaiBalVal.text = numberFormat(gblMFOrder["avaiAmount"], 2);
       gbliSTextBoxAmountFocusTrue = true; // mki, MIB-10909
      kony.print("MKI 12122 true");
      frmMFSelectPurchaseFundMB.CopyFlexContainer0ic09daab510d48.setVisibility(true); //MKI,MIB-12122
      frmMFSelectPurchaseFundMB.CopyFlexContainer0da027117ae7642.setVisibility(true); //MKI,MIB-12122
      frmMFSelectPurchaseFundMB.show();
    }
  }
  catch(ex)
    {
      kony.print("MKi 12122 ex: "+ ex.message);
    }
}

function MBmasterBillerInqMF() {
  showLoadingScreen();
  var billerGroupType = "";
  billerGroupType = "2";
  var inputParams = {
    IsActive: "1",
    BillerGroupType: billerGroupType,
    clientDate: getCurrentDate(),
    flagBillerList : "MB"
  };

  invokeServiceSecureAsync("masterBillerInquiry", inputParams, MBmasterBillerInqMFCallback);
}

function MBmasterBillerInqMFCallback(status, callBackResponse) {
  var toAccountKey="";
  var BillerCompcode="";
  if (status == 400) {
    if (callBackResponse["opstatus"] == "0") {

      var myBillList = callBackResponse["MasterBillerInqRs"];
      if (myBillList.length > 0) {
        var fundHouseCode = gblMFOrder["fundHouseCode"];
        if(GBL_MF_TEMENOS_ENABLE_FLAG != "ON" && fundHouseCode == "TMBAM"){ // mki, MIB-11710
          fundHouseCode = gblFundRulesData["fundCode"]
        }	

        for (var i = 0; i < myBillList.length; i++) {

          if ((myBillList[i]["BillerShortName"] == fundHouseCode) && (myBillList[i]["BillerNameTH"] == fundHouseCode)) {
            gblMFOrder["MFtoAccountKey"] = myBillList[i]["ToAccountKey"];
            gblMFOrder["MFBillerCompcode"] = myBillList[i]["BillerCompcode"];
            break;
          }
        }
        dismissLoadingScreen();
        saveToSessionMFOrder();
      } else {		
        dismissLoadingScreen();
      }
    } else {
      dismissLoadingScreen();
      alert(kony.i18n.getLocalizedString("ECGenericError"));
    }
  } else {
    if (status == 300) {
      dismissLoadingScreen();
      alert(kony.i18n.getLocalizedString("ECGenericError"));
    }
  }
}

function setRedeemStatusIndicator(currentFormId){
  //var currentFormId = kony.application.getCurrentForm();
  currentFormId.lblstep1.text = kony.i18n.getLocalizedString("MF_RD_INX1");
  currentFormId.lblstep2.text = kony.i18n.getLocalizedString("MF_RD_INX2");
  currentFormId.lblstep3.text = kony.i18n.getLocalizedString("MF_RD_INX3");
  currentFormId.lblstep4.text = kony.i18n.getLocalizedString("MF_RD_INX4");
}

function setPurchaseStatusIndicator(currentFormId){
  currentFormId.lblstep1.text = kony.i18n.getLocalizedString("MF_PRC_lbl_Select_Fund");
  currentFormId.lblstep2.text = kony.i18n.getLocalizedString("MF_PRC_lbl_Accept");
  currentFormId.lblstep3.text = kony.i18n.getLocalizedString("MF_PRC_lbl_Submit");
  currentFormId.lblstep4.text = kony.i18n.getLocalizedString("MF_PRC_lbl_Done");
}

function frmMFFailedTransactionMBInitiatization(){
  frmMFFailedTransactionMB.preShow = frmMFFailedTransactionMBPreShow;
  frmMFFailedTransactionMB.btnMainMenu.onClick  = handleMenuBtn;
  frmMFFailedTransactionMB.onDeviceBack = disableBackButton;
  if(gblMFEventFlag == MF_EVENT_PURCHASE || gblMFEventFlag == MF_EVENT_REDEEM){
    frmMFFailedTransactionMB.btnTryAgain.onClick = onClickCancelMF;
  }else{
    frmMFFailedTransactionMB.btnTryAgain.onClick = onclicktryagain;
  }
  frmMFFailedTransactionMB.btnCallCC.onClick = CallTheNumber;
}

function frmMFFailedTransactionMBPreShow(){
  frmMFFailedTransactionMB.lblFailed.text = kony.i18n.getLocalizedString("MF_FailedTrans");
  if(gblMFEventFlag == MF_EVENT_PURCHASE){
    frmMFFailedTransactionMB.lblTransStatus.text = kony.i18n.getLocalizedString("MF_TTL_Purchase") + " " + kony.i18n.getLocalizedString("MF_FailedTrans");
  } else if(gblMFEventFlag == MF_EVENT_REDEEM){
    frmMFFailedTransactionMB.lblTransStatus.text = kony.i18n.getLocalizedString("MF_RD_lbl_redeem") + " " + kony.i18n.getLocalizedString("MF_FailedTrans");
  }else{
    frmMFFailedTransactionMB.lblTransStatus.text = kony.i18n.getLocalizedString("MF_Cancel_Fail")
  }
  frmMFFailedTransactionMB.btnTryAgain.text = kony.i18n.getLocalizedString("btnTryAgain");
  frmMFFailedTransactionMB.btnCallCC.text = kony.i18n.getLocalizedString("keyCall");
}

function frmMFSelectFundRedeemMBInit(){
  frmMFSelectFundRedeemMB.preShow = frmMFSelectFundRedeemMBPreShow;
  frmMFSelectFundRedeemMB.SegmentFundList.onRowClick = callMBMutualFundsDetailsForSelectFund;
  frmMFSelectFundRedeemMB.ButtonClose.onClick = frmMFSelectFundRedeemMBClosePopup;
  frmMFSelectFundRedeemMB.onDeviceBack = disableBackButton;
}
function frmMFSelectFundRedeemMBPreShow(){
  var locale = kony.i18n.getCurrentLocale();
  frmMFSelectFundRedeemMB.LabelSelectFund.text = kony.i18n.getLocalizedString("MF_RDM_8");
}
function frmMFSelectFundRedeemMBClosePopup(){
  frmMFSelectRedeemFundMB.show();
}

function frmMFSeachFundPurchaseMBInit(){
  frmMFSeachFundPurchaseMB.preShow = frmMFSeachFundPurchaseMBPreShow;
   frmMFSeachFundPurchaseMB.postShow = frmMFSeachFundPurchaseMBpostShow;//mki, MIB-9647
  frmMFSeachFundPurchaseMB.SegmentFundList.onRowClick = frmMFSeachFundPurchaseMBonClickOnSegment;
  //frmMFSeachFundPurchaseMB.ButtonClose.onClick = frmMFSelectFundRedeemMBClosePopup;
  frmMFSeachFundPurchaseMB.ButtonFeatured.onClick = frmMFSeachFundPurchaseMBonClickFeatured;
  frmMFSeachFundPurchaseMB.ButtonSearch.onClick = frmMFSeachFundPurchaseMBonClickSearch;
  frmMFSeachFundPurchaseMB.FlexFilterFundHouse.onClick = frmMFSeachFundPurchaseMBonClickFilterFundHouse;
  frmMFSeachFundPurchaseMB.SegmentFundHouseList.onRowClick = frmMFSeachFundPurchaseMBonClickOnSegmentFundHouse;
  frmMFSeachFundPurchaseMB.txtSearch.onTextChange = frmMFSeachFundPurchaseMBSearchTextChange;
  frmMFSeachFundPurchaseMB.ButtonClear.onClick = frmMFSeachFundPurchaseMBClearSearch;
  frmMFSeachFundPurchaseMB.onDeviceBack = disableBackButton;
  frmMFSeachFundPurchaseMB.ButtonClose.onClick = frmMFSeachFundPurchaseMBClosePopup;
}
function frmMFSeachFundPurchaseMBpostShow(){ //mki, MIB-9647
  frmMFSeachFundPurchaseMB.FlexScrollListFund.setContentOffset({"x":"0dp", "y":"0dp"});
}
function frmMFSeachFundPurchaseMBPreShow(){
  var locale = kony.i18n.getCurrentLocale();
  frmMFSeachFundPurchaseMB.LabelFundHouseName.text = kony.i18n.getLocalizedString("MF_RDM_20");
  frmMFSeachFundPurchaseMB.LabelFundHouse.text = kony.i18n.getLocalizedString("MF_Purchase_08");
  frmMFSeachFundPurchaseMB.LabelSelectFund.text = kony.i18n.getLocalizedString("MF_Purchase_06");
  frmMFSeachFundPurchaseMB.ButtonFeatured.text = kony.i18n.getLocalizedString("MF_Purchase_10");
  frmMFSeachFundPurchaseMB.ButtonSearch.text = kony.i18n.getLocalizedString("MF_Purchase_11");
  frmMFSeachFundPurchaseMB.txtSearch.placeholder = kony.i18n.getLocalizedString("MF_Purchase_18");
  frmMFSeachFundPurchaseMB.ButtonClear.text = "Clear";//kony.i18n.getLocalizedString("keyCall");
  gblMFOrder["fundHouseCode"] = ""; 
  frmMFSeachFundPurchaseMBClearSearch();
  frmMFSeachFundPurchaseMBonClickFeatured();
  frmMFSeachFundPurchaseMB.FlexScrollFundHouse.setVisibility(false);
}
function frmMFSeachFundPurchaseMBClosePopup(){
  frmMFSelectPurchaseFundMB.show();
}
function frmMFSeachFundPurchaseMBonClickFilterFundHouse(){
  if(!frmMFSeachFundPurchaseMB.FlexScrollFundHouse.isVisible){
    frmMFSeachFundPurchaseMB.FlexScrollFundHouse.setVisibility(true);
    frmMFSeachFundPurchaseMB.FlexContainerSearch.setEnabled(false);
    setFundHouseListForPurchase();

  } else {
    frmMFSeachFundPurchaseMB.FlexScrollFundHouse.setVisibility(false);
    frmMFSeachFundPurchaseMB.FlexContainerSearch.setEnabled(true);
  }
}

function frmMFSeachFundPurchaseMBonClickOnSegment(){
try{
  var selectedIndex = frmMFSeachFundPurchaseMB.SegmentFundList.selectedIndex[1];
  var fundDataObject = frmMFSeachFundPurchaseMB.SegmentFundList.data[selectedIndex];
  var unitholder = "";
  var fundCode = "";
  var funClassCode = "";
	kony.print("mki frmMFSeachFundPurchaseMBonClickOnSegment fundDataObject.imgWowPoint="+fundDataObject.imgWowPoint);
  gblMFOrder["fundCode"] = fundDataObject.FundCode;
  gblMFOrder["fundHouseCode"] = fundDataObject.FundHouseCode;
  gblMFOrder["fundNickName"] = fundDataObject.LabelFundFullName;
  gblMFOrder["fundNameTH"] = fundDataObject.FundNameTH;
  gblMFOrder["fundNameEN"] = fundDataObject.FundNameEN;
  gblMFOrder["allotType"] = fundDataObject.AllotType;
  gblMFOrder["fundTypeB"] = fundDataObject.imgWowPoint; //MKI,MIB-11522 //Based on image name(If not null) we will verify if Fund is type B or not.
  gblMFOrder["fundShortName"] = fundDataObject.FundShortName;
  gblMFOrder["unitHolderNo"] = "";

  frmMFSelectPurchaseFundMB.show();
}
  catch(ex)
    {
      kony.print("MKI ex:"+ ex.message);
    }
}
function frmMFSeachFundPurchaseMBonClickOnSegmentFundHouse(){
  var locale = kony.i18n.getCurrentLocale();
  var selectedIndex = frmMFSeachFundPurchaseMB.SegmentFundHouseList.selectedIndex[1];
  var fundDataObject = frmMFSeachFundPurchaseMB.SegmentFundHouseList.data[selectedIndex];
  var fundHouseName = fundDataObject.LabelFundHouse;
  var fundHouseCode = fundDataObject.fundHouseCode;
  var fundimg = fundDataObject.ImageFund;
  gblMFOrder["fundHouseCode"] = fundHouseCode;
  frmMFSeachFundPurchaseMBClearSearch();
  if(fundimg != "") { 
    setFundListForPurchase(fundHouseCode,"");
    frmMFSeachFundPurchaseMB.LabelFundHouseName.text = fundHouseName;
  } else {
    setFundListForPurchase("","");
    frmMFSeachFundPurchaseMB.LabelFundHouseName.text = kony.i18n.getLocalizedString("MF_RDM_20");
  }


  frmMFSeachFundPurchaseMB.FlexScrollFundHouse.setVisibility(false);
  frmMFSeachFundPurchaseMB.FlexContainerSearch.setEnabled(true);
}

function frmMFSeachFundPurchaseMBClosePopup(){
  frmMFSelectPurchaseFundMB.show();
}
function frmMFSeachFundPurchaseMBonClickFeatured(){

  var fundHouseCode = ""; 
  if(gblMFOrder["fundHouseCode"] == undefined || gblMFOrder["fundHouseCode"] == "" || gblMFOrder["fundHouseCode"] == kony.i18n.getLocalizedString("MF_RDM_20")){
    fundHouseCode = "";
  } else {
    fundHouseCode = gblMFOrder["fundHouseCode"];
  }
  kony.print("frmMFSeachFundPurchaseMBonClickFeatured fundHouseCode="+fundHouseCode);
  setFundListForPurchase(fundHouseCode, "");

  frmMFSeachFundPurchaseMB.FlexSearch.setVisibility(false);
  frmMFSeachFundPurchaseMB.FlexScrollListFund.height = "90%";
  frmMFSeachFundPurchaseMB.ButtonFeatured.skin = "btnblack20px";
  frmMFSeachFundPurchaseMB.ButtonFeatured.focusSkin = "btnblack20px";
  frmMFSeachFundPurchaseMB.FlexFeaturedLine.skin = "flexLinedarkBlack";

  frmMFSeachFundPurchaseMB.ButtonSearch.skin = "btnBlue160";
  frmMFSeachFundPurchaseMB.ButtonSearch.focusSkin = "btnBlue160";
  frmMFSeachFundPurchaseMB.FlexSearchLine.skin = "flexLineBlue";
}
function frmMFSeachFundPurchaseMBonClickSearch(){
  frmMFSeachFundPurchaseMB.SegmentFundList.removeAll()
  frmMFSeachFundPurchaseMB.FlexSearch.setVisibility(true);
  frmMFSeachFundPurchaseMB.FlexScrollListFund.height = "80%";
  frmMFSeachFundPurchaseMB.ButtonFeatured.skin = "btnBlue160";
  frmMFSeachFundPurchaseMB.ButtonFeatured.focusSkin = "btnBlue160";
  frmMFSeachFundPurchaseMB.FlexFeaturedLine.skin = "flexLineBlue";

  frmMFSeachFundPurchaseMB.ButtonSearch.skin = "btnblack20px";
  frmMFSeachFundPurchaseMB.ButtonSearch.focusSkin = "btnblack20px";
  frmMFSeachFundPurchaseMB.FlexSearchLine.skin = "flexLinedarkBlack";
}

function frmMFFilterUnitHolderNoMBInit(){
  frmMFFilterUnitHolderNoMB.preShow = frmMFFilterUnitHolderNoMBPreShow;
  frmMFFilterUnitHolderNoMB.SegmentList.onRowClick = frmMFFilterUnitHolderNoMBOnSeleteData;
  frmMFFilterUnitHolderNoMB.ButtonClose.onClick = frmMFFilterUnitHolderNoMBClosePopup;
  frmMFFilterUnitHolderNoMB.onDeviceBack = disableBackButton;
}
function frmMFFilterUnitHolderNoMBPreShow(){
  try{
    var locale = kony.i18n.getCurrentLocale();
    frmMFFilterUnitHolderNoMB.LabelSelectUnitHolderNo.text = kony.i18n.getLocalizedString("MF_Purchase_23");
    dismissLoadingScreen();
  }catch(e) {
    kony.print("Error :::" + e);
  }
}
function frmMFFilterUnitHolderNoMBOnSeleteData(){
  try{
    var inputParam = {};
    var selectedIndex = frmMFFilterUnitHolderNoMB.SegmentList.selectedIndex[1];
    var fundDataObject = frmMFFilterUnitHolderNoMB.SegmentList.data[selectedIndex];
    var unitholder = "";
    var mfTotalAmount;
	showLoadingScreen(); //mki, MIB-9014
    if(gblMFOrder["orderFlow"] != undefined && gblMFOrder["orderFlow"] == MF_FUNDSUMMARY_FLOW) {

      unitholder = fundDataObject.LabelUnitHolderNo.replace(/-/gi, "");
      gblMFOrder["unitHolderNo"] = unitholder;
		
      var APchar = unitholder.substring(0, 2);
      kony.print("APchar  "+APchar);
      kony.print(" AS Prefix "+gblMFSummaryData["MF_UHO_ASP_PREFIX"]);
      var AS_prefix  = gblMFSummaryData["MF_UHO_ASP_PREFIX"];
      if(AS_prefix == APchar){
        dismissLoadingScreen();
        alert(kony.i18n.getLocalizedString("MF_AS_Prefix_ERR"));
        return;
      }
      
      getFundRulePurchaseOnSelectedIndex();
    } else {
      unitholder = fundDataObject.LabelUnitHolderNo;
      if(unitholder != kony.i18n.getLocalizedString("MU_SUM_Viewall")) {
        gblUnitHolderNoSelected = unitholder.replace(/-/gi, "");
        frmMFSummary.LabelUnitHolderNo.text = formatUnitHolderNumer(gblUnitHolderNoSelected);
      } else {
        gblUnitHolderNoSelected = "";
        frmMFSummary.LabelUnitHolderNo.text = kony.i18n.getLocalizedString("MU_SUM_Viewall");
      }

      mfAmount = calculateTotalInvestmentValue(gblMFSummaryData, gblUnitHolderNoSelected);
      calculateFundClassPercentageMB(gblMFSummaryData, mfAmount["totalInvestmentValue"], gblUnitHolderNoSelected);
      if(mfAmount["totalInvestmentValue"] > 0.00){
        frmMFSummary.flexChart.setVisibility(true);
        addgenerateChart();
      } else {
        frmMFSummary.flexChart.setVisibility(false);
      }

      frmMFSummary.lblcurntportfolioVal.text = numberWithCommas(numberFormat(mfAmount["totalInvestmentValue"], 2));
      if(mfAmount["totalUnrealizeValue"] >= 0.00){
        frmMFSummary.lblProfitVal.skin = lblGreenMFSummary;
      } else {
        frmMFSummary.lblProfitVal.skin = lblRedMFSummary;
      }
      frmMFSummary.lblProfitVal.text = numberWithCommas(numberFormat(mfAmount["totalUnrealizeValue"], 2));

      var segmentData= MBpopulateDataMutualFundsSummary(gblMFSummaryData);
      frmMFSummary.segAccountDetails.removeAll();
      frmMFSummary.segAccountDetails.setData(segmentData);
      frmMFSummary.show();
      dismissLoadingScreen(); //mki, MIB-9014
    }
  } catch(e) {
    kony.print("Error :::" + e);
  }


}
function frmMFFilterUnitHolderNoMBClosePopup(){
  if(gblMFOrder["orderFlow"] != undefined && gblMFOrder["orderFlow"] == MF_FUNDSUMMARY_FLOW) {
    frmMFSelectPurchaseFundMB.show();
  } else {
    frmMFSummary.show();
  }
}

function frmMFSeachFundPurchaseMBSearchTextChange(){
  var searchText = frmMFSeachFundPurchaseMB.txtSearch.text;
  var fundHouseCode = gblMFOrder["fundHouseCode"];
  if(fundHouseCode == kony.i18n.getLocalizedString("MF_RDM_20")){
    fundHouseCode = "";
  }
  if(searchText.length >= 3){
    setFundListForPurchase(fundHouseCode, searchText);
  } 
}

function frmMFSeachFundPurchaseMBClearSearch(){
  frmMFSeachFundPurchaseMB.txtSearch.text = "";
  frmMFSeachFundPurchaseMB.SegmentFundList.removeAll();
}

function frmMFTncShowFundFactSheet(){
  kony.application.openURL(kony.i18n.getLocalizedString("keyMFFundFactSheet"));
}

function frmSelAccntMutualFundPreshow(){
  if (gblPlatformName == "iPhone"  || gblDeviceInfo["name"] == "iPad"|| gblPlatformName == "iPhone Simulator" ){
	frmSelAccntMutualFund.segTransFrm.viewConfig = {coverflowConfig: {isCircular: true}};
  }
}