gblInterestRateAcc = "";
gblSelectedRecipentName="";
gblAccountStatus="";
function dreamSavingCallService() {
	
	var inputparam = {};
	var selectedRow = frmAccountSummary.segAccountDetails.selectedRowItems[0];
	inputparam["personalizedAcctId"] =  selectedRow["persionlizeAcctId"];
    invokeServiceSecureAsync("crmAccountEnquiry", inputparam, dreamSavingCallBack);
}

function dreamSavingCallBack(status, resulttable) {

	if (status == 400) {
		dismissLoadingScreen();
		if (resulttable["opstatus"] == 0) {
			if (resulttable["crmAccountRec"].length > 0 && null != resulttable["crmAccountRec"][0]["dreamTargetAmount"]) {
				frmAccountDetailsMB.lblUsefulValue2.text = (resulttable["crmAccountRec"][0]["dreamDesc"] != undefined) ? resulttable["crmAccountRec"][0]["dreamDesc"] : "";
				frmAccountDetailsMB.lblUsefulValue3.text = commaFormatted(resulttable["crmAccountRec"][0]["dreamTargetAmount"])+" "+kony.i18n.getLocalizedString(
					"currencyThaiBaht");
			}
			else
			{
				frmAccountDetailsMB.lblUsefulValue2.text="";
				frmAccountDetailsMB.lblUsefulValue3.text="";
			}
			depositAccountInqCallService();
		} else {
			alert(" " + resulttable["errMsg"]);
			dismissLoadingScreen();
		}
	}
}

function depositAccountInqCallService() {
	var inputparam = {};
    var selectedRow = frmAccountSummary.segAccountDetails.selectedRowItems[0];
    inputparam["acctId"] = selectedRow["accId"]; 
	//inputparam["acctId"] =gblAccountTable["custAcctRec"][gblIndex]["accId"];
	var status = invokeServiceSecureAsync("depositAccountInquiry", inputparam, depositAccountInqCallBack);
}

function depositAccountInqCallBack(status, resulttable) {
	
	if (status == 400) {
		
		if (resulttable["opstatus"] == 0) {
		   
			if (resulttable["bankAccountStatus"] != null && resulttable["bankAccountStatus"] != "") {
			    gblAccountStatus=resulttable["bankAccountStatus"];
				var status=resulttable["bankAccountStatus"].split("|");
				var locale = kony.i18n.getCurrentLocale();
				if (locale == "en_US")
				frmAccountDetailsMB.lblUsefulValue9.text =status[0];
				else
				frmAccountDetailsMB.lblUsefulValue9.text =status[1];
			}
			if (resulttable["accountTitle"] != null && resulttable["accountTitle"] != "") {
				frmAccountDetailsMB.lblUsefulValue8.text = resulttable["accountTitle"];
			}
			var availableBal;
			var ledgerBal;
			if (resulttable["AcctBal"].length > 0) {
				for (var i = 0; i < resulttable["AcctBal"].length; i++) {
					if (resulttable["AcctBal"][i]["BalType"] == "Avail") {
						availableBal = resulttable["AcctBal"][i]["Amt"];
					}
					if (resulttable["AcctBal"][i]["BalType"] == "Ledger") {
						ledgerBal = resulttable["AcctBal"][i]["Amt"];
					}
				}
			}
			if (null != availableBal) {
				frmAccountDetailsMB.lblAccountBalanceHeader.text = commaFormatted(availableBal) + " " + kony.i18n.getLocalizedString(
					"currencyThaiBaht");
			}
			if (null != ledgerBal) {
				frmAccountDetailsMB.lblledgerbalval.text = commaFormatted(ledgerBal) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
			}
			if (resulttable["linkedacc"].length == 0) {
				
				frmAccountDetailsMB.hbxUseful4.isVisible = false;
				frmAccountDetailsMB.hbxUseful5.isVisible = false;
			} else {
			    var linkAccount;
			    var lenlinkAccount=resulttable["linkedacc"].length;
			    if(lenlinkAccount==30)
			    linkAccount=resulttable["linkedacc"].substring(20);
			    else
			    linkAccount=resulttable["linkedacc"].substring(16);
				frmAccountDetailsMB.lblUsefulValue13.text = formatAccountNo(linkAccount);
				frmAccountDetailsMB.hbxUseful4.isVisible = true;
				frmAccountDetailsMB.hbxUseful5.isVisible = true;
				var accType=resulttable["linkedAccType"];
				rexferInqCallService(linkAccount,accType)
			}
			dreamSavingAccount();
			dismissLoadingScreen();
		} else {
			alert(" " + resulttable["errMsg"]);
			dismissLoadingScreen();
		}
	}
}

function rexferInqCallService(linkAccount,accType) {
	
	var inputparam = {};
	if (accType == "SDA") {
        inputparam["spName"] = "com.fnf.xes.ST"; //14digits
    }
    if (accType == "DDA") {
        inputparam["spName"] = "com.fnf.xes.IM"; //14digits
        
    }
    
        inputparam["acctId"] = linkAccount;
        //   
        inputparam["acctType"] = accType;
        
        inputparam["clientDt"] = "";
        inputparam["name"] = "MB-INQ";
        
        
	
   
    
    
	var status = invokeServiceSecureAsync("RecurringFundTransinq", inputparam, rexferInqCallBack);
	
}

function rexferInqCallBack(status, resulttable) {
	
	if (status == 400) {
		
		
		if (resulttable["opstatus"] == 0) {
			if (resulttable["monInstAmt"] != "") {
				frmAccountDetailsMB.lblUsefulValue4.text = commaFormatted(resulttable["monInstAmt"])+" "+kony.i18n.getLocalizedString(
					"currencyThaiBaht");
			}
			if (resulttable["frequency"] != null && resulttable["frequency"] != "" && resulttable["frequency"] =="EndOfMonth") {
          
            frmAccountDetailsMB.lblUsefulValue5.text  = "31" + "st";
            mnthlyTransferDate = "31";
          }
			if (resulttable["monIntDate"] != null && resulttable["monIntDate"] != "") {
                mnthlyTransferDate = resulttable["monIntDate"];
                if (mnthlyTransferDate == 1 || mnthlyTransferDate == 1 == 21) {
                    frmAccountDetailsMB.lblUsefulValue5.text = mnthlyTransferDate + "st";
                } else if (mnthlyTransferDate == 2 || mnthlyTransferDate == 22) {
                    frmAccountDetailsMB.lblUsefulValue5.text = mnthlyTransferDate + "nd";
                } else if (mnthlyTransferDate == 3 || mnthlyTransferDate == 23) {
                    frmAccountDetailsMB.lblUsefulValue5.text = mnthlyTransferDate + "rd";
                } else {
                    frmAccountDetailsMB.lblUsefulValue5.text = mnthlyTransferDate + "th";
                }

            }
			//linkaccount="";
		} else {
			alert(" " + resulttable["errMsg"]);
			kony.application.dismissLoadingScreen();
		}
	}
}

function loanAccountCallService(callback) {
	
	
    var selectedRow = frmAccountSummary.segAccountDetails.selectedRowItems[0];
	var inputparam = {};
	inputparam["acctId"] = selectedRow["accId"];
    showLoadingScreen();  
    var status = invokeServiceSecureAsync("doLoanAcctInq", inputparam, callback);
}



function homeLoanAccountCallBck(status, resulttable) {
  try{
    kony.print("@@@ In homeLoanAccountCallBck() @@@");
    if (status == 400) {
      dismissLoadingScreen();
      if (resulttable["opstatus"] == 0) {
        gblLoanAccountTable = resulttable;
        var outStandingBal;
        var temp = [];
        for (var i = 0; i < resulttable["acctBal"].length; i++) {
          if (resulttable["acctBal"][i]["balType"] == "Outstanding") {
            outStandingBal = resulttable["acctBal"][i]["balAmount"];
            break;
          }
        }
        frmAccountDetailsMB.lblHeaderBalance.text = kony.i18n.getLocalizedString("keyMBOutstanding");
        if (null != outStandingBal) {
          frmAccountDetailsMB.lblAccountBalanceHeader.text = commaFormatted(outStandingBal) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
        }
        var expiryDate = [];
        if (resulttable["creditLimitInfo"].length > 0) {
          for (var i = 0; i < resulttable["creditLimitInfo"].length; i++) {
            if (resulttable["creditLimitInfo"][i]["expiryDate"] != "" && resulttable["creditLimitInfo"][i]["expiryDate"] != null) {
              expiryDate[i] = reformatDate(resulttable["creditLimitInfo"][i]["expiryDate"])
            }
            var segTable = {
              lblDate: commaFormatted(resulttable["creditLimitInfo"][i]["loanCreditLimit"]),
              lblTransaction: parseFloat(resulttable["creditLimitInfo"][i]["loanRate"])
              .toFixed(3) + kony.i18n.getLocalizedString("percent"),
              lblBalance: expiryDate[i]
            }
            temp.push(segTable);
          }
          frmAccountDetailsMB.segMaturityDisplay.setData(temp);
        }
        kony.application.dismissLoadingScreen();
        homeLoanAccount();
      } else {
        alert(" " + resulttable["errMsg"]);
        kony.application.dismissLoadingScreen();
      }
    }
  }catch(e){
    kony.print("@@@ In homeLoanAccountCallBck() Exception:::"+e);
  }
}

function cash2GoAccountCallBck(status, resulttable) {
  try{
    kony.print("@@@ In cash2GoAccountCallBck() @@@");
    
    if (status == 400) {
      if (resulttable["opstatus"] == 0) {
        gblLoanAccountTable = resulttable;
        var outStandingBal;
        for (var i = 0; i < resulttable["acctBal"].length; i++) {
          if (resulttable["acctBal"][i]["balType"] == "Outstanding") {
            outStandingBal = resulttable["acctBal"][i]["balAmount"];
            break;
          }
        }
        frmAccountDetailsMB.lblHeaderBalance.text = kony.i18n.getLocalizedString("keyMBOutstanding");
        if (null != outStandingBal) {
          frmAccountDetailsMB.lblAccountBalanceHeader.text = commaFormatted(outStandingBal) + " " + kony.i18n.getLocalizedString(
            "currencyThaiBaht");
        }
        kony.application.dismissLoadingScreen();
        cash2GoAccount();
      } else {
        alert(" " + resulttable["errMsg"]);
        kony.application.dismissLoadingScreen();
      }
    }
  }catch(e){
    kony.print("@@@ In cash2GoAccountCallBck() Exception:::"+e);
  }
	
}



function creditCardCallService() {

	var inputparam = {};
	gblCC_StmtPointAvail = "";
	gblCC_RemainingPoint = "";	
	gblCC_PointExpRemaining = "";
    var selectedRow = frmAccountSummary.segAccountDetails.selectedRowItems[0];
	inputparam["cardId"] = selectedRow["accId"];
	inputparam["tranCode"] = "1";
    showLoadingScreen();
	var status = invokeServiceSecureAsync("creditcardDetailsInq", inputparam, creditCardCallBack);
}

function creditCardCallBack(status, resulttable) {
	if (status == 400) {
		dismissLoadingScreen();
		if (resulttable["opstatus"] == 0) {
            
          if(resulttable["cardStatus"]=="Not Activated" && "Y" == resulttable["isNewCard"])
			{
			  var selectedRow = frmAccountSummary.segAccountDetails.selectedRowItems[0];	
              var cardRefId=selectedRow["cardRefId"];
			  callCreditReadyCardActivateInqService(cardRefId);
			}
			else
			{
				
				var selectedRow = frmAccountSummary.segAccountDetails.selectedRowItems[0];
  				var cardStatus = selectedRow["acctStatus"];
								
				if (resulttable["stmtDate"] != "" && resulttable["stmtDate"] != undefined) {
					//frmAccountDetailsMB.lblUseful5.text = kony.i18n.getLocalizedString("StatDate");
					frmAccountDetailsMB.lblUsefulValue5.text = reformatDate(resulttable["stmtDate"]);
				}
				//if(!flowSpa){
					/*if (allowApplySoGoooddet == "1" && validCicsTranCode(resulttable["cicsTranCode"])) {
						//frmAccountDetailsMB.flexLineApplysoGood.setVisibility(true);
						//frmAccountDetailsMB.flexLinkApplysoGood.setVisibility(true);
						//frmAccountDetailsMB.hbxApplySoGooOD.setVisibility(true);
						
					} else {
						//frmAccountDetailsMB.hbxApplySoGooOD.setVisibility(false);
						//frmAccountDetailsMB.flexLineApplysoGood.setVisibility(false);
						//frmAccountDetailsMB.flexLinkApplysoGood.setVisibility(false);
						}
					if (allowRedeemPointsdet == "1") {
						//frmAccountDetailsMB.flexLinePointsRdeem.setVisibility(true);
						//frmAccountDetailsMB.flexLinkPointsRdeem.setVisibility(true);
						
					} else {
						//frmAccountDetailsMB.flexLinePointsRdeem.setVisibility(false);
						//frmAccountDetailsMB.flexLinkPointsRdeem.setVisibility(false);
					}*/

                  if (resulttable["cardStatus"]=="Not Activated"){
                       	kony.print("inside not activated >>>>>>>>>>");
						frmAccountDetailsMB.flexLineManageCreditCard.setVisibility(true);
						frmAccountDetailsMB.flexLinkManageCreditCard.setVisibility(true);
                       	frmAccountDetailsMB.btnManageCard.text = kony.i18n.getLocalizedString("ActivateCard");
                       	frmAccountDetailsMB.btnManageCard.onClick = callCreditReadyCardActivateInqService;
					} else{
                      	kony.print("inside card activated >>>>>>>>>>");
						frmAccountDetailsMB.flexLineManageCreditCard.setVisibility(true);
						frmAccountDetailsMB.flexLinkManageCreditCard.setVisibility(true);
                      	frmAccountDetailsMB.btnManageCard.text = kony.i18n.getLocalizedString("keyManageCard");
                      	frmAccountDetailsMB.btnManageCard.onClick = callCreditReadyCardInqService;
					}
				
				//}
				
				if (resulttable["bonusPointAvail"] != "" && resulttable["bonusPointAvail"] != undefined) {
					gblCC_StmtPointAvail = resulttable["bonusPointAvail"];
				}	
						
				creditCardCallServiceUnBillAmt();
				creditCardAccount();
			}
		}
	}
}

function creditCardCallServiceUnBillAmt() {
	var inputparam = {};

  	var selectedRow = frmAccountSummary.segAccountDetails.selectedRowItems[0];
	inputparam["cardId"] = selectedRow["accId"];
	inputparam["tranCode"] = TRANSCODEUN;
	var status = invokeServiceSecureAsync("creditcardDetailsInq", inputparam, creditCardCallBackUn);
}

function creditCardCallBackUn(status, resulttable) {
	if (status == 400) {
		
		if (resulttable["opstatus"] == 0) {
			
          	var locale = kony.i18n.getCurrentLocale();
			
			//
			 if (resulttable["cardNo"] != "" && resulttable["cardNo"] != undefined) {
                frmAccountDetailsMB.lblUsefulValue2.text = resulttable["cardNo"];
                frmAccountDetailsMB.lblCardNumber.text = resulttable["cardNo"].split("-").join("  ");
               
            }
            if (resulttable["fromAcctName"] != "" && resulttable["fromAcctName"] != undefined) {
               
                frmAccountDetailsMB.lblUsefulValue3.text = resulttable["fromAcctName"];
	   			
	   			var selectedRow = frmAccountSummary.segAccountDetails.selectedRowItems[0];
	   			if(isNotBlank(selectedRow["acctNickName"])){
	   				frmAccountDetailsMB.lblCardAccountName.text = selectedRow["acctNickName"];
	   			} else {
	   				if (locale == "en_US"){
	   					frmAccountDetailsMB.lblCardAccountName.text =  selectedRow["defaultCurrentNickNameEN"];
	   				} else {
	   					frmAccountDetailsMB.lblCardAccountName.text =  selectedRow["defaultCurrentNickNameTH"];
	   				}
	   			}	   			
            }
			frmAccountDetailsMB.lblUseful1.text = kony.i18n.getLocalizedString("CardType");
			if (resulttable["cardType"] != "" && resulttable["cardType"] != undefined) {
				frmAccountDetailsMB.lblUsefulValue1.text = resulttable["cardType"];
			}
          
			frmAccountDetailsMB.lblUsefulValue14.text="";
			frmAccountDetailsMB.hbxUseful14.setVisibility(false);
	
			frmAccountDetailsMB.lblUsefulValue15.text="";
			frmAccountDetailsMB.hbxUseful15.setVisibility(false);
				
			frmAccountDetailsMB.lblAccountBalanceHeader.text = commaFormatted(resulttable["availCrLimit"]) +" " + kony.i18n.getLocalizedString(
				"currencyThaiBaht");
				
				frmAccountDetailsMB.lblUsefulValue6.text = reformatDate(resulttable["dueDate"]);
				frmAccountDetailsMB.lblUsefulValue4.text = commaFormatted(resulttable["totalCrLimit"]) + " " + kony.i18n.getLocalizedString(
					"currencyThaiBaht");
			
			if (resulttable["fullPmtAmt"] != "") {
				
				frmAccountDetailsMB.lblUsefulValue7.text = commaFormatted(resulttable["fullPmtAmt"]) + " " + kony.i18n.getLocalizedString(
					"currencyThaiBaht");
			}
			if (resulttable["minPmtAmt"] != "") {
				
				frmAccountDetailsMB.lblUsefulValue8.text = commaFormatted(resulttable["minPmtAmt"]) + " " + kony.i18n.getLocalizedString(
					"currencyThaiBaht");
			}

			var allowCardTypeForRedeem = selectedRow["allowCardTypeForRedeem"];		
			if (allowCardTypeForRedeem == "1" && resulttable["remainingPoint"] != "") {
				gblAccountTable["custAcctRec"][gblIndex]["bonusPointAvail"] = resulttable["remainingPoint"];
				frmAccountDetailsMB.lblUsefulValue14.text= commaFormattedPoints(resulttable["remainingPoint"]);
				frmAccountDetailsMB.hbxUseful14.isVisible=true;
				gblCC_RemainingPoint = resulttable["remainingPoint"];						
			}
						
			
			if (allowCardTypeForRedeem == "1" && resulttable["bonusPointUsed"] != "") {
				if (resulttable["stmtDate"] != "") {
					gblAccountTable["custAcctRec"][gblIndex]["bonusPointExpiryDate"] = resulttable["stmtDate"];
					frmAccountDetailsMB.lblUseful15.text = kony.i18n.getLocalizedString("keyPointExpiringOn") + " " + reformatDate(resulttable["stmtDate"])+ ":";
				}
				gblAccountTable["custAcctRec"][gblIndex]["bonusPointExpired"] = resulttable["bonusPointUsed"];
				
				if (resulttable["bonusPointUsed"] != "" && gblCC_StmtPointAvail != "" && resulttable["remainingPoint"] != "") {
					gblCC_PointExpRemaining = Number(resulttable["bonusPointUsed"]) - (Number(gblCC_StmtPointAvail) - Number(resulttable["remainingPoint"]));		
					if (gblCC_PointExpRemaining < 0) {
						gblCC_PointExpRemaining = 0;
					}
					gblCC_PointExpRemaining = gblCC_PointExpRemaining + "";				
					frmAccountDetailsMB.lblUsefulValue15.text = commaFormattedPoints(gblCC_PointExpRemaining);
					frmAccountDetailsMB.hbxUseful15.isVisible=true;		
				}
				
				gblAccountTable["custAcctRec"][gblIndex]["bonusPointExpired"] = gblCC_PointExpRemaining;   
			}			
			
			if(resulttable["directDebitAccNo"].length != undefined || resulttable["directDebitAccNo"].length != null)
			{
				if (resulttable["directDebitAccNo"].length == 0) { 				  
				    frmAccountDetailsMB.lblUseful9.isVisible = false;
                	frmAccountDetailsMB.hbxUseful14.skin = "hboxWhite";
					frmAccountDetailsMB.hbxUseful15.skin = "hboxWhite";	                
				}
				else{					
				    frmAccountDetailsMB.lblUsefulValue9.text = formatAccountNo(resulttable["directDebitAccNo"]);
					frmAccountDetailsMB.hbxUseful14.skin = "hboxWhite"; 
				    frmAccountDetailsMB.hbxUseful15.skin = "hboxWhite";
				}
			}
			else
			{
				alert(kony.i18n.getLocalizedString("keyOpenActGenErr"));
				kony.application.dismissLoadingScreen();
			}
			moreAccountDetails();
			kony.application.dismissLoadingScreen();
		} else {
			alert(" " + resulttable["errMsg"]);
			kony.application.dismissLoadingScreen();
		}
	}
}

function callPartyInquiryService() {
	
	//kony.application.showLoadingScreen(frmLoading, "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
	//showLoadingScreen();
	var inputparam = {};
	inputparam["fIIdent"] = "";
	inputparam["rqUUId"] = "";
	inputparam["partyIdentValue"] = "";
	if(isFromLogin)
	inputparam["TriggerEmail"]="yes";
	inputparam["activityTypeId"] = gblLoginType;	
	isFromLogin=false
	invokeServiceSecureAsync("partyInquiry", inputparam, partyInquiryCallBack);
}

function partyInquiryCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            gblCustomerName = resulttable["customerName"];
            gblCustomerNameTh = resulttable["customerNameTH"];
            if(flowSpa){
           // gblPHONENUMBER = resulttable["PhnNum"];
            }
            else{	
		            if (resulttable["ContactNums"] == undefined || resulttable["ContactNums"] == "" || resulttable["ContactNums"] == null) {
		                showAlertRcMB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"), "info");
		                return false;
		            } 
		            else {
		                for (var i = 0; i < resulttable["ContactNums"].length; i++) {
		                    if (resulttable["ContactNums"][i]["PhnType"] == 'Mobile')
		                        gblPHONENUMBER = resulttable["ContactNums"][i]["PhnNum"];
		                }
		             }
            }
            //frmAccountSummaryLanding.lblAccntHolderName.text = resulttable["customerName"];
            //cleanPreorPostForms();
            if (gblPushNotificationFlag) {

                gblPushNotificationFlag = false;
                cleanPreorPostForms();
                pushNotificationAllow(gblPayLoadKpns);
            } else {
                if (gblLoginType == "Login") {
                    gblLoginType = "";
                    GBL_Fatca_Flow = "login";
                    //gblFATCASkipCounter = parseInt(resulttable["FATCASkipCounter"]);
                    //resulttable["FatcaFlag"] = "0"	//Comment this line when service response comes
                    if (resulttable["FatcaFlag"] == "0") {
                        gblFATCAUpdateFlag = "0";
                        showFACTAInfo();
                    } else if (resulttable["FatcaFlag"] == "8" || resulttable["FatcaFlag"] == "9" || resulttable["FatcaFlag"] == "P" || resulttable["FatcaFlag"] == "R" || resulttable["FatcaFlag"] == "X") {
                        dismissLoadingScreen();
                        gblFATCAUpdateFlag = "Z";
                        setFATCAMBGoToBranchInfoMessage();
                    } else {

                        if (gblActivationFlow == true) {
                            touchflag = getValidTouchCounter();

                            if (touchflag == true && gblActivationFlow == true) {
                                gblTouchStatus = "N";
                                isSignedUser = true;
                                frmTouchIdIntermediateLogin.show();
                                gblActivationFlow = false;
                                dismissLoadingScreen();
                            } else {
                                callCustomerAccountService("");
                            }
                        } else {
                            callCustomerAccountService("");
                        }
                    }
                } else {
                    if (gblActivationFlow == true) {
                        touchflag = getValidTouchCounter();
                        if (touchflag == true && gblActivationFlow == true) {
                            gblTouchStatus = "N";
                            isSignedUser = true;
                            frmTouchIdIntermediateLogin.show();
                            gblActivationFlow = false;
                            dismissLoadingScreen();
                        } else {
                            callCustomerAccountService();
                        }
                    } else {
                        callCustomerAccountService();
                    }

                }
            }
        } else {
            alert(" " + resulttable["errMsg"]);
            kony.application.dismissLoadingScreen();
        }
    }
}

function cleanPreorPostForms(){
  try{
    /*if(frmExchangeRate.hboxMenuHeader != null || frmExchangeRate.hboxMenuHeader != undefined){
      frmExchangeRate.hboxMenuHeader = null;
      TMBUtil.DestroyForm(frmExchangeRate);
    }*/
    if(frmContactUsMB.hboxMenuHeader != null || frmContactUsMB.hboxMenuHeader != undefined){
      frmContactUsMB.hboxMenuHeader = null;
      TMBUtil.DestroyForm(frmContactUsMB);
    }
    if(frmAppTour.hboxMenuHeader != null || frmAppTour.hboxMenuHeader != undefined){
      frmAppTour.hboxMenuHeader = null;
      TMBUtil.DestroyForm(frmAppTour);
    }
    if(frmATMBranchesDetails.hboxMenuHeader != null || frmATMBranchesDetails.hboxMenuHeader != undefined){
      frmATMBranchesDetails.hboxMenuHeader = null;
      TMBUtil.DestroyForm(frmATMBranchesDetails);
    }
    if(frmATMBranch.hboxMenuHeader != null || frmATMBranch.hboxMenuHeader != undefined){
      frmATMBranch.hboxMenuHeader = null;
      TMBUtil.DestroyForm(frmATMBranch);
    }
  }catch(e){
    kony.print("@@@ cleanPreorPostForms() Exception::"+e);
  }
}

function callCustomerAccountService(upgradeSkip) {
  try{
    kony.print("@@@ In callCustomerAccountService() @@@");
    showLoadingScreen();
    var inputparam = {};
    if(upgradeSkip != null && upgradeSkip != undefined && upgradeSkip != ""){
      inputparam["upgradeSkip"] = upgradeSkip;
      clearFatcaGlobal();
    }	
    inputparam["activationCompleteFlag"] = "true";
    if(gblChangeLanguage == true){
      var locale = kony.i18n.getCurrentLocale();
      kony.print("locale in callCustomerAccountService @@: "+locale);
      if (locale == "en_US") {
        inputparam["languageCd"] = "EN";
      } else {
        inputparam["languageCd"] = "TH";
      }
    }
    //Account Summary login flow flag disable 
    //showLoadingScreen();
    isLoginFlow = false;
    invokeServiceSecureAsync("customerAccountInquiry", inputparam, customerAccountCallBack);
  }catch(e){
    kony.print("@@@ In callCustomerAccountService() Exception:::"+e);
    dismissLoadingScreen();
  }
}

function customerAccountCallBack(status, resulttable) {
  try{
    kony.print("@@@ In customerAccountCallBack() @@@");
    if (status == 400) {
      if (resulttable["opstatus"] == 0) {
        setASBGImageOrColor(resulttable["isSpecialOccasion"]); //Sets AS BG image in Occasions.
        
        gblshorCutToAccounts=resulttable.shorCutToAccounts;
        ITMX_TRANSFER_EWALLET_ENABLE = resulttable["ITMX_TRANSFER_EWALLET_ENABLE"];
        if (resulttable["statusCode"] != 0) {
          alert(kony.i18n.getLocalizedString("ECGenericError"));
          cleanPreorPostForms();
          dismissLoadingScreen();
        } else if (resulttable["statusCode"] == -1) {
          //var errMessage = kony.i18n.getLocalizedString("errMsg");
          if(resulttable["errMsg"]=="100" ||resulttable["errMsg"]==100){
            alert(kony.i18n.getLocalizedString("ECGenericError"));
          }else{
            alert(resulttable["errMsg"]);
          }
          cleanPreorPostForms();		
          dismissLoadingScreen();
        } 
        else if(resulttable["opstatus_hidden"] == 2){
          var crmIdIm= resulttable["crmId"];
          if(resulttable["shorCutToAccounts"] != undefined && resulttable["shorCutToAccounts"] != null){
            gblshorCutToAccounts=resulttable["shorCutToAccounts"];
          }
          isSignedUser = true;
          /*
          gblMyProfilepic = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=Y&personalizedId=&billerId=";
          if(gblMyProfilepic == null){
            //frmAccountSummaryLanding.imgProfile.src="avatar1.png";
            frmAccountSummary.imgProfile.base64 = "avatar1.png";
          }else{
            //frmAccountSummaryLanding.imgProfile.src = gblMyProfilepic;
            frmAccountSummary.imgProfile.base64 = gblMyProfilepic;
          }
          
          //MIB-1069
          //frmAccountSummaryLanding.lblHiddenAccount.text=kony.i18n.getLocalizedString("errMsgHidden");
          frmAccountSummaryLanding.lblHiddenAccount.text=kony.i18n.getLocalizedString("MB_ASTxt_NoAccount");
          //frmAccountSummaryLanding.lblAllHidden.setEnabled(true)
          frmAccountSummaryLanding.hbox454820850134417.onClick = onClickWhenNoAcctMB;
          frmAccountSummaryLanding.lblForUse.text = "0.00" + kony.i18n.getLocalizedString("percent");
          frmAccountSummaryLanding.lblForsave.text = "0.00" + kony.i18n.getLocalizedString("percent");
          if(flowSpa) {
            frmAccountSummaryLanding.label4751247744357.text = kony.i18n.getLocalizedString("trmDeposit");
            frmAccountSummaryLanding.lblFunds.text = "0.00" + kony.i18n.getLocalizedString("percent");
          } else {
            frmAccountSummaryLanding.label4751247744357.text = kony.i18n.getLocalizedString("KeyForInvest");
            frmAccountSummaryLanding.lblFunds.text = "0.00" + kony.i18n.getLocalizedString("percent");
          }

          frmAccountSummaryLanding.lblBalanceValue.text = "0.00 " + kony.i18n.getLocalizedString("currencyThaiBaht"); */
          cleanPreorPostForms();
          //if cmp internal link set other than account summary then we do not show account summary form.
          //isCmpFlowFrmActivation set to true, if cmp internal link set	
          //alert("11 in account summuary js , isCmpFlowFrmActivation : "+isCmpFlowFrmActivation);
          if(isCmpFlowFrmActivation){
            kony.print("@@@gblCampaignDataEN:::"+gblCampaignDataEN+"|||gblCampaignDataTH:::"+gblCampaignDataTH);
            //if(gblCampaignDataEN == 'frmAccountSummaryLanding' || gblCampaignDataTH == 'frmAccountSummaryLanding'){
            if(gblCampaignDataEN == 'frmAccountSummary' || gblCampaignDataTH == 'frmAccountSummary'){
              //activaition complete screen banner interal link is set as account summary.
              //frmAccountSummaryLanding.show();
              showAccountSummaryNew();
            }else {
              //activaition complete screen banner interal link is NOT set as account summary.
              //so calling corresponding internal link function
              getCampaignResult();
            }
            isCmpFlowFrmActivation = false;
          }else{
            //to show anyId announcement page while login and actiivation complete screen.
            //gblShowAnyIDRegistration set to true, if any Id is turn ON
            //displayAnnoucementtoUser set to true, if user is not registered with ANYID with TMB
            if("transfer" == gbl3dTouchAction){
              gbl3dTouchAction="";
              transferFromMenu();	//moving to Transfer flow	
            }else if("billpay" == gbl3dTouchAction){
              gbl3dTouchAction="";
              callBillPaymentFromMenu();//moving to Bill Pay here
            }else if("topup" == gbl3dTouchAction){
              gbl3dTouchAction="";
              callTopUpFromMainMenu();//moving to Top Up here
            }else if(gblQuickBalanceFromLogin){
              onClickQuickBalanceMenu();
            }else if(gblPreLoginBillPayFlow){
              gblPreLoginBillPayFlow = false;
              callBillPaymentFromQR();
            }else if(gblQrSetDefaultAccnt){
              gblQrSetDefaultAccnt = false;
              onSelectFromAccntQRPay();
            }else if(gblGoDirectlyToPrompPay){
              gblGoDirectlyToPrompPay = false;
              menuSetUserIDonClick();
            }else if(gblShowAnyIDRegistration && displayAnnoucementtoUser) {
              frmRegAnyIdAnnouncement.imgAnydAnnouPage.src="";
              frmRegAnyIdAnnouncement.show();
            } else if(displayRTPAnnoucement) {
              startUpRTPDisplay();
            } else if(gblTransferPush){
              validatingTFPushdata();
            }else if(gblRTPBillPayPush){
              validatingBPPushdata();
            }
            else {
              if(!gblFromSwitchLanguage){
                //forceAccountSummaryRefresh();
                //frmAccountSummaryLanding.show();
                showAccountSummaryNew();
              }else{
                gblFromSwitchLanguage = false;
              }
            }
          }
          if(flowSpa){
            //	menuOptionEnableTokenActivationSPA();
            //adding code for SPA menu optimisation. Remove below 2 lines code for any issues
            var calledForm = kony.application.getCurrentForm();
            createMenuDynamically(calledForm);
          }
          TMBUtil.DestroyForm(frmMBPreLoginAccessesPin); //frmAfterLogoutMB
          dismissLoadingScreen();
        }else {
          gblFinancialTxnMBLock=resulttable["enableMaintainenceFinancialTxnMB"];
          //Assigning RTP Badge count to Global variable.
          if(resulttable["RTPTotalRecordsCount"] != null && resulttable["RTPTotalRecordsCount"] == undefined)
            gblRTPBadgeCount = resulttable["RTPTotalRecordsCount"];
			setRTPBadgeCountNew();
          gblAccountTable = resulttable;
          if(flowSpa){
            gblspasngleTap=""
            gblspasngleTap = {
              fingers: 1,
              taps: 1
            };
          }

          //createNoOfRowsDynamically(resulttable);
          //New Account Summary Flow.
          gblXCord = "";
          gblYCord = "";
          gblAccountSummaryData = resulttable;
          setProfilePicBalance();
          setAccountSummaryData();
          
          var gblPlatformName = gblDeviceInfo.name;
          if (!isSignedUser) {
            loggingDeviceStoreData();
          }
          isSignedUser = true;
          var deviceId = getDeviceID();
          // 011 - activity id for login,
          // 01 activity status
          //deviceid - activityFlexValues1

          // After loading My Account Summary Page content is loaded..
          if(gblNotificationFor == "bb" || gblNotificationFor == "BB"){
            //go to Beep and Bill flow:
            customerPaymentStatusInquiryForBB(gblPayLoadKpns); 	
            gblNotificationFor = "randomXY";
          }else if(gblNotificationFor == "s2s" || gblNotificationFor == "S2S"){
            showLoadingScreen();
            //go to Send To Save Execution Flow
            gblExeS2S = "true";
            gblSSExcuteCnfrm = "randomXC"; // to control flow
            gblNotificationFor = "randomXC"
            getIBMBStatus();
            gblNotificationFor = "randomXY";//just to make sure that app is coming from notificaton or regular
          }else{
            //if cmp internal link set other than account summary then we do not show account summary form.
            //isCmpFlowFrmActivation set to true, if cmp internal link set	
            //alert("22 in account summuary js , isCmpFlowFrmActivation : "+isCmpFlowFrmActivation);
            if(isCmpFlowFrmActivation){
              kony.print("@@@gblCampaignDataEN:::"+gblCampaignDataEN+"||gblCampaignDataTH:::"+gblCampaignDataTH)
              //if(gblCampaignDataEN == 'frmAccountSummaryLanding' || gblCampaignDataTH == 'frmAccountSummaryLanding'){
              if(gblCampaignDataEN == 'frmAccountSummary' || gblCampaignDataTH == 'frmAccountSummary'){
                //activaition complete screen banner interal link is set as account summary.
                //frmAccountSummaryLanding.show();
                showAccountSummaryNew();
              }else {
                //activaition complete screen banner interal link is NOT set as account summary.
                //so calling corresponding internal link function
                getCampaignResult();
              }
              isCmpFlowFrmActivation = false;
            }else{
              //to show anyId announcement page while login and actiivation complete screen.
              //gblShowAnyIDRegistration set to true, if any Id is turn ON
              //displayAnnoucementtoUser set to true, if user is not registered with ANYID with TMB  
              if("transfer" == gbl3dTouchAction){
                gbl3dTouchAction="";
                transferFromMenu();	//moving to Transfer flow	
              }else if("billpay" == gbl3dTouchAction){
                gbl3dTouchAction="";
                callBillPaymentFromMenu();//moving to Bill Pay here
              }else if("topup" == gbl3dTouchAction){
                gbl3dTouchAction="";
                callTopUpFromMainMenu();//moving to Top Up here
              }else if(gblQuickBalanceFromLogin){
                onClickQuickBalanceMenu();
              }else if(gblGoDirectlyToPrompPay){
                gblGoDirectlyToPrompPay = false;
                menuSetUserIDonClick();
              }else if(gblPreLoginBillPayFlow){
                gblPreLoginBillPayFlow = false;
                callBillPaymentFromQR();
              }else if(gblQrSetDefaultAccnt){
                gblQrSetDefaultAccnt = false;
                onSelectFromAccntQRPay();
              }else if(gblShowAnyIDRegistration && displayAnnoucementtoUser) {
                frmRegAnyIdAnnouncement.imgAnydAnnouPage.src="";
                frmRegAnyIdAnnouncement.show();
              } else if(displayRTPAnnoucement) {
                startUpRTPDisplay();
              } else if (gblTransferPush){
                validatingTFPushdata();
              }else {	
                if(!gblFromSwitchLanguage){
                  //forceAccountSummaryRefresh();
                  //frmAccountSummaryLanding.show();
                  showAccountSummaryNew();
                }else{
                  gblFromSwitchLanguage = false;
                }
              }
            }
            //below condition is called on click of return btn of s2s activation complete screen
            if(gblConfOrComp == true){
              gblConfOrComp = false;	// to control UI of s2s apply flow
              TMBUtil.DestroyForm(frmSSConfirmation);
            }
            dismissLoadingScreen();
          }
          //accountDetailsClear();
          //TMBUtil.DestroyForm(frmAfterLogoutMB);
          kony.application.dismissLoadingScreen();
          GBL_MALWARE_FOR_RISK_FLAG = true;
          collectRiskData();
        }
        if(gblChangeLanguage == true)
          gblChangeLanguage = false;// resetting global language change flag
      }else {
        dismissLoadingScreen();
      }
    }
  	//endAccountSummaryAnimation();
  }catch(e){
    kony.print("@@@ In customerAccountCallBack() Exception:::"+e);
    dismissLoadingScreen();
  }
}

function callTimeDepositDetailService() {
	var selectedRow = frmAccountSummary.segAccountDetails.selectedRowItems[0];
	var inputparam = {};
	inputparam["acctIdentValue"] = selectedRow["accId"].substring(4);
    showLoadingScreen();
  	invokeServiceSecureAsync("tDDetailinq", inputparam, timeDepositAccountCallBack);
}

function timeDepositAccountCallBack(status, resulttable) {
	if (status == 400) {
      	if (resulttable["opstatus"] == 0) {
           dismissLoadingScreen();
			var temp = [];
			if (resulttable["tdDetailsRec"].length > 0) {
				for (var i = 0; i < resulttable["tdDetailsRec"].length; i++) {
					var segTable = {
						lblDate: reformatDate(resulttable["tdDetailsRec"][i]["maturityDate"]),
						lblTransaction: parseFloat(resulttable["tdDetailsRec"][i]["interestRate"])
							.toFixed(2) + kony.i18n.getLocalizedString("percent"),
						lblBalance: commaFormatted(resulttable["tdDetailsRec"][i]["depositAmtVal"])
					}
					temp.push(segTable);
					//frmAccountDetailsMB.segMaturityDisplay.setData(temp);
				}
				frmAccountDetailsMB.segMaturityDisplay.setData(temp);
			}
			depositAccInqCallService();
			TDAccountFunction();
		} else {
			alert(" " + resulttable["errMsg"]);
			kony.application.dismissLoadingScreen();
		}
	}
}

function depositAccInqCallService() {
	var inputparam = {};
	var selectedRow = frmAccountSummary.segAccountDetails.selectedRowItems[0];
    inputparam["acctId"] = selectedRow["accId"];	
    showLoadingScreen();
	invokeServiceSecureAsync("depositAccountInquiry", inputparam, depositAccInqCallBack);
}

function depositAccInqCallBack(status, resulttable) {
	if (status == 400) {
		
		if (resulttable["opstatus"] == 0) {
			if (resulttable["linkedacc"] != null && resulttable["linkedacc"] != "") {
			    var linkAccount;
			    var lenlinkAccount=resulttable["linkedacc"].length;
			    if(lenlinkAccount==30)
			    linkAccount=resulttable["linkedacc"].substring(20);
			    else
			    linkAccount=resulttable["linkedacc"].substring(16);
				frmAccountDetailsMB.lblUsefulValue9.text = formatAccountNo(linkAccount);
			}
			if (resulttable["count"] != null && resulttable["count"] != "") {
				frmAccountDetailsMB.lblUsefulValue2.text = resulttable["count"] + resulttable["termUnit"];
			}
			if (resulttable["bankAccountStatus"] != null && resulttable["bankAccountStatus"] != "") {
			    var locale = kony.i18n.getCurrentLocale();
			    gblAccountStatus=resulttable["bankAccountStatus"];
				var status=resulttable["bankAccountStatus"].split("|");
				if (locale == "en_US")
				frmAccountDetailsMB.lblUsefulValue6.text = status[0];
				else
				frmAccountDetailsMB.lblUsefulValue6.text = status[1];
			}
			frmAccountDetailsMB.lblUsefulValue5.text = resulttable["accountTitle"];
			var availableBal;
			var ledgerBal;
			if (resulttable["AcctBal"].length > 0) {
				for (var i = 0; i < resulttable["AcctBal"].length; i++) {
					if (resulttable["AcctBal"][i]["BalType"] == "Avail") {
						availableBal = resulttable["AcctBal"][i]["Amt"];
					}
					if (resulttable["AcctBal"][i]["BalType"] == "Ledger") {
						ledgerBal = resulttable["AcctBal"][i]["Amt"];
					}
				}
			}
			if (null != availableBal) {
				frmAccountDetailsMB.lblAccountBalanceHeader.text = commaFormatted(availableBal) + " " + kony.i18n.getLocalizedString(
					"currencyThaiBaht");
			}
			if (null != ledgerBal) {
				frmAccountDetailsMB.lblledgerbalval.text = commaFormatted(ledgerBal) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
			}
			
			moreAccountDetails();
			dismissLoadingScreen();
		} else {
			alert(" " + resulttable["errMsg"]);
			dismissLoadingScreen();
		}
	}
}

function IntersetRateInqCallService(){
	var inputparam = {};
  	var status = invokeServiceSecureAsync("interestRateInquiry", inputparam, IntersetRateInqCallBackService);
}

function IntersetRateInqCallBackService(status,resulttable){
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			frmAccountDetailsMB.lblUsefulValue9.text = resulttable["INTERESTRATE"] + "%";
			gblInterestRateAcc = resulttable["INTERESTRATE"] + "%";
		}
	}
}


function depositAccInqCallServiceForNoFixed() {

  	IntersetRateInqCallService();
	var inputparam = {};
    var selectedRow = frmAccountSummary.segAccountDetails.selectedRowItems[0];
	inputparam["acctId"] =  selectedRow["accId"];
    var status = invokeServiceSecureAsync("depositAccountInquiry", inputparam, depositAccInqCallBackNoFixed);
}

function depositAccInqCallBackNoFixed(status, resulttable) {
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			dismissLoadingScreen();
            gblenableDebitCardProductList = resulttable["enableDebitCardProductList"];
            gblenableDebitCardAccountStatus = resulttable["enableDebitCardAccountStatus"];
            var availableBal = "";
			var ledgerBal = "";
			frmAccountDetailsMB.lblUsefulValue10.text = commaFormatted(resulttable["intrestAmount"]) + kony.i18n.getLocalizedString(
				"currencyThaiBaht");
			frmAccountDetailsMB.lblUsefulValue6.text = resulttable["accountTitle"];
			gblAccountStatus=resulttable["bankAccountStatus"];
			var status=resulttable["bankAccountStatus"].split("|");
			var locale = kony.i18n.getCurrentLocale();
			if (locale == "en_US")
			frmAccountDetailsMB.lblUsefulValue7.text = status[0];
			else
			frmAccountDetailsMB.lblUsefulValue7.text = status[1];
			if (resulttable["AcctBal"].length > 0) {
				for (var i = 0; i < resulttable["AcctBal"].length; i++) {
					
					if (resulttable["AcctBal"][i]["BalType"] == "Avail") {
						availableBal = resulttable["AcctBal"][i]["Amt"];
					}
					if (resulttable["AcctBal"][i]["BalType"] == "Ledger") {
						ledgerBal = resulttable["AcctBal"][i]["Amt"];
					}
				}
			}
			if (null != ledgerBal) {
				frmAccountDetailsMB.lblledgerbalval.text = commaFormatted(ledgerBal) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
			}
			if (null != availableBal) {
				frmAccountDetailsMB.lblAccountBalanceHeader.text = commaFormatted(availableBal) + " " + kony.i18n.getLocalizedString(
					"currencyThaiBaht");
			}
			dismissLoadingScreen();
			frmAccountDetailsMB.hbxUseful2.isVisible = false;
			frmAccountDetailsMB.hbxUseful3.isVisible = false;
			frmAccountDetailsMB.hbxUseful11.isVisible = false;
			//callLiquidityInqService();			:#  Commented as it is out of scope after sendtosave removal.
			noFixedAccount();
		} else {
			//alert(" " + resulttable["errMsg"]);
			dismissLoadingScreen();
		}
	}
}

/*function callLiquidityInqService() {
	showLoadingScreen();
	var inputparam = {};
  	//inputparam["locale"] = kony.i18n.getCurrentLocale();
	invokeServiceSecureAsync("doLiquidityInq", inputparam, liquidityInqCallBack);
}*/

function liquidityInqCallBack(status, resulttable) {
	if (status == 400) {
		
		var StatusCode = resulttable["StatusCode"];
		var Severity = resulttable["Severity"];
		var StatusDesc = resulttable["StatusDesc"];
		
		if (resulttable["opstatus"] == 0) {
			if (resulttable["StatusCode"] == 0) {
				if (resulttable["maxCurAmt"] != null && resulttable["maxCurAmt"] != "") {
					frmAccountDetailsMB.lblUsefulValue2.text = commaFormatted(resulttable["maxCurAmt"])+ kony.i18n.getLocalizedString("currencyThaiBaht");
					frmAccountDetailsMB.hbxUseful2.isVisible = true;
				}
				if (resulttable["minCurAmt"] != null && resulttable["minCurAmt"] != "") {
					frmAccountDetailsMB.lblUsefulValue3.text = commaFormatted(resulttable["minCurAmt"])+ kony.i18n.getLocalizedString("currencyThaiBaht");
					frmAccountDetailsMB.hbxUseful3.isVisible = true;
				}
				if (null != resulttable["linkedAccount"] && resulttable["linkedAccount"] != "" && resulttable["linkedAccount"] != 0) {
				    var disPlayLinkAccount;
				    if(resulttable["linkedAccount"].length==14){
				     disPlayLinkAccount=resulttable["linkedAccount"].substring(4);
				    }
				    else{
				    disPlayLinkAccount=resulttable["linkedAccount"]
				    }
					
					frmAccountDetailsMB.lblUsefulValue11.text = formatAccountNo(disPlayLinkAccount);
				}
				dismissLoadingScreen();
			} else {
				dismissLoadingScreen();
				if (resulttable["additionalDS"][2]["StatusDesc"] == "LIQ NOT FOUND" && resulttable["additionalDS"][2]["StatusCode"] ==
					"-1000") {
					//frmAccountDetailsMB.hboxApplySendToSave.isVisible = true;
					frmAccountDetailsMB.hbxUseful2.isVisible = false;
					frmAccountDetailsMB.hbxUseful3.isVisible = false;
					frmAccountDetailsMB.hbxUseful11.isVisible = false;
				} else
					showCommonAlert(resulttable["errMsg"], resulttable["XPServerStatCode"]);
			}
		} else {
			dismissLoadingScreen();
			alert(" " + resulttable["errMsg"]);
		}
	}
}

function reformatDate(dateStr) {
	var newDt = "";  
    if(dateStr != null && dateStr != undefined){
		dstr1 = dateStr.substring(0, 4);
		dstr2 = dateStr.substring(5, 7);
		dstr3 = dateStr.substring(8, 10);
		newDt = dstr3 + "/" + dstr2 + "/" + dstr1;
	} 
	return newDt;
}

function formatAccountNo(accountNo) {
	
	if(accountNo == null ||accountNo=="" ) return;
	var length = accountNo.length;
	var fourthChar = accountNo.charAt(3);
	var middleFive = accountNo.substring(4, 9);
	var lastCharater = accountNo.charAt(length - 1);
	var formatedValue = accountNo.substring(0, 3) + "-" + fourthChar + "-" + middleFive + "-" + lastCharater;
	return formatedValue;
}

//function formatAccountNoNew(accountNo) {
//	var formatedValue = accountNo.substring(accountNo.length-10,accountNo.length);
//	
//	return formatedValue;
//}

function onclickgetTransferFromAccountsFromAccSmry() {
   	showLoadingScreen();
  	resetTransferRTPPush();
	if(checkMBUserStatus()){
      	//var accountStatus = gblAccountTable["custAcctRec"][glb_accountId].acctStatusCode;
        var selectedRow = frmAccountSummary.segAccountDetails.selectedRowItems[0];
    	kony.print("selRow>>"+selectedRow);
        var accountStatus = selectedRow["acctStatus"];
		if(accountStatus.indexOf("Active") == -1){
            dismissLoadingScreen();
			alert(""+ kony.i18n.getLocalizedString('keyNotAllloedTransactions'));
			return;
		}
		
		var noCASAAct = noActiveActs();
	 	if(noCASAAct)
		{
			showAlertWithCallBack(kony.i18n.getLocalizedString("MB_StatusNotEligible"), kony.i18n.getLocalizedString("info"),onclickActivationCompleteStart);
			return false;
		}   
		//var accId = gblAccountTable["accountSummary"][0]["custAcctRec"][glb_accountId]["accId"];
        var accId = selectedRow["accId"];
		//getTransferFromAccountsFromAccSmry(accId);
        glb_accId = accId
	    if (glb_accId.length == 10)
		  glb_accId = "0000" + glb_accId;
        recipientAddFromTransfer = false;
	    gblTransferFromRecipient =false;
        showPreTransferLandingPage();
	}
}

function getTransferFromAccountsFromAccSmry(accountId) {
	gblRefreshGraph = true; //Refresh bar graph
	ResetTransferHomePage();
	//TMBUtil.DestroyForm(frmTransferLanding);
	glb_accId = accountId
	if (glb_accId.length == 10)
		glb_accId = "0000" + glb_accId;
	kony.print(">>>>>>>>inside getTransferFromAccountsFromAccSmry11 ");
	// setMenuSegmentToFalse();
	GBLFINANACIALACTIVITYLOG = {}
	//gblRC_QA_TEST_VAL=1;
	if (gblRC_QA_TEST_VAL == 0) {
		callBackTransferFromAccountsStaticData()
	} else {
		var inputParam = {}
		inputParam["transferFlag"] = "true";
		recipientAddFromTransfer = false;
		gblTransferFromRecipient =false;
		showLoadingScreen();
		if(!gblfromCalender){
          kony.print(">>>>>>>>inside getTransferFromAccountsFromAccSmry22 ");
			invokeServiceSecureAsync("customerAccountInquiry", inputParam, callBackTransferFromAccountSummary)
		}else{
          	resetTransferRTPPush();
          kony.print(">>>>>>>>inside getTransferFromAccountsFromAccSmry33 ");
			invokeServiceSecureAsync("customerAccountInquiry", inputParam, callBackTransferFromAccountsFromSmry)
		}
	}
}

function callBackTransferFromAccountSummary(status, resulttable){
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			tmp_accId = glb_accId;
			var anyIDEligible = false;
			var foundData = false;
			var nonCASAAct = 0;
			var accountTotal = resulttable.custAcctRec.length;
			if(tmp_accId.length==14){
				tmp_accId=tmp_accId.substring(4);
			}
				for (var i = 0; i < resulttable.custAcctRec.length; i++) {
					var accountStatus = resulttable["custAcctRec"][i].acctStatus;
					anyIDEligible = resulttable.custAcctRec[i].anyIdAllowed == "Y";
					if(accountStatus.indexOf("Active") == -1){
						nonCASAAct = nonCASAAct + 1;
					}
					var joinType = resulttable.custAcctRec[i].partyAcctRelDesc;
					var AccStatCde = resulttable.custAcctRec[i].personalisedAcctStatusCode;
					
					if (AccStatCde == undefined || AccStatCde == "01" ||  AccStatCde == ""){	
						if (joinType == "PRIJNT" || joinType == "SECJAN" || joinType == "OTHJNT" || joinType == "SECJNT") {
							accountTotal--;
						} else {
							tmpAccountId = resulttable.custAcctRec[i].accId;
							 	if (tmpAccountId.length != 14)
									tmpAccountId = "0000" + tmpAccountId;
								if (tmpAccountId.length == 14)
								{
									if(tmpAccountId.charAt(0)==0 && tmpAccountId.charAt(1)==0 && tmpAccountId.charAt(2)==0 && tmpAccountId.charAt(3)==0 ){
										flag=true;
									}
									if(flag){
										tmpAccountId = tmpAccountId.substr(4, 10)
									}
								}
							if(accountStatus.indexOf("Active") >= 0){
								if(tmp_accId  == tmpAccountId){
									foundData = true;
									break;
								}
							}
						}
					 }
			   } 
				if(nonCASAAct==resulttable.custAcctRec.length)
				{
					showAlertWithCallBack(kony.i18n.getLocalizedString("MB_CommonError_NoSA"), kony.i18n.getLocalizedString("info"),onclickActivationCompleteStart);
					return false;
				}
				if(foundData){
					if(gblTransferPush){
                    	callBackTransferFromAccountsFromSmry(status, resulttable);
                    }else{
                      if(anyIDEligible){
                          showPreTransferLandingPage();
                      }else{
                          gblSelTransferMode = 1;
                          callBackTransferFromAccountsFromSmry(status, resulttable);
                      }
                    }
				}else{
                  		dismissLoadingScreen();
                  		if(gblTransferPush){
							if(accountTotal > 0){
								glb_accId = "";
								callBackTransferFromAccountsFromSmry(status, resulttable);
							}else{
                              showAlertWithCallBack(
                                kony.i18n.getLocalizedString("TRErr_NoFromAcc"), 
                                kony.i18n.getLocalizedString("info"), callBackPushRTP);
                            }
                        }else{
                          	resetTransferRTPPush();
							alert(kony.i18n.getLocalizedString("keyNotAllloedTransactions"));
                        }
						return false;
				}
		}else{
			dismissLoadingScreen();
			if(gblTransferPush){
			    showAlertWithCallBack(
                kony.i18n.getLocalizedString("MB_CommonError_NoSA"), 
            	kony.i18n.getLocalizedString("info"), callBackPushRTP);
			}else{
				resetTransferRTPPush();
			}
		}
	}
}

function callBackTransferFromAccountsFromSmry(status, resulttable) {
try{	
  if (status == 400) {
      if (resulttable["opstatus"] == 0) {
          ITMX_TRANSFER_ENABLE = resulttable["ITMX_TRANSFER_ENABLE"];
          ITMX_TRANSFER_EWALLET_ENABLE = resulttable["ITMX_TRANSFER_EWALLET_ENABLE"];
          ITMX_TRANSFER_FEE_LIMITS = resulttable["ITMX_TRANSFER_FEE_LIMITS"];
          deviceContactRefreshServerValue = resulttable["DEVICE_CONTACT_REFRESH_VALUE"];
          var setyourIDTransfer = resulttable["setyourIDTransfer"];
          GLOBAL_TODAY_DATE = resulttable["TODAY_DATE"];
		  kony.print("TransfersFEE: resulttable $$$$$=" + JSON.stringify(resulttable["getTransferFee"]));
          loadBankListForTransfers(resulttable["BankList"]);// transfer REdesign calling bank list in biggining and string in global variable
          var fromData = []
          if(resulttable.custAcctRec.length == undefined ){
            dismissLoadingScreen();
            alert("Accounts are not eligible for Transfer or either Hidden");
            return;
          }
          gblSelectedRecipentName=gblCustomerName;
          var allowed=false;
          var j = 1

			/*** below are configurable params driving from Backend and get Cached. **/
			gblMaxTransferORFT = resulttable.ORFTTransLimit;
			gblMaxTransferSMART = resulttable.SMARTTransLimit;
			gblTransORFTSplitAmnt = resulttable.ORFTTransSplitAmnt;
			gblTransSMARTSplitAmnt = resulttable.SMARTTransAmnt;
			gblLimitORFTPerTransaction = resulttable.ORFTTransSplitAmnt;
			gblLimitSMARTPerTransaction = resulttable.SMARTTransAmnt;
            /*** till hereee  ***/
		
        	gblTrnsfrcutoffInd=resulttable["getTransferFee"][0]["cutoffInd"];
  			gblTrnsfrorftcurrentDate=resulttable["getTransferFee"][0]["orftFeeDate"];
        	gblTrnsfrsmartDate=resulttable["getTransferFee"][0]["smartFeeDate"];
  			gblTrnsfrsmartDateNew=resulttable["getTransferFee"][0]["smartFeeDateNew"];
  			gblTrnsfrsmartfuturetime=resulttable["getTransferFee"][0]["smartFutureNew"];
  			gblTrnsfrorftFutureTime=resulttable["getTransferFee"][0]["orftFuture"];
        	
			var ORFTRange1Lower = resulttable.ORFTRange1Lower
			var ORFTRange1Higher = resulttable.ORFTRange1Higher
            var ORFTRange2Lower = resulttable.ORFTRange2Lower
			var ORFTRange2Higher = resulttable.ORFTRange2Higher
            var ORFTSPlitFeeAmnt1 = resulttable.ORFTSPlitFeeAmnt1
			var ORFTSPlitFeeAmnt2 = resulttable.ORFTSPlitFeeAmnt2
            gblORFTRange1Lower = resulttable.ORFTRange1Lower;
            gblORFTRange1Higher= resulttable.ORFTRange1Higher;
            gblORFTRange2Lower = resulttable.ORFTRange2Lower;
			gblORFTRange2Higher= resulttable.ORFTRange2Higher;
            gblORFTSPlitFeeAmnt1 = resulttable.ORFTSPlitFeeAmnt1;
			gblORFTSPlitFeeAmnt2 = resulttable.ORFTSPlitFeeAmnt2;
        
        	gblSMARTRange1Higher = resulttable.SMARTRange1Higher;
			gblSMARTRange1Lower = resulttable.SMARTRange1Lower;
			gblSMARTRange2Higher = resulttable.SMARTRange2Higher;
			gblSMARTRange2Lower = resulttable.SMARTRange2Lower;
			gblSMARTSPlitFeeAmnt1 = resulttable.SMARTSPlitFeeAmnt1;
			gblSMARTSPlitFeeAmnt2 = resulttable.SMARTSPlitFeeAmnt2;
        
            // Added for To Acc Prompt Pay
            setToAccPromptPayGlobalVars(resulttable);
        	var initDPParam = resulttable["initDPParam"];
        	if(initDPParam!=undefined&&initDPParam!=null&&initDPParam!=""){
              	gblDPPk = resulttable["initDPParam"][0]["pk"];
				gblDPRandNumber = resulttable["initDPParam"][0]["randomNumber"];
        		kony.print("Values for initDP Param :"+gblDPPk+" and"+gblDPRandNumber);
            }
        	gblALL_SMART_FREE_TRANS_CODES = resulttable["ALL_SMART_FREE_TRANS_CODES"];	
			gblSMART_FREE_TRANS_CODES = resulttable["SMART_FREE_TRANS_CODES"];	
			gblORFT_FREE_TRANS_CODES = resulttable["ORFT_FREE_TRANS_CODES"];
			gblORFT_ALL_FREE_TRANS_CODES = resulttable["ALL_ORFT_FREE_TRANS_CODES"];	
            kony.print("######Values for Fee Prod Codes"+gblALL_SMART_FREE_TRANS_CODES+"%"+gblSMART_FREE_TRANS_CODES+"%"+gblORFT_FREE_TRANS_CODES+"%"+gblORFT_ALL_FREE_TRANS_CODES);                         
			var SMARTRange1Higher = resulttable.SMARTRange1Higher
			var SMARTRange1Lower = resulttable.SMARTRange1Lower			
			var SMARTRange2Higher = resulttable.SMARTRange2Higher
			var SMARTRange2Lower = resulttable.SMARTRange2Lower			
			var SMARTSPlitFeeAmnt1 = resulttable.SMARTSPlitFeeAmnt1
			var SMARTSPlitFeeAmnt2 = resulttable.SMARTSPlitFeeAmnt2
			
			var WithinBankOwnAccLimit =resulttable.WithinBankOwnAccLimit
  			var WithinBankOwnAccLimitTransaction =resulttable.WithinBankOwnAccLimitTransaction
   
			gblXerSplitData = [];
			var temp1 = [];
			var temp = {
				ORFTRange1Lower: resulttable.ORFTRange1Lower,
				ORFTRange1Higher: resulttable.ORFTRange1Higher,
				SMARTRange1Higher: resulttable.SMARTRange1Higher,
				SMARTRange1Lower: resulttable.SMARTRange1Lower,
				ORFTRange2Lower: resulttable.ORFTRange2Lower,
				ORFTRange2Higher: resulttable.ORFTRange2Higher,
				SMARTRange2Higher: resulttable.SMARTRange2Higher,
				SMARTRange2Lower: resulttable.SMARTRange2Lower,
				ORFTSPlitFeeAmnt1: resulttable.ORFTSPlitFeeAmnt1,
				ORFTSPlitFeeAmnt2: resulttable.ORFTSPlitFeeAmnt2,
				SMARTSPlitFeeAmnt1: resulttable.SMARTSPlitFeeAmnt1,
				SMARTSPlitFeeAmnt2: resulttable.SMARTSPlitFeeAmnt2,
				WithinBankOwnAccLimit:resulttable.WithinBankOwnAccLimit,
    			WithinBankOwnAccLimitTransaction:resulttable.WithinBankOwnAccLimitTransaction
			}
			kony.table.insert(temp1, temp)
			gblXerSplitData = temp1;
			var nonCASAAct = 0;
            var productNameEn,productNameTh,nickName,custName,acctNo;
			gblTransfersOtherAccounts = [];
			for (var i = 0; i < resulttable.custAcctRec.length; i++) {
				var accountStatus = resulttable["custAcctRec"][i].acctStatus;
				var anyIDEligible = true;
				if(gblfromCalender && frmTransfersAckCalendar.lblMobileNumber.isVisible || ( gblSelTransferMode != 1 ) ){// if p2 p show only anyID eligible
                  	anyIDEligible = resulttable.custAcctRec[i].anyIdAllowed == "Y";
				}
				if(accountStatus.indexOf("Active") == -1){
					nonCASAAct = nonCASAAct + 1;
				}
				var joinType = resulttable.custAcctRec[i].partyAcctRelDesc;
				var AccStatCde = resulttable.custAcctRec[i].personalisedAcctStatusCode;
				if (AccStatCde == undefined || AccStatCde == "01" ||  AccStatCde == ""){	
                  if (joinType == "PRIJNT" || joinType == "SECJAN" || joinType == "OTHJNT" || joinType == "SECJNT") {
                    if(isNotBlank(resulttable.custAcctRec[i].productNmeEN)){
                      productNameEn = resulttable.custAcctRec[i].productNmeEN;
                    }else { productNameEn= "";}
                    if(isNotBlank(resulttable.custAcctRec[i].productNmeTH)){
                       productNameTh = resulttable.custAcctRec[i].productNmeTH;
                    }else{productNameTh =""}
                    if(isNotBlank(resulttable.custAcctRec[i].acctNickName)){
                      nickName = resulttable.custAcctRec[i].acctNickName; 
                    }else {nicName = "";}
                    if(isNotBlank(resulttable.custAcctRec[i].accountName)){
                      custName = resulttable.custAcctRec[i].accountName; 
                    }else {custName = "";}
                    if(isNotBlank(resulttable.custAcctRec[i].accId)){
                      acctNo = resulttable.custAcctRec[i].accId; 
                    }else {acctNo = "";}
                    var temp = [{
                                custName: custName,
                                acctNo: addHyphenMB(acctNo),
                                nickName: nickName,
                                productName: productionNameTrun(productNameEn),
                                productNameTH: productionNameTrun(productNameTh)
                    			}];
					kony.table.insert(gblTransfersOtherAccounts, temp[0])
					}else{
						var icon = "";
					  	var iconcategory = "";					
						//icon="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+"NEW_"+resulttable.custAcctRec[i]["ICON_ID"]+"&modIdentifier=PRODICON";	
	                	var icon=loadFromPalleteIcons(resulttable["custAcctRec"][i]["ICON_ID"]);
                    	kony.print("^^^^^Transfers^^^^^"+icon);
                      	iconcategory=resulttable.custAcctRec[i]["ICON_ID"];
	                	// added new From account widget
	                	if (iconcategory=="ICON-01"||iconcategory=="ICON-02") {
		                    var temp = createSegmentRecord(resulttable.custAcctRec[i], hbxSliderNew1, icon)
		                } else if (iconcategory=="ICON-03") {
		                    var temp = createSegmentRecord(resulttable.custAcctRec[i], hbxSliderNew2, icon)
		                } else if (iconcategory=="ICON-04") {
		                    var temp = createSegmentRecord(resulttable.custAcctRec[i], hbxSliderNew3, icon)
		                }
						if(anyIDEligible && accountStatus.indexOf("Active") >= 0)
							kony.table.insert(fromData, temp[0]);
					}
				 }// if end
            } //for
			// for other accounts
			if(nonCASAAct==resulttable.custAcctRec.length){
				showAlertWithCallBack(kony.i18n.getLocalizedString("MB_CommonError_NoSA"), kony.i18n.getLocalizedString("info"),onclickActivationCompleteStart);
				return false;
			}
			for (var i = 0; i < resulttable.OtherAccounts.length; i++) {
				var prdCode = resulttable.OtherAccounts[i].productID;
				if (prdCode == 290) {
					var accountId=kony.string.replace(resulttable.OtherAccounts[i].accId, "-", "");
					if (accountId.length == 14) {
						resulttable.OtherAccounts[i].accId=accountId.substring(4,14);
					}
               
                    if(isNotBlank(resulttable.OtherAccounts[i].productNmeEN)){
                      productNameEn = resulttable.OtherAccounts[i].productNmeEN;
                    }else{productNameEn="";}
                    if(isNotBlank(resulttable.OtherAccounts[i].productNmeTH)){
                       productNameTh = resulttable.OtherAccounts[i].productNmeTH;
                    }else{productNameTh = "";}
                  	if(isNotBlank(resulttable.OtherAccounts[i].acctNickName)){
                      nickName = resulttable.OtherAccounts[i].acctNickName; 
                    }else {nicName = "";}
                    if(isNotBlank(resulttable.OtherAccounts[i].accountName)){
                      custName = resulttable.OtherAccounts[i].accountName; 
                    }else {custName = "";}
                    if(isNotBlank(resulttable.OtherAccounts[i].accId)){
                      acctNo = resulttable.OtherAccounts[i].accId; 
                    }else {acctNo = "";}
					var temp = [{
						custName: custName,
						acctNo: addHyphenMB(acctNo),
						nickName: nickName,
						productName: productionNameTrun(productNameEn),
						productNameTH: productionNameTrun(productNameTh)							
					}]
					kony.table.insert(gblTransfersOtherAccounts, temp[0])
				}
			}
			if(gblDeviceInfo["name"] == "android"){
				if(fromData.length == 1){
					frmTransferLanding.segTransFrm.viewConfig = {
			            "coverflowConfig": {
		    	            "rowItemRotationAngle": 0,
		        	        "isCircular": false,
		            	    "spaceBetweenRowItems": 10,
			                "projectionAngle": 90,
			                "rowItemWidth": 80
			            }
			        };
				}else{
					frmTransferLanding.segTransFrm.viewConfig = {
		            "coverflowConfig": {
		                "rowItemRotationAngle": 0,
		                "isCircular": true,
		                "spaceBetweenRowItems": 10,
		                "projectionAngle": 90,
		                "rowItemWidth": 80
			            }
			        };
				}
			}
			frmTransferLanding.segTransFrm.widgetDataMap = {
				lblACno: "lblACno",
				lblAcntType: "lblAcntType",
				img1: "img1",
				lblCustName: "lblCustName",
				lblBalance: "lblBalance",
				lblActNoval: "lblActNoval",
				lblDummy: "lblDummy",
				lblSliderAccN1: "lblSliderAccN1",
				lblSliderAccN2: "lblSliderAccN2",
				lblRemainFee: "lblRemainFee",
				lblRemainFeeValue: "lblRemainFeeValue"
			}
			gblNoOfFromAcs = fromData.length;
			if(gblNoOfFromAcs == 0){
		       showAlert(kony.i18n.getLocalizedString("MB_CommonError_NoSA"), kony.i18n.getLocalizedString("info"));
		       dismissLoadingScreen();
		       return false;
			}			
			var selectedIndex = 0;
			if(glb_accId.length==14){
				glb_accId=glb_accId.substring(4);
				}
			if(isNotBlank(glb_accId)){
				gblSelTransferFromAcctNo = glb_accId;
			}	
			for (var i = 0; i < gblNoOfFromAcs; i++) {
				var acctNo = fromData[i]["lblActNoval"]
				acctNo = kony.string.replace(acctNo, "-", "");
				if (glb_accId == acctNo) {
					selectedIndex = i;
					allowed=true;
					break;
				}
			}
			if(!allowed && !gblfromCalender && isNotBlank(glb_accId)){
              	resetTransferRTPPush();
				dismissLoadingScreen();
				alert(kony.i18n.getLocalizedString("keyNotAllloedTransactions"));
				return false;
			}

			if (selectedIndex)	
				gbltranFromSelIndex = [0, selectedIndex];
			else
				gbltranFromSelIndex = [0, 0];
			if(gblfromCalender){
              kony.print(">>>>>>>>inside callBackTransferFromAccountsFromSmry44 "+gblSelTransferMode);
				resetTransferRTPPush();
				frmTransferLanding.segTransFrm.data = fromData;
				frmTransferLanding.segTransFrm.selectedIndex = gbltranFromSelIndex;
				//TODO : Add transfer to citizen id method
				if(frmTransfersAckCalendar.lblMobileNumber.isVisible){
					if(gblSelTransferMode == 2){
						transferAgainMobileTransaction();
					} else if(gblSelTransferMode == 3){
						transferAgainCitizenTransaction();
					} else if(gblSelTransferMode == 4){
						transferAgainTaxIDTransaction();
					} else if(gblSelTransferMode == 5){
						transferAgaineWalletTransaction();
					} else if(gblSelTransferMode == 0){
						//transferAgaineAccountTransaction();
                      	populateCompletedTransferDataOnTransferAgain();
					}
                  	
				}else{
					populateCompletedTransferDataOnTransferAgain();
				}
				gbltdFlag = fromData[selectedIndex].tdFlag;
				toggleTDMaturityCombobox(gbltdFlag);
                kony.print("lblTransNPbAckTotVal2>>"+frmTransfersAckCalendar.lblTransNPbAckTotVal2.text);
                var fAmount = frmTransfersAckCalendar.lblTransNPbAckTotVal2.text;
                kony.print("fAmount>>"+fAmount);
                var amt = frmTransfersAckCalendar.lblHiddenTransferAmt.text;
                if(isNotBlank(amt)){
                 if(amt > 0){
                     frmTransferLanding.txtTranLandAmt.text = commaFormatted(parseFloat(removeCommaIB(amt)).toFixed(2));
                 } else{
                    frmTransferLanding.txtTranLandAmt.text = commaFormatted(parseFloat(removeCommaIB(fAmount)).toFixed(2));;
                 }
                }
                kony.print("Amount >>>>"+frmTransferLanding.txtTranLandAmt.text);
              	var transAmount = frmTransferLanding.txtTranLandAmt.text;
                if(isNotBlank(transAmount)){
                  transAmount = transAmount.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
                  frmTransferLanding.txtTranLandAmt.text = transAmount.trim();
                }
                dismissLoadingScreen();
				frmTransferLanding.show();
				fromAccountIsOnlyAllowedForTMB();
				//showLoadingScreen();
				if(gblSelTransferMode == 2){
					invokeServiceP2PITMX(true);
				}else if(gblSelTransferMode == 3 || gblSelTransferMode == 4 || gblSelTransferMode == 5){
                  onTextChangeToCitizenIDP2P("");
				}
			}else if(gblshorCutToAccounts.indexOf(fromData[0].prodCode) < 0
				&& gblshorCutToAccounts.indexOf(fromData[selectedIndex].prodCode) >= 0 ){
				frmTransferLanding.hboxTD.setVisibility(false);
 				frmTransferLanding.lineTD.setVisibility(false); 
				//frmTransferLanding.lineTD.skin = linePopupBlack
				ResetTransferHomePage();
				gbltdFlag="";
				gblisTMB=gblTMBBankCD;
				assignBankSelectDetails(gblisTMB);
				enteringAccountNumberVisbilty(false);
				frmTransferLanding.lblTranLandToName.text = fromData[selectedIndex].nickName;
				frmTransferLanding.lblTranLandToAccountNumber.text = fromData[selectedIndex].lblActNoval;
				frmTransferLanding.tbxAccountNumber.text = fromData[selectedIndex].lblActNoval;
				//frmTransferLanding.imgTranLandTo.src=gblMyProfilepic;
				//frmTransferLanding.lineRecipientDetails.skin = lineBlue;
				gblBANKREF="TMB";
				gblTransEmail=1;
				gblTrasSMS=1;
				gblToAccountType = kony.i18n.getLocalizedString("termDeposit");
				//fromData.splice(selectedIndex,1);
				gbltranFromSelIndex = [0, 0];
				frmTransferLanding.segTransFrm.data = fromData;
				frmTransferLanding.segTransFrm.selectedIndex = [0,0];
				dismissLoadingScreen();
				frmTransferLanding.show();
				frmTransferLanding.txtTranLandAmt.setFocus(true);
              	if(gblTransferPush){
                  preFillDataTransfer();
                }else{
                  resetTransferRTPPush();
                  dismissLoadingScreen();
                  frmTransferLanding.show();
                  frmTransferLanding.txtTranLandAmt.setFocus(true);
                }
			}else{
				frmTransferLanding.segTransFrm.data = fromData;
				frmTransferLanding.segTransFrm.selectedIndex = gbltranFromSelIndex;
              	if(gblTransferPush){
					preFillDataTransfer();
                }else{
                  resetTransferRTPPush();
                  dismissLoadingScreen();
                  frmTransferLanding.show();
                  // Code change for IOS 9
                  gbltdFlag = fromData[selectedIndex].tdFlag;
                  toggleTDMaturityCombobox(gbltdFlag);
                  if(gblSelTransferMode == 1  ){
                         onClickOnUsAccountNumber();
                  }else if(gblSelTransferMode == 2){
                      onClickOnUsMobileNumber();
                  }else if(gblSelTransferMode == 3){
                      onClickOnUsCitizen();
                  }else if(gblSelTransferMode == 4){
                      onClickP2PToTaxID();
                  }else if(gblSelTransferMode == 5){
                      onClickP2PToeWallet();
                  }
                  //Auto Populate TMB if from Account is only eligible to transfer to TMB
                  fromAccountIsOnlyAllowedForTMB();
                }
			}
			/*** decide whether the current selected from is TD accnt */
		} else {
          
          	var msgError = "";
			dismissLoadingScreen();
			if(resulttable["errMsg"] == "No Valid Accounts Available for Transfer"){
				msgError =kony.i18n.getLocalizedString("MB_CommonError_NoSA");
			}else{
              	msgError =" " + resulttable["errMsg"];			
			}
            if(gblTransferPush){
            	showAlertWithCallBack(msgError, kony.i18n.getLocalizedString("info"), callBackPushRTP);
            }else{
              	resetTransferRTPPush();
            	alert(msgError);
            }
		}
	}
  
}catch(e){
  kony.print("Exception in callBackTransferFromAccountsFromSmry>>"+e);
}
}


function callBeneficiaryInquiryService(accid){
	//showLoadingScreen();
	kony.print("***callBeneficiaryInquiryService*** accid="+accid);
	var inputparam = {};
	inputparam["acctId"] = accid;
	gblSavingsCareAccId=accid;
	var status = invokeServiceSecureAsync("beneficiaryInq", inputparam, beneficiaryInquiryCallBack);
}

gblNeverSetBeneficiary="";
function beneficiaryInquiryCallBack(status, resulttable) {
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			if (resulttable["StatusCode"] == 0) {
			kony.print("**beneficiaryInq is success** " + JSON.stringify(resulttable));
					gblBeneficiaryData = resulttable["beneficiaryDS"];
					//gblNeverSetBeneficiary = resulttable["neverSetBeneficiary"];
					gblNeverSetBeneficiary=resulttable["neverSetBeneficiary"];
					if(gblSavingsCareFlow=="AccountDetailsFlow"){
						handleSavingCareMBDetails(frmAccountDetailsMB);
					}else if(gblSavingsCareFlow=="MyAccountFlow"){
						handleSavingCareMBDetails(frmMyAccountView);
					}
			} else {
				kony.print("**beneficiaryInq is Failed1**");
					dismissLoadingScreen();
				//alert(" " + resulttable["errMsg"]);
			}
		} else {
			kony.print("**beneficiaryInq is Failed2**");
			dismissLoadingScreen();
			//alert(" " + resulttable["errMsg"]);
		}
	}
}

function resetBeneficiaryFields(frmName){
kony.print("GOWRI enetered resetBeneficiaryFields started ");
		gblSavingsCareFlow="";
		gblBeneficiaryData="";
		frmName.segBenefiList.setVisibility(false);
		frmName.hboxBenefiListTitle.setVisibility(false);
		frmName.hbox566614587293709.setVisibility(false);
		if(frmName.id !="frmMyAccountView"){
			frmName.linkEditBenefi.setVisibility(false);
		}
		frmName.lblbenefi.text="";
		frmName.richtextDescription.text="";
		kony.print("GOWRI enetered resetBeneficiaryFields done ");
}

function handleSavingCareMBDetails(frmName){
//frmAccountDetailsMB
//frmMyAccountView
	kony.print("enetered in handleSavingCareMBDetails");
	var openingMethod="";
	gblOpeningMethod="";
	if(frmName.id =="frmMyAccountView"){
		openingMethod= frmMyAccountList.segTMBAccntDetails.selectedItems[0].openingMethod;
	}else{
		frmAccountDetailsMB.flexLineEditBeneficiaries.setVisibility(true);
		frmAccountDetailsMB.flexLinkEditBeneficiaries.setVisibility(true);
		frmAccountDetailsMB.Copyflexlinebottom0ece6d5355f9943.setVisibility(true);
        var selectedRow = frmAccountSummary.segAccountDetails.selectedRowItems[0];
		openingMethod = selectedRow["openingMethod"];//gblAccountTable["custAcctRec"][gblIndex]["openingMethod"];
        kony.print("openingMethod>>"+openingMethod);
		//frmName.linkEditBenefi.setVisibility(true);
		frmAccountDetailsMB.flexBenefiListHeader.setVisibility(true);
		frmAccountDetailsMB.flexBenefiList.setVisibility(true);
		frmAccountDetailsMB.flexMaturityDisplayHeader.setVisibility(false);
		frmAccountDetailsMB.flexMaturityDisplay.setVisibility(false);
	}
	gblOpeningMethod=openingMethod;
	kony.print("GOWRI openingMethod handleSavingCareMBDetails="+openingMethod);
	var data=[];
	if(gblBeneficiaryData.length>0){
		var currLocale=kony.i18n.getCurrentLocale();
		for(var i=0;i<gblBeneficiaryData.length;i++){
			var benRec={};
			benRec["lblnameVal"]=gblBeneficiaryData[i]["fullName"];
			if (currLocale == "en_US") {
            	benRec["lblrelationVal"]=gblBeneficiaryData[i]["relationEN"];
            }else{
            	benRec["lblrelationVal"]=gblBeneficiaryData[i]["relationTH"];
            }
            benRec["lblbenefitVal"]=gblBeneficiaryData[i]["percentValue"]+" %";
            kony.print("Name="+benRec["lblnameVal"]+" Relation="+benRec["lblrelationVal"]+" Percentage="+benRec["lblrelationVal"])
            data.push(benRec);
		}
			frmName.segBenefiList.setData(data);
			frmName.segBenefiList.setVisibility(true);
			if(frmName == frmMyAccountView ){
				frmMyAccountView.hboxBenefiListTitle.setVisibility(true);
			}
			frmName.hboxBeneficiary.setVisibility(true);
			frmName.hbox566614587293709.setVisibility(false);	
			frmName.lblname.text=kony.i18n.getLocalizedString("name");
			frmName.lblrelation.text=kony.i18n.getLocalizedString("keyOpenRelation");
			frmName.lblbenefit.text=kony.i18n.getLocalizedString("benefitKey");
			frmName.lblbenefi.text=kony.i18n.getLocalizedString("keyBenificiaries")+ " : "+ kony.i18n.getLocalizedString("keySpecify");

	}else{
		//kony.print("GOWRI no beneficieris gblNeverSetBeneficiary=" + gblNeverSetBeneficiary); && "Y"==gblNeverSetBeneficiary
		if("BRN" == openingMethod && gblNeverSetBeneficiary=="Y"){
				kony.print("GOWRI no beneficieris in BRN");
				//frmName.lblbenefi.text=kony.i18n.getLocalizedString("keyBenificiaries")+ " :"				
				frmName.segBenefiList.setVisibility(false);
				if(frmName == frmMyAccountView ){
				frmMyAccountView.hboxBenefiListTitle.setVisibility(false);
					}
				frmName.hboxBeneficiary.setVisibility(false);
				frmName.richtextDescription.setVisibility(true);
				frmName.hbox566614587293709.setVisibility(true);
				if(gblSavingsCareFlow=="AccountDetailsFlow"){
					frmName.flexMaturityDisplayHeader.setVisibility(false);
					frmName.richtextDescription.text=kony.i18n.getLocalizedString("nobenefic");
					frmAccountDetailsMB.flexBenefiListHeader.setVisibility(false);
				}
				else{
					frmName.richtextDescription.text=kony.i18n.getLocalizedString("nospecifyMyAcct");
				}

		}else{
				kony.print("GOWRI no beneficieris in INT");
				frmName.lblbenefi.text=kony.i18n.getLocalizedString("keyBenificiaries")+ " : "+ kony.i18n.getLocalizedString("keynotSpecify");
				frmName.segBenefiList.setVisibility(false);
				if(frmName == frmMyAccountView ){
				frmMyAccountView.hboxBenefiListTitle.setVisibility(false);
					}
				frmName.hboxBeneficiary.setVisibility(true);
				frmName.hbox566614587293709.setVisibility(true);
				frmName.richtextDescription.setVisibility(true);
				frmName.richtextDescription.text=kony.i18n.getLocalizedString("benefinotSpecifyDesc");
				if(gblSavingsCareFlow=="AccountDetailsFlow"){
					frmName.flexMaturityDisplayHeader.setVisibility(false);
					frmAccountDetailsMB.flexBenefiListHeader.setVisibility(false);
				}
				
		}
	}
	dismissLoadingScreen();

}

function callDepositAccountInquiryService() {
	
	var inputparam = {};
    var selectedRow = frmAccountSummary.segAccountDetails.selectedRowItems[0];
	inputparam["acctId"] = selectedRow["accId"];
    showLoadingScreen();
    var status = invokeServiceSecureAsync("depositAccountInquiry", inputparam, depositAccountInquiryCallBack);
}

function depositAccountInquiryCallBack(status, resulttable) {
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
            dismissLoadingScreen();
			gblenableDebitCardProductList = resulttable["enableDebitCardProductList"];
            gblenableDebitCardAccountStatus = resulttable["enableDebitCardAccountStatus"];
			if (resulttable["bankAccountStatus"] != null && resulttable["bankAccountStatus"] != "") {
			    var locale = kony.i18n.getCurrentLocale();
			    gblAccountStatus=resulttable["bankAccountStatus"];
				var status=resulttable["bankAccountStatus"].split("|");
				if (locale == "en_US")
				frmAccountDetailsMB.lblUsefulValue5.text = status[0];
				else
				frmAccountDetailsMB.lblUsefulValue5.text = status[1];
			}
			if (resulttable["accountTitle"] != null && resulttable["accountTitle"] != "") {
				frmAccountDetailsMB.lblUsefulValue4.text = resulttable["accountTitle"];
			}
			var availableBal;
			var ledgerBal;
			if (resulttable["AcctBal"].length > 0) {
				for (var i = 0; i < resulttable["AcctBal"].length; i++) {
					if (resulttable["AcctBal"][i]["BalType"] == "Avail") {
						availableBal = resulttable["AcctBal"][i]["Amt"];
					}
					if (resulttable["AcctBal"][i]["BalType"] == "Ledger") {
						ledgerBal = resulttable["AcctBal"][i]["Amt"];
					}
				}
			}
			if (null != availableBal) {
				frmAccountDetailsMB.lblAccountBalanceHeader.text = commaFormatted(availableBal) + " " + kony.i18n.getLocalizedString(
					"currencyThaiBaht");
			}
			if (null != ledgerBal) {
				frmAccountDetailsMB.lblledgerbalval.text = commaFormatted(ledgerBal) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
			}
			simpleAccountFunction();   
			var selectedRow = frmAccountSummary.segAccountDetails.selectedRowItems[0];
			if(gblAccountTable["SAVING_CARE_PRODUCT_CODES"].indexOf(selectedRow["productID"]) >= 0){
				gblAccountDetailsNickName=selectedRow["acctNickName"]; //AccountDetails specific
				gblFinActivityLogOpenAct["prodNameEN"] = selectedRow["ProductNameEng"];//c
				gblFinActivityLogOpenAct["prodNameTH"] =selectedRow["ProductNameThai"];//c
				gblSCFromFiident=selectedRow["fiident"];
				gblSCFromActName=selectedRow["accountName"];
				gblSCFromActType=selectedRow["accType"];
				gblBeneficiaryData="";				
				var accid=selectedRow["accId"];
				gblOpenSavingsCareNickName=selectedRow["acctNickName"];//Not sure on defaultCurrentNickNameEN
				gblSavingsCareFlow="AccountDetailsFlow";//frmAccountDetailsMB
				gblSelOpenActProdCode=selectedRow["productID"];//common
				gblAccountId=accid;				
				//The below Gbl variables are used in Editing Nickname service
				gblSavingsCareAccId=accid;
				gblSavingsCarepersonalizedId=selectedRow["persionlizedId"];
				gblSavingsCareBankCD = selectedRow["bankCD"];
				
				callBeneficiaryInquiryService(accid);				
			}else{
				gblSavingsCareFlow="";
			}
			
		} else {
			alert(" " + resulttable["errMsg"]);
			dismissLoadingScreen();
		}
	}
}

function callDepositAccountInquiryServiceNofee() {
	 var inputparam = {};
     var selectedRow = frmAccountSummary.segAccountDetails.selectedRowItems[0];
	 inputparam["acctId"] = selectedRow["accId"];
     var status = invokeServiceSecureAsync("depositAccountInquiry", inputparam, depositAccountInquiryCallBackNofee);
}

function depositAccountInquiryCallBackNofee(status, resulttable) {
	var StatusCode = resulttable["StatusCode"];
	var Severity = resulttable["Severity"];
	var StatusDesc = resulttable["StatusDesc"];
	if (status == 400) {
		
		if (resulttable["opstatus"] == 0) {
			dismissLoadingScreen();
            gblenableDebitCardProductList = resulttable["enableDebitCardProductList"];
            gblenableDebitCardAccountStatus = resulttable["enableDebitCardAccountStatus"];
			if (resulttable["bankAccountStatus"] != null && resulttable["bankAccountStatus"] != "") {
			    gblAccountStatus=resulttable["bankAccountStatus"];
				var status=resulttable["bankAccountStatus"].split("|");
				var locale = kony.i18n.getCurrentLocale();
				if (locale == "en_US")
				frmAccountDetailsMB.lblUsefulValue6.text = status[0];
				else
				frmAccountDetailsMB.lblUsefulValue6.text = status[1];
			}
			if (resulttable["accountTitle"] != null && resulttable["accountTitle"] != "") {
				frmAccountDetailsMB.lblUsefulValue5.text = resulttable["accountTitle"];
			}
			var availableBal;
			var ledgerBal;
			if (resulttable["AcctBal"].length > 0) {
				for (var i = 0; i < resulttable["AcctBal"].length; i++) {
					if (resulttable["AcctBal"][i]["BalType"] == "Avail") {
						availableBal = resulttable["AcctBal"][i]["Amt"];
					}
					if (resulttable["AcctBal"][i]["BalType"] == "Ledger") {
						ledgerBal = resulttable["AcctBal"][i]["Amt"];
					}
				}
			}
			if (null != availableBal) {
				frmAccountDetailsMB.lblAccountBalanceHeader.text = commaFormatted(availableBal) + " " + kony.i18n.getLocalizedString(
					"currencyThaiBaht");
			}
			if (null != ledgerBal) {
				frmAccountDetailsMB.lblledgerbalval.text = commaFormatted(ledgerBal) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
			}
			noFeeSavingAccount();   //callDebitCardInqService(); // skip customerCardList call
            
			//noFeeSavingAccount();
			//kony.application.dismissLoadingScreen();
		} else {
			alert(" " + resulttable["errMsg"]);
			kony.application.dismissLoadingScreen();
		}
	}
}

function callDebitCardInqService() {
	showLoadingScreen();
	var inputparam = {};
	gblDebitCardResult="";
	inputparam = {};
    inputparam["cardFlag"] = "2";
    //inputparam["acctId"] = gblAccountTable["custAcctRec"][gblIndex]["accId"];
    var selectedRow = frmAccountSummary.segAccountDetails.selectedRowItems[0];
    inputparam["acctId"] = selectedRow["accId"]; 
    var status = invokeServiceSecureAsync("customerCardList", inputparam, debitCardInqCallBack);
	
}
function callCreditReadyCardInqService() {
	var inputparam = {};
	showLoadingScreen();
    gblCardList = "";
    gblSelectedCard = "";
    gblManageCardFlow = "";
	if (gblUserLockStatusMB == "03") {
    	showTranPwdLockedPopup();
    } else {
        inputparam = {};
	    inputparam["cardFlag"] = "1";
        var selectedRow = frmAccountSummary.segAccountDetails.selectedRowItems[0];
    	inputparam["acctId"] = selectedRow["accId"]; 
	    //inputparam["cardRefId"] = gblAccountTable["custAcctRec"][gblIndex]["cardRefId"];
	    //kony.print("cardRefId manage>>>>>"+cardRefId);
	    invokeServiceSecureAsync("customerCardList", inputparam, getCallbackShowManageCard);
    }
}
function debitCardInqCallBack(status, resulttable) {
	if (status == 400) {
		
		if (resulttable["opstatus"] == 0) {
			if (resulttable["statusCode"] == 0) {
               	dismissLoadingScreen();
				//stroing here for the Bedit Card management;
				gblDebitCardResult=resulttable;
				GLOBAL_DEBITCARD_TABLE = resulttable["debitCardRec"];
				if (glb_viewName == "PRODUCT_CODE_NOFEESAVING_TABLE") {
					noFeeSavingAccount();
				} else if (glb_viewName == "PRODUCT_CODE_SAVING_TABLE" || glb_viewName == "PRODUCT_CODE_CURRENT_TABLE" ||
						glb_viewName == "PRODUCT_CODE_SAVINGCARE" || glb_viewName == "PRODUCT_CODE_NEWREADYCASH_TABLE") {
					simpleAccountFunction();
				}
				//moreAccountDetails();
			
				//checkShowDebitCardMenu();
			} else {
				dismissLoadingScreen();
				//alert(" " + resulttable["errMsg"]);
			}
		} else {
			dismissLoadingScreen();
			//alert(" " + resulttable["errMsg"]);
		}
	}
}

function onClickWhenNoAcctMB(){
    //alert("onClickWhenNoAcctMB");
    frmMyAccntAddAccount.show();
    myAccountListService();
}

function callcustCardInqService() {
	showLoadingScreen();
	var inputparam = {};
	gblDebitCardResult="";
	inputparam = {};
    inputparam["cardFlag"] = "2";
    var selectedRow = frmAccountSummary.segAccountDetails.selectedRowItems[0];
    inputparam["acctId"] = selectedRow["accId"]; //gblAccountTable["custAcctRec"][gblIndex]["accId"];
    var status = invokeServiceSecureAsync("customerCardList", inputparam, custCardInqCallBack);	
}


function custCardInqCallBack(status, resulttable) {
	if (status == 400) {
		
		if (resulttable["opstatus"] == 0) {
			if (resulttable["statusCode"] == 0) {
				//stroing here for the Dedit Card management;
				gblDebitCardResult=resulttable;
				GLOBAL_DEBITCARD_TABLE = resulttable["debitCardRec"];		
				dismissLoadingScreen();
                gblCardList = gblDebitCardResult;
                getCallbackShowDebitCardList(400, gblCardList);
			} else {
				dismissLoadingScreen();
			}
		} else {
			dismissLoadingScreen();
		}
	}
}