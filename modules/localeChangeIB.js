/**
 * Success callback on locale change in IB
 */

//function onSuccessLocaleChangeIB() {
//	var currentFormId = kony.application.getCurrentForm()
//		.id;
//	var formLocaleList = {
//		"frmIBMyReceipentsHome": syncIBRcLocale,
//		"frmIBMyReceipentsAccounts": syncIBRcLocale,
//		"frmIBMyReceipentsAddBankAccnt": syncIBRcLocale,
//		"frmIBMyReceipentsAddContactFB": syncIBRcLocale,
//		"frmIBMyReceipentsAddContactManually": syncIBRcLocale,
//		"frmIBMyReceipentsAddContactManuallyConf": syncIBRcLocale,
//		"frmIBMyReceipentsEditAccountConf": syncIBRcLocale,
//		"frmIBMyReceipentsFBContactConf": syncIBRcLocale,
//		"frmIBTranferLP": handleLocaleChangeIBTranferLP,
//		"frmIBTransferNowConfirmation": handleLocaleChangeIBTransferNowConfirmation,
//		"frmIBTransferNowCompletion": handleLocaleChangeIBTransferNowCompletion,
//		"frmIBCMMyProfile": IBCMMyProfilePreShow,
//		"frmApplyMBviaIBStep1": handleLocaleChangeIBApplyServiceMain,
//		"frmApplyMbviaIBConf": handleLocaleChangeIBApplyServiceConf
//	}
//	formLocaleList[currentFormId].call();
//}
/**
 * Failure callback on locale change in IB
 * @returns {}
 */

//function onFailureLocaleChangeIB() {
//	alert("Unable to change onFailureLocaleChange");
//}
/**
 * Method to sync IB locale
 * @returns {}
 */

function syncIBLocale() {
  syncIBBillPayment();
  syncIBMyActivities();
  syncIBBeepAndBill();
  syncIBEditFT();
  syncContactUs();
  syncIBChequeServices();
  syncIBDreamSavingMaintainance();
  syncIBAccountDetailsUpdate();
  syncIBProfileLocale();
  syncIBTransferLocale();
  syncIBRcLocale();
  syncIBApplyServicesLocale();
  syncIBTopupLocale();
  syncIBFullStmtLocale();
  syncIBNotfnInboxLocale();
  syncIBOpenAcctLocale();
  syncIBFindTMB();
  syncIBHotPromotions();
  ChangeCampaignLocale();
  syncIBContactUs();
  syncIBMyAccounts();
  changeCampaignPreLocale();
  syncMyBillers();
  syncIBMyAccountSummary();
  syncIBFATCALocale();
  syncIBEditBP();
  syncIBSoGooODLocale();
  syncIBCardActivate();
  syncIBPointRedemption();
  syncIBEStatement();
  syncIBMutualFund();
  syncIBBankAssurance();
  syncIBOTPAndToken();
  syncIBOpenAccountKYC();
  syncIBAnyIDRegistrationFlow();
  syncIBTransferLP();
  syncIBTransferNowConfirmation();
  syncIBTransferNowCompletion();
  syncIBOpenNewDreamAccConfirmation();
  syncIBOpenNewDreamAccComplete();
  syncIBEditMyAccountsFlow();
  syncIBExecutedTransaction();
  syncIBMFSelRedeemFund();
  syncIBMFSuitExpiry();
  syncIBMFSuitTnC();
  syncIBMFSuitTest();
  syncIBMFSuitReview();
  syncIBMFTnC();
  syncIBMFConfirm();
  syncIBMFComplete();
  syncIBMFSelPurchaseFundsEntry();
  syncIBMFSelPurchaseFundsLP();
  syncIBTransferCustomWidgetLP();
}

function syncIBMFSuitReview(){
  var currentFormId = kony.application.getCurrentForm();
  if (currentFormId.id == "frmIBMFSuitReview") {
    frmIBMFSuitReview.lblUrRiskAssesment.text = kony.i18n.getLocalizedString("MF_lbl_01");
    var levelRiskVal = kony.i18n.getLocalizedString("MF_lbl_02");
    levelRiskVal = levelRiskVal.replace("{x}", gblMFRiskLevel);
    levelRiskVal = levelRiskVal.replace("{y}", "5");
    frmIBMFSuitReview.lblMessage.text = levelRiskVal;
    frmIBMFSuitReview.lblTypeOfInvest.text = kony.i18n.getLocalizedString("MF_lbl_InvestorType");
    if(kony.i18n.getCurrentLocale() == "en_US"){
      frmIBMFSuitReview.lblTypeOfInvestVal.text = gblInvestorTypeEN;
    }else{
      frmIBMFSuitReview.lblTypeOfInvestVal.text = gblInvestorTypeTH;
    }
    frmIBMFSuitReview.linkTakeItAgain.text = kony.i18n.getLocalizedString("MF_lbl_Takeagain");
    var msg = frmIBMFSuitReview.lblDateOfExpVal.text;
    var data = msg.split(": ");
    frmIBMFSuitReview.lblDateOfExpVal.text = kony.i18n.getLocalizedString("MF_lbl_ExpiredDate") + ": " +data[1];
    if(frmIBMFSuitReview.lblDateOfExpiry.isVisible){
      msg = frmIBMFSuitReview.lblDateOfExpiry.text;
      data = msg.split(": ");
      frmIBMFSuitReview.lblDateOfExpiry.text = kony.i18n.getLocalizedString("MF_lbl_SuitabilityDate") + ": "+data[1];  
    }
    setFocusOnBtnAccountSum(kony.application.getCurrentForm());
  }
}


function syncIBMFSuitExpiry(){
  var currentFormId = kony.application.getCurrentForm();
  if (currentFormId.id == "frmIBMFSuitExpiry") {
    if(gblIsExpiry){
      frmIBMFSuitExpiry.lblMessage.text = kony.i18n.getLocalizedString("MF_lbl_MFSExpired");
    }else{
      frmIBMFSuitExpiry.lblMessage.text = kony.i18n.getLocalizedString("MF_lbl_NearByExpired");
    }
    var msg = frmIBMFSuitExpiry.lblDateOfExpiry.text;
    var data = msg.split(": ");
    frmIBMFSuitExpiry.lblDateOfExpiry.text = kony.i18n.getLocalizedString("MF_lbl_ExpiredDate") + ": "+data[1];
    setFocusOnBtnAccountSum(kony.application.getCurrentForm());
  }
}

function syncIBMFSuitTnC(){
  var currentFormId = kony.application.getCurrentForm();
  if (currentFormId.id == "frmIBMFSuitTnC") {
    loadMFSuitabilityTermsNConditionsIB();
    setFocusOnBtnAccountSum(kony.application.getCurrentForm());
  }
}

function syncIBMFSuitTest(){
  var currentFormId = kony.application.getCurrentForm();
  if (currentFormId.id == "frmIBMFSuitTest") {
    preShowfrmIBMFSuitTest();
    setMFSuitQuestionAndAnswerDetailIB();
    setFocusOnBtnAccountSum(kony.application.getCurrentForm());
    if(gblMFSuitQuestNo < maxNoOfQuestion){
      frmIBMFSuitTest.btnNext.text = kony.i18n.getLocalizedString("Next");
    }else{
      frmIBMFSuitTest.btnNext.text = kony.i18n.getLocalizedString("MF_ST_Btn_Sumit");
    }
  }
}

function syncIBExecutedTransaction(){
  dismissLoadingScreen();
  if (kony.i18n.getCurrentLocale() == "en_US") {
    frmIBExecutedTransaction.lblBankValue.text = gblTransferTxnENCallBack;
  }else{
    frmIBExecutedTransaction.lblBankValue.text = gblTransferTxnTHCallBack;
  } 
  if(gblSelTransferMode != 1){
    if(frmIBExecutedTransaction.lblXferToAccTyp.isVisible){
      frmIBExecutedTransaction.lblXferToAccTyp.text = getPhraseToPromptpayTitle();
    }
  }
}

function syncIBTransferNowCompletion(){
 try{
   kony.print("@@@ In syncIBTransferNowCompletion() @@@");
   var currentFormId = kony.application.getCurrentForm();
   if (currentFormId.id == "frmIBTransferNowCompletion") {
     if(gblSelTransferMode != 1){
       if(frmIBTransferNowCompletion.lblXferToName.isVisible){
         frmIBTransferNowCompletion.lblXferToName.text = getPhraseToPromptpayTitle();
       }
     }
     if(frmIBTransferNowCompletion.segSplitDet.isVisible){
       var splitSegData = frmIBTransferNowCompletion.segSplitDet.data;
       if(splitSegData != null && splitSegData != "" && splitSegData != undefined){
         frmIBTransferNowCompletion.segSplitDet.removeAll();
         for(var i = 0; i < splitSegData.length; i++ ){
           splitSegData[i].lblSplitTRN = kony.i18n.getLocalizedString(splitSegData[i].lblSplitTRNTxt);
           splitSegData[i].lblSplitAmt = kony.i18n.getLocalizedString("amount");
           splitSegData[i].lblSplitFee = kony.i18n.getLocalizedString("keyFee");
         }
         frmIBTransferNowCompletion.segSplitDet.setData(splitSegData);
       }
     }
   }
 }catch(e){
   kony.print("@@@ In syncIBTransferNowCompletion() Exception:::"+e);
 }
}

function syncIBTransferNowConfirmation(){
  var currentFormId = kony.application.getCurrentForm();
  if (currentFormId.id == "frmIBTransferNowConfirmation") {
		currentFormId.segMenuOptions.removeAll();
		var splitAmtLen = gblsplitAmt.length;
		var tempData = [];
		var totalFee = 0;
		var transferNoToDiaplay = "";
		for (var i = 0; i < splitAmtLen; i++) {
			if(gblsplitFee[i] != 0)
			 totalFee += gblsplitFee[i];
			var pad = "";
			if (i < 9)
				pad = "0" + (i + 1);
			else
				pad = (i + 1);
				
			var refNoToDisplay = "";
			if(gblPaynow){
				transferNoToDiaplay = "NT"+ gblTransferRefNo +pad 
			}else{
				transferNoToDiaplay = "ST"+ gblTransferRefNo +pad 
				frmIBTransferNowConfirmation.lblSplitXferDtVal.text =returnDateForFT()
			}
			temp = {
				lblSplitAmt: kony.i18n.getLocalizedString("amount"),
				lblSplitAmtVal: commaFormatted(gblsplitAmt[i]) + " " +kony.i18n.getLocalizedString("currencyThaiBaht"),
				lblSplitFee: kony.i18n.getLocalizedString("keyFee"),
				lblSplitFeeVal: commaFormatted(gblsplitFee[i]+"") +" " + kony.i18n.getLocalizedString("currencyThaiBaht"),
				lblSplitTRN: gblPaynow ? kony.i18n.getLocalizedString("keyTransactionRefNo") : kony.i18n.getLocalizedString("keySchedule"),
				lblSplitTRNVal: transferNoToDiaplay
			}
			tempData.push(temp);
		}
		frmIBTransferNowConfirmation.lblSplitTotFeeVal.text = commaFormatted(totalFee+"") + " " +  kony.i18n.getLocalizedString("currencyThaiBaht");
		frmIBTransferNowConfirmation.segSplitDet.removeAll();
		frmIBTransferNowConfirmation.segSplitDet.data = tempData;
        frmIBTransferNowConfirmation.lblSplitToBankNameVal.text = getBankNameCurrentLocaleTransfer(gblisTMB);
		if(gblPaynow){
		
			if(gblTransSMART == 1) {
				frmIBTransferNowConfirmation.lblSplitXferDt.text = kony.i18n.getLocalizedString("keyOrderDAteFTIB");
				frmIBTransferNowCompletion.lblSplitXferDt.text = kony.i18n.getLocalizedString("keyOrderDAteFTIB");
			} else {
			frmIBTransferNowConfirmation.lblSplitXferDt.text = kony.i18n.getLocalizedString("keyXferTD");
			frmIBTransferNowCompletion.lblSplitXferDt.text = kony.i18n.getLocalizedString("keyXferTD");
			}
		} else {
			frmIBTransferNowConfirmation.hbxSplitMN.setVisibility(true);
			frmIBTransferNowConfirmation.lblSplitXferDt.text = kony.i18n.getLocalizedString("keyOrderDAteFTIB");
			frmIBTransferNowCompletion.lblSplitXferDt.text = kony.i18n.getLocalizedString("keyOrderDAteFTIB");
		}
		if(kony.i18n.getCurrentLocale() == "th_TH"){
		  frmIBTransferNowConfirmation.btnMenuTransfer.skin = 'btnIBMenuTransferFocusThai';
		  frmIBTransferNowConfirmation.btnMenuMyAccountSummary.skin = 'btnIBMenuMyAccountSummaryThai';
		}else{
		  frmIBTransferNowConfirmation.btnMenuTransfer.skin = 'btnIBMenuTransferFocus';
		  frmIBTransferNowConfirmation.btnMenuMyAccountSummary.skin = 'btnIBMenuMyAccountSummary';
		}
		if(gblSelTransferMode != 1){
		  if(frmIBTransferNowConfirmation.lblXferToName.isVisible){
			frmIBTransferNowConfirmation.lblXferToName.text = getPhraseToPromptpayTitle();
		  }
		}
    }
}


function syncIBTransferLP(){
  var currentFormId = kony.application.getCurrentForm();
  if (currentFormId.id == "frmIBTranferLP") {
    //setSelTabBankAccountOrMobile();
    currentFormId.segMenuOptions.removeAll();
    if(currentFormId.hbxP2PFee.isVisible){
      var fees = currentFormId.lblP2PFeeVal.text.trim();
      var fee = fees.split(" ");
      if(fee.length > 2){
        currentFormId.lblP2PFeeVal.text = kony.i18n.getLocalizedString("TREnter_Fee") +
          " " + fee[1] + " " +kony.i18n.getLocalizedString("currencyThaiBaht");
      }else{
        currentFormId.lblP2PFeeVal.text = kony.i18n.getLocalizedString("keyFreeTransfer");
      }
    }

    if(kony.i18n.getCurrentLocale() == "th_TH"){
      currentFormId.btnMenuMyAccountSummary.skin = 'btnIBMenuMyAccountSummaryThai'
      currentFormId.btnMenuTransfer.skin="btnIBMenuTransferFocusThai";
    }else{
      currentFormId.btnMenuMyAccountSummary.skin = 'btnIBMenuMyAccountSummary'
      currentFormId.btnMenuTransfer.skin="btnIBMenuTransferFocus";
    }

    if(gblSelTransferMode != 1){
      currentFormId.txtXferMobileNumber.placeholder = kony.i18n.getLocalizedString("IB_P2PToSection");
      checkDisplayNotifyP2PMOorCI();
      if(isNotBlank(gblisTMB)){
        currentFormId.lblBankName.text = getBankNameIB(gblisTMB);
      }

      if(!currentFormId.hbxNoteToRecipent.isVisible){
        currentFormId.lineThree.setVisibility(false);
      }else{
        currentFormId.lineThree.setVisibility(true);
      }
    }
    currentFormId.txtTransLndSmsNEmail.placeholder =  kony.i18n.getLocalizedString("TransferMobileNo");
    currentFormId.tbxEmail.placeholder = kony.i18n.getLocalizedString("transferEmailID");
    if(gblSelTransferMode == 1){
      currentFormId.lblSelectTransfer.text = kony.i18n.getLocalizedString("MIB_P2PToAcctLabel");          	 
    }else if(gblSelTransferMode == 2){
      currentFormId.lblSelectTransfer.text = kony.i18n.getLocalizedString("MIB_P2PToMobileLabel");
      currentFormId.txtXferMobileNumber.placeholder = kony.i18n.getLocalizedString("MIB_P2PTREnter_MobNo");
    }else if(gblSelTransferMode == 3){
      currentFormId.lblSelectTransfer.text = kony.i18n.getLocalizedString("MIB_P2PToIDLabel");
      currentFormId.txtXferMobileNumber.placeholder = kony.i18n.getLocalizedString("MIB_P2PTREnter_CI");
    }else if(gblSelTransferMode == 4){
      currentFormId.lblSelectTransfer.text = kony.i18n.getLocalizedString("MIB_P2PToTax");
      currentFormId.txtXferMobileNumber.placeholder = kony.i18n.getLocalizedString("MIB_P2PTREnter_Tax");
    }else if(gblSelTransferMode == 5){
      currentFormId.lblSelectTransfer.text = kony.i18n.getLocalizedString("MIB_P2PToeWallet");
      currentFormId.txtXferMobileNumber.placeholder = kony.i18n.getLocalizedString("MIB_P2PTREnter_eWal");
    }
  }
}

function syncMyBillers()
{
  var currentFormId = kony.application.getCurrentForm().id;
  if (currentFormId == "frmIBMyBillersHome") {
    frmIBMyBillersHome.lblOTPMsg.text = kony.i18n.getLocalizedString('keyotpmsg');
    frmIBMyBillersHome.lblOTPReqMsg.text = kony.i18n.getLocalizedString('keyotpmsgreq');
  }
}

function syncContactUs() {
  frmIBContactUs.label506459299571357.text = kony.i18n.getLocalizedString("keyContactUs");
  frmIBContactUs.lblFeel.text = kony.i18n.getLocalizedString("feebackText");
  var tab = [];
  tab[0] = [1, kony.i18n.getLocalizedString('cnt_topic')];
  tab[1] = [2, kony.i18n.getLocalizedString('Cnt_IB')];
  tab[2] = [3, kony.i18n.getLocalizedString('Cnt_DA')];
  tab[3] = [4, kony.i18n.getLocalizedString('Cnt_LP')];
  tab[4] = [5, kony.i18n.getLocalizedString('Cnt_Compliants')];
  frmIBContactUs.cmbCnt.masterData = tab;
  if(kony.application.getCurrentForm().id=="frmIBContactUs")footerPagesMenu();
}
function syncIBMyAccountSummary()
{
  if (kony.application.getCurrentForm() .id == "frmIBAccntSummary") {
    hbox5036590321206468.lblRemainFree.containerWeight=65;
    if (kony.i18n.getCurrentLocale() == "th_TH") {
      hbox5036590321206468.lblRemainFree.containerWeight=20;
    }
  }
}
function syncIBBeepAndBill()
{
  if (kony.application.getCurrentForm().id == "frmIBBeepAndBillApplyCW") {
    if (kony.i18n.getCurrentLocale() == "en_US") {
      frmIBBeepAndBillApplyCW.lblAddBiller.containerWeight = 19;
      frmIBBeepAndBillApplyCW.lblAddBillerCompCode.containerWeight = 54;
    } else {
      frmIBBeepAndBillApplyCW.lblAddBiller.containerWeight = 26;
      frmIBBeepAndBillApplyCW.lblAddBillerCompCode.containerWeight = 46;
    }
  }
  if (kony.application.getCurrentForm().id == "frmIBBeepAndBillApply") {
    if (kony.i18n.getCurrentLocale() == "en_US") {
      frmIBBeepAndBillApply.lblAddBiller.containerWeight = 19;
      frmIBBeepAndBillApply.lblAddBillerCompCode.containerWeight = 54;
    } else {
      frmIBBeepAndBillApply.lblAddBiller.containerWeight = 23;
      frmIBBeepAndBillApply.lblAddBillerCompCode.containerWeight = 47;
    }
  }
  if (kony.application.getCurrentForm().id == "frmIBBeepAndBillConfAndComp") {
    if (gblTokenSwitchFlagBB == true)
      frmIBBeepAndBillConfAndComp.lblMsgRequset.text=kony.i18n.getLocalizedString("keyPleaseEnterToken");
    else
      frmIBBeepAndBillConfAndComp.lblMsgRequset.text=kony.i18n.getLocalizedString("keyotpmsgreq");
  }
  if (kony.application.getCurrentForm().id == "frmIBBeepAndBillExecuteConfComp") {
    if (gblTokenSwitchFlagBB == true)
      frmIBBeepAndBillExecuteConfComp.lblMsgRequset.text=kony.i18n.getLocalizedString("keyPleaseEnterToken");
    else
      frmIBBeepAndBillExecuteConfComp.lblMsgRequset.text=kony.i18n.getLocalizedString("keyotpmsgreq");
  }
  //if (kony.application.getCurrentForm().id == "frmIBBillPaymentConfirm") {
  //	if (kony.i18n.getCurrentLocale() == "en_US") {
  //        frmIBBillPaymentConfirm.label476047582115284.containerWeight=11;
  //		frmIBBillPaymentConfirm.txtotp.containerWeight=48;
  //    } else {
  //        frmIBBillPaymentConfirm.label476047582115284.containerWeight=22;
  //		frmIBBillPaymentConfirm.txtotp.containerWeight=37;
  //    }
  //}

}

function syncIBBillPayment(){

  if (kony.i18n.getCurrentLocale() == "en_US") {
    frmIBBillPaymentCW.lblBPRef1.text=appendColon(gblRef1LblEN);
    frmIBBillPaymentCW.lblBPRef2.text=appendColon(gblRef2LblEN);
  }else{
    frmIBBillPaymentCW.lblBPRef1.text=appendColon(gblRef1LblTH);
    frmIBBillPaymentCW.lblBPRef2.text=appendColon(gblRef2LblTH);
  }
  if (kony.application.getCurrentForm().id == "frmIBBillPaymentCompletenow") {
    if (frmIBBillPaymentCompletenow.lblBBPaymentValue.isVisible){
      frmIBBillPaymentCompletenow.lblHide.text=kony.i18n.getLocalizedString("Hide");
    }else{
      frmIBBillPaymentCompletenow.lblHide.text=kony.i18n.getLocalizedString("keyUnhide");
    }
  }
  if(gblCompCode!=undefined && gblCompCode!="" && gblEA_BILLER_COMP_CODES.indexOf(gblCompCode) >= 0){

    var frmName=kony.application.getCurrentForm();
    var frmId=kony.application.getCurrentForm().id;
    if(frmId=="frmIBBillPaymentLP" || frmId=="frmIBBillPaymentConfirm" || frmId=="frmIBBillPaymentCompletenow" ){
      if(frmId=="frmIBBillPaymentConfirm" || frmId=="frmIBBillPaymentCompletenow" ){
        showMEACustDetails(frmName);
      }
      if (frmName.hbxAmountDetailsMEA.isVisible){
        frmName.lnkHide.text=kony.i18n.getLocalizedString("Hide")
      }else{
        frmName.lnkHide.text=kony.i18n.getLocalizedString("show")
      }
      if (kony.i18n.getCurrentLocale() == "en_US"){
        if(frmId=="frmIBBillPaymentLP"){
          frmIBBillPaymentLP.lblBPAmount.text  = gblSegBillerData["billerTotalPayAmtEn"]+":";
        }else if(frmId=="frmIBBillPaymentConfirm"){
          frmIBBillPaymentConfirm.lblAmt.text  = gblSegBillerData["billerTotalPayAmtEn"]+":";
        }else if(frmId=="frmIBBillPaymentCompletenow"){
          frmIBBillPaymentCompletenow.label589607987325263.text  = gblSegBillerData["billerTotalPayAmtEn"]+":";
        }
        frmName.lblAmtLabel.text  = gblSegBillerData["billerAmountEn"]+":";
        frmName.lblAmtInterest.text  = gblSegBillerData["billerTotalIntEn"]+":";
        frmName.lblAmtDisconnected.text  = gblSegBillerData["billerDisconnectAmtEn"]+":";
      }else{
        if(frmId=="frmIBBillPaymentLP"){
          frmIBBillPaymentLP.lblBPAmount.text  = gblSegBillerData["billerTotalPayAmtTh"]+":";
        }else if(frmId=="frmIBBillPaymentConfirm"){
          frmIBBillPaymentConfirm.lblAmt.text  = gblSegBillerData["billerTotalPayAmtTh"]+":";
        }else if(frmId=="frmIBBillPaymentCompletenow"){
          frmIBBillPaymentCompletenow.label589607987325263.text  = gblSegBillerData["billerTotalPayAmtTh"]+":";
        }
        frmName.lblAmtLabel.text  = gblSegBillerData["billerAmountTh"]+":";
        frmName.lblAmtInterest.text  = gblSegBillerData["billerTotalIntTh"]+":";
        frmName.lblAmtDisconnected.text  = gblSegBillerData["billerDisconnectAmtTh"]+":";
      }
    }
  }else if (gblCompCode!=undefined && gblCompCode!="" && GLOBAL_ONLINE_PAYMENT_VERSION_MWA.indexOf(gblCompCode) >= 0){ //MWA
    var frmName=kony.application.getCurrentForm();
    var frmId=kony.application.getCurrentForm().id;
    if(frmId=="frmIBBillPaymentLP" || frmId=="frmIBBillPaymentConfirm" || frmId=="frmIBBillPaymentCompletenow" ){
      if(frmId=="frmIBBillPaymentConfirm" || frmId=="frmIBBillPaymentCompletenow" ){
        showMWABillDetails(frmName);
      }
      if (frmName.hbxAmountDetailsMEA.isVisible){
        frmName.lnkHide.text=kony.i18n.getLocalizedString("Hide")
      }else{
        frmName.lnkHide.text=kony.i18n.getLocalizedString("show")
      }
      if (kony.i18n.getCurrentLocale() == "en_US"){
        if(frmId=="frmIBBillPaymentLP"){
          //frmIBBillPaymentLP.lblBPAmount.text  = gblSegBillerData["billerTotalPayAmtEn"]+":";
          frmIBBillPaymentLP.lblMWAAmtLabel.text = gblSegBillerData["billerTotalPayAmtEn"]+":";
        }else if(frmId=="frmIBBillPaymentConfirm"){
          frmIBBillPaymentConfirm.lblAmt.text  = gblSegBillerData["billerTotalPayAmtEn"]+":";
        }else if(frmId=="frmIBBillPaymentCompletenow"){
          frmIBBillPaymentCompletenow.label589607987325263.text  = gblSegBillerData["billerTotalPayAmtEn"]+":";
        }
        frmName.lblAmtLabel.text  = gblSegBillerData["billerTotalPayAmtEn"]+":";
        frmName.lblAmtInterest.text  = gblSegBillerData["billerAmountEn"]+":";
        frmName.lblAmtDisconnected.text  = gblSegBillerData["billerTotalIntEn"]+":";				
      }else{
        if(frmId=="frmIBBillPaymentLP"){
          //frmIBBillPaymentLP.lblBPAmount.text  = gblSegBillerData["billerTotalPayAmtTh"]+":";
          frmIBBillPaymentLP.lblMWAAmtLabel.text = gblSegBillerData["billerTotalPayAmtTh"]+":";
        }else if(frmId=="frmIBBillPaymentConfirm"){
          frmIBBillPaymentConfirm.lblAmt.text  = gblSegBillerData["billerTotalPayAmtTh"]+":";					
        }else if(frmId=="frmIBBillPaymentCompletenow"){
          frmIBBillPaymentCompletenow.label589607987325263.text  = gblSegBillerData["billerTotalPayAmtTh"]+":";
        }
        frmName.lblAmtLabel.text  = gblSegBillerData["billerTotalPayAmtTh"]+":";
        frmName.lblAmtInterest.text  = gblSegBillerData["billerAmountTh"]+":";
        frmName.lblAmtDisconnected.text  = gblSegBillerData["billerTotalIntTh"]+":";
      }
    }
  }


}


function syncIBMyActivities()
{
  if (kony.application.getCurrentForm().id == "frmIBMyActivities") {
    if(kony.i18n.getCurrentLocale()=="th_TH"){
      var isIE8 = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
      if (isIE8) {
        frmIBMyActivities.vbox588717954792719.containerWeight=14;
      }
    }
  }
}

function syncIBFATCALocale() {
  if (kony.application.getCurrentForm().id == "frmIBFATCATandC") {
    changeFATCATnCLocaleIB();
  }

  if (kony.application.getCurrentForm().id == "frmIBFATCAQuestionnaire") {
    changeFATCAFormLocaleIB();
    changeFATCAQuestionLocaleIB();
  }
}

function syncIBEditBP(){
  if (kony.application.getCurrentForm().id == "frmIBBillPaymentView"){
    if (gblOnLoadRepeatAsIB == "Yearly") {
      frmIBBillPaymentView.lblBPRepeatAsVal.text = kony.i18n.getLocalizedString("keyYearly");;
    } 
    else if (gblOnLoadRepeatAsIB == "Monthly") {
      frmIBBillPaymentView.lblBPRepeatAsVal.text = kony.i18n.getLocalizedString("keyMonthly");;
    } 
    else if (gblOnLoadRepeatAsIB == "Weekly") {
      frmIBBillPaymentView.lblBPRepeatAsVal.text = kony.i18n.getLocalizedString("keyWeekly");;
    } 
    else if(gblOnLoadRepeatAsIB == "Daily") {
      frmIBBillPaymentView.lblBPRepeatAsVal.text = kony.i18n.getLocalizedString("Transfer_Daily");;
    }
    if(gblEndingFreqOnLoadIB =="OnDate"){
      frmIBBillPaymentView.lblBPExcuteValTimes.text = kony.i18n.getLocalizedString("keyTimesIB");
    }
    else if(gblEndingFreqOnLoadIB == "After"){

      frmIBBillPaymentView.lblBPExcuteValTimes.text = kony.i18n.getLocalizedString("keyTimesIB");
    }
    else if(gblEndingFreqOnLoadIB == "Never"){
      frmIBBillPaymentView.lblBPExcuteValTimes.text = "-";
    }
    else if(gblOnLoadRepeatAsIB=="Once"){
      frmIBBillPaymentView.lblBPExcuteValTimes.text =kony.i18n.getLocalizedString("keyTimesMB");
    }
  }

  if (kony.application.getCurrentForm().id == "frmIBEditFutureBillPaymentPrecnf"){
    if (gblOnLoadRepeatAsIB == "Yearly") {
      frmIBEditFutureBillPaymentPrecnf.lblBPRepeatAsVal.text = kony.i18n.getLocalizedString("keyYearly");;
    } 
    else if (gblOnLoadRepeatAsIB == "Monthly") {
      frmIBEditFutureBillPaymentPrecnf.lblBPRepeatAsVal.text = kony.i18n.getLocalizedString("keyMonthly");;
    } 
    else if (gblOnLoadRepeatAsIB == "Weekly") {
      frmIBEditFutureBillPaymentPrecnf.lblBPRepeatAsVal.text = kony.i18n.getLocalizedString("keyWeekly");;
    } 
    else if(gblOnLoadRepeatAsIB == "Daily") {
      frmIBEditFutureBillPaymentPrecnf.lblBPRepeatAsVal.text = kony.i18n.getLocalizedString("Transfer_Daily");;
    }
    if(gblEndingFreqOnLoadIB =="OnDate"){
      frmIBEditFutureBillPaymentPrecnf.lblBPExcuteValTimes.text = kony.i18n.getLocalizedString("keyTimesIB");
    }
    else if(gblEndingFreqOnLoadIB == "After"){
      frmIBEditFutureBillPaymentPrecnf.lblBPExcuteValTimes.text = kony.i18n.getLocalizedString("keyTimesIB");
    }
    else if(gblEndingFreqOnLoadIB == "Never"){
      frmIBEditFutureBillPaymentPrecnf.lblBPExcuteValTimes.text = "-";
    }
    else if(gblOnLoadRepeatAsIB=="Once"){
      frmIBEditFutureBillPaymentPrecnf.lblBPExcuteValTimes.text = kony.i18n.getLocalizedString("keyTimesMB");
    }
  }
}

function syncIBSoGooODLocale() {
  if (kony.application.getCurrentForm().id == "frmIBSoGooodTnC") {
    frmIBSoGooodTnCPreShow();         
  }else if (kony.application.getCurrentForm().id == "frmIBSoGooodProdBrief") {
    frmIBSoGooodProdBriefPreShow();
  }else if (kony.application.getCurrentForm().id == "frmIBApplySoGooODComplete") {
    frmIBApplySoGooODComplete_preshow();
  }else if (kony.application.getCurrentForm().id == "frmIBSoGooodConf") {
    frmMBSoGooodConfPreShowIB();
  }
  else if (kony.application.getCurrentForm().id == "frmIBApplySoGooODPlanList") {
    applySoGooODLanding_preShow();
  }
}

function syncIBMutualFund(){
  if (kony.application.getCurrentForm().id == "frmIBMutualFundsSummary") {
    frmIBMutualFundsSummaryPreShow();
  }else if (kony.application.getCurrentForm().id == "frmIBMFAcctFullStatement") {
    frmIBMFAcctFullStatementPreShow();
  }else if (kony.application.getCurrentForm().id == "frmIBMFAcctOrderToBProceed") {
    frmIBMFAcctOrderToBProceedPreShow();
  }else if (kony.application.getCurrentForm().id == "frmIBMutualFundsPortfolio"){
    frmIBMutualFundsPortfolioPreShow();
  }
}

function syncIBBankAssurance(){
  if (kony.application.getCurrentForm().id == "frmIBBankAssuranceSummary") {
    frmIBBankAssuranceSummaryPreShow();
  }
}

function syncIBOpenAccountKYC(){
  if(kony.application.getCurrentForm().id == "frmIBOpenAccountOccupationKYC"){
    populateDataKYCOccupationInfo();
  }else if(kony.application.getCurrentForm().id == "frmIBOpenAccountContactKYC"){
    languageToggleForOpenAccountContactKYC();
  }	
}

function syncIBAnyIDRegistrationFlow(){
  if(kony.application.getCurrentForm().id == "frmIBAnyIDRegistration"){
    languageToggleForAnyIDRegistrationIB();
  }else if(kony.application.getCurrentForm().id == "frmIBAnyIDProdFeature"){
    languageToggleForAnyIDProdFeatureIB();
  }
}

function syncIBOpenNewDreamAccConfirmation(){
  var currentFormId = kony.application.getCurrentForm();
  if (currentFormId.id == "frmIBOpenNewDreamAccConfirmation") {
    var locale = kony.i18n.getCurrentLocale();
    if (locale == "th_TH") {
      frmIBOpenNewDreamAccConfirmation.lblDreamOpenActNameVal.text  = gblPartyInqOARes["FullName"];
    }else if (locale == "en_US") {
      frmIBOpenNewDreamAccConfirmation.lblDreamOpenActNameVal.text  = gblPartyInqOARes["PrefName"];
    }
  }
}

function syncIBOpenNewDreamAccComplete(){
  var currentFormId = kony.application.getCurrentForm();
  if (currentFormId.id == "frmIBOpenNewDreamAccComplete") {
    var locale = kony.i18n.getCurrentLocale();
    if (locale == "th_TH") {
      frmIBOpenNewDreamAccComplete.lblDreamOpenAcctNamComVal.text  = gblPartyInqOARes["FullName"];
    }else if (locale == "en_US") {
      frmIBOpenNewDreamAccComplete.lblDreamOpenAcctNamComVal.text  = gblPartyInqOARes["PrefName"];
    }
  }
}

function syncIBEditMyAccountsFlow() {
  var currentFormId = kony.application.getCurrentForm();
  var previousForm = kony.application.getPreviousForm();
  if (currentFormId.id == "frmIBEditMyAccounts") {
    frmIBEditMyAccounts.lblViewAccntNum.text = kony.i18n.getLocalizedString("keyAccountNoIB");
    frmIBEditMyAccounts.lblViewAccntName.text = kony.i18n.getLocalizedString("AccountName");
    if(frmIBMyAccnts.segTMBAccntsList.selectedItems != null && undefined != frmIBMyAccnts.segTMBAccntsList.selectedItems[0].productID && gblAccountTable["SAVING_CARE_PRODUCT_CODES"].indexOf(frmIBMyAccnts.segTMBAccntsList.selectedItems[0].productID) >= 0){
      changeMasterdatainRelationIBEditMyAccounts();
      frmIBEditMyAccounts.lblViewAccntNum.text = kony.i18n.getLocalizedString("keyAccountNoIB");
      frmIBEditMyAccounts.lblViewAccntName.text = kony.i18n.getLocalizedString("AccountName");
      if(gblBeneficiaryData != null && gblBeneficiaryData.length > 0){
        handleEditSavingCareIBMyAccount(true);
      }else{
        handleEditSavingCareIBMyAccount(false);
      }
    }
  }
  if(currentFormId.id == "frmIBEditMyAccountsConfirmComplete") {
    if(kony.i18n.getCurrentLocale() == "en_US") {
      frmIBEditMyAccountsConfirmComplete.lblProductName.text = frmIBMyAccnts.segTMBAccntsList.selectedItems[0].hiddenProductNameEng;
    } else {
      frmIBEditMyAccountsConfirmComplete.lblProductName.text = frmIBMyAccnts.segTMBAccntsList.selectedItems[0].hiddenProductNameThai;
    }
    if(frmIBEditMyAccounts.btnSpecified.skin == "btnIBTab4LeftFocus"){
      frmIBEditMyAccountsConfirmComplete.lblBeneficiarySpecify.text = appendColon(kony.i18n.getLocalizedString("keyBeneficiaries")) + " " + kony.i18n.getLocalizedString("keyBeneficiariesSpecify");
      frmIBEditMyAccountsConfirmComplete.lblSegBeneficiaryName.text = kony.i18n.getLocalizedString("keyBeneficiaryName");
      frmIBEditMyAccountsConfirmComplete.lblSegBeneficiaryRelation.text = kony.i18n.getLocalizedString("keyBeneficiaryRelationship");
      frmIBEditMyAccountsConfirmComplete.lblSegBeneficiaryBenefit.text = kony.i18n.getLocalizedString("keyBeneficiaryBenefit");
    } else {
      frmIBEditMyAccountsConfirmComplete.lblBeneficiarySpecify.text=appendColon(kony.i18n.getLocalizedString("keyBeneficiaries"))+" "+kony.i18n.getLocalizedString("keyBeneficiariesNotSpecify");
    }
    setEditBenificiariesConfirmCompleteSeg();
  }
}

function syncIBMFSelRedeemFund(){

  var locale = kony.i18n.getCurrentLocale();
  //if (kony.application.getCurrentForm().id == "frmIBMFSelRedeemFund") {{ //MKI,MIB-11147
    /* Menu Part*/
    frmIBMFSelRedeemFund.LinkPortfolio.text = kony.i18n.getLocalizedString("MU_ACC_InvestVal");
    frmIBMFSelRedeemFund.LinkPurchase.text = kony.i18n.getLocalizedString("MU_Purchase");
    frmIBMFSelRedeemFund.LinkRedeem.text = kony.i18n.getLocalizedString("MU_Redeem");
    frmIBMFSelRedeemFund.LinkSutability.text = kony.i18n.getLocalizedString("MU_SB_Suitability");
    //frmIBMFSelRedeemFund.ImageStep.src = (locale == "th_TH") ? "step1th.png" : "step1.png"; //MKI, MIB-8529

    frmIBMFSelRedeemFund.lblMFHeader.text = kony.i18n.getLocalizedString("MF_RDM_1");

    /* Detail Part */
    frmIBMFSelRedeemFund.lblSelectFund.text = kony.i18n.getLocalizedString("MF_RDM_4");
    frmIBMFSelRedeemFund.lblRedeemUnits.text = kony.i18n.getLocalizedString("MF_RDM_16");
    frmIBMFSelRedeemFund.lblBalance.text = kony.i18n.getLocalizedString("MF_RDM_14");
    frmIBMFSelRedeemFund.lblSelectAmount.text = kony.i18n.getLocalizedString("MF_RDM_18");
    frmIBMFSelRedeemFund.btnAll.text = kony.i18n.getLocalizedString("MF_RDM_20");
    frmIBMFSelRedeemFund.btnBaht.text = kony.i18n.getLocalizedString("MF_RDM_21");
    frmIBMFSelRedeemFund.btnUnit.text = kony.i18n.getLocalizedString("MF_RDM_19");
    frmIBMFSelRedeemFund.lblTranOrdDate.text = kony.i18n.getLocalizedString("MF_RDM_25");
    frmIBMFSelRedeemFund.lblTranOrdDateVal.text = formatDateMF(GLOBAL_TODAY_DATE);
    frmIBMFSelRedeemFund.btnNext.text = kony.i18n.getLocalizedString("MF_RDM_6");

    /* Select Fund Part */
    frmIBMFSelRedeemFund.lblSelectFundOpt.text = kony.i18n.getLocalizedString("MF_RDM_8");
  //}{ //MKI,MIB-11147
}


function syncIBMFTnC(){

  var locale = kony.i18n.getCurrentLocale();

  if (kony.application.getCurrentForm().id == "frmIBMFTnC") {
    preShowFrmIBMFTnC();
  }
}
function syncIBMFConfirm(){

  var locale = kony.i18n.getCurrentLocale();

  if(gblMFEventFlag == MF_EVENT_PURCHASE){
    frmIBMFConfirm.lblMFHeader.text = kony.i18n.getLocalizedString("MF_Purchase_01");
  } else if(gblMFEventFlag == MF_EVENT_REDEEM){
    frmIBMFConfirm.lblMFHeader.text = kony.i18n.getLocalizedString("MF_RDM_1");
  }

  frmIBMFConfirm.LinkPortfolio.text = kony.i18n.getLocalizedString("MU_ACC_InvestVal");
  frmIBMFConfirm.LinkPurchase.text = kony.i18n.getLocalizedString("MU_Purchase");
  frmIBMFConfirm.LinkRedeem.text = kony.i18n.getLocalizedString("MU_Redeem");
  frmIBMFConfirm.LinkSutability.text = kony.i18n.getLocalizedString("MU_SB_Suitability");

  frmIBMFConfirm.lblSelectAcct.text = kony.i18n.getLocalizedString("MF_PRC_lbl_Payment");
  frmIBMFConfirm.lblTranOrdDate.text = kony.i18n.getLocalizedString("MF_LBL_TranDate");
  frmIBMFConfirm.lblEffDate.text = kony.i18n.getLocalizedString("MF_LBL_EffectivDate");
  frmIBMFConfirm.lblCutOffTime.text = kony.i18n.getLocalizedString("MF_RD_lbl_cutoff");

  frmIBMFConfirm.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
  frmIBMFConfirm.btnConfirm.text = kony.i18n.getLocalizedString("keyConfirm");

  if(gblMFDetailsResulttable != undefined && gblMFDetailsResulttable != null && Object.keys(gblMFDetailsResulttable).length > 0){
    frmIBMFConfirm.lblfundnamefullval.text = locale == "th_TH" ? gblMFDetailsResulttable["FundNameTH"] : gblMFDetailsResulttable["FundNameEN"];
  } else {
    frmIBMFConfirm.lblfundnamefullval.text = "";
  }


  frmIBMFConfirm.lblTranOrdDateVal.text = formatDateMF(GLOBAL_TODAY_DATE);

  if(gblFundRulesData != undefined && gblFundRulesData != null && Object.keys(gblFundRulesData).length > 0) {
    tempDate = gblFundRulesData["effectiveDate"].substring(0,2)
    tempMonth = gblFundRulesData["effectiveDate"].substring(3,5);
    tempYear = gblFundRulesData["effectiveDate"].substring(6,10);
    effDate = tempMonth + "/" + tempDate + "/" + tempYear;
    frmIBMFConfirm.lblEffDateVal.text = formatDateMF(effDate); 
  } else {
    frmIBMFConfirm.lblEffDateVal.text = "";
  }

  if(gblMFOrder != null && gblMFOrder["acctNickNameEN"] != undefined && gblMFEventFlag == MF_EVENT_PURCHASE){
    frmIBMFConfirm.lblSelectAcctVal.text = (locale == "th_TH") ? gblMFOrder["acctNickNameTH"] : gblMFOrder["acctNickNameEN"];

    frmIBMFConfirm.lblAmount.text = kony.i18n.getLocalizedString("MF_PRC_lbl_Amount");
  } else if(gblMFEventFlag == MF_EVENT_REDEEM){
    frmIBMFConfirm.lblAmount.text = kony.i18n.getLocalizedString("MF_RD_lbl_amount");
  }
  var amountUnit = "";
  if(gblMFOrder != null && gblMFOrder["redeemUnit"] != undefined ) {
    if(gblMFOrder["redeemUnit"] == ORD_UNIT_ALL || gblMFOrder["redeemUnit"] == ORD_UNIT_UNIT) {
      amountUnit = kony.i18n.getLocalizedString("MF_RD_lbl_unit");
    } else {
      amountUnit = kony.i18n.getLocalizedString("currencyThaiBaht");
    }
  } else {
    amountUnit = kony.i18n.getLocalizedString("currencyThaiBaht");
  }
  frmIBMFConfirm.lblAmountVal.text = commaFormatted(gblMFOrder["amount"]) + " " + amountUnit;
  frmIBMFConfirm.LabelSettleDate.text = kony.i18n.getLocalizedString("MF_RDM_44");
}

function syncIBTransferCustomWidgetLP(){
  var currentFormId = kony.application.getCurrentForm();
  if(currentFormId.id == "frmIBTransferCustomWidgetLP") {
    if(gblSelTransferMode == 1){
      frmIBTransferCustomWidgetLP.lblSelectTransfer.text = kony.i18n.getLocalizedString("MIB_P2PToAcctLabel");
    }else if(gblSelTransferMode == 2){
      frmIBTransferCustomWidgetLP.lblSelectTransfer.text = kony.i18n.getLocalizedString("MIB_P2PToMobileLabel");
    }else if(gblSelTransferMode == 3){
      frmIBTransferCustomWidgetLP.lblSelectTransfer.text = kony.i18n.getLocalizedString("MIB_P2PToIDLabel");
    }else if(gblSelTransferMode == 4){
      frmIBTransferCustomWidgetLP.lblSelectTransfer.text = kony.i18n.getLocalizedString("MIB_P2PToTax");
    }else if(gblSelTransferMode == 5){
      frmIBTransferCustomWidgetLP.lblSelectTransfer.text = kony.i18n.getLocalizedString("MIB_P2PToeWallet");
    }
  }
}

function syncIBMFComplete(){

  var locale = kony.i18n.getCurrentLocale();
  frmIBMFConfirm.ImageStep.src = (locale == "th_TH") ? "step4th.png" : "step4.png"; // MKI, MIB-9082/11148
  if(gblMFEventFlag == MF_EVENT_PURCHASE){
    frmIBMFComplete.lblMFHeader.text = kony.i18n.getLocalizedString("MF_Purchase_01");
  } else if(gblMFEventFlag == MF_EVENT_REDEEM){
    frmIBMFComplete.lblMFHeader.text = kony.i18n.getLocalizedString("MF_RDM_1");
  }

  frmIBMFComplete.LinkPortfolio.text = kony.i18n.getLocalizedString("MU_ACC_InvestVal");
  frmIBMFComplete.LinkPurchase.text = kony.i18n.getLocalizedString("MU_Purchase");
  frmIBMFComplete.LinkRedeem.text = kony.i18n.getLocalizedString("MU_Redeem");
  frmIBMFComplete.LinkSutability.text = kony.i18n.getLocalizedString("MU_SB_Suitability");


  if(gblMFOrder != null && gblMFOrder["fundNameTH"] != undefined && gblMFEventFlag == MF_EVENT_PURCHASE){
    frmIBMFComplete.lblfunamefullval.text = locale == "th_TH" ? gblMFOrder["fundNameTH"] : gblMFOrder["fundNameEN"];
  } else {
    frmIBMFComplete.lblfunamefullval.text = "";
  }

  frmIBMFComplete.lblTransRef.text = kony.i18n.getLocalizedString("MF_Confrim_LBL_TransactionRefNo"); // 
  frmIBMFComplete.lblTranOrdDate.text = kony.i18n.getLocalizedString("MF_LBL_TranDate"); //
  frmIBMFComplete.lblEffDate.text = kony.i18n.getLocalizedString("MF_LBL_EffectivDate"); //
  frmIBMFComplete.btnXferNext.text = kony.i18n.getLocalizedString("MF_btn_back"); //

  if(gblMFOrder != undefined && gblMFOrder != null && gblMFOrder["orderDate"] != undefined) {
    frmIBMFComplete.lblTranOrdDateVal.text = formatDateMF(gblMFOrder["orderDate"]) + " " + gblMFOrder["orderTime"];
    frmIBMFComplete.lblEffDateVal.text = formatDateMF(gblMFOrder["effectiveDate"]);
  }
  if(gblMFOrder != null && gblMFOrder["acctNickNameEN"] != undefined && gblMFEventFlag == MF_EVENT_PURCHASE){
    frmIBMFComplete.lblselectAccnt.text = kony.i18n.getLocalizedString("MF_PRC_lbl_Payment");
    frmIBMFComplete.lblselectAccntVal.text = (locale == "th_TH") ? gblMFOrder["acctNickNameTH"] : gblMFOrder["acctNickNameEN"];

    frmIBMFComplete.lblAmount.text = kony.i18n.getLocalizedString("MF_PRC_lbl_Amount");
  } else if(gblMFEventFlag == MF_EVENT_REDEEM){
    frmIBMFComplete.lblAmount.text = kony.i18n.getLocalizedString("MF_RD_lbl_amount");
  }


  var amountUnit = "";
  if(gblMFOrder != null && gblMFOrder["redeemUnit"] != undefined ) {
    if(gblMFOrder["redeemUnit"] == ORD_UNIT_ALL || gblMFOrder["redeemUnit"] == ORD_UNIT_UNIT) {
      amountUnit = kony.i18n.getLocalizedString("MF_RD_lbl_unit");
    } else {
      amountUnit = kony.i18n.getLocalizedString("currencyThaiBaht");
    }
  } else {
    amountUnit = kony.i18n.getLocalizedString("currencyThaiBaht");
  }

  frmIBMFComplete.lblAmountVal.text = commaFormatted(gblMFOrder["amount"]) + " " + amountUnit;

  if(gblMFEventFlag == MF_EVENT_PURCHASE){
    frmIBMFComplete.lblTransStatus.text = kony.i18n.getLocalizedString("MF_TTL_Purchase") + " " + kony.i18n.getLocalizedString("MF_FailedTrans");
  } else if(gblMFEventFlag == MF_EVENT_REDEEM){
    frmIBMFComplete.lblTransStatus.text = kony.i18n.getLocalizedString("MF_RD_lbl_redeem") + " " + kony.i18n.getLocalizedString("MF_FailedTrans");
  }
  frmIBMFComplete.btnTryAgain.text = kony.i18n.getLocalizedString("btnTryAgain");
  frmIBMFComplete.LabelSettleDate.text = kony.i18n.getLocalizedString("MF_RDM_44");
  frmIBMFComplete.lblpaymentmethod.text = kony.i18n.getLocalizedString("RD_lbl_method"); //
}

function syncIBMFSelPurchaseFundsEntry(){
  var locale = kony.i18n.getCurrentLocale();


  frmIBMFSelPurchaseFundsEntry.lblMFHeader.text = kony.i18n.getLocalizedString("MF_Purchase_01");

  frmIBMFSelPurchaseFundsEntry.LinkPortfolio.text = kony.i18n.getLocalizedString("MU_ACC_InvestVal");
  frmIBMFSelPurchaseFundsEntry.LinkPurchase.text = kony.i18n.getLocalizedString("MU_Purchase");
  frmIBMFSelPurchaseFundsEntry.LinkRedeem.text = kony.i18n.getLocalizedString("MU_Redeem");
  frmIBMFSelPurchaseFundsEntry.LinkSutability.text = kony.i18n.getLocalizedString("MU_SB_Suitability");

  frmIBMFSelPurchaseFundsEntry.lblSegSelectFund.text = kony.i18n.getLocalizedString("MF_Purchase_03");

  if(gblMFOrder != null && gblMFOrder["fundNameTH"] != undefined && gblMFEventFlag == MF_EVENT_PURCHASE){
    frmIBMFSelPurchaseFundsEntry.lblfullnamefullval.text = locale == "th_TH" ? gblMFOrder["fundNameTH"] : gblMFOrder["fundNameEN"];
  }
  frmIBMFSelPurchaseFundsEntry.lblSegSelectUnitHolder.text = kony.i18n.getLocalizedString("MF_Purchase_22");
  frmIBMFSelPurchaseFundsEntry.lblunitholder.text = kony.i18n.getLocalizedString("MF_Purchase_26");
  frmIBMFSelPurchaseFundsEntry.lblselectAccntHeader.text = kony.i18n.getLocalizedString("MF_Purchase_32");
  frmIBMFSelPurchaseFundsEntry.lblSegSelectAcct.text = kony.i18n.getLocalizedString("MF_Purchase_28");

  if(gblMFOrder != null && gblMFOrder["acctNickNameTH"] != undefined && gblMFEventFlag == MF_EVENT_PURCHASE){
    frmIBMFSelPurchaseFundsEntry.lblAccountNameVal.text = (locale == "th_TH") ? gblMFOrder["acctNickNameTH"] : gblMFOrder["acctNickNameEN"];	
  }

  frmIBMFSelPurchaseFundsEntry.lblAvailableBalance.text = kony.i18n.getLocalizedString("MF_Purchase_34");
  frmIBMFSelPurchaseFundsEntry.lblAmountname.text = kony.i18n.getLocalizedString("MF_Purchase_36");
  frmIBMFSelPurchaseFundsEntry.lbltransdate.text = kony.i18n.getLocalizedString("MF_Purchase_38");


  //Select Fund 
  frmIBMFSelPurchaseFundsEntry.LabelSelectFundList.text = kony.i18n.getLocalizedString("MF_Purchase_03");
  frmIBMFSelPurchaseFundsEntry.LabelFundHouseVal.text = kony.i18n.getLocalizedString("MF_RDM_20");
  frmIBMFSelPurchaseFundsEntry.LabelFundHouse.text = kony.i18n.getLocalizedString("MF_Purchase_08");
  frmIBMFSelPurchaseFundsEntry.LabelFeatured.text = kony.i18n.getLocalizedString("MF_Purchase_10");
  frmIBMFSelPurchaseFundsEntry.LabelSearch.text = kony.i18n.getLocalizedString("MF_Purchase_11");
  //SegmentFundList

  frmIBMFSelPurchaseFundsEntry.LabelSelectUnitHolderList.text = kony.i18n.getLocalizedString("MF_Purchase_23");

  //frmIBMFSelPurchaseFundsEntry.stepImage.src
  frmIBMFSelPurchaseFundsEntry.LabelErrorNotMatch.text = kony.i18n.getLocalizedString("MF_P_ERR_01001");
  
  updateFundListNameForPurchase();
}

function syncIBMFSelPurchaseFundsLP(){
  var locale = kony.i18n.getCurrentLocale();
  frmIBMFSelPurchaseFundsLP.lblMFHeader.text = kony.i18n.getLocalizedString("MF_Purchase_01");

  frmIBMFSelPurchaseFundsLP.LinkPortfolio.text = kony.i18n.getLocalizedString("MU_ACC_InvestVal");
  frmIBMFSelPurchaseFundsLP.LinkPurchase.text = kony.i18n.getLocalizedString("MU_Purchase");
  frmIBMFSelPurchaseFundsLP.LinkRedeem.text = kony.i18n.getLocalizedString("MU_Redeem");
  frmIBMFSelPurchaseFundsLP.LinkSutability.text = kony.i18n.getLocalizedString("MU_SB_Suitability");

  //frmIBMFSelPurchaseFundsEntry.stepImage.src

  frmIBMFSelPurchaseFundsLP.lblunitholder.text = kony.i18n.getLocalizedString("MF_Purchase_26");

  if(gblMFOrder["fundNameTH"] != undefined && gblMFOrder["fundNameTH"] != null){
    frmIBMFSelPurchaseFundsLP.lblfullnamefullval.text = locale == "th_TH" ? gblMFOrder["fundNameTH"] : gblMFOrder["fundNameEN"];
  }

  frmIBMFSelPurchaseFundsLP.lblselectAccntHeader.text = kony.i18n.getLocalizedString("MF_Purchase_32");
  frmIBMFSelPurchaseFundsLP.lblselacct.text = kony.i18n.getLocalizedString("MF_Purchase_28");
  frmIBMFSelPurchaseFundsLP.lblAmountname.text = kony.i18n.getLocalizedString("MF_Purchase_36");


  frmIBMFSelPurchaseFundsLP.lbltransdate.text = kony.i18n.getLocalizedString("MF_Purchase_38"); 
}