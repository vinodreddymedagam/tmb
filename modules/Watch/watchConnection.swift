

import Foundation
import WatchConnectivity

class PhoneCommunicator : NSObject, WCSessionDelegate {
    
    static var sharedInstance:PhoneCommunicator? = nil;
    var session:WCSession? = nil;
    
    class func getSharedInstance() -> (PhoneCommunicator) {
        if(sharedInstance == nil){
            sharedInstance =  PhoneCommunicator();
        }
        return sharedInstance!;
    }
    
    func activate() {
        session = WCSession.default;
        if(session!.delegate == nil){
            session!.delegate = self;
        }
        session!.activate();
    }
    
    func requestData(message: [String : AnyObject], replyHandler: (([String : Any]) -> Void)?, errorHandler: ((Error) -> Void)?) {
        
        if WCSession.isSupported() {
            print("session is supported on watch");
//            if(session!.isReachable){
                print("session reachable on phone");
                session!.sendMessage(message, replyHandler: replyHandler, errorHandler: errorHandler);
//            }
        }
    }
    
    @available(watchOSApplicationExtension 2.2, *)
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
    }
}

@available(watchOSApplicationExtension 2.2, *)
class PhoneCommunicator2: PhoneCommunicator {
    
    static var sharedInstance2:PhoneCommunicator2? = nil;
    var activationStateCallback:((WCSessionActivationState) -> Void)? = nil;
    
    override class func getSharedInstance() -> (PhoneCommunicator2) {
        if(sharedInstance2 == nil){
            sharedInstance2 =  PhoneCommunicator2();
        }
        return sharedInstance2!;
    }
    
    override func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        if(self.activationStateCallback != nil){
            self.activationStateCallback!(activationState);
        }
    }
    
    func activate(_ activationStateCallback:@escaping ((WCSessionActivationState) -> Void)) {
        self.activationStateCallback = activationStateCallback;
        if(session != nil && session!.activationState == WCSessionActivationState.activated){
            if(self.activationStateCallback != nil){
                self.activationStateCallback!(session!.activationState);
            }
        }
        else{
            super.activate();
        }
    }
}

