//Type your code hereimport Foundation
import WatchKit

var availableFlights:[[String:String]] = [];
var currentFlightInfo:[String:String] = [:];

func onFrmWatchQuickBalanceWillActivate(form:frmWatchQuickBalanceController) {
    form.grpAccounts.setHidden(true);
    form.grpHeader1.setHidden(true);
    form.grpHeader2.setHidden(true);
    form.grpTMBLogo.setHidden(true);
    form.grpLoading.setHidden(false);
/* let fetchResults =  [
    "custAcctRec":[ ["defaultCurrentNickNameEN": "TMB All free",
    "ledgerBal": "109,000.00",
    "AmountDecimal": ".230000","balanceType" : "Balance"],["defaultCurrentNickNameEN": "TMB Credit Card 1235 Bangkok",
    "ledgerBal": "109,000,000.78",
    "AmountDecimal": ".230000","balanceType" :"Avaiable to spend"]
    ]];
 if(!fetchResults.isEmpty){
  
      let accounts = fetchResults["custAcctRec"] as! [[String: String]];
    
      if (accounts.count > 0) {
        print("Setting data to segment");
        form.segAccounts.setNumberOfRows(accounts.count, withRowType: "tmpAccounts");
        for index in 0..<accounts.count {
          let acctsegmentRow = form.segAccounts.rowController(at: index);
          var accountInfo = accounts[index];
          if(acctsegmentRow != nil){
            (acctsegmentRow! as AnyObject).lblAccountNickName!.setText(accountInfo["defaultCurrentNickNameEN"]);
            (acctsegmentRow! as AnyObject).lblAmountNumber!.setText(accountInfo["ledgerBal"]);
            //(acctsegmentRow! as AnyObject).lblAmountDecimal!.setText(accountInfo["AmountDecimal"]);
            (acctsegmentRow! as AnyObject).lblBalanceType!.setText(accountInfo["balanceType"]);
           
          }
        }
      }
	 
	 
	  }*/

    fetchQuickBalance(form: form);
    let duration = 1.2

    
    
        
}

func fetchQuickBalance(form:frmWatchQuickBalanceController) {
    currentFlightInfo = [:];
    
    func onFetchQuickBalanceSuccess(fetchResults:[String : Any]) {
        print("Inside onFetchQuickBalanceSuccess ");
        print("fetch results \(fetchResults)");
        if(!fetchResults.isEmpty){
            let header1 = fetchResults["header1"] as! String;
            let header2 = fetchResults["header2"] as! String;
            form.lblHeaderOne.setText(header1)
            form.lblHeaderTwo.setText(header2)
         var accounts = fetchResults["data"] as! [[String : String]];
         if (accounts.count > 0) {
            form.segAccounts.setNumberOfRows(accounts.count, withRowType: "tmpAccounts");
            for index in 0..<accounts.count {
                let acctsegmentRow = form.segAccounts.rowController(at: index);
                var accountInfo = accounts[index];
                if(acctsegmentRow != nil){
                    (acctsegmentRow! as AnyObject).lblAccountNickName!.setText(accountInfo["lblAccountNickName"]);
                    (acctsegmentRow! as AnyObject).lblAmountNumber!.setText(accountInfo["lblAvailableBalanceValue"]);
                    (acctsegmentRow! as AnyObject).lblBalanceType!.setText(accountInfo["lblAvailBalText"]);
                     (acctsegmentRow! as AnyObject).lblDecimal!.setText(accountInfo["lblAvailableBalanceValueDecimal"]);
                   
                    
                }
                }
            form.grpAccounts.setHidden(false);
            form.grpHeader1.setHidden(true);
            form.grpHeader2.setHidden(true);
            form.grpTMBLogo.setHidden(false);
            form.grpLoading.setHidden(true);
            
         }else{
            form.grpAccounts.setHidden(true);
            form.grpHeader1.setHidden(false);
            form.grpHeader2.setHidden(false);
           form.grpTMBLogo.setHidden(false);
           form.grpLoading.setHidden(true);
            }
                
            

        }else{
            form.grpAccounts.setHidden(true);
            form.grpHeader1.setHidden(false);
            form.grpHeader2.setHidden(false);
            form.grpTMBLogo.setHidden(false);
          form.grpLoading.setHidden(true);
        }
    }
    
    func onfetchQuickBalanceFailure(error:NSError){
        form.grpAccounts.setHidden(true);
        form.grpHeader1.setHidden(false);
        form.grpHeader2.setHidden(false);
        form.grpTMBLogo.setHidden(false);
        form.grpLoading.setHidden(true);
        print("error in fetching data \(error)");
    }
  
    
    if #available(watchOSApplicationExtension 2.2, *) {
        let phoneCommunicator = PhoneCommunicator.getSharedInstance();
        
        phoneCommunicator.activate();
        
        phoneCommunicator.requestData(message: ["requestId": "allImageInfo" as AnyObject], replyHandler: onFetchQuickBalanceSuccess, errorHandler: onfetchQuickBalanceFailure as? ((Error) -> Void));
    } else {
        
    };
}

