function addgenerateChartdsds()
{
    var chartObj = kdv_createChartJSObject();
    
    var chartWidget = new kony.ui.Chart2D3D({
                                            "centerX": "50%",
                                            "centerY": "50%",
                                            "width": "100%",
                                            "height":"100%",
                                            "zIndex": 1,
      										"id": "chartWidget",
                                            "isVisible": true
                                            }, {
											"padding" : [2,2,2,2],
											"margin":[5,5,5,5],
											"hExpand":true,
											"vExpand":true,
                                            "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
                                            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_CENTER,
                                            "containerWeight": 100
                                            },
                                            chartObj);
  	frmMFSummary.flexChart.add(chartWidget);
  	//frmHome.show();
}

//creating chart object with chart properties and chart data...

function kdv_createChartJSObjectsdsd() {
    var chartJSObj = {
        
        "chartProperties":{
            
            "drawEntities":["donutChart"],
            
            "chartHeight":100,
            
            "layerArea":{
                "background":{
                    "color":["0xffffffff"]
                }
            },
            
            "donutChart":{
                
               "exploded":true,
                
                "pieSlice":{
                    "color":["0x3165cbff","0xdc3812ff", "0xff9700ff", "0x11951bff",  "0x990098ff", "0x0099c5ff"]
                },
                
                "border":{
                    "line":{
                        "color":["0xffffffff"]
                    }
                },
                "holeRadius": 70,
                "dataLabels":{
                    "font":{
                        "size":[18],
                        "color" : ["0xffffffff"],
                    }
                }
            }
        },
        
        "chartData":
        {
            "columnNames":{
                "values":["2013"]
            },
            
            "data":{
                "2013":[25,20,14,18,17,13]
            },
            
            "rowNames":{
                "values":["Jan","Feb","Mar","Apr", "May", "Jun"]
            }
        }
        
    };
    return chartJSObj;
}