function frmUploadDocuments_init(){
  frmUploadDocuments.preShow = frmUploadDocuments_preShow_action;
  frmUploadDocuments.postShow = frmUploadDocuments_postShow_action;
  frmUploadDocuments.btnBackUploadDocuments.onClick = frmUploadDocuments_btnBackUploadDocuments_PreCondition;
  frmUploadDocuments.btnCancel.onClick = frmUploadDocuments_btnCancel_PreCondition;
  frmUploadDocuments.btnNext.onClick = frmUploadDocuments_btnNext_Action;
  frmUploadDocuments.onDeviceBack = disableBackButton;
}

function frmUploadDocuments_preShow_action(){
  showLoadingScreen();
  frmUploadDocuments.lblUploadDocuments.text = kony.i18n.getLocalizedString("UploadTitle");
  frmUploadDocuments.btnCancel.text = kony.i18n.getLocalizedString("CAV05_btnCancel");
  frmUploadDocuments.flxStatementInfo.setVisibility(false);
  if(gblIncompleteDoc){
    frmUploadDocuments.btnNext.text = kony.i18n.getLocalizedString("Upload");
  }else{
    frmUploadDocuments.btnNext.text = kony.i18n.getLocalizedString("CAV02_btnNext");
    if(gblChosenTab == 2){
      frmUploadDocuments.flxStatementInfo.setVisibility(true);
    }else{
      frmUploadDocuments.flxStatementInfo.setVisibility(false);
    }
  }
  frmUploadDocuments.lblStatementInfo.text = kony.i18n.getLocalizedString("Remark_SelfEmployed");
  loadSegmentUploadCheckList();
}

function frmUploadDocuments_postShow_action(){
  dismissLoadingScreen();
}
function frmUploadDocuments_btnBackUploadDocuments_PreCondition(){
  popupConfrmExitUpload.btnpopConfDelete.onClick = frmUploadDocuments_btnBackUploadDocuments_Action;
  showConfirmToBackPopup();
}
function frmUploadDocuments_btnBackUploadDocuments_Action(){
  popupConfrmExitUpload.dismiss();
  showLoadingScreen();
  if(gblIncompleteDoc){
    navigateToLoanProductList();
  }else{
  	showfrmLoanApplication();  
  }
}
function frmUploadDocuments_btnCancel_PreCondition(){
  popupConfrmExitUpload.btnpopConfDelete.onClick = frmUploadDocuments_btnCancel_Action;
  showConfirmToBackPopup();
}

function frmUploadDocuments_btnCancel_Action(){
  popupConfrmExitUpload.dismiss();
  showLoadingScreen();
  navigateToLoanProductList();
}
function frmUploadDocuments_btnNext_Action(){
  if(gblIncompleteDoc){
    callSaveIncompleteDocumentService();
  }else{
    showLoanConfirmation();
  }
}

function showUploadDcumentPage(inclopleteDoc){
  if(typeof(inclopleteDoc) == "boolean"){
    gblIncompleteDoc = true;
  }else{
    gblIncompleteDoc = false;
  }
  gblContentLoanUpload = "";
  gblContentListIndex = 0;
  gbluploadObjTmp = {};
  gblUploadCheckList = [];
  showLoadingScreen();
  callGetCheckListService(true);
}

function returnToUploadList(){
  frmUploadDocuments.show();
}

function loadSegmentUploadCheckList (){
  try{
    frmUploadDocuments.uploadList.removeAll();
    var uploadCheckListData = [];
    var completeUpload = true;
    if(gblUploadCheckList.length <= 0){
      completeUpload = false;
    } 
    for(var j = 0; j < gblUploadCheckList.length ; j++){
      var obj = {};
      var uploadCheckListRow = gblUploadCheckList[j];
      var imageLogo = "icon_picture.png";
      var buttonAction = "upload_icon.png";
      var buttonActionVisible = true;
      var setVisibleMark = false;
      var setCounterNumber = false;
      var counter = 0;
      var reasonText = uploadCheckListRow.incompletedDocReasonDesc;
      for(var i in uploadCheckListRow.imageDataSet) {
        counter = uploadCheckListRow.imageDataSet[i].length;
      }
      if(counter <= 0){
        imageLogo = "icon_picture.png";
        setVisibleMark = false;
        completeUpload = false;
      }else{
        setVisibleMark = true;
        buttonActionVisible = false;
        if(counter == 1){
          imageLogo = "icon_picture.png";
        }else{
          setVisibleMark = false;
          buttonActionVisible = false;
          setCounterNumber = true;
          imageLogo = "icon_folder_lighter.png";
        }
      }
      var productName= "";
      if("th_TH" == kony.i18n.getCurrentLocale()){
        productName = uploadCheckListRow.documentDescriptionTH;
      }else{
        productName = uploadCheckListRow.documentDescriptionEN;
      }
      obj = {
        "imgUploadLogo": imageLogo,
        "lblDocumentName":productName,
        "lblSegRowLine":"------",
        "flxMainContainer":"-----",
        "imgUpld":{"src":buttonAction,"isVisible":buttonActionVisible},
        "checkMaskFolder":{"isVisible":setVisibleMark},
        "checkMaskNumber":counter.toFixed(0),
        "checkMaskBox":{"isVisible":setCounterNumber},
        "incompletedDocReasonDesc":{"text":reasonText,"isVisible":gblIncompleteDoc}
      };
      uploadCheckListData.push(obj);
    }
    frmUploadDocuments.uploadList.widgetDataMap = {imgUploadLogo: "imgUploadLogo", lblDocumentName: "lblDocumentName", 
                                                  lblSegRowLine: "lblSegRowLine", imgUpld: "imgUpld",
                                                  checkMaskFolder:"checkMaskFolder",checkMaskNumber:"checkMaskNumber",
                                                  checkMaskBox:"checkMaskBox",flxMainContainer:"flxMainContainer",
                                                  incompletedDocReasonDesc:"incompletedDocReasonDesc"};
    frmUploadDocuments.uploadList.setData(uploadCheckListData);
    frmUploadDocuments.uploadList.onRowClick = showUploadDocumentByType;
	if(completeUpload){
      frmUploadDocuments.btnNext.skin = "btnBlueBGNoRound150pxLoan";
      frmUploadDocuments.btnNext.focusSkin = "btnBlue200GreyBG";
      frmUploadDocuments.btnNext.onClick = frmUploadDocuments_btnNext_Action;
    }else{
      frmUploadDocuments.btnNext.skin = "btnGreyBGNoRoundLoan";
      frmUploadDocuments.btnNext.focusSkin = "btnGreyBGNoRoundLoan";
      frmUploadDocuments.btnNext.onClick = disableBackButton;
    }
  }catch(e){
    kony.print("Exception : "+e);
    frmUploadDocuments.btnNext.skin = "btnGreyBGNoRoundLoan";
    frmUploadDocuments.btnNext.focusSkin = "btnGreyBGNoRoundLoan";
    frmUploadDocuments.btnNext.onClick = disableBackButton;
  }
}
function callGetCheckListService(requireDelete){
  var inputParam = {};
  inputParam["checklistType"] = "CC";  // "Save" -> Submit All Documents
  if(gblIncompleteDoc){
    inputParam["incompleteDocFlag"] = "Y";
  }else{
    inputParam["incompleteDocFlag"] = "N";
  }
  inputParam["caId"] = gblCaId;
  inputParam["appRefNo"] = gblLoanAppRefNo;
  inputParam["categoryCode"] = "BRMS_ECM_DOC_TYPE";
  if(typeof(requireDelete) == "boolean"){
    inputParam["deleteFlag"] = "Y";
  }
  invokeServiceSecureAsync("searchChecklistInfoByCaID", inputParam, callGetCheckListServiceCallback);
}

function callGetCheckListServiceCallback(status, callBackResponse) {
  dismissLoadingScreen();
  if (status == 400) {
    if(callBackResponse.responseCode == "MSG_000"){
      gblUploadCheckList = createCheckList(callBackResponse.customerChecklists);
      frmUploadDocuments.show();
    }else{
      showAlert(replaceAll(kony.i18n.getLocalizedString("Err_LoanApp"),"{errorCode}",callBackResponse.responseCode), kony.i18n.getLocalizedString("info"));
    }
  } else {
    gbluploadObjTmp = {};
    alert(kony.i18n.getLocalizedString("ECGenericError"));
  }
}

function callSaveIncompleteDocumentService(){
  var inputParam = {};
  inputParam["updateFlag"] = "Y";
  inputParam["appRefNo"] = gblCaId;
  invokeServiceSecureAsync("updateIncompleteDocApplication", inputParam, callSaveIncompleteDocumentServiceCallback);
}

function callSaveIncompleteDocumentServiceCallback(status, callBackResponse) {
  dismissLoadingScreen();
  if (status == 400) {
    if(callBackResponse.opstatus != "0"){
      alert(kony.i18n.getLocalizedString("UploadError"));
      return false;
    }else{
      imageFTPService();
    }
  } else {
    gbluploadObjTmp = {};
    alert(kony.i18n.getLocalizedString("UploadError"));
    return false;
  }
}

function imageFTPService(){
  showLoadingScreen();
  var inputParam = {};
  inputParam["actionId"] = "Save";  // "Save" -> Submit All Documents
  inputParam["actionType"] = "upload";
  inputParam["imageType"] = "PDF";
  inputParam["appRefNo"] = gblLoanAppRefNo;
  invokeServiceSecureAsync("ImageFTPService", inputParam, saveAllUploadDocumentsMBCallback);
}
function saveAllUploadDocumentsMBCallback(status, callBackResponse) {
  dismissLoadingScreen();
  if (status == 400) {
      if(gblIncompleteDoc){
        showLoanIncompleteDocComplete();
      }else{
        showLoanComplete();
      }
  } else {
    gbluploadObjTmp = {};
    alert(kony.i18n.getLocalizedString("ECGenericError"));
  }
}
function showLoanIncompleteDocComplete(){
  frmApplicationFormComplete.btnCancel1.onClick  =navigateBackToSubProducts;
  frmApplicationFormComplete.lblTrackingMessage.text = kony.i18n.getLocalizedString("Uplode_Label");
  frmApplicationFormComplete.btnCancel1.text = kony.i18n.getLocalizedString("GotoLoan");
  frmApplicationFormComplete.show();
}
function createCheckList(customerChecklists){
  var dataTmp = [];
  for(var j = 0; j < customerChecklists.length ; j++){
    if(!customerChecklists[j].documentCode.startsWith("AP") && !customerChecklists[j].documentCode.startsWith("NC")  ){
      var obj = {
        "documentCode": customerChecklists[j].documentCode,
        "documentDescriptionTH":customerChecklists[j].extValue1,
        "documentDescriptionEN":customerChecklists[j].extValue1,
        "documentStatus":customerChecklists[j].status,
        "docID":customerChecklists[j].id,
        "refEntryCode":customerChecklists[j].refEntryCode,
        "incompletedDocReasonDesc":customerChecklists[j].incompletedDocReasonDesc,
        "isMandatory":customerChecklists[j].isMandatory,
        "imageDataSet":[]
      };
      dataTmp.push(obj);
    }
  }
  return dataTmp;
}
function showConfirmToBackPopup(){
  popupConfrmExitUpload_init();
  popupConfrmExitUpload.show();
}
function closeConfirmToBackPopup(){
  popupConfrmExitUpload.dismiss();
}

function popupConfrmExitUpload_init(){
  popupConfrmExitUpload.lblheader.text = kony.i18n.getLocalizedString("uploadCancelHeader");
  popupConfrmExitUpload.lblPopupConfText.text = kony.i18n.getLocalizedString("uploadCancelDetail");
  popupConfrmExitUpload.btnPopupConfCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
  popupConfrmExitUpload.btnpopConfDelete.text = kony.i18n.getLocalizedString("keyOK");
  popupConfrmExitUpload.btnPopupConfCancel.onClick = closeConfirmToBackPopup;
}