// Below GLobal Variables which will be used only in this module hence declaring here
gblIsPullToRefreshCall = false;
gblQBCalled = true;
gblQuickBalanceDeviceId = "";




function changeLocalQuickBalanceService(deviceId) {
	gblQBCalled = true;
    dismissLoadingScreen();
    callBackQuickBalance(400, gblQuickBalanceResponse);
}


gotUserStatus=false;
isUserStatusActive=false;
//chkUserStatusCallBack
function chkUserStatusCallBack(status, resulttable) {
    try {
    	//dismissLoadingScreen();
		kony.print("FPRINT USER STATUS IS"+status);
        if (status == 400) {
            if (resulttable["opstatus"] == 8005) {
                if (resulttable["errCode"] == 1002) {
                	kony.print("FPRINT SETTING isUserStatusActive TO FALSE");
               	 	isUserStatusActive=false;
                    kony.store.setItem("isUserStatusActive", false);
					kony.print("FPRINT USER STATUS IS INACTIVE");
                	showFPFlex(false);
					onClickChangeTouchIcon();
					kony.print("FPRINT USER STATUS errMsg="+kony.i18n.getLocalizedString("VQB_MBStatus"));
                } else {
                	kony.print("FPRINT SETTING isUserStatusActive TO TRUE");
                	isUserStatusActive=true;
                    kony.store.setItem("isUserStatusActive", true);
                	kony.print("FPRINT USER STATUS IS ACTIVE");
                    if(gblDeviceInfo["model"]=="iPhone X"){
                        frmMBPreLoginAccessesPin.btnTouchnBack.skin = "btnFaceId";
                        frmMBPreLoginAccessesPin.btnTouchnBack.focusSkin = "btnFaceId";
                    }else{
                        frmMBPreLoginAccessesPin.btnTouchnBack.skin = "btntouchiconstudio5";
                        frmMBPreLoginAccessesPin.btnTouchnBack.focusSkin = "btntouchiconfoc";
                    }
                	showFPFlex(true);
					fpdefault();
					closeQuickBalance();
                }
            }else{
            		kony.print("FPRINT SETTING isUserStatusActive TO TRUE");
            		isUserStatusActive=true;
               		kony.store.setItem("isUserStatusActive", true);
                	kony.print("FPRINT USER STATUS IS ACTIVE ELSE");
                	if(gblDeviceInfo["model"]=="iPhone X"){
                        frmMBPreLoginAccessesPin.btnTouchnBack.skin = "btnFaceId";
                        frmMBPreLoginAccessesPin.btnTouchnBack.focusSkin = "btnFaceId";
                    }else{
                        frmMBPreLoginAccessesPin.btnTouchnBack.skin = "btntouchiconstudio5";
                        frmMBPreLoginAccessesPin.btnTouchnBack.focusSkin = "btntouchiconfoc";
                    }
                	showFPFlex(true);
					fpdefault();
					closeQuickBalance();            	
            } 
            gotUserStatus=true;
            dismissLoadingScreen();
            /*else if (resulttable["opstatus"] == 1011) {
                //showAlertWithCallBack(kony.i18n.getLocalizedString("genErrorWifiOff"), kony.i18n.getLocalizedString("info"), closeQuickBalance);
				kony.print("FPRINT USER STATUS IS INACTIVE");
				kony.print("FPRINT USER STATUS errMsg="+kony.i18n.getLocalizedString("genErrorWifiOff"));
            } else {
                //showAlertWithCallBack(kony.i18n.getLocalizedString("VQB_ServiceDown"), kony.i18n.getLocalizedString("info"), closeQuickBalance);
				kony.print("FPRINT USER STATUS IS INACTIVE");
				kony.print("FPRINT USER STATUS errMsg="+kony.i18n.getLocalizedString("VQB_ServiceDown"));
            } */               
        }
    } catch (e) {
        //showAlertWithCallBack(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"), closeQuickBalance);
        kony.print("FPRINT USER STATUS errMsg="+kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
    }
}



// Quick balance service call back function
function callBackQuickBalance(status, resulttable) {
    try {
    	dismissLoadingScreen();
    	var locale_app=kony.i18n.getCurrentLocale();
        enableFormElements();
        var accountName = "";
        var availBalLabel = "";
        var availBalValue = ""
        var masterData = [];
        if (status == 400) {
            if (resulttable["opstatus"] == "0.0" || resulttable["opstatus"] == 0) {
            	gblQuickBalanceResponse = resulttable;
                if (resulttable["setQuickBalance"] == "true" || resulttable["setQuickBalance"] == true) {
                    var collectionData = resulttable["custAcctRec"];
                    for (var i = 0; i < resulttable["custAcctRec"].length; i++) {
                        if ((resulttable["custAcctRec"][i]["acctNickName"]) == null || (resulttable["custAcctRec"][i]["acctNickName"]) == undefined) {
                            if (locale_app == "th_TH")
                             accountName = resulttable["custAcctRec"][i]["defaultCurrentNickNameTH"];
                            else
                             accountName = resulttable["custAcctRec"][i]["defaultCurrentNickNameEN"];
                        } else {

                            accountName = resulttable["custAcctRec"][i]["acctNickName"];
                        }
                        if ((resulttable["custAcctRec"][i]["accType"]) == "CCA") {
                            availBalLabel = kony.i18n.getLocalizedString("keyMBCreditlimit").replace(":", "");
                            availBalValue = resulttable["custAcctRec"][i]["availableCreditBalDisplay"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                        } else {
                            availBalLabel = kony.i18n.getLocalizedString("Balance").replace(":", "");
                            availBalValue = resulttable["custAcctRec"][i]["availableBalDisplay"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                        }
                        var tempRecord = {
                            "lblAccountNickName": accountName,
                            "lblAvailableBalanceValue": availBalValue,
                            "lblAvailBalText": availBalLabel,
                            "lblSeperator": "."
                        };

                        masterData.push(tempRecord);
                    }
                    frmMBPreLoginAccessesPin.segAccounts.setData(masterData);
                    if (!gblIsPullToRefreshCall) {
                        showQuickBalanceResults();
                    } else {
                        frmMBPreLoginAccessesPin.flexLoading.isVisible = false;
                        frmMBPreLoginAccessesPin.segAccounts.isVisible = true;
                    }
                    gblIsPullToRefreshCall = false;
                } else {
                    showQuickBalanceImage();
                }
            } else if (resulttable["opstatus"] == 8005) {
                frmMBPreLoginAccessesPin.flexLoading.isVisible = false;
                if (resulttable["errCode"] == 1002) {
               		//showFPFlex(false);
					kony.print("FPRINT QUICK SETTING isUserStatusActive TO FALSE");
					isUserStatusActive=false;
					onClickChangeTouchIcon();
                	showAlertWithCallBack(kony.i18n.getLocalizedString("VQB_MBStatus"), kony.i18n.getLocalizedString("info"), closeQuickBalance);
                } else {
                	var errMsg = resulttable["errMsg"];
                	showAlertWithCallBack(errMsg, kony.i18n.getLocalizedString("info"), closeQuickBalance);                
                }
            } else if (resulttable["opstatus"] == 1011) {
                frmMBPreLoginAccessesPin.flexLoading.isVisible = false;
                showAlertWithCallBack(kony.i18n.getLocalizedString("genErrorWifiOff"), kony.i18n.getLocalizedString("info"), closeQuickBalance);
            } else {
                frmMBPreLoginAccessesPin.flexLoading.isVisible = false;
                //showAlertWithCallBack(kony.i18n.getLocalizedString("VQB_ServiceDown"), kony.i18n.getLocalizedString("info"), closeQuickBalance);
            	showAlertDeviceIdTouchMessage(kony.i18n.getLocalizedString("deviceIdEnhancementUnlock"), kony.i18n.getLocalizedString("deviceIDUnlock"));
            }                
        }
    } catch (e) {
        showAlertWithCallBack(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"), closeQuickBalance);
        frmMBPreLoginAccessesPin.flexLoading.isVisible = false;
    }
}



function showQuickBalanceResults() {
    frmMBPreLoginAccessesPin.flexQuickBalTextForAccoiunts.isVisible = true;
    frmMBPreLoginAccessesPin.segAccounts.isVisible = true;
    frmMBPreLoginAccessesPin.flexLoading.isVisible = false;
	gbl3dTouchAction = "";
}

function disableFormElements() {
	frmMBPreLoginAccessesPin.flexQuickBalanceCancel.onTouchEnd = doNothing;
	frmMBPreLoginAccessesPin.btnMenu.onClick = doNothing;
	//frmMBPreLoginAccessesPin.flexQuickBalButton.onTouchEnd = doNothing;
	var gestureConfig = {
        fingers: 1,
        swipeDirection: 4
    };
   // frmMBPreLoginAccessesPin.flexQuickBalanceCancel.addGestureRecognizer(2, gestureConfig, doNothing);
  //  frmMBPreLoginAccessesPin.btnQuickBalanceCancel.setEnabled(false);
  //  frmMBPreLoginAccessesPin.btnMenu.setEnabled(false);
}



function enableFormElements() {
	//frmMBPreLoginAccessesPin.flexQuickBalButton.onTouchEnd = showQuickBalance;
	frmMBPreLoginAccessesPin.flexQuickBalanceCancel.onTouchEnd = closeQuickBalance;
	frmMBPreLoginAccessesPin.btnMenu.onClick = handleMenuBtn;
	var gestureConfig = {
        fingers: 1,
        swipeDirection: 4
    };
   // frmMBPreLoginAccessesPin.flexQuickBalanceCancel.addGestureRecognizer(2, gestureConfig, closeQuickBalance);
   // frmMBPreLoginAccessesPin.btnQuickBalanceCancel.setEnabled(true);
   // frmMBPreLoginAccessesPin.btnMenu.setEnabled(true);
}

function invokeQuickBalanceService(deviceId) {
    gblQBCalled = true;
    var inputParam = {};
    inputParam["quickBalanceFlag"] = "true";
    dismissLoadingScreen();
    if(deviceId != ""){
      gblQuickBalanceDeviceId = deviceId;
    }
    frmMBPreLoginAccessesPin.flexLoading.isVisible = true;
    frmMBPreLoginAccessesPin.segAccounts.isVisible = false;
   // var enCryptDeviceId = encryptData(gblQuickBalanceDeviceId);
   // kony.print("enCryptDeviceId::::"+enCryptDeviceId)
    //inputParam["deviceId"] = gblQuickBalanceDeviceId; // Commented this as we are sending new ecrypted device id to the service
    inputParam["encryptDeviceId"] = gblQuickBalanceDeviceId;
  	var pushRegID=kony.store.getItem("kpnssubcriptiontoken");
  if(pushRegID!=null && pushRegID!="" && pushRegID!=undefined && pushRegID!="undefined")
  {
   	 inputParam["subscriptionToken"]=pushRegID;
  } 
  	
    invokeServiceSecureAsync("quickBalance", inputParam, callBackQuickBalance);
}

function chkUserStatus(deviceId) {
    gblQBCalled = true;
    var inputParam = {};
    inputParam["quickBalanceFlag"] = "true";
    inputParam["chkUserStatusOnly"] = "yes";
    dismissLoadingScreen();
    if(deviceId != ""){
      gblQuickBalanceDeviceId = deviceId;
    }
   // var enCryptDeviceId = encryptData(gblQuickBalanceDeviceId);
   // kony.print("enCryptDeviceId::::"+enCryptDeviceId)
    //inputParam["deviceId"] = gblQuickBalanceDeviceId; // Commented this as we are sending new ecrypted device id to the service
    inputParam["encryptDeviceId"] = gblQuickBalanceDeviceId;
  var isActive=kony.store.getItem("isUserStatusActive");
   kony.print("FPRINT isActive="+isActive);
  if(isActive==null || isActive==undefined){
    //kony.store.setItem("isUserStatusActive", true);
    invokeServiceSecureAsync("quickBalance", inputParam, chkUserStatusCallBack);
  }else{
    if(isActive){
      kony.print("FPRINT IN isUserStatusActive TO TRUE");
      isUserStatusActive=true;
      kony.print("FPRINT USER STATUS IS ACTIVE");
      if(gblDeviceInfo["model"]=="iPhone X"){
          frmMBPreLoginAccessesPin.btnTouchnBack.skin = "btnFaceId";
          frmMBPreLoginAccessesPin.btnTouchnBack.focusSkin = "btnFaceId";
      }else{
          frmMBPreLoginAccessesPin.btnTouchnBack.skin = "btntouchiconstudio5";
          frmMBPreLoginAccessesPin.btnTouchnBack.focusSkin = "btntouchiconfoc";
      }
      showFPFlex(true);
      fpdefault();
      closeQuickBalance();
    }else{
      kony.print("FPRINT IN isUserStatusActive TO FALSE");
      isUserStatusActive=false;
      kony.print("FPRINT USER STATUS IS INACTIVE");
      showFPFlex(false);
      onClickChangeTouchIcon();
      kony.print("FPRINT USER STATUS errMsg="+kony.i18n.getLocalizedString("VQB_MBStatus"));
    }
  }
  
   // invokeServiceSecureAsync("quickBalance", inputParam, chkUserStatusCallBack);
}



function showQuickBalanceImage() {
	kony.print("device details :" + kony.os.deviceInfo())
	var locale_app=kony.i18n.getCurrentLocale();
	if(locale_app == "th_TH"){
		frmMBPreLoginAccessesPin.imgQBStatic.src = "quick_balance_th.png"
	}else{
		frmMBPreLoginAccessesPin.imgQBStatic.src = "quick_balance.png"
	}
    frmMBPreLoginAccessesPin.flexQuickBalTextForAccoiunts.isVisible = false;
    frmMBPreLoginAccessesPin.imgHeaderLogo.isVisible = true;
    //frmMBPreLoginAccessesPin.lblHeader1.isVisible = false;
   // frmMBPreLoginAccessesPin.lblHeader2.isVisible = false;
    frmMBPreLoginAccessesPin.imgQBStatic.isVisible = true;
    var deviceWidth = parseInt(kony.os.deviceInfo()["deviceWidth"]);
    kony.print("deviceWidth:::::::"+deviceWidth)
    var deviceName = kony.os.deviceInfo()["name"];
    kony.print("deviceWidth:::::::"+deviceWidth)
    if(deviceWidth <= 768 && deviceName == "android"){
    	kony.print(":::::: satisfying deviceWidth for android")
    	frmMBPreLoginAccessesPin.btnLoginToQB.width = "300dp"
    }
    frmMBPreLoginAccessesPin.flexLoginToQuickBalance.isVisible = true
    frmMBPreLoginAccessesPin.flexLoading.isVisible = false;
  	gbl3dTouchAction = "";
}

function showQuickBalance() {
	kony.print("device details :" + kony.os.deviceInfo())
	dismissLoadingScreen();
	if(gblQBCalled){
      	frmMBPreLoginAccessesPin.flexAccounts.isVisible = true;
		frmMBPreLoginAccessesPin.flexLoading.isVisible = true;
    	frmMBPreLoginAccessesPin.segAccounts.isVisible = false;
		if(kony.net.isNetworkAvailable(constants.NETWORK_TYPE_ANY)){
		    disableFormElements();
		    frmMBPreLoginAccessesPin.flexAccounts.isVisible = true;
          	frmMBPreLoginAccessesPin.flexLoading.isVisible = true;
		    //frmMBPreLoginAccessesPin.flexQuickBalButton.isVisible = false;
		    
		    frmMBPreLoginAccessesPin.flexQuickBalanceCancel.isVisible = true;
		    frmMBPreLoginAccessesPin.flexQuickBalTextForAccoiunts.isVisible = true;
		   // invokeQuickBalanceService();
		    GBL_FLOW_ID_CA = 11;
		//#ifdef iphone
			TrusteerDeviceId();
		//#else
			//#ifdef android
				getUniqueID();
			//#endif
		//#endif
		
		    frmMBPreLoginAccessesPin.flexKeyboard.animate(
		        kony.ui.createAnimation({
		            "100": {
		                "top": "0%",
		                "stepConfig": {
		                    "timingFunction": kony.anim.EASE
		                }
		            }
		        }), {
		            "delay": 0,
		            "iterationCount": 1,
		            "fillMode": kony.anim.FILL_MODE_FORWARDS,
		            "duration": 0.25
		        }
		    );

		//#ifdef android
			if(frmMBPreLoginAccessesPin.flxFingerPrint.isVisible){
				gblQBClicked=true; 
				abruptStop=true;
				//stopListening(); 
			    frmMBPreLoginAccessesPin.flxFingerPrint.animate(
			        kony.ui.createAnimation({
			            "100": {
			                "top": "0%",
			                "stepConfig": {
			                    "timingFunction": kony.anim.EASE
			                }
			            }
			        }), {
			            "delay": 0,
			            "iterationCount": 1,
			            "fillMode": kony.anim.FILL_MODE_FORWARDS,
			            "duration": 0.25
			        }
			    );		   
			   // abruptStop=true;
				//stopListening(); 
               //MIB-14053: Change FP to kony API
               kony.localAuthentication.cancelAuthentication();
			}
		//#endif
			
	    }else{
	    	alert(kony.i18n.getLocalizedString("genErrorWifiOff"));
	    	gblQBCalled = true;
	    }
	  
    }
}

function hideQBImage() {
    frmMBPreLoginAccessesPin.imgHeaderLogo.isVisible = true;
    //frmMBPreLoginAccessesPin.lblHeader1.isVisible = false;
    //frmMBPreLoginAccessesPin.lblHeader2.isVisible = false;
    frmMBPreLoginAccessesPin.imgQBStatic.isVisible = false;
    frmMBPreLoginAccessesPin.flexLoginToQuickBalance.isVisible = false;
    frmMBPreLoginAccessesPin.flexQuickBalTextForAccoiunts.isVisible = false;
     //#ifdef iphone
  			 //prevent multiple popup
			 //onClickTouchButtonPrelogin();
	 //#endif
}


function hideQuickBalanceResults() {
    frmMBPreLoginAccessesPin.segAccounts.isVisible = false;
    frmMBPreLoginAccessesPin.flexQuickBalTextForAccoiunts.isVisible = false;
    //#ifdef iphone
			 onClickTouchButtonPrelogin();
	//#endif
   
}

function closeQuickBalance() {
  	gbl3dTouchAction = "";
	gblQBCalled = true;
	gblQuickBalanceResponse = "";
    gblLoginTab = "FP";
	//if(frmMBPreLoginAccessesPin.flexKeyboard.isVisible){
	kony.print("FPRINT ENETERED1");
	    frmMBPreLoginAccessesPin.flexKeyboard.animate(
	        kony.ui.createAnimation({
	            "100": {
	                "top": "0%",
	                "stepConfig": {
	                    "timingFunction": kony.anim.EASE
	                }
	            }
	        }), {
	            "delay": 0,
	            "iterationCount": 1,
	            "fillMode": kony.anim.FILL_MODE_FORWARDS,
	            "duration": 0.25
	        }
	    );
	    frmMBPreLoginAccessesPin.flexKeyboard.top="0%";
	//  }  
   
	//#ifdef android    
	if(frmMBPreLoginAccessesPin.flxFingerPrint.isVisible){
	//frmMBPreLoginAccessesPin.flxFingerPrint.forceLayout();
		   frmMBPreLoginAccessesPin.flxFingerPrint.animate(
		        kony.ui.createAnimation({
		            "100": {
		                "top": "0%",
		                "stepConfig": {
		                    "timingFunction": kony.anim.EASE
		                }
		            }
		        }), {
		            "delay": 0,
		            "iterationCount": 1,
		            "fillMode": kony.anim.FILL_MODE_FORWARDS,
		            "duration": 0.25
		        }
		    );    
		    
		    kony.print("FPRINT ENETERED2 top="+frmMBPreLoginAccessesPin.flxFingerPrint.top);
		    frmMBPreLoginAccessesPin.flxFingerPrint.top="0%";
		    frmMBPreLoginAccessesPin.flxFingerPrint.forceLayout();
		     authUsingTouchIDUsingKonyAPI(); //authUsingTouchID();
	   } 
    //#endif
		    kony.print("FPRINT ENETERED3 top="+frmMBPreLoginAccessesPin.flxFingerPrint.top);

    frmMBPreLoginAccessesPin.flexAccounts.isVisible = false;
    //frmMBPreLoginAccessesPin.flexQuickBalButton.isVisible = true;
    frmMBPreLoginAccessesPin.flexQuickBalanceCancel.isVisible = false;
    frmMBPreLoginAccessesPin.flexLoading.isVisible = false;
    if (frmMBPreLoginAccessesPin.segAccounts.isVisible) {
        hideQuickBalanceResults();
    } else {
        hideQuickBalanceResults();
        hideQBImage();
    }
}






function showAnimation1() {

}

function showAnimation2() {
  
}

function genRandomNum() {
    return Math.floor((Math.random() * 10000) + 1);

}

function preshowPreAccessScreen() {
  kony.print("usage >> start frmMBPreLoginAccessesPin "+new Date().getTime());
  kony.print("FPRINT: preshowPreAccessScreen called");
  gblActivationCurrentForm = "";
  kony.print("FPRINT: gblCallPrePost=="+gblCallPrePost);
  if(gblCallPrePost) {
	 kony.print("FPRINT: gblCallPrePost inside");
		 frmMBPreLoginAccessesPin.bounces = false;
	    //frmMBPreLoginAccessesPin.scrollboxMain.bounces = false;
	    afterLogoutMBInPreshowSnippetCode();
	    //DisableFadingEdges(frmMBPreLoginAccessesPin);
	}
	preShowSetLoginCircle();
}

function preShowSetLoginCircle() {
kony.print("FPRINT: inside preShowSetLoginCircle");
	var deviceName = gblDeviceInfo["name"];
	var screenwidth = gblDeviceInfo["deviceWidth"];
  	var deviceModel = gblDeviceInfo["model"]
    kony.print("FPRINT: DEVICE MODEL IS "+deviceModel);
  	frmMBPreLoginAccessesPin.imgone.src = "key_default.png";
    frmMBPreLoginAccessesPin.imgTwo.src = "key_default.png";
    frmMBPreLoginAccessesPin.imgThree.src = "key_default.png";
    frmMBPreLoginAccessesPin.imgFour.src = "key_default.png";
    frmMBPreLoginAccessesPin.imgFive.src = "key_default.png";
    frmMBPreLoginAccessesPin.imgSix.src = "key_default.png";
  	gblPinCount = 0;
    var screenwidth = gblDeviceInfo["deviceWidth"];
    
    var screenheight = gblDeviceInfo["deviceHeight"];
   
    // iphone X condition to fix button the pre access pin screen
    //#ifdef iphone
    if(screenheight > 2400){
        	frmMBPreLoginAccessesPin.CopyFlexContainer0bddc389f3e0d42.height = "80%";
			frmMBPreLoginAccessesPin.CopyFlexContainer063256fd1562a46.height = "80%";
			frmMBPreLoginAccessesPin.CopyFlexContainer0e661061f71994f.height = "80%";
			frmMBPreLoginAccessesPin.CopyFlexContainer096ba943765d344.height = "80%";
			frmMBPreLoginAccessesPin.CopyFlexContainer096ba943765d344.centerY = "46%";
    }
    //#endif
   
    //#ifdef android
           if (screenheight ==  2094 && screenwidth == 1080) {
        frmMBPreLoginAccessesPin.CopyFlexContainer0bddc389f3e0d42.height = "80%";
        frmMBPreLoginAccessesPin.CopyFlexContainer063256fd1562a46.height = "80%";
        frmMBPreLoginAccessesPin.CopyFlexContainer0e661061f71994f.height = "80%";
        frmMBPreLoginAccessesPin.CopyFlexContainer096ba943765d344.height = "80%";
        frmMBPreLoginAccessesPin.CopyFlexContainer096ba943765d344.centerY = "44%";
    }
            
     //#endif
   
	/*
	if (deviceName == "android") {

		if (screenwidth == 1080) {
	
		} else if (screenwidth == 768) {
			frmMBPreLoginAccessesPin.CopyFlexContainer0bddc389f3e0d42.height = "92%";
			frmMBPreLoginAccessesPin.CopyFlexContainer063256fd1562a46.height = "92%";
			frmMBPreLoginAccessesPin.CopyFlexContainer0e661061f71994f.height = "92%";
			frmMBPreLoginAccessesPin.CopyFlexContainer096ba943765d344.height = "92%";
			
			frmMBPreLoginAccessesPin.CopyFlexContainer0bddc389f3e0d42.width = "75%";
			frmMBPreLoginAccessesPin.CopyFlexContainer063256fd1562a46.width = "75%";
			frmMBPreLoginAccessesPin.CopyFlexContainer0e661061f71994f.width = "75%";
			frmMBPreLoginAccessesPin.CopyFlexContainer096ba943765d344.width = "75%";
			
			frmMBPreLoginAccessesPin.btnOne.height = "99%";
			frmMBPreLoginAccessesPin.btnTwo.height = "99%";
			frmMBPreLoginAccessesPin.btnThree.height = "99%";
			frmMBPreLoginAccessesPin.btnFour.height = "99%";
			frmMBPreLoginAccessesPin.btnFive.height = "99%";
			frmMBPreLoginAccessesPin.btnSix.height = "99%";
			frmMBPreLoginAccessesPin.btnSeven.height = "99%";
			frmMBPreLoginAccessesPin.btnEight.height = "99%";
			frmMBPreLoginAccessesPin.btnNine.height = "99%";
			frmMBPreLoginAccessesPin.btnForgotPin.height = "99%";
			frmMBPreLoginAccessesPin.btnZero.height = "99%";
			frmMBPreLoginAccessesPin.btnTouchnBack.height = "99%";
			
		}else if(screenwidth == 720){
			frmMBPreLoginAccessesPin.CopyFlexContainer0bddc389f3e0d42.height = "92%";
			frmMBPreLoginAccessesPin.CopyFlexContainer063256fd1562a46.height = "92%";
			frmMBPreLoginAccessesPin.CopyFlexContainer0e661061f71994f.height = "92%";
			frmMBPreLoginAccessesPin.CopyFlexContainer096ba943765d344.height = "92%";
			
			frmMBPreLoginAccessesPin.btnOne.height = "99%";
			frmMBPreLoginAccessesPin.btnTwo.height = "99%";
			frmMBPreLoginAccessesPin.btnThree.height = "99%";
			frmMBPreLoginAccessesPin.btnFour.height = "99%";
			frmMBPreLoginAccessesPin.btnFive.height = "99%";
			frmMBPreLoginAccessesPin.btnSix.height = "99%";
			frmMBPreLoginAccessesPin.btnSeven.height = "99%";
			frmMBPreLoginAccessesPin.btnEight.height = "99%";
			frmMBPreLoginAccessesPin.btnNine.height = "99%";
			frmMBPreLoginAccessesPin.btnForgotPin.height = "99%";
			frmMBPreLoginAccessesPin.btnZero.height = "99%";
			frmMBPreLoginAccessesPin.btnTouchnBack.height = "99%";
		}
	} */
}

function postshowPreAccessScreen() {
    // This code to reset the pre access screen after selecting prompt pay in myqr
    frmMBPreLoginAccessesPin.flexCrossmark.onClick = onClickCloseMarkQR;
  	var prevForm = kony.application.getPreviousForm();
  	//kony.print("prevForm--->"+prevForm);
  	if(prevForm != null){
      	 var prevFormID=prevForm.id;
      	 kony.print("prevForm ID--->"+prevFormID);
         if(prevFormID != "frmMyQRPromptPayRegister" 
            && prevFormID != "frmscanqrLanding"
            && prevFormID != "frmQRSuccess"
            && prevFormID != "frmEDonationPaymentConfirm"){
         gblGoDirectlyToPrompPay = false;
		 gblPreLoginBillPayFlow=false;
         frmMBPreLoginAccessesPin.flexCrossmark.isVisible = false;
         frmMBPreLoginAccessesPin.flexHeader.isVisible = true;
         frmMBPreLoginAccessesPin.flexSwipeContainer.top = "0%"
         frmMBPreLoginAccessesPin.FlexContainer0b16eb3557e0043.isVisible = true
         frmMBPreLoginAccessesPin.imgQRHeaderLogo.isVisible = false; 
         frmMBPreLoginAccessesPin.LabelLoginForQR.isVisible = false; 
         frmMBPreLoginAccessesPin.FlexLine.isVisible = false; 
         frmMBPreLoginAccessesPin.FlexLoginContainer.top = "0dp";
      
   		}
      	
    }
    //adding to clear qr session
  	clearQRSession();
  	
	if(gblCallPrePost) {
      //#ifdef spaip
    //#define frmMBPreLoginAccessesPin_frmMBPreLoginAccessesPin_postshow_seq0_act0
    //#endif

    //#ifdef frmMBPreLoginAccessesPin_frmMBPreLoginAccessesPin_postshow_seq0_act0
    //commonMBPostShow();
    //#endif
    //frmAccountSummaryLanding = null;
    //frmAccountSummaryLandingGlobals();
    //TMBUtil.DestroyForm(frmAccountSummaryLanding);
    //moved from accsPwdValidatnLogin to here to prevent black screen after entering the access pin

    /* 
	MBcopyright_text_display.call(this);
	
	 */

    /* 
	campaignPreLoginService.call(this,"imgMBPreLogin", "frmAfterLogoutMB", "M");
	
	 */

    /* 
	MBcopyright_text_display.call(this);
	
	 */

    /* 
	 //#ifdef android
	 //#define frmMBPreLoginAccessesPin_frmMBPreLoginAccessesPin_postshow_seq0_act5
	 //#endif
	 //#ifdef iphone
	 //#define frmMBPreLoginAccessesPin_frmMBPreLoginAccessesPin_postshow_seq0_act5
	 //#endif
	 
	 //#ifdef frmMBPreLoginAccessesPin_frmMBPreLoginAccessesPin_postshow_seq0_act5
	frmAfterLogoutMB.tbxAccessPIN.setFocus(true);
	 //#endif
	
	 */
    //setCallBacks.call(this);
    //#ifdef android
    //#define frmMBPreLoginAccessesPin_frmMBPreLoginAccessesPin_postshow_seq0_act7
    //#endif

    //#ifdef frmMBPreLoginAccessesPin_frmMBPreLoginAccessesPin_postshow_seq0_act7
    gblRiskServicecall = false;
    //#endif
    // Below code is for Quick Balance and to show loading indicator animation
	kony.print("device details :" + kony.os.deviceInfo())
    //setGestureToQuick();
    //setGestureToClose();
    if (gbl3dTouchAction != "quickbalance") {
     //#ifdef android
    	if(frmMBPreLoginAccessesPin.flxFingerPrint.isVisible) {
		   //WRITE CODE FOR SWIP RESET
          //closeQuickBalance();
	}
    //#else
    //#endif

    //#ifdef iphone
		if(frmMBPreLoginAccessesPin.flexAccounts.isVisible) {
         //WRITE CODE FOR SWIP RESET
		//closeQuickBalance();
	}
    //#else
    //#endif
    
    }

    }
    //showAnimation1();
    //showAnimation2();
    assignGlobalForMenuPostshow();
    
   //MIB-14053: Use Kony API for Touch ID.  Calling authUsingTouchIDUsingKonyAPI in showFPFlex method
	/*if (gblDeviceInfo["name"] == "android"){
				if(frmMBPreLoginAccessesPin.btnTouchnBack.skin == "btntouchiconstudio5" && frmMBPreLoginAccessesPin.flxFingerPrint.isVisible){
				  authUsingTouchIDUsingKonyAPI();    //authUsingTouchID();
				}
	}    */
    //3d touch quickbalance
  	if (gbl3dTouchAction == "quickbalance") {
		//WRITE SWIPE FUNCTIONALITY TO DISPLAY QUICKBALANCE
      	//showQuickBalance();
      	accessPinOnClickQuickBalance();
	}  
  
  
    //Comment below code as showTouchIdPopupUsingKonyAPI is called in pre login
    /*
    //#ifdef iphone
	   if (!isSignedUser) {
           kony.print("before showing TouchId popup");
		   showTouchIdPopupUsingKonyAPI();
	   }
    //#endif
    */ 
    kony.print("MB_AndFinPrintIns1 text:"+frmMBPreLoginAccessesPin.lblLoginDesc1.text);
    kony.print("MB_AndFinPrintIns2 text:"+frmMBPreLoginAccessesPin.lblLoginDesc2.text);
    kony.print("enterpin:"+frmMBPreLoginAccessesPin.btnAccessPin.text);
	frmMBPreLoginAccessesPin.btnAccessPin.text = "";
 	frmMBPreLoginAccessesPin.lblLoginDesc1.text = "";
 	frmMBPreLoginAccessesPin.lblLoginDesc2.text ="";
    frmMBPreLoginAccessesPin.LabelQuickBalMenu.text ="";
    frmMBPreLoginAccessesPin.LabelLoginMenu.text="";
    frmMBPreLoginAccessesPin.LabelScanMenu.text="";
  	frmMBPreLoginAccessesPin.btnForgotPin.text="";
	kony.print("MB_AndFinPrintIns1:"+kony.i18n.getLocalizedString("MB_AndFinPrintIns1"));
    kony.print("MB_AndFinPrintIns2:"+kony.i18n.getLocalizedString("MB_AndFinPrintIns2"));
    kony.print("enterpin text:"+kony.i18n.getLocalizedString("enterpin"));
    frmMBPreLoginAccessesPin.LabelQuickBalMenu.text = kony.i18n.getLocalizedString('VQB_MSG01');
	frmMBPreLoginAccessesPin.LabelLoginMenu.text = kony.i18n.getLocalizedString('login');
	frmMBPreLoginAccessesPin.LabelScanMenu.text = kony.i18n.getLocalizedString('IbQRScan');
    frmMBPreLoginAccessesPin.btnAccessPin.text = kony.i18n.getLocalizedString("enterpin");
  	frmMBPreLoginAccessesPin.lblLoginDesc1.text = kony.i18n.getLocalizedString("MB_AndFinPrintIns1");
    frmMBPreLoginAccessesPin.lblLoginDesc2.text = kony.i18n.getLocalizedString("MB_AndFinPrintIns2");
    frmMBPreLoginAccessesPin.btnForgotPin.text = kony.i18n.getLocalizedString('btnforgotpin');
    frmMBPreLoginAccessesPin.lblEnterPin.text = kony.i18n.getLocalizedString("enterpin");
     
  	kony.print("usage >> end postshow frmMBPreLoginAccessesPin "+new Date().getTime());
	//writing code for setting up the swipe functionality
  	/*
  	var setSwipetest = {
					fingers: 1,
					swipedistance: 300,
					swipevelocity: 25
	};
	//double tap gesture
    //To add a TAP gesture recognizer on a hbox with ID hbx1 placed on a form frm1
    //var tapGesture=frm1.hbx1.setGgestureRecognizer(1,setupTblTap,myTap);
    var MainContainerGesture = frmMBPreLoginAccessesPin.FlexLoginContainer.addGestureRecognizer(2,setSwipetest, MainContainerGestureCallack);
	var QBBalanceGesture = frmMBPreLoginAccessesPin.FlexQuickBalanceContainer.addGestureRecognizer(2,setSwipetest, QBBalanceGestureCallack);
	//var QRCodeGesture = frmMBPreLoginAccessesNew.flexSwipeQRCode.setGgestureRecognizer(1, setSwipe, myTap);
	*/
}

function MainContainerGestureCallack(myWidget, gestureInfo) {
	if( gestureInfo["swipeDirection"] ==1){
		// SWIPE RIGHT
		frmscanqrLanding.show();
	}
	else {
      	frmMBPreLoginAccessesPin.FlexQuickBalanceContainer.animate(kony.ui.createAnimation({
			"100": {
				"left": "0%",
				"stepConfig": {
					"timingFunction": kony.anim.EASE
				},
				"rectified": true
			}
		}), {
			"delay": 0,
			"iterationCount": 1,
			"fillMode": kony.anim.FILL_MODE_FORWARDS,
			"duration": 0.25
		}, {
			"animationEnd": MainContainerGestureCallackAfterMoveRight
		});
      	
	}
    
}

function QBBalanceGestureCallack(myWidget, gestureInfo) {
	if( gestureInfo["swipeDirection"] ==1){
    frmMBPreLoginAccessesPin.FlexLoginContainer.animate(kony.ui.createAnimation({
        "100": {
            "left": "0%",
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            },
            "rectified": true
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.25
    }, {
        "animationEnd": QBBalanceGestureCallackAfterMove
    });
	}
	else{}
}

function QBBalanceGestureCallackAfterMove() {
	frmMBPreLoginAccessesPin.FlexQuickBalanceContainer.left="-100%";
  	closeQuickBalance();
}

function MainContainerGestureCallackAfterMoveRight() {
  	frmMBPreLoginAccessesPin.FlexLoginContainer.left="100%";
  	showQuickBalance();
  	//alert("here");
}

function accessPinOnClickQuickBalance(){
  gblLoginTab = "QB";
  
  //alert("Ding"+frmMBPreLoginAccessesPin.FlexLoginContainer.left)
  if(frmMBPreLoginAccessesPin.FlexLoginContainer.left=="0%")
  {
     frmMBPreLoginAccessesPin.FlexQuickBalanceContainer.left="0%";
  	frmMBPreLoginAccessesPin.FlexLoginContainer.left="100%";
  	showQuickBalance();
  }  
}

function accessPinOnClickLogin(){
  gblLoginTab = "FP";
  //MIB-14053: Change FP to kony API
 // authUsingTouchIDUsingKonyAPI();
  //alert("Ding"+frmMBPreLoginAccessesPin.FlexQuickBalanceContainer.left)
  if(frmMBPreLoginAccessesPin.FlexQuickBalanceContainer.left=="0%")
  {
    frmMBPreLoginAccessesPin.FlexLoginContainer.left="0%";
    frmMBPreLoginAccessesPin.FlexQuickBalanceContainer.left="-100%";
  	closeQuickBalance();
  }  
}

function onClickKeyBoardButton(eventobject) {
    onClickOfKeypad();
    getPin(eventobject);
    onClickChangeTouchIcon();
}


function onClickLoginToQB() {
    gblQuickBalanceFromLogin = true;
  	frmMBPreLoginAccessesPin.FlexLoginContainer.left="0%";
    frmMBPreLoginAccessesPin.FlexQuickBalanceContainer.left="-100%";
    closeQuickBalance();
}

function showQuickBalanceButton() {
	//frmMBPreLoginAccessesPin.flexQuickBalButton.isVisible = false;
  	frmMBPreLoginAccessesPin.FlexContainerDefault.setVisibility(false);
    frmMBPreLoginAccessesPin.FlexContainerTwo.setVisibility(true);
}

function onPullQuickBalanceSegment() {
    frmMBPreLoginAccessesPin.segAccounts.removeAll();
    disableFormElements();
    frmMBPreLoginAccessesPin.segAccounts.isVisible = false;
    gblIsPullToRefreshCall = true;
    frmMBPreLoginAccessesPin.flexLoading.isVisible = true;
    invokeQuickBalanceService("");

}

function changeSkinForSpecifyBtn(buttonId){
	//var buttonId = eventObject.id;
	if(buttonId == "btnSpecify"){
		if(frmIBOpenNewSavingsAcc.btnSpecify.skin == "sknbtnGreyFont80"){
			frmIBOpenNewSavingsAcc.btnSpecify.skin == "sknBtnBlueFont80"
			frmIBOpenNewSavingsAcc.btnNotSpecify.skin == "sknbtnGreyFont80"	
		}
	
	}else if(buttonId == "btnNotSpecify"){
		if(frmIBOpenNewSavingsAcc.btnNotSpecify.skin == "sknbtnGreyFont80"){
			frmIBOpenNewSavingsAcc.btnNotSpecify.skin == "sknBtnBlueFont80"
			frmIBOpenNewSavingsAcc.btnSpecify.skin == "sknbtnGreyFont80"
		 }
	}
}