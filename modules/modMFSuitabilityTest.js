gblMFSuitQuestNo = 1;
maxNoOfQuestion = 0;
gblMFSuitQuestions = [];
gblMFSuitAnswers = [];
gblMFfxAnswer = "0";
gblMFSuitAlertDays = ["1","2","3","4","5","6","7","15","30","45","60","75","90"];
/**
 * @function
 *
 */
function onClickNextfrmMFSuitabilityReview(){  
  //frmMFSuitabilityTnC.show();  
  if(GBL_MF_TEMENOS_ENABLE_FLAG == "OFF"){
    	showAlert(kony.i18n.getLocalizedString("Mutual_Fund_Suitability_Off"), kony.i18n.getLocalizedString("info"));
        return false;
  } else {
  	 if(GBL_MF_SUITABILITY_FLAG == "OFF") {
  	 	showAlert(kony.i18n.getLocalizedString("Mutual_Fund_Suitability_Off"), kony.i18n.getLocalizedString("info"));
        return false;
  	 } else {
  	 	loadMFSuitabilityTermsNConditions();
  	 }
  }
 
}

/**
 * @function
 *
 */
function getBackToMyMutualFundsSummaryPage(){  
  MBcallMutualFundsSummary();
}

/**
 * @function
 *
 */
function onClickNextfrmSuitabilityReviewTnC(){
  preShowfrmMFSuitabilityTest1();
  MFSuitQuestionnaireService();
}

function onClickBackfrmSuitabilityReviewTnC(){
  getMFSuitibilityReviewDetails();
}

/**
 * @function
 *
 */
function onClickBackfrmMFSuitabilityTest1(){
  //loadMFSuitabilityTermsNConditions();
  frmMFSuitabilityTnC.show();  
}

function clearMFSuitabilityTestAnswers(){
  frmMFSuitabilityTest1.flxAnswer1.skin = "slFbox";
  frmMFSuitabilityTest1.flxAnswer2.skin = "slFbox";
  frmMFSuitabilityTest1.flxAnswer3.skin = "slFbox";
  frmMFSuitabilityTest1.flxAnswer4.skin = "slFbox";
  frmMFSuitabilityTest1.flxAnswer5.skin = "slFbox";
  frmMFSuitabilityTest1.flxAnswer6.skin = "slFbox";
  frmMFSuitabilityTest1.flxTick1.setVisibility(true);
  frmMFSuitabilityTest1.flxTick2.setVisibility(true);
  frmMFSuitabilityTest1.flxTick3.setVisibility(true);
  frmMFSuitabilityTest1.flxTick4.setVisibility(true);
  frmMFSuitabilityTest1.flxTick5.setVisibility(true);
  frmMFSuitabilityTest1.flxTick6.setVisibility(true);
  frmMFSuitabilityTest1.flxTicked1.setVisibility(false);
  frmMFSuitabilityTest1.flxTicked2.setVisibility(false);
  frmMFSuitabilityTest1.flxTicked3.setVisibility(false);
  frmMFSuitabilityTest1.flxTicked4.setVisibility(false);
  frmMFSuitabilityTest1.flxTicked5.setVisibility(false);
  frmMFSuitabilityTest1.flxTicked6.setVisibility(false);
}

function checkForMFSuitTestLastAnswer(){
  if(gblMFSuitQuestNo == maxNoOfQuestion) {
    frmMFSuitabilityTest1.flxStep4.setVisibility(false);
    frmMFSuitabilityTest1.flxStepDone.setVisibility(true);
  }  
}

function onClickMFSuitabilityTestAnswer1(){
  clearMFSuitabilityTestAnswers();
  if(GBL_MF_TEMENOS_ENABLE_FLAG == "OFF"){
     if(gblMFSuitQuestNo == maxNoOfQuestion) gblMFfxAnswer = "1";
  	 else gblMFSuitAnswers[gblMFSuitQuestNo-1] = "1";
  }else{
  	 gblMFSuitAnswers[gblMFSuitQuestNo-1] = getAnswerValue(gblMFSuitQuestNo,"1");
  }
  frmMFSuitabilityTest1.flxAnswer1.skin = "flexGreyBG";
  frmMFSuitabilityTest1.flxTick1.setVisibility(false);
  frmMFSuitabilityTest1.flxTicked1.setVisibility(true);
  checkForMFSuitTestLastAnswer();
  setEnabledNextMFSuitabilityTest();
}
function onClickMFSuitabilityTestAnswer2(){
  clearMFSuitabilityTestAnswers();
  if(GBL_MF_TEMENOS_ENABLE_FLAG == "OFF"){
  	if(gblMFSuitQuestNo == maxNoOfQuestion) gblMFfxAnswer = "2";
  	else gblMFSuitAnswers[gblMFSuitQuestNo-1] = "2";
  }else{
   	gblMFSuitAnswers[gblMFSuitQuestNo-1] = getAnswerValue(gblMFSuitQuestNo,"2");
  }
  frmMFSuitabilityTest1.flxAnswer2.skin = "flexGreyBG";
  frmMFSuitabilityTest1.flxTick2.setVisibility(false);
  frmMFSuitabilityTest1.flxTicked2.setVisibility(true);
  checkForMFSuitTestLastAnswer();
  setEnabledNextMFSuitabilityTest();
}
function onClickMFSuitabilityTestAnswer3(){
  clearMFSuitabilityTestAnswers();
  if(GBL_MF_TEMENOS_ENABLE_FLAG == "OFF"){
  	gblMFSuitAnswers[gblMFSuitQuestNo-1] = "3";
  }else{
    gblMFSuitAnswers[gblMFSuitQuestNo-1] = getAnswerValue(gblMFSuitQuestNo,"3");
  }
  frmMFSuitabilityTest1.flxAnswer3.skin = "flexGreyBG";
  frmMFSuitabilityTest1.flxTick3.setVisibility(false);
  frmMFSuitabilityTest1.flxTicked3.setVisibility(true);
  checkForMFSuitTestLastAnswer();
  setEnabledNextMFSuitabilityTest();
}
function onClickMFSuitabilityTestAnswer4(){
  clearMFSuitabilityTestAnswers();
  if(GBL_MF_TEMENOS_ENABLE_FLAG == "OFF"){
    gblMFSuitAnswers[gblMFSuitQuestNo-1] = "4";
  }else{
  	gblMFSuitAnswers[gblMFSuitQuestNo-1] = getAnswerValue(gblMFSuitQuestNo,"4");  
  }
  frmMFSuitabilityTest1.flxAnswer4.skin = "flexGreyBG";
  frmMFSuitabilityTest1.flxTick4.setVisibility(false);
  frmMFSuitabilityTest1.flxTicked4.setVisibility(true);
  checkForMFSuitTestLastAnswer();
  setEnabledNextMFSuitabilityTest();
}

function onClickMFSuitabilityTestAnswer5(){
  clearMFSuitabilityTestAnswers();
  if(GBL_MF_TEMENOS_ENABLE_FLAG == "OFF"){
    gblMFSuitAnswers[gblMFSuitQuestNo-1] = "5";
  }else{
  	gblMFSuitAnswers[gblMFSuitQuestNo-1] = getAnswerValue(gblMFSuitQuestNo,"5");  
  }
  frmMFSuitabilityTest1.flxAnswer5.skin = "flexGreyBG";
  frmMFSuitabilityTest1.flxTick5.setVisibility(false);
  frmMFSuitabilityTest1.flxTicked5.setVisibility(true);
  checkForMFSuitTestLastAnswer();
  setEnabledNextMFSuitabilityTest();
}

function onClickMFSuitabilityTestAnswer6(){
  clearMFSuitabilityTestAnswers();
  if(GBL_MF_TEMENOS_ENABLE_FLAG == "OFF"){
    gblMFSuitAnswers[gblMFSuitQuestNo-1] = "6";
  }else{
  	gblMFSuitAnswers[gblMFSuitQuestNo-1] = getAnswerValue(gblMFSuitQuestNo,"6");  
  }
  frmMFSuitabilityTest1.flxAnswer6.skin = "flexGreyBG";
  frmMFSuitabilityTest1.flxTick6.setVisibility(false);
  frmMFSuitabilityTest1.flxTicked6.setVisibility(true);
  checkForMFSuitTestLastAnswer();
  setEnabledNextMFSuitabilityTest();
}

function setEnabledNextMFSuitabilityTest(){
  frmMFSuitabilityTest1.btnNext.skin = "btnBlueBGNoRound";
  frmMFSuitabilityTest1.btnNext.focusSkin = "btnBlue200GreyBG";
  frmMFSuitabilityTest1.btnNext.setEnabled(true);
  if(gblMFSuitQuestNo < maxNoOfQuestion){
    frmMFSuitabilityTest1.btnNext.text = kony.i18n.getLocalizedString("Next");
    frmMFSuitabilityTest1.btnNext.onClick = onClickMFSuitNextQuestion;
  }else{
    frmMFSuitabilityTest1.btnNext.text = kony.i18n.getLocalizedString("MF_ST_Btn_Sumit");
    frmMFSuitabilityTest1.btnNext.onClick = onClickMFSuitSubmitQuestion;
  }
}

function setDisabledNextMFSuitabilityTest(){
  frmMFSuitabilityTest1.btnNext.skin = "btnGreyBGNoRound";
  frmMFSuitabilityTest1.btnNext.focusSkin = "btnGreyBGNoRound";
  frmMFSuitabilityTest1.btnNext.setEnabled(false);
  if(gblMFSuitQuestNo < maxNoOfQuestion){
    frmMFSuitabilityTest1.btnNext.text = kony.i18n.getLocalizedString("Next");
  }else{
    frmMFSuitabilityTest1.btnNext.text = kony.i18n.getLocalizedString("MF_ST_Btn_Sumit");
  }
}

function setMFSuitQuestionAndAnswerDetail(){
  kony.print("Curret Locale"+kony.i18n.getCurrentLocale());
  if(kony.i18n.getCurrentLocale() == "en_US"){
      kony.print("inside if");
      frmMFSuitabilityTest1.lblQuestion.text = gblMFSuitQuestions[gblMFSuitQuestNo-1]["questionEN"];
      frmMFSuitabilityTest1.lblAnswer1.text = gblMFSuitQuestions[gblMFSuitQuestNo-1]["answerOneEN"];
      frmMFSuitabilityTest1.lblAnswer2.text = gblMFSuitQuestions[gblMFSuitQuestNo-1]["answerTwoEN"];
      if(isNotBlank(gblMFSuitQuestions[gblMFSuitQuestNo-1]["answerThreeEN"])){
          frmMFSuitabilityTest1.flxAnswer3.setVisibility(true);
          frmMFSuitabilityTest1.flxTick3.setVisibility(true);
          frmMFSuitabilityTest1.lblAnswer3.text = gblMFSuitQuestions[gblMFSuitQuestNo-1]["answerThreeEN"];  
      }else{
          frmMFSuitabilityTest1.lblAnswer3.text ="";
          frmMFSuitabilityTest1.flxTick3.setVisibility(false)
          frmMFSuitabilityTest1.flxAnswer3.setVisibility(false);
      }
      if(isNotBlank(gblMFSuitQuestions[gblMFSuitQuestNo-1]["answerFourEN"])){
          frmMFSuitabilityTest1.flxAnswer4.setVisibility(true);
          frmMFSuitabilityTest1.flxTick4.setVisibility(true);
          frmMFSuitabilityTest1.lblAnswer4.text = gblMFSuitQuestions[gblMFSuitQuestNo-1]["answerFourEN"];  
      }else{
          frmMFSuitabilityTest1.lblAnswer4.text ="";
          frmMFSuitabilityTest1.flxTick4.setVisibility(false);
          frmMFSuitabilityTest1.flxAnswer4.setVisibility(false);
      }
      if(isNotBlank(gblMFSuitQuestions[gblMFSuitQuestNo-1]["answerFiveEN"])){
          frmMFSuitabilityTest1.flxAnswer5.setVisibility(true);
          frmMFSuitabilityTest1.lblAnswer5.text = gblMFSuitQuestions[gblMFSuitQuestNo-1]["answerFiveEN"];
      }else{
          frmMFSuitabilityTest1.lblAnswer5.text ="";
          frmMFSuitabilityTest1.flxTick5.setVisibility(false);
          frmMFSuitabilityTest1.flxAnswer5.setVisibility(false);
      }
      if(isNotBlank(gblMFSuitQuestions[gblMFSuitQuestNo-1]["answerSixEN"])){
          frmMFSuitabilityTest1.flxAnswer6.setVisibility(true);
          frmMFSuitabilityTest1.lblAnswer6.text = gblMFSuitQuestions[gblMFSuitQuestNo-1]["answerSixEN"];  
      }else{
          frmMFSuitabilityTest1.lblAnswer6.text ="";
          frmMFSuitabilityTest1.flxTick6.setVisibility(false);	
          frmMFSuitabilityTest1.flxAnswer6.setVisibility(false);
      }
   }else{
      kony.print("inside else");
      kony.print("Questions of Thai>>>"+gblMFSuitQuestions[gblMFSuitQuestNo-1]["questionTH"]);
      frmMFSuitabilityTest1.lblQuestion.text = gblMFSuitQuestions[gblMFSuitQuestNo-1]["questionTH"];
      frmMFSuitabilityTest1.lblAnswer1.text = gblMFSuitQuestions[gblMFSuitQuestNo-1]["answerOneTH"];
      frmMFSuitabilityTest1.lblAnswer2.text = gblMFSuitQuestions[gblMFSuitQuestNo-1]["answerTwoTH"];
      if(isNotBlank(gblMFSuitQuestions[gblMFSuitQuestNo-1]["answerThreeTH"])){
          frmMFSuitabilityTest1.flxAnswer3.setVisibility(true);
          frmMFSuitabilityTest1.flxTick3.setVisibility(true);
          frmMFSuitabilityTest1.lblAnswer3.text = gblMFSuitQuestions[gblMFSuitQuestNo-1]["answerThreeTH"];  
      }else{
          frmMFSuitabilityTest1.lblAnswer3.text ="";
          frmMFSuitabilityTest1.flxTick3.setVisibility(false)
          frmMFSuitabilityTest1.flxAnswer3.setVisibility(false);
      } 
      if(isNotBlank(gblMFSuitQuestions[gblMFSuitQuestNo-1]["answerFourTH"])){
          frmMFSuitabilityTest1.flxAnswer4.setVisibility(true);
          frmMFSuitabilityTest1.flxTick4.setVisibility(true);
          frmMFSuitabilityTest1.lblAnswer4.text = gblMFSuitQuestions[gblMFSuitQuestNo-1]["answerFourTH"];  
      }else{
          frmMFSuitabilityTest1.lblAnswer4.text ="";
          frmMFSuitabilityTest1.flxTick4.setVisibility(false);
          frmMFSuitabilityTest1.flxAnswer4.setVisibility(false);
      }
      if(isNotBlank(gblMFSuitQuestions[gblMFSuitQuestNo-1]["answerFiveTH"])){
          frmMFSuitabilityTest1.flxAnswer5.setVisibility(true);
          frmMFSuitabilityTest1.lblAnswer5.text = gblMFSuitQuestions[gblMFSuitQuestNo-1]["answerFiveTH"];
      }else{
          frmMFSuitabilityTest1.lblAnswer5.text ="";
          frmMFSuitabilityTest1.flxTick5.setVisibility(false);
          frmMFSuitabilityTest1.flxAnswer5.setVisibility(false);
      }
      if(isNotBlank(gblMFSuitQuestions[gblMFSuitQuestNo-1]["answerSixTH"])){
          frmMFSuitabilityTest1.flxAnswer6.setVisibility(true);
          frmMFSuitabilityTest1.lblAnswer6.text = gblMFSuitQuestions[gblMFSuitQuestNo-1]["answerSixTH"];  
      }else{
          frmMFSuitabilityTest1.lblAnswer6.text ="";
          frmMFSuitabilityTest1.flxTick6.setVisibility(false);	
          frmMFSuitabilityTest1.flxAnswer6.setVisibility(false);
      }
  }
  //checkAnswerForLastQuetsion();
}

function checkAnswerForLastQuetsion(){
  if(gblMFSuitQuestNo == maxNoOfQuestion){
    frmMFSuitabilityTest1.lblQuestion.text = maxNoOfQuestion + ". " + frmMFSuitabilityTest1.lblQuestion.text;
    frmMFSuitabilityTest1.flxAnswer3.setVisibility(false);
    frmMFSuitabilityTest1.flxAnswer4.setVisibility(false);
  }else{
    frmMFSuitabilityTest1.flxAnswer3.setVisibility(true);
    frmMFSuitabilityTest1.flxAnswer4.setVisibility(true);
  }
}

function preShowfrmMFSuitabilityTest1(){
  frmMFSuitabilityTest1.lbMFSuitabilityTitle.text = kony.i18n.getLocalizedString("MF_ttl_suittest");
  frmMFSuitabilityTest1.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
  frmMFSuitabilityTest1.btnNext.text = kony.i18n.getLocalizedString("Next");
}

function resetMFSuitQuestionToBeDefault(){
  gblMFSuitQuestNo = 1;
  gblMFSuitAnswers = [];
  gblMFfxAnswer = "0";
  resetMFSuitQuestionBarToGrey();
  setDisabledNextMFSuitabilityTest();
  clearMFSuitabilityTestAnswers();
  setHeaderBackMFSuitabilityTest();
  frmMFSuitabilityTest1.lblStep1.text = "1";
  frmMFSuitabilityTest1.lblStep2.text = "2";
  frmMFSuitabilityTest1.lblStep3.text = "3";
  frmMFSuitabilityTest1.lblStep4.text = "4";
  frmMFSuitabilityTest1.flxStep1.skin = "flexBlueCustomRounded";
  //frmMFSuitabilityTest1.flxStep1.width = "31dp";
  //frmMFSuitabilityTest1.flxStep1.height = "31dp";
  frmMFSuitabilityTest1.flxLine1.setVisibility(false);
  frmMFSuitabilityTest1.flxStep1.left = "10%";
  frmMFSuitabilityTest1.flxLine5.setVisibility(true);
  frmMFSuitabilityTest1.flxStep4.setVisibility(true);
  frmMFSuitabilityTest1.flxStepDone.setVisibility(false);
}

function setHeaderBackMFSuitabilityTest(){
  if(gblMFSuitQuestNo == 1){
    frmMFSuitabilityTest1.btnHeaderBack.onClick = onClickNextfrmMFSuitabilityReview;
  }else{
    frmMFSuitabilityTest1.btnHeaderBack.onClick = setHeaderBackMFSuitabilityTestPrevQuestion;
  }
}

function setHeaderBackMFSuitabilityTestPrevQuestion(){
  if(gblMFSuitQuestNo != maxNoOfQuestion){
   // 	gblMFSuitAnswers[gblMFSuitQuestNo-2] = "0"; // rest value for current question
    	gblMFSuitAnswers[gblMFSuitQuestNo-1] = "0"; // rest value for current question
  }
  gblMFSuitQuestNo = gblMFSuitQuestNo - 1;	// get back to prev question
  var prevAnswer = gblMFSuitAnswers[gblMFSuitQuestNo-1];	// check the prev answer to be displayed.
  if(prevAnswer == "0"){
    clearMFSuitabilityTestAnswers();
    setDisabledNextMFSuitabilityTest();
  }else if(prevAnswer == "1"){
    onClickMFSuitabilityTestAnswer1();
  }else if(prevAnswer == "2"){
    onClickMFSuitabilityTestAnswer2();
  }else if(prevAnswer == "3"){
    onClickMFSuitabilityTestAnswer3();
  }else if(prevAnswer == "4"){
    onClickMFSuitabilityTestAnswer4();
  }else if(prevAnswer == "5"){
    onClickMFSuitabilityTestAnswer5();
  }else if(prevAnswer == "6"){
    onClickMFSuitabilityTestAnswer6();
  }
  setMFSuitStepQuestionBar();
  setHeaderBackMFSuitabilityTest();
  setMFSuitQuestionAndAnswerDetail();
  if(gblMFSuitQuestNo == maxNoOfQuestion-1){
    frmMFSuitabilityTest1.flxStep4.skin = "flexGreyCustomRounded";
    frmMFSuitabilityTest1.flxStep4.setVisibility(true);
    frmMFSuitabilityTest1.flxStepDone.setVisibility(false);
  }
}

function resetMFSuitQuestionBarToGrey(){
  frmMFSuitabilityTest1.flxStep1.skin = "flexGreyCustomRounded";
  frmMFSuitabilityTest1.flxStep2.skin = "flexGreyCustomRounded";
  frmMFSuitabilityTest1.flxStep3.skin = "flexGreyCustomRounded";
  frmMFSuitabilityTest1.flxStep4.skin = "flexGreyCustomRounded";
  //frmMFSuitabilityTest1.flxStep1.width = "25dp";
  //frmMFSuitabilityTest1.flxStep1.height = "25dp";
  //frmMFSuitabilityTest1.flxStep2.width = "25dp";
  //frmMFSuitabilityTest1.flxStep2.height = "25dp";
  //frmMFSuitabilityTest1.flxStep3.width = "25dp";
  //frmMFSuitabilityTest1.flxStep3.height = "25dp";
  //frmMFSuitabilityTest1.flxStep4.width = "25dp";
  //frmMFSuitabilityTest1.flxStep4.height = "25dp";
  //frmMFSuitabilityTest1.flxStep2.left = "33%";
  //frmMFSuitabilityTest1.flxStep3.left = "58%";
}

function onClickMFSuitNextQuestion(){
  showLoadingScreen();
  gblMFSuitQuestNo = gblMFSuitQuestNo + 1;
  clearMFSuitabilityTestAnswers();
  setDisabledNextMFSuitabilityTest();
  setMFSuitStepQuestionBar();
  setHeaderBackMFSuitabilityTest();
  setMFSuitQuestionAndAnswerDetail();
  dismissLoadingScreen();
}

function setMFSuitStepQuestionBar(){
  resetMFSuitQuestionBarToGrey();
  if(gblMFSuitQuestNo < maxNoOfQuestion-3){
    kony.print("To doing fixed"+(gblMFSuitQuestNo).toFixed(0));
    frmMFSuitabilityTest1.lblStep1.text = (gblMFSuitQuestNo).toFixed(0);
    frmMFSuitabilityTest1.lblStep2.text = (gblMFSuitQuestNo + 1).toFixed(0);
    frmMFSuitabilityTest1.lblStep3.text = (gblMFSuitQuestNo + 2).toFixed(0);
    frmMFSuitabilityTest1.lblStep4.text = (gblMFSuitQuestNo + 3).toFixed(0);
    if(gblMFSuitQuestNo == maxNoOfQuestion-4){
      frmMFSuitabilityTest1.flxLine1.setVisibility(true);
      frmMFSuitabilityTest1.flxStep1.left = "0%";
      frmMFSuitabilityTest1.flxLine5.setVisibility(false);
    }else if(gblMFSuitQuestNo == 1){
      frmMFSuitabilityTest1.flxLine1.setVisibility(false);
      frmMFSuitabilityTest1.flxStep1.left = "10%";
      frmMFSuitabilityTest1.flxLine5.setVisibility(true);
    }else{
      frmMFSuitabilityTest1.flxLine1.setVisibility(true);
      frmMFSuitabilityTest1.flxStep1.left = "0%";
      frmMFSuitabilityTest1.flxLine5.setVisibility(true);
    }
    frmMFSuitabilityTest1.lblStep1.skin = "lblWhiteLarge171";
    frmMFSuitabilityTest1.lblStep2.skin = "lblWhite142";
    frmMFSuitabilityTest1.lblStep3.skin = "lblWhite142";
    frmMFSuitabilityTest1.lblStep4.skin = "lblWhite142";
    frmMFSuitabilityTest1.flxStep1.skin = "flexBlueCustomRounded";
    //frmMFSuitabilityTest1.flxStep1.width = "31dp";
    //frmMFSuitabilityTest1.flxStep1.height = "31dp";
  }else{
    frmMFSuitabilityTest1.lblStep1.text = (maxNoOfQuestion-3).toFixed(0);
    frmMFSuitabilityTest1.lblStep2.text = (maxNoOfQuestion-2).toFixed(0);
    frmMFSuitabilityTest1.lblStep3.text = (maxNoOfQuestion-1).toFixed(0);
    frmMFSuitabilityTest1.lblStep4.text = (maxNoOfQuestion).toFixed(0);  
    if(gblMFSuitQuestNo == maxNoOfQuestion-3){
      frmMFSuitabilityTest1.lblStep1.skin = "lblWhiteLarge171";
      frmMFSuitabilityTest1.lblStep2.skin = "lblWhite142";
      frmMFSuitabilityTest1.lblStep3.skin = "lblWhite142";
      frmMFSuitabilityTest1.lblStep4.skin = "lblWhite142";
      frmMFSuitabilityTest1.flxStep1.skin = "flexBlueCustomRounded";
      //frmMFSuitabilityTest1.flxStep1.width = "31dp";
      //frmMFSuitabilityTest1.flxStep1.height = "31dp";
    }else if(gblMFSuitQuestNo == maxNoOfQuestion-2){
      frmMFSuitabilityTest1.lblStep1.skin = "lblWhite142";
      frmMFSuitabilityTest1.lblStep2.skin = "lblWhiteLarge171";
      frmMFSuitabilityTest1.lblStep3.skin = "lblWhite142";
      frmMFSuitabilityTest1.lblStep4.skin = "lblWhite142";
      frmMFSuitabilityTest1.flxStep2.skin = "flexBlueCustomRounded";
      //frmMFSuitabilityTest1.flxStep2.width = "31dp";
      //frmMFSuitabilityTest1.flxStep2.height = "31dp";
      //frmMFSuitabilityTest1.flxStep2.left = "32%";
    }else if(gblMFSuitQuestNo == maxNoOfQuestion-1){
      frmMFSuitabilityTest1.lblStep1.skin = "lblWhite142";
      frmMFSuitabilityTest1.lblStep2.skin = "lblWhite142";
      frmMFSuitabilityTest1.lblStep3.skin = "lblWhiteLarge171";
      frmMFSuitabilityTest1.lblStep4.skin = "lblWhite142";
      frmMFSuitabilityTest1.flxStep3.skin = "flexBlueCustomRounded";
      //frmMFSuitabilityTest1.flxStep3.width = "31dp";
      //frmMFSuitabilityTest1.flxStep3.height = "31dp";
      //frmMFSuitabilityTest1.flxStep3.left = "57%";
    }else if(gblMFSuitQuestNo == maxNoOfQuestion){
      frmMFSuitabilityTest1.lblStep1.skin = "lblWhite142";
      frmMFSuitabilityTest1.lblStep2.skin = "lblWhite142";
      frmMFSuitabilityTest1.lblStep3.skin = "lblWhite142";
      frmMFSuitabilityTest1.lblStep4.skin = "lblWhiteLarge171";
      frmMFSuitabilityTest1.flxStep4.skin = "flexBlueCustomRounded";
      //frmMFSuitabilityTest1.flxStep4.width = "31dp";
      //frmMFSuitabilityTest1.flxStep4.height = "31dp";
    }
  }
}

function onClickMFSuitSubmitQuestion(){
  MFCalcSuitabilityService();
}

function getMFSuitibilityReviewDetails(){
  showLoadingScreen();
  var inputParam = {};
  if(GBL_MF_TEMENOS_ENABLE_FLAG == "OFF"){
      inputParam["moduleType"] = "MFSuitabilityReview";
      inputParam["issuedIdentType"] = "CI";
      inputParam["unitHolderNo"] = "fromAcctSummary";
      invokeServiceSecureAsync("MFSuitabilityInq", inputParam, callBackGetMFSuitibilityReviewDetails);
  }else{
      invokeServiceSecureAsync("MFSuitabilityInqNew", inputParam, callBackGetMFSuitibilityReviewDetails);
  }   
}

function callBackGetMFSuitibilityReviewDetails(status, resulttable){ //mki, mib-11469/11470 start
  if (status == 400) {
    if (resulttable["opstatus"] == 0) {
      if (resulttable["responseCode"] == "001") {
        dismissLoadingScreen();
        showAlert(resulttable["responseDesc"], kony.i18n.getLocalizedString("info"));
        return false;
      }else{
        var suitabilityScore = resulttable["suitabilityScore"];
        var expireDate = resulttable["suitabilityExpireDate"];
        var createDate = resulttable["CreateDate"];
        var yyyyMMdd = expireDate.split("-");
        expireDate = yyyyMMdd[2] + "/" + yyyyMMdd[1] + "/" + yyyyMMdd[0];
        yyyyMMdd = createDate.split("-");
        createDate = yyyyMMdd[2] + "/" + yyyyMMdd[1] + "/" + yyyyMMdd[0];
        var expiryDays = getMFSuitabilityTestExpiryDays(expireDate);
        if(expiryDays == -1){
          getMFSuitibilityReviewExpiry(expireDate);
        }else if(expiryDays <= 90 && gblMFSuitAlertDays.indexOf(expiryDays.toFixed(0)) != -1){
          if(!gblOrderFlow) {
            getMFSuitibilityReviewExpiryWarning(expireDate);
          } else {
            MBcallGetFundList();
            return;
          }
        }else{
          if(gblOrderFlow) {
            MBcallGetFundList();
            return;
          } else {
            frmMFSuitabilityReview.btnHeaderBack.setVisibility(true);
            frmMFSuitabilityReview.flxFooter.setVisibility(false);
            frmMFSuitabilityReview.imgSuccess.setVisibility(false);
            frmMFSuitabilityReview.lblDateOfCreation.setVisibility(true);
            frmMFSuitabilityReview.lbMFSuitabilityTitle.text = kony.i18n.getLocalizedString("MF_ttl");
            frmMFSuitabilityReview.lblUrRiskAssessment.text = kony.i18n.getLocalizedString("MF_lbl_01");
            frmMFSuitabilityReview.lblLevelVal.text = suitabilityScore;
            var levelRiskVal = kony.i18n.getLocalizedString("MF_lbl_02");
            levelRiskVal = levelRiskVal.replace("{x}", suitabilityScore);
            levelRiskVal = levelRiskVal.replace("{y}", "5");
            frmMFSuitabilityReview.lblRiskLevel.text = levelRiskVal;
            frmMFSuitabilityReview.lblDateOfCreation.text = kony.i18n.getLocalizedString("MF_lbl_SuitabilityDate") + ": "+createDate;
            frmMFSuitabilityReview.lblTypeOfInvest.text = kony.i18n.getLocalizedString("MF_lbl_InvestorType");
            if(kony.i18n.getCurrentLocale() == "en_US"){
              frmMFSuitabilityReview.lblTypeOfInvestVal.text = resulttable["InvestorTypeEN"];
            }else{
              frmMFSuitabilityReview.lblTypeOfInvestVal.text = resulttable["InvestorTypeTH"];
            }
            frmMFSuitabilityReview.lblTakeItAgain.text = kony.i18n.getLocalizedString("MF_lbl_Takeagain");
            frmMFSuitabilityReview.lblDateOfExpiry.text = kony.i18n.getLocalizedString("MF_lbl_ExpiredDate") + ": " +expireDate;
            gblMFSuitabilityFlow = false;
            frmMFSuitabilityReview.show();  
          }
        }
        dismissLoadingScreen();                           
      }
    }else{
      dismissLoadingScreen();
      //MBcallGetFundList();
      showAlert(kony.i18n.getLocalizedString("MF_P_ERR_01005"), kony.i18n.getLocalizedString("info"));
      return false;
     }
  }else{
      dismissLoadingScreen();
      //MBcallGetFundList();
      showAlert(kony.i18n.getLocalizedString("MF_P_ERR_01005"), kony.i18n.getLocalizedString("info"));
      return false;
  }
}  //mki, mib-11469/11470 End

function onClickCanCelMFSuitabilityTest1(){
  popupConfrmDelete.lblheader.text = kony.i18n.getLocalizedString("MF_ttl");
  popupConfrmDelete.btnpopConfDelete.text = kony.i18n.getLocalizedString("keyOK");
  popupConfrmDelete.btnPopupConfCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
  popupConfrmDelete.btnpopConfDelete.onClick = exitMFSuitabilityTest;
  popupConfrmDelete.lblPopupConfText.text = kony.i18n.getLocalizedString("MF_P_01001");
  popupConfrmDelete.show();
}

function exitMFSuitabilityTest(){
  popupConfrmDelete.dismiss();
  getMFSuitibilityReviewDetails();
}

function getMFSuitibilityReviewExpiry(expDate){
  frmMFSuitablilityExpiry.lbMFSuitabilityTitle.text = kony.i18n.getLocalizedString("MF_ttl");
  frmMFSuitablilityExpiry.lblUrRiskNearByExpired.text = kony.i18n.getLocalizedString("MF_lbl_MFSExpired");
  frmMFSuitablilityExpiry.lblDateOfExpiry.text = kony.i18n.getLocalizedString("MF_lbl_ExpiredDate") + ": "+expDate;
  frmMFSuitablilityExpiry.lblUpdateNow.text = kony.i18n.getLocalizedString("MF_lbl_UpdateNow");
  frmMFSuitablilityExpiry.lblUrRiskNearByExpired.skin = "lblRed48px";
  frmMFSuitablilityExpiry.lblDateOfExpiry.skin = "lblRed48px";
  frmMFSuitablilityExpiry.imgRiskLevel.src = "icon_notcomplete.png";
  frmMFSuitablilityExpiry.lblTakeItAgain.setVisibility(false);
  frmMFSuitablilityExpiry.show();
  dismissLoadingScreen();
}

function getMFSuitibilityReviewExpiryWarning(expDate){
  frmMFSuitablilityExpiry.lbMFSuitabilityTitle.text = kony.i18n.getLocalizedString("MF_ttl");
  frmMFSuitablilityExpiry.lblUrRiskNearByExpired.text = kony.i18n.getLocalizedString("MF_lbl_NearByExpired");
  frmMFSuitablilityExpiry.lblDateOfExpiry.text = kony.i18n.getLocalizedString("MF_lbl_ExpiredDate") + ": "+expDate;
  frmMFSuitablilityExpiry.lblUpdateNow.text = kony.i18n.getLocalizedString("MF_lbl_UpdateNow");
  frmMFSuitablilityExpiry.lblTakeItAgain.text = kony.i18n.getLocalizedString("MF_lbl_DoItLater");
  frmMFSuitablilityExpiry.lblUrRiskNearByExpired.skin = "lblGrey48px";
  frmMFSuitablilityExpiry.lblDateOfExpiry.skin = "lblGrey48px";
  frmMFSuitablilityExpiry.imgRiskLevel.src = "icon_alert.png";
  frmMFSuitablilityExpiry.lblTakeItAgain.setVisibility(true);
  frmMFSuitablilityExpiry.show();
  dismissLoadingScreen();
}

function MFSuitQuestionnaireService() {
  showLoadingScreen();
  var inputParam = {};
  if(GBL_MF_TEMENOS_ENABLE_FLAG == "OFF"){
  	inputParam["customerType"] = "P";
  	invokeServiceSecureAsync("SuitQuestionnaireETEInq", inputParam, callBackMFSuitQuestionnaireServiceOld);
  }else{
     invokeServiceSecureAsync("SuitQuestionnaireENTH", inputParam, callBackMFSuitQuestionnaireService);
   }
}

function callBackMFSuitQuestionnaireServiceOld(status, resulttable){
  gblMFSuitQuestions = [];
  if (status == 400) {
    if (resulttable["opstatus"] == 0) {
      var questionList = resulttable["questionGrp1DS"];
      if(questionList != null){
        for(var i=0; i < questionList.length; i++){
          var tempRecord = { 
            "questionEN" : questionList[i]["questionEN"], 
            "questionTH" : questionList[i]["questionTH"],    
            "answerOneEN" : questionList[i]["answerENDS"][0]["answer1"],    
            "answerTwoEN" : questionList[i]["answerENDS"][0]["answer2"],
            "answerThreeEN" : questionList[i]["answerENDS"][0]["answer3"],
            "answerFourEN" : questionList[i]["answerENDS"][0]["answer4"],
            "answerOneTH" : questionList[i]["answerTHDS"][0]["answer1"],   
            "answerTwoTH" : questionList[i]["answerTHDS"][0]["answer2"],    
            "answerThreeTH" : questionList[i]["answerTHDS"][0]["answer3"],
            "answerFourTH" : questionList[i]["answerTHDS"][0]["answer4"]
          };
          gblMFSuitQuestions.push(tempRecord);
        }

        questionList = resulttable["questionGrp2DS"];
        if(questionList != null){
          for(var i=0; i < questionList.length; i++){
            var tempRecord = { 
              "questionEN" : questionList[i]["questionGrp2EN"], 
              "questionTH" : questionList[i]["questionGrp2TH"],    
              "answerOneEN" : questionList[i]["answerENGrp2DS"][0]["answer1Grp2"],    
              "answerTwoEN" : questionList[i]["answerENGrp2DS"][0]["answer2Grp2"],
              "answerThreeEN" : "",
              "answerFourEN" : "",
              "answerOneTH" : questionList[i]["answerTHGrp2DS"][0]["answer1Grp2"],   
              "answerTwoTH" : questionList[i]["answerTHGrp2DS"][0]["answer2Grp2"],    
              "answerThreeTH" : "",
              "answerFourTH" : ""
            };
            gblMFSuitQuestions.push(tempRecord);
          }
        }

        if(gblMFSuitQuestions.length < maxNoOfQuestion){
          showAlert("Question No. <"+maxNoOfQuestion, kony.i18n.getLocalizedString("info"));
          return false;
        }
        resetMFSuitQuestionToBeDefault();
        setMFSuitQuestionAndAnswerDetail();
        frmMFSuitabilityTest1.show();
      }else{
        showAlert("No question.", kony.i18n.getLocalizedString("info"));
        return false;
      }
      dismissLoadingScreen();
    }else{
      dismissLoadingScreen();
      showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
      return false;
    }
  }else{
    dismissLoadingScreen();
    showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
    return false;
  }
}

function callBackMFSuitQuestionnaireService(status, resulttable){
  try{
    kony.print("@@@ In callBackMFSuitQuestionnaireService() @@@");
    kony.print("@@@@resulttable::::"+JSON.stringify(resulttable));
    gblMFSuitQuestions = [];
    if (status == 400) {
      if (resulttable["opstatus"] == 0) {
        var questionList = resulttable
        var engQuestionList = questionList["questionsENDS"];
        var thQuestionList = questionList["questionsTHDS"];
        
        kony.print("@@@EngQLeng::"+engQuestionList.length);
        kony.print("@@@ThaQLeng::"+thQuestionList.length);
        maxNoOfQuestion = engQuestionList.length;
        if(engQuestionList != null && engQuestionList != undefined && 
           thQuestionList != null && thQuestionList != undefined && engQuestionList.length == thQuestionList.length){
          for(var i=0; i < engQuestionList.length; i++){
            var quesEN="", quesTH="";
            var ansEN1="", ansEN2="", ansEN3="", ansEN4="", ansEN5="", ansEN6="";
            var ansTH1="", ansTH2="", ansTH3="", ansTH4="", ansTH5="", ansTH6="";
            var ansENName1="", ansENName2="", ansENName3="", ansENName4="", ansENName5="", ansENName6="";
            var ansTHName1="", ansTHName2="", ansTHName3="", ansTHName4="", ansTHName5="", ansTHName6="";
            var ansENVal1="", ansENVal2="", ansENVal3="", ansENVal4="", ansENVal5="", ansENVal6="";
            var ansTHVal1="", ansTHVal2="", ansTHVal3="", ansTHVal4="", ansTHVal5="", ansTHVal6="";
            
            if(isNotBlank(engQuestionList[i]["answerDS"][0])){
              ansEN1 = engQuestionList[i]["answerDS"][0]["answerLabel"];
              ansENName1 = engQuestionList[i]["answerDS"][0]["answerName"];
              ansENVal1 = engQuestionList[i]["answerDS"][0]["answerValue"];
            }
            if(isNotBlank(engQuestionList[i]["answerDS"][1])){
              ansEN2 = engQuestionList[i]["answerDS"][1]["answerLabel"];
              ansENName2 = engQuestionList[i]["answerDS"][1]["answerName"];
              ansENVal2 = engQuestionList[i]["answerDS"][1]["answerValue"];
            }
            if(isNotBlank(engQuestionList[i]["answerDS"][2])){
              ansEN3 = engQuestionList[i]["answerDS"][2]["answerLabel"];
              ansENName3 = engQuestionList[i]["answerDS"][2]["answerName"];
              ansENVal3 = engQuestionList[i]["answerDS"][2]["answerValue"];
            }
            if(isNotBlank(engQuestionList[i]["answerDS"][3])){
              ansEN4 = engQuestionList[i]["answerDS"][3]["answerLabel"];
              ansENName4 = engQuestionList[i]["answerDS"][3]["answerName"];
              ansENVal4 = engQuestionList[i]["answerDS"][3]["answerValue"];
            }
            if(isNotBlank(engQuestionList[i]["answerDS"][4])){
              ansEN5 = engQuestionList[i]["answerDS"][4]["answerLabel"];
              ansENName5 = engQuestionList[i]["answerDS"][4]["answerName"];
              ansENVal5 = engQuestionList[i]["answerDS"][4]["answerValue"];
            }
            if(isNotBlank(engQuestionList[i]["answerDS"][5])){
              ansEN6 = engQuestionList[i]["answerDS"][5]["answerLabel"];
              ansENName6 = engQuestionList[i]["answerDS"][5]["answerName"];
              ansENVal6 = engQuestionList[i]["answerDS"][5]["answerValue"];
            }
            if(isNotBlank(thQuestionList[i]["answerDS"][0])){
              ansTH1 = thQuestionList[i]["answerDS"][0]["answerLabel"];
              ansTHName1 = thQuestionList[i]["answerDS"][0]["answerName"];
              ansTHVal1 = thQuestionList[i]["answerDS"][0]["answerValue"];
            }
            if(isNotBlank(thQuestionList[i]["answerDS"][1])){
              ansTH2 = thQuestionList[i]["answerDS"][1]["answerLabel"];
              ansTHName2 = thQuestionList[i]["answerDS"][1]["answerName"];
              ansTHVal2 = thQuestionList[i]["answerDS"][1]["answerValue"];
            }
            if(isNotBlank(thQuestionList[i]["answerDS"][2])){
              ansTH3 = thQuestionList[i]["answerDS"][2]["answerLabel"];
              ansTHName3 = thQuestionList[i]["answerDS"][2]["answerName"];
              ansTHVal3 = thQuestionList[i]["answerDS"][2]["answerValue"];
            }
            if(isNotBlank(thQuestionList[i]["answerDS"][3])){
              ansTH4 = thQuestionList[i]["answerDS"][3]["answerLabel"];
              ansTHName4 = thQuestionList[i]["answerDS"][3]["answerName"];
              ansTHVal4 = thQuestionList[i]["answerDS"][3]["answerValue"];
            }
             if(isNotBlank(thQuestionList[i]["answerDS"][4])){
              ansTH5 = thQuestionList[i]["answerDS"][4]["answerLabel"];
              ansTHName5 = thQuestionList[i]["answerDS"][4]["answerName"];
              ansTHVal5 = thQuestionList[i]["answerDS"][4]["answerValue"];
            }
             if(isNotBlank(thQuestionList[i]["answerDS"][5])){
              ansTH6 = thQuestionList[i]["answerDS"][5]["answerLabel"];
              ansTHName6 = thQuestionList[i]["answerDS"][5]["answerName"];
              ansTHVal6 = thQuestionList[i]["answerDS"][5]["answerValue"];
            }
            
            var tempRecord = { 
              "questionEN": engQuestionList[i]["questionLabel"], 
              "questionTH": thQuestionList[i]["questionLabel"],
              
              "answerOneEN": ansEN1,
              "ansenname1": ansENName1,
              "ansenVal1": ansENVal1,
              
              "answerTwoEN": ansEN2,
              "ansenname2": ansENName2,
              "ansenVal2": ansENVal2,
              
              "answerThreeEN": ansEN3,
              "ansenname3": ansENName3,
              "ansenVal3": ansENVal3,
              
              "answerFourEN": ansEN4,
              "ansenname4": ansENName4,
              "ansenVal4": ansENVal4,
              
              "answerFiveEN": ansEN5,
              "ansenname5": ansENName5,
              "ansenVal5": ansENVal5,
              
              "answerSixEN": ansEN6,
              "ansenname6": ansENName6,
              "ansenVal6": ansENVal6,
              
              "answerOneTH": ansTH1,
              "ansthname1": ansTHName1,
              "ansthVal1": ansTHVal1,
              
              "answerTwoTH": ansTH2,
              "ansthname2": ansTHName2,
              "ansthVal2": ansTHVal2,
              
              "answerThreeTH": ansTH3,
              "ansthname3": ansTHName3,
              "ansthVal3": ansTHVal3,
              
              "answerFourTH": ansTH4,
              "ansthname4": ansTHName4,
              "ansthVal4": ansTHVal4,
              
              "answerFiveTH": ansTH5,
              "ansthname5": ansTHName5,
              "ansthVal5": ansTHVal5,
              
              "answerSixTH": ansTH6,
              "ansthname5": ansTHName6,
              "ansthVal6": ansTHVal6
            };
           
            gblMFSuitQuestions.push(tempRecord);
          }
          kony.print("@@@gblMFSuitQuestions:::"+gblMFSuitQuestions.length);
          kony.print("gblMFSuitQuestions>>>>>::"+gblMFSuitQuestions);

          if(gblMFSuitQuestions.length < maxNoOfQuestion){
            showAlert("Question No. < "+maxNoOfQuestion, kony.i18n.getLocalizedString("info"));
            return false;
          }
          resetMFSuitQuestionToBeDefault();
          setMFSuitQuestionAndAnswerDetail();
          frmMFSuitabilityTest1.show();
        }else{
          showAlert("No question.", kony.i18n.getLocalizedString("info"));
          return false;
        }
        dismissLoadingScreen();
      }else{
        dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        return false;
      }
    }else{
      dismissLoadingScreen();
      showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
      return false;
    }
  }catch(exp){
    kony.print("@@@Exceptoin:::"+exp);
    dismissLoadingScreen();
  }
}

function MFCalcSuitabilityService() {
  showLoadingScreen();
  var inputParam = {};
  if(GBL_MF_TEMENOS_ENABLE_FLAG == "OFF"){
        inputParam["documentID"] = gblCustomerIDValue;
        if (kony.string.isNumeric(gblCustomerIDValue) && gblCustomerIDValue.length == 13) {
          inputParam["documentType"] = "CI" // Citizen
        } else {
          inputParam["documentType"] = "PP" // Passport
        }
        //MIB-9990 - Mutual funds cannot update suitability - Send Unit Holder from backend
       /* var data = frmMFSummary.segAccountDetails.data[1].lblunitHolderNumber;
        var unitholder = data.replace(/-/gi, "");
        inputParam["unitHolderNo"] = unitholder; //"110203000296";
        */
        inputParam["customerName"] = gblCustomerName; 
        inputParam["customerType"] = "P";
        inputParam["accountType"] = "S";
        inputParam["branchNo"] = "9882";
        inputParam["answers"] = gblMFSuitAnswers.toString();
        inputParam["fxAnswer"] = gblMFfxAnswer;
        inputParam["channelBr"] = "MIB-MF";
        invokeServiceSecureAsync("CalcSuitabilityETEInq", inputParam, callBackMFCalcSuitabilityService)
  }else{
      var mfanswers = getMFSiutAnswerNames();
      inputParam["documentID"] = gblCustomerIDValue;
      if (kony.string.isNumeric(gblCustomerIDValue) && gblCustomerIDValue.length == 13) {
        inputParam["documentType"] = "CI" // Citizen
      } else {
        inputParam["documentType"] = "PP" // Passport
      }
      //MIB-9990 - Mutual funds cannot update suitability - Send Unit Holder from backend
     /* var data = frmMFSummary.segAccountDetails.data[1].lblunitHolderNumber;
      var unitholder = data.replace(/-/gi, "");
      inputParam["unitHolderNo"] = unitholder; //"110203000296";
      */
      var fixans="";
      //if(gblMFfxAnswer == "1") fixans ="Yes"; else fixans ="No";
      inputParam["customerName"] = gblCustomerName; 
      inputParam["customerType"] = "P";
      inputParam["accountType"] = "S";
      inputParam["branchNo"] = "9882";
      inputParam["answers"] = mfanswers.toString(); //gblMFSuitAnswers.toString();
      //inputParam["fxAnswer"] = fixans;
      inputParam["channelBr"] = "MIB-MF";
      inputParam["answersActLog"]=gblMFSuitAnswers.toString();
      kony.print("@@@Inputparams::::"+JSON.stringify(inputParam));
      invokeServiceSecureAsync("CalcSuitabilityETEInqNew", inputParam, callBackMFCalcSuitabilityService)
  }
}

function callBackMFCalcSuitabilityService(status, resulttable) {
  if (status == 400) {
    if (resulttable["opstatus"] == 0) {
      var riskLevel = resulttable["RiskLevel"];
      var expireDate = resulttable["ExpireDate"];
      var yyyyMMdd = expireDate.split("-");
      expireDate = yyyyMMdd[2] + "/" + yyyyMMdd[1] + "/" + yyyyMMdd[0];
      var investorType = "";
      if(kony.i18n.getCurrentLocale() == "en_US"){
        investorType = resulttable["InvestorTypeEN"];
      }else{
        investorType = resulttable["InvestorTypeTH"];
      }
      frmMFSuitabilityReview.lbMFSuitabilityTitle.text = kony.i18n.getLocalizedString("Complete");
      frmMFSuitabilityReview.lblUrRiskAssessment.text = kony.i18n.getLocalizedString("MF_lbl_01");
      var levelRiskVal = kony.i18n.getLocalizedString("MF_lbl_02");
      levelRiskVal = levelRiskVal.replace("{x}", riskLevel);
      levelRiskVal = levelRiskVal.replace("{y}", "5");
      frmMFSuitabilityReview.lblRiskLevel.text = levelRiskVal;
      frmMFSuitabilityReview.lblLevelVal.text = riskLevel;
      frmMFSuitabilityReview.lblDateOfCreation.setVisibility(false);
      frmMFSuitabilityReview.btnHeaderBack.setVisibility(false);
      frmMFSuitabilityReview.imgSuccess.setVisibility(true);
      frmMFSuitabilityReview.flxFooter.setVisibility(true);
      frmMFSuitabilityReview.lblTypeOfInvest.text = kony.i18n.getLocalizedString("MF_lbl_InvestorType");
      frmMFSuitabilityReview.lblTypeOfInvestVal.text = investorType;
      frmMFSuitabilityReview.lblTakeItAgain.text = kony.i18n.getLocalizedString("MF_lbl_Takeagain");
      frmMFSuitabilityReview.lblDateOfExpiry.text = kony.i18n.getLocalizedString("MF_lbl_ExpiredDate") + ": " +expireDate;
      frmMFSuitabilityReview.lblGoToPort.text = kony.i18n.getLocalizedString("MF_btn_portfolio");
      gblMFSuitabilityFlow = false;
      frmMFSuitabilityReview.show();
      dismissLoadingScreen();
    }else{
      dismissLoadingScreen();
      showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
      return false;
    }
  }else{
    dismissLoadingScreen();
    showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
    return false;
  }
}

// for Terms & COnditions
function loadMFSuitabilityTermsNConditions(){
  var input_param = {};
  var locale = kony.i18n.getCurrentLocale();
  if (locale == "en_US") {
    input_param["localeCd"]="en_US";
  }else{
    input_param["localeCd"]="th_TH";
  }	
  var prodDescTnC = "TMBMFSuitabilityTnC";	
  input_param["moduleKey"]= prodDescTnC;	
  showLoadingScreen();	
  invokeServiceSecureAsync("readUTFFile", input_param, loadMFSuitabilityTNCBackMB);
}

/**
 * @function
 *
 */
function loadMFSuitabilityTNCBackMB(status, result){
  if(status == 400){
    if(result["opstatus"] == 0){
      frmMFSuitabilityTnC.richtextTnC.text = result["fileContent"]; // current 
      //frmMFSuitabilityTnC.lbMFSuitabilityTitle.setFocus(true);
      dismissLoadingScreen();
      frmMFSuitabilityTnC.show(); 
    }else{	
      dismissLoadingScreen();
      showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
      return false;
    }
  }

}

function getMFSuitabilityTestExpiryDays(expDate){
  var currentdate = new Date();
  var today = new Date(currentdate.getFullYear(), currentdate.getMonth(), currentdate.getDate());
  var endDate = parseDate(expDate);
  if(today.getTime() > endDate.getTime()) {
    return -1; // expired
  }
  return daydiff(today, endDate); // + current date 
}

/**
 * @function
 *
 */
function onClickCancelfrmMFSuitabilityTnC(){
  if(gblMFSuitabilityFlow){
    gblMFSuitabilityFlow = false;
    frmMFSuitablilityExpiry.show();    
  }else{
     frmMFSuitabilityReview.show();
  }
}

/**
 * @function
 *
 */
function preshowfrmMFSuitabilityTnC(){
  frmMFSuitabilityTnC.lbMFSuitabilityTitle.text = kony.i18n.getLocalizedString("MF_ttl_terms");
  frmMFSuitabilityTnC.lblCashAdvanceDescSubTitle.text = kony.i18n.getLocalizedString("MF_Obj_Title");
  frmMFSuitabilityTnC.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton"); //TO-DO - Implement this Key MF_lbl_cancel
  frmMFSuitabilityTnC.btnNext.text = kony.i18n.getLocalizedString("MF_RDM_34");
}

function checkMutualFundServiceStatus(){
  try{
    checkSuitabilityExpireMB();
     //Commented below code to get the error message from service
  /*  if(isNotBlank(gblMutualFundService)){
      if(gblMutualFundService=="ON"||gblMutualFundService=="on"){
        checkSuitabilityExpireMB();
      }else if(gblMutualFundService=="WARN"||gblMutualFundService=="warn"){
            showAlertMFWarn();
      }else if(gblMutualFundService=="OFF"||gblMutualFundService=="off"){
        	showAlertMFoff();
      }
    }else{
      //do nothing
    } */
  }catch(e){
    //do nothing
  }
}

function checkSuitabilityExpireMB(){
  try{
    kony.print("IN checkSuitabilityExpireMB ");
  var inputParam = {};
  if(GBL_MF_TEMENOS_ENABLE_FLAG == "OFF"){
      inputParam["moduleType"] = "MFSuitabilityReview";
      inputParam["issuedIdentType"] = "CI";
      inputParam["unitHolderNo"] = "fromAcctSummary";
      invokeServiceSecureAsync("MFSuitabilityInq", inputParam, callBackCheckSuitabilityExpireMB);
  }else{
    invokeServiceSecureAsync("MFSuitabilityInqNew", inputParam, callBackCheckSuitabilityExpireMB);
  }
    }catch(e){
      kony.print("checkSuitabilityExpireMB e "+e);
   }
}

function callBackCheckSuitabilityExpireMB(status, resulttable){  //mki, mib-11469/11470 start
  if (status == 400) {
    // kony.print("resulttable in callBackCheckSuitabilityExpireMB");
     kony.print("resulttable in callBackCheckSuitabilityExpireMB   "+JSON.stringify(resulttable) ) 
     
     kony.print("resulttable in callBackCheckSuitabilityExpireMB gblMFEventFlag is " + gblMFEventFlag + " MF_EVENT_SWO " +MF_EVENT_SWO);
    
    if (resulttable["opstatus"] == 999) {
      dismissLoadingScreen();
      if(kony.i18n.getCurrentLocale() == "en_US")
         showAlertMFoff(resulttable["errMsgEn"]);
      else 
         showAlertMFoff(resulttable["errMsgTH"]);     
      return false;
    }
    
    if (resulttable["opstatus"] == 0) {
      if (resulttable["responseCode"] == "001") {
          dismissLoadingScreen();
          showAlert(resulttable["responseDesc"], kony.i18n.getLocalizedString("info"));
          return false;
      }else{
          var suitabilityScore = resulttable["suitabilityScore"];
        gblMFSuitabilityScore = suitabilityScore;
        kony.print("suitabilityScore NIk+ " +suitabilityScore)
          var expireDate = resulttable["suitabilityExpireDate"];
          var createDate = resulttable["CreateDate"];
          var yyyyMMdd = expireDate.split("-");
          expireDate = yyyyMMdd[2] + "/" + yyyyMMdd[1] + "/" + yyyyMMdd[0];
          yyyyMMdd = createDate.split("-");
          createDate = yyyyMMdd[2] + "/" + yyyyMMdd[1] + "/" + yyyyMMdd[0];
          var expiryDays = getMFSuitabilityTestExpiryDays(expireDate);
        kony.print("expiryDays  "+expiryDays ) 
          if(expiryDays == -1){
                getMFSuitibilityReviewExpiry(expireDate);
          }else if(expiryDays <= 90 && gblMFSuitAlertDays.indexOf(expiryDays.toFixed(0)) != -1){
                getMFSuitibilityReviewExpiryWarning(expireDate);
          }else if(gblMFEventFlag == MF_EVENT_SWO){
           
                frmMFSwitchLanding.show();
          }else{
          //  	gblMFPortfolioType = "PT"; // for  Normal Portfolio 
            	gblMFPortfolioType = "OW"  //Temporary
            	frmMFSummary.btnMFFundPortflio.skin = "btnWhiteBGMF";//"btnBlueBGMF";
  				frmMFSummary.btnMFAutoSmartPortfolio.skin = "btnWhiteBGMF";
            	frmMFSummary.btnMFOverview.skin = "btnBlueBGMF";
                MBcallMutualFundsSummary();
          }
          dismissLoadingScreen();                         
      }
    }else{
                dismissLoadingScreen();
      		//	gblMFPortfolioType = "PT"; // for  Normal Portfolio 
      			gblMFPortfolioType = "OW"  //Temporary
            	frmMFSummary.btnMFFundPortflio.skin = "btnWhiteBGMF";//"btnBlueBGMF";
  				frmMFSummary.btnMFAutoSmartPortfolio.skin = "btnWhiteBGMF";
            	frmMFSummary.btnMFOverview.skin = "btnBlueBGMF";
                MBcallMutualFundsSummary();
    }
  }else{
    dismissLoadingScreen();
   // gblMFPortfolioType = "PT";   // for  Normal Portfolio  
    gblMFPortfolioType = "OW"  //Temporary
            	frmMFSummary.btnMFFundPortflio.skin = "btnWhiteBGMF";//"btnBlueBGMF";
  				frmMFSummary.btnMFAutoSmartPortfolio.skin = "btnWhiteBGMF";
            	frmMFSummary.btnMFOverview.skin = "btnBlueBGMF";
    MBcallMutualFundsSummary();
  }
}  //mki, mib-11469/11470 start End

function showAlertMFWarn() {
  	var KeyTitle = "Info"//kony.i18n.getLocalizedString("keyOK");
   // var keyMsg = kony.i18n.getLocalizedString("keyTouchMessage");
    var okk = kony.i18n.getLocalizedString("keyOK");
    
	//Defining basicConf parameter for alert
  	var keyMsg = kony.i18n.getLocalizedString("Mutual_Fund_Service_Warn");
	var basicConf = {
		message: keyMsg,
		alertType: constants.ALERT_TYPE_INFO,
		alertTitle: KeyTitle,
		yesLabel: okk,
		noLabel: "",
		alertHandler: checkSuitabilityExpireMB
	};
	//Defining pspConf parameter for alert
	var pspConf = {};
	//Alert definition
	var infoAlert = kony.ui.Alert(basicConf, pspConf);
    function handleok(response){}
    
	return;
}

function showAlertMFoff(keyMsg) {
  	var KeyTitle = "Info"//kony.i18n.getLocalizedString("keyOK");
   // var keyMsg = kony.i18n.getLocalizedString("keyTouchMessage");
    var okk = kony.i18n.getLocalizedString("keyOK");
    
	//Defining basicConf parameter for alert
  //	var keyMsg = kony.i18n.getLocalizedString("Mutual_Fund_Service_Off");
	var basicConf = {
		message: keyMsg,
		alertType: constants.ALERT_TYPE_INFO,
		alertTitle: KeyTitle,
		yesLabel: okk,
		noLabel: "",
		alertHandler: handleok
	};
	//Defining pspConf parameter for alert
	var pspConf = {};
	//Alert definition
	var infoAlert = kony.ui.Alert(basicConf, pspConf);
    function handleok(response){}
    
	return;
}
