//Type your code here
gblENotificationAccounts = [];
swithOffNotificationsAllAccounts = false;

function menuENotificationonClick() {
  	try{
    var deviceNotificationStatus = arePushNotificationsEnabled();
    gblENotificationAccounts = [];
    if (deviceNotificationStatus == true) {
        showENotificationSettings();
    } else {
        frmENotificationLanding.show();
    }
    }catch (e) {
        kony.print("Error in Enotification"+e);
      	kony.print("Error in Enotification"+e.message);
    }
}

function frmENotificationLandingInit() {
    frmENotificationLanding.btnBack.onClick = showAccountSummaryFromMenu;
    frmENotificationLanding.btnLater.onClick = showAccountSummaryFromMenu;
    frmENotificationLanding.btnSetupNow.onClick = showENotificationSettings;
    frmENotificationLanding.onDeviceBack = doNothing;
    frmENotificationLanding.preShow  = frmENotificationLandingPreshow;
}

function frmENotificationLandingPreshow(){
  frmENotificationLanding.lblNotificationDesc.text = kony.i18n.getLocalizedString("NST_txtDes");
}

function hideSettingsPopup() {
    frmENotificationSettings.flexSettings.isVisible = false;
    frmENotificationSettings.btnNotifications.skin = btnUnCheckedImage;
    frmENotificationSettings.switchNotifications.selectedIndex = 1;
}

function frmENotificationSettingsInit() {
    frmENotificationSettings.preShow = frmENotificationSettingsPreshow;
    frmENotificationSettings.onDeviceBack = doNothing;
    frmENotificationSettings.btnBack.onClick = onClickBackfrmENotificationSettings;
    frmENotificationSettings.flexAccounts.onClick = showENotificationAccounts;
    frmENotificationSettings.btnSetings.onClick = navigatetoPushNotificationSettings;
    frmENotificationSettings.btnSettingCancel.onClick = hideSettingsPopup;
    
    //#ifdef iphone
  	frmENotificationSettings.switchNews.onSlide = newsOnOff;
    frmENotificationSettings.switchNotifications.onSlide = notificationsOnOff;
   //#endif
    frmENotificationSettings.btnNotifications.onClick = notificationsOnOff;
    frmENotificationSettings.btnNews.onClick = newsOnOff;
}

function onClickBackfrmENotificationSettings() {
    var prevForm = kony.application.getPreviousForm().id;
    kony.print("prevForm-->" + prevForm)
    if (prevForm == "frmENotificationLanding") {
        frmENotificationLanding.show();
    } else {
        showAccountSummaryFromMenu();
    }

}

function frmENotificationSettingsPreshow() {
  	try{
    //#ifdef android
    frmENotificationSettings.switchNotifications.isVisible = false;
    frmENotificationSettings.switchNews.isVisible = false;
    //#endif
    //#ifdef iphone
    frmENotificationSettings.btnNotifications.isVisible = false;
    frmENotificationSettings.btnNews.isVisible = false;
    //#endif
    frmENotificationAccountSelection.lblwifiinformation.text = kony.i18n.getLocalizedString("NST_txtPUdesc");
      }catch (e) {
        kony.print("Error in Enotification"+e);
      	kony.print("Error in Enotification"+e.message);
    }
}

function frmENotificationAccountSelectionInit() {
    frmENotificationAccountSelection.onDeviceBack = doNothing;
    frmENotificationAccountSelection.preShow = frmENotificationAccountSelectionPreshow;
    frmENotificationAccountSelection.btnSave.onClick = updateENotificationData;
    frmENotificationAccountSelection.btnOwnAccount.onClick = selectNotificationsToOwnAccounts;
    frmENotificationAccountSelection.switchOwnAccount.onClick = selectNotificationsToOwnAccounts;
    frmENotificationAccountSelection.btnBackArrow.onClick = function() {
        frmENotificationSettings.show()
    };
}

function selectNotificationsToOwnAccounts() {
    if (frmENotificationAccountSelection.btnOwnAccount.skin == btnUnCheckedImage) {
        frmENotificationAccountSelection.btnOwnAccount.skin = btnCheckedImage;
    } else {
        frmENotificationAccountSelection.btnOwnAccount.skin = btnUnCheckedImage
    }
   /* if (frmENotificationAccountSelection.switchOwnAccount.selectedIndex == 1) {
        frmENotificationAccountSelection.switchOwnAccount.selectedIndex = 0;
    } else {
        frmENotificationAccountSelection.switchOwnAccount.selectedIndex = 1
    } */
}

function frmENotificationAccountSelectionPreshow() {
  	try{
    frmENotificationAccountSelection.btnOwnAccount.skin = btnUnCheckedImage;
    frmENotificationAccountSelection.switchOwnAccount.selectedIndex = 1;
  	enableDisableSaveButton();
    //#ifdef android
    frmENotificationAccountSelection.switchOwnAccount.isVisible = false;
    //#endif
    //#ifdef iphone
    frmENotificationAccountSelection.btnOwnAccount.isVisible = false;
    //#endif
    frmENotificationAccountSelection.lblNewsDesc.text = kony.i18n.getLocalizedString("NST_txtOwnDes");
    
    if (gblENotificationAccounts["accountsdata"].length > 0) {
        for (var i = 0; i < gblENotificationAccounts["accountsdata"].length; i++) {
            if (gblENotificationAccounts["accountsdata"][i]["accountNotification"] == "true") {
               if(gblENotificationAccounts["transfer_to_your_account"] == "true"){
                  frmENotificationAccountSelection.btnOwnAccount.skin = btnCheckedImage;
                  frmENotificationAccountSelection.switchOwnAccount.selectedIndex = 0;
                }else{
                  frmENotificationAccountSelection.btnOwnAccount.skin = btnUnCheckedImage;
                  frmENotificationAccountSelection.switchOwnAccount.selectedIndex = 1;
                }
                break;
            }
        }
    }
      }catch (e) {
        kony.print("Error in Enotification"+e);
      	kony.print("Error in Enotification"+e.message);
    }
}

function showENotificationSettings() {
    var input_param = {}
    showLoadingScreen();

    invokeServiceSecureAsync("notificationInquiry", input_param, notificationInquiryCallback);

}

function notificationInquiryCallback(status, result) {
  try{
    dismissLoadingScreen();
    if (status == 400) {
        if (result["opstatus"] == 0) {
            gblENotificationAccounts = [];
            gblENotificationAccounts = result;
            var notifcationStatus = result["notification"];
            var newsStatus = result["tmb_news"];
            var deviceNotificationStatus = arePushNotificationsEnabled();
            if (deviceNotificationStatus == true && notifcationStatus == "true") {
                frmENotificationSettings.switchNotifications.selectedIndex = 0;
                frmENotificationSettings.btnNotifications.skin = btnCheckedImage;
                frmENotificationSettings.flexNotificationSettings.isVisible = true;
            } else if (deviceNotificationStatus == false && notifcationStatus == "true") {
                frmENotificationSettings.switchNotifications.selectedIndex = 1;
                frmENotificationSettings.btnNotifications.skin = btnUnCheckedImage;
                frmENotificationSettings.flexNotificationSettings.isVisible = false;
              	frmENotificationSettings.switchNews.selectedIndex = 1;
            } else if (deviceNotificationStatus == true && notifcationStatus == "false") {
                kony.print("3rd condition")
                frmENotificationSettings.switchNotifications.selectedIndex = 1;
                frmENotificationSettings.btnNotifications.skin = btnUnCheckedImage;
                frmENotificationSettings.flexNotificationSettings.isVisible = false;
              	frmENotificationSettings.switchNews.selectedIndex = 1;
            } else if ( deviceNotificationStatus == false && notifcationStatus ==  "false") {
                frmENotificationSettings.switchNotifications.selectedIndex = 1;
                frmENotificationSettings.btnNotifications.skin = btnUnCheckedImage;
                frmENotificationSettings.flexNotificationSettings.isVisible = false;
              	frmENotificationSettings.switchNews.selectedIndex = 1;
            }
            if (newsStatus == "true") {
                frmENotificationSettings.switchNews.selectedIndex = 0;
                frmENotificationSettings.btnNews.skin = btnCheckedImage;
                frmENotificationSettings.lblNewsDesc.isVisible = true;
      		    frmENotificationSettings.lblNewsHeader.height = "50%"
            } else {
                frmENotificationSettings.switchNews.selectedIndex = 1;
                frmENotificationSettings.btnNews.skin = btnUnCheckedImage;
                frmENotificationSettings.lblNewsDesc.isVisible = false;
        		frmENotificationSettings.lblNewsHeader.height = "100%"
            }
            var acctNickNameString = "";
			frmENotificationSettings.lblAccountNickNames.text = "";
			 kony.print("----->"+result["accountsdata"]);
          	kony.print("----->"+result["accountsdata"].length);
            for (var i = 0; i < result["accountsdata"].length; i++) {
              if(result["accountsdata"][i]["accountNotification"] == "true"){
                var acctNickName = result["accountsdata"][i]["acctNickName"];
                if (acctNickNameString.length >= 27) {
                    acctNickNameString = acctNickNameString + "...";
                    break;
                }
                if (acctNickNameString == "") {
                    acctNickNameString = acctNickName;
                } else {
                    acctNickNameString = acctNickNameString.substring(0, 27) + "," + acctNickName;
                }
            }
            }
            kony.print("length is --->"+acctNickNameString.length)
            if(acctNickNameString != ""){
              frmENotificationSettings.lblAccountAlertsHeader.height = "50%"
              frmENotificationSettings.lblAccountAlertsHeader.top = "10dp"
              frmENotificationSettings.lblAccountNickNames.text = acctNickNameString;
              frmENotificationSettings.lblAccountNickNames.isVisible = true;
            }else{
              frmENotificationSettings.lblAccountAlertsHeader.height = "100%"
              frmENotificationSettings.lblAccountNickNames.text = "";
              frmENotificationSettings.lblAccountNickNames.isVisible = false;
              frmENotificationSettings.lblAccountAlertsHeader.top = "0dp"
            }
            
            frmENotificationSettings.show();
        }else{
          showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
        }
    }
	}catch (e) {
        kony.print("Error in Enotification"+e);
      	kony.print("Error in Enotification"+e.message);
    }
}

function showENotificationAccounts() {
  try{
    var segmentData = [];
    var accounts = gblENotificationAccounts["accountsdata"];
    //kony.print("transfertoOwnaccount-->"+gblENotificationAccounts["transfertoOwnaccount"])
    if(gblENotificationAccounts["transfer_to_your_account"] == "true"){
      frmENotificationAccountSelection.btnOwnAccount.skin = btnCheckedImage;
      frmENotificationAccountSelection.switchOwnAccount.selectedIndex = 0;
    }else{
      frmENotificationAccountSelection.btnOwnAccount.skin = btnUnCheckedImage;
      frmENotificationAccountSelection.switchOwnAccount.selectedIndex = 1;
    }
    
    kony.print("accounts--->" + accounts)
    kony.print("accounts--->" + accounts.length)
    for (var i = 0; i < accounts.length; i++) {
        var obj = "";
        var btnSelectAccountSkin = btnUnCheckedImage;
        var switchSelectAccountIndex = 1;
        kony.print("accounts--->" + accounts[i]["accountNotification"])
        kony.print("account number--->" + accounts[i]["account"])
        if (accounts[i]["accountNotification"] == "true") {
            btnSelectAccountSkin = btnCheckedImage;
            switchSelectAccountIndex = 0
        }
       //#ifdef android
      		obj = {
            "lblAccountName": accounts[i]["acctNickName"],
          	"lblAccountNumber": accounts[i]["account"],
            "btnSelectAccount": {
                skin: btnSelectAccountSkin,
              	focusSkin: btnSelectAccountSkin,
                isVisible: true,
                onClick: OnOffAccountsNotification
            },
            "switchSelectAccount": {
                selectedIndex: switchSelectAccountIndex,
                isVisible: false,
              	 info: {
                    selectedIndex: switchSelectAccountIndex
                }
            }
        };
  		 //#endif
        //#ifdef iphone
      		obj = {
            "lblAccountName": accounts[i]["acctNickName"],
          	"lblAccountNumber": accounts[i]["account"],
            "btnSelectAccount": {
                isVisible: false
            },
            "switchSelectAccount": {
                selectedIndex: switchSelectAccountIndex,
                isVisible: true,
                info: {
                    selectedIndex: switchSelectAccountIndex
                },
                onSlide: OnOffAccountsNotification
            }
        };
  		 //#endif
        kony.print("accounts--->" + obj)
        segmentData.push(obj);
    }
    kony.print("accounts--->" + segmentData)
    frmENotificationAccountSelection.segAccounts.removeAll();
    frmENotificationAccountSelection.segAccounts.setData(segmentData)
    frmENotificationAccountSelection.show();
    }catch (e) {
        kony.print("Error in Enotification"+e);
      	kony.print("Error in Enotification"+e.message);
    }
}


function OnOffAccountsNotification(event, rowClicked) {
  try{
    kony.print("onclick is working fine")
    var accountSelectedIndex = frmENotificationAccountSelection.segAccounts.selectedIndex[1];
   kony.print("accountSelectedIndex--->"+accountSelectedIndex)
    var accountSelectedObj = frmENotificationAccountSelection.segAccounts.data[accountSelectedIndex];
    var btnCheckBoxSkin = btnUnCheckedImage,
        switchMobileValue = 1;
    kony.print("skin of the account-->"+accountSelectedObj.btnSelectAccount.skin)
    if ((accountSelectedObj.btnSelectAccount.skin == "btnUnCheckedImage" && isAndroid()) || (accountSelectedObj.switchSelectAccount.info["selectedIndex"] == 1 && !isAndroid())) {
        btnCheckBoxSkin = btnCheckedImage;
        switchMobileValue = 0;
    }
    //#ifdef android
    accountSelectedObj["btnSelectAccount"] = {
        "skin": btnCheckBoxSkin,
      	"focusSkin": btnCheckBoxSkin,
        onClick: OnOffAccountsNotification,
        "isVisible" : isAndroid()
    };
    //#endif
    //#ifdef iphone
    accountSelectedObj["switchSelectAccount"]["selectedIndex"] = switchMobileValue;
  	accountSelectedObj["switchSelectAccount"]["isVisible"] =  !isAndroid();
    accountSelectedObj["switchSelectAccount"]["info"]["selectedIndex"] = switchMobileValue;
    accountSelectedObj["switchSelectAccount"]["onSlide"] = OnOffAccountsNotification;
   //#endif
    frmENotificationAccountSelection.segAccounts.setDataAt(accountSelectedObj, accountSelectedIndex);
  	enableDisableSaveButton();
    }catch (e) {
        kony.print("Error in Enotification"+e);
      	kony.print("Error in Enotification"+e.message);
    }
}


function newsOnOff() {
  	 if (frmENotificationSettings.btnNews.skin == btnUnCheckedImage) {
        frmENotificationSettings.btnNews.skin = btnCheckedImage;
    } else {
        frmENotificationSettings.btnNews.skin = btnUnCheckedImage;
    }
  	kony.print("newsOnOff-->"+frmENotificationSettings.switchNews.selectedIndex);
   /* if (frmENotificationSettings.switchNews.selectedIndex == 1) {
        frmENotificationSettings.switchNews.selectedIndex = 0;
    } else {
        frmENotificationSettings.switchNews.selectedIndex = 1;
    } */
    updateENotificationData();
}

function notificationsOnOff() {
  try{
    var deviceNotificationStatus = arePushNotificationsEnabled();
  	swithOffNotificationsAllAccounts = false;  
     if (frmENotificationSettings.btnNotifications.skin == btnUnCheckedImage) {
        frmENotificationSettings.btnNotifications.skin = btnCheckedImage;
    } else {
        frmENotificationSettings.btnNotifications.skin = btnUnCheckedImage;
    }
   /* if (frmENotificationSettings.switchNotifications.selectedIndex == 1) {
        frmENotificationSettings.switchNotifications.selectedIndex = 0;
    } else {
        frmENotificationSettings.switchNotifications.selectedIndex = 1;
    }  */
    kony.print("skin---?" + frmENotificationSettings.btnNotifications.skin)
    kony.print("skin---?" + frmENotificationSettings.flexNotificationSettings.isVisible)
    if ((frmENotificationSettings.switchNotifications.selectedIndex == 1 && !isAndroid()) || (frmENotificationSettings.btnNotifications.skin == btnUnCheckedImage && isAndroid())) {
      swithOffNotificationsAllAccounts = true;  
      switchOffNotificationsAllAccounts();
      frmENotificationSettings.flexNotificationSettings.isVisible = false
    } else {
        if (frmENotificationSettings.flexNotificationSettings.isVisible == false) {
            if (deviceNotificationStatus == true) {
                kony.print("device true and now notification true")
              	updateENotificationData();
               // frmENotificationSettings.flexNotificationSettings.isVisible = true;
              	//showENotificationSettings();
            } else {
              	frmENotificationSettings.flexSettings.isVisible = true;
                
            }
        }
    }
    }catch (e) {
        kony.print("Error in Enotification"+e);
      	kony.print("Error in Enotification"+e.message);
    }
}

function goToPushSettingsFromENotification() {

}

function updateENotificationData() {
  try{
    var input_param = {}
    //#ifdef iphone
    kony.print("switch news  index"+frmENotificationSettings.switchNews.selectedIndex)
    if (frmENotificationSettings.switchNews.selectedIndex == 0) {
        input_param["tmb_news"] = "Y"
    } else {
        input_param["tmb_news"] = "N"
    }
    if (frmENotificationSettings.switchNotifications.selectedIndex == 0) {
        input_param["notification"] = "Y"
    } else {
        input_param["notification"] = "N"
    }
    if (frmENotificationAccountSelection.switchOwnAccount.selectedIndex == 0) {
        input_param["transfer_to_your_account"] = "Y"
    } else {
        input_param["transfer_to_your_account"] = "N"
    }
    //#endif
    //#ifdef android
    if (frmENotificationSettings.btnNews.skin == btnCheckedImage) {
        input_param["tmb_news"] = "Y"
    } else {
        input_param["tmb_news"] = "N"
    }
    if (frmENotificationSettings.btnNotifications.skin == btnCheckedImage) {
        input_param["notification"] = "Y"
    } else {
        input_param["notification"] = "N"
    }
    if (frmENotificationAccountSelection.btnOwnAccount.skin == btnCheckedImage) {
        input_param["transfer_to_your_account"] = "Y"
    } else {
        input_param["transfer_to_your_account"] = "N"
    }
    //#endif
  	kony.print("----->"+frmENotificationAccountSelection.segAccounts)
    var currForm = kony.application.getCurrentForm();
    if (frmENotificationAccountSelection.segAccounts != null && currForm == frmENotificationAccountSelection) {
        var segData = frmENotificationAccountSelection.segAccounts.data;
      	kony.print("----->"+segData)
        kony.print("gblENotificationAccounts--->"+gblENotificationAccounts)
        var accountsData = gblENotificationAccounts;
        for (var i = 0; i < segData.length; i++) {
            var selectedData = frmENotificationAccountSelection.segAccounts.data[i];
            kony.print("")
            accountsData["accountsdata"][i]["account"] = selectedData["lblAccountNumber"];
            //#ifdef android
            if (selectedData["btnSelectAccount"]["skin"] == "btnCheckedImage") {
              	if(swithOffNotificationsAllAccounts){
                  accountsData["accountsdata"][i]["accountNotification"] = "false";
                  
                }else{
                  accountsData["accountsdata"][i]["accountNotification"] = "true";
                }
                
            } else {
                accountsData["accountsdata"][i]["accountNotification"] = "false";
            }
            //#endif
            //#ifdef iphone
            if (selectedData["switchSelectAccount"]["selectedIndex"] == "0") {
                	if(swithOffNotificationsAllAccounts){
                  accountsData["accountsdata"][i]["accountNotification"] = "false";
                }else{
                  accountsData["accountsdata"][i]["accountNotification"] = "true";
                }
            } else {
                accountsData["accountsdata"][i]["accountNotification"] = "false";
            }
            //#endif
        }
        input_param["accounts"] = accountsData["accountsdata"];
       kony.print("----->"+input_param["accounts"]);
    }else if(input_param["notification"] == "N") {
      	for (var i = 0; i < gblENotificationAccounts["accountsdata"].length; i++) {
       		 gblENotificationAccounts["accountsdata"][i]["accountNotification"] = "false";
    }
  		  input_param["accounts"] = gblENotificationAccounts["accountsdata"];
    }else {
        input_param["accounts"] = gblENotificationAccounts["accountsdata"];
    }

    showLoadingScreen();
    if(swithOffNotificationsAllAccounts){
         input_param["tmb_news"] = "N";
      	 input_param["transfer_to_your_account"] = "N";
     }
    invokeServiceSecureAsync("notificationUpdate", input_param, notificationUpdateCallback);
    }catch (e) {
        kony.print("Error in Enotification"+e);
      	kony.print("Error in Enotification"+e.message);
    }
}


function notificationUpdateCallback(status, result) {
  	try{
    dismissLoadingScreen();
  	swithOffNotificationsAllAccounts = false;
  	 if (currForm == frmENotificationAccountSelection) {
                frmENotificationSettings.show();
     }
    var currForm = kony.application.getCurrentForm();
    if (status == 400) {
        if (result["opstatus"] == 0) {
           // if (currForm == frmENotificationAccountSelection) {
              	showENotificationSettings();
                //frmENotificationSettings.show();
           // }
        }else{
          showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
        }
    }
      }catch (e) {
        kony.print("Error in Enotification"+e);
      	kony.print("Error in Enotification"+e.message);
    }
}

function switchOffNotificationsAllAccounts() {
   swithOffNotificationsAllAccounts = true;
   updateENotificationData();
  /*  var input_param = {}
    // input_param["tmb_news"] = gblENotificationAccounts["tmb_news"];
    input_param["notification"] = "N";
    // input_param["transfertoOwnaccount"] = gblENotificationAccounts["transfer_to_your_account"];
    // input_param["transfertoOwnaccount"] = "N";
    kony.print("gblENotificationAccounts-->" + gblENotificationAccounts)
    for (var i = 0; i < gblENotificationAccounts["accountsdata"].length; i++) {
        gblENotificationAccounts["accountsdata"][i]["notification"] = "N";
    }
    input_param["accounts"] = gblENotificationAccounts["accountsdata"];
    kony.print("notificationUpdate input_param--->" + input_param)
    showLoadingScreen();
    frmENotificationSettings.flexNotificationSettings.isVisible = false;
    invokeServiceSecureAsync("notificationUpdate", input_param, notificationUpdateCallback); */
}


function enableDisableSaveButton(){
  try{
   frmENotificationAccountSelection.btnOwnAccount.setEnabled(true);
   frmENotificationAccountSelection.switchOwnAccount.setEnabled(true)
   frmENotificationAccountSelection.btnSave.setEnabled(false);
   frmENotificationAccountSelection.btnSave.skin = btnGreyBGNoRound;
   frmENotificationAccountSelection.btnSave.focusSkin = btnGreyBGNoRound;
   frmENotificationAccountSelection.btnSave.focusSkin = btnGreyBGNoRound;
   var segData = frmENotificationAccountSelection.segAccounts.data;
  if(checkAnyAccountSelectedForENotif()){
    frmENotificationAccountSelection.btnSave.setEnabled(true);
    frmENotificationAccountSelection.btnSave.skin = btnBlueBGNoRound;
    frmENotificationAccountSelection.btnSave.focusSkin = btnWhiteBGBlueFont200px;
   	
    
  }else{
   frmENotificationAccountSelection.btnOwnAccount.setEnabled(false);
   	 frmENotificationAccountSelection.btnOwnAccount.skin = btnUnCheckedImage;  
  //  if(frmENotificationAccountSelection.switchOwnAccount.selectedIndex == 0){
      frmENotificationAccountSelection.switchOwnAccount.setEnabled(false)
      frmENotificationAccountSelection.switchOwnAccount.selectedIndex = 1;
    //}
	
	
	
  }
    }catch (e) {
        kony.print("Error in Enotification"+e);
      	kony.print("Error in Enotification"+e.message);
    }
}



function checkAnyAccountSelectedForENotif(){
   var segData = frmENotificationAccountSelection.segAccounts.data;
  	var haveAtleastOneAccount = false;
    for (var i = 0; i < segData.length; i++) {
        var selectedData = frmENotificationAccountSelection.segAccounts.data[i];
        //#ifdef android
        if (selectedData["btnSelectAccount"]["skin"] == "btnCheckedImage") {
            gblENotificationAccounts["accountsdata"][i]["notification"] = "Y";
          	haveAtleastOneAccount = true;
           break;
        } else {
            gblENotificationAccounts["accountsdata"][i]["notification"] = "N";
        }
        //#endif
        //#ifdef iphone
        if (selectedData["switchSelectAccount"]["selectedIndex"] == "0") {
            gblENotificationAccounts["accountsdata"][i]["notification"] = "Y";
          	haveAtleastOneAccount = true;
          	break;
        } else {
            gblENotificationAccounts["accountsdata"][i]["notification"] = "N";
        }
        //#endif
    }
  
  return haveAtleastOneAccount;
}

function foreGroundOnENotificaions(){
    var deviceNotificationStatus = arePushNotificationsEnabled();
        gblENotificationAccounts = [];
        if (deviceNotificationStatus == true) {
          	frmENotificationSettings.flexSettings.isVisible = false;
            showENotificationSettings();
        }
}