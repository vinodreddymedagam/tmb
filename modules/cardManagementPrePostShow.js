function postShowCardList(){
	gblManageCardFlow = kony.application.getCurrentForm().id;
  	assignGlobalForMenuPostshow();
}

function frmCardListLocalChange(){
	changeStatusBarColor();
	var cardType = "";
	var cardCode = "";
	var cardNickName = "";
  	//#ifdef android    
  	//mki, mib-10789 Start
	// The below code snippet is to fix the whilte label on some screens
	frmMBCardList.FlexMain.showFadingEdges  = false;
	//#endif  
	//mki, mib-10789 End
	if(gblCardList == null || gblCardList == "" || gblCardList == undefined){
		return;
	}
	frmMBCardList.FlexMain.setContentOffset({"x":"0dp", "y":"0dp"});
	frmMBCardList.lblCardList.text = kony.i18n.getLocalizedString("Menu_keyCard");
	
	if (gblCardList["creditCardRec"].length > 0) {
        cardType = gblCardList["creditCardRec"][0]["cardType"]; 
        cardCode = setCardId(cardType)
        frmMBCardList["lblSectionHeader" + cardCode].text = getCardHeaderText(cardType);
		for(var index = 0; index < gblCardList["creditCardRec"].length; index++){
			cardStatus = gblCardList["creditCardRec"][index]["cardStatus"];
			allowManageCard = "";
			linkText = getLinkTextCard(cardType, cardStatus, allowManageCard);
			frmMBCardList["lblLinkText" + cardCode + index].text = linkText;
          
            if (cardStatus == "Not Activated") {
              cardNickName = kony.i18n.getLocalizedString("keywaitactivate");
            } else {
              if(gblCardList["creditCardRec"][index]["acctNickName"] != undefined ) {
                cardNickName = gblCardList["creditCardRec"][index]["acctNickName"];
                if(cardNickName == "") {
                  if(kony.i18n.getCurrentLocale() == "en_US") {
                    cardNickName = gblCardList["creditCardRec"][index]["defaultCurrentNickNameEN"];
                  } else {
                    cardNickName = gblCardList["creditCardRec"][index]["defaultCurrentNickNameTH"];
                  }
                }
              } else {
                if(kony.i18n.getCurrentLocale() == "en_US") {
                  cardNickName = gblCardList["creditCardRec"][index]["defaultCurrentNickNameEN"];
                } else {
                  cardNickName = gblCardList["creditCardRec"][index]["defaultCurrentNickNameTH"];
                }
              }
            }
          	frmMBCardList["lblCardNickName" + cardCode + index].text = cardNickName;
		}
	}     
	if (gblCardList["debitCardRec"].length > 0) {
        cardType = gblCardList["debitCardRec"][0]["cardType"];
        cardCode = setCardId(cardType)
        frmMBCardList["lblSectionHeader" + cardCode].text = getCardHeaderText(cardType);
		for(var index = 0; index < gblCardList["debitCardRec"].length; index++){
			cardStatus = gblCardList["debitCardRec"][index]["cardStatus"];
			allowManageCard = gblCardList["debitCardRec"][index]["allowManageCard"];
			linkText = getLinkTextCard(cardType, cardStatus, allowManageCard);
			frmMBCardList["lblLinkText" + cardCode + index].text = linkText;
          	if (cardStatus == "Not Activated") {
              cardNickName = kony.i18n.getLocalizedString("keywaitactivate");
            } else {
              if(gblCardList["debitCardRec"][index]["acctNickName"] != undefined ) {
                cardNickName = gblCardList["debitCardRec"][index]["acctNickName"];
                if(cardNickName == "") {
                  if(kony.i18n.getCurrentLocale() == "en_US") {
                    cardNickName = gblCardList["debitCardRec"][index]["defaultCurrentNickNameEN"];
                  } else {
                    cardNickName = gblCardList["debitCardRec"][index]["defaultCurrentNickNameTH"];
                  }
                }
              } else {
                if(kony.i18n.getCurrentLocale() == "en_US") {
                  cardNickName = gblCardList["debitCardRec"][index]["defaultCurrentNickNameEN"];
                } else {
                  cardNickName = gblCardList["debitCardRec"][index]["defaultCurrentNickNameTH"];
                }
              }
            }
          	frmMBCardList["lblCardNickName" + cardCode + index].text = cardNickName;
		}
	}
	if (gblCardList["readyCashRec"].length > 0) {
        cardType = gblCardList["readyCashRec"][0]["cardType"];
        cardCode = setCardId(cardType)
        frmMBCardList["lblSectionHeader" + cardCode].text = getCardHeaderText(cardType);
    	for(var index = 0; index < gblCardList["readyCashRec"].length; index++){
			cardStatus = gblCardList["readyCashRec"][index]["cardStatus"];
			allowManageCard = "";
			linkText = getLinkTextCard(cardType, cardStatus, allowManageCard);
			frmMBCardList["lblLinkText" + cardCode + index].text = linkText;
                    	if (cardStatus == "Not Activated") {
              cardNickName = kony.i18n.getLocalizedString("keywaitactivate");
            } else {
              if(gblCardList["readyCashRec"][index]["acctNickName"] != undefined ) {
                cardNickName = gblCardList["readyCashRec"][index]["acctNickName"];
                if(cardNickName == "") {
                  if(kony.i18n.getCurrentLocale() == "en_US") {
                    cardNickName = gblCardList["readyCashRec"][index]["defaultCurrentNickNameEN"];
                  } else {
                    cardNickName = gblCardList["readyCashRec"][index]["defaultCurrentNickNameTH"];
                  }
                }
              } else {
                if(kony.i18n.getCurrentLocale() == "en_US") {
                  cardNickName = gblCardList["readyCashRec"][index]["defaultCurrentNickNameEN"];
                } else {
                  cardNickName = gblCardList["readyCashRec"][index]["defaultCurrentNickNameTH"];
                }
              }
            }
          	frmMBCardList["lblCardNickName" + cardCode + index].text = cardNickName;
		}
    }  
}
function frmDebitCardListLocalChange(){
  	//#ifdef android    
  	//mki, mib-10789 Start
	// The below code snippet is to fix the whilte label on some screens
	frmMBListDebitCard.FlexMain.showFadingEdges  = false;
	//#endif  
	//mki, mib-10789 End
	changeStatusBarColor();
	var cardType = "";
	var cardCode = "";
	var cardNickName = "";
	if(gblCardList == null || gblCardList == "" || gblCardList == undefined){
		return;
	}
	
	frmMBListDebitCard.lblDebitCard.text = kony.i18n.getLocalizedString("DBA01_Title");
	 
	if (gblCardList["debitCardRec"].length > 0) {
        cardType = gblCardList["debitCardRec"][0]["cardType"];
        cardCode = setCardId(cardType)
		for(var index = 0; index < gblCardList["debitCardRec"].length; index++){
			cardStatus = gblCardList["debitCardRec"][index]["cardStatus"];
			allowManageCard = gblCardList["debitCardRec"][index]["allowManageCard"];
			linkText = getLinkTextCard(cardType, cardStatus, allowManageCard);
			frmMBListDebitCard["lblLinkText" + cardCode + index].text = linkText;
            
          	if (cardStatus == "Not Activated") {
              cardNickName = kony.i18n.getLocalizedString("keywaitactivate");
            } else {
              if(gblCardList["debitCardRec"][index]["acctNickName"] != undefined ) {
                cardNickName = gblCardList["debitCardRec"][index]["acctNickName"];
                if(cardNickName == "") {
                  if(kony.i18n.getCurrentLocale() == "en_US") {
                    cardNickName = gblCardList["debitCardRec"][index]["defaultCurrentNickNameEN"];
                  } else {
                    cardNickName = gblCardList["debitCardRec"][index]["defaultCurrentNickNameTH"];
                  }
                }
              } else {
                if(kony.i18n.getCurrentLocale() == "en_US") {
                  cardNickName = gblCardList["debitCardRec"][index]["defaultCurrentNickNameEN"];
                } else {
                  cardNickName = gblCardList["debitCardRec"][index]["defaultCurrentNickNameTH"];
                }
              }
            }
          	frmMBListDebitCard["lblCardNickName" + cardCode + index].text = cardNickName;
        }
	}
	if(canIssueCard()){
		frmMBListDebitCard["btnIssueCard"].text = kony.i18n.getLocalizedString("keyIssueNC");
	}
	frmMBListDebitCard.buttonBack.text = kony.i18n.getLocalizedString("Back");
	
}
function frmCreditCardListLocalChange(){
	changeStatusBarColor();
	var cardType = "";
	var cardCode = "";
	var cardNickName = "";
	if(gblCardList == null || gblCardList == "" || gblCardList == undefined){
		return;
	}
	 
	if (gblCardList["creditCardRec"].length > 0) {
		frmMBListCreditCard.lblCreditCard.text = kony.i18n.getLocalizedString("keyCreditCard");
        cardType = gblCardList["creditCardRec"][0]["cardType"];
        cardCode = setCardId(cardType)
		for(var index = 0; index < gblCardList["creditCardRec"].length; index++){
			cardStatus = gblCardList["creditCardRec"][index]["cardStatus"];
			allowManageCard = "";
			linkText = getLinkTextCard(cardType, cardStatus, allowManageCard);
			frmMBListCreditCard["lblLinkText" + cardCode + index].text = linkText;
			if (cardStatus == "Not Activated") {
              cardNickName = kony.i18n.getLocalizedString("keywaitactivate");
            } else {
              if(gblCardList["creditCardRec"][index]["acctNickName"] != undefined ) {
                cardNickName = gblCardList["creditCardRec"][index]["acctNickName"];
                if(cardNickName == "") {
                  if(kony.i18n.getCurrentLocale() == "en_US") {
                    cardNickName = gblCardList["creditCardRec"][index]["defaultCurrentNickNameEN"];
                  } else {
                    cardNickName = gblCardList["creditCardRec"][index]["defaultCurrentNickNameTH"];
                  }
                }
              } else {
                if(kony.i18n.getCurrentLocale() == "en_US") {
                  cardNickName = gblCardList["creditCardRec"][index]["defaultCurrentNickNameEN"];
                } else {
                  cardNickName = gblCardList["creditCardRec"][index]["defaultCurrentNickNameTH"];
                }
              }
            }
          	frmMBListCreditCard["lblCardNickName" + cardCode + index].text = cardNickName;
		}
	} else if (gblCardList["readyCashRec"].length > 0) {
		frmMBListCreditCard.lblCreditCard.text = kony.i18n.getLocalizedString("keyReadyCashCard");
        cardType = gblCardList["readyCashRec"][0]["cardType"];
        cardCode = setCardId(cardType)
		for(var index = 0; index < gblCardList["readyCashRec"].length; index++){
			cardStatus = gblCardList["readyCashRec"][index]["cardStatus"];
			allowManageCard = "";
			linkText = getLinkTextCard(cardType, cardStatus, allowManageCard);
			frmMBListCreditCard["lblLinkText" + cardCode + index].text = linkText;
			if (cardStatus == "Not Activated") {
              cardNickName = kony.i18n.getLocalizedString("keywaitactivate");
            } else {
              if(gblCardList["readyCashRec"][index]["acctNickName"] != undefined ) {
                cardNickName = gblCardList["readyCashRec"][index]["acctNickName"];
                if(cardNickName == "") {
                  if(kony.i18n.getCurrentLocale() == "en_US") {
                    cardNickName = gblCardList["readyCashRec"][index]["defaultCurrentNickNameEN"];
                  } else {
                    cardNickName = gblCardList["readyCashRec"][index]["defaultCurrentNickNameTH"];
                  }
                }
              } else {
                if(kony.i18n.getCurrentLocale() == "en_US") {
                  cardNickName = gblCardList["readyCashRec"][index]["defaultCurrentNickNameEN"];
                } else {
                  cardNickName = gblCardList["readyCashRec"][index]["defaultCurrentNickNameTH"];
                }
              }
            }
          	frmMBListCreditCard["lblCardNickName" + cardCode + index].text = cardNickName;
		}
	}
	frmMBListCreditCard.buttonBack.text = kony.i18n.getLocalizedString("Back");
}