function preShowFrmMBRTPApplyBiller(){
  	frmMBRTPApplyBiller.lblTitle.text = kony.i18n.getLocalizedString("MB_RTPAddBillTitle");
  	frmMBRTPApplyBiller.btnSave.text = kony.i18n.getLocalizedString("keysave");
  	frmMBRTPApplyBiller.lblNickNameTiltle.text = kony.i18n.getLocalizedString("keylblNickname");
}

function onClickRTPBillerSettingBtn(){
  	var segData = [];
 	var btnStatusTxt = "";
  	var btnStatusSkin = "";
  	// populate my bills
  	for(var i = 0; i < 2;i++){
      	if(i == 0) { 
          	btnStatusTxt = kony.i18n.getLocalizedString("MB_RTPStatusActive");
          	btnStatusSkin = "btnWhiteBGGreenFont20px";
        }else{
          	btnStatusTxt = kony.i18n.getLocalizedString("MB_RTPStatusPending");
          	btnStatusSkin = "btnWhiteBGGreyFont20px";
        }	
      	
       	var tempRecord =  {
           imgLogo: "myrecpicon.png",
           lblBillerName: "Pat.W",
           lblBillerNameComp:{
             text: "TMB Credit Card (0069)",
             skin: "lblGrey36px"
           },
           btnStatus:{
             text: btnStatusTxt,
             skin: btnStatusSkin
           },
           template: flxRTPMyBills                 
       	};
      	segData.push(tempRecord);
    }
  	frmMBRTPBillerSettings.segMyBills.removeAll();
  	frmMBRTPBillerSettings.segMyBills.setData(segData);
  
  	// populate suggest biller
  	segData = [];
  	for(var i = 0; i < 3;i++){
		btnStatusTxt = kony.i18n.getLocalizedString("MB_RTPStatusApply");
		btnStatusSkin = "btnBlueBGWhiteFont20px";
      	
       	var tempRecord =  {
           imgLogo: "prod_creditcard.png",
           lblBillerName: "Credit Card Bank " + i,
           lblBillerNameComp:{
             text: "(0069)",
             skin: "lblbluemedium160"
           },
           btnStatus:{
             text: btnStatusTxt,
             skin: btnStatusSkin,
             onClick: onClickRTPApplyBillerBtn
           },
           template: flxRTPMyBills                 
       	};
      	segData.push(tempRecord);
    }
  	frmMBRTPBillerSettings.segSuggestedBillers.removeAll();
  	frmMBRTPBillerSettings.segSuggestedBillers.setData(segData);
  	onClickRTPBillerSettingSearchCancelBtn();
  	preShowFrmMBRTPBillerSettings();
  	gblSelectBillerCategoryID = "0";
  	frmMBRTPBillerSettings.show();
}
 
function onClickRTPApplyBillerBtn(){
  	frmMBRTPApplyBiller.txtNickName.text = frmMBRTPApplyBiller.lblNickName.text;
  	frmMBRTPApplyBiller.txtRef1.text = frmMBRTPApplyBiller.lblRef1.text;
  	frmMBRTPApplyBiller.txtRef2.text = frmMBRTPApplyBiller.lblRef2.text;
  	preShowFrmMBRTPApplyBiller();
  	setToDefaultDisplayRTPApplyBiller();
  	frmMBRTPApplyBiller.show();
  	onClickRTPApplyBillerNickName();
}

function onClickRTPCloseApplyBiller(){
  	onClickRTPBillerSettingSearchCancelBtn();
  	frmMBRTPBillerSettings.show();
  	frmMBRTPBillerSettings.lblHeader.setFocus(true);
}

function onClickRTPSaveApplyBiller(){
  	showLoadingScreen();
  	if(!validateRTPAddToMyBillsFields()){
      	dismissLoadingScreen();
		return false;
    }
  	onClickRTPBillerSettingSearchCancelBtn();
  	dismissLoadingScreen();
  	frmMBRTPBillerSettings.show();
  	frmMBRTPBillerSettings.lblHeader.setFocus(true);
}

function onClickRPTBackBtnBillerSettings(){
  	frmMBRTPSettings.show();
}

function setToDefaultDisplayRTPApplyBiller(){
  	frmMBRTPApplyBiller.flxNickName.setVisibility(true);
  	frmMBRTPApplyBiller.flxTxtNickName.setVisibility(false);
  	frmMBRTPApplyBiller.flxRef1.setVisibility(true);
  	frmMBRTPApplyBiller.flxTxtRef1.setVisibility(false);
  	frmMBRTPApplyBiller.flxRef2.setVisibility(true);
  	frmMBRTPApplyBiller.flxTxtRef2.setVisibility(false);
}

function onClickRTPApplyBillerNickName(){
    frmMBRTPApplyBiller.flxNickName.setVisibility(false);
  	frmMBRTPApplyBiller.flxTxtNickName.setVisibility(true);
  	frmMBRTPApplyBiller.txtNickName.placeholder = frmMBRTPApplyBiller.lblNickNameTiltle.text;
  	frmMBRTPApplyBiller.txtNickName.text = frmMBRTPApplyBiller.lblNickName.text;
    frmMBRTPApplyBiller.txtNickName.setFocus(true);
  	
  	if(!isNotBlank(frmMBRTPApplyBiller.txtRef1.text)){
      frmMBRTPApplyBiller.txtRef1.text = "";
      frmMBRTPApplyBiller.lblRef1.text = "";
    }else{
      frmMBRTPApplyBiller.flxTxtRef1.setVisibility(false);
      frmMBRTPApplyBiller.flxRef1.setVisibility(true);
      frmMBRTPApplyBiller.lblRef1.text = frmMBRTPApplyBiller.txtRef1.text;
    }
  
  	if(!isNotBlank(frmMBRTPApplyBiller.txtRef2.text)){
      frmMBRTPApplyBiller.txtRef2.text = "";
      frmMBRTPApplyBiller.lblRef2.text = "";
    }else{
      frmMBRTPApplyBiller.flxTxtRef2.setVisibility(false);
      frmMBRTPApplyBiller.flxRef2.setVisibility(true);
      frmMBRTPApplyBiller.lblRef2.text = frmMBRTPApplyBiller.txtRef2.text;
    }
  
  	frmMBRTPApplyBiller.flxRef1.setVisibility(true);
  	frmMBRTPApplyBiller.flxRef2.setVisibility(true);
    frmMBRTPApplyBiller.flxTxtRef1.setVisibility(false);
  	frmMBRTPApplyBiller.flxTxtRef2.setVisibility(false);
}

function onClickRTPApplyBillerRef1(){
    frmMBRTPApplyBiller.flxRef1.setVisibility(false);
  	frmMBRTPApplyBiller.flxTxtRef1.setVisibility(true);
  	frmMBRTPApplyBiller.txtRef1.placeholder = frmMBRTPApplyBiller.lblBillerRef1Title.text;
  	frmMBRTPApplyBiller.txtRef1.setFocus(true);
  
  	if(!isNotBlank(frmMBRTPApplyBiller.txtNickName.text)){
      frmMBRTPApplyBiller.txtNickName.text = "";
      frmMBRTPApplyBiller.lblNickName.text = "";
    }else{
      frmMBRTPApplyBiller.flxTxtNickName.setVisibility(false);
      frmMBRTPApplyBiller.flxNickName.setVisibility(true);
      frmMBRTPApplyBiller.lblNickName.text = frmMBRTPApplyBiller.txtNickName.text;
    }
  
  	if(!isNotBlank(frmMBRTPApplyBiller.txtRef2.text)){
      frmMBRTPApplyBiller.txtRef2.text = "";
      frmMBRTPApplyBiller.lblRef2.text = "";
    }else{
      frmMBRTPApplyBiller.flxTxtRef2.setVisibility(false);
      frmMBRTPApplyBiller.flxRef2.setVisibility(true);
      frmMBRTPApplyBiller.lblRef2.text = frmMBRTPApplyBiller.txtRef2.text;
    }
  
  	frmMBRTPApplyBiller.flxNickName.setVisibility(true);
  	frmMBRTPApplyBiller.flxRef2.setVisibility(true);
    frmMBRTPApplyBiller.flxTxtNickName.setVisibility(false);
    frmMBRTPApplyBiller.flxTxtRef2.setVisibility(false);
}

function onClickRTPApplyBillerRef2(){
  	frmMBRTPApplyBiller.flxRef2.setVisibility(false);
  	frmMBRTPApplyBiller.flxTxtRef2.setVisibility(true);
  	frmMBRTPApplyBiller.txtRef2.placeholder = frmMBRTPApplyBiller.lblBillerReft2Title.text;
  	frmMBRTPApplyBiller.txtRef2.text = frmMBRTPApplyBiller.lblRef2.text;
  	frmMBRTPApplyBiller.txtRef2.setFocus(true);
  
  	if(!isNotBlank(frmMBRTPApplyBiller.txtNickName.text)){
      frmMBRTPApplyBiller.txtNickName.text = "";
      frmMBRTPApplyBiller.lblNickName.text = "";
    }else{
      frmMBRTPApplyBiller.flxTxtNickName.setVisibility(false);
      frmMBRTPApplyBiller.flxNickName.setVisibility(true);
      frmMBRTPApplyBiller.lblNickName.text = frmMBRTPApplyBiller.txtNickName.text;
    }
  
  	if(!isNotBlank(frmMBRTPApplyBiller.txtRef1.text)){
      frmMBRTPApplyBiller.txtRef1.text = "";
      frmMBRTPApplyBiller.lblRef1.text = "";
    }else{
      frmMBRTPApplyBiller.flxTxtRef1.setVisibility(false);
      frmMBRTPApplyBiller.flxRef1.setVisibility(true);
      frmMBRTPApplyBiller.lblRef1.text = frmMBRTPApplyBiller.txtRef1.text;
    }
  
  	frmMBRTPApplyBiller.flxNickName.setVisibility(true);
  	frmMBRTPApplyBiller.flxRef1.setVisibility(true);
    frmMBRTPApplyBiller.flxTxtNickName.setVisibility(false);
    frmMBRTPApplyBiller.flxTxtRef1.setVisibility(false);
}

function onDoneRTPApplyBillerTxtNickName(){
  	
  	if(!isNotBlank(frmMBRTPApplyBiller.txtNickName.text)){
      showAlert(kony.i18n.getLocalizedString("MIB_BPErr_NoAccNickname"), kony.i18n.getLocalizedString("info"));
      return;
    }else{
      frmMBRTPApplyBiller.flxTxtNickName.setVisibility(false);	
      frmMBRTPApplyBiller.flxNickName.setVisibility(true);
      frmMBRTPApplyBiller.lblNickName.text = frmMBRTPApplyBiller.txtNickName.text;
    }
  
  	if(!isNotBlank(frmMBRTPApplyBiller.txtRef1.text)){
      	frmMBRTPApplyBiller.flxRef1.setVisibility(false);
  		frmMBRTPApplyBiller.flxTxtRef1.setVisibility(true);
      	frmMBRTPApplyBiller.txtRef1.placeholder = frmMBRTPApplyBiller.lblBillerRef1Title.text;
      	frmMBRTPApplyBiller.txtRef1.setFocus(true);
    }else if(!isNotBlank(frmMBRTPApplyBiller.txtRef2.text)){
      	rtp_log("---Ref2 empty--");
      	frmMBRTPApplyBiller.flxRef2.setVisibility(false);
  		frmMBRTPApplyBiller.flxTxtRef2.setVisibility(true);
      	frmMBRTPApplyBiller.txtRef2.placeholder = frmMBRTPApplyBiller.lblBillerReft2Title.text;
      	frmMBRTPApplyBiller.txtRef2.setFocus(true);
    }else{
      	frmMBRTPApplyBiller.btnSave.setFocus(true);
    }
}

function onDoneRTPApplyBillerRef1(){
  
  	if(!isNotBlank(frmMBRTPApplyBiller.txtRef1.text)){
      var noRefMsg =  kony.i18n.getLocalizedString("MIB_BPNoRef");
      noRefMsg = noRefMsg.replace("{ref_label}",  frmMBRTPApplyBiller.lblBillerRef1Title.text+"");	
      frmMBRTPApplyBiller.lblRef1.text = "Ref.1"
      showAlert(noRefMsg, kony.i18n.getLocalizedString("info"));
      return;
    }else{
      frmMBRTPApplyBiller.flxTxtRef1.setVisibility(false);
      frmMBRTPApplyBiller.flxRef1.setVisibility(true);
      frmMBRTPApplyBiller.lblRef1.text = frmMBRTPApplyBiller.txtRef1.text;
    }
  	
  	if(!isNotBlank(frmMBRTPApplyBiller.txtRef2.text)){
      	rtp_log("---Ref2 empty--");
      	frmMBRTPApplyBiller.flxRef2.setVisibility(false);
  		frmMBRTPApplyBiller.flxTxtRef2.setVisibility(true);
      	frmMBRTPApplyBiller.txtRef2.placeholder = frmMBRTPApplyBiller.lblBillerReft2Title.text;
      	frmMBRTPApplyBiller.txtRef2.setFocus(true);
    }else if(!isNotBlank(frmMBRTPApplyBiller.txtNickName.text)){
      	rtp_log("---NickName empty--");
      	frmMBRTPApplyBiller.flxNickName.setVisibility(false);
      	frmMBRTPApplyBiller.flxTxtNickName.setVisibility(true);	
        frmMBRTPApplyBiller.txtNickName.placeholder = frmMBRTPApplyBiller.lblNickName.text;
        frmMBRTPApplyBiller.txtNickName.setFocus(true);
    }else{
      	frmMBRTPApplyBiller.flxTxtNickName.setVisibility(false);
  		frmMBRTPApplyBiller.flxTxtRef2.setVisibility(false);
      	frmMBRTPApplyBiller.btnSave.setFocus(true);
    }
}

function onDoneRTPApplyBillerRef2(){
  	frmMBRTPApplyBiller.flxRef2.setVisibility(true);
  	frmMBRTPApplyBiller.flxTxtRef2.setVisibility(false);
  	frmMBRTPApplyBiller.lblRef2.text = frmMBRTPApplyBiller.txtRef2.text
    
  	/*if(!isNotBlank(frmMBRTPApplyBiller.txtRef2.text)){
      var noRefMsg =  kony.i18n.getLocalizedString("MIB_BPNoRef");
      noRefMsg = noRefMsg.replace("{ref_label}",  frmMBRTPApplyBiller.lblBillerReft2Title.text+"");	
      showAlert(noRefMsg, kony.i18n.getLocalizedString("info"));
      return;
    }else{
      frmMBRTPApplyBiller.flxTxtRef2.setVisibility(false);
      frmMBRTPApplyBiller.flxRef2.setVisibility(true);
      frmMBRTPApplyBiller.lblRef2.text = frmMBRTPApplyBiller.txtRef2.text;
    }
  
  	if(!isNotBlank(frmMBRTPApplyBiller.txtRef1.text)){
      	rtp_log("---Ref1 empty--");
      	frmMBRTPApplyBiller.flxRef1.setVisibility(false);
  		frmMBRTPApplyBiller.flxTxtRef1.setVisibility(true);
      	frmMBRTPApplyBiller.txtRef1.placeholder = frmMBRTPApplyBiller.lblBillerRef1Title.text;
      	frmMBRTPApplyBiller.txtRef1.setFocus(true);
    }else if(!isNotBlank(frmMBRTPApplyBiller.txtNickName.text)){
      	rtp_log("---NickName empty--");
      	frmMBRTPApplyBiller.flxNickName.setVisibility(false);
      	frmMBRTPApplyBiller.flxTxtNickName.setVisibility(true);	
        frmMBRTPApplyBiller.txtNickName.placeholder = frmMBRTPApplyBiller.lblNickName.text;
        frmMBRTPApplyBiller.txtNickName.setFocus(true);
    }else{
  		frmMBRTPApplyBiller.flxTxtNickName.setVisibility(false);
      	frmMBRTPApplyBiller.flxTxtRef1.setFocus(false);
      	frmMBRTPApplyBiller.btnSave.setFocus(true);
    }*/
}



function preShowFrmMBRTPBillerSettings(){
  	frmMBRTPBillerSettings.lblHeader.text = kony.i18n.getLocalizedString("MB_RTPBillerSetbtn");
  	frmMBRTPBillerSettings.txtSearch.placeholder = kony.i18n.getLocalizedString("keySearch");
  	frmMBRTPBillerSettings.lblCategories.text = kony.i18n.getLocalizedString("MIB_BPAllCate");
  	frmMBRTPBillerSettings.lblMyBillsTitle.text = kony.i18n.getLocalizedString("keyMyBills");
  	frmMBRTPBillerSettings.lblSuggestedBillersTitle.text = kony.i18n.getLocalizedString("keySuggestedBillers");
  	frmMBRTPBillerSettings.lblCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
}

function onClickRTPBillerSettingSearchCancelBtn(){
  	frmMBRTPBillerSettings.txtSearch.text = "";
  	frmMBRTPBillerSettings.flxTxtSearch.width = "94%";
  	frmMBRTPBillerSettings.flxCancel.setVisibility(false);
}

function onClickRTPBillerSettingTxtSearchBtn(){
  	frmMBRTPBillerSettings.flxTxtSearch.width = "75%";
  	frmMBRTPBillerSettings.flxCancel.setVisibility(true);
  	frmMBRTPBillerSettings.lblCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
  	frmMBRTPBillerSettings.txtSearch.setFocus(true);
}

function validateRTPAddToMyBillsFields(){
	var billerNickName = frmMBRTPApplyBiller.txtNickName.text;
	if(!isNotBlank(billerNickName)){
      	showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_BPErr_NoAccNickname"), kony.i18n.getLocalizedString("info"), onClickRTPApplyBillerNickName);
		//showAlert(kony.i18n.getLocalizedString("MIB_BPErr_NoAccNickname"), kony.i18n.getLocalizedString("info"));
      	return false;
	}
	if(billerNickName.length < 3 || billerNickName.length > 20 || !NickNameValid(billerNickName)){
      	//showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_BPkeyInvalidNickName"), kony.i18n.getLocalizedString("info"), callBackRTPAppyBillerNickName);
      	showAlert(kony.i18n.getLocalizedString("MIB_BPkeyInvalidNickName"), kony.i18n.getLocalizedString("info"));
		return false;
	}
	if(!checkDuplicateBillerNickCheck(billerNickName)){
		//showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_BPErr_alert_correctnickname"), kony.i18n.getLocalizedString("info"), callBackRTPAppyBillerNickName);
      	showAlert(kony.i18n.getLocalizedString("MIB_BPErr_alert_correctnickname"), kony.i18n.getLocalizedString("info"));
		return false;
	}
	if(!checkMaxBillerCountAddToMyBills()){
      	//showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_BPErrMaxBiller"), kony.i18n.getLocalizedString("info"), callBackRTPAppyBillerNickNameReachMax);
      	showAlert(kony.i18n.getLocalizedString("MIB_BPErrMaxBiller"), kony.i18n.getLocalizedString("info"));
		return false;
	}
  	if(!isNotBlank(frmMBRTPApplyBiller.txtRef1.text)){
		var noRefMsg =  kony.i18n.getLocalizedString("MIB_BPNoRef");
		noRefMsg = noRefMsg.replace("{ref_label}",  frmMBRTPApplyBiller.lblBillerRef1Title.text+"");	
		showAlertWithCallBack(noRefMsg, kony.i18n.getLocalizedString("info"), onClickRTPApplyBillerRef1);
      	//showAlert(noRefMsg, kony.i18n.getLocalizedString("info"));
		return false;
	}
  	/*if(!isNotBlank(frmMBRTPApplyBiller.txtRef2.text)){
		var noRefMsg =  kony.i18n.getLocalizedString("MIB_BPNoRef");
		noRefMsg = noRefMsg.replace("{ref_label}",  frmMBRTPApplyBiller.lblBillerReft2Title.text+"");	
		showAlertWithCallBack(noRefMsg, kony.i18n.getLocalizedString("info"), onClickRTPApplyBillerRef2);
      	//showAlert(noRefMsg, kony.i18n.getLocalizedString("info"));
		return false;
	}*/
	return true;
}

function callBackRTPAppyBillerNickName(){
  	onDoneRTPApplyBillerRef1();
  	onDoneRTPApplyBillerRef2();
	frmMBRTPApplyBiller.flxNickName.setVisibility(false);
  	frmMBRTPApplyBiller.flxTxtNickName.setVisibility(true);
  	frmMBRTPApplyBiller.txtNickName.text = "";
    frmMBRTPApplyBiller.lblNickName.text = "";
  	frmMBRTPApplyBiller.txtNickName.placeholder = frmMBRTPApplyBiller.lblNickNameTiltle.text;
  	frmMBRTPApplyBiller.txtNickName.setFocus(true);
}

function callBackRTPAppyBillerNickNameReachMax(){
   	frmMBRTPBillerSettings.show();
}

function callBackRTPAppyBillerRef1(){
  	onDoneRTPApplyBillerTxtNickName();
  	onDoneRTPApplyBillerRef2();
  	frmMBRTPApplyBiller.flxRef1.setVisibility(false);
  	frmMBRTPApplyBiller.flxTxtRef1.setVisibility(true);
  	frmMBRTPApplyBiller.txtRef1.text = "";
    frmMBRTPApplyBiller.lblRef1.text = "";
  	frmMBRTPApplyBiller.txtRef1.placeholder = frmMBRTPApplyBiller.lblBillerRef1Title.text;
  	frmMBRTPApplyBiller.txtRef1.setFocus(true);
}

function callBackRTPAppyBillerRef2(){
  	onDoneRTPApplyBillerTxtNickName();
  	onDoneRTPApplyBillerRef1();
  	frmMBRTPApplyBiller.flxRef2.setVisibility(false);
  	frmMBRTPApplyBiller.flxTxtRef2.setVisibility(true);
  	frmMBRTPApplyBiller.txtRef2.text = "";
    frmMBRTPApplyBiller.lblRef2.text = "";
  	frmMBRTPApplyBiller.txtRef2.placeholder = frmMBRTPApplyBiller.lblBillerReft2Title.text;
  	frmMBRTPApplyBiller.txtRef2.setFocus(true);
}

function assignValueRTPBillerSettings(){
    gblSelectBillerCategoryID = frmBillPaymentBillerCategories.segBillerCategories.selectedItems[0].BillerCategoryID.text;
    frmMBRTPBillerSettings.lblCategories.text = frmBillPaymentBillerCategories.segBillerCategories.selectedItems[0].lblCategory.text;
    //searchMyBillsAndSuggestedBillers();
    frmMBRTPBillerSettings.show();
}