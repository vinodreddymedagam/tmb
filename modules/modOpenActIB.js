var producticonfordream = "";



function IBvalidateBenfFields(firsTxtName,scndTxtName,befTxtName,relSel){
	
	if (firsTxtName.text == "" || firsTxtName.text == null){
	
	showAlert(kony.i18n.getLocalizedString("keyBeneficiaryAddDetail") , kony.i18n.getLocalizedString("info"));
	firsTxtName.skin = "txtErrorBG";
	return false;
	}else if (scndTxtName.text == "" || scndTxtName.text == null){
	showAlert(kony.i18n.getLocalizedString("keyBeneficiaryAddDetail") , kony.i18n.getLocalizedString("info"));
	scndTxtName.skin = "txtErrorBG";
	return false;
	}else if (befTxtName.text == "" || befTxtName.text == null){
	showAlert(kony.i18n.getLocalizedString("keyBeneficiaryAddDetail") , kony.i18n.getLocalizedString("info"));
	befTxtName.skin = "txtErrorBG";
	return false;
	}else if (relSel == "P" || relSel == null ){
	
	showAlert(kony.i18n.getLocalizedString("keyBeneficiarySelRelationship") , kony.i18n.getLocalizedString("info"));
	return false;
	}
	else if (!(benNameAlpNumValidation(firsTxtName.text))){
	
	showAlert(kony.i18n.getLocalizedString("keyBenFirstNameAlphaNum"), kony.i18n.getLocalizedString("info"));
	firsTxtName.skin = txtErrorBG;
	return false;
	}else if (!(benNameAlpNumValidation(scndTxtName.text))){
	showAlert(kony.i18n.getLocalizedString("keyBenSecondNameAlphNum") , kony.i18n.getLocalizedString("info"));
	scndTxtName.skin = txtErrorBG;
	return false;
	}
	else if (((kony.os.toNumber(befTxtName.text)) <= 0) && ((kony.os.toNumber(befTxtName.text)) >= 100)){
	showAlert(kony.i18n.getLocalizedString("keyBenpercentage") , kony.i18n.getLocalizedString("info"));
	befTxtName.skin = "txtErrorBG";
	return false;
	}else
	return true;
	
}










function IBonClickPreShowSCConfirm(){
	frmIBOpenNewSavingsCareAccConfirmation.btnRight.setVisibility(false);
	
	frmIBOpenNewSavingsCareAccConfirmation.imgOASCTitle.src = frmIBOpenNewSavingsCareAcc.imgSavinCareSelProdIcon.src;
	frmIBOpenNewSavingsCareAccConfirmation.lblOASCTitle.text = frmIBOpenNewSavingsCareAcc.lblSavinCareSelProdTxt.text;
	frmIBOpenNewSavingsCareAccConfirmation.lblOASCNicNamVal.text = frmIBOpenNewSavingsCareAcc.txtNickName.text;
}



function IBfrmOpenProdDetnTnCPreShow(){
	var selectedProdList = frmIBOpenActSelProd["segOpenActSelProd"]["selectedItems"][0]["productcode"];
	if(frmIBOpenActSelProd["segOpenActSelProd"]["selectedItems"][0]["hiddenIssueCardOnline"] != undefined)
		gblIssueOnlineCard = frmIBOpenActSelProd["segOpenActSelProd"]["selectedItems"][0]["hiddenIssueCardOnline"];
	else
		gblIssueOnlineCard="0";	
	var locale = kony.i18n.getCurrentLocale();
						if (kony.i18n.getCurrentLocale() == "en_US") {
							frmIBOpenProdDetnTnC.lblOpenActDescSubTitle.text = gblFinActivityLogOpenAct["prodNameEN"];
						} else {
							frmIBOpenProdDetnTnC.lblOpenActDescSubTitle.text = gblFinActivityLogOpenAct["prodNameTH"];
						}
	var inputParam = {};
	showLoadingScreenPopup();
	inputParam["selectedCategory"] = "";
	inputParam["productCode"] = frmIBOpenActSelProd["segOpenActSelProd"]["selectedItems"][0]["productcode"];
	invokeServiceSecureAsync("getInterestRate",inputParam,IBcallBackReceiveAckInterest)
}

/**
 * Below method is called when open account is set as internal link in campaign screens
 */

function IBOpenAccProdBriefFromCampaignFlow(){
	//var prodD = gblProudctDetails.ProdDetails[gblProdCode];
	var prodD = gblProudctDetails[gblProdCode];
	//alert("product issue card online : "+prodD.issueCardOnline);
	gblIssueOnlineCard = prodD.issueCardOnline;		
	var locale = kony.i18n.getCurrentLocale();
	if (kony.i18n.getCurrentLocale() == "en_US") {
		frmIBOpenProdDetnTnC.lblOpenActDescSubTitle.text = gblFinActivityLogOpenAct["prodNameEN"];
	} else {
		frmIBOpenProdDetnTnC.lblOpenActDescSubTitle.text = gblFinActivityLogOpenAct["prodNameTH"];
	}
	var inputParam = {};
	showLoadingScreenPopup();
	inputParam["selectedCategory"] = "";
	inputParam["productCode"] = gblProdCode
	invokeServiceSecureAsync("getInterestRate",inputParam,IBcallBackReceiveAckInterest)
}

/*
 This method will be called on click of cancel button of Open Account
 */
function onOpenActCancel(){
	IBsetDataForUse();
}


function IBsetDataForUse(){
	showLoadingScreenPopup();
	gblSelProduct = "ForUse";
	frmIBOpenActSelProd.vbxOpenActSave.skin = vbxSave;
	frmIBOpenActSelProd.vbxOpenActUse.skin = vbxUseFocus;
	frmIBOpenActSelProd.vbxOpenActTerm.skin = vbxTerm;
	frmIBOpenActSelProd.segOpenActSelProd.removeAll();
	frmIBOpenActSelProd.segOpenActSelProd.containerWeight = 100;
	frmIBOpenActSelProd.segOpenActSelProd.widgetDataMap = {
		imgOpnActSelProd: "imgOpnActSelProd",
		lblOpnActSelProd: "lblOpnActSelProd",
		imgOpnActSelProdRtArrow: "imgOpnActSelProdRtArrow"
	}
	var inputParam = {};
	inputParam["selectedCategory"] = gblSelProduct;
	invokeServiceSecureAsync("selectProdCategoryForOpenAcc", inputParam, IBcallBackReceiveAckProdCategory);
	
}


function IBsetDataForSave(){
	showLoadingScreenPopup();
	gblSelProduct = "ForSave";
	frmIBOpenActSelProd.vbxOpenActSave.skin = vbxSaveFocus;
	frmIBOpenActSelProd.vbxOpenActUse.skin = vbxUse;
	frmIBOpenActSelProd.vbxOpenActTerm.skin = vbxTerm;
	frmIBOpenActSelProd.segOpenActSelProd.removeAll();
	frmIBOpenActSelProd.segOpenActSelProd.containerWeight = 100;
	frmIBOpenActSelProd.segOpenActSelProd.widgetDataMap = {
		imgOpnActSelProd: "imgOpnActSelProd",
		lblOpnActSelProd: "lblOpnActSelProd",
		imgOpnActSelProdRtArrow: "imgOpnActSelProdRtArrow"
	}
	
	var inputParam = {};
	inputParam["selectedCategory"] = gblSelProduct;
	invokeServiceSecureAsync("selectProdCategoryForOpenAcc", inputParam, IBcallBackReceiveAckProdCategory);
}

function IBsetDataForTD(){
	showLoadingScreenPopup();
	gblSelProduct = "ForTerm";
	frmIBOpenActSelProd.vbxOpenActSave.skin = vbxSave;
	frmIBOpenActSelProd.vbxOpenActUse.skin = vbxUse;
	frmIBOpenActSelProd.vbxOpenActTerm.skin = vbxTermFocus;
	frmIBOpenActSelProd.segOpenActSelProd.removeAll();
	frmIBOpenActSelProd.segOpenActSelProd.containerWeight = 100;
	frmIBOpenActSelProd.segOpenActSelProd.widgetDataMap = {
		imgOpnActSelProd: "imgOpnActSelProd",
		lblOpnActSelProd: "lblOpnActSelProd",
		imgOpnActSelProdRtArrow: "imgOpnActSelProdRtArrow"
	}
	
	var inputParam = {};
	inputParam["selectedCategory"] = gblSelProduct;
	invokeServiceSecureAsync("selectProdCategoryForOpenAcc", inputParam, IBcallBackReceiveAckProdCategory);
}



function setSavingCareDataIB()
{
		
		if (GLOBAL_NICKNAME_LENGTH != null || GLOBAL_NICKNAME_LENGTH != ""){
		frmIBOpenNewSavingsCareAcc.txtNickName.maxTextLength = kony.os.toNumber(GLOBAL_NICKNAME_LENGTH);
		}else{
		frmIBOpenNewSavingsCareAcc.txtNickName.maxTextLength = 20;
		}
		getRelationShipsFromService();
		/* for(var i=0;i<gblBenificiaryCount ; i++){
			frmIBOpenNewSavingsCareAcc["comboRelationship"+i].selectedKey = "P"
		} */
		frmIBOpenNewSavingsCareAcc.imgSavinCareSelProdIcon.src = frmIBOpenActSelProd["segOpenActSelProd"]["selectedItems"][0].imgOpnActSelProd;
		if (kony.i18n.getCurrentLocale() == "en_US") {
		    frmIBOpenNewSavingsCareAcc.lblSavinCareSelProdTxt.text = gblFinActivityLogOpenAct["prodNameEN"];
		      frmIBOpenNewSavingsCareAcc.lblFromAccount.text = gblFinActivityLogOpenAct["PrdctOpoenCareEN"] ;
		 }else{
		 frmIBOpenNewSavingsCareAcc.lblSavinCareSelProdTxt.text = gblFinActivityLogOpenAct["prodNameTH"];
		 frmIBOpenNewSavingsCareAcc.lblFromAccount.text = gblFinActivityLogOpenAct["PrdctOpoenCareTH"]
		 }
		 /*
			var masterOpenData = [];
        	var count = 0;
			for(i=1;i<=8;i++){
			var trasDate;
			
						
					if (count == 0) {
							trasDate = ["P",kony.i18n.getLocalizedString("keyOpenRelation")];
							count = count + 1
						}
						else if (count == 1) {
							trasDate = ["F",kony.i18n.getLocalizedString("keyFather")];
							count = count + 1
						}
						else if (count == 2) {
							trasDate = ["M",kony.i18n.getLocalizedString("keyMother")];
							count = count + 1
						}else if (count == 3) {
							trasDate = ["R",kony.i18n.getLocalizedString("keyRelative")];
							count = count + 1
						}else if (count == 4) {
							trasDate = ["S",kony.i18n.getLocalizedString("keySpouseReg")];
							count = count + 1
						}else if (count == 5) {
							trasDate = ["L",kony.i18n.getLocalizedString("keySpouseNL")];
							count = count + 1
						}
						else if (count == 6) {
							trasDate = ["C",kony.i18n.getLocalizedString("keyChild")];
							count = count + 1
						}else if (count == 7) {
							trasDate = ["O",kony.i18n.getLocalizedString("keyOtherRelation")];
							count = 0;
							break;
						}
						masterOpenData.push(trasDate);
			}
			arrbefore=masterOpenData;
			frmIBOpenNewSavingsCareAcc.cmbxOne.masterData = masterOpenData;
			frmIBOpenNewSavingsCareAcc.cmbxTwo.masterData = masterOpenData;
			frmIBOpenNewSavingsCareAcc.cmbxThree.masterData = masterOpenData;
			frmIBOpenNewSavingsCareAcc.cmbxFour.masterData = masterOpenData;
			frmIBOpenNewSavingsCareAcc.cmbxFive.masterData = masterOpenData;*/
		frmIBOpenNewSavingsCareAcc.show();
}

function setNormalSavingsDataIB()
{
	// added below if else condition to fix MIB-1601
	if(undefined != frmIBOpenActSelProd["segOpenActSelProd"]["selectedItems"] && null != frmIBOpenActSelProd["segOpenActSelProd"]["selectedItems"]){
		frmIBOpenNewSavingsAcc.imgNSProdName.src = frmIBOpenActSelProd["segOpenActSelProd"]["selectedItems"][0].imgOpnActSelProd;
		//frmIBOpenNewSavingsAcc.lblNSProdName.text = frmIBOpenActSelProd["segOpenActSelProd"]["selectedItems"][0].lblOpnActSelProd;
		frmIBOpenNewSavingsAccConfirmation.lblNSProdName.text = frmIBOpenActSelProd["segOpenActSelProd"]["selectedItems"][0].lblOpnActSelProd;
		frmIBOpenNewSavingsAccConfirmation.imgNSProdName.src = frmIBOpenActSelProd["segOpenActSelProd"]["selectedItems"][0].imgOpnActSelProd;
	}else {
		//var prodD = gblProudctDetails.ProdDetails[gblProdCode];
		var prodD = gblProudctDetails[gblProdCode];
		kony.print("product Details : "+prodD);
		frmIBOpenNewSavingsAcc.imgNSProdName.src = prodD.imageProduct;
		frmIBOpenNewSavingsAccConfirmation.imgNSProdName.src = prodD.imageProduct;
	}
	
	frmIBOpenNewSavingsAcc.hbxSelActAmt.setVisibility(true);
	frmIBOpenNewSavingsAcc.txtOpenActSelAmt.placeholder = commaFormattedOpenAct(gblMinOpenAmt) + kony.i18n.getLocalizedString("currencyThaiBaht");
	
	//frmIBOpenNewSavingsAcc.txtOpenActNicNam.text = "";
	//frmIBOpenNewSavingsAcc.txtOpenActSelAmt.text = commaFormattedOpenAct(gblMinOpenAmt) + kony.i18n.getLocalizedString("currencyThaiBaht");
	frmIBOpenNewSavingsAcc.hbxSelActSelNickName.setVisibility(true);
	frmIBOpenNewSavingsAcc.txtDSAmountSel.setVisibility(false);
	//frmIBOpenNewSavingsAcc.line1.setVisibility(true); ######removed line1, now using a different skin fo rthe hbox - hbxIBMyAccLightBlue
	frmIBOpenNewSavingsAcc.line2.setVisibility(true);
	frmIBOpenNewSavingsAcc.line3.setVisibility(false);
	frmIBOpenNewSavingsAcc.line4.setVisibility(false);
	//frmIBOpenNewSavingsAcc.txtOpenActNicNam.placeholder = kony.i18n.getLocalizedString("keylblNickname");
	if (kony.i18n.getCurrentLocale() == "en_US") {
			    frmIBOpenNewSavingsAcc.lblNSProdName.text = gblFinActivityLogOpenAct["prodNameEN"];
			 }else{
				 frmIBOpenNewSavingsAcc.lblNSProdName.text = gblFinActivityLogOpenAct["prodNameTH"];
			 }
	//IBonclickSavingsAccountCoverFlow();
	//alert("on click")
	if (frmIBOpenNewSavingsCareAcc.coverFlowTP.data.length <= 1){
		IBonclickSavingsAccountCoverFlow();
	}else{		
		//Clear nickname and amount value
		frmIBOpenNewSavingsAcc.txtOpenActNicNam.text = "";
		frmIBOpenNewSavingsAcc.txtOpenActSelAmt.text = "";
		
		//Clear From Account Box
		frmIBOpenNewSavingsAcc.lblFromAccount.text ="";
		frmIBOpenNewSavingsAcc.lblAccNo.text="";
		frmIBOpenNewSavingsAcc.imageAcc.src="";
		frmIBOpenNewSavingsAcc.imageAcc.setVisibility(false);
		
		frmIBOpenNewSavingsAcc.coverFlowTP.setVisibility(true);
		frmIBOpenNewSavingsAcc.hbxCoverFlowTP.setVisibility(true);
		frmIBOpenNewSavingsAcc.vbox866794452610619.setVisibility(true);
		if(frmIBOpenNewSavingsAcc.imageTmb != undefined){			
			frmIBOpenNewSavingsAcc.imageTmb.setVisibility(false);
		}
	}
	frmIBOpenNewSavingsAcc.show();
}

function ProdDetails(){
    ProdDetails = new Object();
    this.add = function(key, value){
        ProdDetails[""+key+""] = value;
    };
    this.ProdDetails = ProdDetails;
}

function IBcallBackReceiveAckProdCategory(status, callBackResponse) 
{
	if (status == 400) 
	{
		
			if (callBackResponse["opstatus"] == 0)
			{
				if(callBackResponse["productList"]!=undefined && callBackResponse["productList"].length == 0)
					frmIBOpenActSelProd.lblForUseTabDisplay.setVisibility(true);
				else
					frmIBOpenActSelProd.lblForUseTabDisplay.setVisibility(false);	
				var segment_temp = [];
				var prodName;
				//try {
					//gblProudctDetails = new ProdDetails();
				//}catch(e){
					//we are getting prodDetails is not contructor, need to look into this, but this is not stopping functionality
					//alert(e);
				//}
				gblProudctDetails = {};
				for (var i1 = 0; i1 < callBackResponse["productList"].length; i1++) 
				{
					imageProduct = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+callBackResponse["productList"][i1]["ICON_ID"]+"&modIdentifier=PRODICON";
					var locale = kony.i18n.getCurrentLocale();
					if (kony.i18n.getCurrentLocale() == "en_US") {
							prodName =  callBackResponse["productList"][i1]["PRODUCT_NAME_EN"];
					} else {
							prodName =  callBackResponse["productList"][i1]["PRODUCT_NAME_TH"];
					}
					var acctTypeVal =  callBackResponse["productList"][i1]["TYPE"];
					
					// added below code for campaign
					var prodCode = callBackResponse["productList"][i1]["PROD_CODE"];
					if(undefined != gblProdCode && "" != gblProdCode && gblProdCode == prodCode){
						var obj1 = new Object();
						obj1.prodName = prodName;
						obj1.prodCode = prodCode;
						obj1.prodNameEN = callBackResponse["productList"][i1]["PRODUCT_NAME_EN"];
						obj1.prodNameTH = callBackResponse["productList"][i1]["PRODUCT_NAME_TH"];
						obj1.actTypeEN = callBackResponse["productList"][i1]["TYPE"];
						obj1.actTypeTH = callBackResponse["productList"][i1]["TYPE_TH"];
						obj1.prodDes = callBackResponse["productList"][i1]["PROD_DESCRIPTION"];
						obj1.issueCardOnline = callBackResponse["productList"][i1]["ISSUE_CARD_ONLINE"];
						obj1.imageProduct = imageProduct;
						
						//gblProudctDetails.add(prodCode, obj1);
						gblProudctDetails[prodCode] = obj1;
					}					
				 	if(gblSelProduct == "ForUse"){
						segment_temp.push({"lblOpnActSelProd": prodName,
						                   "productcode": callBackResponse["productList"][i1]["PROD_CODE"],
						                   // "imgOpnActSelProd": "prod_currentac.png",
											 "imgOpnActSelProd":imageProduct,
						                    "imgOpnActSelProdRtArrow":"bg_arrow_right_grayb.png",
						                    hiddenProdNameEN:callBackResponse["productList"][i1]["PRODUCT_NAME_EN"],
						                    hiddenProdNameTH:callBackResponse["productList"][i1]["PRODUCT_NAME_TH"],
						                    hiddenActTypeEN:callBackResponse["productList"][i1]["TYPE"],
						                    hiddenActTypeTH:callBackResponse["productList"][i1]["TYPE_TH"],
						                    hiddenProdDes:callBackResponse["productList"][i1]["PROD_DESCRIPTION"],
						                    hiddenIssueCardOnline:callBackResponse["productList"][i1]["ISSUE_CARD_ONLINE"]
						 })
					}else if(gblSelProduct == "ForSave"){
						 producticonfordream = imageProduct;
						 	segment_temp.push({"lblOpnActSelProd": prodName,
						                    "productcode": callBackResponse["productList"][i1]["PROD_CODE"],
						                    //"imgOpnActSelProd": "prod_savingd.png",
											 "imgOpnActSelProd":imageProduct,
						                    "imgOpnActSelProdRtArrow":"bg_arrow_right_grayb.png",
						                    hiddenProdNameEN:callBackResponse["productList"][i1]["PRODUCT_NAME_EN"],
						                    hiddenProdNameTH:callBackResponse["productList"][i1]["PRODUCT_NAME_TH"],
						                    hiddenActTypeEN:callBackResponse["productList"][i1]["TYPE"],
						                    hiddenActTypeTH:callBackResponse["productList"][i1]["TYPE_TH"],
						                    hiddenProdDes:callBackResponse["productList"][i1]["PROD_DESCRIPTION"],
						                    hiddenIssueCardOnline:callBackResponse["productList"][i1]["ISSUE_CARD_ONLINE"]
						 })
					}else if(gblSelProduct == "ForTerm"){
						 	segment_temp.push({"lblOpnActSelProd": prodName,
						                    "productcode": callBackResponse["productList"][i1]["PROD_CODE"],
						                    //"imgOpnActSelProd": "prod_termdeposits.png",
											 "imgOpnActSelProd":imageProduct,
						                    "imgOpnActSelProdRtArrow":"bg_arrow_right_grayb.png",
						                    hiddenProdNameEN:callBackResponse["productList"][i1]["PRODUCT_NAME_EN"],
						                    hiddenProdNameTH:callBackResponse["productList"][i1]["PRODUCT_NAME_TH"],
						                    hiddenActTypeEN:callBackResponse["productList"][i1]["TYPE"],
						                    hiddenActTypeTH:callBackResponse["productList"][i1]["TYPE_TH"],
						                    hiddenProdDes:callBackResponse["productList"][i1]["PROD_DESCRIPTION"]
						 })
					}
				} 
				frmIBOpenActSelProd.segOpenActSelProd.setData(segment_temp);
				
				// if campaign flow, making isCmpFlow to true;
				if(isCmpFlow){
					snippetCodeOnClickOfAcctSegment();
					IBOpenAccProdBriefFromCampaignFlow();
				} else {
					dismissLoadingScreenPopup();
					frmIBOpenActSelProd.show();
				}
				
			}else{
				dismissLoadingScreenPopup();
				showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
				return false;
			}
	}
}

function IBcallBackReceiveAckInterest(status, callBackResponse) 
{
	if (status == 400) 
	{
			if (callBackResponse["opstatus"] == 0)
			{
				 var segment_temp = [];
				 if(callBackResponse["SavingTargets"]!=null && callBackResponse["SavingTargets"].length != 0 ){
					frmIBOpenProdDetnTnC.lblOpenAccInt.text = callBackResponse["SavingTargets"][0]["DATA_VALUE"] + " %";
					if (kony.i18n.getCurrentLocale() == "en_US") {
                      	frmIBOpenProdDetnTnC.lblOpenAccIntType.text = callBackResponse["SavingTargets"][0]["CATEGORY_EN"]+":";
                    } else {
                        frmIBOpenProdDetnTnC.lblOpenAccIntType.text =  callBackResponse["SavingTargets"][0]["CATEGORY_TH"]+":";
                    }
									
					gblFinActivityLogOpenAct["intersetRateLabelEN"] = callBackResponse["SavingTargets"][0]["CATEGORY_EN"];
					gblFinActivityLogOpenAct["intersetRateLabelTH"] = callBackResponse["SavingTargets"][0]["CATEGORY_TH"];
					
				}else{
					frmIBOpenProdDetnTnC.lblOpenAccInt.text = "0.00%";
				}					 
				gblMinOpenAmt = callBackResponse["minLimitOpenAct"];
				gblMaxOpenAmt = callBackResponse["maxLimitOpenAct"];
				gblisDisToActTD = callBackResponse["isDisToActTD"];
				gblFinActivityLogOpenAct["cardBinNo"] = callBackResponse["cardBinNo"];
				gblFinActivityLogOpenAct["cardFee"] = callBackResponse["cardFee"];
				gblFinActivityLogOpenAct["cardImg"] = callBackResponse["cardImg"];
				gblFinActivityLogOpenAct["cardType"] = callBackResponse["cardType"];
				if(callBackResponse["cardType"] != undefined && callBackResponse["cardType"] != null){
					gblCardOpenAccount=true;
					frmIBOpenProdDetnTnC.lblOpenNewAccount.text = kony.i18n.getLocalizedString("keyOpenNewAccountAndDebitCard");
				}else{
					gblCardOpenAccount=false;
					frmIBOpenProdDetnTnC.lblOpenNewAccount.text = kony.i18n.getLocalizedString("kelblOpenNewAccount");
				}
				gblFinActivityLogOpenAct["custType"] = callBackResponse["custType"];
				if(isCmpFlow){
					//var prodD = gblProudctDetails.ProdDetails[gblProdCode];
					var prodD = gblProudctDetails[gblProdCode];
					//alert("Product Name : "+prodD.prodName);
					frmIBOpenProdDetnTnC.lblOpenActDescSubTitle.text = prodD.prodName
				} else {
					frmIBOpenProdDetnTnC.lblOpenActDescSubTitle.text=frmIBOpenActSelProd["segOpenActSelProd"]["selectedItems"][0].lblOpnActSelProd;
				}
				//need to make campaign flow false here
				isCmpFlow = false;
				
				frmIBOpenProdDetnTnC.hbxConCanclRC.setVisibility(false);
				frmIBOpenProdDetnTnC.hboxSaveCamEmail.setVisibility(false);
				frmIBOpenProdDetnTnC.lblOpenActDesc.setVisibility(false);
				frmIBOpenProdDetnTnC.hbox45734110037959.setVisibility(false);
				//frmIBOpenProdDetnTnC.btnOpenProdContinue.setVisibility(true);
				frmIBOpenProdDetnTnC.hbxOpenProdContinue.setVisibility(true);
				frmIBOpenProdDetnTnC.hbxOpnProdDet.setVisibility(true);
				frmIBOpenProdDetnTnC.richTextProdDetIB.setVisibility(true);
				frmIBOpenProdDetnTnC.imgProductPackage.setVisibility(false);
				frmIBOpenProdDetnTnC.lblOpenActDescSubTitle.isVisible = true;
				/*
				if(gblSelOpenActProdCode == "220"){
				 frmIBOpenProdDetnTnC.richTextProdDetIB.text = kony.i18n.getLocalizedString('keyNoFeeProdDet');
				} else */ 
				if(gblOpenActList["FOR_USE_PRODUCT_CODES"] != null && gblOpenActList["FOR_USE_PRODUCT_CODES"] != undefined && gblOpenActList["FOR_USE_PRODUCT_CODES"].indexOf(gblSelOpenActProdCode) > 0){
					var product_package_image_name = callBackResponse["cardImg"];
					product_details_i18n_key = callBackResponse["tPhraseContentId"];
					frmIBOpenProdDetnTnC.richTextProdDetIB.text = kony.i18n.getLocalizedString(product_details_i18n_key); //kony.i18n.getLocalizedString('keyNormalSavProdDet')
					var product_package_image="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+product_package_image_name+"&modIdentifier=PRODUCTPACKAGEIMG";
					frmIBOpenProdDetnTnC.imgProductPackage.src = product_package_image;
					frmIBOpenProdDetnTnC.imgProductPackage.isVisible = true;
				} 
				/*else if(gblSelOpenActProdCode == "222" ){
				frmIBOpenProdDetnTnC.richTextProdDetIB.text = kony.i18n.getLocalizedString('keyFreeFlowProdDet');
				}*/
				else if(gblSelOpenActProdCode == "206"){
					frmIBOpenProdDetnTnC.richTextProdDetIB.text = kony.i18n.getLocalizedString('keyDreamProdDet');
				}else if(gblOpenActList["SAVING_CARE_PRODUCT_CODES"] != null && gblOpenActList["SAVING_CARE_PRODUCT_CODES"] != undefined && gblOpenActList["SAVING_CARE_PRODUCT_CODES"].indexOf(gblSelOpenActProdCode) >= 0){
					//Code to handle Saving Care products
					var saving_care_image_name = "SavingsCareIntroEN";
					if(kony.i18n.getCurrentLocale() == "th_TH")
						saving_care_image_name = "SavingsCareIntroTH";
					var saving_care_image="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+saving_care_image_name+"&modIdentifier=PRODUCTPACKAGEIMG";
					frmIBOpenProdDetnTnC.imgProductPackage.src = saving_care_image;
					frmIBOpenProdDetnTnC.imgProductPackage.isVisible = true;
					frmIBOpenProdDetnTnC.hbxOpnProdDet.isVisible = false;
					frmIBOpenProdDetnTnC.lblOpenActDescSubTitle.text="";
					
					frmIBOpenProdDetnTnC.richTextProdDetIB.text = "";
				}else if(gblSelOpenActProdCode == "221"){
					//Code to handle Saving Care products
					var noFixed_image_name = "NoFixedAcctEN";
					if(kony.i18n.getCurrentLocale() == "th_TH")
						noFixed_image_name = "NoFixedAcctTH";
					var NoFixed_image="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+saving_care_image_name+"&modIdentifier=PRODUCTPACKAGEIMG";
					frmIBOpenProdDetnTnC.imgProductPackage.src = NoFixed_image;
					frmIBOpenProdDetnTnC.imgProductPackage.isVisible = true;
					frmIBOpenProdDetnTnC.hbxOpnProdDet.isVisible = false;
					frmIBOpenProdDetnTnC.lblOpenActDescSubTitle.isVisible = false;
					frmIBOpenProdDetnTnC.richTextProdDetIB.isVisible = false;
				}else if(gblSelOpenActProdCode == "300" || gblSelOpenActProdCode == "301"  || gblSelOpenActProdCode == "302" || gblSelOpenActProdCode == "601" || gblSelOpenActProdCode == "602"){
					frmIBOpenProdDetnTnC.richTextProdDetIB.text = kony.i18n.getLocalizedString('keyTermProdDet');
				} else if(gblSelOpenActProdCode == "659"){
					frmIBOpenProdDetnTnC.richTextProdDetIB.text = kony.i18n.getLocalizedString('keyTermUpProdDet');
				} else if(gblSelOpenActProdCode == "664" || gblSelOpenActProdCode == "666"){
					frmIBOpenProdDetnTnC.richTextProdDetIB.text = kony.i18n.getLocalizedString('keyTermQuickProdDet');
				}
				frmIBOpenProdDetnTnC.btnRight.skin = "btnShare";
				dismissLoadingScreenPopup();
				frmIBOpenProdDetnTnC.show();
			}else{
				dismissLoadingScreenPopup();
				showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
				return false;
			}
	}
}
function IBverifyOTPMyNewAccounts() {
    
    curr_form = kony.application.getCurrentForm();
     var  token ;
     var fromActId;
     var fromActName;
     var fromActNickName;
     var fromFiident;
     var fromActType;
     var toActNum;
     var toActType;
     var toFiident;
     var dreamAmt;
     var transferAmount;
     var fromProdId;
    var inputParam = {};
    if (gblTokenSwitchFlag == true) {
      token = curr_form.tbxToken.text
    } else {
     token = curr_form.txtOTP.text;
    }
	 	if (gblSelProduct == "TMBDreamSavings") {
				    var fdata = frmIBOpenNewSavingsAcc.coverFlowTP.data[gblCWSelectedItem];
                    var fdata1 = frmIBOpenNewDreamAcc.coverFlowTP.data[gblCWSelectedItem];
                    var selvalue = frmIBOpenNewSavingsAcc.btnDreamSavecombo.selectedKeyValue;
                    fromActName = fdata.lblhideaccountName;
                    fromActId = fdata.lblActNoval;
                    fromActNickName = fdata.lblCustName;
                    fromFiident = fdata.lblfiident;
                    fromActType = fdata.lblaccType;
                    fromProdId = fdata.lblProdID;
                    gblFinActivityLogOpenAct["fromActIdFormatted"] = fromActId;
                    fromActId = fromActId.toString().replace(/-/g, "");
                    //var tarAmount = frmIBOpenNewDreamAcc.txtODTargetAmt.text
                   // tarAmount = tarAmount.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(/,/g, "");	
                    
                    var tarAmount = commaFormattedOpenAct(frmIBOpenNewDreamAcc.txtODTargetAmt.text);
                    tarAmount = tarAmount.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
                    
                    gblFinActivityLogOpenAct["fromActId"] = fromActId;
                   	gblFinActivityLogOpenAct["fromActName"] = fromActName;
                   	gblFinActivityLogOpenAct["fromActType"] = fromActType;
                    gblFinActivityLogOpenAct["fromActNickName"] = fromActNickName;
                    gblFinActivityLogOpenAct["fromFiident"] = fromFiident;
                    gblFinActivityLogOpenAct["fromProdId"] = fromProdId
                    gblFinActivityLogOpenAct["toActType"] = "SDA";
                    gblFinActivityLogOpenAct["toActName"] = fromActName;
                    gblFinActivityLogOpenAct["toActNickName"] = frmIBOpenNewSavingsAcc.txtOpenActNicNam.text;
                    gblFinActivityLogOpenAct["toFiident"] = "";
                    gblFinActivityLogOpenAct["toActId"] = "";
                    gblFinActivityLogOpenAct["toProductName"] = frmIBOpenNewDreamAcc.lblDreamSavings.text;
                    var dreamAmt = frmIBOpenNewDreamAccConfirmation.lblOADSAmtVal.text;
                    var dreamMonth = frmIBOpenNewDreamAccConfirmation.lblOADMnthVal.text;
                    dreamAmt = dreamAmt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(/,/g, "").trim();
                    gblFinActivityLogOpenAct["dreamAmt"] = dreamAmt.trim();
                    gblFinActivityLogOpenAct["dreamMonth"] = dreamMonth;
                    
                    gblFinActivityLogOpenAct["dreamdesc"] = frmIBOpenNewDreamAcc.txtODNickNameVal.text;
                    gblFinActivityLogOpenAct["targetAmount"] = frmIBOpenNewDreamAcc.txtODTargetAmt.text.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
                    gblFinActivityLogOpenAct["transferrecdate"] = frmIBOpenNewDreamAccConfirmation.lblOADMnthVal.text;
                    
                    transferAmount = frmIBOpenNewDreamAccConfirmation.lblOADSAmtVal.text;
                    transferAmount = transferAmount.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(/,/g, "");	
                    gblFinActivityLogOpenAct["transferAmount"] = transferAmount;
                   gblFinActivityLogOpenAct["tdInterestRate"] = frmIBOpenProdDetnTnC.lblOpenAccInt.text;
                   gblFinActivityLogOpenAct["channelName"] = "IB";
                   gblFinActivityLogOpenAct["toBranchName"] = frmIBOpenNewDreamAccConfirmation.lblOADSBranchVal.text;
                   gblFinActivityLogOpenAct["openingDate"] = frmIBOpenNewDreamAccConfirmation.lblOpeningDateValue.text;
				   gblFinActivityLogOpenAct["affiliatedAcctNickname"] = "";
				  gblFinActivityLogOpenAct["affiliatedAcctId"] = "";
			 
			validateOTPtextOpenAccountsJavaService(token,  dreamAmt, gblDreamSelData.lblDream, tarAmount,"",gblDreamSelData.lblDreamTargetID,"01",frmIBOpenNewDreamAcc.txtODNickNameVal.text);
		
		}else if (gblSelProduct == "ForTerm") {
				if (gblisDisToActTD == "Y") {
                        toActNum = gblToTermActs.lblActNoval;
                        toActType = gblToTermActs.lblaccType;
                        toActNum = toActNum.toString().replace(/-/g, "");
                        toFiident = gblToTermActs.lblfiident;
                        gblFinActivityLogOpenAct["toActType"] = toActType;
                        gblFinActivityLogOpenAct["affiliatedAcctNickname"] = frmIBOpenNewTermDepositAccConfirmation.lblPayingToName.text;
				  		gblFinActivityLogOpenAct["affiliatedAcctId"] = frmIBOpenNewTermDepositAccConfirmation.lblPayingToAccountType.text;
				  		gblFinActivityLogOpenAct["affiliatedAcctName"] = gblToTermActs.lblhideaccountName;
                    } else {
                        toActNum = "";
                        toActType = "";
                        toFiident = "";
                        gblFinActivityLogOpenAct["toActType"] = "CDA";
                        gblFinActivityLogOpenAct["affiliatedAcctNickname"] = "";
				  		gblFinActivityLogOpenAct["affiliatedAcctId"] = "";
				  		gblFinActivityLogOpenAct["affiliatedAcctName"] = "";
                    }
                    fromActName = gblFromTermActs.lblhideaccountName;
                    fromActId = gblFromTermActs.lblActNoval;
                    fromActNickName = gblFromTermActs.lblCustName;
                    fromFiident = gblFromTermActs.lblfiident;
                    fromActType = gblFromTermActs.lblaccType;
                    gblFinActivityLogOpenAct["fromActIdFormatted"] = fromActId;
                    fromActId = fromActId.toString().replace(/-/g, "");
                    gblFinActivityLogOpenAct["toProductName"] = frmIBOpenNewTermDepositAcc.lblOATDProdName.text;
                    
                    gblFinActivityLogOpenAct["fromActId"] = fromActId;
                    gblFinActivityLogOpenAct["fromActName"] = fromActName;
                    gblFinActivityLogOpenAct["fromActType"] = fromActType;
                    gblFinActivityLogOpenAct["fromActNickName"] = fromActNickName;
                    gblFinActivityLogOpenAct["fromFiident"] = fromFiident;
                    gblFinActivityLogOpenAct["toActName"] = fromActName;
                    gblFinActivityLogOpenAct["toFiident"] = toFiident;
                    gblFinActivityLogOpenAct["toActNickName"] = frmIBOpenNewTermDepositAcc.txtDSNickName.text;
                    gblFinActivityLogOpenAct["toActId"] = toActNum;
                    gblFinActivityLogOpenAct["dreamAmt"] = "";
                    gblFinActivityLogOpenAct["dreamMonth"] = "";
                    gblFinActivityLogOpenAct["fromProdId"] = "";
                    gblFinActivityLogOpenAct["tdInterestRate"] = frmIBOpenNewTermDepositAccConfirmation.lblInterestVal.text.replace("%", "");
                    gblFinActivityLogOpenAct["channelName"] = "IB";
                     gblFinActivityLogOpenAct["toBranchName"] = frmIBOpenNewTermDepositAccConfirmation.lblBranchVal.text;
                    gblFinActivityLogOpenAct["openingDate"] = frmIBOpenNewTermDepositAccConfirmation.lblOpeningDateVal.text;
			
			validateOTPtextOpenAccountsJavaService(token,  "", "", "","","","01","");
		
		}else if (gblSelProduct == "TMBSavingcare") {
					var fdata = frmIBOpenNewSavingsCareAcc.coverFlowTP.data[gblCWSelectedItem];
                    fromActName = fdata.lblhideaccountName;
                    fromActId = fdata.lblActNoval;
                    fromActNickName = fdata.lblCustName;
                    fromFiident = fdata.lblfiident;
                    fromActType = fdata.lblaccType;
                    fromProdId = fdata.lblProdID;
                    gblFinActivityLogOpenAct["fromActIdFormatted"] = fromActId;
                    fromActId = fromActId.toString().replace(/-/g, "");
                    transferAmount = gblFinActivityLogOpenAct["transferAmount"];
                   
                   	gblFinActivityLogOpenAct["toProductName"] = frmIBOpenNewSavingsCareAcc.lblSavinCareSelProdTxt.text;
                   	gblFinActivityLogOpenAct["fromActId"] = fromActId;
                   	gblFinActivityLogOpenAct["fromActName"] = fromActName;
                   	gblFinActivityLogOpenAct["fromActType"] = fromActType;
                   	gblFinActivityLogOpenAct["fromActNickName"] = fromActNickName;
                   	gblFinActivityLogOpenAct["fromFiident"] = fromFiident;
                   	gblFinActivityLogOpenAct["toActType"] = "SDA";
                   	gblFinActivityLogOpenAct["toActNickName"] = frmIBOpenNewSavingsCareAcc.txtNickName.text;
                    gblFinActivityLogOpenAct["toActName"] = fromActName
                    gblFinActivityLogOpenAct["toFiident"] = "";
                    gblFinActivityLogOpenAct["toActId"] = "";
                    gblFinActivityLogOpenAct["dreamAmt"] = "";
                    gblFinActivityLogOpenAct["dreamMonth"] = "";
                    gblFinActivityLogOpenAct["fromProdId"] = fromProdId
                    gblFinActivityLogOpenAct["tdInterestRate"] = frmIBOpenProdDetnTnC.lblOpenAccInt.text;
                    gblFinActivityLogOpenAct["channelName"] = "IB";
                     gblFinActivityLogOpenAct["toBranchName"] = frmIBOpenNewSavingsCareAccConfirmation.lblOASCBranchVal.text;
                    gblFinActivityLogOpenAct["openingDate"] = frmIBOpenNewSavingsCareAccConfirmation.lblOASCOpenDateVal.text;
                    /* gblFinActivityLogOpenAct["Name"] = frmIBOpenNewSavingsCareAccComplete.lblBefNameValCnfrm1.text;
                    gblFinActivityLogOpenAct["Relation"] = frmIBOpenNewSavingsCareAccComplete.lblBefRsCnfrmVal1.text;
                    gblFinActivityLogOpenAct["Percentage"] = frmIBOpenNewSavingsCareAccComplete.lblBefPerCnfrmVal1.text; */
                    gblFinActivityLogOpenAct["affiliatedAcctNickname"] = "";
				  	gblFinActivityLogOpenAct["affiliatedAcctId"] = "";
					
					validateOTPtextOpenAccountsJavaService(token,  "", "", "",gblFinActivityLogOpenAct,"","01","");
		
		}else if (gblSelProduct == "ForUse") {
			
			
					var fdata = frmIBOpenNewSavingsAcc.coverFlowTP.data[gblCWSelectedItem];
                    fromActName = frmIBOpenNewSavingsAccConfirmation.lblActName.text;
                    fromActId = fdata.lblActNoval;
                    fromActNickName = fdata.lblCustName;
                    fromFiident = fdata.lblfiident;
                    fromActType = fdata.lblaccType;
                    fromProdId = fdata.lblProdID;
               //     fromaccType = fdata.lblaccType
                    gblFinActivityLogOpenAct["fromActIdFormatted"] = fromActId;
                    fromActId = fromActId.toString().replace(/-/g, "");
                    toActNickName = frmIBOpenNewSavingsAcc.txtOpenActNicNam.text;
                   
                    gblFinActivityLogOpenAct["fromActId"] = fromActId;
                    gblFinActivityLogOpenAct["fromActName"] = fromActName;
                    gblFinActivityLogOpenAct["fromActType"] = fromActType;
                    gblFinActivityLogOpenAct["fromActNickName"] = fromActNickName;
                    gblFinActivityLogOpenAct["fromFiident"] = fromFiident;
                   
                    gblFinActivityLogOpenAct["fromProdId"] = fromProdId
                    gblFinActivityLogOpenAct["toActNickName"] = toActNickName
                    gblFinActivityLogOpenAct["toActType"] = "SDA";
                    gblFinActivityLogOpenAct["toActName"] = fromActName
                     gblFinActivityLogOpenAct["toProductName"] = frmIBOpenNewSavingsAcc.lblNSProdName.text;
                    
                    gblFinActivityLogOpenAct["toBranchName"] = frmIBOpenNewSavingsAccConfirmation.lblBranchVal.text;
                    gblFinActivityLogOpenAct["openingDate"] = frmIBOpenNewSavingsAccConfirmation.lblOpeningDateVal.text
                    
                    gblFinActivityLogOpenAct["toFiident"] = "";
                    gblFinActivityLogOpenAct["toActId"] = "";
                    gblFinActivityLogOpenAct["dreamAmt"] = "";
                    gblFinActivityLogOpenAct["dreamMonth"] = "";
                    gblFinActivityLogOpenAct["tdInterestRate"] = frmIBOpenProdDetnTnC.lblOpenAccInt.text;
					gblFinActivityLogOpenAct["channelName"] = "IB";
					gblFinActivityLogOpenAct["affiliatedAcctNickname"] = "";
				    gblFinActivityLogOpenAct["affiliatedAcctId"] = "";
			        validateOTPtextOpenAccountsJavaService(token,  "", "", "","","","01","");
		}
		if (gblTokenSwitchFlag == true) 
		{
      		curr_form.tbxToken.text=""
    	}
    	else
    	{
     		curr_form.txtOTP.text="";
    	}
    
}


function IBreqOTPMYNewAccnts() {
	 var inputParams = {};
	var locale = kony.i18n.getCurrentLocale();
	
    frmIBOpenNewTermDepositAccConfirmation.lblOTPinCurr.text = " ";//kony.i18n.getLocalizedString("invalidOTP"); //
	frmIBOpenNewTermDepositAccConfirmation.lblPlsReEnter.text = " "; 
	frmIBOpenNewTermDepositAccConfirmation.hbxOTPincurrect.isVisible = false;
	frmIBOpenNewTermDepositAccConfirmation.hbox866854059301598.isVisible = true;
	frmIBOpenNewTermDepositAccConfirmation.hbox866854059301638.isVisible = true;
	frmIBOpenNewTermDepositAccConfirmation.txtOTP.setFocus(true);
	
	 inputParams["Channel"] = "OpenAccountOnline";
	 inputParams["retryCounterRequestOTP"]=gblRetryCountRequestOTP;
	 inputParams["locale"]=locale;
	 
	 //InputParms for activity logging
	var platformChannel = gblDeviceInfo.name;
	if (platformChannel == "thinclient")
		inputParams["channelId"] = GLOBAL_IB_CHANNEL;
	else
		inputParams["channelId"] = GLOBAL_MB_CHANNEL;
		inputParams["logLinkageId"] = "";
		inputParams["deviceNickName"] = "";
		inputParams["activityFlexValues2"] = "";

	if(gblSelOpenActProdCode == "200" || gblSelOpenActProdCode == "220"  || gblSelOpenActProdCode == "222" ){
	
			
			inputParams["activityTypeID"] = "039";
			inputParams["activityFlexValues1"] = gblSelOpenActProdCode + "+" + frmIBOpenNewSavingsAccConfirmation.lblNSProdName.text;
			inputParams["activityFlexValues3"] = frmIBOpenNewSavingsAccConfirmation.lblNickNameVal.text;
			inputParams["activityFlexValues4"] = gblFinActivityLogOpenAct["transferAmount"];
			inputParams["activityFlexValues5"] = "";
			
	
		//activityLogServiceCall("039", "", "00", "", gblSelOpenActProdCode + "+" + frmIBOpenNewSavingsAccConfirmation.lblNSProdName.text, "",frmIBOpenNewSavingsAccConfirmation.lblNickNameVal.text, gblFinActivityLogOpenAct["transferAmount"], "", ""); //Savings 
		}else if(gblSelOpenActProdCode == "221"){
		
			inputParams["activityTypeID"] = "042";
			inputParams["activityFlexValues1"] = gblSelOpenActProdCode + "+" + frmIBOpenNewSavingsAccConfirmation.lblNSProdName.text;
			inputParams["activityFlexValues3"] = frmIBOpenNewSavingsAccConfirmation.lblNickNameVal.text;
			inputParams["activityFlexValues4"] = gblFinActivityLogOpenAct["transferAmount"];
			inputParams["activityFlexValues5"] = "";
		
		
		//activityLogServiceCall("042", "", "00", "", gblSelOpenActProdCode + "+" + frmIBOpenNewSavingsAccConfirmation.lblNSProdName.text, "",frmIBOpenNewSavingsAccConfirmation.lblNickNameVal.text, gblFinActivityLogOpenAct["transferAmount"], "", "");
		}else if(gblSelOpenActProdCode == "206"){
		
			inputParams["activityTypeID"] = "040";
			inputParams["activityFlexValues1"] = frmIBOpenNewDreamAccConfirmation.lblOADMnthVal.text;
			inputParams["activityFlexValues3"] = frmIBOpenNewDreamAccConfirmation.lblOAMnthlySavActType.text;
			inputParams["activityFlexValues4"] = frmIBOpenNewDreamAccConfirmation.lblOADSAmtVal.text;
			inputParams["activityFlexValues5"] = gblDreamSelData.lblDreamTargetID + "+" + frmIBOpenNewDreamAcc.txtODTargetAmt.text;
		
		}else if(gblSelOpenActProdCode == "203"){
		
		var befPerVthSym = "";
				var befPer = frmIBOpenNewSavingsCareAccConfirmation.lblBefPerCnfrmVal1.text;
				befPer = befPer.replace("%", "");
				var befName = frmIBOpenNewSavingsCareAccConfirmation.lblBefNameValCnfrm1.text + " " + frmIBOpenNewSavingsCareAccConfirmation.lblBefRsCnfrmVal1.text + " " + befPer;
				
				if (frmIBOpenNewSavingsCareAccConfirmation.hbxBefNameCnfrm2.isVisible == true){
				befPerVthSym = frmIBOpenNewSavingsCareAccConfirmation.lblBefPerCnfrmVal2.text;
    			befPerVthSym =befPerVthSym.replace("%", "");
    			befName = befName + "," + frmIBOpenNewSavingsCareAccConfirmation.lblBefNameValCnfrm2.text + " " + frmIBOpenNewSavingsCareAccConfirmation.lblBefRsCnfrmVal2.text + " " + befPerVthSym;
				}
				if (frmIBOpenNewSavingsCareAccConfirmation.hbxBefNameCnfrm3.isVisible == true){
				befPerVthSym = frmIBOpenNewSavingsCareAccConfirmation.lblBefPerCnfrmVal3.text;
    			befPerVthSym =befPerVthSym.replace("%", "");
				befName = befName + "," + frmIBOpenNewSavingsCareAccConfirmation.lblBefNameValCnfrm3.text + " " + frmIBOpenNewSavingsCareAccConfirmation.lblBefRsCnfrmVal3.text + " " + befPerVthSym;;
				}
				if (frmIBOpenNewSavingsCareAccConfirmation.hbxBefNameCnfrm4.isVisible == true){
				befPerVthSym = frmIBOpenNewSavingsCareAccConfirmation.lblBefPerCnfrmVal4.text;
    			befPerVthSym =befPerVthSym.replace("%", "");
				befName = befName + "," + frmIBOpenNewSavingsCareAccConfirmation.lblBefNameValCnfrm4.text + " " + frmIBOpenNewSavingsCareAccConfirmation.lblBefRsCnfrmVal4.text + " " + befPerVthSym;;
				}
				if (frmIBOpenNewSavingsCareAccConfirmation.hbxBefNameCnfrm5.isVisible == true){
				befPerVthSym = frmIBOpenNewSavingsCareAccConfirmation.lblBefPerCnfrmVal5.text;
    			befPerVthSym =befPerVthSym.replace("%", "");
				befName = befName + "," + frmIBOpenNewSavingsCareAccConfirmation.lblBefNameValCnfrm5.text + " " + frmIBOpenNewSavingsCareAccConfirmation.lblBefRsCnfrmVal5.text + " " + befPerVthSym;;
				}
			
			
			inputParams["activityTypeID"] = "081";
			inputParams["activityFlexValues1"] = gblSelOpenActProdCode + "+" + frmIBOpenNewSavingsCareAccConfirmation.lblOASCTitle.text;
			inputParams["activityFlexValues3"] = frmIBOpenNewSavingsCareAccConfirmation.lblOASCNicNamVal.text;
			inputParams["activityFlexValues4"] = gblFinActivityLogOpenAct["transferAmount"];
			inputParams["activityFlexValues5"] = befName;
			
				
		}else if(gblSelOpenActProdCode == "300" || gblSelOpenActProdCode == "301"  || gblSelOpenActProdCode == "302" || gblSelOpenActProdCode == "601" || gblSelOpenActProdCode == "602" || gblSelOpenActProdCode == "659" || gblSelOpenActProdCode == "664" || gblSelOpenActProdCode == "666"){
		
			inputParams["activityTypeID"] = "041";
			inputParams["activityFlexValues1"] = gblSelOpenActProdCode + "+" + frmIBOpenNewTermDepositAccConfirmation.lblTDCnfmTitle.text;
			inputParams["activityFlexValues3"] = frmIBOpenNewTermDepositAccConfirmation.lblOATDNickNameVal.text;
			inputParams["activityFlexValues4"] = gblFinActivityLogOpenAct["transferAmount"];
			inputParams["activityFlexValues5"] = "";
		
		}

	 //End for input params of activity logging
	 
	showLoadingScreenPopup();
	invokeServiceSecureAsync("generateOTPWithUser", inputParams, callBackIBreqOTPIBMYANew); 

}

function callBackIBreqOTPIBMYANew(status, callBackResponse) {
//alert(status)
//alert(callBackResponse["opstatus"])
	if (status == 400) {
		
			
			
			if (callBackResponse["errCode"] == "GenOTPRtyErr00002") {
				//alert("Test1");
				dismissLoadingScreenPopup();
				showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00002"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (callBackResponse["errCode"] == "JavaErr00001") {
				//alert("Test2");
				dismissLoadingScreenPopup();
				showAlertIB(kony.i18n.getLocalizedString("ECJavaErr00001"), kony.i18n.getLocalizedString("info"));
				return false;
			}else if (callBackResponse["errCode"] == "GenOTPRtyErr00001") {
                    dismissLoadingScreenPopup();
                    showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                    return false;
              }
			
			else if (callBackResponse["errCode"] == "GenOTPRtyErr00001") {
                    dismissLoadingScreenPopup();
                    showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                    return false;
           	}
			if (callBackResponse["opstatus"] == 0) {
			//gblOTPdisabletime = kony.os.toNumber(callBackResponse["requestOTPEnableTime"]) * 100;
			gblRetryCountRequestOTP = kony.os.toNumber(callBackResponse["retryCounterRequestOTP"]);
			gblOTPLENGTH = kony.os.toNumber(callBackResponse["otpLength"]);
			
			var reqOtpTimer = kony.os.toNumber(callBackResponse["requestOTPEnableTime"]);
			kony.timer.schedule("OtpTimer", IBOTPTimercallbackOpenAccount, reqOtpTimer, false);
			   
        	    	
          	gblOTPReqLimit=gblOTPReqLimit+1;
          //	setTimeout('OTPTimercallbackOpenAccount()',reqOtpTimer);

			var refVal="";
			for(var d=0;d<callBackResponse["Collection1"].length;d++){
				if(callBackResponse["Collection1"][d]["keyName"] == "pac"){
						refVal=callBackResponse["Collection1"][d]["ValueString"];
						break;
					}
				}
				
				if(gblOnClickReq){
				
						if(gblSelProduct == "ForUse"){
							//frmIBOpenNewSavingsAccConfirmation.lblOpeningDateVal.text=openingDate
							frmIBOpenNewSavingsAccConfirmation.btnRequest.skin = btnIBREQotp;
            				frmIBOpenNewSavingsAccConfirmation.btnRequest.focusSkin = btnIBREQotp;
            				frmIBOpenNewSavingsAccConfirmation.btnRequest.onClick = ""; 
            				frmIBOpenNewSavingsAccConfirmation.lblBankRefNoVal.text = " "+refVal;			
							frmIBOpenNewSavingsAccConfirmation.txtOTP.maxTextLength = gblOTPLENGTH
							frmIBOpenNewSavingsAccConfirmation.hbox866854059250156.setVisibility(true);
							frmIBOpenNewSavingsAccConfirmation.lblRequest.setVisibility(true);
							frmIBOpenNewSavingsAccConfirmation.tbxToken.text = "";
							frmIBOpenNewSavingsAccConfirmation.txtOTP.text = "";
							dismissLoadingScreenPopup();
							if(gblTokenSwitchFlag == true && gblSwitchToken == false){
							  frmIBOpenNewSavingsAccConfirmation.tbxToken.setFocus(true);
							}else{
							  frmIBOpenNewSavingsAccConfirmation.txtOTP.setFocus(true);
							}
							
						//	frmIBOpenNewSavingsAccConfirmation.txtOTP.placeholder = kony.i18n.getLocalizedString("enterOTP");
						    gblOpenAcctConfirm = "verOTP";
						//	curr_form.lblRefNumValue.text = callBackResponse["Collection1"][8]["ValueString"]; //callBackResponse["pac"]; //callBackResponse["bankRefNo"];
							frmIBOpenNewSavingsAccConfirmation.lblOTPMblNum.text =" " +maskingIB(gblPHONENUMBER);
							
						 frmIBOpenNewSavingsAccConfirmation.show();
							
						}else if(gblSelProduct == "TMBDreamSavings"){
							//frmIBOpenNewDreamAccConfirmation.lblOpeningDateValue.text=	openingDate;
							frmIBOpenNewDreamAccConfirmation.btnRequest.skin = btnIBREQotp;
            				frmIBOpenNewDreamAccConfirmation.btnRequest.focusSkin = btnIBREQotp;
            				frmIBOpenNewDreamAccConfirmation.btnRequest.onClick = ""; 
            				frmIBOpenNewDreamAccConfirmation.lblBankRefNoVal.text =" " +refVal;			
							frmIBOpenNewDreamAccConfirmation.txtOTP.maxTextLength = gblOTPLENGTH
						//	frmIBOpenNewDreamAccConfirmation.lblTransRefNo.setVisibility(false);
						//	curr_form.lblRefNumValue.text = callBackResponse["Collection1"][8]["ValueString"]; //callBackResponse["pac"]; //callBackResponse["bankRefNo"];
							frmIBOpenNewDreamAccConfirmation.lblOTPMblNum.text = " " +maskingIB(gblPHONENUMBER);
							frmIBOpenNewDreamAccConfirmation.hbox866854059250156.setVisibility(true);
							frmIBOpenNewDreamAccConfirmation.btnedit.setVisibility(true);
							frmIBOpenNewDreamAccConfirmation.lblRequest.setVisibility(true);
							dismissLoadingScreenPopup();
							frmIBOpenNewDreamAccConfirmation.tbxToken.text = "";
							frmIBOpenNewDreamAccConfirmation.txtOTP.text = "";
							if(gblTokenSwitchFlag == true && gblSwitchToken == false){
							  frmIBOpenNewDreamAccConfirmation.tbxToken.setFocus(true);
							}else{
							  frmIBOpenNewDreamAccConfirmation.txtOTP.setFocus(true);
							}
							gblOpenAcctConfirm = "verOTP";
							//frmIBOpenNewDreamAccConfirmation.txtOTP.placeholder = kony.i18n.getLocalizedString("enterOTP");
							frmIBOpenNewDreamAccConfirmation.show();
									
						}else if(gblSelProduct == "TMBSavingcare"){
						   // frmIBOpenNewSavingsCareAccConfirmation.lblOASCOpenDateVal.text=openingDate
							frmIBOpenNewSavingsCareAccConfirmation.btnRequest.skin = btnIBREQotp;
            				frmIBOpenNewSavingsCareAccConfirmation.btnRequest.focusSkin = btnIBREQotp;
            				frmIBOpenNewSavingsCareAccConfirmation.btnRequest.onClick = ""; 
            				frmIBOpenNewSavingsCareAccConfirmation.lblBankRefNoVal.text =" " +refVal;			
							frmIBOpenNewSavingsCareAccConfirmation.txtOTP.maxTextLength = gblOTPLENGTH
						//	curr_form.lblRefNumValue.text = callBackResponse["Collection1"][8]["ValueString"]; //callBackResponse["pac"]; //callBackResponse["bankRefNo"];
							frmIBOpenNewSavingsCareAccConfirmation.lblOTPMblNum.text =" " + maskingIB(gblPHONENUMBER);
							//frmIBOpenNewSavingsCareAccConfirmation.txtOTP.placeholder = kony.i18n.getLocalizedString("enterOTP");
						    frmIBOpenNewSavingsCareAccConfirmation.hbox866854059250156.setVisibility(true);
						    frmIBOpenNewSavingsCareAccConfirmation.lblRequest.setVisibility(true);
						    gblOpenAcctConfirm = "verOTP";
						   
							dismissLoadingScreenPopup();
							frmIBOpenNewSavingsCareAccConfirmation.tbxToken.text = "";
							frmIBOpenNewSavingsCareAccConfirmation.txtOTP.text = "";
							if(gblTokenSwitchFlag == true && gblSwitchToken == false){
							  frmIBOpenNewSavingsCareAccConfirmation.tbxToken.setFocus(true);
							}else{
							  frmIBOpenNewSavingsCareAccConfirmation.txtOTP.setFocus(true);
							}
							frmIBOpenNewSavingsCareAccConfirmation.show();
						//	IBonClickPreShowSCConfirm();				
						}else if(gblSelProduct == "ForTerm"){
							//frmIBOpenNewTermDepositAccConfirmation.lblOpeningDateVal.text=	openingDate	
							frmIBOpenNewTermDepositAccConfirmation.btnRequest.skin = btnIBREQotp;
            				frmIBOpenNewTermDepositAccConfirmation.btnRequest.focusSkin = btnIBREQotp;
            				frmIBOpenNewTermDepositAccConfirmation.btnRequest.onClick = ""; 
            				frmIBOpenNewTermDepositAccConfirmation.lblBankRefNoVal.text = " " +refVal;			
							frmIBOpenNewTermDepositAccConfirmation.txtOTP.maxTextLength = gblOTPLENGTH
						//	curr_form.lblRefNumValue.text = callBackResponse["Collection1"][8]["ValueString"]; //callBackResponse["pac"]; //callBackResponse["bankRefNo"];
							frmIBOpenNewTermDepositAccConfirmation.lblOTPMblNum.text =" " + maskingIB(gblPHONENUMBER);
							frmIBOpenNewTermDepositAccConfirmation.hbox866854059250156.setVisibility(true);
							frmIBOpenNewTermDepositAccConfirmation.lblRequest.setVisibility(true);
							
							dismissLoadingScreenPopup();
							frmIBOpenNewTermDepositAccConfirmation.tbxToken.text = "";
							frmIBOpenNewTermDepositAccConfirmation.txtOTP.text = "";
							if(gblTokenSwitchFlag == true && gblSwitchToken == false){
							  frmIBOpenNewTermDepositAccConfirmation.tbxToken.setFocus(true);
							}else{
							  frmIBOpenNewTermDepositAccConfirmation.txtOTP.setFocus(true);
							}
							gblOpenAcctConfirm = "verOTP";
							frmIBOpenNewTermDepositAccConfirmation.show();
						}
				
				}else{		
					 			
		   		curr_form = kony.application.getCurrentForm();
				curr_form.btnRequest.skin = btnIBREQotp;
            	curr_form.btnRequest.focusSkin = btnIBREQotp;
            	curr_form.btnRequest.onClick = ""; 
            	curr_form.lblBankRefNoVal.text = " "+refVal;			
				//curr_form = kony.application.getCurrentForm();
				curr_form.txtOTP.maxTextLength = gblOTPLENGTH;
				curr_form.txtOTP.text = "";
			//	curr_form.lblRefNumValue.text = callBackResponse["Collection1"][8]["ValueString"]; //callBackResponse["pac"]; //callBackResponse["bankRefNo"];
				curr_form.lblOTPMblNum.text =" " + maskingIB(gblPHONENUMBER);
				dismissLoadingScreenPopup();
				}
			
		} else {
			dismissLoadingScreenPopup();
			alert("Error " + callBackResponse["errMsg"]);
		}
	} else {
		if (status == 300) {
			dismissLoadingScreenPopup();
			alert("Error");
		}
	}
}

function IBonclickSavingsAccountCoverFlow()
{

var fdata=frmIBOpenNewSavingsAcc.coverFlowTP.data[gblCWSelectedItem];
var accType;
frmIBOpenNewSavingsAccConfirmation.lblAccNSName.text=fdata.lblCustName;
gblFinActivityLogOpenAct["PrdctOpoenNSEN"] = fdata.hiddenProdENGSA
gblFinActivityLogOpenAct["PrdctOpoenNSTH"] = fdata.hiddenproductThaiSA
//alert("aaaa"+ fdata.hiddenProdENGSA)

   var accTypeDreamOpen;
        savingMB="";
        currentMB="";

       
		if (fdata.lblaccType == "SDA") {
            savingMB = "SDA";
            accTypeDreamOpen=kony.i18n.getLocalizedString("SavingMB")
        } else if (fdata.lblaccType == "DDA") {
            currentMB = "DDA";
            accTypeDreamOpen=kony.i18n.getLocalizedString("CurrentMB");
        }
        frmIBOpenNewDreamAccConfirmation.lblOAMnthlySavNum.text = accTypeDreamOpen;
    

frmIBOpenNewSavingsAccConfirmation.lblActNoval.text=accTypeDreamOpen;
frmIBOpenNewSavingsAccConfirmation.lblIBOpenAccCnfmBalAmt.text=fdata.lblBalance;
frmIBOpenNewSavingsAccConfirmation.imgNoFix.src=fdata.img1;
frmIBOpenNewSavingsAccConfirmation.lblBranchVal.text=fdata.lblRemFeeval;
frmIBOpenNewSavingsAccConfirmation.lblActName.text=fdata.lblhideaccountName;
frmIBOpenNewSavingsAccConfirmation.lblOpeningDateVal.text=fdata.lblcreatedDate;
frmIBOpenNewSavingsAccConfirmation.lblNFAccType.text = fdata.lblActNoval;
frmIBOpenNewSavingsAcc.lblFromAccount.text = fdata.lblCustName;
frmIBOpenNewSavingsAcc.lblAccNo.text=fdata.lblActNoval;
frmIBOpenNewSavingsAcc.imageAcc.src=fdata.img1;
frmIBOpenNewSavingsAcc.imageAcc.setVisibility(true);
frmIBOpenNewSavingsAcc.vbox866794452610619.setVisibility(false);
gblFinActivityLogOpenAct["PrdctOpoenEN"] = fdata.hiddenProdENG
gblFinActivityLogOpenAct["PrdctOpoenTH"] = fdata.hiddenproductThai


gblOnClickCoverflow=true;
IBcloseRightPanelOpenAccount();
}

function IBcloseRightPanelOpenAccount(){
		var imageTmb = new kony.ui.Image2({
			        "id": "imageTmb",
			        "isVisible": true,
			        "src": "tmb_logo_blue_new.png",
			        "imageWhenFailed": null,
			        "imageWhileDownloading": null
			   }, {
			        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
			        "margin": [0, 35, 0, 0],
			        "padding": [0, 0, 0, 0],
			        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
			        "referenceWidth": null,
			        "referenceHeight": null,
			        "marginInPixel": false,
			        "paddingInPixel": false,
			        "containerWeight": 100
			   }, {
			        "toolTip": null
		});
		if(frmIBOpenNewSavingsAcc.imageTmb == undefined){	  
			frmIBOpenNewSavingsAcc.vbxBPRight.add(imageTmb);
		}		
		frmIBOpenNewSavingsAcc.hbxCoverFlowTP.setVisibility(false);				
		frmIBOpenNewSavingsAcc.imageTmb.setVisibility(true);

}

function IBonclickSavingsCareAccountCoverFlow()
{
gblOnClickCoverflow="SAVINGSCARE"
var accType;
var fdata=frmIBOpenNewSavingsCareAcc.coverFlowTP.data[gblCWSelectedItem];

  var accTypeCareOpen;
        savingMBCareOpen="";
        currentMBCareOpen="";

       
		if (fdata.lblaccType == "SDA") {
            savingMBCareOpen = "SDA";
            accTypeCareOpen=kony.i18n.getLocalizedString("SavingMB")
        } else if (fdata.lblaccType == "DDA") {
            currentMBCareOpen = "DDA";
            accTypeCareOpen=kony.i18n.getLocalizedString("CurrentMB");
        }	
		
gblFinActivityLogOpenAct["PrdctOpoenCareEN"] = fdata.hiddenProdENGSA
gblFinActivityLogOpenAct["PrdctOpoenCareTH"] = fdata.hiddenproductThaiSA

frmIBOpenNewSavingsCareAccConfirmation.lblOASCFrmNameAck.text=fdata.lblCustName;
frmIBOpenNewSavingsCareAccConfirmation.lblOASCFrmNumAck.text=accTypeCareOpen;
frmIBOpenNewSavingsCareAccConfirmation.lblOASCFrmNum.text = fdata.lblActNoval; 
frmIBOpenNewSavingsCareAccConfirmation.imgOASCFrmPicAck.src=fdata.img1;
frmIBOpenNewSavingsCareAccConfirmation.lblOASCBranchVal.text=fdata.lblRemFeeval;
frmIBOpenNewSavingsCareAccConfirmation.lblOASCActNameVal.text=fdata.lblhideaccountName;
//frmIBOpenNewSavingsCareAccConfirmation.lblOASCOpenDateVal.text=fdata.lblcreatedDate;
frmIBOpenNewSavingsCareAccConfirmation.lblBalance.text=fdata.lblBalance;
frmIBOpenNewSavingsCareAcc.lblFromAccount.text = fdata.lblCustName;
frmIBOpenNewSavingsCareAcc.lblAccNo.text=fdata.lblActNoval;
frmIBOpenNewSavingsCareAcc.imageAcc.src=fdata.img1;

}


//function for NEXT OF SAVINGS ACCOUNT
function IBonClickNextselAct() {

	frmIBOpenNewSavingsAccConfirmation.lblAddressHeader.setVisibility(false);
	frmIBOpenNewSavingsAccConfirmation.hbxAddress.setVisibility(false);
	frmIBOpenNewSavingsAccConfirmation.line474106367283368.setVisibility(false);
	frmIBOpenNewSavingsAccConfirmation.line476132054113112.setVisibility(false);
	frmIBOpenNewSavingsAccComplete.lblAddressHeader.setVisibility(false);
	frmIBOpenNewSavingsAccComplete.hbxAddress.setVisibility(false);
	frmIBOpenNewSavingsAccComplete.line474106367283368.setVisibility(true);
	frmIBOpenNewSavingsAccConfirmation.hbxCardType.setVisibility(false);
	frmIBOpenNewSavingsAccConfirmation.hbxCardFee.setVisibility(false);
	var textformat=frmIBOpenNewSavingsAcc.txtOpenActNicNam.text;
	//var patrn = /^[a-zA-Z0-9@.-_\\s]+$/;
	//var matching = patrn.test(textformat);
	//if ((textformat == null) || (textformat == "") || (!matching) || (textformat.length<3) || (textformat.length>20)) {
	if(NickNameValid(textformat.trim()) == false) {
		showAlert(kony.i18n.getLocalizedString("keyOpenAccntNickname"), kony.i18n.getLocalizedString("info"));
		frmIBOpenNewSavingsAcc.txtOpenActNicNam.skin = "txtErrorBG";
		return false;
	}
	
	if (frmIBOpenNewSavingsAcc.lblFromAccount.text == "" || frmIBOpenNewSavingsAcc.lblFromAccount.text == null) {
        showAlert(kony.i18n.getLocalizedString("keyIBTransferFromSelect"), kony.i18n.getLocalizedString("info"));
        return false;
    }
	
    var enteredAmount;
    var isCrtFormt;
    var fdata = frmIBOpenNewSavingsAcc.coverFlowTP.data[gblCWSelectedItem];
    var hiddenActBal = fdata.lblavailbal;
    if (gblSelProduct == "TMBDreamSavings") {
      
      	//MIB-4723 No Fixed Account cannot open Dream Saving Account
      	if (fdata.lblProdID == "221") {
            showAlertIB(kony.i18n.getLocalizedString("keyWarnOpenAccountDS"), kony.i18n.getLocalizedString("info"));
            return false;  
        }
      
		enteredAmount = frmIBOpenNewSavingsAcc.txtDSAmountSel.text;
        isCrtFormt = amountValidationMBOpenAct(enteredAmount);
          var accTypeDreamOpen;
        savingMB="";
        currentMB="";

       
		if (fdata.lblaccType == "SDA") {
            savingMB = "SDA";
            accTypeDreamOpen=kony.i18n.getLocalizedString("SavingMB");
        } else if (fdata.lblaccType == "DDA") {
            currentMB = "DDA";
            accTypeDreamOpen=kony.i18n.getLocalizedString("CurrentMB");
        }
        
	 var  removetar= frmIBOpenNewDreamAcc.lbltargettext.text;
	   var targettextamnt = removetar .replace(/,/g, "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
	    var mnthtext = frmIBOpenNewSavingsAcc.txtDSAmountSel.text;
	  	 mnthtext = mnthtext.replace(kony.i18n.getLocalizedString("currencyThaiBaht"),"").replace("/", "").replace(/,/g, "").replace(kony.i18n.getLocalizedString("keyCalendarMonth"), "");
	    targetamnt = kony.os.toNumber(targettextamnt);
	    mnthtextamnt = kony.os.toNumber(mnthtext);
	    var hiddenActBal = fdata.lblavailbal;
	  
		if ((mnthtextamnt) < (kony.os.toNumber(gblMinOpenAmt))) {
            showAlert(kony.i18n.getLocalizedString("keyMinLimitAmt"), kony.i18n.getLocalizedString("info"));
            frmIBOpenNewSavingsAcc.txtDSAmountSel.skin = "txtErrorBG";
            return false;
        }
        if (gblMaxOpenAmt != "No Limit") {
            if ((mnthtextamnt) > (kony.os.toNumber(gblMaxOpenAmt))) {
                showAlert(kony.i18n.getLocalizedString("keyMaxLimitAmt"), kony.i18n.getLocalizedString("info"));
                frmIBOpenNewSavingsAcc.txtDSAmountSel.skin = "txtErrorBG";
                return false;
            }
        }

        var nickName = IBopenActNickNameUniq(frmIBOpenNewSavingsAcc.txtOpenActNicNam.text);
        if (nickName == false) {
            showAlert(kony.i18n.getLocalizedString("keyuniqueNickName"), kony.i18n.getLocalizedString("info"));
            frmIBOpenNewSavingsAcc.txtOpenActNicNam.skin = "txtErrorBG";
            return false;
        }
        if(frmIBOpenNewSavingsAcc.btnDreamSavecombo.selectedKeyValue[0]== "keyMBTransferEvryMnth"){
		 showAlert(kony.i18n.getLocalizedString("keypleaseSelecttransfer"), kony.i18n.getLocalizedString("info"));
	   	 return false;
		}
		
		
        frmIBOpenNewDreamAccConfirmation.imgDrm.src = producticonfordream;
        frmIBOpenNewDreamAccConfirmation.imgOADreamDetail.src =frmIBOpenNewDreamAcc.imgDream.src;
        frmIBOpenNewDreamAccConfirmation.lblMyDream.text = frmIBOpenNewSavingsAcc.lblNSProdName.text;
        frmIBOpenNewDreamAccConfirmation.lblOADSNickNameVal.text = frmIBOpenNewSavingsAcc.txtOpenActNicNam.text;
        //	frmIBOpenNewDreamAccConfirmation.lblOADSActNameVal.text = frmIBOpenNewDreamAcc.txtODTargetAmt.text;
        //		frmIBOpenNewDreamAccConfirmation.lblOADSBranchVal.text = frmIBOpenNewDreamAcc.txtODTargetAmt.text;
        frmIBOpenNewDreamAccConfirmation.lblOADSAmtVal.text = frmIBOpenNewDreamAcc.txtODMnthSavAmt.text;
        var selvalue = frmIBOpenNewSavingsAcc.btnDreamSavecombo.selectedKeyValue;
        frmIBOpenNewDreamAccConfirmation.lblOADMnthVal.text = selvalue[1];
        //	alert(frmIBOpenNewSavingsAcc.btnDreamSavecombo.selectedKeyValue)
        var fdata = frmIBOpenNewSavingsAcc.coverFlowTP.data[gblCWSelectedItem];
        frmIBOpenNewDreamAccConfirmation.lblOAMnthlySavName.text = fdata.lblCustName;
        frmIBOpenNewDreamAccConfirmation.lblOAMnthlySavBal.text = fdata.lblBalance;
        frmIBOpenNewDreamAccConfirmation.imgOAMnthlySavPic.src = fdata.img1;
        frmIBOpenNewDreamAccConfirmation.lblDreamOpenActNameVal.text = fdata.lblhideaccountName;
        
        
        frmIBOpenNewDreamAccConfirmation.lblOAMnthlySavNum.text = accTypeDreamOpen;
        frmIBOpenNewDreamAccConfirmation.lblOAMnthlySavActType.text = fdata.lblActNoval;
        frmIBOpenNewDreamAccConfirmation.lblOADSBranchVal.text = fdata.lblRemFeeval;
        if (kony.i18n.getCurrentLocale() == "en_US") {
            frmIBOpenNewDreamAccConfirmation.lblInterest.text = gblFinActivityLogOpenAct["intersetRateLabelEN"] + ":";
               frmIBOpenNewDreamAccConfirmation.lblDreamSavings.text = gblFinActivityLogOpenAct["prodNameEN"];
              
        } else {
            frmIBOpenNewDreamAccConfirmation.lblInterest.text = gblFinActivityLogOpenAct["intersetRateLabelTH"] + ":";
             frmIBOpenNewDreamAccConfirmation.lblDreamSavings.text = gblFinActivityLogOpenAct["prodNameTH"];
             
        }
        
        	gblFinActivityLogOpenAct["branchEN"] = fdata.hiddenBranchEN;
			gblFinActivityLogOpenAct["branchTH"] = fdata.hiddenBranchTH;
			gblFinActivityLogOpenAct["nickTH"] = fdata.hiddenproductThai;
			gblFinActivityLogOpenAct["nickEN"] = fdata.hiddenProdENG; 
			gblFinActivityLogOpenAct["actTypeFromOpen"] = fdata.lblaccType; 
        
        var transferDreamAmount = frmIBOpenNewDreamAccConfirmation.lblOADSAmtVal.text;
        transferDreamAmount = transferDreamAmount.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(/,/g, ""); 
        gblFinActivityLogOpenAct["transferAmount"] = transferDreamAmount;
        
       var  fromActId = fdata.lblActNoval;
       var fromActType = fdata.lblaccType;
       fromActId = fromActId.toString().replace(/-/g, "");
        frmIBOpenNewSavingsAcc.txtDSAmountSel.skin = "txtIB20pxBlack";
   		frmIBOpenNewSavingsAcc.txtOpenActNicNam.skin = "txtIB20pxBlack";
   		
        IBcheckIsEligibleToOpenAct(fromActId, fromActType);
       
	} else if (gblSelProduct == "ForUse") {
		
			
        enteredAmount = frmIBOpenNewSavingsAcc.txtOpenActSelAmt.text;
        enteredAmount = enteredAmount.replace(kony.i18n.getLocalizedString("currencyThaiBaht"),"").replace(/,/g, "");
        gblFinActivityLogOpenAct["transferAmount"] = enteredAmount;
        
        
        var fdata = frmIBOpenNewSavingsAcc.coverFlowTP.data[gblCWSelectedItem];
        var hiddenActBal = fdata.lblavailbal;
        isCrtFormt = amountValidationMBOpenAct(enteredAmount);
        if (!isCrtFormt) {
            showAlert(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), kony.i18n.getLocalizedString("info"));
            frmIBOpenNewSavingsAcc.txtOpenActSelAmt.text = "";
            frmIBOpenNewSavingsAcc.txtOpenActSelAmt.skin = "txtErrorBG";
            return false;
        }

        if ((kony.os.toNumber(enteredAmount)) < (kony.os.toNumber(gblMinOpenAmt))) {
            showAlert(kony.i18n.getLocalizedString("keyMinLimitAmt"), kony.i18n.getLocalizedString("info"));
            frmIBOpenNewSavingsAcc.txtOpenActSelAmt.skin = "txtErrorBG";
            return false;
        }
        if (gblMaxOpenAmt != "No Limit") {
            if ((kony.os.toNumber(enteredAmount)) > (kony.os.toNumber(gblMaxOpenAmt))) {
                showAlert(kony.i18n.getLocalizedString("keyMaxLimitAmt"), kony.i18n.getLocalizedString("info"));
                frmIBOpenNewSavingsAcc.txtOpenActSelAmt.skin = "txtErrorBG";
                return false;
            }
        }

        if ((kony.os.toNumber(enteredAmount)) > (kony.os.toNumber(hiddenActBal))) {
            showAlert(kony.i18n.getLocalizedString("keyAmtLessThanAvaBal"), kony.i18n.getLocalizedString("info"));
            frmIBOpenNewSavingsAcc.txtOpenActSelAmt.skin = "txtErrorBG";
            return false;
        }

        var nickName = IBopenActNickNameUniq(frmIBOpenNewSavingsAcc.txtOpenActNicNam.text);
        if (nickName == false) {
            showAlert( kony.i18n.getLocalizedString("keyuniqueNickName"), kony.i18n.getLocalizedString("info"));
            frmIBOpenNewSavingsAcc.txtOpenActNicNam.skin = "txtErrorBG";
            return false;
        }
    
    frmIBOpenNewSavingsAcc.txtOpenActSelAmt.skin = "txtIB20pxBlack";
    frmIBOpenNewSavingsAcc.txtOpenActNicNam.skin = "txtIB20pxBlack";
    frmIBOpenNewSavingsAccConfirmation.lblNSProdName.text = frmIBOpenNewSavingsAcc.lblNSProdName.text;
    frmIBOpenNewSavingsAccConfirmation.imgNSProdName.src = frmIBOpenNewSavingsAcc.imgNSProdName.src;
    frmIBOpenNewSavingsAccConfirmation.lblConfirmation.text = kony.i18n.getLocalizedString("Confirmation");
    frmIBOpenNewSavingsAccConfirmation.btnConfirm.text = kony.i18n.getLocalizedString("keyConfirm");
    frmIBOpenNewSavingsAccConfirmation.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
    frmIBOpenNewSavingsAccConfirmation.lblNickNameVal.text = frmIBOpenNewSavingsAcc.txtOpenActNicNam.text;
    frmIBOpenNewSavingsAccConfirmation.lblOpenAmtVal.text = commaFormatted(parseFloat(removeCommos(gblFinActivityLogOpenAct["transferAmount"])).toFixed(2)) +" "+ kony.i18n.getLocalizedString("currencyThaiBaht");
        
   if(gblOpenActList["FOR_USE_PRODUCT_CODES"] != null && gblOpenActList["FOR_USE_PRODUCT_CODES"] != undefined && gblOpenActList["FOR_USE_PRODUCT_CODES"].indexOf(gblSelOpenActProdCode) > 0)
    {
  	    if(gblIssueOnlineCard == "1"){
		    frmIBOpenNewSavingsAccConfirmation.lblAddressHeader.setVisibility(true);
			frmIBOpenNewSavingsAccConfirmation.hbxAddress.setVisibility(true);
			frmIBOpenNewSavingsAccConfirmation.line474106367283368.setVisibility(true);
			frmIBOpenNewSavingsAccComplete.lblAddressHeader.text = appendColon(kony.i18n.getLocalizedString("keyAddressMailingCard"));
			frmIBOpenNewSavingsAccComplete.lblAddressHeader.setVisibility(true);
			frmIBOpenNewSavingsAccComplete.hbxAddress.setVisibility(true);
			frmIBOpenNewSavingsAccConfirmation.hbxCardType.setVisibility(true);
			frmIBOpenNewSavingsAccConfirmation.hbxCardFee.setVisibility(true);
			var AddressLine1="";
				var AddressLine2="";
				frmIBOpenProdMailAddressEdit.hboxEdit.setVisibility(false);
				frmIBOpenProdMailAddressEdit.hbxcnf2.setVisibility(true);
				if (province == kony.i18n.getLocalizedString("BangkokThaiValueProfile")){
					AddressLine1 =	gblAddress1Value + " " + gblAddress2Value + " " + kony.i18n.getLocalizedString("gblsubDtPrefixThaiB") + gblsubdistrictValue; 
					AddressLine2 =  kony.i18n.getLocalizedString("gblDistPrefixThaiB") + gbldistrictValue + " " + gblStateValue + " " + gblzipcodeValue; 
				}	
				else{
					AddressLine1 = gblAddress1Value + " " + gblAddress2Value + " " + kony.i18n.getLocalizedString("gblsubDtPrefixThai") + "." + gblsubdistrictValue;
					AddressLine2 = kony.i18n.getLocalizedString("gblDistPrefixThai") + "." + gbldistrictValue + " " + gblStateValue + " " + gblzipcodeValue;
				}
				//frmIBOpenNewSavingsAccConfirmation.clblContactAdd1.text=kony.i18n.getLocalizedString("keyContactAddress");
				frmIBOpenNewSavingsAccConfirmation.lblAddressHeader.text = appendColon(kony.i18n.getLocalizedString("keyAddressMailingCard"));
				frmIBOpenNewSavingsAccConfirmation.clblContactAdd2.text = AddressLine1;
				frmIBOpenNewSavingsAccConfirmation.clblContactAdd3.text = AddressLine2;
				frmIBOpenNewSavingsAccConfirmation.lblCardfeeValue.text = commaFormatted(parseFloat(removeCommos(gblFinActivityLogOpenAct["cardFee"])).toFixed(2)) +" "+kony.i18n.getLocalizedString("currencyThaiBaht");
				frmIBOpenNewSavingsAccConfirmation.lblCardTypeValue.text = gblFinActivityLogOpenAct["cardType"];
				frmIBOpenNewSavingsAccComplete.clblContactAdd1.text=kony.i18n.getLocalizedString("keyContactAddress");
				frmIBOpenNewSavingsAccComplete.clblContactAdd2.text = AddressLine1;
				frmIBOpenNewSavingsAccComplete.clblContactAdd3.text = AddressLine2;
				frmIBOpenNewSavingsAccComplete.lblCardfeeValue.text = commaFormatted(parseFloat(removeCommos(gblFinActivityLogOpenAct["cardFee"])).toFixed(2)) +" "+kony.i18n.getLocalizedString("currencyThaiBaht");
				frmIBOpenNewSavingsAccComplete.lblCardTypeValue.text = gblFinActivityLogOpenAct["cardType"];
		}	
	}else{
	 	frmIBOpenNewSavingsAccConfirmation.lblAddressHeader.setVisibility(false);
		frmIBOpenNewSavingsAccConfirmation.hbxAddress.setVisibility(false);
		frmIBOpenNewSavingsAccConfirmation.hbxCardType.setVisibility(false);
		frmIBOpenNewSavingsAccConfirmation.hbxCardFee.setVisibility(false);
	} 
    var fdata = frmIBOpenNewSavingsAcc.coverFlowTP.data[gblCWSelectedItem];
    var accType;
    frmIBOpenNewSavingsAccConfirmation.lblAccNSName.text = fdata.lblCustName;
 
      var accTypeSavingOpen;
        savingMBSavingOpen="";
        currentMBSAvingOpen="";

       
		if (fdata.lblaccType == "SDA") {
            savingMBSavingOpen = "SDA";
            accTypeSavingOpen=kony.i18n.getLocalizedString("SavingMB");
        } else if (fdata.lblaccType == "DDA") {
            currentMBSAvingOpen = "DDA";
            accTypeSavingOpen=kony.i18n.getLocalizedString("CurrentMB");
        }
        
     gblFinActivityLogOpenAct["branchEN"] = fdata.hiddenBranchEN;
	 gblFinActivityLogOpenAct["branchTH"] = fdata.hiddenBranchTH;
	 gblFinActivityLogOpenAct["nickTH"] = fdata.hiddenproductThaiAssign;
	 gblFinActivityLogOpenAct["nickEN"] = fdata.hiddenprodENGAssign; 
	 gblFinActivityLogOpenAct["actTypeFromOpen"] = fdata.lblaccType;  
        
    frmIBOpenNewSavingsAccConfirmation.lblActNoval.text = accTypeSavingOpen;
    frmIBOpenNewSavingsAccConfirmation.lblIBOpenAccCnfmBalAmt.text = fdata.lblBalance;
    frmIBOpenNewSavingsAccConfirmation.imgNoFix.src = fdata.img1;
    //frmIBOpenNewSavingsAccConfirmation.btnRight.skin = btnEdit;
 //alert("on click next"+savingMBSavingOpen)
    if (kony.i18n.getCurrentLocale() == "en_US") {
        frmIBOpenNewSavingsAccConfirmation.lblInterestRate.text = gblFinActivityLogOpenAct["intersetRateLabelEN"] + ":";
         frmIBOpenNewSavingsAccConfirmation.lblNSProdName.text = gblFinActivityLogOpenAct["prodNameEN"];
    } else {
        frmIBOpenNewSavingsAccConfirmation.lblInterestRate.text = gblFinActivityLogOpenAct["intersetRateLabelTH"] + ":";
         frmIBOpenNewSavingsAccConfirmation.lblNSProdName.text = gblFinActivityLogOpenAct["prodNameTH"];
    }
	
    	IBinvokePartyInquiryOpenAct();
	}
}

function genOTPOpenAccounts() {
 
  		//Added code to fix ENH_87:DEF340 - OTP Incorrect inline message issue
											                    
        if (gblSelProduct == "TMBDreamSavings") {
       		frmIBOpenNewDreamAccConfirmation.lblOTPinCurr.text = "";
            frmIBOpenNewDreamAccConfirmation.lblPlsReEnter.text = ""; 
            frmIBOpenNewDreamAccConfirmation.hbxOTPincurrect.isVisible = false;
            frmIBOpenNewDreamAccConfirmation.hbox866854059301598.isVisible = true;
            frmIBOpenNewDreamAccConfirmation.hbox866854059301638.isVisible = true;
        } else if (gblSelProduct == "TMBSavingcare") {
        //Not in Scope
       
        } else if (gblSelProduct == "ForTerm") {
            frmIBOpenNewTermDepositAccConfirmation.lblOTPinCurr.text = "";
            frmIBOpenNewTermDepositAccConfirmation.lblPlsReEnter.text = ""; 
            frmIBOpenNewTermDepositAccConfirmation.hbxOTPincurrect.isVisible = false;
            frmIBOpenNewTermDepositAccConfirmation.hbox866854059301598.isVisible = true;
            frmIBOpenNewTermDepositAccConfirmation.hbox866854059301638.isVisible = true;
        } else if (gblSelProduct == "ForUse") {
       		frmIBOpenNewSavingsAccConfirmation.lblOTPinCurr.text = "";
            frmIBOpenNewSavingsAccConfirmation.lblPlsReEnter.text = ""; 
            frmIBOpenNewSavingsAccConfirmation.hbxOTPincurrect.isVisible = false;
            frmIBOpenNewSavingsAccConfirmation.hbox866854059301598.isVisible = true;
            frmIBOpenNewSavingsAccConfirmation.hbox866854059301638.isVisible = true;
        	frmIBOpenNewSavingsAccConfirmation.line476132054113112.setVisibility(true);
        }
	 	if (gblSwitchToken == false && gblTokenSwitchFlag == false) {
	 	 
	        IBOpencheckTokenFlag();
	    }else if (gblTokenSwitchFlag == true && gblSwitchToken == false) {
	        
	        
                    if (gblSelProduct == "ForUse") {
                        IBTokenForUse();

                    } else if (gblSelProduct == "TMBDreamSavings") {
                        IBTokenTMBDreamSavings();

                    } else if (gblSelProduct == "TMBSavingcare") {
                        IBTokenTMBSavingcare();

                    } else if (gblSelProduct == "ForTerm") {
                        IBTokenForTerm();

                    }
	    }else if (gblTokenSwitchFlag == true && gblSwitchToken == true) {
	     
	       IBreqOTPMYNewAccnts();
	    }else if (gblTokenSwitchFlag == false && gblSwitchToken == true) {
			 
			IBreqOTPMYNewAccnts();
		} 
	
}
function IBOpencheckTokenFlag() {
    var inputParam = [];
    inputParam["crmId"] = gblcrmId;
    showLoadingScreenPopup();
    invokeServiceSecureAsync("tokenSwitching", inputParam, IBOpenibTokenFlagCallbackfunction);
}



function IBOpenibTokenFlagCallbackfunction(status, callbackResponse) {
    if (status == 400) {
        if (callbackResponse["opstatus"] == 0) {
            if (callbackResponse["deviceFlag"].length == 0) {
                
                IBreqOTPMYNewAccnts();
               
            }
            if (callbackResponse["deviceFlag"].length > 0) {
                var tokenFlag = callbackResponse["deviceFlag"][0]["TOKEN_DEVICE_FLAG"];
                var mediaPreference = callbackResponse["deviceFlag"][0]["MEDIA_PREFERENCE"];
                var tokenStatus = callbackResponse["deviceFlag"][0]["TOKEN_STATUS_ID"];

                if (tokenFlag == "Y" && (mediaPreference == "Token" || mediaPreference == "TOKEN" ) && tokenStatus=='02') {
                    gblTokenSwitchFlag = true;
                    gblOpenAcctConfirm = "verOTP";
                    if (gblSelProduct == "ForUse") {
                        IBTokenForUse();

                    } else if (gblSelProduct == "TMBDreamSavings") {
                        IBTokenTMBDreamSavings();

                    } else if (gblSelProduct == "TMBSavingcare") {
                        IBTokenTMBSavingcare();

                    } else if (gblSelProduct == "ForTerm") {
                        IBTokenForTerm();

                    }
                } else {
                    gblTokenSwitchFlag = false;
                    
                    IBreqOTPMYNewAccnts();
                    gblTokenSwitchFlag = false;
                }
            }
        } else {
            dismissLoadingScreenPopup();
        }
    }

}

function IBTokenForUse() {
    frmIBOpenNewSavingsAccConfirmation.hbox866854059301598.isVisible = false
    frmIBOpenNewSavingsAccConfirmation.hbox866854059301638.isVisible = false
    frmIBOpenNewSavingsAccConfirmation.hbox866854059301656.isVisible = false
    frmIBOpenNewSavingsAccConfirmation.hbox866854059250283.isVisible = false
    frmIBOpenNewSavingsAccConfirmation.hbxTokenMessage.isVisible = true;
    frmIBOpenNewSavingsAccConfirmation.hbox866854059250156.setVisibility(true);
    frmIBOpenNewSavingsAccConfirmation.hbxToken.setVisibility(true);
    
    gblOpenAcctConfirm = "verOTP";
    dismissLoadingScreenPopup();
    frmIBOpenNewSavingsAccConfirmation.tbxToken.text ="";
	  frmIBOpenNewSavingsAccConfirmation.tbxToken.setFocus(true);
	
      // IBinvokePartyInquiryOpenAct();
}

function IBTokenTMBDreamSavings() {
    frmIBOpenNewDreamAccConfirmation.hbox866854059301598.isVisible = false
    frmIBOpenNewDreamAccConfirmation.hbox866854059301638.isVisible = false
    frmIBOpenNewDreamAccConfirmation.hbox866854059301656.isVisible = false
    frmIBOpenNewDreamAccConfirmation.hbox866854059250283.isVisible = false
    frmIBOpenNewSavingsAccConfirmation.hbxTokenMessage.isVisible = true;
    frmIBOpenNewDreamAccConfirmation.hbox866854059250156.setVisibility(true);
    frmIBOpenNewDreamAccConfirmation.hbxToken.setVisibility(true);
    gblOpenAcctConfirm = "verOTP";
    dismissLoadingScreenPopup();
    frmIBOpenNewDreamAccConfirmation.tbxToken.text ="";
    
	
	  frmIBOpenNewDreamAccConfirmation.tbxToken.setFocus(true);
	
   // IBinvokePartyInquiryOpenAct();
}

function IBTokenForTerm() {
	frmIBOpenNewTermDepositAccConfirmation.hbox866854059250156.setVisibility(true);
	//frmIBOpenNewTermDepositAccConfirmation.hbox866854059250344.setVisibility(true);
	//frmIBOpenNewTermDepositAccConfirmation.hbxcnfrm.setVisibility(false);
    frmIBOpenNewTermDepositAccConfirmation.hbox866854059301598.isVisible = false
    frmIBOpenNewTermDepositAccConfirmation.hbox866854059301638.isVisible = false
    //frmIBOpenNewTermDepositAccConfirmation.hbox866854059301656.isVisible = false
    frmIBOpenNewTermDepositAccConfirmation.hbox866854059250283.isVisible = false
    if(kony.application.getCurrentForm().id == "frmIBOpenNewTermDepositAccConfirmation"){
        frmIBOpenNewTermDepositAccConfirmation.hbox866854059301656.isVisible = true;
 		frmIBOpenNewTermDepositAccConfirmation.lblRequest.text = kony.i18n.getLocalizedString("keyPleaseEnterToken");
	}
    frmIBOpenNewSavingsAccConfirmation.hbxTokenMessage.isVisible = true;
    frmIBOpenNewTermDepositAccConfirmation.hbxToken.setVisibility(true);
    gblOpenAcctConfirm = "verOTP";
    dismissLoadingScreenPopup();
    frmIBOpenNewTermDepositAccConfirmation.tbxToken.text ="";
    frmIBOpenNewTermDepositAccConfirmation.tbxToken.setFocus(true);
    //IBinvokePartyInquiryOpenAct();
}

function IBTokenTMBSavingcare() {
    frmIBOpenNewSavingsCareAccConfirmation.hbox866854059301598.isVisible = false
    frmIBOpenNewSavingsCareAccConfirmation.hbox866854059301638.isVisible = false
    frmIBOpenNewSavingsCareAccConfirmation.hbox866854059301656.isVisible = false
    frmIBOpenNewSavingsCareAccConfirmation.hbox866854059250283.isVisible = false
    frmIBOpenNewSavingsCareAccConfirmation.hbox866854059250156.setVisibility(true);
   // frmIBOpenNewSavingsCareAccConfirmation.hbxTokenMessage.isVisible = true;
    frmIBOpenNewSavingsCareAccConfirmation.hbxToken.setVisibility(true);
    frmIBOpenNewSavingsCareAccConfirmation.hbox866854059301656.setVisibility(true);
    frmIBOpenNewSavingsCareAccConfirmation.lblRequest.text = kony.i18n.getLocalizedString("keyPleaseEnterToken");
    gblOpenAcctConfirm = "verOTP";
    dismissLoadingScreenPopup();
    //IBinvokePartyInquiryOpenAct();
    frmIBOpenNewSavingsCareAccConfirmation.tbxToken.text ="";
    frmIBOpenNewSavingsCareAccConfirmation.tbxToken.setFocus(true);
}

function IBinvokePartyInquiryOpenAct() {
    
    showLoadingScreenPopup();
    var inputparam = {};
    inputparam["openAccountPartyInq"] = "openAccount";
    invokeServiceSecureAsync("partyInquiry", inputparam, IBpartyInquiryOpenActCallBack);

}
	
function IBpartyInquiryOpenActCallBack(status, callBackResponse) {
	    if (status == 400) {
	
	        if (callBackResponse["opstatus"] == 0) {
	            var inputparam = {};
	            gblPartyInqOARes = callBackResponse;
	            var locale = kony.i18n.getCurrentLocale();
				if (gblPartyInqOARes["issuedIdentType"] == "CI" ){
		    		frmIBOpenNewTermDepositAccConfirmation.lblAccNameValue.text  = gblPartyInqOARes["FullName"];
		    		if (locale == "th_TH") {
		    			frmIBOpenNewDreamAccConfirmation.lblDreamOpenActNameVal.text  = gblPartyInqOARes["FullName"];
		    		}else if (locale == "en_US") {
		    			frmIBOpenNewDreamAccConfirmation.lblDreamOpenActNameVal.text  = gblPartyInqOARes["PrefName"];
		    		}
					frmIBOpenNewSavingsAccConfirmation.lblActName.text  = gblPartyInqOARes["FullName"];
				}else{
					frmIBOpenNewTermDepositAccConfirmation.lblAccNameValue.text = gblPartyInqOARes["PrefName"];
					if (locale == "th_TH") {
		    			frmIBOpenNewDreamAccConfirmation.lblDreamOpenActNameVal.text  = gblPartyInqOARes["FullName"];
		    		}else if (locale == "en_US") {
		    			frmIBOpenNewDreamAccConfirmation.lblDreamOpenActNameVal.text  = gblPartyInqOARes["PrefName"];
		    		}
					frmIBOpenNewSavingsAccConfirmation.lblActName.text  = gblPartyInqOARes["FullName"];
				}
	            inputparam["fullName"] = callBackResponse["customerName"];
	            inputparam["prodCode"] = "01";
	            inputparam["productCode"] = gblSelOpenActProdCode;
	            inputparam["cardNo"] = callBackResponse["issuedIdentValue"];
	            inputparam["amount"] = gblFinActivityLogOpenAct["transferAmount"];
	            
	            if(gblSelProduct == "ForUse"){
					//inputparam["userAccountNum"] = "xxx-x-"+ frmIBOpenNewSavingsAccConfirmation.lblNFAccType.text.substring(6, 11) + "-x";
					inputparam["activityTypeID"] = "039";
					inputparam["activityFlexValues1"] = gblSelOpenActProdCode + "+" + frmIBOpenNewSavingsAcc.lblNSProdName.text;
				    inputparam["activityFlexValues3"] = frmIBOpenNewSavingsAcc.txtOpenActNicNam.text;
				    inputparam["activityFlexValues4"] = gblFinActivityLogOpenAct["transferAmount"];
				    inputparam["activityFlexValues5"] = "";
					inputparam["userAccountNum"] = frmIBOpenNewSavingsAccConfirmation.lblNFAccType.text;
		 			inputparam["ProductName"] = frmIBOpenNewSavingsAcc.lblNSProdName.text;
				}
				else if(gblSelProduct == "TMBDreamSavings"){
					inputparam["activityTypeID"] = "040";
					inputparam["activityFlexValues1"] = gblSelOpenActProdCode + "+" + frmIBOpenNewSavingsAcc.lblNSProdName.text;
				    inputparam["activityFlexValues3"] = frmIBOpenNewSavingsAcc.txtOpenActNicNam.text;
				    inputparam["activityFlexValues4"] = gblFinActivityLogOpenAct["transferAmount"];
				    inputparam["activityFlexValues5"] = "";
						//inputparam["userAccountNum"] = "xxx-x-"+ frmIBOpenNewDreamAccConfirmation.lblOAMnthlySavActType.text.substring(6, 11) + "-x";
					inputparam["userAccountNum"] = kony.string.replace(frmIBOpenNewDreamAccConfirmation.lblOAMnthlySavActType.text, "-", "")
		 			//inputparam["amount"] = frmIBOpenNewDreamAcc.txtODMnthSavAmt.text;
		 			inputparam["ProductName"] = frmIBOpenNewDreamAcc.lblDreamSavings.text;	
				}
				else if(gblSelProduct == "TMBSavingcare"){
					inputparam["activityTypeID"] = "081";
				    inputparam["activityFlexValues1"] = "211" + "+" + "TMB Savings Care Account";
				    inputparam["activityFlexValues3"] = frmIBOpenNewSavingsCareAcc.txtNickName.text;
				    inputparam["activityFlexValues4"] = frmIBOpenNewSavingsCareAcc.txtAmount.text;
				    var benifList = "";
				    var recordNo = 1;
					if(gblOpenActBenList.length != 0){
					for(var i = 0; i< gblOpenActBenList.length; i++){
						
						var element = recordNo + "." + gblOpenActBenList[i]["firstName"] + " " + gblOpenActBenList[i]["lastName"] + "," +gblOpenActBenList[i]["relationShip"] + "," + gblOpenActBenList[i]["benifit"] + "%"
						recordNo = recordNo + 1;  
						benifList = benifList + element;
						kony.print("benifList"+benifList)
					}
					benifList = "Specify" + benifList;
					}else{
						kony.print("benifList"+benifList)
						benifList = "Not Specified";
					}
				    inputparam["activityFlexValues5"] = benifList;
				    
					inputparam["userAccountNum"] = frmIBOpenNewSavingsCareAccConfirmation.lblOASCFrmNum.text;
					inputparam["ProductName"] = frmIBOpenNewSavingsCareAcc.lblSavinCareSelProdTxt.text;		
				}
				else if(gblSelProduct == "ForTerm"){
					inputparam["activityTypeID"] = "041";
					inputparam["activityFlexValues1"] = gblSelOpenActProdCode + "+" + frmIBOpenNewTermDepositAcc.lblOATDProdName.text;
				    inputparam["activityFlexValues3"] = frmIBOpenNewTermDepositAcc.txtDSNickName.text;
				    inputparam["activityFlexValues4"] = gblFinActivityLogOpenAct["transferAmount"];
				    inputparam["activityFlexValues5"] = "";
					//inputparam["userAccountNum"] = "xxx-x-"+ frmIBOpenNewTermDepositAccConfirmation.lblPayingFromAccountType.text.substring(6, 11) + "-x";
					inputparam["userAccountNum"] = frmIBOpenNewTermDepositAccConfirmation.lblPayingFromAccountType.text;
		        	if(gblSelOpenActProdCode == "659"){
		            	inputparam["ProductName"] = "Up and Up Account 24 Months"; // changed as per mail from mohan peddada on 25-03-2014
		            }
		            else{
		            	inputparam["ProductName"] = frmIBOpenNewTermDepositAcc.lblOATDProdName.text;	
		            }
		      	}
		      	inputparam["userAccountNum"] = kony.string.replace(inputparam["userAccountNum"], "-", "");
		      	
	            
	            invokeServiceSecureAsync("SaveOpenAccountTxn", inputparam, IBriskLevelOpenActCallBack);
	        } else {
	              
	             dismissLoadingScreenPopup();
	             showAlert(callBackResponse["errMsg"], kony.i18n.getLocalizedString("info"));
	             return false;
	           
	        }
	    }
	}




function IBriskLevelOpenActCallBack(status, callBackResponse) {

    if (status == 400) {

        if (callBackResponse["opstatus"] == 0) {
       
              //  dismissLoadingScreen();
                var openingDate = callBackResponse["openingDate"];
                gblOnClickReq = true;
                dismissLoadingScreenPopup();
                if(gblSelProduct=="ForTerm")
				{
				//IBcallGenerateTransferRefNoserviceOpenActMB()
				if (grtFormattedAmount(gblFinActivityLogOpenAct["transferAmount"]) >0){
							frmIBOpenNewTermDepositAccConfirmation.hbox866854059249871.setVisibility(true);
							frmIBOpenNewTermDepositAccConfirmation.hbox867569043311329.setVisibility(true);
							frmIBOpenNewTermDepositAccConfirmation.lblBalanceBefrTransfr.setVisibility(true);
							frmIBOpenNewTermDepositAccConfirmation.lblIBOpenAccCnfmBalAmt.setVisibility(true);
							frmIBOpenNewTermDepositAccConfirmation.hbox866854059249891.skin = hboxLightGrey320px;
							frmIBOpenNewTermDepositAccConfirmation.hbox867569043311341.skin	= hbox320pxpadding;
							dismissLoadingScreenPopup();
						    IBcallGenerateTransferRefNoserviceOpenActMB();
							}
							else{
							frmIBOpenNewTermDepositAccConfirmation.hbox866854059249871.setVisibility(false);
							frmIBOpenNewTermDepositAccConfirmation.hbox867569043311329.setVisibility(false);
							frmIBOpenNewTermDepositAccConfirmation.lblBalanceBefrTransfr.setVisibility(false);
							frmIBOpenNewTermDepositAccConfirmation.lblIBOpenAccCnfmBalAmt.setVisibility(false);
							frmIBOpenNewTermDepositAccConfirmation.hbox866854059249891.skin = hbox320pxpadding;
							frmIBOpenNewTermDepositAccConfirmation.hbox867569043311341.skin	= hboxLightGrey320px;
							dismissLoadingScreenPopup();
							}
				frmIBOpenNewTermDepositAccConfirmation.lblOpeningDateVal.text = openingDate
				gblOpenAcctConfirm = "genOTP";
				frmIBOpenNewTermDepositAccConfirmation.show()
				}
                
                if (gblSelProduct == "ForUse") {
                	frmIBOpenNewSavingsAccConfirmation.lblOpeningDateVal.text = openingDate;
                    frmIBOpenNewSavingsAccConfirmation.hbox866854059250156.setVisibility(false);
					gblOpenAcctConfirm = "genOTP";
					if (kony.os.toNumber(grtFormattedAmount(gblFinActivityLogOpenAct["transferAmount"])) >= 0){
								  frmIBOpenNewSavingsAccConfirmation.hbox866854059249811.setVisibility(true);
								  frmIBOpenNewSavingsAccConfirmation.hbox866854059249891.setVisibility(true);
								  frmIBOpenNewSavingsAccConfirmation.lblBalanceBefrTransfr.setVisibility(true);
								  frmIBOpenNewSavingsAccConfirmation.lblIBOpenAccCnfmBalAmt.setVisibility(true);
								  frmIBOpenNewSavingsAccConfirmation.hbox866854059249897.skin = hboxLightGrey320px;
								  frmIBOpenNewSavingsAccConfirmation.hbox866854059249871.skin = hbox320pxpadding;
								  frmIBOpenNewSavingsAccConfirmation.hbxCardType.skin = hboxLightGrey320px;
								  frmIBOpenNewSavingsAccConfirmation.hbxCardFee.skin = hbox320pxpadding;
								  frmIBOpenNewSavingsAccConfirmation.btnRight.setVisibility(true);
								   IBcallGenerateTransferRefNoserviceOpenActMB();
								   dismissLoadingScreenPopup();
							}else{
								  frmIBOpenNewSavingsAccConfirmation.hbox866854059249811.setVisibility(false);
								  frmIBOpenNewSavingsAccConfirmation.hbox866854059249891.setVisibility(false);
								  frmIBOpenNewSavingsAccConfirmation.lblBalanceBefrTransfr.setVisibility(false);
								  frmIBOpenNewSavingsAccConfirmation.lblIBOpenAccCnfmBalAmt.setVisibility(false);
								  frmIBOpenNewSavingsAccConfirmation.hbox866854059249897.skin = hbox320pxpadding;
								  frmIBOpenNewSavingsAccConfirmation.hbox866854059249871.skin = hboxLightGrey320px;
								  frmIBOpenNewSavingsAccConfirmation.hbxCardType.skin = hbox320pxpadding;
								  frmIBOpenNewSavingsAccConfirmation.hbxCardFee.skin = hboxLightGrey320px;
								  
								  dismissLoadingScreenPopup();
								  frmIBOpenNewSavingsAccConfirmation.btnRight.setVisibility(false);
								 // frmIBOpenNewSavingsAccConfirmation.show();
							}
					
					frmIBOpenNewSavingsAccConfirmation.show();
                } else if (gblSelProduct == "TMBDreamSavings") {
                    frmIBOpenNewDreamAccConfirmation.lblOpeningDateValue.text = openingDate;
                   // frmIBOpenNewDreamAccConfirmation.lblTransRefNo.setVisibility(false);
					frmIBOpenNewDreamAccConfirmation.hbox866854059250156.setVisibility(false);
					gblOpenAcctConfirm = "genOTP";
					frmIBOpenNewDreamAccConfirmation.show();
                } else if (gblSelProduct == "TMBSavingcare") {
                	frmIBOpenNewSavingsCareAccConfirmation.lblOASCOpenDateVal.text = openingDate
                    frmIBOpenNewSavingsCareAccConfirmation.hbox866854059250156.setVisibility(false);
					dismissLoadingScreenPopup();
					if (grtFormattedAmount(gblFinActivityLogOpenAct["transferAmount"]) >0){
								frmIBOpenNewSavingsCareAccConfirmation.hbxOASCOpenAct.setVisibility(true);
								frmIBOpenNewSavingsCareAccConfirmation.hbxOASCTranRefNo.setVisibility(true);
								frmIBOpenNewSavingsCareAccConfirmation.hbxOASCOpenDate.skin = hbox320pxpadding;
								frmIBOpenNewSavingsCareAccConfirmation.hbxOASCIntRate.skin = hboxLightGrey320px;
								frmIBOpenNewSavingsCareAccConfirmation.lblBalanceBefrTra.setVisibility(true);
								frmIBOpenNewSavingsCareAccConfirmation.lblBalance.setVisibility(true);
								IBcallGenerateTransferRefNoserviceOpenActMB();
								IBonClickPreShowSCConfirm();								
								frmIBOpenNewSavingsCareAccConfirmation.btnRight.setVisibility(false);
								dismissLoadingScreenPopup();								
								
				}else{
								frmIBOpenNewSavingsCareAccConfirmation.hbxOASCOpenAct.setVisibility(false);
								frmIBOpenNewSavingsCareAccConfirmation.hbxOASCTranRefNo.setVisibility(false);
								frmIBOpenNewSavingsCareAccConfirmation.hbxOASCOpenDate.skin = hboxLightGrey320px;
								frmIBOpenNewSavingsCareAccConfirmation.hbxOASCIntRate.skin = hbox320pxpadding;
								frmIBOpenNewSavingsCareAccConfirmation.lblBalanceBefrTra.setVisibility(false);
								frmIBOpenNewSavingsCareAccConfirmation.lblBalance.setVisibility(false);
								dismissLoadingScreenPopup();
								IBonClickPreShowSCConfirm();
								frmIBOpenNewSavingsCareAccConfirmation.btnRight.setVisibility(false);
								dismissLoadingScreenPopup();								
			}
					gblOpenAcctConfirm = "genOTP";
					frmIBOpenNewSavingsCareAccConfirmation.show();
                    //	IBonClickPreShowSCConfirm();
									
                } else if (gblSelProduct == "ForTerm") {
               	 gblOpenAcctConfirm = "genOTP";
                    frmIBOpenNewTermDepositAccConfirmation.lblOpeningDateVal.text = openingDate
                }


        } else {
           
           
           if (callBackResponse["errCode"] == "Entered amount is greater than maximum amount") {
               dismissLoadingScreenPopup();
              showAlert(kony.i18n.getLocalizedString("keyMaxLimitAmt"), kony.i18n.getLocalizedString("info"));
             return false;
            }else  if (callBackResponse["errCode"] == "Entered amount is less than minimum amount") {
                dismissLoadingScreenPopup();
              showAlert(kony.i18n.getLocalizedString("keyMinLimitAmt"), kony.i18n.getLocalizedString("info"));
             return false;
            }else{
             dismissLoadingScreenPopup();
             showAlert("You cannot open the account.",kony.i18n.getLocalizedString("info"));
             return false;
            }
           
        }
    }
    dismissLoadingScreenPopup();
}
function RelationI18n(key)
{
	var k=key;
	var p=kony.i18n.getLocalizedString("keyOpenRelation");
	var f=kony.i18n.getLocalizedString("keyFather");
	var m=kony.i18n.getLocalizedString("keyMother");
	var r=kony.i18n.getLocalizedString("keyRelative");
	var s=kony.i18n.getLocalizedString("keySpouseReg");
	var l=kony.i18n.getLocalizedString("keySpouseNL");
	var c=kony.i18n.getLocalizedString("keyChild");
	var o=kony.i18n.getLocalizedString("keyOtherRelation"); 
	
	switch(key)
	{
	case "P":
		return p;
		break;
	case "F":
		return f;
		break;
	case "M":
		return m;
		break;
	case "R":
		return r;
		break;
	case "S":
		return s;
		break;
	case "L":
		return l;
		break;
	case "C":
		return c;
		break;
	case "O":
		return o;
		break;
	default:
		return "0";
		break;
	//return returnkey;
	
	}
}
var arrbefore=[];
var arrbenf1 =[] ;
var arrbenf2=[] ;
var arrbenf3 =[];
var arrbenf4 =[];
var arrbenf5 =[];
//var arrafter =[];

function IBonClickSavingCareLanding() {
	resetTextBoxesSkinSavingCareIB();
	if ((frmIBOpenNewSavingsCareAcc.txtNickName.text == null) || (frmIBOpenNewSavingsCareAcc.txtNickName.text == "")) {
	showAlert(kony.i18n.getLocalizedString("keyActNickNameEmpty"), kony.i18n.getLocalizedString("info"));
	frmIBOpenNewSavingsCareAcc.txtNickName.skin = "txtErrorBG";
	return false;
	}
	
	if (frmIBOpenNewSavingsCareAcc.lblFromAccount.text == "" || frmIBOpenNewSavingsCareAcc.lblFromAccount.text == null) {
        showAlert(kony.i18n.getLocalizedString("keyIBTransferFromSelect"), kony.i18n.getLocalizedString("info"));
        return false
    }
	
	//frmIBOpenNewSavingsCareAcc.txtAmount.text = commaFormattedOpenAct(frmIBOpenNewSavingsCareAcc.txtAmount.text) + kony.i18n.getLocalizedString("currencyThaiBaht");
	var removeOpenBaht = frmIBOpenNewSavingsCareAcc.txtAmount.text;

	frmIBOpenNewSavingsCareAcc.txtAmount.text = removeOpenBaht.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(/,/g, "")

	
    var nickName = NickNameValid(frmIBOpenNewSavingsCareAcc.txtNickName.text);
    var isValid = true;
    gblOpenActBenList = [];
    var fdata = frmIBOpenNewSavingsCareAcc.coverFlowTP.data[gblCWSelectedItem];
    var hiddenActBal = fdata.lblavailbal;
    enteredAmount = frmIBOpenNewSavingsCareAcc.txtAmount.text;
    enteredAmount = enteredAmount.replace(kony.i18n.getLocalizedString("currencyThaiBaht"),"").replace(/,/g, "");
    enteredAmount = enteredAmount.trim();
    		gblFinActivityLogOpenAct["branchEN"] = fdata.hiddenBranchEN;
			gblFinActivityLogOpenAct["branchTH"] = fdata.hiddenBranchTH;
			gblFinActivityLogOpenAct["nickTH"] = fdata.hiddenproductThaiAssign;
			gblFinActivityLogOpenAct["nickEN"] = fdata.hiddenprodENGAssign; 
			gblFinActivityLogOpenAct["actTypeFromOpen"] = fdata.lblaccType; 
    
    
    gblFinActivityLogOpenAct["transferAmount"] = enteredAmount;
    var isCrtFormt = amountValidationMBOpenAct(enteredAmount);
    if (!isCrtFormt) {
        showAlert(kony.i18n.getLocalizedString("keyIncorrectDepsoit"), kony.i18n.getLocalizedString("info"));
        frmIBOpenNewSavingsCareAcc.txtAmount.text = "";
        frmIBOpenNewSavingsCareAcc.txtAmount.skin = "txtErrorBG";
        return false
    }

    if ((kony.os.toNumber(enteredAmount)) < (kony.os.toNumber(gblMinOpenAmt))) {
        showAlert(kony.i18n.getLocalizedString("keyIncorrectDepsoit"), kony.i18n.getLocalizedString("info"));
        frmIBOpenNewSavingsCareAcc.txtAmount.skin = "txtErrorBG";
        return false
    }

    if (gblMaxOpenAmt != "No Limit") {
        if ((kony.os.toNumber(enteredAmount)) > (kony.os.toNumber(gblMaxOpenAmt))) {
            showAlert(kony.i18n.getLocalizedString("keyIncorrectDepsoit"), kony.i18n.getLocalizedString("info"));
            frmIBOpenNewSavingsCareAcc.txtAmount.skin = "txtErrorBG";
            return false
        }
    }

    if ((kony.os.toNumber(enteredAmount)) > (kony.os.toNumber(hiddenActBal))) {
        showAlert(kony.i18n.getLocalizedString("keyAmtLessThanAvaBal"), kony.i18n.getLocalizedString("info"));
        frmIBOpenNewSavingsCareAcc.txtAmount.skin = "txtErrorBG";
        return false
    }

    if (nickName == false) {
        showAlert(kony.i18n.getLocalizedString("keyincorrectNickName"), kony.i18n.getLocalizedString("info"));
        frmIBOpenNewSavingsCareAcc.txtNickName.skin = "txtErrorBG";
        //	alert("INVALID DEVICE NAME");
        return false;
    }
		var nickNameUniq = IBopenActNickNameUniq(frmIBOpenNewSavingsCareAcc.txtNickName.text);
        if (nickNameUniq == false) {
            showAlert(kony.i18n.getLocalizedString("keyuniqueNickName"), kony.i18n.getLocalizedString("info"));
            frmIBOpenNewSavingsCareAcc.txtNickName.skin = "txtErrorBG";
            return false
        }
	
   	//IBsetSACnfWigFalse();

    
    //isValid = IBvalidateBenfFields(frmIBOpenNewSavingsCareAcc.txtBefFisrtName1, frmIBOpenNewSavingsCareAcc.txtBefSecName1, frmIBOpenNewSavingsCareAcc.txtBefBen1, frmIBOpenNewSavingsCareAcc.cmbxOne.selectedKeyValue[0]);
    if(gblBenificiaryCount == 0){
    	isValid = true;
		successValidateBeneficiary();
    }else{
    	//MIB-4914
    	isValid  = validateBenificiaries();
    
    
	    if (!isValid) {
	        return false
	    } else {
	    	if(gblHaveErrorFlag == true) {
	    		validateZeroValue();
	    	} else {
	    		confirmValidateZeroValue();
	    	}
	    	
	    }
	}
}

function validateZeroValue() {
	popupIBCommonConfirmCancel.lblPopUpHeader.text = kony.i18n.getLocalizedString("Confirmation");
    popupIBCommonConfirmCancel.lblPopUpMessage.text = kony.i18n.getLocalizedString("benefit_notAllocatedBenfi");
    popupIBCommonConfirmCancel.show();
    popupIBCommonConfirmCancel.btnPopUpConfirm.onClick = function() {
		confirmValidateZeroValue();
    };
	popupIBCommonConfirmCancel.btnPopUpCancel.onClick = function() {
   		unsuccessValidateBeneficiary();
    };
}

function unsuccessValidateBeneficiary() {
	popupIBCommonConfirmCancel.dismiss();
}

function successValidateBeneficiary() {
	resetTextBoxesSkinSavingCareIB();
    if (kony.i18n.getCurrentLocale() == "en_US") {
        frmIBOpenNewSavingsCareAccConfirmation.lblOASCIntRate.text = gblFinActivityLogOpenAct["intersetRateLabelEN"] + ":"
    } else {
        frmIBOpenNewSavingsCareAccConfirmation.lblOASCIntRate.text = gblFinActivityLogOpenAct["intersetRateLabelTH"] + ":"
    }
    IBinvokePartyInquiryOpenAct();
}

function confirmValidateZeroValue() {
	popupIBCommonConfirmCancel.dismiss();
	
	pushBenificiaryDatatoArray();
	
   	var nameUniq = checkForDuplicateNames()
    
    if (nameUniq == false) {
        showAlert(kony.i18n.getLocalizedString("keyBeneficiaryDupName"), kony.i18n.getLocalizedString("info"));
        return false;
    }

	var totCalPer = validatePercentagesIB();

    if (totCalPer != 100) {
        showAlert(kony.i18n.getLocalizedString("keyBenTotalPer"), kony.i18n.getLocalizedString("info"));
        return false;
    }
	successValidateBeneficiary();
}



function IBshowTDConfirmation() {

    var fdata = frmIBOpenNewTermDepositAcc.coverFlowTP.data[gblCWSelectedItem];
    var hiddenActBal = gblFromTermActs.lblavailbal;
    var tdNickName=frmIBOpenNewTermDepositAcc.txtDSNickName.text;
	if(NickNameValid(tdNickName.trim()) == false) {
		showAlert(kony.i18n.getLocalizedString("keyOpenAccntNickname"), kony.i18n.getLocalizedString("info"));
		 frmIBOpenNewTermDepositAcc.txtDSNickName.skin = "txtErrorBG";
		return false;
	}
    
    if (frmIBOpenNewTermDepositAcc.lblFromAccount.text == "" || frmIBOpenNewTermDepositAcc.lblFromAccount.text == null) {
        showAlert(kony.i18n.getLocalizedString("keyIBTransferFromSelect"), kony.i18n.getLocalizedString("info"));
        return false
    }

    if (frmIBOpenNewTermDepositAcc.txtTDamount.text == "" || frmIBOpenNewTermDepositAcc.txtTDamount.text == null) {
        showAlert(kony.i18n.getLocalizedString("keyPleaseEnterAmount"), kony.i18n.getLocalizedString("info"));
        frmIBOpenNewTermDepositAcc.txtTDamount.skin = "txtErrorBG";
        return false
    }
    //frmIBOpenNewTermDepositAcc.txtTDamount.text = commaFormattedOpenAct(frmIBOpenNewTermDepositAcc.txtTDamount.text) + kony.i18n.getLocalizedString("currencyThaiBaht");
    var enterdAmount = frmIBOpenNewTermDepositAcc.txtTDamount.text;
	enterdAmount = enterdAmount.replace(kony.i18n.getLocalizedString("currencyThaiBaht"),"").replace(/,/g, "");
	enterdAmount = enterdAmount.trim();
	gblFinActivityLogOpenAct["transferAmount"] = enterdAmount;

    if (!(amountValidationMBOpenAct(gblFinActivityLogOpenAct["transferAmount"]))) {
        showAlert(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), kony.i18n.getLocalizedString("info"));
        frmIBOpenNewTermDepositAcc.txtTDamount.skin = "txtErrorBG";
    }

    if ((kony.os.toNumber(gblFinActivityLogOpenAct["transferAmount"])) < (kony.os.toNumber(gblMinOpenAmt))) {
        showAlert(kony.i18n.getLocalizedString("keyMinLimitAmt"), kony.i18n.getLocalizedString("info"));
        frmIBOpenNewTermDepositAcc.txtTDamount.skin = "txtErrorBG";
        return false
    }

    if (gblMaxOpenAmt != "No Limit") {
        if ((kony.os.toNumber(gblFinActivityLogOpenAct["transferAmount"])) > (kony.os.toNumber(gblMaxOpenAmt))) {
            showAlert(kony.i18n.getLocalizedString("keyMaxLimitAmt"), kony.i18n.getLocalizedString("info"));
            frmIBOpenNewTermDepositAcc.txtTDamount.skin = "txtErrorBG";
            return false
        }
    }

    if ((kony.os.toNumber(gblFinActivityLogOpenAct["transferAmount"])) > (kony.os.toNumber(hiddenActBal))) {
        showAlert(kony.i18n.getLocalizedString("keyAmtLessThanAvaBal"), kony.i18n.getLocalizedString("info"));
        frmIBOpenNewTermDepositAcc.txtTDamount.skin = "txtErrorBG";
        return false
    }

    var nickName = IBopenActNickNameUniq(frmIBOpenNewTermDepositAcc.txtDSNickName.text);
    if (nickName == false) {
        showAlert(kony.i18n.getLocalizedString("keyuniqueNickName"), kony.i18n.getLocalizedString("info"));
        frmIBOpenNewTermDepositAcc.txtDSNickName.skin = "txtErrorBG";
        return false
    }

    var fdata = frmIBOpenNewSavingsAcc.coverFlowTP.data[gblCWSelectedItem];
    frmIBOpenNewTermDepositAccConfirmation.imgTDCnfmTitle.src = frmIBOpenNewTermDepositAcc.imgOATDProd.src;
    frmIBOpenNewTermDepositAccConfirmation.lblTDCnfmTitle.text = frmIBOpenNewTermDepositAcc.lblOATDProdName.text;
    frmIBOpenNewTermDepositAccConfirmation.lblOATDNickNameVal.text = frmIBOpenNewTermDepositAcc.txtDSNickName.text;
    frmIBOpenNewTermDepositAccConfirmation.lblOATDAmtVal.text = commaFormattedOpenAct(gblFinActivityLogOpenAct["transferAmount"]) + kony.i18n.getLocalizedString("currencyThaiBaht");
    frmIBOpenNewTermDepositAccConfirmation.lblAccNameValue.text = fdata.lblhideaccountName;
    
    
			gblFinActivityLogOpenAct["nickTH"] = fdata.hiddenproductThaiAssign;
			gblFinActivityLogOpenAct["nickEN"] = fdata.hiddenprodENGAssign; 
			
    
    if (kony.i18n.getCurrentLocale() == "en_US") {
        frmIBOpenNewTermDepositAccConfirmation.lblIntrstRate.text = gblFinActivityLogOpenAct["intersetRateLabelEN"] + ":"
    } else {
        frmIBOpenNewTermDepositAccConfirmation.lblIntrstRate.text = gblFinActivityLogOpenAct["intersetRateLabelTH"] + ":"
    }
    frmIBOpenNewTermDepositAccConfirmation.hbox866854059250156.setVisibility(false);
	frmIBOpenNewTermDepositAcc.txtDSNickName.skin = "txtIB20pxBlack";
	frmIBOpenNewTermDepositAcc.txtTDamount.skin = "txtIB20pxBlack";
	IBinvokePartyInquiryOpenAct();
}




function custRiskLevel()
{
	var inputParam ={};
	showLoadingScreenPopup();
	invokeServiceSecureAsync("CustomerRiskLevel", inputParam, IBriskLevelCallBack);
}
function IBriskLevelCallBack(status, callBackResponse)
{
	if (status == 400) {
        if (callBackResponse["opstatus"] == 0) {
            if (callBackResponse["riskVerified"]==undefined || callBackResponse["riskVerified"]=="Fail")
            { 
            	showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_Alert_CheckRisk"), kony.i18n.getLocalizedString("info"),onclickActivationCompleteStart);	
	            return false;
            }
            else
            {
            	IBcheckTranPwdLocked();
            }
         }
    }
}
function checkopenActCustStatus()
{
     	if(getCRMLockStatus())
		{
			curr_form=kony.application.getCurrentForm().id;
			dismissLoadingScreenPopup();
			popIBBPOTPLocked.show();
		}
		else 
		{
			custRiskLevel();
		}
}
function IBcheckTranPwdLocked(){
	var inputParam ={
		rqUUId: "",
		channelName: "IB-INQ",
		openActFlag: "true"
		
	}
	 showLoadingScreenPopup();
	 
	 invokeServiceSecureAsync("crmProfileInq", inputParam, IBcallBackUserLockTransStatus)
}

function IBcallBackUserLockTransStatus(status, callBackResponse){
	if(status == 400){
		if(callBackResponse["opstatus"] == 0)
		{
		 	var IBUserStatus=callBackResponse["ibUserStatusIdTr"];
		 	var key = newLineTextIB(kony.i18n.getLocalizedString("keyBusinessHours"));
            var res = key.replace("<time>",callBackResponse["openActStartTime"] + "-" + callBackResponse["openActEndTime"]);
		 	
			if(IBUserStatus == "04"){
				dismissLoadingScreenPopup();
				showAlert(kony.i18n.getLocalizedString("accLock"), kony.i18n.getLocalizedString("info"));
			}
			else{
				//custRiskLevel();
				gblEmailId = callBackResponse["emailAddr"];
				//IBcheckOpenActBusHrs();
				if(callBackResponse["openActBusinessHrsFlag"] == "true") {
					var ifCASAActs = checkTermDepositCount();
					if(ifCASAActs == 0){
						dismissLoadingScreenPopup();
						showAlertWithCallBack(kony.i18n.getLocalizedString("MB_CommonError_NoSA"), kony.i18n.getLocalizedString("info"),onclickActivationCompleteStart);
						return false;
					}
				
					//FATCA CR starts
					gblFATCAUpdateError = false;
					gblFATCAUpdateFlag = callBackResponse["FatcaFlag"];
					if(callBackResponse["FatcaFlag"] == "0") {
						dismissLoadingScreenPopup();
						gblFATCAFlow = "openact";
						showFACTAInfoIB();
					}
					else if(callBackResponse["FatcaFlag"] == "8" || callBackResponse["FatcaFlag"] == "9"
						|| callBackResponse["FatcaFlag"] == "P" || callBackResponse["FatcaFlag"] == "R" || callBackResponse["FatcaFlag"] == "X") {
						dismissLoadingScreenPopup();
						gblFATCAFlow = "openact";
						setFATCAIBGoToBranchInfoMessage("N");
					}
					else {
						IBivokeCustActInqForOpenAct();
					}
					//FATCA CR ends
									
					//setDataForUse();
					//frmOpenActSelProd.show();
													
				}else{
					dismissLoadingScreenPopup();
					//showAlert((kony.i18n.getLocalizedString("keyBusinessHours") + " " + callBackResponse["openActStartTime"] + "-" + callBackResponse["openActEndTime"] + (kony.i18n.getLocalizedString("keyOpenActLastSymbol"))), kony.i18n.getLocalizedString("info"));
					showAlert(res,kony.i18n.getLocalizedString("info"));
					return false;
				}
			}
		}
		else {
		 	dismissLoadingScreenPopup();
		 	showAlert(kony.i18n.getLocalizedString("accLock"), kony.i18n.getLocalizedString("info"));
		} 
	}
}


function IBivokeCustActInqForOpenAct(){
	initFATCAGlobalVars();
	showLoadingScreenPopup()
	var inputparam = {};
	inputparam["openActFlag"]= "true";
	//inputparam["selProductCode"] = gblSelOpenActProdCode;
	invokeServiceSecureAsync("customerAccountInquiry", inputparam, IBcallBackOpenAcntFromActs);
}

function IBcallBackOpenAcntFromActs(status, resulttable){
	
	if (status == 400) {
	
		if (resulttable["opstatus"] == 0) {
		gblOpenActList = resulttable;
		var nonCASAAct=0;
		//getting all product code from tmb.property file.
		gblOpenAcctNormalSavings = resulttable["PROD_CODES_OPEN_ACCT_NORMALSAVINGS"];
		gblForUserProdCodes = resulttable["FOR_USE_PRODUCT_CODES"];
		gblOpenAcctSavingsCare = resulttable["SAVING_CARE_PRODUCT_CODES"];
		gblOpenAcctDreamSaving = resulttable["PROD_CODES_OPEN_ACCT_DREAMSAVINGS"];
		gblOpenAcctTermDeposit = resulttable["PROD_CODES_OPEN_ACCT_TERMDEPOSIT"];	
		for (var i = 0; i < resulttable.custAcctRec.length; i++) 
		{
			var accountStatus = resulttable["custAcctRec"][i].acctStatus;
			if(accountStatus.indexOf("Active") == -1){
				nonCASAAct = nonCASAAct + 1;
				//showAlertWithCallBack(kony.i18n.getLocalizedString("MB_StatusNotEligible"), kony.i18n.getLocalizedString("info"),onclickActivationCompleteStart);
					//return false;
				}
		}
		//checkIfUserHasActsWithProd();
		if(isCmpFlow){
			callProdCategoryBasedOnProdCode();
		} else {
			if(nonCASAAct==resulttable.custAcctRec.length)
			{
				showAlertWithCallBack(kony.i18n.getLocalizedString("MB_StatusNotEligible"), kony.i18n.getLocalizedString("info"),onclickActivationCompleteStart);
				return false;
			}
			if(resulttable["custAcctFromRec"] == null || resulttable["custAcctFromRec"] == "" || resulttable["custAcctFromRec"] == undefined )
			{
				dismissLoadingScreenPopup();
				showAlertWithCallBack(kony.i18n.getLocalizedString("MB_CommonError_NoSA"), kony.i18n.getLocalizedString("info"),onclickActivationCompleteStart);
				return false;
			}
			IBsetDataForUse();
		}	
		
		//IBsetDataForSave();		//For ENH_087
	//	frmIBOpenActSelProd.show();
		//onClickAgreeOPenAcnt();
		}else{
		dismissLoadingScreenPopup();	
		showAlert(resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
		return false;
		}
	}
	
}

/** 
 * added below function to implement campign internal link to open account prod breif screen.
 *  based on product code need to call appropriate product category.
 */
function callProdCategoryBasedOnProdCode(){

	if(undefined != gblProdCode && "" != gblProdCode) {
		if(gblOpenAcctNormalSavings.indexOf(gblProdCode) != -1 || gblForUserProdCodes.indexOf(gblProdCode) != -1){
			if(undefined != gblProdCode && "" != gblProdCode && gblProdCode == 221 ){//TMB No Fixed Account is a for save
				IBsetDataForSave();
			}else {
				IBsetDataForUse();
			}
	  	} else if(gblOpenAcctSavingsCare.indexOf(gblProdCode) != -1 || gblOpenAcctDreamSaving.indexOf(gblProdCode) != -1){
	     	IBsetDataForSave();
	  	} else if(gblOpenAcctTermDeposit.indexOf(gblProdCode) != -1){
	     	IBsetDataForTD();
	  	} else {
          	dismissLoadingScreenPopup();
          	showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        }
	}
  	
}

function IBonClickAgreeOpenDSActs(){
	
	//showLoadingScreen();
	var inputparam = {};
	showLoadingScreenPopup();
	invokeServiceSecureAsync("getDreamSavingTargets", inputparam, IBcallBackOpenDreamSavingTargets);
}

function IBcallBackOpenDreamSavingTargets(status, resulttable) {
	if (status == 400) {
		
		if (resulttable["opstatus"] == 0) {
		    //alert("result:"+resulttable["SavingTargets"]);
			//dismissLoadingScreen();
			gblOpenDrmAcct=resulttable;
			frmIBOpenNewDreamAcc.lblPicturITText.text = "";
			frmIBOpenNewSavingsAcc.imgNSProdName.src = frmIBOpenActSelProd["segOpenActSelProd"]["selectedItems"][0].imgOpnActSelProd;
			frmIBOpenNewDreamAcc.txtODTargetAmt.skin="txtBoxTransParentGrey";
			frmIBOpenNewDreamAcc.txtODNickNameVal.skin="txtBoxTransParentGrey";
			frmIBOpenNewDreamAcc.txtODMnthSavAmt.skin="txtBoxTransParentGrey";
			frmIBOpenNewDreamAcc.txtODMyDream.skin="txtBoxTransParentGrey";
			frmIBOpenNewDreamAcc.txtODMnthSavAmtSave.skin="txtBoxTransParentGrey";
			frmIBOpenNewDreamAcc.txtODMyDreamSave.skin="txtBoxTransParentGrey";
			frmIBOpenNewSavingsAcc.txtDSAmountSel.skin = "txtIB20pxBlack";
   			frmIBOpenNewSavingsAcc.txtOpenActNicNam.skin = "txtIB20pxBlack";
   			frmIBOpenNewDreamAcc.hbxBPAll.skin="hbxIBSizelimiterRightGrey"
    		frmIBOpenNewDreamAcc.vboxMiddle.skin = "vbxMiddle13px"
    		frmIBOpenNewDreamAcc.vbxBPRight.skin = "vbxRightColumnGrey"
			setDataforDreamOpenFromActsIB(gblOpenDrmAcct);
			gbldreamCoverflow = false;
			frmIBOpenNewDreamAcc.hbxPictureIt.skin="hbxProperFocus";
			frmIBOpenNewDreamAcc.hbxBuildMyDream.setVisibility(false);
			frmIBOpenNewDreamAcc.hbxImage.setVisibility(false);
			frmIBOpenNewDreamAcc.hbxCoverflow.setVisibility(true);
			frmIBOpenNewDreamAcc.coverFlowTP.onSelect=onClickDreamSCoverFlowIB;
			frmIBOpenNewDreamAcc.image24573708914676.isVisible = false
			frmIBOpenNewDreamAcc.image2867539252331117.isVisible = true
			frmIBOpenNewDreamAcc.vboxMiddle.skin = "vbxMiddle13px"
			frmIBOpenNewDreamAcc.vbxBPRight.skin = "vbxRightColumnGrey"
			var a = frmIBOpenNewDreamAcc.lblTargetAccount.text.replace(":","");
			frmIBOpenNewDreamAcc.lblTargetAccount.text=a;
			if(kony.i18n.getCurrentLocale()=="en_US") frmIBOpenNewDreamAcc.lblDreamSavings.text=gblFinActivityLogOpenAct["prodNameEN"];
			if(kony.i18n.getCurrentLocale()=="th_TH") frmIBOpenNewDreamAcc.lblDreamSavings.text=gblFinActivityLogOpenAct["prodNameTH"] 
			frmIBOpenNewDreamAcc.vbxBPMiddleRight.skin="vbxMidRight"
			
			onClickDreamSCoverFlowIB(); 
			frmIBOpenNewDreamAcc.show();
		  }
     }
}
	 
function setDataforDreamOpenFromActsIB(resulttable)   {        			
			var list = [] ;
			var count = 0;
		     gblOpenActListSavingTarget =  resulttable["SavingTargets"]
			for (j = 0; j < resulttable["SavingTargets"].length; j++) {
				var dreamName;
				 dreamNameEn="";
				dreamNameTh = "";
				dreamNameEn = gblOpenActListSavingTarget[j]["DREAM_TARGET_DESC_EN"];
				dreamNameTh = gblOpenActListSavingTarget[j]["DREAM_TARGET_DESC_TH"];
				var dreamimage;
				var locale = kony.i18n.getCurrentLocale();
				if (locale == "en_US") {
								dreamName = gblOpenActListSavingTarget[j]["DREAM_TARGET_DESC_EN"];
								
						} else {
								dreamName = gblOpenActListSavingTarget[j]["DREAM_TARGET_DESC_TH"];
							
						}
				if (resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"] == "My Vacation") {
					dreamimage = "prod_vacation.png";
				} else if (resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"] == "My Dream") {
					dreamimage = "prod_dream.png";
				} else if (resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"] == "My Car") {
					dreamimage = "prod_car.png";
				} else if (resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"] == "My Education") {
					dreamimage = "prod_education.png";
				} else if (resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"] == "My Own House") {
					dreamimage = "prod_homeloan.png";
				} else if (resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"] == "My Business") {
					dreamimage = "prod_business.png";
				} else if (resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"] == "My Saving") {
					dreamimage = "prod_saving.png";
				}
				if (count == 0) {
									list.push({
										
								        img1: dreamimage,
										lblDream:dreamName,
										lblDreamEn:dreamNameEn, 
										lblDreamTh:dreamNameTh, 
										lblDreamTargetID:gblOpenActListSavingTarget[j]["DREAM_TARGET_ID"]
								       
									})
									count = count + 1
								}
								else if (count == 1) {
									list.push({
										
								        img1: dreamimage,
								       lblDream:dreamName,
								       lblDreamEn:dreamNameEn, 
										lblDreamTh:dreamNameTh, 
								       lblDreamTargetID:gblOpenActListSavingTarget[j]["DREAM_TARGET_ID"]
									})
									count = count + 1
								}
								else if (count == 2) {
									list.push({
										img1: dreamimage,
								       lblDream:dreamName,
								       lblDreamEn:dreamNameEn, 
										lblDreamTh:dreamNameTh, 
								       lblDreamTargetID:gblOpenActListSavingTarget[j]["DREAM_TARGET_ID"]
								       })
									count = 0
								}
					}
			var data1=list;
			frmIBOpenNewDreamAcc.coverFlowTP.data=data1;
			var fdata=frmIBOpenNewDreamAcc.coverFlowTP.data[gblCWSelectedItem];
			gblDreamCVDataTh = fdata.lblDreamTh
			gblDreamCVDataEn = fdata.lblDreamEn
			//dismissLoadingScreenPopup();	
			//frmIBOpenNewDreamAcc.lblDreamSavings.text=frmIBOpenProdDetnTnC.lblOpenActDescSubTitle.text
			if(kony.i18n.getCurrentLocale()=="en_US") {
			frmIBOpenNewDreamAcc.lblDreamSavings.text=gblFinActivityLogOpenAct["prodNameEN"]; 
				//frmIBOpenNewDreamAcc.lblPicturITText.text=fdata.lblDreamEn; 
			
			}
			if(kony.i18n.getCurrentLocale()=="th_TH"){
			 frmIBOpenNewDreamAcc.lblDreamSavings.text=gblFinActivityLogOpenAct["prodNameTH"];  
			 	//frmIBOpenNewDreamAcc.lblPicturITText.text=fdata.lblDreamTh; 
			 	
			 }
			 //frmIBOpenNewSavingsAcc.imgNSProdName.src = frmIBOpenActSelProd["segOpenActSelProd"]["selectedItems"][0].imgOpnActSelProd;
			frmIBOpenNewDreamAcc.txtODMnthSavAmt.text = "";
			frmIBOpenNewDreamAcc.txtODMnthSavAmtSave.text = "";
			frmIBOpenNewDreamAcc.txtODMyDream.text = "";
			frmIBOpenNewDreamAcc.txtODMyDreamSave.text = "";
			frmIBOpenNewDreamAcc.txtODNickNameVal.text = "";
			
			
			dismissLoadingScreenPopup();
			frmIBOpenNewDreamAcc.show();
		
	
}


function setDataforTermDepositPayingAccts(resulttable) {
    //
	var list = []
            		 var count = 0
            		 if(resulttable["custAcctToRec"] == null || resulttable["custAcctToRec"] == "" || resulttable["custAcctToRec"] == undefined ){
		            	kony.application.dismissLoadingScreen();
						showAlert(kony.i18n.getLocalizedString("keyMbActvNoAct"), kony.i18n.getLocalizedString("info"));
						return false;
	            	}
            		 
            		 var actResults = resulttable["custAcctToRec"];
            		
                     for (var i = 0; i < resulttable["custAcctToRec"].length; i++) {
                     	
                     	var productName = "";
						var locale = kony.i18n.getCurrentLocale();
                     	if (locale == "en_US") {
						productType = actResults[i]["ProductNameEng"];
						productName = productType;
						branchName = actResults[i]["BranchNameEh"];
						} else {
						productType = actResults[i]["ProductNameThai"];
						productName = productType;
						branchName = actResults[i]["BranchNameTh"];
						}
				 			hiddenProdENGAssignTD="";
                              hiddenProductThaiAssignTD=""; 
                            if ((resulttable["custAcctToRec"][i]["acctNickName"]) == null || (resulttable["custAcctToRec"][i]["acctNickName"]) == ''){
                            
                             var  sbStrOpenAct = resulttable["custAcctToRec"][i]["accId"];
                             var accLength = sbStrOpenAct.length;
                             var thaiProd = resulttable["custAcctToRec"][i]["ProductNameThai"];
							 var engProd = resulttable["custAcctToRec"][i]["ProductNameEng"];
                             sbStrOpenAct = sbStrOpenAct.substring(accLength - 4, accLength);
                             	
                             			if(thaiProd.length > 15){
										thaiProd = thaiProd.substring(0,15);
										}
										if(engProd.length > 15){
										engProd = engProd.substring(0,15);
										}
                             
                             hiddenProductThaiAssignTD = thaiProd +" "+ sbStrOpenAct; 
                             hiddenProdENGAssignTD = engProd+" "+ sbStrOpenAct;
                            
                            }else{
                            hiddenProdENGAssignTD = resulttable["custAcctToRec"][i]["acctNickName"];
                            hiddenProductThaiAssignTD= resulttable["custAcctToRec"][i]["acctNickName"]; 
                            }		
				
						//++++++++++++++++++++++++++
				    //   var availableBalance = actResults[j]["availableBalDisplay"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht")
					var acctNickName;	
                var accType = resulttable["custAcctToRec"][i]["accType"];
                var accVal = resulttable["custAcctToRec"][i]["accId"];
				var accValLength = accVal.length;
				if (accType != "CCA"){
				accVal = accVal.substring((accValLength-10), (accValLength-7)) + "-" + accVal.substring((accValLength-7), (accValLength-6)) + "-" + accVal.substring((accValLength-6), (accValLength-1)) + "-" + accVal.substring((accValLength-1), accValLength);
				}
				else{
				accVal = accVal.substring((accValLength-16), (accValLength-12)) + "-" + accVal.substring((accValLength-12), (accValLength-8)) + "-" + accVal.substring((accValLength-8), (accValLength-4)) + "-" + accVal.substring((accValLength-4), accValLength);
				}
				
                /*
				var  accTypeDisp;
				if(accType == "SDA"){
				accTypeDisp = kony.i18n.getLocalizedString("SavingMB");
				}else if(accType == "DDA"){
				accTypeDisp = kony.i18n.getLocalizedString("CurrentMB");
				}
				*/
				
                	if ((resulttable["custAcctToRec"][i]["acctNickName"]) == null || (resulttable["custAcctToRec"][i]["acctNickName"]) == ''){
							var sbStr = resulttable["custAcctToRec"][i]["accId"];
							var accLength = sbStr.length;
							sbStr = sbStr.substring(accLength - 4, accLength);
										if(productType.length > 15){
										productType = productType.substring(0,15);
										}
							acctNickName = productType + " " + sbStr;
							}
							else{
							acctNickName = resulttable["custAcctToRec"][i]["acctNickName"];
							}
                
                
		               var imageName = "";
					
							imageName="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+resulttable["custAcctToRec"][i]["ICON_ID"]+"&modIdentifier=PRODICON";
						if (count == 0) {
							list.push({
								lblProduct: kony.i18n.getLocalizedString("Product"),
        						lblProductVal: productName,
						        lblACno: kony.i18n.getLocalizedString("AccountNo"),
						        img1: imageName,
						        lblCustName: acctNickName,
						        lblBalance: resulttable["custAcctToRec"][i]["availableBalDisplay"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
						        lblActNoval : accVal,//formatAccountNo(resulttable["custAcctRec"][i]["accId"]),
						        lblRemFeeval:branchName, // resulttable["custAcctToRec"][i]["BranchNameEh"],
                                lblRemFee:kony.i18n.getLocalizedString("Branch"),
                                lblhideaccountName:resulttable["custAcctToRec"][i]["accountName"],
                                lblcreatedDate:resulttable["custAcctToRec"][i]["createdDate"],
                                lblfiident:resulttable["custAcctToRec"][i]["fiident"],
                                lblaccType:accType,//resulttable["custAcctToRec"][i]["accType"],
                                lblPersId:resulttable["custAcctToRec"][i]["persionlizeAcctId"],
                                lblavailbal:resulttable["custAcctToRec"][i]["availableBal"],
                                lblProdID:resulttable["custAcctToRec"][i]["productID"],
                                hiddenBranchEN: resulttable["custAcctToRec"][i]["BranchNameEh"],
								hiddenBranchTH: resulttable["custAcctToRec"][i]["BranchNameTh"],                                
                                lblCustNameEN:hiddenProdENGAssignTD,
                                lblCustNameTH:hiddenProductThaiAssignTD
							})
							count = count + 1
						}
						else if (count == 1) {
							list.push({
								lblProduct: kony.i18n.getLocalizedString("Product"),
        						lblProductVal: productName,
						        lblACno: kony.i18n.getLocalizedString("AccountNo"),
						        img1: imageName,
						        lblCustName: acctNickName,
						        lblBalance: resulttable["custAcctToRec"][i]["availableBalDisplay"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
						        lblActNoval : accVal,//formatAccountNo(resulttable["custAcctToRec"][i]["accId"]),
						        lblRemFeeval:branchName,
                                lblRemFee:kony.i18n.getLocalizedString("Branch"),                                
                                lblhideaccountName:resulttable["custAcctToRec"][i]["accountName"],
                                lblcreatedDate:resulttable["custAcctToRec"][i]["createdDate"],
                                lblfiident:resulttable["custAcctToRec"][i]["fiident"],
                                lblaccType:accType,//resulttable["custAcctToRec"][i]["accType"],
                                lblPersId:resulttable["custAcctToRec"][i]["persionlizeAcctId"],
                                lblavailbal:resulttable["custAcctToRec"][i]["availableBal"],
                                lblProdID:resulttable["custAcctToRec"][i]["productID"],
                                hiddenBranchEN: resulttable["custAcctToRec"][i]["BranchNameEh"],
								hiddenBranchTH: resulttable["custAcctToRec"][i]["BranchNameTh"], 
                                lblCustNameEN:hiddenProdENGAssignTD,
                                lblCustNameTH:hiddenProductThaiAssignTD
							})
							count = count + 1
						}
						else if (count == 2) {
							list.push({
								lblProduct: kony.i18n.getLocalizedString("Product"),
        						lblProductVal: productName,
						        lblACno: kony.i18n.getLocalizedString("AccountNo"),
						        img1: imageName,
						        lblCustName: acctNickName,
						        lblBalance: resulttable["custAcctToRec"][i]["availableBalDisplay"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
						        lblActNoval : accVal,//formatAccountNo(resulttable["custAcctToRec"][i]["accId"]),
						        lblRemFeeval:branchName,
                                lblRemFee:kony.i18n.getLocalizedString("Branch"),                            
                                lblhideaccountName:resulttable["custAcctToRec"][i]["accountName"],
                                lblcreatedDate:resulttable["custAcctToRec"][i]["createdDate"],
                                lblfiident:resulttable["custAcctToRec"][i]["fiident"],
                                lblaccType:accType,//resulttable["custAcctToRec"][i]["accType"],
                                lblPersId:resulttable["custAcctToRec"][i]["persionlizeAcctId"],
                                lblavailbal:resulttable["custAcctToRec"][i]["availableBal"],
                                lblProdID:resulttable["custAcctToRec"][i]["productID"],
                                hiddenBranchEN: resulttable["custAcctToRec"][i]["BranchNameEh"],
								hiddenBranchTH: resulttable["custAcctToRec"][i]["BranchNameTh"], 
                                lblCustNameEN:hiddenProdENGAssignTD,
                                lblCustNameTH:hiddenProductThaiAssignTD
							})
							count = 0
						}
					
				
                  }
	var data1=list;
	JSON.stringify("custrec values::", data1);
	frmIBOpenNewTermDepositAcc.coverFlowTP.data=data1;
	frmIBOpenNewTermDepositAcc.show();
	
}


function IBonClickPreShowSCAck(){
	frmIBOpenNewSavingsCareAccComplete.show();
}


function nextOfDreamAcc()
{
	var inputparam = {};
	inputparam["openActFlag"]= "true";
	invokeServiceSecureAsync("customerAccountInquiry", inputparam, callBackOpenAcntFromActsIBfromDream);
}	
	
	function callBackOpenAcntFromActsIBfromDream(status, resulttable) {
	if (status == 400) {
		
		if (resulttable["opstatus"] == 0) {
			gblOpenActList = resulttable;
            IBsetDataforOpenFromActsfromDream(gblOpenActList);
		}
		//else alert("opstatus not zero...");
	}
	//else alert("status not 400...");
}

function IBsetDataforOpenFromActsfromDream(resulttable) {
    //
	var list = []
            		 var count = 0
            		 
            		if(resulttable["custAcctFromRec"] == null || resulttable["custAcctFromRec"] == "" || resulttable["custAcctFromRec"] == undefined ){
		            	showAlert(kony.i18n.getLocalizedString("keyMbActvNoAct"), kony.i18n.getLocalizedString("info"));
		            	dismissLoadingScreenPopup()
						return false;
	            	}
	            	var actResults = resulttable["custAcctFromRec"];
                     for (var i = 0; i < resulttable["custAcctFromRec"].length; i++) {
                     	
                     	var productName = "";
                     	var locale = kony.i18n.getCurrentLocale();
                     	if (locale == "en_US") {
						productType = actResults[i]["ProductNameEng"];
						productName = productType;
						branchName = actResults[i]["BranchNameEh"];
						} else {
						productType = actResults[i]["ProductNameThai"];
						productName = productType;
						branchName = actResults[i]["BranchNameTh"];
						}
				 			hiddenProdENGAssign="";
                              hiddenProductThaiAssign=""; 
                             if ((resulttable["custAcctFromRec"][i]["acctNickName"]) == null || (resulttable["custAcctFromRec"][i]["acctNickName"]) == ''){
                            
                            	var  sbStrOpenAct = resulttable["custAcctFromRec"][i]["accId"];
                            	var accLength = sbStrOpenAct.length;
                            	var thaiProd = resulttable["custAcctFromRec"][i]["ProductNameThai"];
								var engProd = resulttable["custAcctFromRec"][i]["ProductNameEng"];
                            	sbStrOpenAct = sbStrOpenAct.substring(accLength - 4, accLength);
                             			if(thaiProd.length > 15){
										thaiProd = thaiProd.substring(0,15);
										}
										if(engProd.length > 15){
										engProd = engProd.substring(0,15);
										}
                             	
                             	hiddenProductThaiAssign = thaiProd +" "+ sbStrOpenAct; 
                             	hiddenProdENGAssign = engProd+" "+ sbStrOpenAct;
                             }else{
                            hiddenProdENGAssign = resulttable["custAcctFromRec"][i]["acctNickName"];
                            hiddenProductThaiAssign= resulttable["custAcctFromRec"][i]["acctNickName"]; 
                            
                            }		
				
				var acctNickNameDream;
				var accType = resulttable["custAcctFromRec"][i]["accType"];
                var accVal = resulttable["custAcctFromRec"][i]["accId"];
				var accValLength = accVal.length;
				if (accType != "CCA"){
				accVal = accVal.substring((accValLength-10), (accValLength-7)) + "-" + accVal.substring((accValLength-7), (accValLength-6)) + "-" + accVal.substring((accValLength-6), (accValLength-1)) + "-" + accVal.substring((accValLength-1), accValLength);
				}
				else{
				accVal = accVal.substring((accValLength-16), (accValLength-12)) + "-" + accVal.substring((accValLength-12), (accValLength-8)) + "-" + accVal.substring((accValLength-8), (accValLength-4)) + "-" + accVal.substring((accValLength-4), accValLength);
				}
				
                /*
				var  accTypeDisp;
				if(accType == "SDA"){
				accTypeDisp = kony.i18n.getLocalizedString("SavingMB");
				}else if(accType == "DDA"){
				accTypeDisp = kony.i18n.getLocalizedString("CurrentMB");
				}
				*/
							
							if ((resulttable["custAcctFromRec"][i]["acctNickName"]) == null || (resulttable["custAcctFromRec"][i]["acctNickName"]) == ''){
							var sbStr = resulttable["custAcctFromRec"][i]["accId"];
							var accLength = sbStr.length;
							sbStr = sbStr.substring(accLength - 4, accLength);
										
										if(productType.length > 15){
										productType = productType.substring(0,15);
										}
							
							acctNickNameDream = productType + " " + sbStr;
							}
							else{
							acctNickNameDream = resulttable["custAcctFromRec"][i]["acctNickName"];
							}
                
		               var imageName = "";
 					   imageName="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+resulttable["custAcctFromRec"][i]["ICON_ID"]+"&modIdentifier=PRODICON";
						if (count == 0) {
							list.push({
								lblProduct: kony.i18n.getLocalizedString("Product"),
        						lblProductVal: productName,
						        lblACno: kony.i18n.getLocalizedString("AccountNo"),
						        img1: imageName,
						        lblCustName: acctNickNameDream,
						        lblBalance: resulttable["custAcctFromRec"][i]["availableBalDisplay"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
						        lblActNoval : accVal,//formatAccountNo(resulttable["custAcctRec"][i]["accId"]),
						        lblRemFeeval:branchName,
                                lblRemFee:kony.i18n.getLocalizedString("Branch"),
                                lblhideaccountName:resulttable["custAcctFromRec"][i]["accountName"],
                                lblcreatedDate:resulttable["custAcctFromRec"][i]["createdDate"],
                                lblfiident:resulttable["custAcctFromRec"][i]["fiident"],
                                lblaccType:accType,//resulttable["custAcctFromRec"][i]["accType"],
                                lblPersId:resulttable["custAcctFromRec"][i]["persionlizeAcctId"],
                                lblavailbal:resulttable["custAcctFromRec"][i]["availableBal"],
                                lblProdID:resulttable["custAcctFromRec"][i]["productID"],
                                hiddenBranchEN: resulttable["custAcctFromRec"][i]["BranchNameEh"],
								hiddenBranchTH: resulttable["custAcctFromRec"][i]["BranchNameTh"],
                                hiddenProdENG: hiddenProdENGAssign,
                                hiddenproductThai : hiddenProductThaiAssign  
                                
							})
							
							count = count + 1
						}
						else if (count == 1) {
							list.push({
								lblProduct: kony.i18n.getLocalizedString("Product"),
        						lblProductVal: productName,
						        lblACno: kony.i18n.getLocalizedString("AccountNo"),
						        img1: imageName,
						       lblCustName: acctNickNameDream,
						        lblBalance: resulttable["custAcctFromRec"][i]["availableBalDisplay"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
						        lblActNoval : accVal,//formatAccountNo(resulttable["custAcctFromRec"][i]["accId"]),
						        lblRemFeeval:branchName,
                                lblRemFee:kony.i18n.getLocalizedString("Branch"),                                
                                lblhideaccountName:resulttable["custAcctFromRec"][i]["accountName"],
                                lblcreatedDate:resulttable["custAcctFromRec"][i]["createdDate"],
                                lblfiident:resulttable["custAcctFromRec"][i]["fiident"],
                                lblaccType:accType,//resulttable["custAcctFromRec"][i]["accType"],
                                lblPersId:resulttable["custAcctFromRec"][i]["persionlizeAcctId"],
                                lblavailbal:resulttable["custAcctFromRec"][i]["availableBal"],
                                lblProdID:resulttable["custAcctFromRec"][i]["productID"],
                                hiddenBranchEN: resulttable["custAcctFromRec"][i]["BranchNameEh"],
								hiddenBranchTH: resulttable["custAcctFromRec"][i]["BranchNameTh"],
                                hiddenProdENG: hiddenProdENGAssign,
                                hiddenproductThai : hiddenProductThaiAssign  
							})
							count = count + 1
						}
						else if (count == 2) {
							list.push({
								lblProduct: kony.i18n.getLocalizedString("Product"),
        						lblProductVal: productName,
						        lblACno: kony.i18n.getLocalizedString("AccountNo"),
						        img1: imageName,
						        lblCustName: acctNickNameDream,
						        lblBalance: resulttable["custAcctFromRec"][i]["availableBalDisplay"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
						        lblActNoval : accVal,//formatAccountNo(resulttable["custAcctFromRec"][i]["accId"]),
						        lblRemFeeval: branchName,
                                lblRemFee:kony.i18n.getLocalizedString("Branch"),                            
                                lblhideaccountName:resulttable["custAcctFromRec"][i]["accountName"],
                                lblcreatedDate:resulttable["custAcctFromRec"][i]["createdDate"],
                                lblfiident:resulttable["custAcctFromRec"][i]["fiident"],
                                lblaccType:accType,//resulttable["custAcctFromRec"][i]["accType"],
                                lblPersId:resulttable["custAcctFromRec"][i]["persionlizeAcctId"],
                                lblavailbal:resulttable["custAcctFromRec"][i]["availableBal"],
                                lblProdID:resulttable["custAcctFromRec"][i]["productID"],
                                hiddenBranchEN: resulttable["custAcctFromRec"][i]["BranchNameEh"],
								hiddenBranchTH: resulttable["custAcctFromRec"][i]["BranchNameTh"],
                                hiddenProdENG: hiddenProdENGAssign,
                                hiddenproductThai : hiddenProductThaiAssign  
							})
							count = 0
						}
					
				
                  }
	var data1=list;
	if (list.length <= 0) {
       			dismissLoadingScreenPopup();
       			 showAlert(kony.i18n.getLocalizedString("keyMbActvNoAct"), kony.i18n.getLocalizedString("info"));
				return false;
       } 
	JSON.stringify("custrec values::", data1);
	frmIBOpenNewSavingsAcc.coverFlowTP.data=data1;
	dismissLoadingScreenPopup();
	IBonclickSavingsAccountCoverFlow();
	frmIBOpenNewSavingsAcc.coverFlowTP.setVisibility(true);
	frmIBOpenNewSavingsAcc.hbxCoverFlowTP.setVisibility(true);
	frmIBOpenNewSavingsAcc.show();
}


function IBOTPTimercallbackOpenAccount() {
	//curr_form=kony.application.getCurrentForm();
var curr_form;
					if(gblSelProduct == "ForUse"){
							curr_form = frmIBOpenNewSavingsAccConfirmation;
						}else if(gblSelProduct == "TMBDreamSavings"){
							curr_form = frmIBOpenNewDreamAccConfirmation;
						}else if(gblSelProduct == "TMBSavingcare"){
						   curr_form = frmIBOpenNewSavingsCareAccConfirmation
						//	IBonClickPreShowSCConfirm();				
						}else if(gblSelProduct == "ForTerm"){
							curr_form= frmIBOpenNewTermDepositAccConfirmation
						}
	curr_form.btnRequest.skin = btnIBREQotpFocus;
	curr_form.btnRequest.onClick = IBreqOTPMYNewAccnts;
	
	try {
		kony.timer.cancel("OtpTimer");
	} catch (e) {
		
	}
}

function dataTransfrFromConfToComDream()
{
//    frmIBOpenNewDreamAccComplete.imgDSAckTitle.src = frmIBOpenNewDreamAccConfirmation.imgDSCnfmTitle.src;
				    frmIBOpenNewDreamAccComplete.lblDSAckTitle.text = frmIBOpenNewDreamAccConfirmation.lblDreamSavings.text;
				  //frmIBOpenNewDreamAccComplete.lblMyDreamDesValAck.text = frmIBOpenNewDreamAccConfirmation.lblMyDreamDesVal.text;
				    frmIBOpenNewDreamAccComplete.imgOADreamDetail.src = frmIBOpenNewDreamAccConfirmation.imgOADreamDetail.src;
				    frmIBOpenNewDreamAccComplete.lblOADreamDetailTarAmtVal.text = frmIBOpenNewDreamAccConfirmation.lblOADreamDetailTarAmtVal.text;
				    frmIBOpenNewDreamAccComplete.lblNickNameVal.text = frmIBOpenNewDreamAccConfirmation.lblOADSNickNameVal.text;
				//  frmIBOpenNewDreamAccComplete.lblOADSActNameValAck.text = frmIBOpenNewDreamAccConfirmation.lblOADSActNameVal.text;
				 	frmIBOpenNewDreamAccComplete.lblOADSAmtVal.text = frmIBOpenNewDreamAccConfirmation.lblOADSAmtVal.text;
				    frmIBOpenNewDreamAccComplete.lblBranchVal.text = frmIBOpenNewDreamAccConfirmation.lblOADSBranchVal.text;
					frmIBOpenNewDreamAccComplete.lblOADMnthVal.text = frmIBOpenNewDreamAccConfirmation.lblOADMnthVal.text;
				    frmIBOpenNewDreamAccComplete.lblInterestRateVal.text = frmIBOpenNewDreamAccConfirmation.lblInterestValue.text;
				    frmIBOpenNewDreamAccComplete.lblOpeningDateVal.text = frmIBOpenNewDreamAccConfirmation.lblOpeningDateValue.text;
				 	frmIBOpenNewDreamAccComplete.lblNFAccName.text = frmIBOpenNewDreamAccConfirmation.lblOAMnthlySavName.text;
				    frmIBOpenNewDreamAccComplete.lblNFAccBal.text = frmIBOpenNewDreamAccConfirmation.lblOAMnthlySavNum.text;
				    frmIBOpenNewDreamAccComplete.lblNFAccActType.text = frmIBOpenNewDreamAccConfirmation.lblOAMnthlySavActType.text;
					frmIBOpenNewDreamAccComplete.image2866854059250111.src = frmIBOpenNewDreamAccConfirmation.imgOAMnthlySavPic.src;
			//	 	frmIBOpenNewDreamAccComplete.lblIBOpenAccCnfmBalAmt.text =parseFloat(frmIBOpenNewDreamAccConfirmation.lblOAMnthlySavBal.text)-parseFloat(frmIBOpenNewDreamAccConfirmation.lblOADSAmtVal.text)
}
function dataTransfrfromConfToCompTerm()
{
		//    frmIBOpenNewTermDepositAccComplete.imgOATDAckTitle.src = frmIBOpenNewTermDepositAccConfirmation.imgTDCnfmTitle.src;
				        frmIBOpenNewTermDepositAccComplete.lblType.text = frmIBOpenNewTermDepositAccConfirmation.lblTDCnfmTitle.text;
				    frmIBOpenNewTermDepositAccComplete.lblOATDNickNameVal.text = frmIBOpenNewTermDepositAccConfirmation.lblOATDNickNameVal.text;
				    frmIBOpenNewTermDepositAccComplete.lblOpeningAmountValue.text = frmIBOpenNewTermDepositAccConfirmation.lblAccNameValue.text;
				    frmIBOpenNewTermDepositAccComplete.lblBranchVal.text = frmIBOpenNewTermDepositAccConfirmation.lblBranchVal.text;
				    frmIBOpenNewTermDepositAccComplete.lblOATDAmtVal.text = frmIBOpenNewTermDepositAccConfirmation.lblOATDAmtVal.text;
				    frmIBOpenNewTermDepositAccComplete.lblInterestVal.text = frmIBOpenNewTermDepositAccConfirmation.lblInterestVal.text;
				    frmIBOpenNewTermDepositAccComplete.lblOpeningDateVal.text = frmIBOpenNewTermDepositAccConfirmation.lblOpeningDateVal.text;
				    frmIBOpenNewTermDepositAccComplete.lblTdTransRefVal.text = frmIBOpenNewTermDepositAccConfirmation.lblTdTransRefVal.text;
				    frmIBOpenNewTermDepositAccComplete.lblPayingToName.text = frmIBOpenNewTermDepositAccConfirmation.lblPayingToName.text;
				     frmIBOpenNewTermDepositAccComplete.lblAccountBalanceTypeFrom.text = frmIBOpenNewTermDepositAccConfirmation.lblPayingFromAccountType.text;
				    frmIBOpenNewTermDepositAccComplete.lblPayingToAccountType.text = frmIBOpenNewTermDepositAccConfirmation.lblPayingToAccountType.text;
				    frmIBOpenNewTermDepositAccComplete.lblAccountBalance.text = frmIBOpenNewTermDepositAccConfirmation.lblOpeningFromAccount.text;
				     frmIBOpenNewTermDepositAccComplete.lblPayingToAccount.text = frmIBOpenNewTermDepositAccConfirmation.lblPayingToAccount.text;
				//   frmIBOpenNewTermDepositAccComplete.lblOATDToActType.text = frmIBOpenNewTermDepositAccConfirmation.lblTDTransToActType.text;
				//    frmIBOpenNewTermDepositAccComplete.lblOATDToAmt.text = frmIBOpenNewTermDepositAccConfirmation.lblTDTransToAccBal.text;
				    frmIBOpenNewTermDepositAccComplete.lblAccNSName.text = frmIBOpenNewTermDepositAccConfirmation.lblAccNSName.text;
				//    frmIBOpenNewTermDepositAccComplete.lblOAFrmTypeAck.text = frmIBOpenNewTermDepositAccConfirmation.lblTDTransFrmActType.text;
				//    frmIBOpenNewTermDepositAccComplete.lblIBOpenAccCnfmBalAmt.text = parseFloat(frmIBOpenNewTermDepositAccConfirmation.lblIBOpenAccCnfmBalAmt.text)-parseFloat(frmIBOpenNewTermDepositAccConfirmation.lblOATDAmtVal.text)
				    frmIBOpenNewTermDepositAccComplete.image2866854059250111.src = frmIBOpenNewTermDepositAccConfirmation.imageOpeningAmtFrom.src;
				    frmIBOpenNewTermDepositAccComplete.image2867569043311375.src = frmIBOpenNewTermDepositAccConfirmation.imagePayIntTo.src;  
}
function dataTrasfrFromConfToCompSavingCare()
	{
    frmIBOpenNewSavingsCareAccComplete.lblOASCNicNamVal.text = frmIBOpenNewSavingsCareAccConfirmation.lblOASCNicNamVal.text;
    frmIBOpenNewSavingsCareAccComplete.lblOASCActNameVal.text = frmIBOpenNewSavingsCareAccConfirmation.lblOASCActNameVal.text;
    frmIBOpenNewSavingsCareAccComplete.lblOASCOpenActVal.text = frmIBOpenNewSavingsCareAccConfirmation.lblOASCOpenActVal.text;
    frmIBOpenNewSavingsCareAccComplete.lblOASCBranchVal.text = frmIBOpenNewSavingsCareAccConfirmation.lblOASCBranchVal.text; 
    frmIBOpenNewSavingsCareAccComplete.lblOASCIntRateVal.text = frmIBOpenNewSavingsCareAccConfirmation.lblOASCIntRateVal.text;
    frmIBOpenNewSavingsCareAccComplete.lblOASCOpenDateVal.text = frmIBOpenNewSavingsCareAccConfirmation.lblOASCOpenDateVal.text;
    frmIBOpenNewSavingsCareAccComplete.lblNFAccName.text = frmIBOpenNewSavingsCareAccConfirmation.lblOASCFrmNameAck.text;
    frmIBOpenNewSavingsCareAccComplete.lblNFAccBal.text = frmIBOpenNewSavingsCareAccConfirmation.lblOASCFrmNumAck.text;
    frmIBOpenNewSavingsCareAccComplete.imgNoFix.src = frmIBOpenNewSavingsCareAccConfirmation.imgOASCFrmPicAck.src;
    frmIBOpenNewSavingsCareAccComplete.lblOASCFrmNumAck.text = frmIBOpenNewSavingsCareAccConfirmation.lblOASCFrmNum.text;
    frmIBOpenNewSavingsCareAccComplete.lblOASCTranRefNoVal.text = frmIBOpenNewSavingsCareAccConfirmation.lblOASCTranRefNoVal.text;
  //  frmIBOpenNewSavingsCareAccComplete.lblIBOpenAccCnfmBalAmt.text = parseFloat(frmIBOpenNewSavingsCareAccConfirmation.lblBalance.text)-parseFloat(frmIBOpenNewSavingsCareAccConfirmation.lblOASCOpenActVal.text)
	}

	
function IBcallGenerateTransferRefNoserviceOpenActMB(){
	var inputParam = {};
	inputParam["transRefType"]="NT";
	showLoadingScreenPopup();
	invokeServiceSecureAsync("generateTransferRefNo", inputParam, IBcallGenerateTransferRefNoserviceOpenActMBCallBack);
}

function IBcallGenerateTransferRefNoserviceOpenActMBCallBack(status,result){

 
 if(status == 400) //success responce
 {
   
  if(result["opstatus"] == 0)
  {
      
   var refNum = result["transRefNum"];  
   gblTransferRefNo = refNum  + "00";
   
   			if(gblSelProduct == "ForUse"){
   				frmIBOpenNewSavingsAccConfirmation.lbltransactionVal.text = refNum + "00";
   					dismissLoadingScreenPopup();
					//frmIBOpenNewSavingsAccConfirmation.show();
				}else if(gblSelProduct == "TMBDreamSavings"){
				//	frmIBOpenNewDreamAccConfirmation.lblTransRefNoValue.text=refNum + "00";
					dismissLoadingScreenPopup();
					//frmIBOpenNewDreamAccConfirmation.show();				
				}else if(gblSelProduct == "TMBSavingcare"){
					dismissLoadingScreenPopup();
					frmIBOpenNewSavingsCareAccConfirmation.lblOASCTranRefNoVal.text = refNum + "00";
					//IBonClickPreShowSCConfirm();				
				}else if(gblSelProduct == "ForTerm"){
					dismissLoadingScreenPopup();
					frmIBOpenNewTermDepositAccConfirmation.lblTdTransRefVal.text = refNum + "00";
				//	frmIBOpenNewTermDepositAccConfirmation.show();				
					frmIBOpenNewTermDepositAccConfirmation.show();	
									
				}
      
  //return refNum;
  }
  else
    {
     dismissLoadingScreenPopup();
     alert(" "+result["errMsg"]);  
    }
 } 
}	


function IBcleanDataForNormalSavings()
{
	frmIBOpenNewSavingsAcc.txtOpenActSelAmt.text=""
	frmIBOpenNewSavingsAcc.txtOpenActNicNam.text=""
	frmIBOpenNewSavingsAcc.hbox47679117769442.isVisible=false;
	frmIBOpenNewSavingsAccConfirmation.txtOTP.text="";
	frmIBOpenNewSavingsAccConfirmation.lblBankRefNoVal.text="";
	frmIBOpenNewSavingsAccConfirmation.lblOTPMblNum.text="";
}

function IBcleanDataForSavingsCare()
{
	frmIBOpenNewSavingsCareAcc.txtAmount.text = commaFormattedOpenAct(gblMinOpenAmt) + kony.i18n.getLocalizedString("currencyThaiBaht");
	frmIBOpenNewSavingsCareAcc.txtNickName.text="";
	frmIBOpenNewSavingsCareAcc.txtBefBen1.text="";
	frmIBOpenNewSavingsCareAcc.txtBefBen2.text="";
	frmIBOpenNewSavingsCareAcc.txtBefBen3.text=""
	frmIBOpenNewSavingsCareAcc.txtBefBen4.text=""
	frmIBOpenNewSavingsCareAcc.txtBefBen5.text="";
	frmIBOpenNewSavingsCareAcc.txtBefFisrtName1.text=""
	frmIBOpenNewSavingsCareAcc.txtBefFisrtName2.text=""
	frmIBOpenNewSavingsCareAcc.txtBefFisrtName3.text=""
	frmIBOpenNewSavingsCareAcc.txtBefFisrtName4.text=""
	frmIBOpenNewSavingsCareAcc.txtBefFisrtName5.text="";
	frmIBOpenNewSavingsCareAcc.txtBefSecName1.text="";
	frmIBOpenNewSavingsCareAcc.txtBefSecName2.text="";
	frmIBOpenNewSavingsCareAcc.txtBefSecName3.text=""
	frmIBOpenNewSavingsCareAcc.txtBefSecName4.text=""
	frmIBOpenNewSavingsCareAcc.txtBefSecName5.text="";
	frmIBOpenNewSavingsCareAccConfirmation.txtOTP=text="";
	frmIBOpenNewSavingsCareAccConfirmation.lblBankRefNoVal.text=""
	frmIBOpenNewSavingsCareAccConfirmation.lblOTPMblNum.text=""
}
function IBcleanDataForTermDeposit()
{
	frmIBOpenNewTermDepositAcc.txtDSNickName.text="";
	frmIBOpenNewTermDepositAcc.txtTDamount.text= commaFormattedOpenAct(gblMinOpenAmt) +" "+ kony.i18n.getLocalizedString("currencyThaiBaht");
	frmIBOpenNewTermDepositAcc.hbox866794452643234.isVisible=false
	frmIBOpenNewTermDepositAcc.hbox47679117769442.isVisible=false
	frmIBOpenNewTermDepositAccConfirmation.txtOTP.text="";
	frmIBOpenNewTermDepositAccConfirmation.lblBankRefNoVal.text="";
	frmIBOpenNewTermDepositAccConfirmation.lblOTPMblNum.text="";
}
function IBcleanDataForDreamSavings()
{
	frmIBOpenNewDreamAcc.txtODMnthSavAmt.text="";
	frmIBOpenNewDreamAcc.txtODMnthSavAmtSave.text="";
	frmIBOpenNewDreamAcc.txtODMyDream.text="";
	frmIBOpenNewDreamAcc.txtODMyDreamSave.text="";
	frmIBOpenNewDreamAcc.txtODNickNameVal.text="";
	frmIBOpenNewDreamAcc.txtODTargetAmt.text="";
	frmIBOpenNewSavingsAcc.txtOpenActSelAmt.text=""
	frmIBOpenNewSavingsAcc.txtOpenActNicNam.text=""
	frmIBOpenNewSavingsAcc.hbox47679117769442.isVisible=false;
	frmIBOpenNewSavingsAcc.txtDSAmountSel.text="";
	frmIBOpenNewDreamAccConfirmation.txtOTP.text="";
	frmIBOpenNewDreamAccConfirmation.lblBankRefNoVal.text="";
	frmIBOpenNewDreamAccConfirmation.lblOTPMblNum.text="";
}

function IBDreamValidation(amount) {
	if (amount != null && amount != "") {
		if (kony.string.containsOnlyGivenChars(amount, ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", ",", "."])) {		
			return true;
		} else {			
			return false;
		}
	}
}

function IBsaveAsPdfImageOpenActAll(filetype) 
{
    var inputParam = {}
    var term = "";
    var openAcctType = "";
   	if (gblSelProduct == "TMBDreamSavings") 
	{	
	 inputParam["templatename"] = "OpenAccountDS";	  // name of xsl file without extension
    openAcctType = "DS";
    gblFinActivityLogOpenAct["toProductName"] = frmIBOpenNewDreamAccComplete.lblDSAckTitle.text;
     term = commaFormattedOpenAct(gblFinActivityLogOpenAct["transferAmount"]) + kony.i18n.getLocalizedString("currencyThaiBaht");
    }
	else if (gblSelProduct == "ForTerm")
	{
	 inputParam["templatename"] = "OpenAccountTD"; // name of xsl file without extension
 	 openAcctType = "TD";
 		if (gblisDisToActTD == "Y")
		{
 		inputParam["affiliatedAccountNo"] = frmIBOpenNewTermDepositAccComplete.lblPayingToAccountType.text;
        inputParam["affiliatedAccountName"] = gblFinActivityLogOpenAct["affiliatedAcctName"];
        }else{
        inputParam["affiliatedAccountNo"] = "";
        inputParam["affiliatedAccountName"] ="";
        }
       gblFinActivityLogOpenAct["toProductName"] = frmIBOpenNewTermDepositAccComplete.lblTDCnfmTitle.text;
         term = frmIBOpenNewTermDepositAccComplete.lblTermval.text;
	}
	else if (gblSelProduct == "TMBSavingcare") {
	for(var i = 0; i<gblOpenActBenList.length ; i++){
		gblOpenActBenList[i]["careBeneficiaryName"] = gblOpenActBenList[i]["firstName"] + " " + gblOpenActBenList[i]["lastName"]
		gblOpenActBenList[i]["careBeneficiaryRelation"] = gblOpenActBenList[i]["relationShip"]
		gblOpenActBenList[i]["careBeneficiaryPct"] = gblOpenActBenList[i]["benifit"] + "%"
	}
	if(gblOpenActBenList.length > 0){
		inputParam["templatename"] = "OpenAccountSC"; // name of xsl file without extension
	}else{
		inputParam["templatename"] = "OpenAccountSCNoBen"; // name of xsl file without extension
	}
	//inputParam["templatename"] = "OpenAccountSC"; // name of xsl file without extension
    gblFinActivityLogOpenAct["toProductName"] = frmIBOpenNewSavingsCareAccComplete.lblOASCTitle.text;
    openAcctType = "SC";
	}
	else {
	inputParam["templatename"] = "OpenAccountNS";
	openAcctType = "NS";
    gblFinActivityLogOpenAct["toProductName"] = frmIBOpenNewSavingsAccComplete.lblNSProdName.text;
	}
	inputParam["filetype"] = filetype;
	inputParam["outputtemplatename"] = "OpenAccountDetails_"+kony.os.date("DDMMYYYY");
	var meturityDate = frmIBOpenNewTermDepositAccComplete.lblMaturityDateVal.text;
	
	accountNameinPDF = "";
	if (gblPartyInqOARes["issuedIdentType"] == "CI" ){
	
		accountNameinPDF = gblPartyInqOARes["FullName"];
	}
	else
	{
		accountNameinPDF = gblPartyInqOARes["PrefName"];
	}
  	var toAccountID = "";
  	if( gblFinActivityLogOpenAct["toActId"].length >10){
    	toAccountID = formatAccountNumber(gblFinActivityLogOpenAct["toActId"]);
    }else{
    	toAccountID = formatAccountNumber(gblFinActivityLogOpenAct["toActId"]);
    }
	var amountFinal = frmIBOpenNewSavingsCareAccComplete.lblOASCOpenActVal.text.replace(kony.i18n.getLocalizedString("currencyThaiBaht"),"")
    var pdfImagedata = 
    {
     "localeId":kony.i18n.getCurrentLocale(), 
      "fromAccountNo":"xxx-x-" + gblFinActivityLogOpenAct["fromActIdFormatted"].substring(6, 11) + "-x",
      "productName":gblFinActivityLogOpenAct["toProductName"],
      //"fromAccountName":accountNameinPDF,
      "fromAccountName":gblFinActivityLogOpenAct["fromActName"],
      "toAccountNo":toAccountID,
      "toAccountName":gblFinActivityLogOpenAct["toActName"],
	  "branch":gblFinActivityLogOpenAct["toBranchName"],
      "Amount":commaFormattedOpenAct(gblFinActivityLogOpenAct["transferAmount"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
	  //"Amount":gblFinActivityLogOpenAct["transferAmount"] +  kony.i18n.getLocalizedString("currencyThaiBaht"),
	  //"Amount":amountFinal.replace(",", ""),
      "openingDate":gblFinActivityLogOpenAct["openingDate"],
      "TransactionRefNo":gblTransferRefNo,
      "dreamdesc":gblFinActivityLogOpenAct["dreamdesc"],
      // "targetAmount":commaFormattedOpenAct(gblFinActivityLogOpenAct["targetAmount"]) +  kony.i18n.getLocalizedString("currencyThaiBaht"),
      "targetAmount":gblFinActivityLogOpenAct["targetAmount"] +  kony.i18n.getLocalizedString("currencyThaiBaht"),
      "term":term,
      "transferrecdate":gblFinActivityLogOpenAct["transferrecdate"],
      "maturityDate":meturityDate, 
      "beneficiariesList" : gblOpenActBenList,
      "affiliatedAccountNo" : "",
      "affiliatedAccountName": "",
      "localeId" : kony.i18n.getCurrentLocale()      
    };
    
    if (gblSelProduct == "ForTerm"){
    
    	if (gblisDisToActTD == "Y")
		{
 		pdfImagedata.affiliatedAccountNo = "xxx-x-" + gblFinActivityLogOpenAct["affiliatedAcctId"].substring(6, 11) + "-x";
        pdfImagedata.affiliatedAccountName = gblFinActivityLogOpenAct["affiliatedAcctName"];
        }
    
    }
	
     inputParam["datajson"] = JSON.stringify(pdfImagedata, "", "");
     kony.print("Coming here in pdf");
    // saveOpenAccountPDFIB(filetype, "063", gblTransferRefNo, openAcctType);
	invokeServiceSecureAsync("generatePdfImage", inputParam, ibRenderFileCallbackfunction);
     
}



function IBOnClickPrinter(filetype){
	var inputParam = {};
	var scheduleDetails = "";
	var pdfImagedata = {};
	var term = "";
	var meturityDate = "";
	fileType = filetype;
	
	if (gblSelProduct == "TMBDreamSavings") 
	{	
	 inputParam["templatename"] = "OpenAccountDS"; // name of xsl file without extension
	 gblFinActivityLogOpenAct["toProductName"] = frmIBOpenNewDreamAccComplete.lblDSAckTitle.text;
	 gblFinActivityLogOpenAct["toActName"] = frmIBOpenNewDreamAccComplete.lblDreamOpenAcctNamComVal.text;
     term = commaFormattedOpenAct(gblFinActivityLogOpenAct["transferAmount"]) + kony.i18n.getLocalizedString("currencyThaiBaht");
   //  openAcctType = "DS"
    }
	else if (gblSelProduct == "ForTerm")
	{
	 inputParam["templatename"] = "OpenAccountTD"; // name of xsl file without extension
	 gblFinActivityLogOpenAct["toProductName"]= frmIBOpenNewTermDepositAccComplete.lblTDCnfmTitle.text;
	 term = frmIBOpenNewTermDepositAccComplete.lblTermval.text;
	 meturityDate = frmIBOpenNewTermDepositAccComplete.lblMaturityDateVal.text;
	// openAcctType = "TD"
	}
	else if (gblSelProduct == "TMBSavingcare") {
	for(var i = 0; i<gblOpenActBenList.length ; i++){
		gblOpenActBenList[i]["careBeneficiaryName"] = gblOpenActBenList[i]["firstName"] + " " + gblOpenActBenList[i]["lastName"]
		gblOpenActBenList[i]["careBeneficiaryRelation"] = gblOpenActBenList[i]["relationShip"]
		gblOpenActBenList[i]["careBeneficiaryPct"] = gblOpenActBenList[i]["benifit"] + "%"
		//openAcctType = "SC"
	}
	gblFinActivityLogOpenAct["toProductName"] = frmIBOpenNewSavingsCareAccComplete.lblOASCTitle.text;
	if(gblOpenActBenList.length > 0){
		inputParam["templatename"] = "OpenAccountSC"; // name of xsl file without extension
	}else{
		inputParam["templatename"] = "OpenAccountSCNoBen"; // name of xsl file without extension
	}
	
	}
	else {
	gblFinActivityLogOpenAct["toProductName"] = frmIBOpenNewSavingsAccComplete.lblNSProdName.text;
	inputParam["templatename"] = "OpenAccountNS";
	//openAcctType = "NS"
	}
	
	
	
	var pdfImagedata = {
      "localeId":kony.i18n.getCurrentLocale(), 
      "fromAccountNo":"xxx-x-" + gblFinActivityLogOpenAct["fromActIdFormatted"].substring(6, 11) + "-x",
      "productName":gblFinActivityLogOpenAct["toProductName"],
      "fromAccountName":gblFinActivityLogOpenAct["fromActName"],
      "toAccountNo":"xxx-x-" + gblFinActivityLogOpenAct["toActId"].substring(6, 11)+ "-x",
      "toAccountName":gblFinActivityLogOpenAct["toActName"],
	  "branch":gblFinActivityLogOpenAct["toBranchName"],
      "Amount":commaFormattedOpenAct(gblFinActivityLogOpenAct["transferAmount"]) + kony.i18n.getLocalizedString("currencyThaiBaht"),
      "openingDate":gblFinActivityLogOpenAct["openingDate"],
      "openingdate":gblFinActivityLogOpenAct["openingdate"],
      "TransactionRefNo":gblTransferRefNo,
      "dreamdesc":gblFinActivityLogOpenAct["dreamdesc"],
      "targetAmount":gblFinActivityLogOpenAct["targetAmount"] + kony.i18n.getLocalizedString("currencyThaiBaht"),
      "term":term,
      "transferrecdate":gblFinActivityLogOpenAct["transferrecdate"],
      "affiliatedAccountNo":"",
      "affiliatedAccountName":"",
      "maturityDate":meturityDate,
      "beneficiariesList" : gblOpenActBenList,
      "localeId" : kony.i18n.getCurrentLocale()             
       }
    
    inputParam["filetype"] = fileType;
     
     
     if (gblSelProduct == "ForTerm"){
    
    	if (gblisDisToActTD == "Y")
		{
 		pdfImagedata.affiliatedAccountNo = "xxx-x-" + gblFinActivityLogOpenAct["affiliatedAcctId"].substring(6, 11) + "-x";
        pdfImagedata.affiliatedAccountName = gblFinActivityLogOpenAct["affiliatedAcctNickname"];
        }
    
    }
	
	
    inputParam["datajson"] = JSON.stringify(pdfImagedata, "", "");
    inputParam["outputtemplatename"] = "OpenAccountDetails_"+kony.os.date("DDMMYYYY");
    //saveOpenAccountPDFIB(filetype, "063", gblTransferRefNo, openAcctType);
   invokeServiceSecureAsync("generatePdfImage", inputParam, ibRenderFileCallbackfunction);
}


function IBcheckIsEligibleToOpenAct(acctId,linkedAccType) {


    var inputparam = {};
    if (linkedAccType == "SDA") {
        inputparam["spName"] = "com.fnf.xes.ST"; //14digits
    }
    if (linkedAccType == "DDA") {
        inputparam["spName"] = "com.fnf.xes.IM"; //14digits
    }
    inputparam["acctId"] = acctId;
    inputparam["acctType"] = linkedAccType;
    
    inputparam["clientDt"] = "";
    inputparam["name"] = "IB-INQ";
    invokeServiceSecureAsync("RecurringFundTransinq", inputparam, callBackcheckIsEligibleToOpenActIB);
   
}


function callBackcheckIsEligibleToOpenActIB(status,resulttable){
 if (status == 400) {
        
        if (resulttable["opstatus"] == 0) {

            if (resulttable["toAcctId"] != null && resulttable["toAcctId"] != "") {
            dismissLoadingScreen();
            dismissLoadingScreenPopup();
		 	showAlert(kony.i18n.getLocalizedString("keyOpenActMapped"), kony.i18n.getLocalizedString("info"));
		 	return false            
            }
            else{
            
	       
             IBinvokePartyInquiryOpenAct();
            
            }

    }else{
    		dismissLoadingScreen();
    		dismissLoadingScreenPopup();
		 	showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
		 	return false;    
    }
  }
}


function OTPValdatnOpen(text) {
 if(gblTokenSwitchFlag==false || gblTokenSwitchFlag=="0" || gblTokenSwitchFlag==""){
	if (text == null || text == '') {
	alert(kony.i18n.getLocalizedString("Receipent_alert_correctOTP"));
	return false;
	}
	var txtLen = text.length;
	
	if (txtLen > gblOTPLENGTH) {
		alert("OTP code should be maximum " + gblOTPLENGTH + " characters");
		return false;
	}
	}else{
	var currForm = kony.application.getCurrentForm();
	text = currForm.tbxToken.text;
	if(text==""||text==null){
		//alert("Please Enter token");
        alert(kony.i18n.getLocalizedString("Receipent_tokenId"));
		return false;
	}
	var isNum = kony.string.isNumeric(text);
	if (!isNum) {
		alert("Token should be numeric");
		return false;
	}

	
	}
	IBverifyOTPMyNewAccounts();
	return true;
}

function OTPValdatnOpenSaving(text) {
	if(gblTokenSwitchFlag==false || gblTokenSwitchFlag=="0" || gblTokenSwitchFlag==""){
	if (text == null) return false;
	var txtLen = text.length;
	
	var isNum = kony.string.isNumeric(text);
	if (!isNum) {
		alert("OTP should be numberic only");
		return false;
	}
	if (txtLen > gblOTPLENGTH) {
		alert("OTP code should be maximum " + gblOTPLENGTH + " characters");
		return false;
	}	
	}else{
	
	if(frmIBOpenNewSavingsAccConfirmation.tbxToken.text==""||frmIBOpenNewSavingsAccConfirmation.tbxToken.text==null){
		alert("Please Enter token");
		return false;
	}

	}
	frmIBOpenNewSavingsAccComplete.lblNickNameVal.text=frmIBOpenNewSavingsAccConfirmation.lblNickNameVal.text
	frmIBOpenNewSavingsAccComplete.lblAccuntNoVal.text=frmIBOpenNewSavingsAccConfirmation.lblActNoval.text
	frmIBOpenNewSavingsAccComplete.lblActName.text=frmIBOpenNewSavingsAccConfirmation.lblActName.text
	frmIBOpenNewSavingsAccComplete.lblBranchVal.text=frmIBOpenNewSavingsAccConfirmation.lblBranchVal.text
	frmIBOpenNewSavingsAccComplete.lblOpenAmtVal.text=frmIBOpenNewSavingsAccConfirmation.lblOpenAmtVal.text
	frmIBOpenNewSavingsAccComplete.lblInterestRateVal.text=frmIBOpenNewSavingsAccConfirmation.lblInterestRateVal.text
	frmIBOpenNewSavingsAccComplete.lblOpeningDateVal.text=frmIBOpenNewSavingsAccConfirmation.lblOpeningDateVal.text
	frmIBOpenNewSavingsAccComplete.image2866854059250111.src = frmIBOpenNewSavingsAccConfirmation.imgNoFix.src;
	frmIBOpenNewSavingsAccComplete.lbltransactionVal.text=frmIBOpenNewSavingsAccConfirmation.lbltransactionVal.text
	//frmIBOpenNewSavingsAccComplete.lblNFAccName.text=frmIBOpenNewSavingsAccConfirmation.lblNFAccName.text
//	frmIBOpenNewSavingsAccComplete.lblNFAccBal.text=frmIBOpenNewSavingsAccConfirmation.lblNFAccBal.text
	frmIBOpenNewSavingsAccComplete.lblBalanceBefrTransfr.text=frmIBOpenNewSavingsAccConfirmation.lblBalanceBefrTransfr.text
	frmIBOpenNewSavingsAccComplete.lblIBOpenAccCnfmBalAmt.text=frmIBOpenNewSavingsAccConfirmation.lblIBOpenAccCnfmBalAmt.text
//	frmIBOpenNewSavingsAccComplete.show()
	IBverifyOTPMyNewAccounts();
	return true;
}

function loadTermsNConditionOpenAccountIB(){
	var input_param = {};
	var locale = kony.i18n.getCurrentLocale();
	    if (locale == "en_US") {
		input_param["localeCd"]="en_US";
		}else{
		input_param["localeCd"]="th_TH";
		}
//module key again is from tmb_tnc file
	var prodDescTnC = gblFinActivityLogOpenAct["prodDesc"];
	prodDescTnC = prodDescTnC.replace(/\s+/g, '');
	input_param["moduleKey"]= prodDescTnC;
	
	invokeServiceSecureAsync("readUTFFile", input_param, loadTNCOpenActCallBackIB);
}


function loadTNCOpenActCallBackIB(status,result){
if(status == 400){
	dismissLoadingScreenPopup();
	if(result["opstatus"] == 0){
		var data = result["fileContent"];
		var tncTitle = kony.i18n.getLocalizedString("keyTermsNConditions");
	 if(tncTitle=="Terms & Conditions")
	 {
	 	tncTitle = "TermsandConditions";
	 }
		showPopup(tncTitle,data);
	}else{
	showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
	return false;
	}
	}
}

function invokeCustAccInqToUpdateOpenAct(){

	var inputparam = {};
	inputparam["openActFlag"]= "true";
	invokeServiceSecureAsync("customerAccountInquiry", inputparam, callBackOpenAcntCustAccInqToUpdate);

}

function callBackOpenAcntCustAccInqToUpdate(status,resultable){

}

function resetTextBoxesSkinSavingCareIB(){
	frmIBOpenNewSavingsCareAcc.txtAmount.skin = "txtIB20pxGrey";
	frmIBOpenNewSavingsCareAcc.txtNickName.skin="txtIB20pxGrey";
	for(var i=0;i<gblBenificiaryCount ; i++){
		if(frmIBOpenNewSavingsCareAcc["txtFirstName"+i] != undefined){
			frmIBOpenNewSavingsCareAcc["txtFirstName"+i].skin="txtIB20pxGreyBackground";
			frmIBOpenNewSavingsCareAcc["txtLastName"+i].skin="txtIB20pxGreyBackground";
			frmIBOpenNewSavingsCareAcc["txtBenifit"+i].skin="txtIB20pxGreyBackground";
		}
		
	}
	/* frmIBOpenNewSavingsCareAcc.txtAmount.skin = "txtIB20pxGrey";
	frmIBOpenNewSavingsCareAcc.txtNickName.skin="txtIB20pxGrey";
	frmIBOpenNewSavingsCareAcc.txtBefBen1.skin="txtIB20pxGreyBackground";
	frmIBOpenNewSavingsCareAcc.txtBefBen2.skin="txtIB20pxGreyBackground";
	frmIBOpenNewSavingsCareAcc.txtBefBen3.skin="txtIB20pxGreyBackground"
	frmIBOpenNewSavingsCareAcc.txtBefBen4.skin="txtIB20pxGreyBackground"
	frmIBOpenNewSavingsCareAcc.txtBefBen5.skin="txtIB20pxGreyBackground";
	frmIBOpenNewSavingsCareAcc.txtBefFisrtName1.skin="txtIB20pxGreyBackground"
	frmIBOpenNewSavingsCareAcc.txtBefFisrtName2.skin="txtIB20pxGreyBackground"
	frmIBOpenNewSavingsCareAcc.txtBefFisrtName3.skin="txtIB20pxGreyBackground"
	frmIBOpenNewSavingsCareAcc.txtBefFisrtName4.skin="txtIB20pxGreyBackground"
	frmIBOpenNewSavingsCareAcc.txtBefFisrtName5.skin="txtIB20pxGreyBackground";
	frmIBOpenNewSavingsCareAcc.txtBefSecName1.skin="txtIB20pxGreyBackground";
	frmIBOpenNewSavingsCareAcc.txtBefSecName2.skin="txtIB20pxGreyBackground";
	frmIBOpenNewSavingsCareAcc.txtBefSecName3.skin="txtIB20pxGreyBackground"
	frmIBOpenNewSavingsCareAcc.txtBefSecName4.skin="txtIB20pxGreyBackground"
	frmIBOpenNewSavingsCareAcc.txtBefSecName5.skin="txtIB20pxGreyBackground"; */
}

function showAddressForPackageProduct(){
	var inputParams = {};
    showLoadingScreenPopup();
    inputParams["viewFlag"] = "OpenAccount";
    invokeServiceSecureAsync("MyProfileViewCompositeService", inputParams, IBviewAddressServiceCallBack);
}

function IBviewAddressServiceCallBack(status,resulttable){
	if (status == 400) //success response
    {
      	if(resulttable["opstatus"] == 0) {
			for (var i = 0; i < resulttable["Persondata"].length; i++) {
            	if(resulttable["Persondata"]!= null || resulttable["Persondata"]!= undefined){
					var addressType = resulttable["Persondata"][i]["AddrType"];
					if (resulttable["Persondata"][i]["addr3"] != null || resulttable["Persondata"][i]["addr3"] != "" || resulttable["Persondata"][i]["addr3"] != undefined) {
                        var adr3 = resulttable["Persondata"][i]["addr3"];
                        var reg = / {1,}/;
                        var tempArr = [];
                        tempArr = adr3.split(reg);
                        
                        var subDistBan = kony.i18n.getLocalizedString("gblsubDtPrefixThaiB");
						var subDistNotBan = kony.i18n.getLocalizedString("gblsubDtPrefixThai") + ".";
						var distBan = kony.i18n.getLocalizedString("gblDistPrefixThaiB");
						var distNotBan = kony.i18n.getLocalizedString("gblDistPrefixThai") + ".";
						
						if (addressType == "Primary") {
							if (tempArr[0] != null && tempArr[1] != null && tempArr[0] != "" && tempArr[1] != "" && tempArr[0] != undefined && tempArr[1] != undefined) {
                                if (resulttable["Persondata"][i]["City"] != null && resulttable["Persondata"][i]["City"] != "" && resulttable["Persondata"][i]["City"] != undefined) {
                                    if(tempArr[0].indexOf(subDistBan, 0) >= 0)
										gblsubdistrictValue = tempArr[0].substring(4);
									else if(tempArr[0].indexOf(subDistNotBan, 0) >= 0) 
											gblsubdistrictValue = tempArr[0].substring(2);
										 else gblsubdistrictValue = "";	
									if(tempArr[1].indexOf(distBan, 0) >= 0)  	 
										gbldistrictValue = tempArr[1].substring(3);
									else if(tempArr[1].indexOf(distNotBan, 0) >= 0) 
											gbldistrictValue = tempArr[1].substring(2); 
										 else 	gbldistrictValue = "";
                                } 
                            } else {
                                gblsubdistrictValue = "";
                                gbldistrictValue = "";
                            }
                            if(resulttable["Persondata"][i]["City"] != null && resulttable["Persondata"][i]["City"] != "" && resulttable["Persondata"][i]["City"] != undefined)
								gblStateValue = resulttable["Persondata"][i]["City"];
							else	gblStateValue = "";
							if(resulttable["Persondata"][i]["PostalCode"] != null && resulttable["Persondata"][i]["PostalCode"] != "" && resulttable["Persondata"][i]["PostalCode"] != undefined)
								gblzipcodeValue = resulttable["Persondata"][i]["PostalCode"];
							else 	
								gblzipcodeValue = resulttable["Persondata"][i]["PostalCode"];
							if(resulttable["Persondata"][i]["CountryCodeValue"] != null && resulttable["Persondata"][i]["CountryCodeValue"] != "" && resulttable["Persondata"][i]["CountryCodeValue"] != undefined)	
								gblcountryCode = resulttable["Persondata"][i]["CountryCodeValue"];
							else	gblcountryCode = "";
							if (resulttable["Persondata"][i]["CountryCodeValue"] != kony.i18n.getLocalizedString("Thailand")) {
                                gblnotcountry = true;
                            } else {
                                gblnotcountry = false;
                            } 
							frmIBOpenProdMailAddress.lblHouseNo.text = resulttable["Persondata"][i]["addr1"];
							gblAddress1Value = resulttable["Persondata"][i]["addr1"];
							frmIBOpenProdMailAddress.lblStreet.text = resulttable["Persondata"][i]["addr2"];
							gblAddress2Value = resulttable["Persondata"][i]["addr2"];
							frmIBOpenProdMailAddress.lblSubDistrict.text = gblsubdistrictValue;
							frmIBOpenProdMailAddress.lblDistrict.text = gbldistrictValue;
							frmIBOpenProdMailAddress.lblProvince.text = resulttable["Persondata"][i]["City"];
							frmIBOpenProdMailAddress.lblPostCode.text = resulttable["Persondata"][i]["PostalCode"];
						}
					}
					frmIBOpenProdMailAddress.show();	
				}
			
			}
		}
	}
}

function editAddressForOpenAccount(){
	gblEditAddressOpenAccount=true;
	gblAddressFlag=0;
	IBdropDownStatePopulate();
	if(isOpenAccountKYCRelated()){
		frmIBOpenAccountContactKYC.txtAddress1.text=gblAddress1Value;
		frmIBOpenAccountContactKYC.txtAddress2.text=gblAddress2Value;
	}else{
		frmIBOpenProdMailAddressEdit.txtAddress1.text = frmIBOpenProdMailAddress.lblHouseNo.text.trim();
		frmIBOpenProdMailAddressEdit.txtAddress2.text = frmIBOpenProdMailAddress.lblStreet.text.trim();
	}	
}

function IBpopulateAddressFieldsDataOpenAccount(resultDS){
	var curr_form = frmIBOpenProdMailAddressEdit;
    if(isOpenAccountKYCRelated()){
    	curr_form = frmIBOpenAccountContactKYC;
    }
	var tab = [];
    var tempRec = [];
    var currentLocales = kony.i18n.getCurrentLocale();
    var j = 1;
    tab[0] = [0, kony.i18n.getLocalizedString('keyIBPleaseSelect')]
    var j = 0,k = 1;
    for (; j < resultDS.length; k++, j++) {
        if (gblMyProfileAddressFlag == "state") {
            if (currentLocales == "th_TH") {
                tab[k] = [k, resultDS[j].ProvinceNameTH];
            } else {
                tab[k] = [k, resultDS[j].ProvinceNameEN];
            }
        } else if (gblMyProfileAddressFlag == "district") {
            chngdistrict = true;
            if (currentLocales == "th_TH") {
                tab[k] = [k, resultDS[j].DistrictNameTH];
            } else {
                tab[k] = [k, resultDS[j].DistrictNameEN];
            }
        } else if (gblMyProfileAddressFlag == "subdistrict") {
            chngsubdistrict = true;
            if (currentLocales == "th_TH") {
                tab[k] = [k, resultDS[j].SubDistrictNameTH];
            } else {
                tab[k] = [k, resultDS[j].SubDistrictNameEN];
            }
        } else if (gblMyProfileAddressFlag == "zipcode") {
            chngzipcode = true;
            tab[k] = [k, resultDS[j].ZipCode];
        } 
    }
    resulttableRS = resultDS;
    var i = 0;
    if (gblMyProfileAddressFlag == "state") {
    	curr_form.btnProvince.masterData = tab;
        if (gblStateValue == ""|| gblAddressFlag != 0) curr_form.btnProvince.selectedKey = 0;
        else
        {
             
	         for (index=0; index < resulttableRS.length; index++) {
	            if (resulttableRS[index].ProvinceNameTH == gblStateValue) {
	            	curr_form.btnProvince.selectedKey = index + 1;
	                break;
	            }
          }  
        }
        //frmIBOpenProdMailAddressEdit.btnProvince.selectedKey = 0;
    }
    if (gblMyProfileAddressFlag == "district") {
    	curr_form.btnDist.masterData = tab;
    	curr_form.btnDist.selectedKey = 0;
        if (gbldistrictValue == "" || gblAddressFlag != 0) curr_form.btnDist.selectedKey = 0;
        else
        {
             
	         for (index=0; index < resulttableRS.length; index++) {
	            if (resulttableRS[index].DistrictNameTH == gbldistrictValue) {
	            	curr_form.btnDist.selectedKey = index + 1;
	                break;
	            }
          }  
        }
    }
    if (gblMyProfileAddressFlag == "subdistrict") {
    	curr_form.btnSubDist.masterData = tab;
    	curr_form.btnSubDist.selectedKey = 0;
        if (gblsubdistrictValue == "" || gblAddressFlag != 0) curr_form.btnSubDist.selectedKey = 0;
        else
        {
             
	         for (index=0; index < resulttableRS.length; index++) {
	            if (resulttableRS[index].SubDistrictNameTH == gblsubdistrictValue) {
	            	curr_form.btnSubDist.selectedKey = index + 1;
	                break;
	            }
          }  
        }
    }
    if (gblMyProfileAddressFlag == "zipcode") {
    	curr_form.btnPostalCode.masterData = tab;
        if (resulttableRS.length == 1) {
        	curr_form.btnPostalCode.selectedKeyValue = ["0", resulttableRS[0].ZipCode];
            gblmyProfileAddrZipCode = resulttableRS[0].ZipCode;
            zipcodeValue = resulttableRS[0].ZipCode;
        }
        if (gblzipcodeValue == "" || gblAddressFlag != 0) curr_form.btnPostalCode.selectedKey = 0;
        else
        {
	         for (index=0; index < resulttableRS.length; index++) {
	            if (resulttableRS[index].ZipCode == gblzipcodeValue) {
	            	curr_form.btnPostalCode.selectedKey = index + 1;
	                break;
	            }
          	 }  
        }
        if(isOpenAccountKYCRelated()){
        	frmIBOpenAccountContactKYC.hboxEdit.setVisibility(true);
        	frmIBOpenAccountContactKYC.hbxEditEmailAddressCancelConfirm.setVisibility(true);
        }else{
        	frmIBOpenProdMailAddressEdit.show();
        }	
    }
}


function IBonRowSelectOpenAccountAddr() {
	var curr_form = frmIBOpenProdMailAddressEdit;
    if(isOpenAccountKYCRelated()){
    	curr_form = frmIBOpenAccountContactKYC;
    }
    var index = 0;
    var locale = kony.i18n.getCurrentLocale();
    if (gblMyProfileAddressFlag == "state") {
        stateFlag = true;
        resulttableRS = resulttableRSState;
        var key = curr_form.btnProvince.selectedKeyValue[1];
        if(curr_form.btnProvince.selectedKeyValue[0] != 0) {
	        if (locale == "th_TH") {
	            for (; index < resulttableRS.length; index++) {
	                if (resulttableRS[index].ProvinceNameTH == key) {
	                    lblAddrField = resulttableRS[index].ProvinceNameTH;
	                    break;
	                }
	            }
	        } else {
	            for (; index < resulttableRS.length; index++) {
	                if (resulttableRS[index].ProvinceNameEN == key) {
	                    lblAddrField = resulttableRS[index].ProvinceNameEN;
	                    break;
	                }
	            }
	        }
	        idHiddenAddrField = resulttableRS[index].ProvinceCD;
	        lblbankListThaiField = resulttableRS[index].ProvinceNameTH;
	        lblAddrFieldStateTH = resulttableRS[index].ProvinceNameTH;
	        //var lblAddrFieldState = resulttableRS[index].ProvinceNameEN;
	        
	        
	        curr_form.btnDist.setEnabled(true);
	        gblmyProfileAddrState = idHiddenAddrField;
	        StateValue = lblbankListThaiField;
	        gblAddressFlag = 2;
	        gblMyProfileAddressFlag = "district";
	        IBeditMyDistrict();
	        clearEditAddressDropdowns();
		}
		else {
        	var tab = [];
			tab[0] = [0, kony.i18n.getLocalizedString('keyIBPleaseSelect')];
			curr_form.btnDist.masterData = tab;
			curr_form.btnSubDist.masterData = tab;
			curr_form.btnPostalCode.masterData = tab;
		}
    } else if (gblMyProfileAddressFlag == "district") {
        resulttableRS = resulttableRSDistrict;
        var key = curr_form.btnDist.selectedKeyValue[1];
        if(curr_form.btnDist.selectedKeyValue[0] != 0) {
	        if (locale == "th_TH") {
	            for (; index < resulttableRS.length; index++)
	            if (resulttableRS[index].DistrictNameTH == key) {
	                lblAddrField = resulttableRS[index].DistrictNameTH;
	                break
	            }
	        } else {
	            for (; index < resulttableRS.length; index++)
	            if (resulttableRS[index].DistrictNameEN == key) {
	                lblAddrField = resulttableRS[index].DistrictNameEN;
	                break;
	            }
	        }
	        idHiddenAddrField = resulttableRS[index].DistrictCD;
	        lblbankListThaiField = resulttableRS[index].DistrictNameTH;
	        
	        
	        curr_form.btnSubDist.setEnabled(true);
	        lblAddrFieldDistTH = resulttableRS[index].DistrictNameTH;
	        //var lblAddrFieldDist = resulttableRS[index].DistrictNameEN;;
	        gblmyProfileAddrDistrict = idHiddenAddrField;
	        districtValue = lblbankListThaiField;
	        gblAddressFlag = 3;
	        gblMyProfileAddressFlag = "subdistrict";
	        IBeditMySubDistrict();
	        clearEditAddressDropdowns();
	  	}
	  	else {
	  		var tab = [];
			tab[0] = [0, kony.i18n.getLocalizedString('keyIBPleaseSelect')];
			curr_form.btnSubDist.masterData = tab;
			curr_form.btnPostalCode.masterData = tab;
	  	}
    } else if (gblMyProfileAddressFlag == "subdistrict") {
        resulttableRS = resulttableRSSubDistrict;
        var key = curr_form.btnSubDist.selectedKeyValue[1];
        if(curr_form.btnSubDist.selectedKeyValue[0] != 0) {
	        if (locale == "th_TH") {
	            for (; index < resulttableRS.length; index++)
	            if (resulttableRS[index].SubDistrictNameTH == key) {
	                lblAddrField = resulttableRS[index].SubDistrictNameTH;
	                break;
	            }
	        } else {
	            for (; index < resulttableRS.length; index++)
	            if (resulttableRS[index].SubDistrictNameEN == key) {
	                lblAddrField = resulttableRS[index].SubDistrictNameEN;
	                break;
	            }
	        }
	        idHiddenAddrField = resulttableRS[index].SubDistrictCD;
	        lblbankListThaiField = resulttableRS[index].SubDistrictNameTH;
	        
	        
	        curr_form.btnPostalCode.setEnabled(true);
	        gblmyProfileAddrSubDistrict = idHiddenAddrField;
	        subdistrictValue = lblbankListThaiField;
	        gblAddressFlag = 4;
	        lblAddrFieldSubDistTH = resulttableRS[index].SubDistrictNameTH;
	        //var lblAddrFieldSubDist = resulttableRS[index].SubDistrictNameEN;
	        gblMyProfileAddressFlag = "zipcode";
	        IBeditMyZipcode();
	  	}
	  	else {
	  		var tab = [];
			tab[0] = [0, kony.i18n.getLocalizedString('keyIBPleaseSelect')];
			curr_form.btnPostalCode.masterData = tab;
	  	}
    } else if (gblMyProfileAddressFlag == "zipcode") {
        resulttableRS = resulttableRSProvince;
        var key = curr_form.btnPostalCode.selectedKeyValue[1];
        if(curr_form.btnPostalCode.selectedKeyValue[0] != 0) {
	        for (; index < resulttableRS.length; index++)
	        if (resulttableRS[index].ZipCode == key) break;
	        lblAddrField = resulttableRS[index].ZipCode;
	        idHiddenAddrField = resulttableRS[index].ZipCode;
	        lblbankListThaiField = resulttableRS[index].ZipCode;
	        
	        
	        gblmyProfileAddrZipCode = idHiddenAddrField;
	        lblAddrFieldZipCode = gblmyProfileAddrZipCode;
	        zipcodeValue = lblbankListThaiField;
	  	}
    }
}

function clearEditAddressDropdowns(){
	var curr_form = frmIBOpenProdMailAddressEdit;
    if(isOpenAccountKYCRelated()){
    	curr_form = frmIBOpenAccountContactKYC;
    }
	var tab = [];
    tab[0] = [0, kony.i18n.getLocalizedString('keyIBPleaseSelect')]
	if(gblAddressFlag == 2){
		curr_form.btnSubDist.masterData = tab;
		curr_form.btnSubDist.selectedKey = 0;
		curr_form.btnPostalCode.masterData = tab;
		curr_form.btnPostalCode.selectedKey = 0;
		
	    districtValue="";
	    subdistrictValue="";
	    zipcodeValue="";
	}
	else if(gblAddressFlag == 3){
		curr_form.btnPostalCode.masterData = tab;
		curr_form.btnPostalCode.selectedKey = 0;

	    subdistrictValue="";
	    zipcodeValue="";
	} else if(gblAddressFlag == 4){
		zipcodeValue="";	
	}
		
}

function checkEditAddressChanged(){
	
	var curr_form = frmIBOpenProdMailAddressEdit;
	var invalidEmail1 = kony.i18n.getLocalizedString("invalidEmail");
	var info1 = kony.i18n.getLocalizedString("info");
    if(isOpenAccountKYCRelated()){
    	curr_form = frmIBOpenAccountContactKYC;
    	var emailtxt = frmIBOpenAccountContactKYC.txtemailvalue.text;
    	if(!validateEmail(emailtxt)){
    		showAlert(invalidEmail1, info1);
        	return false;
    	}
    }
    var newKey = kony.i18n.getLocalizedString('keyIBPleaseSelect');
    
    if ((curr_form.btnSubDist.selectedKeyValue[1] == newKey) || (curr_form.btnDist.selectedKeyValue[1] == newKey) ||
    (curr_form.btnPostalCode.selectedKeyValue[1] == newKey) || (curr_form.btnProvince.selectedKeyValue[1] == newKey)||
    (curr_form.txtAddress1.text == "") || (curr_form.txtAddress2.text == "")) {
        showAlert(kony.i18n.getLocalizedString("keyenterDetails"), "Error");
        return false;
    }
	
	if(StateValue == undefined || StateValue == "")
		StateValue = gblStateValue;
	if(districtValue == undefined  || districtValue == "")
	    districtValue = gbldistrictValue;
	if(subdistrictValue == undefined || subdistrictValue == "")    
	   	subdistrictValue = gblsubdistrictValue;
	if(zipcodeValue == undefined || zipcodeValue == "")   	
	    zipcodeValue = gblzipcodeValue;
	
	checkMyProfileEditPro();    
}
function saveAddrOpenAccountCallBack(status,result){
	var curr_form = frmIBOpenProdMailAddressEdit;
    if(isOpenAccountKYCRelated()){
    	curr_form = frmIBOpenAccountContactKYC;
    }
	if (status == 400) {
		if (result["opstatus"] == "0") {
			var AddressLine1="";
			var AddressLine2="";
			curr_form.hboxEdit.setVisibility(false);
			curr_form.hbxcnf2.setVisibility(true);
			if (province == kony.i18n.getLocalizedString("BangkokThaiValueProfile")){
				AddressLine1 =	result["Addr1"] + " " + result["Addr2"] + " " + kony.i18n.getLocalizedString("gblsubDtPrefixThaiB") + result["subDistrict"]; 
				AddressLine2 =  kony.i18n.getLocalizedString("gblDistPrefixThaiB") + result["district"] + " " + result["province"] + " " + result["zipcode"]; 
			}	
			else{
				AddressLine1 = result["Addr1"] + " " + result["Addr2"] + " " + kony.i18n.getLocalizedString("gblsubDtPrefixThai") + "." + result["subDistrict"];
				AddressLine2 = kony.i18n.getLocalizedString("gblDistPrefixThai") + "." + result["district"] + " " + result["province"] + " " + result["zipcode"];
			}
			curr_form.clblContactAdd2.text = AddressLine1;
			curr_form.clblContactAdd3.text = AddressLine2;
			curr_form.btnConfirm.text = kony.i18n.getLocalizedString("keyConfirm");
			if(curr_form.id == "frmIBOpenAccountContactKYC"){
				frmIBOpenAccountContactKYC.clblEmailVal.text = result["email"];
			}else{
				curr_form.lblRightHeader.setVisibility(true);
			}
			//onMyProfileAddressNextIB();		call on button click
		}
	}
	dismissLoadingScreenPopup();
}



function hideOTPShowEditAddress(){
	frmIBOpenProdMailAddressEdit.hboxEdit.setVisibility(true);
	frmIBOpenProdMailAddressEdit.hbxcnf2.setVisibility(false);
	frmIBOpenProdMailAddressEdit.lblRightHeader.setVisibility(true);
	frmIBOpenProdMailAddressEdit.hbxOtpBox.setVisibility(false);
	frmIBOpenProdMailAddressEdit.btnConfirm.text = kony.i18n.getLocalizedString("keysave");
	frmIBOpenProdMailAddressEdit.clblContactAdd2.text="";
	frmIBOpenProdMailAddressEdit.clblContactAdd3.text="";
}

function verifyOTPOpenAccountEditAddress(otptext) {
    var otpIBText = otptext;
    showLoadingScreenPopup();
    var compositeEditAddressIB_inputParam = {}
    // sending the global params 
    var addressLine1 = frmIBOpenProdMailAddressEdit.txtAddress1.text.trim();
    var addressLine2 = frmIBOpenProdMailAddressEdit.txtAddress2.text.trim();
    
   	province = StateValue;
    district = districtValue;
    subdistrict = subdistrictValue;
    zipcode = zipcodeValue;
    
    
    
    if (province == kony.i18n.getLocalizedString("BangkokThaiValueProfile")) 
    	compositeEditAddressIB_inputParam["globalvar_gblAddress"] = frmIBOpenProdMailAddress.lblHouseNo.text + " " + frmIBOpenProdMailAddress.lblStreet.text + " " + kony.i18n.getLocalizedString("gblsubDtPrefixThaiB") + frmIBOpenProdMailAddress.lblSubDistrict.text + " " + kony.i18n.getLocalizedString("gblDistPrefixThaiB") + frmIBOpenProdMailAddress.lblDistrict.text + " " + frmIBOpenProdMailAddress.lblProvince.text + " " + frmIBOpenProdMailAddress.lblPostCode.text;
    else 
    	compositeEditAddressIB_inputParam["globalvar_gblAddress"] = frmIBOpenProdMailAddress.lblHouseNo.text + " " + frmIBOpenProdMailAddress.lblStreet.text + " " + kony.i18n.getLocalizedString("gblsubDtPrefixThai") + "." + frmIBOpenProdMailAddress.lblSubDistrict.text + " " + kony.i18n.getLocalizedString("gblDistPrefixThai") + "." + frmIBOpenProdMailAddress.lblDistrict.text + " " + frmIBOpenProdMailAddress.lblProvince.text + " " + frmIBOpenProdMailAddress.lblPostCode.text;
    //compositeEditAddressIB_inputParam["globalvar_gblAddressOld"] = frmIBCMMyProfile.lblContactAdd2.text + " " + frmIBCMMyProfile.lblContactAdd3.text;
	// new address will be taken from service and old address is sent in "globalvar_gblAddress" param
    compositeEditAddressIB_inputParam["globalvar_gblTokenSwitchFlag"] = gblTokenSwitchFlag;
    
    //ending the global params
    compositeEditAddressIB_inputParam["verifyPswdMyProfile_loginModuleId"] = "MB_TxPwd";
    compositeEditAddressIB_inputParam["verifyPswdMyProfile_retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
    compositeEditAddressIB_inputParam["verifyPswdMyProfile_retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
    compositeEditAddressIB_inputParam["verifyPswdMyProfile_userStoreId"] = "DefaultStore";
    compositeEditAddressIB_inputParam["verifyPswdMyProfile_password"] = otpIBText;
    compositeEditAddressIB_inputParam["verifyPswdMyProfile_caseTrans"] = "profile";
    //compositeEditAddressIB_inputParam["verifyPswdMyProfile_profileAddrFlag"] = profileAddrFlagIB;
    //compositeEditAddressIB_inputParam["verifyPswdMyProfile_profileedit"] = "true";
    compositeEditAddressIB_inputParam["channelId"] = "IB";
    //address flag
    //sending addresschnage
    var locale = kony.i18n.getCurrentLocale();
    if (locale == "en_US") {
        compositeEditAddressIB_inputParam["partyUpdateInputMap_Language"] = "EN";
    } else {
        compositeEditAddressIB_inputParam["partyUpdateInputMap_Language"] = "TH";
    }
    var primaryaddrtype = "Primary";
    compositeEditAddressIB_inputParam["partyUpdateInputMap_Addr1"] = addressLine1;
    compositeEditAddressIB_inputParam["partyUpdateInputMap_Addr2"] = addressLine2;
    //compositeEditAddressIB_inputParam["partyUpdateInputMap_Addr3"] = "testing";
    if (province == kony.i18n.getLocalizedString("BangkokThaiValueProfile")) {
        
        compositeEditAddressIB_inputParam["partyUpdateInputMap_Addr3"] = kony.i18n.getLocalizedString("gblsubDtPrefixThaiB") + subdistrict + " " + kony.i18n.getLocalizedString("gblDistPrefixThaiB") + district;
    } else {
        
        compositeEditAddressIB_inputParam["partyUpdateInputMap_Addr3"] = kony.i18n.getLocalizedString("gblsubDtPrefixThai") + "." + subdistrict + " " + kony.i18n.getLocalizedString("gblDistPrefixThai") + "." + district;
    }
    compositeEditAddressIB_inputParam["partyUpdateInputMap_City"] = province; //kony.i18n.getLocalizedString("BangkokThaiValue");
    compositeEditAddressIB_inputParam["partyUpdateInputMap_StateProv"] = gblnewStateValue;
    compositeEditAddressIB_inputParam["partyUpdateInputMap_PostalCode"] = zipcode;
    //compositeEditAddressIB_inputParam["partyUpdateInputMap_AddrType"] = primaryaddrtype;
    compositeEditAddressIB_inputParam["partyUpdateInputMap_CountryCodeValue"] = kony.i18n.getLocalizedString("Thailand");
    //compositeEditAddressIB_inputParam["crmProfileMod_actionType"] = "333";
    //compositeEditAddressIB_inputParam["crmProfileMod_emailAddr"] = frmIBCMEditMyProfile.txtemailvalue.text;
    invokeServiceSecureAsync("addressModifyCompositeJavaService", compositeEditAddressIB_inputParam, addressModifyCompositeJavaServicecallBackIB)
}

function addressModifyCompositeJavaServicecallBackIB(status, resulttable) {
    var subDistBan = kony.i18n.getLocalizedString("gblsubDtPrefixThaiB");
    var subDistNotBan = kony.i18n.getLocalizedString("gblsubDtPrefixThai") + ".";
    var distBan = kony.i18n.getLocalizedString("gblDistPrefixThaiB");
    var distNotBan = kony.i18n.getLocalizedString("gblDistPrefixThai") + ".";


    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (resulttable["statusCode"] == 0) {
                // start callback
                profileEmailFlag = "";
                chngdistrict = false;
                chngsubdistrict = false;
                chngzipcode = false;
                //gblcrmId = resulttable["crmId"];     removed from results.
                completeicon = true;
                if (resulttable["errMsg_addr"] != null && resulttable["errMsg_addr"] != undefined) {

                    if (resulttable["errMsg_addr"].trim() == "Maximum Length Exceeded")
                        showAlert("" + kony.i18n.getLocalizedString("keyAdressMaxLengthExceeded"), null);
                    else
                        showAlert("" + kony.i18n.getLocalizedString("keyPartyUpdateFailure"), null);
                    completeicon = false;
                    dismissLoadingScreenPopup();
                    return false;
                }

                for (var i = 0; i < resulttable["Persondata"].length; i++) {
                    var tempAddrtype = resulttable["Persondata"][i]["AddrType"];
                    if (tempAddrtype == "Primary") {
                        //alert("primary address...");
                        if (resulttable["Persondata"][i]["AddrType"] != null && resulttable["Persondata"][i]["AddrType"] != "" && resulttable["Persondata"][i]["AddrType"] != undefined) {
                            //if(resulttable["Persondata"][i]["addr3"]){}
                            if (resulttable["Persondata"][i]["addr3"] != null || resulttable["Persondata"][i]["addr3"] != "" || resulttable["Persondata"][i]["addr3"] != undefined) {
                                var adr3 = resulttable["Persondata"][i]["addr3"];
                                //alert(adr3);
                                var reg = / {1,}/;
                                var tempArr = [];
                                tempArr = adr3.split(reg);
                                //if (resulttable["Persondata"][i]["City"] != null && resulttable["Persondata"][i]["City"] != "" && resulttable["Persondata"][i]["City"] != undefined) 
                                {
                                    if (tempArr[0] != null && tempArr[1] != null && tempArr[0] != "" && tempArr[1] != "" && tempArr[0] != undefined && tempArr[1] != undefined) {
                                        if (resulttable["Persondata"][i]["City"] != "" && resulttable["Persondata"][i]["City"] != null && resulttable["Persondata"][i]["City"] != undefined) {
                                            if (tempArr[0].indexOf(subDistBan, 0) >= 0)
                                                gblsubdistrictValue = tempArr[0].substring(4);
                                            else if (tempArr[0].indexOf(subDistNotBan, 0) >= 0)
                                                gblsubdistrictValue = tempArr[0].substring(2);
                                            else gblsubdistrictValue = "";

                                            if (tempArr[1].indexOf(distBan, 0) >= 0)
                                                gbldistrictValue = tempArr[1].substring(3);
                                            else if (tempArr[1].indexOf(distNotBan, 0) >= 0)
                                                gbldistrictValue = tempArr[1].substring(2);
                                            else gbldistrictValue = "";
                                        }
                                    } else {
                                        if (tempArr[0] == undefined || tempArr[0] == "" || tempArr[0] == null || tempArr[0] == "undefined") gblsubdistrictValue = kony.i18n.getLocalizedString("keyIBPleaseSelect");
                                        else gblsubdistrictValue = tempArr[0];
                                        if (tempArr[1] == undefined || tempArr[1] == "" || tempArr[1] == null || tempArr[1] == "undefined") gbldistrictValue = kony.i18n.getLocalizedString("keyIBPleaseSelect");
                                        else gbldistrictValue = tempArr[1];
                                    }
                                    if (tempArr[0] == undefined || tempArr[0] == "" || tempArr[0] == null || tempArr[0] == "undefined") gblViewsubdistrictValue = "";
                                    else gblViewsubdistrictValue = tempArr[0];
                                    if (tempArr[1] == undefined || tempArr[1] == "" || tempArr[1] == null || tempArr[1] == "undefined") gblViewdistrictValue = "";
                                    else gblViewdistrictValue = tempArr[1];
                                    gblAddress1Value = resulttable["Persondata"][i]["addr1"];
                                    gblAddress2Value = resulttable["Persondata"][i]["addr2"];
                                    if (resulttable["Persondata"][i]["City"] != "" && resulttable["Persondata"][i]["City"] != null && resulttable["Persondata"][i]["City"] != undefined)
                                        gblStateValue = resulttable["Persondata"][i]["City"];
                                    else gblStateValue = "";
                                    if (resulttable["Persondata"][i]["PostalCode"] != "" && resulttable["Persondata"][i]["PostalCode"] != null && resulttable["Persondata"][i]["PostalCode"] != undefined)
                                        gblzipcodeValue = resulttable["Persondata"][i]["PostalCode"];
                                    else gblzipcodeValue = "";
                                    gblnewStateValue = resulttable["Persondata"][i]["StateProv"];
                                    if (resulttable["Persondata"][i]["CountryCodeValue"] != "" && resulttable["Persondata"][i]["CountryCodeValue"] != null && resulttable["Persondata"][i]["CountryCodeValue"] != undefined)
                                        gblcountryCodeIB = resulttable["Persondata"][i]["CountryCodeValue"];
                                    else gblcountryCodeIB = "";
                                    if (gblzipcodeValue == undefined || gblzipcodeValue == "") gblzipcodeValue = "";
                                    gblAddressOld = frmIBCMMyProfile.lblContactAdd2.text + " " + frmIBCMMyProfile.lblContactAdd3.text;

                                }
                            }
                        }
                    }
                }
                frmIBOpenNewSavingsAcc.txtOpenActSelAmt.skin = "txtIB20pxBlack";
                frmIBOpenNewSavingsAcc.txtOpenActNicNam.skin = "txtIB20pxBlack";
                setNormalSavingsDataIB();
                //end callback
            }
        } else {
            dismissLoadingScreenPopup();
            if (resulttable["errCode"] == "VrfyOTPErr00001") {
                gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
                dismissLoadingScreenPopup();
                //showAlert("" + kony.i18n.getLocalizedString("invalidOTP"), null);//commented by swapna.
                var curFrm = kony.application.getCurrentForm().id;
                if(curFrm == "frmIBCMEditMyProfile"){
                    frmIBCMEditMyProfile.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");//kony.i18n.getLocalizedString("invalidOTP"); //
                    frmIBCMEditMyProfile.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo"); 
                    frmIBCMEditMyProfile.hbxOTPincurrect.isVisible = true;
                    frmIBCMEditMyProfile.hbox476047582127699.isVisible = false;
                    frmIBCMEditMyProfile.hbxOTPsnt.isVisible = false;
                    frmIBCMEditMyProfile.txtBxOTP.text = "";
                    frmIBCMEditMyProfile.tbxToken.text = "";
                    if(gblTokenSwitchFlag == true && gblSwitchToken == false){
				       frmIBCMEditMyProfile.tbxToken.setFocus(true);
				    }else{
				       frmIBCMEditMyProfile.txtBxOTP.setFocus(true);
				         }
			                    	
                  }else if(curFrm == "frmIBOpenProdMailAddressEdit"){
                    frmIBOpenProdMailAddressEdit.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");//kony.i18n.getLocalizedString("invalidOTP"); //
                    frmIBOpenProdMailAddressEdit.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo"); 
                    frmIBOpenProdMailAddressEdit.hbxOTPincurrect.isVisible = true;
                    frmIBOpenProdMailAddressEdit.hbox476047582127699.isVisible = false;
                    frmIBOpenProdMailAddressEdit.hbxOTPsnt.isVisible = false;  
                    frmIBOpenProdMailAddressEdit.txtBxOTP.text = "";                
                    frmIBOpenProdMailAddressEdit.tbxToken.text = "";   
                    if(gblTokenSwitchFlag == true && gblSwitchToken == false){
				       frmIBOpenProdMailAddressEdit.tbxToken.setFocus(true);
				    }else{
				       frmIBOpenProdMailAddressEdit.txtBxOTP.setFocus(true);
				         }
                  }
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00004") {
                dismissLoadingScreenPopup();
                showAlert("" + kony.i18n.getLocalizedString("invalidOTP"), null);
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00005") {
                dismissLoadingScreenPopup();
                showAlert("" + kony.i18n.getLocalizedString("invalidOTP"), null);
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00002") {
                dismissLoadingScreenPopup();
                //alert("" + kony.i18n.getLocalizedString("ECVrfyOTPErr"));
                gblVerifyOTPCounter = "0";
                frmIBOpenProdMailAddress.show();
                handleOTPLockedIB(resulttable);
                return false;
            } else if (resulttable["errMsg"] != undefined) {
                dismissLoadingScreenPopup();
                alert(resulttable["errMsg"]);
                return false;
            } else {
                if(curFrm == "frmIBCMEditMyProfile"){
                    frmIBCMEditMyProfile.lblOTPinCurr.text = " ";//kony.i18n.getLocalizedString("invalidOTP"); //
                    frmIBCMEditMyProfile.lblPlsReEnter.text = " "; 
                    frmIBCMEditMyProfile.hbxOTPincurrect.isVisible = false;
                    frmIBCMEditMyProfile.hbox476047582127699.isVisible = true;
                    frmIBCMEditMyProfile.hbxOTPsnt.isVisible = true;
                    dismissLoadingScreenPopup();
                    
                    frmIBCMEditMyProfile.txtBxOTP.text="";
                    frmIBCMEditMyProfile.tbxToken.text="";
                    if(gblTokenSwitchFlag == true && gblSwitchToken == false){
				       frmIBCMEditMyProfile.tbxToken.setFocus(true);
				    }else{
				       frmIBCMEditMyProfile.txtBxOTP.setFocus(true);
				         }
                  }else if(curFrm == "frmIBOpenProdMailAddressEdit"){
                    frmIBOpenProdMailAddressEdit.lblOTPinCurr.text = " ";//kony.i18n.getLocalizedString("invalidOTP"); //
                    frmIBOpenProdMailAddressEdit.lblPlsReEnter.text = " "; 
                    frmIBOpenProdMailAddressEdit.hbxOTPincurrect.isVisible = false;
                    frmIBOpenProdMailAddressEdit.hbox476047582127699.isVisible = true;
                    frmIBOpenProdMailAddressEdit.hbxOTPsnt.isVisible = true; 
                    dismissLoadingScreenPopup();
                    frmIBOpenProdMailAddressEdit.txtBxOTP.text="";
                    frmIBOpenProdMailAddressEdit.tbxToken.text="";  
                    if(gblTokenSwitchFlag == true && gblSwitchToken == false){
				       frmIBOpenProdMailAddressEdit.tbxToken.setFocus(true);
				    }else{
				       frmIBOpenProdMailAddressEdit.txtBxOTP.setFocus(true);
				         }              
                  }
             //   dismissLoadingScreenPopup();
                alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
            }
        }
    }
}

function snippetCodeOnClickOfAcctSegment(){
	gblTransferRefNo = "";
	if(isCmpFlow){
		gblSelOpenActProdCode = gblProdCode;
		//var prodD = gblProudctDetails.ProdDetails[gblProdCode];
		if(undefined !== gblProudctDetails[gblProdCode]){
			var prodD = gblProudctDetails[gblProdCode];
			//alert("Product Details : "+prodD);
			gblFinActivityLogOpenAct["prodNameEN"] = prodD.prodNameEN;
			gblFinActivityLogOpenAct["prodNameTH"] = prodD.prodNameTH;	
			gblFinActivityLogOpenAct["prodDesc"] = prodD.prodDes;
			gblFinActivityLogOpenAct["accTypeValTH"] = prodD.actTypeTH;
			gblFinActivityLogOpenAct["accTypeValEN"] = prodD.actTypeEN;
		}else{
			alert(kony.i18n.getLocalizedString("keyCampaignAlreadyHaveAcct"));
			return;
		}
	} else {
		gblSelOpenActProdCode = frmIBOpenActSelProd["segOpenActSelProd"]["selectedItems"][0].productcode;
		gblFinActivityLogOpenAct["prodNameEN"] = frmIBOpenActSelProd["segOpenActSelProd"]["selectedItems"][0].hiddenProdNameEN;
		gblFinActivityLogOpenAct["prodNameTH"] = frmIBOpenActSelProd["segOpenActSelProd"]["selectedItems"][0].hiddenProdNameTH;
		gblFinActivityLogOpenAct["prodDesc"] = frmIBOpenActSelProd["segOpenActSelProd"]["selectedItems"][0].hiddenProdDes;
		gblFinActivityLogOpenAct["accTypeValTH"] = frmIBOpenActSelProd["segOpenActSelProd"]["selectedItems"][0].hiddenActTypeTH;
		gblFinActivityLogOpenAct["accTypeValEN"] = frmIBOpenActSelProd["segOpenActSelProd"]["selectedItems"][0].hiddenActTypeEN;
	}
	
	gblFinActivityLogOpenAct["PrdctOpoenTDEN"] = "";
	gblFinActivityLogOpenAct["PrdctOpoenTDTOEN"] = "";
	gblFinActivityLogOpenAct["PrdctOpoenCareEN"] = "";
	gblFinActivityLogOpenAct["PrdctOpoenCareTH"] = "";
	if (gblOpenActList["FOR_USE_PRODUCT_CODES"] != null && gblOpenActList["FOR_USE_PRODUCT_CODES"] != undefined 
		&& gblOpenActList["FOR_USE_PRODUCT_CODES"].indexOf(gblSelOpenActProdCode) > 0) {
		
		gblSelProduct = "ForUse";
	} else if (gblSelOpenActProdCode == "206") {
		gblSelProduct = "TMBDreamSavings";
	} else if (gblOpenActList["SAVING_CARE_PRODUCT_CODES"] != null && gblOpenActList["SAVING_CARE_PRODUCT_CODES"] != undefined 
		&& gblOpenActList["SAVING_CARE_PRODUCT_CODES"].indexOf(gblSelOpenActProdCode) >= 0)  {
		arrbenf1 = [];
		arrbenf2 = [];
		arrbenf3 = [];
		arrbenf4 = [];
		arrbenf5 = [];
		gblSelProduct = "TMBSavingcare";
	} else if (gblSelOpenActProdCode == "300" || gblSelOpenActProdCode == "301" || gblSelOpenActProdCode == "302" 
		|| gblSelOpenActProdCode == "601" || gblSelOpenActProdCode == "602" || gblSelOpenActProdCode == "659" 
		|| gblSelOpenActProdCode == "664" || gblSelOpenActProdCode == "666") {
		
		gblSelProduct = "ForTerm";
	} else if (gblSelOpenActProdCode == "221") {
		gblSelProduct = "ForUse";
	}
	gblRetryCountRequestOTP = "0";
}

function snippetCodeOnClickOfMailAddress(){
	frmIBOpenNewSavingsAcc.txtOpenActSelAmt.skin = "txtIB20pxBlack";
	frmIBOpenNewSavingsAcc.txtOpenActNicNam.skin = "txtIB20pxBlack";
	frmIBOpenNewSavingsAcc.txtOpenActNicNam.text = "";
	frmIBOpenNewSavingsAcc.txtOpenActSelAmt.text = "";
	setNormalSavingsDataIB();
}

