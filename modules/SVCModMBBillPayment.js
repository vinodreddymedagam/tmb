







/*
 *
 * Method for getting values from the service by name CustomerAccountService
 *
 *
 *
 */
fullAmt="0.00";
function callBillPaymentCustomerAccountService() {
    showLoadingScreen();
    var inputParam = {}
    inputParam["billPayInd"] = "billPayInd";
    invokeServiceSecureAsync("customerAccountInquiry", inputParam, billPaymentCustomerAccountCallBack);
}

function billPaymentCustomerAccountCallBack(status, resulttable) {
    
    var selectedIndex = 0;
    if (status == 400) //success responce
    {
        if (resulttable["opstatus"] == 0) {
            /** checking for Soap status below  */
            var StatusCode = resulttable["statusCode"];
            
            //	if( resulttable["StatusCode"] !=null &&   resulttable["StatusCode"] == "0"){
            var fromData = []
            var j = 1
            var nonCASAAct=0;
            gbltranFromSelIndex = [0, 0];
           
          var initDPParam = resulttable["initDPParam"];
        	if(initDPParam!=undefined && initDPParam!=null && initDPParam!=""){
              	gblDPPk = resulttable["initDPParam"][0]["pk"];
				gblDPRandNumber = resulttable["initDPParam"][0]["randomNumber"];
        		kony.print("Values for initDP Param :"+gblDPPk+" and"+gblDPRandNumber);
            }
          
            for (var i = 0; i < resulttable.custAcctRec.length; i++) {
            var accountStatus = resulttable["custAcctRec"][i].acctStatus;
				if(accountStatus.indexOf("Active") == -1){
				nonCASAAct = nonCASAAct + 1;
				//showAlertWithCallBack(kony.i18n.getLocalizedString("MB_StatusNotEligible"), kony.i18n.getLocalizedString("info"),onclickActivationCompleteStart);
					//return false;
				}
				if(accountStatus.indexOf("Active") >=0){
                var icon = "";
                var iconcategory="";
                if (resulttable.custAcctRec[i].personalisedAcctStatusCode == "02") {
                    continue; //do not populate if account is deleted
                }
                //icon="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+"NEW_"+resulttable.custAcctRec[i]["ICON_ID"]+"&modIdentifier=PRODICON";	
				icon=loadFromPalleteIcons(resulttable["custAcctRec"][i]["ICON_ID"]);
                kony.print("^^^^^BillPayment^^^^^"+icon);
                  
                iconcategory=resulttable.custAcctRec[i]["ICON_ID"];
                if (iconcategory=="ICON-01"||iconcategory=="ICON-02") {
                    var temp = createSegmentRecordBills(resulttable.custAcctRec[i], hbxSliderNew1, icon)
                } else if (iconcategory=="ICON-03") {
                    var temp = createSegmentRecordBills(resulttable.custAcctRec[i], hbxSliderNew2, icon)
                } else if (iconcategory=="ICON-04") {
                    var temp = createSegmentRecordBills(resulttable.custAcctRec[i], hbxSliderNew3, icon)
                  
                } 
                // added for removing the joint account and others for SPA and MB
                var jointActXfer = resulttable.custAcctRec[i].partyAcctRelDesc;
                if (jointActXfer == "PRIJNT" || jointActXfer == "SECJNT" || jointActXfer == "OTHJNT" || jointActXfer == "SECJAN") {
                    
                } else {
                    
                    kony.table.insert(fromData, temp[0])
                }
                j++;
                }

            } //for
			  gblNoOfFromAcs = fromData.length;
            var temp_glb_accId = glb_accId;
          
              if(temp_glb_accId.length == 14){
				temp_glb_accId = temp_glb_accId.substring(4, temp_glb_accId.length);
				}
            for (var i = 0; i < gblNoOfFromAcs; i++) {
                if (temp_glb_accId == fromData[i].accountNo) {
                    selectedIndex = i;
                    glb_accId = 0;
                    break;
                }
            }
            if (selectedIndex != 0)
                gbltranFromSelIndex = [0, selectedIndex];
            else
                gbltranFromSelIndex = [0, 0];
            if (gblMyBillerTopUpBB == 0) {
                
					frmBillPayment.segSlider.widgetDataMap = [];
                    frmBillPayment.segSlider.widgetDataMap = {
                        lblAcntType: "lblAcntType",
                        img1: "img1",
                        lblCustName: "lblCustName",
                        lblBalance: "lblBalance",
                        lblActNoval: "lblActNoval",
                        lblDummy: "lblDummy",
                        lblSliderAccN2: "lblSliderAccN2",
                        lblRemainFee: "lblRemainFee",
                        lblRemainFeeValue: "lblRemainFeeValue"
                    }
                    frmBillPayment.segSlider.data = [];
                    //frmBillPayment.segSlider.data=fromData;
					if(nonCASAAct==resulttable.custAcctRec.length)
					{ 	 showAlertWithCallBack(kony.i18n.getLocalizedString("MB_StatusNotEligible"), kony.i18n.getLocalizedString("info"),onClickOfAccountDetailsBack);
						    return false;
						
					}
                    if (fromData.length == 0) {
                        frmBillPayment.segSlider.setVisibility(false);
                        showAlertWithCallBack(kony.i18n.getLocalizedString("MB_CommonError_NoSA"), kony.i18n.getLocalizedString("info"),onClickOfAccountDetailsBack);
						return false;
                    } else {
                    	if(gblDeviceInfo["name"] == "android"){
	        				if(fromData.length == 1){
								frmBillPayment.segSlider.viewConfig = {
					            "coverflowConfig": {
					                "rowItemRotationAngle": 0,
					                "isCircular": false,
					                "spaceBetweenRowItems": 10,
					                "projectionAngle": 90,
					                "rowItemWidth": 80
					            }
					        	};
							}else{
									frmBillPayment.segSlider.viewConfig = {
						            "coverflowConfig": {
					                "rowItemRotationAngle": 0,
					                "isCircular": true,
					                "spaceBetweenRowItems": 10,
					                "projectionAngle": 90,
					                "rowItemWidth": 80
					            }
					        	};
							}
						}
                        frmBillPayment.segSlider.data = fromData;
                        frmBillPayment.segSlider.selectedIndex = gbltranFromSelIndex;
                    }
                billpaymentNoOfAccounts = fromData.length;
                dismissLoadingScreen(); 
                frmBillPayment.show();
              
            } else if (gblMyBillerTopUpBB == 1) {
                
                        frmTopUp.segSlider.widgetDataMap = [];
                        frmTopUp.segSlider.widgetDataMap = {
                            lblAcntType: "lblAcntType",
                            img1: "img1",
                            lblCustName: "lblCustName",
                            lblBalance: "lblBalance",
                            lblActNoval: "lblActNoval",
                            lblDummy: "lblDummy",
                            lblSliderAccN2: "lblSliderAccN2",
                            lblRemainFee: "lblRemainFee",
                            lblRemainFeeValue: "lblRemainFeeValue"
                        }
                        frmTopUp.segSlider.data = [];
                        if(nonCASAAct==resulttable.custAcctRec.length)
						{
							showAlertWithCallBack(kony.i18n.getLocalizedString("MB_StatusNotEligible"), kony.i18n.getLocalizedString("info"),onClickOfAccountDetailsBack);
							return false;
						}
                        if (fromData.length == 0) {
                            frmTopUp.segSlider.setVisibility(false);
                            //alert(kony.i18n.getLocalizedString("MB_CommonError_NoSA"))
							showAlertWithCallBack(kony.i18n.getLocalizedString("MB_CommonError_NoSA"), kony.i18n.getLocalizedString("info"),onClickOfAccountDetailsBack);
							return false;

                        } else {
                        	if(gblDeviceInfo["name"] == "android"){
	            				if(fromData.length == 1){
									frmTopUp.segSlider.viewConfig = {
						            "coverflowConfig": {
						                "rowItemRotationAngle": 0,
						                "isCircular": false,
						                "spaceBetweenRowItems": 10,
						                "projectionAngle": 90,
						                "rowItemWidth": 80
						            }
						        	};
								}else{
										frmTopUp.segSlider.viewConfig = {
						            "coverflowConfig": {
						                "rowItemRotationAngle": 0,
						                "isCircular": true,
						                "spaceBetweenRowItems": 10,
						                "projectionAngle": 90,
						                "rowItemWidth": 80
						            }
							        };
								}
							}
                            frmTopUp.segSlider.data = fromData;
                            
                            frmTopUp.segSlider.selectedIndex = gbltranFromSelIndex;
                        }
                    
                    TopUpNoOfAccounts = fromData.length;
                    frmTopUp.show();
                    dismissLoadingScreen();
                }
                
            }
        else if (resulttable["opstatus"] == 1) {
			showAlertWithCallBack(kony.i18n.getLocalizedString("MB_CommonError_NoSA"), kony.i18n.getLocalizedString("info"),onClickOfAccountDetailsBack);
			return false;
		}
             else {
               dismissLoadingScreen();
               alert(" " + resulttable["errMsg"]);
              
            }

        } //status 400
}
    /*
     * Method called when the user came to the confirm screen
     *
     */
    function callBillPaymentServiceOnConfirmClick() {
        var billerMethod = frmSelectBiller.segMyBills.selectedItems[0].billerMethod;
        //	var billerGroupType = frmSelectBiller.segMyBills.selectedItems[0].billerGroupType;
        //alert("hi in callBillPaymentServiceOnConfirmClick");
        if (gblPaynow == true) {
            if (billerMethod == 0) {
                callBillPaymentAddServiceMB();

            }
            if (billerMethod == 1) {
                callOnlinePaymentAddServiceMB();
            }
            if (billerMethod == 2 || billerMethod == 3 || billerMethod == 4) {
                callBillPaymentTransferServiceMB();
            }
        } else {
            callPaymentAddBillServiceMB(billerMethod);
        }
        // TODO::user clicks on the check box


        //	callBillPaymentNotificationAddService(); placed in call back of CRMupdatete

        //gotoBillPaymentComplete();
    }



    /*
     * Method to get the Amount from the OnlinePaymentInquiry Service
     *
     *
     */

function BillPaymentOnlinePaymentInqServiceCallBack(status, result) {
        penalty = "0.00";
        fullAmt = "0.00";
        minAmt = "0.00";
        TranId = "";
        gblRef1 = "";
        gblRef2 = "";
        gblRef3 = "";
        gblRef4 = "";
        gblMinAmount = "";
        gblMaxAmount = "";
		
		//ENH_113_1 - MEA show warn message for I004 & I005 
		var warnStatusCode = "";
    	var warnMsg = "";
    	
        if (status == 400) //success responce
        {

            if (result["opstatus"] == 0) {
				 
             	if(result["onlinePaymentBusinessHrsFlag"] == "false") {
             		var key = kony.i18n.getLocalizedString("keyOnlinePmtBusinessHours");
           			var res = key.replace("<time>",result["onlinePaymentStartTime"] + "-" + result["onlinePaymentEndTime"]);
           			
                  	if(gblPEA_BILLER_COMP_CODE.indexOf(gblCompCode) >= 0) {
                        res = kony.i18n.getLocalizedString("ECGenericErrorNew");
                        res = res.replace("{start_time}", result["onlinePaymentEndTime"]);
                        res = res.replace("{end_time}", result["onlinePaymentStartTime"]);
                    }
                  
                  	launchBillPaymentFirstTime();
                    dismissLoadingScreen();
             		alert(res);
					return;
             	}

				 if (result["StatusCode"] == 0) {

                    for (var i = 0; i < result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"].length; i++) {

                        if (result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscName"] == "PenaltyAmt")
                            penalty = result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscText"];
                        if (result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscName"] == "FullAmt")
                            fullAmt = result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscText"];
                        if (result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscName"] == "BilledAmt")
                            minAmt = result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscText"];
                            
						if (result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscName"] == "StatusCode") {
	                        warnStatusCode = result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscText"];
						}
                        //CR71 Implementation	
                        if (kony.string.equalsIgnoreCase(result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscName"], "Minimum")) {
                            gblMinAmount = result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscText"];
                        }

                        if (kony.string.equalsIgnoreCase(result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscName"], "Maximum")) {
                            gblMaxAmount = result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscText"];
                        }
                    }
                    BankRefId = result["OnlinePmtInqRs"][0]["BankRefId"];
                    TranId = result["OnlinePmtInqRs"][0]["TrnId"];
                    gblRef1 = result["OnlinePmtInqRs"][0]["Ref1"];
                    gblRef2 = result["OnlinePmtInqRs"][0]["Ref2"];
                    gblRef3 = result["OnlinePmtInqRs"][0]["Ref3"];
                    gblRef4 = result["OnlinePmtInqRs"][0]["Ref4"];
					
					warnMsg = result["OnlinePmtInqRs"][0]["warnMsg"];
					
					if(GLOBAL_ONLINE_PAYMENT_VERSION_MWA.indexOf(gblCompCode) != -1){
						fullAmt = result["OnlinePmtInqRs"][0]["Amt"];
					}
					
                    penalty = parseFloat(penalty).toFixed(2);
                    if (penalty != "0.00" && !penalty.isNaN()) {
                        gblPenalty = true
                    }
                    frmBillPayment.lblForFullPayment.setVisibility(true);
                    frmBillPayment.tbxAmount.setVisibility(false);
                    gblFullPayment = true;
                    frmBillPayment.tbxAmount.text = "";
                    gblFullPayment = true;
                    if (gblPenalty) {
                        frmBillPayment.hbxMainAmountForPenalty.setVisibility(true);
                        frmBillPayment.lblAmtValue.text = commaFormatted(fullAmt);
                        frmBillPayment.lblPenaltyValue.text = penalty;
                        frmBillPayment.lblForFullPayment.text = commaFormatted((parseInt(fullAmt) + parseInt(penalty)));
                    } else {
                        frmBillPayment.hbxMainAmountForPenalty.setVisibility(false);
                        frmBillPayment.lblForFullPayment.text = commaFormatted(fullAmt);
                    }
                    //frmIBBillPaymentLP.txtBillAmt.text = Amt;

                    if (gblPayFull == "Y") {
                        gblFullPayment = true;

                        frmBillPayment.lblForFullPayment.setVisibility(true);
                        frmBillPayment.tbxAmount.setVisibility(false);

                        frmBillPayment.hbxFullMinSpecButtons.setVisibility(false);
                        frmBillPayment.hbxFullSpecButton.setVisibility(false);
                    }
                    
                    //Thai Life Warning Message PBI
                    if (gblCompCode == "0947" || gblCompCode == "0016") {
                    	if(removeCommos(gblScanAmount).toFixed(2) > 0 && warnStatusCode == "0001") { //Added gblScanAmount condition and it will work only for Barcode Scan
                    		alert(kony.i18n.getLocalizedString("keyThaiLifeWarning"));
                    	}
                    }
                     
                    //ENH_113
                    if (gblEA_BILLER_COMP_CODES.indexOf(gblCompCode) >= 0) { //MEA biller
                        gblSegBillerDataMB["MeterNo"] = result["OnlinePmtInqRs"][0]["Ref1"];
                        gblSegBillerDataMB["CustName"] = result["OnlinePmtInqRs"][0]["BankRefId"];
                        gblSegBillerDataMB["CustAddress"] = result["OnlinePmtInqRs"][0]["CustReference"];
                        if (TranId != "" && gblRef4 == "Y") {
                        
							if(warnStatusCode == "I004" || warnStatusCode == "I005") {
                    			alert(warnMsg);
                    		}
                        	
                            var locale = kony.i18n.getCurrentLocale();
                            if (kony.string.startsWith(locale, "en", true) == true) {
                            	frmBillPayment.lblMEACustName.text = gblSegBillerDataMB["billerCustNameEn"];
                            	frmBillPayment.lblMeterNo.text = gblSegBillerDataMB["billerMeterNoEn"];
                                frmBillPayment.lblAmount.text = gblSegBillerDataMB["billerTotalPayAmtEn"];
                                frmBillPayment.lblAmtLabel.text = gblSegBillerDataMB["billerAmountEn"];
                                frmBillPayment.lblAmtInterest.text = gblSegBillerDataMB["billerTotalIntEn"];
                                frmBillPayment.lblAmtDisconnected.text = gblSegBillerDataMB["billerDisconnectAmtEn"];
                            } else {
                            	frmBillPayment.lblMEACustName.text = gblSegBillerDataMB["billerCustNameTh"];
                            	frmBillPayment.lblMeterNo.text = gblSegBillerDataMB["billerMeterNoTh"];
                                frmBillPayment.lblAmount.text = gblSegBillerDataMB["billerTotalPayAmtTh"];
                                frmBillPayment.lblAmtLabel.text = gblSegBillerDataMB["billerAmountTh"];
                                frmBillPayment.lblAmtInterest.text = gblSegBillerDataMB["billerTotalIntTh"];
                                frmBillPayment.lblAmtDisconnected.text = gblSegBillerDataMB["billerDisconnectAmtTh"];
                            }
                            //set values in all label recieved from service for split amount
                            frmBillPayment.hbxAmountDetailsMEA.setVisibility(true);
                            
                            frmBillPayment.lineMeterNo.setVisibility(true);
                            frmBillPayment.hbxMeterNo.setVisibility(true);
                            frmBillPayment.hbxMeaCustomerName.setVisibility(true);
                            frmBillPayment.lineRef1.setVisibility(true);
                            frmBillPayment.lblMeterNoVal.text = gblSegBillerDataMB["MeterNo"];
                            frmBillPayment.lblMEACustNameValue.text = gblSegBillerDataMB["CustName"];
                            frmBillPayment.lblForFullPayment.text = commaFormatted(parseFloat(removeCommos(fullAmt)).toFixed(2));

                            if (gblRef2 == undefined || gblRef2 == null || gblRef2 == "") {
                                gblRef2 = "0.00";
                            }
                            if (gblRef3 == undefined || gblRef3 == null || gblRef3 == "") {
                                gblRef3 = "0.00";
                            }
                            var newAmtValue = fullAmt - gblRef2 - gblRef3;
                            frmBillPayment.lblAmountValue.text = commaFormatted(parseFloat(removeCommos(newAmtValue.toString())).toFixed(2));
                            frmBillPayment.lblAmtInterestValue.text = commaFormatted(parseFloat(removeCommos(gblRef2)).toFixed(2));
                            frmBillPayment.lblAmtDisconnectedValue.text = commaFormatted(parseFloat(removeCommos(gblRef3)).toFixed(2));
                            gblOnlinePmtType = result["OnlinePmtInqRs"][0]["OnlinePmtType"];

                        } else {
                            //dismissLoadingScreenPopup();
                           // alert("keyMEARef2orRef4Invalid");
                        }

                    } else if(GLOBAL_ONLINE_PAYMENT_VERSION_MWA.indexOf(gblCompCode) != -1){
                      
                       //Warning message for PEA
                       //MIB-15629
  					   //showPEAMessageForOverDue();
                     
                     	// <Total invoice>|<sequenceNo1>|<billNumber1>|<vatAmount1>|<TotalAmount1>|<sequenceNo2>|<billNumber2>|<vatAmount2>|<TotalAmount2>|<sequenceNo3>|<billNumber3>|<vatAmount3>|<TotalAmount3>
						// StatusDesc = "4|1|BILL001|5.00|100.00|2|BILL0020|100.00|2000.00|3|BILL00300|1500.00|30000.00|4|BILL0040000|20.00|400.00";										                                             		                  
                        var billDetailDesc = warnMsg.split('|');
						var billDetails = [];	
			
						gblSegBillerDataMB["TotalInvoice"] = billDetailDesc[0];
						
						var tempInvoice = {};							
						var mwaFieldCount = 4;
						
						for (var i = 1; i < billDetailDesc.length; i++) {								
							var index = i % mwaFieldCount;
							switch (index) {
									case 1:	// sequenceNo
										sequenceNo = billDetailDesc[i];
										break;
									case 2:	// billNumber
										billNumber = formatBillNumber(billDetailDesc[i]);
										break;
									case 3:	// vatAmount
										vatAmount = billDetailDesc[i];
										break;
									case 0: // TotalAmount
										TotalAmount = billDetailDesc[i];
										tempInvoice = {
											"sequenceNo": sequenceNo,											
											"billNumber": billNumber,												
											"vatAmount": autoFormatAmount(vatAmount),												
											"TotalAmount": autoFormatAmount(TotalAmount)											
										};
										billDetails.push(tempInvoice);
										break;
									default:						
							}	
						}
						
						gblSegBillerDataMB["BillDetails"] = billDetails;                                                 
               			if (gblPEA_BILLER_COMP_CODE.indexOf(gblCompCode) != -1 ) {
                          frmBillPayment.hbxMeterNo.setVisibility(false);
                          frmBillPayment.lineMeterNo.setVisibility(false);
                          frmBillPayment.vboxPeainfo.setVisibility(true);
                        }else{
                          frmBillPayment.hbxMeterNo.setVisibility(true);
                          frmBillPayment.lineMeterNo.setVisibility(true);
                          frmBillPayment.vboxPeainfo.setVisibility(false); 
                        }
                      
               			// Bill Number
               			gblSegBillerDataMB["MeterNo"] = gblSegBillerDataMB["BillDetails"][0]["billNumber"];
                        gblSegBillerDataMB["CustName"] = result["OnlinePmtInqRs"][0]["Ref1"];                        
                        gblSegBillerDataMB["CustAddress"] = result["OnlinePmtInqRs"][0]["CustReference"];
                        
                		if (TranId != "") {
                			var locale = kony.i18n.getCurrentLocale();
							if (kony.string.startsWith(locale, "en", true) == true) {
                            	frmBillPayment.lblMEACustName.text = gblSegBillerDataMB["billerCustNameEn"];
                            	frmBillPayment.lblMeterNo.text = gblSegBillerDataMB["billerMeterNoEn"];
                                frmBillPayment.lblAmount.text = gblSegBillerDataMB["billerTotalPayAmtEn"];
                                frmBillPayment.lblAmtLabel.text = gblSegBillerDataMB["billerAmountEn"];
                                frmBillPayment.lblAmtInterest.text = gblSegBillerDataMB["billerTotalIntEn"];                                
                    		} else {
                            	frmBillPayment.lblMEACustName.text = gblSegBillerDataMB["billerCustNameTh"];
                            	frmBillPayment.lblMeterNo.text = gblSegBillerDataMB["billerMeterNoTh"];
                                frmBillPayment.lblAmount.text = gblSegBillerDataMB["billerTotalPayAmtTh"];
                                frmBillPayment.lblAmtLabel.text = gblSegBillerDataMB["billerAmountTh"];
                                frmBillPayment.lblAmtInterest.text = gblSegBillerDataMB["billerTotalIntTh"];                                
                            }
                            //set values in all label recieved from service for split amount
                            frmBillPayment.hbxAmountDetailsMEA.setVisibility(true);
                            
                            //frmBillPayment.lineMeterNo.setVisibility(true);
                            //frmBillPayment.hbxMeterNo.setVisibility(true);
                            frmBillPayment.hbxMeaCustomerName.setVisibility(true);
                            frmBillPayment.lineRef1.setVisibility(true);
                            frmBillPayment.lblMeterNoVal.text = gblSegBillerDataMB["MeterNo"];
                            frmBillPayment.lblMEACustNameValue.text = gblSegBillerDataMB["CustName"];
                            frmBillPayment.lblForFullPayment.text = commaFormatted(parseFloat(removeCommos(fullAmt)).toFixed(2));

                            if (gblRef2 == undefined || gblRef2 == null || gblRef2 == "") {
                                gblRef2 = "0.00";
                            }                      
                                  
                            var newAmtValue = fullAmt - gblRef2;
                            frmBillPayment.lblAmountValue.text = commaFormatted(parseFloat(removeCommos(newAmtValue.toString())).toFixed(2));
                            frmBillPayment.lblAmtInterestValue.text = commaFormatted(parseFloat(removeCommos(gblRef2)).toFixed(2));                            
                            gblOnlinePmtType = result["OnlinePmtInqRs"][0]["OnlinePmtType"];	         				
						}													
                    } else {
                    	//This code is added to fix unable to enter amount for OnlineBillers which are having Full & Specified
						//(because of onDone event on Ref1 & Ref2 fields).
						if (frmBillPayment.hbxFullSpecButton.isVisible) {
							frmBillPayment.tbxMyNoteValue.setFocus(true);
						}
                    }
                     
                    if ((gblEA_BILLER_COMP_CODES.indexOf(gblCompCode) >= 0) && gblRef4 == "N") {
                        launchBillPaymentFirstTime();
                        dismissLoadingScreen();
                        alert(kony.i18n.getLocalizedString("keyMEARef2orRef4Invalid"));
                        return;
                    } 
                    
                    if(kony.application.getCurrentForm().id != "frmBillPayment"){                    	
                    	callBillPaymentCustomerAccountService();
                    } else {
                    	dismissLoadingScreen();
                    }	
                    
                } else if (gblEA_BILLER_COMP_CODES.indexOf(gblCompCode) >= 0) {
                    var addMEABiller = false;
                    var statusCode = ["PGI001", "PGI002", "PGI003", "PGI004", "PGI005", "PGI006", "PGI007", "PGI008", "PGI009", "PGI010"];
                    var currForm = kony.application.getCurrentForm();
                    if (currForm.id == "frmAddTopUpBillerconfrmtn") {
                        for (i = 0; i < statusCode.length; i++) {
                            if (result["XPServerStatCode"] == statusCode[i]) {
                                addMEABiller = true;
                            }
                        }
                    } else {
                        addMEABiller = false;
                    }
                    if (addMEABiller == true) {
                        launchBillPaymentFirstTime();
                        dismissLoadingScreen();
                        alert(" " + result["errMsg"]);
                    } else {
                        launchBillPaymentFirstTime();
                        dismissLoadingScreen();
                        if (result["XPServerStatCode"] == "PGI003" || result["XPServerStatCode"] == "PGI006") {
                            alert(" " + result["additionalDS"][2]["StatusDesc"]);
                        } else {
                            alert(" " + result["errMsg"]);
                        }
                    }
                    return;
                } else if(gblPEA_BILLER_COMP_CODE.indexOf(gblCompCode) != -1){
                  	launchBillPaymentFirstTime();
                    dismissLoadingScreen();
              		if(isNotBlank(result["errKey"])){
                      	alert(" " +kony.i18n.getLocalizedString(result["errKey"]));
                    }else{
                      	alert(" " + result["errMsg"]);
                    }
              		
                } else {
                    launchBillPaymentFirstTime();
                    dismissLoadingScreen();
                    if (result["XPServerStatCode"] == "PGI003" || result["XPServerStatCode"] == "PGI006") {
                        alert(" " + result["additionalDS"][2]["StatusDesc"]);
                    } else {
                        alert(" " + result["errMsg"]);
                    }
                    return;
                }
            } else {
                dismissLoadingScreen();
                alert(" " + result["errMsg"]);

            }
        } else {
	        dismissLoadingScreen();
        }
    }

    /*
     * Method to get the FeeAmount from the BillPaymentInquiry Service
     *
     *
     */
    function BillPaymentInquiryModule() {
    	
        inputParam = {};
          if (GblBillTopFlag) {
			var selectedItem = frmBillPayment.segSlider.selectedIndex[1];
            var selectedData = frmBillPayment.segSlider.data[selectedItem];
	        var tranCode;
	        if (selectedData.tdFlag == kony.i18n.getLocalizedString("DDA")) {
	            tranCode = "88" + "10";
	        } else {
	            tranCode = "88" + "20";
	        }

	        var invoice;
	        if (gblreccuringDisablePay == "N") {
	            invoice = "";
	        } else {
	            invoice = frmBillPayment.tbxRef2Value.text;
	        }
			inputParam["tranCode"] = tranCode;
			inputParam["fIIdent"] = selectedData.fromFIIdent;
			inputParam["postedDate"] = changeDateFormatForMBFBillPay(frmBillPayment.lblPayBillOnValue.text);
			inputParam["ePayCode"] = "";
			inputParam["waiveCode"] = "I";
			
			if (gblBillerMethod == "0"){ // Off line biller	
				var mobileOrCI = "05";
	           	inputParam["mobileOrCI"] = mobileOrCI;
	            inputParam["fromAcctNo"] = removeHyphenIB(selectedData.lblActNoval);
	            inputParam["fromAcctTypeValue"] = selectedData.tdFlag;
	            inputParam["transferAmt"] = billAmountBillPayMB;
	            inputParam["ref1"] = frmBillPayment.lblRef1Value.text;
	            inputParam["ref2"] = invoice;
	            inputParam["ref3"] = "";
	            inputParam["ref4"] = "";
	            inputParam["toAcctNo"] = gblBillerTaxID;
				if(gblCompCode.length > 4){
					inputParam["compCode"] = "";				
				}else{
					inputParam["compCode"] = gblCompCode;
				}			
	            showLoadingScreen();
	            invokeServiceSecureAsync("promptPayInq", inputParam, PromptPayInquiryServiceCallBack);
			}else{	// On line biller            
	            inputParam["fromAcctIdentValue"] = removeHyphenIB(selectedData.lblActNoval);
	            inputParam["fromAcctTypeValue"] = selectedData.tdFlag;	            
	            inputParam["transferAmount"] = billAmountBillPayMB;
	            inputParam["pmtRefIdent"] = frmBillPayment.lblRef1Value.text;
	            inputParam["invoiceNumber"] = invoice;	                        
	            inputParam["compCode"] = gblCompCode;
	            showLoadingScreen();
        		invokeServiceSecureAsync("billPaymentInquiry", inputParam, BillPaymentInquiryServiceCallBack);
			} 
        }else {
        
                var selectedItem = frmTopUp.segSlider.selectedIndex[1];
                var selectedData = frmTopUp.segSlider.data[selectedItem];
            //alert(selectedItem);
            //alert("selected data ="+selectedData);
            var tranCode;
            var amt;
            if (frmTopUp.hbxTopUpamntComboBox.isVisible) {
                amt = parseFloat(removeCommos(frmTopUp.btnTopUpAmountComboBox.text)).toFixed(2);
            } else {
                amt = parseFloat(removeCommos(frmTopUp.tbxExcludingBillerOne.text)).toFixed(2);
            }
            if (selectedData.tdFlag == kony.i18n.getLocalizedString("DDA")) {
                tranCode = "88" + "10";
            } else {
                tranCode = "88" + "20";
            }
          
            inputParam["tranCode"] = tranCode;
            inputParam["fromAcctIdentValue"] = removeHyphenIB(selectedData.lblActNoval);
            inputParam["fromAcctTypeValue"] = selectedData.tdFlag;
            inputParam["transferAmount"] = amt;
            inputParam["pmtRefIdent"] = frmTopUp.lblRef1Value.text;
            inputParam["invoiceNumber"] = "";
            inputParam["postedDate"] = changeDateFormatForMBFBillPay(frmTopUp.lblPayBillOnValue.text);
            inputParam["ePayCode"] = "";
            inputParam["waiveCode"] = "I";
            inputParam["compCode"] = gblCompCode;
            inputParam["fIIdent"] = selectedData.fromFIIdent;
            showLoadingScreen();
			invokeServiceSecureAsync("billPaymentInquiry", inputParam, BillPaymentInquiryServiceCallBack);
        }       
        //invokeServiceSecureAsync("billPaymentInquiry", inputParam, BillPaymentInquiryServiceCallBack);

    }
    /*
     *MIB-6010 P2P Bill Payment
	 *
	 */
	function PromptPayInquiryServiceCallBack(status, result) {

        if (status == 400) //success responce
        { //dismissLoadingScreen();
            
            if (result["opstatus"] == 0) {
                if (result["StatusCode"] == 0) {
                	var FeeAmnt = "0";
                    if (result["WaiveFlag"] == "Y") {
                        FeeAmnt = "0";
                    } else {
                        FeeAmnt = result["FeeAmt"];
                    }
                    
					if(gblCompCode.length > 4){
						var toAcctName = result["toAcctName"];
						if (toAcctName != undefined && toAcctName != null && toAcctName != "")  {
							gblPromptPayBillerAcctName = result["toAcctName"] + "(" + gblBillerTaxID + ")";
						} 
					}                    
                    frmBillPaymentConfirmationFuture.lblPaymentFeeValue.text = parseFloat(FeeAmnt).toFixed(2)+ " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                    checkBillPaymentCrmProfileInqMB();
                } else {
                    dismissLoadingScreen();
                    alert(result["errMsg"]);
                }
            } else {
                dismissLoadingScreen();
                alert(" " + result["errMsg"]);
            }
        }


    }	
    /*
     * call back of BillPaymentInquiryServiceCallBack
     *
     */
    function BillPaymentInquiryServiceCallBack(status, result) {

        if (status == 400) //success responce
        { //dismissLoadingScreen();
            
            if (result["opstatus"] == 0) {
                if (result["StatusCode"] == 0) {
                	var FeeAmnt = "0";
                    for (var i = 0; i < result["BillPmtInqRs"].length; i++) {
                        if (result["BillPmtInqRs"][i]["WaiveFlag"] == "Y") {
                            FeeAmnt = "0";
                        } else {
                            FeeAmnt = result["BillPmtInqRs"][i]["FeeAmnt"];
                        }
                    }
                    frmBillPaymentConfirmationFuture.lblPaymentFeeValue.text = parseFloat(FeeAmnt).toFixed(2)+ " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                    checkBillPaymentCrmProfileInqMB();
                } else {
                    dismissLoadingScreen();
                    alert(result["errMsg"]);
                }
            } else {
                dismissLoadingScreen();
                alert(" " + result["errMsg"]);
            }
        }


    }
    /*
     * call back of creditcardDetailsInqService
     *
     */
function BillPaymentcreditcardDetailsInqServiceCallBack(status, result) {

        fullAmt = "0.00";
        minAmt = "0.00";
        if (status == 400) //success responce
        {
            
            if (result["opstatus"] == 0) {
                var StatusCode = result["StatusCode"];
                var StatusDesc = result["StatusDesc"]
                /** checking for Soap status below  */
                
                if (StatusCode != 0) {
                	dismissLoadingScreen();
                    alert(result["errMsg"]);
                    return false;
                } else {
                    
                    fullAmt = result["fullPmtAmt"];
                    if(parseInt(fullAmt)<0)gblPaymentOverpaidFlag=true;
                    minAmt = result["minPmtAmt"];
                    
                    if (fullAmt == "") {
                        fullAmt = "0.00";
                    }
                    if (minAmt == "") {
                        minAmt = "0.00";
                    }

                }
	            frmBillPayment.lblAmount.text = kony.i18n.getLocalizedString("TREnter_PL_Amount");
				gblPenalty = false;
				frmBillPayment.hbxPenalty.setVisibility(false);
				frmBillPayment.hbxMainAmountForPenalty.setVisibility(false);
				frmBillPayment.hbxFullSpecButton.setVisibility(false);
			
				frmBillPayment.hbxFullMinSpecButtons.setVisibility(true);
				frmBillPayment.button475004897849.skin = btnScheduleEndLeftFocus;
				frmBillPayment.button475004897851.skin = btnScheduleMid;
				frmBillPayment.button475004897853.skin = btnScheduleEndRight;
				frmBillPayment.lblForFullPayment.text = commaFormatted(fullAmt);
                frmBillPayment.lblForFullPayment.setVisibility(true);
                frmBillPayment.tbxAmount.setVisibility(false);
                frmBillPayment.tbxAmount.text = commaFormatted(fullAmt);
                frmBillPayment.tbxAmount.setEnabled(true);
                frmBillPayment.hbxFullSpecButton.setVisibility(false);
                frmBillPayment.hbxFullMinSpecButtons.setVisibility(true);
                gblFullPayment = true;
                paymentOverpaidCheck();
                if(kony.application.getCurrentForm().id != "frmBillPayment"){
                	callBillPaymentCustomerAccountService();
                } else {
                   	dismissLoadingScreen();
                }
            } else {
                dismissLoadingScreen();
                alert(" " + result["errMsg"]);
            }
        } else {
			dismissLoadingScreen();
        }

    }
    /*
     * CallBack of doLoanAcctInqService
     *
     */
function BillPaymentdoLoanAcctInqServiceCallBack(status, result) {
        
        fullAmt = "0.00";
        minAmt = "0.00";
        if (status == 400) //success responce
        {
            if (result["opstatus"] == 0) {
                var balAmount;
                fullAmt = result["regPmtCurAmt"];
                
                frmBillPayment.tbxAmount.setVisibility(false);
                
                frmBillPayment.lblForFullPayment.text = commaFormatted(fullAmt);
                frmBillPayment.lblForFullPayment.setVisibility(true);
                frmBillPayment.tbxAmount.text = commaFormatted(fullAmt);
                frmBillPayment.tbxAmount.setEnabled(true);
                frmBillPayment.hbxFullSpecButton.setVisibility(true);
                frmBillPayment.hbxFullMinSpecButtons.setVisibility(false);
                gblFullPayment = true;
                fullPress();
                if(kony.application.getCurrentForm().id != "frmBillPayment"){
					callBillPaymentCustomerAccountService();
				} else {
                	dismissLoadingScreen();
                }
            } else {
                dismissLoadingScreen();
                alert(kony.i18n.getLocalizedString("keyErrAccntNoInvalid"));
            }
        } else {
			dismissLoadingScreen();
        }
 }
    /*
     * CallBack of DepositAccountInquiry
     *
     */
function BillPaymentdepositAccountInquiryServiceCallBack(status, result) {

        if (status == 400) //success responce
        {
            
            if (result["opstatus"] == 0) {
                var StatusCode = result["StatusCode"];
                /** checking for Soap status below  S caps in service response*/
                
                var BalType;
                var Amt;
                var CurCode;

                for (var i = 0; i < result["AcctBal"].length; i++) {

                    BalType = result["AcctBal"][i]["BalType"];
                    
                    if (BalType == "Principle") {
                        Amt = result["AcctBal"][i]["Amt"];
                        CurCode = result["AcctBal"][i]["CurCode"];

                    }

                }
                frmBillPayment.lblForFullPayment.text = commaFormatted(Amt);
                if(kony.application.getCurrentForm().id != "frmBillPayment"){
                	callBillPaymentCustomerAccountService();
                } else {
                	dismissLoadingScreen();
                }
            } else {
                dismissLoadingScreen();
                alert(" " + result["errMsg"]);

            }
        }

}

    function callPaymentAddBillServiceMB(billerMethod) {
        var inputParam = {}
        //inputParam["crmId"]="001100000000000000000009925939";

        var frmAcct = frmBillPaymentConfirmationFuture.lblAccountNum.text;
        GBLFINANACIALACTIVITYLOG.toAcctId = frmSelectBiller.segMyBills.selectedItems[0]["ToAccountKey"];
        var toAcctID = frmSelectBiller["segMyBills"]["selectedItems"][0]["ToAccountKey"];
        var billerID = frmSelectBiller["segMyBills"]["selectedItems"][0]["CustomerBillID"];
        var toAcct;
        for (var i = 0; i < toAcctID.length; i++) {
            if (toAcctID[i] != "-") {
                if (toAcct == null) {
                    toAcct = toAcctID[i];
                } else {
                    toAcct = toAcct + toAcctID[i];
                }
            }
        }
        var toAccType;
        var dueDate = frmBillPaymentConfirmationFuture.lblStartOnValue.text.trim();
        var acctIdlen = toAcct.length;
        var fourthDigit;
        if (acctIdlen == 14) {
            fourthDigit = toAcct.charAt(7);
        } else {
            fourthDigit = toAcct.charAt(3);
        }

        if (fourthDigit == "2" || fourthDigit == "7" || fourthDigit == "9")
            toAccType = "SDA";
        else if (fourthDigit == "1")
            toAccType = "DDA";
        //else if (fourthDigit == "3")
          //  toAccType = "CDA";
        //else if (fourthDigit == "5" || fourthDigit == "6")
          //  toAccType = "LOC";
        //else
          //  toAccType = "CCA";
		if(billerMethod == 2)
			toAccType = "CCA";
		else if(billerMethod == 3){
			toAccType = "LOC";
		}
		else if(billerMethod == 4)
			toAccType = "CDA";
        var fromAcctID;
        var fromAcctType;
        if(gblreccuringDisablePay == "Y")
        inputParam["pmtRefNo2"] = frmBillPaymentConfirmationFuture.lblRef2Value.text;
        for (var i = 0; i < frmAcct.length; i++) {
            if (frmAcct[i] != "-") {
                if (fromAcctID == null) {
                    fromAcctID = frmAcct[i];
                } else {
                    fromAcctID = fromAcctID + frmAcct[i];
                }
            }
        }
        if (fromAcctID.length == 14) {
            fromAcctType = "SDA";
            fourthDigit = fromAcctID.charAt(7);
        } else {
            fourthDigit = fromAcctID.charAt(3);
            if (fourthDigit == "2" || fourthDigit == "7" || fourthDigit == "9") {
                fromAcctType = "SDA";
                fromAcctID = "0000" + fromAcctID;
            } else {
                fromAcctType = "DDA";
            }
        }

        //if (fromAcctID.length == 14) {
        //        fromAcctType = "SDA";
        //        fourthDigit = fromAcctID.charAt(7);
        //        fromAcctID="0000"+fromAcctID;
        //     } else{
        //        fourthDigit = fromAcctID.charAt(3);    
        //    }

        //    if (fourthDigit == "2" || fourthDigit == "7" || fourthDigit == "9") {
        //        fromAcctType = "SDA";
        //    } else {
        //        fromAcctType = "DDA";
        //    }
        var dateOrTimes = frmBillPaymentConfirmationFuture.lblHiddenVariableForScheduleEnd.text;
        if (dateOrTimes == "2") {
            inputParam["NumInsts"] = frmBillPaymentConfirmationFuture.lblExecuteValue.text.substring(0, 1);
        } else if (dateOrTimes == "3") {
            inputParam["FinalDueDt"] = changeDateFormatForService(getFormattedDate(frmBillPaymentConfirmationFuture.lblEndOnValue.text, "en_US"));
        } else if (dateOrTimes == "1") {
            //inputParam["NumInsts"] = "99";
        }

        var tranCode;
        var f;
        var t;
        if (fromAcctType == "DDA") {
            f = "1";
        } else if (fromAcctType == "SDA") {
            f = "2";
        } else {
            f = "3";
        }
        if (toAccType == "DDA") {
            t = "1";
        } else if (toAccType == "SDA") {
            t = "2";
        } else if (toAccType == "CDA") {
            t = "3";
        } else if (toAccType == "LOC") {
            t = "5";
        } else {
            t = "9";
        }
        var pmtMethod = "BILL_PAYMENT"
        if (billerMethod == 1) {
            pmtMethod = "ONLINE_PAYMENT";
            inputParam["PmtMiscType"] = "MOBILE";
            inputParam["MiscText"] = frmBillPaymentConfirmationFuture.lblRef1Value.text;
        } else if (billerMethod == 2 || billerMethod == 3 || billerMethod == 4) {
            pmtMethod = "INTERNAL_PAYMENT"
        }
        var amount = parseFloat(removeCommos(frmBillPaymentConfirmationFuture.lblAmountValue.text))
        //tranCode = "88" + f + t;
        if (billerMethod == 0 || billerMethod == 1) {
            tranCode = "88" + f + "0"
        } else {
            tranCode = "88" + f + t;
        }
        inputParam["channelId"] = "MB";
        inputParam["rqUID"] = "";
        inputParam["toBankId"] = "011";
        inputParam["crmId"] = gblcrmId;
        inputParam["amt"] = amount.toFixed(2); //getAmount();
        inputParam["fromAcct"] = fromAcctID;
        inputParam["fromAcctType"] = fromAcctType;
        inputParam["toAcct"] = toAcct;
        inputParam["toAccTType"] = toAccType;
        inputParam["dueDate"] = changeDateFormatForService(getFormattedDate(frmBillPaymentConfirmationFuture.lblStartOnValue.text, "en_US"));
        inputParam["pmtRefNo1"] = frmBillPaymentConfirmationFuture.lblRef1Value.text;
        inputParam["custPayeeId"] = billerID;
        inputParam["transCode"] = tranCode;
        inputParam["memo"] = frmBillPaymentConfirmationFuture.lblMyNoteValue.text;
        inputParam["pmtMethod"] = pmtMethod;
        inputParam["curCode"] = "THB";
        var tranId = frmBillPaymentConfirmationFuture.lblTxnNumValue.text
        inputParam["extTrnRefId"] = "SB" + kony.string.sub(tranId, 2, tranId.length);
        var frequency = frmBillPaymentConfirmationFuture.lblEveryMonth.text;
        
        inputParam["freq"] = frequency;
        if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyDaily"))) {
            inputParam["freq"] = "Daily";
        }
        if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Weekly"))) {
            inputParam["freq"] = "Weekly";
        }
        if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Monthly"))) {
            inputParam["freq"] = "Monthly";
        }
        if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Yearly"))) {
            inputParam["freq"] = "Annually";
        }
        if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyOnce"))) {
            inputParam["freq"] = "once";
        }
        inputParam["desc"] = "";
        
        invokeServiceSecureAsync("doPmtAdd", inputParam, callPaymentAddBillServiceMBCallBack);
    }

    function callPaymentAddBillServiceMBCallBack(status, resulttable) {
        
        
        if (status == 400) //success responce
        {
            
            
            
            if (resulttable["opstatus"] == 0) {
                
                if (resulttable["statusCode"] != 0) {
                    dismissLoadingScreen();
                    alert(" " + resulttable["errMsg"]);
                    return;
                }
                gblCurrentBillPayTime = resulttable["currentTime"];
                callBillPaymentCrmProfileUpdateService();
            }
        }
    }

    function checkBillPaymentCrmProfileInqMB() {
        var inputParam = {}
        inputParam["crmId"] = gblcrmId;
       // inputParam["transferFlag"] = "true"
        showLoadingScreen()
        invokeServiceSecureAsync("crmProfileInq", inputParam, callBackBillPayCrmProfileInqMB)
    }

    function callBackBillPayCrmProfileInqMB(status, resulttable) {
        if (status == 400) {
            
            
            if (resulttable["opstatus"] == 0) {
                /// check channel limit and available balance
                var StatusCode = resulttable["statusCode"];
                var Severity = resulttable["Severity"];
                var StatusDesc = resulttable["StatusDesc"];
                if (StatusCode == 0) {
                    
                    //setCrmProfileInqData(resulttable);  //caching profile information for profile Update; 

                    var temp1 = [];
                    var temp = {
                        ebAccuUsgAmtDaily: resulttable["ebAccuUsgAmtDaily"]
                    }
                    kony.table.insert(temp1, temp);

                    gblCRMProfileData = temp1;
                    gblEmailIdBillerNotification = resulttable["emailAddr"];
                    
                    DailyLimit = parseFloat(resulttable["ebMaxLimitAmtCurrent"].toString());
                    UsageLimit = parseFloat(resulttable["ebAccuUsgAmtDaily"].toString());
                    // DailyLimit = 7000000;
                    // UsageLimit = 500000;
                    // gblTransferRefNo=resulttable["acctIdentValue"]
                    // 
                    // var billAmount = frmBillPaymentConfirmationFuture.lblAmountValue.text;
                    var billAmount = AmountPaid;
                    var billAmountFee = frmBillPaymentConfirmationFuture.lblPaymentFeeValue.text
                    
                    
                    totalBillPayAmt = parseFloat(removeCommos(billAmount));// + parseFloat(removeCommos(billAmountFee));

                    
                    gblchannelLimit = DailyLimit - UsageLimit;
                    gblchannelLimit = fixedToTwoDecimal(gblchannelLimit);
                    var BPdailylimitExceedMsg =  kony.i18n.getLocalizedString("MIB_BPExcDaily");
					BPdailylimitExceedMsg = BPdailylimitExceedMsg.replace("{remaining_limit}",  commaFormatted(gblchannelLimit+""));
                    
                    if(gblPaynow) {
                    if (gblchannelLimit > 0) {
                        
                        if (gblchannelLimit < totalBillPayAmt) {
                            dismissLoadingScreen();
                            showAlert(BPdailylimitExceedMsg, kony.i18n.getLocalizedString("info"));
                            //alert(kony.i18n.getLocalizedString("keyexceedsdailylimit"));
                            // alert(" "+"keyUserchannellimitexceedsfortheday"));
                            dismissLoadingScreen();
                            return false;
                        } else {

                            callGenerateTransferRefNoserviceBillPaymentMB();
                            return true;
                        }
                    } else {
                        // alert("user channel limit exceeds for the day!!!");
                        dismissLoadingScreen();
                      	 showAlert(BPdailylimitExceedMsg, kony.i18n.getLocalizedString("info"));
                        //alert(kony.i18n.getLocalizedString("keyexceedsdailylimit"));
                    }
					}else {
						if(DailyLimit>0){
						if (DailyLimit < totalBillPayAmt) {
                            dismissLoadingScreen()
                            showAlert(BPdailylimitExceedMsg, kony.i18n.getLocalizedString("info"));
                            //alert(kony.i18n.getLocalizedString("keyexceedsdailylimit"));
                            // alert(" "+"keyUserchannellimitexceedsfortheday"));
    
                            return false;
                        } else {

                            callGenerateTransferRefNoserviceBillPaymentMB();
                            return true;
                        }
						}else{
                        // alert("user channel limit exceeds for the day!!!");
                        dismissLoadingScreen();
                        showAlert(BPdailylimitExceedMsg, kony.i18n.getLocalizedString("info"));
                        //alert(kony.i18n.getLocalizedString("keyexceedsdailylimit"));
                    }
					}
                } else {
                    dismissLoadingScreen();
                    alert(" " + StatusDesc);
                    return false;
                }
            } else {
                dismissLoadingScreen();
                alert(" " + resulttable["errMsg"]);
                /**dismissLoadingScreen();**/
            }
        }
    }

    function callGenerateTransferRefNoserviceBillPaymentMB() {
        inputParam = {};
        if (GblBillTopFlag) {
            if (gblPaynow) {
                inputParam["transRefType"] = "NB";
            } else {
                inputParam["transRefType"] = "SB";
            }
        } else {
            if (gblPaynow) {
                inputParam["transRefType"] = "NU";
            } else {
                inputParam["transRefType"] = "SU";
            }
        }


        invokeServiceSecureAsync("generateTransferRefNo", inputParam, callGenerateTransferRefNoserviceBillPaymentMBCallBack);
    }

	function callGenerateTransferRefNoserviceBillPaymentMBCallBack(status, result) {
        
        if (status == 400) //success responce
        {
            
            if (result["opstatus"] == 0) {
                
                var refNum = result["transRefNum"];

                frmBillPaymentConfirmationFuture.lblTxnNumValue.text = refNum + "00";
                 if (GblBillTopFlag) {
                	saveToSessionBillPaymentMB();
                }else{
                saveToSessionTopUpMB();
                }
                gotoBillPaymentConfirmation();
            } else {
                dismissLoadingScreen();
                alert(" " + result["errMsg"]);
            }
        }
    }
