gblPermissionsJSONObj = {
	"PHONE_GROUP":"android.permission.READ_PHONE_STATE",
	"STORAGE_GROUP":"android.permission.WRITE_EXTERNAL_STORAGE",
	"LOCATION_GROUP":"android.permission.ACCESS_FINE_LOCATION",
	"CAMERA_GROUP":"android.permission.CAMERA",
	"CONTACTS_GROUP":"android.permission.READ_CONTACTS"
};

gblPermissionInitialized = false;

function checkStoragePermissions(){
   var options = {};
   var result = kony.application.checkPermission(kony.os.RESOURCE_EXTERNAL_STORAGE, options);	
   kony.print("checkStoragePermissions result ###########  "+result.status);
   if (result.status == kony.application.PERMISSION_DENIED){
      kony.print("checkStoragePermissions Permission denied  ");
	  if (result.canRequestPermission){
         kony.application.requestPermission(kony.os.RESOURCE_EXTERNAL_STORAGE, reqStoragePermissionCallback);
       }else{
         //gblStoragePermissionAllowed = false;
         kony.print("checkStoragePermissions cannot request permission for Phone Group");         
         var messageErr = kony.i18n.getLocalizedString("keyDenyPermissionsMsg"); // "App cannot proceed further unless the required permisssions are granted!"; //Need to chk with customer on msg text
		 showAlertForUniqueGenFail(messageErr);
       }
   } else if (result.status == kony.application.PERMISSION_GRANTED) {
      
      kony.store.setItem("VTAPPermissionsinit",true);
      kony.print("checkStoragePermissions Permission granted  ");
      kony.print("Permission allowed. ");
     // setUpVTapSDK();     
   } else if (result.status == kony.application.PERMISSION_RESTRICTED) {
      //gblStoragePermissionAllowed = false;
      kony.print("checkStoragePermissions Permission restricted ");
      var messageErr = kony.i18n.getLocalizedString("keyDenyPermissionsMsg"); //"App cannot proceed further unless the required permisssions are granted!"; //Need to chk with customer on msg text
	  showAlertForUniqueGenFail(messageErr);
   }
   
    function reqStoragePermissionCallback(response) {
        kony.print("reqStoragePermissionCallback "+response.status);
		if (response.status == kony.application.PERMISSION_GRANTED) {
            kony.store.setItem("VTAPPermissionsinit",true);
			kony.print("reqStoragePermissionCallback-PERMISSION_GRANTED: " + JSON.stringify(response));
			//gblStoragePermissionAllowed = true;
             kony.print("Permission allowed. ");
          //  setUpVTapSDK();            
		} else if (response.status == kony.application.PERMISSION_DENIED) {
			kony.print("reqStoragePermissionCallback-PERMISSION_DENIED: " + JSON.stringify(response));
			//gblStoragePermissionAllowed = false;
            var messageErr = kony.i18n.getLocalizedString("keyDenyPermissionsMsg"); //"App cannot proceed further unless the required permisssions are granted!"; //Need to chk with customer on msg text
			showAlertForUniqueGenFail(messageErr);
		}else {
            kony.print("reqStoragePermissionCallback-: " + JSON.stringify(response));
			//gblStoragePermissionAllowed = false;
            var messageErr = kony.i18n.getLocalizedString("keyDenyPermissionsMsg"); //"App cannot proceed further unless the required permisssions are granted!"; //Need to chk with customer on msg text
			showAlertForUniqueGenFail(messageErr);
        }
	}
}

function callBackShowAlertPermissionsInfo(response){
  kony.print("inside callBackShowAlertPermissionsInfo");   
  if(response == true){
    chkRunTimePermissionsForTrusteer();
  }else{
    try{
         kony.application.exit();
      }
   catch(Error)
      {
         
      }
  }
}

function showPermissoinsInfoAlert(){
   var keyCancelBtn = kony.i18n.getLocalizedString("keyCancelButton");
    var keyOKBtn = kony.i18n.getLocalizedString("keyOK");
    var locale = kony.i18n.getCurrentLocale();
	var list;
    var KeyTitle = "Info";  
    var keyMsg = kony.i18n.getLocalizedString("keyPermissionsMsg");
     //   "TMB Touch is about to ask you to allow some required permissions in order to continue. These will let TMB Touch perform security checking on your device.";
	/* if (locale == "en_US") {
	   keyMsg = "TMB Touch is about to ask you to allow some required permissions in order to continue. These will let TMB Touch perform security checking on your device.";
	 } else {
	   keyMsg = "TMB Touch ?????????????????????????????????????????????????????????????????";				
	 }*/
   //Defining basicConf parameter for alert
	var basicConf = {
		message: keyMsg,
		alertType: constants.ALERT_TYPE_CONFIRMATION,
		alertTitle: KeyTitle,
		yesLabel: keyOKBtn,
		noLabel: keyCancelBtn,
		alertHandler: callBackShowAlertPermissionsInfo
	};
  
     var options = {};
     var result = kony.application.checkPermission(kony.os.RESOURCE_EXTERNAL_STORAGE, options);
          
     //var VTAPPermissionsinitialized = kony.store.getItem("VTAPPermissionsinit");
     //kony.print("VTAPPermissionsinitialized -->"+VTAPPermissionsinitialized);
     //kony.print("isNotBlank(VTAPPermissionsinitialized) -> "+isNotBlank(VTAPPermissionsinitialized));
	//Defining pspConf parameter for alert
	var pspConf = {};
	//Alert definition
    //if(!isNotBlank(VTAPPermissionsinitialized) ||(VTAPPermissionsinitialized != null && VTAPPermissionsinitialized != true))
     if (isAndroidM() && result.status != kony.application.PERMISSION_GRANTED)
	   var infoAlert = kony.ui.Alert(basicConf, pspConf);
  
    function handleok(response){
      chkRunTimePermissionsForTrusteer();
    }
    return;
}

function chkRunTimePermissionsForTrusteer(){
//	if(isAndroidM()){
		kony.print("chkRunTimePermissionsForTrusteer : gblPermissionInitialized :"+gblPermissionInitialized);
      kony.print("checking phone permissionsa");
         try{
             var uid = kony.os.deviceInfo().uid ;
         }catch (ex){
           kony.print("user denied phone permissions");
           kony.print("checkStoragePermissions cannot request permission for Phone Group");
           var messageErr = kony.i18n.getLocalizedString("keyDenyPermissionsMsg"); //"App cannot proceed further unless the required permisssions are granted!"; //Need to chk with customer on msg text
		   showAlertForUniqueGenFail(messageErr);     
           return;
         }
		kony.print("Before Calling  checkStoragePermissions");
		checkStoragePermissions();
		kony.print("After Calling  checkStoragePermissions");
  }

function isAndroidM(){
	//#ifdef android
		var droidVersion = gblDeviceInfo.version;
		//kony.print("FPRINT here1 MARSHMALLOW....Checking.....droidVersion="+droidVersion);
		droidVersion = droidVersion.replace(/\./g, "");
		//kony.print("FPRINT here2 MARSHMALLOW....Checking.....droidVersion="+droidVersion);
		if(droidVersion.length == 2) droidVersion = droidVersion + "0";
		//kony.print("FPRINT here3 MARSHMALLOW....Checking.....droidVersion="+droidVersion);
		if(parseInt(droidVersion) < 600) {
			//kony.print("FPRINT NOT A MARSHMALLOW DEVICE....");
			return false;
		}
		return true;
	//#else
		return false;
	//#endif
}


function MarshmallowPermissionCheckResults(result) {
	if(result == "1"){
		kony.print("GOWRI: IN FINAL CALLBACK PERMISSION IS AVAIALABLE");
      
     
      
     //   setUpVTapSDK();
			
		//trusteerIntialize();
	}else{
		kony.print("GOWRI: IN FINAL CALLBACK PERMISSION IS UNAVAIALABLE");
		var messageErr="App cannot proceed further unless the required permisssions are granted!" //Need to chk with customer on msg text
		showAlertForUniqueGenFail(messageErr);
	}
}



var TmbTrusteerFFIObject;

function intialize(){
//showLoadingScreen();
//kony.print("GOWRI: TRUSTEER INITIALIZATION HAS STARTED!")
//registerPushCallBacks();  //moved to post-app init
  chkRunTimePermissionsForTrusteer();      //showPermissoinsInfoAlert();  
}

function trusteerIntialize(){

	/**
	 * The below code is commented, MIB-6454 Analysis of Trusteer code flow
	 * 
	 * We are going to send the KonydeviceId isnted of Trusteer unique id
	 ***/

/*
kony.print("GOWRI: IN FtrusteerIntialize");
if(TmbTrusteerFFIObject == null)
TmbTrusteerFFIObject = new trust.TmbTrusteerFFI();
//Invokes method 'intialize_trusteer' on the object
TmbTrusteerFFIObject.intialize_trusteer(callbackInit);
*/

}

//Commented for Arxan
/*

var TmbTrusteerFFIObject;
function intialize(){
showLoadingScreen();
if(TmbTrusteerFFIObject == null)
TmbTrusteerFFIObject = new trust.TmbTrusteerFFI();
//Invokes method 'intialize_trusteer' on the object
TmbTrusteerFFIObject.intialize_trusteer(callbackInit);
}

*/


//function checkSuspiciousApp(){
////Creates an object of class 'TmbTrusteerFFI'
//if(TmbTrusteerFFIObject == null)
//TmbTrusteerFFIObject = new trust.TmbTrusteerFFI();
//TmbTrusteerFFIObject.checkSuspiciousApp(
//		/**Function*/ callbackSuspicious);
//}
function checkMallware(){
//Commented for Arxan
/*
//Creates an object of class 'TmbTrusteerFFI'
if(TmbTrusteerFFIObject == null)
TmbTrusteerFFIObject = new trust.TmbTrusteerFFI();
//Invokes method 'checkMallware' on the object
TmbTrusteerFFIObject.checkMallware(
		/**Function*/ //callbackMal);*/
}
//function checkRoot(){
//
//		//Creates an object of class 'TmbTrusteerFFI'
// if(TmbTrusteerFFIObject == null)
// TmbTrusteerFFIObject = new trust.TmbTrusteerFFI();
////Invokes method 'getRootValue' on the object
//TmbTrusteerFFIObject.getRoot(
//		/**Function*/ callbackRoot);
//
//}
//function checkRiskScore(){
//
//if(TmbTrusteerFFIObject == null)
//TmbTrusteerFFIObject = new trust.TmbTrusteerFFI();
//////Invokes method 'getRiskAssessmentValue' on the object
//TmbTrusteerFFIObject.getAllResult(
//		/**Function*/ callbackRisk);
//
//}
//function getAllSecurityInfo(){
//
//if(TmbTrusteerFFIObject == null)
//TmbTrusteerFFIObject = new trust.TmbTrusteerFFI();
//TmbTrusteerFFIObject.getAllResult(
//		/**Function*/ callbackAll);
//}
//function getAppRestrictValue(){
//
//if(TmbTrusteerFFIObject == null)
//TmbTrusteerFFIObject = new trust.TmbTrusteerFFI();
//TmbTrusteerFFIObject.getAppRestrict(
//		/**Function*/ callbackApp);
//}
function getUniqueID(){

	/**
	 * The below code is commented, MIB-6454 Analysis of Trusteer code flow
	 * 
	 * We are going to send the KonydeviceId isnted of Trusteer unique id
	 ***/

	/**
    if(TmbTrusteerFFIObject == null)
        TmbTrusteerFFIObject = new trust.TmbTrusteerFFI();
    else
        TmbTrusteerFFIObject.getUniqueID(/**Function  callbackUnique);

	**/
	callbackUnique(getDeviceID());
}

function getConfigUpdateValue(){
//Commented for Arxan
/*
    if(TmbTrusteerFFIObject == null)
        TmbTrusteerFFIObject = new trust.TmbTrusteerFFI();
        TmbTrusteerFFIObject.getconfig(/**Function*/ //callbackConfig);*/
}

function sslValidationAndroid(){
//Commented for Arxan
    /*if(TmbTrusteerFFIObject == null) TmbTrusteerFFIObject = new trust.TmbTrusteerFFI();
    var url = appConfig.serverIp;
    TmbTrusteerFFIObject.sslValidationAndroid(/**Function*/ //callbackSSL,url);*/
}

function startConfigUpdate(){
/*
//Invokes method 'startConfigupdate' on the object
if(TmbTrusteerFFIObject == null)
TmbTrusteerFFIObject = new trust.TmbTrusteerFFI();
TmbTrusteerFFIObject.startConfigupdate();*/
}
//function checkOSversion(){
//if(TmbTrusteerFFIObject == null)
//TmbTrusteerFFIObject = new trust.TmbTrusteerFFI();
//TmbTrusteerFFIObject.getOsUpdatedValue(
//		/**Function*/ callbackOsUpdate);
//}

function callbackOsUpdate(osResult){

}
//function callbackRisk(riskResult){
//
//}

function callbackRoot(rootResult){
	
	if(rootResult == 0){
		GBL_rootedFlag =0;
	}else{
		GBL_rootedFlag =1;
	}
}






function startSecurutyValidations()
{
	sslValidationAndroid();
}
function callbackSSL(sslResult){
	if(sslResult == 0){
		//getConfigUpdateValue();
	} else {
		/*showAlertForUniqueGenFail(kony.i18n.getLocalizedString("keySSLValidationFailureMsg"));
		dismissLoadingScreen();*/
		
		//Network Down i.e, No Internet/WIFI - Should not display any message - TODO : Need to handle it properly
	//	alert(kony.i18n.getLocalizedString("keySSLValidationFailureMsg"));
		dismissLoadingScreen();
	}
	getConfigUpdateValue();
}
function callbackConfig(configResult){
	
	
	if(configResult == 0){
		
	}else{
		startConfigUpdate();
	}
	checkMallware();
}
function callbackMal(mallresult,info){
	
	
	if(mallresult == 0){
		//checkSuspiciousApp();
		//dismissLoadingScreen(); //dismissLoadingScreen should call after login success.  
		//GBL_Session_LOCK=false;
	}else{
		//GBL_Session_LOCK=true;
		dismissLoadingScreen();
		GBL_MalWare_Names = info;
		var messageErr = kony.i18n.getLocalizedString("keyMallwareFoundMsg");
		if(messageErr == null){
				messageErr = "The dangerous malware/suspicious app being present on device 1. The bank strongly recommend to cleansing malware before access to the application. Without cleansing the malware and access to the application, you can�t transfer money out. Please contact # Call center for more advice."	
			}
		messageErr = messageErr.replace("NAMES", info);
		var getEncrKeyFromDevice = kony.store.getItem("encrytedText");
		if (getEncrKeyFromDevice != null) {
			showAlertForSecValidation(messageErr);
		} else {
			showAlertForUniqueGenFail(messageErr);
		}
		
		
	}
}
function callbackSuspicious(result,info){
		
		dismissLoadingScreen();
		
		if(result == 0){
			kony("Security Validation completed for android");
		}else{
			//GBL_Session_LOCK=true;
			var messageErr = kony.i18n.getLocalizedString("keyMallwareFoundMsg");
			if(messageErr == null){
				messageErr = "The dangerous malware/suspicious app being present on device 1. The bank strongly recommend to cleansing malware before access to the application. Without cleansing the malware and access to the application, you can�t transfer money out. Please contact # Call center for more advice."	
			}
			messageErr = messageErr.replace("NAMES", info);
			var getEncrKeyFromDevice = kony.store.getItem("encrytedText");
			if (getEncrKeyFromDevice != null) {
				showAlertForSecValidation(messageErr);
			} else {
				showAlertForUniqueGenFail(messageErr);
			}
		}
}
function callbackInit(result){
	
	kony.print("Trusteer Initialisation Finish : " + result);
	if(result == "true"){
		startSecurutyValidations();
	}else{
		//showAlertForUniqueGenFail(kony.i18n.getLocalizedString("keyPharmingErrMsg"));
		var messageErr = kony.i18n.getLocalizedString("keyTrusteerInitFailErrMsg");
		if(isNotBlank(messageErr)){
			messageErr = messageErr.replace("ErrCD", result);
		}
		showAlertForUniqueGenFail(messageErr);
		dismissLoadingScreen();
	}
	
}


function startRiskDataCollForAndroid()
{
	
	function getAllSecurityInfoMW(){
	//Commented for Arxan
	/*
		if(TmbTrusteerFFIObject == null)
		TmbTrusteerFFIObject = new trust.TmbTrusteerFFI();
		TmbTrusteerFFIObject.getAllResult(
				/**Function*/ //callbackAllMW);*/
	}
	function callbackAllMW(str){
		GBL_Risk_DataTo_Log = str;
		checkRootMW();
	}
	function checkRootMW(){
	//Commented for Arxan	
	/*
			//Creates an object of class 'TmbTrusteerFFI'
	 if(TmbTrusteerFFIObject == null)
	 TmbTrusteerFFIObject = new trust.TmbTrusteerFFI();
	//Invokes method 'getRootValue' on the object
	TmbTrusteerFFIObject.getRoot(
			/**Function*/ //callbackRootMW);*/
	
	}
	function callbackRootMW(rootResult){
		
		if(rootResult == 0){
			GBL_rootedFlag =0;
		}else{
			GBL_rootedFlag =1;
		}
		checkMallwareMW();
	}
	function checkMallwareMW(){
	//Commented for Arxan
	/*
		//Creates an object of class 'TmbTrusteerFFI'
		if(TmbTrusteerFFIObject == null)
		TmbTrusteerFFIObject = new trust.TmbTrusteerFFI();
		//Invokes method 'checkMallware' on the object
		TmbTrusteerFFIObject.checkMallware(
				/**Function*/ //callbackMalMW);*/
		}
	function callbackMalMW(mallresult,info){
		if(mallresult == 0){
			GBL_MalWare_Names = "";
			GBL_Session_LOCK=false;
		}else{
			GBL_Session_LOCK=true;
			dismissLoadingScreen();
			GBL_MalWare_Names = info;
		}
		callServiceToLogRiskInfo();
	}
	getAllSecurityInfoMW();
}

function callbackRisk(riskResult){
	
	//GBL_Risk_DataTo_Log=riskResult;
	//callServiceToLogRiskInfo();
}
