gblSwitchToOrftSmart = false;
// Prompt pay logic for from Account Number for Mobile
function promptPayeligbleforFromAcc()
{
  var flag =false;
  var fromData = frmTransferLanding.segTransFrm.data;
  kony.print("From Data in promptPayeligbleforFromAcc");
  var fromAcctSelIndx = gbltranFromSelIndex[1];
  kony.print("selected index");
  kony.print(fromAcctSelIndx);
  var prompflag = fromData[fromAcctSelIndx].isOtherBankAllowed;
  kony.print("prompflag");
  kony.print(prompflag);
  if(fromData.length > 0)
    {
      if(prompflag == "Y"){
         flag  = true;
      }else{
        flag = false;
      }
    }
 return flag;
} 
  
// Prompt pay logic for from Account Number for IB
function promptPayeligbleforFromAccIB()
{
  var flag =false;
  gblcwselectedData = frmIBTransferCustomWidgetLP.custom1016496273208904.data[gblCWSelectedItem];
  var promptpayFlag = gblcwselectedData.isPromptpayFlag;
   if(promptpayFlag == "Y"){
      flag = true;
   }else{
	 flag =false;
   }
 return flag;
} 

// Check the from Account is All free Account
function isFromAccountIsAllFree()
{
  var flag =false;
  var fromData = frmTransferLanding.segTransFrm.data;
  kony.print("From Data in promptPayeligbleforFromAcc");
  var fromAcctSelIndx = gbltranFromSelIndex[1];
  kony.print("selected index");
  kony.print(fromAcctSelIndx);
  var prodCode = fromData[fromAcctSelIndx].prodCode;
  kony.print("prompflag");
  kony.print(prompflag);
  if(prodCode == "225"){
    flag  = true;
  }else{
    flag = false;
  }

 return flag;
} 


function isFromAccountIsAllFreeIB()
{
  var flag =false;
  gblcwselectedData = frmIBTransferCustomWidgetLP.custom1016496273208904.data[gblCWSelectedItem];
  var prodCode = gblcwselectedData.prodCode;
  alert(prodCode);
   if(prodCode == "Y"){
      flag = true;
   }else{
	 flag =false;
   }
 return flag;
} 

function setToAccPromptPayGlobalVars(accountSummaryResp)
{
  try{
    gbltoAccPromptPayRange1Lower = accountSummaryResp.toAccPromptPayRange1Lower;
    gbltoAccPromptPayRange1Higher= accountSummaryResp.toAccPromptPayRange1Higher;
    gbltoAccPromptPayRange2Lower = accountSummaryResp.toAccPromptPayRange2Lower;
    gbltoAccPromptPayRange2Higher= accountSummaryResp.toAccPromptPayRange2Higher;
    gbltoAccPromptPaySPlitFeeAmnt1 = accountSummaryResp.toAccPromptPaySPlitFeeAmnt1;
    gbltoAccPromptPaySPlitFeeAmnt2 = accountSummaryResp.toAccPromptPaySPlitFeeAmnt2;
    gblMaxTransfertoAccPromptPay = accountSummaryResp.toAccPromptPayTransLimit;
    gblTranstoAccPromptPaySplitAmnt = accountSummaryResp.toAccPromptPayTransSplitAmnt;
    gblLimittoAccPromptPayPerTransaction = accountSummaryResp.toAccPromptPayTransSplitAmnt;
    gblTOACC_PP_FREE_PROD_CODES = accountSummaryResp.TO_ACCOUNT_PROMPTPAY_FREE_PROD_CODES;
    gblTO_ACC_P2P_FEE_WAIVE_CODES = accountSummaryResp.TO_ACC_P2P_FEE_WAIVE_PROD_CODES;
  
    kony.print("setToAccPromptPayGlobalVars gbltoAccPromptPayRange1Lower>>"+gbltoAccPromptPayRange1Lower);
    kony.print("setToAccPromptPayGlobalVars gbltoAccPromptPayRange1Higher>>"+gbltoAccPromptPayRange1Higher);
    kony.print("setToAccPromptPayGlobalVars gbltoAccPromptPayRange2Lower>>"+gbltoAccPromptPayRange2Lower);
    kony.print("setToAccPromptPayGlobalVars gbltoAccPromptPayRange2Higher>>"+gbltoAccPromptPayRange2Higher);
    kony.print("setToAccPromptPayGlobalVars gbltoAccPromptPaySPlitFeeAmnt1>>"+gbltoAccPromptPaySPlitFeeAmnt1);
    kony.print("setToAccPromptPayGlobalVars gbltoAccPromptPaySPlitFeeAmnt2>>"+gbltoAccPromptPaySPlitFeeAmnt2);
    kony.print("setToAccPromptPayGlobalVars gblMaxTransfertoAccPromptPay>>"+gblMaxTransfertoAccPromptPay);
    kony.print("setToAccPromptPayGlobalVars gblTranstoAccPromptPaySplitAmnt>>"+gblTranstoAccPromptPaySplitAmnt);
    kony.print("setToAccPromptPayGlobalVars gblLimittoAccPromptPayPerTransaction>>"+gblLimittoAccPromptPayPerTransaction);
    kony.print("setToAccPromptPayGlobalVars gblTOACC_PP_FREE_PROD_CODES>>"+gblTOACC_PP_FREE_PROD_CODES);
    kony.print("setToAccPromptPayGlobalVars gblTO_ACC_P2P_FEE_WAIVE_CODES>>"+gblTO_ACC_P2P_FEE_WAIVE_CODES);
   
  }catch(e){
    kony.print("Exception in setToAccPromptPayGlobalVars>>"+e);
  }
  
}


function splitAmountToAccPromptPay(Amount) {
   try{
	kony.print("gblLimittoAccPromptPayPerTransaction>>"+gblLimittoAccPromptPayPerTransaction);
    kony.print("gbltoAccPromptPaySPlitFeeAmnt1>>"+gbltoAccPromptPaySPlitFeeAmnt1);
    kony.print("gbltoAccPromptPaySPlitFeeAmnt2>>"+gbltoAccPromptPaySPlitFeeAmnt2);
    kony.print("gbltoAccPromptPayRange1Lower>>"+gbltoAccPromptPayRange1Lower);
    kony.print("gbltoAccPromptPayRange1Higher>>"+gbltoAccPromptPayRange1Higher);
    kony.print("Amount>>"+Amount);
    gblsplitAmt = [];
	gblsplitFee = [];
    var transtoAccPromptPaySplitAmnt = parseFloat(gblLimittoAccPromptPayPerTransaction);
    kony.print("transtoAccPromptPaySplitAmnt>>"+transtoAccPromptPaySplitAmnt);
	var i = gbltranFromSelIndex[1]
	var fromData = frmTransferLanding.segTransFrm.data;
	var prodCode = fromData[i].prodCode;
	var remainingFee = fromData[i].remainingFee;
	remainingFee = parseInt(remainingFee);
	var minFee = parseInt(gbltoAccPromptPaySPlitFeeAmnt1);
	var maxFee = parseInt(gbltoAccPromptPaySPlitFeeAmnt2);
	var toAccPromptPayRange1Lower = parseInt(gbltoAccPromptPayRange1Lower);
	var toAccPromptPayRange1High = parseInt(gbltoAccPromptPayRange1Higher);
	var SplitCnt = parseInt(Amount / transtoAccPromptPaySplitAmnt);
    kony.print("SplitCnt>>"+SplitCnt);  
	var indexdot = Amount.indexOf(".");
	var decimal = "";
	var remAmt = "";
     
     kony.print("indexdot>>"+indexdot);
	if (indexdot > 0) {
		decimal = Amount.substr(indexdot, 3);
		Amount = Amount.substr(0, indexdot)
		remAmt = Amount % transtoAccPromptPaySplitAmnt;
		remAmt = remAmt + decimal;
		kony.print("remAmt in IF>>"+remAmt);
	} else {
		remAmt = Amount % transtoAccPromptPaySplitAmnt;
        remAmt = remAmt + ".00";
      	kony.print("remAmt>>"+remAmt);
	}
     kony.print("Before ForLoop");
	for (var i = 0; i < SplitCnt; i++) {
		gblsplitAmt.push(transtoAccPromptPaySplitAmnt + ".00");
		if(gblTOACC_PP_FREE_PROD_CODES.indexOf(prodCode) >= 0){
			gblsplitFee.push("0");
		} else if (gblTO_ACC_P2P_FEE_WAIVE_CODES.indexOf(prodCode) >= 0) {
				if (gblPaynow && remainingFee > 0) {
					gblsplitFee.push("0");
					remainingFee = remainingFee - 1;
				} else {
					if (transtoAccPromptPaySplitAmnt > toAccPromptPayRange1High)
						gblsplitFee.push(maxFee);
					else
						gblsplitFee.push(minFee);
				}
		} else {
			if (transtoAccPromptPaySplitAmnt > toAccPromptPayRange1High)
				gblsplitFee.push(maxFee);
			else
				gblsplitFee.push(minFee);
		}
	}
     kony.print("After ForLoop");
	if (remAmt > toAccPromptPayRange1Lower) {
		gblsplitAmt.push(remAmt);
       kony.print("Compare>>"+remAmt+"<>"+toAccPromptPayRange1High);
		if (remAmt <= toAccPromptPayRange1High) {
			if(gblTOACC_PP_FREE_PROD_CODES.indexOf(prodCode) >= 0){
				gblsplitFee.push("0");
			}else if (gblTO_ACC_P2P_FEE_WAIVE_CODES.indexOf(prodCode) >= 0) {
					if (gblPaynow && remainingFee > 0) {
						gblsplitFee.push("0");
						remainingFee = remainingFee - 1;
					} else
						gblsplitFee.push(minFee);
			}else{
				gblsplitFee.push(minFee);
			}
		} else {
			if(gblTOACC_PP_FREE_PROD_CODES.indexOf(prodCode) >= 0){
				gblsplitFee.push("0");
			} else if (gblTO_ACC_P2P_FEE_WAIVE_CODES.indexOf(prodCode) >= 0) {
					if (gblPaynow && remainingFee > 0) {
						gblsplitFee.push("0");
						remainingFee = remainingFee - 1;
					} else
						gblsplitFee.push(maxFee);
			}else{
				gblsplitFee.push(maxFee);
			}
		}
	}
   }catch(e){
     kony.print("Exception in splitAmountToAccPromptPay>>"+e);
   }
}
function getToAccPromptPayFee() {
    kony.print("entering into getToAccPromptPayFee params are"+gbltoAccPromptPaySPlitFeeAmnt1+"%"+gbltoAccPromptPaySPlitFeeAmnt2+"%"+gbltoAccPromptPayRange1Higher+"%"+gbltoAccPromptPayRange2Lower+"%"+gbltoAccPromptPayRange2Higher);
    // Reading the configurable amounts from properties files.
    var totFeeAmnt = 0.00;
    try {
        var Amnt = frmTransferLanding.txtTranLandAmt.text;
        Amnt = kony.string.replace(Amnt, ",", "")
        var fromAcctSelIndx = gbltranFromSelIndex[1]
        var fromData = frmTransferLanding.segTransFrm.data;
        var prodCode = fromData[fromAcctSelIndx].prodCode;
        //var noFreeTrns = fromData[fromAcctSelIndx].remainingFee; 
        var remainingFee = fromData[fromAcctSelIndx].remainingFee;
        var noFreeTrns = "";
        if(gblPaynow){
          noFreeTrns = remainingFee;
        }else{
          noFreeTrns = "0";
        }
      
      	var feeAmnt1 = parseFloat(gbltoAccPromptPaySPlitFeeAmnt1);
        var feeAmnt2 = parseFloat(gbltoAccPromptPaySPlitFeeAmnt2);
        var range1Lower = parseFloat(gbltoAccPromptPayRange1Lower);
        var range1Higher = parseFloat(gbltoAccPromptPayRange1Higher);
        var range2Lower = parseFloat(gbltoAccPromptPayRange2Lower);
        var range2Higher = parseFloat(gbltoAccPromptPayRange2Higher);
      
      	kony.print("The values are"+feeAmnt1+".."+feeAmnt2+".."+range1Lower+".."+range1Higher+".."+range2Lower+".."+range2Higher);

        var freeTransCnt = noFreeTrns;// MIB-12390	CR:Transfer to acccount-System debt remaining fee when transfer to account promptpay with from account is 219
        var splitCiel = 0.00;
        var splitFloor = 0.00;
        var maxChanAmnt = gblTranstoAccPromptPaySplitAmnt;
        var transAmnt = Amnt;
        var splitCnt = transAmnt / maxChanAmnt;
        var remainingAmnt = 0.00;
		kony.print(prodCode + "gblTOACC_PP_FREE_PROD_CODES.. " + gblTOACC_PP_FREE_PROD_CODES);
        kony.print("prodCode : " + prodCode );

        if (gblTOACC_PP_FREE_PROD_CODES.indexOf(prodCode) >= 0) {

            totFeeAmnt = 0.00; 

        } else {
           if (!(gblTO_ACC_P2P_FEE_WAIVE_CODES.indexOf(prodCode) >= 0)){
              freeTransCnt = 0;
            }
            kony.print(prodCode + "getToAccPromptPayFee start freeTransCnt.. " + freeTransCnt+"<<splitCnt>>"+splitCnt);
            if (splitCnt >= freeTransCnt) {
                kony.print(prodCode + "getToAccPromptPayFee start splitCnt.. " + splitCnt + " freeTransCnt=" + freeTransCnt);

                splitCnt = splitCnt - freeTransCnt;
                kony.print(prodCode + "getToAccPromptPayFee start splitCnt.. " + splitCnt);
                splitCiel = Math.ceil(splitCnt);
                kony.print(prodCode + "getToAccPromptPayFee start ..splitCiel " + splitCiel);

                splitFloor = Math.floor(splitCnt);
                kony.print(prodCode + "getToAccPromptPayFee start ..splitFloor " + splitFloor);

                transAmnt = transAmnt - (maxChanAmnt * freeTransCnt);
            } else {
                return 0.00;
            }
            kony.print(prodCode + "getToAccPromptPayFee start freeTransCnt.. " + freeTransCnt);

            // Gives maxSplits for the maximum amount
            if (splitFloor != splitCiel) {
                remainingAmnt = transAmnt - (splitFloor * maxChanAmnt);
            }
            kony.print("splitCiel.. " + splitCiel);

            if (splitCiel > 1) {
                if (maxChanAmnt > range1Higher) {
                    totFeeAmnt = feeAmnt2 * splitFloor;
                } else {
                    totFeeAmnt = feeAmnt1 * splitFloor;
                }
            } else {
                if (transAmnt > range1Higher) {
                    totFeeAmnt = feeAmnt2 * splitFloor;
                } else {
                    totFeeAmnt = feeAmnt1 * splitFloor;
                }
            }
            if (remainingAmnt != 0) {
                if (remainingAmnt > range1Lower && remainingAmnt <= range1Higher) {
                    totFeeAmnt = totFeeAmnt + feeAmnt1;
                } else if (remainingAmnt >= range2Lower && remainingAmnt <= range2Higher) {
                    totFeeAmnt = totFeeAmnt + feeAmnt2;
                }
            }

        }

    } catch (e) {
        kony.print("Exception in calculations of getToAccPromptPayFee>>" + e.message);
    }
    //alert("Total Amount is"+totFeeAmnt);
  	return totFeeAmnt.toFixed(2);
}



function toAccPromptPayFlow(){
  try{
    kony.print("toAccPromptPayFlow fun");
    var enteredAmount = frmTransferLanding.txtTranLandAmt.text;
    var accNumber = "";
    if(frmTransferLanding.tbxAccountNumber.isVisible){
      accNumber =  removeHyphenIB(frmTransferLanding.tbxAccountNumber.text);
    }else{
      accNumber = removeHyphenIB(frmTransferLanding.lblTranLandToAccountNumber.text);
      frmTransferLanding.tbxAccountNumber.text = frmTransferLanding.lblTranLandToAccountNumber.text;
    }
    kony.print("accNumber:"+accNumber);
    gblSelTransferMode = 0;
    gblTransSMART =0;
    gblTrasORFT = 0;
    frmTransferLanding.hbxTransLndFee.setVisibility(false);
    if(!isNotBlank(frmTransferLanding.lblMobileNoTemp.text)){
			frmTransferLanding.lblMobileNoTemp.text = "";
	}
    if(!isNotBlank(enteredAmount) || parseFloat(enteredAmount, 10) == 0){
      displayP2PITMXFee(false, enteredAmount);
    }else{
      if(parseFloat(enteredAmount, 10) > 0 && isNotBlank(accNumber)){
         kony.print("lblMobileNoTemp>>"+frmTransferLanding.lblMobileNoTemp.text);
         kony.print("tbxAccountNumber>>"+frmTransferLanding.tbxAccountNumber.text);
        if(kony.string.equalsIgnoreCase(frmTransferLanding.lblMobileNoTemp.text, frmTransferLanding.tbxAccountNumber.text)){
			displayP2PITMXFee(true, enteredAmount);
		}else{   
           frmTransferLanding.lblMobileNoTemp.text = frmTransferLanding.tbxAccountNumber.text;
           kony.print("callCheckOnUsPromptPayinqServiceMB cal>>");
           callCheckOnUsPromptPayinqServiceMB(); 
        }
       }
    }
  
  }catch(e){
    kony.print("Exception in toAccPromptPayFlow function>> "+e);    
  }
   
  }


function transferAgaineAccountTransaction(){
    kony.print("@@@@@transferAgaineAccountTransaction@@@");
    gblSwitchToOrftSmart = false;
	showLoadingScreen();
	gblSelTransferMode = 0;
	setOnClickSetCanlenderBtn(true);
	displayOnUsEnterMobileNumber(false);
	displayP2PITMXBank(false);
	displayOnUsEnterBankAccount(true);	
	gblisTMB = frmTransfersAckCalendar.lblToBankCDHidden.text;
  	assignBankSelectDetails(gblisTMB);
	frmTransferLanding.txtOnUsMobileNo.setVisibility(false);
  	var accno = frmTransfersAckCalendar.lblToAcctNumHidden.text;
	frmTransferLanding.tbxAccountNumber.text = encodeAccntNumbers(accno);
	var amt = frmTransfersAckCalendar.lblHiddenTransferAmt.text;
  	kony.print("@@@@@@amt var"+amt);
  	frmTransferLanding.txtTranLandAmt.text = commaFormatted(parseFloat(removeCommaIB(amt)).toFixed(2));
	var amount1 = frmTransferLanding.txtTranLandAmt.text;
  	displayP2PITMXFee(true,amount1);
  	kony.print("@@@@@@text vale" +frmTransferLanding.txtTranLandAmt.text);
  	frmTransferLanding.txtTranLandMyNote.text = frmTransfersAckCalendar.lblMyNote.text.trim();
  	kony.print(">>>>>>transferAgaineAccountTransaction..."+frmTransfersAckCalendar.lblConfirmSendersNotes.text);
//   	if(isNotBlank(frmTransfersAckCalendar.lblConfirmSendersNotes.text)){
//   		frmTransferLanding.textRecNoteEmail.text = frmTransfersAckCalendar.lblConfirmSendersNotes.text;
//     }else{
//       	frmTransferLanding.textRecNoteEmail.placeholder =   kony.i18n.getLocalizedString("TREnter_PL_SNote"); 
//     }
      makeWidgetsBelowTransferFeeButtonVisible(true);	
	frmTransferLanding.txtTranLandRecNote.text = "";
  	frmTransferLanding.textRecNoteEmail.text = "";
	//frmTransferLanding.imgOnUsMob.src = "tran_smartphone.png";
  	//frmTransferLanding.imgOnUsMob.margin = [0,1,0,0];
  	frmTransferLanding.textRecNoteEmail.placeholder =   kony.i18n.getLocalizedString("TREnter_PL_SNote"); 
	frmTransferLanding.txtTranLandRecNote.placeholder =  kony.i18n.getLocalizedString("TREnter_PL_SNote"); 
	dismissLoadingScreen();
}

function calculteFeeSwitchToORFTSmartFee(isNext){
  try{
      /*if(isNotBlank(isNext)){
        if (gblTransSMART == 1 || gblTrasORFT == 1) {
            return; // User already selected fee option hence return
        }
     } */
        var amt = frmTransferLanding.txtTranLandAmt.text;
        amt = kony.string.replace(amt, ",", "");
        kony.print("amt>>"+amt);
        var fromAcctSelIndx = gbltranFromSelIndex[1];
        fromData = frmTransferLanding.segTransFrm.data;
        var prodCode = fromData[fromAcctSelIndx].prodCode;
        kony.print("prodCode>>"+prodCode);
        var remainingFee = fromData[fromAcctSelIndx].remainingFee;
        kony.print("@ calculteFeeSwitchToORFTSmartFee Fee Calculation start .. Amnt:"+amt+"  remainingFee:"+remainingFee+ "   prodcode :" + prodCode);  
        kony.print("gblPaynow >>"+gblPaynow);
        if(!gblPaynow){// For schedule fee should calculate and it will waive while execution
          remainingFee = 0;
        }
    	var orftFee = getORFTFee(amt, remainingFee, prodCode);
        var smartFee = getSMARTFee(amt, remainingFee, prodCode);
        var cutoffInd = gblTrnsfrcutoffInd;
        var currentDate = gblTrnsfrorftcurrentDate;
    	
        smartDate = gblTrnsfrsmartDate;
        // changed/added below code to fix DEF1068
        var smartDateNew = gblTrnsfrsmartDateNew;
        var smartfuturetime = gblTrnsfrsmartfuturetime;
        var orftFutureTime = gblTrnsfrorftFutureTime;
    	orftFee=orftFee+".00";
        smartFee=smartFee+".00";
    	kony.print("The values of the fee before are"+orftFee+"&"+smartFee)
        orftFee = commaFormatted(orftFee);
        smartFee = commaFormatted(smartFee);
    	kony.print("The values of the fee after are"+orftFee+"&"+smartFee)

        var orftFlag = getORFTFlag(gblisTMB);
        var smartFlag = getSMARTFlag(gblisTMB);
    	kony.print("orftFlag in calculteFeeSwitchToORFTSmartFee>>"+orftFlag)
        if (smartFlag == "N" && orftFlag == "N" && gblisTMB != gblTMBBankCD) {
             showAlert(kony.i18n.getLocalizedString("MIB_P2PDestTimeout"), kony.i18n.getLocalizedString("info"));
             frmTransferLanding.btnTranLandNext.setEnabled(false);
             return false;
        } else {
            frmTransferLanding.btnTranLandNext.setEnabled(true);
        }
    	if (orftFlag == "N") {
           kony.print("Inside SMART Flag>>");
           if(gblSwitchToOrftSmart) {// To habdle confirmation alert wil come only once
             displaySwitchSmartFee();
           } else {
             showAlertWithYesNoHandler(kony.i18n.getLocalizedString("MIB_TRSMARTConfirm"), "", 
                                      kony.i18n.getLocalizedString("keyOK"), kony.i18n.getLocalizedString("keyCancelButton"), 
                                      displaySwitchSmartFee, onClickCancelBtnTransferLandingMB);
           }
           
          } else {
                var fromAcctSelIndx = gbltranFromSelIndex[1];
                var fromData = frmTransferLanding.segTransFrm.data;
                var prodCode = fromData[fromAcctSelIndx].prodCode;
				frmTransferLanding.hbxTransLndFee.setVisibility(false);
				frmTransferLanding.hbxFeeTransfer.setVisibility(true);
				frmTransferLanding.lblITMXFee.setVisibility(true);
				frmTransferLanding.lblFeeTransfer.text = kony.i18n.getLocalizedString("MIB_P2PTRFeeTransfer");
				frmTransferLanding.lblITMXFee.skin = "lblGrey48px";
				frmTransferLanding.lblFeeTransfer.skin = "lblGrey36px";
				gblTrasORFT = 0;
				gblTransSMART = 0;
				ehFrmTransferLanding_btnTransLndORFT_onClick();
                if (gblORFT_ALL_FREE_TRANS_CODES.indexOf(prodCode) >= 0) {
                   frmTransferLanding.lblITMXFee.text = kony.i18n.getLocalizedString("keyFreeTransfer");
                   frmTransferLanding.btnTransLndORFT.text = "0.00" + " \n" + kony.i18n.getLocalizedString("TREnter_ORFT");
                }else if(gblORFT_FREE_TRANS_CODES.indexOf(prodCode) >= 0 && parseFloat(orftFee) == 0 ) {
                     frmTransferLanding.hbxTransLndFee.setVisibility(false);
                     frmTransferLanding.hbxFeeTransfer.setVisibility(true);
                     frmTransferLanding.lblITMXFee.setVisibility(true);
                     frmTransferLanding.lblITMXFee.text = kony.i18n.getLocalizedString("keyFreeTransfer");
                     frmTransferLanding.lblFeeTransfer.text = kony.i18n.getLocalizedString("MIB_P2PTRFeeTransfer");
                     frmTransferLanding.lblITMXFee.skin = "lblGrey48px";
                     frmTransferLanding.lblFeeTransfer.skin = "lblGrey36px";
                     gblTrasORFT = 0;
                     ehFrmTransferLanding_btnTransLndORFT_onClick();
                } else {
                  
                     frmTransferLanding.hbxTransLndFee.setVisibility(false);
                     frmTransferLanding.hbxFeeTransfer.setVisibility(true);
                     frmTransferLanding.lblITMXFee.setVisibility(true);
                     frmTransferLanding.lblITMXFee.text = orftFee;
                     frmTransferLanding.lblFeeTransfer.text = kony.i18n.getLocalizedString("MIB_P2PTRFeeTransfer");
                     frmTransferLanding.lblITMXFee.skin = "lblGrey48px";
                     frmTransferLanding.lblFeeTransfer.skin = "lblGrey36px";
                     gblTrasORFT = 0;
                     ehFrmTransferLanding_btnTransLndORFT_onClick();
                    /*frmTransferLanding.btnTransLndSmart.setEnabled(true);
                    frmTransferLanding.btnTransLndSmart.setVisibility(true);
                    if (!frmTransferLanding.hbxTransLndFee.isVisible) { //reset if smart only selected already and change to orft allowed bank
                        gblTrasORFT = 0;
                        gblTransSMART = 0;
                        frmTransferLanding.btnTransLndORFT.skin = "btnFeeTop";
                        frmTransferLanding.btnTransLndSmart.skin = "btnFeeBottom";
                        frmTransferLanding.lblRecievedBy.text = "-";
                        frmTransferLanding.lblRecievedByValue.text = "-";
                    } else {
                        if (gblTransSMART == 1 && !frmTransferLanding.hbxTranLandShec.isVisible) {
                            gblTransSMART = 0;
                            gblTrasORFT = 0;
                            ehFrmTransferLanding_btnTransLndSmart_onClick();
                        } else if (gblTrasORFT == 1 && !frmTransferLanding.hbxTranLandShec.isVisible) {
                            gblTrasORFT = 0;
                            gblTransSMART = 0;
                            ehFrmTransferLanding_btnTransLndORFT_onClick();
                }
            }
                    frmTransferLanding.hbxTransLndFee.setVisibility(true);
                    frmTransferLanding.hbxFeeTransfer.setVisibility(false);
                    */
                 }
            }
        frmTransferLanding.btnTransLndSmart.text = smartFee + " \n" + kony.i18n.getLocalizedString("TREnter_SMART");
        frmTransferLanding.btnTransLndORFT.text = orftFee + " \n" + kony.i18n.getLocalizedString("TREnter_ORFT");  
        var currentDate = currentDate.toString();
        var dateTD = currentDate.substr(0, 2);
        var yearTD = currentDate.substr(6, 4);
        var monthTD = currentDate.substr(3, 2);
        var completeDate = yearTD + "-" + monthTD + "-" + dateTD;
        gblTransferDate = completeDate;
        
  }catch(e){
    kony.print("The exception in calculteFeeSwitchToORFTSmartFee"+e.message);
  }
 }


function displaySwitchSmartFee(){
  
    var amt = frmTransferLanding.txtTranLandAmt.text;
    amt = kony.string.replace(amt, ",", "");
    kony.print("amt>>"+amt);
    var fromAcctSelIndx = gbltranFromSelIndex[1];
    fromData = frmTransferLanding.segTransFrm.data;
    var prodCode = fromData[fromAcctSelIndx].prodCode;
    kony.print("prodCode>>"+prodCode);
    var remainingFee = fromData[fromAcctSelIndx].remainingFee;
    if(!gblPaynow){// For schedule fee should calculate and it will waive while execution
          remainingFee = 0;
    }
    kony.print("displaySwitchSmartFee- Fee Calculation start .. Amnt:"+amt+"  NofeeTrans:"+remainingFee+ "   prodcode :" + prodCode);  
    var smartFee = getSMARTFee(amt, remainingFee, prodCode);
    smartDate = gblTrnsfrsmartDate;
    smartFee=smartFee+".00";
    kony.print("displaySwitchSmartFee - The values of the fee before are"+smartFee);
    smartFee = commaFormatted(smartFee);
    kony.print("displaySwitchSmartFee - The values of the fee after are"+smartFee);
  
   	frmTransferLanding.btnTransLndORFT.setEnabled(false);
	frmTransferLanding.btnTransLndSmart.setEnabled(false);
	frmTransferLanding.hbxTransLndFee.setVisibility(false);
	frmTransferLanding.hbxFeeTransfer.setVisibility(true);
	makeWidgetsBelowTransferFeeButtonVisible(true);
	displayHbxNotifyRecipient(true);
	if (frmTransferLanding.txtTransLndSmsNEmail.isVisible || frmTransferLanding.txtTransLndSms.isVisible) {
		//frmTransferLanding.lineNotifyRecipientButton.setVisibility(true);
	} else {
		//frmTransferLanding.lineNotifyRecipientButton.setVisibility(false);
	}
	if (parseFloat(smartFee) == 0) {
		frmTransferLanding.lblITMXFee.text = kony.i18n.getLocalizedString("keyFreeTransfer");
        frmTransferLanding.btnTransLndSmart.text = "0.00" + " \n" + kony.i18n.getLocalizedString("TREnter_SMART");
	} else {
		frmTransferLanding.lblITMXFee.text = smartFee;
        frmTransferLanding.btnTransLndSmart.text = smartFee + " \n" + kony.i18n.getLocalizedString("TREnter_SMART");
	}
	frmTransferLanding.lblFeeTransfer.text = kony.i18n.getLocalizedString("TREnter_SMART");
	frmTransferLanding.lblFeeTransfer.skin = "lblGrey36px";
	frmTransferLanding.lblITMXFee.setVisibility(true);

	var smartDateTime = smartDate.split(" ");
	if (smartDateTime.length > 1) {
		frmTransferLanding.lblRecievedBy.text = smartDateTime[0];
		frmTransferLanding.lblRecievedByValue.text = smartDateTime[1];
	}
	frmTransferLanding.lblRcvTime.text = kony.i18n.getLocalizedString("TREnter_Time_02");
	frmTransferLanding.lineRecipientNote.setVisibility(true);
	frmTransferLanding.btnTransLndSmart.setVisibility(true);
	frmTransferLanding.btnTransLndORFT.setVisibility(true);
	frmTransferLanding.btnTransLndORFT.skin = "btnFeeTop";
	frmTransferLanding.btnTransLndSmart.skin = "btnFeeBottom";
	gblTrasORFT = 0;
	gblTransSMART = 1;
}


function getToAccPPFeeFromLanding(){
  var fee = 0;
  if(frmTransferLanding.hbxFeeTransfer.isVisible && isNotBlank(frmTransferLanding.lblITMXFee.text)){
	var fee = frmTransferLanding.lblITMXFee.text+"";
	kony.print("ITMXFee text in indexof >>"+fee);
	var feecheck = fee.indexOf(".");
	if(feecheck == -1){
		fee = 0;
	}
}
 return fee;  
}


function toAccPPgetTransferFeeCall(){
  		var inputParam = {}
        var fromData;
        var amt = frmTransferLanding.txtTranLandAmt.text;
        amt = kony.string.replace(amt, ",", "");
        kony.print("@@ toAccPPgetTransferFeeCall-amt>>"+amt);

        var fromAcctSelIndx = gbltranFromSelIndex[1]
        fromData = frmTransferLanding.segTransFrm.data;
        var prodCode = fromData[fromAcctSelIndx].prodCode;
        kony.print("@@ toAccPPgetTransferFeeCall-prodCode>>"+prodCode);
        var remainingFee = fromData[fromAcctSelIndx].remainingFee;
        kony.print("@@toAccPPgetTransferFeeCall- remainingFee>>"+remainingFee);
        if(gblPaynow){
            inputParam["freeTransCnt"] = remainingFee;
        }else{
            inputParam["freeTransCnt"] = "0";
            remainingFee=0;
        }
        inputParam["Amount"] = amt
        inputParam["prodCode"] = prodCode;
        showLoadingScreen();
        invokeServiceSecureAsync("getTransferFee", inputParam, callBackgetTransferFeeMB);
}


function toAccPromptPayFeeCallback(resulttable){
	try{
		if (resulttable["opstatus"] == 0) {
			dismissLoadingScreen(); 
			var fee = resulttable["TOACC_PROMPTPAY_FEE"];
			kony.print("ToAccpromptPay @@ fee:>>"+fee);
			frmTransferLanding.lblITMXFee.setVisibility(true);
			frmTransferLanding.btnTransLndSmart.setVisibility(false);
			frmTransferLanding.btnTransLndORFT.setEnabled(false);
			frmTransferLanding.hbxTransLndFee.setVisibility(false);
			frmTransferLanding.hbxFeeTransfer.setVisibility(true);
			var date=new Date(GLOBAL_TODAY_DATE);
			var curMnth = date.getMonth()+1;
			var curDate = date.getDate();
			if ((curMnth.toString().length) == 1) {
			   curMnth = "0" + curMnth;
			}
			if ((curDate.toString().length) == 1) {
			   curDate = "0" + curDate;
			}
			var datetime = "" + curDate + "/" + curMnth + "/" + date.getFullYear();
			frmTransferLanding.lblRecievedBy.text = datetime;
			frmTransferLanding.lblRecievedByValue.text = kony.i18n.getLocalizedString("keyNOW");
			frmTransferLanding.lineRecipientNote.setVisibility(true);
			frmTransferLanding.lblRcvTime.text = kony.i18n.getLocalizedString("TREnter_Time_01");
			holdPreviouslySelectedNotifyDetails();
			frmTransferLanding.lblFeeTransfer.text = kony.i18n.getLocalizedString("MIB_P2PTRFeeTransfer");
			if(parseFloat(fee) == 0 || (gblisTMB == gblTMBBankCD)){
				frmTransferLanding.lblITMXFee.text = kony.i18n.getLocalizedString("keyFreeTransfer");
				frmTransferLanding.lblFeeTransfer.skin = "lblGrey36px";
			}else{
				frmTransferLanding.lblITMXFee.text = fee;
				frmTransferLanding.lblFeeTransfer.skin = "lblGrey36px";
			}
	  }	
	}catch(e){
		kony.print("Exception  in toAccPrompatPayFeeCallback>>"+e);
	}
}

//MegaTransfer flag checking 

function megaTransFlagCheck(){
  kony.print("gblisTMB in megaTransFlagCheck>>"+gblisTMB);
  var megaTransferFlag = getMegaTransferFlag(gblisTMB);
  kony.print("megaTransferFlag");
  kony.print(megaTransferFlag);
  var flag = true;
  if(isNotBlank(megaTransferFlag))
    {  
      if(megaTransferFlag == "0"){flag  = false;}
      else{flag = true;}
    }
   kony.print("flag>>"+flag)
   return flag; 
  
}
