function frmEKYCIdpDetailsList_init_action(){
  frmEKYCIdpDetailsList.preShow = frmEKYCIdpDetailsList_preShow_function;
  frmEKYCIdpDetailsList.postShow = frmEKYCIdpDetailsList_postShow_function;
  frmEKYCIdpDetailsList.onDeviceBack = disableBackButton;
  frmEKYCIdpDetailsList.btnSettings.onClick = frmEKYCIdpNoActive_btnSetting_function;
  frmEKYCIdpDetailsList.btnMenu.onClick = handleMenuBtn;
  frmEKYCIdpDetailsList.btnSortbyArrow.onClick = onClickEKYCIdpDetailsSort;
  frmEKYCIdpDetailsList.flxSortby.onClick = onClickEKYCIdpDetailsSort;
  frmEKYCIdpDetailsList.btnClose.onClick = onCloseEKYCIdpSortPopup;
  frmEKYCIdpDetailsList.flxExpDate.onClick = onClickSortExpDate;
  frmEKYCIdpDetailsList.flxReqType.onClick = onClickSortReqType;
  frmEKYCIdpDetailsList.flxBankName.onClick = onClickSortBankName;
  frmEKYCIdpDetailsList.btnUpdateIAL.onClick = updateIALViaListPage;
  frmEKYCIdpDetailsList.btnCloseAIL.onClick = openUpgradeIALPopup;
}
function frmEKYCIdpDetailsList_preShow_function(){
  showLoadingScreen();
  frmEKYCIdpDetailsList.flexIDPPopup.setVisibility(false);
  frmEKYCIdpDetailsList.flxSortPop.setVisibility(false);
  frmEKYCIdpDetailsList.lblHeader.text = kony.i18n.getLocalizedString("eKYC_titleIdPConsent");
  frmEKYCIdpDetailsList.lblSortByText.text = kony.i18n.getLocalizedString("eKYC_lbSortby");
  if(gblRTPDetailsSortBy == 1){
    frmEKYCIdpDetailsList.lblSortByVal.text = kony.i18n.getLocalizedString("eKYC_lbSortbyExp");
  }else if(gblRTPDetailsSortBy == 2){
    frmEKYCIdpDetailsList.lblSortByVal.text = kony.i18n.getLocalizedString("eKYC_lbSortbyReq");
  }else if(gblRTPDetailsSortBy == 3){
    frmEKYCIdpDetailsList.lblSortByVal.text = kony.i18n.getLocalizedString("eKYC_lbSortbyBankName");
  }else{
    frmEKYCIdpDetailsList.lblSortByVal.text = kony.i18n.getLocalizedString("eKYC_lbSortbyExp");
    gblRTPDetailsSortBy = 1;
  }
  frmEKYCIdpDetailsList.lblExpDate.text = kony.i18n.getLocalizedString("eKYC_lbSortbyExp");
  frmEKYCIdpDetailsList.lblReqType.text = kony.i18n.getLocalizedString("eKYC_lbSortbyReq");
  frmEKYCIdpDetailsList.lblBankName.text = kony.i18n.getLocalizedString("eKYC_lbSortbyBankName");
  // TO show button when the NFC is enabled on the device
  var nfcFlag = isNFCEnabled();
  frmEKYCIdpDetailsList.flxArrow.isVisible = false;
  frmEKYCIdpDetailsList.flexNoNFC.isVisible = false;
  frmEKYCIdpDetailsList.btnUpdateIAL.isVisible = false;
  if (null !== nfcFlag && false !== nfcFlag) {
    frmEKYCIdpDetailsList.btnUpdateIAL.isVisible = true;
  }else{
    frmEKYCIdpDetailsList.flxArrow.isVisible = true;
    frmEKYCIdpDetailsList.flexNoNFC.isVisible = true;
  }
  loadEkycIdpList();
}
function frmEKYCIdpDetailsList_postShow_function(){
  dismissLoadingScreen();
  animateIdAndSelfieImages(frmEKYCIdpDetailsList.flxArrow, 45);
}
function onClickEKYCIdpDetailsSort(){
  if(gblRTPDetailsSortBy == 1){
    frmEKYCIdpDetailsList.imgExpDate.setVisibility(true);
    frmEKYCIdpDetailsList.imgReqType.setVisibility(false);
    frmEKYCIdpDetailsList.imgBankName.setVisibility(false);
    
    frmEKYCIdpDetailsList.flxExpDate.skin = flexLightGrey;
    frmEKYCIdpDetailsList.flxReqType.skin = slFbox;
	frmEKYCIdpDetailsList.flxBankName.skin = slFbox;
  }else if(gblRTPDetailsSortBy == 2){
    frmEKYCIdpDetailsList.imgExpDate.setVisibility(false);
    frmEKYCIdpDetailsList.imgReqType.setVisibility(true);
    frmEKYCIdpDetailsList.imgBankName.setVisibility(false);
    
    frmEKYCIdpDetailsList.flxExpDate.skin = slFbox;
    frmEKYCIdpDetailsList.flxReqType.skin = flexLightGrey;
	frmEKYCIdpDetailsList.flxBankName.skin = slFbox;
  }else if(gblRTPDetailsSortBy == 3){
    frmEKYCIdpDetailsList.imgExpDate.setVisibility(false);
    frmEKYCIdpDetailsList.imgReqType.setVisibility(false);
    frmEKYCIdpDetailsList.imgBankName.setVisibility(true);
    
    frmEKYCIdpDetailsList.flxExpDate.skin = slFbox;
    frmEKYCIdpDetailsList.flxReqType.skin = slFbox;
	frmEKYCIdpDetailsList.flxBankName.skin = flexLightGrey;
  }
  frmEKYCIdpDetailsList.flxSortPop.setVisibility(true);
}

function onCloseEKYCIdpSortPopup(){
  try{
    frmEKYCIdpDetailsList.flxSortPop.setVisibility(false);
  }catch(exp){

  }
}

function loadEkycIdpList(){
  frmEKYCIdpDetailsList.segEkycIdpDetails.removeAll();
  frmEKYCIdpDetailsList.flxNoRecordFound.setVisibility(false);
  frmEKYCIdpDetailsList.flxSortby.setVisibility(true);
  frmEKYCIdpDetailsList.flxLine5.setVisibility(true);
  frmEKYCIdpDetailsList.flxScrlSegEkycIdpDetails.setVisibility(true);
  var ekycSegData = [];
  if(gblIdpList != undefined){
    for(var i = 0; i < gblIdpList.length; i++ ){
      var imageLogo = "";
      var eventTypeName = "Loan";
      var eventName = "";
      var descriptionTxt = "";
      var skinLabel;
      var skinHeader;
      var skinRound;
      var fskin = segBgLightGry;
      if(gblIdpList[i].eventType == "account_opening"){
        imageLogo = "icon_03.png";
        eventTypeName = "Acc";
        eventName = kony.i18n.getLocalizedString("account_opening");
      }else{
        imageLogo = "icon_alert.png";
        eventTypeName = "Loan";
        eventName = "Unknown Action";
      }
      if(gblIdpList[i].status == "PENDING"){
        descriptionTxt = kony.i18n.getLocalizedString("eKYC_lbExpiryDate") + ". " + convertDateFromIDPResponse(gblIdpList[i].expirtyDate);
        skinHeader = lblBlueMed171;
        skinLabel = lblbluemedium160;
        skinRound = sknFlexRoundBdrblue;
      }else if(gblIdpList[i].status == "CONFIRMED"){
        descriptionTxt = kony.i18n.getLocalizedString("eKYC_lbApproved") + ". " + convertDateFromIDPResponse(gblIdpList[i].modifiedDate);
        skinHeader = lblGreenMed171;
        skinLabel = lblGreenemedium160;
        skinRound = sknFlexRoundBdrgreen;
        fskin = null;
      }else if(gblIdpList[i].status == "DENIED"){
        descriptionTxt = kony.i18n.getLocalizedString("eKYC_lbRejected") + ". " + convertDateFromIDPResponse(gblIdpList[i].modifiedDate);
        skinHeader = lblOrangeMed171;
        skinLabel = lblorangemedium160;
        skinRound = sknFlexRoundBdrorange;
        fskin = null;
      }else if(gblIdpList[i].status == "TIMEOUT"){
        descriptionTxt = kony.i18n.getLocalizedString("eKYC_lbExpired") + ". " + convertDateFromIDPResponse(gblIdpList[i].expirtyDate);
        skinHeader = lblLightGreyMed171;
        skinLabel = lblLightGreymedium160;
        skinRound = sknFlexRoundBdrgrey;
        fskin = null;
      }
      var tempData = {
        //imgIcon: {src: imageLogo},
        lblEventName: {text:eventName, skin:skinHeader },
        lblApplicationName: {text:gblIdpList[i].idpDiscription, skin:skinLabel },
        lblStatus:{text:descriptionTxt, skin:skinLabel },
        lblProductType:{text:eventTypeName, skin:skinLabel },
        flxRound:{skin:skinRound },
        flxBlock:{focusSkin:fskin }
      };
      ekycSegData.push(tempData);
    }
    if(ekycSegData.length == 0){
      frmEKYCIdpDetailsList.flxNoRecordFound.setVisibility(true);
      frmEKYCIdpDetailsList.flxSortby.setVisibility(false);
      frmEKYCIdpDetailsList.flxLine5.setVisibility(false);
      frmEKYCIdpDetailsList.flxScrlSegEkycIdpDetails.setVisibility(false);
    }else{
      frmEKYCIdpDetailsList.segEkycIdpDetails.setData(ekycSegData);
      frmEKYCIdpDetailsList.segEkycIdpDetails.onRowClick = frmEKYCIdpDetailsList_segEkycIdpDetails_selectedRowItems;
    }
  }else{
	frmEKYCIdpDetailsList.flxNoRecordFound.setVisibility(true);
    frmEKYCIdpDetailsList.flxSortby.setVisibility(false);
    frmEKYCIdpDetailsList.flxLine5.setVisibility(false);
    frmEKYCIdpDetailsList.flxScrlSegEkycIdpDetails.setVisibility(false);
  }
}
function onClickSortExpDate(){
  gblRTPDetailsSortBy = 1;
  frmEKYCIdpDetailsList.imgExpDate.setVisibility(true);
  frmEKYCIdpDetailsList.imgReqType.setVisibility(false);
  frmEKYCIdpDetailsList.imgBankName.setVisibility(false);
  frmEKYCIdpDetailsList.lblSortByVal.text = kony.i18n.getLocalizedString("eKYC_lbSortbyExp");
  frmEKYCIdpDetailsList.flxSortPop.setVisibility(false);
  loadEkycIdpListWithSortingModule("ExpDate");
}
function onClickSortReqType(){
  gblRTPDetailsSortBy = 2;
  frmEKYCIdpDetailsList.imgExpDate.setVisibility(false);
  frmEKYCIdpDetailsList.imgReqType.setVisibility(true);
  frmEKYCIdpDetailsList.imgBankName.setVisibility(false);
  frmEKYCIdpDetailsList.lblSortByVal.text = kony.i18n.getLocalizedString("eKYC_lbSortbyReq");
  frmEKYCIdpDetailsList.flxSortPop.setVisibility(false);
  loadEkycIdpListWithSortingModule("ReqType");
}
function onClickSortBankName(){
  gblRTPDetailsSortBy = 3;
  frmEKYCIdpDetailsList.imgExpDate.setVisibility(false);
  frmEKYCIdpDetailsList.imgReqType.setVisibility(false);
  frmEKYCIdpDetailsList.imgBankName.setVisibility(true);
  frmEKYCIdpDetailsList.lblSortByVal.text = kony.i18n.getLocalizedString("eKYC_lbSortbyBankName");
  frmEKYCIdpDetailsList.flxSortPop.setVisibility(false);
  loadEkycIdpListWithSortingModule("BankName");
}
function frmEKYCIdpDetailsList_segEkycIdpDetails_selectedRowItems(){
  var ekycIdpDetail =  gblIdpList[frmEKYCIdpDetailsList.segEkycIdpDetails.selectedIndex[1]];
  if(ekycIdpDetail.status == "PENDING"){
    var expected_ial = parseFloat(ekycIdpDetail.expectedIAL);
    var expected_aal = parseFloat(ekycIdpDetail.expectedAAL);
    var current_ial = parseFloat(gblCurrentIAL);
    if(!isNaN(current_ial)){
      current_ial = current_ial/100;
    }else{
      current_ial = 0;
    }
    if(current_ial >= expected_ial){
      if(expected_aal >= gblCurrentAAL){
        if(isAndroid()){
        gotoApproveDetail(ekycIdpDetail,true);
      }else{
        //alert("No support for Iphone")
      }
      }else{
        gotoApproveDetail(ekycIdpDetail,false);
      }
    }else{
      openUpgradeIALPopup(ekycIdpDetail);
    }
  }
}

function loadEkycIdpListWithSortingModule(sortBy){
  showLoadingScreen();
  var inputParams ={};
  inputParams["moduleName"] = "getAutRequestSettings";
  inputParams["sortBy"] = sortBy;
  invokeServiceSecureAsync("IDPApprovalFlow", inputParams, loadIDPApprovalFlowCallBack);
}

function convertDateFromIDPResponse(strDate){
  arrayDate = strDate.split(" ");
  var d = new Date(arrayDate[0] + " " + arrayDate[1] + " " + arrayDate[2] + " " + arrayDate[5]);
  var month = d.getMonth() + 1;
  return d.getDate().toString().padStart(2, '0') + "/" + month.toString().padStart(2, '0') + "/" + d.getFullYear();;
}

function updateIALViaListPage(){
  gblIALFlow = "frmEKYCIdpDetailsList";
  gblIALFailCount = 0;
  updateIAL();
}
