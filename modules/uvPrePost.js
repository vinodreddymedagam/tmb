
function frmUVActivateNowMenuPreshow() {
	frmUVActivateNowPreShow.call(this);
}

function frmUVActivateNowPreShow() {
  
	frmUVActivateNow.lblgetnotification.text=kony.i18n.getLocalizedString("UVI_txtTopic");
    frmUVActivateNow.lbMFSuitabilityTitle.text=kony.i18n.getLocalizedString("FIN02_txtTittle");
 	frmUVActivateNow.lblUsePine.text=kony.i18n.getLocalizedString("UVI_txtDesc");
  	frmUVActivateNow.btnDone.text=kony.i18n.getLocalizedString("UVI_btnActivate");
    //var locale = kony.i18n.getCurrentLocale(); 
     
}

function frmUVActivationSuccessfulMenuPreshow() {
        frmUVActivationSuccessful.onDeviceBack = doNothing;
        frmUVActivationSuccessfulPreShow.call(this);
}

function frmUVActivationSuccessfulPreShow() {
  	frmUVActivationSuccessful.lblCanuseTMBtouch.setVisibility(true);
  	if(gblActivationCurrentForm == "cardlessWithdraw" || gblActivationCurrentForm == "TMBConfirm"){
      frmUVActivationSuccessful.lbMFSuitabilityTitle.text=kony.i18n.getLocalizedString("TCF_txtTittle");
      frmUVActivationSuccessful.lblActivationSucc.text=kony.i18n.getLocalizedString("TCF_txtTopic");
      frmUVActivationSuccessful.lblCanuseTMBtouch.setVisibility(false);
      frmUVActivationSuccessful.btnDone.text=kony.i18n.getLocalizedString("FIN02_btnNext");
    }else{
      frmUVActivationSuccessful.lbMFSuitabilityTitle.text=kony.i18n.getLocalizedString("FIN01_txtTittle");
      frmUVActivationSuccessful.lblActivationSucc.text=kony.i18n.getLocalizedString("FIN01_txtTopic");
      frmUVActivationSuccessful.lblCanuseTMBtouch.text=kony.i18n.getLocalizedString("FIN01_txtDes");
      frmUVActivationSuccessful.btnDone.text=kony.i18n.getLocalizedString("FIN01_btnStart");
    }
}

function frmUVConfirmHomeMenuPreshow() {
	frmUVConfirmHomePreShow.call(this);
}

function frmUVConfirmHomePreShow() {
  
	frmUVConfirmHome.lbMFSuitabilityTitle.text=kony.i18n.getLocalizedString("FIN02_txtTittle");
    frmUVConfirmHome.lblTmbConfDesc.text=kony.i18n.getLocalizedString("FIN02_txtTopic");
 	frmUVConfirmHome.lblnorequest.text=kony.i18n.getLocalizedString("UVI_msgNoRequest");
  	frmUVConfirmHome.lblDetailDateDesc.text=kony.i18n.getLocalizedString("UVL_txtInform");
    frmUVConfirmHome.lblDesc1.text=kony.i18n.getLocalizedString("UVL_txtDesc");
    //var locale = kony.i18n.getCurrentLocale(); 
     
}

function navigateTofrmActiavtionAttention(){
  	isFromActivateNow=true;
  //alert("isFromActivateNow"+JSON.stringify(isFromActivateNow));
   	frmActivationAttention.show();
}

function showfrmUVConfirmHome(){
  if(gblActivationCurrentForm == "cardlessWithdraw"){
    showCardLessWithDrawLanding();
  }else{
    callUVPushTxnHistory();
  }
}

function showfrmUVActivationSuccessful(){
  
  frmUVActivationSuccessful.show();
}

