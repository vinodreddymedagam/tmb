//SaveAlert Schedule Save.
function showfrmSend2SaveSchedule(context){
  try{
    isSaveAlertAutoFocus = true;
    kony.print("@@@ In showfrmSend2SaveSchedule() @@@");
    var widgetId = context.id;
    kony.print("@@@ widgetId:::"+widgetId);
    frmSend2SaveSchedule.calStartDate.validStartDate = currentDateForScheduleCalender();
    if(gblSavelAlertMode == "normal"){
      kony.print("@@@currentDateForScheduleCalender():::"+currentDateForScheduleCalender());
      kony.print("@@@Duedate::"+frmSend2SaveCreateSmartRequest.lblDueDateValue.text);
      if(isNotBlank(widgetId) && widgetId == "flxDueDate"){
        frmSend2SaveSchedule.calStartDate.dateComponents = currentDateForScheduleCalender();
        saveAlertSelectedNoOfDays("btnWhiteBGMF", "btnWhiteBGMF", "btnWhiteBGMF", "btnWhiteBGMF", "btnWhiteBGMF", "btnWhiteBGMF");
        setSaveAlertScheduleToDefaults();
      }
    }else {
      var selectedRow = frmSend2SaveDashboard.segSend2SaveDashboardDetails.selectedRowItems[0];
      if(isNotBlank(selectedRow)){
        kony.print("@@@ ExpiryDate::"+ selectedRow["ExpiryDate"]+"|@@@Repeat OneTime:::"+selectedRow["isOneTime"]);
        if(selectedRow["isOneTime"] == "Y"){
          frmSend2SaveSchedule.txtRepeatValue.text="";   
          frmSend2SaveSchedule.flxRecuringTimes.setVisibility(false);
          frmSend2SaveSchedule.calStartDate.dateComponents = currentDateForScheduleCalender(yyymmddToddmmyyyySaveAlert(selectedRow["ExpiryDate"])) ;	  
          frmSend2SaveSchedule.btnOneTime.skin = "btnBlueBGMF";
          frmSend2SaveSchedule.btnMonthly.skin = "btnWhiteBGMF";
          gblS2SRepeatType = "OneTime";
        }else{
          frmSend2SaveSchedule.flxRecuringTimes.setVisibility(true);
          frmSend2SaveSchedule.txtRepeatValue.text = selectedRow["RepeatTimes"];
          frmSend2SaveSchedule.txtRepeatValue.setVisibility(true);
          frmSend2SaveSchedule.calStartDate.dateComponents = currentDateForScheduleCalender(yyymmddToddmmyyyySaveAlert(selectedRow["NextDueDt"])) ;	  
          frmSend2SaveSchedule.btnOneTime.skin = "btnWhiteBGMF";
          frmSend2SaveSchedule.btnMonthly.skin = "btnBlueBGMF";
          gblS2SRepeatType = "Monthly";
        }
      }
      gblS2SSendRequestType = gblSaveAlertExistValues.ExpireDays;
      populateDaysBefore(gblSaveAlertExistValues.ExpireDays);
    }
    gblS2SSendRequestText = "";
    frmSend2SaveSchedule.show();
  }catch(e){
    kony.print("@@@ In showfrmSend2SaveSchedule() Exception:::"+e);
  }
}

function populateDaysBefore(daysBefore){
  switch(daysBefore){
    case "Today":saveAlertSelectedNoOfDays("btnBlueBGMF", "btnWhiteBGMF", "btnWhiteBGMF", "btnWhiteBGMF", "btnWhiteBGMF", "btnWhiteBGMF");  break; 
    case "1":saveAlertSelectedNoOfDays("btnWhiteBGMF", "btnBlueBGMF", "btnWhiteBGMF", "btnWhiteBGMF", "btnWhiteBGMF", "btnWhiteBGMF");  break; 
    case "3":saveAlertSelectedNoOfDays("btnWhiteBGMF", "btnWhiteBGMF", "btnBlueBGMF", "btnWhiteBGMF", "btnWhiteBGMF", "btnWhiteBGMF");  break; 
    case "7":saveAlertSelectedNoOfDays("btnWhiteBGMF", "btnWhiteBGMF", "btnWhiteBGMF", "btnBlueBGMF", "btnWhiteBGMF", "btnWhiteBGMF");  break;
    case "15":saveAlertSelectedNoOfDays("btnWhiteBGMF", "btnWhiteBGMF", "btnWhiteBGMF", "btnWhiteBGMF", "btnBlueBGMF", "btnWhiteBGMF");  break;
    case "30":saveAlertSelectedNoOfDays("btnWhiteBGMF", "btnWhiteBGMF", "btnWhiteBGMF", "btnWhiteBGMF", "btnWhiteBGMF", "btnBlueBGMF");  break;
  }
}

function frmSend2SaveScheduleInit(){
  try{
    kony.print("@@@ In frmSend2SaveScheduleInit() @@@");
    frmSend2SaveSchedule.preShow = frmSend2SaveSchedulePreshow;
    frmSend2SaveScheduleActions();
  }catch(e){
    kony.print("@@@ In frmSend2SaveScheduleInit() Exception:::"+e);
  }
}

function frmSend2SaveSchedulePreshow() {
  try{
    frmSend2SaveSchedule.lblScheduleHeader.text = kony.i18n.getLocalizedString("MB_S2SScheduleTitle");
    frmSend2SaveSchedule.lblDueDate.text = kony.i18n.getLocalizedString("MB_S2SDuedateLab2");
    frmSend2SaveSchedule.lblSendRequest.text = kony.i18n.getLocalizedString("MB_S2SSendRequestDateLab");
    frmSend2SaveSchedule.btn1Day.text = kony.i18n.getLocalizedString("S2S1Day");
    frmSend2SaveSchedule.btnToday.text = kony.i18n.getLocalizedString("S2SToday");
    frmSend2SaveSchedule.btn3Day.text = kony.i18n.getLocalizedString("S2S3Days");
    frmSend2SaveSchedule.btn7Day.text = kony.i18n.getLocalizedString("S2S7Days");
    frmSend2SaveSchedule.btn15Day.text = kony.i18n.getLocalizedString("S2S15Days");
    frmSend2SaveSchedule.btn30Day.text = kony.i18n.getLocalizedString("S2S30Days");
    frmSend2SaveSchedule.lblRepeat.text = kony.i18n.getLocalizedString("MB_S2SRepeatDateLab");
    frmSend2SaveSchedule.btnOneTime.text = kony.i18n.getLocalizedString("MB_S2SRepeat1Time");
    frmSend2SaveSchedule.btnMonthly.text = kony.i18n.getLocalizedString("MB_S2SRepeatMth");
    frmSend2SaveSchedule.lblTimes.text = kony.i18n.getLocalizedString("MB_S2SRecurringTimeLab");
    frmSend2SaveSchedule.btnSave.text = kony.i18n.getLocalizedString("keysave");
    frmSend2SaveSchedule.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
  }catch(e){
    kony.print("@@@ In frmSend2SaveSchedulePreshow() Exception:::"+e);
  }
}

function frmSend2SaveScheduleActions() {
  try{
    kony.print("@@@ In frmSend2SaveScheduleActions() @@@");
    if (gblDeviceInfo["name"] == "android"){
      frmSend2SaveSchedule.onDeviceBack = disableBackButton;
    }
    frmSend2SaveSchedule.btnBackArrow.onClick = backTofrmSend2SaveCreateSmartRequest;
    
    frmSend2SaveSchedule.btnToday.onClick = onClickSelectNoOfDays;
    frmSend2SaveSchedule.btn1Day.onClick = onClickSelectNoOfDays;
    frmSend2SaveSchedule.btn3Day.onClick = onClickSelectNoOfDays;
    frmSend2SaveSchedule.btn7Day.onClick = onClickSelectNoOfDays;
    frmSend2SaveSchedule.btn15Day.onClick = onClickSelectNoOfDays;
    frmSend2SaveSchedule.btn30Day.onClick = onClickSelectNoOfDays;
    
    frmSend2SaveSchedule.btnOneTime.onClick = onClickSelectNoOfDays;
    frmSend2SaveSchedule.btnMonthly.onClick = onClickSelectNoOfDays;
    
    frmSend2SaveSchedule.btnSave.onClick = onClickSaveSceduleS2S;
    frmSend2SaveSchedule.btnCancel.onClick = backTofrmSend2SaveCreateSmartRequest;
    
    frmSend2SaveSchedule.calStartDate.onSelection = onSelectDueDate;
  }catch(e){
    kony.print("@@@ In frmSend2SaveScheduleActions() Exception:::" + e);
  }
}

function onSelectDueDate(){
  try{
    kony.print("@@@ In onSelectDueDate() @@@");
    var selDate = frmSend2SaveSchedule.calStartDate.formattedDate;
    if(isNotBlank(selDate)){
      var splitSelDate = selDate.split("/");
      selDate = splitSelDate[1]+"/"+splitSelDate[0]+"/"+splitSelDate[2];
    }
    var today = formateDateMMDDYYYY(new Date());
    var noOfDays = getDaysDiffrence(today, selDate);
    gblNoofDays = noOfDays;
    var selRequestDate = "";
    if(gblS2SSendRequestType == "Today"){
      selRequestDate = 0;
    }else{
      selRequestDate = gblS2SSendRequestType;
    }
  
    kony.print("@@@@ noOfDays:::"+noOfDays+"|selRequestDate::"+selRequestDate+"|gblSaveAlertAcceptMonthly::"+gblSaveAlertAcceptMonthly);
    if(isNotBlank(noOfDays) && (parseInt(noOfDays) == 0) || parseInt(noOfDays) < parseInt(selRequestDate)){
      kony.print("---- Resetting the values to defaults ----");
      saveAlertSelectedNoOfDays("btnBlueBGMF", "btnWhiteBGMF", "btnWhiteBGMF", "btnWhiteBGMF", "btnWhiteBGMF", "btnWhiteBGMF");
      setSaveAlertScheduleToDefaults();
      frmSend2SaveSchedule.lblSendRequest.text = kony.i18n.getLocalizedString("MB_S2SSendRequestDateText_Today").replace("{Send request on value}", kony.i18n.getLocalizedString("S2SToday"));
      gblS2SSendRequestType = "Today";
    }
    if(isNotBlank(noOfDays) && (parseInt(noOfDays) == 1 || parseInt(noOfDays) == parseInt(selRequestDate) )){
      kony.print("--- Not allow Monthly ----");
      hideSaveAlertMontlyRepeat();
      gblSaveAlertAcceptMonthly = true;
    }else{
      gblSaveAlertAcceptMonthly = false;
    }
  }catch(e){
    kony.print("@@@ In onSelectDueDate() Exception:::"+e);
  }
}

function setSaveAlertScheduleToDefaults(){
  try{
    kony.print("@@@ In setSaveAlertScheduleToDefaults() @@@");
    frmSend2SaveSchedule.lblSendRequest.text = kony.i18n.getLocalizedString("MB_S2SSendRequestDateLab");
    gblS2SSendRequestType = "";
    gblS2SRepeatType = "OneTime";
    gblSaveAlertAcceptMonthly = false;
    hideSaveAlertMontlyRepeat();
  }catch(e){
    kony.print("@@@ In setSaveAlertScheduleToDefaults() Exception:::"+e);
  }
}

function onClickSelectNoOfDays(widgetId) {
  try{
    kony.print("@@@ In onClickSelectNoOfDays() @@@");
    kony.print("@@@ WidgetID:::"+widgetId.id);
    kony.print("@@frmSend2SaveSchedule.calStartDate.formattedDate:::N:::"+frmSend2SaveSchedule.calStartDate.formattedDate);
    var selDate = frmSend2SaveSchedule.calStartDate.formattedDate;
    if(isNotBlank(selDate)){
      var splitSelDate = selDate.split("/");
      selDate = splitSelDate[1]+"/"+splitSelDate[0]+"/"+splitSelDate[2];
      selDate = getFormattedDate(selDate, kony.i18n.getCurrentLocale());
    }
    var today = formateDateMMDDYYYY(new Date());
    var noOfDays = getDaysDiffrence(today, selDate);
    gblNoofDays = noOfDays;
    kony.print("@@@ currentDate:::"+today+"|||@@@ Selected Date:::"+selDate);
    kony.print("@@@@ No.of DiffDays:::"+noOfDays);
    if(isNotBlank(widgetId.id)){
      if(isNotBlank(noOfDays)){
        if(widgetId.id == "btnToday"){
          if(parseInt(noOfDays) >= 0){
            gblS2SSendRequestType = "Today";
            gblS2SSendRequestText = "S2SToday";
            saveAlertSelectedNoOfDays("btnBlueBGMF", "btnWhiteBGMF", "btnWhiteBGMF", "btnWhiteBGMF", "btnWhiteBGMF", "btnWhiteBGMF");
            gblS2SRepeatType = "OneTime";
            hideSaveAlertMontlyRepeat();
            gblSaveAlertAcceptMonthly = true;
          }else{
            showAlert(kony.i18n.getLocalizedString("MB_S2SDateNotMatch"), kony.i18n.getLocalizedString("info"));
          }
        }else if(widgetId.id == "btn1Day"){
          if(parseInt(noOfDays) == 1){
            gblSaveAlertAcceptMonthly = true;
          }else{
            gblSaveAlertAcceptMonthly = false;
          }
            
          if(parseInt(noOfDays) >= 1){
            gblS2SSendRequestType = "1";
            gblS2SSendRequestText = "S2S1Day";
          	saveAlertSelectedNoOfDays("btnWhiteBGMF", "btnBlueBGMF", "btnWhiteBGMF", "btnWhiteBGMF", "btnWhiteBGMF", "btnWhiteBGMF");
          }else{
            showAlert(kony.i18n.getLocalizedString("MB_S2SDateNotMatch"), kony.i18n.getLocalizedString("info"));
          }
        }else if(widgetId.id == "btn3Day"){
          if(parseInt(noOfDays) == 3){
            gblSaveAlertAcceptMonthly = true;
          }else{
            gblSaveAlertAcceptMonthly = false;
          }
          if(parseInt(noOfDays) >= 3){
            gblS2SSendRequestType = "3";
            gblS2SSendRequestText = "S2S3Days";
            saveAlertSelectedNoOfDays("btnWhiteBGMF", "btnWhiteBGMF", "btnBlueBGMF", "btnWhiteBGMF", "btnWhiteBGMF", "btnWhiteBGMF");
          }else{
            showAlert(kony.i18n.getLocalizedString("MB_S2SDateNotMatch"), kony.i18n.getLocalizedString("info"));
          }
        }else if(widgetId.id == "btn7Day"){
          if(parseInt(noOfDays) == 7){
            gblSaveAlertAcceptMonthly = true;
          }else{
            gblSaveAlertAcceptMonthly = false;
          }
          if(parseInt(noOfDays) >= 7){
            gblS2SSendRequestType = "7";
            gblS2SSendRequestText = "S2S7Days";
            saveAlertSelectedNoOfDays("btnWhiteBGMF", "btnWhiteBGMF", "btnWhiteBGMF", "btnBlueBGMF", "btnWhiteBGMF", "btnWhiteBGMF");
          }else{
            showAlert(kony.i18n.getLocalizedString("MB_S2SDateNotMatch"), kony.i18n.getLocalizedString("info"));
          }
        }else if(widgetId.id == "btn15Day"){
          if(parseInt(noOfDays) == 15){
            gblSaveAlertAcceptMonthly = true;
          }else{
            gblSaveAlertAcceptMonthly = false;
          }
          if(parseInt(noOfDays) >= 15){
            gblS2SSendRequestType = "15";
            gblS2SSendRequestText = "S2S15Days";
            saveAlertSelectedNoOfDays("btnWhiteBGMF", "btnWhiteBGMF", "btnWhiteBGMF", "btnWhiteBGMF", "btnBlueBGMF", "btnWhiteBGMF");
          }else{
            showAlert(kony.i18n.getLocalizedString("MB_S2SDateNotMatch"), kony.i18n.getLocalizedString("info"));
          }
        }else if(widgetId.id == "btn30Day"){
          if(parseInt(noOfDays) == 30){
            gblSaveAlertAcceptMonthly = true;
          }else{
            gblSaveAlertAcceptMonthly = false;
          }
          if(parseInt(noOfDays) >= 30){
            gblS2SSendRequestType = "30";
            gblS2SSendRequestText = "S2S30Days";
            saveAlertSelectedNoOfDays("btnWhiteBGMF", "btnWhiteBGMF", "btnWhiteBGMF", "btnWhiteBGMF", "btnWhiteBGMF", "btnBlueBGMF");
          }else{
            showAlert(kony.i18n.getLocalizedString("MB_S2SDateNotMatch"), kony.i18n.getLocalizedString("info"));
          }
        }
      }
      if(widgetId.id != "btnOneTime" || widgetId.id != "btnMonthly" ){
        var sendReqDateText = kony.i18n.getLocalizedString("MB_S2SSendRequestDateText");
        if(isNotBlank(gblS2SSendRequestText) && gblS2SSendRequestText != "S2SToday"){
          sendReqDateText = sendReqDateText.replace("{Send request on value}", kony.i18n.getLocalizedString(gblS2SSendRequestText));
        }else if(gblS2SSendRequestText == "S2SToday"){
          sendReqDateText = kony.i18n.getLocalizedString("MB_S2SSendRequestDateText_Today").replace("{Send request on value}", kony.i18n.getLocalizedString("S2SToday"));
        }else{
          sendReqDateText = kony.i18n.getLocalizedString("MB_S2SSendRequestDateLab"); //sendReqDateText.replace("{Send request on value}", kony.i18n.getLocalizedString("MB_S2SSendRequestDateLab"));
        }
        frmSend2SaveSchedule.lblSendRequest.text = sendReqDateText;
      }
      
      kony.print("@@@gblS2SSendRequestType:::"+gblS2SSendRequestType+"||@@gblSaveAlertAcceptMonthly:::"+gblSaveAlertAcceptMonthly);
      if(gblSaveAlertAcceptMonthly){
        hideSaveAlertMontlyRepeat();
      }
      if(widgetId.id == "btnOneTime"){
        gblS2SRepeatType = "OneTime";
        hideSaveAlertMontlyRepeat();
      }else if(widgetId.id == "btnMonthly"){
        if(!isNotBlank(gblS2SSendRequestType) || gblS2SSendRequestType == "Today" || gblSaveAlertAcceptMonthly){
          showAlert(kony.i18n.getLocalizedString("Schedule_TodayErr"), kony.i18n.getLocalizedString("info"));
          hideSaveAlertMontlyRepeat();
        }else{
          gblS2SRepeatType = "Monthly";
          frmSend2SaveSchedule.btnOneTime.skin = "btnWhiteBGMF";
          frmSend2SaveSchedule.btnMonthly.skin = "btnBlueBGMF";
          frmSend2SaveSchedule.flxRecuringTimes.setVisibility(true);
          frmSend2SaveSchedule.txtRepeatValue.setFocus(true);
        }
      }
    }
  }catch(e){
    kony.print("@@@ In onClickSelectNoOfDays() Exception:::" + e);
  }
}

function hideSaveAlertMontlyRepeat(){
  frmSend2SaveSchedule.btnOneTime.skin = "btnBlueBGMF";
  frmSend2SaveSchedule.btnMonthly.skin = "btnWhiteBGMF";
  frmSend2SaveSchedule.flxRecuringTimes.setVisibility(false);
  frmSend2SaveSchedule.txtRepeatValue.text = "";
}

function getDaysDiffrence(firstDate, secondDate){
  try{
    if(isNotBlank(firstDate) && isNotBlank(secondDate)){
      var date1 = new Date(firstDate);
      var date2 = new Date(secondDate);
      var timeDiff = Math.abs(date1.getTime() - date2.getTime());
      var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
      return diffDays;
    }else{
      return "";
    }
  }catch(e){
    kony.print("@@@ In getDaysDiffrence() Exception:::"+e);
  }
}

function formateDateMMDDYYYY(formateDate){
  try{
    if(isNotBlank(formateDate) ){
      formateDate = new Date(formateDate);
      var month = formateDate .getMonth() + 1;
      var day = formateDate .getDate();
      var year = formateDate .getFullYear();
      return  month + "/" + day + "/" + year;
    }else{
      return "";
    }
  }catch(e){
    kony.print("@@@ In formateDateMMDDYYYY() Exception:::"+e);
  }
}

function saveAlertSelectedNoOfDays(today, oneDay, threeDays, sevenDays, fifteenDays, thirtyDays) {
  try{
   	frmSend2SaveSchedule.btnToday.skin = today;
    frmSend2SaveSchedule.btn1Day.skin = oneDay;
    frmSend2SaveSchedule.btn3Day.skin = threeDays;
    frmSend2SaveSchedule.btn7Day.skin = sevenDays;
    frmSend2SaveSchedule.btn15Day.skin = fifteenDays;
    frmSend2SaveSchedule.btn30Day.skin = thirtyDays;
  }catch(e){
    kony.print("@@@ In saveAlertSelectedNoOfDays() Exception:::" + e);
  }
}

function onClickSaveSceduleS2S(){
  try{
    kony.print("@@@ In onClickSaveSceduleS2S() @@@");
    
    var selectedDate = frmSend2SaveSchedule.calStartDate.formattedDate;
	gblSelectedDateSA = selectedDate;
    var repeatValue = frmSend2SaveSchedule.txtRepeatValue.text;
    kony.print("@@@selectedDate:::"+selectedDate+"|gblS2SRepeatType:::"+gblS2SRepeatType+"|repeatValue::"+repeatValue);
    
    if(!isNotBlank(gblS2SSendRequestType)){
      showAlert(kony.i18n.getLocalizedString("MIB_S2SNoRequestDate"), kony.i18n.getLocalizedString("info"));
      return;
    }
    
    if(isNotBlank(gblS2SRepeatType) && gblS2SRepeatType == "Monthly" && gblS2SSendRequestType == "Today"){
      showAlert(kony.i18n.getLocalizedString("Schedule_TodayErr"), kony.i18n.getLocalizedString("info"));
      gblS2SRepeatType = "OneTime";
      frmSend2SaveSchedule.btnOneTime.skin = "btnBlueBGMF";
      frmSend2SaveSchedule.btnMonthly.skin = "btnWhiteBGMF";
      return;
    }
    kony.print("@@MIN:::"+GBL_SAVEALERT_SCHEDULE_MONTHLY_REPEAT_MIN+"|MAX::"+GBL_SAVEALERT_SCHEDULE_MONTHLY_REPEAT_MAX);
    if(gblS2SRepeatType == "Monthly" && !isNotBlank(repeatValue)){
      showAlert(kony.i18n.getLocalizedString("Error_EnterNumOfTimes"), kony.i18n.getLocalizedString("info"));
    }else{
      if(gblS2SRepeatType == "Monthly"){
        if(isNotBlank(repeatValue) && (repeatValue.trim() < GBL_SAVEALERT_SCHEDULE_MONTHLY_REPEAT_MIN && repeatValue.trim() >= GBL_SAVEALERT_SCHEDULE_MONTHLY_REPEAT_MAX)){
          var repeaErrMsg = kony.i18n.getLocalizedString("MIB_S2SRecurringCheck");
          repeaErrMsg = repeaErrMsg.replace("{minimum times}", GBL_SAVEALERT_SCHEDULE_MONTHLY_REPEAT_MIN);
          repeaErrMsg = repeaErrMsg.replace("{maximum times}", GBL_SAVEALERT_SCHEDULE_MONTHLY_REPEAT_MAX);
          showAlert(repeaErrMsg, kony.i18n.getLocalizedString("info"));
          return;
        }
        frmSend2SaveCreateSmartRequest.flxRepeat.setVisibility(true);
		frmSend2SaveCreateSmartRequest.lblRepeatTimes.text = repeatValue;
        frmSend2SaveCreateSmartRequest.lblRepeat.setVisibility(true)
        frmSend2SaveCreateSmartRequest.lblRepeatValue.setVisibility(true); 
        frmSend2SaveCreateSmartRequest.lblRepeatValue.text = repeatValue +" "+ kony.i18n.getLocalizedString("keyTimesMB");
      }else{
        frmSend2SaveCreateSmartRequest.flxRepeat.setVisibility(false);
      }
      showOrHideSaveAlertDueDate(false);
      frmSend2SaveCreateSmartRequest.lblDueDateValue.text = changeDateFormatForSaveAlert(selectedDate);
      frmSend2SaveCreateSmartRequest.show();
    }
  }catch(e){
    kony.print("@@@ In onClickSaveSceduleS2S() Exception:::" + e);
  }
}

function showfrmSend2SaveCreateSmartRequest(){
  try{
    kony.print("@@@ In showfrmSend2SaveCreateSmartRequest() @@@");
    showOrHideSaveAlertDueDate(true);
    showOrHideSaveAlertPromptPayTextBox(true);
    frmSend2SaveCreateSmartRequest.txtValuePromptPayID.setFocus(true);
	frmSend2SaveCreateSmartRequest.txtValuePromptPayID.text = "";
    frmSend2SaveCreateSmartRequest.txtRequestSubject.text = "";
    frmSend2SaveCreateSmartRequest.txtAmount.text ="";
    frmSend2SaveCreateSmartRequest.lblReceivedAccountValue.text ="";
    frmSend2SaveCreateSmartRequest.show();
  }catch(e){
    kony.print("@@@ In showfrmSend2SaveCreateSmartRequest() Exception:::" + e);
  }
}


function backTofrmSend2SaveCreateSmartRequest(){
  try{
    kony.print("@@@ In backTofrmSend2SaveCreateSmartRequest() @@@");
    frmSend2SaveCreateSmartRequest.show();
  }catch(e){
    kony.print("@@@ In backTofrmSend2SaveCreateSmartRequest() Exception:::" + e);
  }
}
