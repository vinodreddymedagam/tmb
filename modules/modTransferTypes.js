//Type your code here
gblShowCoverFlow = false;
gblTrasferFrmAcntSummary = false; // global variable to identify transfer shotcut flow from account summary.
/**
 * @function
 *
 */
function onClickToAccount(){
  gblTrasORFT = 0;
  gblTransSMART = 0;
  gblSelTransferMode = 1;
  gblShowCoverFlow = true;   
  setSelTabBankAccountOrMobile();
  frmIBTranferLP.btnXferShowMobileContact.setVisibility(true);
  var currentForm = kony.application.getCurrentForm().id;
  if(kony.application.getCurrentForm().id == "frmIBTranferLP"){
     clearValueIBTransferLP();
     setDateOnly();
     frmIBTranferLP.lblSelectTransfer.text = popIBTransferOptions.lblToAccount.text;
  	 frmIBTranferLP.imgTransferType.setVisibility(true);    
 	 frmIBTranferLP.imgTransferType.src = popIBTransferOptions.imgToAcct.src; 
     clearValueHbxToLP();
     clearValuesfrmIBTranferLP();
     closeRightPanelTransfer();
  }else{
     frmIBTransferCustomWidgetLP.lblSelectTransfer.text = popIBTransferOptions.lblToAccount.text;
  	 frmIBTransferCustomWidgetLP.imgTransferType.setVisibility(true);
 	 frmIBTransferCustomWidgetLP.imgTransferType.src = popIBTransferOptions.imgToAcct.src;
     clearValueHbxToWidgetLP();
  } 
   displayBtnSchedule(currentForm, true);
   displayHbxNotifyRecipent(currentForm, true);
   popIBTransferOptions.dismiss();
  
  if(gblTrasferFrmAcntSummary && promptPayAccountFlag) {
    	showFromAccountsfrmIBTranferLP(true);
  } else {
     	onClickBankAccountNumberIB(); 
  }
  

}

/**
 * @function
 *
 */
function onClickToMobile(){
  gblTrasORFT = 0;
  gblTransSMART = 0;
  gblSelTransferMode = 2;
  gblXferPhoneNo = "";
  gblXferEmail = "";
  gblShowCoverFlow = true;
  frmIBTranferLP.txtXferMobileNumber.maxTextLength = 12;
  frmIBTranferLP.txtXferMobileNumber.placeholder = kony.i18n.getLocalizedString("MIB_P2PTREnter_MobNo");
  setSelTabBankAccountOrMobile();
  frmIBTranferLP.btnXferShowMobileContact.setVisibility(true);
  var currentForm = kony.application.getCurrentForm().id;
  if(kony.application.getCurrentForm().id == "frmIBTranferLP"){
     clearValueIBTransferLP();
     setDateOnly();
     frmIBTranferLP.lblSelectTransfer.text = popIBTransferOptions.lblToMobile.text;
  	 frmIBTranferLP.imgTransferType.setVisibility(true);     
 	 frmIBTranferLP.imgTransferType.src = popIBTransferOptions.imgToMobile.src;
    clearValueHbxToLP();
    clearValuesfrmIBTranferLP();
    closeRightPanelTransfer();
  }else{
    clearValueHbxToWidgetLP();
    frmIBTransferCustomWidgetLP.lblSelectTransfer.text = popIBTransferOptions.lblToMobile.text;
    frmIBTransferCustomWidgetLP.imgTransferType.setVisibility(true);
    frmIBTransferCustomWidgetLP.imgTransferType.src = popIBTransferOptions.imgToMobile.src;
  }
  displayBtnSchedule(currentForm, false);
  displayHbxNotifyRecipent(currentForm, false);
  popIBTransferOptions.dismiss(); 
  if(gblTrasferFrmAcntSummary && promptPayAccountFlag) {
    	showFromAccountsfrmIBTranferLP(true);
  } else {
  		onClickOnUsMobileNumberIB();
  }
}

/**
 * @function
 *
 */
function onClickToCitizenID(){
  gblTrasORFT = 0;
  gblTransSMART = 0;
  gblSelTransferMode = 3;
  gblXferPhoneNo = "";
  gblXferEmail = "";
  gblShowCoverFlow = true; 
  setSelTabBankAccountOrMobile();
  frmIBTranferLP.btnXferShowMobileContact.setVisibility(false);
  var currentForm = kony.application.getCurrentForm().id;
  frmIBTranferLP.txtXferMobileNumber.maxTextLength = 17;
  frmIBTranferLP.txtXferMobileNumber.placeholder = kony.i18n.getLocalizedString("MIB_P2PTREnter_CI");
  if(kony.application.getCurrentForm().id == "frmIBTranferLP"){
    clearValueIBTransferLP();
    setDateOnly();
    clearValueHbxToLP();
    frmIBTranferLP.lblSelectTransfer.text = popIBTransferOptions.lblToID.text;
    frmIBTranferLP.imgTransferType.setVisibility(true);    
    frmIBTranferLP.imgTransferType.src = popIBTransferOptions.imgToID.src; 
    clearValuesfrmIBTranferLP();
    closeRightPanelTransfer();
  }else{
    clearValueHbxToWidgetLP();
    frmIBTransferCustomWidgetLP.lblSelectTransfer.text = popIBTransferOptions.lblToID.text;
    frmIBTransferCustomWidgetLP.imgTransferType.setVisibility(true);
    frmIBTransferCustomWidgetLP.imgTransferType.src = popIBTransferOptions.imgToID.src;     
  }
  
  displayBtnSchedule(currentForm, false);
  displayHbxNotifyRecipent(currentForm, true);
  popIBTransferOptions.dismiss();  
  
  if(gblTrasferFrmAcntSummary && promptPayAccountFlag) {
    	showFromAccountsfrmIBTranferLP(true);
  } else {
   		onClickOnUsMobileNumberIB();
  }
}

/**
 * @function
 *
 */
function onClickToTaxID(){
  gblTrasORFT = 0;
  gblTransSMART = 0;
  gblSelTransferMode = 4;
  gblXferPhoneNo = "";
  gblXferEmail = "";
  gblShowCoverFlow = true;
  setSelTabBankAccountOrMobile();
   frmIBTranferLP.btnXferShowMobileContact.setVisibility(false);
   var currentForm = kony.application.getCurrentForm().id;
  frmIBTranferLP.txtXferMobileNumber.maxTextLength = 17;
  frmIBTranferLP.txtXferMobileNumber.placeholder = kony.i18n.getLocalizedString("MIB_P2PTREnter_Tax");
  if(kony.application.getCurrentForm().id == "frmIBTranferLP"){
    clearValueIBTransferLP();
    setDateOnly();
    clearValueHbxToLP();
    frmIBTranferLP.lblSelectTransfer.text = popIBTransferOptions.lblTaxID.text;
    frmIBTranferLP.imgTransferType.setVisibility(true);    
    frmIBTranferLP.imgTransferType.src = popIBTransferOptions.imgTaxID.src;
    clearValuesfrmIBTranferLP();
    closeRightPanelTransfer();
  }else{
    clearValueHbxToWidgetLP();
    frmIBTransferCustomWidgetLP.lblSelectTransfer.text = popIBTransferOptions.lblTaxID.text;
    frmIBTransferCustomWidgetLP.imgTransferType.setVisibility(true);
    frmIBTransferCustomWidgetLP.imgTransferType.src = popIBTransferOptions.imgTaxID.src;
  }
  displayBtnSchedule(currentForm, false);
  displayHbxNotifyRecipent(currentForm, true);
  popIBTransferOptions.dismiss();  
  
    if(gblTrasferFrmAcntSummary && promptPayAccountFlag) {
    	showFromAccountsfrmIBTranferLP(true);
  } else {
  		onClickOnUsMobileNumberIB(); 
  }
}

/**
 * @function
 *
 */
function onClickToEWalletID(){
  gblTrasORFT = 0;
  gblTransSMART = 0;
  gblSelTransferMode = 5;
  gblXferPhoneNo = "";
  gblXferEmail = "";
  gblShowCoverFlow = true; 
  setSelTabBankAccountOrMobile();
  frmIBTranferLP.btnXferShowMobileContact.setVisibility(false);
  var currentForm = kony.application.getCurrentForm().id;
  frmIBTranferLP.txtXferMobileNumber.maxTextLength = 16;
  frmIBTranferLP.txtXferMobileNumber.placeholder = kony.i18n.getLocalizedString("MIB_P2PTREnter_eWal");
   if(kony.application.getCurrentForm().id == "frmIBTranferLP"){
     clearValueIBTransferLP();
     setDateOnly();
     clearValueHbxToLP();
     frmIBTranferLP.lblSelectTransfer.text = popIBTransferOptions.lblEwallletID.text;
     frmIBTranferLP.imgTransferType.setVisibility(true);     
     frmIBTranferLP.imgTransferType.src = popIBTransferOptions.imgWallteID.src;
     clearValuesfrmIBTranferLP();
     closeRightPanelTransfer();
  }else{
    clearValueHbxToWidgetLP();
     frmIBTransferCustomWidgetLP.lblSelectTransfer.text = popIBTransferOptions.lblEwallletID.text;
     frmIBTransferCustomWidgetLP.imgTransferType.setVisibility(true);
     frmIBTransferCustomWidgetLP.imgTransferType.src = popIBTransferOptions.imgWallteID.src;
  }
  
  displayBtnSchedule(currentForm, false);
  displayHbxNotifyRecipent(currentForm, true);
  popIBTransferOptions.dismiss();
  
    if(gblTrasferFrmAcntSummary && promptPayAccountFlag) {
    	showFromAccountsfrmIBTranferLP(true);
  } else {
  		onClickOnUsMobileNumberIB(); 
  }
}

function formatAndValidateEWallet(txt){
	//Max length of e-Wallet ID after format 000-000000000000
	var maxlength = 16;
	frmIBTranferLP.txtXferMobileNumber.maxTextLength = maxlength;
	
	if (txt == null) return false;
	var numChars = txt.length;
	var temp = "";
	var i, txtLen = numChars;
	var currLen = numChars;
	if (gblPrevLen < currLen) {
		for (i = 0; i < numChars; ++i) {
			if (txt[i] != '-') {
				temp = temp + txt[i];
			} else {
				txtLen--;
			}
		}
		var iphenText = "";
		for (i = 0; i < txtLen; i++) {
			iphenText += temp[i];
			if (i == 2) {
				iphenText += '-';
			}
		}
		frmIBTranferLP.txtXferMobileNumber.text = iphenText;
	}
	gblPrevLen = currLen;
	
	var eWalletID = frmIBTranferLP.txtXferMobileNumber.text;
	if(isNotBlank(eWalletID)){
		eWalletID = removeHyphenIB(eWalletID);
		if(!isNotBlank(frmIBTranferLP.lblMobileNumberTemp.text)){
			frmIBTranferLP.lblMobileNumberTemp.text = "";
		}
		if(eWalletID.length >= 15){
			frmIBTransferNowConfirmation.lblXferToName.text = "";
			if(!kony.string.equalsIgnoreCase(frmIBTranferLP.lblMobileNumberTemp.text, frmIBTranferLP.txtXferMobileNumber.text)){
				isOwnAccountP2P = false;
              
              var enteredAmt = frmIBTranferLP.txbXferAmountRcvd.text;
              if(isNotBlank(enteredAmt) && parseFloat(enteredAmt, 10) > 0 )
              {
                checkCallCheckOnUsPromptPayinqServiceIB();
              }
              
			}else{
				displayPrevValueForSameEnteredMobile();
			}
		}
	}
}

function clearValuesfrmIBTranferLP(){
      frmIBTranferLP.txtXferMobileNumber.text = "";
      frmIBTranferLP.txbXferAmountRcvd.text = "";
      frmIBTranferLP.lblP2PFeeVal.text = "";
      frmIBTranferLP.txtArMn.text = "";
      frmIBTranferLP.txtTransLndSmsNEmail.text = "";
      frmIBTranferLP.tbxEmail.text = "";
      frmIBTranferLP.tbxXferNTR.text = "";
      frmIBTranferLP.textRecNoteEmail.text = "";
}

/**
 * @function
 *
 */
function showFromAccountsfrmIBTranferLP(show){   
	frmIBTranferLP.hbxRecipient.setVisibility(show);
    frmIBTranferLP.hbxFrom.setVisibility(show); 
  	
  	if(show)
  		frmIBTranferLP.imageXferFrmLP.src=gblcwselectedData.img1;
}

/**
 * @function
 *
 */
function closepopIBTransferOptions(){
   popIBTransferOptions.dismiss();
}