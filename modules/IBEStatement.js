glbEStmtCC = "";
glbEStmtRDC = "";
glbEStmtC2G = "";
gblEStatementEMail="";
var glbWidgetName = ["", "btnRegisterAddr", "btnOfficeAddr", "btnContactAddr"];
isEStmtUpdate = false;
isEnableNextBtn = false;
var defaultPaperOpts = "N,0";
var defaultEmailOpts = "Y,0";
var isNoAddress = false;
var glbAllowCreditCard = false;
var glbAllowReadyCash = false;
var glbAllowCash2Go = false;


function resetEStmtGlobalVariable(){
	glbEStmtCC = "";
	glbEStmtRDC = "";
	glbEStmtC2G = "";
	gblEStatementEMail="";
}

function frmIBEStatementProdFeaturePreShow() {
	var currentLocale = kony.i18n.getCurrentLocale();
	var eStatementProdFeature_image_name="EStatementBriefEN";
	if(currentLocale == "th_TH"){
		eStatementProdFeature_image_name = "EStatementBriefTH";
	}
	var eStatementProdFeature_image="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+eStatementProdFeature_image_name+"&modIdentifier=PRODUCTPACKAGEIMG";
		
	frmIBEStatementProdFeature.imgEStatementProdFeature.src = eStatementProdFeature_image;
	dataSegment=[];

	if(GLOBAL_E_STATEMENT_IMAGES!=null && GLOBAL_E_STATEMENT_PRODUCTS!=null){
	
		for(i=0;i<GLOBAL_E_STATEMENT_PRODUCTS.length;i++){
				var prodImg = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+GLOBAL_E_STATEMENT_IMAGES[i]+""+"&modIdentifier=PRODICON";
				var tempSegmentRecord;
				tempSegmentRecord = {"imgProdType":prodImg,"lblProdName":kony.i18n.getLocalizedString(GLOBAL_E_STATEMENT_PRODUCTS[i])}
				dataSegment.push(tempSegmentRecord);
		}
			frmIBEStatementProdFeature.segEStatementProdAllow.setData(dataSegment);
	}
}

function frmIBEStatementTnCPreShow() {
	var currentLocale = kony.i18n.getCurrentLocale();
	var locale = kony.i18n.getCurrentLocale();
    var input_param = {};
    if (locale == "en_US") {
		input_param["localeCd"]="en_US";
	}else{
		input_param["localeCd"]="th_TH";
	}

	input_param["moduleKey"]='TMBEStatement';
	showLoadingScreenPopup();
    invokeServiceSecureAsync("readUTFFile", input_param, setIBEStatementTnC);	
}

function setIBEStatementTnC(status,result){
	if(status==400){
		dismissLoadingScreenPopup();
		if(result["opstatus"] == 0){
			frmIBEStatementTnC.lblEStatementDesc.text = result["fileContent"];
		}
	}
}

function loadTermsNConditionEStatementIB(){
	var input_param = {};
	var locale = kony.i18n.getCurrentLocale();
	    if (locale == "en_US") {
		input_param["localeCd"]="en_US";
		}else{
		input_param["localeCd"]="th_TH";
		}
	input_param["moduleKey"]= 'TMBEStatement';
	
	invokeServiceSecureAsync("readUTFFile", input_param, loadTNCPointEStatementIBCallBack);
}

function loadTNCPointEStatementIBCallBack(status,result){
	if(status == 400){
		if(result["opstatus"] == 0){
			var data = result["fileContent"];
			var tncTitle = kony.i18n.getLocalizedString("keyTermsNConditions");
			if(tncTitle=="Terms & Conditions")
				{
				 	tncTitle = "TermsandConditions";
				 }
			showPopup(tncTitle,data);
		}else{
			showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
			return false;
		}
	}
}

function onClickEmailTnCEStatementIB() {
	
	showLoadingScreenPopup()
	var inputparam = {};
	inputparam["channelName"] = "Internet Banking";
	inputparam["channelID"] = "01";
	inputparam["notificationType"] = "Email"; // always email
	inputparam["phoneNumber"] = gblPHONENUMBER;
	inputparam["mail"] = gblEmailId;
	inputparam["customerName"] = gblCustomerName;
	inputparam["localeCd"] = kony.i18n.getCurrentLocale();
    inputparam["moduleKey"] = "TMBEStatement";
    //if (kony.i18n.getCurrentLocale() == "en_US") {
	inputparam["productName"] = "";
	//} else {
	inputparam["productNameTH"] = "";
	//}
	invokeServiceSecureAsync("TCEMailService", inputparam, callBackEmailTnCEStatementIB);
	
}

function callBackEmailTnCEStatementIB(status, result) {
	if (status == 400) {
		if (result["opstatus"] == 0) {
			var StatusCode = result["StatusCode"];
			var Severity = result["Severity"];
			var StatusDesc = result["StatusDesc"];
			if (StatusCode == 0) {
				showAlert(kony.i18n.getLocalizedString("keytermOpenAcnt"), kony.i18n.getLocalizedString("info"));
				dismissLoadingScreenPopup();
			} else {
				dismissLoadingScreenPopup();
				return false;
			}
		} else {
			dismissLoadingScreenPopup()
		}
	}
}

function onClickNextTnCEStatementIB(){
	
	var input_param = {};
	invokeServiceSecureAsync("eStmtDetailsCompositeJavaService", input_param, callBackeStmtIB);	
}

function callBackeStmtIB(status,result){
	if(status==400){
		dismissLoadingScreen();
		if(result["opstatus"]==0)
		{
			if(result["EStmtAnswerDS"]!=null)
			{
				gblstmtAns=result["EStmtAnswerDS"];
			}else{
				showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
				return false;
			}
			
			if(result["RegfullAddr"]!=null)
			{
				gblEStmtRegisterAddr = result["RegfullAddr"];
			}
			if(result["officefullAddr"]!=null)
			{
				gblEStmtOfficeAddr = result["officefullAddr"];
			}
			if(result["primaryfullAddr"]!=null)
			{
				gblEStmtContactAddr = result["primaryfullAddr"];
			}
			if(result["emailId"]!=null)
			{
				gblEmailAddr = result["emailId"];
			}
			frmIBEStatementLanding.show();
		}
		else
		{
			showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
			return false;
		}
	}
}

function backToAccountSummaryESTMT(){
	accountsummaryLangToggleIB.call(this);
    frmIBAccntSummary.segAccountDetails.selectedIndex = [0,0];
}

function frmIBEStatementLandingPreShow(){

	putFocusEStatementIBTextbox();
	if(!isEStmtUpdate){
		resetEStmtGlobalVariable();
		frmIBEStatementLanding.hbxCreditCard.setVisibility(false);
		frmIBEStatementLanding.hbxReadyCash.setVisibility(false);
		frmIBEStatementLanding.hbxCash2Go.setVisibility(false);
		frmIBEStatementLanding.txtEStatementEmail.text = gblEmailAddr;
		frmIBEStatementLanding.btnNext.setEnabled(false);
	   	frmIBEStatementLanding.btnNext.skin="btnIB158disabled";
	   	frmIBEStatementLanding.btnNext.focusSkin="btnIB158disabled";
	   	frmIBEStatementLanding.btnNext.hoverSkin="btnIB158disabled";
		
		if(gblAccountTable!=null)
		{
		
		glbAllowCreditCard = gblAccountTable["haveCreditCard"] == "yes" && gbleStmtProducts["haveCreditCardProd"]!==undefined && gbleStmtProducts["haveCreditCardImg"]!==undefined;
		glbAllowReadyCash = gblAccountTable["haveReadyCash"] == "yes" && gbleStmtProducts["haveReadyCashProd"]!==undefined && gbleStmtProducts["haveReadyCashImg"]!==undefined;
		glbAllowCash2Go = gblAccountTable["haveCash2Go"] == "yes" && gbleStmtProducts["haveCash2GoProd"]!==undefined && gbleStmtProducts["haveCash2GoImg"]!==undefined;
		
			if(glbAllowCreditCard) 
			{
				frmIBEStatementLanding.hbxCreditCard.setVisibility(true);
				frmIBEStatementLanding.hbxCreditCardProdHeader.setVisibility(true);
				frmIBEStatementLanding.hbxCreditCardAddress.setVisibility(false);
				frmIBEStatementLanding.lblEStmtCCAddress.setVisibility(false);
				frmIBEStatementLanding.lblEStmtCCAddressNote.setVisibility(false);
				frmIBEStatementLanding.lblEStmtCCDefault.setVisibility(false);
				var prodImg = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+gbleStmtProducts["haveCreditCardImg"]+""+"&modIdentifier=PRODICON";
				frmIBEStatementLanding.imgProdEStmtCC.src = prodImg;
				
				if(gblstmtAns[1]["EStmt_CC"]=="Y")
				{
					frmIBEStatementLanding.btnEStmtCCEmailOption.skin = btnIBEmailRgtFocus;
					frmIBEStatementLanding.btnEStmtCCEmailOption.focusSkin = btnIBEmailRgtFocus;
					frmIBEStatementLanding.btnEStmtCCPaperOption.skin = btnIbSMSRoundedCorner;
					frmIBEStatementLanding.btnEStmtCCPaperOption.focusSkin = btnIbSMSRoundedCorner;
					glbEStmtCC = defaultEmailOpts;
				}
				else
				{
					frmIBEStatementLanding.btnEStmtCCEmailOption.skin = btnIBemailRoundedCorner;
					frmIBEStatementLanding.btnEStmtCCEmailOption.focusSkin = btnIBemailRoundedCorner;
					frmIBEStatementLanding.btnEStmtCCPaperOption.skin = btnIBSMSfocusleft;
					frmIBEStatementLanding.btnEStmtCCPaperOption.focusSkin = btnIBSMSfocusleft;
					glbEStmtCC = defaultPaperOpts;
				}
			}
			
			if(glbAllowReadyCash) 
			{
				frmIBEStatementLanding.hbxReadyCash.setVisibility(true);
				frmIBEStatementLanding.hbxReadyCashHeader.setVisibility(true);
				frmIBEStatementLanding.hbxReadyCashAddress.setVisibility(false);
				frmIBEStatementLanding.lblEStmtRDCAddress.setVisibility(false);
				frmIBEStatementLanding.lblEStmtRDCAddressNote.setVisibility(false);
				frmIBEStatementLanding.lblEStmtRDCDefault.setVisibility(false);
				var prodImg = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+gbleStmtProducts["haveReadyCashImg"]+""+"&modIdentifier=PRODICON";
				frmIBEStatementLanding.imgProdEStmtRDC.src = prodImg;
	
				if(gblstmtAns[2]["EStmt_RDC"]=="Y")
				{
					frmIBEStatementLanding.btnEStmtRDCEmailOption.skin = btnIBEmailRgtFocus;
					frmIBEStatementLanding.btnEStmtRDCEmailOption.focusSkin = btnIBEmailRgtFocus;
					frmIBEStatementLanding.btnEStmtRDCPaperOption.skin = btnIbSMSRoundedCorner;
					frmIBEStatementLanding.btnEStmtRDCPaperOption.focusSkin = btnIbSMSRoundedCorner;
					glbEStmtRDC = defaultEmailOpts;
				}
				else
				{
					frmIBEStatementLanding.btnEStmtRDCEmailOption.skin = btnIBemailRoundedCorner;
					frmIBEStatementLanding.btnEStmtRDCEmailOption.focusSkin = btnIBemailRoundedCorner;
					frmIBEStatementLanding.btnEStmtRDCPaperOption.skin = btnIBSMSfocusleft;
					frmIBEStatementLanding.btnEStmtRDCPaperOption.focusSkin = btnIBSMSfocusleft;
					glbEStmtRDC = defaultPaperOpts;
				}
			}
			
			if(glbAllowCash2Go)
			{
				frmIBEStatementLanding.hbxCash2Go.setVisibility(true);
				frmIBEStatementLanding.hbxCash2GoHeader.setVisibility(true);
				frmIBEStatementLanding.hbxCash2GoAddress.setVisibility(false);
				frmIBEStatementLanding.lblEStmtC2GAddress.setVisibility(false);
				frmIBEStatementLanding.lblEStmtC2GAddressNote.setVisibility(false);
				frmIBEStatementLanding.lblEStmtC2GDefault.setVisibility(false);
				var prodImg = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+gbleStmtProducts["haveCash2GoImg"]+""+"&modIdentifier=PRODICON";
				frmIBEStatementLanding.imgProdEStmtC2G.src = prodImg;
	
				if(gblstmtAns[3]["EStmt_C2G"]=="Y")
				{
					frmIBEStatementLanding.btnEStmtC2GEmailOption.skin = btnIBEmailRgtFocus;
					frmIBEStatementLanding.btnEStmtC2GEmailOption.focusSkin = btnIBEmailRgtFocus;
					frmIBEStatementLanding.btnEStmtC2GPaperOption.skin = btnIbSMSRoundedCorner;
					frmIBEStatementLanding.btnEStmtC2GPaperOption.focusSkin = btnIbSMSRoundedCorner;
					glbEStmtC2G = defaultEmailOpts;
				}
				else
				{
					frmIBEStatementLanding.btnEStmtC2GEmailOption.skin = btnIBemailRoundedCorner;
					frmIBEStatementLanding.btnEStmtC2GEmailOption.focusSkin = btnIBemailRoundedCorner;
					frmIBEStatementLanding.btnEStmtC2GPaperOption.skin = btnIBSMSfocusleft;
					frmIBEStatementLanding.btnEStmtC2GPaperOption.focusSkin = btnIBSMSfocusleft;
					glbEStmtC2G = defaultPaperOpts;
				}
			}
		}
	}else{
		  isEStmtUpdate = false;
		 if(glbAllowCreditCard) {

				frmIBEStatementLanding.hbxCreditCard.setVisibility(true);
				frmIBEStatementLanding.hbxCreditCardProdHeader.setVisibility(true);
				frmIBEStatementLanding.hbxCreditCardAddress.setVisibility(false);
				frmIBEStatementLanding.lblEStmtCCAddress.setVisibility(false);
				frmIBEStatementLanding.lblEStmtCCAddressNote.setVisibility(false);
				frmIBEStatementLanding.lblEStmtCCDefault.setVisibility(false);
				var prodImg = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+gbleStmtProducts["haveCreditCardImg"]+""+"&modIdentifier=PRODICON";
				frmIBEStatementLanding.imgProdEStmtCC.src = prodImg;
				
				if(glbEStmtCC.indexOf("Y") > -1){
					frmIBEStatementLanding.btnEStmtCCEmailOption.skin = btnIBEmailRgtFocus;
					frmIBEStatementLanding.btnEStmtCCEmailOption.focusSkin = btnIBEmailRgtFocus;
					frmIBEStatementLanding.btnEStmtCCPaperOption.skin = btnIbSMSRoundedCorner;
					frmIBEStatementLanding.btnEStmtCCPaperOption.focusSkin = btnIbSMSRoundedCorner;
				}else if(glbEStmtCC == defaultPaperOpts){
					frmIBEStatementLanding.btnEStmtCCEmailOption.skin = btnIBemailRoundedCorner;
					frmIBEStatementLanding.btnEStmtCCEmailOption.focusSkin = btnIBemailRoundedCorner;
					frmIBEStatementLanding.btnEStmtCCPaperOption.skin = btnIBSMSfocusleft;
					frmIBEStatementLanding.btnEStmtCCPaperOption.focusSkin = btnIBSMSfocusleft;
				}else{
					frmIBEStatementLanding.hbxCreditCardAddress.setVisibility(true);
					frmIBEStatementLanding.lblEStmtCCAddress.setVisibility(true);
					frmIBEStatementLanding.lblEStmtCCAddressNote.setVisibility(true);
					onClickEStmtChooseAddressButtonIB(glbWidgetName[glbEStmtCC.split(",")[1]], "EStmtCC");
					}
			}
			
			if(glbAllowReadyCash)  {
				
				frmIBEStatementLanding.hbxReadyCash.setVisibility(true);
				frmIBEStatementLanding.hbxReadyCashHeader.setVisibility(true);
				frmIBEStatementLanding.hbxReadyCashAddress.setVisibility(false);
				frmIBEStatementLanding.lblEStmtRDCAddress.setVisibility(false);
				frmIBEStatementLanding.lblEStmtRDCAddressNote.setVisibility(false);
				frmIBEStatementLanding.lblEStmtRDCDefault.setVisibility(false);
				var prodImg = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+gbleStmtProducts["haveReadyCashImg"]+""+"&modIdentifier=PRODICON";
				frmIBEStatementLanding.imgProdEStmtRDC.src = prodImg;
	
				if(glbEStmtRDC.indexOf("Y") > -1){
					frmIBEStatementLanding.btnEStmtRDCEmailOption.skin = btnIBEmailRgtFocus;
					frmIBEStatementLanding.btnEStmtRDCEmailOption.focusSkin = btnIBEmailRgtFocus;
					frmIBEStatementLanding.btnEStmtRDCPaperOption.skin = btnIbSMSRoundedCorner;
					frmIBEStatementLanding.btnEStmtRDCPaperOption.focusSkin = btnIbSMSRoundedCorner;
				}else if(glbEStmtRDC == defaultPaperOpts){
					frmIBEStatementLanding.btnEStmtRDCEmailOption.skin = btnIBemailRoundedCorner;
					frmIBEStatementLanding.btnEStmtRDCEmailOption.focusSkin = btnIBemailRoundedCorner;
					frmIBEStatementLanding.btnEStmtRDCPaperOption.skin = btnIBSMSfocusleft;
					frmIBEStatementLanding.btnEStmtRDCPaperOption.focusSkin = btnIBSMSfocusleft;
				}else{
					frmIBEStatementLanding.hbxReadyCashAddress.setVisibility(true);
					frmIBEStatementLanding.lblEStmtRDCAddress.setVisibility(true);
					frmIBEStatementLanding.lblEStmtRDCAddressNote.setVisibility(true);
					onClickEStmtChooseAddressButtonIB(glbWidgetName[glbEStmtRDC.split(",")[1]], "EStmtRDC");
					}
			}
			
			if(glbAllowCash2Go){
				frmIBEStatementLanding.hbxCash2Go.setVisibility(true);
				frmIBEStatementLanding.hbxCash2GoHeader.setVisibility(true);
				frmIBEStatementLanding.hbxCash2GoAddress.setVisibility(false);
				frmIBEStatementLanding.lblEStmtC2GAddress.setVisibility(false);
				frmIBEStatementLanding.lblEStmtC2GAddressNote.setVisibility(false);
				frmIBEStatementLanding.lblEStmtC2GDefault.setVisibility(false);
				var prodImg = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+gbleStmtProducts["haveCash2GoImg"]+""+"&modIdentifier=PRODICON";
				frmIBEStatementLanding.imgProdEStmtC2G.src = prodImg;
							
				if(glbEStmtC2G.indexOf("Y") > -1){
					frmIBEStatementLanding.btnEStmtC2GEmailOption.skin = btnIBEmailRgtFocus;
					frmIBEStatementLanding.btnEStmtC2GEmailOption.focusSkin = btnIBEmailRgtFocus;
					frmIBEStatementLanding.btnEStmtC2GPaperOption.skin = btnIbSMSRoundedCorner;
					frmIBEStatementLanding.btnEStmtC2GPaperOption.focusSkin = btnIbSMSRoundedCorner;
				}else if(glbEStmtC2G == defaultPaperOpts){
					frmIBEStatementLanding.btnEStmtC2GEmailOption.skin = btnIBemailRoundedCorner;
					frmIBEStatementLanding.btnEStmtC2GEmailOption.focusSkin = btnIBemailRoundedCorner;
					frmIBEStatementLanding.btnEStmtC2GPaperOption.skin = btnIBSMSfocusleft;
					frmIBEStatementLanding.btnEStmtC2GPaperOption.focusSkin = btnIBSMSfocusleft;
				}else{
					frmIBEStatementLanding.hbxCash2GoAddress.setVisibility(true);
					frmIBEStatementLanding.lblEStmtC2GAddress.setVisibility(true);
					frmIBEStatementLanding.lblEStmtC2GAddressNote.setVisibility(true);
					onClickEStmtChooseAddressButtonIB(glbWidgetName[glbEStmtC2G.split(",")[1]], "EStmtC2G");
				}
			}
	}
}

function onClickEStmtEmailPaperOptionIB(widgetName, productName){
		if(productName == "EStmtCC"){
			setEmailPaperOptionIBCreditCard(widgetName);
		}else if(productName == "EStmtRDC"){
			setEmailPaperOptionIBReadyCash(widgetName);
		}else{
			setEmailPaperOptionIBCash2Go(widgetName);
		}
}

function setEmailPaperOptionIBCreditCard(widgetName){
	if(widgetName == "btnPaperOption"){
		frmIBEStatementLanding.hbxCreditCardAddress.setVisibility(true);
		frmIBEStatementLanding.lblEStmtCCDefault.setVisibility(true);
		frmIBEStatementLanding.lblEStmtCCAddress.setVisibility(false);
		frmIBEStatementLanding.lblEStmtCCAddressNote.setVisibility(false);
		frmIBEStatementLanding.btnEStmtCCEmailOption.skin = btnIBemailRoundedCorner;
		frmIBEStatementLanding.btnEStmtCCEmailOption.focusSkin = btnIBEmailRgtFocus;
		frmIBEStatementLanding.btnEStmtCCEmailOption.hoverSkin = btnIBEmailRgtFocus;
		frmIBEStatementLanding.btnEStmtCCPaperOption.skin = btnIBSMSfocusleft;
		frmIBEStatementLanding.btnEStmtCCPaperOption.focusSkin = btnIBSMSfocusleft;
		frmIBEStatementLanding.btnEStmtCCRegisterAddr.skin = btnRegisterAddrGrey;
		frmIBEStatementLanding.btnEStmtCCRegisterAddr.hoverSkin = btnRegisterAddr;
		frmIBEStatementLanding.btnEStmtCCOfficeAddr.skin = btnOfficeAddrGrey;
		frmIBEStatementLanding.btnEStmtCCOfficeAddr.hoverSkin = btnOfficeAddr;
		frmIBEStatementLanding.btnEStmtCCContactAddr.skin = btnContactAddrGrey;
		frmIBEStatementLanding.btnEStmtCCContactAddr.hoverSkin = btnContactAddr;
		frmIBEStatementConfirm.lblEStmtCCSendby.text = kony.i18n.getLocalizedString("keyEStmtSendBy")+" "+kony.i18n.getLocalizedString("keyEStmtPaper");
		glbEStmtCC =  "N,N";
		disableEStmtNextButton();
	}else{
		glbEStmtCC = defaultEmailOpts;
		frmIBEStatementLanding.hbxCreditCardAddress.setVisibility(false);
		frmIBEStatementLanding.lblEStmtCCDefault.setVisibility(false);
		frmIBEStatementLanding.btnEStmtCCEmailOption.skin = btnIBEmailRgtFocus;
		frmIBEStatementLanding.btnEStmtCCEmailOption.focusSkin = btnIBEmailRgtFocus;
		frmIBEStatementLanding.btnEStmtCCPaperOption.skin = btnIbSMSRoundedCorner;
		frmIBEStatementLanding.btnEStmtCCPaperOption.focusSkin = btnIBSMSfocusleft;
		frmIBEStatementLanding.btnEStmtCCPaperOption.hoverSkin = btnIBSMSfocusleft;
		frmIBEStatementLanding.lblEStmtCCAddress.setVisibility(false);
		frmIBEStatementLanding.lblEStmtCCAddressNote.setVisibility(false);
		frmIBEStatementConfirm.lblEStmtCCSendby.text = kony.i18n.getLocalizedString("keyEStmtSendBy")+" "+kony.i18n.getLocalizedString("email");
		checkESmtNextButtonEnable("EStmtCC");
	}
}

function setEmailPaperOptionIBReadyCash(widgetName){
	if(widgetName == "btnPaperOption"){
		frmIBEStatementLanding.hbxReadyCashAddress.setVisibility(true);
		frmIBEStatementLanding.lblEStmtRDCDefault.setVisibility(true);
		frmIBEStatementLanding.lblEStmtRDCAddress.setVisibility(false);
		frmIBEStatementLanding.lblEStmtRDCAddressNote.setVisibility(false);
		frmIBEStatementLanding.btnEStmtRDCEmailOption.skin = btnIBemailRoundedCorner;
		frmIBEStatementLanding.btnEStmtRDCEmailOption.focusSkin = btnIBEmailRgtFocus;
		frmIBEStatementLanding.btnEStmtRDCEmailOption.hoverSkin = btnIBEmailRgtFocus;
		frmIBEStatementLanding.btnEStmtRDCPaperOption.skin = btnIBSMSfocusleft;
		frmIBEStatementLanding.btnEStmtRDCPaperOption.focusSkin = btnIBSMSfocusleft;
		frmIBEStatementLanding.btnEStmtRDCRegisterAddr.skin = btnRegisterAddrGrey;
		frmIBEStatementLanding.btnEStmtRDCRegisterAddr.hoverSkin = btnRegisterAddr;
		frmIBEStatementLanding.btnEStmtRDCOfficeAddr.skin = btnOfficeAddrGrey;
		frmIBEStatementLanding.btnEStmtRDCOfficeAddr.hoverSkin = btnOfficeAddr;
		frmIBEStatementLanding.btnEStmtRDCContactAddr.skin = btnContactAddrGrey;
		frmIBEStatementLanding.btnEStmtRDCContactAddr.hoverSkin = btnContactAddr;
		frmIBEStatementConfirm.lblEStmtRDCSendby.text = kony.i18n.getLocalizedString("keyEStmtSendBy")+" "+kony.i18n.getLocalizedString("keyEStmtPaper");
		glbEStmtRDC =  "N,N";
		disableEStmtNextButton();
	}else{
		glbEStmtRDC = defaultEmailOpts;
		frmIBEStatementLanding.hbxReadyCashAddress.setVisibility(false);
		frmIBEStatementLanding.lblEStmtRDCDefault.setVisibility(false);
		frmIBEStatementLanding.btnEStmtRDCEmailOption.skin = btnIBEmailRgtFocus;
		frmIBEStatementLanding.btnEStmtRDCEmailOption.focusSkin = btnIBEmailRgtFocus;
		frmIBEStatementLanding.btnEStmtRDCPaperOption.skin = btnIbSMSRoundedCorner;
		frmIBEStatementLanding.btnEStmtRDCPaperOption.focusSkin = btnIBSMSfocusleft;
		frmIBEStatementLanding.btnEStmtRDCPaperOption.hoverSkin = btnIBSMSfocusleft;
		frmIBEStatementLanding.lblEStmtRDCAddress.setVisibility(false);
		frmIBEStatementLanding.lblEStmtRDCAddressNote.setVisibility(false);
		frmIBEStatementConfirm.lblEStmtRDCSendby.text = kony.i18n.getLocalizedString("keyEStmtSendBy")+" "+kony.i18n.getLocalizedString("email");
		checkESmtNextButtonEnable("EStmtRDC");
	}
}

function setEmailPaperOptionIBCash2Go(widgetName){
	if(widgetName == "btnPaperOption"){
		frmIBEStatementLanding.hbxCash2GoAddress.setVisibility(true);
		frmIBEStatementLanding.lblEStmtC2GDefault.setVisibility(true);
		frmIBEStatementLanding.lblEStmtC2GAddress.setVisibility(false);
		frmIBEStatementLanding.lblEStmtC2GAddressNote.setVisibility(false);
		frmIBEStatementLanding.btnEStmtC2GEmailOption.skin = btnIBemailRoundedCorner;
		frmIBEStatementLanding.btnEStmtC2GEmailOption.focusSkin = btnIBEmailRgtFocus;
		frmIBEStatementLanding.btnEStmtC2GEmailOption.hoverSkin = btnIBEmailRgtFocus;
		frmIBEStatementLanding.btnEStmtC2GPaperOption.skin = btnIBSMSfocusleft;
		frmIBEStatementLanding.btnEStmtC2GPaperOption.focusSkin = btnIBSMSfocusleft;
		frmIBEStatementLanding.btnEStmtC2GRegisterAddr.skin = btnRegisterAddrGrey;
		frmIBEStatementLanding.btnEStmtC2GRegisterAddr.hoverSkin = btnRegisterAddr;
		frmIBEStatementLanding.btnEStmtC2GOfficeAddr.skin = btnOfficeAddrGrey;
		frmIBEStatementLanding.btnEStmtC2GOfficeAddr.hoverSkin = btnOfficeAddr;
		frmIBEStatementLanding.btnEStmtC2GContactAddr.skin = btnContactAddrGrey;
		frmIBEStatementLanding.btnEStmtC2GContactAddr.hoverSkin = btnContactAddr;
		frmIBEStatementConfirm.lblEStmtC2GSendby.text = kony.i18n.getLocalizedString("keyEStmtSendBy")+" "+kony.i18n.getLocalizedString("keyEStmtPaper");
		glbEStmtC2G =  "N,N";
		disableEStmtNextButton();
	}else{
		glbEStmtC2G = defaultEmailOpts;
		frmIBEStatementLanding.hbxCash2GoAddress.setVisibility(false);
		frmIBEStatementLanding.lblEStmtC2GDefault.setVisibility(false);
		frmIBEStatementLanding.btnEStmtC2GEmailOption.skin = btnIBEmailRgtFocus;
		frmIBEStatementLanding.btnEStmtC2GEmailOption.focusSkin = btnIBEmailRgtFocus;
		frmIBEStatementLanding.btnEStmtC2GPaperOption.skin = btnIbSMSRoundedCorner;
		frmIBEStatementLanding.btnEStmtC2GPaperOption.focusSkin = btnIBSMSfocusleft;
		frmIBEStatementLanding.btnEStmtC2GPaperOption.hoverSkin = btnIBSMSfocusleft;
		frmIBEStatementLanding.lblEStmtC2GAddress.setVisibility(false);
		frmIBEStatementLanding.lblEStmtC2GAddressNote.setVisibility(false);
		frmIBEStatementConfirm.lblEStmtC2GSendby.text = kony.i18n.getLocalizedString("keyEStmtSendBy")+" "+kony.i18n.getLocalizedString("email");
		checkESmtNextButtonEnable("EStmtC2G");
	}
}


function checkESmtNextButtonEnable(productName){

	kony.print("### productName "+productName);
	
	if(productName == "EStmtCC"){
		if((glbAllowReadyCash && frmIBEStatementLanding.lblEStmtRDCDefault.isVisible) ||
		   (glbAllowCash2Go && frmIBEStatementLanding.lblEStmtC2GDefault.isVisible)){
		   	disableEStmtNextButton();
		}else{
			checkNextButtonEnableEStatementLandingIB();
		}
	}else if(productName == "EStmtRDC"){
		if((glbAllowCreditCard && frmIBEStatementLanding.lblEStmtCCDefault.isVisible) ||
		   (glbAllowCash2Go && frmIBEStatementLanding.lblEStmtC2GDefault.isVisible)){
		   	disableEStmtNextButton();
		}else{
			checkNextButtonEnableEStatementLandingIB();
		}
	
	}else if(productName == "EStmtC2G"){
		if((glbAllowCreditCard && frmIBEStatementLanding.lblEStmtCCDefault.isVisible) ||
		   (glbAllowReadyCash && frmIBEStatementLanding.lblEStmtRDCDefault.isVisible)){
		   	disableEStmtNextButton();
		}else{
			checkNextButtonEnableEStatementLandingIB();
		}
	}
}


function setGlobalEmail(){
	gblEStatementEMail = frmIBEStatementLanding.txtEStatementEmail.text;
}

function verifyEStmtOption(){
	var optionMapping = {"ADN":"N", "BDY":"Y", "CDN":"N", "DDN":"N", "EDY":"Y", "FDN":"N", "GDY":"Y", 
						 "ACY":"Y", "BCY":"Y", "CCN":"N", "DCN":"N", "ECY":"Y", "FCN":"N", "GCY":"Y"};
	var alwayDisable = ["CDN", "CCN", "FDN", "FCN"];
	
	var optionSelected = [glbEStmtCC, glbEStmtRDC, glbEStmtC2G];
	var originalState = [gblstmtAns[1]["EStmt_CC"], gblstmtAns[2]["EStmt_RDC"], gblstmtAns[3]["EStmt_C2G"]];
	var haveProducts = [glbAllowCreditCard, glbAllowReadyCash, glbAllowCash2Go];
	var matchingState = [];
	var showHideState = [];
	
	setGlobalEmail();
	
	for(var i=0; i < optionSelected.length; i++){
		if(haveProducts[i]){
			var temp = "";
				if(originalState[i] == "Y"){
					if(optionSelected[i] == defaultEmailOpts){
						temp = "A";
					}else if(optionSelected[i] == "N,N"){
						temp = "C";
					}else{
						temp = "B";
					}
				
				}else{
					if(optionSelected[i] == defaultPaperOpts){
						temp = "D";
					}else if(optionSelected[i] == "N,N"){
						temp = "F";
					}else if(optionSelected[i] == defaultEmailOpts){
						temp = "G";
					}else{
						temp = "E";
					}
				}
				
			
			if(gblEmailAddr != null && gblEmailAddr == gblEStatementEMail){
				
				if(temp == "A")matchingState[i] = "ADN";
				if(temp == "B")matchingState[i] = "BDY";
				if(temp == "C")matchingState[i] = "CDN";
				if(temp == "D")matchingState[i] = "DDN";
				if(temp == "E")matchingState[i] = "EDY";
				if(temp == "F")matchingState[i] = "FDN";
				if(temp == "G")matchingState[i] = "GDY";
			}else {
			
				if(temp == "A")matchingState[i] = "ACY";
				if(temp == "B")matchingState[i] = "BCY";
				if(temp == "C")matchingState[i] = "CCN";
				if(temp == "D")matchingState[i] = "DCN";
				if(temp == "E")matchingState[i] = "ECY";
				if(temp == "F")matchingState[i] = "FCN";
				if(temp == "G")matchingState[i] = "GCY";
			}
			
			kony.print("#### temp = "+temp);
			kony.print("#### matchingState =  "+matchingState[i]);
		}
	}

	for(var i=0; i<matchingState.length; i++){
		
		for(var j=0; j<alwayDisable.length; j++){
			if(matchingState[i] == alwayDisable[j]){
			kony.print("#### Match alway disable "+alwayDisable[j]);
				disableEStmtNextButton();
				return;
			}
		}
		
		showHideState[i] = optionMapping[matchingState[i]];
	
	}
	
	kony.print("#### showHideState "+showHideState);
	
	if(showHideState.indexOf("Y") > -1){
		enableEStmtNextButton();
	}else{
		disableEStmtNextButton();
	}
	
	
	if(isOnlyPaper()){
		frmIBEStatementConfirm.lblEStatementEmail.setVisibility(false);
	}else{
		frmIBEStatementConfirm.lblEStatementEmail.setVisibility(true);
	}
	
	if(isNoAddress){
		disableEStmtNextButton();
	}

}


function onClickEStmtChooseAddressButtonIB(widgetName, productName){

		if(productName == "EStmtCC"){
			setEStmtChooseAddressIBCreditCard(widgetName);
		}else if(productName == "EStmtRDC"){
			setEStmtChooseAddressIBReadyCash(widgetName);
		}else if(productName == "EStmtC2G"){
			setEStmtChooseAddressIBCash2Go(widgetName);
		}
		
		checkESmtNextButtonEnable(productName);
}

function setEStmtChooseAddressIBCreditCard(widgetName){
	if(widgetName == "btnRegisterAddr"){
		if("undefined" !== typeof gblEStmtRegisterAddr){
			frmIBEStatementLanding.lblEStmtCCAddressNote.setVisibility(true);
			frmIBEStatementLanding.lblEStmtCCAddress.text = gblEStmtRegisterAddr;
			frmIBEStatementLanding.lblEStmtCCAddressNote.text = kony.i18n.getLocalizedString("keyEStmtNoteForRegisteredAndOffice");
			frmIBEStatementConfirm.lblEStmtCCAddrType.text = appendColon(kony.i18n.getLocalizedString("keyEStmtRegAddr"));
			frmIBEStatementConfirm.lblEStmtCCAddress.text = gblEStmtRegisterAddr;
			isNoAddress = false;
		}else{
			frmIBEStatementLanding.lblEStmtCCAddress.text = kony.i18n.getLocalizedString("keyEStmtNoAddress");
			frmIBEStatementLanding.lblEStmtCCAddressNote.setVisibility(false);
			isNoAddress = true;
		}
		glbEStmtCC = "N,1";
		frmIBEStatementLanding.lblEStmtCCAddress.setVisibility(true);
		frmIBEStatementLanding.lblEStmtCCDefault.setVisibility(false);
		frmIBEStatementLanding.btnEStmtCCRegisterAddr.skin = btnRegisterAddr;
		frmIBEStatementLanding.btnEStmtCCOfficeAddr.skin = btnOfficeAddrGrey;
		frmIBEStatementLanding.btnEStmtCCOfficeAddr.hoverSkin = btnOfficeAddr;
		frmIBEStatementLanding.btnEStmtCCContactAddr.skin = btnContactAddrGrey;
		frmIBEStatementLanding.btnEStmtCCContactAddr.hoverSkin = btnContactAddr;
		
	}else if(widgetName == "btnOfficeAddr"){
	
		if("undefined" !== typeof gblEStmtOfficeAddr){
			frmIBEStatementLanding.lblEStmtCCAddressNote.setVisibility(true);
			frmIBEStatementLanding.lblEStmtCCAddress.text = gblEStmtOfficeAddr;
			frmIBEStatementLanding.lblEStmtCCAddressNote.text = kony.i18n.getLocalizedString("keyEStmtNoteForRegisteredAndOffice");
			frmIBEStatementConfirm.lblEStmtCCAddrType.text = appendColon(kony.i18n.getLocalizedString("keyEStmtOfficeAddr"));
			frmIBEStatementConfirm.lblEStmtCCAddress.text = gblEStmtOfficeAddr;
			isNoAddress = false;
		}else{
			frmIBEStatementLanding.lblEStmtCCAddress.text = kony.i18n.getLocalizedString("keyEStmtNoAddress");
			frmIBEStatementLanding.lblEStmtCCAddressNote.setVisibility(false);
			isNoAddress = true;
		}
		glbEStmtCC = "N,2";
		frmIBEStatementLanding.lblEStmtCCAddress.setVisibility(true);
		frmIBEStatementLanding.lblEStmtCCDefault.setVisibility(false);
		frmIBEStatementLanding.btnEStmtCCOfficeAddr.skin = btnOfficeAddr;
		frmIBEStatementLanding.btnEStmtCCRegisterAddr.skin = btnRegisterAddrGrey;
		frmIBEStatementLanding.btnEStmtCCRegisterAddr.hoverSkin = btnRegisterAddr;
		frmIBEStatementLanding.btnEStmtCCContactAddr.skin = btnContactAddrGrey;
		frmIBEStatementLanding.btnEStmtCCContactAddr.hoverSkin = btnContactAddr;
		
	}else{
	
		if("undefined" !== typeof gblEStmtContactAddr){
			frmIBEStatementLanding.lblEStmtCCAddressNote.setVisibility(true);
			frmIBEStatementLanding.lblEStmtCCAddress.text = gblEStmtContactAddr;
			frmIBEStatementLanding.lblEStmtCCAddressNote.text = kony.i18n.getLocalizedString("keyEStmtNoteForContact");
			frmIBEStatementConfirm.lblEStmtCCAddrType.text = appendColon(kony.i18n.getLocalizedString("keyEStmtContactAddr"));
			frmIBEStatementConfirm.lblEStmtCCAddress.text = gblEStmtContactAddr;
			
			isNoAddress = false;
		}else{
			frmIBEStatementLanding.lblEStmtCCAddress.text = kony.i18n.getLocalizedString("keyEStmtNoAddress");
			frmIBEStatementLanding.lblEStmtCCAddressNote.setVisibility(false);
			isNoAddress = true;
		}
		glbEStmtCC = "N,3";
		frmIBEStatementLanding.lblEStmtCCAddress.setVisibility(true);
		frmIBEStatementLanding.lblEStmtCCDefault.setVisibility(false);
		frmIBEStatementLanding.btnEStmtCCContactAddr.skin = btnContactAddr;
		frmIBEStatementLanding.btnEStmtCCRegisterAddr.skin = btnRegisterAddrGrey;
		frmIBEStatementLanding.btnEStmtCCRegisterAddr.hoverSkin = btnRegisterAddr;
		frmIBEStatementLanding.btnEStmtCCOfficeAddr.skin = btnOfficeAddrGrey;
		frmIBEStatementLanding.btnEStmtCCOfficeAddr.hoverSkin = btnOfficeAddr;
	}
}

function setEStmtChooseAddressIBReadyCash(widgetName){
	if(widgetName == "btnRegisterAddr"){
	
		if("undefined" !== typeof gblEStmtRegisterAddr){
			frmIBEStatementLanding.lblEStmtRDCAddressNote.setVisibility(true);
			frmIBEStatementLanding.lblEStmtRDCAddress.text = gblEStmtRegisterAddr;
			frmIBEStatementLanding.lblEStmtRDCAddressNote.text = kony.i18n.getLocalizedString("keyEStmtNoteForRegisteredAndOffice");
			frmIBEStatementConfirm.lblEStmtRDCAddrType.text = appendColon(kony.i18n.getLocalizedString("keyEStmtRegAddr"));
			frmIBEStatementConfirm.lblEStmtRDCAddress.text = gblEStmtRegisterAddr;
			isNoAddress = false;
			
		}else{
			frmIBEStatementLanding.lblEStmtRDCAddress.text = kony.i18n.getLocalizedString("keyEStmtNoAddress");
			frmIBEStatementLanding.lblEStmtRDCAddressNote.setVisibility(false);
			isNoAddress = true;
		}
		glbEStmtRDC ="N,1";
		frmIBEStatementLanding.lblEStmtRDCAddress.setVisibility(true);
		frmIBEStatementLanding.lblEStmtRDCDefault.setVisibility(false);
		frmIBEStatementLanding.btnEStmtRDCRegisterAddr.skin = btnRegisterAddr;
		frmIBEStatementLanding.btnEStmtRDCOfficeAddr.skin = btnOfficeAddrGrey;
		frmIBEStatementLanding.btnEStmtRDCOfficeAddr.hoverSkin = btnOfficeAddr;
		frmIBEStatementLanding.btnEStmtRDCContactAddr.skin = btnContactAddrGrey;
		frmIBEStatementLanding.btnEStmtRDCContactAddr.hoverSkin = btnContactAddr;
		
	}else if(widgetName == "btnOfficeAddr"){
	
		if("undefined" !== typeof gblEStmtOfficeAddr){
			frmIBEStatementLanding.lblEStmtRDCAddressNote.setVisibility(true);
			frmIBEStatementLanding.lblEStmtRDCAddress.text = gblEStmtOfficeAddr;
			frmIBEStatementLanding.lblEStmtRDCAddressNote.text = kony.i18n.getLocalizedString("keyEStmtNoteForRegisteredAndOffice");
			frmIBEStatementConfirm.lblEStmtRDCAddrType.text = appendColon(kony.i18n.getLocalizedString("keyEStmtOfficeAddr"));
			frmIBEStatementConfirm.lblEStmtRDCAddress.text = gblEStmtOfficeAddr;
			isNoAddress = false;
		}else{
			frmIBEStatementLanding.lblEStmtRDCAddress.text = kony.i18n.getLocalizedString("keyEStmtNoAddress");
			frmIBEStatementLanding.lblEStmtRDCAddressNote.setVisibility(false);
			isNoAddress = true;
		}
		glbEStmtRDC ="N,2";
		frmIBEStatementLanding.lblEStmtRDCAddress.setVisibility(true);
		frmIBEStatementLanding.lblEStmtRDCDefault.setVisibility(false);
		frmIBEStatementLanding.btnEStmtRDCOfficeAddr.skin = btnOfficeAddr;
		frmIBEStatementLanding.btnEStmtRDCRegisterAddr.skin = btnRegisterAddrGrey;
		frmIBEStatementLanding.btnEStmtRDCContactAddr.skin = btnContactAddrGrey;
		frmIBEStatementLanding.btnEStmtRDCRegisterAddr.hoverSkin = btnRegisterAddr;
		frmIBEStatementLanding.btnEStmtRDCContactAddr.hoverSkin = btnContactAddr;
		
	}else{
	
		if("undefined" !== typeof gblEStmtContactAddr){
			frmIBEStatementLanding.lblEStmtRDCAddressNote.setVisibility(true);
			frmIBEStatementLanding.lblEStmtRDCAddress.text = gblEStmtContactAddr;
			frmIBEStatementLanding.lblEStmtRDCAddressNote.text = kony.i18n.getLocalizedString("keyEStmtNoteForContact");
			frmIBEStatementConfirm.lblEStmtRDCAddrType.text = appendColon(kony.i18n.getLocalizedString("keyEStmtContactAddr"));
			frmIBEStatementConfirm.lblEStmtRDCAddress.text = gblEStmtContactAddr;
			isNoAddress = false;
			
		}else{
			frmIBEStatementLanding.lblEStmtRDCAddress.text = kony.i18n.getLocalizedString("keyEStmtNoAddress");
			frmIBEStatementLanding.lblEStmtRDCAddressNote.setVisibility(false);
			isNoAddress = true;
		}
		glbEStmtRDC ="N,3";
		frmIBEStatementLanding.lblEStmtRDCAddress.setVisibility(true);
		frmIBEStatementLanding.lblEStmtRDCDefault.setVisibility(false);
		frmIBEStatementLanding.btnEStmtRDCContactAddr.skin = btnContactAddr;
		frmIBEStatementLanding.btnEStmtRDCRegisterAddr.skin = btnRegisterAddrGrey;
		frmIBEStatementLanding.btnEStmtRDCOfficeAddr.skin = btnOfficeAddrGrey;
		frmIBEStatementLanding.btnEStmtRDCRegisterAddr.hoverSkin = btnRegisterAddr;
		frmIBEStatementLanding.btnEStmtRDCOfficeAddr.hoverSkin = btnOfficeAddr;
		
	}
}

function setEStmtChooseAddressIBCash2Go(widgetName){
	if(widgetName == "btnRegisterAddr"){
		
		if("undefined" !== typeof gblEStmtRegisterAddr){
			frmIBEStatementLanding.lblEStmtC2GAddressNote.setVisibility(true);
			frmIBEStatementLanding.lblEStmtC2GAddress.text = gblEStmtRegisterAddr;
			frmIBEStatementLanding.lblEStmtC2GAddressNote.text = kony.i18n.getLocalizedString("keyEStmtNoteForRegisteredAndOffice");
			frmIBEStatementConfirm.lblEStmtC2GAddrType.text = appendColon(kony.i18n.getLocalizedString("keyEStmtRegAddr"));
			frmIBEStatementConfirm.lblEStmtC2GAddress.text = gblEStmtRegisterAddr;
			isNoAddress = false;
			
		}else{
			frmIBEStatementLanding.lblEStmtC2GAddress.text = kony.i18n.getLocalizedString("keyEStmtNoAddress");
			frmIBEStatementLanding.lblEStmtC2GAddressNote.setVisibility(false);
			isNoAddress = true;
		}
		glbEStmtC2G = "N,1";
		frmIBEStatementLanding.lblEStmtC2GAddress.setVisibility(true);
		frmIBEStatementLanding.lblEStmtC2GDefault.setVisibility(false);
		frmIBEStatementLanding.btnEStmtC2GRegisterAddr.skin = btnRegisterAddr;
		frmIBEStatementLanding.btnEStmtC2GOfficeAddr.skin = btnOfficeAddrGrey;
		frmIBEStatementLanding.btnEStmtC2GContactAddr.skin = btnContactAddrGrey;
		frmIBEStatementLanding.btnEStmtC2GOfficeAddr.hoverSkin = btnOfficeAddr;
		frmIBEStatementLanding.btnEStmtC2GContactAddr.hoverSkin = btnContactAddr;
		
	}else if(widgetName == "btnOfficeAddr"){
	
		if("undefined" !== typeof gblEStmtOfficeAddr){
			frmIBEStatementLanding.lblEStmtC2GAddressNote.setVisibility(true);
			frmIBEStatementLanding.lblEStmtC2GAddress.text = gblEStmtOfficeAddr;
			frmIBEStatementLanding.lblEStmtC2GAddressNote.text = kony.i18n.getLocalizedString("keyEStmtNoteForRegisteredAndOffice");
			frmIBEStatementConfirm.lblEStmtC2GAddrType.text = appendColon(kony.i18n.getLocalizedString("keyEStmtOfficeAddr"));
			frmIBEStatementConfirm.lblEStmtC2GAddress.text = gblEStmtOfficeAddr;
			isNoAddress = false;
			
		}else{
			frmIBEStatementLanding.lblEStmtC2GAddress.text = kony.i18n.getLocalizedString("keyEStmtNoAddress");
			frmIBEStatementLanding.lblEStmtC2GAddressNote.setVisibility(false);
			isNoAddress = true;
		}
		glbEStmtC2G = "N,2";
		frmIBEStatementLanding.lblEStmtC2GAddress.setVisibility(true);
		frmIBEStatementLanding.lblEStmtC2GDefault.setVisibility(false);
		frmIBEStatementLanding.btnEStmtC2GOfficeAddr.skin = btnOfficeAddr;
		frmIBEStatementLanding.btnEStmtC2GRegisterAddr.skin = btnRegisterAddrGrey;
		frmIBEStatementLanding.btnEStmtC2GContactAddr.skin = btnContactAddrGrey;
		frmIBEStatementLanding.btnEStmtC2GRegisterAddr.hoverSkin = btnRegisterAddr;
		frmIBEStatementLanding.btnEStmtC2GContactAddr.hoverSkin = btnContactAddr;
		
	}else{
	
		if("undefined" !== typeof gblEStmtContactAddr){
			frmIBEStatementLanding.lblEStmtC2GAddressNote.setVisibility(true);
			frmIBEStatementLanding.lblEStmtC2GAddress.text = gblEStmtContactAddr;
			frmIBEStatementLanding.lblEStmtC2GAddressNote.text = kony.i18n.getLocalizedString("keyEStmtNoteForContact");
			frmIBEStatementConfirm.lblEStmtC2GAddrType.text = appendColon(kony.i18n.getLocalizedString("keyEStmtContactAddr"));
			frmIBEStatementConfirm.lblEStmtC2GAddress.text = gblEStmtContactAddr;
			isNoAddress = false;
			
		}else{
			frmIBEStatementLanding.lblEStmtC2GAddress.text = kony.i18n.getLocalizedString("keyEStmtNoAddress");
			frmIBEStatementLanding.lblEStmtC2GAddressNote.setVisibility(false);
			isNoAddress = true;
		}
		glbEStmtC2G = "N,3";
		frmIBEStatementLanding.lblEStmtC2GAddress.setVisibility(true);
		frmIBEStatementLanding.lblEStmtC2GDefault.setVisibility(false);
		frmIBEStatementLanding.btnEStmtC2GContactAddr.skin = btnContactAddr;
		frmIBEStatementLanding.btnEStmtC2GOfficeAddr.skin = btnOfficeAddrGrey;
		frmIBEStatementLanding.btnEStmtC2GRegisterAddr.skin = btnRegisterAddrGrey;
		frmIBEStatementLanding.btnEStmtC2GOfficeAddr.hoverSkin = btnOfficeAddr;
		frmIBEStatementLanding.btnEStmtC2GRegisterAddr.hoverSkin = btnRegisterAddr;
		
	}
}

function checkNextButtonEnableEStatementLandingIB(){
	if(isOnlyPaper() && (gblEmailAddr == frmIBEStatementLanding.txtEStatementEmail.text)){
		verifyEStmtOption();
	}else{
		var emailText=frmIBEStatementLanding.txtEStatementEmail.text;
		if (validateEmail(emailText)) {
	       	verifyEStmtOption();
	    }else{
	    	disableEStmtNextButton();
	    	showAlertWithCallBack(kony.i18n.getLocalizedString("invalidEmail"), kony.i18n.getLocalizedString("info"),putFocusEStatementIBTextbox);
	    }
	}	
}

function onEditEmailKeyUpNextButtonEnableEStatementLandingIB(){
	var emailText=frmIBEStatementLanding.txtEStatementEmail.text;
	if (validateEmail(emailText)) {
       	verifyEStmtOption();
    }else{
    	disableEStmtNextButton();
    }
}

function putFocusEStatementIBTextbox(){
	frmIBEStatementLanding.txtEStatementEmail.setFocus(true);
}


function enableEStmtNextButton(){
	   	frmIBEStatementLanding.btnNext.setEnabled(true);
       	frmIBEStatementLanding.btnNext.skin="btnIB158";
       	frmIBEStatementLanding.btnNext.hoverSkin="btnIB158active";
	   	frmIBEStatementLanding.btnNext.focusSkin="btnIB158active";
}

function disableEStmtNextButton(){
	frmIBEStatementLanding.btnNext.setEnabled(false);
   	frmIBEStatementLanding.btnNext.skin="btnIB158disabled";
   	frmIBEStatementLanding.btnNext.focusSkin="btnIB158disabled";
   	frmIBEStatementLanding.btnNext.hoverSkin="btnIB158disabled";
}


function saveEStatementInSessionIB(){
	showLoadingScreenPopup();
	//"Promotions_CreditCard,keyReadyCash,keyCash2Go";
	glbEStmtCC = setupEStmtSessionParamIB(gblstmtAns[1]["EStmt_CC"], glbEStmtCC);
	glbEStmtRDC = setupEStmtSessionParamIB(gblstmtAns[2]["EStmt_RDC"], glbEStmtRDC);
	glbEStmtC2G = setupEStmtSessionParamIB(gblstmtAns[3]["EStmt_C2G"], glbEStmtC2G);
	

	var inputParam = {};
	
	if(isOnlyPaper()){
		inputParam["eMailEStatement"] = gblEmailAddr;
	}else{
		inputParam["eMailEStatement"] = gblEStatementEMail;
	}
	
	    inputParam["EStmtCC"] = glbEStmtCC;
	    inputParam["EStmtRDC"] = glbEStmtRDC;
	    inputParam["EStmtC2G"] = glbEStmtC2G;

	    kony.print("########### "+JSON.stringify(inputParam));
    
    
    invokeServiceSecureAsync("saveEStatementSession", inputParam, saveEStatementInSessionCallbackfunctionIB);
}

function saveEStatementInSessionCallbackfunctionIB(status, resulttable){
	if (status == 400) {
		dismissLoadingScreenPopup();
		if (resulttable["opstatus"] == "0") {
			//Show the Confirm Form
			frmIBEStatementConfirmPreShow();
		}
		else{
			showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
			return false;
		}
	}	
}

function setupEStmtSessionParamIB(defaultStmtAns, glbParam){
	if(glbParam == "" && defaultStmtAns == "Y"){
		
    	return defaultEmailOpts;
    }else if(glbParam == "" && defaultStmtAns != "Y"){
    	return defaultPaperOpts;
    }else{
    	return glbParam;
    }
}


function frmIBEStatementConfirmPreShow(){

	frmIBEStatementConfirm.hbxCreditCard.setVisibility(false);
	frmIBEStatementConfirm.hbxReadyCash.setVisibility(false);
	frmIBEStatementConfirm.hbxCash2Go.setVisibility(false);
	frmIBEStatementConfirm.btnEditEStatement.setVisibility(true);
	frmIBEStatementConfirm.hbxOtpBox.setVisibility(false);
	var txtEmail = gblEStatementEMail != "" ? txtEmail = gblEStatementEMail : txtEmail = frmIBEStatementLanding.txtEStatementEmail.text;
	frmIBEStatementConfirm.lblEStatementEmail.text = kony.i18n.getLocalizedString("email")+": "+txtEmail;
	
	if(glbAllowCreditCard){
		
			frmIBEStatementConfirm.hbxCreditCard.setVisibility(true);
			frmIBEStatementConfirm.hbxCreditCardProdHeader.setVisibility(true);
			frmIBEStatementConfirm.hbxCreditCardAddress.setVisibility(false);
			frmIBEStatementConfirm.lblEStmtCCAddress.setVisibility(false);
			
			var prodImg = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+gbleStmtProducts["haveCreditCardImg"]+""+"&modIdentifier=PRODICON";
			frmIBEStatementConfirm.imgProdEStmtCC.src = prodImg;
			if(glbEStmtCC == defaultEmailOpts){
				frmIBEStatementConfirm.lblEStmtCCSendby.text = kony.i18n.getLocalizedString("keyEStmtSendBy")+" "+kony.i18n.getLocalizedString("email");
			}else if(glbEStmtCC == defaultPaperOpts){
				frmIBEStatementConfirm.lblEStmtCCSendby.text = kony.i18n.getLocalizedString("keyEStmtSendBy")+" "+kony.i18n.getLocalizedString("keyEStmtPaper");
			}else{
			
				if(glbEStmtCC.indexOf("Y") > -1){
					frmIBEStatementConfirm.lblEStmtCCSendby.text = kony.i18n.getLocalizedString("keyEStmtSendBy")+" "+kony.i18n.getLocalizedString("email");
				}else{
					frmIBEStatementConfirm.lblEStmtCCSendby.text = kony.i18n.getLocalizedString("keyEStmtSendBy")+" "+kony.i18n.getLocalizedString("keyEStmtPaper");
				}
			
				frmIBEStatementConfirm.hbxCreditCardAddress.setVisibility(true);
				frmIBEStatementConfirm.lblEStmtCCAddress.setVisibility(true);
			}
		}
		
		if(glbAllowReadyCash) {
			
			frmIBEStatementConfirm.hbxReadyCash.setVisibility(true);
			frmIBEStatementConfirm.hbxReadyCashHeader.setVisibility(true);
			frmIBEStatementConfirm.hbxReadyCashAddress.setVisibility(false);
			frmIBEStatementConfirm.lblEStmtRDCAddress.setVisibility(false);
			
			var prodImg = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+gbleStmtProducts["haveReadyCashImg"]+""+"&modIdentifier=PRODICON";
			frmIBEStatementConfirm.imgProdEStmtRDC.src = prodImg;

			if(glbEStmtRDC == defaultEmailOpts){
				frmIBEStatementConfirm.lblEStmtRDCSendby.text = kony.i18n.getLocalizedString("keyEStmtSendBy")+" "+kony.i18n.getLocalizedString("email");
			}else if(glbEStmtRDC == defaultPaperOpts){
				frmIBEStatementConfirm.lblEStmtRDCSendby.text = kony.i18n.getLocalizedString("keyEStmtSendBy")+" "+kony.i18n.getLocalizedString("keyEStmtPaper");
			}else{
				if(glbEStmtRDC.indexOf("Y") > -1){
					frmIBEStatementConfirm.lblEStmtRDCSendby.text = kony.i18n.getLocalizedString("keyEStmtSendBy")+" "+kony.i18n.getLocalizedString("email");
				}else{
					frmIBEStatementConfirm.lblEStmtRDCSendby.text = kony.i18n.getLocalizedString("keyEStmtSendBy")+" "+kony.i18n.getLocalizedString("keyEStmtPaper");
				}
				frmIBEStatementConfirm.hbxReadyCashAddress.setVisibility(true);
				frmIBEStatementConfirm.lblEStmtRDCAddress.setVisibility(true);
				
			}
		}
		
		if(glbAllowCash2Go)
		{
			frmIBEStatementConfirm.hbxCash2Go.setVisibility(true);
			frmIBEStatementConfirm.hbxCash2GoHeader.setVisibility(true);
			frmIBEStatementConfirm.hbxCash2GoAddress.setVisibility(false);
			frmIBEStatementConfirm.lblEStmtC2GAddress.setVisibility(false);
			var prodImg = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+gbleStmtProducts["haveCash2GoImg"]+""+"&modIdentifier=PRODICON";
			frmIBEStatementConfirm.imgProdEStmtC2G.src = prodImg;
			
			if(glbEStmtC2G == defaultEmailOpts){
				frmIBEStatementConfirm.lblEStmtC2GSendby.text = kony.i18n.getLocalizedString("keyEStmtSendBy")+" "+kony.i18n.getLocalizedString("email");
			}else if(glbEStmtC2G == defaultPaperOpts){
				frmIBEStatementConfirm.lblEStmtC2GSendby.text = kony.i18n.getLocalizedString("keyEStmtSendBy")+" "+kony.i18n.getLocalizedString("keyEStmtPaper");
			}else{
			
				if(glbEStmtC2G.indexOf("Y") > -1){
					frmIBEStatementConfirm.lblEStmtC2GSendby.text = kony.i18n.getLocalizedString("keyEStmtSendBy")+" "+kony.i18n.getLocalizedString("email");
				}else{
					frmIBEStatementConfirm.lblEStmtC2GSendby.text = kony.i18n.getLocalizedString("keyEStmtSendBy")+" "+kony.i18n.getLocalizedString("keyEStmtPaper");
				}
				frmIBEStatementConfirm.hbxCash2GoAddress.setVisibility(true);
				frmIBEStatementConfirm.lblEStmtC2GAddress.setVisibility(true);
			}
		}
		gblestmt="genOTP"
		frmIBEStatementConfirm.show();

}

function isOnlyPaper(){
	var optionSelected = [glbEStmtCC, glbEStmtRDC, glbEStmtC2G];
	var haveProducts = [glbAllowCreditCard, glbAllowReadyCash, glbAllowCash2Go];
	var allowProductSize = 0;
	var selectedPaperSize = 0;
		for(var i=0; i < optionSelected.length; i++){
			if(haveProducts[i]){
				allowProductSize++;
				
				if(optionSelected[i].indexOf("N") > -1){
					selectedPaperSize++;
				}
			}
		}
		
		if(selectedPaperSize == allowProductSize){
			return true;
		}else{
				return false;
		}

}


function syncIBEStatement(){
	
	var currForm = kony.application.getCurrentForm();
	
	if("frmIBEStatementProdFeature" == currForm.id){
		frmIBEStatementProdFeaturePreShow();
	}
	
	if("frmIBEStatementTnC" == currForm.id){
		frmIBEStatementTnCPreShow();
	}


	if("frmIBEStatementLanding" == currForm.id){
	
		if(glbEStmtCC != defaultPaperOpts && glbEStmtCC != defaultEmailOpts && glbEStmtCC != "N,N"){
			onClickEStmtChooseAddressButtonIB(glbWidgetName[glbEStmtCC.split(",")[1]], "EStmtCC");
		}
		
		if(glbEStmtRDC != defaultPaperOpts && glbEStmtRDC != defaultEmailOpts && glbEStmtRDC != "N,N"){
			onClickEStmtChooseAddressButtonIB(glbWidgetName[glbEStmtRDC.split(",")[1]], "EStmtRDC");
		}
		
		if(glbEStmtC2G != defaultPaperOpts && glbEStmtC2G != defaultEmailOpts && glbEStmtC2G !="N,N"){
			onClickEStmtChooseAddressButtonIB(glbWidgetName[glbEStmtC2G.split(",")[1]], "EStmtC2G");
		}
	
	}

	
	if("frmIBEStatementConfirm" == currForm.id){
		var txtEmail = gblEStatementEMail != "" ? txtEmail = gblEStatementEMail : txtEmail = frmIBEStatementLanding.txtEStatementEmail.text;
		
		if(glbEStmtCC.indexOf("Y") > -1){
			frmIBEStatementConfirm.lblEStmtCCSendby.text = kony.i18n.getLocalizedString("keyEStmtSendBy")+" "+kony.i18n.getLocalizedString("email");
			frmIBEStatementConfirm.lblEStatementEmail.text = kony.i18n.getLocalizedString("email")+": "+txtEmail;
		}else{
			frmIBEStatementConfirm.lblEStmtCCSendby.text = kony.i18n.getLocalizedString("keyEStmtSendBy")+" "+kony.i18n.getLocalizedString("keyEStmtPaper");
		}
	
		if(glbEStmtRDC.indexOf("Y") > -1){
			frmIBEStatementConfirm.lblEStmtRDCSendby.text = kony.i18n.getLocalizedString("keyEStmtSendBy")+" "+kony.i18n.getLocalizedString("email");
			frmIBEStatementConfirm.lblEStatementEmail.text = kony.i18n.getLocalizedString("email")+": "+txtEmail;
		}else{
			frmIBEStatementConfirm.lblEStmtRDCSendby.text = kony.i18n.getLocalizedString("keyEStmtSendBy")+" "+kony.i18n.getLocalizedString("keyEStmtPaper");
		}
	
		if(glbEStmtC2G.indexOf("Y") > -1){
			frmIBEStatementConfirm.lblEStmtC2GSendby.text = kony.i18n.getLocalizedString("keyEStmtSendBy")+" "+kony.i18n.getLocalizedString("email");
			frmIBEStatementConfirm.lblEStatementEmail.text = kony.i18n.getLocalizedString("email")+": "+txtEmail;
		}else{
			frmIBEStatementConfirm.lblEStmtC2GSendby.text = kony.i18n.getLocalizedString("keyEStmtSendBy")+" "+kony.i18n.getLocalizedString("keyEStmtPaper");
		}
		
		
		if(glbEStmtCC != defaultPaperOpts && glbEStmtCC != defaultEmailOpts){
			onClickEStmtChooseAddressButtonIB(glbWidgetName[glbEStmtCC.split(",")[1]], "EStmtCC");
		}
		
		if(glbEStmtRDC != defaultPaperOpts && glbEStmtRDC != defaultEmailOpts){
			onClickEStmtChooseAddressButtonIB(glbWidgetName[glbEStmtRDC.split(",")[1]], "EStmtRDC");
		}
		
		if(glbEStmtC2G != defaultPaperOpts && glbEStmtC2G != defaultEmailOpts){
			onClickEStmtChooseAddressButtonIB(glbWidgetName[glbEStmtC2G.split(",")[1]], "EStmtC2G");
		}
		
		if(gblTokenSwitchFlag == true){
		   
		   frmIBEStatementConfirm.lblkeyOTPMsg.text = kony.i18n.getLocalizedString("keyPleaseEnterToken");
		}else{
		   frmIBEStatementConfirm.lblkeyOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
		}
	}
}
// for OTP validtion
function OTPValdatnestmt(text) {
	if(gblTokenSwitchFlag==false || gblTokenSwitchFlag=="0" || gblTokenSwitchFlag==""){
		if (text == null || text == '') {
		showAlert(kony.i18n.getLocalizedString("Receipent_alert_correctOTP"), kony.i18n.getLocalizedString("info"));
		return false;
		}
		var txtLen = text.length;
		if (txtLen > gblOTPLENGTH) {
			alert("OTP code should be maximum " + gblOTPLENGTH + " characters");
			return false;
		}
	}else{
		text = frmIBEStatementConfirm.tbxToken.text;
		if(text==""||text==null){
			showAlert(kony.i18n.getLocalizedString("Receipent_tokenId"), kony.i18n.getLocalizedString("info"));
			return false;
		}
		var isNum = kony.string.isNumeric(text);
		if (!isNum) {
			alert("Token should be numeric");
			return false;
		}
	}
	updateEStmtCompositServiceIB();
	return true;
}

function updateEStmtCompositServiceIB(){

 	var  tokenorOTP ;
 	showLoadingScreenPopup();
 	var inputParam = {};
    var locale = kony.i18n.getCurrentLocale();
    var txtEmail = frmIBEStatementLanding.txtEStatementEmail.text
    
    if(isOnlyPaper()){
		inputParam["eMailEStatement"] = gblEmailAddr;
	}else{
		inputParam["eMailEStatement"] = gblEStatementEMail;
	}
    
    var inputParam = {};
    	 if(!isOnlyPaper()){
		    if (gblTokenSwitchFlag == true){
		     	tokenorOTP = frmIBEStatementConfirm.tbxToken.text
		     	// TOKEN
		     	inputParam["verifyToken_loginModuleId"] = "IB_HWTKN";
		     	inputParam["verifyToken_userStoreId"] = "DefaultStore";
		        inputParam["verifyToken_retryCounterVerifyAccessPin"] = "0";
		        inputParam["verifyToken_retryCounterVerifyTransPwd"] = "0";
		        inputParam["verifyToken_userId"] = gblUserName;
		        
		        inputParam["verifyToken_sessionVal"] = "";
		        inputParam["verifyToken_segmentId"] = "segmentId";
		        inputParam["verifyToken_segmentIdVal"] = "MIB";
		        inputParam["verifyToken_channel"] = "Internet Banking";
		     	 
		    } else {
		    	tokenorOTP = frmIBEStatementConfirm.txtBxOTP.text;
		    	inputParam["verifyPwd_loginModuleId"] = "IBSMSOTP",
		    	 //OTP
		        inputParam["verifyPwd_retryCounterVerifyOTP"] = "0",
		        inputParam["verifyPwd_userId"] = gblUserName,
		        inputParam["verifyPwd_userStoreId"] = "DefaultStore",
		        inputParam["verifyPwd_segmentId"] = "MIB"
		    }
		    
		    inputParam["password"] = tokenorOTP;
	        inputParam["locale"] = locale;
	        inputParam["notificationAdd_appID"] = appConfig.appId;
	        
	        inputParam["TokenSwitchFlag"] = gblTokenSwitchFlag;
	        inputParam["eMailEStatement"] = gblEStatementEMail;
		    
		}else{
			inputParam["eMailEStatement"] = gblEmailAddr;
		}
        
	    inputParam["EStmtCC"] = glbEStmtCC;
	    inputParam["EStmtRDC"] = glbEStmtRDC;
	    inputParam["EStmtC2G"] = glbEStmtC2G;
        
        invokeServiceSecureAsync("eStmtModifyCompositeJavaService", inputParam, callBackUpdateEStmtCompJavaServiceIB);

}

function callBackUpdateEStmtCompJavaServiceIB(status,resulttable){
	if(status == 400){
		dismissLoadingScreenPopup();
		if(resulttable["opstatus"] == 0){
			if(resulttable["pointRedeemActBusinessHrsFlag"] == "false") 
			{
				var startTime = resulttable["pointRedeemStartTime"];
         		var endTime = resulttable["pointRedeemEndTime"];
         		var messageUnavailable = kony.i18n.getLocalizedString("keySoGooODServiceUnavailable");
         		messageUnavailable = messageUnavailable.replace("{start_time}", startTime);
         		messageUnavailable = messageUnavailable.replace("{end_time}", endTime);
               showAlertWithCallBack(messageUnavailable, kony.i18n.getLocalizedString("info"),onclickActivationCompleteStart);
               	return false;
			}
			else
			{
				frmIBEStatementComplete.hbxSuccess.setVisibility(true);
				frmIBEStatementComplete.hbxFail.setVisibility(false);
				frmIBEStatementComplete.lblEStmtSuccess.setVisibility(true);
				frmIBEStatementComplete.lblEStmtFailure.setVisibility(false);
			}
		}
		else if (resulttable["errCode"] == "VrfyTxPWDErr00001" || resulttable["errCode"] == "VrfyTxPWDErr00002") {
			setTransPwdFailedError(kony.i18n.getLocalizedString("invalidTxnPwd"));
		} 
        else if (resulttable["errCode"] == "VrfyTxPWDErr00003") {
			showTranPwdLockedPopup();
		}else if (resulttable["opstatus"] == 8005) {
            if (resulttable["errCode"] == "VrfyOTPErr00001") {
                gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
                frmIBEStatementConfirm.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");//kony.i18n.getLocalizedString("invalidOTP"); //
                frmIBEStatementConfirm.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
                frmIBEStatementConfirm.lblOTPinCurr.setVisibility(true);
				frmIBEStatementConfirm.lblPlsReEnter.setVisibility(true);
				frmIBEStatementConfirm.txtBxOTP.text="";
				frmIBEStatementConfirm.tbxToken.text="";
                //alert("" + kony.i18n.getLocalizedString("invalidOTP"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00002") {
                handleOTPLockedIB(resulttable);
                //startRcCrmUpdateProfilBPIB("04");
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00005") {
                alert("" + kony.i18n.getLocalizedString("KeyTokenSerialNumError"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00006") {
                gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
                alert("" + resulttable["errMsg"]);
                return false;
            }
            else if (resulttable["errCode"] == "GenOTPRtyErr00001") {
                    dismissLoadingScreenPopup();
                    showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                    return false;
              }
            else if (resulttable["errCode"] == "VrfyOTPErr00004") {
                alert("" + resulttable["errMsg"]);
                return false;
            }else{
              	alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
                return false;
            }
        } 
		
		else{
			frmIBEStatementComplete.hbxSuccess.setVisibility(false);
			frmIBEStatementComplete.hbxFail.setVisibility(true);
			frmIBEStatementComplete.lblEStmtSuccess.setVisibility(false);
			frmIBEStatementComplete.lblEStmtFailure.setVisibility(true);
		}
		frmIBEStatementComplete.show();
	}
}

function preOTPScreenestmt()
{	
	var txtEmail;
	
		txtEmail = frmIBEStatementLanding.txtEStatementEmail.text;
	
	
		if(isOnlyPaper() || gblEmailAddr==txtEmail){
				updateEStmtCompositServiceIB();
			}else{
		 		var locale = kony.i18n.getCurrentLocale();
					frmIBEStatementConfirm.hbxToken.setVisibility(false);
					frmIBEStatementConfirm.lblOTPinCurr.setVisibility(false);
					frmIBEStatementConfirm.lblPlsReEnter.setVisibility(false);
					frmIBEStatementConfirm.hbxOTPEntry.setVisibility(true);
					frmIBEStatementConfirm.hbxOtpBox.setVisibility(true);
					frmIBEStatementConfirm.txtBxOTP.setFocus(true);
					frmIBEStatementConfirm.hbxOTPRef.setVisibility(true);
					frmIBEStatementConfirm.hbxOTPsnt.setVisibility(true);
					frmIBEStatementConfirm.txtBxOTP.setFocus(true);
					//frmIBEStatementConfirm.hbxBtnNext.margin = [0,0,0,0];
					frmIBEStatementConfirm.btnEditEStatement.setVisibility(false);
			    	var inputParam = [];
			    	inputParam["crmId"] = gblcrmId;
			    	showLoadingScreenPopup();
			    	invokeServiceSecureAsync("tokenSwitching", inputParam, estmtCallbackfunction);
	    	}
}

function estmtCallbackfunction(status, callbackResponse) {
    if (status == 400) {
        if (callbackResponse["opstatus"] == 0) {
            if (callbackResponse["deviceFlag"].length == 0) {
                IBestmtGenereteOTP();
            }
            if (callbackResponse["deviceFlag"].length > 0) {
                var tokenFlag = callbackResponse["deviceFlag"][0]["TOKEN_DEVICE_FLAG"];
                var mediaPreference = callbackResponse["deviceFlag"][0]["MEDIA_PREFERENCE"];
                var tokenStatus = callbackResponse["deviceFlag"][0]["TOKEN_STATUS_ID"];

                if (tokenFlag == "Y" && (mediaPreference == "Token" || mediaPreference == "TOKEN" ) && tokenStatus=='02') {
                    gblTokenSwitchFlag = true;
                    IBTokenForestmt();
                } else {
                    gblTokenSwitchFlag = false;
                    IBestmtGenereteOTP();
                    gblTokenSwitchFlag = false;
                }
            }
        } else {
            dismissLoadingScreenPopup();
        }
    }
}

function IBTokenForestmt() {
    frmIBEStatementConfirm.hbxOTPEntry.setVisibility(false);
    frmIBEStatementConfirm.hbxOTPsnt.setVisibility(false);
    frmIBEStatementConfirm.lblkeyOTPMsg.text = kony.i18n.getLocalizedString("keyPleaseEnterToken");
    frmIBEStatementConfirm.hbxToken.setVisibility(true);
    frmIBEStatementConfirm.hbxOTPRef.isVisible=false;
    gblestmt = "verOTP";
    dismissLoadingScreenPopup();
    frmIBEStatementConfirm.tbxToken.text ="";
    frmIBEStatementConfirm.tbxToken.setFocus(true);
}


function IBestmtGenereteOTP()
{
		showLoadingScreenPopup();
		frmIBEStatementConfirm.lblOTPinCurr.text = "";
        frmIBEStatementConfirm.lblPlsReEnter.text = "";
		var inputParams = {};
		inputParams["Channel"] = "EStatementChangeEmail";
		inputParams["retryCounterRequestOTP"]=gblRetryCountRequestOTP;
	 	inputParams["locale"]=kony.i18n.getCurrentLocale();
	 
	 	//InputParms for activity logging
		var platformChannel = gblDeviceInfo.name;
		if (platformChannel == "thinclient")
			inputParams["channelId"] = GLOBAL_IB_CHANNEL;
		else
			inputParams["channelId"] = GLOBAL_MB_CHANNEL;
			inputParams["logLinkageId"] = "";
			inputParams["deviceNickName"] = "";
			inputParams["activityFlexValues2"] = "";
		invokeServiceSecureAsync("generateOTPWithUser", inputParams, callBackIBreqOTPIBestmt);
}

function callBackIBreqOTPIBestmt(status, callBackResponse) {
	if (status == 400) {
			if (callBackResponse["errCode"] == "GenOTPRtyErr00002") {
				//alert("Test1");
				dismissLoadingScreenPopup();
				showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00002"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (callBackResponse["errCode"] == "JavaErr00001") {
				//alert("Test2");
				dismissLoadingScreenPopup();
				showAlertIB(kony.i18n.getLocalizedString("ECJavaErr00001"), kony.i18n.getLocalizedString("info"));
				return false;
			}else if (callBackResponse["errCode"] == "GenOTPRtyErr00001") {
                    dismissLoadingScreenPopup();
                    showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                    return false;
              }
			
			else if (callBackResponse["errCode"] == "GenOTPRtyErr00001") {
                    dismissLoadingScreenPopup();
                    showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                    return false;
           	}
			if (callBackResponse["opstatus"] == 0) {
			
			//Resetting the OTP screen
			frmIBEStatementConfirm.hbxOTPEntry.setVisibility(true);
		    frmIBEStatementConfirm.hbxOTPsnt.setVisibility(true);
		    frmIBEStatementConfirm.lblkeyOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
		    frmIBEStatementConfirm.hbxOTPRef.isVisible=true;
		    frmIBEStatementConfirm.hbxToken.setVisibility(false);
		    gblTokenSwitchFlag=false;
		    dismissLoadingScreenPopup();
		    //frmIBEStatementConfirm.tbxToken.text ="";
		    //frmIBEStatementConfirm.tbxToken.setFocus(true);
    		//Finish reset here

			//gblOTPdisabletime = kony.os.toNumber(callBackResponse["requestOTPEnableTime"]) * 100;
			gblRetryCountRequestOTP = kony.os.toNumber(callBackResponse["retryCounterRequestOTP"]);
			gblOTPLENGTH = kony.os.toNumber(callBackResponse["otpLength"]);
			
			var reqOtpTimer = kony.os.toNumber(callBackResponse["requestOTPEnableTime"]);
			kony.timer.schedule("OtpTimerestmt", IBOTPTimercallbackestmt, reqOtpTimer, false);
			   
        	    	
          	gblOTPReqLimit=gblOTPReqLimit+1;
          //	setTimeout('OTPTimercallbackOpenAccount()',reqOtpTimer);

			var refVal="";
			for(var d=0;d<callBackResponse["Collection1"].length;d++){
				if(callBackResponse["Collection1"][d]["keyName"] == "pac"){
						refVal=callBackResponse["Collection1"][d]["ValueString"];
						break;
					}
				}
					
				gblestmt="verOTP";	 			
		   		frmIBEStatementConfirm.btnOTPReq.skin = btnIBREQotp;
            	frmIBEStatementConfirm.btnOTPReq.focusSkin = btnIBREQotp;
            	frmIBEStatementConfirm.btnOTPReq.onClick = ""; 
            	frmIBEStatementConfirm.lblBankRefVal.text = refVal;			
				//curr_form = kony.application.getCurrentForm();
				frmIBEStatementConfirm.txtBxOTP.maxTextLength = gblOTPLENGTH;
				frmIBEStatementConfirm.txtBxOTP.text = "";
				frmIBEStatementConfirm.lblOTPMblNum.text =" " + maskingIB(gblPHONENUMBER);
				dismissLoadingScreenPopup();
		} else {
			dismissLoadingScreenPopup();
			alert("Error " + callBackResponse["errMsg"]);
		}
	} else {
		if (status == 300) {
			dismissLoadingScreenPopup();
			alert("Error");
		}
	}
}

function IBOTPTimercallbackestmt(){
	frmIBEStatementConfirm.btnOTPReq.skin = btnIBREQotpFocus;
	frmIBEStatementConfirm.btnOTPReq.focusSkin = btnIBREQotpFocus;
	frmIBEStatementConfirm.btnOTPReq.onClick = IBestmtGenereteOTP;
	
	try {
		kony.timer.cancel("OtpTimerestmt");
	} catch (e) {
		
	}
}

function checkCustStatuseStmt()
{
     	if(getCRMLockStatus())
		{
			curr_form=kony.application.getCurrentForm().id;
			dismissLoadingScreenPopup();
			popIBBPOTPLocked.show();
		}
		else 
		{
			checkestmtBousinessHrs();
		}
}

function checkestmtBousinessHrs(){
	var input_param = {};
    invokeServiceSecureAsync("pointRedeemBousinessHrs", input_param, checkpointestmtBousinessHrsCallBack);
}

function checkpointestmtBousinessHrsCallBack(status,result){
       if(status == 400){
              if(result["opstatus"] == 0){
              gbleStmtProducts = result;
                     var serviceHrsFlag = result["pointRedeemActBusinessHrsFlag"];
                     dismissLoadingScreenPopup();
                     if(serviceHrsFlag=="true"){
                     	   GLOBAL_E_STATEMENT_PRODUCTS_STR=result["E_STATEMENT_PRODUCTS"];
                     if(GLOBAL_E_STATEMENT_PRODUCTS_STR!=null && GLOBAL_E_STATEMENT_PRODUCTS_STR!="" && GLOBAL_E_STATEMENT_PRODUCTS_STR.length>0)
                     {
                     	 GLOBAL_E_STATEMENT_PRODUCTS = [];
                     	 var estmtProdArray = GLOBAL_E_STATEMENT_PRODUCTS_STR.split(",");
	                     for(i=0;i<estmtProdArray.length;i++)
	                     {
	                     	GLOBAL_E_STATEMENT_PRODUCTS.push(estmtProdArray[i]);
	                     }
                     }
                     GLOBAL_E_STATEMENT_IMAGES_STR=result["E_STATEMENT_IMAGES"];
                      if(GLOBAL_E_STATEMENT_IMAGES_STR!=null && GLOBAL_E_STATEMENT_IMAGES_STR!="" && GLOBAL_E_STATEMENT_IMAGES_STR.length>0)
                     {
                     	 GLOBAL_E_STATEMENT_IMAGES = [];
                     	 var estmtProdArray = GLOBAL_E_STATEMENT_IMAGES_STR.split(",");
	                     for(i=0;i<estmtProdArray.length;i++)
	                     {
	                     	GLOBAL_E_STATEMENT_IMAGES.push(estmtProdArray[i]);
	                     }
                     }
                           if(kony.application.getCurrentForm().id != "frmIBEStatementProdFeature"){
                                  frmIBEStatementProdFeature.show();
                           }else{
                                  frmIBEStatementProdFeature.postShow();
                           }
                           
                     }else{
                 		var startTime = result["pointRedeemStartTime"];
                 		pntRedeem_StartTime=result["pointRedeemStartTime"]
                 		var endTime = result["pointRedeemEndTime"];
                 		pntRedeem_EndTime=result["pointRedeemEndTime"];
                 		var messageUnavailable = kony.i18n.getLocalizedString("keySoGooODServiceUnavailable");
                 		messageUnavailable = messageUnavailable.replace("{start_time}", startTime);
                 		messageUnavailable = messageUnavailable.replace("{end_time}", endTime);
                 		showAlertWithCallBack(messageUnavailable, kony.i18n.getLocalizedString("info"),onclickActivationCompleteStart);
                        return false;
                     }                    
              }else{
                    	dismissLoadingScreenBasedOnChannel(channel);
						if (resulttable["errMsg"] != null || resulttable["errMsg"] != "") {
								showAlert("Sorry, System found error : " + resulttable["errMsg"], kony.i18n.getLocalizedString("info") );
								return false;
						}else{
								showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
								return false;
						}
              }
       }
}
