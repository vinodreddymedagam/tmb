/**
  This file has utility functions to manage the cache used in the app. 
*/


//To initialise the caches on pre app init of the app.
function intiliseCacheAtStartup(){
  
  if(isCacheEmpty("gblProfilePicSavedVersion"))
    setCacheData("gblProfilePicSavedVersion",0);
  
  if(isCacheEmpty("12_Hour_Refresh_BillerCache"))
    setCacheData("12_Hour_Refresh_BillerCache", Math.round(new Date().getTime()/1000));
 
}

//Purge cache at logout
function removeCacheAtLogout(){
  
}


//Cache Getter
function getCacheData(cacheName){
  return kony.store.getItem(cacheName);
}

//Cache Setter
function setCacheData(cacheName, cacheData){
  kony.store.setItem(cacheName,cacheData);
}

//Cache empty check
function isCacheEmpty(cacheName){
  if( kony.store.getItem(cacheName) === null || kony.store.getItem(cacheName) === undefined )
    return true;
  else 
    return false;
}


//Purge the cache
function removeCache(cacheName){
  if( kony.store.getItem(cacheName) !== null ){
    kony.store.removeItem(cacheName);
  }
}

//Purge the caches on activation
function removeCacheOnActivation(){
  removeCache("TransferRecipientsCache");
  removeCache("BillerFlowCache");
  removeCache("MasterBillerInqCacheLength");
  removeCache("24_Hour_Refresh_MasterBillerCache");
  removeCache("billerCategoryInquiryCache");
  purgeMasterBillerCache();
}

function setMasterBillerBatchCache(responseData){ 
  try{
    setCacheData("MasterBillerInqCacheLength", responseData.length);

    searchString_masterBillerCache_KeyValue = {};

    for (var i = 0; i < responseData.length; i++) { 
      setCacheData("MasterBillerInqCache"+i, responseData[i]);
      var searchValue = "";
      if(isNotBlank(responseData[i]["BillerNameEN"]))
        searchValue += responseData[i]["BillerNameEN"].toLowerCase()+ "~";
      if(isNotBlank(responseData[i]["BillerNameTH"]))
        searchValue += responseData[i]["BillerNameTH"].toLowerCase()+ "~";
      if(isNotBlank(responseData[i]["BillerTaxID"]))
        searchValue += responseData[i]["BillerTaxID"].toLowerCase()+ "~";
      if(isNotBlank(responseData[i]["BillerCompcode"]))
        searchValue += responseData[i]["BillerCompcode"].toLowerCase()+ "~";

      searchString_masterBillerCache_KeyValue[searchValue] = "MasterBillerInqCache"+i; //Key value pair of biller search string with biller id.
    }
    setCacheData("searchString_masterBillerCache_KeyValue", searchString_masterBillerCache_KeyValue);
  }catch(e){
    kony.print("error in  setMasterBillerBatchCache" + e);
  }
}
function purgeMasterBillerCache(){ 
  var i = 0;
  while(!isCacheEmpty("MasterBillerInqCache"+i)) { 
    removeCache("MasterBillerInqCache"+i);
    i++;
  }
}
