function frmGetNewLoanInit(){
  frmGetNewLoan.flxCreditCards.onClick = onClickCreditCards;
  frmGetNewLoan.flxPersonalLoan.onClick = onClickPersonalLoan;
  frmGetNewLoan.btnMenuIcon.onClick = onClickMenuOnLoanModule;
  frmGetNewLoan.preShow = preShowFrmGetNewLoan;
  frmGetNewLoan.postShow = postShowFrmGetNewLoan;
  frmGetNewLoan.onDeviceBack = disableBackButton;
}

function onClickMenuOnLoanModule(){
  kony.i18n.setCurrentLocaleAsync(gblCurrentLocaleBeforeLoan, function (){handleMenuBtn();}, onFailureLocaleChange, "");
  
}
//Preshow
function preShowFrmGetNewLoan() {
	frmGetNewLoan.lblCreditCard.opacity = 0;
	frmGetNewLoan.lblPersonalLoan.opacity = 0;
	frmGetNewLoan.imghand.opacity = 0;
  	frmGetNewLoan.imgCreditCards1.centerX = "150%";
  	frmGetNewLoan.imgCreditCards2.centerX = "150%";
  	frmGetNewLoan.imbCreditCards3.centerX = "150%";
  	frmGetNewLoan.imgCircle1.centerX = "150%";
  	frmGetNewLoan.imgCircle2.centerX = "150%";
  	frmGetNewLoan.imgCircle3.centerX = "150%";
    frmGetNewLoan.lblLoanHeader.text = kony.i18n.getLocalizedString("LoanTitle");
    frmGetNewLoan.lblCreditCard.text =  kony.i18n.getLocalizedString("CC_Apply");
  	frmGetNewLoan.lblPersonalLoan.text =  kony.i18n.getLocalizedString("PL_Apply");
}
  	function Animate_Labels1() {
        var trans101 = kony.ui.makeAffineTransform();
        trans101.scale(1, 1);
      	if(citizenValidation()){
          frmGetNewLoan.flxPersonalLoan.setVisibility(true);
          frmGetNewLoan.lblPersonalLoan.animate(
            kony.ui.createAnimation({"100":{"stepConfig":{"timingFunction":kony.anim.EASE}, "opacity":1, "transform":trans101}}),
            {"delay":3.25,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.5}
          );
          frmGetNewLoan.imghand.animate(
            kony.ui.createAnimation({"100":{"stepConfig":{"timingFunction":kony.anim.EASE}, "opacity":1, "transform":trans101}}),
            {"delay":2.75,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.5}
          );
        }else{
          frmGetNewLoan.flxPersonalLoan.setVisibility(false);
        }
        frmGetNewLoan.lblCreditCard.animate(
          kony.ui.createAnimation({"100":{"stepConfig":{"timingFunction":kony.anim.EASE}, "opacity":1, "transform":trans101}}),
          {"delay":0.5,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.5}
        );  
    }
function postShowFrmGetNewLoan() {
  


    var trans100 = kony.ui.makeAffineTransform();
    trans100.scale(0.5, 0.5);
	frmGetNewLoan.lblCreditCard.animate(
      kony.ui.createAnimation({"100":{"stepConfig":{"timingFunction":kony.anim.EASE}, "transform":trans100}}),
      {"delay":0,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.1}
    );
    if(citizenValidation()){
      frmGetNewLoan.flxPersonalLoan.setVisibility(true);
      frmGetNewLoan.lblPersonalLoan.animate(
        kony.ui.createAnimation({"100":{"stepConfig":{"timingFunction":kony.anim.EASE}, "transform":trans100}}),
        {"delay":0,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.1}
      );

      frmGetNewLoan.imghand.animate(
        kony.ui.createAnimation({"100":{"stepConfig":{"timingFunction":kony.anim.EASE}, "transform":trans100}}),
        {"delay":0,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.1}
      );
      frmGetNewLoan.imgCircle1.animate(
        kony.ui.createAnimation({"100":{"stepConfig":{"timingFunction":kony.anim.EASE}, "centerX":"50%"}}),
        {"delay":2,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.5}
      );
      frmGetNewLoan.imgCircle2.animate(
        kony.ui.createAnimation({"100":{"stepConfig":{"timingFunction":kony.anim.EASE}, "centerX":"52%"}}),
        {"delay":2.25,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.5}
      );
      frmGetNewLoan.imgCircle3.animate(
        kony.ui.createAnimation({"100":{"stepConfig":{"timingFunction":kony.anim.EASE}, "centerX":"54%"}}),
        {"delay":2.5,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.5}
      );
    }else{
          frmGetNewLoan.flxPersonalLoan.setVisibility(false);
    }
    frmGetNewLoan.imgCreditCards1.animate(
        kony.ui.createAnimation({"100":{"stepConfig":{"timingFunction":kony.anim.EASE}, "centerX":"50%"}}),
        {"delay":0,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.5},
        {"animationEnd":Animate_Labels1 }
      );
    frmGetNewLoan.imgCreditCards2.animate(
      kony.ui.createAnimation({"100":{"stepConfig":{"timingFunction":kony.anim.EASE}, "centerX":"50%"}}),
      {"delay":0.25,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.5}
    );
    frmGetNewLoan.imbCreditCards3.animate(
      kony.ui.createAnimation({"100":{"stepConfig":{"timingFunction":kony.anim.EASE}, "centerX":"50%"}}),
      {"delay":0.5,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.5}
    );
}

//OnClick Creditcard
function onClickCreditCards() {
  if(citizenValidation()){
    if(validateAge(20, 65, kony.i18n.getLocalizedString("Age_Err_NotEligible_CC"))){
      getLoanSubmissionData("26");
    }
  }else{
    openCallMeNowPage("CC",false);
  }
}

//OnClick Loan
function onClickPersonalLoan() {
  if(citizenValidation()){
    if(validateAge(20, 59, kony.i18n.getLocalizedString("Age_Err_NotEligible_PL"))){
      getLoanSubmissionData("27");
    }
  }else{
    alert(kony.i18n.getLocalizedString("CI_Err_NotEligible"));
    if(gblDeeplinkExternalAccess){
      //frmAccountSummaryLanding.show();
      showAccountSummaryNew();
      gblDeeplinkExternalAccess = false;
    }
    return false;
  }
}
function validateAge(minAge, maxAge, errorMessage){
  	var customerAge = getAge(gblCustomerAge);
    if(customerAge < minAge || customerAge >maxAge){
    	alert(errorMessage);
      	 if(gblDeeplinkExternalAccess) {
           //frmAccountSummaryLanding.show();
           showAccountSummaryNew();
           gblDeeplinkExternalAccess = false;
         }
    	return false;
  	}else{
      	if(customerAge == maxAge){
          var today = new Date();
          var dateSprit = gblCustomerAge.split("/");
          var birthDate = new Date(dateSprit[2],parseInt(dateSprit[1]) -1,dateSprit[0]);
          var age = today.getFullYear() - birthDate.getFullYear();
          var m = today.getMonth() - birthDate.getMonth();
          if(m < 0 || (m === 0 && today.getDate() > birthDate.getDate())){
            alert(errorMessage);
            if(gblDeeplinkExternalAccess) {
              //frmAccountSummaryLanding.show();
              showAccountSummaryNew();
              gblDeeplinkExternalAccess = false;
            }
            return false;
          }else{
            return true;
          }
        }else{
          return true;
        }
  	}
}
function getAge(dateString) {
    var today = new Date();
  	var dateSprit = dateString.split("/");
    var birthDate = new Date(dateSprit[2],parseInt(dateSprit[1]) -1,dateSprit[0]);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    if(!isNaN(age)){
    	return age;
    }else{
    	return 0;
    }
}

function getLoanSubmissionData(appType){
  	showLoadingScreen();
  	var inputparam = {};
	inputparam["categoryCode"] = appType;
  	gblLoanAppType = appType;
	invokeServiceSecureAsync("loanStatusTrackingDetails", inputparam, loanSubmissionTrackingCallBack);
}

function loanSubmissionTrackingCallBack(status, resulttable){
  gblLoanApplicationList = [];
  if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			for(i = 0; i < resulttable["loanDetailsInfo"].length;i++){
              gblLoanApplicationList.push(resulttable["loanDetailsInfo"][i]);
            }
          
            if(gblDeeplinkExternalAccess && isNotBalnk(gblContentDeeplinkData)){
                var urlSchema = gblContentDeeplinkData["launchparams"];
                var action = urlSchema["eventActions"];
                if (action == "tmbCash2GoLoanDetails") {
                      deepLinkCash2GoDetails(gblLoanApplicationList);
                  	  gblDeeplinkExternalAccess = false;
                      gblContentDeeplinkData = "";
                      dismissLoadingScreen();
                  	  return;
                }
            } else if(gbl3dTouchAction=="tmbCash2GoLoanDetails") {
              		deepLinkCash2GoDetails(gblLoanApplicationList);
              		gbl3dTouchAction = "";
                    dismissLoadingScreen();
                  	return;
            }
          
            if(gblLoanAppType == "26"){
              showCreditCardProducts();
            }else if(gblLoanAppType == "27"){
              showPersonalLoanProducts();
            }
          	
          	dismissLoadingScreen();
        }else{
          dismissLoadingScreen();
          alert(kony.i18n.getLocalizedString("ECGenericError"));
        }
  }else{
    dismissLoadingScreen();
  	alert(kony.i18n.getLocalizedString("ECGenericError"));
  }
  
}

//Loan Application Staus
function frmLoanApplicationStatusInit(){
  frmLoanApplicationStatus.btnHome.onClick = showAccountSummaryFromMenu;
  frmLoanApplicationStatus.btnBack.onClick = onBackfrmLoanApplicationStatus;
  frmLoanApplicationStatus.preShow = frmLoanApplicationStatusPreshow;
  if (gblDeviceInfo["name"] == "android"){
    frmLoanApplicationStatus.onDeviceBack = disableBackButton;
  }
}
  
function frmLoanApplicationStatusPreshow(){
  frmLoanApplicationStatus.lblHeader.text = kony.i18n.getLocalizedString("AppStatus_Title");
  frmLoanApplicationStatus.btnBack.text = kony.i18n.getLocalizedString("Back");
  frmLoanApplicationStatus.brwLoanStatus.onSuccess = dismissLoadingScreen;
  frmLoanApplicationStatus.brwLoanStatus.onFailure = dismissLoadingScreen;
}

function onBackfrmLoanApplicationStatus(){
  frmGetNewLoan.show();
  frmLoanApplicationStatus.destroy();
}

function checkLoanStatus(){
  try{
    kony.print("@@@ In checkLoanStatus() @@@");
    kony.print("@@@ gblLoanStatusURL:::"+gblLoanStatusURL);
    kony.print("@@@ gblLoanStatusEncryptType:::"+gblLoanStatusEncryptType);
    kony.print("@@@ gblEncryptedCitizenID:::"+gblEncryptedCitizenID);
	kony.print("@@@ gblPHONENUMBER:::"+gblPHONENUMBER+"|||Encrypt:::"+gblEncryptedPhoneNumber);
    var langVal = "";
    showLoadingScreen();
    if(kony.i18n.getCurrentLocale() == "en_US"){
      langVal = "EN";
    }else if(kony.i18n.getCurrentLocale() == "th_TH"){
      langVal = "TH";
    }
    if(isNotBlank(gblLoanStatusURL) && isNotBlank(gblLoanStatusEncryptType) && isNotBlank(gblEncryptedCitizenID) && isNotBlank(gblEncryptedPhoneNumber)){
      kony.print("---- Loan Status Details Available -----");
      var headersConf = {
        "Touch-App": gblLoanStatusEncryptType,
        "Touch-Ref1": gblEncryptedCitizenID,
        "Touch-Ref2": gblEncryptedPhoneNumber,
        "Touch-Lang": langVal
      };
      frmLoanApplicationStatus.brwLoanStatus.requestURLConfig = {
        URL: gblLoanStatusURL,
        requestMethod: constants.BROWSER_REQUEST_METHOD_GET,
        headers: headersConf
      };
      frmLoanApplicationStatus.show();
    }
  }catch(e){
    kony.print("@@@ In checkLoanStatus Exception:::"+e);
  }
}


function deepLinkCash2GoDetails(resultLoanProducts) {
  setDatatoLoansProductList();
  for (var i = 0; i < resultLoanProducts.length; i++) {
          if (resultLoanProducts[i]["productCode"] == "C2G01") {
              if (resultLoanProducts[i]["currentLoanStatus"] != 2) {
					 alert(kony.i18n.getLocalizedString("Dup_productcode"));
                     if(gblDeeplinkExternalAccess) {
                       //frmAccountSummaryLanding.show();
                       showAccountSummaryNew();
                       gblDeeplinkExternalAccess = false;
                     }
              } else {
					setDatatoProductDetail(resultLoanProducts[i]["productCode"]);
              }
            break;
          }
    }
}

function citizenValidation(){
  var isCitizenAvail = true;
  if (gblCustomerIDType != "CI") {
    isCitizenAvail = false;
  }else{
   	 return isCitizenAvail; 
  }
}