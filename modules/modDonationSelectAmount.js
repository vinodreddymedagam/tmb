//Type your code here
gblTaxDec = "T";
function frmDonationSelectAmountInit() {
    frmDonationSelectAmount.btnMore.onClick = showFoundationDetails;
    frmDonationSelectAmount.btnLess.onClick = hideFoundationDetails;
    frmDonationSelectAmount.preShow = frmDonationSelectAmountPreshow;
    frmDonationSelectAmount.btnBack.onClick = frmDonationSelectAmount_btnBack_Action;
    frmDonationSelectAmount.btnConfirm.onClick = onClickDonateAfterSelectAmount;
    frmDonationSelectAmount.postShow = frmDonationSelectAmountPostshow;
    frmDonationSelectAmount.btnSetSchedule.onClick =  onClickDonationOption;
    frmDonationSelectAmount.btnDonateOnce.onClick =  onClickDonationOption;
    frmDonationSelectAmount.btn1.onClick = onClickDonationAmount;
  	frmDonationSelectAmount.btn2.onClick = onClickDonationAmount;
  	frmDonationSelectAmount.btn3.onClick = onClickDonationAmount;
  	frmDonationSelectAmount.btn4.onClick = onClickDonationAmount;
  	frmDonationSelectAmount.btn5.onClick = onClickDonationAmount;
  	frmDonationSelectAmount.btn6.onClick = onClickDonationAmount;
    frmDonationSelectAmount.flexDeclareTax.onClick = onClickDeclareCheckbox;
    frmDonationSelectAmount.txtAmount.onDone = frmDonationSelectAmount_txtAmount_onDone;   
    frmDonationSelectAmount.txtAmount.onBeginEditing = onBenginEditingDonationAmt;
    frmDonationSelectAmount.txtAmount.onTextChange = ontextChangeDonationAmount;
    frmDonationSelectAmount.txtAmount.onEndEditing = frmDonationSelectAmount_txtAmount_onDone;
    frmDonationSelectAmount.onDeviceBack = doNothing;   
}

function onBenginEditingDonationAmt(){
	 var amt = frmDonationSelectAmount.txtAmount.text;
  if(amt.length == 0){
    frmDonationSelectAmount.btnConfirm.skin = "sknBtnLightGreyDisableSKin";
   	frmDonationSelectAmount.btnConfirm.focusSkin = "sknBtnLightGreyDisableSKin";
 }
 frmDonationSelectAmount.flexDonationAmtTextBox.skin = "sknFlexBoxBGFocus";
 setDefaultSkinToDonationAmt(); 
}
function frmDonationSelectAmount_btnBack_Action(){
    kony.print("gbleDonationType on back---"+gbleDonationType)
  	if(gbleDonationType == "QR_Type"){
		showQRScanningLanding();
    }else if(gbleDonationType == "Barcode_Type"){
        frmDonationSelectAmount.txtAmount.text = "";
        setDefaultSkinToDonationAmt();
		gotoBillpaymentLandingScreen();
    }
}

function gotoBillpaymentLandingScreen(){
  gbleDonationType = "";
  if(gblQRPayData["QR_TransType"]== "QR_Bill_Payment"){
      invokeQRPaymentDefaultAccntServiceEdonation();
  } 
  callBillPaymentFromMenu();
}
function frmDonationSelectAmount_txtAmount_onDone()
{
  frmDonationSelectAmount.flexDummy.setFocus(true);
  frmDonationSelectAmount.txtAmount.text = onDoneEditingAmountValue(frmDonationSelectAmount.txtAmount.text);
  donationAmountOnDone();
}
function onClickDonateAfterAmount(){
  	showLoadingScreen();
  	kony.print("initial log");
  	//
  	if(gblQRPayData["QR_TransType"]== "QR_Bill_Payment"){
      invokeQRPaymentDefaultAccntServiceEdonation();
    }  	
}

function checkDailyLimitForDonation(){
  showLoadingScreen();
  var inputParams = {};
  invokeServiceSecureAsync("crmProfileInq", inputParams, checkDailyLimitForDonationCallback);
}
function checkToggleAmountSelect(){
  var isToggleAmtSelected = false;
  for(var i=1;i<7;i++){
    if(frmDonationSelectAmount["btn"+i].skin == "btnWhiteFont160BlueBG"){
    isToggleAmtSelected = true;
  }
  }
  return isToggleAmtSelected;
}

function checkDailyLimitForDonationCallback(status, resulttable){
                        var DailyLimit = resulttable["ebMaxLimitAmtCurrent"];
						DailyLimit = parseFloat(DailyLimit)
						if (resulttable["ebAccuUsgAmtDaily"] == "" && resulttable["ebAccuUsgAmtDaily"].length == 0) {
							UsageLimit = 0;
						} else
							UsageLimit = parseFloat(resulttable["ebAccuUsgAmtDaily"]);						
						var amtVal ="" ;
                        if(checkToggleAmountSelect()){
                          amtVal =  kony.string.replace(gblEDonation["amount"], ",", "");
                        }else{
                          amtVal = kony.string.replace(frmDonationSelectAmount.txtAmount.text, ",", "");
                        }
                     
  						amtVal = kony.string.replace(amtVal, kony.i18n.getLocalizedString("currencyThaiBaht"), "");
 						var transferAmt = parseFloat(amtVal);
                      	kony.print("transferAmt>>"+transferAmt);
						kony.print("DailyLimit>>"+DailyLimit);
                      	kony.print("UsageLimit>>"+UsageLimit);
                        var channelLimit = DailyLimit - UsageLimit;
                        kony.print("channelLimit>>"+channelLimit);
						if (channelLimit < 0){
							channelLimit = 0; 
						}
						var dailylimitExceedMsg =  "";
                     
								//Display DailyLimit
								if (transferAmt > channelLimit){
                                      	dismissLoadingScreen();
                                        
                                        var alertMsg = kony.i18n.getLocalizedString("MIB_BPExcDaily").replace("{remaining_limit}", commaFormatted(fixedToTwoDecimal(channelLimit)));
                                      	showAlert(alertMsg, kony.i18n.getLocalizedString("info"));
                                     	return false;
								}else{
                                     onClickDonateAfterAmount();
                                }								
							
}
function clearDonationAmountandSetFocus(){
  frmDonationSelectAmount.txtAmount.text = "";
  frmDonationSelectAmount.txtAmount.setFocus(true);
}
function onClickDonateAfterSelectAmount(){
  if(frmDonationSelectAmount.btnConfirm.skin != "sknBtnLightGreyDisableSKin"){
      var amtVal = kony.string.replace(frmDonationSelectAmount.txtAmount.text, ",", "");
      amtVal = kony.string.replace(amtVal, kony.i18n.getLocalizedString("currencyThaiBaht"), "");
      var donateAmt = parseFloat(amtVal);
      var maxLimit = gblEDMaxLimitAmt;
      kony.print("gblEDonationMaxLimiAmount>>>>>"+maxLimit);
      kony.print("donateAmt>>>>>"+donateAmt);
      if(maxLimit == undefined){
      	var maxLimit = 1000000;
      }
       
      // Below condition is to check the max transaction limit when the user enters amount manually
      if(donateAmt>0){
		checkDailyLimitForDonation();
		/*
        if(donateAmt <= maxLimit){
         	checkDailyLimitForDonation();
      	}else{
         	var alertMsg = kony.i18n.getLocalizedString("MIB_BPExcTxnLimit").replace("{transaction_limit}", commaFormatted(maxLimit+""));
         	showAlertWithCallBack(alertMsg, kony.i18n.getLocalizedString("info"),clearDonationAmountandSetFocus);
         	return false;
      	}
      	*/
      }else{// This else for amount toggle
        checkDailyLimitForDonation();
      }
  }
  
}
// function onClickeDonateAmount(){
//    if(gbleDonationType == "QR_Type"){
//      onClickDonateAfterAmount_QR();
//    }else{
//         onClickDonateAfterAmount();
//    }
// }

function billPaymentValidationForEDonation() {
    inputParam = {};
    var frmAcct = "";
    var fromAcctID = gblQRPayData["fromAcctID"];

    frmAcct = fromAcctID.replace(/-/g, "");
    inputParam["EDonationFlag"] = "Y";
    inputParam["toAccountNo"] = gblQRPayData["toAcctIDNoFormat"];
    inputParam["billerMethod"] = "0";
    inputParam["fromAccountNo"] = frmAcct;
    inputParam["ref1"] = gblQRPayData["ref1"];
  //TODO EDonation
    if(gbleDonationType == "Barcode_Type"){
      	inputParam["EDonationFlag"] = "Y";
    }else{
    	inputParam["QRFlag"] = "Y";
    }
    if(gblTaxDec == "T"){
	    inputParam["ref2"] = gblCustomerIDValue;
	}else{
	    inputParam["ref2"] = "0";
	}
   if(gbleDonationType == "Barcode_Type"){
     if(frmDonationSelectAmount.txtAmount.text != ""){
      gblEDonation["amount"] = frmDonationSelectAmount.txtAmount.text;
     }
    }
  	
    inputParam["amount"] = gblEDonation["amount"];
    inputParam["compCode"] = gblQRPayData["billerCompCode"]; // gblQRPayData["billerID"];

    inputParam["flex1"] = gblQRPayData["toAccountName"];
    inputParam["flex2"] = gblQRPayData["fee"]; 

    inputParam["typeID"] = "027"


    inputParam["billerId"] = gblQRPayData["billerID"];
	kony.print("billPaymentValidationService");
    invokeServiceSecureAsync("billPaymentValidationService", inputParam, billPaymentValidationServiceForEDonation);

}

function billPaymentValidationServiceForEDonation(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
          frmEDonationPaymentConfirm.lblPaymentDateVal.text = resulttable["paymentServerDate"] + " [" + resulttable["paymentServerTime"] +"]";
          if(frmDonationSelectAmount.btnConfirm.skin == "btnBlueSkin"){
            inputParam = {};
            inputParam["transRefType"] = "NB";
            invokeServiceSecureAsync("generateTransferRefNo", inputParam, generateTransferRefNoserviceNBCallBack);

          }
        }else{
          alert(kony.i18n.getLocalizedString("ECGenericError"));
          dismissLoadingScreen();
        }
    }else{
      alert(kony.i18n.getLocalizedString("ECGenericError"));
      dismissLoadingScreen();
    }
}

function generateTransferRefNoserviceNBCallBack(status, result) {


    if (status == 400)
    {
        if (result["opstatus"] == 0) {
          gblbilPayRefNum = result["transRefNum"] + "00"; 
          gblserverDate = result["serverDate"];
          frmEDonationPaymentConfirm.transactionRefNoVal.text = result["transRefNum"] + "00"; 
           frmEDonationPaymentConfirm.lblPaymentDateVal.text = result["serverDate"]; 
		  frmEDonationPaymentConfirm.show();
		  dismissLoadingScreen();
        } else {
            dismissLoadingScreenPopup();
            alert(" " + result["errMsg"]);
			dismissLoadingScreen();
			return false;
        }
    }else{
	    alert(kony.i18n.getLocalizedString("ECGenericError"));
		dismissLoadingScreen();
		return false;
	}
}

function showDonationSchedule(){
  frmSchedule.show();
}

function unCheckTaxCheckbox(){
  frmDonationSelectAmount.imgTaxCheckbox.src = "radio_uncheck.png";
  gblTaxDec = "F";
}
function onClickDeclareCheckbox(){
   if(frmDonationSelectAmount.imgTaxCheckbox.src == "radio_check.png"){
     showAlertWithYesNoHandler(kony.i18n.getLocalizedString("MB_eDConfirmConsentMsg"),kony.i18n.getLocalizedString("CAV07_keyTitle"),kony.i18n.getLocalizedString("keyConfirm"),kony.i18n.getLocalizedString("CAV05_btnCancel"),unCheckTaxCheckbox,doNothing);
   }else{
     frmDonationSelectAmount.imgTaxCheckbox.src = "radio_check.png" ;
     gblTaxDec = "T";
   }
  
}
function frmDonationSelectAmountPreshow(){
//     var prevForm = kony.application.getPreviousForm();
//     if(prevForm != frmSchedule){
//       setDefaultSkinToDonationAmt();
//       setDetaultPaymentOption();
//       assignDonationAmounts();
//       frmDonationSelectAmount.txtAmount.text = "";
//     }
//   frmDonationSelectAmount.flexAmountDetails.setFocus(true);
  frmDonationSelectAmount.btnMore.text =  kony.i18n.getLocalizedString("MB_eDMoreBtn")
  frmDonationSelectAmount.lblAmountToDonate.text =  kony.i18n.getLocalizedString("MB_eDAmtLab")
  frmDonationSelectAmount.txtAmount.placeholder =  kony.i18n.getLocalizedString("MB_eDAmtText")
  frmDonationSelectAmount.CopylblAmountToDonate0ge90553a505045.text =  kony.i18n.getLocalizedString("MB_eDDeclareTax")
  frmDonationSelectAmount.btnConfirm.text =  kony.i18n.getLocalizedString("MB_eDNextbtn-1Time")
  frmDonationSelectAmount.btnLess.text =  kony.i18n.getLocalizedString("MB_eDLessBtn")
  
  setSelectAmountScreen();
  showDeclareTaxCheckbox();
  dismissLoadingScreen();
}
function showDeclareTaxCheckbox(){
  if(isCitizenIdAvailble()){
    frmDonationSelectAmount.flexDlecareTaxCheckbox.isVisible = true;
  }else{
	gblTaxDec = "F";
    frmDonationSelectAmount.flexDlecareTaxCheckbox.isVisible = false;
  }
}

function isCitizenIdAvailble(){
  var isCitizenAvail = true;
  kony.print("gblCustomerIDValue-->"+gblCustomerIDValue);
  if (gblCustomerIDType != "CI") {
    isCitizenAvail = false;
  }else{
   	 return isCitizenAvail; 
  }
 
}
function frmDonationSelectAmountPostshow(){
  frmDonationSelectAmount.flexScrollBoxMain.enableScrolling = true;
  kony.print("gbleDonationType-->"+gbleDonationType)
  if(gbleDonationType == "Barcode_Type"){
    var scanAmount = parseFloat(replaceAll(gblScanAmount,",","")+"");
    kony.print("scanAmount-->"+scanAmount)
    if(scanAmount > 0){
    kony.print("scanAmount-->"+scanAmount)
    frmDonationSelectAmount.txtAmount.text =  commaFormatted(scanAmount+"");
	gblScanAmount = 0;
    kony.print("scanAmount-->"+commaFormatted(scanAmount+""))
    frmDonationSelectAmount.flexAmountDetails.setFocus(true);
    frmDonationSelectAmount.btnConfirm.skin = "btnBlueSkin";
    frmDonationSelectAmount.btnConfirm.focusSkin = "btnDarkBlueFoc";
  }
  }
 // setAlignmentToDeclarTaxFLex();
}

function setAlignmentToDeclarTaxFLex(){
  var locale = kony.i18n.getCurrentLocale();
        if (locale == "en_US") {
          frmDonationSelectAmount.flexDlecareTaxCheckbox.height = "12%"
          frmDonationSelectAmount.flexDeclareTax.centerX = "33%";
          frmDonationSelectAmount.flexDlecareTaxCheckbox.widgets = "45";
          //frmDonationSelectAmount.btnConfirm.centerY = "15%";
        } else {
          frmDonationSelectAmount.flexDlecareTaxCheckbox.height = "18%"
          frmDonationSelectAmount.flexDeclareTax.centerX = "18%";
          frmDonationSelectAmount.flexDlecareTaxCheckbox.widgets = "85%";
          //frmDonationSelectAmount.btnConfirm.centerY = "15%";
          
        }
}

function setDetaultPaymentOption(){
  frmDonationSelectAmount.btnDonateOnce.skin = btnBlueTabBGNoRound140px;
  frmDonationSelectAmount.btnSetSchedule.skin = btnTabWhiteRightRound140px;
}
function assignDonationAmounts(){
  //var donationAmounts = "1000,900,500,400,200,100";
  var donationAmountArray = gblEDonationAmounts.split(",");
  frmDonationSelectAmount.btn1.text = commaFormattedTransfer(donationAmountArray[0])+" " + kony.i18n.getLocalizedString("currencyThaiBaht");
  frmDonationSelectAmount.btn2.text = commaFormattedTransfer(donationAmountArray[1])+" " + kony.i18n.getLocalizedString("currencyThaiBaht");
  frmDonationSelectAmount.btn3.text = commaFormattedTransfer(donationAmountArray[2])+" " + kony.i18n.getLocalizedString("currencyThaiBaht");
  frmDonationSelectAmount.btn4.text = commaFormattedTransfer(donationAmountArray[3])+" " + kony.i18n.getLocalizedString("currencyThaiBaht");
  frmDonationSelectAmount.btn5.text = commaFormattedTransfer(donationAmountArray[4])+" " + kony.i18n.getLocalizedString("currencyThaiBaht");
  frmDonationSelectAmount.btn6.text = commaFormattedTransfer(donationAmountArray[5])+" " + kony.i18n.getLocalizedString("currencyThaiBaht");
}

function donationAmountOnDone(){
  var amt = commaFormatted(parseFloat(removeCommos(frmDonationSelectAmount.txtAmount.text)).toFixed(2));
  kony.print("amt-->1"+amt);
  if(amt.length > 0 && parseInt(amt)>0){
    kony.print("amt-->2"+amt);
    setDefaultSkinToDonationAmt();
    frmDonationSelectAmount.flexDonationAmtTextBox.skin = "sknFlexBoxBGFocus";
    frmDonationSelectAmount.btnConfirm.skin = "btnBlueSkin";
    kony.print("parseInt amt"+amt);
    gblEDonation["amount"] = amt;
  }else{
    setDefaultSkinToDonationAmt();
    frmDonationSelectAmount.flexDonationAmtTextBox.skin = "sknFlexBoxBG";
    frmDonationSelectAmount.btnConfirm.skin = "sknBtnLightGreyDisableSKin";
   	frmDonationSelectAmount.btnConfirm.focusSkin = "sknBtnLightGreyDisableSKin";
  }
  //TakeoutFocusFromDonationAmount();
}

function setDefaultSkinToDonationAmt(){
  
  frmDonationSelectAmount.btn1.skin = "btnBlueFont160WhiteBG"
  frmDonationSelectAmount.btn2.skin = "btnBlueFont160WhiteBG"
  frmDonationSelectAmount.btn3.skin = "btnBlueFont160WhiteBG"
  frmDonationSelectAmount.btn4.skin = "btnBlueFont160WhiteBG"
  frmDonationSelectAmount.btn5.skin = "btnBlueFont160WhiteBG"
  frmDonationSelectAmount.btn6.skin = "btnBlueFont160WhiteBG"
 /* frmDonationSelectAmount.btn1.focusSkin = "btnWhiteFont160BlueBG"
  frmDonationSelectAmount.btn2.focusSkin = "btnWhiteFont160BlueBG"
  frmDonationSelectAmount.btn3.focusSkin = "btnWhiteFont160BlueBG"
  frmDonationSelectAmount.btn4.focusSkin = "btnWhiteFont160BlueBG"
  frmDonationSelectAmount.btn5.focusSkin = "btnWhiteFont160BlueBG"
  frmDonationSelectAmount.btn6.focusSkin = "btnWhiteFont160BlueBG" */
}
 
function TakeoutFocusFromDonationAmount(){
  frmDonationSelectAmount.flexAmountDetails.setFocus(true);
}
function onClickDonationAmount(eventobj){
	 var donationAmounts = "1000,900,500,400,200,100";
  if(gblEDonationAmounts == undefined){
    gblEDonationAmounts = donationAmounts;
  }
  var donationAmountArray = gblEDonationAmounts.split(",");
  gblScanAmount = 0;
  kony.print("--->"+eventobj);
  setDefaultSkinToDonationAmt();
  frmDonationSelectAmount.flexDonationAmtTextBox.skin = "sknFlexBoxBG";
  frmDonationSelectAmount.flexAmountDetails.setFocus(true);
  var index = eventobj['id'].replace("btn","")
  kony.print("--->"+index);
  kony.print("SKinnnn--->"+frmDonationSelectAmount.btn1.skin);
  if(frmDonationSelectAmount["btn"+index].skin == "btnBlueFont160WhiteBG"){
    frmDonationSelectAmount["btn"+index].skin = "btnWhiteFont160BlueBG"
    frmDonationSelectAmount["btn"+index].focusSkin = "btnWhiteFont160BlueBG"
  }else if(frmDonationSelectAmount["btn"+index].skin == "btnWhiteFont160BlueBG"){
  frmDonationSelectAmount["btn"+index].skin = "btnBlueFont160WhiteBG"
  frmDonationSelectAmount["btn"+index].focusSkin = "btnBlueFont160WhiteBG"
  }
  
  frmDonationSelectAmount.txtAmount.text = "";
  gblEDonation["amount"] = commaFormattedTransfer(donationAmountArray[index-1]);
  //frmDonationSelectAmount.txtAmount.setFocus(false)
  frmDonationSelectAmount.flexDonationAmtTextBox.skin = "sknFlexBoxBG";
  frmDonationSelectAmount.btnConfirm.skin = "btnBlueSkin";
  frmDonationSelectAmount.btnConfirm.focusSkin = "btnDarkBlueFoc";
  
}

function onClickDonationOption(eventObject){
  kony.print("Button ID"+ eventObject['id']);
  if(eventObject['id'] == "btnSetSchedule"){
    if(frmDonationSelectAmount.btnSetSchedule.skin == "btnTabWhiteRightRound140px"){
     frmDonationSelectAmount.btnSetSchedule.skin = "btnTabBlueRightRound140px"
     frmDonationSelectAmount.btnDonateOnce.skin = "btnWhiteTabBGNoRound140px"
   }else{
   }
  }else if(eventObject["id"] == "btnDonateOnce"){
     if(frmDonationSelectAmount.btnDonateOnce.skin == "btnWhiteTabBGNoRound140px"){ // white
     frmDonationSelectAmount.btnSetSchedule.skin = "btnTabWhiteRightRound140px"
     frmDonationSelectAmount.btnDonateOnce.skin = "btnBlueTabBGNoRound140px"
   }else{
     
   }
    
  }
   
   gblreccuringDisablePay = "N";
   gblreccuringDisableAdd = "N";
  // frmSchedule.show();
  //frmDonationSelectAmount.btnDonateOnce.skin = btnTabWhiteRightRound140px;
  //frmDonationSelectAmount.btnSetSchedule.skin = btnBlueTabBGNoRound140px;
}

function showFoundationDetails() {
    AS_Button_e41661b0e9e64fa3b10f80fb2b79ccd0();
    frmDonationSelectAmount.btnMore.isVisible = false;
    frmDonationSelectAmount.btnBack.top = "56%"
    frmDonationSelectAmount.btnBack.onClick = hideFoundationDetails;
    frmDonationSelectAmount.flexFoundationName.top = "73%"
    //frmDonationSelectAmount.flexScrollBoxMain.enableScrolling(false)
    frmDonationSelectAmount.flexScrollBoxMain.enableScrolling = false;
    
}

function hideFoundationDetails() {
    AS_Button_b98d7428f7d54c6ebeecff56bd603e98();
    frmDonationSelectAmount.btnMore.isVisible = true;
    frmDonationSelectAmount.btnBack.top = "0%"
    frmDonationSelectAmount.btnBack.onClick = frmDonationSelectAmount_btnBack_Action;
    frmDonationSelectAmount.flexFoundationName.top = "60%"
    //frmDonationSelectAmount.flexScrollBoxMain.enableScrolling(true)
    frmDonationSelectAmount.flexScrollBoxMain.enableScrolling = true;
}

function AS_Button_e41661b0e9e64fa3b10f80fb2b79ccd0(eventobject) {
    function MOVE_ACTION____c15d0740e8ab41909438b0546860259c_Callback() {
        //frmDonationSelectAmount.flexAmountDetails["isVisible"] = false;
        //frmDonationSelectAmount.flexFoundationDetails["isVisible"] = true;
    }
     frmDonationSelectAmount.flexFoundationDetails.animate(kony.ui.createAnimation({
            "100": {
                "top": "20%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "rectified": true
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        }, {
            "animationEnd": MOVE_ACTION____c15d0740e8ab41909438b0546860259c_Callback
        });

    function MOVE_ACTION____hc679f7705444a2c8420c85a2a1b2727_Callback() {
       
    }
    frmDonationSelectAmount.flexAmountDetails.animate(kony.ui.createAnimation({
        "100": {
            "top": "100%",
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            },
            "rectified": true
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.25
    });
    frmDonationSelectAmount.flexFoundationImage.animate(kony.ui.createAnimation({
        "100": {
            "top": "-20%",
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            },
            "rectified": true
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.25
    });
}

function AS_Button_b98d7428f7d54c6ebeecff56bd603e98(eventobject) {
    
	//frmDonationSelectAmount.flexFoundationDetails["isVisible"] = false;
	//frmDonationSelectAmount.flexAmountDetails["isVisible"] = true;
	 frmDonationSelectAmount.flexFoundationImage.animate(kony.ui.createAnimation({
        "100": {
            "top": "0%",
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            },
            "rectified": true
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.25
    });
      frmDonationSelectAmount.flexAmountDetails.animate(kony.ui.createAnimation({
        "100": {
            "top": "35%",
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            },
            "rectified": true
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.25
    });
  
	  frmDonationSelectAmount.flexFoundationDetails.animate(kony.ui.createAnimation({
            "100": {
                "top": "-80%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "rectified": true
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        });
   
}

function ontextChangeDonationAmount(){
	var enteredAmount = frmDonationSelectAmount.txtAmount.text;
	if(isNotBlank(enteredAmount)) {
		enteredAmount = kony.string.replace(enteredAmount, ",", "");
		if(isNotBlank(enteredAmount) && enteredAmount.length > 0 && parseFloat(enteredAmount, 10) == 0){
			
		}else{
			frmDonationSelectAmount.txtAmount.text = commaFormattedTransfer(enteredAmount);
		}
	}
}



function getFoundationDetails(compCode) {
    kony.print("compCode-->"+compCode)
    frmDonationSelectAmount.btnMore.isVisible = false;
    var input_param = {};
    var locale = kony.i18n.getCurrentLocale();
    if (locale == "en_US") {
        input_param.localeCd = "en_US";
    } else {
        input_param.localeCd = "th_TH";
    }
    input_param.moduleKey = "eDonationFoundationDetails";
    input_param.compCode = compCode;
   //showLoadingScreen();
    invokeServiceSecureAsync("readUTFFile", input_param, callUTFeDonationDetailsCallback);
}

function callUTFeDonationDetailsCallback(status,result){
   if (status == 400) {
        if (result["opstatus"] == 0) {
          	frmDonationSelectAmount.richTextDonationDetails.text=result["fileContent"];
            frmDonationSelectAmount.btnMore.isVisible = true;
          	//dismissLoadingScreen();
        }else{
            frmDonationSelectAmount.richTextDonationDetails.text = "";
            frmDonationSelectAmount.btnMore.isVisible = false;
        }
    }
}

function destroyeDonationForms(){
  frmDonationSelectAmount.destroy();
  frmEDonationPaymentConfirm.destroy();
  frmEDonationPaymentComplete.destroy();
}


function invokeQRPaymentDefaultAccntServiceEdonation() {
  var inputParam = {};

  showLoadingScreen();

  gblQRPaymentDeviceId = getDeviceID();

  inputParam["encryptDeviceId"] = getDeviceID();
  if(gblQRPayData["mobileNo"] != undefined && gblQRPayData["mobileNo"] != ""){
    inputParam["promptPayType"] = "02";
    inputParam["promptPayId"] = gblQRPayData["mobileNo"];
  } else if(gblQRPayData["cId"] != undefined && gblQRPayData["cId"] != "") {
    inputParam["promptPayType"] = "01";
    inputParam["promptPayId"] = gblQRPayData["cId"];
  } else if(gblQRPayData["eWallet"] != undefined && gblQRPayData["eWallet"] != "") {
    inputParam["promptPayType"] = "04";
    inputParam["promptPayId"] = gblQRPayData["eWallet"];
  } else if(gblQRPayData["billerID"] != undefined && gblQRPayData["billerID"] != "") {
    inputParam["promptPayId"] = gblQRPayData["billerID"];
    inputParam["promptPayType"] = "05";
  }else {
    alert("Incorrect input param");
    return;
  }
  var amtVal ="" ;
  if(checkToggleAmountSelect()){
        amtVal = removeCommos(gblEDonation["amount"]);
  }else{
        amtVal = removeCommos(frmDonationSelectAmount.txtAmount.text);
  }
  inputParam["transAmount"] = amtVal;

  if(gblQRPayData["QR_TransType"]== "QR_Bill_Payment"){
      inputParam["transType"] = "BILLPAY_PROMPTPAY";
      inputParam["ref1"] = gblQRPayData["ref1"];
	  if (gbleDonationType == "QR_Type" || gbleDonationType == "Barcode_Type") {
		if (gblTaxDec == "T") {
			inputParam["ref2"] = gblCustomerIDValue;
		} else {
			inputParam["ref2"] = "0";
		}
		inputParam["eDonationType"] = gbleDonationType;
	  } else {
		inputParam["ref2"] = gblQRPayData["ref2"];
	  }
      inputParam["ref3"] = gblQRPayData["ref3"];

  }else{
      inputParam["transType"] = "TRANSFER_PROMPTPAY";
  }
  kony.print("transAmount :: ");
  invokeServiceSecureAsync("QRPaymentDefaultAccount", inputParam, callBackQRDefaultAccounteDonation);
}

function callBackQRDefaultAccounteDonation(status, resulttable){
  var foundError = false;
  
  kony.print("callBackQRDefaultAccounteDonation :: ");
  if (status == 400) {
  	if (resulttable["opstatus"] == "0" || resulttable["opstatus"] == 0) {
    	var errCode = resulttable["pp_errCode"];

  		kony.print("errCode :: " + errCode);
       	if (isNotBlank(errCode)) {
        	foundError = true;
        } else {
          	billPaymentValidationForEDonation();
        }
  	}
  }else{
  	foundError = true; 
  }
  
  kony.print("foundError :: " + foundError);
  if (foundError) {
      var errorText = kony.i18n.getLocalizedString("ECGenericError");
      alert(errorText);
      return;  
  }
}