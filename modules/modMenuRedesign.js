function displayPreMenu()
{
			
			var segMenuTable;
			if ((null != gblPlatformName) && ((kony.string.equalsIgnoreCase("iphone", gblPlatformName) || kony.string.equalsIgnoreCase("iPad", gblPlatformName)))) {
				segMenuTable = [{
						lblMenuItem: kony.i18n.getLocalizedString("MenuFindTMB")
						}, {
						lblMenuItem: kony.i18n.getLocalizedString("MenuExRates")
						}, {
						lblMenuItem: kony.i18n.getLocalizedString("MenuTour")
						},{
						lblMenuItem: kony.i18n.getLocalizedString("keyContactUs")
						},{
						lblMenuItem: kony.i18n.getLocalizedString("keyTermsNConditions")
						},{
						lblMenuItem: kony.i18n.getLocalizedString("keySecurityAdvices")
						},{
						lblMenuItem: kony.i18n.getLocalizedString("Key_Language_Switch")
						}
				]
			}else{
				segMenuTable = [{
						lblMenuItem: kony.i18n.getLocalizedString("MenuFindTMB")
						}, {
						lblMenuItem: kony.i18n.getLocalizedString("MenuExRates")
					}, {
						lblMenuItem: kony.i18n.getLocalizedString("MenuTour")
					},{
						lblMenuItem: kony.i18n.getLocalizedString("keyContactUs")
					},{
						lblMenuItem: kony.i18n.getLocalizedString("keyTermsNConditions")
					},{
						lblMenuItem: kony.i18n.getLocalizedString("keySecurityAdvices")
					},{
						lblMenuItem: kony.i18n.getLocalizedString("Key_Language_Switch")
					},{
						lblMenuItem: ""
					}
				]
			}
		frmPreMenu.lblmenuLogin.text = kony.i18n.getLocalizedString("login");
		frmPreMenu.btnThai.text=kony.i18n.getLocalizedString("languageThai");
		frmPreMenu.btnEng.text=kony.i18n.getLocalizedString("languageEng");
		var locale = kony.i18n.getCurrentLocale();
		if (locale == "en_US")
	    {
			frmPreMenu.btnEng.skin = btnEngMenu;
			frmPreMenu.btnEng.zIndex = 2;
			frmPreMenu.btnThai.skin = btnThaiMenu;
			frmPreMenu.btnThai.zIndex = 1;
            frmPreMenu.btnThai.padding =  [7, 0, 0, 0]
            frmPreMenu.btnEng.padding = [0, 0, 0, 0]
            
            
		}
		else
		{
            frmPreMenu.btnThai.padding =  [0, 0, 0, 0]
            frmPreMenu.btnEng.padding = [0, 0, 5, 0]
			frmPreMenu.btnEng.skin = btnThaiMenu;
			frmPreMenu.btnEng.zIndex=1;
			frmPreMenu.btnThai.skin = btnEngMenu;
			frmPreMenu.btnThai.zIndex=2;
		}
			
		frmPreMenu.lblversion.text=kony.i18n.getLocalizedString("keyVersion")+" "+ appConfig.appVersion;
		frmPreMenu.lblCopyright.text=getCopyRightText();
		frmPreMenu.segPreLogin.setData(segMenuTable);
		frmPreMenu.show();
}

function onClickMenuPreMenu()
{
	var selectedIndexVal = frmPreMenu.segPreLogin.selectedIndex[1];
  kony.print("selectedIndexVal @@: "+selectedIndexVal);
	if(selectedIndexVal == 0){           
           loadFunctionalModuleSync("findATMModule");
				assignGlobalForMenuClick("frmATMBranch");
				gblLatitude = "";
				gblLongitude = "";
				gblCurrenLocLat = "";
				gblCurrenLocLon = "";
				provinceID = "";
				districtID = "";
				gblProvinceData = [];
				gblDistrictData = [];
				frmATMBranch.btnCombProvince.text = kony.i18n.getLocalizedString("FindTMB_Province");
				frmATMBranch.btnCombDistrict.text =  kony.i18n.getLocalizedString("FindTMB_District");
				frmATMBranch.txtKeyword.text = "";
				onClickATM();
			}else if(selectedIndexVal == 1){               
                loadFunctionalModuleSync("exchangeRatesModule");
				assignGlobalForMenuClick("");
				onClickExchangeRates();
			}else if(selectedIndexVal == 2){
				assignGlobalForMenuClick("frmAppTour");
				//App Tour
				showAppTourForm();
			}
			else if (selectedIndexVal == 3) {                
                loadFunctionalModuleSync("contactUsModule");
				assignGlobalForMenuClick("frmContactUsMB");
				conatctUsForm();
			}else if (selectedIndexVal == 4) {
				assignGlobalForMenuClick("");
				onClickDownloadTnC('pdf', 'TermsAndCondtionsFooter');
			}else if (selectedIndexVal == 5) {
				assignGlobalForMenuClick("");
				onClickSecurityAdvices();
			}else if (selectedIndexVal == 6) {                
                loadFunctionalModuleSync("localeModule");
              	kony.print("lang matched @@");
				//assignGlobalForMenuClick("frmChangeLanguage");
				onClickChangeLanguage();
			}
	
}

function onClickClosebuttonMenu()
{
   try{
	gblCallPrePost = false;
	var displayform=kony.application.getPreviousForm();
    if(displayform.id == "frmGetNewLoan"){
      kony.i18n.setCurrentLocaleAsync("th_TH", function(){frmGetNewLoan.show();}, function(){}, "");
    }else{
      //alert("gblCurrentLocale"+gblCurrentLocale)
      if(gblCurrentLocaleBeforeLoan!= ""){
        kony.i18n.setCurrentLocaleAsync(gblCurrentLocaleBeforeLoan, function(){displayform.show();gblCurrentLocaleBeforeLoan="";}, function(){}, "");
      }else{
        displayform.show();
      }
    }
   }catch(e){
     kony.print(e)
   }
	
}

function assignGlobalForMenuClick(menuClickForm) {
  kony.print("gblCurrentForm @@: "+gblCurrentForm);
  kony.print("menuClickForm @@: "+menuClickForm);
	if(gblCurrentForm == menuClickForm) {
		gblCallPrePost = false;
	}
}

function assignGlobalForMenuPostshow() {
	gblCallPrePost = true;
	gblCurrentForm = kony.application.getCurrentForm().id;
	try{
		changeStatusBarColor();
		kony.application.getCurrentForm().scrollboxMain.scrollToEnd();
	}catch(error){
	}
}