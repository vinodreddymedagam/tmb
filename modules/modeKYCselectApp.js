gblSelappPressed = false;
gblIDPData = "";
gblSelectedAppData = [];
gblIDPSearchData = "";
gblIsSearch = false;
gblImageID = "";
function init_frmMBeKycSelectApp(){
  frmMBeKycSelectApp.preShow = frmMBeKycSelectApp_Preshow_Action;
}

function frmMBeKycSelectApp_Preshow_Action(){
  frmMBeKycSelectApp.txtSearch.onTextChange = onclickTextChangesearchIDP;
  frmMBeKycSelectApp.lblSelectAppDesc.text = kony.i18n.getLocalizedString("eKYC_msgSelectBank");
  setHeaderContentToForm(kony.i18n.getLocalizedString("eKYC_TitleSelectBank"),null,false,"3");
  setFooterSinglebtnToForm(kony.i18n.getLocalizedString("eKYC_btnSendRequest"),onClicksendRequest);  
  if(gblSelappPressed){
    frmMBeKycSelectApp.footers[0].isVisible =true
  }else{
    frmMBeKycSelectApp.footers[0].isVisible =false
  }
}

function onClickbtnNexteKYCIntro(){
  kony.print("gblIDPFlow::::"+gblIDPFlow);
  if(gblIDPFlow == 0 || gblIDPFlow == "0"){
    onClickFATCA();
  }else{
    getIdentityproviderService();
  }
}
function getIdentityproviderService(){
  try{
    var inputParam = {};
    inputParam["citizenId"] = "1234567890123";  
    invokeServiceSecureAsync("getIdentityProviders", inputParam , callBackgetIdentityproviderService);
  }catch(e){
    kony.print("@@@ getIdentityproviderService() Exeption:::"+e);
  }
}

function callBackgetIdentityproviderService(status, resultTable){
  try{
    if (status == 400){
      if (resultTable["opstatus"] == 0){
        gblIDPData = resultTable;
        kony.print("gblIDPData::::::"+JSON.stringify(gblIDPData) );
        gblSelappPressed = false;
        addWidgetsSelectApplication(gblIDPData["providerList"]);
      }
  	}
  }catch(e){
    kony.print("@@@ callBackgetIdentityproviderService() Exeption:::"+e);
  }
}

function addWidgetsSelectApplication(providerListData) {
var i;
  frmMBeKycSelectApp.flexDynamic.removeAll();
for(i=0;i<providerListData.length;i++){
	if(i==0){
		flexLeft = "0%";
		flexTop = "10dp";
	}else if(i/4 < 1){
		flexTop = "10dp";
		if(i%4==0){
			flexLeft = "0%";
		}else if(i%4 == 1){
			flexLeft = "27%";
		}else if(i%4 == 2){
			flexLeft = "54%";
		}else if(i%4 == 3){
			flexLeft = "81%";
		}
	}else if(i/4 < 2 && i/4 >= 1){
		flexTop = "110dp";
		if(i%4==0){
			flexLeft = "0%";
		}else if(i%4 == 1){
			flexLeft = "27%";
		}else if(i%4 == 2){
			flexLeft = "54%";
		}else if(i%4 == 3){
			flexLeft = "81%";
		}
	}else if(i/4 < 3 && i/4 >= 2){
		flexTop = "210dp";
		if(i%4==0){
			flexLeft = "0%";
		}else if(i%4 == 1){
			flexLeft = "27%";
		}else if(i%4 == 2){
			flexLeft = "54%";
		}else if(i%4 == 3){
			flexLeft = "81%";
		}
	}else if(i/4 < 4 && i/4 >= 3){
		flexTop = "310dp";
		if(i%4==0){
			flexLeft = "0%";
		}else if(i%4 == 1){
			flexLeft = "27%";
		}else if(i%4 == 2){
			flexLeft = "54%";
		}else if(i%4 == 3){
			flexLeft = "81%";
		}
	}
	var flexSelectApp= new kony.ui.FlexContainer({
        "id": "flexApp"+i,
        "top": flexTop,
        "left": flexLeft,
        "width": "65dp",
        "height": "90dp",
        "zIndex": 1,
        "isVisible": true,
        "clipBounds": true,
        "layoutType": kony.flex.FLOW_VERTICAL
    }, {
        "padding": [0, 0, 0, 0]
    }, {});;
    flexSelectApp.add();
	var ImageApp = new kony.ui.Image2({
        "id": "ImageApp"+i,
      	"info": ""+i,
        "isVisible": true,
		"width": "100%",
        "height": "70%",
		"centerX": "50%",
      	"onTouchStart":onClickAppSelect,
        "src": getIdpAppicon(providerListData[i].provider),
        "imageWhenFailed": null,
        "imageWhileDownloading": null
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "margin": [0, 0, 0, 0],
        "padding": [0, 0, 0, 0],
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "referenceWidth": null,
        "referenceHeight": null,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 100
    }, {
        "toolTip": null
    });
	var lblApp = new kony.ui.Label({
        "id": "lblApp"+i,
        "top": "0%",
        "width": "100%",
        "centerX": "50%",
        "zIndex": 1,
        "isVisible": true,
        "text": providerListData[i].applicationName,
        "skin": "lblGrey20px"
    }, {
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {
        "textCopyable": false
    });
	flexSelectApp.add(ImageApp,lblApp);

    frmMBeKycSelectApp.flexDynamic.add(flexSelectApp);
  frmMBeKycSelectApp.show();
}
    
}

function onClickAppSelect(eventobject){
  var idpProviderList;
  if(gblIsSearch){
    idpProviderList = gblIDPSearchData;
  }else{
    idpProviderList = gblIDPData["providerList"];
  }
  kony.print("The eventobject is "+eventobject);
  var imageID = eventobject.id;
  imageID = imageID.substr(8, 4);
     
  	 kony.print("gblImageID:::"+gblImageID);
     if(gblImageID !== "" && gblImageID !== undefined){
       kony.print("Inside Def IF:::");
       var defAppCode = idpProviderList[gblImageID].provider
       defAppCode = defAppCode.replace("_sel", "");
       kony.print("defAppCode:::"+defAppCode);
       frmMBeKycSelectApp["ImageApp"+gblImageID].src = getIdpAppicon(defAppCode);
     }
  
  if(!gblSelappPressed){
    gblImageID = imageID;
    gblSelappPressed = true;
    var selAppCode = idpProviderList[imageID].provider;
    selAppCode = selAppCode+"_sel";
    kony.print("selAppCode:::"+selAppCode);
    frmMBeKycSelectApp["ImageApp"+imageID].src = getIdpAppicon(selAppCode);
    frmMBeKycSelectApp.footers[0].isVisible =true
    gblSelectedAppData["applicationName"] = idpProviderList[imageID].applicationName;
    gblSelectedAppData["applicationCode"] = idpProviderList[imageID].applicationCode;
    gblSelectedAppData["provider"] = idpProviderList[imageID].provider;
    gblSelectedAppData["id"] = idpProviderList[imageID].id;
  }else{
    if(gblImageID == imageID){
      gblSelappPressed = false;
      var unSelAppCode = idpProviderList[imageID].provider;
      unSelAppCode = unSelAppCode.replace("_sel", "");
      kony.print("unSelAppCode:::"+unSelAppCode);
      frmMBeKycSelectApp["ImageApp"+imageID].src = getIdpAppicon(unSelAppCode);
      frmMBeKycSelectApp.footers[0].isVisible =false
      gblSelectedAppData["applicationName"] = "";
      gblSelectedAppData["applicationCode"] = "";
      gblSelectedAppData["id"] = "";
    }else{
      gblImageID = imageID;
      gblSelappPressed = true;
      var selAppCode = idpProviderList[imageID].provider;
      selAppCode = selAppCode+"_sel";
      kony.print("selAppCode:::"+selAppCode);
      frmMBeKycSelectApp["ImageApp"+imageID].src = getIdpAppicon(selAppCode);
      frmMBeKycSelectApp.footers[0].isVisible =true
      gblSelectedAppData["applicationName"] = idpProviderList[imageID].applicationName;
      gblSelectedAppData["applicationCode"] = idpProviderList[imageID].applicationCode;
      gblSelectedAppData["provider"] = idpProviderList[imageID].provider;
      gblSelectedAppData["id"] = idpProviderList[imageID].id;
    }
    
  }
}

function getIdpAppicon(imageName) {
  var randomnum = Math.floor((Math.random() * 10000) + 1);
  var appIcon = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+imageName+"&modIdentifier=IDPBANKAPPICON&rr="+randomnum;
  kony.print("appIcon:::"+appIcon);
  return appIcon;
}
function onClicksendRequest(){
  getIdentitySendRequest();
}

function searchApplist(searchAppData) {
  kony.print("Inside searchApplist");
    var provider = "";
  	var applicationCode =  "";
	var applicationName =  "";
  var searchAppList = [];
  gblIsSearch = true;
  gblSelappPressed = false;
  gblImageID = "";
	frmMBeKycSelectApp.flexDynamic.removeAll();
    for (var i = 0; i < gblIDPData["providerList"].length; i++) {	
      provider = gblIDPData["providerList"][i]["provider"].toLowerCase();
      applicationCode =  gblIDPData["providerList"][i]["applicationCode"].toLowerCase();
      applicationName =  gblIDPData["providerList"][i]["applicationName"].toLowerCase();
      if (provider.includes(searchAppData.toLowerCase()) || applicationCode.includes(searchAppData.toLowerCase()) 
          || applicationName.includes(searchAppData.toLowerCase())) {
        searchAppList.push(gblIDPData["providerList"][i]);
        kony.print("Inside if searchAppList:::"+JSON.stringify(searchAppList));   
      }
    }
  gblIDPSearchData = searchAppList;
  addWidgetsSelectApplication(searchAppList);

}

function onclickTextChangesearchIDP(){
  var searchText = frmMBeKycSelectApp.txtSearch.text;
  kony.print("Inside search"+searchText.length);
  if(searchText.length >=3 || searchText.length == 0){
    searchApplist(searchText);
  }
}