//Type your code here
function initfrmeKYCFaceRecg(){
  frmMBeKYCFaceRecg.preShow=preShowfrmeKYCFaceRecg;
  frmMBeKYCFaceRecg.onDeviceBack= doNothing; 
   frmMBeKYCFaceRecg.postShow=postShowfrmeKYCFaceRecg;
}

function postShowfrmeKYCFaceRecg(){
  if(gblIDPApproveRequestFlow){
	addAccessPinKeypad(frmMBeKYCFaceRecg);
  }
   animateIdAndSelfieImages(frmMBeKYCFaceRecg.ImgPassportPic, -20);
   animateIdAndSelfieImages(frmMBeKYCFaceRecg.ImgSelfiePic, 20);
   animateIdAndSelfieImages(frmMBeKYCFaceRecg.flxArrow, 45);
//    animateIdAndSelfieImages(frmMBeKYCFaceRecg.flxArrowError, 45);
//     frmMBeKYCFaceRecg.imbCreditCards3.animate(
//       kony.ui.createAnimation({"100":{"stepConfig":{"timingFunction":kony.anim.EASE}, "centerX":"50%"}}),
//       {"delay":0.5,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.5}
//     );
}

function backbtnnavfrmekyc(){
  var previousForm = kony.application.getPreviousForm();
  previousForm.id.show();
}


function animateIdAndSelfieImages(frmName, rotationAngle){
                var trans100 = kony.ui.makeAffineTransform();
    trans100.rotate(rotationAngle);
    frmName.animate(
    kony.ui.createAnimation({
        "100": {
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            },
            "transform": trans100
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.001
    }, {
        "animationEnd": null
    });
}


 function preShowfrmeKYCFaceRecg(){
   if(!(gblIALFlow == "frmEKYCIdpSetting" || gblIALFlow == "frmEKYCIdpDetailsList")){
     setHeaderContentToForm(kony.i18n.getLocalizedString("eKYC_TitleFaceRecognition"),NavbackEnterSerialNo,true,"1");
   }else{
     setHeaderContentToForm(kony.i18n.getLocalizedString("eKYC_TitleFaceRecognition"),navToEnterLaserCode,true,"0");
   }
   //setFooterSinglebtnToForm(kony.i18n.getLocalizedString("btnNext"),invokeFaceRecogFFI);
   setFooterTwoVerticalbtnToForm(kony.i18n.getLocalizedString("btnNext"),"",invokeFaceRecogFFI,null,true,false);
   frmMBeKYCFaceRecg.RichTextFacerecognitionTips.text = kony.i18n.getLocalizedString("eKYC_FaceRecognitionTips");
//    frmMBeKYCFaceRecg.flxFaceRecognitionCheck.setVisibility(false);
//    frmMBeKYCFaceRecg.flexBodyScroll.setVisibility(true);
 } 

function NavbackEnterSerialNo(){
//     backbtnEnterSerialNo();
    frmMBeKYCCitezenIDInfo.show();
    onClicknextCitizenID();

}

function showfrmMBeKYCFaceRecg(){
  if(onDonetxtLaserCode()){
    gbleKYCStep="2";
//  navigateKYCIntroduction();
  frmMBeKYCFaceRecg.show();
    }
}

function invokeFaceRecogFFI(){
  kony.print("Need to invoke FFI");
  var selifeDataParams = {"BiometricOperation":"VERIFICATION","URLServer":"https://mevit.tau2904.com/ReRouteSoapCalls/SoapEndPoint?destination=http://10.209.23.233:8080/epid-webservices/api/epidService?wsdl",
                          "SubjectID":"","firstName":"","lastName":"","middleName":"","dob":"","JPEGQuality":90,"Timer":5,"suffixOrComplement":"","institution":"MB",
                          "workstation":"TestPassportReader","isCaptureAutomatic":true,"ContinuousSuccess":1,"isFace_Spoof_Detection":true,"certificateName":"epidrsa_new.cer",
                          "isFaceConsistencyCheck":true,"isCaptureOnly":true,"isKonyMobileFabric":false,"appkey":"","appsecret":"","konyurl":"","konyservicename":"","KonyRequestTokenOperationName":"",
                          "KonySubmitBiometricOperationName":"","getActionVideo":false,"getActionImage":false,"isSmile":true,"isBlink":true,"isMoveHeadLeft":true,"isMoveHeadRight":true,
                          "isCloseLeftEye":false,"isCloseRightEye":false,"isIncludeImage":false,"isIncludeVoice":true,"NFCImage":null,
                          "ChallengeCount":3,"AllowRepeatAction":false};
  scanEKYC.verifyFace(JSON.stringify(selifeDataParams), getSelifeImageCallBack, null);
}

function getSelifeImageCallBack(data){
    showLoadingScreen();
    var documentData = JSON.parse(data);
    if(documentData && documentData.hasOwnProperty("captureOnlyImage")){
      eKYCData.SelifeImage = documentData.captureOnlyImage;
    }
    var inputParam = {};
    if(gblIDPApproveRequestFlow){
      inputParam.primaryBiometricData = eKYCData.biomentricImage;
    inputParam.primaryBiometricDataLength = eKYCData.biomentricImage ? eKYCData.biomentricImage.length : 0;
    }else{
       inputParam.primaryBiometricData = eKYCData.NFCImage;
       inputParam.primaryBiometricDataLength = eKYCData.NFCImage ? eKYCData.NFCImage.length : 0;
    }
   	if(gblUpdateIALFlow){
      inputParam.moduleId = "IDP";
      inputParam.currentIAL = gblCurrentIAL;
    }
    //inputParam.primaryBiometricDataMethod = eKYCData.scanType == "PP" ? "PP" : "ci";
    //inputParam.primaryBiometricDataFormat = "JPG";
    inputParam.secondaryBiometricData = eKYCData.SelifeImage;
    inputParam.secondaryBiometricDataLength = eKYCData.SelifeImage ? eKYCData.SelifeImage.length : 0;
    //inputParam.secondaryBiometricDataMethod = "selfie";
    //inputParam.secondaryBiometricDataFormat = "JPG";
    invokeServiceSecureAsync("EKYCOne2OneVerification", inputParam, one2oneVerificationServiceCallBack);
    //one2one verification service
    //invoke service
  //var resulttable = {};
  //resulttable.opstatus = 0;
  //one2oneVerificationServiceCallBack(400, resulttable);
  //one2oneVerificationServiceCallBackTemp();
 }

function one2oneVerificationServiceCallBackTemp(){
  frmMBeKYCFaceRecg.show();
  frmMBeKYCFaceRecg.flxFaceRecognitionCheck.setVisibility(true);
  frmMBeKYCFaceRecg.flexBodyScroll.setVisibility(false);
//   setHeaderContentToForm(kony.i18n.getLocalizedString("MB_Title_eKYCFaceRecResult"),NavbackEnterSerialNo,false,"1");
  frmMBeKYCFaceRecg.ImgSelfiePic.base64 = eKYCData.SelifeImage;
  frmMBeKYCFaceRecg.ImgPassportPic.base64 = eKYCData.NFCImage;
  dismissLoadingScreen();
  setFooterTwoVerticalbtnToForm(kony.i18n.getLocalizedString("btnNext"),"",statusCheck,null,true,false);
 //setFooterSinglebtnToForm(kony.i18n.getLocalizedString("btnNext"),statusCheck);
}

function statusCheck(){
  gbleKYCStep = "2";
  navigateKYCIntroduction();
}

function one2oneVerificationServiceCallBack(status, resulttable){
    try{
         frmMBeKYCFaceRecg.flxSelfiePic.centerX = "-10%";
         frmMBeKYCFaceRecg.flxPassportPic.centerX = "120%";
         frmMBeKYCFaceRecg.imgError.setVisibility(false);
         frmMBeKYCFaceRecg.flxlblcircle1.setVisibility(false);
      if(status == 400){
        if(resulttable["opstatus"] === 0){
          var retryStatus = resulttable["reTry"];
          var resultCode = resulttable["resultCode"];
          var scoreType = resulttable["scoreType"];
          var idp = resulttable["idp"];
          
          gblIDPFlow = idp;  // idp == 0, FATCA; idp > 0 Select another APP
          if(GLOBAL_NDID_FLAG =="OFF" && scoreType == "YELLOW"){
            scoreType="RED";
          }
          frmMBeKYCFaceRecg.show();
          
          if(gblIDPApproveRequestFlow){
          	setFooterTwoVerticalbtnToForm(kony.i18n.getLocalizedString("btnNext"),"",approveIDPRequest,null,true,false);
            setHeaderContentToForm(kony.i18n.getLocalizedString("MB_Title_eKYCFaceRecResult"),function(){frmEKYCIdpRequestDetailsAndComplete.show();},false,"1");
          }else{
            if(!gblUpdateIALFlow){
              setFooterTwoVerticalbtnToForm(kony.i18n.getLocalizedString("btnNext"),"",statusCheck,null,true,false);
              setHeaderContentToForm(kony.i18n.getLocalizedString("MB_Title_eKYCFaceRecResult"),NavbackEnterSerialNo,false,"1");
            }
          }
          frmMBeKYCFaceRecg.flxFaceRecognitionCheck.setVisibility(true);
          frmMBeKYCFaceRecg.flexBodyScroll.setVisibility(false);
          frmMBeKYCFaceRecg.ImgSelfiePic.base64 = eKYCData.SelifeImage;
          frmMBeKYCFaceRecg.ImgPassportPic.base64 = eKYCData.NFCImage;
          dismissLoadingScreen();
          frmMBeKYCFaceRecg.lblTitleInfo.text = getLocalizedString("eKYC_msgFaceMatched");
          frmMBeKYCFaceRecg.lblMessage.text = getLocalizedString("MB_msgFaceMatched");
//           frmMBeKYCFaceRecg.imgError.setVisibility(false);
//           frmMBeKYCFaceRecg.flxlblcircle1.setVisibility(true);
         //setFooterSinglebtnToForm(kony.i18n.getLocalizedString("btnNext"),statusCheck);
            if(scoreType == ""){
              showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
              frmMBeKYCFaceRecg.lblTitleInfo.text = getLocalizedString("eKYC_msgFaceNotMatched");
              frmMBeKYCFaceRecg.lblMessage.text = getLocalizedString("MB_msgFaceNotmatched");
              frmMBeKYCFaceRecg.lblErrorStatus.text = "error";
//               frmMBeKYCFaceRecg.imgError.setVisibility(true);
//               frmMBeKYCFaceRecg.flxlblcircle1.setVisibility(false);
            }else if(scoreType === "GREEN" || scoreType == "YELLOW"){
              frmMBeKYCFaceRecg.lblTitleInfo.text = getLocalizedString("eKYC_msgFaceMatched");
              frmMBeKYCFaceRecg.lblMessage.text = getLocalizedString("MB_msgFaceMatched");
              frmMBeKYCFaceRecg.lblErrorStatus.text = "success";
              if(gblUpdateIALFlow){
                setHeaderContentToForm(kony.i18n.getLocalizedString("MB_Title_eKYCFaceRecResult"),null,false,"0");
                flxHeader.btnMainMenu.setVisibility(false);
                if(scoreType === "GREEN"){
                  gblCurrentIAL = resulttable["IALLevel"];
                  setFooterTwoVerticalbtnToForm(kony.i18n.getLocalizedString("btnNext"),"",continueEKYCIDPFlowPage,null,true,false);
                }else{
                  frmMBeKYCFaceRecg.lblTitleInfo.text = getLocalizedString("eKYC_msgFaceNotMatched");
                  frmMBeKYCFaceRecg.lblMessage.text = getLocalizedString("MB_msgFaceNotmatched");
                  frmMBeKYCFaceRecg.lblErrorStatus.text = "error";
                  setFooterTwoVerticalbtnToForm(kony.i18n.getLocalizedString("MB_btneKYCSelfieAgain"),kony.i18n.getLocalizedString("eKYC_btnScanPassport"),invokeFaceRecogFFI,rescanGetYourId,true,true);
                }
              }
            }else if(scoreType === "RED"){
              frmMBeKYCFaceRecg.lblTitleInfo.text = getLocalizedString("eKYC_msgFaceNotMatched");
              frmMBeKYCFaceRecg.lblMessage.text = getLocalizedString("MB_msgFaceNotmatched");
              frmMBeKYCFaceRecg.lblErrorStatus.text = "error";
//               frmMBeKYCFaceRecg.imgError.setVisibility(true);
//               frmMBeKYCFaceRecg.flxlblcircle1.setVisibility(false);
              if(retryStatus == "Y"){
                setFooterTwoVerticalbtnToForm(kony.i18n.getLocalizedString("MB_btneKYCSelfieAgain"),kony.i18n.getLocalizedString("eKYC_btnScanPassport"),invokeFaceRecogFFI,rescanGetYourId,true,true);
              }else if(retryStatus == "N"){
                frmMBeKYCFaceRecg.lblMessage.text = getLocalizedString("eKYC_msgFaceReject");
                 if(gblIDPApproveRequestFlow){
                   setFooterTwoVerticalbtnToForm(kony.i18n.getLocalizedString("CAV02_btnNext"), "", approveIDPRequest, null, true, false);
                 }else{
                   if(gblUpdateIALFlow){
                     gblIALFailCount +=1;
                     if(gblIALFailCount == 3){
                       setFooterTwoVerticalbtnToForm(kony.i18n.getLocalizedString("Home"), "", navigateToeKYCIntroduction, null, true, false);
                     }else{
                       setFooterTwoVerticalbtnToForm(kony.i18n.getLocalizedString("MB_btneKYCSelfieAgain"),kony.i18n.getLocalizedString("eKYC_btnScanPassport"),invokeFaceRecogFFI,rescanGetYourId,true,true);
                     }
                   }else{
                     setFooterTwoVerticalbtnToForm(kony.i18n.getLocalizedString("Home"), "", btnTnCBackeKYC, null, true, false);
                   }
                 }
                 
              }
            }
              frmMBeKYCFaceRecg.flxSelfiePic.animate(
              kony.ui.createAnimation({"100":{"stepConfig":{"timingFunction":kony.anim.EASE}, "centerX":"39%"}}),
              {"delay":0.25,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":1},{
              "animationEnd": null}
              );
              frmMBeKYCFaceRecg.flxPassportPic.animate(
              kony.ui.createAnimation({"100":{"stepConfig":{"timingFunction":kony.anim.EASE}, "centerX":"60%"}}),
              {"delay":0.25,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":1},{
              "animationEnd": sucessicon}
              );
          dismissLoadingScreen();
        }
        else{
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            dismissLoadingScreen();
        }
      }
      else{
         showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
         dismissLoadingScreen();
      }
    }
    catch(e){
        kony.print("Exception in one2oneVerificationServiceCallBack"+e);
        dismissLoadingScreen();
    }
 }

function sucessicon(){
  if(frmMBeKYCFaceRecg.lblErrorStatus.text === "error"){
   frmMBeKYCFaceRecg.imgError.setVisibility(true);
   }else {
    frmMBeKYCFaceRecg.flxlblcircle1.setVisibility(true);
   }
}

function onClickNextRegistrationFlow(){
  if(onDonetxtLaserCode()){
    frmMBeKYCFaceRecg.show();
  }
}

function onNextclickfrmMBeKYCFaceRecg(){
  cameraCheckPermission(showMobileProviderNo);
}

function cameraCheckPermission(callback) {
	var options = {
		isAccessModeAlways: true
	};
	var result = kony.application.checkPermission(kony.os.RESOURCE_CAMERA, options);
	if (result.status == kony.application.PERMISSION_DENIED) { //Indicates permission denied
		if (result.canRequestPermission) {
			kony.application.requestPermission(kony.os.RESOURCE_CAMERA, permissionStatusCallback);
		} else {
			var basicConfig = {
				alertType: constants.ALERT_TYPE_CONFIRMATION,
				message: kony.i18n.getLocalizedString("eKYC_msgAccessCamera"),
				alertHandler: permissionAlertHandler
			};
			kony.ui.Alert(basicConfig);
		}
	} else if (result.status == kony.application.PERMISSION_GRANTED) {
		callback();
	} else if (result.status == kony.application.PERMISSION_RESTRICTED) {
		alert(kony.i18n.getLocalizedString("eKYC_msgAccessCamera"));
	}
	function permissionAlertHandler(resp) {
		if (resp)
			kony.application.openApplicationSettings();
	}
	function permissionStatusCallback(response) {
		if (response.status == kony.os.PERMISSION_GRANTED) {
			callback();
		} else if (response.status == kony.os.PERMISSION_DENIED) {
			var basicConfig = {
				alertType: constants.ALERT_TYPE_CONFIRMATION,
				message: kony.i18n.getLocalizedString("eKYC_msgAccessCamera"),
				alertHandler: permissionAlertHandler
			};
			kony.ui.Alert(basicConfig);
		}
	}
}
