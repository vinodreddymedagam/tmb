//MKI, MIB-11728 start
var CountrySelectedDropDownId = null;
var gbleKYCSubdistrictData = null; 
var gblPrevLen = 0;
function navigatefrmMBeKYCContactInfo(){  
 // gblEncryptedCitizenID = "A0000000000002" ;
  gblsegAddressData = null;
  showLoadingScreen();
  getContactInfoMasterData("GET_ADDRESS_FROM_DB");     
}
function saveCustomerInfoDataInDB(){
  var input_param ={};
  //input_param.citizenId = gblEncryptedCitizenID;       
  input_param.regAddrNum = frmMBeKYCContactInfo.tbxAddressNo.text;
  input_param.regAddrMoo =  frmMBeKYCContactInfo.tbxMoo.text;
  input_param.regAddrBuilding = frmMBeKYCContactInfo.tbxBuildingNo.text ;
  input_param.regAddrSoi = frmMBeKYCContactInfo.tbxSoi.text ;
  input_param.regAddrRoad = frmMBeKYCContactInfo.tbxRoad.text ;
  input_param.regSubdistrict = frmMBeKYCContactInfo.tbxSubDistrict.text;
  input_param.regDistrict = frmMBeKYCContactInfo.tbxDistrict.text ;
  input_param.regProvince = frmMBeKYCContactInfo.tbxProvince.text ;
  input_param.regZipCode = frmMBeKYCContactInfo.tbxZipCode.text ;
  input_param.regCountry = frmMBeKYCContactInfo.lblCountryCode.text ;

  if(frmMBeKYCContactInfo.flxScrollAddressCurrentAddress.isVisible){
    input_param.isCurrentAddressSameAsRegisterAdd = false;
    input_param.curAddrNo = frmMBeKYCContactInfo.tbxAddressNoCurrentAddress.text ;
    input_param.curAddrMoo = frmMBeKYCContactInfo.tbxMooCurrentAddress.text ;
    input_param.curAddrBuilding = frmMBeKYCContactInfo.tbxBuildingNoCurrentAddress.text ;
    input_param.curAddrSoi = frmMBeKYCContactInfo.txtSoiCurrentAddress.text ;
    input_param.curAddrRoad = frmMBeKYCContactInfo.txtRoadCurrentAddress.text ;
    input_param.curSubdistrict = frmMBeKYCContactInfo.tbxSubDistrictCurrentAddress.text ;
    input_param.curDistrict = frmMBeKYCContactInfo.tbxDistrictCurrentAddress.text ;
    input_param.curProvince = frmMBeKYCContactInfo.tbxProvinceCurrentAddress.text ;
    input_param.curZipcode = frmMBeKYCContactInfo.tbxZipCodeCurrentAddress.text ;
    input_param.curCountry = frmMBeKYCContactInfo.lblCountryCodeCurrentAddress.text ;
    input_param.curHomePhone = replaceAll(frmMBeKYCContactInfo.txtHomePhone2.text,"-","");
    input_param.curHomeExt = frmMBeKYCContactInfo.txExtNo2.text ;
  }else{
    input_param.isCurrentAddressSameAsRegisterAdd = true;
    input_param.curAddrNo = frmMBeKYCContactInfo.tbxAddressNo.text;
    input_param.curAddrMoo = frmMBeKYCContactInfo.tbxMoo.text;
    input_param.curAddrBuilding = frmMBeKYCContactInfo.tbxBuildingNo.text ;
    input_param.curAddrSoi = frmMBeKYCContactInfo.tbxSoi.text ;
    input_param.curAddrRoad = frmMBeKYCContactInfo.tbxRoad.text ;
    input_param.curSubdistrict = frmMBeKYCContactInfo.tbxSubDistrict.text;
    input_param.curDistrict = frmMBeKYCContactInfo.tbxDistrict.text ;
    input_param.curProvince = frmMBeKYCContactInfo.tbxProvince.text ;
    input_param.curZipcode = frmMBeKYCContactInfo.tbxZipCode.text ;
    input_param.curCountry = frmMBeKYCContactInfo.lblCountryCode.text ;   
    input_param.curHomePhone = replaceAll(frmMBeKYCContactInfo.txtHomePhone.text,"-","");
    input_param.curHomeExt = frmMBeKYCContactInfo.txtExtNoContactAdd.text ;
  }

  if(frmMBeKYCContactInfo.flxScrollAddressOfficeAddress.isVisible){
    input_param.isOfficeAddressSameAsRegisterAdd = false;
    input_param.officeAddrNo = frmMBeKYCContactInfo.tbxAddressNoOfficeAddress.text ;
    input_param.officeAddrMoo = frmMBeKYCContactInfo.tbxMooOfficeAddress.text ;
    input_param.officeAddrBuilding = frmMBeKYCContactInfo.tbxBuildingNoOfficeAddress.text ;
    input_param.officeAddrSoi = frmMBeKYCContactInfo.txtSoiOfficeAddress.text ;
    input_param.officeAddrRoad = frmMBeKYCContactInfo.txtRoadOfficeAddress.text ;
    input_param.officeSubdistrict = frmMBeKYCContactInfo.tbxSubDistrictOfficeAddress.text ;
    input_param.officeDistrict = frmMBeKYCContactInfo.tbxDistrictOfficeAddress.text ;
    input_param.officeProvince = frmMBeKYCContactInfo.tbxProvinceOfficeAddress.text ;
    input_param.officeZipCode = frmMBeKYCContactInfo.tbxZipCodeOfficeAddress.text ;
    input_param.officeCountry = frmMBeKYCContactInfo.lblCountryCodeOfficeAddress.text ;
    input_param.companyName = frmMBeKYCContactInfo.txtCompanyName2.text ;
    input_param.officePhone = replaceAll(frmMBeKYCContactInfo.txtOfficePhone2.text,"-","");
    input_param.officeExt = frmMBeKYCContactInfo.txExtNoOfficeAdd2.text ;
  }else{
    input_param.isOfficeAddressSameAsRegisterAdd = true;
    input_param.officeAddrNo = frmMBeKYCContactInfo.tbxAddressNo.text;
    input_param.officeAddrMoo = frmMBeKYCContactInfo.tbxMoo.text;
    input_param.officeAddrBuilding = frmMBeKYCContactInfo.tbxBuildingNo.text ;
    input_param.officeAddrSoi = frmMBeKYCContactInfo.tbxSoi.text ;
    input_param.officeAddrRoad = frmMBeKYCContactInfo.tbxRoad.text ;
    input_param.officeSubdistrict = frmMBeKYCContactInfo.tbxSubDistrict.text;
    input_param.officeDistrict = frmMBeKYCContactInfo.tbxDistrict.text ;
    input_param.officeProvince = frmMBeKYCContactInfo.tbxProvince.text ;
    input_param.officeZipCode = frmMBeKYCContactInfo.tbxZipCode.text ;
    input_param.officeCountry = frmMBeKYCContactInfo.lblCountryCode.text ;    
    input_param.companyName = frmMBeKYCContactInfo.txtCompanyName.text ;
    input_param.officePhone = replaceAll(frmMBeKYCContactInfo.txtOfficePhone.text,"-","");
    input_param.officeExt = frmMBeKYCContactInfo.txtExtNoOfficeAdd.text ;
  }

  invokeServiceSecureAsync("saveUpdateEKycCustomerInfo", input_param, callBacksaveCustomerInfoDataInDB);
}
function callBacksaveCustomerInfoDataInDB(status, result){
  if(status == 400){
    if(result.opstatus == "0"){
      getPersonalInfoDatafromService();
    }
  }
}
function setPrePopulateDatainForm(resultData){
  frmMBeKYCContactInfo.tbxBuildingNo.text=resultData.regAddrBuilding; 
  frmMBeKYCContactInfo.tbxMoo.text=resultData.regAddrMoo;
  frmMBeKYCContactInfo.tbxAddressNo.text = resultData.regAddrNum;
  frmMBeKYCContactInfo.tbxSoi.text =resultData.regAddrSoi;
  frmMBeKYCContactInfo.tbxRoad.text = resultData.regAddrRoad;
  frmMBeKYCContactInfo.tbxSubDistrict.text=resultData.regSubdistrict;
  frmMBeKYCContactInfo.tbxDistrict.text = resultData.regDistrict;
  frmMBeKYCContactInfo.tbxProvince.text = resultData.regProvince;
  frmMBeKYCContactInfo.tbxZipCode.text = resultData.regZipCode;
  //resultData.regCountry;//Need to set logic for country
  var local = kony.i18n.getCurrentLocale();

  var SelectedCountry = [];
  for(var i =0 ; i<gblCountryCollectionData.length;i++){
    if(gblCountryCollectionData[i].countryCode == resultData.regCountry && gblCountryCollectionData[i].Lang == local){ //countryCode
      SelectedCountry = gblCountryCollectionData[i];
    }
  }

  frmMBeKYCContactInfo.lblCountryVal.text = SelectedCountry.CountryName;
  frmMBeKYCContactInfo.lblCountryCode.text = SelectedCountry.countryCode;

  var isCurrentAddressSameAsRegisterAdd =  (resultData.isCurrentAddressSameAsRegisterAdd.toLowerCase() =="true");
  if(isCurrentAddressSameAsRegisterAdd){    
    gblPrevLen = 0;
    frmMBeKYCContactInfo.txtHomePhone.text =resultData.curHomePhone;
    onEditMobileNumbereKYC(frmMBeKYCContactInfo.txtHomePhone);
    frmMBeKYCContactInfo.txtHomePhone.parent.skin = lFboxLoan;
    frmMBeKYCContactInfo.txtExtNoContactAdd.text =resultData.curHomeExt;
  }
  else{
    frmMBeKYCContactInfo.tbxAddressNoCurrentAddress.text =resultData.curAddrNo;  
    frmMBeKYCContactInfo.tbxMooCurrentAddress.text =resultData.curAddrMoo;      
    frmMBeKYCContactInfo.tbxBuildingNoCurrentAddress.text =resultData.curAddrBuilding;
    frmMBeKYCContactInfo.txtSoiCurrentAddress.text =resultData.curAddrSoi;
    frmMBeKYCContactInfo.txtRoadCurrentAddress.text = resultData.curAddrRoad;
    frmMBeKYCContactInfo.tbxSubDistrictCurrentAddress.text = resultData.curSubdistrict;    
    frmMBeKYCContactInfo.tbxDistrictCurrentAddress.text = resultData.curDistrict;
    frmMBeKYCContactInfo.tbxProvinceCurrentAddress.text = resultData.curProvince;
    frmMBeKYCContactInfo.tbxZipCodeCurrentAddress.text =resultData.curZipcode;         

    for( i =0 ; i<gblCountryCollectionData.length;i++){
      if(gblCountryCollectionData[i].countryCode == resultData.curCountry && gblCountryCollectionData[i].Lang == local){ //countryCode
        SelectedCountry = gblCountryCollectionData[i];
      }
    }

    frmMBeKYCContactInfo.lblCountryCurrentAddress.text = SelectedCountry.CountryName;
    frmMBeKYCContactInfo.lblCountryCodeCurrentAddress.text = SelectedCountry.countryCode;
	gblPrevLen = 0;
    frmMBeKYCContactInfo.txtHomePhone2.text =resultData.curHomePhone;
    onEditMobileNumbereKYC(frmMBeKYCContactInfo.txtHomePhone2);
    frmMBeKYCContactInfo.txtHomePhone2.parent.skin = lFboxLoan;
    frmMBeKYCContactInfo.txExtNo2.text =resultData.curHomeExt;
  }

  var isOfficeAddressSameAsRegisterAdd =  (resultData.isOfficeAddressSameAsRegisterAdd.toLowerCase() =="true");

  if(isOfficeAddressSameAsRegisterAdd){ 
    frmMBeKYCContactInfo.txtCompanyName.text =resultData.companyName;
    
    gblPrevLen = 0;
    frmMBeKYCContactInfo.txtOfficePhone.text = resultData.officePhone;
    onEditMobileNumbereKYC(frmMBeKYCContactInfo.txtOfficePhone);
    frmMBeKYCContactInfo.txtOfficePhone.parent.skin = lFboxLoan;
    frmMBeKYCContactInfo.txtExtNoOfficeAdd.text = resultData.officeExt;                   
  }else{
    frmMBeKYCContactInfo.tbxAddressNoOfficeAddress.text =resultData.officeAddrNo;
    frmMBeKYCContactInfo.tbxMooOfficeAddress.text =  resultData.officeAddrMoo;
    frmMBeKYCContactInfo.tbxBuildingNoOfficeAddress.text = resultData.officeAddrBuilding;

    frmMBeKYCContactInfo.txtSoiOfficeAddress.text = resultData.officeAddrSoi;
    frmMBeKYCContactInfo.txtRoadOfficeAddress.text = resultData.officeAddrRoad;
    frmMBeKYCContactInfo.tbxSubDistrictOfficeAddress.text =resultData.officeSubdistrict;
    frmMBeKYCContactInfo.tbxDistrictOfficeAddress.text =resultData.officeDistrict;
    frmMBeKYCContactInfo.tbxProvinceOfficeAddress.text =resultData.officeProvince;
    frmMBeKYCContactInfo.tbxZipCodeOfficeAddress.text =resultData.officeZipCode;

    for( i =0 ; i<gblCountryCollectionData.length;i++){
      if(gblCountryCollectionData[i].countryCode == resultData.officeCountry && gblCountryCollectionData[i].Lang == local){ //countryCode
        SelectedCountry = gblCountryCollectionData[i];
      }
    }

    frmMBeKYCContactInfo.lblCountryOfficeAddress.text = SelectedCountry.CountryName;
    frmMBeKYCContactInfo.lblCountryCodeOfficeAddress.text = SelectedCountry.countryCode;

    frmMBeKYCContactInfo.txtCompanyName2.text =resultData.companyName;
    
    gblPrevLen = 0;
    frmMBeKYCContactInfo.txtOfficePhone2.text = resultData.officePhone;
    onEditMobileNumbereKYC(frmMBeKYCContactInfo.txtOfficePhone2);
    frmMBeKYCContactInfo.txtOfficePhone2.parent.skin = lFboxLoan;
    frmMBeKYCContactInfo.txExtNoOfficeAdd2.text = resultData.officeExt;    
  }


}
function setPrePopulateUIinForm(resultData){
  onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.FlexAddressNo);
  onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.FlexMoo);
  onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.FlexBuildingNo);
  onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.FlexSoi);
  onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.FlexRoad);
  onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.flxSubDistrict);
  onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.flxDistrict);
  onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.flxProvince);
  onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.flxZipCode);

  CountrySelectedDropDownId= "flxCountry";
  setCountryListPlaceHolderVisibility(false);

  var isCurrentAddressSameAsRegisterAdd =  (resultData.isCurrentAddressSameAsRegisterAdd.toLowerCase() == "true");

  if(isCurrentAddressSameAsRegisterAdd){
    onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.flxHomePhoneSub);
    onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.flxExtNoContactAdd);
    frmMBeKYCContactInfo.flxChkbxCurrentAddress.skin = skinFilledluechkedChkbx;
    
    frmMBeKYCContactInfo.flxChkbxCurrentAddress.skin = "skinFilledluechkedChkbx";
    frmMBeKYCContactInfo.lblCurrAddChangeNote.isVisible = true;
    frmMBeKYCContactInfo.flxHomePhoneMain.isVisible = true;
    frmMBeKYCContactInfo.flxScrollAddressCurrentAddress.isVisible = false;
  }else{
    onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.FlexAddressNoCurrentAddress);
    onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.FlexMooCurrentAddress);
    onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.FlexBuildingNoCurrentAddress);
    onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.FlexSoiCurrentAddress);
    onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.FlexRoadCurrentAddress);
    onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.flxSubDistrictCurrentAddress);
    onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.flxDistrictCurrentAddress);
    onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.flxProvinceCurrentAddress);
    onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.flxZipCodeCurrentAddress);
    onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.flxHomePhoneSub2);
    onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.flxExtNo2);
    frmMBeKYCContactInfo.flxChkbxCurrentAddress.skin = skinFilledlueUnchkedChkbx;

    frmMBeKYCContactInfo.flxChkbxCurrentAddress.skin = "skinFilledlueUnchkedChkbx";
    frmMBeKYCContactInfo.lblCurrAddChangeNote.isVisible = false;
    frmMBeKYCContactInfo.flxHomePhoneMain.isVisible = false;
    frmMBeKYCContactInfo.flxScrollAddressCurrentAddress.isVisible = true; 

    CountrySelectedDropDownId= "flxCountryCurrentAddress";
    setCountryListPlaceHolderVisibility(false);
  }
  //onClickFlexAddressChkbx(frmMBeKYCContactInfo.flxCurrentAddressChkbx);
  var isOfficeAddressSameAsRegisterAdd =  (resultData.isOfficeAddressSameAsRegisterAdd.toLowerCase() == "true");

  if(isOfficeAddressSameAsRegisterAdd){
    onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.flxCompanyName);
    onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.flxOfficePhoneSub);
    onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.flxExtNoOfficeAdd);
    frmMBeKYCContactInfo.flxChkbxOfficeAddress.skin = skinFilledluechkedChkbx;
    

    frmMBeKYCContactInfo.lblOfficeAddChangeNote.isVisible = true;
    frmMBeKYCContactInfo.flxCompanyName.isVisible = true;
    frmMBeKYCContactInfo.flxOfficePhoneMain.isVisible = true;
    frmMBeKYCContactInfo.flxScrollAddressOfficeAddress.isVisible = false;
  }else{
    onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.FlexAddressNoOfficeAddress);
    onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.FlexMooOfficeAddress);
    onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.FlexBuildingNoOfficeAddress);
    onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.FlexSoiOfficeAddress);
    onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.FlexRoadOfficeAddress);
    onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.flxSubDistrictOfficeAddress);
    onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.flxDistrictOfficeAddress);
    onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.flxProvinceOfficeAddress);
    onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.flxZipCodeOfficeAddress);
    onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.flxCompanyName2);
    onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.flxOfficePhoneSub2);
    onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.flxExtNoOfficeAdd2);
    frmMBeKYCContactInfo.flxChkbxOfficeAddress.skin = skinFilledlueUnchkedChkbx;

    frmMBeKYCContactInfo.flxChkbxOfficeAddress.skin = "skinFilledlueUnchkedChkbx";
    frmMBeKYCContactInfo.lblOfficeAddChangeNote.isVisible = false;
    frmMBeKYCContactInfo.flxCompanyName.isVisible = false;
    frmMBeKYCContactInfo.flxOfficePhoneMain.isVisible = false;
    frmMBeKYCContactInfo.flxScrollAddressOfficeAddress.isVisible = true; 

    CountrySelectedDropDownId= "flxCountryOfficeAddress";
    setCountryListPlaceHolderVisibility(false);
  }
  
}
function seteKYCCustomerTransactionDataPrepopulated(resultData){
  setPrePopulateUIinForm(resultData);
  setPrePopulateDatainForm(resultData);

}
function callBackgetContactInfoTransactionData(status, result){
  if (status == 400) {
    try{
      if (result.opstatus == "0"  ){
        if((result.regAddrBuilding !==undefined && result.regAddrBuilding !== null && result.regAddrBuilding !== "" )) {   
          seteKYCCustomerTransactionDataPrepopulated(result);                       
        }       
        frmMBeKYCContactInfo.show();   
      }else {
        dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        return false;
      }
    }catch(e){
      alert("Exception in callbackOfgetPersonalInfoFields, e : "+e);
    }
  }
}
// Get data from service------------------ Start
function getContactInfoMasterData(catagoryCode){
  var input_param = {};
  invokeServiceSecureAsync("GetSubdistrictforContactInfo", input_param, callbackFullAddressContactInfo);
}
// Get data from service------------------ End

// callback functions--------------------- StartsetCountryDataforDropDown
function callbackFullAddressContactInfo(status, result){
  // kony.print("result[addressDetails] = "+ result["addressDetails"]);
  try{
    if (status == 400) {  
      var segAddressData = [];
      if (result.opstatus == "0" && result.addressDetails !== null && result.addressDetails !== undefined) {
        dismissLoadingScreen();
        var temprowData = {};       
        var fullAddress = result.addressDetails;
        fullAddress.sort(dynamicSortContactInfo("subDistProvince"));
        for (var i = 0; i < fullAddress.length; i++) {
          var subDistrictFulladdress = "";
          gbleKYCSubdistrictData = fullAddress;
          subDistrictFulladdress = gbleKYCSubdistrictData[i].subDistProvince;
          temprowData = {
            "lblTotalAddress": subDistrictFulladdress,
          };  
          segAddressData.push(temprowData);          
        }

        gblCountryCollectionData =result.CountryList;  
        setDefaultCountryForThailand();
        //setCountryDataforDropDown();

        var input_param= {};                  
        //input_param.citizenId = gblEncryptedCitizenID;
        invokeServiceSecureAsync("getEKycCustomerInfo", input_param, callBackgetContactInfoTransactionData); 
      }else {
        dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        return false;
      }
      gblsegAddressData = segAddressData;
        
    }
  }catch(e){
    alert("Exception in callbackOfgetPersonalInfoFields, e : "+e);
  }
}
function getContactInfoMasterDataCallback(status, result){
  if (status == 400) {
    try{
      if (result.opstatus == "0") {                                       
        gblCountryCollectionData =result.CountryList;     

        setCountryDataforDropDown(gblCountryCollectionData);
      }else {
        dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        return false;
      }
    }catch(e){
      alert("Exception in callbackOfgetPersonalInfoFields, e : "+e);
    }
  }
}
// callback functions--------------------- End

// Form Level Events------------------------- Start
function frmMBeKYCContactInfoInit(){
  frmMBeKYCContactInfo.preShow =  ContactInformationPreShow;
}
function ContactInformationPreShow(){ 
  setHeaderContentToForm(kony.i18n.getLocalizedString("Kyc_lbl_contactInfo"),null,false,"3");
  setFooterSinglebtnToForm(kony.i18n.getLocalizedString("Next"),onClickbtnNextContactInfo);
  frmMBeKYCContactInfo.onDeviceBack = disableBackButton;
  linkContactInfoEvents();	
  isDistrictTxtEditEnable();
  setTxtbxKeyboardActionLabelasDone();
  dismissLoadingScreen();
}

function setTxtbxKeyboardActionLabelasDone(){
  frmMBeKYCContactInfo.tbxAddressNo.keyboardActionLabel=constants.TEXTBOX_KEYBOARD_LABEL_DONE;
  frmMBeKYCContactInfo.tbxMoo.keyboardActionLabel=constants.TEXTBOX_KEYBOARD_LABEL_DONE;
  frmMBeKYCContactInfo.tbxBuildingNo.keyboardActionLabel=constants.TEXTBOX_KEYBOARD_LABEL_DONE;
  frmMBeKYCContactInfo.tbxSoi.keyboardActionLabel=constants.TEXTBOX_KEYBOARD_LABEL_DONE;
  frmMBeKYCContactInfo.tbxRoad.keyboardActionLabel=constants.TEXTBOX_KEYBOARD_LABEL_DONE;
  frmMBeKYCContactInfo.tbxSubDistrict.keyboardActionLabel=constants.TEXTBOX_KEYBOARD_LABEL_DONE;
  frmMBeKYCContactInfo.tbxDistrict.keyboardActionLabel=constants.TEXTBOX_KEYBOARD_LABEL_DONE;
  frmMBeKYCContactInfo.tbxProvince.keyboardActionLabel=constants.TEXTBOX_KEYBOARD_LABEL_DONE;
  frmMBeKYCContactInfo.tbxZipCode.keyboardActionLabel=constants.TEXTBOX_KEYBOARD_LABEL_DONE;
  //--Country-------------------
  frmMBeKYCContactInfo.txtHomePhone.keyboardActionLabel=constants.TEXTBOX_KEYBOARD_LABEL_DONE;
  frmMBeKYCContactInfo.txtExtNoContactAdd.keyboardActionLabel=constants.TEXTBOX_KEYBOARD_LABEL_DONE;
  frmMBeKYCContactInfo.tbxAddressNoCurrentAddress.keyboardActionLabel=constants.TEXTBOX_KEYBOARD_LABEL_DONE;
  frmMBeKYCContactInfo.tbxMooCurrentAddress.keyboardActionLabel=constants.TEXTBOX_KEYBOARD_LABEL_DONE;
  frmMBeKYCContactInfo.tbxBuildingNoCurrentAddress.keyboardActionLabel=constants.TEXTBOX_KEYBOARD_LABEL_DONE;
  frmMBeKYCContactInfo.txtSoiCurrentAddress.keyboardActionLabel=constants.TEXTBOX_KEYBOARD_LABEL_DONE;
  frmMBeKYCContactInfo.txtRoadCurrentAddress.keyboardActionLabel=constants.TEXTBOX_KEYBOARD_LABEL_DONE;
  frmMBeKYCContactInfo.tbxSubDistrictCurrentAddress.keyboardActionLabel=constants.TEXTBOX_KEYBOARD_LABEL_DONE;
  frmMBeKYCContactInfo.tbxDistrictCurrentAddress.keyboardActionLabel=constants.TEXTBOX_KEYBOARD_LABEL_DONE;
  frmMBeKYCContactInfo.tbxProvinceCurrentAddress.keyboardActionLabel=constants.TEXTBOX_KEYBOARD_LABEL_DONE;
  frmMBeKYCContactInfo.tbxZipCodeCurrentAddress.keyboardActionLabel=constants.TEXTBOX_KEYBOARD_LABEL_DONE;
  frmMBeKYCContactInfo.txtHomePhone2.keyboardActionLabel=constants.TEXTBOX_KEYBOARD_LABEL_DONE;
  frmMBeKYCContactInfo.txExtNo2.keyboardActionLabel=constants.TEXTBOX_KEYBOARD_LABEL_DONE;
  //--Office--------------------------
  frmMBeKYCContactInfo.txtCompanyName.keyboardActionLabel=constants.TEXTBOX_KEYBOARD_LABEL_DONE;
  frmMBeKYCContactInfo.txtOfficePhone.keyboardActionLabel=constants.TEXTBOX_KEYBOARD_LABEL_DONE;
  frmMBeKYCContactInfo.txtExtNoOfficeAdd.keyboardActionLabel=constants.TEXTBOX_KEYBOARD_LABEL_DONE;
  frmMBeKYCContactInfo.tbxAddressNoOfficeAddress.keyboardActionLabel=constants.TEXTBOX_KEYBOARD_LABEL_DONE;
  frmMBeKYCContactInfo.tbxMooOfficeAddress.keyboardActionLabel=constants.TEXTBOX_KEYBOARD_LABEL_DONE;
  frmMBeKYCContactInfo.tbxBuildingNoOfficeAddress.keyboardActionLabel=constants.TEXTBOX_KEYBOARD_LABEL_DONE;
  frmMBeKYCContactInfo.txtSoiOfficeAddress.keyboardActionLabel=constants.TEXTBOX_KEYBOARD_LABEL_DONE;
  frmMBeKYCContactInfo.txtRoadOfficeAddress.keyboardActionLabel=constants.TEXTBOX_KEYBOARD_LABEL_DONE;
  frmMBeKYCContactInfo.tbxSubDistrictOfficeAddress.keyboardActionLabel=constants.TEXTBOX_KEYBOARD_LABEL_DONE;
  frmMBeKYCContactInfo.tbxDistrictOfficeAddress.keyboardActionLabel=constants.TEXTBOX_KEYBOARD_LABEL_DONE;
  frmMBeKYCContactInfo.tbxProvinceOfficeAddress.keyboardActionLabel=constants.TEXTBOX_KEYBOARD_LABEL_DONE;
  frmMBeKYCContactInfo.tbxZipCodeOfficeAddress.keyboardActionLabel=constants.TEXTBOX_KEYBOARD_LABEL_DONE;
  frmMBeKYCContactInfo.txtCompanyName2.keyboardActionLabel=constants.TEXTBOX_KEYBOARD_LABEL_DONE;
  frmMBeKYCContactInfo.txtOfficePhone2.keyboardActionLabel=constants.TEXTBOX_KEYBOARD_LABEL_DONE;
  frmMBeKYCContactInfo.txExtNoOfficeAdd2.keyboardActionLabel=constants.TEXTBOX_KEYBOARD_LABEL_DONE;

}
// Form Level Events------------------------ End

//Country Dropdown Related Functions-------------------- Start
function setDefaultCountryForThailand(){
  var countryCode= eKYCconfigObject.DefaultCountry.CountryCode;//"TH";
  var countryName= "";//eKYCconfigObject.DefaultCountry.CountryName; //"Thailand";
  var locale = kony.i18n.getCurrentLocale();
  for(var i=0 ; i < gblCountryCollectionData.length ; i++){
    if(countryCode == gblCountryCollectionData[i].countryCode && locale === gblCountryCollectionData[i].Lang){
      countryName = gblCountryCollectionData[i].CountryName ;
      break;
    }
  }
  CountrySelectedDropDownId= "flxCountry";
  setCountryListPlaceHolderVisibility(false);
  frmMBeKYCContactInfo.lblCountryCode.text = countryCode;
  frmMBeKYCContactInfo.lblCountryVal.text = countryName;

  CountrySelectedDropDownId= "flxCountryCurrentAddress";
  setCountryListPlaceHolderVisibility(false);
  frmMBeKYCContactInfo.lblCountryCodeCurrentAddress.text = countryCode;
  frmMBeKYCContactInfo.lblCountryCurrentAddress.text = countryName;

  CountrySelectedDropDownId= "flxCountryOfficeAddress";
  setCountryListPlaceHolderVisibility(false);
  frmMBeKYCContactInfo.lblCountryCodeOfficeAddress.text = countryCode;
  frmMBeKYCContactInfo.lblCountryOfficeAddress.text = countryName;
}
function setCountryDataforDropDown(countryCollectionData){
  var selectedFlag = false;
  var selectedStatus="";
  var countryLabel = false;
  var CountryLabelObj = null;

  if(CountrySelectedDropDownId == "flxCountry"){
    CountryLabelObj = frmMBeKYCContactInfo.lblCountryVal ;
  }else if(CountrySelectedDropDownId == "flxCountryCurrentAddress"){
    CountryLabelObj = frmMBeKYCContactInfo.lblCountryCurrentAddress ;
  }else if(CountrySelectedDropDownId == "flxCountryOfficeAddress"){
    CountryLabelObj = frmMBeKYCContactInfo.lblCountryOfficeAddress ;
  }

  if(CountryLabelObj.isVisible) {
    selectedFlag = true;     
    selectedStatus = CountryLabelObj.text;
  }
  setCountryDropDownDatainSeg(countryCollectionData,selectedFlag,selectedStatus,"SCI_COUNTRY");
}
function setCountryDropDownDatainSeg(content,selectedFlag,selectedStatus, flowName) {
  var locale = kony.i18n.getCurrentLocale();
  var currForm =kony.application.getCurrentForm();
  var segData = [];

  if(gblCountryCollectionData.length> 15 && content !== gblPersonalInfo.EMPLOYMENT_STATUS){
    currForm.flxSearch.setVisibility(true);
  }else {
    currForm.flxSearch.setVisibility(false);
  }
  content.sort(dynamicSortLoan("CountryName"));
  for (var i = 0; i < content.length; i++) {
    kony.print("getcontent @@ in loop");
    var entryName = content[i].CountryName;
    var entryCode = content[i].countryCode;
    var entryID = content[i].countryCode;
    var temprowData = {};
    var dataLocal = content[i].Lang ;
    if (selectedFlag && selectedStatus == entryName) {
      kony.print("getcontent @@ matched");
      temprowData = {
        "lblName": entryName,
        "lblSegRowLine": ".",
        "imgTick": "tick_icon.png",
        "entryID": entryID,
        "lblEntryCode": entryCode,
        "flxSegLoanGeneric": {
          "skin": flexGreyBG
        }
      };
      segData.push(temprowData);
      kony.print("getcontent @@ pushed selected");
    } else {
      temprowData = {
        "lblName": entryName,
        "lblSegRowLine": ".",
        "imgTick": "",
        "entryID": entryID,
        "lblEntryCode": entryCode,
        "flxSegLoanGeneric": {
          "skin": flexWhiteBG
        }
      };
      if (locale == dataLocal) {
        segData.push(temprowData);
      } 

    }
  }
  frmMBeKYCContactInfo.segSourceofIncome.removeAll();
  //if(null !== flowName && "SCI_COUNTRY"=== flowName){
  frmMBeKYCContactInfo.lblSIHeading.text = kony.i18n.getLocalizedString("Country_Label");
  frmMBeKYCContactInfo.segSourceofIncome.onRowClick = segCountrySourceofIncomeonRowClick;
  frmMBeKYCContactInfo.btnCloseSIPopUp.onClick = btnCloseCountrySIPopUponClickscroldown;
  frmMBeKYCContactInfo.segSourceofIncome.setData(segData);
  //frmMBeKYCContactInfo.flxScrollAddress.setVisibility(false);  
  frmMBeKYCContactInfo.flxSourceOfIncomePopUp.setVisibility(true);
  frmMBeKYCContactInfo.headers[0].isVisible = false;
  frmMBeKYCContactInfo.footer[0].isVisible = false;
  dismissLoadingScreen();
  //}	
}
function btnCloseCountrySIPopUponClickscroldown (){
  //frmMBeKYCContactInfo.flxScrollAddress.setVisibility(true);  
  frmMBeKYCContactInfo.flxSourceOfIncomePopUp.setVisibility(false);
  frmMBeKYCContactInfo.headers[0].isVisible = true;
  frmMBeKYCContactInfo.footer[0].isVisible = true;
  var widgetToscroll = frmMBeKYCContactInfo[CountrySelectedDropDownId] ;
  isDistrictTxtEditEnable();

}
function setCountryListPlaceHolderVisibility(IsVisibleBlueText){
  if(CountrySelectedDropDownId== "flxCountry"){
    frmMBeKYCContactInfo.lblCountryBlue.isVisible = IsVisibleBlueText;
    frmMBeKYCContactInfo.lblCountryBlack.isVisible = !IsVisibleBlueText;
    frmMBeKYCContactInfo.lblCountryVal.isVisible = !IsVisibleBlueText;      
  }
  else if(CountrySelectedDropDownId== "flxCountryCurrentAddress"){
    frmMBeKYCContactInfo.lblCountryBlueCurrentAddress.isVisible = IsVisibleBlueText;
    frmMBeKYCContactInfo.lblCountryBlackCurrentAddress.isVisible = !IsVisibleBlueText;
    frmMBeKYCContactInfo.lblCountryCurrentAddress.isVisible = !IsVisibleBlueText;  	
  }else if(CountrySelectedDropDownId == "flxCountryOfficeAddress"){
    frmMBeKYCContactInfo.lblCountryBlueOfficeAddress.isVisible = IsVisibleBlueText;
    frmMBeKYCContactInfo.lblCountryBlackOfficeAddress.isVisible = !IsVisibleBlueText;
    frmMBeKYCContactInfo.lblCountryOfficeAddress.isVisible = !IsVisibleBlueText;  	
  }
}
function segCountrySourceofIncomeonRowClick (){
  var countryName = frmMBeKYCContactInfo.segSourceofIncome.selectedRowItems[0].lblName;  	
  var countryCode = frmMBeKYCContactInfo.segSourceofIncome.selectedRowItems[0].lblEntryCode;  	

  frmMBeKYCContactInfo.flxSourceOfIncomePopUp.setVisibility(false);
  frmMBeKYCContactInfo.headers[0].isVisible = true;
  frmMBeKYCContactInfo.footers[0].isVisible = true;
  setCountryListPlaceHolderVisibility(false);
  if(CountrySelectedDropDownId == "flxCountry"){    
    frmMBeKYCContactInfo.lblCountryCode.text = countryCode;
    frmMBeKYCContactInfo.lblCountryVal.text = countryName;	
    resetEditedTextBoxContactInfo(frmMBeKYCContactInfo.lblMainSubDistrict,frmMBeKYCContactInfo.lblSubDistrict,frmMBeKYCContactInfo.tbxSubDistrict);  
    resetEditedTextBoxContactInfo(frmMBeKYCContactInfo.lblMainDistrict,frmMBeKYCContactInfo.lblDistrict,frmMBeKYCContactInfo.tbxDistrict);  
    resetEditedTextBoxContactInfo(frmMBeKYCContactInfo.lblMainProvince,frmMBeKYCContactInfo.lblProvince,frmMBeKYCContactInfo.tbxProvince);  
    resetEditedTextBoxContactInfo(frmMBeKYCContactInfo.lblMainZipCode,frmMBeKYCContactInfo.lblZipCode,frmMBeKYCContactInfo.tbxZipCode); 

    frmMBeKYCContactInfo.tbxSubDistrict.setFocus(true);
  }else if(CountrySelectedDropDownId == "flxCountryCurrentAddress"){    	
    frmMBeKYCContactInfo.lblCountryCodeCurrentAddress.text = countryCode;
    frmMBeKYCContactInfo.lblCountryCurrentAddress.text = countryName;
    resetEditedTextBoxContactInfo(frmMBeKYCContactInfo.lblMainSubDistrictCurrentAddress,frmMBeKYCContactInfo.lblSubDistrictCurrentAddress,frmMBeKYCContactInfo.tbxSubDistrictCurrentAddress); 
    resetEditedTextBoxContactInfo(frmMBeKYCContactInfo.lblMainDistrictCurrentAddress,frmMBeKYCContactInfo.lblDistrictCurrentAddress,frmMBeKYCContactInfo.tbxDistrictCurrentAddress);  
    resetEditedTextBoxContactInfo(frmMBeKYCContactInfo.lblMainProvinceCurrentAddress,frmMBeKYCContactInfo.lblProvinceCurrentAddress,frmMBeKYCContactInfo.tbxProvinceCurrentAddress);  
    resetEditedTextBoxContactInfo(frmMBeKYCContactInfo.lblMainZipCodeCurrentAddress,frmMBeKYCContactInfo.lblZipCodeCurrentAddress,frmMBeKYCContactInfo.tbxZipCodeCurrentAddress);    
    frmMBeKYCContactInfo.tbxSubDistrictCurrentAddress.setFocus(true);
  }else if(CountrySelectedDropDownId == "flxCountryOfficeAddress"){    
    frmMBeKYCContactInfo.lblCountryCodeOfficeAddress.text = countryCode;
    frmMBeKYCContactInfo.lblCountryOfficeAddress.text = countryName;    
    resetEditedTextBoxContactInfo(frmMBeKYCContactInfo.lblMainSubDistrictOfficeAddress,frmMBeKYCContactInfo.lblSubDistrictOfficeAddress,frmMBeKYCContactInfo.tbxSubDistrictOfficeAddress);  
    resetEditedTextBoxContactInfo(frmMBeKYCContactInfo.lblMainDistrictOfficeAddress,frmMBeKYCContactInfo.lblDistrictOfficeAddress,frmMBeKYCContactInfo.tbxDistrictOfficeAddress);  
    resetEditedTextBoxContactInfo(frmMBeKYCContactInfo.lblMainProvinceOfficeAddress,frmMBeKYCContactInfo.lblProvinceOfficeAddress,frmMBeKYCContactInfo.tbxProvinceOfficeAddress);  
    resetEditedTextBoxContactInfo(frmMBeKYCContactInfo.lblMainZipCodeOfficeAddress,frmMBeKYCContactInfo.lblZipCodeOfficeAddress,frmMBeKYCContactInfo.tbxZipCodeOfficeAddress);     
    frmMBeKYCContactInfo.tbxSubDistrictOfficeAddress.setFocus(true);
  }   
  isDistrictTxtEditEnable();
}
function onClickCountryDropDown(dropDownType){ 
  var dropDownTypeVal = "";

  frmMBeKYCContactInfo.tbxSearch.text = "";
  if(null !== dropDownType && null !== dropDownType.id){
    dropDownTypeVal = dropDownType.id;
    CountrySelectedDropDownId = dropDownTypeVal;
  }
  var categoryCode = "SCI_COUNTRY";
  if( gblCountryCollectionData !== null && gblCountryCollectionData !== ""  ){
    setCountryDataforDropDown(gblCountryCollectionData);
  }
  else{
    getContactInfoMasterData(categoryCode);  
  }
}
function searchCountryByNameContactinfo(){

  var currForm = kony.application.getCurrentForm();
  var userInput = currForm.tbxSearch.text;
  kony.print("Search String ::: "+userInput);
  
  var tempDataToPass = [];
  var countMatchedKey = 0;
  var legthOfuserInput = userInput.length;
  var locale = kony.i18n.getCurrentLocale();
  if(legthOfuserInput === 0){
    kony.print("Reset All Values and legthOfSearchString is "+legthOfuserInput);
    setCountryDataforDropDown(gblCountryCollectionData);
     if (currForm == frmMBeKYCContactInfo) {
      frmMBeKYCContactInfo.flxScrllSourceOfIncome.setVisibility(true);
      frmMBeKYCContactInfo.flxInformationMsg.setVisibility(false);
    }
  }else if(legthOfuserInput >= 1){
    kony.print("Reset All Values and legthOfSearchString is "+legthOfuserInput);

    userInput = userInput.toLowerCase();


    for (var countVal = 0; countVal < gblCountryCollectionData.length; countVal++){
      var keyToSearch = "";
      if (locale == "th_TH") {
        keyToSearch = gblCountryCollectionData[countVal].CountryName;
      } else {				
        keyToSearch  = gblCountryCollectionData[countVal].CountryName;
      }
      keyToSearch = keyToSearch.toLowerCase();
      kony.print("userInput "+userInput +" keyToSearch "+keyToSearch);
      if(keyToSearch.indexOf(userInput) >=0 ){
        tempDataToPass[countMatchedKey] = gblCountryCollectionData[countVal];
        countMatchedKey = countMatchedKey+1;
      }
    }
    kony.print("tempDataToPass "+tempDataToPass.length);
    setCountryDataforDropDown(tempDataToPass);
   
  }else{
    return;
  }   
  if (countMatchedKey === 0) {
    frmMBeKYCContactInfo.flxInformationMsg.setVisibility(true);
    frmMBeKYCContactInfo.flxScrllSourceOfIncome.setVisibility(false);
  } else {
    frmMBeKYCContactInfo.flxInformationMsg.setVisibility(false);
    frmMBeKYCContactInfo.flxScrllSourceOfIncome.setVisibility(true);
  }
}
//Country Dropdown Related Functions-------------------- End

//SubDistrict Dropdown Related Functions-------------------- Start
function isDistrictTxtEditEnable(){
  if(frmMBeKYCContactInfo.lblCountryCode.text == "TH"){
    frmMBeKYCContactInfo.flxDistrict.onClick = disableBackButton;
    frmMBeKYCContactInfo.flxProvince.onClick = disableBackButton;
    frmMBeKYCContactInfo.flxZipCode.onClick = disableBackButton;
    frmMBeKYCContactInfo.tbxDistrict.setEnabled(false);
    frmMBeKYCContactInfo.tbxProvince.setEnabled(false);
    frmMBeKYCContactInfo.tbxZipCode.setEnabled(false);
  }
  else{
    frmMBeKYCContactInfo.flxDistrict.onClick = onFlexClickActivateTextboxContactInfo;
    frmMBeKYCContactInfo.flxProvince.onClick = onFlexClickActivateTextboxContactInfo;
    frmMBeKYCContactInfo.flxZipCode.onClick = onFlexClickActivateTextboxContactInfo;
    frmMBeKYCContactInfo.tbxDistrict.setEnabled(true);
    frmMBeKYCContactInfo.tbxProvince.setEnabled(true);
    frmMBeKYCContactInfo.tbxZipCode.setEnabled(true);
  }

  if(frmMBeKYCContactInfo.lblCountryCodeCurrentAddress.text == "TH"){
    frmMBeKYCContactInfo.flxDistrictCurrentAddress.onClick = disableBackButton;
    frmMBeKYCContactInfo.flxProvinceCurrentAddress.onClick = disableBackButton;
    frmMBeKYCContactInfo.flxZipCodeCurrentAddress.onClick = disableBackButton;
    frmMBeKYCContactInfo.tbxDistrictCurrentAddress.setEnabled(false);
    frmMBeKYCContactInfo.tbxProvinceCurrentAddress.setEnabled(false);
    frmMBeKYCContactInfo.tbxZipCodeCurrentAddress.setEnabled(false);
  }
  else{
    frmMBeKYCContactInfo.flxDistrictCurrentAddress.onClick = onFlexClickActivateTextboxContactInfo;
    frmMBeKYCContactInfo.flxProvinceCurrentAddress.onClick = onFlexClickActivateTextboxContactInfo;
    frmMBeKYCContactInfo.flxZipCodeCurrentAddress.onClick = onFlexClickActivateTextboxContactInfo;
    frmMBeKYCContactInfo.tbxDistrictCurrentAddress.setEnabled(true);
    frmMBeKYCContactInfo.tbxProvinceCurrentAddress.setEnabled(true);
    frmMBeKYCContactInfo.tbxZipCodeCurrentAddress.setEnabled(true);
  }

  if(frmMBeKYCContactInfo.lblCountryCodeOfficeAddress.text == "TH"){
    frmMBeKYCContactInfo.flxDistrictOfficeAddress.onClick = disableBackButton;
    frmMBeKYCContactInfo.flxProvinceOfficeAddress.onClick = disableBackButton;
    frmMBeKYCContactInfo.flxZipCodeOfficeAddress.onClick = disableBackButton;
    frmMBeKYCContactInfo.tbxDistrictOfficeAddress.setEnabled(false);
    frmMBeKYCContactInfo.tbxProvinceOfficeAddress.setEnabled(false);
    frmMBeKYCContactInfo.tbxZipCodeOfficeAddress.setEnabled(false);
  }
  else{
    frmMBeKYCContactInfo.flxDistrictOfficeAddress.onClick = onFlexClickActivateTextboxContactInfo;
    frmMBeKYCContactInfo.flxProvinceOfficeAddress.onClick = onFlexClickActivateTextboxContactInfo;
    frmMBeKYCContactInfo.flxZipCodeOfficeAddress.onClick = onFlexClickActivateTextboxContactInfo;
    frmMBeKYCContactInfo.tbxDistrictOfficeAddress.setEnabled(true);
    frmMBeKYCContactInfo.tbxProvinceOfficeAddress.setEnabled(true);
    frmMBeKYCContactInfo.tbxZipCodeOfficeAddress.setEnabled(true);
  }
}
function selectFullAddressContactInfo(obj){
 
  var lblFullAddresstext = obj.selectedRowItems[0].lblTotalAddress;
  var addressSplit = replaceAll(lblFullAddresstext," >> " , "~");
  if(obj.id == "segSubDistrictSearch"){
    frmMBeKYCContactInfo.flxSubDistrict.skin = lFboxLoan ;
    frmMBeKYCContactInfo.flxSearchAddress.setVisibility(false);
    frmMBeKYCContactInfo.tbxSubDistrict.text = addressSplit.split("~")[0] !== undefined ? addressSplit.split("~")[0] : "";
    frmMBeKYCContactInfo.lblDistrict.setVisibility(true);
    frmMBeKYCContactInfo.tbxDistrict.setVisibility(true);
    frmMBeKYCContactInfo.lblMainDistrict.setVisibility(false);
    frmMBeKYCContactInfo.tbxDistrict.text = addressSplit.split("~")[1] !== undefined ? addressSplit.split("~")[1] : "";
    frmMBeKYCContactInfo.lblProvince.setVisibility(true);
    frmMBeKYCContactInfo.tbxProvince.setVisibility(true);
    frmMBeKYCContactInfo.lblMainProvince.setVisibility(false);
    frmMBeKYCContactInfo.tbxProvince.text = addressSplit.split("~")[2] !== undefined ? addressSplit.split("~")[2] : "";
    frmMBeKYCContactInfo.lblZipCode.setVisibility(true);
    frmMBeKYCContactInfo.tbxZipCode.setVisibility(true);
    frmMBeKYCContactInfo.lblMainZipCode.setVisibility(false);
    frmMBeKYCContactInfo.tbxZipCode.text = addressSplit.split("~")[3] !== undefined ? addressSplit.split("~")[3] : "";
  }else if(obj.id == "segSubDistrictSearchCurrentAddress"){
    frmMBeKYCContactInfo.flxSubDistrictCurrentAddress.skin = lFboxLoan ;
    frmMBeKYCContactInfo.flxSearchAddressCurrentAddress.setVisibility(false);
    frmMBeKYCContactInfo.tbxSubDistrictCurrentAddress.text = addressSplit.split("~")[0] !== undefined ? addressSplit.split("~")[0] : "";
    frmMBeKYCContactInfo.lblDistrictCurrentAddress.setVisibility(true);
    frmMBeKYCContactInfo.tbxDistrictCurrentAddress.setVisibility(true);
    frmMBeKYCContactInfo.lblMainDistrictCurrentAddress.setVisibility(false);
    frmMBeKYCContactInfo.tbxDistrictCurrentAddress.text = addressSplit.split("~")[1] !== undefined ? addressSplit.split("~")[1] : "";
    frmMBeKYCContactInfo.lblProvinceCurrentAddress.setVisibility(true);
    frmMBeKYCContactInfo.tbxProvinceCurrentAddress.setVisibility(true);
    frmMBeKYCContactInfo.lblMainProvinceCurrentAddress.setVisibility(false);
    frmMBeKYCContactInfo.tbxProvinceCurrentAddress.text = addressSplit.split("~")[2] !== undefined ? addressSplit.split("~")[2] : "";
    frmMBeKYCContactInfo.lblZipCodeCurrentAddress.setVisibility(true);
    frmMBeKYCContactInfo.tbxZipCodeCurrentAddress.setVisibility(true);
    frmMBeKYCContactInfo.lblMainZipCodeCurrentAddress.setVisibility(false);
    frmMBeKYCContactInfo.tbxZipCodeCurrentAddress.text = addressSplit.split("~")[3] !== undefined ? addressSplit.split("~")[3] : "";
  }else if(obj.id == "segSubDistrictSearchOfficeAddress"){
    frmMBeKYCContactInfo.flxSubDistrictOfficeAddress.skin = lFboxLoan ;
    frmMBeKYCContactInfo.flxSearchAddressOfficeAddress.setVisibility(false);
    frmMBeKYCContactInfo.tbxSubDistrictOfficeAddress.text = addressSplit.split("~")[0] !== undefined ? addressSplit.split("~")[0] : "";
    frmMBeKYCContactInfo.lblDistrictOfficeAddress.setVisibility(true);
    frmMBeKYCContactInfo.tbxDistrictOfficeAddress.setVisibility(true);
    frmMBeKYCContactInfo.lblMainDistrictOfficeAddress.setVisibility(false);
    frmMBeKYCContactInfo.tbxDistrictOfficeAddress.text = addressSplit.split("~")[1] !== undefined ? addressSplit.split("~")[1] : "";
    frmMBeKYCContactInfo.lblProvinceOfficeAddress.setVisibility(true);
    frmMBeKYCContactInfo.tbxProvinceOfficeAddress.setVisibility(true);
    frmMBeKYCContactInfo.lblMainProvinceOfficeAddress.setVisibility(false);
    frmMBeKYCContactInfo.tbxProvinceOfficeAddress.text = addressSplit.split("~")[2] !== undefined ? addressSplit.split("~")[2] : "";
    frmMBeKYCContactInfo.lblZipCodeOfficeAddress.setVisibility(true);
    frmMBeKYCContactInfo.tbxZipCodeOfficeAddress.setVisibility(true);
    frmMBeKYCContactInfo.lblMainZipCodeOfficeAddress.setVisibility(false);
    frmMBeKYCContactInfo.tbxZipCodeOfficeAddress.text = addressSplit.split("~")[3] !== undefined ? addressSplit.split("~")[3] : "";
  }


}
function searchsubdistrictContactInfo(obj){
  //isValidAddress();
  var SelectedCountryId = eKYCconfigObject.DefaultCountry.CountryCode;
  if(obj.id == "tbxSubDistrict"){
    if(frmMBeKYCContactInfo.lblCountryCode.text === SelectedCountryId){
      autosearchsubdistrictContactInfo(frmMBeKYCContactInfo.tbxSubDistrict, frmMBeKYCContactInfo.flxSearchAddress, 
                                       frmMBeKYCContactInfo.segSubDistrictSearch, frmMBeKYCContactInfo.lblAddressNotFound);
    }
  }
  else if(obj.id == "tbxSubDistrictCurrentAddress"){
    if(frmMBeKYCContactInfo.lblCountryCodeCurrentAddress.text === SelectedCountryId){
      autosearchsubdistrictContactInfo(frmMBeKYCContactInfo.tbxSubDistrictCurrentAddress, frmMBeKYCContactInfo.flxSearchAddressCurrentAddress, 
                                       frmMBeKYCContactInfo.segSubDistrictSearchCurrentAddress, frmMBeKYCContactInfo.lblAddressNotFoundCurrentAddress);
    }
  }  
  else if(obj.id == "tbxSubDistrictOfficeAddress"){
    if(frmMBeKYCContactInfo.lblCountryCodeOfficeAddress.text === SelectedCountryId){
      autosearchsubdistrictContactInfo(frmMBeKYCContactInfo.tbxSubDistrictOfficeAddress, frmMBeKYCContactInfo.flxSearchAddressOfficeAddress, 
                                       frmMBeKYCContactInfo.segSubDistrictSearchOfficeAddress, frmMBeKYCContactInfo.lblAddressNotFoundOfficeAddress);
    }
  }
}
function autosearchsubdistrictContactInfo(tbxSubDistrict, flxSearchAddress, segSubDistrictSearch , lblAddressNotFound){
  var searchSubDistrictList = [];
  var searchSuggestedSubDistrictList = [];
  var searchText = tbxSubDistrict.text;
  var searchValue = "";

  if (isNotBlank(searchText) && searchText.length >= 1) {
    searchText = searchText.toLowerCase();
    var countMatchedValue = 0;
    flxSearchAddress.isVisible = true;
   for(var countVal=0; countVal<gbleKYCSubdistrictData.length; countVal++){
      keyToSearch = gbleKYCSubdistrictData[countVal].subDistProvince;
      keyToSearch = keyToSearch.toLowerCase();
      if(keyToSearch.indexOf(searchText) >=0 ){
        searchSuggestedSubDistrictList[countMatchedValue] = gbleKYCSubdistrictData[countVal];
        countMatchedValue = countMatchedValue+1;
      }                
    }        
  }else{
    flxSearchAddress.isVisible = false;
  }
  var dataset={};
  for(var count = 0; count < searchSuggestedSubDistrictList.length; count++){
    var filtervalue = searchSuggestedSubDistrictList[count].subDistProvince;
    dataset={
      "lblTotalAddress": filtervalue,
    };
    searchSubDistrictList.push(dataset);
  }       
  segSubDistrictSearch.setData(searchSubDistrictList);
  if(searchSuggestedSubDistrictList.length === 0){
    segSubDistrictSearch.setVisibility(false);
    lblAddressNotFound.setVisibility(true);
  }else{
    segSubDistrictSearch.setVisibility(true);
    lblAddressNotFound.setVisibility(false);
  }
}
//SubDistrict Dropdown Related Functions-------------------- End

//Common Functions-------------------- Start
function dynamicSortContactInfo(property) {
  var sortOrder = 1;
  if(property[0] === "-") {
    sortOrder = -1;
    property = property.substr(1);
  }
  return function (a,b) {
    var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0 ;
    return result * sortOrder ;
  }
} 
function onClickTxtbxPhone(obj){
  onEditMobileNumbereKYC(obj);
}
function onEditMobileNumbereKYC(obj) {
  var txt = obj.text;
  if (txt === null) 
    return false;

  var noChars = txt.length;
  var temp = "";
  var i, txtLen = noChars;
  var currLen = noChars;
  var hyphenText = "";

  if (gblPrevLen < currLen) {
    for (i = 0; i < noChars; ++i) {
      if (txt[i] != '-') {
        temp = temp + txt[i];
      }
    }       
    if(temp.length <= 9){
      for (i = 0; i < temp.length; i++) {
        hyphenText += temp[i];			
        if (i == 1 || i == 4) {
          hyphenText += '-';
        }                          
      }
    }
    else{
      for (i = 0; i < temp.length; i++) {
        hyphenText += temp[i];			
        if (i == 2 || i == 5) {
          hyphenText += '-';
        }                          
      }
    }
    obj.text = hyphenText;					
  }
  gblPrevLen = currLen;
  if(gblPrevLen>=11){
    obj.parent.skin = lFboxLoan;
  }
  else{
    obj.parent.skin = lFboxLoanIncorrect;
  }
}


function linkContactInfoEvents(){
  //==============Register Address=================Start
  frmMBeKYCContactInfo.FlexAddressNo.onClick = onFlexClickActivateTextboxContactInfo;
  frmMBeKYCContactInfo.tbxAddressNo.onDone = onTextChangeContactInfo;
  frmMBeKYCContactInfo.tbxAddressNo.onEndEditing = onTextChangeContactInfo;
  frmMBeKYCContactInfo.tbxAddressNo.onTextChange = isValidAddress;

  frmMBeKYCContactInfo.FlexMoo.onClick = onFlexClickActivateTextboxContactInfo;
  frmMBeKYCContactInfo.tbxMoo.onDone = onTextChangeContactInfo;
  frmMBeKYCContactInfo.tbxMoo.onEndEditing = onTextChangeContactInfo;
  frmMBeKYCContactInfo.tbxMoo.onTextChange = isValidAddress;

  frmMBeKYCContactInfo.FlexBuildingNo.onClick = onFlexClickActivateTextboxContactInfo;
  frmMBeKYCContactInfo.tbxBuildingNo.onDone = onTextChangeContactInfo;
  frmMBeKYCContactInfo.tbxBuildingNo.onEndEditing = onTextChangeContactInfo;
  frmMBeKYCContactInfo.tbxBuildingNo.onTextChange = isValidAddress;

  frmMBeKYCContactInfo.FlexSoi.onClick = onFlexClickActivateTextboxContactInfo;
  frmMBeKYCContactInfo.tbxSoi.onDone = onTextChangeContactInfo;
  frmMBeKYCContactInfo.tbxSoi.onEndEditing = onTextChangeContactInfo;
  frmMBeKYCContactInfo.tbxSoi.onTextChange = isValidAddress;

  frmMBeKYCContactInfo.FlexRoad.onClick = onFlexClickActivateTextboxContactInfo;
  frmMBeKYCContactInfo.tbxRoad.onDone = onTextChangeContactInfo;
  frmMBeKYCContactInfo.tbxRoad.onEndEditing = onTextChangeContactInfo;
  frmMBeKYCContactInfo.tbxRoad.onTextChange = isValidAddress;

  frmMBeKYCContactInfo.flxSubDistrict.onClick = onFlexClickActivateTextboxContactInfo;
  frmMBeKYCContactInfo.tbxSubDistrict.onDone = onTextChangeContactInfo;
  frmMBeKYCContactInfo.tbxSubDistrict.onTextChange = searchsubdistrictContactInfo;
  frmMBeKYCContactInfo.tbxSubDistrict.onEndEditing = onTextChangeContactInfo;
  frmMBeKYCContactInfo.segSubDistrictSearch.onRowClick = selectFullAddressContactInfo;
  //frmMBeKYCContactInfo.tbxSubDistrict.onTextChange = isValidAddress;

  frmMBeKYCContactInfo.flxDistrict.onClick = onFlexClickActivateTextboxContactInfo;
  frmMBeKYCContactInfo.tbxDistrict.onDone = onTextChangeContactInfo;
  frmMBeKYCContactInfo.tbxDistrict.onEndEditing = onTextChangeContactInfo;
  frmMBeKYCContactInfo.tbxDistrict.onTextChange = isValidAddress;

  frmMBeKYCContactInfo.flxProvince.onClick = onFlexClickActivateTextboxContactInfo;
  frmMBeKYCContactInfo.tbxProvince.onDone = onTextChangeContactInfo;
  frmMBeKYCContactInfo.tbxProvince.onEndEditing = onTextChangeContactInfo;
  frmMBeKYCContactInfo.tbxProvince.onTextChange = isValidAddress;

  frmMBeKYCContactInfo.flxZipCode.onClick = onFlexClickActivateTextboxContactInfo;
  frmMBeKYCContactInfo.tbxZipCode.onDone = onTextChangeContactInfo;
  frmMBeKYCContactInfo.tbxZipCode.onEndEditing = onTextChangeContactInfo;
  frmMBeKYCContactInfo.tbxZipCode.onTextChange = isValidAddress;

  frmMBeKYCContactInfo.flxCountry.onClick = onClickCountryDropDown;
  //==============Register Address=================End  

  //==============Current Address=================Start
  frmMBeKYCContactInfo.flxHomePhoneSub.onClick = onFlexClickActivateTextboxContactInfo;
  frmMBeKYCContactInfo.txtHomePhone.onDone = onTextChangeContactInfo;
  frmMBeKYCContactInfo.txtHomePhone.onTextChange = onClickTxtbxPhone;
  frmMBeKYCContactInfo.txtHomePhone.onEndEditing = onTextChangeContactInfo;

  frmMBeKYCContactInfo.flxExtNoContactAdd.onClick = onFlexClickActivateTextboxContactInfo;
  frmMBeKYCContactInfo.txtExtNoContactAdd.onDone = onTextChangeContactInfo;
  frmMBeKYCContactInfo.txtExtNoContactAdd.onEndEditing = onTextChangeContactInfo;

  frmMBeKYCContactInfo.FlexAddressNoCurrentAddress.onClick = onFlexClickActivateTextboxContactInfo;
  frmMBeKYCContactInfo.tbxAddressNoCurrentAddress.onDone = onTextChangeContactInfo;
  frmMBeKYCContactInfo.tbxAddressNoCurrentAddress.onEndEditing = onTextChangeContactInfo;
  frmMBeKYCContactInfo.tbxAddressNoCurrentAddress.onTextChange = isValidAddress;

  frmMBeKYCContactInfo.FlexMooCurrentAddress.onClick = onFlexClickActivateTextboxContactInfo;
  frmMBeKYCContactInfo.tbxMooCurrentAddress.onDone = onTextChangeContactInfo;
  frmMBeKYCContactInfo.tbxMooCurrentAddress.onEndEditing = onTextChangeContactInfo;
  frmMBeKYCContactInfo.tbxMooCurrentAddress.onTextChange = isValidAddress;

  frmMBeKYCContactInfo.FlexBuildingNoCurrentAddress.onClick = onFlexClickActivateTextboxContactInfo;
  frmMBeKYCContactInfo.tbxBuildingNoCurrentAddress.onDone = onTextChangeContactInfo;
  frmMBeKYCContactInfo.tbxBuildingNoCurrentAddress.onEndEditing = onTextChangeContactInfo;
  frmMBeKYCContactInfo.tbxBuildingNoCurrentAddress.onTextChange = isValidAddress;

  frmMBeKYCContactInfo.FlexSoiCurrentAddress.onClick = onFlexClickActivateTextboxContactInfo;
  frmMBeKYCContactInfo.txtSoiCurrentAddress.onDone = onTextChangeContactInfo;
  frmMBeKYCContactInfo.txtSoiCurrentAddress.onEndEditing = onTextChangeContactInfo;
  frmMBeKYCContactInfo.txtSoiCurrentAddress.onTextChange = isValidAddress;

  frmMBeKYCContactInfo.FlexRoadCurrentAddress.onClick = onFlexClickActivateTextboxContactInfo;
  frmMBeKYCContactInfo.txtRoadCurrentAddress.onDone = onTextChangeContactInfo;
  frmMBeKYCContactInfo.txtRoadCurrentAddress.onEndEditing = onTextChangeContactInfo;
  frmMBeKYCContactInfo.txtRoadCurrentAddress.onTextChange = isValidAddress;

  frmMBeKYCContactInfo.flxSubDistrictCurrentAddress.onClick = onFlexClickActivateTextboxContactInfo;
  frmMBeKYCContactInfo.tbxSubDistrictCurrentAddress.onDone = onTextChangeContactInfo;
  frmMBeKYCContactInfo.tbxSubDistrictCurrentAddress.onTextChange = searchsubdistrictContactInfo;
  frmMBeKYCContactInfo.tbxSubDistrictCurrentAddress.onEndEditing = onTextChangeContactInfo;
  frmMBeKYCContactInfo.segSubDistrictSearchCurrentAddress.onRowClick = selectFullAddressContactInfo;
  //frmMBeKYCContactInfo.tbxSubDistrictCurrentAddress.onTextChange = isValidAddress;

  frmMBeKYCContactInfo.flxDistrictCurrentAddress.onClick = onFlexClickActivateTextboxContactInfo;
  frmMBeKYCContactInfo.tbxDistrictCurrentAddress.onDone = onTextChangeContactInfo;
  frmMBeKYCContactInfo.tbxDistrictCurrentAddress.onEndEditing = onTextChangeContactInfo;
  frmMBeKYCContactInfo.tbxDistrictCurrentAddress.onTextChange = isValidAddress;

  frmMBeKYCContactInfo.flxProvinceCurrentAddress.onClick = onFlexClickActivateTextboxContactInfo;
  frmMBeKYCContactInfo.tbxProvinceCurrentAddress.onDone = onTextChangeContactInfo;
  frmMBeKYCContactInfo.tbxProvinceCurrentAddress.onEndEditing = onTextChangeContactInfo;
  frmMBeKYCContactInfo.tbxProvinceCurrentAddress.onTextChange = isValidAddress;

  frmMBeKYCContactInfo.flxZipCodeCurrentAddress.onClick = onFlexClickActivateTextboxContactInfo;
  frmMBeKYCContactInfo.tbxZipCodeCurrentAddress.onDone = onTextChangeContactInfo;
  frmMBeKYCContactInfo.tbxZipCodeCurrentAddress.onEndEditing = onTextChangeContactInfo;	
  frmMBeKYCContactInfo.tbxZipCodeCurrentAddress.onTextChange = isValidAddress;

  frmMBeKYCContactInfo.flxCountryCurrentAddress.onClick = onClickCountryDropDown;

  frmMBeKYCContactInfo.flxHomePhoneSub2.onClick = onFlexClickActivateTextboxContactInfo;
  frmMBeKYCContactInfo.txtHomePhone2.onDone = onTextChangeContactInfo;
  frmMBeKYCContactInfo.txtHomePhone2.onTextChange = onClickTxtbxPhone;
  frmMBeKYCContactInfo.txtHomePhone2.onEndEditing = onTextChangeContactInfo;

  frmMBeKYCContactInfo.flxExtNo2.onClick = onFlexClickActivateTextboxContactInfo;
  frmMBeKYCContactInfo.txExtNo2.onDone = onTextChangeContactInfo;
  frmMBeKYCContactInfo.txExtNo2.onEndEditing = onTextChangeContactInfo;

  //==============Current Address=================End  

  //==============Office Address=================Start
  frmMBeKYCContactInfo.flxCompanyName.onClick = onFlexClickActivateTextboxContactInfo;
  frmMBeKYCContactInfo.txtCompanyName.onDone = onTextChangeContactInfo;
  frmMBeKYCContactInfo.txtCompanyName.onEndEditing = onTextChangeContactInfo;
  frmMBeKYCContactInfo.txtCompanyName.onTextChange = isValidAddress;

  frmMBeKYCContactInfo.flxOfficePhoneSub.onClick = onFlexClickActivateTextboxContactInfo;
  frmMBeKYCContactInfo.txtOfficePhone.onDone = onTextChangeContactInfo;
  frmMBeKYCContactInfo.txtOfficePhone.onTextChange = onClickTxtbxPhone;
  frmMBeKYCContactInfo.txtOfficePhone.onEndEditing = onTextChangeContactInfo;

  frmMBeKYCContactInfo.flxExtNoOfficeAdd.onClick = onFlexClickActivateTextboxContactInfo;
  frmMBeKYCContactInfo.txtExtNoOfficeAdd.onDone = onTextChangeContactInfo;
  frmMBeKYCContactInfo.txtExtNoOfficeAdd.onEndEditing = onTextChangeContactInfo;

  frmMBeKYCContactInfo.FlexAddressNoOfficeAddress.onClick = onFlexClickActivateTextboxContactInfo;
  frmMBeKYCContactInfo.tbxAddressNoOfficeAddress.onDone = onTextChangeContactInfo;
  frmMBeKYCContactInfo.tbxAddressNoOfficeAddress.onEndEditing = onTextChangeContactInfo;
  frmMBeKYCContactInfo.tbxAddressNoOfficeAddress.onTextChange = isValidAddress;

  frmMBeKYCContactInfo.FlexMooOfficeAddress.onClick = onFlexClickActivateTextboxContactInfo;
  frmMBeKYCContactInfo.tbxMooOfficeAddress.onDone = onTextChangeContactInfo;
  frmMBeKYCContactInfo.tbxMooOfficeAddress.onEndEditing = onTextChangeContactInfo;
  frmMBeKYCContactInfo.tbxMooOfficeAddress.onTextChange = isValidAddress;

  frmMBeKYCContactInfo.FlexBuildingNoOfficeAddress.onClick = onFlexClickActivateTextboxContactInfo;
  frmMBeKYCContactInfo.tbxBuildingNoOfficeAddress.onDone = onTextChangeContactInfo;
  frmMBeKYCContactInfo.tbxBuildingNoOfficeAddress.onEndEditing = onTextChangeContactInfo;
  frmMBeKYCContactInfo.tbxBuildingNoOfficeAddress.onTextChange = isValidAddress;

  frmMBeKYCContactInfo.FlexSoiOfficeAddress.onClick = onFlexClickActivateTextboxContactInfo;
  frmMBeKYCContactInfo.txtSoiOfficeAddress.onDone = onTextChangeContactInfo;
  frmMBeKYCContactInfo.txtSoiOfficeAddress.onEndEditing = onTextChangeContactInfo;
  frmMBeKYCContactInfo.txtSoiOfficeAddress.onTextChange = isValidAddress;

  frmMBeKYCContactInfo.FlexRoadOfficeAddress.onClick = onFlexClickActivateTextboxContactInfo;
  frmMBeKYCContactInfo.txtRoadOfficeAddress.onDone = onTextChangeContactInfo;
  frmMBeKYCContactInfo.txtRoadOfficeAddress.onEndEditing = onTextChangeContactInfo;
  frmMBeKYCContactInfo.txtRoadOfficeAddress.onTextChange = isValidAddress;

  frmMBeKYCContactInfo.flxSubDistrictOfficeAddress.onClick = onFlexClickActivateTextboxContactInfo;
  frmMBeKYCContactInfo.tbxSubDistrictOfficeAddress.onDone = onTextChangeContactInfo;
  frmMBeKYCContactInfo.tbxSubDistrictOfficeAddress.onTextChange = searchsubdistrictContactInfo;
  frmMBeKYCContactInfo.tbxSubDistrictOfficeAddress.onEndEditing = onTextChangeContactInfo;
  frmMBeKYCContactInfo.segSubDistrictSearchOfficeAddress.onRowClick = selectFullAddressContactInfo;
  //frmMBeKYCContactInfo.tbxSubDistrictOfficeAddress.onTextChange = isValidAddress;

  frmMBeKYCContactInfo.flxDistrictOfficeAddress.onClick = onFlexClickActivateTextboxContactInfo;
  frmMBeKYCContactInfo.tbxDistrictOfficeAddress.onDone = onTextChangeContactInfo;
  frmMBeKYCContactInfo.tbxDistrictOfficeAddress.onEndEditing = onTextChangeContactInfo;
  frmMBeKYCContactInfo.tbxDistrictOfficeAddress.onTextChange = isValidAddress;

  frmMBeKYCContactInfo.flxProvinceOfficeAddress.onClick = onFlexClickActivateTextboxContactInfo;
  frmMBeKYCContactInfo.tbxProvinceOfficeAddress.onDone = onTextChangeContactInfo;
  frmMBeKYCContactInfo.tbxProvinceOfficeAddress.onEndEditing = onTextChangeContactInfo;
  frmMBeKYCContactInfo.tbxProvinceOfficeAddress.onTextChange = isValidAddress;

  frmMBeKYCContactInfo.flxZipCodeOfficeAddress.onClick = onFlexClickActivateTextboxContactInfo;
  frmMBeKYCContactInfo.tbxZipCodeOfficeAddress.onDone = onTextChangeContactInfo;
  frmMBeKYCContactInfo.tbxZipCodeOfficeAddress.onEndEditing = onTextChangeContactInfo;
  frmMBeKYCContactInfo.tbxZipCodeOfficeAddress.onTextChange = isValidAddress;

  frmMBeKYCContactInfo.flxCountryOfficeAddress.onClick = onClickCountryDropDown;

  frmMBeKYCContactInfo.flxCompanyName2.onClick = onFlexClickActivateTextboxContactInfo;
  frmMBeKYCContactInfo.txtCompanyName2.onDone = onTextChangeContactInfo;
  frmMBeKYCContactInfo.txtCompanyName2.onEndEditing = onTextChangeContactInfo;
  frmMBeKYCContactInfo.txtCompanyName2.onTextChange = isValidAddress;

  frmMBeKYCContactInfo.flxOfficePhoneSub2.onClick = onFlexClickActivateTextboxContactInfo;
  frmMBeKYCContactInfo.txtOfficePhone2.onDone = onTextChangeContactInfo;
  frmMBeKYCContactInfo.txtOfficePhone2.onTextChange = onClickTxtbxPhone;
  frmMBeKYCContactInfo.txtOfficePhone2.onEndEditing = onTextChangeContactInfo;

  frmMBeKYCContactInfo.flxExtNoOfficeAdd2.onClick = onFlexClickActivateTextboxContactInfo;
  frmMBeKYCContactInfo.txExtNoOfficeAdd2.onDone = onTextChangeContactInfo;
  frmMBeKYCContactInfo.txExtNoOfficeAdd2.onEndEditing = onTextChangeContactInfo;
  //==============Office Address=================End  

  // Link events for Current address Check box functionality---- start

  frmMBeKYCContactInfo.flxChkbxCurrentAddress.onClick = onClickFlexAddressChkbx;
  frmMBeKYCContactInfo.flxChkbxOfficeAddress.onClick = onClickFlexAddressChkbx;
  frmMBeKYCContactInfo.tbxSearch.onTextChange = searchCountryByNameContactinfo;
  // Link events for Current address Check box functionality---- End

  frmMBeKYCContactInfo.btnNext.onClick = onClickbtnNextContactInfo;

}
function onClickbtnNextContactInfo(){      	
   //getPersonalInfoDatafromService();
   if(checkMandatoryFieldsValidation()){
     showLoadingScreen();
     saveCustomerInfoDataInDB();
   }
}
function onClickFlexAddressChkbx(obj){
  try{
    if(obj.id == "flxChkbxCurrentAddress"){      
      if(frmMBeKYCContactInfo.flxChkbxCurrentAddress.skin == "skinFilledlueUnchkedChkbx"){
        frmMBeKYCContactInfo.flxChkbxCurrentAddress.skin = "skinFilledluechkedChkbx";
        frmMBeKYCContactInfo.lblCurrAddChangeNote.isVisible = true;
        frmMBeKYCContactInfo.flxHomePhoneMain.isVisible = true;
        frmMBeKYCContactInfo.flxScrollAddressCurrentAddress.isVisible = false;
      }
      else{
        frmMBeKYCContactInfo.flxChkbxCurrentAddress.skin = "skinFilledlueUnchkedChkbx";
        frmMBeKYCContactInfo.lblCurrAddChangeNote.isVisible = false;
        frmMBeKYCContactInfo.flxHomePhoneMain.isVisible = false;
        frmMBeKYCContactInfo.flxScrollAddressCurrentAddress.isVisible = true;       
      }
    }
    else if(obj.id == "flxChkbxOfficeAddress"){    
      if(frmMBeKYCContactInfo.flxChkbxOfficeAddress.skin == "skinFilledlueUnchkedChkbx"){
        frmMBeKYCContactInfo.flxChkbxOfficeAddress.skin = "skinFilledluechkedChkbx";
        frmMBeKYCContactInfo.lblOfficeAddChangeNote.isVisible = true;
        frmMBeKYCContactInfo.flxCompanyName.isVisible = true;
        frmMBeKYCContactInfo.flxOfficePhoneMain.isVisible = true;
        frmMBeKYCContactInfo.flxScrollAddressOfficeAddress.isVisible = false;
      }
      else{
        frmMBeKYCContactInfo.flxChkbxOfficeAddress.skin = "skinFilledlueUnchkedChkbx";
        frmMBeKYCContactInfo.lblOfficeAddChangeNote.isVisible = false;
        frmMBeKYCContactInfo.flxCompanyName.isVisible = false;
        frmMBeKYCContactInfo.flxOfficePhoneMain.isVisible = false;
        frmMBeKYCContactInfo.flxScrollAddressOfficeAddress.isVisible = true;             
      }
    }
  }
  catch(e)
  {
    kony.print("mki onClickFlexAddressChkbx e.message="+ e.message);
  }
}
function onFlexClickActivateTextboxContactInfo(obj){
  switch(obj.id){
    case "FlexAddressNo":
      onFlexClickActivateTextboxforEditing(frmMBeKYCContactInfo.lblMainAddressNo,frmMBeKYCContactInfo.lblAddressNo,frmMBeKYCContactInfo.tbxAddressNo);  
      break;
    case "FlexMoo":
      onFlexClickActivateTextboxforEditing(frmMBeKYCContactInfo.lblMainMoo,frmMBeKYCContactInfo.lblMoo,frmMBeKYCContactInfo.tbxMoo);  
      break;
    case "FlexBuildingNo":
      onFlexClickActivateTextboxforEditing(frmMBeKYCContactInfo.lblMainBuildingNo,frmMBeKYCContactInfo.lblBuildingNo,frmMBeKYCContactInfo.tbxBuildingNo);    
      break;
    case "FlexSoi":
      onFlexClickActivateTextboxforEditing(frmMBeKYCContactInfo.lblMainSoi,frmMBeKYCContactInfo.lblSoi,frmMBeKYCContactInfo.tbxSoi);  
      break;
    case "FlexRoad":
      onFlexClickActivateTextboxforEditing(frmMBeKYCContactInfo.LblMainRoadNo,frmMBeKYCContactInfo.lblRoad,frmMBeKYCContactInfo.tbxRoad);  
      break;
    case "flxSubDistrict":
      onFlexClickActivateTextboxforEditing(frmMBeKYCContactInfo.lblMainSubDistrict,frmMBeKYCContactInfo.lblSubDistrict,frmMBeKYCContactInfo.tbxSubDistrict);  
      break;
    case "flxDistrict":
      onFlexClickActivateTextboxforEditing(frmMBeKYCContactInfo.lblMainDistrict,frmMBeKYCContactInfo.lblDistrict,frmMBeKYCContactInfo.tbxDistrict);  
      break;
    case "flxProvince":
      onFlexClickActivateTextboxforEditing(frmMBeKYCContactInfo.lblMainProvince,frmMBeKYCContactInfo.lblProvince,frmMBeKYCContactInfo.tbxProvince);  
      break;
    case "flxZipCode":
      onFlexClickActivateTextboxforEditing(frmMBeKYCContactInfo.lblMainZipCode,frmMBeKYCContactInfo.lblZipCode,frmMBeKYCContactInfo.tbxZipCode);  
      break;


    case "flxHomePhoneSub":
      onFlexClickActivateTextboxforEditing(frmMBeKYCContactInfo.lblHomePhoneMain,frmMBeKYCContactInfo.lblhomePhone,frmMBeKYCContactInfo.txtHomePhone);  
      break;
    case "flxExtNoContactAdd":
      onFlexClickActivateTextboxforEditing(frmMBeKYCContactInfo.lblExtNoContactAddMain,frmMBeKYCContactInfo.lblExtNoContactAdd,frmMBeKYCContactInfo.txtExtNoContactAdd);  
      break;
    case "FlexAddressNoCurrentAddress":
      onFlexClickActivateTextboxforEditing(frmMBeKYCContactInfo.lblMainAddressNoCurrentAddress,frmMBeKYCContactInfo.lblAddressNoCurrentAddress,frmMBeKYCContactInfo.tbxAddressNoCurrentAddress);  
      break;
    case "FlexMooCurrentAddress":
      onFlexClickActivateTextboxforEditing(frmMBeKYCContactInfo.lblMainMooCurrentAddress,frmMBeKYCContactInfo.lblMooCurrentAddress,frmMBeKYCContactInfo.tbxMooCurrentAddress);  
      break;
    case "FlexBuildingNoCurrentAddress":
      onFlexClickActivateTextboxforEditing(frmMBeKYCContactInfo.lblMainBuildingNoCurrentAddress,frmMBeKYCContactInfo.lblBuildingNoCurrentAddress,frmMBeKYCContactInfo.tbxBuildingNoCurrentAddress);    
      break;
    case "FlexSoiCurrentAddress":
      onFlexClickActivateTextboxforEditing(frmMBeKYCContactInfo.lblMainSoiCurrentAddress,frmMBeKYCContactInfo.lblSoiCurrentAddress,frmMBeKYCContactInfo.txtSoiCurrentAddress);  
      break;
    case "FlexRoadCurrentAddress":
      onFlexClickActivateTextboxforEditing(frmMBeKYCContactInfo.lblMainRoadCurrentAddress,frmMBeKYCContactInfo.lblRoadCurrentAddress,frmMBeKYCContactInfo.txtRoadCurrentAddress);  
      break;
    case "flxSubDistrictCurrentAddress":
      onFlexClickActivateTextboxforEditing(frmMBeKYCContactInfo.lblMainSubDistrictCurrentAddress,frmMBeKYCContactInfo.lblSubDistrictCurrentAddress,frmMBeKYCContactInfo.tbxSubDistrictCurrentAddress);  
      break;
    case "flxDistrictCurrentAddress":
      onFlexClickActivateTextboxforEditing(frmMBeKYCContactInfo.lblMainDistrictCurrentAddress,frmMBeKYCContactInfo.lblDistrictCurrentAddress,frmMBeKYCContactInfo.tbxDistrictCurrentAddress);  
      break;
    case "flxProvinceCurrentAddress":
      onFlexClickActivateTextboxforEditing(frmMBeKYCContactInfo.lblMainProvinceCurrentAddress,frmMBeKYCContactInfo.lblProvinceCurrentAddress,frmMBeKYCContactInfo.tbxProvinceCurrentAddress);  
      break;
    case "flxZipCodeCurrentAddress":
      onFlexClickActivateTextboxforEditing(frmMBeKYCContactInfo.lblMainZipCodeCurrentAddress,frmMBeKYCContactInfo.lblZipCodeCurrentAddress,frmMBeKYCContactInfo.tbxZipCodeCurrentAddress);  
      break;
    case "flxHomePhoneSub2":
      onFlexClickActivateTextboxforEditing(frmMBeKYCContactInfo.lblHomePhoneMain2,frmMBeKYCContactInfo.lblhomePhone2,frmMBeKYCContactInfo.txtHomePhone2);  
      break;
    case "flxExtNo2":
      onFlexClickActivateTextboxforEditing(frmMBeKYCContactInfo.lblExtNo2Main,frmMBeKYCContactInfo.lblExtNo2,frmMBeKYCContactInfo.txExtNo2);  
      break;


    case "flxCompanyName":
      onFlexClickActivateTextboxforEditing(frmMBeKYCContactInfo.lblCompanyNameMain,frmMBeKYCContactInfo.lblCompanyName,frmMBeKYCContactInfo.txtCompanyName);  
      break;
    case "flxOfficePhoneSub":
      onFlexClickActivateTextboxforEditing(frmMBeKYCContactInfo.lblOfficePhoneMain,frmMBeKYCContactInfo.lblOfficePhone,frmMBeKYCContactInfo.txtOfficePhone);  
      break;
    case "flxExtNoOfficeAdd":
      onFlexClickActivateTextboxforEditing(frmMBeKYCContactInfo.lblExtNoOfficeAddMain,frmMBeKYCContactInfo.lblExtNoOfficeAdd,frmMBeKYCContactInfo.txtExtNoOfficeAdd);    
      break;
    case "FlexAddressNoOfficeAddress":
      onFlexClickActivateTextboxforEditing(frmMBeKYCContactInfo.lblMainAddressNoOfficeAddress,frmMBeKYCContactInfo.lblAddressNoOfficeAddress,frmMBeKYCContactInfo.tbxAddressNoOfficeAddress);  
      break;
    case "FlexMooOfficeAddress":
      onFlexClickActivateTextboxforEditing(frmMBeKYCContactInfo.lblMainMooOfficeAddress,frmMBeKYCContactInfo.lblMooOfficeAddress,frmMBeKYCContactInfo.tbxMooOfficeAddress);  
      break;
    case "FlexBuildingNoOfficeAddress":
      onFlexClickActivateTextboxforEditing(frmMBeKYCContactInfo.lblMainBuildingNoOfficeAddress,frmMBeKYCContactInfo.lblBuildingNoOfficeAddress,frmMBeKYCContactInfo.tbxBuildingNoOfficeAddress);    
      break;
    case "FlexSoiOfficeAddress":
      onFlexClickActivateTextboxforEditing(frmMBeKYCContactInfo.lblMainSoiOfficeAddress,frmMBeKYCContactInfo.lblSoiOfficeAddress,frmMBeKYCContactInfo.txtSoiOfficeAddress);  
      break;
    case "FlexRoadOfficeAddress":
      onFlexClickActivateTextboxforEditing(frmMBeKYCContactInfo.lblMainRoadOfficeAddress,frmMBeKYCContactInfo.lblRoadOfficeAddress,frmMBeKYCContactInfo.txtRoadOfficeAddress);  
      break;
    case "flxSubDistrictOfficeAddress":
      onFlexClickActivateTextboxforEditing(frmMBeKYCContactInfo.lblMainSubDistrictOfficeAddress,frmMBeKYCContactInfo.lblSubDistrictOfficeAddress,frmMBeKYCContactInfo.tbxSubDistrictOfficeAddress);  
      break;
    case "flxDistrictOfficeAddress":
      onFlexClickActivateTextboxforEditing(frmMBeKYCContactInfo.lblMainDistrictOfficeAddress,frmMBeKYCContactInfo.lblDistrictOfficeAddress,frmMBeKYCContactInfo.tbxDistrictOfficeAddress);  
      break;
    case "flxProvinceOfficeAddress":
      onFlexClickActivateTextboxforEditing(frmMBeKYCContactInfo.lblMainProvinceOfficeAddress,frmMBeKYCContactInfo.lblProvinceOfficeAddress,frmMBeKYCContactInfo.tbxProvinceOfficeAddress);  
      break;
    case "flxZipCodeOfficeAddress":
      onFlexClickActivateTextboxforEditing(frmMBeKYCContactInfo.lblMainZipCodeOfficeAddress,frmMBeKYCContactInfo.lblZipCodeOfficeAddress,frmMBeKYCContactInfo.tbxZipCodeOfficeAddress);  
      break;
    case "flxCompanyName2":
      onFlexClickActivateTextboxforEditing(frmMBeKYCContactInfo.lblCompanyNameMain2,frmMBeKYCContactInfo.lblCompanyName2,frmMBeKYCContactInfo.txtCompanyName2);  
      break;
    case "flxOfficePhoneSub2":
      onFlexClickActivateTextboxforEditing(frmMBeKYCContactInfo.lblOfficePhoneMain2,frmMBeKYCContactInfo.lblOfficePhone2,frmMBeKYCContactInfo.txtOfficePhone2);  
      break;
    case "flxExtNoOfficeAdd2":
      onFlexClickActivateTextboxforEditing(frmMBeKYCContactInfo.lblExtNoMainOfficeAdd2,frmMBeKYCContactInfo.lblExtNoOfficeAdd2,frmMBeKYCContactInfo.txExtNoOfficeAdd2);    
      break;
  }
}
function onTextChangeContactInfo(obj){
  switch(obj.id){
    case "tbxAddressNo":
      OnTextEditingDoneEmptyCheck(frmMBeKYCContactInfo.lblMainAddressNo,frmMBeKYCContactInfo.lblAddressNo,frmMBeKYCContactInfo.tbxAddressNo);  
      break;
    case "tbxMoo":
      OnTextEditingDoneEmptyCheck(frmMBeKYCContactInfo.lblMainMoo,frmMBeKYCContactInfo.lblMoo,frmMBeKYCContactInfo.tbxMoo);  
      break;
    case "tbxBuildingNo":
      OnTextEditingDoneEmptyCheck(frmMBeKYCContactInfo.lblMainBuildingNo,frmMBeKYCContactInfo.lblBuildingNo,frmMBeKYCContactInfo.tbxBuildingNo);    
      break;
    case "tbxSoi":
      OnTextEditingDoneEmptyCheck(frmMBeKYCContactInfo.lblMainSoi,frmMBeKYCContactInfo.lblSoi,frmMBeKYCContactInfo.tbxSoi);  
      break;
    case "tbxRoad":
      OnTextEditingDoneEmptyCheck(frmMBeKYCContactInfo.LblMainRoadNo,frmMBeKYCContactInfo.lblRoad,frmMBeKYCContactInfo.tbxRoad);  
      break;
    case "tbxSubDistrict":
      OnTextEditingDoneEmptyCheck(frmMBeKYCContactInfo.lblMainSubDistrict,frmMBeKYCContactInfo.lblSubDistrict,frmMBeKYCContactInfo.tbxSubDistrict);  
      break;
    case "tbxDistrict":
      OnTextEditingDoneEmptyCheck(frmMBeKYCContactInfo.lblMainDistrict,frmMBeKYCContactInfo.lblDistrict,frmMBeKYCContactInfo.tbxDistrict);  
      break;
    case "tbxProvince":
      OnTextEditingDoneEmptyCheck(frmMBeKYCContactInfo.lblMainProvince,frmMBeKYCContactInfo.lblProvince,frmMBeKYCContactInfo.tbxProvince);  
      break;
    case "tbxZipCode":
      OnTextEditingDoneEmptyCheck(frmMBeKYCContactInfo.lblMainZipCode,frmMBeKYCContactInfo.lblZipCode,frmMBeKYCContactInfo.tbxZipCode);  
      break;


    case "txtHomePhone":
      OnTextEditingDoneEmptyCheck(frmMBeKYCContactInfo.lblHomePhoneMain,frmMBeKYCContactInfo.lblhomePhone,frmMBeKYCContactInfo.txtHomePhone);  
      break;
    case "txtExtNoContactAdd":
      OnTextEditingDoneEmptyCheck(frmMBeKYCContactInfo.lblExtNoContactAddMain,frmMBeKYCContactInfo.lblExtNoContactAdd,frmMBeKYCContactInfo.txtExtNoContactAdd);  
      break;
    case "tbxAddressNoCurrentAddress":
      OnTextEditingDoneEmptyCheck(frmMBeKYCContactInfo.lblMainAddressNoCurrentAddress,frmMBeKYCContactInfo.lblMainAddressNoCurrentAddress,frmMBeKYCContactInfo.tbxAddressNoCurrentAddress);  
      break;
    case "tbxMooCurrentAddress":
      OnTextEditingDoneEmptyCheck(frmMBeKYCContactInfo.lblMainMooCurrentAddress,frmMBeKYCContactInfo.lblMooCurrentAddress,frmMBeKYCContactInfo.tbxMooCurrentAddress);  
      break;
    case "tbxBuildingNoCurrentAddress":
      OnTextEditingDoneEmptyCheck(frmMBeKYCContactInfo.lblMainBuildingNoCurrentAddress,frmMBeKYCContactInfo.lblBuildingNoCurrentAddress,frmMBeKYCContactInfo.tbxBuildingNoCurrentAddress);    
      break;
    case "txtSoiCurrentAddress":
      OnTextEditingDoneEmptyCheck(frmMBeKYCContactInfo.lblMainSoiCurrentAddress,frmMBeKYCContactInfo.lblSoiCurrentAddress,frmMBeKYCContactInfo.txtSoiCurrentAddress);  
      break;
    case "txtRoadCurrentAddress":
      OnTextEditingDoneEmptyCheck(frmMBeKYCContactInfo.lblMainRoadCurrentAddress,frmMBeKYCContactInfo.lblRoadCurrentAddress,frmMBeKYCContactInfo.txtRoadCurrentAddress);  
      break;
    case "tbxSubDistrictCurrentAddress":
      OnTextEditingDoneEmptyCheck(frmMBeKYCContactInfo.lblMainSubDistrictCurrentAddress,frmMBeKYCContactInfo.lblSubDistrictCurrentAddress,frmMBeKYCContactInfo.tbxSubDistrictCurrentAddress);  
      break;
    case "tbxDistrictCurrentAddress":
      OnTextEditingDoneEmptyCheck(frmMBeKYCContactInfo.lblMainDistrictCurrentAddress,frmMBeKYCContactInfo.lblDistrictCurrentAddress,frmMBeKYCContactInfo.tbxDistrictCurrentAddress);  
      break;
    case "tbxProvinceCurrentAddress":
      OnTextEditingDoneEmptyCheck(frmMBeKYCContactInfo.lblMainProvinceCurrentAddress,frmMBeKYCContactInfo.lblProvinceCurrentAddress,frmMBeKYCContactInfo.tbxProvinceCurrentAddress);  
      break;
    case "tbxZipCodeCurrentAddress":
      OnTextEditingDoneEmptyCheck(frmMBeKYCContactInfo.lblMainZipCodeCurrentAddress,frmMBeKYCContactInfo.lblZipCodeCurrentAddress,frmMBeKYCContactInfo.tbxZipCodeCurrentAddress);  
      break;
    case "txtHomePhone2":
      OnTextEditingDoneEmptyCheck(frmMBeKYCContactInfo.lblHomePhoneMain2,frmMBeKYCContactInfo.lblhomePhone2,frmMBeKYCContactInfo.txtHomePhone2);  
      break;
    case "txExtNo2":
      OnTextEditingDoneEmptyCheck(frmMBeKYCContactInfo.lblExtNo2Main,frmMBeKYCContactInfo.lblExtNo2,frmMBeKYCContactInfo.txExtNo2);  
      break;


    case "txtCompanyName":
      OnTextEditingDoneEmptyCheck(frmMBeKYCContactInfo.lblCompanyNameMain,frmMBeKYCContactInfo.lblCompanyName,frmMBeKYCContactInfo.txtCompanyName);  
      break;
    case "txtOfficePhone":
      OnTextEditingDoneEmptyCheck(frmMBeKYCContactInfo.lblOfficePhoneMain,frmMBeKYCContactInfo.lblOfficePhone,frmMBeKYCContactInfo.txtOfficePhone);  
      break;
    case "txtExtNoOfficeAdd":
      OnTextEditingDoneEmptyCheck(frmMBeKYCContactInfo.lblExtNoOfficeAddMain,frmMBeKYCContactInfo.lblExtNoOfficeAdd,frmMBeKYCContactInfo.txtExtNoOfficeAdd);    
      break;
    case "tbxAddressNoOfficeAddress":
      OnTextEditingDoneEmptyCheck(frmMBeKYCContactInfo.lblMainAddressNoOfficeAddress,frmMBeKYCContactInfo.lblAddressNoOfficeAddress,frmMBeKYCContactInfo.tbxAddressNoOfficeAddress);  
      break;
    case "tbxMooOfficeAddress":
      OnTextEditingDoneEmptyCheck(frmMBeKYCContactInfo.lblMainMooOfficeAddress,frmMBeKYCContactInfo.lblMooOfficeAddress,frmMBeKYCContactInfo.tbxMooOfficeAddress);  
      break;
    case "tbxBuildingNoOfficeAddress":
      OnTextEditingDoneEmptyCheck(frmMBeKYCContactInfo.lblMainBuildingNoOfficeAddress,frmMBeKYCContactInfo.lblBuildingNoOfficeAddress,frmMBeKYCContactInfo.tbxBuildingNoOfficeAddress);    
      break;
    case "txtSoiOfficeAddress":
      OnTextEditingDoneEmptyCheck(frmMBeKYCContactInfo.lblMainSoiOfficeAddress,frmMBeKYCContactInfo.lblSoiOfficeAddress,frmMBeKYCContactInfo.txtSoiOfficeAddress);  
      break;
    case "txtRoadOfficeAddress":
      OnTextEditingDoneEmptyCheck(frmMBeKYCContactInfo.lblMainRoadOfficeAddress,frmMBeKYCContactInfo.lblRoadOfficeAddress,frmMBeKYCContactInfo.txtRoadOfficeAddress);  
      break;
    case "tbxSubDistrictOfficeAddress":
      OnTextEditingDoneEmptyCheck(frmMBeKYCContactInfo.lblMainSubDistrictOfficeAddress,frmMBeKYCContactInfo.lblSubDistrictOfficeAddress,frmMBeKYCContactInfo.tbxSubDistrictOfficeAddress);  
      break;
    case "tbxDistrictOfficeAddress":
      OnTextEditingDoneEmptyCheck(frmMBeKYCContactInfo.lblMainDistrictOfficeAddress,frmMBeKYCContactInfo.lblDistrictOfficeAddress,frmMBeKYCContactInfo.tbxDistrictOfficeAddress);  
      break;
    case "tbxProvinceOfficeAddress":
      OnTextEditingDoneEmptyCheck(frmMBeKYCContactInfo.lblMainProvinceOfficeAddress,frmMBeKYCContactInfo.lblProvinceOfficeAddress,frmMBeKYCContactInfo.tbxProvinceOfficeAddress);  
      break;
    case "tbxZipCodeOfficeAddress":
      OnTextEditingDoneEmptyCheck(frmMBeKYCContactInfo.lblMainZipCodeOfficeAddress,frmMBeKYCContactInfo.lblZipCodeOfficeAddress,frmMBeKYCContactInfo.tbxZipCodeOfficeAddress);  
      break;
    case "txtCompanyName2":
      OnTextEditingDoneEmptyCheck(frmMBeKYCContactInfo.lblCompanyNameMain2,frmMBeKYCContactInfo.lblCompanyName2,frmMBeKYCContactInfo.txtCompanyName2);  
      break;
    case "txtOfficePhone2":
      OnTextEditingDoneEmptyCheck(frmMBeKYCContactInfo.lblOfficePhoneMain2,frmMBeKYCContactInfo.lblOfficePhone2,frmMBeKYCContactInfo.txtOfficePhone2);  
      break;
    case "txExtNoOfficeAdd2":
      OnTextEditingDoneEmptyCheck(frmMBeKYCContactInfo.lblExtNoMainOfficeAdd2,frmMBeKYCContactInfo.lblExtNoOfficeAdd2,frmMBeKYCContactInfo.txExtNoOfficeAdd2);    
      break;
  }
}
function onFlexClickActivateTextboxforEditing(lblMainX,lblX,txtX){
  lblMainX.setVisibility(false);
  lblX.setVisibility(true);
  txtX.setVisibility(true);
  txtX.text = isNotBlank(txtX.text) ? "" : txtX.text;
  txtX.setFocus(true);
}
function checkMandatoryFieldsValidation(){
  var isValidationSuccess=true;
  //========================Validation for Register address===================================
  if(frmMBeKYCContactInfo.tbxAddressNo.text === null || frmMBeKYCContactInfo.tbxAddressNo.text === "" || frmMBeKYCContactInfo.tbxAddressNo.parent.skin == lFboxLoanIncorrect){
    frmMBeKYCContactInfo.tbxAddressNo.parent.skin = lFboxLoanIncorrect;
    onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.tbxAddressNo.parent);
    frmMBeKYCContactInfo.tbxAddressNo.setFocus(true);
    isValidationSuccess =false;
  }else if(frmMBeKYCContactInfo.tbxRoad.text === null || frmMBeKYCContactInfo.tbxRoad.text === "" || frmMBeKYCContactInfo.tbxRoad.parent.skin == lFboxLoanIncorrect){
    frmMBeKYCContactInfo.tbxRoad.parent.skin = lFboxLoanIncorrect;
    onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.tbxRoad.parent);
    frmMBeKYCContactInfo.tbxRoad.setFocus(true);
    isValidationSuccess =false;
  }else if(frmMBeKYCContactInfo.tbxSubDistrict.text === null || frmMBeKYCContactInfo.tbxSubDistrict.text === "" || frmMBeKYCContactInfo.tbxSubDistrict.parent.skin == lFboxLoanIncorrect){
    frmMBeKYCContactInfo.tbxSubDistrict.parent.skin = lFboxLoanIncorrect;
    onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.tbxSubDistrict.parent);
    frmMBeKYCContactInfo.tbxSubDistrict.setFocus(true);
    isValidationSuccess =false;
  }else if(frmMBeKYCContactInfo.tbxDistrict.text === null || frmMBeKYCContactInfo.tbxDistrict.text === "" || frmMBeKYCContactInfo.tbxDistrict.parent.skin == lFboxLoanIncorrect){
    frmMBeKYCContactInfo.tbxDistrict.parent.skin = lFboxLoanIncorrect;
    onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.tbxDistrict.parent);
    frmMBeKYCContactInfo.tbxDistrict.setFocus(true);
    isValidationSuccess =false;
  }else if(frmMBeKYCContactInfo.tbxProvince.text === null || frmMBeKYCContactInfo.tbxProvince.text === "" || frmMBeKYCContactInfo.tbxProvince.parent.skin == lFboxLoanIncorrect){
    frmMBeKYCContactInfo.tbxProvince.parent.skin = lFboxLoanIncorrect;
    onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.tbxProvince.parent);
    frmMBeKYCContactInfo.tbxProvince.setFocus(true);
    isValidationSuccess =false;
  }else if(frmMBeKYCContactInfo.tbxZipCode.text === null || frmMBeKYCContactInfo.tbxZipCode.text === "" || frmMBeKYCContactInfo.tbxZipCode.parent.skin == lFboxLoanIncorrect){
    frmMBeKYCContactInfo.tbxZipCode.parent.skin = lFboxLoanIncorrect;
    onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.tbxZipCode.parent);
    frmMBeKYCContactInfo.tbxZipCode.setFocus(true);
    isValidationSuccess =false;
  }else if(frmMBeKYCContactInfo.flxScrollAddressCurrentAddress.isVisible){ //========================Validation for Contact address===================================
    if(frmMBeKYCContactInfo.tbxAddressNoCurrentAddress.text=== null || frmMBeKYCContactInfo.tbxAddressNoCurrentAddress.text=== "" || frmMBeKYCContactInfo.tbxAddressNoCurrentAddress.parent.skin == lFboxLoanIncorrect){
      frmMBeKYCContactInfo.tbxAddressNoCurrentAddress.parent.skin = lFboxLoanIncorrect;
      onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.tbxAddressNoCurrentAddress.parent);
      frmMBeKYCContactInfo.tbxAddressNoCurrentAddress.setFocus(true);
      isValidationSuccess =false;
    }else if(frmMBeKYCContactInfo.txtRoadCurrentAddress.text=== null || frmMBeKYCContactInfo.txtRoadCurrentAddress.text=== "" || frmMBeKYCContactInfo.txtRoadCurrentAddress.parent.skin == lFboxLoanIncorrect){
      frmMBeKYCContactInfo.txtRoadCurrentAddress.parent.skin = lFboxLoanIncorrect;
      onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.txtRoadCurrentAddress.parent);
      frmMBeKYCContactInfo.txtRoadCurrentAddress.setFocus(true);
      isValidationSuccess =false;
    }else if(frmMBeKYCContactInfo.tbxSubDistrictCurrentAddress.text=== null || frmMBeKYCContactInfo.tbxSubDistrictCurrentAddress.text=== "" || frmMBeKYCContactInfo.tbxSubDistrictCurrentAddress.parent.skin == lFboxLoanIncorrect){
      frmMBeKYCContactInfo.tbxSubDistrictCurrentAddress.parent.skin = lFboxLoanIncorrect;
      onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.tbxSubDistrictCurrentAddress.parent);
      frmMBeKYCContactInfo.tbxSubDistrictCurrentAddress.setFocus(true);
      isValidationSuccess =false;
    }else if(frmMBeKYCContactInfo.tbxDistrictCurrentAddress.text=== null || frmMBeKYCContactInfo.tbxDistrictCurrentAddress.text=== "" || frmMBeKYCContactInfo.tbxDistrictCurrentAddress.parent.skin == lFboxLoanIncorrect){
      frmMBeKYCContactInfo.tbxDistrictCurrentAddress.parent.skin = lFboxLoanIncorrect;
      onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.tbxDistrictCurrentAddress.parent);
      frmMBeKYCContactInfo.tbxDistrictCurrentAddress.setFocus(true);
      isValidationSuccess =false;
    }else if(frmMBeKYCContactInfo.tbxProvinceCurrentAddress.text=== null || frmMBeKYCContactInfo.tbxProvinceCurrentAddress.text=== "" || frmMBeKYCContactInfo.tbxProvinceCurrentAddress.parent.skin == lFboxLoanIncorrect){
      frmMBeKYCContactInfo.tbxProvinceCurrentAddress.parent.skin = lFboxLoanIncorrect;
      onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.tbxProvinceCurrentAddress.parent);
      frmMBeKYCContactInfo.tbxProvinceCurrentAddress.setFocus(true);
      isValidationSuccess =false;
    }else if(frmMBeKYCContactInfo.tbxZipCodeCurrentAddress.text=== null || frmMBeKYCContactInfo.tbxZipCodeCurrentAddress.text=== "" || frmMBeKYCContactInfo.tbxZipCodeCurrentAddress.parent.skin == lFboxLoanIncorrect){
      frmMBeKYCContactInfo.tbxZipCodeCurrentAddress.parent.skin = lFboxLoanIncorrect;
      onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.tbxZipCodeCurrentAddress.parent);
      frmMBeKYCContactInfo.tbxZipCodeCurrentAddress.setFocus(true);
      isValidationSuccess =false;
    }else if(frmMBeKYCContactInfo.txtHomePhone2.text === null || frmMBeKYCContactInfo.txtHomePhone2.text.length <= 10 || frmMBeKYCContactInfo.txtHomePhone2.parent.skin == lFboxLoanIncorrect){
      frmMBeKYCContactInfo.txtHomePhone2.parent.skin = lFboxLoanIncorrect;
      //onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.txtHomePhone2.parent);
      frmMBeKYCContactInfo.lblHomePhoneMain2lFboxLoan.setVisibility(false);
      frmMBeKYCContactInfo.lblhomePhone2.setVisibility(true);
      frmMBeKYCContactInfo.txtHomePhone2.setVisibility(true);  
      frmMBeKYCContactInfo.txtHomePhone2.setFocus(true);
      isValidationSuccess =false;
    }
  }else if(frmMBeKYCContactInfo.txtHomePhone.text === null || frmMBeKYCContactInfo.txtHomePhone.text.length <= 10 || frmMBeKYCContactInfo.txtHomePhone.parent.skin == lFboxLoanIncorrect ){
    frmMBeKYCContactInfo.txtHomePhone.parent.skin = lFboxLoanIncorrect;
    //onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.txtHomePhone.parent);
    frmMBeKYCContactInfo.lblHomePhoneMain.setVisibility(false);
    frmMBeKYCContactInfo.lblhomePhone.setVisibility(true);
    frmMBeKYCContactInfo.txtHomePhone.setVisibility(true);    
    frmMBeKYCContactInfo.txtHomePhone.setFocus(true);
    isValidationSuccess =false;
  }else if(frmMBeKYCContactInfo.flxScrollAddressOfficeAddress.isVisible){//========================Validation for Office address===================================
    if(frmMBeKYCContactInfo.tbxAddressNoOfficeAddress.text === null || frmMBeKYCContactInfo.tbxAddressNoOfficeAddress.text === "" || frmMBeKYCContactInfo.tbxAddressNoOfficeAddress.parent.skin == lFboxLoanIncorrect){
      frmMBeKYCContactInfo.tbxAddressNoOfficeAddress.parent.skin = lFboxLoanIncorrect;
      onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.tbxAddressNoOfficeAddress.parent);
      frmMBeKYCContactInfo.tbxAddressNoOfficeAddress.setFocus(true);
      isValidationSuccess =false;
    }else if(frmMBeKYCContactInfo.txtRoadOfficeAddress.text === null || frmMBeKYCContactInfo.txtRoadOfficeAddress.text === "" || frmMBeKYCContactInfo.txtRoadOfficeAddress.parent.skin == lFboxLoanIncorrect){
      frmMBeKYCContactInfo.txtRoadOfficeAddress.parent.skin = lFboxLoanIncorrect;
      onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.txtRoadOfficeAddress.parent);
      frmMBeKYCContactInfo.txtRoadOfficeAddress.setFocus(true);
      isValidationSuccess =false;
    }else if(frmMBeKYCContactInfo.tbxSubDistrictOfficeAddress.text === null || frmMBeKYCContactInfo.tbxSubDistrictOfficeAddress.text === "" || frmMBeKYCContactInfo.tbxSubDistrictOfficeAddress.parent.skin == lFboxLoanIncorrect){
      frmMBeKYCContactInfo.tbxSubDistrictOfficeAddress.parent.skin = lFboxLoanIncorrect;
      onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.tbxSubDistrictOfficeAddress.parent);
      frmMBeKYCContactInfo.tbxSubDistrictOfficeAddress.setFocus(true);
      isValidationSuccess =false;
    }else if(frmMBeKYCContactInfo.tbxDistrictOfficeAddress.text === null || frmMBeKYCContactInfo.tbxDistrictOfficeAddress.text === "" || frmMBeKYCContactInfo.tbxDistrictOfficeAddress.parent.skin == lFboxLoanIncorrect){
      frmMBeKYCContactInfo.tbxDistrictOfficeAddress.parent.skin = lFboxLoanIncorrect;
      onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.tbxDistrictOfficeAddress.parent);
      frmMBeKYCContactInfo.tbxDistrictOfficeAddress.setFocus(true);
      isValidationSuccess =false;
    }else if(frmMBeKYCContactInfo.tbxProvinceOfficeAddress.text === null || frmMBeKYCContactInfo.tbxProvinceOfficeAddress.text === "" || frmMBeKYCContactInfo.tbxProvinceOfficeAddress.parent.skin == lFboxLoanIncorrect){
      frmMBeKYCContactInfo.tbxProvinceOfficeAddress.parent.skin = lFboxLoanIncorrect;
      onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.tbxProvinceOfficeAddress.parent);
      frmMBeKYCContactInfo.tbxProvinceOfficeAddress.setFocus(true);
      isValidationSuccess =false;
    }else if(frmMBeKYCContactInfo.tbxZipCodeOfficeAddress.text === null || frmMBeKYCContactInfo.tbxZipCodeOfficeAddress.text === "" || frmMBeKYCContactInfo.tbxZipCodeOfficeAddress.parent.skin == lFboxLoanIncorrect){
      frmMBeKYCContactInfo.tbxZipCodeOfficeAddress.parent.skin = lFboxLoanIncorrect;
      onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.tbxZipCodeOfficeAddress.parent);
      frmMBeKYCContactInfo.tbxZipCodeOfficeAddress.setFocus(true);
      isValidationSuccess =false;
    }else if( frmMBeKYCContactInfo.txtCompanyName2.text === null || frmMBeKYCContactInfo.txtCompanyName2.text === "" || frmMBeKYCContactInfo.txtCompanyName2.parent.skin == lFboxLoanIncorrect){
      frmMBeKYCContactInfo.txtCompanyName2.parent.skin = lFboxLoanIncorrect;
      onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.txtCompanyName2.parent);
      frmMBeKYCContactInfo.txtCompanyName2.setFocus(true);
      isValidationSuccess =false;
    }else if(frmMBeKYCContactInfo.txtOfficePhone2.text === null || frmMBeKYCContactInfo.txtOfficePhone2.text.length <= 10 || frmMBeKYCContactInfo.txtOfficePhone2.parent.skin == lFboxLoanIncorrect){
      frmMBeKYCContactInfo.txtOfficePhone2.parent.skin = lFboxLoanIncorrect;
      frmMBeKYCContactInfo.lblOfficePhoneMain2.setVisibility(false);
      frmMBeKYCContactInfo.lblOfficePhone2.setVisibility(true);
      frmMBeKYCContactInfo.txtOfficePhone2.setVisibility(true);  
      frmMBeKYCContactInfo.txtOfficePhone2.setFocus(true);
      isValidationSuccess =false;
    }
  }else if( frmMBeKYCContactInfo.txtCompanyName.text === null || frmMBeKYCContactInfo.txtCompanyName.text === "" || frmMBeKYCContactInfo.txtCompanyName.parent.skin == lFboxLoanIncorrect){
    frmMBeKYCContactInfo.txtCompanyName.parent.skin = lFboxLoanIncorrect;
    onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.txtCompanyName.parent);
    frmMBeKYCContactInfo.txtCompanyName.setFocus(true);
    isValidationSuccess =false;
  }else if(frmMBeKYCContactInfo.txtOfficePhone.text === null || frmMBeKYCContactInfo.txtOfficePhone.text.length <= 10 || frmMBeKYCContactInfo.txtOfficePhone.parent.skin == lFboxLoanIncorrect){
    frmMBeKYCContactInfo.txtOfficePhone.parent.skin = lFboxLoanIncorrect;
    //onFlexClickActivateTextboxContactInfo(frmMBeKYCContactInfo.txtOfficePhone.parent);
    frmMBeKYCContactInfo.lblOfficePhoneMain.setVisibility(false);
    frmMBeKYCContactInfo.lblOfficePhone.setVisibility(true);
    frmMBeKYCContactInfo.txtOfficePhone.setVisibility(true);  
    frmMBeKYCContactInfo.txtOfficePhone.setFocus(true);
    isValidationSuccess =false;
  }
  else{
    // set default skin of all widgets after successfull varification.
    frmMBeKYCContactInfo.tbxAddressNo.parent.skin = lFboxLoan;
    frmMBeKYCContactInfo.tbxRoad.parent.skin = lFboxLoan;
    frmMBeKYCContactInfo.tbxSubDistrict.parent.skin = lFboxLoan;
    frmMBeKYCContactInfo.tbxDistrict.parent.skin = lFboxLoan;
    frmMBeKYCContactInfo.tbxProvince.parent.skin = lFboxLoan;
    frmMBeKYCContactInfo.tbxZipCode.parent.skin = lFboxLoan;
    if(frmMBeKYCContactInfo.flxScrollAddressCurrentAddress.isVisible){
      frmMBeKYCContactInfo.tbxAddressNoCurrentAddress.parent.skin = lFboxLoan;
      frmMBeKYCContactInfo.txtRoadCurrentAddress.parent.skin = lFboxLoan;
      frmMBeKYCContactInfo.tbxSubDistrictCurrentAddress.parent.skin = lFboxLoan;
      frmMBeKYCContactInfo.tbxDistrictCurrentAddress.parent.skin = lFboxLoan;
      frmMBeKYCContactInfo.tbxProvinceCurrentAddress.parent.skin = lFboxLoan;
      frmMBeKYCContactInfo.tbxZipCodeCurrentAddress.parent.skin = lFboxLoan;
      frmMBeKYCContactInfo.txtHomePhone2.parent.skin = lFboxLoan;	
    }
    else{
      frmMBeKYCContactInfo.txtHomePhone.parent.skin = lFboxLoan;
    }
    if(frmMBeKYCContactInfo.flxScrollAddressOfficeAddress.isVisible){
      frmMBeKYCContactInfo.tbxAddressNoOfficeAddress.parent.skin = lFboxLoan;
      frmMBeKYCContactInfo.txtRoadOfficeAddress.parent.skin = lFboxLoan;
      frmMBeKYCContactInfo.tbxSubDistrictOfficeAddress.parent.skin = lFboxLoan;
      frmMBeKYCContactInfo.tbxDistrictOfficeAddress.parent.skin = lFboxLoan;
      frmMBeKYCContactInfo.tbxProvinceOfficeAddress.parent.skin = lFboxLoan;
      frmMBeKYCContactInfo.tbxZipCodeOfficeAddress.parent.skin = lFboxLoan;
      frmMBeKYCContactInfo.txtCompanyName2.parent.skin = lFboxLoan;
      frmMBeKYCContactInfo.txtOfficePhone2.parent.skin = lFboxLoan;
    }else{
      frmMBeKYCContactInfo.txtCompanyName.parent.skin = lFboxLoan;
      frmMBeKYCContactInfo.txtOfficePhone.parent.skin = lFboxLoan;
    }
  }

  return isValidationSuccess;
}

function OnTextEditingDoneEmptyCheck(lblMainX , lblX , txtX){
  if(!isNotBlank(txtX.text)) {
    lblMainX.isVisible=true;
    lblX.isVisible=false;
    txtX.isVisible=false;
    txtX.setFocus(false);
  }
}
function resetEditedTextBoxContactInfo(lblMainX , lblX , txtX){
  lblMainX.isVisible=true;
  lblX.isVisible=false;
  txtX.isVisible=false;
  txtX.text = "";
  txtX.setFocus(false);
}
function isValidAddress(objtxtBx){
  var pat1 = /^([a-zA-Z0-9 \u0E00-\u0E7F\/-]+)$/g; //Reg Exp for Alphanumeric, Number, Thai,  hiphen, forard slash 
  var isValid = true;
  if(objtxtBx.text !== ""){
    isValid = pat1.test(objtxtBx.text);
  }
  if(isValid)
  {    
    objtxtBx.parent.skin=lFboxLoan;
  }
  else{
    objtxtBx.parent.skin=lFboxLoanIncorrect;

  }
  return isValid;
}
//Common Functions-------------------- End
// MKI, MIB-11728 end



