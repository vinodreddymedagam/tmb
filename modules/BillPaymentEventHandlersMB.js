function eh_frmBillPayment_btnSchedulePay_onClick() {
	gblScheduleEndBPTmp = gblScheduleEndBP;
	gblScheduleRepeatBPTmp = gblScheduleRepeatBP;
	frmSchedule.show();
};




function eh_frmBillPaymentConfirmationFuture_preshow() {
    frmBillPaymentConfirmationFuture.scrollboxMain.scrollToEnd();
    showdetails=true;
    //frmBillPaymentConfirmationFuture.hbxAmountDetailsMEA.setVisibility(true);
	frmBillPaymentConfirmationFuture.hbxAmtValue.setVisibility(true);
	frmBillPaymentConfirmationFuture.hbxAmtInterest.setVisibility(true);
	frmBillPaymentConfirmationFuture.hbxAmtDisconnected.setVisibility(true);
    
    frmBillPaymentConfirmationFuturePreShow();
    isMenuShown = false;
    isSignedUser = true;
    if (GblBillTopFlag) {
        frmBillPaymentConfirmationFuture.lblTopUpProductName.setVisibility(false);
        frmBillPaymentConfirmationFuture.hbxEasyPass.setVisibility(false);
        frmBillPaymentConfirmationFuture.hbxEasyPassTxnId.setVisibility(false);
    } else {
        frmBillPaymentConfirmationFuture.hbxRef2.setVisibility(false);
        if (gblEasyPassTopUp) {
            frmBillPaymentConfirmationFuture.lblTopUpProductName.setVisibility(true);
            frmBillPaymentConfirmationFuture.hbxEasyPass.setVisibility(true);
            frmBillPaymentConfirmationFuture.hbxEasyPassTxnId.setVisibility(true);
            kony.print("easy pass true frmBillPaymentConfirmationFuture.lblBillerNickname.text is " + frmBillPaymentConfirmationFuture.lblBillerNickname.text);
        } else {
            frmBillPaymentConfirmationFuture.lblTopUpProductName.setVisibility(false);
            frmBillPaymentConfirmationFuture.hbxEasyPass.setVisibility(false);
            frmBillPaymentConfirmationFuture.hbxEasyPassTxnId.setVisibility(false);
            kony.print("easypass false frmBillPaymentConfirmationFuture.lblBillerNickname.text is " + frmBillPaymentConfirmationFuture.lblBillerNickname.text);
        }
    }
    DisableFadingEdges(frmBillPaymentConfirmationFuture);
}


function eh_frmBillPaymentConfirmationFuture_postshow() {
	assignGlobalForMenuPostshow();
};

function eh_frmBillPaymentConfirmationFuture_btnRight_onClick() {
    if (GblBillTopFlag) {
        frmBillPayment.show();
    } else {
        frmTopUp.show();
    }
};

function eh_frmBillPaymentConfirmationFuture_btnConfirm_onClick() {
    popupTractPwd.lblPopupTract7.skin = lblPopupLabelTxt;
    popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = tbxPopupBlue;
    popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = tbxPopupBlue;
    onBillPaymentConfirm();
};

function frmBillPaymentConfirmationFuture_btnCancel_onClick_seq1() {
    gblScannedBiller = false;
    if (GblBillTopFlag) {
        gblFirstTimeBillPayment = true;
        gblPaynow = true;
        frmBillPayment.show();
    } else {
        gblFirstTimeTopUp = true;
        gblPaynow = true;
        frmTopUp.show();
    }
};

function frmBillPaymentConfirmationFuture_btnCancelSpa_onClick_seq1() {
    if (GblBillTopFlag) {
        gblFirstTimeBillPayment = true;
        gblPaynow = true;
        frmBillPayment.show();
    } else {
        gblFirstTimeTopUp = true;
        gblPaynow = true;
        frmTopUp.show();
    }
};

function eh_frmBillPaymentComplete_preshow() {
//gblDisplayBalanceBillPayment = true;
    frmBillPaymentComplete.scrollboxMain.scrollToEnd();
    frmBillPaymentCompletePreShow();
    isMenuShown = false;
    isSignedUser = true;
    
    if (GblBillTopFlag) {
        frmBillPaymentComplete.lblTopUpProductName.setVisibility(false);
        frmBillPaymentComplete.hbxEasyPass.setVisibility(false);
        frmBillPaymentComplete.hbxEasyPassTxnId.setVisibility(false);
    } else {
        frmBillPaymentComplete.hbxRef2.setVisibility(false)
        if (gblEasyPassTopUp) {
            frmBillPaymentComplete.lblTopUpProductName.setVisibility(true);
            frmBillPaymentComplete.hbxEasyPass.setVisibility(true);
            frmBillPaymentComplete.hbxEasyPassTxnId.setVisibility(true);
        } else {
            frmBillPaymentComplete.lblTopUpProductName.setVisibility(false);
            frmBillPaymentComplete.hbxEasyPass.setVisibility(false);
            frmBillPaymentComplete.hbxEasyPassTxnId.setVisibility(false);
        }
    }
    frmBillPaymentComplete.hbxAmountDetailsMEA.setVisibility(false);
    DisableFadingEdges(frmBillPaymentComplete);
    //frmBillPaymentComplete.hboxaddfblist.isVisible = false;
    frmBillPaymentComplete.btnRight.skin = btnShare;
    frmBillPaymentComplete.imgHeaderMiddle.src = "arrowtop.png";
    frmBillPaymentComplete.imgHeaderRight.src = "empty.png";
    
	//code for personalized banner display
   try{
    	frmBillPaymentComplete.hbxAdv.setVisibility(false);
    	frmBillPaymentComplete.hbxCommon.setVisibility(false);
    	frmBillPaymentComplete.img1.src="";
    	frmBillPaymentComplete.imgTwo.src="";
    	frmBillPaymentComplete.gblBrwCmpObject.handleRequest="";
    	frmBillPaymentComplete.gblBrwCmpObject.htmlString="";
    	frmBillPaymentComplete.gblVbxCmp.remove(gblBrwCmpObject);
       	frmBillPaymentComplete.hbxAdv.remove(gblVbxCmp);
       	frmBillPaymentComplete.hbxCommon.remove(gblVbxCmp);
    }
    catch(e)
    {
    }
	
};

function eh_frmBillPaymentComplete_postshow() {
    
    if (GblBillTopFlag == true) {
        campaginService("img1", "frmBillPaymentComplete", "M");
    } else {
        campaginService("imgTwo", "frmTopupPaymentComplete", "M");
    }
};

function eh_frmBillPaymentComplete_hbox47792425956433_onClick() {

    onClickForInnerBoxes();
    if (isMenuShown == false) {
        handleMenuBtn();
    } else {
        onClickForInnerBoxes();
    }
};

function eh_frmSelectBiller_frmSelectBiller_preshow() {
	changeStatusBarColor();
	frmSelectBiller.scrollboxMain.scrollToEnd();
    if (GblBillTopFlag) {
        frmSelectBiller.lblHdrTxt.text = kony.i18n.getLocalizedString("keyBillPaymentSelectBill");
        frmSelectBiller.tbxSearch.placeholder = kony.i18n.getLocalizedString("keySearch");
    } else {
        frmSelectBiller.lblHdrTxt.text = kony.i18n.getLocalizedString('SelectTopUp');
        frmSelectBiller.tbxSearch.placeholder = kony.i18n.getLocalizedString("keySearch");
		//frmSelectBiller.lblCategories.text = kony.i18n.getLocalizedString("MIB_BPCateTitle");
        if(gblCategoryFormClosed) {
        	gblCategoryFormClosed = false;
        }else{
        	getMyBillTopUpSuggestBillerListMB();
        }
    }
    frmSelectBiller.lblCancel.text = kony.i18n.getLocalizedString('keyCancelButton');
    
}

function gotoSelectBillerForm() {
	displaySelectBillerActive(true);
	displaySelectBillerHrdBack();
	disableSelectBillerCancelBtn();
	showLoadingScreen();
	gblSelectBillerCategoryID = "0";
	gblMyBillerTopUpBB = 0;
	GblBillTopFlag = true;
	isSearched = false;
	frmSelectBiller.tbxSearch.text = "";
	frmSelectBiller.tbxSearch.placeholder = kony.i18n.getLocalizedString("keySearch");
	frmSelectBiller.lblCategories.text = kony.i18n.getLocalizedString("MIB_BPCateTitle"); 
	frmSelectBiller.lblNoBillers.setVisibility(false);
	getMyBillTopUpSuggestBillerListMB();
}

function eh_frmSelectBiller_btnRight_onClick() {
    if (checkMaxBillerCountMB()) {
        if (checkMaxBillerCountMB()) {
            frmAddTopUpBillerconfrmtn.segConfirmationList.removeAll();
            TMBUtil.DestroyForm(frmAddTopUpToMB);
            frmAddTopUpToMB.imgAddedBiller.src = "";
            frmAddTopUpToMB.lblAddbillerName.text = "";
            frmAddTopUpToMB.lblAddedRef1.text = kony.i18n.getLocalizedString("keyRef1");
            frmAddTopUpToMB.lblAddedRef2.text = kony.i18n.getLocalizedString("keyRef2");
            frmAddTopUpToMB.show();
        } else {
            alert(kony.i18n.getLocalizedString("Valid_MoreThan50"));
        }
    } else {
        var alert_seq2_act0 = kony.ui.Alert({
            "message": kony.i18n.getLocalizedString("Valid_MoreThan50"),
            "alertType": constants.ALERT_TYPE_ERROR,
            "alertTitle": "",
            "yesLabel": "Ok",
            "noLabel": "",
            "alertIcon": "",
            "alertHandler": null
        }, {});
    }
};
function showHideDetails(){
	if(showdetails==true){
		showdetails=false;
		frmBillPaymentConfirmationFuture.segPayDetails.setVisibility(false);
      	frmBillPaymentConfirmationFuture.hbxMeterNum.setVisibility(true);
		frmBillPaymentConfirmationFuture.linesegbelow.setVisibility(false);
		frmBillPaymentConfirmationFuture.linkshowhide.text=kony.i18n.getLocalizedString("ShowDetail");
	}else{
		showdetails=true;
		frmBillPaymentConfirmationFuture.segPayDetails.setVisibility(true);
      	frmBillPaymentConfirmationFuture.hbxMeterNum.setVisibility(false);
		frmBillPaymentConfirmationFuture.linesegbelow.setVisibility(true);
		frmBillPaymentConfirmationFuture.linkshowhide.text=kony.i18n.getLocalizedString("HideDetail");
	}

}
function showHideDetailsComplete(){
	if(showdetails==true){
		showdetails=false;
		frmBillPaymentComplete.segPayDetails.setVisibility(false);
		frmBillPaymentComplete.linesegbelow.setVisibility(false);
		frmBillPaymentComplete.linkshowhide.text=kony.i18n.getLocalizedString("ShowDetail");
	}else{
		showdetails=true;
		frmBillPaymentComplete.segPayDetails.setVisibility(true);
		frmBillPaymentComplete.linesegbelow.setVisibility(true);
		frmBillPaymentComplete.linkshowhide.text=kony.i18n.getLocalizedString("HideDetail");
	}

}

function showMEACustDetailsMB(frmName){
	if(gblSegBillerDataMB["MeterNo"]!=undefined && gblSegBillerDataMB["MeterNo"]!=null && gblSegBillerDataMB["MeterNo"]!="" ){
		frmName.lblMeterNumValue.text=gblSegBillerDataMB["MeterNo"];
        if(gblPEA_BILLER_COMP_CODE.indexOf(gblCompCode) >= 0) {
        	frmName.hbxMeterNum.setVisibility(false);
        } else {
			frmName.hbxMeterNum.setVisibility(true);
        }
		
	}else{
		frmName.hbxMeterNum.setVisibility(false);
	}
	if(gblSegBillerDataMB["CustName"]!=undefined && gblSegBillerDataMB["CustName"]!=null && gblSegBillerDataMB["CustName"]!="" ){
		frmName.lblCustNameValue.text=gblSegBillerDataMB["CustName"];
		frmName.hbxMEACustName.setVisibility(true);
	}else{
		frmName.hbxMEACustName.setVisibility(false);
	}
	if(gblSegBillerDataMB["CustAddress"]!=undefined && gblSegBillerDataMB["CustAddress"]!=null && gblSegBillerDataMB["CustAddress"]!="" ){
		if (kony.i18n.getCurrentLocale() != "th_TH"){
			frmName.lblCustAddressValue.text=gblSegBillerDataMB["billerCustAddressEn"]+"  "+gblSegBillerDataMB["CustAddress"];
		}else{
			frmName.lblCustAddressValue.text=gblSegBillerDataMB["billerCustAddressTh"]+"  "+gblSegBillerDataMB["CustAddress"];
		}		
		frmName.hbxCustAddress.setVisibility(true);
	}else{
		frmName.hbxCustAddress.setVisibility(false);
	}
	if (kony.i18n.getCurrentLocale() != "th_TH"){
		frmName.lblMeterNum.text=gblSegBillerDataMB["billerMeterNoEn"];
		frmName.lblCustName.text=gblSegBillerDataMB["billerCustNameEn"];
		//frmName.lblCustAddress.text=gblSegBillerDataMB["billerCustAddressEn"]+":";
	}else{
		frmName.lblMeterNum.text=gblSegBillerDataMB["billerMeterNoTh"];
		frmName.lblCustName.text=gblSegBillerDataMB["billerCustNameTh"];
		//frmName.lblCustAddress.text=gblSegBillerDataMB["billerCustAddressTh"]+":";
	}
}

function onDoneAmountOfBillPay() {
	if(isNotBlank(frmBillPayment.tbxAmount.text)) {
		frmBillPayment.tbxAmount.text = commaFormatted(parseFloat(removeCommos(frmBillPayment.tbxAmount.text)).toFixed(2)) + kony.i18n.getLocalizedString("currencyThaiBaht");
	} else {
		//frmBillPayment.tbxAmount.text = "0.00" + kony.i18n.getLocalizedString("currencyThaiBaht");
		frmBillPayment.tbxAmount.text = "";
	}
}

function editTopUpBillerValidation() {
	if(NickNameValid(frmMyTopUpEditScreens.txtEditName.text)) {
		callCustomerBillUpdateMB();
	} else {
		frmMyTopUpEditScreens.txtEditName.skin = txtErrorBG;
		frmMyTopUpEditScreens.txtEditName.focusSkin = txtErrorBG;
		alert(kony.i18n.getLocalizedString("Valid_BillerNicknameMandatory"));
	}
}

function frmBillPaymentEditFutureComplete_Preshow() {
gblDisplayBalanceBillPayment = true;
    frmBillPaymentEditFutureComplete.scrollboxMain.scrollToEnd();
    frmBillPaymentEditFutureCompletePreShow();
    isMenuShown = false;
    isSignedUser = true;
    
    if (GblBillTopFlag) {
        frmBillPaymentEditFutureComplete.lblTopUpProductName.setVisibility(false);
        frmBillPaymentEditFutureComplete.hbxEasyPass.setVisibility(false);
        frmBillPaymentEditFutureComplete.hbxEasyPassTxnId.setVisibility(false);
    } else {
        frmBillPaymentEditFutureComplete.hbxRef2.setVisibility(false)
        if (gblEasyPassTopUp) {
            frmBillPaymentEditFutureComplete.lblTopUpProductName.setVisibility(true);
            frmBillPaymentEditFutureComplete.hbxEasyPass.setVisibility(true);
            frmBillPaymentEditFutureComplete.hbxEasyPassTxnId.setVisibility(true);
        } else {
            frmBillPaymentEditFutureComplete.lblTopUpProductName.setVisibility(false);
            frmBillPaymentEditFutureComplete.hbxEasyPass.setVisibility(false);
            frmBillPaymentEditFutureComplete.hbxEasyPassTxnId.setVisibility(false);
        }
    }
    frmBillPaymentEditFutureComplete.hbxAmountDetailsMEA.setVisibility(false);
    DisableFadingEdges(frmBillPaymentEditFutureComplete);
    frmBillPaymentEditFutureComplete.btnRight.skin = btnShare;
    frmBillPaymentEditFutureComplete.imgHeaderMiddle.src = "arrowtop.png";
    frmBillPaymentEditFutureComplete.imgHeaderRight.src = "empty.png";
    
	//code for personalized banner display
   try{
    	frmBillPaymentEditFutureComplete.hbxAdv.setVisibility(false);
    	frmBillPaymentEditFutureComplete.hbxCommon.setVisibility(false);
    	frmBillPaymentEditFutureComplete.img1.src="";
    	frmBillPaymentEditFutureComplete.imgTwo.src="";
    	frmBillPaymentEditFutureComplete.gblBrwCmpObject.handleRequest="";
    	frmBillPaymentEditFutureComplete.gblBrwCmpObject.htmlString="";
    	frmBillPaymentEditFutureComplete.gblVbxCmp.remove(gblBrwCmpObject);
       	frmBillPaymentEditFutureComplete.hbxAdv.remove(gblVbxCmp);
       	frmBillPaymentEditFutureComplete.hbxCommon.remove(gblVbxCmp);
    }
    catch(e)
    {
    }
	
};

function frmBillPaymentEditFutureComplete_Postshow() {
//     if (gblBPflag) {
//         campaginService("img1", "frmBillPaymentEditFutureComplete", "M");
//     } else {
//         campaginService("imgTwo", "frmTopupPaymentComplete", "M");
//     }
};
