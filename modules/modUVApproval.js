gblUVRejectedReason = -1;
gblUVTransactionData = {};
gblFromUVReject = false;


function navigateToUVApproval(){
  gblFromUVReject = true;
  frmUVApprovalMB.show();
}

function gotoUVPreviousScreen(){
  	var prevFrm = kony.application.getPreviousForm();
  	prevFrm.show();
}


function rejectPKIAuthenticate(){
  	var dataToBeSigned = dataTobeAuthenticate.dataToBeSigned;
    kony.print("Inside showUVPushDetails dataToBeSigned>>> "+dataToBeSigned);
    var messageId = dataTobeAuthenticate.messageId;
    kony.print("Inside showUVPushDetails messageId>>> "+messageId);
    var authResult = setUpPKIAuthntication(messageId, dataToBeSigned, true);
   dismissLoadingScreen();  
 	if(authResult) {
      gotoUVRejectCompleteScreen(0);
      invokeUVActivityLogging("uvAuthActivityLogging", getEmptyValIfNoPropInJsonObj(gblUVTransactionData, "transactionID"), "2");
    } else {
      gotoUVRejectCompleteScreen(1);
      invokeUVActivityLogging("uvAuthActivityLogging", getEmptyValIfNoPropInJsonObj(gblUVTransactionData, "transactionID"), "3");
    }
}
/**
 * @function
 *
 */
function onClickOnRejectUVApprovalTransDetails(){
  	
  // Below service for UV Tracking
  	showLoadingScreen();
    var inputParam = {};
  	inputParam["deviceId"] = getDeviceID();
  	inputParam["reasonID"] = gblUVRejectedReason;
   if(gblUVRejectedReason == 0){
        inputParam["reasonText"] = frmUVTransRejectMenu.btnReason1.text;
   }else if(gblUVRejectedReason == 1){
      	inputParam["reasonText"] = frmUVTransRejectMenu.btnReason2.text;
    }else if(gblUVRejectedReason == 2){
      	inputParam["reasonText"] = frmUVTransRejectMenu.btnReason3.text;
      	inputParam["reasonOtherText"] = frmUVTransRejectMenu.txtAreaReason3.text;
    }
  	inputParam["tranType"] = getEmptyValIfNoPropInJsonObj(gblUVTransactionData, "transactionType");
  	inputParam["transactionID"] = getEmptyValIfNoPropInJsonObj(gblUVTransactionData, "transactionID");
  	inputParam["channelID"] = getEmptyValIfNoPropInJsonObj(gblUVTransactionData, "channelID");
    invokeServiceSecureAsync("rejectReason", inputParam, callBackUVRejectService);
}

function callBackUVRejectService(status, resulttable){
  	if (status == 400) {
      if (resulttable["opstatus"] == "0") {  
          kony.print("RejectReason Service executed Successfully");
         rejectPKIAuthenticate(); //pkiAuthenticate to VTAP
      }else{
          	dismissLoadingScreen();
            gotoUVRejectCompleteScreen(1);
      		invokeUVActivityLogging("uvAuthActivityLogging", getEmptyValIfNoPropInJsonObj(gblUVTransactionData, "transactionID"), "3");
        }
    }
}

function populateUVRejectCompleteScreen(status){
  frmUVApprovalCompleteNew.btnDone.setVisibility(false);
    frmUVApprovalCompleteNew.btnGoToAcctSum.setVisibility(false);
  try{  
  if(status == 1){	// 10. Failed Reject Approval
      frmUVApprovalCompleteNew.imgStatus.src = "uv_error.gif";
      frmUVApprovalCompleteNew.lblTitle.text = kony.i18n.getLocalizedString("Result_txtTitle101");
      frmUVApprovalCompleteNew.lblDesc.text = kony.i18n.getLocalizedString("Result_txtDes102");
      frmUVApprovalCompleteNew.btnDone.setVisibility(true);
      frmUVApprovalCompleteNew.btnDone.text = kony.i18n.getLocalizedString("Back");
      frmUVApprovalCompleteNew.btnDone.onClick = gotoUVPreviousScreen;
      frmUVApprovalCompleteNew.btnGoToAcctSum.setVisibility(true);
      frmUVApprovalCompleteNew.btnGoToAcctSum.text = kony.i18n.getLocalizedString("Result_urlTxn14");
      frmUVApprovalCompleteNew.btnGoToAcctSum.onClick = onClickUVApprovalGoToAccountSummary;
    }else{	// Success Reject 
      kony.print("gblUVRejectedReason >>>>"+gblUVRejectedReason);
      frmUVApprovalCompleteNew.imgStatus.src = "uv_cancelrequest.gif";
      frmUVApprovalCompleteNew.lblTitle.text = kony.i18n.getLocalizedString("Result_txtTitle71");
      if(gblUVRejectedReason === 0){
        frmUVApprovalCompleteNew.lblDesc.text = kony.i18n.getLocalizedString("Result_txtDes72");
        frmUVApprovalCompleteNew.btnDone.setVisibility(true);
        frmUVApprovalCompleteNew.btnDone.text = kony.i18n.getLocalizedString("Result_urlTxn24");
        frmUVApprovalCompleteNew.btnDone.onClick = onClickOnTMBCall1558;
        frmUVApprovalCompleteNew.btnGoToAcctSum.setVisibility(true);
        frmUVApprovalCompleteNew.btnGoToAcctSum.text = kony.i18n.getLocalizedString("Result_urlTxn14");
        frmUVApprovalCompleteNew.btnGoToAcctSum.onClick = onClickUVApprovalGoToAccountSummary;
      }else if(gblUVRejectedReason == 1){
        frmUVApprovalCompleteNew.lblDesc.text = kony.i18n.getLocalizedString("Result_txtDes92");
        frmUVApprovalCompleteNew.btnDone.setVisibility(true);
        frmUVApprovalCompleteNew.btnDone.text = kony.i18n.getLocalizedString("Result_btnTxn13");
        frmUVApprovalCompleteNew.btnDone.onClick = gotoTMBConfirmOrExit;
        frmUVApprovalCompleteNew.btnGoToAcctSum.setVisibility(true);
        frmUVApprovalCompleteNew.btnGoToAcctSum.text = kony.i18n.getLocalizedString("Result_urlTxn24");
        frmUVApprovalCompleteNew.btnGoToAcctSum.onClick = onClickOnTMBCall1558;
      }else if(gblUVRejectedReason == 2){
        frmUVApprovalCompleteNew.lblDesc.text = kony.i18n.getLocalizedString("Result_txtDes92");
        frmUVApprovalCompleteNew.btnDone.setVisibility(true);
        frmUVApprovalCompleteNew.btnDone.text = kony.i18n.getLocalizedString("Result_urlTxn24");
        frmUVApprovalCompleteNew.btnDone.onClick = onClickOnTMBCall1558;
        frmUVApprovalCompleteNew.btnGoToAcctSum.setVisibility(true);
        frmUVApprovalCompleteNew.btnGoToAcctSum.text = kony.i18n.getLocalizedString("Result_urlTxn14");
        frmUVApprovalCompleteNew.btnGoToAcctSum.onClick = onClickUVApprovalGoToAccountSummary;
      }
      frmUVApprovalCompleteNew.lblScreenTitle.text = kony.i18n.getLocalizedString("UVR_txtTitle");
      dismissLoadingScreen();
    }
    frmUVApprovalCompleteNew.show();
  }catch(e){
    kony.print("Exception ### : "+e);
  }
}

/**
 * @function
 *
 * @param status 
 */
function populateUVRejectCompletePopUp(status){
  try{
  kony.print("populateUVRejectCompletePopUp status >>>>"+status);
  kony.print("populateUVRejectCompletePopUp gblUVRejectedReason >>>>"+gblUVRejectedReason);
  	frmUVApprovalCompletePopUpNew.btnDone.setVisibility(false);
    frmUVApprovalCompletePopUpNew.btnGoToAcctSum.setVisibility(false);
    if(status == 1){	// 10. Failed Reject Approval
      frmUVApprovalCompletePopUpNew.imgStatus.src = "uv_error.gif";
      frmUVApprovalCompletePopUpNew.lblTitle.text = kony.i18n.getLocalizedString("Result_txtTitle101");
      frmUVApprovalCompletePopUpNew.lblDesc.text = kony.i18n.getLocalizedString("Result_txtDes102");
      frmUVApprovalCompletePopUpNew.btnDone.setVisibility(true);
      frmUVApprovalCompletePopUpNew.btnDone.text = kony.i18n.getLocalizedString("Result_txtDes23");
      frmUVApprovalCompletePopUpNew.btnDone.onClick = gotoLoginTransactionDetails;
      frmUVApprovalCompletePopUpNew.btnGoToAcctSum.setVisibility(true);
      frmUVApprovalCompletePopUpNew.btnGoToAcctSum.text = kony.i18n.getLocalizedString("Result_urlTxn14");
      frmUVApprovalCompletePopUpNew.btnGoToAcctSum.onClick = onClickUVApprovalGoToAccountSummary;
    }else{	// Success Reject 
      frmUVApprovalCompletePopUpNew.imgStatus.src = "uv_cancelrequest.gif";
      frmUVApprovalCompletePopUpNew.lblTitle.text = kony.i18n.getLocalizedString("Result_txtTitle71");
      if(gblUVRejectedReason === 0){
        frmUVApprovalCompletePopUpNew.lblDesc.text = kony.i18n.getLocalizedString("Result_txtDes72");
        frmUVApprovalCompletePopUpNew.btnDone.setVisibility(true);
        frmUVApprovalCompletePopUpNew.btnDone.text = kony.i18n.getLocalizedString("Result_urlTxn24");
        frmUVApprovalCompletePopUpNew.btnDone.onClick = onClickOnTMBCall1558;
        frmUVApprovalCompletePopUpNew.btnGoToAcctSum.setVisibility(true);
        frmUVApprovalCompletePopUpNew.btnGoToAcctSum.text = kony.i18n.getLocalizedString("Result_urlTxn14");
        frmUVApprovalCompletePopUpNew.btnGoToAcctSum.onClick = onClickUVApprovalGoToAccountSummary;
      }else if(gblUVRejectedReason == 1){
        frmUVApprovalCompletePopUpNew.lblDesc.text = kony.i18n.getLocalizedString("Result_txtDes92");
        frmUVApprovalCompletePopUpNew.btnDone.setVisibility(true);
        frmUVApprovalCompletePopUpNew.btnDone.text = kony.i18n.getLocalizedString("Result_btnTxn13");
        frmUVApprovalCompletePopUpNew.btnDone.onClick = gotoTMBConfirmOrExit;
        frmUVApprovalCompletePopUpNew.btnGoToAcctSum.setVisibility(true);
        frmUVApprovalCompletePopUpNew.btnGoToAcctSum.text = kony.i18n.getLocalizedString("Result_urlTxn24");
        frmUVApprovalCompletePopUpNew.btnGoToAcctSum.onClick = onClickOnTMBCall1558;
      }else if(gblUVRejectedReason == 2){
        frmUVApprovalCompletePopUpNew.lblDesc.text = kony.i18n.getLocalizedString("Result_txtDes92");
        frmUVApprovalCompletePopUpNew.btnDone.setVisibility(true);
        frmUVApprovalCompletePopUpNew.btnDone.text = kony.i18n.getLocalizedString("Result_urlTxn24");
        frmUVApprovalCompletePopUpNew.btnDone.onClick = onClickOnTMBCall1558;
        frmUVApprovalCompletePopUpNew.btnGoToAcctSum.setVisibility(true);
        frmUVApprovalCompletePopUpNew.btnGoToAcctSum.text = kony.i18n.getLocalizedString("Result_urlTxn14");
        frmUVApprovalCompletePopUpNew.btnGoToAcctSum.onClick = onClickUVApprovalGoToAccountSummary;
      }
      //frmUVApprovalCompletePopUpNew.btnClose.onClick = ; // minimize app
      dismissLoadingScreen();
    }
    frmUVApprovalCompletePopUpNew.show();
  }catch(e){
    kony.print("Exception ##33434 : "+e);
  }
}

/**
 * @function
 *
 */
function onClickOnConfirmUVApprovalTransDetails(){
  resetKeypadApproval();
  //showAccessPinScreenKeypad();
  invokeUVActivityLogging("uvApprovalActivityLogging", getEmptyValIfNoPropInJsonObj(gblUVTransactionData, "transactionID"), "");
  
  //MIB-9799 && MIB-9498
  checkUVMBStatusBeforeShowAccessPin();
  
 /*  popUpAccesspinPwd.destroy();
  popUpAccesspinPwd.show();
  popUpAccesspinPwd.txtAccessPin.setFocus(true);	
  accessPinShowPopup("");
  */
  /* frmUVApprovalMB.flexKeyBoard.animate(
			        kony.ui.createAnimation({
			            "100": {
			                "top": "60%",
			                "stepConfig": {
			                    "timingFunction": kony.anim.EASE
			                }
			            }
			        }), {
			            "delay": 0,
			            "iterationCount": 1,
			            "fillMode": kony.anim.FILL_MODE_FORWARDS,
			            "duration": 0.5
			        }
			    );
  frmUVApprovalMB.flexKeyBoard.isVisible = true;*/
  //frmUVApprovalMB.flexFullBody.top = "60%" 
  	
}

/**
 * @function
 *
 * @param status 
 */
function populateUVApprovalCompleteScreen(status){
   kony.print("populateUVApprovalCompleteScreen status >>>>"+status);
  	frmUVApprovalCompleteNew.btnDone.setVisibility(false);
  	frmUVApprovalCompleteNew.btnGoToAcctSum.setVisibility(false);
  	dismissLoadingScreen();
  	if(status == "0"){ // success
        if(frmUVApprovalMB.flxSectionE.isVisible){	//	3. Success Approval Non-Financial Transaction
          frmUVApprovalCompleteNew.imgStatus.src = "uv_done.gif";
        }else{													// 1. Success Approval Financial Transaction
          frmUVApprovalCompleteNew.imgStatus.src = "uv_transactiondone.gif";
        }
        frmUVApprovalCompleteNew.lblTitle.text = kony.i18n.getLocalizedString("Result_txtTitle11");
        frmUVApprovalCompleteNew.lblDesc.text = "";
        frmUVApprovalCompleteNew.btnDone.setVisibility(true);
        frmUVApprovalCompleteNew.btnDone.text = kony.i18n.getLocalizedString("Result_btnTxn13");
        frmUVApprovalCompleteNew.btnDone.onClick = onClickTMBConfirm;
        frmUVApprovalCompleteNew.btnGoToAcctSum.setVisibility(true);
        frmUVApprovalCompleteNew.btnGoToAcctSum.text = kony.i18n.getLocalizedString("Result_urlTxn14");
        frmUVApprovalCompleteNew.btnGoToAcctSum.onClick = onClickUVApprovalGoToAccountSummary;
      	frmUVApprovalCompleteNew.lblScreenTitle.text = kony.i18n.getLocalizedString("UVR_txtTitle");
    	frmUVApprovalCompleteNew.show();
    }else if(status == "1"){ // Failed
        if(frmUVTransReqHistoryDetails.flxSectionE.isVisible){ 	// 4. Failed Approval Non-Financial Transaction
          frmUVApprovalCompleteNew.imgStatus.src = "uv_error.gif";
          frmUVApprovalCompleteNew.lblTitle.text = kony.i18n.getLocalizedString("Result_txtTitle21");
          frmUVApprovalCompleteNew.lblDesc.text = kony.i18n.getLocalizedString("Result_txtDes22");
        }else{													// 2. Failed Approval Financial Transaction
          frmUVApprovalCompleteNew.imgStatus.src = "uv_transactionfailed.gif";
          frmUVApprovalCompleteNew.lblTitle.text = kony.i18n.getLocalizedString("Result_txtTitle21");
          frmUVApprovalCompleteNew.lblDesc.text = kony.i18n.getLocalizedString("Result_txtDes22");
          frmUVApprovalCompleteNew.btnGoToAcctSum.setVisibility(true);
          frmUVApprovalCompleteNew.btnGoToAcctSum.text = kony.i18n.getLocalizedString("Result_urlTxn24");
          frmUVApprovalCompleteNew.btnGoToAcctSum.onClick = onClickOnTMBCall1558;
        }
        frmUVApprovalCompleteNew.btnDone.setVisibility(true);
        frmUVApprovalCompleteNew.btnDone.text = kony.i18n.getLocalizedString("Result_urlTxn14");
        frmUVApprovalCompleteNew.btnDone.onClick = onClickUVApprovalGoToAccountSummary;
      	frmUVApprovalCompleteNew.lblScreenTitle.text = kony.i18n.getLocalizedString("UVR_txtTitle");
    	frmUVApprovalCompleteNew.show();
    }else if(status == "3"){ // 6. PIN LOCKED
        gotoUVPINLockedScreenPopUp();
    }
}

/**
 * @function
 *
 * @param status 
 */
function populateUVApprovalCompletePopUp(status){
  kony.print("populateUVApprovalCompletePopUp status >>>>"+status);
  	frmUVApprovalCompletePopUpNew.btnDone.setVisibility(false);
  	frmUVApprovalCompletePopUpNew.btnGoToAcctSum.setVisibility(false);
  	dismissLoadingScreen();
  	if(status == "0"){ // success
        if(frmUVApprovalMB.flxSectionE.isVisible){	//	3. Success Approval Non-Financial Transaction
          frmUVApprovalCompletePopUpNew.imgStatus.src = "uv_done.gif";
        }else{													// 1. Success Approval Financial Transaction
          frmUVApprovalCompletePopUpNew.imgStatus.src = "uv_transactiondone.gif";
        }
        frmUVApprovalCompletePopUpNew.lblTitle.text = kony.i18n.getLocalizedString("Result_txtTitle11");
        frmUVApprovalCompletePopUpNew.lblDesc.text = "";
        frmUVApprovalCompletePopUpNew.btnDone.setVisibility(true);
        frmUVApprovalCompletePopUpNew.btnDone.text = kony.i18n.getLocalizedString("Result_btnTxn13");
        frmUVApprovalCompletePopUpNew.btnDone.onClick = gotoTMBConfirmOrExit;
        frmUVApprovalCompletePopUpNew.btnGoToAcctSum.setVisibility(true);
        frmUVApprovalCompletePopUpNew.btnGoToAcctSum.text = kony.i18n.getLocalizedString("Result_urlTxn14");
        frmUVApprovalCompletePopUpNew.btnGoToAcctSum.onClick = onClickUVApprovalGoToAccountSummary;
      	frmUVApprovalCompletePopUpNew.show();
    }else if(status == "1"){ // Failed
        if(frmUVApprovalMB.flxSectionE.isVisible){ 	// 4. Failed Approval Non-Financial Transaction
          frmUVApprovalCompletePopUpNew.imgStatus.src = "uv_error.gif";
          frmUVApprovalCompletePopUpNew.lblTitle.text = kony.i18n.getLocalizedString("Result_txtTitle21");
          frmUVApprovalCompletePopUpNew.lblDesc.text = kony.i18n.getLocalizedString("Result_txtDes22");
        }else{													// 2. Failed Approval Financial Transaction
          frmUVApprovalCompletePopUpNew.imgStatus.src = "uv_transactionfailed.gif";
          frmUVApprovalCompletePopUpNew.lblTitle.text = kony.i18n.getLocalizedString("Result_txtTitle21");
          frmUVApprovalCompletePopUpNew.lblDesc.text = kony.i18n.getLocalizedString("Result_txtDes22");
          frmUVApprovalCompletePopUpNew.btnGoToAcctSum.setVisibility(true);
          frmUVApprovalCompletePopUpNew.btnGoToAcctSum.text = kony.i18n.getLocalizedString("Result_urlTxn24");
          frmUVApprovalCompletePopUpNew.btnGoToAcctSum.onClick = onClickOnTMBCall1558;
        }
        frmUVApprovalCompletePopUpNew.btnDone.setVisibility(true);
        frmUVApprovalCompletePopUpNew.btnDone.text = kony.i18n.getLocalizedString("Result_txtDes23");
        frmUVApprovalCompletePopUpNew.btnDone.onClick = gotoLoginTransactionDetails;
      	frmUVApprovalCompletePopUpNew.show();
    }else if(status == "3"){ // 6. PIN LOCKED
      	gotoUVPINLockedScreenPopUp();
    }
    //frmUVApprovalCompletePopUpNew.btnClose.onClick = ; // minimize app
}

function gotoUVPINLockedScreenPopUp(){
    kony.store.setItem("isUserStatusActive",false);
    invokeLogoutService();
  	frmUVApprovalCompletePopUpNew.imgStatus.src = "uv_pinlock.gif";
  	frmUVApprovalCompletePopUpNew.lblTitle.text = kony.i18n.getLocalizedString("Result_txtTitle61");
  	frmUVApprovalCompletePopUpNew.lblDesc.text = "";
  	frmUVApprovalCompletePopUpNew.btnDone.setVisibility(true);
  	frmUVApprovalCompletePopUpNew.btnDone.text = kony.i18n.getLocalizedString("Result_btnTxn63");
  	frmUVApprovalCompletePopUpNew.btnDone.onClick = gotoReActivationScreenForUV;
  	frmUVApprovalCompletePopUpNew.btnGoToAcctSum.setVisibility(true);
  	frmUVApprovalCompletePopUpNew.btnGoToAcctSum.text = kony.i18n.getLocalizedString("Result_urlTxn24");
  	frmUVApprovalCompletePopUpNew.btnGoToAcctSum.onClick = onClickOnTMBCall1558;
  	frmUVApprovalCompletePopUpNew.show();
}

function gotoLoginTransactionDetails(){ // Log In > The transaction detail 
  if(isSignedUser) {
    	onClickUVApprovalGoToAccountSummary();
  } else {
    	gotoLoginOrActivation("");
  }
}

/**
 * @function
 *
 */
function gotoToUVApprovalTimesUpScreen(){
  	 if(isSignedUser){
      	frmUVApprovalCompleteNew.lblScreenTitle.text = kony.i18n.getLocalizedString("UVR_txtTitle");
        frmUVApprovalCompleteNew.imgStatus.src = "uv_timeup.gif";
        frmUVApprovalCompleteNew.lblTitle.text = kony.i18n.getLocalizedString("Result_txtTitle51");
        frmUVApprovalCompleteNew.lblDesc.text = kony.i18n.getLocalizedString("Result_txtDes22");
        frmUVApprovalCompleteNew.btnDone.setVisibility(false);
        frmUVApprovalCompleteNew.btnGoToAcctSum.setVisibility(true);
        frmUVApprovalCompleteNew.btnGoToAcctSum.text = kony.i18n.getLocalizedString("Result_urlTxn14");
        frmUVApprovalCompleteNew.btnGoToAcctSum.onClick = onClickUVApprovalGoToAccountSummary;
        frmUVApprovalCompleteNew.show();
    }else{
        frmUVApprovalCompletePopUpNew.imgStatus.src = "uv_timeup.gif";
        frmUVApprovalCompletePopUpNew.lblTitle.text = kony.i18n.getLocalizedString("Result_txtTitle51");
        frmUVApprovalCompletePopUpNew.lblDesc.text = kony.i18n.getLocalizedString("Result_txtDes22");
        frmUVApprovalCompletePopUpNew.btnDone.setVisibility(false);
        frmUVApprovalCompletePopUpNew.btnGoToAcctSum.setVisibility(true);
        frmUVApprovalCompletePopUpNew.btnGoToAcctSum.text = kony.i18n.getLocalizedString("Result_urlTxn14");
        frmUVApprovalCompletePopUpNew.btnGoToAcctSum.onClick = onClickUVApprovalGoToAccountSummary;
        frmUVApprovalCompletePopUpNew.show();
    }
  invokeUVActivityLogging("uvAuthActivityLogging", getEmptyValIfNoPropInJsonObj(gblUVTransactionData, "transactionID"), "4");
}

function gotoReActivationScreenForUV(){
  	gotoLoginOrActivation("true");
}

function onClickBackfrmUVApprovalMB(){
   	onClickTMBConfirm();
}


function onClickUVApprovalGoToAccountSummary(){
  if(isSignedUser) {
    	callCustomerAccountService();
  } else {
    	gotoLoginOrActivation("");
  }
   
}


function gotoTMBConfirmOrExit(){
  if(isSignedUser) {
  		onClickTMBConfirm();
  } else {
    	closeApplicationBackBtn();
  }
}

function gotoLoginOrActivation(isReactivate) {
  	if(isNotBlank(isReactivate)) {
      	invokeUVLogOut();
      	onClickForgotPin();
    } else {
  		var getEncrKeyFromDevice = kony.store.getItem("encrytedText");
		if (getEncrKeyFromDevice != null) {
			resetValues();
			frmMBPreLoginAccessesPin.show();
		} else {
            invokeUVLogOut();
			frmMBanking.show();
		}
    }
}

function navigateTofrmUVRequestHistory(){
    onClickTMBConfirm();
}

function onClickBackfrmUVTransReqHistoryDetails(){
  	onClickTMBConfirm();
}


function navigateTofrmUVTransReqHistoryDetails(){  
  	populateUVTransactionHistoryDetails();
}


function onClickTransRejectRequest(reason){
  	if(reason == 3){
      	frmUVTransRejectMenu.btnReason3.skin = "btnBlueTransBlueFont200";
      	frmUVTransRejectMenu.txtAreaReason3.text = "";
      	frmUVTransRejectMenu.flxOtherReason.setVisibility(true);
      	frmUVTransRejectMenu.btnSubmit.setVisibility(true);
    }else{
      	frmUVTransRejectMenu.btnReason3.focusSkin = "btnBlueTransBlueFont200";
    	frmUVTransRejectMenu.btnReason3.skin = "btnBlue200";
      	frmUVTransRejectMenu.txtAreaReason3.text = "";
      	frmUVTransRejectMenu.flxOtherReason.setVisibility(false);
      	frmUVTransRejectMenu.btnSubmit.setVisibility(false);
      	onClickOnRejectUVApprovalTransDetails()
    }
}

function onClickSubmitUVRejectReason3(){
  	if(gblUVRejectedReason == 2 && !isNotBlank(frmUVTransRejectMenu.txtAreaReason3.text)){
      	showAlert(kony.i18n.getLocalizedString("Reject_txtOther"), kony.i18n.getLocalizedString("info"));
    }else{
      	onClickOnRejectUVApprovalTransDetails();
    }
}

/**
 * @function
 *
 */
function onClickfrmUVTransRejectMenu(){
  	gblUVRejectedReason = -1;
  	frmUVTransRejectMenu.flxOtherReason.setVisibility(false);
    frmUVTransRejectMenu.btnSubmit.setVisibility(false);
  	frmUVTransRejectMenu.btnReason1.focusSkin = "btnBlueTransBlueFont200";
    frmUVTransRejectMenu.btnReason1.skin = "btnBlue200";
  	frmUVTransRejectMenu.btnReason2.focusSkin = "btnBlueTransBlueFont200";
    frmUVTransRejectMenu.btnReason2.skin = "btnBlue200";
  	frmUVTransRejectMenu.btnReason3.focusSkin = "btnBlueTransBlueFont200";
    frmUVTransRejectMenu.btnReason3.skin = "btnBlue200";
  	frmUVTransRejectMenu.lblUVCancelRequestTitle.text = kony.i18n.getLocalizedString("Reject_txtTitle");
  	frmUVTransRejectMenu.lblHelp.text = kony.i18n.getLocalizedString("Reject_txt01");
  	frmUVTransRejectMenu.lblWhathappen.text = kony.i18n.getLocalizedString("Reject_txt02");
  	frmUVTransRejectMenu.btnReason1.text = kony.i18n.getLocalizedString("Reject_txtFlex01");
  	frmUVTransRejectMenu.btnReason2.text = kony.i18n.getLocalizedString("Reject_txtFlex02");
  	frmUVTransRejectMenu.btnReason3.text = kony.i18n.getLocalizedString("Reject_txtFlex03");
  	frmUVTransRejectMenu.txtAreaReason3.placeholder = kony.i18n.getLocalizedString("Reject_txtText");
  	frmUVTransRejectMenu.txtAreaReason3.text = "";
  	frmUVTransRejectMenu.btnSubmit.text = kony.i18n.getLocalizedString("MF_ST_Btn_Sumit");
  	frmUVTransRejectMenu.show();
}


function onClickOfTMBID() {
  //4. Setting up VTAP SDK
  loadFunctionalModuleSync("cardlessModule");
  setUpVTapSDK();  
  onClickTMBConfirm();
}

function onClickTMBConfirm(){
  if(isSignedUser) {
  		if(gblUVregisterMB == "Y"){
    		callUVPushTxnHistory();
    	 }else{
    		callUVMaintInqService(callBackUVMaintInqServiceForTMBConfirmList);
  		}
    } else {
      	gotoLoginOrActivation("")
    }
}

function callUVPushTxnHistory() {
		showLoadingScreen();
         var inputParams = {
           	historyType : "PUSHNOTIFICATION"
         };
   		 kony.print("uvTransactionHistory call service--->" + JSON.stringify(inputParams));
    	invokeServiceSecureAsync("uvTransactionHistory", inputParams, getUVRequestsCallBack);
}

/**
 * @function
 *
 * @param status 
 * @param callBackResponse 
 */
function getUVRequestsCallBack(status, callBackResponse){
  kony.print("getUVRequestsCallBack callBackResponse--->" + JSON.stringify(callBackResponse));     
  if (status == 400) {
		if (callBackResponse["opstatus"] == "0") {    
          var source;
          var tempSrc;
          var requestType;
          var segDataActive = [];
          var segDataHistory = [];
          var imgLogo;
          var transDesc;
          var historyGroupDate;
          var locale = kony.i18n.getCurrentLocale();
   		  var remainingTime = "";
          for(var i = 0; i < callBackResponse["Result"].length;i++){
              source = callBackResponse["Result"][i]["CHANNEL"];             
              requestType = callBackResponse["Result"][i]["STATUS"];
               if (kony.string.startsWith(locale, "en", true) == true) {
					transDesc = callBackResponse["Result"][i]["TXN_DESC_EN"];
                 	remainingTime = callBackResponse["Result"][i]["REMAINING_TIME_EN"];
              } else if (kony.string.startsWith(locale, "th", true) == true) {
					transDesc = callBackResponse["Result"][i]["TXN_DESC_TH"];
                	remainingTime = callBackResponse["Result"][i]["REMAINING_TIME_TH"];
              }
             if(requestType == "0"){ //Active Records
                 tempSrc = getUVChannelText(source);
                 imgLogo = getChannelIconUVTransDetails(source, "blue");
                // if(source == "999" || source == "04"){ // Filtering only ATM requests, get Confirmation once again
                       tempRespDetails =  {
                        imgLogo: imgLogo,
                        lblReqSource:{
                          				text:tempSrc,
                          				skin:"lblbluemedium"
                                     },
                        lblSourceAccount:transDesc,                   
                        lblTime:callBackResponse["Result"][i]["TIME_ONLY"],                 
                        imgTime:"timerunning.png", //TO-DO --  Need to get account Name from service 
                        lblSourceTime:remainingTime,                      
                        "TRANSACTION_REF_ID": callBackResponse["Result"][i]["TRANSACTION_REF_ID"],  
                        "STATUS" :  requestType,  
                        "APPROVAL" :  callBackResponse["Result"][i]["APPROVAL"],
                    	"terminal_id":callBackResponse["Result"][i]["TERMINAL_ID"], 
                         "terminal_location":callBackResponse["Result"][i]["TERMINAL_LOCATION"], 
                         "account_number":callBackResponse["Result"][i]["ACCOUNT_NUMBER"], 
                         "account_name":callBackResponse["Result"][i]["ACCOUNT_NAME"], 
                         "transaction_type":callBackResponse["Result"][i]["TRANSACTION_TYPE"], 
                         "amount":callBackResponse["Result"][i]["AMOUNT"], 
                         "server_date":callBackResponse["Result"][i]["CREATED_DATE"], 
                         "channel":source, 
                        template: flexSourceDtls                 
                    };
                    segDataActive.push(tempRespDetails);   
               //   }
                          	
             }else { //History Records
               	tempSrc = getUVChannelText(source);
                imgLogo = getChannelIconUVTransDetails(source, "grey");
               	// if(source == "999" || source == "04"){ // Filtering only ATM requests, get Confirmation once again
               	var imgSatusIcon = getStatusIconForHistory(requestType, callBackResponse["Result"][i]["APPROVAL"]);
              	var lblStatusText = getStatusTextForHistory(requestType, callBackResponse["Result"][i]["APPROVAL"]);
               
                  if(callBackResponse["Result"][i]["GROUP_BY_DATE"] == "Y") {
                           segDataHistory.push({
                              lblDate:{
                                  text : callBackResponse["Result"][i]["SERVER_DATE"],
                                  contentAlignment : constants.CONTENT_ALIGN_MIDDLE_RIGHT
                                },
                              caterogeryFlag : "header",
                              template: flexActiveReq
                            });
                      }
               
               
                       tempHistoryDetails =  {
                        imgLogo: imgLogo,
                        lblReqSource:{
                          				text:tempSrc,
                          				skin:"lblBlackOZoneMedium142"
                                     },
                        lblSourceAccount:transDesc, 
                        lblTime:callBackResponse["Result"][i]["TIME_ONLY"], 
                        imgTime: imgSatusIcon,
                        lblSourceTime: lblStatusText,
                        "TRANSACTION_REF_ID": callBackResponse["Result"][i]["TRANSACTION_REF_ID"],
                        "STATUS" :  requestType,  
                        "APPROVAL" :  callBackResponse["Result"][i]["APPROVAL"],
                         "terminal_id":callBackResponse["Result"][i]["TERMINAL_ID"], 
                         "terminal_location":callBackResponse["Result"][i]["TERMINAL_LOCATION"], 
                         "account_number":callBackResponse["Result"][i]["ACCOUNT_NUMBER"], 
                         "account_name":callBackResponse["Result"][i]["ACCOUNT_NAME"], 
                         "transaction_type":callBackResponse["Result"][i]["TRANSACTION_TYPE"], 
                         "amount":callBackResponse["Result"][i]["AMOUNT"], 
                         "server_date":callBackResponse["Result"][i]["CREATED_DATE"], 
                         "channel":source, 
                       	template: flexSourceDtls                 
                    };
                	segDataHistory.push(tempHistoryDetails);
                  //}
             }
          } 
          // Active Request
          if(segDataActive.length > 0){
            	frmUVRequestHistoryNew.lblActiveRequestsHrd.text = kony.i18n.getLocalizedString("UVL_txtSectionB"); 
            	frmUVRequestHistoryNew.lbltoday.text = kony.i18n.getLocalizedString("keyToday");
            	frmUVRequestHistoryNew.flexActiveReq.setVisibility(true);
            	frmUVRequestHistoryNew.flexUnderActiveReq.setVisibility(true);
            	frmUVRequestHistoryNew.segActiveRequest.setVisibility(true);
            	frmUVRequestHistoryNew.segActiveRequest.removeAll();
            	frmUVRequestHistoryNew.segActiveRequest.setData(segDataActive);
          }else{
            	frmUVRequestHistoryNew.flexActiveReq.setVisibility(false);
            	frmUVRequestHistoryNew.flexUnderActiveReq.setVisibility(false);
            	frmUVRequestHistoryNew.segActiveRequest.setVisibility(false);
          }
          // Request Hisotry
          if(segDataHistory.length > 0){
            	var segData = [];
           
                segData = segData.concat(segDataHistory);
            	segData.push({
                  lblDate:{
                      text : kony.i18n.getLocalizedString("UVL_txtInform"),
                      contentAlignment : constants.CONTENT_ALIGN_CENTER
                  	},
                  caterogeryFlag : "info",
                  template: flexActiveReq
                });
            	frmUVRequestHistoryNew.lblHistoryReq.text = kony.i18n.getLocalizedString("UVL_txtSectionC");
            	frmUVRequestHistoryNew.segHistoryReq.removeAll();
            	frmUVRequestHistoryNew.segHistoryReq.setVisibility(true);
            	frmUVRequestHistoryNew.segHistoryReq.setData(segData);
          }else{
            	frmUVRequestHistoryNew.flexHistoryReq.setVisibility(false);
            	frmUVRequestHistoryNew.segHistoryReq.setVisibility(false);
          }
          kony.print("before frmUVRequestHistoryNew show()");
          dismissLoadingScreen();
          frmUVRequestHistoryNewLocalePreShow();
          frmUVRequestHistoryNew.show();    
          }else if (callBackResponse["errCode"] == "UV100") { 
            	dismissLoadingScreen();
            	frmUVConfirmHome.show();
          }else{
              	dismissLoadingScreen();
              	showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
          }
    }else{
        dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
    }
}


function resetDisplaySectionUVDetails(){
  	frmUVApprovalMB.flxSectionB.setVisibility(false);
  	frmUVApprovalMB.flxSectionC.setVisibility(false);
  	frmUVApprovalMB.flxSectionD.setVisibility(false);
  	frmUVApprovalMB.flxSectionE.setVisibility(false);
  	frmUVApprovalMB.flxSectionF.setVisibility(false);
  	frmUVApprovalMB.flxLine1.setVisibility(false);
  	frmUVApprovalMB.flxLine2.setVisibility(false);
}

function resetDisplaySectionUVHistoryDetails(){
  	frmUVTransReqHistoryDetails.flxSectionB.setVisibility(false);
  	frmUVTransReqHistoryDetails.flxSectionC.setVisibility(false);
  	frmUVTransReqHistoryDetails.flxSectionD.setVisibility(false);
  	frmUVTransReqHistoryDetails.flxSectionE.setVisibility(false);
  	frmUVTransReqHistoryDetails.flxSectionF.setVisibility(false);
  	frmUVTransReqHistoryDetails.flxLine1.setVisibility(false);
  	frmUVTransReqHistoryDetails.flxLine2.setVisibility(false);
}

function populateUVTransactionDetails(isActive){
  	var caterogeryFlag = "";
  	if(!isActive){
      	caterogeryFlag = frmUVRequestHistoryNew.segHistoryReq.selectedRowItems[0].caterogeryFlag;
    }
   	if(caterogeryFlag == "header" || caterogeryFlag == "info" ){
      	return;
	}else{
    	resetDisplaySectionUVDetails();
      	var selectedItem;
      	var selectedData;
      	if(isActive){
          	selectedItem = frmUVRequestHistoryNew.segActiveRequest.selectedIndex[1];
        	selectedData = frmUVRequestHistoryNew.segActiveRequest.data[selectedItem];
        }else{
          	selectedItem = frmUVRequestHistoryNew.segHistoryReq.selectedIndex[1];
        	selectedData = frmUVRequestHistoryNew.segHistoryReq.data[selectedItem];
        }
        kony.print("populateUVTransactionDetails selectedData-------->" + JSON.stringify(selectedData));

         if(selectedData["STATUS"] == "0"){
              var inputParameters = {};
              inputParameters.mode = "RS";
              inputParameters.transactionID  = selectedData.TRANSACTION_REF_ID;
              inputParameters.channel = selectedData.channel;
              invokeServiceSecureAsync("reSendUVPushnotification", inputParameters,callbackInvokeReSentUVPushnotification);
         }else{
              populateUVTransactionHistoryDetails();
        }
  	}
}


function setTimerToApprovePush(){ 
  if(gblRemainingSeconds < gblTimeToShowUVTimerInRed){ // TODO Configure in variable
    	frmUVApprovalMB.lblTimer.skin = "lblRed200pxDigitNew"
  }else{
    	frmUVApprovalMB.lblTimer.skin = "lblBlue200pxDigit"
  }
  if(gblRemainingSeconds >= 0){
	  	frmUVApprovalMB.lblTimer.text = secondsToHms(gblRemainingSeconds--);
  } else {
     kony.print("Timer Expired hence cancelling time gblUniqId-------->" + gblUniqId);
      if(gblUniqId != undefined && gblUniqId != 0){
        cancelScheduledTimer(gblUniqId);
      }
      if(kony.application.getCurrentForm() != null && kony.application.getCurrentForm() != undefined && (kony.application.getCurrentForm().id == "frmUVApprovalMB" || kony.application.getCurrentForm().id == "frmUVTransRejectMenu")) {
        	kony.print("Timer Expired hence navigating to TimesUp screen only from frmUVApprovalMB------->");
            gotoToUVApprovalTimesUpScreen();
      }
      	
   }
  
}

/**
 * @function
 *
 * @param msgToDisplayJson 
 */
function populateUVTransactionDetailsDisplay(msgToDisplayJson){
  if(msgToDisplayJson == undefined) {
    return;
  }
  	resetDisplaySectionUVDetails();
    kony.print("msgToDisplayJson-------->" + JSON.stringify(msgToDisplayJson));
  	gblRemainingSeconds = 180; //Default timeout
   	getRemainingTime(msgToDisplayJson.expected_expire);
  	var channel = msgToDisplayJson.channel;
  	var transaction_type = msgToDisplayJson.transaction_type;
  	
 	 kony.print("channel >>>"+channel+", transaction_type>>"+transaction_type);
  	kony.print("gblUVDetailsDisplaySectionB >>>"+gblUVDetailsDisplaySectionB);
  	kony.print("gblUVDetailsDisplaySectionC >>>"+gblUVDetailsDisplaySectionC);
  	kony.print("gblUVDetailsDisplaySectionD >>>"+gblUVDetailsDisplaySectionD);
  	kony.print("gblUVDetailsDisplaySectionE >>>"+gblUVDetailsDisplaySectionE);
  
  	gblUVTransactionData.transactionType = transaction_type;
  	gblUVTransactionData.transactionID = msgToDisplayJson.transactionRefID;
  	gblUVTransactionData.channelID = channel;
  
  	frmUVApprovalMB.lblTMBConfirmTitle.text = kony.i18n.getLocalizedString("UVI_Menu");
    frmUVApprovalMB.lblUVInstraction.text = kony.i18n.getLocalizedString("UV_txtInstruction");
    frmUVApprovalMB.lblChannelVal.text = getUVChannelText(channel);
	frmUVApprovalMB.lblCreateTimeVal.text = formatDateForUV(msgToDisplayJson.server_date);
  	if(gblUVDetailsDisplaySectionB.indexOf(transaction_type) != -1){
      	frmUVApprovalMB.flxSectionB.setVisibility(true);
      	frmUVApprovalMB.lblFromAcctNameVal.text = msgToDisplayJson.account_name;
  		frmUVApprovalMB.lblAcctNumberVal.text = formatAccountNo(removeHyphenIB(msgToDisplayJson.account_number));
    }
  	if(gblUVDetailsDisplaySectionC.indexOf(transaction_type) != -1){
      	frmUVApprovalMB.flxSectionC.setVisibility(true);
      	frmUVApprovalMB.flxLine1.setVisibility(true);
        frmUVApprovalMB.lblTransTypeVal.text = getUVTransactionTypeText(transaction_type);
      	frmUVApprovalMB.imgTransType.src = getTransTypeIconUVTransDetails(transaction_type, "grey");
      	frmUVApprovalMB.lblAmtVal.text = commaFormatted(parseFloat(msgToDisplayJson.amount).toFixed(2)) 
          + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
    }
  	if(gblUVDetailsDisplaySectionD.indexOf(transaction_type) != -1){
      	frmUVApprovalMB.flxSectionD.setVisibility(true);
      	frmUVApprovalMB.flxLine2.setVisibility(true);
      	frmUVApprovalMB.lblBillerNameVal.text = ""; // biller name/fund name
      	frmUVApprovalMB.lblAcctNoRefVal.text = ""; // ref 1 & ref 2
      	var bankCode = ""; // comp code
		frmUVApprovalMB.imgLogo.src = loadBankIcon(bankCode);
      	//frmUVApprovalMB.imgLogo.src = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort+ "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+bankCode+"&modIdentifier=BANKICON";
    }
  	if(gblUVDetailsDisplaySectionE.indexOf(transaction_type) != -1){
      	frmUVApprovalMB.flxSectionE.setVisibility(true);
      	frmUVApprovalMB.flxLine1.setVisibility(true);
      	if(kony.i18n.getCurrentLocale() == "en_US"){
            frmUVApprovalMB.lblDescription.text = msgToDisplayJson.section.section_E_EN;
        }else{
            frmUVApprovalMB.lblDescription.text = msgToDisplayJson.section.section_E_TH;
        }
    }
  	if(channel == "03" || channel == "04"){ // ATM or Branch
      	frmUVApprovalMB.flxSectionF.setVisibility(true);
      	frmUVApprovalMB.flxLine2.setVisibility(true);
      	frmUVApprovalMB.lblBranchATMNameVal.text = msgToDisplayJson.terminal_location;
      	//frmUVApprovalMB.lblAddressVal.text = msgToDisplayJson.terminal_location;
    }
  	frmUVApprovalMB.imgChannel.src = getChannelIconUVTransDetails(channel, "grey");
  	frmUVApprovalMB.btnApprove.text = kony.i18n.getLocalizedString("UV_btnApprove");
  	frmUVApprovalMB.btnHavingProblem.text = kony.i18n.getLocalizedString("UV_btnReject");
}

function populateUVTransactionHistoryDetails(){
  	resetDisplaySectionUVHistoryDetails();
  	var selectedItem = frmUVRequestHistoryNew.segHistoryReq.selectedIndex[1];
	var selectedData = frmUVRequestHistoryNew.segHistoryReq.data[selectedItem];
  	var channel = selectedData.channel;
  	var transaction_type = selectedData.transaction_type;
  	
 	kony.print("channel >>>"+channel+", transaction_type>>"+transaction_type);
  	kony.print("gblUVDetailsDisplaySectionB >>>"+gblUVDetailsDisplaySectionB);
  	kony.print("gblUVDetailsDisplaySectionC >>>"+gblUVDetailsDisplaySectionC);
  	kony.print("gblUVDetailsDisplaySectionD >>>"+gblUVDetailsDisplaySectionD);
  	kony.print("gblUVDetailsDisplaySectionE >>>"+gblUVDetailsDisplaySectionE);
  	
  	frmUVTransReqHistoryDetails.lblHdrTMBConfirmTitle.text = kony.i18n.getLocalizedString("UVI_Menu");
    frmUVTransReqHistoryDetails.lblChannelVal.text = getUVChannelText(channel);
	frmUVTransReqHistoryDetails.lblCreateTimeVal.text = formatDateForUV(selectedData.server_date);
  	if(gblUVDetailsDisplaySectionB.indexOf(transaction_type) != -1){
      	frmUVTransReqHistoryDetails.flxSectionB.setVisibility(true);
      	frmUVTransReqHistoryDetails.lblFromAcctNameVal.text = selectedData.account_name;
  		frmUVTransReqHistoryDetails.lblAcctNumberVal.text = formatAccountNo(removeHyphenIB(selectedData.account_number));
    }
  	if(gblUVDetailsDisplaySectionC.indexOf(transaction_type) != -1){
      	frmUVTransReqHistoryDetails.flxSectionC.setVisibility(true);
      	frmUVTransReqHistoryDetails.flxLine1.setVisibility(true);
      	frmUVTransReqHistoryDetails.lblTransTypeVal.text = getUVTransactionTypeText(transaction_type);
      	frmUVTransReqHistoryDetails.imgTransType.src = getTransTypeIconUVTransDetails(transaction_type, "grey");
      	frmUVTransReqHistoryDetails.lblAmtVal.text = commaFormatted(parseFloat(selectedData.amount).toFixed(2)) 
          + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
    }
  	if(gblUVDetailsDisplaySectionD.indexOf(transaction_type) != -1){
      	frmUVTransReqHistoryDetails.flxSectionD.setVisibility(true);
      	frmUVTransReqHistoryDetails.flxLine2.setVisibility(true);
      	frmUVTransReqHistoryDetails.lblBillerNameVal.text = ""; // biller name/fund name
      	frmUVTransReqHistoryDetails.lblAcctNoRefVal.text = ""; // ref 1 & ref 2
      	var bankCode = ""; // comp code
		frmUVTransReqHistoryDetails.imgLogo.src = loadBankIcon(bankCode);
      	//frmUVTransReqHistoryDetails.imgLogo.src = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort+ "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+bankCode+"&modIdentifier=BANKICON";
    }
  	if(gblUVDetailsDisplaySectionE.indexOf(transaction_type) != -1){
      	frmUVTransReqHistoryDetails.flxSectionE.setVisibility(true);
      	frmUVTransReqHistoryDetails.flxLine1.setVisibility(true);
        frmUVTransReqHistoryDetails.lblDescription.text = selectedData.lblSourceAccount;
    }
  	if(channel == "03" || channel == "04"){ // ATM or Branch
      	frmUVTransReqHistoryDetails.flxSectionF.setVisibility(true);
      	frmUVTransReqHistoryDetails.flxLine2.setVisibility(true);
      	frmUVTransReqHistoryDetails.lblBranchATMNameVal.text = selectedData.terminal_location;
      	//frmUVTransReqHistoryDetails.lblAddressVal.text = selectedData.terminal_location;
    }
  	frmUVTransReqHistoryDetails.imgChannel.src = getChannelIconUVTransDetails(channel, "grey");
  	frmUVTransReqHistoryDetails.lblReqStatus.text = kony.i18n.getLocalizedString("UVH_txtStatus");
  	frmUVTransReqHistoryDetails.imgStatus.src = getStatusIconForHistory(selectedData.STATUS, selectedData.APPROVAL);
  	frmUVTransReqHistoryDetails.lblStatusVal.text = getStatusTextForHistory(selectedData.STATUS, selectedData.APPROVAL);
    frmUVTransReqHistoryDetails.show();
}

function getUVChannelText(id){
  	if(id == "01"){
      	return kony.i18n.getLocalizedString("UV_Channel_IB");
    }else if(id == "03"){
      	return kony.i18n.getLocalizedString("UV_Channel_Branch");
    }else if(id == "04"){
      	return kony.i18n.getLocalizedString("UV_Channel_ATM");
    }else if(id == "15"){
      	return kony.i18n.getLocalizedString("UV_Channel_ContactCenter");
    }
}

function getUVTransactionTypeText(transID){
  	if(transID == "M15"){
      	return kony.i18n.getLocalizedString("UV_Trans_M015");
    }else{
      	return kony.i18n.getLocalizedString("UV_Trans_M035");
    }
}

function getChannelIconUVTransDetails(channel, color){
  	var channelIcon = "";
  	if(channel == "04"){
      	if(color == "blue") channelIcon = "icon_atm_blue.png";
      	else channelIcon = "icon_atm_grey.png";
    }else if(channel == "03"){
      	if(color == "blue") channelIcon = "icon_branch_blue.png";
      	else channelIcon = "icon_branch_grey.png";
    }else if(channel == "01" || channel == "02"){
      	if(color == "blue") channelIcon = "icon_internet_blue.png";
      	else channelIcon = "icon_internet_grey.png";
    } else if(channel == "15"){
      	if(color == "blue") channelIcon = "icon_contact_blue.png";
      	else channelIcon = "icon_contact_grey.png";
    }
  	return channelIcon;
}

function getTransTypeIconUVTransDetails(ID, color){
  	var transTypeIcon = "";
  	if(gblUVTransTypeTransfer.indexOf(ID) != -1){
      	if(color == "blue")	transTypeIcon = "icon_transfer_blue.png";
        else transTypeIcon = "icon_transfer_grey.png";
    }else if(gblUVTransTypeBillPay.indexOf(ID) != -1){
      	if(color == "blue")	transTypeIcon = "icon_billpay_grey.png";
        else transTypeIcon = "icon_billpay_grey.png";
    }else if(gblUVTransTypeTopUp.indexOf(ID) != -1){
      	if(color == "blue")	transTypeIcon = "icon_topup_blue.png";
        else transTypeIcon = "icon_topup_grey.png";
    }else if(gblUVTransTypeOpenAcct.indexOf(ID) != -1){
      	if(color == "blue")	transTypeIcon = "icon_openacct_blue.png";
        else transTypeIcon = "icon_openacct_grey.png";
    }else if(gblUVTransTypeWithdraw.indexOf(ID) != -1){
      	if(color == "blue")	transTypeIcon = "icon_withdraw_blue.png";
        else transTypeIcon = "icon_withdraw_grey.png";
    }else if(gblUVTransTypeProfile.indexOf(ID) != -1){
      	if(color == "blue")	transTypeIcon = "icon_profile_blue.png";
        else transTypeIcon = "icon_profile_grey.png";
    }else if(gblUVTransTypePromptPay.indexOf(ID) != -1){
      	if(color == "blue")	transTypeIcon = "icon_promptpay_blue.png";
        else transTypeIcon = "icon_promptpay_grey.png";
    }else if(gblUVTransTypeCardMangt.indexOf(ID) != -1){
      	if(color == "blue")	transTypeIcon = "icon_card_blue.png";
        else transTypeIcon = "icon_card_grey.png";
    }else if(gblUVTransTypeCheque.indexOf(ID) != -1){
      	if(color == "blue")	transTypeIcon = "icon_cheque_blue.png";
        else transTypeIcon = "icon_cheque_grey.png";
    }else if(gblUVTransTypeMutualFund.indexOf(ID) != -1){
      	if(color == "blue")	transTypeIcon = "icon_mf_blue.png";
        else transTypeIcon = "icon_mf_grey.png";
    }else if(gblUVTransTypeAuth.indexOf(ID) != -1){
      	if(color == "blue")	transTypeIcon = "icon_authen_blue.png";
        else transTypeIcon = "icon_authen_grey.png";
    }
  	return transTypeIcon;
}

function getStatusIconForHistory(status, approval){
  	//1) Approved 2) Rejected 3) Cancelled and 4) Expired
  	var statusIcon = "";
  	if(status == "1"){
      	if(approval == "0") statusIcon = "icon_done_green.png";
      	else  statusIcon = "icon_reject.png";
    }else if(status == "2"){
      	statusIcon = "icon_timeout.png";
    }else if(status == "3"){
      	statusIcon = "icon_reject.png";
    }else if(status == "4"){
      	statusIcon = "icon_reverse.png";
    }else if(status == "5"){
      	statusIcon = "icon_pending.png";
    }
  	return statusIcon;
}

function getStatusTextForHistory(status, approval){
  	//1) Approved 2) Rejected 3) Cancelled and 4) Expired
  	var statusTxt = "";
  	if(status == "1"){
      	if(approval == "0") statusTxt = kony.i18n.getLocalizedString("UV_Request_Status011");
      	else  statusTxt = kony.i18n.getLocalizedString("UV_Request_Status012");
    }else if(status == "2"){
      	statusTxt = kony.i18n.getLocalizedString("UV_Request_Status02");
    }else if(status == "3"){
      	if(approval == "0") statusTxt = kony.i18n.getLocalizedString("UV_Request_Status031");
      	else  statusTxt = kony.i18n.getLocalizedString("UV_Request_Status032");
    }else if(status == "4"){
      	statusTxt = kony.i18n.getLocalizedString("UV_Request_Status04");
    }else if(status == "5"){
      	statusTxt = kony.i18n.getLocalizedString("UV_Request_Status05");
    }
  	return statusTxt;
}

/**
 * @function
 *
 */
function frmUVRequestHistoryNewPreshow(){
  if (gblCallPrePost) {
		frmUVRequestHistoryNewLocalePreShow();
	}  
}

/**
 * @function
 *
 */
function frmUVRequestHistoryNewLocalePreShow(){  
  	frmUVRequestHistoryNew.lbMFSuitabilityTitle.text = kony.i18n.getLocalizedString("FIN02_txtTittle");
  	frmUVRequestHistoryNew.lblTmbConfDesc.text = kony.i18n.getLocalizedString("FIN02_txtTopic");
  	frmUVRequestHistoryNew.lblDesc1.text = kony.i18n.getLocalizedString("UVL_txtDesc");
  	//frmUVRequestHistoryNew.lblActiveRequestsHrd.text = kony.i18n.getLocalizedString("UVL_txtSectionB");
  	//frmUVRequestHistoryNew.lblDetailDateDesc.text = kony.i18n.getLocalizedString("UVL_txtInform");
   	//frmUVRequestHistoryNew.lbltoday.text = kony.i18n.getLocalizedString("keyToday");
}

function onClickOnTMBCall1558(){
  	TMBCallPopup.label506459299657124.text = kony.i18n.getLocalizedString("DBI03_Call1558");
  	TMBCallPopup.label506459299657124.setVisibility(true);
  	TMBCallPopup.button506459299657128.text = kony.i18n.getLocalizedString("keyCancelButton");
 	TMBCallPopup.button506459299657130.text = kony.i18n.getLocalizedString("FindTMB_Call");
  	TMBCallPopup.show();
}


function callbackInvokeReSentUVPushnotification(status, resultTable){
  if (status == 400) {
		if (resultTable.opstatus == 0) {
   				
          kony.print("UV Resend Push notification success "+JSON.stringify(resultTable));
        
        }
  } 
}



/**
 * @function
 *
 */
function onClickUVAttentionNext(){
  showLoadingScreen() ;
  var inputParam={} ;
  inputParam.deviceId = getDeviceID();
  inputParam.userDeviceMobile = mobileNumberFromUserDevice;
  inputParam.networkProvider = networkProvider;
  inputParam.userMobileNumberCheckOn = kony.i18n.getLocalizedString("userMobileNumberCheckOn");
  invokeServiceSecureAsync("deleteAllPreviousAssociations", inputParam, callBackonClickUVAttentionNext)
  
}

function callBackonClickUVAttentionNext(status, resultTable){

  if (status == 400) {
    if (resultTable.opstatus == 0) {
       //Provisioning Vtap/Activate UV
        if(checkKSIDandPushRegId()) {
          gblStartClickFromActivationMB = true;
          registerAppWithPushNotificationsInfrastructure();
        } else {
           gblStartClickFromActivationMB = true;
           setUpProvisioning();
        }
        
        dismissLoadingScreen();
        kony.print("UV deletion of all associations are successfully completed -- "+JSON.stringify(resultTable));
       // frmUVActivationSuccessful.show() ;	
    }else{
		 dismissLoadingScreen();
		if(gblActivationWithWifi != true){
			if( resultTable["errCode"] == "UV_msgMobileNotMatched"){
              alert(kony.i18n.getLocalizedString("UV_msgMobileNotMatched"));
              return false;
            }else{
              alert(kony.i18n.getLocalizedString("ECGenericError"));
              return false;
            }
		}else{
           //Provisioning Vtap/Activate UV
            if(checkKSIDandPushRegId()) {
              	gblStartClickFromActivationMB = true;
            	registerAppWithPushNotificationsInfrastructure();
          	} else {
               gblStartClickFromActivationMB = true;
             	setUpProvisioning();
        	}
    		// frmUVActivationSuccessful.show();
		}
    }

  }
} 
  

function invokeAccessPinVerification(pinValue) {
    var inputParam = {};
	showLoadingScreen();
	kony.print("invokeAccessPinVerification >>"+pinValue)
    inputParam["deviceId"] = getDeviceID();
    inputParam["uvChannel"] = getUVChannelText(gblUVTransactionData.channelID);
    inputParam["moduleKey"] = getUVTransactionTypeText(gblUVTransactionData.transactionType);
  	inputParam["transactionType"] = gblUVTransactionData.transactionType;
  	inputParam["uvChannelID"] = gblUVTransactionData.channelID;
   	inputParam["txnRefId"] = getEmptyValIfNoPropInJsonObj(gblUVTransactionData, "transactionID");
	inputParam["password"] =  encryptData(pinValue);
  	inputParam["serviceType"] = "loginWithAccessPinJavaService";

    invokeServiceSecureAsync("loginWithAccessPinJavaService", inputParam, callBackinvokeAccessPinVerification);
}

function callBackinvokeAccessPinVerification(status, resulttable){
    if (status == 400) {
        kony.print("Printing the result for loginWithAccessPinJavaService "+JSON.stringify(resulttable));
        if (resulttable["opstatus"] == 0) {
        
          if(resulttable["errCode"] == "E10020" || resulttable["errMsg"] == "Wrong Password" ){
              //Wrong password Entered 
            	resetKeypadApproval();
            	var badLoginCount = resulttable["badLoginCount"];
            	var incorrectPinText = kony.i18n.getLocalizedString("PIN_Incorrect");
            	incorrectPinText = incorrectPinText.replace("{rem_attempt}", gblTotalPinAttempts - badLoginCount);
            	frmUVApprovalMB.lblForgotPin.text = incorrectPinText;
            	frmUVApprovalMB.lblForgotPin.skin = "lblBlackMed150NewRed";
            	frmUVApprovalMB.lblForgotPin.onTouchEnd = doNothing;
                dismissLoadingScreen();
              
            }else if(resulttable["errCode"] == "E10403" || resulttable["errMsg"] == "Password Locked" ){
              // password Locked 
              closeApprovalKeypad();
              dismissLoadingScreen();
              gotoUVApprovalCompleteScreen("3");
              
            }else{
          		closeApprovalKeypad();
              //  popUpAccesspinPwd.dismiss();
                var dataToBeSigned = dataTobeAuthenticate.dataToBeSigned;
                kony.print("Inside showUVPushDetails dataToBeSigned>>> "+dataToBeSigned);
                var messageId = dataTobeAuthenticate.messageId;
                kony.print("Inside showUVPushDetails messageId>>> "+messageId);
                var authResult = setUpPKIAuthntication(messageId, dataToBeSigned, false);
                dismissLoadingScreen();
                if(authResult){
                    gotoUVApprovalCompleteScreen("0");
                  	invokeUVActivityLogging("uvAuthActivityLogging", getEmptyValIfNoPropInJsonObj(gblUVTransactionData, "transactionID"), "0");
                }else{
					gotoUVApprovalCompleteScreen("1");
                  	invokeUVActivityLogging("uvAuthActivityLogging", getEmptyValIfNoPropInJsonObj(gblUVTransactionData, "transactionID"), "1");
                }
          	}
          
        }else{
          	dismissLoadingScreen();
        	showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        }
    }
  
}
function gotoUVApprovalCompleteScreen(status){
  if(isSignedUser){
    populateUVApprovalCompleteScreen(status);
  }else{
    populateUVApprovalCompletePopUp(status);
  }
}

function gotoUVRejectCompleteScreen(status){
  if(isSignedUser){
    populateUVRejectCompleteScreen(status);
  }else{
    populateUVRejectCompletePopUp(status);
  }
}

/**
 * @function
 *
 * @param expectedExpire 
 */
function getExpiryTime(expectedExpire) {
  kony.print("expectedExpire >>> "+expectedExpire);
  var startDate = new Date();
  //#ifdef iphone
  expectedExpire=dateformatteriPhone(expectedExpire);
  //#endif
  var endDate = new Date(expectedExpire);
  return getTimeDiffInSeconds(startDate, endDate);
}


function getTimeDiffInSeconds(startDate, endDate) {
  kony.print("startDate >>> "+startDate+", endDate >>> "+endDate);
  var timeDiff = (endDate.getTime() - startDate.getTime()) / 1000;
  kony.print("timeDiff before floor >>> "+timeDiff);
  timeDiff = Math.floor(timeDiff);
  kony.print("timeDiff after floor >>> "+timeDiff);
  return timeDiff;
}



function getEmptyValIfNoPropInJsonObj(inputJsonObj, objProp) {
  	return inputJsonObj.hasOwnProperty(objProp) ? inputJsonObj[objProp] : "";
}



function frmUVApprovalMBInit(){
 frmUVApprovalMB.postShow = frmUVApprovalMBPostShow;
 frmUVApprovalMB.preShow = frmUVApprovalMBPreShow;
}


function frmUVApprovalMBPreShow(){
  kony.print("PUSHPRESHOW : gblUVPushContent : "+gblUVPushContent+", gblFromUVReject:"+gblFromUVReject);
  //Disable backbutton in preLogin screen. MIB-12005
  if(isSignedUser){
    frmUVApprovalMB.flxHeader.setVisibility(true);
    frmUVApprovalMB.btnBack.setVisibility(false);
    
    frmUVApprovalMB.flexBody.height = "70%";
  }else{
    frmUVApprovalMB.flxHeader.setVisibility(false);
    frmUVApprovalMB.btnBack.setVisibility(false);
    
    frmUVApprovalMB.flexBody.height = "78%";
  }
  if(gblUVPushContent != null && gblUVPushContent != undefined && !gblFromUVReject) {
          handleUVPushMessage(gblUVPushContent);
    }
}

function frmUVApprovalMBPostShow(){
 	addAccessPinKeypad(frmUVApprovalMB); 
  	if(gblRemainingSeconds < 0) {
      		kony.print("PostShow: Timer Expired hence navigating to TimesUp screen when you try to display frmUVApprovalMB screen");
           gotoToUVApprovalTimesUpScreen();
    }
}


function invokeUVActivityLogging(serviceType, txnRefId, txnResult) {
    var inputParam = {};
	inputParam["deviceId"] = getDeviceID();
    inputParam["txnResult"] = txnResult;
    inputParam["txnRefId"] = txnRefId;
	inputParam["serviceType"] = serviceType;
  	inputParam["transactionType"] = gblUVTransactionData.transactionType;
  	inputParam["uvChannelID"] = gblUVTransactionData.channelID;
   	
    invokeServiceSecureAsync("loginWithAccessPinJavaService", inputParam, callBackInvokeUVActivityLogging);
}

function callBackInvokeUVActivityLogging(status, resulttable){
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
		}
	}
}


function getRemainingTime(expectedExpire) {
	showLoadingScreen();
	var input_params={};
	input_params["uvExpectedExpiry"] = expectedExpire;
  	input_params["deviceId"] = getDeviceID();
	invokeServiceSecureAsync("GetServerDateTime", input_params, callbackGetRemainingTime)
} 
function callbackGetRemainingTime(status,resultTable){
	if(status==400){
		dismissLoadingScreen();
		if(resultTable["opstatus"]==0){
              gblRemainingSeconds = resultTable["uvRemainingTime"];
          	  
                if(isNotBlank(resultTable["timeToShowRedText"])) {
                      gblTimeToShowUVTimerInRed = resultTable["timeToShowRedText"];
                }
          
                  kony.print("gblRemainingSeconds-------->" + gblRemainingSeconds);
                 if(gblRemainingSeconds >= 0){
                      frmUVApprovalMB.lblTimer.text = secondsToHms(gblRemainingSeconds);
                         if(gblUniqId != undefined && gblUniqId != 0){
                           cancelScheduledTimer(gblUniqId);
                        }
                        var uniqueIdGen = Math.random().toString(36).substr(2, 9)+"";
                        gblUniqId = uniqueIdGen;
                        if(gblRemainingSeconds != 0){
                          var timer = kony.timer.schedule(uniqueIdGen, setTimerToApprovePush, 1, true);
                        }
                 } else {
                      if(kony.application.getCurrentForm() != null && kony.application.getCurrentForm() != undefined && (kony.application.getCurrentForm().id == "frmUVApprovalMB" || kony.application.getCurrentForm().id == "frmUVTransRejectMenu")) {
                   	  	kony.print("Timer Expired hence navigating to TimesUp screen when you try to display frmUVApprovalMB screen");
                      	gotoToUVApprovalTimesUpScreen();
                      }
                      return;
                        
                 }
		} else {
			kony.print("Get Remaining Seconds from Backend Failed");
		}
	}
}

function checkUVMBStatusBeforeShowAccessPin(){
  showLoadingScreen();
  var inputParams = {};
  invokeServiceSecureAsync("crmProfileInq", inputParams, checkUVMBStatusBeforeShowAccessPinCallback);
}

function checkUVMBStatusBeforeShowAccessPinCallback(status, resulttable){
  dismissLoadingScreen();
  if (status == 400 && resulttable["opstatus"] == 0) {
    var mbStatus = resulttable["mbFlowStatusIdRs"];
    if (mbStatus == "05") { //Access Locked
      gotoUVPINLockedScreenPopUp();
    }else if (mbStatus != "02" && mbStatus != "05") { //Not Access and Not Access Locked
      alrtMsg=kony.i18n.getLocalizedString("QRC_msgMBNotActive");
      showAlertErrorMBNotActive(alrtMsg);
    }else{
      showAccessPinScreenKeypad();
    }
  }else{
    showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
  }
}

function showAlertErrorMBNotActive(alrtMsg) {
  function customAlertHandler(response) {
    frmMBanking.show();
  }
  var customYesLabel = kony.i18n.getLocalizedString("keyOK");
  var customAlertTitle = kony.i18n.getLocalizedString("Receipent_alert_Error");
  var basicConf = {
    message: alrtMsg,
    alertType: constants.ALERT_TYPE_ERROR,
    alertTitle: customAlertTitle,
    yesLabel: customYesLabel,
    noLabel: "",
    alertHandler: customAlertHandler
  };
  var pspConf = {};
  var infoAlert = kony.ui.Alert(basicConf, pspConf);
  return;
}


function invokeUVLogOut(){
  
  	if(isSignedUser) {
          var  inputParam = {};
          isSignedUser = false;
          inputParam["deviceId"] = GBL_UNIQ_ID;
          inputParam["channelId"] = GLOBAL_MB_CHANNEL;
          inputParam["timeOut"] = GBL_Time_Out;
          var locale = kony.i18n.getCurrentLocale();

          if (locale == "en_US") {
            inputParam["languageCd"] = "EN";
          } else {
            inputParam["languageCd"] = "TH";
          }
          invokeServiceSecureAsync("logOutTMB", inputParam, callBackUVLogOutService);
  	}
}

function callBackUVLogOutService(status, resulttable) {
  if (status == 400) {
    if (resulttable["opstatus"] == 0) {
     	//LogOut success in UV flow
    }
  }
}
