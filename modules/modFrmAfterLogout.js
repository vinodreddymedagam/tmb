function onLogoutPopUpConfrm() {
    //Changes as per ENH_223
    // moving the code gblFlagMenu = "" from above function to here as SPA the touch events are effected
    gblFromLogout = true;
    gblFPLoginSvsCalled = false;
    gblLoginTab = "FP";
    frmMBPreLoginAccessesPin.show();
    gblFlagMenu = "";
    isSignedUser = false;
    var currentForm = kony.application.getCurrentForm();
    resetTransferRTPPush();
    gblDeeplinkExternalAccess = false;
    gblContentDeeplinkData = "";
    frmMenu.flexdynamicContainer.removeAll();    
    //changes for 30771 only for android
    //when user click logout, application should set time out to false
    GBL_Time_Out = "false";
    //#ifdef android
  	  invokeLogoutServicetimerforand();
  	//#endif
  	//#ifdef iphone
    invokeLogoutService();
  	//#endif
}

/**
 * Function to invoke the logout service call after timer
 *  no return values
 */

function invokeLogoutServicetimerforand()
{
  try {
    kony.timer.cancel("onlogouttimerand");
  } catch (e) {}
  kony.timer.schedule("onlogouttimerand", invokeLogoutService, 2, false)
}
/**
 * Function to invoke the logout service call
 *  no return values
 */

function invokeLogoutService() {
  //kony.application.showLoadingScreen(frmLoading, "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
  gblDeeplinkExternalAccess = false;
  gblContentDeeplinkData = "";
  resetTransferRTPPush();
  //showLoadingScreen();
  inputParam = {};
  var channelId = "";
  if (flowSpa) {
    channelId = "01";
  } else {
    channelId = "02";
  }

  if (GLOBAL_MB_CHANNEL != null && GLOBAL_MB_CHANNEL != "") {
    if (flowSpa) {
      channelId = GLOBAL_IB_CHANNEL;
    } else {
      channelId = GLOBAL_MB_CHANNEL;
    }
  }

  if (flowSpa) {
    inputParam["deviceId"] = "";
  } else {
    inputParam["deviceId"] = GBL_UNIQ_ID;
  }

  inputParam["channelId"] = channelId;
  inputParam["timeOut"] = GBL_Time_Out;
  var locale = kony.i18n.getCurrentLocale();

  if (locale == "en_US") {
    inputParam["languageCd"] = "EN";
  } else {
    inputParam["languageCd"] = "TH";
  }
  invokeServiceSecureAsync("logOutTMB", inputParam, callBackLogOutService)
}
/*****************************************************************
 *	Name    : getDeviceID
 *	Author  : Kony Solutions
 *	Purpose : To get the device ID
 ******************************************************************/

/*function getDeviceID() {
	var hardwareID = ""
	var deviceInfo = kony.os.deviceInfo();
	if (deviceInfo["name"] == "android") {
		hardwareID = deviceInfo["deviceid"];

	}
	if (deviceInfo["name"] == "iPhone") {
		hardwareID = deviceInfo["deviceid"];

	}
	return hardwareID
}*/
/**
 * call back to handle logout service responce.Usagetime is returned from logout service.
 *  no return values
 */

function callBackLogOutService(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            kony.print("logout success");
            kony.application.unregisterForIdleTimeout();
            var usageTime = resulttable["usageTime"];
        }
      	if(GLOBAL_EKYC_ENABLE_FLAG === "true" || GLOBAL_EKYC_ENABLE_FLAG == true){  
        	gblFromLogout = true;
        }else{
           gblTouchShow = false;
           clearGlobalVariables();
           spaFormGlobalsCall();
           cleanPreorPostForms();
           gblFromLogout = true;
         //kony.application.dismissLoadingScreen();
        }
    }
}



function resetMBPreloginaccesspinUI(){
    kony.print("%%%%%Calling for IOS issue");
    frmMBPreLoginAccessesPin.flexCrossmark.isVisible = false;
    frmMBPreLoginAccessesPin.flexHeader.isVisible = true;
    frmMBPreLoginAccessesPin.flexSwipeContainer.top = "0%"
    frmMBPreLoginAccessesPin.FlexContainer0b16eb3557e0043.isVisible = true
    frmMBPreLoginAccessesPin.imgQRHeaderLogo.isVisible = false; 
    frmMBPreLoginAccessesPin.LabelLoginForQR.isVisible = false; 
    frmMBPreLoginAccessesPin.FlexLine.isVisible = false; 
    frmMBPreLoginAccessesPin.FlexLoginContainer.top = "0dp";
}
