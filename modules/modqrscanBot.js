gblqrmobileno = "";
gblqrFormattedMobileNum = "";
gblqrAmnt = "";
gblqrToUsrName = "";
gblqrToUsrAccnt = "";
gblqrTransFee = "";
gblqrToAccnt = "";
gblqrToAccntBal = "";
gblqrFromAccntNum = "";
gblqrFromAccntNickName = "";
gblqrBankCode = "";
gblqrTransferAmnt = "";
gblqrflow="";
var qrid = "";
var qrsid = "";
var ibUserId = "";
var qrExpiryDate="";
botReadValue="";
gblref2Val="";

function QRLoginBanking() {
  var inputParam = {};
  inputParam["deviceId"] = getDeviceID();
  inputParam["deviceOS"] = getDeviceOS();
  inputParam["qrStartTime"] = qrExpiryDate;
  //alert("calling QRLoginScanBussiRulesChk...");
  invokeServiceSecureAsync("QRLoginScanBussiRulesChk", inputParam, QRLoginBankingCallBack);
}
function showQRPopupAllow(imgIcon, msgText, callBackOnConfirm, callBackCancel, btnAllowText, btnDontAllowText) {

  popAllowOrNot.lblTitle = "Error";
  popAllowOrNot.imgIcon.src = imgIcon;	
  popAllowOrNot.lblMsgText.text = msgText;
  popAllowOrNot.btnAllow.onClick = callBackOnConfirm;
  popAllowOrNot.btnDontAllow.onClick = callBackCancel;
  popAllowOrNot.btnDontAllow.text = btnDontAllowText;// kony.i18n.getLocalizedString("keyCancelButton");
  popAllowOrNot.btnAllow.text = btnAllowText;//kony.i18n.getLocalizedString("keyConfirm");
  popAllowOrNot.show();
  //	return true;
}

QRIBStatusActivateLINK_EN="https://www.tmbbank.com/howto/e-banking/register.php";

function hndlCancel(){
  popAllowOrNot.dismiss();
}

function hndlHowToAplly(){
  kony.application.openURL(QRIBStatusActivateLINK_EN);
}

function showQRAlertError(keyMsg) {
  var okk = kony.i18n.getLocalizedString("keyOK");
  var KeyTitle = kony.i18n.getLocalizedString("Receipent_alert_Error");
  //Defining basicConf parameter for alert
  var basicConf = {
    message: keyMsg,
    alertType: constants.ALERT_TYPE_ERROR,
    alertTitle: KeyTitle,
    yesLabel: okk,
    noLabel: "",
    alertHandler: handle2
  };
  //Defining pspConf parameter for alert
  var pspConf = {};
  //Alert definition
  var infoAlert = kony.ui.Alert(basicConf, pspConf);

  function handle2(response) {
    invokeqrerrortimer();
  }
  return;
}

function showQRHowToApplyAlert(keyMsg,HowToApplyLink) {
  //var okk = kony.i18n.getLocalizedString("keyOK");
  var KeyTitle = kony.i18n.getLocalizedString("Receipent_alert_Error");
  var yeslbl=kony.i18n.getLocalizedString("QR_HowToApply");
  kony.print("DOL yeslbl="+yeslbl);
  var nolbl = kony.i18n.getLocalizedString("keyCancelButton"); 
  //Defining basicConf parameter for alert
  var basicConf = {
    message: keyMsg,
    alertType: constants.ALERT_TYPE_CONFIRMATION,
    alertTitle:KeyTitle,
    yesLabel: yeslbl,
    noLabel: nolbl,
    alertHandler: handle2
  };
  //Defining pspConf parameter for alert
  var pspConf = {};
  //Alert definition
  var infoAlert = kony.ui.Alert(basicConf, pspConf);

  function handle2(response) {
    if(response){
      kony.application.openURL(HowToApplyLink);
    }else{
      invokeqrerrortimer();
      return;
    }
  }
  return;
}

function showQRAlertConfirm(keyMsg,HowToUnlockLink) {
  //var okk = kony.i18n.getLocalizedString("keyOK");
  var KeyTitle = kony.i18n.getLocalizedString("Receipent_alert_Error");
  var yesLabel=kony.i18n.getLocalizedString("QR_HowToUnlock");	
  var nolbl = kony.i18n.getLocalizedString("keyCancelButton"); 
  //Defining basicConf parameter for alert
  var basicConf = {
    message: keyMsg,
    alertType: constants.ALERT_TYPE_CONFIRMATION,
    alertTitle:KeyTitle,
    yesLabel: yesLabel,
    noLabel: nolbl,
    alertHandler: handle2
  };
  //Defining pspConf parameter for alert
  var pspConf = {};
  //Alert definition
  var infoAlert = kony.ui.Alert(basicConf, pspConf);

  function handle2(response) {
    if(response){
      kony.application.openURL(HowToUnlockLink);
    }else{
      invokeqrerrortimer();
      return;
    }
  }
  return;
}

function QRLoginBankingCallBack(status, callBackResponse) {
  if (status == 400) {
     kony.print("QRLOGINFLOW: opstatus="+callBackResponse["opstatus"]);
     kony.print("QRLOGINFLOW: errMessage="+callBackResponse["errMessage"]);
    if (callBackResponse["opstatus"] == 0) {
      var ibStatus=callBackResponse["ibUserStatusIdTr"];
      var mbStatus=callBackResponse["mbFlowStatusIdRs"];
      var uvStatus=callBackResponse["UVStatus"];
      var dupLogin=callBackResponse["dupLogin"];
      var qrExpired=callBackResponse["qrExpired"];
      var alrtMsg="";
      var yeslbl="";
      //alert("ibStatus= "+ibStatus+"mbStatus= "+mbStatus+" uvStatus="+uvStatus);
      if(qrExpired=="yes"){
        alrtMsg=kony.i18n.getLocalizedString("QR_msgExpired");
        showQRAlertError(alrtMsg);
      }else if (uvStatus != "A") {// Check UV status OK
        alrtMsg=kony.i18n.getLocalizedString("QR_msgInvalidMBStatus");
        showQRAlertError(alrtMsg);
      }else  if(ibStatus=="05"){// Check whether user is locked
        alrtMsg=kony.i18n.getLocalizedString("QRC_msgIBLocked");
        var HowToUnlockLink="https://www.tmbbank.com/howto/e-banking/unlock.php";  
        showQRAlertConfirm(alrtMsg,HowToUnlockLink);
      }else if(ibStatus !="02" && ibStatus !="05"){// Check whether user is not active
        alrtMsg=kony.i18n.getLocalizedString("QRC_msgIBNotActive");
        var HowToApplyLink="https://www.tmbbank.com/howto/e-banking/register.php";
        showQRHowToApplyAlert(alrtMsg,HowToApplyLink);
      }else if (dupLogin == "Y") {// Check UV status OK
        alrtMsg=kony.i18n.getLocalizedString("keyIBLoginError002");
        showQRAlertError(alrtMsg);
      }else{
        ibUserId=callBackResponse["ibUserId"] ;
        // alert("calling UVLoginQRScan ibUserId="+ibUserId+"qrsid="+qrsid+"qrid="+qrid);
        UVLoginQRScan(qrsid,qrid,ibUserId);	 
      }
    }else if(callBackResponse["opstatus"] == -1 && callBackResponse["errMessage"] === "Device Details Not Found"){
      showAlertDeviceIdTouchMessage(kony.i18n.getLocalizedString("deviceIdEnhancementUnlock"),kony.i18n.getLocalizedString("deviceIDUnlock"));
    }else{
      alrtMsg=kony.i18n.getLocalizedString("keyOpenActGenErr");
      showQRAlertError(alrtMsg);      
    }
  }
}

function UVLoginQRScan(qrsid, qrid, ibUserId) {
  var inputParam = {};
  inputParam["jsessionId"] = qrsid;
  inputParam["qrsid"] = qrsid;
  inputParam["qrflag"] = "N";
  inputParam["qrid"] = qrid;
  inputParam["ibUserId"] = ibUserId;
  inputParam["serverDate"] = qrExpiryDate;
  inputParam["expectExpire"] = qrExpiryDate;
  invokeServiceSecureAsync("loginQRScan", inputParam, UVLoginQRScanCallback);
}


function UVLoginQRScanCallback(status, callBackResponse) {
  // alert("UV CALL SUCCESS!.... CALLING QRCodeApprovalIB status=" + status + "opstatus=" + callBackResponse["opstatus"]);
  //QRCodeApprovalIB(qrsid, qrid, ibUserId);
  if (status == 400) {
    if (callBackResponse["opstatus"] == 0) {
      //alert("UV CALL SUCCESS!.... ");
      kony.print("QRLOGIN: UV CALL SUCCESS!....");
      loadFunctionalModuleSync("cardlessModule");
      //QRCodeApprovalIB(qrsid,qrid,ibUserId);	 
    }else{
      // alert("UV CALL FAILED!.... ");
      kony.print("QRLOGIN: UV CALL FAILED!....");
      alrtMsg=kony.i18n.getLocalizedString("keyOpenActGenErr");
      showQRAlertError(alrtMsg); 
    }
  }
}

function QRCodeApprovalIB(qrsid, qrid, ibUserId) {
  var inputParam = {};
  inputParam["qrsid"] = qrsid;
  inputParam["qrflag"] = "N";
  inputParam["qrid"] = qrid;
  inputParam["ibUserId"] = ibUserId;
  invokeServiceSecureAsync("QRCodeApproval", inputParam, QRCodeApprovalIBCallback);
}

function QRCodeApprovalIBCallback(status, callBackResponse) {
  if (status == 400) {
    if (callBackResponse["opstatus"] == 0) {
      alert("SUCCESS!");
    }
  }
}


function scanLoginQRCode() {
  gblFromAccountSummary = false;
  //#ifdef android
  androidScanLoginQrcode();
  //#endif
  //#ifdef iphone
  iphoneScanBarcodemer();
  //alert(kony.i18n.getLocalizedString("keyScanNotWkg"));
  //#endif
}

function androidScanLoginQrcode() {
  //Creates an object of class 'Scanner'
  var BarScanObject = new barcode.BarScan();
  //Invokes method 'scanbarcode' on the object
  BarScanObject.scanbarcode(androidScanLoginQrcodeCallBack);
}

var qrid = "";
var qrsid = "";

function androidScanLoginQrcodeCallBack(result, resultFormat) {
  //alert("Sanjay result is"+result);
  if (result == "Back") {
    alert("Unable to scan");
  } else {
    var botReadValue = result;
    var arrValue = botReadValue.split(",");
    qrid = arrValue[0];
    qrsid = arrValue[1];
    alert("botReadValue=" + botReadValue + " qrid=" + arrValue[0] + " qrsid=" + arrValue[1]);
    if (qrid != "" && qrsid != "") {
      QRLoginBanking();
    }
  }
}



function scanqrcode() {
  gblFromAccountSummary = false;
  //#ifdef android
  androidScanQrcode();
  //#endif
  //#ifdef iphone
  iphoneScanBarcodemer();
  //alert(kony.i18n.getLocalizedString("keyScanNotWkg"));
  //#endif
}

function callback_onSubmitasynciphoneJSClassqr(result) {
  androidBarcodeResultsMerchant(result, "");
}

function androidScanQrcode() {
  //Creates an object of class 'Scanner'
  var BarScanObject = new barcode.BarScan();
  //Invokes method 'scanbarcode' on the object
  BarScanObject.scanbarcode(androidBarcodeResultsMerchant);
}

function iphoneScanBarcodemer() {
  BarScanObject.scanbarcode(callback_onSubmitasynciphoneJSClassqr);
}


function checkPermissionsInQR() {
  //loadFunctionalModules("eDonationModule");
  //kony.modules.loadFunctionalModule("eDonationModule");
  loadFunctionalModuleSync("eDonationModule"); 
  gblLoginTab = "QR";
  
  if(!isSignedUser){
   try{
	  idleTimeOut = 5;
      //alert("In registerforTimeout");
      if (GLOBAL_KONY_IDLE_TIMEOUT != null && GLOBAL_KONY_IDLE_TIMEOUT != undefined) {

              idleTimeOut = kony.os.toNumber(GLOBAL_KONY_IDLE_TIMEOUT);
      }
      kony.application.registerForIdleTimeout(idleTimeOut, onIdleTimeOutQRCallBack);
	}
	catch(e){
	}
  }
  //#ifdef android
  //MIB-14053: Change FP to kony API
  kony.localAuthentication.cancelAuthentication();
  chkRunTimePermissionsForQR();
  //#endif
  //#ifdef iphone
  showQRScanningLanding();
  //#endif
}


function chkRunTimePermissionsForQR(){
   var options = {};
   var result = kony.application.checkPermission(kony.os.RESOURCE_CAMERA, options);	
   kony.print("chkRunTimePermissionsForQR result ###########  "+result.status);
   if (result.status == kony.application.PERMISSION_DENIED){
      kony.print("chkRunTimePermissionsForQR Permission denied  ");
	  if (result.canRequestPermission){
         kony.application.requestPermission(kony.os.RESOURCE_CAMERA, reqCameraPermissionQRCallback);
       }else{
         kony.print("IN FINAL CALLBACK PERMISSION IS UNAVAIALABLE");
         var messageErr = "App cannot proceed further unless the required permisssions are granted!" //Need to chk with customer on msg text
         alert(messageErr);
       }
   } else if (result.status == kony.application.PERMISSION_GRANTED) {
      kony.print("chkRunTimePermissionsForQR Permission granted  ");
      kony.print("Permission allowed.");
      var permission=kony.store.getItem("campermission");
      kony.print("IN FINAL CALLBACK PERMISSION IS "+permission);
      if(permission == null || permission == undefined ||permission == ""){
           invokeqrerrortimer();
           kony.store.setItem("campermission", 1);
       }else{
            showQRScanningLanding();
        }    
   } else if (result.status == kony.application.PERMISSION_RESTRICTED) {
      kony.print("chkRunTimePermissionsForQR Permission restricted ");
      kony.print("IN FINAL CALLBACK PERMISSION IS UNAVAIALABLE");
      var messageErr = "App cannot proceed further unless the required permisssions are granted!" //Need to chk with customer on msg text
      alert(messageErr);
   }

    function reqCameraPermissionQRCallback(response) {
        kony.print("reqCameraPermissionQRCallback "+result.status);
		if (response.status == kony.application.PERMISSION_GRANTED) {
			kony.print("reqCameraPermissionQRCallback-PERMISSION_GRANTED: " + JSON.stringify(response));
             kony.print("Permission allowed.");
             kony.print("IN FINAL CALLBACK PERMISSION IS AVAIALABLE");
             var permission=kony.store.getItem("campermission");
             kony.print("IN FINAL CALLBACK PERMISSION IS "+permission);
             if(permission == null || permission == undefined ||permission == ""){
                  invokeqrerrortimer();
                  kony.store.setItem("campermission", 1);
             }else{
                  invokeqrerrortimer();
              }    
        
		} else if (response.status == kony.application.PERMISSION_DENIED) {
			kony.print("reqCameraPermissionQRCallback-PERMISSION_DENIED: " + JSON.stringify(response));
			kony.print("IN FINAL CALLBACK PERMISSION IS UNAVAIALABLE");
            var messageErr = "App cannot proceed further unless the required permisssions are granted!" //Need to chk with customer on msg text
            alert(messageErr);
		}
	}
}

/*
function chkRunTimePermissionsForQR() {
  if (isAndroidM()) {
    kony.print(" CALLIING PERMISSIONS FFI....");
    var permissionsArr = [gblPermissionsJSONObj.CAMERA_GROUP];
    //Creates an object of class 'PermissionFFI'
    var RuntimePermissionsChkFFIObject = new MarshmallowPermissionChecks.RuntimePermissionsChkFFI();
    //Invokes method 'checkpermission' on the object
    RuntimePermissionsChkFFIObject.checkPermissions(permissionsArr, null, chkRunTimePermissionsForQRCallBack);
  } else {
    showQRScanningLanding();
  }
}

function chkRunTimePermissionsForQRCallBack(result) {
  if (result == "1") {
    kony.print("IN FINAL CALLBACK PERMISSION IS AVAIALABLE");
    var permission=kony.store.getItem("campermission");
    kony.print("IN FINAL CALLBACK PERMISSION IS "+permission);
    if(permission == null || permission == undefined ||permission == ""){
      invokeqrerrortimer();
      kony.store.setItem("campermission", 1);
    }else{
      invokeqrerrortimer();
    }    

  } else {
    kony.print("IN FINAL CALLBACK PERMISSION IS UNAVAIALABLE");
    var messageErr = "App cannot proceed further unless the required permisssions are granted!" //Need to chk with customer on msg text
    alert(messageErr);
  }
} */

//Actual QR Code Widget

function showQRScanningLanding() {
  gblQRPayData = {};
  
  //#ifdef iphone
  frmscanqrLanding.destroy();
  //#endif
  
  //frmscanqrLanding.destroy();
  frmscanqrLanding.flexInitialQrCode.setVisibility(true);
  frmscanqrLanding.flexValidQR.setVisibility(false);
  frmscanqrLanding.flexInvalidQR.setVisibility(false);
  frmscanqrLanding.flexQrBorder.skin = flexQRInitialBorder;

  //write code for initialisation and change of skins
  frmscanqrLanding.show();
  frmQRPaymentSuccess.destroy();
  
  //2. Setting up VTAP SDK
   setUpVTapSDK();
  
}

function successQrscancallBack() {
  frmQRSuccess.destroy();
  try {
    kony.timer.cancel("successqrtimer");
  } catch (e) {}
  kony.timer.schedule("successqrtimer", invokeqrsuccesstimer, 2, false)

}

function errorQrscancallBack() {
  try {
    kony.timer.cancel("errorsqrtimer");
  } catch (e) {}
  kony.timer.schedule("errorsqrtimer", invokeqrerrortimer, 2, false)

}

function reInitialiseForm() {
  try {
    kony.timer.cancel("initialiseForm");
  } catch (e) {}
  kony.timer.schedule("initialiseForm", invokeqrformReLaunch, 2, false)

}

function invokeqrsuccesstimer() {
  frmQRSuccess.show();
  invokeQRPaymentDefaultAccountService();
  frmscanqrLanding.destroy();
}

function invokeqrerrortimer() {
  frmLoadingPleaseWait.show();
  frmscanqrLanding.destroy();
  reInitialiseForm();
}

function invokeqrformReLaunch() {
  showQRScanningLanding();
}

function scanBarcodeResultCallback(scanResult) {
  gblQRPayData = {};
  var currentForm=kony.application.getCurrentForm();
  if(currentForm!=null){
   if(currentForm.id == "frmscanqrLanding" ){
		//to prevent scanning in background
		if (scanResult == "Back") {
			//alert("Unable to scan");
			var KeyTitle="";
			keyMsg = "Your QR Code has expired, please tap \"Refresh QR Code\" on TMB Internet Banking and try again.";
			alertType = "error";
			showAlertRcMB(keyMsg, KeyTitle, alertType);
		} else {
			kony.print("Value of the Result is"+scanResult);
			botReadValue = scanResult;
			gblqrAmnt = "";
			frmscanqrLanding.lblAmount.text = "";
			//alert(botReadValue);
			var botStr = getCSRBOTString(botReadValue);
			//    var csrValue = getCSRValueFromBOT(botReadValue);
			//var csrVerifier = setparcrc16(botStr, 0x1021, 0xFFFF, false);
			if(botReadValue.indexOf("|")!=-1)
			{
			  gblPreLoginQRResult=botReadValue;
			  if(isSignedUser){
				callBillPaymentFromQR();
				return;
			  }else{
			  gblPreLoginBillPayFlow=true;
			  //frmMBPreLoginAccessesPin.show();
					if(frmMBPreLoginAccessesPin.FlexQuickBalanceContainer.left == "0%"){
						frmMBPreLoginAccessesPin.FlexQuickBalanceContainer.left="-100%";
						frmMBPreLoginAccessesPin.FlexLoginContainer.left="0%";
						//showFPFlex(false)
						//closeQuickBalance();
					}
					frmMBPreLoginAccessesPin.flexCrossmark.isVisible = true;
					frmMBPreLoginAccessesPin.flexHeader.isVisible = false;
					frmMBPreLoginAccessesPin.flexSwipeContainer.top = "20%"
					frmMBPreLoginAccessesPin.FlexContainer0b16eb3557e0043.isVisible = false
					frmMBPreLoginAccessesPin.imgQRHeaderLogo.isVisible = true;
					frmMBPreLoginAccessesPin.LabelLoginForQR.setVisibility(true);
					frmMBPreLoginAccessesPin.LabelLoginForQR.text = kony.i18n.getLocalizedString("msgeLoginforQRPayment");
					frmMBPreLoginAccessesPin.FlexLine.setVisibility(true);
					frmMBPreLoginAccessesPin.FlexLoginContainer.top = "9%"; 
					gblApplicationLaunch = true;
                    gblLoginTab = "FP";
                    gblFPLoginSvsCalled = false;
					frmMBPreLoginAccessesPin.show();
					return;
				}		  
			   
			}  
			//call bot qr call
          	kony.print("^^^^calling bot csr call");
			qrBOTCRCVerifier(botStr);

		}
        //
    }else{
        //do nothing
    }
  }else{
    //do nothing
  }
  
}

function qrBOTCRCVerifier(crcText) {
    var inputParam = {}
    //showLoadingScreenPopup();
    inputParam["crcString"] = crcText;
    invokeServiceSecureAsync("crcVerifier", inputParam, qrBOTCRCVerifierCallback)
}

function qrBOTCRCVerifierCallback(status, resulttable) {
    kony.print("CRC Verifier : status>>" + status);
    var csrVerifier = "";
    if (status == 400) {
        kony.print("CRC : opstatus>>" + resulttable["opstatus"]);
        if (resulttable["opstatus"] == 0) {
            csrVerifier = resulttable["CRCVALUE"];
          	gbleDonationType = "";
            var botMainMap = {};
            try {
                getBoTSubString(botReadValue, botMainMap);
            } catch (e) {
                kony.print("Exception" + e.message);
            }

            var csrValue = "";
			if(botMainMap["63"] != undefined){
              csrValue = botMainMap["63"];
            }else{
              csrValue = botMainMap["91"];
            }

            if (csrVerifier.length == 3) {
                csrVerifier = "0" + csrVerifier;
            }
            if (csrValue.toUpperCase() != csrVerifier) {
                frmscanqrLanding.flexQrBorder.skin = flexQRErrorBorder;
                frmscanqrLanding.flexInitialQrCode.setVisibility(false);
                frmscanqrLanding.flexValidQR.setVisibility(false);
                frmscanqrLanding.flexInvalidQR.setVisibility(true);
                frmscanqrLanding.LabelInvalidQR.text = kony.i18n.getLocalizedString("invalidTQRC");

                errorQrscancallBack();
            } else {
                // var botMainMap = {};
                // getBoTSubString(botReadValue, botMainMap);
                var tranBoTMap = {};
                //
                if (botMainMap["54"] != undefined) {
                    if (botMainMap["54"] == "0" || botMainMap["54"] == "0.00" || botMainMap["54"] == "0.0") {
                        gblqrAmnt = "";
                        frmscanqrLanding.flexAmount.setVisibility(false);
                        gblQRPayData["transferAmt"] = "";
                    } else {
                        gblqrAmnt = botMainMap["54"];
                        frmscanqrLanding.flexAmount.setVisibility(true);
                        gblQRPayData["transferAmt"] = gblqrAmnt;
                        var formattedAmount = amountFormat(gblqrAmnt);
                        frmscanqrLanding.lblAmount.text = formattedAmount[0];
                        frmscanqrLanding.LabelAmountDigits.text = formattedAmount[1];
                    }
                } else {
                    gblqrAmnt = "";
                    frmscanqrLanding.flexAmount.setVisibility(false);
                    gblQRPayData["transferAmt"] = "";
                }
              	
              	
                if (botMainMap["91"] != undefined) {
                    getBoTSubString(botMainMap["00"], tranBoTMap);
                  	var senderBankID="";
                    if (tranBoTMap["00"] == "000001") {
                       	senderBankID = tranBoTMap["01"];
                      	if(senderBankID!="011"){
                            csrVerifier = "0";
                            frmscanqrLanding.flexQrBorder.skin = flexQRErrorBorder;
                            frmscanqrLanding.flexInitialQrCode.setVisibility(false);
                            frmscanqrLanding.flexValidQR.setVisibility(false);
                            frmscanqrLanding.flexInvalidQR.setVisibility(true);
                            frmscanqrLanding.LabelInvalidQR.text = kony.i18n.getLocalizedString("Msg_QRITMXnotReady");
                            errorQrscancallBack();
                        }else{
                            gblQRPayData["qrtransRefID"]=tranBoTMap["02"];
                            checkTransactionRef(getDeviceID());
                        }
                        
                    }  else {
                        csrVerifier = "0";
                        frmscanqrLanding.flexQrBorder.skin = flexQRErrorBorder;
                        frmscanqrLanding.flexInitialQrCode.setVisibility(false);
                        frmscanqrLanding.flexValidQR.setVisibility(false);
                        frmscanqrLanding.flexInvalidQR.setVisibility(true);
                        frmscanqrLanding.LabelInvalidQR.text = kony.i18n.getLocalizedString("invalidTQRC");
                        errorQrscancallBack();
                    }
                } else if (botMainMap["29"] != undefined) {
                    var additionalDatafiled = {};
                    getBoTSubString(botMainMap["29"], tranBoTMap);
                    getBoTSubString(botMainMap["62"], additionalDatafiled);

                    if (tranBoTMap["00"] == "A000000677010111") {
                        //frmBotInformation.value1.text = "AID - "+tranBoTMap["00"];
                        if (tranBoTMap["01"] != undefined) {
                          	gblQRPayData["QR_TransType"]="QR_Transfer";
                            var mobnum = tranBoTMap["01"];
                            if (mobnum != undefined) {
                                mobnum = mobnum.substring(4, mobnum.length);
                            }
                            gblqrmobileno = "0" + mobnum;
                            var formatterMobileQRNum = formatMobileNumber(gblqrmobileno);
                            gblQRPayData["mobileNo"] = gblqrmobileno;
                            frmscanqrLanding.flexQrBorder.skin = flexQRSuccessBorder;
                            frmscanqrLanding.flexInitialQrCode.setVisibility(false);
                            frmscanqrLanding.flexValidQR.setVisibility(true);
                            frmscanqrLanding.flexInvalidQR.setVisibility(false);
                            frmscanqrLanding.lblMobileNum.text = formatterMobileQRNum;
                            gblSelTransferMode = 2;
                            gblTransEmail = 1;
                            gblTrasSMS = 1;
                            successQrscancallBack();
                        } else if (tranBoTMap["02"] != undefined) {
                            gblQRPayData["QR_TransType"]="QR_Transfer";
                            gblQRPayData["cId"] = tranBoTMap["02"];
                            frmscanqrLanding.flexQrBorder.skin = flexQRSuccessBorder;
                            frmscanqrLanding.flexInitialQrCode.setVisibility(false);
                            frmscanqrLanding.flexValidQR.setVisibility(true);
                            frmscanqrLanding.flexInvalidQR.setVisibility(false);
                            frmscanqrLanding.lblMobileNum.text = formatCitizenID(gblQRPayData["cId"]);
                            gblSelTransferMode = 3;
                            gblTransEmail = 1;
                            gblTrasSMS = 1;
                            successQrscancallBack();
                        } else if (tranBoTMap["03"] != undefined) {
                          	gblQRPayData["QR_TransType"]="QR_Transfer";
                            gblQRPayData["eWallet"] = tranBoTMap["03"];
                            gblTrasORFT = 0;
                            gblTransSMART = 0;
                            gblSelTransferMode = 5;
                            gblTransEmail = 1;
                            gblTrasSMS = 1;
                            frmscanqrLanding.flexQrBorder.skin = flexQRSuccessBorder;
                            frmscanqrLanding.flexInitialQrCode.setVisibility(false);
                            frmscanqrLanding.flexValidQR.setVisibility(true);
                            frmscanqrLanding.flexInvalidQR.setVisibility(false);
                            frmscanqrLanding.lblMobileNum.text = formatEWalletNumber(gblQRPayData["eWallet"]);
                            successQrscancallBack();
                        } else {
                            frmscanqrLanding.flexQrBorder.skin = flexQRErrorBorder;
                            frmscanqrLanding.flexInitialQrCode.setVisibility(false);
                            frmscanqrLanding.flexValidQR.setVisibility(false);
                            frmscanqrLanding.flexInvalidQR.setVisibility(true);
                            frmscanqrLanding.LabelInvalidQR.text = kony.i18n.getLocalizedString("invalidTQRC");
                            errorQrscancallBack();
                        }
                    } else if (tranBoTMap["00"] == "D15600000000") {
                        //alert("AID - " + tranBoTMap["00"]);
                        merchantAccountInfo = tranBoTMap["05"];
                        if (tranBoTMap["05"] != undefined) {
                            //alert("Merchant Account - " + tranBoTMap["05"]);
                        }
                    } else {
                        //alert("Invalid QR Transaction Data");
                        return;
                    }
                    //alert("Reserved for PromptPay - Credit Transfer with PromptPayID");
                    if (additionalDatafiled["07"] != undefined) {
                        //alert("Additional Info - "+additionalDatafiled["07"]);
                    }
                } else if (botMainMap["30"] != undefined) {
                  var additionalDatafiled = {};
                    getBoTSubString(botMainMap["30"], tranBoTMap);
                    getBoTSubString(botMainMap["62"], additionalDatafiled);
                    
                    if (tranBoTMap["00"] == "A000000677010112") {
                       
                      	gblQRPayData["billerID"]=tranBoTMap["01"];
                      	//alert("Testing"+tranBoTMap["02"]);
                      	if(tranBoTMap["02"]!=undefined){
                          gblQRPayData["ref1"]=tranBoTMap["02"];
                          frmscanqrLanding.lblReferenceNum1.text=kony.i18n.getLocalizedString("Ref1")+" "+tranBoTMap["02"];
                          frmscanqrLanding.lblBillerID.text=gblQRPayData["billerID"];
                        }else{
                          frmscanqrLanding.flexQrBorder.skin = flexQRErrorBorder;
                          frmscanqrLanding.flexInitialQrCode.setVisibility(false);
                          frmscanqrLanding.flexValidQR.setVisibility(false);
                          frmscanqrLanding.flexInvalidQR.setVisibility(true);
                          frmscanqrLanding.LabelInvalidQR.text = kony.i18n.getLocalizedString("invalidRef1TQRC");
                          errorQrscancallBack();
                          return;
                        }if (tranBoTMap["03"] != undefined) {
                          gblref2Val=true;
                          gblQRPayData["ref2"]=tranBoTMap["03"];
                          if(gblQRPayData["ref2"] == "0" || gblQRPayData["ref2"] == "0.00"){
                            frmscanqrLanding.lblReferenceNum2.setVisibility(false);
                          }else{
                            frmscanqrLanding.lblReferenceNum2.text=kony.i18n.getLocalizedString("Ref2")+" "+tranBoTMap["03"];
                          }
                        }else{
                          gblref2Val=false
                          frmscanqrLanding.lblReferenceNum2.setVisibility(false);
                        }
                      if (additionalDatafiled["07"] != undefined) {
                        //alert("Additional Info - "+additionalDatafiled["07"]);
                        gblQRPayData["ref3"]=additionalDatafiled["07"];
                      }else{
                        gblQRPayData["ref3"]="";
                      }
                      gblQRPayData["QR_TransType"]="QR_Bill_Payment";
                      frmscanqrLanding.flexQrBorder.skin = flexQRSuccessBorder;
                      frmscanqrLanding.flexMobileNum.setVisibility(false);
                      frmscanqrLanding.flexRefNumber.setVisibility(true);                                     
                      frmscanqrLanding.flexInitialQrCode.setVisibility(false);
                      frmscanqrLanding.flexValidQR.setVisibility(true);
                      frmscanqrLanding.flexInvalidQR.setVisibility(false);
                      successQrscancallBack();

                    } else {
                        csrVerifier = "0";
                        frmscanqrLanding.flexQrBorder.skin = flexQRErrorBorder;
                        frmscanqrLanding.flexInitialQrCode.setVisibility(false);
                        frmscanqrLanding.flexValidQR.setVisibility(false);
                        frmscanqrLanding.flexInvalidQR.setVisibility(true);
                        frmscanqrLanding.LabelInvalidQR.text = kony.i18n.getLocalizedString("invalidTQRC");
                        errorQrscancallBack();
                        return;
                    }
                } else if (botMainMap["31"] != undefined) {
                    getBoTSubString(botMainMap["31"], tranBoTMap);
                  	var senderBankID="";
                  kony.print("tranBoTMap :::"+JSON.stringify(tranBoTMap) );
                    if (tranBoTMap["00"] == "A000000677010113") {
                       	senderBankID = tranBoTMap["03"];
                      	if(senderBankID!="011"){
                            csrVerifier = "0";
                            frmscanqrLanding.flexQrBorder.skin = flexQRErrorBorder;
                            frmscanqrLanding.flexInitialQrCode.setVisibility(false);
                            frmscanqrLanding.flexValidQR.setVisibility(false);
                            frmscanqrLanding.flexInvalidQR.setVisibility(true);
                            frmscanqrLanding.LabelInvalidQR.text = kony.i18n.getLocalizedString("Msg_QRITMXnotReady");
                            errorQrscancallBack();
                        }else{
                            gblQRPayData["qrtransRefID"]=tranBoTMap["04"];
                            checkTransactionRef(getDeviceID());
                        }
                        
                    } else if (tranBoTMap["00"] == "A000000677012004") {
                        
                      	senderBankID = tranBoTMap["03"];
                      	if(senderBankID!="011"){
                            csrVerifier = "0";
                            frmscanqrLanding.flexQrBorder.skin = flexQRErrorBorder;
                            frmscanqrLanding.flexInitialQrCode.setVisibility(false);
                            frmscanqrLanding.flexValidQR.setVisibility(false);
                            frmscanqrLanding.flexInvalidQR.setVisibility(true);
                            frmscanqrLanding.LabelInvalidQR.text = kony.i18n.getLocalizedString("Msg_QRITMXnotReady");
                            errorQrscancallBack();
                        }else{
                            gblQRPayData["qrtransRefID"]=tranBoTMap["04"];
                            checkTransactionRef(getDeviceID());  
                        }
                        
                    } else if (tranBoTMap["00"] == "D15600000001") {
                        merchantAccountInfo = tranBoTMap["03"];
                    } else {
                        csrVerifier = "0";
                        frmscanqrLanding.flexQrBorder.skin = flexQRErrorBorder;
                        frmscanqrLanding.flexInitialQrCode.setVisibility(false);
                        frmscanqrLanding.flexValidQR.setVisibility(false);
                        frmscanqrLanding.flexInvalidQR.setVisibility(true);
                        frmscanqrLanding.LabelInvalidQR.text = kony.i18n.getLocalizedString("invalidTQRC");
                        errorQrscancallBack();
                    }
                } else if (botMainMap["91"] != undefined) {
                    getBoTSubString(botMainMap["91"], tranBoTMap);
                    if (tranBoTMap["00"] == "A011223344998877") {
                        merchantAccountInfo = tranBoTMap["07"];
                    } else {
                       	csrVerifier = "0";
                        frmscanqrLanding.flexQrBorder.skin = flexQRErrorBorder;
                        frmscanqrLanding.flexInitialQrCode.setVisibility(false);
                        frmscanqrLanding.flexValidQR.setVisibility(false);
                        frmscanqrLanding.flexInvalidQR.setVisibility(true);
                        frmscanqrLanding.LabelInvalidQR.text = kony.i18n.getLocalizedString("invalidTQRC");
                        errorQrscancallBack();                        
                    }
                } else if (botMainMap["80"] != undefined) {
                    getBoTSubString(botMainMap["80"], tranBoTMap);
                    qrid = tranBoTMap["00"];
                    qrsid = tranBoTMap["01"];
                    qrExpiryDate = tranBoTMap["02"];
                    //  alert("BOT qrid=" + qrid + "BOT qrsid=" + qrsid+ "qrExpiryDate=" + qrExpiryDate);
                    if (qrid != "" && qrsid != "") {
                        frmscanqrLanding.flexQrBorder.skin = flexQRSuccessBorder;
                        QRLoginBanking();
                    }
                } else {
                   	csrVerifier = "0";
                    frmscanqrLanding.flexQrBorder.skin = flexQRErrorBorder;
                    frmscanqrLanding.flexInitialQrCode.setVisibility(false);
                    frmscanqrLanding.flexValidQR.setVisibility(false);
                    frmscanqrLanding.flexInvalidQR.setVisibility(true);
                    frmscanqrLanding.LabelInvalidQR.text = kony.i18n.getLocalizedString("invalidTQRC");
                    errorQrscancallBack();
                }

            }

        } else {
            csrVerifier = "0";
            frmscanqrLanding.flexQrBorder.skin = flexQRErrorBorder;
            frmscanqrLanding.flexInitialQrCode.setVisibility(false);
            frmscanqrLanding.flexValidQR.setVisibility(false);
            frmscanqrLanding.flexInvalidQR.setVisibility(true);
            frmscanqrLanding.LabelInvalidQR.text = kony.i18n.getLocalizedString("invalidTQRC");
            errorQrscancallBack();
        }
    } else {
            csrVerifier = "0";
            frmscanqrLanding.flexQrBorder.skin = flexQRErrorBorder;
            frmscanqrLanding.flexInitialQrCode.setVisibility(false);
            frmscanqrLanding.flexValidQR.setVisibility(false);
            frmscanqrLanding.flexInvalidQR.setVisibility(true);
            frmscanqrLanding.LabelInvalidQR.text = kony.i18n.getLocalizedString("invalidTQRC");
            errorQrscancallBack();
    }
}

function getCSRValueFromBOT(botStr) {
  //var index = botStr.indexOf("6304");
  //return botStr.substring(index+4,botStr.length);
  return botStr.substring(botStr.length - 4, botStr.length);
}

// New BOT Reader ----Start

function getCSRBOTString(botStr) {
  //var index = botStr.indexOf("6304");
  //return botStr.substring(0,index+4);
  return botStr.substring(0, botStr.length - 4);
}

function _stringToBytes(str) {
  var ch, st, re = [];
  for (var i = 0; i < str.length; i++) {
    ch = str.charCodeAt(i);
    st = [];
    do {
      st.push(ch & 0xFF);
      ch = ch >> 8;
    }
    while (ch);
    re = re.concat(st.reverse());
  }
  return re;
}

function decimalToHexString(number) {
  if (number < 0) {
    number = 0xFFFFFFFF + number + 1;
  }

  return number.toString(16).toUpperCase();
}

function setparcrc16(inputStr, polynomial, crc, isHex) {

  var strLen = inputStr.length;
  var intArray;

  if (isHex) {
    if (strLen % 2 != 0) {
      inputStr = inputStr.substring(0, strLen - 1) + "0" + inputStr.substring(strLen - 1, strLen);
      strLen++;
    }

    intArray = new Array(strLen / 2);
    var ctr = 0;
    for (var n = 0; n < strLen; n += 2) {
      var inputValue = inputStr.substring(n, n + 2)
      if (inputValue != "") {
        intArray[ctr] = parseInt(inputStr.substring(n, n + 2), 16);
        ctr++;
      }
    }
  } else {
    var bytes = _stringToBytes(inputStr);
    intArray = new Array(bytes.length);
    var ctr = 0;
    for (var i = 0, iTop = bytes.length; i < iTop; i++) {
      intArray[ctr] = bytes[i];
      ctr++;
    }
  }

  // main code for computing the 16-bit CRC-CCITT
  for (var b = 0; b < intArray.length; b++) {
    for (var i = 0; i < 8; i++) {
      var bit = ((intArray[b] >> (7 - i) & 1) == 1);
      var c15 = ((crc >> 15 & 1) == 1);
      crc <<= 1;
      if (c15 ^ bit) crc ^= polynomial;
    }
  }

  crc &= 0xFFFF;
  //  return Integer.toHexString(crc).toUpperCase();
  return decimalToHexString(crc, 16);;

}

// New BOT Reader ----End

function getBOTString(botStr) {
  var index = botStr.indexOf("6304");
  return botStr.substring(0, index);
}

function getBoTSubString(botStr, map) {
  if (botStr != undefined) {
    var tagId = botStr.substring(0, 2);
    var length = botStr.substring(2, 4);
    var tagLenth = 4 + parseInt(length);
    var tagValue = botStr.substring(4, tagLenth);
    //map.put(tagId, tagValue);
    map[tagId] = tagValue;
    botStr = botStr.substring(tagLenth, botStr.length);
    if ("" == botStr || botStr == null) {
      return "";
    }
    return getBoTSubString(botStr, map);
  }
  return;
}

function compute(dataText, reflectFlag, reflectByteFlag) {
  // computes crc value
  var i;
  var j;
  var k;
  var bit;
  var datalen;
  var len;
  var actchar;
  var flag;
  var counter;
  var c;
  var crc = new Array(8 + 1);
  var mask = new Array(8);
  var hexnum = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F");

  var data;
  var order;
  var polynom = new Array(8);
  var init = new Array(8);
  var xor = new Array(8);
  var result = "";

  // convert crc order
  order = 16;
  if (isNaN(order) == true || order < 1 || order > 64) {
    result = "CRC order must be between 1 and 64";
    return;
  }


  // convert crc polynom
  polynom = convertentry("1021", order);

  if (polynom[0] < 0) {
    result = "Invalid CRC polynom";
    return;
  }


  // check if polynom is valid (bit 0 must be set)
  //  if (!(polynom[7]&1))
  //  {
  //    result = "CRC polynom LSB must be set";
  //    return;
  //  }
  // convert crc init value
  init = convertentry("FFFF", order);
  if (init[0] < 0) {
    result = "Invalid initial value";
    return;
  }


  // convert crc xor value
  xor = convertentry("0", order);
  if (xor[0] < 0) {
    result = "Invalid XOR value";
    return;
  }


  // generate bit mask
  counter = order;
  for (i = 7; i >= 0; i--) {
    if (counter >= 8) mask[i] = 255;
    else mask[i] = (1 << counter) - 1;
    counter -= 8;
    if (counter < 0) counter = 0;
  }

  crc = init;

  data = dataText;
  datalen = data.length;
  len = 0; // number of data bytes
  crc[8] = 0;


  // main loop, algorithm is fast bit by bit type
  for (i = 0; i < datalen; i++) {
    c = data.charCodeAt(i);

    if (data.charAt(i) == '%') // unescape byte by byte (%00 allowed)
    {
      if (i > datalen - 3) {
        result = "Invalid data sequence";
        return;
      }
      ch = parseInt(data.charAt(++i), 16);
      if (isNaN(ch) == true) {
        result = "Invalid data sequence";
        return;
      }
      c = parseInt(data.charAt(++i), 16);
      if (isNaN(c) == true) {
        result = "Invalid data sequence";
        return;
      }
      c = (c & 15) | ((ch & 15) << 4);
    }


    // perform revin
    if (reflectFlag) c = reflectByte(c);

    // rotate one data byte including crcmask
    for (j = 0; j < 8; j++) {
      bit = 0;

      if (crc[7 - ((order - 1) >> 3)] & (1 << ((order - 1) & 7))) {
        bit = 1;
      }
      if (c & 0x80) {
        bit ^= 1;
      }
      c <<= 1;
      for (k = 0; k < 8; k++) // rotate all (max.8) crc bytes
      {
        crc[k] = ((crc[k] << 1) | (crc[k + 1] >> 7)) & mask[k];
        if (bit) crc[k] ^= polynom[k];
      }
    }
    len++;
  }


  // perform revout
  //alert("Before CRC"+crc)
  if (reflectByteFlag) crc = reflect(crc, order, 0);
  //alert("After CRC"+crc)
  // perform xor value
  for (i = 0; i < 8; i++) crc[i] ^= xor[i];
  //alert("After CRC xor"+crc)
  // write result
  result = "";

  flag = 0;
  for (i = 0; i < 8; i++) {
    actchar = crc[i] >> 4;
    if (flag || actchar) {
      result += hexnum[actchar];
      flag = 1;
    }
    //alert("actchar"+actchar+"hexnum[actchar]"+hexnum[actchar])
    actchar = crc[i] & 15;
    if (flag || actchar || i == 7) {
      result += hexnum[actchar];
      flag = 1;
    }
  }
  return result;
}

function convertentry(input, order) {
  var len;
  var actchar;
  var polynom = new Array(0, 0, 0, 0, 0, 0, 0, 0);
  var brk = new Array(-1, 0, 0, 0, 0, 0, 0, 0);
  len = input.length;
  for (i = 0; i < len; i++) {
    actchar = parseInt(input.charAt(i), 16);

    if (isNaN(actchar) == true) return (brk);
    actchar &= 15;

    for (j = 0; j < 7; j++) polynom[j] = ((polynom[j] << 4) | (polynom[j + 1] >> 4)) & 255;
    polynom[7] = ((polynom[7] << 4) | actchar) & 255;
  }
  count = 64;
  for (i = 0; i < 8; i++) {
    for (j = 0x80; j; j >>= 1) {
      if (polynom[i] & j) break;
      count--;
    }
    if (polynom[i] & j) break;
  }

  if (count > order) return (brk);

  return (polynom);
}

function reflect(crc, bitnum, startLSB) {
  var i, j, k, iw, jw, bit;

  for (k = 0; k + startLSB < bitnum - 1 - k; k++) {

    iw = 7 - ((k + startLSB) >> 3);
    jw = 1 << ((k + startLSB) & 7);
    i = 7 - ((bitnum - 1 - k) >> 3);
    j = 1 << ((bitnum - 1 - k) & 7);

    bit = crc[iw] & jw;
    if (crc[i] & j) crc[iw] |= jw;
    else crc[iw] &= (0xff - jw);
    if (bit) crc[i] |= j;
    else crc[i] &= (0xff - j);
  }
  return (crc);
}

function reflectByte(inbyte) {
  var outbyte = 0;
  var i = 0x01;
  var j;

  for (j = 0x80; j; j >>= 1) {
    if (inbyte & i) outbyte |= j;
    i <<= 1;
  }
  return (outbyte);
}

function frmscanqrLandingPreShow() {
  var screenwidth = gblDeviceInfo["deviceWidth"];
  var screenheight = gblDeviceInfo["deviceHeight"];
  var screenheightpx = (80 * screenheight)/100;
  frmscanqrLanding.FlexQRScanner.height = screenwidth + "px";
  var fullBody = screenheightpx;
  var qrScan = screenwidth;
  var contentqr = fullBody - qrScan;
  frmscanqrLanding.flexContents.height = contentqr + "px";
  gblQRPayData = {};
  frmscanqrLanding.lblHdrTxt.text = kony.i18n.getLocalizedString("keyScanQR");
  frmscanqrLanding.btnRight.text = kony.i18n.getLocalizedString("btnMyQR");
  if(gblShowMyQEGenerator == "true"){
    frmscanqrLanding.btnRight.isVisible = true;
  }else{
    frmscanqrLanding.btnRight.isVisible = false;
  }

  //frmscanqrLanding.LabelInvalidQR.text = kony.i18n.getLocalizedString("msgInvalidQRCode");
  frmscanqrLanding.LabelBrowse.text = kony.i18n.getLocalizedString("btnBrowse");
  frmscanqrLanding.LabelPointCamera.text = kony.i18n.getLocalizedString("msgScanQRCode");
  //frmMyQRCode.destroy();
  //frmMyQRInformation.destroy();
  //frmMyQRSelectPromptPay.destroy()
  //frmMyQRPromptPayRegister.destroy();
}

function frmscanqrLandingOnClickMyQR() {
  checkPromptPayRegiser();
}

function invokeAccesspinVerificationQR(pinValue) {
  showLoadingScreen();
  var inputParam = {};

  inputParam["appID"] = "tmb"; 
  inputParam["channel"] = "rc"; 

  inputParam["loginTypeTouch"] = "true";

  inputParam["activityTypeId"] = "Login";
  inputParam["password"] = pinValue;

  inputParam["deviceId2"]=getDeviceID();
  inputParam["osDeviceId"] = getDeviceID();//getDeviceID();
  inputParam["deviceOS"] = getDeviceOS();

  invokeServiceSecureAsync("QRPaymentValidation", inputParam, callbackmodaccessqr)
}

function callbackmodaccessqr(status, resulttable){
  if (status == 400) {

    if (resulttable["opstatus"] == 0) {

      if (resulttable["deviceStatus"] == "0") {
        showAlert(kony.i18n.getLocalizedString("ECKonyDeviceErr00002"), kony.i18n.getLocalizedString("info"));
        //resetValues();
        kony.application.dismissLoadingScreen();
        return false;
      } else if (resulttable["deviceStatus"] == "2") {
        showAlert("Device is blocked", kony.i18n.getLocalizedString("info"));
        //resetValues();
        kony.application.dismissLoadingScreen();
        return false;
      } else if (resulttable["deviceStatus"] == "3") {
        showAlert("Device is cancelled", kony.i18n.getLocalizedString("info"));
        //resetValues();
        kony.application.dismissLoadingScreen();
        return false;
      }
      else if (resulttable["errMsg"] == "Password is verified") {
        gblqrPreloginPwdVerified=true;
        loadMapforQrPreLoginPay();
		gblTotalPinAttemptsTransfers = 3;
      } else if (resulttable["errMsg"] == "Password Locked" || resulttable["errCode"] == "E10105") {
        /*
				resetValues();
				isUserStatusActive=false;
				popupAccesesPinLocked.lblPopupConfText.text = kony.i18n.getLocalizedString("acclockmsg");
				popupAccesesPinLocked.btnPopupConfCancel.text= kony.i18n.getLocalizedString("keyCancelButton");
				popupAccesesPinLocked.btnCallTMB.text= kony.i18n.getLocalizedString("keyCall");
				popupAccesesPinLocked.show();
				*/
        //alert(" " + kony.i18n.getLocalizedString("keyAccessPwdLocked"));
        kony.application.dismissLoadingScreen();
        return false;
      } else if (resulttable["errMsg"] == "User is already login to application") {
        //resetValues();
        kony.application.dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECUserLoggedIn"), kony.i18n.getLocalizedString("info"));
        //frmMBPreLoginAccessesPin.show();
        return false;
      } else if (resulttable["errCode"] == "E10020") { 
        kony.print("Inside error code E10020");
        	if(gbleDonationType == "QR_Type" || gbleDonationType == "Barcode_Type"){
              kony.print("Inside QR_Type"+gbleDonationType);
              gblTotalPinAttemptsTransfers = 3;
              dismissLoadingScreen();
              resetKeypadApproval();           
              var badLoginCount = resulttable["badLoginCount"];
              var incorrectPinText = kony.i18n.getLocalizedString("PIN_Incorrect");
              incorrectPinText = incorrectPinText.replace("{rem_attempt}", gblTotalPinAttemptsTransfers - badLoginCount); 
              frmEDonationPaymentConfirm.lblForgotPin.text = incorrectPinText; //kony.i18n.getLocalizedString("invalidCurrPIN"); //MKI, MIB-9892
              frmEDonationPaymentConfirm.lblForgotPin.skin = "lblBlackMed150NewRed";
              frmEDonationPaymentConfirm.lblForgotPin.onTouchEnd = doNothing;
            }else{
              resetKeypadApproval();
			var badLoginCount = resulttable["badLoginCount"];
			var incorrectPinText = kony.i18n.getLocalizedString("PIN_Incorrect");
      		incorrectPinText = incorrectPinText.replace("{rem_attempt}", gblTotalPinAttemptsTransfers - badLoginCount); 
             
			frmQRSuccess.lblForgotPin.text = incorrectPinText; //kony.i18n.getLocalizedString("invalidCurrPIN");
			frmQRSuccess.lblForgotPin.skin = "lblBlackMed150NewRed";
			frmQRSuccess.lblForgotPin.onTouchEnd = doNothing;
        	kony.application.dismissLoadingScreen();
            }
        return false; 
      } else {
        //resetValues();
        showAlertRcMB(kony.i18n.getLocalizedString("keyWrongPwd"),kony.i18n.getLocalizedString("info"), "info");
        kony.application.dismissLoadingScreen();
        return false;
      }
    } else if (resulttable["errMsg"] == "User is already login to application") {
      //resetValues();
      kony.application.dismissLoadingScreen();
      showAlert(kony.i18n.getLocalizedString("ECUserLoggedIn"), kony.i18n.getLocalizedString("info"));
      //frmMBPreLoginAccessesPin.show();
      return false;
    }   else {
      var errorMessage = resulttable["errMsg"];
      var middlewareErrorMessage = resulttable["errmsg"];
      if(errorMessage == null || errorMessage == undefined || errorMessage == "" ){
        if(middlewareErrorMessage != null && middlewareErrorMessage != undefined ){
          errorMessage =  middlewareErrorMessage;
        }
      }		
      //resetValues();
      kony.application.dismissLoadingScreen();
      alert(" " + errorMessage);
      return false;
    }
  }else{
    //resetValues();	
  }

}

function invokeQRPaymentDefaultAccountServiceCalFee() {
  var inputParam = {};

  showLoadingScreen();

  //frmQRSuccess.FlexLoading.setVisibility(true);
  gblQRPaymentDeviceId = getDeviceID();

  inputParam["encryptDeviceId"] = getDeviceID();
  if(gblQRPayData["mobileNo"] != undefined && gblQRPayData["mobileNo"] != ""){
    inputParam["promptPayType"] = "02";
    inputParam["promptPayId"] = gblQRPayData["mobileNo"];
  } else if(gblQRPayData["cId"] != undefined && gblQRPayData["cId"] != "") {
    inputParam["promptPayType"] = "01";
    inputParam["promptPayId"] = gblQRPayData["cId"];
  } else if(gblQRPayData["eWallet"] != undefined && gblQRPayData["eWallet"] != "") {
    inputParam["promptPayType"] = "04";
    inputParam["promptPayId"] = gblQRPayData["eWallet"];
  } else if(gblQRPayData["billerID"] != undefined && gblQRPayData["billerID"] != "") {
    inputParam["promptPayId"] = gblQRPayData["billerID"];
    inputParam["promptPayType"] = "05";
  }else {
    alert("Incorrect input param");
    return;
  }
  kony.print("QR AMount is"+gblqrAmnt);
  inputParam["transAmount"] = removeCommos(frmQRSuccess.tbxEnterAmount.text);

  if(gblQRPayData["QR_TransType"]== "QR_Bill_Payment"){
      inputParam["transType"] = "BILLPAY_PROMPTPAY";
      inputParam["ref1"] = gblQRPayData["ref1"];
      inputParam["ref2"] = gblQRPayData["ref2"];
      inputParam["ref3"] = gblQRPayData["ref3"];
    }else{
      inputParam["transType"] = "TRANSFER_PROMPTPAY";
    }

  invokeServiceSecureAsync("QRPaymentDefaultAccount", inputParam, callBackQRDefaultAccountCalFee);
}


function callBackQRDefaultAccountCalFee(status, resulttable) {
  try {
    dismissLoadingScreen();
    var locale_app = kony.i18n.getCurrentLocale();
    //enableFormElements();
    var banckCode = "";
    var usrAccntNickname = "";
    var usrAcctNum = ""
    var accountNickName = "";
    var accountId = "";
    var accountNoFomatted = "";
    var availBalLabel = "";
    var availBalValue = ""
    var msgError = "";

    if (status == 400) {
      if (resulttable["opstatus"] == "0" || resulttable["opstatus"] == 0) {
        gblQRDefaultAccountResponse = resulttable;
		if(resulttable["pp_isOwn"] == 'Y'){ // MKI, MIB-9972 ---start
       		gblpp_isOwnAccount = true; 
       	} // MKI, MIB-9972 ---End
        if (resulttable["pp_promptPayFlag"] == "1") {
          var errCode = resulttable["pp_errCode"];
          var errorText = "";

          if (isNotBlank(errCode)) {

          } else if (errCode == 'XB240063') {
            msgError = kony.i18n.getLocalizedString("MIB_P2PRecBankRejectAmt");
          } else if (errCode == 'XB240048') 
          {
            msgError = kony.i18n.getLocalizedString("msgErrRecipientNotRegisterdPromptpay");
          } else if (errCode == 'XB240066') 
          {
            if (gblSelTransferMode == 2) {
              msgError = kony.i18n.getLocalizedString("MIB_P2PNullAcctName");
            } else if(gblSelTransferMode == 3){
              msgError = kony.i18n.getLocalizedString("MIB_P2PnullAcctName2");
            }
          } else if (errCode == 'XB240088') {
            if (gblSelTransferMode == 2) {
              msgError = kony.i18n.getLocalizedString("MIB_P2PAccInActive");
            } else if(gblSelTransferMode == 3 || gblSelTransferMode == 4 || gblSelTransferMode == 5){
              msgError = kony.i18n.getLocalizedString("MIB_P2PAccInActive");
            }            
          } else if (errCode == 'XB240098') {
            msgError = kony.i18n.getLocalizedString("MIB_P2PExceedeWal");
          } else if (errCode == 'XB240067') {
            msgError = kony.i18n.getLocalizedString("MIB_P2PCutOffTime");
          } else if (errCode == 'XB240072') {
            msgError = kony.i18n.getLocalizedString("MIB_P2PDestTimeout");
          } else if (errCode == 'X8899') {
            msgError = kony.i18n.getLocalizedString("MIB_P2PCloseBranchErr");
          } else if (errCode == 'X1120') {
            msgError = kony.i18n.getLocalizedString("billernotfoundTQRC");
          } else {
            var errorText = kony.i18n.getLocalizedString("ECGenericError") + " (" + errCode + ")";
            msgError = errorText;
          }
          frmQRSuccess.lblerrormsg1.text = msgError;
          frmQRSuccess.flxErrorBody.height = "94%"
          frmQRSuccess.flxErrorBody.setVisibility(true);
          frmQRSuccess.flxPayFooter.setVisibility(false);
          frmQRSuccess.flexChooseAccntFooter.setVisibility(false);
          frmQRSuccess.flxLoginFooter.setVisibility(false);
          return;
        } else if(resulttable["pp_destBankCode"] != undefined){
          banckCode = resulttable["pp_destBankCode"];
          gblisTMB = banckCode;
          gblqrUsrName = resulttable["pp_toAccTitle"];
          if(!isNotBlank(gblisTMB)){
            msgError = kony.i18n.getLocalizedString("ECGenericError");
            frmQRSuccess.lblerrormsg1.text = msgError;
            frmQRSuccess.flxErrorBody.height = "94%"
            frmQRSuccess.flxErrorBody.setVisibility(true);
            frmQRSuccess.flxPayFooter.setVisibility(false);
            frmQRSuccess.flexChooseAccntFooter.setVisibility(false);
            frmQRSuccess.flxLoginFooter.setVisibility(false);
            return;
          }
          if(gblisTMB != gblTMBBankCD){
            gblqrUsrName = resulttable["pp_toAcctName"];
            gblqrUsrAccnt = resulttable["pp_toAcctNo"];
            gblqrTransFee = resulttable["pp_itmxfee"];
            //gblqrToUsrName = resulttable["pp_toAccTitle"];
			gblQRPayData["billerCompCode"] = resulttable["qrCompCode"];
            gblQRPayData["toAccountName"] = resulttable["pp_toAccTitle"];
            gblQRPayData["toAccountName"] = resulttable["pp_toAcctName"];
            gblQRPayData["toAcctIDNoFormat"] = resulttable["pp_toAcctNo"];
            gblQRPayData["custAcctName"] = resulttable["pp_toAccTitle"];
            if (resulttable["pp_itmxFee"] !== null || resulttable["pp_itmxFee"] !== undefined) {
              gblQRPayData["fee"] = resulttable["pp_itmxFee"];
              if(gblQRPayData["fee"]=="0.00" || gblQRPayData["fee"]=="")
              {
                frmQRSuccess.LabelFee.setVisibility(false);
              }
              else
              {
                frmQRSuccess.LabelFee.text = kony.i18n.getLocalizedString("keyFee") + gblQRPayData["fee"] + " " + kony.i18n.getLocalizedString("keyBaht");
                frmQRSuccess.LabelFee.setVisibility(true);
              }              
            } else {
              gblQRPayData["fee"] = "0.00";
              frmQRSuccess.LabelFee.setVisibility(false);
            }
          } else {
            gblqrUsrName = resulttable["pp_toAccTitle"];
            gblqrUsrAccnt = resulttable["pp_toAcctNo"];
            gblqrTransFee = resulttable["pp_itmxfee"];
            gblQRPayData["toAccountName"] = resulttable["pp_toAcctName"];
            gblQRPayData["billerCompCode"] = resulttable["qrCompCode"];
            //gblqrToUsrName = resulttable["pp_toAccTitle"];
			//gblQRPayData["toAccountName"] = resulttable["pp_toAccTitle"];
            gblQRPayData["toAcctIDNoFormat"] = resulttable["pp_toAcctNo"];
            gblQRPayData["custAcctName"] = resulttable["pp_toAccTitle"];
            if (resulttable["pp_itmxFee"] !== null || resulttable["pp_itmxFee"] !== undefined) {
              gblQRPayData["fee"] = resulttable["pp_itmxFee"];
              if(gblQRPayData["fee"]=="0.00" || gblQRPayData["fee"]=="")
              {
                frmQRSuccess.LabelFee.setVisibility(false);
              }
              else
              {
                frmQRSuccess.LabelFee.text = kony.i18n.getLocalizedString("keyFee") + gblQRPayData["fee"] + " " + kony.i18n.getLocalizedString("keyBaht");
                frmQRSuccess.LabelFee.setVisibility(true);
              }
            } else {
              gblQRPayData["fee"] = "0.00";
              frmQRSuccess.LabelFee.setVisibility(false);
            }
          }
          if (resulttable["custAcctRec"] != undefined && resulttable["custAcctRec"].length > 0) {
            //frmQRSuccessPreShow();

            frmQRSuccess.flexAmountlbl.setVisibility(false);
            frmQRSuccess.flexAmounttbx.setVisibility(true);
            frmQRSuccess.flxPayFooter.setVisibility(true);
            var collectionData = resulttable["custAcctRec"];
			gblQRPayData["accountName"]= resulttable["custAcctRec"][0]["accountName"]; //MKI, MIB-9891
            if(resulttable["custAcctRec"][0]["acctNickName"] != undefined 
               && resulttable["custAcctRec"][0]["acctNickName"] != ""){
              gblQRPayData["custAcctName"] = resulttable["custAcctRec"][0]["acctNickName"]
              gblQRPayData["custName"] = resulttable["custAcctRec"][0]["acctNickName"];
              kony.print("MKITEST resulttable[custAcctRec][0][accountName]="+ resulttable["custAcctRec"][0]["accountName"]);
              //gblQRPayData["accountName"]= resulttable["custAcctRec"][0]["accountName"]; //MKI, MIB-9891
              gblqrFromAccntNickName = resulttable["custAcctRec"][0]["acctNickName"]
            } else {
              if (locale_app == "th_TH") {
                gblQRPayData["custAcctName"] = resulttable["custAcctRec"][0]["defaultCurrentNickNameTH"];
                gblQRPayData["custName"] = resulttable["custAcctRec"][0]["defaultCurrentNickNameTH"];
                gblqrFromAccntNickName = resulttable["custAcctRec"][0]["defaultCurrentNickNameTH"];
              } else {
                gblQRPayData["custAcctName"] = resulttable["custAcctRec"][0]["defaultCurrentNickNameEN"];
                gblQRPayData["custName"] = resulttable["custAcctRec"][0]["defaultCurrentNickNameEN"];
                gblqrFromAccntNickName = resulttable["custAcctRec"][0]["defaultCurrentNickNameEN"];
              }
            }

            if ((resulttable["custAcctRec"][0]["accId"]) != null && (resulttable["custAcctRec"][0]["accId"]) != undefined) {
              accountId = resulttable["custAcctRec"][0]["accId"];
              gblqrFromAccntNum = resulttable["custAcctRec"][0]["accId"];
              gblQRPayDefaultAcct = gblqrFromAccntNum;
            }

            if ((resulttable["custAcctRec"][0]["accountNoFomatted"]) != null && (resulttable["custAcctRec"][0]["accountNoFomatted"]) != undefined) {
              gblqrToAccnt = resulttable["custAcctRec"][0]["accountNoFomatted"];
              gblQRPayData["fromAcctID"] = resulttable["custAcctRec"][0]["accountNoFomatted"];
            }

            if ((resulttable["custAcctRec"][0]["accType"]) == "CCA") {
              gblqrToAccntBal = resulttable["custAcctRec"][0]["availableCreditBalDisplay"];
            } else {
              gblqrToAccntBal = resulttable["custAcctRec"][0]["availableBalDisplay"];
            }
            //frmQRSuccessPreShow();

          } else {
            msgError = kony.i18n.getLocalizedString("keyNoEligibleAct");
            frmQRSuccess.lblerrormsg1.text = msgError;
            frmQRSuccess.flxErrorBody.height = "94%"
            frmQRSuccess.flxErrorBody.setVisibility(true);
            frmQRSuccess.flxPayFooter.setVisibility(false);
            frmQRSuccess.flexChooseAccntFooter.setVisibility(false);
            frmQRSuccess.flxLoginFooter.setVisibility(false);
            return false;
          }
        } else {
         var errCode = resulttable["pp_errCode"];
         var errorText = "";
         if (isNotBlank(errCode)) {
             if (errCode == 'X1120') {
              msgError = kony.i18n.getLocalizedString("billernotfoundTQRC");
            }else{
              var errorText = kony.i18n.getLocalizedString("ECGenericError") + " (" + errCode + ")";
              msgError = errorText;
          	}           
          }else{
            var errorText = kony.i18n.getLocalizedString("ECGenericError") + " (" + errCode + ")";
            msgError = errorText;
          }       
          frmQRSuccess.lblerrormsg1.text = msgError;
          frmQRSuccess.flxErrorBody.height = "94%"
          frmQRSuccess.flxErrorBody.setVisibility(true);
          frmQRSuccess.flxPayFooter.setVisibility(false);
          frmQRSuccess.flexChooseAccntFooter.setVisibility(false);
          frmQRSuccess.flxLoginFooter.setVisibility(false);
          return;
        }
      } else if (resulttable["opstatus"] == 8005) {

        if (resulttable["errCode"] == 1002) {
          isUserStatusActive = false;
          //alert(kony.i18n.getLocalizedString("VQB_MBStatus"));
          showAlertDeviceIdTouchMessage(kony.i18n.getLocalizedString("deviceIdEnhancementUnlock"), kony.i18n.getLocalizedString("deviceIDUnlock"));
        } else {
          var errMsg = resulttable["errMsg"];
          alert(errMsg);
        }
      } else if (resulttable["opstatus"] == 1011) {
        alert(kony.i18n.getLocalizedString("genErrorWifiOff"));
        if (resulttable["errCode"] == 1002) {
          isUserStatusActive = false;
          msgError = kony.i18n.getLocalizedString("VQB_MBStatus");
          alert(msgError);
          onLogoutPopUpConfrm(); //MKI, MIB-9955
        } else {
          alert(kony.i18n.getLocalizedString("VQB_ServiceDown"));
          msgError = resulttable["errMsg"];
          //alert(msgError);
          showAlertDeviceIdTouchMessage(kony.i18n.getLocalizedString("deviceIdEnhancementUnlock"), kony.i18n.getLocalizedString("deviceIDUnlock"));
        }
      } else if (resulttable["opstatus"] == 1011) {
        msgError = kony.i18n.getLocalizedString("genErrorWifiOff");
        alert(msgError);
      } else {
        msgError = kony.i18n.getLocalizedString("VQB_ServiceDown");
        //alert(msgError);
        showAlertDeviceIdTouchMessage(kony.i18n.getLocalizedString("deviceIdEnhancementUnlock"), kony.i18n.getLocalizedString("deviceIDUnlock"));
      }
    } else {
      alert("error");
    } 
  } catch (e) {
    alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
    msgError = kony.i18n.getLocalizedString("ECGenOTPRtyErr00001");
    alert(msgError);
  }
}

//Navigation to BILL Payment Flow

function callBillPaymentFromQR(){
  if(checkMBUserStatus()){
    gblMyBillerTopUpBB = 0;
    GblBillTopFlag = true;
    glb_accId = 0;
    gblFromAccountSummary = false;
    callMasterBillerServiceQR();
  }	
}


function callMasterBillerServiceQR(){
  showLoadingScreen();
  var billerGroupType = "";
  if (gblMyBillerTopUpBB == 0) {
    billerGroupType = "0";
  } else {
    billerGroupType = "1";
  }
  var inputParams = {
    IsActive: "1",
    BillerGroupType: billerGroupType,
    clientDate: getCurrentDate(),
    flagBillerList : "MB"
  };
  invokeServiceSecureAsync("masterBillerInquiry", inputParams, callMasterBillerServiceCallBackQR);
}

function callMasterBillerServiceCallBackQR(status, callBackResponse) {
  if (status == 400) {
    if (callBackResponse["opstatus"] == "0") {

      if(gblMyBillerTopUpBB == 0) {
        showBillPaymentLandingForQR();
      } else {
        //gotoTopUpSelectBillerForm()
        alert("In Else");
      }

    } else {
      dismissLoadingScreen();
      alert(kony.i18n.getLocalizedString("ECGenericError"));
    }
  } else {
    if (status == 300) {
      dismissLoadingScreen();
      alert(kony.i18n.getLocalizedString("ECGenericError"));
    }
  }		
}

function showBillPaymentLandingForQR() {
  gblFlagMenu = "";
  destroyMenuSpa()
  gblMyBillerTopUpBB = 0;
  gblFirstTimeBillPayment = true;
  gblRetryCountRequestOTP = "0";
  loadBillPaymentfromQR();
  //frmSelectBillerLanding.segMyBills.removeAll();
  //frmSelectBillerLanding.show();
}

function loadBillPaymentfromQR(){
  var inputParam = {};
  inputParam["barcode"] = gblPreLoginQRResult;
  showLoadingScreen();
  invokeServiceSecureAsync("checkBiller", inputParam, scannedBiller);
}

function loadimagefromGallery()
{
  //#ifdef android
  var BarScanObject = new barcode.BarScan();
  BarScanObject.imagereader(callbackqrimagereaderresult);
  //#endif

  //#ifdef iphone
  var BarScanObject = new iphoneBarcode.BarScan();
  BarScanObject.imagereader(callbackqrimagereaderresult);
  //#endif

}

function callbackqrimagereaderresult(result)
{
  scanBarcodeResultCallback(result);
}

function navigateBillerNotFound()
{  
  var currentForm=kony.application.getCurrentForm();
  if(currentForm!=null){
    if(isSignedUser){
      if(currentForm.id == "frmscanqrLanding" ){
        showAccuntSummaryScreen();
      }else if(currentForm.id == "frmMBPreLoginAccessesPin"){
        showAccuntSummaryScreen();
      }
    }else{
		//callBillPaymentFromMenu();
    }
  }
  BillerNotAvailable.dismiss();
}

function clearQRSession(){
  
  if(gblqrflow=="preLogin" && gblqrPreloginPwdVerified)
  {
    qrLogoutServiceCall()
  }else{
    //do nothing
  }
}


function qrLogoutServiceCall(){
  //kony.application.showLoadingScreen(frmLoading, "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
  //showLoadingScreen();
  inputParam = {};
  var channelId = "";
  if (flowSpa) {
    channelId = "01";
  } else {
    channelId = "02";
  }

  if (GLOBAL_MB_CHANNEL != null && GLOBAL_MB_CHANNEL != "") {
    if (flowSpa) {
      channelId = GLOBAL_IB_CHANNEL;
    } else {
      channelId = GLOBAL_MB_CHANNEL;
    }
  }

  if (flowSpa) {
    inputParam["deviceId"] = "";
  } else {
    inputParam["deviceId"] = GBL_UNIQ_ID;
  }

  inputParam["channelId"] = channelId;
  inputParam["timeOut"] = GBL_Time_Out;
  var locale = kony.i18n.getCurrentLocale();

  if (locale == "en_US") {
    inputParam["languageCd"] = "EN";
  } else {
    inputParam["languageCd"] = "TH";
  }
  invokeServiceSecureAsync("logOutTMB", inputParam, callBackQRLogOutService)
}

function callBackQRLogOutService(status, resulttable) {
  if (status == 400) {
    if (resulttable["opstatus"] == 0) {
      gblqrPreloginPwdVerified=false;
      var usageTime = resulttable["usageTime"];
    }
  }
  //dismissLoadingScreen();
}

function onIdleTimeOutQRCallBack() {
	dismissLoadingScreen();
  	gblApplicationLaunch = true;
  	gblqrPreloginPwdVerified = false;
	frmMBPreLoginAccessesPin.show();
}

function checkTransactionRef(deviceID)
{
  	showLoadingScreen();
    var inputParam = {};
    if(deviceID != ""){
        gblQRPaymentDeviceId = deviceID;
    }
	var enCryptDeviceId = encryptData(gblQRPaymentDeviceId);
    kony.print("enCryptDeviceId :: " + enCryptDeviceId)
    inputParam["encryptDeviceId"] = enCryptDeviceId;

    inputParam["transactionId"] = gblQRPayData["qrtransRefID"];
    invokeServiceSecureAsync("QRPaymentInquiry", inputParam, callBackPromptPayTrnInq);

}

function callBackPromptPayTrnInq(status, resulttable) {
  try{
        if (status == 400) {
        	if (resulttable["opstatus"] == 0) {
          
                  if(resulttable["PromptPayTrnDS"]!=""||resulttable["PromptPayTrnDS"]!=null||resulttable["PromptPayTrnDS"]!=undefined){
                  var fromicon = resulttable["PromptPayTrnDS"][0]["fromBankCode"];
                  var originalAmountVal = resulttable["PromptPayTrnDS"][0]["transAmount"]; 
                  var numericVal = commaFormatted(originalAmountVal.substring(0,originalAmountVal.indexOf('.')));
                    kony.print("numericVal value   "+numericVal);
                  originalAmountVal = numericVal.substring(0,numericVal.indexOf('.')) + "<a href=''>"+ originalAmountVal.substring(originalAmountVal.indexOf('.'),originalAmountVal.length)+" "+kony.i18n.getLocalizedString("currencyThaiBaht")+"</a>";     
                    kony.print("original value   "+originalAmountVal);
                    //kony.print("original value   "+commaFormatted(originalAmountVal));
                  frmQRTransactionVerify.lblTransactionAmt.text = originalAmountVal;
                  frmQRTransactionVerify.imgTransNPbAckFrm.src = loadBankIcon(fromicon);
                  frmQRTransactionVerify.lblTransNPbAckFrmNum.text = "xxx-x-" + resulttable["PromptPayTrnDS"][0]["fromAcctNo"].substring(5,11) + "-x";                  
                  frmQRTransactionVerify.lblTransNPbAckFrmCustomerName.text = resulttable["PromptPayTrnDS"][0]["fromAcctNameEN"];
                    frmQRTransactionVerify.lblTransNPbAckFrmName.text = kony.i18n.getLocalizedString("lbl_QRFromAccount");
                    frmQRTransactionVerify.lblTransacttionResultHdr.text = kony.i18n.getLocalizedString("lb_QRTxnDetails");
                    frmQRTransactionVerify.lblSuccessTransaction.text = kony.i18n.getLocalizedString("Msg_QRVerifyTransRefSuccess");
                  //To Account
                  frmQRTransactionVerify.lblTransNPbAckToAccountName.text = resulttable["PromptPayTrnDS"][0]["toAcctName"];
                  var toicon = resulttable["PromptPayTrnDS"][0]["toBankCode"];
                  frmQRTransactionVerify.imgTransNPbAckTo.src = loadBankIcon(toicon);
                  frmQRTransactionVerify.lblTransNPbAckToName.text = resulttable["PromptPayTrnDS"][0]["toAcctName"];
                  var proxyType = resulttable["PromptPayTrnDS"][0]["proxyType"];
                  var proxyValue = resulttable["PromptPayTrnDS"][0]["proxyValue"];
                  var formattedProxyValue= "";
                  if(proxyType=="01"){
                    //CI
                    kony.print("Inside Proxy1");
                    frmQRTransactionVerify.lblTransNPbAckToName.text = kony.i18n.getLocalizedString("TR_LandingCitizen");
                    formattedProxyValue = maskCitizenID(proxyValue);
                    frmQRTransactionVerify.lblMorefields1.setVisibility(false);
                  }else if(proxyType=="02"){
                    //Mobile
                    frmQRTransactionVerify.lblTransNPbAckToName.text = kony.i18n.getLocalizedString("lbl_Header_Mobile_no");
                    formattedProxyValue = maskMobileNumber(proxyValue);
                    frmQRTransactionVerify.lblMorefields1.setVisibility(false);
                  }else if(proxyType=="03"){
                    //TAX ID
                  }else if(proxyType=="04"){
                    //E Wallet
                    formattedProxyValue = maskeWalletID(proxyValue);
                    frmQRTransactionVerify.lblMorefields1.setVisibility(false);
                  }else if(proxyType=="05"){
                    //Bill Payment
                    frmQRTransactionVerify.lblMorefields1.setVisibility(true);
                  }
                  frmQRTransactionVerify.lblTransNPbAckToNum.text = formattedProxyValue;
                  frmQRTransactionVerify.LabelRefId.text = kony.i18n.getLocalizedString("TRConfirm_RefNo")+": "+gblQRPayData["qrtransRefID"];
                  frmQRTransactionVerify.lblTransactiondate.text = kony.i18n.getLocalizedString("keyMBTransactionDate")+": "+resulttable["PromptPayTrnDS"][0]["postedDt"]+" "+resulttable["PromptPayTrnDS"][0]["postedTime"];
                  kony.print("Before Ffrm show");
                  frmQRTransactionVerify.imgTransNBpAckComplete.src="completeico.png";
                  frmQRTransactionVerify.FlexContainertwo.setVisibility(true);
				  frmQRTransactionVerify.imgTransNBpAckComplete.top = "15dp"
                  frmQRTransactionVerify.lblSuccessTransaction.setVisibility(true);
                  frmQRTransactionVerify.lblTransacttionResultHdr.setVisibility(true);
                  frmQRTransactionVerify.lblFailTransaction.setVisibility(false);
                  frmQRTransactionVerify.show();
                                }
                }else{
                    //failed case
                    var errCode = resulttable["errCode"]
                    if(errCode == "9003"){
                      var errMsg = kony.i18n.getLocalizedString("Msg_QRReceiverIDnotMatch");
                    }else{
                      var errMsg = kony.i18n.getLocalizedString("Msg_QRVerifyTransRefNotfound");
                    }

                    frmQRTransactionVerify.imgTransNBpAckComplete.src="iconnotcomplete.png";
                    frmQRTransactionVerify.FlexContainertwo.setVisibility(false);
                    frmQRTransactionVerify.lblSuccessTransaction.setVisibility(false);
                    frmQRTransactionVerify.lblTransacttionResultHdr.setVisibility(false);
                    frmQRTransactionVerify.lblFailTransaction.setVisibility(true);
					frmQRTransactionVerify.imgTransNBpAckComplete.top = "200dp"
                    frmQRTransactionVerify.lblFailTransaction.text = errMsg;
                    frmQRTransactionVerify.show();
                  }
                }
                dismissLoadingScreen();
             }catch(e){
              dismissLoadingScreen();
              kony.print("exception is QR"+e.message);
  }
}

