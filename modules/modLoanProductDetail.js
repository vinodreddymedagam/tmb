//Type your code here
function frmLoanProductDetailsPreshow() {
   // frmLoanProductDetails.lblProduceHeader.text = "";
    kony.print("@@@ending loan detail pre show")
}

function frmLoanProductDetailsPostshow() {
    kony.print("@@@Inside loan detail post show")
    frmLoanProductDetails.btnApply.text = kony.i18n.getLocalizedString("Loan_Apply");
    frmLoanProductDetails.btnCancel.text = kony.i18n.getLocalizedString("CAV06_btnCancel");
    frmLoanProductDetails.lblHeader.text = kony.i18n.getLocalizedString("LoanProductDetail");
    kony.print("@@@ending loan detail post show")
}

function frmLoanProductDetailsInit() {
  frmLoanProductDetails.preShow = frmLoanProductDetailsPreshow;
  frmLoanProductDetails.postShow = frmLoanProductDetailsPostshow;
  frmLoanProductDetails.btnCancel.onClick = showLoanProductListScreen;
  frmLoanProductDetails.btnBack.onClick = showLoanProductListScreen;
  frmLoanProductDetails.btnApply.onClick = navigateToLoanKYC;
  frmLoanProductDetails.onDeviceBack = doNothing;
}

function animateProductDetailCard() {}

function setDatatoProductDetail(productCode) {
    kony.print("@@@selected product:" + productCode);
    frmLoanProductDetails.lblProduceHeader.text = "";
  	frmLoanProductDetails.btnApply.onClick = navigateToLoanKYC;
    frmLoanProductDetails.segLoanProductData.removeAll();
    frmLoanProductDetails.flexHeader1.isVisible = false;
    frmLoanProductDetails.flexHeader2.isVisible = false;
    frmLoanProductDetails.imgLoanProduct.height = "60%";
    frmLoanProductDetails.imgLoanProduct.width = "75%";
	frmLoanProductDetails.imgLoanProduct.centerY = "40%";
  	frmLoanProductDetails.flxProductImage.skin = flexGreyBG;
    kony.print("@@@Going to if condition");
    //var product = "tmbsofast"
    if (productCode == "VP") { //  TMB So fast
        frmLoanProductDetails.imgLoanProduct.src = "sofast.png";
        frmLoanProductDetails.lblProduceHeader.text = kony.i18n.getLocalizedString("Loan_sofast_Header");
        kony.print("@@@Inside VP");
        var data = [{
            "imgBullet": "tickmark_grey.png",
            "richTextDetailPoints": kony.i18n.getLocalizedString("Loan_Sofast_Point1")
        }, {
            "imgBullet": "tickmark_grey.png",
            "richTextDetailPoints": kony.i18n.getLocalizedString("Loan_Sofast_Point2")
        }, {
            "imgBullet": "tickmark_grey.png",
            "richTextDetailPoints": kony.i18n.getLocalizedString("Loan_Sofast_Point3")
        }]
        kony.print("@@@Inside VP ending");
    } else if (productCode == "MS") { // TMB so chill
        frmLoanProductDetails.imgLoanProduct.src = "sochill.png";
        frmLoanProductDetails.lblProduceHeader.text = kony.i18n.getLocalizedString("Loan_SoChill_header");
        var data = [{
            "imgBullet": "tickmark_grey.png",
            "richTextDetailPoints": kony.i18n.getLocalizedString("Loan_SoChill_Point1")
        }, {
            "imgBullet": "tickmark_grey.png",
            "richTextDetailPoints": kony.i18n.getLocalizedString("Loan_SoChill_Point2")
        }, {
            "imgBullet": "tickmark_grey.png",
            "richTextDetailPoints": kony.i18n.getLocalizedString("Loan_SoChill_Point3")
        }]
    } else if (productCode == "VM") { // TMB so smart
        frmLoanProductDetails.imgLoanProduct.src = "sosmart.png";
        frmLoanProductDetails.lblProduceHeader.text = kony.i18n.getLocalizedString("Loan_SoSmart_header");
      	frmLoanProductDetails.btnApply.onClick = checkingProductNoFix;
        var data = [{
            "imgBullet": "tickmark_grey.png",
            "richTextDetailPoints": kony.i18n.getLocalizedString("Loan_SoSmart_Point1")
        }, {
            "imgBullet": "tickmark_grey.png",
            "richTextDetailPoints": kony.i18n.getLocalizedString("Loan_SoSmart_Point2")
        }, {
            "imgBullet": "tickmark_grey.png",
            "richTextDetailPoints": kony.i18n.getLocalizedString("Loan_SoSmart_Point3")
        }]
    } else if (productCode == "VT") { // TMB royal brass  topbrass
        frmLoanProductDetails.imgLoanProduct.src = "topbrass.png";
        frmLoanProductDetails.lblProduceHeader.text = kony.i18n.getLocalizedString("Loan_RoyalBrass_header");
        frmLoanProductDetails.lblProduceHeader1.text = kony.i18n.getLocalizedString("Loan_RoyalBrass_header1");
        frmLoanProductDetails.lblProduceHeader2.text = kony.i18n.getLocalizedString("Loan_RoyalBrass_header2");
        frmLoanProductDetails.flexHeader1.isVisible = true;
        frmLoanProductDetails.flexHeader2.isVisible = true;
        var data = [{
            "imgBullet": "tickmark_grey.png",
            "richTextDetailPoints": kony.i18n.getLocalizedString("Loan_RoyalBrass_Point1")
        }, {
            "imgBullet": "tickmark_grey.png",
            "richTextDetailPoints": kony.i18n.getLocalizedString("Loan_RoyalBrass_Point2")
        }, {
            "imgBullet": "tickmark_grey.png",
            "richTextDetailPoints": kony.i18n.getLocalizedString("Loan_RoyalBrass_Point3")
        }, {
            "imgBullet": "tickmark_grey.png",
            "richTextDetailPoints": kony.i18n.getLocalizedString("Loan_RoyalBrass_Point4")
        }, {
            "imgBullet": "tickmark_grey.png",
            "richTextDetailPoints": kony.i18n.getLocalizedString("Loan_RoyalBrass_Point5")
        }]
    } else if (productCode == "RC01") { // TMB TMB Ready Cash
        frmLoanProductDetails.imgLoanProduct.src = "ready.png";
        frmLoanProductDetails.lblProduceHeader.text = kony.i18n.getLocalizedString("Loan_Readycash_header");
        var data = [{
            "imgBullet": "tickmark_grey.png",
            "richTextDetailPoints": kony.i18n.getLocalizedString("Loan_Readycash_Point1")
        }, {
            "imgBullet": "tickmark_grey.png",
            "richTextDetailPoints": kony.i18n.getLocalizedString("Loan_Readycash_Point2")
        }, {
            "imgBullet": "tickmark_grey.png",
            "richTextDetailPoints": kony.i18n.getLocalizedString("Loan_Readycash_Point3")
        }]
    } else if (productCode == "C2G01") { // TMB cash to go
        frmLoanProductDetails.imgLoanProduct.src = "cashtogogif.gif";
      	frmLoanProductDetails.flxProductImage.skin = cashtogoBG;
        frmLoanProductDetails.lblProduceHeader.text = kony.i18n.getLocalizedString("Loan_cashtogo_header");
        frmLoanProductDetails.imgLoanProduct.height = "100%";
        frmLoanProductDetails.imgLoanProduct.width = "100%";
      	frmLoanProductDetails.imgLoanProduct.centerY = "50%";
        var data = [{
            "imgBullet": "tickmark_grey.png",
            "richTextDetailPoints": kony.i18n.getLocalizedString("Loan_cashtogo_Point1")
        }, {
            "imgBullet": "tickmark_grey.png",
            "richTextDetailPoints": kony.i18n.getLocalizedString("Loan_cashtogo_Point2")
        }, {
            "imgBullet": "tickmark_grey.png",
            "richTextDetailPoints": kony.i18n.getLocalizedString("Loan_cashtogo_Point3")
        }]
    }
    kony.print("@@@before setting the data")
    frmLoanProductDetails.segLoanProductData.setData(data);
    kony.print("@@@after setting data");
    frmLoanProductDetails.show();
}

function checkingProductNoFix(){
  	var inputparam = {};
	inputparam["activationCompleteFlag"] = "true";
	invokeServiceSecureAsync("customerAccountInquiry", inputparam, checkNoFixedAccountForCeditCardCallBack);
}

function checkNoFixedAccountForCeditCardCallBack(status, callBackResponse){
  var hasNoFixed = false;
  if (status == 400) {
    if (callBackResponse.opstatus == 0) {
      for (var i1 = 0; i1 < callBackResponse.custAcctRec.length; i1++) {
        if(callBackResponse.custAcctRec[i1].productID == "221"){
          hasNoFixed = true;
          break;
        }
      }
      if(!hasNoFixed){
        openOpenAccountPopup();
      }else{
        frmMBLoanKYC.show();
      }
      dismissLoadingScreen();
    }else{
      dismissLoadingScreen();
      showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
      return false;
    }
  }else{
    dismissLoadingScreen();
    showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
    return false;
  }
}

function openOpenAccountPopup(){
  var keyMsg = kony.i18n.getLocalizedString("Alert_Open_Nofixed");
  var keyContinue = kony.i18n.getLocalizedString("keyApplyNow");
  var keyCancel = kony.i18n.getLocalizedString("keyXferBtnCancel");
  var basicConf = {
    message: keyMsg,
    alertType: constants.ALERT_TYPE_CONFIRMATION,
    alertTitle: "",
    yesLabel: keyContinue,
    noLabel: keyCancel,
    alertHandler: directToOpenAccount
  };
  var pspConf = {};
  var infoAlert = kony.ui.Alert(basicConf, pspConf);
}

function directToOpenAccount(response){
  if(response == true){
    showLoadingScreen();
    gbl3dTouchAction = "";
    gblProdCode = "221";
    isCmpFlow = true;
    OnClickOpenNewAccount();
  }
}