var gblFromSource = "";
var gblFBFriendsAvailability = false;
var gblPhotoSource = "none";
var gblMixedCallsTrack = "";
var gblMixedCallsTrack1 = "";
var gblAddMoreRcTrack = 0;
var gblMAXRecipientCount = 0;
var gblMAXAccountCount = 0;
var deleteAccountType = "";
var gblMyFBdelinkTrack = false;
var gblEditFacebookSelection = false;
var gblFBListChanged = false;
var gblIsContactsFlow = false;
var gblRequestFromForm = null;
var gblAddContactFlow=false
var gblEditContactList=false;

function executeRecipientMB(){
  try{
    kony.print("@@@ In executeRecipientMB() @@@");

	//---DEF1264
    if(frmMyRecipientAddProfile.imgProfilePic.src!=null&&frmMyRecipientAddProfile.imgProfilePic.src!=undefined){	
        beforeOTPMB=frmMyRecipientAddProfile.imgProfilePic.src
        if(beforeOTPMB.indexOf("fbcdn")>=0)
        gblPhotoSource=frmMyRecipientAddProfile.imgProfilePic.src
        //added above code for fb recipients add flow
    }
	var inputParams ={};
	if(flowSpa){
 		//gblFlagTransPwdFlow=spaChnage;
 	 	if (gblTokenSwitchFlag == true) {
          inputParams["password"] = popOtpSpa.txttokenspa.text;
          inputParams["segmentIdVal"] = "MIB";
          inputParams["loginModuleId"] = "IB_HWTKN";
          inputParams["userStoreId"] = "DefaultStore";
          inputParams["segmentId"] = "segmentId";
          inputParams["channel"] = "InterNet Banking";
          inputParams["gblTokenSwitchFlag"] = gblTokenSwitchFlag; //future
          popOtpSpa.txttokenspa.text = "";
		}else {
          inputParams["password"]= popOtpSpa.txtOTP.text;
          popOtpSpa.txtOTP.text = "";
		}
	 }else{
       var passwd = "";
       if(gblAuthAccessPin == true){
         passwd = gblNum;
       }else{
         passwd = popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text; 
       }
        
       if (passwd == null || passwd == '') {
         setTransPwdFailedError(kony.i18n.getLocalizedString("emptyMBTransPwd"));
         return false;
       }
       inputParams["loginModuleId"] = "MB_TxPwd";
       inputParams["password"] = passwd;   //popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text;
       popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text = "";
	 }
      //alert(gblFlagTransPwdFlow);
      kony.application.dismissLoadingScreen();
    	kony.print("@@@gblFlagTransPwdFlow::::"+gblFlagTransPwdFlow);

      if(gblFlagTransPwdFlow == "editProfile"){
        if(gblRcBase64List != ""){
          inputParams["fileType"]="png";
          inputParams["personalizedId"]=gblRcPersonalizedIdList;
          inputParams["base64ImageString"]=gblRcBase64List;
          //gblRcPersonalizedIdList = currentRecipientDetails.rcId;
          //imageServiceCall();
        }
        inputParams["personalizedId"]= currentRecipientDetails["rcId"];
        inputParams["personalizedName"]= frmMyRecipientEditProfile.tbxRecipientName.text.replace(/^\s+|\s+$/g, "");
        if(gblRcImageUrlList[0] != undefined && gblRcImageUrlList[0] !=null)
          inputParams["personalizedPictureId"]= gblRcImageUrlList[0];//frmMyRecipientEditProfile.imgprofilepic.src,
        else
          inputParams["personalizedPictureId"]=null;
        
        inputParams["personalizedMailId"]= frmMyRecipientEditProfile.tbxEmail.text;
        inputParams["personalizedMobileNumber"]= stripDashPh(frmMyRecipientEditProfile.tbxMobileNo.text);
        inputParams["personalizedFacebookId"]= frmMyRecipientEditProfile.tbxFbID.text;
        inputParams["personalizedStatus"]= "Added";
        inputParams["gblTransactionType"]="2";
        inputParams["oldRcName"]=currentRecipientDetails["name"]//frmIBMyReceipentsAccounts.lblRcName.text
        inputParams["oldRcmobile"]=stripDashPh(currentRecipientDetails["mobile"])//frmIBMyReceipentsAccounts.lblRcMobileNo.text
        inputParams["oldRcEmail"]=currentRecipientDetails["email"]//frmIBMyReceipentsAccounts.lblRcEmail.text
        inputParams["oldFbId"]=currentRecipientDetails["facebook"]//frmIBMyReceipentsAccounts.lblRcFbId.text
        //invokeServiceSecureAsync("receipentEditService", inputParams, startExistingRcMBProfileEditServiceAsyncCallback);
        //startExistingRcMBProfileEditService("imageNotAdded");
        //startExistingRcMBProfileEditService();
      }else if(gblFlagTransPwdFlow == "addAccount"){
        if (isRecipientNew == false) {
          if(AddAccountList.length ==0){
            showAlert(kony.i18n.getLocalizedString("Receipent_alert_atleastonebankaccount"), kony.i18n.getLocalizedString("info"));
            return;
          }
          inputParams["personalizedAccList"] = completeRecepientAddAccount();
          inputParams["gblTransactionType"]="4";
          inputParams["oldRcName"]=currentRecipientDetails["name"]//frmIBMyReceipentsAccounts.lblRcName.text
          inputParams["oldRcmobile"]=stripDashPh(currentRecipientDetails["mobile"])//frmIBMyReceipentsAccounts.lblRcMobileNo.text
          inputParams["oldRcEmail"]=currentRecipientDetails["email"]//frmIBMyReceipentsAccounts.lblRcEmail.text
          inputParams["oldFbId"]=currentRecipientDetails["facebook"]//frmIBMyReceipentsAccounts.lblRcFbId.text
          //invokeServiceSecureAsync("receipentAddBankAccntService", inputParams, addAccountServiceCallbackMB);
        }else {
          if(gblRcBase64List != ""){
            gblRcPersonalizedIdList = frmMyRecipientAddAccConf.lblName.text; //DEF-11492
            kony.print("@@@gblRcPersonalizedIdList:::"+gblRcPersonalizedIdList);
            inputParams["fileType"]="png";
            inputParams["personalizedId"]=gblRcPersonalizedIdList;
            inputParams["base64ImageString"]=gblRcBase64List;
            //imageServiceCall();
          }else if(gblRcBase64ListFromContacts != "" && gblRcBase64ListFromContacts != undefined){
            inputParams["fileType"]="png";
            inputParams["personalizedId"]=frmMyRecipientAddAccConf.lblName.text;
            inputParams["base64ImageString"]=gblRcBase64ListFromContacts;
          }
          if(AddAccountList.length > 0){
            var totalDataRecipient = [];
            var name = frmMyRecipientAddProfile.tbxRecipientName.text;
            var PictureId = gblPhotoSource;//frmMyRecipientAddProfile.imgProfilePic.src;
            var Email = frmMyRecipientAddProfile.tbxEmail.text;
            var fbId = frmMyRecipientAddProfile.tbxFbID.text;
            var mobileNo = stripDashPh(frmMyRecipientAddProfile.tbxMobileNo.text);
            var recepientdata = [name, PictureId , Email, mobileNo, fbId, "Added","N"];
            gblRcPersonalizedIdList = name;
            totalDataRecipient.push(recepientdata);
            kony.print("@@@gblRcPersonalizedIdList:After::"+gblRcPersonalizedIdList);

            //var selectedRcId = callBackResponse["personalizedIdList"][0]["personalizedId"]
            var totalData = [];
            for (var i = 0; i < AddAccountList.length; i++) {
              var acctName = "";
              var BankName = AddAccountList[i].lblBankName.text;
              var Number = stripDashAcc(AddAccountList[i].lblAccountNo.text);
              var NickName = AddAccountList[i].lblNick.text;
              var bankCode = AddAccountList[i].bankCode;
              //var acctName = AddAccountList[i].lblAcctValue.text;
              if(AddAccountList[i] != undefined && AddAccountList[i].lblAcctValue != undefined){
                acctName = AddAccountList[i].lblAcctValue.text;
              }
              if(acctName == null || acctName == undefined || acctName == ""){
                acctName = "Not returned from GSB";
              }
              var bankData = [ crmId, "", bankCode, Number, NickName, "Added",acctName];
              totalData.push(bankData);
            }
            inputParams["personalizedAccList"] = totalData.toString()
            inputParams["receipentList"]= totalDataRecipient.toString()
            inputParams["oldRcName"]=name;
            inputParams["oldRcEmail"]=Email;
            inputParams["oldRcmobile"]=stripDashPh(mobileNo);
            inputParams["oldFbId"]=fbId;	
            inputParams["gblTransactionType"]="3";
            //startNewRcAddwithAccntServiceMB();
          }else{
            inputParams["receipentList"] = prepareNewRcListMB()
            inputParams["gblTransactionType"]="1";
            //addNewRecipientDetailsMB();		
            isRecipientNew = true;	
          }
        }
        //addAccountRecipentComplete();
      }else if(gblFlagTransPwdFlow == "addRcManually"){

        inputParams["receipentList"] = prepareNewRcListMB();
        inputParams["gblTransactionType"]="1";
        //addNewRecipientDetailsMB();
        isRecipientNew = true;
      }else if(gblFlagTransPwdFlow == "addRcContacts"){

        for(var i = 0; i < multiSelectedContacts.length; i++){
          var name = multiSelectedContacts[i].lblName;
          var mobile = addCrossPh(stripDashPh(multiSelectedContacts[i].lblMobile)); 
          var email = addCrossEmail(multiSelectedContacts[i].lblEmail);
          var facebook = multiSelectedContacts[i].lblFacebook;
          //	activityLogServiceCall("045", "", "01", "","Add", name, mobile, email, facebook, "");
        }
        var totaldata=transConfirmSelectContacts();
        inputParams["receipentList"]=totaldata.toString();
        inputParams["gblTransactionType"]="1";
        if(gblRcBase64List != ""){
          inputParams["fileType"]="png";
          inputParams["personalizedId"]=gblRcPersonalizedIdList;
          inputParams["base64ImageString"]=gblRcBase64List;
          //imageServiceCall();
        }
        //transConfirmSelectContacts();
      }else if(gblFlagTransPwdFlow == "addRcFacebook"){
        for(var i = 0; i < multiSelectedFacebook.length; i++){
          var name = multiSelectedFacebook[i].lblName;
          var mobile = addCrossPh(stripDashPh(multiSelectedFacebook[i].lblMobile));
          var email = addCrossEmail(multiSelectedFacebook[i].lblEmail);
          var facebook = multiSelectedFacebook[i].lblFacebook;
          //activityLogServiceCall("045", "", "01", "","Add", name, mobile, email, facebook, "");
        }
        var totaldata =transConfirmSelectFacebook();
        inputParams["receipentList"]=totaldata.toString();
        inputParams["gblTransactionType"]="1";
      }else if(gblFlagTransPwdFlow == "addTopup"){
        popTransactionPwd.dismiss();
        onClickNextMBApplyBB();
      }
	    
      showLoadingScreen();
      if(recipientAddFromTransfer){
        var productCode="";
        if(flowSpa){
          productCode = gblmbSpaselectedData.prodCode;
        }else{
          productCode = frmTransferLanding.segTransFrm.selectedItems[0].prodCode;
        }
        inputParams["productCode"] = productCode;
      }

      inputParams["serviceFlow"]="recipientsComp";
      invokeServiceSecureAsync("ExecuteMyRecipientAddService", inputParams, compositeServiceCallbackMB)
  }catch(e){
    kony.print("@@@ In executeRecipientMB() Exception:::"+e);
  }
}

function FacebookManualFolwNext()
{
//created this function for populating recp name & FB ID 
if(frmMyRecipientSelectFbID.segMyRecipient.selectedItems!=null||frmMyRecipientSelectFbID.segMyRecipient.selectedItems!=undefined){
	selectedFbID = frmMyRecipientSelectFbID.segMyRecipient.selectedItems[0]["lblUsername"];
	selectedFBUserName = frmMyRecipientSelectFbID.segMyRecipient.selectedItems[0]["lblName"];
	selectedFBImage=frmMyRecipientSelectFbID.segMyRecipient.selectedItems[0]["imgprofilepic"];//added for 16927
}
if(selectedFbID == null || selectedFbID == undefined){
	selectedFbID="";
}
if(selectedFBUserName == null || selectedFBUserName == undefined){
	selectedFBUserName="";
}
if(selectedFBImage == null || selectedFBImage == undefined){
	selectedFBImage="";
}
if(requestFromForm=="editprofile"){
	gblRequestFromForm ="editprofile";
	if(selectedFbID){
		frmMyRecipientEditProfile.tbxFbID.text = selectedFbID;
	}
	frmMyRecipientEditProfile.tbxRecipientName.text=selectedFBUserName;
	frmMyRecipientEditProfile.imgprofilepic.src=selectedFBImage;
	frmMyRecipientEditProfile.show();
}
else if(requestFromForm=="addprofile"){
	gblRequestFromForm ="addprofile";//added for populating recipient name in add flow
	if(selectedFbID){
		frmMyRecipientAddProfile.tbxFbID.text = selectedFbID;
	}
	frmMyRecipientAddProfile.tbxRecipientName.text=selectedFBUserName;
	frmMyRecipientAddProfile.imgProfilePic.src=selectedFBImage;
	frmMyRecipientAddProfile.show();
}
}

function isRecipientsNewFalse(){
		isChMyRecipientsRs = true;
		isChRecipientAccountsRs = true;
		frmMyRecipientAddAccComplete.lblName.text = currentRecipientDetails["name"];
		if(currentRecipientDetails["mobile"] == null || currentRecipientDetails["mobile"] == ""){
			frmMyRecipientAddAccComplete.hbox47502979411849.setVisibility(false);
		}else{
			frmMyRecipientAddAccComplete.hbox47502979411849.setVisibility(true);
			frmMyRecipientAddAccComplete.lblMobile.text = currentRecipientDetails["mobile"];
		}
		if(currentRecipientDetails["email"] == null || currentRecipientDetails["email"] == ""){
			frmMyRecipientAddAccComplete.hbox47502979411850.setVisibility(false);
		}else{
			frmMyRecipientAddAccComplete.hbox47502979411850.setVisibility(true);
			frmMyRecipientAddAccComplete.lblEmail.text = currentRecipientDetails["email"];
		}
		if(currentRecipientDetails["facebook"] == null || currentRecipientDetails["facebook"] == ""){
			frmMyRecipientAddAccComplete.hbox47502979411851.setVisibility(false);
		}else{
			frmMyRecipientAddAccComplete.hbox47502979411851.setVisibility(true);
			frmMyRecipientAddAccComplete.lblFbID.text = currentRecipientDetails["facebook"];
		}
		if(flowSpa)
		{
			frmMyRecipientAddAccComplete.imgprofilepic.base64= frmMyRecipientAddAccConf.imgprofilepic.base64;
		}
		else
		{
			frmMyRecipientAddAccComplete.imgprofilepic.src = frmMyRecipientAddAccConf.imgprofilepic.src;
		}
		frmMyRecipientAddAccComplete.segMyRecipientDetail.removeAll();
		frmMyRecipientAddAccComplete.lblAccounts.isVisible = false;
		if(AddAccountList.length > 0){
			frmMyRecipientAddAccComplete.segMyRecipientDetail.setData(AddAccountList);
			frmMyRecipientAddAccComplete.lblAccounts.isVisible = true;
		}
		if (frmMyRecipientAddAccConf.segMyRecipientDetail.data.length > 1) {
			frmMyRecipientAddAccComplete.lblAccounts.text = kony.i18n.getLocalizedString("Receipent_Accounts");
		}
		else {
			frmMyRecipientAddAccComplete.lblAccounts.text = kony.i18n.getLocalizedString("Receipent_Account");
		}
		frmMyRecipientAddAccComplete.show();
}
function newrecipientaddflowcall(resulttable){
  try{
    kony.print("@@@ In newrecipientaddflowcall() @@@");
    isChMyRecipientsRs = true;
    isChRecipientAccountsRs = true;
    frmMyRecipientAddAccComplete.lblName.text = frmMyRecipientAddAccConf.lblName.text;
    if(frmMyRecipientAddAccConf.lblMobile.text == null || frmMyRecipientAddAccConf.lblMobile.text == ""){
      frmMyRecipientAddAccComplete.hbox47502979411849.setVisibility(false);
    }else{
      frmMyRecipientAddAccComplete.hbox47502979411849.setVisibility(true);
      frmMyRecipientAddAccComplete.lblMobile.text = frmMyRecipientAddAccConf.lblMobile.text;
    }
    if(frmMyRecipientAddAccConf.lblEmail.text == null || frmMyRecipientAddAccConf.lblEmail.text == ""){
      frmMyRecipientAddAccComplete.hbox47502979411850.setVisibility(false);
    }else{
      frmMyRecipientAddAccComplete.hbox47502979411850.setVisibility(true);
      frmMyRecipientAddAccComplete.lblEmail.text = frmMyRecipientAddAccConf.lblEmail.text;
    }
    if(frmMyRecipientAddAccConf.lblFbIDstudio5.text == null || frmMyRecipientAddAccConf.lblFbIDstudio5.text == ""){//Modified by Studio Viz
      frmMyRecipientAddAccComplete.hbox47502979411851.setVisibility(false);
    }else{
      frmMyRecipientAddAccComplete.hbox47502979411851.setVisibility(true);
      frmMyRecipientAddAccComplete.lblFbID.text = frmMyRecipientAddAccConf.lblFbIDstudio5.text;//Modified by Studio Viz
    }
    if(flowSpa){
      frmMyRecipientAddAccComplete.imgprofilepic.src =   gblRcImageUrlConstant + "&personalizedId=" + resulttable["personalizedIdList"][0]["personalizedId"];
    }else{
      if(frmMyRecipientAddAccConf.imgprofilepic.rawBytes != null){
        frmMyRecipientAddAccComplete.imgprofilepic.rawBytes = frmMyRecipientAddAccConf.imgprofilepic.rawBytes;
      }else{
        frmMyRecipientAddAccComplete.imgprofilepic.src = frmMyRecipientAddAccConf.imgprofilepic.src;
      }
      if(gblAddContactFlow==true){
        frmMyRecipientAddAccComplete.imgprofilepic.base64 = frmMyRecipientAddAccConf.imgprofilepic.base64;
      }
    }
    frmMyRecipientAddAccComplete.segMyRecipientDetail.removeAll();
    frmMyRecipientAddAccComplete.lblAccounts.isVisible = false;
    if(AddAccountList.length > 0){
      frmMyRecipientAddAccComplete.segMyRecipientDetail.setData(AddAccountList);
      frmMyRecipientAddAccComplete.lblAccounts.isVisible = true;
    }
    frmMyRecipientAddAccComplete.show();
    if(flowSpa){
      if(profilePicFlag == "picture"){
        var perID=resulttable["personalizedIdList"][0]["personalizedId"]
        startImageServiceCallRecipientsSpa(perID);
      }
    }else{

    }
    isRecipientNew = true;
    kony.application.dismissLoadingScreen();
  }catch(e){
    kony.print("@@@ In newrecipientaddflowcall() Exception:::"+e);
  }
}


function compositeServiceCallbackMB(status,resulttable){
  try{
    kony.print("@@@ In compositeServiceCallbackMB() @@@");
	if (status == 400){ 
	isChRecipientAccountsRs = true;
	isChMyRecipientsRs = true;
	
	if(resulttable["opstatus"]==0){
      if(flowSpa){
        gblRetryCountRequestOTP = "0";
        popOtpSpa.dismiss();
      }else{
        popupTractPwd.dismiss();
        onClickCancelAccessPin();
      }
      if (gblFlagTransPwdFlow != "addAccount"){
        popupTractPwd.dismiss();
        onClickCancelAccessPin();
      }
      if(gblFlagTransPwdFlow == "editProfile"){
        if(flowSpa){
          frmMyRecipientAddAccComplete.imgprofilepic.src = frmMyRecipientAddAccConf.imgprofilepic.src;
          if(profilePicFlag == "picture"){
            var perID=resulttable["Results"][0]["personalizedId"];
            startImageServiceCallRecipientsSpa(perID);
          }
        }
        isChMyRecipientsRs = true;
        isChRecipientAccountsRs = true;
        kony.application.dismissLoadingScreen();
        frmMyRecipientDetail.show();
      }else if(gblFlagTransPwdFlow == "addAccount"){
        if (isRecipientNew == false) {
          isRecipientsNewFalse()
        }else{
          if(AddAccountList.length > 0){
            gblRcImageUrlConstant="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/ImageRender?billerId=&crmId=Y";

            if(flowSpa){
              frmMyRecipientAddAccComplete.imgprofilepic.src =   gblRcImageUrlConstant + "&personalizedId=" + resulttable["personalizedIdList"][0]["personalizedId"];
              if(profilePicFlag == "picture")
              {
                var perID=resulttable["personalizedIdList"][0]["personalizedId"]
                startImageServiceCallRecipientsSpa(perID);
              }
            }
            if(recipientAddFromTransfer){//enh069
              //DEF1330 Data is not cleared if the user tries to add new recipient through transfers flow   START
              gblTrasSMS = 0;
              gblTransEmail = 0;
              ResetTransferHomePage();
              enteringAccountNumberVisbilty(false);
              makeWidgetsBelowTransferFeeButtonVisible(false);
              //DEF1330 Data is not cleared if the user tries to add new recipient through transfers flow  END 
              var accountData = frmMyRecipientAddAccConf.segMyRecipientDetail.data[0];
              var isTransferAllowed=resulttable["isTransferAllowed"];
              if(accountData["lblAcctValue"]["text"] != undefined)
                gblSmartAccountName= accountData["lblAcctValue"]["text"];
              else
                gblSmartAccountName= accountData["lblAcctValue"];
              if(gblSmartAccountName == null || gblSmartAccountName == undefined || gblSmartAccountName == "NONE" || gblSmartAccountName == "NA"){
                gblSmartAccountName="";
              }
              if(accountData["lblBankName"]["text"] != undefined)
                gblBANKREF = accountData["lblBankName"]["text"];
              else
                gblBANKREF = accountData["lblBankName"];
              gblisTMB = accountData["bankCode"];
              gblSelectedRecipentName=frmMyRecipientAddAccConf.lblName.text;

              if(frmMyRecipientAddAccConf.imgprofilepic.rawBytes != null){
                frmMyRecipientAddAccComplete.imgprofilepic.rawBytes = frmMyRecipientAddAccConf.imgprofilepic.rawBytes;
                if(flowSpa)
                  frmMyRecipientAddAccComplete.imgprofilepic.src =   gblRcImageUrlConstant + "&personalizedId=" + resulttable["personalizedIdList"][0]["personalizedId"];
              }else{
                frmMyRecipientAddAccComplete.imgprofilepic.src = frmMyRecipientAddAccConf.imgprofilepic.src;
                if(flowSpa)
                  frmMyRecipientAddAccComplete.imgprofilepic.src =   gblRcImageUrlConstant + "&personalizedId=" + resulttable["personalizedIdList"][0]["personalizedId"];
              }
              frmTransferLanding.lblTranLandToName.text = accountData["lblNick"]["text"];
              frmTransferLanding.lblTranLandToAccountNumber.text = accountData["lblAccountNo"]["text"];
              //frmTransferLanding.imgTranLandTo.isVisible=true;
              gblXferPhoneNo = frmMyRecipientAddAccConf.lblMobile.text;
              gblXferEmail = frmMyRecipientAddAccConf.lblEmail.text;
              enteringAccountNumberVisbilty(false);
              if(isTransferAllowed == "N"){
                alert(kony.i18n.getLocalizedString("fromAccNotEligible"));
                getTransferFromAccounts();
                return 
              }
              //Fix for DEF1151
              if (flowSpa) {
                gblspaSelIndex = gblnormSelIndex;
              }
              frmTransferLanding.show();

            }else{
              isChRecipientAccountsRs = true;
              frmMyRecipientAddAccComplete.lblName.text = frmMyRecipientAddAccConf.lblName.text;
              if(frmMyRecipientAddAccConf.lblMobile.text == null || frmMyRecipientAddAccConf.lblMobile.text == ""){
                frmMyRecipientAddAccComplete.hbox47502979411849.setVisibility(false);
              }else{
                frmMyRecipientAddAccComplete.hbox47502979411849.setVisibility(true);
                frmMyRecipientAddAccComplete.lblMobile.text = frmMyRecipientAddAccConf.lblMobile.text;
              }
              if(frmMyRecipientAddAccConf.lblEmail.text == null || frmMyRecipientAddAccConf.lblEmail.text == ""){
                frmMyRecipientAddAccComplete.hbox47502979411850.setVisibility(false);
              }else{
                frmMyRecipientAddAccComplete.hbox47502979411850.setVisibility(true);
                frmMyRecipientAddAccComplete.lblEmail.text = frmMyRecipientAddAccConf.lblEmail.text;
              }
              if(frmMyRecipientAddAccConf.lblFbIDstudio5.text == null || frmMyRecipientAddAccConf.lblFbIDstudio5.text == ""){//Modified by Studio Viz
                frmMyRecipientAddAccComplete.hbox47502979411851.setVisibility(false);
              }else{
                frmMyRecipientAddAccComplete.hbox47502979411851.setVisibility(true);
                frmMyRecipientAddAccComplete.lblFbID.text = frmMyRecipientAddAccConf.lblFbIDstudio5.text;//Modified by Studio Viz
              }
              if(frmMyRecipientAddAccConf.imgprofilepic.rawBytes != null){
                frmMyRecipientAddAccComplete.imgprofilepic.rawBytes = frmMyRecipientAddAccConf.imgprofilepic.rawBytes;
                if(flowSpa)
                  frmMyRecipientAddAccComplete.imgprofilepic.src =   gblRcImageUrlConstant + "&personalizedId=" + resulttable["personalizedIdList"][0]["personalizedId"];
              }else{
                frmMyRecipientAddAccComplete.imgprofilepic.src = frmMyRecipientAddAccConf.imgprofilepic.src;
                if(flowSpa)
                  frmMyRecipientAddAccComplete.imgprofilepic.src =   gblRcImageUrlConstant + "&personalizedId=" + resulttable["personalizedIdList"][0]["personalizedId"];
              }
              setToAddDatatoManualConfirmationSegMB();
            }
            kony.application.dismissLoadingScreen();							
            //startNewRcAddwithAccntServiceMB();
          }else{
            newrecipientaddflowcall(resulttable)	
          }
        }
        //addAccountRecipentComplete();
      }else if(gblFlagTransPwdFlow == "addRcManually"){
        newrecipientaddflowcall(resulttable);
      }else if(gblFlagTransPwdFlow == "addRcContacts"){
        var responseData = resulttable["Results"];
        if (responseData.length > 0) {
          //kony.application.dismissLoadingScreen();
          var CurrForm = kony.application.getCurrentForm();
          if(CurrForm.id == "frmMyRecipientSelectContactsConf"){
            var rcList = "";
            for(var i = 0; i < multiSelectedContacts.length ; i++){
              if(i == (multiSelectedContacts.length - 1)){
                rcList = rcList + multiSelectedContacts[i]["lblName"];
              }else{
                rcList = rcList + multiSelectedContacts[i]["lblName"] + ", ";
              }
            }
            //sendDeleteNotification("", "", "", rcList, "", "", "recipientsAdd", "");
            frmMyRecipientSelectContactsComp.segment24733076528972.setData(multiSelectedContacts);
            kony.application.dismissLoadingScreen();
            frmMyRecipientSelectContactsComp.show();
            isChMyRecipientsRs = true;
            isChRecipientAccountsRs = true;
          }else if(CurrForm.id == "frmMyRecipientSelectFBConf"){
            var rcList = "";
            for(var i = 0; i < multiSelectedFacebook.length ; i++){
              if(i == (multiSelectedFacebook.length - 1)){
                rcList = rcList + multiSelectedFacebook[i]["lblName"];
              }else{
                rcList = rcList + multiSelectedFacebook[i]["lblName"] + ", ";
              }
            }
            //sendDeleteNotification("", "", "", rcList, "", "", "recipientsAdd", "");
            frmMyRecipientSelectFBComp.sgmntFBFrndsRecpList.setData(multiSelectedFacebook);
            kony.application.dismissLoadingScreen();
            frmMyRecipientSelectFBComp.show();
            isChMyRecipientsRs = true;
            isChRecipientAccountsRs = true;
          }
        }else{
          //kony.application.dismissLoadingScreen();
          kony.application.dismissLoadingScreen();
          showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_Error"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
          //alert(kony.i18n.getLocalizedString("Receipent_alert_Error"));
        }
        kony.application.dismissLoadingScreen();
        //transConfirmSelectContacts();
      }else if(gblFlagTransPwdFlow == "addRcFacebook"){
        var responseData = resulttable["Results"];
        if (responseData.length > 0) {
          //kony.application.dismissLoadingScreen();
          var CurrForm = kony.application.getCurrentForm();
          if(CurrForm.id == "frmMyRecipientSelectContactsConf"){
            var rcList = "";
            for(var i = 0; i < multiSelectedContacts.length ; i++){
              if(i == (multiSelectedContacts.length - 1)){
                rcList = rcList + multiSelectedContacts[i]["lblName"];
              }else{
                rcList = rcList + multiSelectedContacts[i]["lblName"] + ", ";
              }
            }
            //sendDeleteNotification("", "", "", rcList, "", "", "recipientsAdd", "");
            frmMyRecipientSelectContactsComp.segment24733076528972.setData(multiSelectedContacts);
            frmMyRecipientSelectContactsComp.show();
            isChMyRecipientsRs = true;
            isChRecipientAccountsRs = true;
            if(gblRcBase64List.length > 0){
              imageServiceCall();
            }
          }else if(CurrForm.id == "frmMyRecipientSelectFBConf"){
            var rcList = "";
            for(var i = 0; i < multiSelectedFacebook.length ; i++){
              if(i == (multiSelectedFacebook.length - 1)){
                rcList = rcList + multiSelectedFacebook[i]["lblName"];
              }else{
                rcList = rcList + multiSelectedFacebook[i]["lblName"] + ", ";
              }
            }
            //sendDeleteNotification("", "", "", rcList, "", "", "recipientsAdd", "");
            frmMyRecipientSelectFBComp.sgmntFBFrndsRecpList.setData(multiSelectedFacebook);
            kony.application.dismissLoadingScreen();
            frmMyRecipientSelectFBComp.show();
            isChMyRecipientsRs = true;
            isChRecipientAccountsRs = true;
          }
        }else {
          kony.application.dismissLoadingScreen();
          showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_Error"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
        }
        kony.application.dismissLoadingScreen();
        //transConfirmSelectFacebook();
      }
	}else if (resulttable["opstatus"] == 8005){
        if(flowSpa){
          if(resulttable["errCode"] == "VrfyOTPErr00002"){
            popOtpSpa.dismiss();
            kony.application.dismissLoadingScreen();
            popTransferConfirmOTPLock.show();
          }
          else if(resulttable["errCode"] == "VrfyOTPErr00005"){
            popOtpSpa.lblTokenMsg.text = kony.i18n.getLocalizedString("wrongOTP");
            popOtpSpa.txttokenspa.text="";
            kony.application.dismissLoadingScreen();
          }
          else if(resulttable["errCode"] == "VrfyOTPErr00001"){
            if(gblTokenSwitchFlag){
              popOtpSpa.lblTokenMsg.text = kony.i18n.getLocalizedString("wrongOTP");
              popOtpSpa.txttokenspa.text="";
            }else{
              popOtpSpa.lblPopupTract2Spa.text = kony.i18n.getLocalizedString("wrongOTP");
              popOtpSpa.lblPopupTract4Spa.text = "";
            }
            kony.application.dismissLoadingScreen();
          }else{
            if(resulttable["errMsg"] != undefined){
              alert(resulttable["errMsg"]);
            }else{
              alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
            }
            kony.application.dismissLoadingScreen();
          }
        }else{
          if(resulttable["errCode"] == "VrfyTxPWDErr00003" || resulttable["errCode"] == "VrfyAcPWDErr00003"){
            if(gblAuthAccessPin == true) { 
                onClickCancelAccessPin();
                gotoUVPINLockedScreenPopUp(); 
            } else {
              showTranPwdLockedPopup();
            }             
            
          }else {
             if(gblAuthAccessPin == true){
               kony.print("invalid pin"); //To do : set red skin to enter access pin
               popupEnterAccessPin.lblWrongPin.setVisibility(true);
               resetAccessPinImg(resulttable["badLoginCount"]);
             }
             else{
                setTransPwdFailedError(kony.i18n.getLocalizedString("invalidTxnPwd"))  
             } 
             //setTransPwdFailedError(kony.i18n.getLocalizedString("invalidTxnPwd"));
          }
        }
        kony.application.dismissLoadingScreen();
      }else{
        if(flowSpa){
          popOtpSpa.dismiss();
        }else{
          popupTractPwd.dismiss();
          onClickCancelAccessPin();
        }
        kony.application.dismissLoadingScreen();
        if(resulttable["errMsg"] != undefined){
          alert(resulttable["errMsg"]);
        }else{
          alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
        }			
      }
      kony.application.dismissLoadingScreen();
    }
  }catch(e){
    kony.print("@@@ In compositeServiceCallbackMB() Exception:::"+e);
  }
}
function imageServiceCall(){
		showLoadingScreen();
		if (!gblIsContactsFlow && gblDeviceInfo.name == "android") {
			gblRcBase64List = ImageResizer.reduceImageSize(gblRcBase64List);
			gblRcBase64List = gblRcBase64List.replace(/[\n\r\s\f\t\v]+/g, '');
		}
		gblIsContactsFlow = false;
		gblPhotoSource = "png";
		inputParam = {
		fileType : gblPhotoSource,
		personalizedId : gblRcPersonalizedIdList,
		base64ImageString : gblRcBase64List
		};
		invokeServiceSecureAsync("imageUploadService", inputParam, imageServiceCallback)
}


function imageServiceCallback(status, callBackResponse){
	if (status == 400) {
		if (callBackResponse["uploadOpStatus"] == "Upload Success") {
			kony.application.dismissLoadingScreen();
			if(flowSpa)
			{
			
				if( spaChnage == "editProfile"){
					if(currentRecipientDetails["pic"] == "avatar_dis.png"){
						gblRcImageUrlList[0] = "";
					}
					else{
						gblRcImageUrlList[0] = currentRecipientDetails.pic.split("/ImageRender")[1];
					}
				    startExistingRcMBProfileEditService("imageAdded");
					}
					else
					 {
									kony.application.dismissLoadingScreen();
									//alert(kony.i18n.getLocalizedString("KeyUploadFailed"));
									showAlertRcMB(kony.i18n.getLocalizedString("KeyUploadFailed"), kony.i18n.getLocalizedString("info"), "info")
					 }
			}
			else
			{
				if(gblFlagTransPwdFlow == "editProfile" ){
				
					if(currentRecipientDetails["pic"] == "avatar_dis.png"){
						gblRcImageUrlList[0] = "";
					}
					else{
					
						gblRcImageUrlList[0] = currentRecipientDetails.pic.split("/ImageRender")[1];
					}
				    startExistingRcMBProfileEditService("imageAdded");
				}
				
			}
			
		}
		else {
					kony.application.dismissLoadingScreen();
					//alert(kony.i18n.getLocalizedString("KeyUploadFailed"));
					showAlertRcMB(kony.i18n.getLocalizedString("KeyUploadFailed"), kony.i18n.getLocalizedString("info"), "info")
				}
		
	}
	else {
		kony.application.dismissLoadingScreen();
		//kony.application.dismissLoadingScreen();
		if (status == 300) {
			//alert(kony.i18n.getLocalizedString("ECGenericError"));
			showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
		}
	}
}


//myRecipientsRs cache for recpients response
//recipientAccountsRs cache for accounts response
//get crmid
var globalRcListCache = [];
var globalRcAccListCache = [];
var AccountList = [];
var gblAddMoreFromContactsCache = [];
gblAddMoreFromContactsCache.length = 0;
var gblAddMoreFromFacebookCache = [];
gblAddMoreFromFacebookCache.length = 0;
var gblAddMoreFromManualCache = [];
gblAddMoreFromManualCache.length = 0;
//var crmId = " ";
var isSearched = null;
var showList = new Array();
//var isChMyRecipientsRs = true;

//function checkTransAllowStatus(addFrom) {
//	if (MB_USER_STATUS_ID == 03) {
//		alert("Your [OTP/ TXN Password] is locked, not allow to make new transaction");
//	}
//	else {
//		if (addFrom == "contacts") {
//			frmMyRecipientSelectContacts.show();
//		}
//		if (addFrom == "facebook") {
//			frmMyRecipientSelectFacebook.show();
//		}
//		if (addFrom == "manually") {
//			frmMyRecipientAddProfile.show();
//		}
//	}
//}

/**
 * (final comment)
 * Method to check number of recipients under a user to disable adding more recipients
 * @returns {}
 */
//function recipientLimitCheck() {
//	if (myRecipientsRs.length == gblMAXRecipientCount) {
//		frmMyRecipients.button47428498625.setEnabled(false);
//		frmMyRecipients.btnEmailto.setEnabled(false);
//		frmMyRecipients.btnPhoto.setEnabled(false);
//	}
//	else {
//		frmMyRecipients.button47428498625.setEnabled(true);
//		frmMyRecipients.btnEmailto.setEnabled(true);
//		frmMyRecipients.btnPhoto.setEnabled(true);
//	}
//}

//function checkNoOfRecipientsMaxTemp() {
//	
//	
//	if(gblUserLockStatusMB == gblFinancialTxnMBLock){
//		frmMyRecipients.btnEmailto.onClick = alertUserStatusLocked;
//		frmMyRecipients.btnPhoto.onClick = alertUserStatusLocked;
//		frmMyRecipients.button47428498625.onClick = alertUserStatusLocked;
//	}
//	else {
//		frmMyRecipients.btnEmailto.onClick = callAddManually;
//		frmMyRecipients.btnPhoto.onClick = callFaceBook;
//		frmMyRecipients.button47428498625.onClick = callContacts;
//	}
//}

function checkNoOfRecipientsMax(eventobject) {
  try{
    kony.print("@@@ In checkNoOfRecipientsMax() @@@");
    
	if (myRecipientsRs.length >= gblMAXRecipientCount) {
      alert99LimitReached();
      return;
	}
	if(flowSpa){
      if(gblIBFlowStatus == "04"){
        popTransferConfirmOTPLock.show()
        return;
      }else {
        if(eventobject.id == "btnEmailto"){
          callAddManually();
        }
        if(eventobject.id == "btnPhoto"){
          callFaceBook();
        }
        if(eventobject.id == "button47428498625"){
          callContacts();
        }
      }
	}else{
      if(gblUserLockStatusMB == "03" || gblUserLockStatusMB == gblFinancialTxnMBLock){
        alertUserStatusLocked();
        return;
      }else {
        if(eventobject.id == "btnEmailto"){
          callAddManually();
        }
        if(eventobject.id == "btnPhoto"){
          callFaceBook();
        }
        if(eventobject.id == "button47428498625"){
          //#ifdef android
          checkContactsPermissionsForMyRecipient();	
          //#else
          callContacts();
          //#endif		
        }
      }
	}
  }catch(e){
    kony.print("@@@ In checkNoOfRecipientsMax() Exception:::"+e);
  }
	
	
}


function checkContactsPermissionsForMyRecipient(){
   var options = {};
   var result = kony.application.checkPermission(kony.os.RESOURCE_CONTACTS, options);	
   kony.print("checkContactsPermissionsForMyRecipient result ###########  "+result.status);
   if (result.status == kony.application.PERMISSION_DENIED){
      kony.print("checkContactsPermissionsForMyRecipient Permission denied  ");
	  if (result.canRequestPermission){
         kony.application.requestPermission(kony.os.RESOURCE_CONTACTS, reqContactsPermissionCallback);
       }else{
         var messageErr="App cannot proceed further unless the required permisssions are granted!" //Need to chk with customer on msg text
		 alert(messageErr);
       }
   } else if (result.status == kony.application.PERMISSION_GRANTED) {
      kony.print("checkContactsPermissionsForMyRecipient Permission granted  ");
      kony.print("Permission allowed.");
      callContacts();    
   } else if (result.status == kony.application.PERMISSION_RESTRICTED) {
      kony.print("checkContactsPermissionsForMyRecipient Permission restricted ");
      var messageErr="App cannot proceed further unless the required permisssions are granted!" //Need to chk with customer on msg text
	  alert(messageErr);
   }
   
    function reqContactsPermissionCallback(response) {
        kony.print("reqContactsPermissionCallback "+result.status);
		if (response.status == kony.application.PERMISSION_GRANTED) {
			kony.print("reqContactsPermissionCallback-PERMISSION_GRANTED: " + JSON.stringify(response));
             kony.print("Permission allowed.");
             callContacts();        
		} else if (response.status == kony.application.PERMISSION_DENIED) {
			kony.print("reqContactsPermissionCallback-PERMISSION_DENIED: " + JSON.stringify(response));
			var messageErr="App cannot proceed further unless the required permisssions are granted!" //Need to chk with customer on msg text
		    alert(messageErr);
		}
	}
}

//Commented below code to use Kony API in place of FFI to request permissions
/*
function chkRunTimePermissionsForContactMyRecipient(){
      if(isAndroidM()){
            kony.print(" CALLIING PERMISSIONS FFI....");
            var permissionsArr=[gblPermissionsJSONObj.CONTACTS_GROUP];
            //Creates an object of class 'PermissionFFI'
            var RuntimePermissionsChkFFIObject = new MarshmallowPermissionChecks.RuntimePermissionsChkFFI();
            //Invokes method 'checkpermission' on the object
            RuntimePermissionsChkFFIObject.checkPermissions(permissionsArr,null,callBackMarshmallowPermissionCheckMyRecipient);
      }else{
          callContacts();
      }
}

function callBackMarshmallowPermissionCheckMyRecipient(result) {
	if(result == "1"){
		kony.print("IN FINAL CALLBACK PERMISSION IS AVAIALABLE");
		callContacts();
		
	}else{
		kony.print("IN FINAL CALLBACK PERMISSION IS UNAVAIALABLE");
		var messageErr="App cannot proceed further unless the required permisssions are granted!" //Need to chk with customer on msg text
		alert(messageErr);
	}
}*/


function callFaceBook() {
	if(flowSpa)
	{
	//getFriendListFromFacebook("0")
	frmMyRecipientSelectFacebook.show();
	requestFromForm = "addFacebook";
	}
	else
	{
	frmMyRecipientSelectFacebook.show();
	requestFromForm = "addFacebook";
	}
}

function callContacts() {
	frmMyRecipientSelectContacts.show();
}

function callAddManually() {
	frmMyRecipientAddProfile.tbxRecipientName.text = "";
	frmMyRecipientAddProfile.tbxMobileNo.text = "";
	frmMyRecipientAddProfile.tbxEmail.text = "";
	frmMyRecipientAddProfile.tbxFbID.text = "";
	frmMyRecipientAddProfile.imgProfilePic.src = "avatar.png";
	frmMyRecipientAddProfile.show();
}

/**
 * (final comment)
 * Method to list recipeints in the main screen
 * @returns {}
 */
function frmMyRecipientsListing() {
	
	if (true) { //info: if recipientlist is changed
		startReceipentListingServiceMB();
		isChMyRecipientsRs = false;
		gblRequestFromForm=null;
	}
	else {
		
 		frmMyRecipients.segMyRecipient.setData(myRecipientsRs);
	}
}

/**
 * (final comment)
 * service call and callback to "receipentListingService" to list recipients in the main screen
 * @returns {}
 */
function startReceipentListingServiceMB() {
	
	var inputParams = {};
    showLoadingScreen();
    frmMyRecipients.lblNoRecipientfound.isVisible = false;
	invokeServiceSecureAsync("receipentListingService", inputParams, startReceipentListingServiceAsyncCallbackMB);
}

function startReceipentListingServiceAsyncCallbackMB(status, callBackResponse) {
  try{
    kony.print("@@@ In startReceipentListingServiceAsyncCallbackMB() @@@");
	if (status == 400) {
      AddAccountList=[];
      AccountList=[];
      gblEditContactList=false;
      frmMyRecipients.lblNoRecipientfound.isVisible = false;
      if (callBackResponse["opstatus"] == "0") {
        //var responseData = callBackResponse["Results"];
        gblMAXRecipientCount = callBackResponse["recipientMaxCount"];
        gblMAXAccountCount = callBackResponse["recipientAccountMaxCount"];
        MAX_BANK_ACCNT_BULK_ADD = callBackResponse["recipientBulkAccntAddCount"];
        MAX_RECIPIENT_BULK_COUNT=callBackResponse["recipientBulkMaxCount"]
        if(callBackResponse["crmId"] != null && callBackResponse["crmId"] != undefined){
          gblcrmId = callBackResponse["crmId"];
        }
        if (callBackResponse["Results"].length > 0) {
          //recipientLimitCheck();
          populateRecipients(callBackResponse["Results"]);
          kony.application.dismissLoadingScreen();
        }else {
          myRecipientsRs.length = 0;
          frmMyRecipients.segMyRecipient.removeAll();
          kony.application.dismissLoadingScreen();
          gblMixedCallsTrack = "done";
          frmMyRecipients.lblNoRecipientfound.isVisible = true;
          frmMyRecipients.lblNoRecipientfound.text =  kony.i18n.getLocalizedString("Receipent_alert_NoRecipientfound");
        }
      }else {
        kony.application.dismissLoadingScreen();
        gblMixedCallsTrack = "done";
      }
      if(gblMixedCallsTrack == ""){
        gblMixedCallsTrack = "done";
      }else{
        kony.application.dismissLoadingScreen();
      }
	}else {
      if (status == 300) {
        kony.application.dismissLoadingScreen();
      }
	}
  }catch(e){
    kony.print("@@@ In startReceipentListingServiceAsyncCallbackMB() Exception:::"+e);
  }
}

function populateRecipients(collectionData) {
  try{
	kony.print("@@@ In populateRecipients() @@@");
    kony.print("@@@collectionData:::"+JSON.stringify(collectionData));
	myRecipientsRs.length = 0;
	var skinFav = null;
	var mobile = null;
	var email = null;
	var fb = null;
	var pic = null;
	var arrow = "";
	var randomnum = Math.floor((Math.random()*10000)+1); 
  	var widthFix = 84; 
    
    if(gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "android"){
      widthFix = 65; 
    }
	gblRcImageUrlConstant="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/ImageRender?billerId=&crmId=Y";
	if(collectionData.length > 0){
      for (var i = 0; i < collectionData.length; i++) {
        skinFav = "btnStarGrey";
        mobile = "mobile2.png";
        email = "email2.png";
        fb = "facebook2.png";
        arrow = "navarrow3.png";
        pic = gblRcImageUrlConstant + "&personalizedId=" + collectionData[i]["personalizedId"]+"&rr="+randomnum;
        if (collectionData[i]["favoriteFlag"] == "Y") skinFav = "btnStarFoc";
        if (collectionData[i]["personalizedMobileNumber"] != null && collectionData[i]["personalizedMobileNumber"].trim()!="" && collectionData[i]["personalizedMobileNumber"]!="undefined" && collectionData[i]["personalizedMobileNumber"].length > 0) mobile = "mobileico.png";
        if (collectionData[i]["personalizedMailId"] != null && collectionData[i]["personalizedMailId"].trim()!= "" && collectionData[i]["personalizedMailId"] != "undefined" && collectionData[i]["personalizedMailId"].length > 0) email = "emailico.png";
        if (collectionData[i]["personalizedFacebookId"] != null && collectionData[i]["personalizedFacebookId"].trim()!="" && collectionData[i]["personalizedFacebookId"]!="undefined" && collectionData[i]["personalizedFacebookId"].length > 0) fb = "facebookico.png";
        if (collectionData[i]["personalizedPictureId"] != null && collectionData[i]["personalizedPictureId"].length > 0) {
          if (collectionData[i]["personalizedPictureId"].substring(0, 4) == "http") {
            pic = collectionData[i]["personalizedPictureId"];
          } else {
            pic = gblRcImageUrlConstant + "&personalizedId=" + collectionData[i]["personalizedId"]+"&rr="+randomnum;
          }
        }
        var recName = "";
        if(isNotBlank(collectionData[i]["personalizedName"])){
          recName = collectionData[i]["personalizedName"];
        }
        kony.print("@@Pic:::"+pic);
        var tempRecord = {
          "crmId": collectionData[i]["crmId"],
          "recipientID": collectionData[i]["personalizedId"],
          "lblName": {"text": recName},
          "btnFav": {"skin": skinFav},
          "status": collectionData[i]["personalizedStatus"],
          "imgfb": {"src": fb},
          "facebook": collectionData[i]["personalizedFacebookId"],
          "imgemail": {"src": email},
          "email": collectionData[i]["personalizedMailId"],
          "imgmob": {"src": mobile},
          "mobile": collectionData[i]["personalizedMobileNumber"],
          "imgArrow": {"skin": "btnrightarrow"},
          "lblAccountNum": {"text": collectionData[i]["totalCount"] + " " +kony.i18n.getLocalizedString("Receipent_Accounts")},
          "accNo": collectionData[i]["totalCount"],
          "imgprofilepic": { "src": pic},
          "vbox101086657956690": {"containerWeight": widthFix}
        }
        myRecipientsRs.push(tempRecord);
      }
	}
    frmMyRecipients.segMyRecipient.widgetDataMap = {imgprofilepic: "imgprofilepic", lblName: "lblName", lblAccountNum: "lblAccountNum", imgmob: "imgmob", imgemail: "imgemail", imgfb: "imgfb", btnFav: "btnFav", imgArrow: "imgArrow",vbox101086657956690:"vbox101086657956690"};
    kony.print("@@@myRecipientsRs::::"+JSON.stringify(myRecipientsRs));
	frmMyRecipients.segMyRecipient.removeAll();
	frmMyRecipients.segMyRecipient.setData(myRecipientsRs);
  }catch(e){
    kony.print("@@@ In populateRecipients() Exception:::"+e);
  }
}

/**
 * (final comment)
 * search recipients in the main screen
 * @returns {}
 */
var srch = false; 
function searchForMyRecipient(eventobject) {
	frmMyRecipients.hbox474969373109363.setVisibility(true);
    frmMyRecipients.hbox86679448253627.setVisibility(false);
    var search = frmMyRecipients.textbox247327209467957.text;
    var manualSearch = false;
    if (search != null && search != "") {
        //if(search != null){
        if (kony.string.containsChars(frmMyRecipients.textbox247327209467957.text, charsArr)) {
            showAlertRcMB(kony.i18n.getLocalizedString("KeyInvalidSearch"), kony.i18n.getLocalizedString("info"), "info")
            //alert(kony.i18n.getLocalizedString("KeyInvalidSearch"));
            return;
        }
    }
    if (eventobject.id == "btnSearchRecipeints") {
        manualSearch = true;
    }
    showList.length = 0;
    isSearched = false;
    if (myRecipientsRs.length > 0) {
        if (search.length >= 3 || manualSearch) {
            srch = true;
            var searchtxt = search;
            var j = 0;
            //var regexp = new RegExp("(" + searchtxt + ")", "i");
            for (var i = 0; i < myRecipientsRs.length; i++) {
                if (myRecipientsRs[i]["lblName"]) {
                    
                    if (myRecipientsRs[i]["lblName"]["text"].toLowerCase().indexOf(searchtxt.toLowerCase()) >= 0) {
                        showList[j] = myRecipientsRs[i];
                        j++;
                    }
                }
            }
            if (showList.length > 0) {
                isSearched = true;
                frmMyRecipients.segMyRecipient.removeAll();
                frmMyRecipients.segMyRecipient.setData(showList);
                
            } else {
                frmMyRecipients.hbox86679448253627.setVisibility(true);
                frmMyRecipients.hbox474969373109363.setVisibility(false);
            }
        } else {
            isSearched = false;
            if(srch == true)
            {
            frmMyRecipients.segMyRecipient.removeAll();
            frmMyRecipients.segMyRecipient.setData(myRecipientsRs);
            }
            srch = false;
        }
    } else {
        frmMyRecipients.hbox86679448253627.setVisibility(true);
        //alert(kony.i18n.getLocalizedString("NoRecipients"))
    }
}

/**
 * (final comment)
 * set recipients as favourite in the main screen
 * @returns {}
 */
function setReceipentAsFavoriteMB() {
	var fav = frmMyRecipients.segMyRecipient.selectedItems[0]["btnFav"]["skin"];
	var ind = frmMyRecipients.segMyRecipient.selectedIndex;
	ind = ind[1];
	var receipentId = myRecipientsRs[ind].recipientID;
	if (fav != null) {
		if (fav == "btnStarGrey") {
			startRcSetFavServiceMB(crmId, receipentId, "Y");
		}
		else {
			startRcSetFavServiceMB(crmId, receipentId, "N");
		}
	}
	else {
		startRcSetFavServiceMB(crmId, receipentId, "Y");
	}
}

function startRcSetFavServiceMB(crmId, receipentId, isfav) {
	var inputParams = {
		//serviceID:"receipentEditService",
		crmId: crmId,
		personalizedId: receipentId,
		favoriteFlag: isfav,
		personalizedName: frmMyRecipients.segMyRecipient.selectedItems[0].lblName.text,
		personalizedPictureId: frmMyRecipients.segMyRecipient.selectedItems[0].imgprofilepic.src,
		personalizedMailId: frmMyRecipients.segMyRecipient.selectedItems[0].email,
		personalizedMobileNumber: frmMyRecipients.segMyRecipient.selectedItems[0].mobile,
		personalizedFacebookId: frmMyRecipients.segMyRecipient.selectedItems[0].facebook,
		personalizedStatus: "Added"
	};
	
	//kony.application.showLoadingScreen(frmLoading, "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
    showLoadingScreen();
	invokeServiceSecureAsync("receipentSetFavService", inputParams, startRcSetFavServiceAsyncCallbackMB);
}

function startRcSetFavServiceAsyncCallbackMB(status, callBackResponse) {
	if (status == 400) {
		if (callBackResponse["opstatus"] == "0") {
			isChMyRecipientsRs = true;
			isChRecipientAccountsRs = true;
			frmMyRecipientsListing();
			isSearched = false;
			kony.application.dismissLoadingScreen();
		}
		else {
			kony.application.dismissLoadingScreen();
			alert(kony.i18n.getLocalizedString("Receipent_alert_Error"));
		}
	}
	else {
		if (status == 300) {
			//kony.application.dismissLoadingScreen();
			showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
			//alert(kony.i18n.getLocalizedString("ECGenericError"));
		}
	}
}

/**
 * (final comment)
 * select recipient and navigate to details screen
 * @returns {}
 */
function populateCurrentRecipientCache() {
	isChRecipientAccountsRs = true;
	//currentRecipient = frmMyRecipients.segMyRecipient.selectedItems[0]["lblName"]["text"];
	var i = frmMyRecipients.segMyRecipient.selectedIndex[1];
	isRecipientNew = false;
	if (isSearched) {
		currentRecipientDetails["crmId"] = showList[i]["crmId"];
		currentRecipientDetails["name"] = showList[i]["lblName"]["text"];
		currentRecipientDetails["rcId"] = showList[i]["recipientID"];
		currentRecipientDetails["mobile"] = showList[i]["mobile"];
		currentRecipientDetails["email"] = showList[i]["email"];
		currentRecipientDetails["facebook"] = showList[i]["facebook"];
		currentRecipientDetails["fav"] = showList[i]["status"];
		currentRecipientDetails["pic"] = showList[i]["imgprofilepic"]["src"];
		currentRecipientDetails["accNo"] = showList[i]["accNo"];
	}
	else {
		currentRecipientDetails["crmId"] = myRecipientsRs[i]["crmId"];
		currentRecipientDetails["name"] = myRecipientsRs[i]["lblName"]["text"];
		currentRecipientDetails["rcId"] = myRecipientsRs[i]["recipientID"];
		currentRecipientDetails["mobile"] = myRecipientsRs[i]["mobile"];
		currentRecipientDetails["email"] = myRecipientsRs[i]["email"];
		currentRecipientDetails["facebook"] = myRecipientsRs[i]["facebook"];
		currentRecipientDetails["fav"] = myRecipientsRs[i]["status"];
		currentRecipientDetails["pic"] = myRecipientsRs[i]["imgprofilepic"]["src"];
		currentRecipientDetails["accNo"] = myRecipientsRs[i]["accNo"];
	}
}
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^main

/**
 * (final comment)
 * list recpient details in details screen
 * @returns {}
 */
function frmMyRecipientsAccountListing() {
	if (isChRecipientAccountsRs) { //info: if recipient account is changed
		startReceipentAccListingServiceMB(crmId);
		isChRecipientAccountsRs = false;
	}
	else {
		//frmMyRecipientDetail.segMyRecipientDetail.setData(globalRcAccListCache);
		populateRecipientDetails();
		populateRecipientAccounts(recipientAccountsRs);
	}
}

function startReceipentAccListingServiceMB(crmId) {
	var inputParams = {
		crmId: crmId,
		personalizedId: currentRecipientDetails["rcId"]
	};
	//kony.application.showLoadingScreen(frmLoading, "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
    showLoadingScreen();
	invokeServiceSecureAsync("receipentAllDetailsService", inputParams, startgetBankAccntforRCServiceMBAsyncCallback);
}

function startgetBankAccntforRCServiceMBAsyncCallback(status, callBackResponse) {
	if (status == 400) {
		if (callBackResponse["opstatus"] == 0) {
			var responseData = callBackResponse["Results"];
			if (responseData != null) {
				currentRecipient = responseData["personalizedName"];
				if(responseData[0]["personalizedMailId"] == undefined)
				responseData[0]["personalizedMailId"]="";
				if(responseData[0]["personalizedMobileNumber"] == undefined || responseData[0]["personalizedMobileNumber"] == "undefined"){
					responseData[0]["personalizedMobileNumber"]="";
				}
				if(responseData[0]["personalizedFacebookId"] == undefined)
				responseData[0]["personalizedFacebookId"]="";
				var randomnum = Math.floor((Math.random()*10000)+1); 
				currentRecipientDetails["crmId"] = responseData[0]["crmId"];
				currentRecipientDetails["name"] = responseData[0]["personalizedName"];
				currentRecipientDetails["rcId"] = responseData[0]["personalizedId"];
				currentRecipientDetails["mobile"] = responseData[0]["personalizedMobileNumber"];
				currentRecipientDetails["email"] = responseData[0]["personalizedMailId"];
				currentRecipientDetails["facebook"] = responseData[0]["personalizedFacebookId"];
				currentRecipientDetails["fav"] = responseData[0]["favoriteFlag"];
				gblRcImageUrlConstant="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/ImageRender?billerId=&crmId=Y";				
				BANK_LOGO_URL = "https://" + appConfig.serverIp + "/" + appConfig.middlewareContext + "/Bank-logo/bank-logo-";
				
				if(responseData[0]["personalizedPictureId"] != null || responseData[0]["personalizedPictureId"] != undefined){
					if(responseData[0]["personalizedPictureId"].substring(0, 4) == "http"){
						currentRecipientDetails["pic"] = responseData[0]["personalizedPictureId"];
					}
					else{
						currentRecipientDetails["pic"] = gblRcImageUrlConstant + "&personalizedId=" + responseData[0]["personalizedId"]+"&rr="+randomnum;
					}
				}
				else{
					currentRecipientDetails["pic"] =gblRcImageUrlConstant + "&personalizedId=" + responseData[0]["personalizedId"]+"&rr="+randomnum; 
				}
				currentRecipientDetails["accNo"] = ""; //responseData[0]["crmId"];
				populateRecipientDetails();
				if ((responseData.length - 1) > 0) {
					recipientAccountsRs.length = 0;
					var acctName;
					var fav;
					var bankName;
					var bankNameTh;
					var bankLogo;
					for (var i = 1; i < responseData.length; i++) {
						
						if(responseData[i]["bankCD"] == "11"){
							bankLogo = "myrecpicon.png";
						}
						else{
							//bankLogo = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+responseData[i]["bankCD"]+"&modIdentifier=BANKICON";
							bankLogo = loadBankIcon(responseData[i]["bankCD"]);
						}

						if (responseData[i]["accFavoriteFlag"] == null) {
							fav = "";
						}
						else {
							fav = responseData[i]["accFavoriteFlag"];
						}
						
						for(var j = 0; j < globalSelectBankData.length; j++){
							
							if(responseData[i]["bankCD"].toString() == globalSelectBankData[j][0].toString()){
								
								bankName = globalSelectBankData[j][7];
								bankNameTh=globalSelectBankData[j][8]
								
								
								break;
							}
							else{
								bankName = "BANK";
							}
						}
						//if (responseData[i]["bankCD"] != "11"){
						//	acctName = "";
						//}
					//	else {
							acctName = responseData[i]["PersonalizedAcctName"];
					//	}
						var tempRec = {
							"bankLogo": bankLogo,
 							"lblBankCode": responseData[i]["bankCD"],
							"lblBankName": bankName,
							"lblAccountNo": responseData[i]["personalizedAcctId"],
							"lblAccountNickname": responseData[i]["PersonalizedAcctNickName"],
							"lblAccountName": acctName,
							"isFav": fav,
							"personalizedId": responseData[i]["personalizedId"],
							"bankNameTH":bankNameTh
						}
						recipientAccountsRs.push(tempRec);
					}
					for (var i in recipientAccountsRs) {
						
					}
					if(recipientAccountsRs.length >= gblMAXAccountCount)
                     {
                          frmMyRecipientDetail.btnAddAcc.skin=btnAddDisable;
                          frmMyRecipientDetail.btnAddAcc.focusSkin=btnAddDisable;
                     }
                    else
                    {
                         frmMyRecipientDetail.btnAddAcc.skin=btnAdd;
                         frmMyRecipientDetail.btnAddAcc.focusSkin=btnAdd;
                     }
					populateRecipientAccounts(recipientAccountsRs);
				}
				else {
					
					recipientAccountsRs.length = 0;
					AddAccountList.length = 0;
					//kony.application.dismissLoadingScreen();
					//alert(kony.i18n.getLocalizedString("NoAccounts"));
				}
			} //resulttable !=null
			//kony.application.dismissLoadingScreen();
		}
		else {
			//kony.application.dismissLoadingScreen();
			showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_Error"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
			//alert(kony.i18n.getLocalizedString("Receipent_alert_Error"));
		} //opstatus == 0
		kony.application.dismissLoadingScreen();
	}
	else {
		if (status == 300) {
			//kony.application.dismissLoadingScreen();
			//alert(kony.i18n.getLocalizedString("ECGenericError"));
		}
	}
}

/**
 * (final comment)
 * list recpient profile details in details screen
 * @returns {}
 */
function populateRecipientDetails() {
	frmMyRecipientDetail.lblName.text = currentRecipientDetails["name"];
	frmMyRecipientDetail.hbox47502979411849.setVisibility(true);
	frmMyRecipientDetail.hbox47502979411850.setVisibility(true);
	frmMyRecipientDetail.hbox47502979411851.setVisibility(true);
	//to hide mobile field accordingly
	if (!(currentRecipientDetails["mobile"])) {
		frmMyRecipientDetail.hbox47502979411849.isVisible = false;
	}
	else {
		frmMyRecipientDetail.hbox47502979411849.isVisible = true;
		var ph = currentRecipientDetails["mobile"];
		ph = stripDash(ph);
		//ph = addDashPh(ph);
		frmMyRecipientDetail.label47502979411852.text = kony.i18n.getLocalizedString("Receipent_Mobileno");
		frmMyRecipientDetail.lblMobile.text = ph;
	}
	//to hide email field accordingly
	if (!(currentRecipientDetails["email"])) {
		frmMyRecipientDetail.hbox47502979411850.isVisible = false;
	}
	else {
		frmMyRecipientDetail.hbox47502979411850.isVisible = true;
		frmMyRecipientDetail.label47502979412689.text = kony.i18n.getLocalizedString("Receipent_Email");
		frmMyRecipientDetail.lblEmail.text = currentRecipientDetails["email"];
	}
	//to hide facebookID field accordingly
	if (!(currentRecipientDetails["facebook"])) {
		frmMyRecipientDetail.hbox47502979411851.isVisible = false;
	}
	else {
		frmMyRecipientDetail.hbox47502979411851.isVisible = true;
		frmMyRecipientDetail.lblFBstudio4.text = kony.i18n.getLocalizedString("Receipent_FacebookID");//Modified by Studio Viz
		frmMyRecipientDetail.lblFbstudio3.text = currentRecipientDetails["facebook"];//Modified by Studio Viz
	}
	//to hide pic field accordingly

	if (currentRecipientDetails["pic"] == "") {
		frmMyRecipientDetail.imgprofilepic.src = "avatar_dis.png";
	}
	else {
		for(var i=0;i<10;i++){
			
		}
		
		frmMyRecipientDetail.imgprofilepic.src = currentRecipientDetails["pic"];
	}
	frmMyRecipientDetail.lblAccounts.text = kony.i18n.getLocalizedString("Receipent_Account");
	//frmMyRecipientDetail.imgprofilepic.src = frmMyRecipients.segMyRecipient.selectedItems[0].imgprofilepic.src;
}

/**
 * (final comment)
 * list recpient account details in details screen
 * @returns {}
 */
function populateRecipientAccounts(recipientAccountsRes) {
	var skinFav = null;
	var mobile = null;
	var email = null;
	var fb = null;
	AccountList.length = 0;
	frmMyRecipientDetail.segMyRecipientDetail.removeAll();
	for (var i = 0; i < recipientAccountsRes.length; i++) {
		skinFav = "btnStarGrey";
		if (recipientAccountsRes[i]["isFav"] == "Y") skinFav = "btnStarFoc";
		var accNameTemp="", accNameLblTemp="";
		if(recipientAccountsRes[i]["lblAccountName"] == null || recipientAccountsRes[i]["lblAccountName"] == ""){
			//
		}else{
			accNameTemp = recipientAccountsRes[i]["lblAccountName"];
			accNameLblTemp = kony.i18n.getLocalizedString('AccountName');
		}	
		var bankName="";
		var locale = kony.i18n.getCurrentLocale();
		if (kony.string.startsWith(locale, "en", true) == true)
		bankName=recipientAccountsRes[i]["lblBankName"];
		else
		bankName=recipientAccountsRes[i]["bankNameTH"];
		var tempRecord = {
			"bankLogo": {
				"src": recipientAccountsRes[i]["bankLogo"]
			},
			"lblBankName": {
				"text": bankName
			},
			"btnFav": {
				"skin": skinFav
			},
			"lblAccountNo": {
				"text": addDashAc(stripDashAcc(recipientAccountsRes[i]["lblAccountNo"]))
			},
			"lblNick": {
				"text": recipientAccountsRes[i]["lblAccountNickname"]
			},
			"lblAcctName": {
				"text": accNameTemp,"isVisible" :true
			},
			"personalizedId": {
				"text": recipientAccountsRes[i]["personalizedId"],"isVisible" :false
			},
			"btnTransfer":{		text: kony.i18n.getLocalizedString("Transfer"),
								skin: "btnBlueSkin"
								},
			"lblBankCD": recipientAccountsRes[i]["lblBankCode"],
			//"lblBank": kony.i18n.getLocalizedString('keyBank'),
			"lblNumber": kony.i18n.getLocalizedString('Receipent_Number'),
			"lblNickName": kony.i18n.getLocalizedString('Nickname'),
			"lblAccountName": accNameLblTemp
		}
		AccountList.push(tempRecord);
	}
	
	frmMyRecipientDetail.segMyRecipientDetail.setData(AccountList);
	if (frmMyRecipientDetail.segMyRecipientDetail.data) {
		
		if (frmMyRecipientDetail.segMyRecipientDetail.data.length > 1) {
			frmMyRecipientDetail.lblAccounts.text = kony.i18n.getLocalizedString("Receipent_Accounts");
		}
		else {
			frmMyRecipientDetail.lblAccounts.text = kony.i18n.getLocalizedString("Receipent_Account");
		}
	}
}

/**
 * (final comment)
 * set account as favourite
 * @returns {}
 */
function setAccountAsFavoriteMB() {
	var fav = frmMyRecipientDetail.segMyRecipientDetail.selectedItems[0]["btnFav"]["skin"];
	//var receipentId = frmMyRecipients.segMyRecipient.selectedItems[0]["receipentID"];
	if (fav != null) {
		if (fav == "btnStarGrey") {
			startAccountSetFavServiceMB("Y");
		}
		else {
			startAccountSetFavServiceMB("N");
		}
	}
	else {
		startAccountSetFavServiceMB("Y");
	}
}

function startAccountSetFavServiceMB(isfav) {
	var inputParams = {
		//serviceID:"receipentEditService",
		//personalizedId:receipentId,
		accFavoriteFlag: isfav,
		personalizedAcctId: stripDashAcc(frmMyRecipientDetail.segMyRecipientDetail.selectedItems[0].lblAccountNo.text),
		personalizedId: currentRecipientDetails["rcId"],
		bankCD: frmMyRecipientDetail.segMyRecipientDetail.selectedItems[0].lblBankCD,
		acctStatus: "Added"
		//bankCd:currentRecipientDetails["pic"]
	};
	//kony.application.showLoadingScreen(frmLoading, "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
    showLoadingScreen();
	invokeServiceSecureAsync("receipentSetFavBankAccntService", inputParams, startAccountSetFavServiceAsyncCallbackMB);
}


function startAccountSetFavServiceAsyncCallbackMB(status, callBackResponse) {
	if (status == 400) {
		if (callBackResponse["opstatus"] == "0") {
			isChRecipientAccountsRs = true;
			isChMyRecipientsRs = true;
			frmMyRecipientsAccountListing();
			kony.application.dismissLoadingScreen();
		}
		else {
			kony.application.dismissLoadingScreen();
			showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_Error"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
			//alert(kony.i18n.getLocalizedString("Receipent_alert_Error"));
		}
	}
	else {
		if (status == 300) {
			//kony.application.dismissLoadingScreen();
			showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
			//alert(kony.i18n.getLocalizedString("ECGenericError"));
		}
	}
}

//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^details


//add recipient from contacts functions------------------------------------------------------------------

function populateContacts() {
	
	showLoadingScreen();
	//selectedMobile = null;
	frmMyRecipientSelectContacts.label475095112172022.isVisible = false;
	frmMyRecipientSelectContacts.btnReturn.isVisible = false;
	multiSelectedContacts.length = 0;
	ContactList1.length = 0;
	var fnTemp = "temp";
	var fn = null;
	var ln = null;
	var ph = null;
	var em = null;
	var ph1 = null;
	var ph2 = null;
	var ph3 = null;
	var btnn1 = null;
	var btnn2 = null;
	var btnn3 = null;
	var phnos = new Array();
	var flag1 = 0;
	var flag2 = 0;
	var flag3 = 0;
	/*frmMyRecipientSelectMobile.segMyRecipient.widgetDataMap = {
    lblName: "lblName",
    lblMobile: "lblMobile",
    lblEmail: "lblEmail",
    imgprofilepic: "imgprofilepic",
    imgchbxbtn: "imgchbxbtn",
    button47506532118567: btnn1,
    btn1: btnn2,
    btn2: btnn3,
    lblMobile1: "lblMobile1",
    lblMobile2: "lblMobile2",
    lblMobile3: "lblMobile3"
}*/
		//kony.application.showLoadingScreen(frmLoading, "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
        //showLoadingScreen();
		var contacts = kony.contact.find("*", true);
		if (contacts == null || contacts == undefined) {
			frmMyRecipientSelectContacts.label475095112172022.isVisible = true;
			frmMyRecipientSelectContacts.btnReturn.isVisible = true;
			frmMyRecipientSelectContacts.btnNext.isVisible = false;
			frmMyRecipientSelectContacts.segMyRecipient.isVisible=false;
		}
		else {
			for (var contact = 0; contact < contacts.length; contact++) {
				if(contacts[contact]["phone"] == null || contacts[contact]["phone"] == undefined){
					phnos.push("NULL");
				}
				else{
					phnos.push(contacts[contact]["phone"]);
				}
			}
			for (var contact = 0; contact < contacts.length; contact++) {
				//break;
				if (contacts[contact]["firstname"] == null || contacts[contact]["firstname"] == "") fn = "";
				else fn = contacts[contact]["firstname"];
				if (contacts[contact]["lastname"] == null || contacts[contact]["lastname"] == "") ln = "";
				else ln = contacts[contact]["lastname"];
				var m = 0;
				//
				while (true) {
					if(phnos[contact] == "NULL"){
						break;
					}
					 	if(phnos[contact][m] == null || phnos[contact][m] == undefined){
					 		break;
					 	}
						if (phnos[contact][m]["name"] == "mobile") {
							ph1 = stripDashNew(phnos[contact][m]["number"]);
							ph1 = addDashPh(ph1);
							flag1 = 1;
							//make things visible
						}
						else if (phnos[contact][m]["name"] == "home") {
							ph2 = stripDashNew(phnos[contact][m]["number"]);
							ph2 = addDashPh(ph2);
							flag2 = 1;
							//make things visible
						}
						else if (phnos[contact][m]["name"] == "work") {
							ph3 = stripDashNew(phnos[contact][m]["number"]);
							ph3 = addDashPh(ph3);
							flag3 = 1;
							//make things visible
						}
						else{
							if(flag1 == 0){
								ph1 = stripDashNew(phnos[contact][m]["number"]);
								ph1 = addDashPh(ph1);
								flag1 = 1;
							}
							else if(flag2 == 0){
								ph2 = stripDashNew(phnos[contact][m]["number"]);
								ph2 = addDashPh(ph2);
								flag2 = 1;
							}
							else {
								ph3 = stripDashNew(phnos[contact][m]["number"]);
								ph3 = addDashPh(ph3);
								flag3 = 1;
							}
						}
					m++;
				}
				if (flag1 == 0) {
					ph1 = "";
				}
				if (flag2 == 0) {
					ph2 = "";
				}
				if (flag3 == 0) {
					ph3 = "";
				}
				flag1 = 0;
				flag2 = 0;
				flag3 = 0;
				/*if (contacts[contact]["phone"][0]["number"] == null || contacts[contact]["phone"][0]["number"] == "") pn = "";
            else{
            	pn = contacts[contact]["phone"][0]["number"];
            	for( var i=0;i<(pn.length-1);i++){
					if(pn[i] == '-' || pn[i] == '(' || pn[i] == ')' || pn[i] == ' '){
					pn = pn.replace("-","");
					pn = pn.replace("(","");
					pn = pn.replace(")","");
					pn = pn.replace(" ","");
					}
				}
            }*/
				//if (contacts[contact]["email"][0]["id"] == null || contacts[contact]["email"][0]["id"] == "") em = "";
				//else em = contacts[contact]["email"][0]["id"];
				if (contacts[contact]["email"]) {
					if (contacts[contact]["email"][0]) {
						if (contacts[contact]["email"][0]["id"]) {
							if(validateEmailReceipent(contacts[contact]["email"][0]["id"])){
								em = contacts[contact]["email"][0]["id"];
							}
							else{
								em = "";
							}
						}
						else {
							em = "";
						}
					}
					else {
						em = "";
					}
				}
				else {
					em = "";
				}
				if (contacts[contact]["photorawbytes"] == null || contacts[contact]["photorawbytes"] == "") ph = "";
				else ph = contacts[contact]["photorawbytes"];
				
				if(fn != ""){
					if(ph1 != "" || ph2 != "" || ph3 != ""){
						ContactList1.push(new Contact(fn, ln, ph1, ph2, ph3, em, ph));
					}
				}
				
			}
			if(ContactList1.length > 0){
				ContactList1.sort(dynamicSortOther("lblName"));
				frmMyRecipientSelectContacts.btnNext.isVisible = true;
				frmMyRecipientSelectContacts.segMyRecipient.removeAll();
				frmMyRecipientSelectContacts.segMyRecipient.data = ContactList1;
				//frmMyRecipientSelectContacts.segMyRecipient.setData(dictionaryView);
				var contactListToggle1 = true;
			}
			else{
				frmMyRecipientSelectContacts.btnNext.isVisible = false;
				frmMyRecipientSelectContacts.btnReturn.isVisible = true;
				frmMyRecipientSelectContacts.label475095112172022.text=kony.i18n.getLocalizedString("NoContacts");
				frmMyRecipientSelectContacts.label475095112172022.isVisible = true;
				frmMyRecipientSelectContacts.segMyRecipient.removeAll();
				frmMyRecipientSelectContacts.segMyRecipient.isVisible=false;
				var contactListToggle1 = true;
			}
		}
	dismissLoadingScreen();
}

function Contact(lblFName, lblLName, lblMobile1, lblMobile2, lblMobile3, lblEmail, imgprofilepic) {
	var i = null;
	var j = null;
	var k = null;
	var l = null;
	//var deviceInfo = kony.os.deviceInfo();
	this.lblName = lblFName + " " + lblLName;
	this.lblMobileNo1 = lblMobile1;
	this.lblMobileNo2 = lblMobile2;
	this.lblMobileNo3 = lblMobile3;
	this.lblEmail = lblEmail;
	if (imgprofilepic == null || imgprofilepic == "") {
		this.imgprofilepic = "avatar_dis.png";
	}
	else {
		this.imgprofilepic = {rawBytes: imgprofilepic};
	}
	this.imgchbxbtn = "chkbox2.png";
	if (lblMobile1 == "") i = 0;
	else i = 1;
	if (lblMobile2 == "") j = 0;
	else j = 1;
	if (lblMobile3 == "") k = 0;
	else k = 1;
	if ((parseInt(i) + parseInt(j) + parseInt(k)) == 1) l = 1;
	else l = 0;
	if (lblMobile1 != "") {
		if (l == 1) btnn1 = {
			isVisible: false
		};
		else btnn1 = {
			isVisible: true,
			skin: "btnRadioFoc"
		};
		this.btnMobileNo1 = btnn1;
	}
	if (lblMobile2 != "") {
		if (l == 1) btnn2 = {
			isVisible: false
		};
		else btnn2 = {
			isVisible: true,
			skin: "btnRadio"
		};
		this.btnMobileNo2 = btnn2;
	}
	if (lblMobile3 != "") {
		if (l == 1) btnn3 = {
			isVisible: false
		};
		else btnn3 = {
			isVisible: true,
			skin: "btnRadio"
		};
		this.btnMobileNo3 = btnn3;
	}
}

function stripDash(pn) {
	if(pn == null || pn == undefined)
		return "";
	for (var i = 0; i < (pn.length - 1); i++) {
		if (pn[i] == '-' || pn[i] == '(' || pn[i] == ')' || pn[i] == ' ') {
			pn = pn.replace("-", "");
			pn = pn.replace("(", "");
			pn = pn.replace(")", "");
			pn = pn.replace(" ", "");
		}
	}
	return pn;
}

function ContactsMSfunc(btnID) {

//frmMyRecipientSelectContacts.segMyRecipient.onRowClick=
	var index = frmMyRecipientSelectContacts.segMyRecipient.selectedIndex;
	index = index[1];
	var flag = "empty";
	var items = frmMyRecipientSelectContacts.segMyRecipient.selectedItems;
	if (items[0]["btnMobileNo1"]) {
			items[0]["btnMobileNo1"]["skin"] = "btnRadioFoc";
	}
	//items[0]["btnMobileNo1"]["skin"] = "btnRadioFoc";
	
	if (items[0]["btnMobileNo2"]) {
		items[0]["btnMobileNo2"]["skin"] = "btnRadio";
	}
	if (items[0]["btnMobileNo3"]) {
		items[0]["btnMobileNo3"]["skin"] = "btnRadio";
	}
	if (btnID == "one") {
		items[0]["btnMobileNo1"]["skin"] = "btnRadioFoc";
		selectedMobile = items[0]["lblMobileNo1"];
	}
	else {
		if (items[0]["btnMobileNo1"]) {
			items[0]["btnMobileNo1"]["skin"] = "btnRadio";
		}
	}
	if (btnID == "two") {
		items[0]["btnMobileNo2"]["skin"] = "btnRadioFoc";
		selectedMobile = items[0]["lblMobileNo2"];
	}
	else {
		if (items[0]["btnMobileNo2"]) {
			items[0]["btnMobileNo2"]["skin"] = "btnRadio";
		}
	}
	if (btnID == "three") {
		items[0]["btnMobileNo3"]["skin"] = "btnRadioFoc";
		selectedMobile = items[0]["lblMobileNo3"];
	}
	else {
		if (items[0]["btnMobileNo3"]) {
			items[0]["btnMobileNo3"]["skin"] = "btnRadio";
		}
	}
	items[0]["imgchbxbtn"] = "chkbox.png";
	var base64Contact = null;
	if(items[0]["imgprofilepic"] == "avatar_dis.png"){
		base64Contact = "avatar_dis.png";
	}
	else{
		base64Contact = {rawBytes : items[0]["imgprofilepic"].rawBytes};
	}
	if (multiSelectedContacts.length == 0) {
	
		multiSelectedContacts.push({
			"lblName": items[0]["lblName"].replace(/^\s+|\s+$/g, ""),
			"lblMobile": selectedMobile,
			"lblEmail": items[0]["lblEmail"],
			"lblFacebook": "",
			"status": "",
			"crmId": "",
			"imgprofilepic": base64Contact//items[0]["imgprofilepic"]
		});
	}
	else{
		for (var i in multiSelectedContacts) {
			if (multiSelectedContacts[i]["lblName"] == items[0]["lblName"].replace(/^\s+|\s+$/g, "")) {
				if (multiSelectedContacts[i]["lblMobile"] == selectedMobile) {
					//do nothing
				}
				else if (multiSelectedContacts[i]["lblMobile"] != selectedMobile) {
					multiSelectedContacts[i]["lblMobile"] = selectedMobile;
				}
				break;
			}
			else {
				flag = "noContactsMatched";
			}
		}
		if(flag == "noContactsMatched"){
			multiSelectedContacts.push({
					"lblName": items[0]["lblName"].replace(/^\s+|\s+$/g, ""),
					"lblMobile": selectedMobile,
					"lblEmail": items[0]["lblEmail"],
					"lblFacebook": "",
					"status": "",
					"crmId": "",
					"imgprofilepic": base64Contact//items[0]["imgprofilepic"]
				});
		}
	}
	
	frmMyRecipientSelectContacts.segMyRecipient.setDataAt(items[0], index);

}

function ContactsMSRowfunc() {

	var index = frmMyRecipientSelectContacts.segMyRecipient.selectedIndex;
	index = index[1];
	flag = "empty";
	var items = frmMyRecipientSelectContacts.segMyRecipient.selectedItems;
	var i, j, k;
	if (items[0]["lblMobileNo1"] != "") i = 1;
	else i = 0;
	if (items[0]["lblMobileNo2"] != "") j = 1;
	else j = 0;
	if (items[0]["lblMobileNo3"] != "") k = 1;
	else k = 0;
	var base64Contact = null;
	if(items[0]["imgprofilepic"] == "avatar_dis.png"){
		base64Contact = "avatar_dis.png";
	}
	else{
		base64Contact = {rawBytes : items[0]["imgprofilepic"].rawBytes};
	}
	if (parseInt(i) + parseInt(j) + parseInt(k) == 1) { //single contact exists
		if (items[0]["imgchbxbtn"] == "chkbox.png") { //then deselect
			items[0]["imgchbxbtn"] = "chkbox2.png";
			for (var i in multiSelectedContacts) {
				if (multiSelectedContacts[i]["lblName"] == items[0]["lblName"].replace(/^\s+|\s+$/g, "")) {
					multiSelectedContacts.splice(i, 1);
				}
			}
			frmMyRecipientSelectContacts.segMyRecipient.setDataAt(items[0], index);
		}
		else if (items[0]["imgchbxbtn"] == "chkbox2.png") { //then select
			if (items[0]["lblMobileNo1"]) selectedMobile = items[0]["lblMobileNo1"];
			else if (items[0]["lblMobileNo2"]) selectedMobile = items[0]["lblMobileNo2"];
			else if (items[0]["lblMobileNo3"]) selectedMobile = items[0]["lblMobileNo3"];
			items[0]["imgchbxbtn"] = "chkbox.png";
			if (multiSelectedContacts.length == 0) {
				//multiSelectedContacts.push({"name" : items[0]["lblName"], "mobile" : selectedMobile});
				multiSelectedContacts.push({
					"lblName": items[0]["lblName"].replace(/^\s+|\s+$/g, ""),
					"lblMobile": selectedMobile,
					"lblEmail": items[0]["lblEmail"],
					"lblFacebook": "",
					"status": "",
					"crmId": "",
					"imgprofilepic": base64Contact
				});
			}
			else{
				for (var i in multiSelectedContacts) {
					if (multiSelectedContacts[i]["lblName"] == items[0]["lblName"].replace(/^\s+|\s+$/g, "")) {
						if (multiSelectedContacts[i]["lblMobile"] == selectedMobile) {
							//do nothing
						}
						else if (multiSelectedContacts[i]["lblMobile"] != selectedMobile) {
							multiSelectedContacts[i]["lblMobile"] = selectedMobile;
						}
					}
					else {
						flag = "noContactsMatched";
					}
				}
				if(flag == "noContactsMatched"){
						multiSelectedContacts.push({
							"lblName": items[0]["lblName"].replace(/^\s+|\s+$/g, ""),
							"lblMobile": selectedMobile,
							"lblEmail": items[0]["lblEmail"],
							"lblFacebook": "",
							"status": "",
							"crmId": "",
							"imgprofilepic": base64Contact
						});
				}
			}
			
			frmMyRecipientSelectContacts.segMyRecipient.setDataAt(items[0], index);
		}
	} //-----------------------------------------------------------------
	else {
		if (items[0]["imgchbxbtn"] == "chkbox.png") { //then deselect
			items[0]["imgchbxbtn"] = "chkbox2.png";
			//add logic to change radio too
			for (var i in multiSelectedContacts) {
				if (multiSelectedContacts[i]["lblName"] == items[0]["lblName"].replace(/^\s+|\s+$/g, "")) {
					multiSelectedContacts.splice(i, 1);
				}
			}
			frmMyRecipientSelectContacts.segMyRecipient.setDataAt(items[0], index);
		}
		else if (items[0]["imgchbxbtn"] == "chkbox2.png") {
			if (items[0]["lblMobileNo1"] != "" || items[0]["lblMobileNo1"] != null) selectedMobile = items[0]["lblMobileNo1"];
			else if (items[0]["lblMobileNo2"] || items[0]["lblMobileNo2"] != null) selectedMobile = items[0]["lblMobileNo2"];
			else if (items[0]["lblMobileNo3"] || items[0]["lblMobileNo3"] != null) selectedMobile = items[0]["lblMobileNo3"];
			items[0]["imgchbxbtn"] = "chkbox.png";
			if (multiSelectedContacts.length == 0) {
				//multiSelectedContacts.push({"name" : items[0]["lblName"], "mobile" : selectedMobile});
				multiSelectedContacts.push({
					"lblName": items[0]["lblName"].replace(/^\s+|\s+$/g, ""),
					"lblMobile": selectedMobile,
					"lblEmail": items[0]["lblEmail"],
					"lblFacebook": "",
					"status": "",
					"crmId": "",
					"imgprofilepic": base64Contact
				});
			}
			else{
				for (var i in multiSelectedContacts) {
					if (multiSelectedContacts[i]["lblName"] == items[0]["lblName"].replace(/^\s+|\s+$/g, "")) {
						if (multiSelectedContacts[i]["lblMobile"] == selectedMobile) {
							//do nothing
						}
						else if (multiSelectedContacts[i]["lblMobile"] != selectedMobile) {
							multiSelectedContacts[i]["lblMobile"] = selectedMobile;
						}
					}
					else {
						flag = "noContactsMatched";
					}
				}
				if(flag == "noContactsMatched"){
						multiSelectedContacts.push({
							"lblName": items[0]["lblName"].replace(/^\s+|\s+$/g, ""),
							"lblMobile": selectedMobile,
							"lblEmail": items[0]["lblEmail"],
							"lblFacebook": "",
							"status": "",
							"crmId": "",
							"imgprofilepic": base64Contact
						});
				}
			}
			
			frmMyRecipientSelectContacts.segMyRecipient.setDataAt(items[0], index);
			//add default phone to multiselect array
		}
	}
	
}

function selectedContactsfn() {
	notAllowedContacts.length = 0;
	var extra99Check = null;
	var alertString = "";
	var dashStripped = null;
	gblEditContactList=false;
	extra99Check = gblMAXRecipientCount - (myRecipientsRs.length + gblAddMoreFromContactsCache.length + multiSelectedContacts.length);
	if (multiSelectedContacts.length != 0) {
		if (extra99Check < 0) {
			showAlertRcMB(kony.i18n.getLocalizedString("J200000001"), kony.i18n.getLocalizedString("info"), "info")
			return;
		}
		if (multiSelectedContacts.length > MAX_RECIPIENT_BULK_COUNT) {
			showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_selectlessthanfive").replace("<1>",MAX_RECIPIENT_BULK_COUNT), kony.i18n.getLocalizedString("info"), "info")
			return;
		}
		else {
			if (myRecipientsRs.length == 0 || myRecipientsRs == null) {
				//add without checking duplicacy
				frmMyRecipientSelectContactsConf.segMyRecipient.setData(multiSelectedContacts);
				frmMyRecipientSelectContactsConf.show();
			}
			else {
				for (var i = 0; i < multiSelectedContacts.length; i++) {
					for (var k = 0; k < myRecipientsRs.length; k++) {
						dashStripped = stripDashPh(multiSelectedContacts[i]["lblMobile"]);
						if (multiSelectedContacts[i]["lblName"] == myRecipientsRs[k]["lblName"]["text"]) {
							notAllowedContacts.push(multiSelectedContacts[i]["lblName"]);
						}
					}
				}
				for (var i = 0; i < multiSelectedContacts.length; i++) {
					for (var k = 0; k < gblAddMoreFromContactsCache.length; k++) {
						dashStripped = stripDashPh(multiSelectedContacts[i]["lblMobile"]);
						if (multiSelectedContacts[i]["lblName"] == gblAddMoreFromContactsCache[k].lblName) {
							notAllowedContacts.push(multiSelectedContacts[i]["lblName"]);
						}
					}
				}
				if (notAllowedContacts.length != 0) {
					for (var j = 0; j < notAllowedContacts.length; j++) {
						alertString = notAllowedContacts[j] + " ";
					}
					showAlertRcMB(alertString + " " + kony.i18n.getLocalizedString("RecipientExists"), kony.i18n.getLocalizedString("info"), "info")
					//alert(alertString + " " + kony.i18n.getLocalizedString("RecipientExists"));
					frmMyRecipientSelectContacts.show();
				}
				else {
					frmMyRecipientSelectContactsConf.segMyRecipient.setData(multiSelectedContacts);
					frmMyRecipientSelectContactsConf.show();
				}
			}
		}
	}
	else
		showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_selectone"), kony.i18n.getLocalizedString("info"), "info")
		//alert(kony.i18n.getLocalizedString("Receipent_alert_selectone"));
}

function transCancelSelectContacts() {
	popTransactionPwd.dismiss();
}

function showConfPopupAddRecipientContacts()
{
   if(gblAuthAccessPin == true){
     showAccesspinPopup();
   }else{
     showOTPPopup(kony.i18n.getLocalizedString("TransactionPass") + ":", "", "", transConfirmSelectContactsPre, 3);
   }
}


function transConfirmSelectContactsPre() {
	gblFlagTransPwdFlow = "addRcContacts";
	checkVerifyPWDMBRC();
}

function transConfirmSelectContacts() {
		gblPhotoSource = "jpeg";
		var tempData = [];
		var totalData = [];
		
		for (var i = 0; i < multiSelectedContacts.length; i++) {
			var Name = multiSelectedContacts[i]["lblName"];
			var Picture = gblPhotoSource;
			var Number = multiSelectedContacts[i]["lblMobile"];
			var Email = multiSelectedContacts[i]["lblEmail"]; 
			var fbId = multiSelectedContacts[i]["lblFacebook"];
			
			var contactRcData = [Name, Picture, Email, Number, , "Added","N"];
			totalData.push(contactRcData);
			
		}
		gblRcBase64List = "";
		gblRcPersonalizedIdList = "";
		for (var i = 0; i < multiSelectedContacts.length; i++) {
			if(multiSelectedContacts[i].imgprofilepic == "avatar_dis.png"){
				if(i == (multiSelectedContacts.length - 1)){
					gblRcBase64List = gblRcBase64List + "";
					gblRcPersonalizedIdList = gblRcPersonalizedIdList + multiSelectedContacts[i].lblName;
				}
				else{
					gblRcBase64List = gblRcBase64List + ",";
					gblRcPersonalizedIdList = gblRcPersonalizedIdList + multiSelectedContacts[i].lblName + ",";
				}
			}
			else{
				//if(gblDeviceInfo.name == "android"){
//					gblRcBase64List = "";
//					gblRcPersonalizedIdList = "";
//				}
				//else{
					try{
						if(i == (multiSelectedContacts.length - 1)){
							gblRcBase64List = gblRcBase64List + kony.convertToBase64(multiSelectedContacts[i].imgprofilepic.rawBytes).replace(/[\n\r\s\f\t\v]+/g, '');
							gblRcPersonalizedIdList = gblRcPersonalizedIdList + multiSelectedContacts[i].lblName;
						}
						else{
							gblRcBase64List = gblRcBase64List + kony.convertToBase64(multiSelectedContacts[i].imgprofilepic.rawBytes).replace(/[\n\r\s\f\t\v]+/g, '') + ",";
							gblRcPersonalizedIdList = gblRcPersonalizedIdList + multiSelectedContacts[i].lblName + ",";
						}
					}
					catch(exception){
						multiSelectedContacts[i].imgprofilepic = "avatar_dis.png";
						if(i == (multiSelectedContacts.length - 1)){
							gblRcBase64List = gblRcBase64List + "";
							gblRcPersonalizedIdList = gblRcPersonalizedIdList + multiSelectedContacts[i].lblName;
						}
						else{
							gblRcBase64List = gblRcBase64List + ",";
							gblRcPersonalizedIdList = gblRcPersonalizedIdList + multiSelectedContacts[i].lblName + ",";
						}
					}
				//}
			}
		}
		
		
		if (gblRcBase64List.length > 0) {
			gblIsContactsFlow = true;
		}
		return totalData;
		//startReceipentSavingServiceMB(crmId, totalData.toString());
}

//function selectedContactsfnComp() {
//	if (multiSelectedContacts != null) {
//		for (var i = 0; i < multiSelectedContacts.length; i++) {
//			var tempData = {
//				lblName: multiSelectedContacts[i]["lblName"],
//				imgprofilepic: multiSelectedContacts[i]["imgprofilepic"],
//				lblAccountNum: "0 Accounts",
//				lblMob: stripDashPh(multiSelectedContacts[i]["lblMobile"]),
//				lblEmail: multiSelectedContacts[i]["lblEmail"],
//				lblFb: "",
//				isFav: "no"
//			}
//			myRecipientsRs.push(tempData);
//		}
//		isChMyRecipientsRs = true;
//		frmMyRecipientSelectContactsComp.segment24733076528972.setData(multiSelectedContacts);
//		multiSelectedContacts = [];
//		frmMyRecipientSelectContactsComp.show();
//	}
//}


//function startReceipentSavingServiceMB(crmId, collectionData) {
//	var inputParams = {
//		//serviceID:"receipentNewAddService",
//		crmId: crmId,
//		receipentList: collectionData
//	};
//	//kony.application.showLoadingScreen(frmLoading, "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
//    showLoadingScreen();
//	invokeServiceSecureAsync("receipentNewAddService", inputParams, startReceipentSavingServiceAsyncCallbackMB);
//}

/*function startReceipentSavingServiceAsyncCallbackMB(status, callBackResponse) {
	if (status == 400) {
		if (callBackResponse["opstatus"] == 0) {
			var responseData = callBackResponse["Results"];
			if (responseData.length > 0) {
				//kony.application.dismissLoadingScreen();
				var CurrForm = kony.application.getCurrentForm();
				if(CurrForm.id == "frmMyRecipientSelectContactsConf"){
					var rcList = "";
					for(var i = 0; i < multiSelectedContacts.length ; i++){
						if(i == (multiSelectedContacts.length - 1)){
							rcList = rcList + multiSelectedContacts[i]["lblName"];
						}
						else{
							rcList = rcList + multiSelectedContacts[i]["lblName"] + ", ";
						}
					}
					sendDeleteNotification("", "", "", rcList, "", "", "recipientsAdd", "");
					frmMyRecipientSelectContactsComp.segment24733076528972.setData(multiSelectedContacts);
					frmMyRecipientSelectContactsComp.show();
					isChMyRecipientsRs = true;
					isChRecipientAccountsRs = true;
					if(gblRcBase64List.length > 0){
						imageServiceCall();
					}
				}
				else if(CurrForm.id == "frmMyRecipientSelectFBConf"){
					var rcList = "";
					for(var i = 0; i < multiSelectedFacebook.length ; i++){
						if(i == (multiSelectedFacebook.length - 1)){
							rcList = rcList + multiSelectedFacebook[i]["lblName"];
						}
						else{
							rcList = rcList + multiSelectedFacebook[i]["lblName"] + ", ";
						}
					}
					sendDeleteNotification("", "", "", rcList, "", "", "recipientsAdd", "");
					frmMyRecipientSelectFBComp.sgmntFBFrndsRecpList.setData(multiSelectedFacebook);
					frmMyRecipientSelectFBComp.show();
					isChMyRecipientsRs = true;
					isChRecipientAccountsRs = true;
				}
			}
			else {
				//kony.application.dismissLoadingScreen();
				showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_Error"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
				//alert(kony.i18n.getLocalizedString("Receipent_alert_Error"));
			}
			kony.application.dismissLoadingScreen();
		}
		else {
			kony.application.dismissLoadingScreen();
			showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_Error"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
			//alert(kony.i18n.getLocalizedString("Receipent_alert_Error"));
		}
	}
	else {
		if (status == 300) {
			//kony.application.dismissLoadingScreen();
			showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
			//alert(kony.i18n.getLocalizedString("ECGenericError"));
		}
	}
}*/
//my recipients details page-------------------------------------------------------------------------------------------------------------------








function selectMobileNumber() {
	
	showLoadingScreen();
	selectedMobile = null;
	frmMyRecipientSelectMobile.label475095112172022.isVisible = false;
	frmMyRecipientSelectMobile.btnReturn.isVisible = false;
	var contact = 0;
	var fn = null;
	var ln = null;
	var ph = null;
	var em = null;
	var ph1 = null;
	var ph2 = null;
	var ph3 = null;
	var btnn1 = null;
	var btnn2 = null;
	var btnn3 = null;
	var phnos = new Array();
	var flag1 = 0;
	var flag2 = 0;
	var flag3 = 0;
	ContactList.length = 0;
	/*frmMyRecipientSelectMobile.segMyRecipient.widgetDataMap = {
	    lblName: "lblName",
	    lblMobile: "lblMobile",
	    lblEmail: "lblEmail",
	    imgprofilepic: "imgprofilepic",
	    imgchbxbtn: "imgchbxbtn",
	    button47506532118567: btnn1,
	    btn1: btnn2,
	    btn2: btnn3,
	    lblMobile1: "lblMobile1",
	    lblMobile2: "lblMobile2",
	    lblMobile3: "lblMobile3"
	}*/
	var contacts = kony.contact.find("*", true);
	
	if (contacts == null || contacts == undefined || (contacts != null && contacts!=undefined && contacts.length < 1)) {
		frmMyRecipientSelectMobile.label475095112172022.isVisible = true;
		frmMyRecipientSelectMobile.label475095112172022.text=kony.i18n.getLocalizedString("NoContacts");
		frmMyRecipientSelectMobile.btnReturn.isVisible = true;
		frmMyRecipientSelectMobile.btnNext.isVisible = false;
	}
	else{
		for (var contact = 0; contact < contacts.length; contact++) {
				if(contacts[contact]["phone"] == null || contacts[contact]["phone"] == undefined){
				
					phnos.push("NULL");
					
				}
				else{
				
					phnos.push(contacts[contact]["phone"]);
				}
		}
			for (var contact = 0; contact < contacts.length; contact++) {
				//break;
				if (contacts[contact]["firstname"] == null || contacts[contact]["firstname"] == "") fn = "";
				else fn = contacts[contact]["firstname"];
				if (contacts[contact]["lastname"] == null || contacts[contact]["lastname"] == "") ln = "";
				else ln = contacts[contact]["lastname"];
				var m = 0;
				while (true) {
					if(phnos[contact] == "NULL"){
					
						break;
					}
					if(phnos[contact][m] == null || phnos[contact][m] == undefined){
						break;
				 	}
					if (phnos[contact][m]["name"] == "mobile") {
							ph1 = stripDashNew(phnos[contact][m]["number"]);
							ph1 = addDashPh(ph1);
							flag1 = 1;
							//make things visible
						}
						else if (phnos[contact][m]["name"] == "home") {
							ph2 = stripDashNew(phnos[contact][m]["number"]);
							ph2 = addDashPh(ph2);
							flag2 = 1;
							//make things visible
						}
						else if (phnos[contact][m]["name"] == "work") {
							ph3 = stripDashNew(phnos[contact][m]["number"]);
							ph3 = addDashPh(ph3);
							flag3 = 1;
							//make things visible
						}
						else{
							if(flag1 == 0){
								ph1 = stripDashNew(phnos[contact][m]["number"]);
								ph1 = addDashPh(ph1);
								flag1 = 1;
							}
							else if(flag2 == 0){
								ph2 = stripDashNew(phnos[contact][m]["number"]);
								ph2 = addDashPh(ph2);
								flag2 = 1;
							}
							else {
								ph3 = stripDashNew(phnos[contact][m]["number"]);
								ph3 = addDashPh(ph3);
								flag3 = 1;
							}
						}
					m++;
				}
				if (flag1 == 0) {
					ph1 = "";
				}
				if (flag2 == 0) {
					ph2 = "";
				}
				if (flag3 == 0) {
					ph3 = "";
				}
				flag1 = 0;
				flag2 = 0;
				flag3 = 0;
				
				/*if (contacts[contact]["phone"][0]["number"] == null || contacts[contact]["phone"][0]["number"] == "") pn = "";
	            else{
	            	pn = contacts[contact]["phone"][0]["number"];
	            	for( var i=0;i<(pn.length-1);i++){
						if(pn[i] == '-' || pn[i] == '(' || pn[i] == ')' || pn[i] == ' '){
						pn = pn.replace("-","");
						pn = pn.replace("(","");
						pn = pn.replace(")","");
						pn = pn.replace(" ","");
						}
					}
	            }*/
				//if (contacts[contact]["email"][0]["id"] == null || contacts[contact]["email"][0]["id"] == "") em = "";
				//else em = contacts[contact]["email"][0]["id"];
				if (contacts[contact]["email"]) {
					if (contacts[contact]["email"][0]) {
						if (contacts[contact]["email"][0]["id"]) {
							em = contacts[contact]["email"][0]["id"];
						}
						else {
							em = "";
						}
					}
					else {
						em = "";
					}
				}
				else {
					em = "";
				}
				if (contacts[contact]["photorawbytes"] == null || contacts[contact]["photorawbytes"] == "") ph = "";
				else ph = contacts[contact]["photorawbytes"];
				if(fn != ""){
					if(ph1 != "" || ph2 != "" || ph3 != ""){
						ContactList.push(new Contact1(fn, ln, ph1, ph2, ph3, em, ph));
					}
				}
				ContactList.sort(dynamicSortOther("lblName"));
			}
			frmMyRecipientSelectMobile.btnNext.isVisible = true;
			frmMyRecipientSelectMobile.segMyRecipient.setData(ContactList);
			contactListToggle = true;
			/*for(var i in ContactList){
	        	var character = i["lblName"];
	        	character = character.charAt(0);
	        	data==["section1"];
	        	var data = [ [ "section1",  ], [ "section2",  ]];
	        	frmMyRecipientSelectMobile.segMyRecipient.setSectionAt(data, 0)
	        }*/
	}
	dismissLoadingScreen();
}

function Contact1(lblFName, lblLName, lblMobile1, lblMobile2, lblMobile3, lblEmail, imgprofilepic) {
	var i = null;
	var j = null;
	var k = null;
	var l = null;
	this.lblName = lblFName + " " + lblLName;
	this.lblMobileNo1 = lblMobile1;
	this.lblMobileNo2 = lblMobile2;
	this.lblMobileNo3 = lblMobile3;
	this.lblEmail = lblEmail;
	if (imgprofilepic == null || imgprofilepic == "") {
		this.imgprofilepic = "avatar_dis.png";
	}
	else {
		this.imgprofilepic = {
			rawBytes: imgprofilepic
		};
	}
	this.imgchbxbtn = "radiobtn2.png";
	if (lblMobile1 == "") i = 0;
	else i = 1;
	if (lblMobile2 == "") j = 0;
	else j = 1;
	if (lblMobile3 == "") k = 0;
	else k = 1;
	if ((parseInt(i) + parseInt(j) + parseInt(k)) == 1) l = 1;
	else l = 0;
	if (lblMobile1 != "") {
		if (l == 1) btnn1 = {
			isVisible: false
		};
		else btnn1 = {
			isVisible: true,
			skin: "btnRadioFoc"
		};
		this.btnMobileNo1 = btnn1;
	}
	if (lblMobile2 != "") {
		if (l == 1) btnn2 = {
			isVisible: false
		};
		else btnn2 = {
			isVisible: true,
			skin: "btnRadio"
		};
		this.btnMobileNo2 = btnn2;
	}
	if (lblMobile3 != "") {
		if (l == 1) btnn3 = {
			isVisible: false
		};
		else btnn3 = {
			isVisible: true,
			skin: "btnRadio"
		};
		this.btnMobileNo3 = btnn3;
	}
}

function stripDashNew(pn) {
	if (pn == null) return "";
	for (var i = 0; i < (pn.length - 1); i++) {
		if (pn[i] == '-' || pn[i] == '(' || pn[i] == ')' || pn[i] == ' ') {
			pn = pn.replace('-', '');
			pn = pn.replace('(', '');
			pn = pn.replace(')', '');
			pn = pn.replace(' ', '');
			//pn = pn.replace("(", "");
//			pn = pn.replace(")", "");
//			pn = pn.replace(" ", "");
		}
	}
	return pn;
}
//---------------------------------------facebook functionality ---------------------------------------------------------------------------
var offset = "0",
	limit = "0";
var multiSelectedFacebook = new Array();
var notAllowedFacebook = new Array();


//---------------------------------facebook functionality-------------------------------------------------------------------------------------------
var gblOffsetMB = "0", gblLimitMB = "0", gblStart = "0";
var gblFBID = "";
var gblSearch = "0";
function getFriendListFromFacebook(eventobject) {
	var getFriends_inputparam = {};
	getFriends_inputparam["fbUser"] = "";//gblFBID;
	getFriends_inputparam["offset"] = gblOffsetMB;
	getFriends_inputparam["limit"] = gblLimitMB;
	getFriends_inputparam["search"] = gblSearch;
	getFriends_inputparam["start"] = gblStart;
	if(eventobject == null || eventobject == "" || eventobject == undefined){
		getFriends_inputparam["character"] = "0";
	}else{
		getFriends_inputparam["character"] = eventobject.text;
	}
	getFriends_inputparam["httpheaders"] = {};
	getFriends_inputparam["httpconfigs"] = {};
	//kony.application.showLoadingScreen(frmLoading, "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
    showLoadingScreen();
    if(eventobject.id == "btnBackFacebook"){
    	gblPOWstateTrack = true;
    }
	invokeServiceSecureAsync("getFriends",getFriends_inputparam, callbackFacebookFriendsList);
}

var gbltempDataFriends = new Array();
var gblMORE = false;

function callbackFacebookFriendsList(status, getFriends) {
	
	if(status == 400)
	{
		if(getFriends["opstatus"] == 0)
		{
			
			//invokeFacebookSetUp();
			if(getFriends["invokeServlet"] == "yes"){
				
				if(gblPOWstateTrack == true){
					kony.application.dismissLoadingScreen();
					if(requestFromForm == "editprofile"){
						frmMyRecipientEditProfile.show();
					}
					if(requestFromForm == "addprofile"){
						frmMyRecipientAddProfile.show();
					}
					if(requestFromForm == "addFacebook"){
						frmMyRecipients.show();
					}
					//gblPOWstateTrack = false;
				}
				else{
					kony.application.dismissLoadingScreen();
					invokeFacebookSetUp();
				}
			}else{
				/* if(flowSpa)
				{
					if(requestFromForm=="editprofile" || requestFromForm=="addprofile"){
						frmMyRecipientSelectFbID.show();
					}
						
					else if(requestFromForm=="addFacebook"){
							frmMyRecipientSelectFacebook.show();
							}
				} */
				
				//var tempData = [];
				//replacing old tempData with gbltempDataFriends to append friends to previous set of friends onclick of MORE
				gbltempDataFriends.length = 0;
				gbltempDataFriends = [];
				var currForm = kony.application.getCurrentForm();
				if(gblPOWstateTrack == true){
					if(requestFromForm == "editprofile" || requestFromForm == "addprofile"){
						currForm = frmMyRecipientSelectFbID;
					}
					if(requestFromForm == "addFacebook"){
						currForm = frmMyRecipientSelectFacebook;
					}
					currForm.show();
				}
				var len = getFriends["NewResult"].length;
				if(gblStart == "0"){
					if(len > 0){
						gblFBFriendsAvailability = true;
					}
					else{
						gblFBFriendsAvailability = false;
					}
				}
				if(len > 0){//friendlist is returned
					if (getFriends["flag"] != "0") {//no more friends
						if (getFriends["flag"] == "1") { 
							//len = len - 1;
							currForm.btnMore.setVisibility(false);
						}
					}else{//more friends available
						currForm.btnMore.setVisibility(true);
					}
					if(gblLimitMB == "0"){
						currForm.btnMore.setVisibility(false);
					}
					currForm.label47402263614029.setVisibility(false);
					if(flowSpa)
					{
					currForm.btnReturnSpa.setVisibility(false);
					currForm.btnNextSpa.setVisibility(true);
					}
					else
					{
					currForm.btnReturn.setVisibility(false);
					currForm.btnNext.setVisibility(true);
					}
					
					if(currForm.id == "frmMyRecipientSelectFacebook")
						select = "chkbox2.png";
					else{
						select = "radiobtn2.png";
					}
					for (var i1 = 0; i1 < len; i1++) {
						gbltempDataFriends.push({
							"imgprofilepic" : getFriends["NewResult"][i1]["picture"],
							"lblName": getFriends["NewResult"][i1]["name"],
							"lblMobile": getFriends["NewResult"][i1]["mobile"],
							"lblUsername": getFriends["NewResult"][i1]["id"],
							"imgchbxbtn": select
						});
					}
					//currForm.segMyRecipient.data = tempData;
					if(gblMORE == true){
						currForm.segMyRecipient.addAll(gbltempDataFriends);
						gblMORE = false;
					}
					else{
						currForm.segMyRecipient.setData(gbltempDataFriends);
					}
					//currForm.show();
				}
				else
				{//friendlist not returned
					currForm.segMyRecipient.removeAll();
					if(gblFBFriendsAvailability == true){
						currForm.btnMore.setVisibility(false);
						currForm.label47402263614029.setVisibility(true);
						if(flowSpa)
						{
						currForm.btnReturnSpa.setVisibility(false);
						currForm.btnNextSpa.setVisibility(false);
						}
						else
						{
						currForm.btnReturn.setVisibility(false);
						currForm.btnNext.setVisibility(false);
						}
						
					}
					else{
						currForm.btnMore.setVisibility(false);
						currForm.label47402263614029.setVisibility(true);
						if(flowSpa)
						{
						currForm.btnReturnSpa.setVisibility(true);
						currForm.btnNextSpa.setVisibility(false);
						}
						else
						{
						currForm.btnReturn.setVisibility(true);
						currForm.btnNext.setVisibility(false);
						}
						
					}
				}//
				kony.application.dismissLoadingScreen();
				
			}
			gblPOWstateTrack = false;
			//kony.application.dismissLoadingScreen();
		}
		else{
			if(getFriends["empty"]=="page")
			{
				kony.application.dismissLoadingScreen();
			}
			else
			{
				showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
				//alert(kony.i18n.getLocalizedString("ECGenericError"));
				kony.application.dismissLoadingScreen();
			}
		}
	}
	
}

function invokeFacebookSetUp(){
	
	if(frmContactusFAQMB != null && frmContactusFAQMB != undefined && flowSpa == false){
		frmContactusFAQMB.browser506459299404741.clearHistory();
		TMBUtil.DestroyForm(frmContactusFAQMB);
	} else{
	}
	
	
	if(flowSpa) {
	    var fburl ="https://" + appConfig.serverIp + "/" + appConfig.middlewareContext + "/fbsetup?locale=" + kony.i18n.getCurrentLocale() + "&fbidentity=" + encryptCRMId(gblcrmId);
	} else {
	    var fburl = "https://" + appConfig.serverIp + "/" + appConfig.middlewareContext + "/fbsetup?locale=" + kony.i18n.getCurrentLocale();
	}	
	//var fburl = "https://" + appConfig.serverIp + "/TMBIB/fbsetup";
	frmContactusFAQMB.browser506459299404741.url = fburl;
	frmContactusFAQMB.hbox475124774143.setVisibility(false);
    frmContactusFAQMB.hbox476018633224052.setVisibility(true);
    frmContactusFAQMB.button506459299404751.setVisibility(false);
    assignHandleRequest();
	frmContactusFAQMB.show();
}

function selectedFacebookfn() {
	notAllowedFacebook.length = 0;
	multiSelectedFacebook.length = 0;
	var extra99Check = null;
	var alertString = "";
	var dashStripped = null;
	
	//extra99Check = 99 - myRecipientsRs.length + multiSelectedFacebook.length;
//	extra99Check = 5 - extra99Check;
	if (frmMyRecipientSelectFacebook.segMyRecipient.selectedItems != null) {
		
		for (var i=0; i < frmMyRecipientSelectFacebook.segMyRecipient.selectedItems.length; i++) {
				multiSelectedFacebook.push(frmMyRecipientSelectFacebook.segMyRecipient.selectedItems[i]);
		}
		extra99Check = gblMAXRecipientCount - (myRecipientsRs.length + gblAddMoreFromFacebookCache.length + multiSelectedFacebook.length);
		if (extra99Check < 0) {
			showAlertRcMB(kony.i18n.getLocalizedString("J200000001"), kony.i18n.getLocalizedString("info"), "info")
			//alert(kony.i18n.getLocalizedString("KeyLimitExceeded"));
			return;
		}
		if (multiSelectedFacebook.length > MAX_RECIPIENT_BULK_COUNT) {
			showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_selectlessthanfive").replace("<1>",MAX_RECIPIENT_BULK_COUNT), kony.i18n.getLocalizedString("info"), "info")
			//alert(kony.i18n.getLocalizedString("Receipent_alert_selectlessthanfive"));
			return;
		}
		else {
			if (myRecipientsRs.length == 0 || myRecipientsRs == null) {
				//add without checking duplicacy
				frmMyRecipientSelectFBConf.segment24733076528972.setData(multiSelectedFacebook);
				frmMyRecipientSelectFBConf.show();
			}
			else {
				for (var i = 0; i < multiSelectedFacebook.length; i++) {
					for (var k = 0; k < myRecipientsRs.length; k++) {
						if (multiSelectedFacebook[i]["lblName"] == myRecipientsRs[k]["lblName"]["text"]) {
							notAllowedFacebook.push(multiSelectedFacebook[i]["lblName"]);
						}
					}
				}
				for (var i = 0; i < multiSelectedFacebook.length; i++) {
					for (var k = 0; k < gblAddMoreFromFacebookCache.length; k++) {
						if (multiSelectedFacebook[i]["lblName"] == gblAddMoreFromFacebookCache[k].lblName) {
							notAllowedFacebook.push(multiSelectedFacebook[i]["lblName"]);
						}
					}
				}
				if (notAllowedFacebook.length != 0) {
					for (var j = 0; j < notAllowedFacebook.length; j++) {
						alertString = notAllowedFacebook[j] + " ";
					}
					showAlertRcMB(alertString + " " + kony.i18n.getLocalizedString("RecipientExists"), kony.i18n.getLocalizedString("info"), "info")
					//alert(alertString + " " + kony.i18n.getLocalizedString("RecipientExists"));
				}
				else {
					frmMyRecipientSelectFBConf.segment24733076528972.setData(multiSelectedFacebook);
					frmMyRecipientSelectFBConf.show();
				}
			}
		}
	}
	else
		showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_selectone"), kony.i18n.getLocalizedString("info"), "info")
		//alert(kony.i18n.getLocalizedString("Receipent_alert_selectone"));
}

function transCancelSelectFacebook() {
	popTransactionPwd.dismiss();
}

function transConfirmSelectFacebookPre() {
	gblFlagTransPwdFlow = "addRcFacebook";
	checkVerifyPWDMBRC();
}

function transConfirmSelectFacebook() {
	var totalData = [];
	for (var i = 0; i < multiSelectedFacebook.length; i++) {
		var Name = multiSelectedFacebook[i]["lblName"];
		var Pic = multiSelectedFacebook[i]["imgprofilepic"];
		var Number = multiSelectedFacebook[i]["lblMobile"];
		//var Email = multiSelectedFacebook[i]["lblEmail"];
		var fbId = multiSelectedFacebook[i]["lblUsername"];
		var FacebookRcData = [Name, Pic, , Number, fbId, "Added","N"];
		totalData.push(FacebookRcData);
	}
	return totalData;
	//startReceipentSavingServiceMB(crmId, totalData.toString());
}

function checkVerifyPWDMBRC(){
	executeRecipientMB();
} 


function sendDeleteNotification(deleted, subject, content, RecipientName, RecipientNickName, ProfilePic, Source, AddedAccountDetails)
{
	
	if(deleted == "recipient"){
		subject = "Deleted Recipient";
		content = "Recipient '"+currentRecipientDetails["name"]+"' has been deleted";
	}else if(deleted == "account"){
		subject = "Deleted Account";
		content = "Account '"+frmMyRecipientViewAccount.lblAccountNo.text+"' has been deleted from recipient '" +currentRecipientDetails["name"]+ "'";
	}else if(deleted == "addaccount"){
		if(AddAccountList.length == 1){
			subject = "Account Added";
			content = "Account '"+addDashAc(stripDashAcc(AddAccountList[0].lblAccountNo.text))+"' has been added to recipient '" +currentRecipientDetails["name"]+ "'";
			Source = "recipientsAdd";
			AddedAccountDetails = createListForRcAccountNotificationMB();
			
		}else if(AddAccountList.length == 2){
		
			subject = "Accounts Added";
			content = "Accounts '"+addDashAc(stripDashAcc(AddAccountList[0].lblAccountNo.text))+"' and '"+addDashAc(stripDashAcc(AddAccountList[1].lblAccountNo.text))+"' have been added to recipient '" +currentRecipientDetails["name"]+ "'";
			Source = "recipientsAdd";
			AddedAccountDetails = createListForRcAccountNotificationMB();
			
		}
	}else if(deleted == "addrecipient"){
		subject = "Recipient Added";
		content = "Recipient '"+frmMyRecipientAddProfile.tbxRecipientName.text+"' has been added";
		RecipientName = frmMyRecipientAddProfile.tbxRecipientName.text;
		RecipientNickName = "";
		ProfilePic = "";
		AddedAccountDetails = "";
		Source = "recipientsAdd";
	}else if(deleted == "addrecipientacc"){
		subject = "Recipient Added with Account";
		content = "Recipient '";
		RecipientName = frmMyRecipientAddProfile.tbxRecipientName.text;
		RecipientNickName = "";
		ProfilePic = "";
		Source = "recipientsAdd";
		AddedAccountDetails = createListForRcAccountNotificationMB();
		//if(AddAccountList.length == 1){
//			subject = "Recipient Added with Account";
//			content = "Recipient '"+frmMyRecipientAddAccConf.lblName.text+"' has been added with Account '"+addDashAc(stripDashAcc(AddAccountList[0].lblAccountNo.text))+"'";
//			AddedAccountDetails = createListForRcAccountNotification();
//		}else if(AddAccountList.length == 2){
//			subject = "Recipient Added with Accounts";
//			content = "Recipient '"+frmMyRecipientAddAccConf.lblName.text+"' has been added with Accounts '"+addDashAc(stripDashAcc(AddAccountList[0].lblAccountNo.text))+"' and '"+addDashAc(stripDashAcc(AddAccountList[1].lblAccountNo.text))+"'";
//			AddedAccountDetails = createListForRcAccountNotification();
//		}
	}
	var locale="en_US";
	if(gblCustomerName == null || gblCustomerName == undefined){
		gblCustomerName = "";
	}
	if(gblPHONENUMBER == null || gblPHONENUMBER == undefined){
		gblPHONENUMBER = "";
	}
	var inputParams = {
		//channelName:"MB-INQ",
		notificationType:"Email",
		phoneNumber:gblPHONENUMBER,
		//emailId:gblEmailAddr,
		notificationSubject:subject,
		notificationContent:content,
		recipientName:RecipientName,
		recipientNickName:gblCustomerName,
		profilePic:"",
		addedAccountDetails:AddedAccountDetails,
		source:Source,
		Locale:locale,
		Channel:"MB",
		mobile:"",
		mail:"",
		facebook:"",
		custNME:gblCustomerName
	};
    invokeServiceSecureAsync("NotificationAdd", inputParams, sendDeleteNotificationCallBack);
}

function sendDeleteNotificationCallBack(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == 0) {
         /*   var responseData = callBackResponse["notificationAddRs"];
            if(kony.type(responseData.length)

        } else {
            alert(kony.i18n.getLocalizedString("keyErrNotification"));
        }*/
        }
    } else {
        if (status == 300) {
           // alert(kony.i18n.getLocalizedString("keyErrNotification"));
        }
    }
}

function createListForRcAccountNotificationMB(){
	var totalData = "";
	if (AddAccountList.length > 0) {
		for (var i = 0; i < AddAccountList.length; i++) {
			totalData = totalData + addCrossAccount(stripDashAcc(AddAccountList[i].lblAccountNo.text)) + ",";
			totalData = totalData + AddAccountList[i].lblNick.text+ ",";
			totalData = totalData + AddAccountList[i].lblBankName.text
			if(i != (AddAccountList.length - 1)){
				totalData = totalData + ",";
			}
		}
	}
	return totalData.toString();
}


function onClickMyRecipientAddAccConform(){
  try{
    kony.print("@@@ In onClickMyRecipientAddAccConform() @@@");
    kony.print("@@@DeviceType:::"+gblDeviceInfo["name"]);
    if(gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "android"){
      if((gblUserLockStatusMB == gblFinancialTxnMBLock)){
        alertUserStatusLocked.call(this);
      }else{
        showOTPPopup(kony.i18n.getLocalizedString("TransactionPass") + ":", "", "", addAccountRecipentCompletePre, 3);
      }
    }else{
      if((gblIBFlowStatus == "04")){
        alertUserStatusLocked.call(this);
      }else{
        var inputParams = {}
        spaChnage = "addrecipients"
        gblOTPFlag = true;
        try{
          kony.timer.cancel("otpTimer")
        }catch(e){
          kony.print("Timer doesnot exist");
        }
        gblSpaChannel = "IB-NEW_RC_ADDITION";
        if(locale == "en_US"){
          SpaEventNotificationPolicy = "MIB_AddRecipient_EN";
          SpaSMSSubject = "MIB_AddRecipient_EN";
        }else{
          SpaEventNotificationPolicy = "MIB_AddRecipient_TH";
          SpaSMSSubject = "MIB_AddRecipient_TH";
        }
        saveRecipientDeatilsMB();
      }
    }
  }catch(e){
    kony.print("@@@ In onClickMyRecipientAddAccConform() Exception:::"+e);
  }
}



function alertUserStatusLocked(){
if(flowSpa)
	{
		//showAlert(kony.i18n.getLocalizedString("Receipent_OTPLocked"), kony.i18n.getLocalizedString("info"));
		popTransferConfirmOTPLock.show();
			
			return false;
	}
else
	 popTransferConfirmOTPLock.show();
	 //updated the locked status by popup
	//showAlertRcMB(kony.i18n.getLocalizedString("keyErrTxnPasslock"), kony.i18n.getLocalizedString("info"), "info")
	//alert(kony.i18n.getLocalizedString("keyErrTxnPasslock"));
}

function alert99LimitReached(){
	showAlertRcMB(kony.i18n.getLocalizedString("J200000001"), kony.i18n.getLocalizedString("info"), "info")
	//alert(kony.i18n.getLocalizedString("J200000001"));
}

function checkAccountDuplicate(accountNo){
	accountNo = stripDashAcc(accountNo);
	gblReceipentAccts = gblAccountTable["custAcctRec"];
	if(gblReceipentAccts != undefined && gblReceipentAccts.length > 0){
		for(var i = 0; i < gblReceipentAccts.length ; i++){
			var testAcc = stripDashAcc(gblReceipentAccts[i]["accId"]);
			if(testAcc.length == 14){
				testAcc = testAcc.substr(4);
			}
			if(stripDashAcc(accountNo) == testAcc){
				if(gblReceipentAccts[i]["productID"] == Gbl_Prod_Code_MEACC)
					showAlertRcMB(kony.i18n.getLocalizedString("keyReceipent_alert_MEAccountAdd"), kony.i18n.getLocalizedString("info"), "info")
				else
					showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_correctaccount"), kony.i18n.getLocalizedString("info"), "info")
				return false;
			}
		}
	}
	return true;
}

function CheckRecipientNameUniq(text,textAllowed){
	
	if(text == null || text == undefined){
		return false;
	}
	for(var i = 0; i < myRecipientsRs.length; i++){
		if(isNotBlank(myRecipientsRs[i]["lblName"]["text"])){
			if(isNotBlank(textAllowed)){
				if(kony.string.equals(myRecipientsRs[i]["lblName"]["text"], textAllowed)){
					//do nothing
				}else{
					if(kony.string.equals(myRecipientsRs[i]["lblName"]["text"], text)){
						return false;
					}
				}
			}
			else{
				if(kony.string.equals(myRecipientsRs[i]["lblName"]["text"], text)){
					return false;
				}
			}
		}
	}
	if(gblAddMoreFromManualCache.length > 0){
		for(var i = 0; i < gblAddMoreFromManualCache.length; i++){
			if(kony.string.equals(gblAddMoreFromManualCache[i], text)){
				return false;
			}
		}
	}
	return true;
}



function startIBFBPOWService()
{		
				FACEBOOK_SERVLET_URL = "https://" + appConfig.serverIp + "/" + appConfig.middlewareContext + "/fbsetup?locale=" + kony.i18n.getCurrentLocale() +
					"&fbidentity=" + encryptCRMId(gblcrmId);
				frmIBFBLogin.browser587790738116119.url = FACEBOOK_SERVLET_URL;
				frmIBFBLogin.button474139778233674.setEnabled(false);
				frmIBFBLogin.button474139778233674.setVisibility(false);
				frmIBFBLogin.button1010836759314200.setEnabled(false);
				frmIBFBLogin.button1010836759314200.setVisibility(false);
				frmIBFBLogin.button588742039507654.setEnabled(true);
				frmIBFBLogin.button588742039507654.setVisibility(true);
				frmIBFBLogin.show();
				//window.location = FACEBOOK_SERVLET_URL;
}

function startIBFBMyProfileService()
{		
				FACEBOOK_SERVLET_URL = "https://" + appConfig.serverIp + "/" + appConfig.middlewareContext + "/fbsetup?locale=" + kony.i18n.getCurrentLocale() +
					"&fbidentity=" + encryptCRMId(gblcrmId);
				frmIBFBLogin.browser587790738116119.url = FACEBOOK_SERVLET_URL;
				frmIBFBLogin.button474139778233674.setEnabled(false);
				frmIBFBLogin.button474139778233674.setVisibility(false);
				frmIBFBLogin.button1010836759314200.setEnabled(true);
				frmIBFBLogin.button1010836759314200.setVisibility(true);
				frmIBFBLogin.button588742039507654.setEnabled(false);
				frmIBFBLogin.button588742039507654.setVisibility(false);
				frmIBFBLogin.show();
				//window.location = FACEBOOK_SERVLET_URL;
}

function startIBFBFriendsService()
{
	FACEBOOK_SERVLET_URL = "https://" + appConfig.serverIp + "/" + appConfig.middlewareContext + "/fbsetup?locale=" + kony.i18n.getCurrentLocale() +
						"&fbidentity=" + encryptCRMId(gblcrmId);
	frmIBFBLogin.browser587790738116119.url = FACEBOOK_SERVLET_URL;
	frmIBFBLogin.button474139778233674.setEnabled(true);
	frmIBFBLogin.button474139778233674.setVisibility(true);
	frmIBFBLogin.button1010836759314200.setEnabled(false);
	frmIBFBLogin.button1010836759314200.setVisibility(false);
	frmIBFBLogin.button588742039507654.setEnabled(false);
	frmIBFBLogin.button588742039507654.setVisibility(false);
	//FBTimerCount = 0;
	frmIBFBLogin.show();
	//window.location = FACEBOOK_SERVLET_URL;
}

function delinkFacebookWrapper(fbUser){
	var inputParam = {
						fbUser : fbUser
					 };
	invokeServiceSecureAsync("fbdelinkservice", inputParam, delinkFacebookWrapperCB);
}

function delinkFacebookWrapperCB(status, resulttable) {
	if (status == 400) {
		if (resulttable["status"] == "success") {
			if(flowSpa)
			{
				fbprofileviewServiceCall();
			}
			else{	
				if(gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "android"){
					fbprofileviewServiceCall();
				}
				else{
					fbDlink=false;
					IBfbprofileviewServiceCall();
				}
			}
		}
		else if (resulttable["status"] == "fail") {
			if(flowSpa)
			{
				//handler for SPA like dismiss loading and show some alert;
			}
			else{	
				if(gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "android"){
					//handler for MB like dismiss loading and show some alert;
				}
				else{
					dismissLoadingScreenPopup();
					// some alert message also needs to be shown
				}
			}
			//alert("DELINK FAILURE");
		}
	}		
}

function assignHandleRequest(){
//	frmFBLogin.browserFB.handleRequest = handleRequestCallback;
	frmContactusFAQMB.browser506459299404741.handleRequest=handleRequestCallback;
}
 
function handleRequestCallback(brwsrCitiWallet,params){
	try{
		//alert(params["originalURL"]);
    var	autoCloseURL = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/closewindow.html"; 
    var	autoCloseURL1 = "https://" + appConfig.serverIp + "/" + appConfig.middlewareContext + "/closewindow.html"; 
        if(params["originalURL"].toLowerCase() == autoCloseURL.toLowerCase() || params["originalURL"].toLowerCase() == autoCloseURL1.toLowerCase()){
       // 	frmFBLogin.btnBackFacebook.onClick(frmFBLogin.btnBackFacebook);
        	frmContactusFAQMB.btnBackFacebook.onClick(frmContactusFAQMB.btnBackFacebook);
        	return true;
        }
        else{
        	return false;
        }
	}
    catch(err){
    	
    }
}

function saveRecipientDeatilsMB(){
	if(flowSpa)
	recipientTokenExchangeServiceMB();
	
}
function saveRecipientDeatilsCallBackMB(status,results){
	
		if(status==400){
		dismissLoadingScreen();
		if(results["opstatus"] == 1){
			alert(results["errMsg"]);
			return false;
		}else{
		if(flowSpa){
			onClickOTPRequestSpa();
		}
		}
	}
	
}
function recipientTokenExchangeServiceMB() {
 var inputParam = [];
 showLoadingScreen();
 invokeServiceSecureAsync("tokenSwitching", inputParam, recipientTokenExchangeServiceCallbackMB);
}

function recipientTokenExchangeServiceCallbackMB(status,resulttable){
//---DEF1264
	if(frmMyRecipientAddProfile.imgProfilePic.src!=null&&frmMyRecipientAddProfile.imgProfilePic.src!=undefined){
	beforeOTPMB=frmMyRecipientAddProfile.imgProfilePic.src
	if(beforeOTPMB.indexOf("fbcdn")>=0)
	gblPhotoSource=frmMyRecipientAddProfile.imgProfilePic.src
	//added above code for fb recipients add flow
}
  if (status == 400) {
	   if(resulttable["opstatus"] == 0){
	   var inputParams={};
		if(gblFlagTransPwdFlow == "editProfile"){
					
					if(gblRcBase64List != ""){
							
							inputParams["fileType"]="png";
							inputParams["personalizedId"]=gblRcPersonalizedIdList;
							inputParams["base64ImageString"]=gblRcBase64List;
							//gblRcPersonalizedIdList = currentRecipientDetails.rcId;
							//imageServiceCall();
						}
						
						inputParams["personalizedId"]= currentRecipientDetails["rcId"],
						inputParams["personalizedName"]= frmMyRecipientEditProfile.tbxRecipientName.text.replace(/^\s+|\s+$/g, ""),
						inputParams["personalizedPictureId"]= gblRcImageUrlList[0],//frmMyRecipientEditProfile.imgprofilepic.src,
						inputParams["personalizedMailId"]= frmMyRecipientEditProfile.tbxEmail.text,
						inputParams["personalizedMobileNumber"]= stripDashPh(frmMyRecipientEditProfile.tbxMobileNo.text),
						inputParams["personalizedFacebookId"]= frmMyRecipientEditProfile.tbxFbID.text,
						inputParams["personalizedStatus"]= "Added";
						inputParams["gblTransactionType"]="2";
						inputParams["oldRcName"]=currentRecipientDetails["name"]//frmIBMyReceipentsAccounts.lblRcName.text
						inputParams["oldRcmobile"]=stripDashPh(currentRecipientDetails["mobile"])//frmIBMyReceipentsAccounts.lblRcMobileNo.text
						inputParams["oldRcEmail"]=currentRecipientDetails["email"]//frmIBMyReceipentsAccounts.lblRcEmail.text
						inputParams["oldFbId"]=currentRecipientDetails["facebook"]//frmIBMyReceipentsAccounts.lblRcFbId.text
						//invokeServiceSecureAsync("receipentEditService", inputParams, startExistingRcMBProfileEditServiceAsyncCallback);
						//startExistingRcMBProfileEditService("imageNotAdded");
					
				//startExistingRcMBProfileEditService();
			}else if(gblFlagTransPwdFlow == "addAccount"){
				
				if (isRecipientNew == false) {
					if(AddAccountList.length ==0){
							//showAlert(kony.i18n.getLocalizedString("Receipent_alert_atleastonebankaccount"), kony.i18n.getLocalizedString("info"));
							//return;
					}
						inputParams["personalizedAccList"] = completeRecepientAddAccount();
			          	inputParams["gblTransactionType"]="4";
			          	inputParams["oldRcName"]=currentRecipientDetails["name"]//frmIBMyReceipentsAccounts.lblRcName.text
						inputParams["oldRcmobile"]=stripDashPh(currentRecipientDetails["mobile"])//frmIBMyReceipentsAccounts.lblRcMobileNo.text
						inputParams["oldRcEmail"]=currentRecipientDetails["email"]//frmIBMyReceipentsAccounts.lblRcEmail.text
						inputParams["oldFbId"]=currentRecipientDetails["facebook"]//frmIBMyReceipentsAccounts.lblRcFbId.text
						//invokeServiceSecureAsync("receipentAddBankAccntService", inputParams, addAccountServiceCallbackMB);
					}
					else {
						
						if(gblAddContactFlow==true){
									
									
									gblRcBase64List = frmMyRecipientAddAccComplete.imgprofilepic.base64;
									if(gblRcBase64List == null ) gblRcBase64List="";
									gblRcBase64List = gblRcBase64List.replace(/[\n\r\s\f\t\v]+/g, '')+",";
									gblRcPersonalizedIdList=frmMyRecipientAddAccComplete.lblName.text;
								//imageServiceCall();
						}
						if(gblRcBase64List != ""){
									inputParams["fileType"]="png";
									inputParams["personalizedId"]=gblRcPersonalizedIdList;
									inputParams["base64ImageString"]=gblRcBase64List;
									//imageServiceCall();
						}
						if(AddAccountList.length > 0){
							gblRcPersonalizedIdList = frmMyRecipientAddAccConf.lblName.text;
							var totalDataRecipient = [];
							var name = frmMyRecipientAddProfile.tbxRecipientName.text;
							var PictureId = gblPhotoSource;//frmMyRecipientAddProfile.imgProfilePic.src;
							var Email = frmMyRecipientAddProfile.tbxEmail.text;
							var fbId = frmMyRecipientAddProfile.tbxFbID.text;
							var mobileNo = frmMyRecipientAddProfile.tbxMobileNo.text;
							var recepientdata = [name, PictureId , Email, mobileNo, fbId, "Added","N"];
							gblRcPersonalizedIdList = name;
							totalDataRecipient.push(recepientdata);
							
							//var selectedRcId = callBackResponse["personalizedIdList"][0]["personalizedId"]
							var totalData = [];
							for (var i = 0; i < AddAccountList.length; i++) {
								var acctName = "";
								var BankName = AddAccountList[i].lblBankName.text;
								var Number = stripDashAcc(AddAccountList[i].lblAccountNo.text);
								var NickName = AddAccountList[i].lblNick.text;
								var bankCode = AddAccountList[i].bankCode;
								//var acctName = AddAccountList[i].lblAcctValue.text;
								if(AddAccountList[i] != undefined && AddAccountList[i].lblAcctValue != undefined)
								{
									acctName = AddAccountList[i].lblAcctValue.text;
								}
								if(acctName == null || acctName == undefined || acctName == ""){
									acctName = "Not returned from GSB";
								}
								var bankData = [ crmId, "", bankCode, Number, NickName, "Added",acctName];
								totalData.push(bankData);
							}
							inputParams["personalizedAccList"] = totalData.toString()
							inputParams["receipentList"]= totalDataRecipient.toString()
							inputParams["oldRcName"]=name;
							inputParams["oldRcEmail"]=Email;
							inputParams["oldRcmobile"]=stripDashPh(mobileNo);
							inputParams["oldFbId"]=fbId;	
							inputParams["gblTransactionType"]="3";
							//startNewRcAddwithAccntServiceMB();
						}
						else{
								
							
							inputParams["receipentList"] = prepareNewRcListMB()
							inputParams["gblTransactionType"]="1";
							//addNewRecipientDetailsMB();		
							isRecipientNew = true;	
						}
					}
				//addAccountRecipentComplete();
			}else if(gblFlagTransPwdFlow == "addRcManually"){
				
				inputParams["receipentList"] = prepareNewRcListMB();
				inputParams["gblTransactionType"]="1";
				//addNewRecipientDetailsMB();
				isRecipientNew = true;
			}else if(gblFlagTransPwdFlow == "addRcContacts"){
			
				for(var i = 0; i < multiSelectedContacts.length; i++){
					var name = multiSelectedContacts[i].lblName;
					var mobile = addCrossPh(stripDashPh(multiSelectedContacts[i].lblMobile));
					var email = addCrossEmail(multiSelectedContacts[i].lblEmail);
					var facebook = multiSelectedContacts[i].lblFacebook;
				//	activityLogServiceCall("045", "", "01", "","Add", name, mobile, email, facebook, "");
				}
				var totaldata=transConfirmSelectContacts();
				inputParams["receipentList"]=totaldata.toString();
				inputParams["gblTransactionType"]="1";
				if(gblRcBase64List != ""){
					inputParams["fileType"]="png";
					inputParams["personalizedId"]=gblRcPersonalizedIdList;
					inputParams["base64ImageString"]=gblRcBase64List;
					//imageServiceCall();
				}
				
				//transConfirmSelectContacts();
			}
			else if(gblFlagTransPwdFlow == "addRcFacebook"){
			
				for(var i = 0; i < multiSelectedFacebook.length; i++){
					var name = multiSelectedFacebook[i].lblName;
					var mobile = addCrossPh(stripDashPh(multiSelectedFacebook[i].lblMobile));
					var email = addCrossEmail(multiSelectedFacebook[i].lblEmail);
					var facebook = multiSelectedFacebook[i].lblFacebook;
					//activityLogServiceCall("045", "", "01", "","Add", name, mobile, email, facebook, "");
				}
				var totaldata =transConfirmSelectFacebook();
				inputParams["receipentList"]=totaldata.toString();
				inputParams["gblTransactionType"]="1";
			}
			else if(gblFlagTransPwdFlow == "addTopup")
			{
				popTransactionPwd.dismiss();
				onClickNextMBApplyBB();
			}
			inputParams["flow"]="recipients";
			inputParams["Channel"]=gblSpaChannel;
			inputParams["AccDetailMsg"]=gblAccountDetails;
			inputParams["userAccountName"]=gblToSpaAccountName;
			showLoadingScreen();
			invokeServiceSecureAsync("saveAddAccountDetails", inputParams, saveRecipientDeatilsCallBackMB);
	   
	   	
	   
	   }else{
		    dismissLoadingScreen();
		    alert(kony.i18n.getLocalizedString("keyErrResponseOne"));
	   }
   }
}

function onClickTransferFromRecipientsMB(){
			gblTransferFromRecipient=true;
  			gblTrasferFrmAcntSummary = false;
			var curr_form = kony.application.getPreviousForm();
			var accountData = "";
			var recpImg="";
			var bankCode="";
			var recipientName="";
			var accountName="";
			//getTransferFromAccounts();
			if(gbltransferFromForm == "frmMyRecipientAddAccComplete"){
				accountData = frmMyRecipientAddAccComplete.segMyRecipientDetail.selectedItems[0];
				bankCode = accountData["bankCode"];
				accountName = accountData["lblAcctValue"]["text"];
				if(!flowSpa)
				{
				recpImg=frmMyRecipientAddAccComplete.imgprofilepic.rawBytes;
				} else {
				recpImg=frmMyRecipientAddAccComplete.imgprofilepic.src;
				}
				recipientName=frmMyRecipientAddAccConf.lblName.text;
				gblXferPhoneNo = frmMyRecipientAddAccConf.lblMobile.text;
				gblXferEmail = frmMyRecipientAddAccConf.lblEmail.text;
			}else if(gbltransferFromForm == "frmMyRecipientDetail" || gbltransferFromForm == "frmMyRecipientViewAccount" || gbltransferFromForm == "frmMyRecipientEditAccComplete"){
				accountData=frmMyRecipientDetail.segMyRecipientDetail.selectedItems[0];
				recpImg=frmMyRecipientDetail.imgprofilepic.src;
				bankCode = accountData["lblBankCD"];
				accountName = accountData["lblAcctName"]["text"];
				recipientName=frmMyRecipientDetail.lblName.text;
				gblXferPhoneNo = frmMyRecipientDetail.lblMobile.text;
				gblXferEmail = frmMyRecipientDetail.lblEmail.text;
			}else{
				accountData=frmMyRecipientDetail.segMyRecipientDetail.selectedItems[0];
				recpImg=frmMyRecipientDetail.imgprofilepic.src;
				bankCode = accountData["lblBankCD"];
				accountName = accountData["lblAcctName"]["text"];
				recipientName=frmMyRecipientDetail.lblName.text;
				gblXferPhoneNo = frmMyRecipientDetail.lblMobile.text;
				gblXferEmail = frmMyRecipientDetail.lblEmail.text;
			}
			if(gbltransferFromForm == "frmMyRecipientEditAccComplete"){
				accountData["lblNick"]["text"] = frmMyRecipientEditAccComplete.lblNick.text;
				accountName = frmMyRecipientEditAccComplete.lblAcctName.text;
			}
			frmTransferLanding.lblTranLandToName.text = accountData["lblNick"]["text"];
			gblSmartAccountName=accountName;
			if(gblSmartAccountName == null || gblSmartAccountName == undefined || gblSmartAccountName == "NONE" || gblSmartAccountName == "NA")
			{
				gblSmartAccountName="";
			}
			if(accountData["lblBankName"]["text"] != undefined)
				gblBANKREF = accountData["lblBankName"]["text"];
			else
				gblBANKREF = accountData["lblBankName"];
			gblisTMB=bankCode;
			gblSelectedRecipentName=recipientName;
			if(gbltransferFromForm == "frmMyRecipientAddAccComplete"){
			if(!flowSpa)
				{
					//frmTransferLanding.imgTranLandTo.rawBytes = recpImg;
				} else {
					//frmTransferLanding.imgTranLandTo.src = recpImg;
				}		
			} else {
		//	frmTransferLanding.imgTranLandTo.src = recpImg;
			}
			frmTransferLanding.lblTranLandToAccountNumber.text =  accountData["lblAccountNo"]["text"];
			//frmTransferLanding.imgTranLandTo.isVisible=true;
			enteringAccountNumberVisbilty(false);
}

function onTransferShortcutClick(){
		if(!isNotBlank(frmMyRecipientDetail.lblName.text)){
           showAlert(kony.i18n.getLocalizedString("Recipient_TR_NameInvalid"), kony.i18n.getLocalizedString("info"));
           return;
		}
		gblTransferFromRecipient=true;
		gbltransferFromForm = "frmMyRecipientDetail";
		getTransferFromAccounts();
}

function deleteAccountFromRecipientConfirmation(){
	deleteAccountType = "soft";
	popDelRecipient.label474136033103478.text = kony.i18n.getLocalizedString("keyConfirm");
	popDelRecipient.lblDeleteConfirmation.text = kony.i18n.getLocalizedString("keyDeleteRcAccPop");
	popDelRecipient.btnPopDeleteCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
	popDelRecipient.btnPopDeleteYEs.text = kony.i18n.getLocalizedString("keyYes");
	popDelRecipient.show();
}

function deleteAccountFromRecipientView(){
	deleteAccountType = "hard";
	//popDelRecipient.label474136033103478.text = kony.i18n.getLocalizedString("keyConfirm");
	popDelRecipient.lblDeleteConfirmation.text = kony.i18n.getLocalizedString("keyDeleteRcAccPop");
	popDelRecipient.btnPopDeleteCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
	popDelRecipient.btnPopDeleteYEs.text = kony.i18n.getLocalizedString("keyYes");
	popDelRecipient.show();
}

function deletePopupYes(){
	popDelRecipient.dismiss();
	if(deleteAccountType == "soft"){
		deleteRecepientAccountFromCache();
	}else{
		startExistingRcAccountMBEditService("", "", frmMyRecipientViewAccount.lblAcctName.text, "Deleted");
	}
}