gblCASelectedToAcctNum = "";
gblCASelectedToAcctNameEN = "";
gblCASelectedToAcctNameTH = "";
gblCACardRefNumber = "";
gblMinCashAdvanceAmount = "";
gblHideDisplayAmtLabelValue = true;
gblAvailableBalance = "";
gblRemainDailyLimit = "";
gblAvailableToSpend = "";
gblCASelectedToAcctStatus = "";
gblCAFullPaymentFlag = true;
isCashChillChill = false;
gblCCReceivedByDate = "";
gblCCReceivedByTime = "";
gblCCFee = "0.00";

function onClickOfCashAdvanceMenu() {
	cashAdvanceServiceHoursCheck();
}

function onClickNextOfCardInfoScreen() {
	var errorMsg = "";
	if(parseFloat(gblAvailableBalance) < parseFloat(gblMinCashAdvanceAmount)) {
		 dismissLoadingScreen();
		 errorMsg = kony.i18n.getLocalizedString("CashAdv_Err_Amount");
		 errorMsg = errorMsg.replace("{min_amount}", commaFormatted(gblMinCashAdvanceAmount));
				
		 showAlert(errorMsg, kony.i18n.getLocalizedString("info"));
         return false;
         
	}else {
        var inputParam = {};
        inputParam["cardRefId"] = gblCACardRefNumber;
        inputParam["applyValue"] = "3";
        invokeServiceSecureAsync("cashChillChill", inputParam, callBackCheckCashChillChill);
	}
}

function onClickBackOfCardInfoScreen() {
	frmMBManageCard.show();
}

function onClickCancelOfAccountSelectScreen() {
	var previousForm = kony.application.getPreviousForm();
  	previousForm.show();
	frmMBCashAdvAcctSelect.destroy();
}

function onRowClickSegmentOfAccountSelectScreen() {
  	var previousForm = kony.application.getPreviousForm();
	var accountName = frmMBCashAdvAcctSelect.segAccounts.selectedItems[0].lblAccountName;
	gblCASelectedToAcctNum = frmMBCashAdvAcctSelect.segAccounts.selectedItems[0].lblAccountNum;
	gblCASelectedToAcctNameEN = frmMBCashAdvAcctSelect.segAccounts.selectedItems[0].lblAccountNameEN;
	gblCASelectedToAcctNameTH = frmMBCashAdvAcctSelect.segAccounts.selectedItems[0].lblAccountNameTH;
	gblCASelectedToAcctStatus = frmMBCashAdvAcctSelect.segAccounts.selectedItems[0].lblAccountStatus;
	previousForm.lblSelectedAccount.text = accountName;
	previousForm.show();
}


function onSelectAccountCashAdvDetailsScreen() {
	customerAccountInquiryForSelectAccounts();
}

function showCashAdvanceFullPaymentDetails(){
  	gblCAFullPaymentFlag = true;
  	frmCashAdvanceInstallmentPlanDetails.flxEnterFullAmountDtls.setVisibility(false);
  	frmCashAdvanceInstallmentPlanDetails.flxSelectPaymentPlan.setVisibility(false);
  	frmCashAdvanceInstallmentPlanDetails.flxInstallmentPlan.setVisibility(false);
  	frmCashAdvanceInstallmentPlanDetails.flxFullPayment.setVisibility(true);
  	frmCashAdvanceInstallmentPlanDetails.flxDebitCardDateDetails.setVisibility(true);
	frmCashAdvanceInstallmentPlanDetails.lblReceivedDate.text = getFormattedDate(currentSystemDate(), kony.i18n.getCurrentLocale());
	frmCashAdvanceInstallmentPlanDetails.lblDebitCardDate.text = getFormattedDate(currentSystemDate(), kony.i18n.getCurrentLocale());
	frmCashAdvanceInstallmentPlanDetails.lblNowDesc.text = kony.i18n.getLocalizedString("keyNOW");
	frmCashAdvanceInstallmentPlanDetails.lblAvailableCashAmt.text = commaFormatted(gblAvailableToSpend);
 	frmCashAdvanceInstallmentPlanDetails.lblRemainDailyLimitVal.text = commaFormatted(gblRemainDailyLimit);
	frmCashAdvanceInstallmentPlanDetails.lblAvailableCashDesc.text =  kony.i18n.getLocalizedString("CAV02_keyAvailToSpend");
  	frmCashAdvanceInstallmentPlanDetails.lblRemainDailyLimit.text =  kony.i18n.getLocalizedString("CAV02_keyRCreditLimit");
	frmCashAdvanceInstallmentPlanDetails.lblEnterAmount.text = commaFormatted(gblMinCashAdvanceAmount);
	frmCashAdvanceInstallmentPlanDetails.txtEnterAmount.text = commaFormatted(gblMinCashAdvanceAmount);
	enableOrDisableAddAmount(parseFloat(gblMinCashAdvanceAmount), parseFloat(removeCommos(gblAvailableBalance)));
	enableOrDisableMinusAmount(parseFloat(gblMinCashAdvanceAmount)); //12345
	frmCashAdvanceInstallmentPlanDetails.flxAmount.setVisibility(true);
  	frmCashAdvanceInstallmentPlanDetails.flxEnterFullAmountDtls.setVisibility(false);
	cashAdvanceCreditCardDetailsInqGetInterest();
}

function showCashAdvanceInstallmentPlanDetails(){
  	gblCAFullPaymentFlag = false;
  	gblCashAdvPaymentType = "";
  	frmCashAdvanceInstallmentPlanDetails.flxEnterFullAmountDtls.setVisibility(false);
  	frmCashAdvanceInstallmentPlanDetails.flxDebitCardDateDetails.setVisibility(false);
  	frmCashAdvanceInstallmentPlanDetails.flxInstallmentPlan.setVisibility(false);
  	frmCashAdvanceInstallmentPlanDetails.flxFullPayment.setVisibility(false);
  	frmCashAdvanceInstallmentPlanDetails.flxSelectPaymentPlan.setVisibility(true);
  	frmCashAdvanceInstallmentPlanDetails.lblSelectPaymentPlan.text = kony.i18n.getLocalizedString("CAV04_plPaymentPlan");
  	frmCashAdvanceInstallmentPlanDetails.lblPaymentPlanTitle.text = kony.i18n.getLocalizedString("CAV04_keyPaymentPlan");
	frmCashAdvanceInstallmentPlanDetails.lblAvailableCashAmt.text = commaFormatted(gblAvailableToSpend);
 	frmCashAdvanceInstallmentPlanDetails.lblRemainDailyLimitVal.text = commaFormatted(gblRemainDailyLimit);
	frmCashAdvanceInstallmentPlanDetails.lblAvailableCashDesc.text =  kony.i18n.getLocalizedString("CAV02_keyAvailToSpend");
  	frmCashAdvanceInstallmentPlanDetails.lblRemainDailyLimit.text =  kony.i18n.getLocalizedString("CAV02_keyRCreditLimit");
	frmCashAdvanceInstallmentPlanDetails.lblEnterAmount.text = commaFormatted(gblMinCashAdvanceAmount);
	frmCashAdvanceInstallmentPlanDetails.txtEnterAmount.text = commaFormatted(gblMinCashAdvanceAmount);
  	frmCashAdvanceInstallmentPlanDetails.lblDebitCardDate.text = getFormattedDate(currentSystemDate(), kony.i18n.getCurrentLocale());
  	frmCashAdvanceInstallmentPlanDetails.lblReceivedDate.text = gblCCReceivedByDate;
  	frmCashAdvanceInstallmentPlanDetails.lblNowDesc.text = gblCCReceivedByTime;
	enableOrDisableAddAmount(parseFloat(gblMinCashAdvanceAmount), parseFloat(removeCommos(gblAvailableBalance)));
	enableOrDisableMinusAmount(parseFloat(gblMinCashAdvanceAmount)); //12345
	frmCashAdvanceInstallmentPlanDetails.flxAmount.setVisibility(true);
  	frmCashAdvanceInstallmentPlanDetails.flxCardType.setVisibility(true);
  	frmCashAdvanceInstallmentPlanDetails.lblSelectedAccount.text = kony.i18n.getLocalizedString("CAV04_plToAct");
  	frmCashAdvanceInstallmentPlanDetails.lblAccount.text = kony.i18n.getLocalizedString("CAV04_keyToAct");
  	frmCashAdvanceInstallmentPlanDetails.show();
	//cashAdvanceCreditCardDetailsInqGetInterest();
}

function onClickBackOfCashAdvTncScreen() {
  if(gblManageCardFlow == "frmMBCreditCardListDeeplink"){
    gblManageCardFlow = "";
    getCustomerCardListForDeeplink();
  }else{
    frmMBManageCard.show();
    frmMBCashAdvanceTnC.destroy();
  }
}

function onClickNextOfCashAdvPlanDetailScreen() {
	if(!onClickNextValidatePlanAmount()){
	 	return false;
	}else{
		callCashAdvanceSaveToSessionService();
	}
}

function onClickBackOfCashAdvPlanDetailScreen() {
  	TMBUtil.DestroyForm(frmMBCashAdvanceTnC);
	gblCASelectedToAcctNum = "";
  	frmMBCashAdvanceTnC.lblCashAdvanceDescSubTitle.setFocus(true);
	frmMBCashAdvanceTnC.show();
	frmCashAdvanceInstallmentPlanDetails.destroy();
}

function onClickConfirmOfCashAdvConfirmScreen() {
	showCashAdvancePwdPopupForTransfers();
}

function onClickCancelOfCashAdvConfirmScreen() {
  	frmCashAdvanceInstallmentPlanDetails.destroy();
	onClickNextOfCashAdvTncScreen();
	
}

function onClickBackOfCashAdvConfirmScreen() {
	frmCashAdvanceInstallmentPlanDetails.flexBodyScroll.scrollToWidget(frmCashAdvanceInstallmentPlanDetails.flxTop);
	frmCashAdvanceInstallmentPlanDetails.show();
}

function showCashAdvancePwdPopupForTransfers() {
  if(gblAuthAccessPin == true){
     showAccesspinPopup();
  }else{
     var lblText = kony.i18n.getLocalizedString("transPasswordSub");
	 var refNo = "";
	 var mobNO = "";
	 showLoadingScreen();
	 showOTPPopup(lblText, refNo, mobNO, onClickCashAdvaceConfirmPop, 3);
  }
	
}

function onClickCashAdvaceConfirmPop(tranPassword) {
	if (isNotBlank(tranPassword)) {
		showLoadingScreen();
        if(gblAuthAccessPin != true){
          popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("keyTransPwdMsg");
		  popupTractPwd.lblPopupTract7.skin = lblPopupLabelTxt;
		  popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = tbxPopupBlue;
		  popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = tbxPopupBlue;
		  popupTractPwd.hbxPopupTranscPwd.skin = tbxPopupBlue;  
        }		
		callCashAdvanceCompositeService(tranPassword);	
	} else{
		setTransPwdFailedError(kony.i18n.getLocalizedString("emptyMBTransPwd"));
		return false;
	}
}

function gotoCAPaymentPlanCompleteMB(status){
	if(status == "0"){ //success 
		frmCAPaymentPlanComplete.imgIconComplete.src = "completeico.png";
		frmCAPaymentPlanComplete.lblCashAdvanceVal.text = kony.i18n.getLocalizedString("CAV02_keyTitle")
														+ " " + frmCAPaymentPlanComplete.lblAmountVal.text;
		frmCAPaymentPlanComplete.lblAmountVal.text = frmCAPaymentPlanConfirmation.lblAmountVal.text;
		frmCAPaymentPlanComplete.lblFeeVal.text = frmCAPaymentPlanConfirmation.lblFeeVal.text;
	}else{ //failed
		frmCAPaymentPlanComplete.imgIconComplete.src = "icon_notcomplete.png";
		frmCAPaymentPlanComplete.lblAmountVal.text = "0.00" + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
		frmCAPaymentPlanComplete.lblCashAdvanceVal.text = kony.i18n.getLocalizedString("CAV02_keyTitle") 
														+ " 0.00 " + kony.i18n.getLocalizedString("currencyThaiBaht");
		frmCAPaymentPlanComplete.lblFeeVal.text = "0.00" + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
	}
	frmCAPaymentPlanComplete.lblFromAcctName.text = frmCAPaymentPlanConfirmation.lblFromAcctName.text;
	frmCAPaymentPlanComplete.lblFromAcctNo.text = frmCAPaymentPlanConfirmation.lblFromAcctNo.text;
	frmCAPaymentPlanComplete.lblToAcctName.text = frmCAPaymentPlanConfirmation.lblToAcctName.text;
	frmCAPaymentPlanComplete.lblToAcctNo.text = frmCAPaymentPlanConfirmation.lblToAcctNo.text;
  	frmCAPaymentPlanComplete.lblAnnualRateVal.text = frmCAPaymentPlanConfirmation.lblAnnualRateVal.text;
	frmCAPaymentPlanComplete.lblReferenceVal.text = frmCAPaymentPlanConfirmation.lblReferenceVal.text;
	frmCAPaymentPlanComplete.lblDebitCardOnVal.text = frmCAPaymentPlanConfirmation.lblDebitCardOnVal.text;
	frmCAPaymentPlanComplete.lblRecieveDateVal.text = frmCAPaymentPlanConfirmation.lblRecieveDateVal.text;
	frmCAPaymentPlanComplete.lblManageOtherCards.text = kony.i18n.getLocalizedString("CAV08_btnManageOther");
  	if(gblCAFullPaymentFlag == true){
      	frmCAPaymentPlanComplete.lblAnnualRate.text = kony.i18n.getLocalizedString("CAV04_keyRate02");
      	frmCAPaymentPlanComplete.flxTenor.setVisibility(false);
      	frmCAPaymentPlanComplete.flxPaymentAmt.setVisibility(false);
    }else{
      	frmCAPaymentPlanComplete.lblAnnualRate.text = kony.i18n.getLocalizedString("CAV05_keyRate");
        frmCAPaymentPlanComplete.lblTenorVal.text = frmCAPaymentPlanConfirmation.lblTenorVal.text;
        frmCAPaymentPlanComplete.lblPaymentAmtVal.text = frmCAPaymentPlanConfirmation.lblPaymentAmtVal.text;
        frmCAPaymentPlanComplete.flxTenor.setVisibility(true);
        frmCAPaymentPlanComplete.flxPaymentAmt.setVisibility(true);
    }
	frmCAPaymentPlanComplete.show();
}

// Added by vijay
function onClickAmount(){
  	frmCashAdvanceInstallmentPlanDetails.flxAmount.setVisibility(false);
  	frmCashAdvanceInstallmentPlanDetails.txtEnterAmount.setFocus(true);
  	frmCashAdvanceInstallmentPlanDetails.flxEnterFullAmountDtls.setVisibility(true);
  	frmCashAdvanceInstallmentPlanDetails.txtEnterAmount.text = frmCashAdvanceInstallmentPlanDetails.lblEnterAmount.text;
  	frmCashAdvanceInstallmentPlanDetails.txtEnterAmount.skin = "txtBlueNormal200";
  	frmCashAdvanceInstallmentPlanDetails.txtEnterAmount.focusSkin = "txtBlueNormal200";
	veriflyEnterAmountValueCA();
}

function veriflyEnterAmountValueCA(){
    var amountVal = frmCashAdvanceInstallmentPlanDetails.txtEnterAmount.text;
    var availableBal = frmCashAdvanceInstallmentPlanDetails.lblAvailableCashAmt.text;
    var minAmountErrKey = kony.i18n.getLocalizedString("CAV04_msgMinErr");
    minAmountErrKey = minAmountErrKey.replace("{Minimum_Amount}", commaFormatted(gblMinCashAdvanceAmount));

    if(parseFloat(removeCommos(amountVal)) > parseFloat(removeCommos(availableBal))) {                
      frmCashAdvanceInstallmentPlanDetails.lblEnterAmountDesc.text = kony.i18n.getLocalizedString("CAV04_msgMaxErr"); 
      frmCashAdvanceInstallmentPlanDetails.flxEnterAmt.skin = "flexWhiteBGRedBorder";
      frmCashAdvanceInstallmentPlanDetails.txtEnterAmount.skin = "txtRed48px";
      frmCashAdvanceInstallmentPlanDetails.txtEnterAmount.focusSkin = "txtRed48px";
      frmCashAdvanceInstallmentPlanDetails.lblEnterAmountDesc.skin = "lblRedInlineErr";
    }else if(parseFloat(removeCommos(amountVal)) < parseFloat(removeCommos(gblMinCashAdvanceAmount))){ 
      frmCashAdvanceInstallmentPlanDetails.lblEnterAmountDesc.text = minAmountErrKey; 
      frmCashAdvanceInstallmentPlanDetails.flxEnterAmt.skin = "flexWhiteBGRedBorder";
      frmCashAdvanceInstallmentPlanDetails.txtEnterAmount.skin = "txtRed48px";
      frmCashAdvanceInstallmentPlanDetails.txtEnterAmount.focusSkin = "txtRed48px";
      frmCashAdvanceInstallmentPlanDetails.lblEnterAmountDesc.skin = "lblRedInlineErr";
    }else if(!isNotBlank(amountVal)){
      frmCashAdvanceInstallmentPlanDetails.lblEnterAmountDesc.text = minAmountErrKey;
      frmCashAdvanceInstallmentPlanDetails.flxEnterAmt.skin = "flexWhiteBGRedBorder";
      frmCashAdvanceInstallmentPlanDetails.txtEnterAmount.skin = "txtRed48px";
      frmCashAdvanceInstallmentPlanDetails.txtEnterAmount.focusSkin = "txtRed48px";
      frmCashAdvanceInstallmentPlanDetails.lblEnterAmountDesc.skin = "lblRedInlineErr";
    }else{
      amountVal = kony.string.replace(amountVal, ",", "");
      frmCashAdvanceInstallmentPlanDetails.txtEnterAmount.text = commaFormattedTransfer(amountVal);
      frmCashAdvanceInstallmentPlanDetails.txtEnterAmount.skin = "txtBlueNormal200";
      frmCashAdvanceInstallmentPlanDetails.txtEnterAmount.focusSkin = "txtBlueNormal200";
      frmCashAdvanceInstallmentPlanDetails.lblEnterAmountDesc.text = kony.i18n.getLocalizedString("CAV04_keyAmount");
      frmCashAdvanceInstallmentPlanDetails.flxEnterAmt.skin = "flexWhiteBGBlueBorder";
      frmCashAdvanceInstallmentPlanDetails.lblEnterAmountDesc.skin = "lblGrey36px";
    }
}

function onDoneEnterAmount(){
  onValidateChangePlanAmount();
  var curFrm = kony.application.getCurrentForm();
  if(!gblHideDisplayAmtLabelValue){
  		gblHideDisplayAmtLabelValue = true;
  		curFrm.flxEnterFullAmountDtls.setVisibility(true);
  		curFrm.flxAmount.setVisibility(false);
  }else{
  		curFrm.lblEnterAmount.text = commaFormatted(parseFloat(removeCommos(curFrm.txtEnterAmount.text)).toFixed(2));
		curFrm.flxEnterFullAmountDtls.setVisibility(false);
		curFrm.flxAmount.setVisibility(true);
		if(!isNotBlank(curFrm.lblEnterAmount.text)){
			curFrm.btnMinusAmount.setEnabled(false);
		}else{
			enableOrDisableAddAmount(parseFloat(removeCommos(curFrm.lblEnterAmount.text)), parseFloat(removeCommos(curFrm.lblAvailableCashAmt.text)));
			enableOrDisableMinusAmount(parseFloat(removeCommos(curFrm.lblEnterAmount.text)));
		}
    	if(gblCAFullPaymentFlag == true){
        	cashAdvanceCreditCardDetailsInqGetInterest();  
        }else{
          	displaySelectPaymentPlan();
        }
  }
}

// Added by Vijay for Plan details Add/Minus Amount
function addPlanAmount(){
  	var curFrm = kony.application.getCurrentForm();
	var amount = parseFloat(removeCommos(curFrm.lblEnterAmount.text));
  	var availableBal = parseFloat(removeCommos(gblAvailableBalance));
  	amount = amount + parseFloat(gblMinCashAdvanceAmount);
  	if(amount <= availableBal){
        enableOrDisableAddAmount(amount,availableBal);
        curFrm.lblEnterAmount.text = commaFormatted(parseFloat(removeCommos(amount)).toFixed(2));
      	curFrm.txtEnterAmount.text = commaFormatted(parseFloat(removeCommos(amount)).toFixed(2));
        enableOrDisableMinusAmount(amount);
        if(gblCAFullPaymentFlag == true){
            cashAdvanceCreditCardDetailsInqGetInterest();
        }else{
            displaySelectPaymentPlan();
            curFrm.flxDebitCardDateDetails.setVisibility(false);
        }
    }
}

function minusPlanAmount(){
  	var curFrm = kony.application.getCurrentForm();
	var amount = parseFloat(removeCommos(curFrm.lblEnterAmount.text));
  	amount = amount - parseFloat(gblMinCashAdvanceAmount);
  	if(amount > 0){
        enableOrDisableMinusAmount(amount);
        curFrm.lblEnterAmount.text = commaFormatted(parseFloat(removeCommos(amount)).toFixed(2));
      	curFrm.txtEnterAmount.text = commaFormatted(parseFloat(removeCommos(amount)).toFixed(2));
        var availableBal = parseFloat(removeCommos(gblAvailableBalance));
        enableOrDisableAddAmount(amount,availableBal);
        if(gblCAFullPaymentFlag == true){
            cashAdvanceCreditCardDetailsInqGetInterest();
        }else{
            displaySelectPaymentPlan();
            curFrm.flxDebitCardDateDetails.setVisibility(false);
        }  
    }
}

function onValidateChangePlanAmount(){
	var minAmountErrKey = kony.i18n.getLocalizedString("CAV04_msgMinErr");
    minAmountErrKey = minAmountErrKey.replace("{Minimum_Amount}", commaFormatted(gblMinCashAdvanceAmount));
	var curFrm = kony.application.getCurrentForm();
	var amountVal = curFrm.txtEnterAmount.text;
	if(!isNotBlank(amountVal)){
    	 curFrm.txtEnterAmount.text = amountVal;
    	 curFrm.lblEnterAmountDesc.text = minAmountErrKey;
    	 curFrm.flxEnterAmt.skin = flexWhiteBGRedBorder;
    	 curFrm.txtEnterAmount.skin = "txtRed48px";
    	 curFrm.txtEnterAmount.focusSkin = "txtRed48px";
    	 curFrm.lblEnterAmountDesc.skin = lblRedInlineErr;
    	 gblHideDisplayAmtLabelValue = false;
	}else{
		amountVal = commaFormatted(fixedToTwoDecimal(amountVal));
	    if(parseFloat(removeCommos(amountVal)) > parseFloat(removeCommos(gblAvailableBalance))) { 
	    	 curFrm.txtEnterAmount.text = amountVal; 
	      	 curFrm.lblEnterAmountDesc.text = kony.i18n.getLocalizedString("CAV04_msgMaxErr"); 
	      	 curFrm.flxEnterAmt.skin = flexWhiteBGRedBorder;
	      	 curFrm.txtEnterAmount.skin = "txtRed48px";
	      	 curFrm.txtEnterAmount.focusSkin = "txtRed48px";
	      	 curFrm.lblEnterAmountDesc.skin = lblRedInlineErr;
	      	 gblHideDisplayAmtLabelValue = false;
	    }else if(parseFloat(removeCommos(amountVal)) < parseFloat(removeCommos(gblMinCashAdvanceAmount))){ 
	    	 curFrm.txtEnterAmount.text = amountVal;  
	    	 curFrm.lblEnterAmountDesc.text = minAmountErrKey; 
	      	 curFrm.flxEnterAmt.skin = flexWhiteBGRedBorder;
	      	 curFrm.txtEnterAmount.skin = "txtRed48px";
	      	 curFrm.txtEnterAmount.focusSkin = "txtRed48px";
	      	 curFrm.lblEnterAmountDesc.skin = lblRedInlineErr;
	      	 gblHideDisplayAmtLabelValue = false;
	    }else if(!isNotBlank(amountVal)){
	    	 curFrm.txtEnterAmount.text = amountVal;
	    	 curFrm.lblEnterAmountDesc.text = minAmountErrKey;
	    	 curFrm.flxEnterAmt.skin = flexWhiteBGRedBorder;
	    	 curFrm.txtEnterAmount.skin = "txtRed48px";
	    	 curFrm.txtEnterAmount.focusSkin = "txtRed48px";
	    	 curFrm.lblEnterAmountDesc.skin = lblRedInlineErr;
	    	 gblHideDisplayAmtLabelValue = false;
	    }else{
	    	curFrm.txtEnterAmount.text = amountVal;  
	    	curFrm.lblEnterAmountDesc.text = kony.i18n.getLocalizedString("CAV04_keyAmount");
	    	curFrm.flxEnterAmt.skin = flexWhiteBGBlueBorder;
	    	curFrm.txtEnterAmount.skin = "txtBlueNormal200";
	  		curFrm.txtEnterAmount.focusSkin = "txtBlueNormal200";
	    	curFrm.lblEnterAmountDesc.skin = lblGrey36px;
	    	gblHideDisplayAmtLabelValue = true;
	    }
	}
    curFrm.txtEnterAmount.text = isNotBlank(amountVal) ? amountVal : ""; 
    curFrm.lblEnterAmount.text = isNotBlank(amountVal) ? amountVal : "";
}


function onTextChangePlanAmount(){
  	var curFrm = kony.application.getCurrentForm();
	var amountVal = curFrm.txtEnterAmount.text;
    if (isNotBlank(amountVal)) {
        amountVal = kony.string.replace(amountVal, ",", "");        
        if(isNotBlank(amountVal) && amountVal.length > 0 && parseFloat(amountVal, 10) == 0){
			
		}else{
			curFrm.txtEnterAmount.text = commaFormattedTransfer(amountVal);
            curFrm.lblEnterAmount.text = commaFormattedTransfer(amountVal);
		}
    }
  	if(gblCAFullPaymentFlag == false){
  		displaySelectPaymentPlan();
      	curFrm.flxDebitCardDateDetails.setVisibility(false);
    }
}

function onClickNextValidatePlanAmount(){
	var minAmountErrKey = kony.i18n.getLocalizedString("CAV04_msgMinErr");
    minAmountErrKey = minAmountErrKey.replace("{Minimum_Amount}", commaFormatted(gblMinCashAdvanceAmount));
	
	var amountVal = frmCashAdvanceInstallmentPlanDetails.txtEnterAmount.text;
	var selAccounttxt = frmCashAdvanceInstallmentPlanDetails.lblSelectedAccount.text;
	var activeAcct = "Active";//Check Active Account
	var inActiveAcct = "Inactive";//Check Active Account
  
  	if(parseFloat(removeCommos(amountVal)) > parseFloat(removeCommos(gblAvailableBalance)) &&
      (frmCashAdvanceInstallmentPlanDetails.flxSelectPaymentPlan.isVisible || 
       kony.string.equalsIgnoreCase(selAccounttxt, kony.i18n.getLocalizedString("CAV04_plToAct")))){
      	 showAlert(kony.i18n.getLocalizedString("CAV04_msgMaxErr"), kony.i18n.getLocalizedString("info"));
    	 return false;
    }else if(parseFloat(removeCommos(amountVal)) < parseFloat(removeCommos(gblMinCashAdvanceAmount))){ 
    	 showAlert(minAmountErrKey, kony.i18n.getLocalizedString("info")); 
    	 return false;
    }else if(!isNotBlank(amountVal)){
    	 showAlert(minAmountErrKey, kony.i18n.getLocalizedString("info")); 
    	 return false;
    }else if(frmCashAdvanceInstallmentPlanDetails.flxSelectPaymentPlan.isVisible){
    	 showAlert(kony.i18n.getLocalizedString("CAV04_msgPaymentPlan"), kony.i18n.getLocalizedString("info"));
    	 return false;
    }else if(kony.string.equalsIgnoreCase(selAccounttxt, kony.i18n.getLocalizedString("CAV04_plToAct"))){
    	 showAlert(kony.i18n.getLocalizedString("CAV04_msgToAccount"), kony.i18n.getLocalizedString("info"));
    	 return false;
    }else if(!kony.string.startsWith(gblCASelectedToAcctStatus, activeAcct, true)&&!kony.string.startsWith(gblCASelectedToAcctStatus, inActiveAcct, true)){ 
    	 showAlert(kony.i18n.getLocalizedString("CAV04_msgInvalidStatus"), kony.i18n.getLocalizedString("info"));
    	 return false;
    }else if((parseFloat(removeCommos(amountVal)) > parseFloat(removeCommos(gblAvailableBalance))) && gblCAFullPaymentFlag) {
      	 var errMsg = kony.i18n.getLocalizedString("CashAdv_Err_ChillOver");
      	 errMsg = errMsg.replace("{available_amt}", commaFormatted(gblAvailableBalance));
    	 showAlert(errMsg, kony.i18n.getLocalizedString("info")); 
    	 return false;              
    }else if((parseFloat(removeCommos(amountVal)) > parseFloat(removeCommos(gblAvailableBalance))) && !gblCAFullPaymentFlag) { 
      	  var errMsg = kony.i18n.getLocalizedString("CashAdv_Err_CashOver");
      	  errMsg = errMsg.replace("{available_amt}", commaFormatted(gblAvailableBalance));
    	  showAlert(errMsg, kony.i18n.getLocalizedString("info"));  
    	 return false;              
    }else {
    	return true;
    }
}

function calculateCashAdvanceFee(amount, rate) {
	var fee = "0.00";
	
	if(isNotBlank(amount)) {
		amount = removeCommos(amount);
		rate = parseFloat(rate);
		
		fee = (amount/100)*rate;
	}
	
	fee = parseFloat(fee).toFixed(2);
	
	return fee;
}

function loadTermsNConditionCashAdvanceMB(){
	var input_param = {};
	var locale = kony.i18n.getCurrentLocale();
	    if (locale == "en_US") {
			input_param["localeCd"]="en_US";
		}else{
			input_param["localeCd"]="th_TH";
		}
		
	//module key again is from tmb_tnc file for Cash Advance
	input_param["moduleKey"]= "TMBCashAdvance";
	showLoadingScreen();
	invokeServiceSecureAsync("readUTFFile", input_param, loadTNCCahAdvanceCallBackMB);
	
}

function loadTNCCahAdvanceCallBackMB(status,result){
	if(status == 400){
		if(result["opstatus"] == 0){
			frmMBCashAdvanceTnC.lblCashAdvanceDescSubTitle.text = kony.i18n.getLocalizedString('keyTermsNConditions');
			frmMBCashAdvanceTnC.lblCashAdvanceHdrTxt.text = kony.i18n.getLocalizedString("CAV02_keyTitle");
			frmMBCashAdvanceTnC.btnCashAdvanceBack.text = kony.i18n.getLocalizedString("CAV02_btnBack");
			frmMBCashAdvanceTnC.btnCashAdvanceAgree.text = kony.i18n.getLocalizedString("CAV03_btnAgree");
			
			frmMBCashAdvanceTnC.richtextTnC.setVisibility(true);
			frmMBCashAdvanceTnC.richtextTnC.text = result["fileContent"];
			frmMBCashAdvanceTnC.lblCashAdvanceDescSubTitle.setFocus(true);
			//frmMBCashAdvanceTnC.flexBodyScroll.scrollToWidget(frmMBCashAdvanceTnC.flexContainer761510865415876);
			dismissLoadingScreen();
			
		}else{
			dismissLoadingScreen();
			showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
			return false;
		}
	}
}

function onClickEmailTnCMBCashAdvance(){
	popupConfrmDelete.lblheader.text = kony.i18n.getLocalizedString("TncInfoHeader");
    popupConfrmDelete.btnpopConfDelete.text = kony.i18n.getLocalizedString("keyOK");
    popupConfrmDelete.btnPopupConfCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
    popupConfrmDelete.btnpopConfDelete.onClick = onClickOKEmailCashAdvance;
    popupConfrmDelete.lblPopupConfText.text = kony.i18n.getLocalizedString("keytermOpenAcnt");
    popupConfrmDelete.show();
}

function onClickOKEmailCashAdvance(){
	popupConfrmDelete.dismiss();
	showLoadingScreen();
	var inputparam = {};
	inputparam["channelName"] = "Mobile Banking";
	inputparam["channelID"] = "02";
	inputparam["notificationType"] = "Email"; // always email
	inputparam["phoneNumber"] = gblPHONENUMBER;
	inputparam["mail"] = gblEmailId;
	inputparam["customerName"] = gblCustomerName;
	inputparam["localeCd"] = kony.i18n.getCurrentLocale();
    inputparam["moduleKey"] = "TMBCashAdvance";
	invokeServiceSecureAsync("TCEMailService", inputparam, callBackEmailTnCCashAdvanceMB);
	
}

function callBackEmailTnCCashAdvanceMB(status, result) {
	if (status == 400) {
		if (result["opstatus"] == 0) {
			var StatusCode = result["StatusCode"];
			var Severity = result["Severity"];
			var StatusDesc = result["StatusDesc"];
			if (StatusCode == 0) {
				//showAlert(kony.i18n.getLocalizedString("keytermOpenAcnt"), kony.i18n.getLocalizedString("info"));
				dismissLoadingScreen();
			} else {
				dismissLoadingScreen();
				return false;
			}
		} else {
			dismissLoadingScreen();
		}
	}
}

function enableOrDisableAddAmount(enteredAmt,availableBal){
	var checkAmount = enteredAmt + parseFloat(gblMinCashAdvanceAmount); // Should not allow next time	
	//kony.print("checkAmount--->" + checkAmount + "availableBal---->" + availableBal);
	if(checkAmount > availableBal){
		//kony.print("Enable false in add amount");
        frmCashAdvanceInstallmentPlanDetails.btnAddAmount.setEnabled(false);
        frmCashAdvanceInstallmentPlanDetails.btnAddAmount.skin = "btnDisabledAddAmount";
        frmCashAdvanceInstallmentPlanDetails.btnAddAmount.focusSkin = "btnDisabledAddAmount";
	}else{
		//kony.print("Enable true in add amount");
        frmCashAdvanceInstallmentPlanDetails.btnAddAmount.setEnabled(true);
        frmCashAdvanceInstallmentPlanDetails.btnAddAmount.skin = "btnTransWBGBlueFont";
        frmCashAdvanceInstallmentPlanDetails.btnAddAmount.focusSkin = "btnTransGreyBGBlueFont";
	}
}

function enableOrDisableMinusAmount(enteredAmt){
	var minusAmt = parseFloat(gblMinCashAdvanceAmount);
	//kony.print("minusAmt--->" + minusAmt + "enteredAmt---->" + enteredAmt);
	
	var checkAmount = enteredAmt - minusAmt; // Should not allow next time	

	if(checkAmount < minusAmt){
      	//kony.print("Enable false in add minus amount");
        frmCashAdvanceInstallmentPlanDetails.btnMinusAmount.setEnabled(false);
        frmCashAdvanceInstallmentPlanDetails.btnMinusAmount.skin = "btnDisabledAddAmount";
        frmCashAdvanceInstallmentPlanDetails.btnMinusAmount.focusSkin = "btnDisabledAddAmount";
	}else{
		//kony.print("Enable true in add minus amount");
        frmCashAdvanceInstallmentPlanDetails.btnMinusAmount.setEnabled(true);
        frmCashAdvanceInstallmentPlanDetails.btnMinusAmount.skin = "btnTransWBGBlueFont";
        frmCashAdvanceInstallmentPlanDetails.btnMinusAmount.focusSkin = "btnTransGreyBGBlueFont";
	}
}

function onClickManageOtherCardCompleteCA(){
	gblisCardActivationCompleteFlow = true;
	gblManageCardFlow = "frmMBCardList";
	onClickBackManageListCard();
}

function onClickCASelectPaymentPlan(){
  kony.print("@@@ onClickCASelectPaymentPlan() @@@");
  kony.print("@@@ gblCashAdvPaymentType:::"+gblCashAdvPaymentType);
  if(isNotBlank(gblCashAdvPaymentType)){
    frmMBCashAdvanceSelectPlan.show();
  }else{
    getCashAdvPaymentPlan();
  }
}

function onRowClickSegmentOfPaymentPlanScreen() {
  	gblCashAdvPaymentType = "Installment";
  	var selInstalItem = frmMBCashAdvanceSelectPlan.segPaymentPlan.selectedItems;
    if (selInstalItem != null && selInstalItem != "" && selInstalItem != undefined) {
      frmMBCashAdvanceSelectPlan.flxFooter.setVisibility(true);
    } else {
      frmMBCashAdvanceSelectPlan.flxFooter.setVisibility(false);
    }
  	/*
  	showLoadingScreen();
  	gblCAFullPaymentFlag = false;
  	gblAvailableBalance = gblAvailableToSpend;
	var tenorVal = frmMBCashAdvanceSelectPlan.segPaymentPlan.selectedItems[0].lblTenorVal;
	var rateVal = frmMBCashAdvanceSelectPlan.segPaymentPlan.selectedItems[0].lblRateVal;
	var amountVal = frmMBCashAdvanceSelectPlan.segPaymentPlan.selectedItems[0].lblAmountVal;
	frmCashAdvanceInstallmentPlanDetails.lblTenorVal.text = tenorVal;
  	frmCashAdvanceInstallmentPlanDetails.lblRatePerMonthVal.text = rateVal;
  	frmCashAdvanceInstallmentPlanDetails.lblPaymentAmtVal.text = amountVal;
  	frmCashAdvanceInstallmentPlanDetails.flxInstallmentPlan.setVisibility(true);
  	frmCashAdvanceInstallmentPlanDetails.flxDebitCardDateDetails.setVisibility(true);
  	frmCashAdvanceInstallmentPlanDetails.flxSelectPaymentPlan.setVisibility(false);
  	frmCashAdvanceInstallmentPlanDetails.lblReceivedDate.text = gblCCReceivedByDate;
  	frmCashAdvanceInstallmentPlanDetails.lblNowDesc.text = gblCCReceivedByTime;
  	frmCashAdvanceInstallmentPlanDetails.lblInsFeeVal.text = gblCCFee;
  	frmCashAdvanceInstallmentPlanDetails.lblInstallmentTitle.text = kony.i18n.getLocalizedString("CAV04_plInstallment");
    frmCashAdvanceInstallmentPlanDetails.lblTenorDesc.text = kony.i18n.getLocalizedString("CAV04_keyTenor");
    frmCashAdvanceInstallmentPlanDetails.lblRatePerMonth.text = kony.i18n.getLocalizedString("CAV04_keyRate01");
    frmCashAdvanceInstallmentPlanDetails.lblPaymentAmt.text = kony.i18n.getLocalizedString("CAV04_keyPayAmt");
    frmCashAdvanceInstallmentPlanDetails.lblTime.text = kony.i18n.getLocalizedString("CAV04_keyTime02");
  	frmCashAdvanceInstallmentPlanDetails.flxTenor.setVisibility(true);
	frmCashAdvanceInstallmentPlanDetails.flxAnnualRateDetails.setVisibility(true);
  	frmCashAdvanceInstallmentPlanDetails.flxPaymentAmount.setVisibility(true);
  	frmCashAdvanceInstallmentPlanDetails.rchTxtNote2.text = "";
	frmCashAdvanceInstallmentPlanDetails.rchTxtNote2.setVisibility(false);
	frmCashAdvanceInstallmentPlanDetails.show();
  	dismissLoadingScreen();*/
}

function onRowClickSegmentOfFullPaymentPlanScreen() {
	gblCashAdvPaymentType = "FullPayment";
  	frmMBCashAdvanceSelectPlan.flxFooter.setVisibility(true);
  	/*frmCashAdvanceInstallmentPlanDetails.lblInstallmentTitle.text = kony.i18n.getLocalizedString("CAV04_plFullPayment");
  	frmCashAdvanceInstallmentPlanDetails.lblInsPaymentPlan.text = kony.i18n.getLocalizedString("CAV04_keyPaymentPlan");
	frmCashAdvanceInstallmentPlanDetails.lblInsFeeDesc.text = kony.i18n.getLocalizedString("CAV04_keyFee");
    frmCashAdvanceInstallmentPlanDetails.lblTenorDesc.text = kony.i18n.getLocalizedString("CAV04_keyRate02");
	frmCashAdvanceInstallmentPlanDetails.lblNowDesc.text = kony.i18n.getLocalizedString("keyNOW");
  	frmCashAdvanceInstallmentPlanDetails.lblTime.text = kony.i18n.getLocalizedString("CAV04_keyTime01");
  	frmCashAdvanceInstallmentPlanDetails.lblReceivedDate.text = getFormattedDate(currentSystemDate(), kony.i18n.getCurrentLocale());
	frmCashAdvanceInstallmentPlanDetails.lblNowDesc.text = kony.i18n.getLocalizedString("keyNOW");
  	frmCashAdvanceInstallmentPlanDetails.flxInstallmentPlan.setVisibility(true);
  	frmCashAdvanceInstallmentPlanDetails.flxDebitCardDateDetails.setVisibility(true);
  	frmCashAdvanceInstallmentPlanDetails.flxSelectPaymentPlan.setVisibility(false);
  	frmCashAdvanceInstallmentPlanDetails.flxFullPayment.setVisibility(false);
  	frmCashAdvanceInstallmentPlanDetails.flxTenor.setVisibility(true);
	frmCashAdvanceInstallmentPlanDetails.flxAnnualRateDetails.setVisibility(false);
  	frmCashAdvanceInstallmentPlanDetails.flxPaymentAmount.setVisibility(false);
  	cashAdvanceCreditCardDetailsInqGetInterest();
	frmCashAdvanceInstallmentPlanDetails.show(); */
}

function displaySelectPaymentPlan(){
  	frmCashAdvanceInstallmentPlanDetails.flxSelectPaymentPlan.setVisibility(true);
  	frmCashAdvanceInstallmentPlanDetails.flxInstallmentPlan.setVisibility(false);
  	frmCashAdvanceInstallmentPlanDetails.flxFullPayment.setVisibility(false);
  	frmCashAdvanceInstallmentPlanDetails.lblSelectPaymentPlan.text = kony.i18n.getLocalizedString("CAV04_plPaymentPlan");
	frmCashAdvanceInstallmentPlanDetails.lblPaymentPlanTitle.text = kony.i18n.getLocalizedString("CAV04_keyPaymentPlan");
}