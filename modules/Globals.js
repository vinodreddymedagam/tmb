function defineTMBGlobals(){
	
    gblServerIp = "mibdev1.tau2904.com"; // Added bcos of Platfrom team has removed from kony visz8.2
    gblSecurePort = "443"; //// Added bcos of Platfrom team has removed from kony visz8.2
    GLOBAL_UPDATE_URL = "";
	GLOBAL_MB_CHANNEL = "02";
	GLOBAL_IB_CHANNEL = "01";
	Gbl_Image_Size_Limit=5242880;
	GLOBAL_INI_MAX_LIMIT_AMT="700000";
	GLOBAL_CEILING_LIMIT="2000000";
	GLOBAL_MAX_LIMIT_HIST="500000";
	GLOBAL_ACCT_NUM_POSITION = "4";
	GLOBAL_NUM_AT_POS_CA = "1";
	GLOBAL_NUM_AT_POS_TD = "3";
	GLOBAL_NUM_AT_POS_SA = "2,7,9";
	GLOBAL_BLOCKED_TRANS = [];
	GLOBAL_BLOCKED_ACCESS = [];
	GLOBAL_BLOCKED_USERIDS = [];
	GLOBAL_DEBITCARD_TABLE=""
	GLOBAL_CHEQUESERVICE_TABLE=""
	GLOBAL_KONY_IDLE_TIMEOUT = "5";
	//GLOBAL_PDF_DOWNLOAD_URL_en_US="https://mibdev1.tau2904.com/services/pdforimagerender?filetype=pdf&filename=TermsAndConditions&localID=en_US";
    GLOBAL_PDF_DOWNLOAD_URL_en_US="https://"+appConfig.serverIp+"/services/pdforimagerender?filetype=pdf&filename=TermsAndConditions&localID=en_US";
	GLOBAL_IMAGE_DOWNLOAD_URL_en_US="https://"+appConfig.serverIp+"/services/pdforimagerender?filetype=png&filename=TermsAndConditions&localID=en_US";
	gblSMART_FREE_TRANS_CODES="219,220,222";
	gblORFT_FREE_TRANS_CODES="219";
	gblORFT_ALL_FREE_TRANS_CODES="225";
	gblITMX_FREE_TRANS_CODES="";
  	gblTO_ACC_P2P_FEE_WAIVE_CODES="219,220,222";
    gblTOACC_PP_FREE_PROD_CODES="225";
	
	 // MIB-4884-Allow special characters for My Note and Note to recipient field
	GBL_ALLOW_SPECIAL_CHAR_KEY_NAMES="doPmtAdd_memo,doPmtAdd_receipientNote,NotificationAdd_memo,NotificationAdd_mynote,NotificationAdd_recipientMessage,financialActivityLog_finTxnMemo,memo,mynote,finTxnMemo,receipientNote";
	gblPrevTextNoEmoji = "";
	GBL_VALID_CHARS_MY_NOTE_NOW="";
	GBL_VALID_CHARS_MY_NOTE_FUTURE="";
	
	GLOBAL_PDF_DOWNLOAD_URL_th_TH="https://"+appConfig.serverIp+"/services/pdforimagerender?filetype=pdf&filename=TermsAndConditions&localID=th_TH";
	GLOBAL_IMAGE_DOWNLOAD_URL_th_TH="https://"+appConfig.serverIp+"/services/pdforimagerender?filetype=png&filename=TermsAndConditions&localID=th_TH";
	
	GLOBAL_LOAD_MORE="15";
	GLOBAL_MAX_BILL_COUNT="50";
	GLOBAL_MAX_BILL_ADD="5";
  	GLOBAL_UV_STATUS_FLAG = "OFF";
  	gblVtapInitialized = false;
    GLOBAL_RTP_ENABLE = "OFF";
  
   //MF Temenos ON/OFF Support Flag
  	GBL_MF_TEMENOS_ENABLE_FLAG = "OFF";
  	
  	GBL_EMAIL_REG_PATTERN = "^[\\w#-]+(?:\\.[\\w#-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
  
  //MF Suitability ON/OFF Support Flag
    GBL_MF_SUITABILITY_FLAG = "OFF";
  
	GLOBAL_LOGINQR_PARAMID = 0;
  	gblVtapServerURL = "https://uvdev.tau2904.com/vtap";
    gblProvServerURL = "https://uvdev.tau2904.com/provision";
    gblPkiServerURL = "https://uvdev.tau2904.com";
	GBL_KS_ID = "";
	// Online Payment Based on MWA 
	GLOBAL_ONLINE_PAYMENT_VERSION_MWA="";
	
//	WAIVERCODE = "1";
//TRANSCODEMIN = "8290";
//TRANSCODEUN = "8190";
	TRANSCODEMIN = "2";
	TRANSCODEUN = "2";
//	LOANACCOUNTTABLE = "";
//	ACTIONFLAG="I";
//	glbUserId ="";
	glb_viewName="";
	glb_accId="";
	glb_accountId="";
	glb_selectedData="";
	GBLFINANACIALACTIVITYLOG={}
//	glb_localeFlag=false
//Defaulting it to 1 minute similar to kony network libs.
	DEFAULT_SERVICE_TIMEOUT=60000
//biller logo URL
	BILLER_LOGO_URL="";
	GBL_FTRANSFER_DATA = {};
	GLOBAL_NICKNAME_LENGTH="20";
	GLOBAL_DREAM_DESC_MAXLENGTH="30";
//MB Account delete check account exist or not before deleting.
	gblAccExist=true;
	
// IB and MB terms and conditions global variables
	GLOBAL_TERMS_CONDITIONS_th_TH_1 = "";
	GLOBAL_TERMS_CONDITIONS_th_TH_2 = "";
	GLOBAL_TERMS_CONDITIONS_th_TH_3 = "";
	GLOBAL_TERMS_CONDITIONS_en_US = "";
	GBL_FLOW_ID_CA = 0;
	GBL_TIMEFOR_LOGIN = "";
	GBL_UNIQ_ID = "SDFSDVDFVDSFSDFDFVDFHHKTJRTJTSFSZXGJYMYHMCG";
	GBL_Session_LOCK=false;
	GBL_Risk_DataTo_Log = "";
	GBL_RiskScoreIph="";
	GBL_MalWare_Names = "";
	GBL_rootedFlag ="";
	GLOBAL_OPENACT_STARTTIME = "";
	GLOBAL_OPENACT_ENDTIME = "";
	GLOBAL_EDITDREAM_STARTTIME="";
	GLOBAL_E_STATEMENT_PRODUCTS=[];
	GLOBAL_E_STATEMENT_IMAGES=[];
	GLOBAL_EDITDREAM_ENDTIME="";
	GBL_SMART_DATE_FT = "";
	monthClicked = false;
	weekClicked = false;
	DailyClicked = false;
	YearlyClicked = false;
	GBL_MALWARE_FOR_RISK_FLAG = false;
	gblIBRetryCountRequestOTP = [];
	gblIBRetryCountRequestOTP["chngIBPwd"] = "0";
	gblIBRetryCountRequestOTP["chngIBUserId"] = "0";
	GLOBAL_ACTIVATION_HELP_TEXT_EN = "You can get your Activation Code from any TMB ATM or Branch. To unlock your TMB Touch or add another  device, you can also request an activation code from TMB Contact Center on 1558, or after logging into your TMB Touch app.";
	GLOBAL_ACTIVATION_HELP_TEXT_TH = "ท่านสามารถขอรหัสเริ่มใช้งานได้ที่ตู้ ATM ของ TMB ทุกตู้ หรือที่ TMB ทุกสาขา. กรณีต้องการปลดล็อค ทีเอ็มบี ทัช หรือเพิ่มการใช้งานบนอุปกรณ์อื่น ท่านสามารถขอรหัสเริ่มใช้งานได้จากฝ่ายบริการลูกค้าสัมพันธ์ โทร.1558 หรือหลังจากล็อคอินเข้าใช้งาน ทีเอ็มบี ทัช แอพพลิเคชั่น";
	GLOBAL_REACTIVATION_HELP_TEXT_EN = "You can get your Activation Code from any TMB ATM or Branch. To unlock your TMB Touch or add another  device, you can also request an activation code from TMB Contact Center on 1558, or after logging into your TMB Touch app.";
	GLOBAL_REACTIVATION_HELP_TEXT_TH="ท่านสามารถขอรหัสเริ่มใช้งานได้ที่ตู้ ATM ของ TMB ทุกตู้ หรือที่ TMB ทุกสาขา. กรณีต้องการปลดล็อค ทีเอ็มบี ทัช หรือเพิ่มการใช้งานบนอุปกรณ์อื่น ท่านสามารถขอรหัสเริ่มใช้งานได้จากฝ่ายบริการลูกค้าสัมพันธ์ โทร.1558 หรือหลังจากล็อคอินเข้าใช้งาน ทีเอ็มบี ทัช แอพพลิเคชั่น";
	
	GLOBAL_IB_ACTIVATION_HELP_TEXT_EN = "";
	GLOBAL_IB_ACTIVATION_HELP_TEXT_TH = "";
	GLOBAL_IB_REACTIVATION_HELP_TEXT_EN = "";
	GLOBAL_IB_REACTIVATION_HELP_TEXT_TH = "";
	tnckeyword = "";
	Recp_category = 0;
	gblNotificationData = "";
	GBL_Time_Out = "false";
	gblLogoutTime = "";
	gblIsFromMigration = false;
	var cryptoP = "";
	var cryptoQ = "";
	var cPubModulo = "";
	var cryptoPrivateVal = "";
	var cPubExp = "";
	cSerPub = "";
	gblDeviceInfo = kony.os.deviceInfo();
	gblPlatformName = gblDeviceInfo.name;
	gblUnreadCount= "0";
	gblMessageCount = "0";
	isInvestFromAccSumm = false;
	gblPinCount = 0;
	gblNum = "";
	gblTouchCount = 0;
	gblTouchDevice = "";
	gblSuccess=false;
	gblActivationFlow = false;
	gblTouchShow = true;
	gblTouchActSum="N";
	gblMobileNewChange="N";
	startDateStmt="";
	endDateStmt="";
	selectedIndex=0;
	isMFEnabled=true;
	//cSerVar1 = "";
	//cSerVar2 = "";
	// these global variables are used in MB promotions, to save offers in the pdf and image format
	promotionDetailsText1 = ""; promotionDetailsText2 = ""; promotionDetailsText3 = ""; promotionDetailsText4 = ""; promotionDetailsText5 = "";
	isCmpFlow = false;
	gblProdCode = "";
	gblProudctDetails = {};
	gblHaveMutualFundsFlg = false;
	gblHaveBankAssuranceFlg = false;
	gbl3dTouchAction="";
	gblQuickBalanceResponse = "";
	gblDeepLinkFlow = false;
	//Global Variables for Savings Care
	gblrelationshipsEN={};
	gblrelationshipsTH={};
	//arrayBenificiary=[];
	gblSavingsCareFlow="";
	gblBeneficiaryData="";
	gblRelationCodeAndDescEN={};
	gblRelationCodeAndDescTH={};	
	gblRelationDescAndCodeEN={};
	gblRelationDescAndCodeTH={};
	gblSavingsCareAccId = "";
	gblInterest = "";
	gblAccountDetailsNickName="";
	gblEditClickFromAccountDetails=false;
	gblBeneficiaryCount=0;
	gblBeneficaryIndexArray=[];
	gblMaxBeneficiaryConfig = 5;
	gblOpenSavingsCareNickName = "";
	gblOpenActSavingCareEditCont=false;
	selectedCNTIndex = 1;
	gblAccountId="";
	gblSCFromFiident="";
	gblSCFromActName="";
	gblSCFromActType="";
	gblSCFromProdId="";
	gblOpeningMethod="";
	gblFromNickNameBack = true;
	gblArrayBenificiaryOld=[];
	gblNeverSetBeneficiary="";
	gblSavingsCarepersonalizedId="";
	gblSavingsCareBankCD="";
  
  	//Added by Jag
  
  	gblActivationWithWifi = true;
  
  
	//Added By Vijay to block activation via Userid/pwd
	gblShowUserIdLink = true;
	gblHaveErrorFlag = false;
	gblTransferTxnENCallBack = "";
	gblTransferTxnTHCallBack = "";
	//block credit card
	gblMBNewTncFlow = "";
	gblCardList = "";
	gblSelectedCard = "";
	gnCurrCalDay=0;
	gnCurrCalMonth=0;
	gnCurrCalYear=0;
	gblisCardActivationCompleteFlow=false;
	gblPlanNo = "";
	gblHaveTransaction = 0;
	gblFromSwitchLanguage = false;
	gblTranLists=[];
	gblStatementFlow = "";
	gblIstxtAmtEnable = true; //MKI, MIB-12109
    gblMFOrder = {};
    gblMFEventFlag = ' ';
    gblMBMFDetailsResulttable = {};
    gblMFDetailsResulttable = {};
  
    gblDeviceIDUnEncrypt = "";
    gblExitByBackBtn = false;
	gblFundShort="";
    // Added for MIB-7822 Issue
    ITMX_TRANSFER_EWALLET_ENABLE = "";

  	//enable or disable maintainance page function
  	gblIsBackgroundEvent = false;
  	gblUnavailableFlag = false;
  	gblMaintenanceURI = "/maintenance_flag";
	gblTimeoutNetworkInSecond = 3;  
	gblTimeoutServerDownInSecond = 30;
	gblTimeoutMaintenanceInSecond = 30;
  	gblIsLoading = false;
  	gblMBLoggingIn = false;
  
	gblReferCD = "";
    gblTokenSerial="";
	gblTMBBonusflag = "OFF";
  	gblTMBBonusURL="";
  	gblTMBBonusAuthorization = "";
  	gblQrSetDefaultAccnt=false;
  	gblUVregisterMB = "N";
    glbFlowForAccessPin ="";
  	gblActivationCurrentForm = "";
  	gblPreLoginQRResult="";
  	gblPreLoginBillPayFlow=false;
    gblMFSuitabilityFlow = false;
//   	gblIsSuitabilityError = false; // mki, MIB- 10144
  	gblIsFundCodeNull = false;	// mki, MIB- 10003
	gblCardlessFlowCurrentForm = "";
  
  	gblStartClickFromActivationMB=false;
  	gblUVLoadingIndicatorTimeOut=30;
	
	gblConfirmLoadingIndicatorCoun=30;
  	gblTotalPinAttempts = 3; //TODO: move to propertyfile
  	gblUVPushContent = null;
    gbliSTextBoxAmountFocusTrue = false; // mki, MIB-10909
    
  	setDefaultUVSectionDisplayValue(); // Setting all UV globals in one function
  
    gblShowMyQEGenerator = true;
    gblShowlblQRDisclaimer = true;
    gblCardlessWithDrawMaxAmt = "20,000";
  
  	//RTP Globals
  	gblMediaObj = null;
  	gblRTPPrevSelIndex = null;
  	gblRTPListData = null; //To store RTP List Data.
  	gblRTPBillPayPush=false;
  	gblRTPBadgeCount=0;
  
  	//Manage Debit Card Globals
  	gblEditAmountFlow = null;
  	gblCardOperData = null;
    
  	displayRTPAnnoucement = false;
    isRTPCustomer = false;
  	gblTransferPush = false;
  	gblTransferRefCode = "c";
  	gblBillpayRefCode = "r";
  	gblBillerTaxID = "";
  	gblBillerITMXFlag = "";
    isMasterBillerRowCreatorFlow = false;
  	gblPushTransferRTPData = {};
  	gblPushData = {};
	gblofflinepush=false;
  	gblqrPreloginPwdVerified=false;
    gblpp_isOwnAccount = false; // MKI, MIB-9972
 	flowSpa=false;
  	gblMutualFundService="ON";
  	gblLogiflowOptimisation="ON";
  	gblnewLoginCompositeresult="";
	gblCallPhrasesFlag = false;  
  	gblMFSuitabilityScore="";
    // Global variable to show the QR pay slip
    gblShowQRPayslip = "ON";
  	gblDeeplinkExternalAccess = false;
  	gblContentDeeplinkData ="";
  	gblCallErrReportSvcFlag = true;
	gblORFTRange1Lower="";
  	gblORFTRange1Higher="";
    gblORFTRange2Lower="";
	gblORFTRange2Higher="";
  	gblORFTSPlitFeeAmnt1="";
	gblORFTSPlitFeeAmnt2="";
  
    gblSMARTRange1Higher="";
	gblSMARTRange1Lower="";
	gblSMARTRange2Higher="";
	gblSMARTRange2Lower="";
	gblSMARTSPlitFeeAmnt1="";
	gblSMARTSPlitFeeAmnt2="";
  
    // To Acc prompt pay
  	gbltoAccPromptPayRange1Lower = "";
    gbltoAccPromptPayRange1Higher= "";
    gbltoAccPromptPayRange2Lower = "";
    gbltoAccPromptPayRange2Higher= "";
    gbltoAccPromptPaySPlitFeeAmnt1 = "";
    gbltoAccPromptPaySPlitFeeAmnt2 = "";
    gblMaxTransfertoAccPromptPay = "";
    gblTranstoAccPromptPaySplitAmnt = "";
    gblLimittoAccPromptPayPerTransaction = "";
    gblSwitchToOrftSmart = false;// Onlu used when swithed to Promptpay to ORFT
  
  	gblTrnsfrcutoffInd="";
  	gblTrnsfrorftcurrentDate="";
  	gblTrnsfrsmartDate="";
  	gblTrnsfrsmartDateNew="";
  	gblTrnsfrsmartfuturetime="";
  	countTryAgain = 0; //MKI, MIB-10723
  	gblTrnsfrorftFutureTime="";
  	SWITCH_CAL_TRANSFER_FEE="OFF";
	
	//Account Summary Cache related flags
    allowOtherOpr = true;
    isCacheFlow = false;
    cacheBasedActivationFlow = false;
    changeLanguage = false;
    gblforcedCleanAS = false;
    gblAnnouncementFlow = false;
    gblCustomerAge = "";
    swipeCurrentIndex = -1;
    isLoginFlow = false;
    gblRefreshGraph = false;
    gblLastRefresh = null;
  	// New language switch global variable
  	gblChangeLanguage = false;
    GBL_TOUCH_ID_STATUS_COUNT = "1";
    gblMenuConfig = [];
    gblNumTimeVar1 = 1;
    gblNumTimeVar2 = 2;
    gblTimeVar2 = 5;
    gblTimeVar1 = 25;
    gblTimeVar3 = 3;    
    gblShowPwdNo = "3";
    gblShowPwd = "3";
    gblShowPwdNoFinal = "3";
    gblShowPwdFinal = "3";
    gblTotalTimeVar = 180;
    KPNS_REFRESH = "15";
    gblQuickBalanceEnable = "ON";
    GLOBAL_Exchange_Rates_Link_EN = "https://www.tmbbank.com/en/rates/foreign_exchange";
    firstTimeActivationPopUp = "true";
    gblPromptPayBillerAcctName = "";    
	gblTransferCompleteShareFlow = ""; // This is to define in trasnfer complete from calendar to check  callback  based on the share(line,messenger,save image or others)
  	gblLoanApplicationList = [];
  
    // Loan App related globals
  	gblCustomerPassport = "";
  	gblCustomerDob = "";
    gblPaymentCompleteShareFlow = "";
	gblQRPaymentCompleteShareFlow = "";
	gblFPLoginSvsCalled = false;
    gblLoginTab = "FP";
    gblEDonationAmounts = "1000,900,500,400,200,100";
    gblEDonationMaxLimiAmount = "1000000";
    configObject = {
	keys : {
		"APP_KEY" : "563607432caf3fc29e96de4446852692",
		"APP_SECRET" : "cdd553bd6fb7573d763a9ea4d3ecc211"		
    	}
    }
    gblITMXFlag = "";
    // Profile pic versioning
    gblProfilePicCurrentVersion = 0;
    gblAppUpdateAvlble = false;
  	gblDonationNormalFlow = false;
    gblCardMgmtSrvCalled = false;
    gblAccountSummaryFlagMB = "false"
    gblLoanNav = "";
  	gblPolicyNumber = "";
  	gblCashAdvPaymentType = "";
  	gblLoanIncomeInformation = [];
  	gblIncomeInvalidField = null;
  	gblPersonalInfo = [];
  	gblbankInfo = "";
  	gblEncryptedCitizenID = "";
  	gblLoanStatusURL = "";
  	gblAmountPaymentFlag = "";//MKI,MIB-12109
  	gblLoanStatusEncryptType = "";
  	gblEncryptedPhoneNumber = "";
  
  	// Online Bill Payment for PEA
  	gblEA_BILLER_COMP_CODES = "2533";
  	gblPEA_BILLER_COMP_CODE = "2700";
  	allowSchedule = "";
  	gblEDMaxLimitAmt = 1000000;
	gblgetPhrasesCalled = false;
  // loan Credit Card limit
   gblMinresultval = null;
   gblMaxresultval = null;
   gblSliderUsagelimitVal = null;
   gblLoanFullAddressSearch = [];
   //GetPhrases
    GLOBAL_GET_CAMPAIGN_FLAG = 'Y';
    GLOBAL_ACCOUNT_SUMMARY_REFRESH_INTERVAL = 0;
	GET_MASTER_BILLER_REFRESH_FLAG = null;
	GET_MY_BILLER_REFRESH_FLAG = null;
    gblUploadDocumentList = [];
    gblRunningNumber = "01";
    gblContentLoanUpload = "";
    gblContentListIndex = 0;
    gbluploadObjTmp = {};
    gblUploadCheckList = [];
    gblIncompleteDoc = false;
  	gblCaId = "";
  	gblChosenTab = "";
  	gblLoanAppRefNo = "";
    gblenableDebitCardProductList="";
    gblenableDebitCardAccountStatus = "";  
    gblAuthAccessPin = true;
  	gblCallMeNowUrl = "";
  	gblproductService = ""; 
	gblsegAddressData = "" ;
    gblCountryCollectionData = null ;
  	gblServiceType = "";
    gblCardRef = "";
    gblPersonalInfoMasterCollectionData = null;
    gblPersonalInfoMasterSelectedDropDownId = null;
	 //eKYC
	GLOBAL_EKYC_ENABLE_FLAG = "";
  	GLOBAL_EKYC_CID_FLAG="";
  	GLOBAL_NDID_FLAG="";
  	//Account Summary New Design
  	gblAccountSummaryData = null;
  	gblASPrevSelRowVal = "";
  	gblASSelRowIndex = "";
  	gblEnabledLoan = "";
  	gblXCord = "";
  	gblYCord = "";
    //Scan Passport Data
    eKYCData = {};
    gblManageCardFlow ="";
    gblMFSummaryData = "";
    gblRMOpenAcct = false;
    gblBillerCompCodeCalendar = "";
  	gblLoanFullAddressSearchForEditAddress = [];
  	gblAddrSearchLang = "";
	gblEditAddressFlow = "";
    gbleKYCConsentflag = false;
    gblRMOpenAcctresult = [];
    eKYCconfigObject = {
      DefaultCountry : {
        CountryName : "Thailand",
        CountryCode : "TH"
      },
      DefaultSecondNationality:{
        NationalityCode : "-999"
      },
      OtherObjectiveOfAccount:{
        code: "-900"
      }
    }
  	gblIDVerificationFlag = "N";
    gblEKYCFlowExpireDate = "";
    gblEKYCFlowExpireDays = "";
  	
    //SaveAlert
  	gblSaveAlertList = [];
  	gblS2SRepeatType = "OneTime";
  	gblS2SSendRequestType = "";
	gblSelectedDateSA = "";
	gblSavelAlertMode ="normal";
  	gblS2SSendRequestText = "";
  	gblNoofDays = 0;
  	GBL_SAVEALERT_SCHEDULE_MONTHLY_REPEAT_MIN = "2";
  	GBL_SAVEALERT_SCHEDULE_MONTHLY_REPEAT_MAX = "99";
    gblSaveAlertExistValues = {};
  	gblSaveAlertAcceptMonthly = false;
  	gblIdpList = [];
  	gblCurrentAAL = 2.3;
  	gblIDPApproveRequestFlow = false;
  	gblIALFlow = "";
  	gblIDPFlow = "";
    isSaveAlertAutoFocus = false;
	gblSaveAlertNext = false;

	
	//Transfer - onClick next should not allow to select bank
    gblTransferNextClick = false;
    gblUpdateIALFlow = false;
	
	gblIDPApproveRequestFlow = false;
  	gblTimeToShowUVTimerInRed = 30; //To Show UV approval count down timer in Red color
    
    // Verify email Gloabals
    gblisEmailVerifiedinECAS = false;
    gblResendOTPOptionValue = 0;
    gblEmailVerifyModuleName = "";
    gblEmailOTPTime = 59;
    gblEmailIncorrectOTP = 0;
    gblCurrentLocaleBeforeLoan = "";
  	gblUpdateIAlCount = 0;
  	gblAllowUpdateIAl = 1;
	gblMaximumLoanUploadDocumentSize = 960;
}
