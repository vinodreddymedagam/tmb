gblMFSuitQuestNo = 1;
maxNoOfQuestion = 0;
gblMFSuitQuestions = [];
gblMFSuitAnswers = [];
gblMFfxAnswer = "0";
gblMFSuitAlertDays = ["1","2","3","4","5","6","7","15","30","45","60","75","90"];
gblInvestorTypeEN = "";
gblInvestorTypeTH = "";
gblIsExpiry = false;
gblMFRiskLevel = 0;

function preShowfrmIBMFSuitTest(){
  frmIBMFSuitTest.lblMFHearder.text = kony.i18n.getLocalizedString("MU_SB_Suitability");
  frmIBMFSuitTest.btnCancel.text = kony.i18n.getLocalizedString("Back");
  frmIBMFSuitTest.btnNext.text = kony.i18n.getLocalizedString("Next");
}


function onClickNextfrmMFSuitabilityReviewIB(){
  if(GBL_MF_TEMENOS_ENABLE_FLAG == "OFF"){
     	showAlert(kony.i18n.getLocalizedString("Mutual_Fund_Suitability_Off"), kony.i18n.getLocalizedString("info"));
        return false;
  } else {
    if(GBL_MF_SUITABILITY_FLAG == "OFF") {
  	 	showAlert(kony.i18n.getLocalizedString("Mutual_Fund_Suitability_Off"), kony.i18n.getLocalizedString("info"));
        return false;
  	 } else {
  		loadMFSuitabilityTermsNConditionsIB();
  	 }
  }
}

function resetMFSuitQuestionToBeDefaultIB(){
  gblMFSuitQuestNo = 1;
  gblMFSuitAnswers = [];
  gblMFfxAnswer = "0";
  resetMFSuitQuestionBarToGreyIB();
  setDisabledNextMFSuitabilityTestIB();
  clearMFSuitabilityTestAnswersIB();
  setBackMFSuitabilityTestIB();
  frmIBMFSuitTest.btnStep1.text = "1";
  frmIBMFSuitTest.btnStep2.text = "2";
  frmIBMFSuitTest.btnStep3.text = "3";
  frmIBMFSuitTest.btnStep4.text = "4";
  frmIBMFSuitTest.btnStep1.setEnabled(false);
  frmIBMFSuitTest.btnStep2.setEnabled(false);
  frmIBMFSuitTest.btnStep2.setEnabled(false);
  frmIBMFSuitTest.btnStep2.setEnabled(false);
  frmIBMFSuitTest.btnStep1.skin = "btnIBCircleStepBlue31px";
  frmIBMFSuitTest.line4.setVisibility(true);
}

function onClickNextfrmSuitabilityReviewTnCIB(){
  preShowfrmIBMFSuitTest();
  MFSuitQuestionnaireServiceIB();
}

function clearMFSuitabilityTestAnswersIB(){
  frmIBMFSuitTest.imgTick1.src = "android_unsel.png";
  frmIBMFSuitTest.imgTick2.src = "android_unsel.png";
  frmIBMFSuitTest.imgTick3.src = "android_unsel.png";
  frmIBMFSuitTest.imgTick4.src = "android_unsel.png";
  frmIBMFSuitTest.imgTick5.src = "android_unsel.png";
  frmIBMFSuitTest.imgTick6.src = "android_unsel.png";
}

function checkForMFSuitTestLastAnswerIB(){
 // alert(gblMFSuitQuestNo+"=" +maxNoOfQuestion);
  if(gblMFSuitQuestNo == maxNoOfQuestion) {
    frmIBMFSuitTest.btnStep4.skin = "btnIBStepDoneBlue";
    frmIBMFSuitTest.btnStep4.text = "";
  }
}

function setDisabledNextMFSuitabilityTestIB(){
  frmIBMFSuitTest.btnNext.skin = "btnIB158disabled";
  frmIBMFSuitTest.btnNext.focusSkin = "btnIB158disabled";
  frmIBMFSuitTest.btnNext.setEnabled(false);
  if(gblMFSuitQuestNo < maxNoOfQuestion){
    frmIBMFSuitTest.btnNext.text = kony.i18n.getLocalizedString("Next");
  }else{
    frmIBMFSuitTest.btnNext.text = kony.i18n.getLocalizedString("MF_ST_Btn_Sumit");
  }
}

function setEnabledNextMFSuitabilityTestIB(){
  frmIBMFSuitTest.btnNext.skin = "btnIB158";
  frmIBMFSuitTest.btnNext.focusSkin = "btnIB158active";
  frmIBMFSuitTest.btnNext.setEnabled(true);
  if(gblMFSuitQuestNo < maxNoOfQuestion){
    frmIBMFSuitTest.btnNext.text = kony.i18n.getLocalizedString("Next");
    frmIBMFSuitTest.btnNext.onClick = onClickMFSuitNextQuestionIB;
  }else{
    frmIBMFSuitTest.btnNext.text = kony.i18n.getLocalizedString("MF_ST_Btn_Sumit");
    frmIBMFSuitTest.btnNext.onClick = onClickMFSuitSubmitQuestionIB;
  }
}

function onClickMFSuitabilityTestAnswer1IB(){
  clearMFSuitabilityTestAnswersIB();
  kony.print("questionno:"+gblMFSuitQuestNo);
  
  if(GBL_MF_TEMENOS_ENABLE_FLAG == "OFF")
  {
  		if(gblMFSuitQuestNo == maxNoOfQuestion) gblMFfxAnswer = "1";
  		else gblMFSuitAnswers[gblMFSuitQuestNo-1] = "1";
   }else{
 		 gblMFSuitAnswers[gblMFSuitQuestNo-1] = getAnswerValue(gblMFSuitQuestNo,"1");
   } 
   frmIBMFSuitTest.imgTick1.src = "icon_answer_choose.png";
   checkForMFSuitTestLastAnswerIB();
   setEnabledNextMFSuitabilityTestIB();
}

function onClickMFSuitabilityTestAnswer2IB(){
  clearMFSuitabilityTestAnswersIB();
  if(GBL_MF_TEMENOS_ENABLE_FLAG == "OFF"){
     if(gblMFSuitQuestNo == maxNoOfQuestion) gblMFfxAnswer = "2";
  	 else gblMFSuitAnswers[gblMFSuitQuestNo-1] = "2";
  }else{
  	gblMFSuitAnswers[gblMFSuitQuestNo-1] = getAnswerValue(gblMFSuitQuestNo,"2");
  }
  frmIBMFSuitTest.imgTick2.src = "icon_answer_choose.png";
  checkForMFSuitTestLastAnswerIB();
  setEnabledNextMFSuitabilityTestIB();
}

function onClickMFSuitabilityTestAnswer3IB(){
  clearMFSuitabilityTestAnswersIB();
  if(GBL_MF_TEMENOS_ENABLE_FLAG == "OFF"){
    gblMFSuitAnswers[gblMFSuitQuestNo-1] = "3";
  }else{
  	gblMFSuitAnswers[gblMFSuitQuestNo-1] = getAnswerValue(gblMFSuitQuestNo,"3");  
  }
  frmIBMFSuitTest.imgTick3.src = "icon_answer_choose.png";
  checkForMFSuitTestLastAnswerIB();
  setEnabledNextMFSuitabilityTestIB();
}

function onClickMFSuitabilityTestAnswer4IB(){
  clearMFSuitabilityTestAnswersIB();
  if(GBL_MF_TEMENOS_ENABLE_FLAG == "OFF"){
    gblMFSuitAnswers[gblMFSuitQuestNo-1] = "4";
  }else{
    gblMFSuitAnswers[gblMFSuitQuestNo-1] = getAnswerValue(gblMFSuitQuestNo,"4");
  }
  frmIBMFSuitTest.imgTick4.src = "icon_answer_choose.png";
  checkForMFSuitTestLastAnswerIB();
  setEnabledNextMFSuitabilityTestIB();
}


function onClickMFSuitabilityTestAnswer5IB(){
  clearMFSuitabilityTestAnswersIB();
  if(GBL_MF_TEMENOS_ENABLE_FLAG == "OFF"){
    gblMFSuitAnswers[gblMFSuitQuestNo-1] = "5";
  }else{
    gblMFSuitAnswers[gblMFSuitQuestNo-1] = getAnswerValue(gblMFSuitQuestNo,"5");
  }
  frmIBMFSuitTest.imgTick5.src = "icon_answer_choose.png";
  checkForMFSuitTestLastAnswerIB();
  setEnabledNextMFSuitabilityTestIB();
}

function onClickMFSuitabilityTestAnswer6IB(){
  clearMFSuitabilityTestAnswersIB();
  if(GBL_MF_TEMENOS_ENABLE_FLAG == "OFF"){
    gblMFSuitAnswers[gblMFSuitQuestNo-1] = "6";
  }else{
    gblMFSuitAnswers[gblMFSuitQuestNo-1] = getAnswerValue(gblMFSuitQuestNo,"6");
  }
  frmIBMFSuitTest.imgTick6.src = "icon_answer_choose.png";
  checkForMFSuitTestLastAnswerIB();
  setEnabledNextMFSuitabilityTestIB();
}

function getAnswerValue(questNo,inVal){
  var val="";
 // var len = gblMFSuitQuestions.length;
  //kony.print("len"+len);
  questNo = questNo-1;
  kony.print("questNo:"+questNo+":inVal:"+inVal);
  //kony.print("@@@gblMFSuitQuestions:::"+JSON.stringify(gblMFSuitQuestions));
  
  //for(var i=0;i<len;i++){
    if(kony.i18n.getCurrentLocale() == "en_US"){ 
      //if(i == questNo){
        if(inVal == "1")val = gblMFSuitQuestions[questNo]["ansenVal1"];
        if(inVal == "2")val = gblMFSuitQuestions[questNo]["ansenVal2"];
        if(inVal == "3")val = gblMFSuitQuestions[questNo]["ansenVal3"];
        if(inVal == "4")val = gblMFSuitQuestions[questNo]["ansenVal4"];
        if(inVal == "5")val = gblMFSuitQuestions[questNo]["ansenVal5"];
        if(inVal == "6")val = gblMFSuitQuestions[questNo]["ansenVal6"];
     // }  
    }else{
     // if(i == questNo){
        if(inVal == "1")val = gblMFSuitQuestions[questNo]["ansthVal1"];
        if(inVal == "2")val = gblMFSuitQuestions[questNo]["ansthVal2"];
        if(inVal == "3")val = gblMFSuitQuestions[questNo]["ansthVal3"];
        if(inVal == "4")val = gblMFSuitQuestions[questNo]["ansthVal4"];
        if(inVal == "5")val = gblMFSuitQuestions[questNo]["ansthVal5"];
        if(inVal == "6")val = gblMFSuitQuestions[questNo]["ansthVal6"];
      //}
   // }
  }
  //kony.print("val:"+val);
  return val;	
}

function onClickMFSuitNextQuestionIB(){
  showLoadingScreenPopup();
  gblMFSuitQuestNo = gblMFSuitQuestNo + 1;
  clearMFSuitabilityTestAnswersIB();
  setDisabledNextMFSuitabilityTestIB();
  setMFSuitStepQuestionBarIB();
  setBackMFSuitabilityTestIB();
  setMFSuitQuestionAndAnswerDetailIB();
  dismissLoadingScreenPopup();
}

function setMFSuitQuestionAndAnswerDetailIB(){
  if(kony.i18n.getCurrentLocale() == "en_US"){
    frmIBMFSuitTest.lblQuestion.text = gblMFSuitQuestions[gblMFSuitQuestNo-1]["questionEN"];
    frmIBMFSuitTest.lblAnswer1.text = gblMFSuitQuestions[gblMFSuitQuestNo-1]["answerOneEN"];
    frmIBMFSuitTest.lblAnswer2.text = gblMFSuitQuestions[gblMFSuitQuestNo-1]["answerTwoEN"];
    if(isNotBlank(gblMFSuitQuestions[gblMFSuitQuestNo-1]["answerThreeEN"])){
          kony.print("inside if 3");
          frmIBMFSuitTest.hbxAnswer3.setVisibility(true);
          frmIBMFSuitTest.imgTick3.setVisibility(true);
          frmIBMFSuitTest.lblAnswer3.text = gblMFSuitQuestions[gblMFSuitQuestNo-1]["answerThreeEN"];
     }else{
          kony.print("else of Ans 3");
          frmIBMFSuitTest.lblAnswer3.text ="";
          frmIBMFSuitTest.imgTick3.setVisibility(false);
          frmIBMFSuitTest.hbxAnswer3.setVisibility(false);
    }
    if(isNotBlank(gblMFSuitQuestions[gblMFSuitQuestNo-1]["answerFourEN"])){
          kony.print("inside if 4");
          frmIBMFSuitTest.hbxAnswer4.setVisibility(true);
          frmIBMFSuitTest.imgTick4.setVisibility(true);
          frmIBMFSuitTest.lblAnswer4.text = gblMFSuitQuestions[gblMFSuitQuestNo-1]["answerFourEN"];
    }else{
          kony.print("else of Ans 4");
          frmIBMFSuitTest.lblAnswer4.text ="";
          frmIBMFSuitTest.imgTick4.setVisibility(false);
          frmIBMFSuitTest.hbxAnswer4.setVisibility(false);
    }
    if(isNotBlank(gblMFSuitQuestions[gblMFSuitQuestNo-1]["answerFiveEN"])){
          kony.print("inside if 5");
          frmIBMFSuitTest.hbxAnswer5.setVisibility(true);
          frmIBMFSuitTest.imgTick5.setVisibility(true);
          frmIBMFSuitTest.lblAnswer5.text = gblMFSuitQuestions[gblMFSuitQuestNo-1]["answerFiveEN"];
    }else{
          kony.print("else of Ans 5");
          frmIBMFSuitTest.lblAnswer5.text ="";
          frmIBMFSuitTest.imgTick5.setVisibility(false);
          frmIBMFSuitTest.hbxAnswer5.setVisibility(false);
    }
    if(isNotBlank(gblMFSuitQuestions[gblMFSuitQuestNo-1]["answerSixEN"])){
          kony.print("inside if 6");
          frmIBMFSuitTest.hbxAnswer6.setVisibility(true);
          frmIBMFSuitTest.imgTick6.setVisibility(true);
          frmIBMFSuitTest.lblAnswer6.text = gblMFSuitQuestions[gblMFSuitQuestNo-1]["answerSixEN"];
    }else{
          kony.print("else of Ans 6");
          frmIBMFSuitTest.lblAnswer6.text ="";
          frmIBMFSuitTest.imgTick6.setVisibility(false);
          frmIBMFSuitTest.hbxAnswer6.setVisibility(false);
    }
    
  }else{
    frmIBMFSuitTest.lblQuestion.text = gblMFSuitQuestions[gblMFSuitQuestNo-1]["questionTH"];
    frmIBMFSuitTest.lblAnswer1.text = gblMFSuitQuestions[gblMFSuitQuestNo-1]["answerOneTH"];
    frmIBMFSuitTest.lblAnswer2.text = gblMFSuitQuestions[gblMFSuitQuestNo-1]["answerTwoTH"];
    if(isNotBlank(gblMFSuitQuestions[gblMFSuitQuestNo-1]["answerThreeTH"])){
          kony.print("inside if 3");
          frmIBMFSuitTest.hbxAnswer3.setVisibility(true);
          frmIBMFSuitTest.imgTick3.setVisibility(true);
          frmIBMFSuitTest.lblAnswer3.text = gblMFSuitQuestions[gblMFSuitQuestNo-1]["answerThreeTH"];
     }else{
          kony.print("else of Ans 3");
          frmIBMFSuitTest.lblAnswer3.text ="";
          frmIBMFSuitTest.imgTick3.setVisibility(false);
          frmIBMFSuitTest.hbxAnswer3.setVisibility(false);
    }
    if(isNotBlank(gblMFSuitQuestions[gblMFSuitQuestNo-1]["answerFourTH"])){
          kony.print("inside if 4");
          frmIBMFSuitTest.hbxAnswer4.setVisibility(true);
          frmIBMFSuitTest.imgTick4.setVisibility(true);
          frmIBMFSuitTest.lblAnswer4.text = gblMFSuitQuestions[gblMFSuitQuestNo-1]["answerFourTH"];
    }else{
          kony.print("else of Ans 4");
          frmIBMFSuitTest.lblAnswer4.text ="";
          frmIBMFSuitTest.imgTick4.setVisibility(false);
          frmIBMFSuitTest.hbxAnswer4.setVisibility(false);
    }
    if(isNotBlank(gblMFSuitQuestions[gblMFSuitQuestNo-1]["answerFiveTH"])){
          kony.print("inside if 5");
          frmIBMFSuitTest.hbxAnswer5.setVisibility(true);
          frmIBMFSuitTest.imgTick5.setVisibility(true);
          frmIBMFSuitTest.lblAnswer5.text = gblMFSuitQuestions[gblMFSuitQuestNo-1]["answerFiveTH"];
    }else{
          kony.print("else of Ans 5");
          frmIBMFSuitTest.lblAnswer5.text ="";
          frmIBMFSuitTest.imgTick5.setVisibility(false);
          frmIBMFSuitTest.hbxAnswer5.setVisibility(false);
    }
    if(isNotBlank(gblMFSuitQuestions[gblMFSuitQuestNo-1]["answerSixTH"])){
          kony.print("inside if 6");
          frmIBMFSuitTest.hbxAnswer6.setVisibility(true);
          frmIBMFSuitTest.imgTick6.setVisibility(true);
          frmIBMFSuitTest.lblAnswer6.text = gblMFSuitQuestions[gblMFSuitQuestNo-1]["answerSixTH"];
    }else{
          kony.print("else of Ans 6");
          frmIBMFSuitTest.lblAnswer6.text ="";
          frmIBMFSuitTest.imgTick6.setVisibility(false);
          frmIBMFSuitTest.hbxAnswer6.setVisibility(false);
    }
  }
  //checkAnswerForLastQuetsionIB();
}

function checkAnswerForLastQuetsionIB(){
  if(gblMFSuitQuestNo == maxNoOfQuestion){
    frmIBMFSuitTest.lblQuestion.text = maxNoOfQuestion + ". " + frmIBMFSuitTest.lblQuestion.text;
    frmIBMFSuitTest.hbxAnswer3.setVisibility(false);
    frmIBMFSuitTest.hbxAnswer4.setVisibility(false);
  }else{
    frmIBMFSuitTest.hbxAnswer3.setVisibility(true);
    frmIBMFSuitTest.hbxAnswer4.setVisibility(true);
  }
}

function resetMFSuitQuestionBarToGreyIB(){
  frmIBMFSuitTest.btnStep1.skin = "btnIBCircleStepGrey28px";
  frmIBMFSuitTest.btnStep2.skin = "btnIBCircleStepGrey28px";
  frmIBMFSuitTest.btnStep3.skin = "btnIBCircleStepGrey28px";
  frmIBMFSuitTest.btnStep4.skin = "btnIBCircleStepGrey28px";
}

function setMFSuitStepQuestionBarIB(){
  resetMFSuitQuestionBarToGreyIB();
  if(gblMFSuitQuestNo < maxNoOfQuestion-3){
	frmIBMFSuitTest.btnStep1.text = (gblMFSuitQuestNo).toFixed(0);
    frmIBMFSuitTest.btnStep2.text = (gblMFSuitQuestNo + 1).toFixed(0);
    frmIBMFSuitTest.btnStep3.text = (gblMFSuitQuestNo + 2).toFixed(0);
    frmIBMFSuitTest.btnStep4.text = (gblMFSuitQuestNo + 3).toFixed(0);
    if(gblMFSuitQuestNo == maxNoOfQuestion-4){
      frmIBMFSuitTest.line4.setVisibility(false);
    }else{
      frmIBMFSuitTest.line4.setVisibility(true);
    }
    frmIBMFSuitTest.btnStep1.skin = "btnIBCircleStepBlue31px";
    frmIBMFSuitTest.btnStep2.skin = "btnIBCircleStepGrey28px";
    frmIBMFSuitTest.btnStep3.skin = "btnIBCircleStepGrey28px";
    frmIBMFSuitTest.btnStep4.skin = "btnIBCircleStepGrey28px";
  }else{
    frmIBMFSuitTest.btnStep1.text = maxNoOfQuestion-3;
    frmIBMFSuitTest.btnStep2.text = maxNoOfQuestion-2;
    frmIBMFSuitTest.btnStep3.text = maxNoOfQuestion-1;
    frmIBMFSuitTest.btnStep4.text = maxNoOfQuestion; 
    if(gblMFSuitQuestNo == maxNoOfQuestion-3){
	  frmIBMFSuitTest.btnStep1.skin = "btnIBCircleStepBlue31px";
      frmIBMFSuitTest.btnStep2.skin = "btnIBCircleStepGrey28px";
      frmIBMFSuitTest.btnStep3.skin = "btnIBCircleStepGrey28px";
      frmIBMFSuitTest.btnStep4.skin = "btnIBCircleStepGrey28px";
    }else if(gblMFSuitQuestNo == maxNoOfQuestion-2){
	  frmIBMFSuitTest.btnStep1.skin = "btnIBCircleStepGrey28px";
      frmIBMFSuitTest.btnStep2.skin = "btnIBCircleStepBlue31px";
      frmIBMFSuitTest.btnStep3.skin = "btnIBCircleStepGrey28px";
      frmIBMFSuitTest.btnStep4.skin = "btnIBCircleStepGrey28px";
    }else if(gblMFSuitQuestNo == maxNoOfQuestion-1){
	  frmIBMFSuitTest.btnStep1.skin = "btnIBCircleStepGrey28px";
      frmIBMFSuitTest.btnStep2.skin = "btnIBCircleStepGrey28px";
      frmIBMFSuitTest.btnStep3.skin = "btnIBCircleStepBlue31px";
      frmIBMFSuitTest.btnStep4.skin = "btnIBCircleStepGrey28px";
    }else if(gblMFSuitQuestNo == maxNoOfQuestion){
	  frmIBMFSuitTest.btnStep1.skin = "btnIBCircleStepGrey28px";
      frmIBMFSuitTest.btnStep2.skin = "btnIBCircleStepGrey28px";
      frmIBMFSuitTest.btnStep3.skin = "btnIBCircleStepGrey28px";
      frmIBMFSuitTest.btnStep4.skin = "btnIBCircleStepBlue31px";
    }
  }
}

function setBackMFSuitabilityTestIB(){
  if(gblMFSuitQuestNo == 1){
    frmIBMFSuitTest.btnCancel.onClick = onClickNextfrmMFSuitabilityReviewIB;
  }else{
    frmIBMFSuitTest.btnCancel.onClick = setBackMFSuitabilityTestPrevQuestionIB;
  }
}

function setBackMFSuitabilityTestPrevQuestionIB(){
  if(gblMFSuitQuestNo != maxNoOfQuestion){
 //   	gblMFSuitAnswers[gblMFSuitQuestNo-2] = "0"; // rest value for current question
 		gblMFSuitAnswers[gblMFSuitQuestNo-1] = "0"; // rest value for current question
  }
  gblMFSuitQuestNo = gblMFSuitQuestNo - 1;	// get back to prev question
  var prevAnswer = gblMFSuitAnswers[gblMFSuitQuestNo-1];	// check the prev answer to be displayed.
  if(prevAnswer == "0"){
    clearMFSuitabilityTestAnswersIB();
    setDisabledNextMFSuitabilityTestIB();
  }else if(prevAnswer == "1"){
    onClickMFSuitabilityTestAnswer1IB()
  }else if(prevAnswer == "2"){
    onClickMFSuitabilityTestAnswer2IB()
  }else if(prevAnswer == "3"){
    onClickMFSuitabilityTestAnswer3IB();
  }else if(prevAnswer == "4"){
    onClickMFSuitabilityTestAnswer4IB();
  }else if(prevAnswer == "5"){
    onClickMFSuitabilityTestAnswer5IB();
  }else if(prevAnswer == "6"){
    onClickMFSuitabilityTestAnswer6IB();
  }
  setMFSuitStepQuestionBarIB();
  setBackMFSuitabilityTestIB();
  setMFSuitQuestionAndAnswerDetailIB();
  if(gblMFSuitQuestNo == 12){
    frmIBMFSuitTest.btnStep4.skin = "btnIBCircleStepGrey28px";
  }
}

function MFSuitQuestionnaireServiceIB() {
  showLoadingScreenPopup();
  var inputParam = {};
  if(GBL_MF_TEMENOS_ENABLE_FLAG == "OFF"){
      inputParam["customerType"] = "P";
      invokeServiceSecureAsync("SuitQuestionnaireETEInq", inputParam, callBackMFSuitQuestionnaireServiceIBold);	
  }else{
  	invokeServiceSecureAsync("SuitQuestionnaireENTH", inputParam, callBackMFSuitQuestionnaireServiceIB);  
  }
}

function callBackMFSuitQuestionnaireServiceIBold(status, resulttable){
  gblMFSuitQuestions = [];
  if (status == 400) {
    if (resulttable["opstatus"] == 0) {
      var questionList = resulttable["questionGrp1DS"];
      //alert("questionList:"+questionList);
      if(questionList != null){
        for(var i=0; i < questionList.length; i++){
          var tempRecord = { 
            "questionEN" : questionList[i]["questionEN"], 
            "questionTH" : questionList[i]["questionTH"],    
            "answerOneEN" : questionList[i]["answerENDS"][0]["answer1"],    
            "answerTwoEN" : questionList[i]["answerENDS"][0]["answer2"],
            "answerThreeEN" : questionList[i]["answerENDS"][0]["answer3"],
            "answerFourEN" : questionList[i]["answerENDS"][0]["answer4"],
            "answerOneTH" : questionList[i]["answerTHDS"][0]["answer1"],   
            "answerTwoTH" : questionList[i]["answerTHDS"][0]["answer2"],    
            "answerThreeTH" : questionList[i]["answerTHDS"][0]["answer3"],
            "answerFourTH" : questionList[i]["answerTHDS"][0]["answer4"]
          };
          gblMFSuitQuestions.push(tempRecord);
        }

        questionList = resulttable["questionGrp2DS"];
        if(questionList != null){
          for(var i=0; i < questionList.length; i++){
            var tempRecord = { 
              "questionEN" : questionList[i]["questionGrp2EN"], 
              "questionTH" : questionList[i]["questionGrp2TH"],    
              "answerOneEN" : questionList[i]["answerENGrp2DS"][0]["answer1Grp2"],    
              "answerTwoEN" : questionList[i]["answerENGrp2DS"][0]["answer2Grp2"],
              "answerThreeEN" : "",
              "answerFourEN" : "",
              "answerOneTH" : questionList[i]["answerTHGrp2DS"][0]["answer1Grp2"],   
              "answerTwoTH" : questionList[i]["answerTHGrp2DS"][0]["answer2Grp2"],    
              "answerThreeTH" : "",
              "answerFourTH" : ""
            };
            gblMFSuitQuestions.push(tempRecord);
          }
        }

        if(gblMFSuitQuestions.length < maxNoOfQuestion){
          alert("Question No. < 13");
          return false;
        }
        resetMFSuitQuestionToBeDefaultIB();
        setMFSuitQuestionAndAnswerDetailIB();
        frmIBMFSuitTest.show();
      }else{
        alert("No question.");
        return false;
      }
      dismissLoadingScreenPopup();
    }else{
      dismissLoadingScreenPopup();
      alert(kony.i18n.getLocalizedString("ECGenericError"));
      return false;
    }
  }else{
    dismissLoadingScreenPopup();
    alert(kony.i18n.getLocalizedString("ECGenericError"));
    return false;
  }
}

function callBackMFSuitQuestionnaireServiceIB(status, resulttable){
	try{
	  gblMFSuitQuestions = [];
	  if (status == 400) {
		if (resulttable["opstatus"] == 0) {
		  var questionListTh = resulttable["questionsTHDS"];
		  var questionListEn = resulttable["questionsENDS"];
		  kony.print("Th len:"+questionListTh.length);
          maxNoOfQuestion = questionListTh.length;
		  if(isNotBlank(questionListTh) && isNotBlank(questionListEn) && questionListTh.length == questionListEn.length){
			for(var i=0; i < questionListTh.length; i++){
			  var questth = questionListTh[i]["questionLabel"];
			  var ansthname1="",ansthname2="",ansthname3="",ansthname4="",ansthname5="",ansthname6="";
			  var ansthVal1="",ansthVal2="",ansthVal3="",ansthVal4="",ansthVal5="",ansthVal6="";
			  var ansth1="",ansth2="",ansth3="",ansth4="",ansth5="",ansth6="";
              if(isNotBlank(questth)){quest = questionListTh[i]["questionLabel"];
			  }else { quest ="";}			  
			  
			  if(isNotBlank(questionListTh[i]["answerDS"][0]))
			  {
                ansth1 = questionListTh[i]["answerDS"][0]["answerLabel"];
			    ansthname1= questionListTh[i]["answerDS"][0]["answerName"];
			    ansthVal1 =questionListTh[i]["answerDS"][0]["answerValue"];
			  }
			  if(isNotBlank(questionListTh[i]["answerDS"][1]))
			  {
				ansth2 = questionListTh[i]["answerDS"][1]["answerLabel"];
				ansthname2= questionListTh[i]["answerDS"][1]["answerName"];
			    ansthVal2 =questionListTh[i]["answerDS"][1]["answerValue"];
			  }
			  if(isNotBlank(questionListTh[i]["answerDS"][2]))
			  {
				ansth3 = questionListTh[i]["answerDS"][2]["answerLabel"];
				ansthname3= questionListTh[i]["answerDS"][2]["answerName"];
			    ansthVal3 =questionListTh[i]["answerDS"][2]["answerValue"];
			  }
			  if(isNotBlank(questionListTh[i]["answerDS"][3]))
			  {
				ansth4 = questionListTh[i]["answerDS"][3]["answerLabel"];
				ansthname4= questionListTh[i]["answerDS"][3]["answerName"];
			    ansthVal4 =questionListTh[i]["answerDS"][3]["answerValue"];
		      }
			  if(isNotBlank(questionListTh[i]["answerDS"][4]))
			  {
				ansth5 = questionListTh[i]["answerDS"][4]["answerLabel"];
				ansthname5= questionListTh[i]["answerDS"][4]["answerName"];
			    ansthVal5 =questionListTh[i]["answerDS"][4]["answerValue"];
		      }
			  if(isNotBlank(questionListTh[i]["answerDS"][5]))
			  {
				ansth6 = questionListTh[i]["answerDS"][5]["answerLabel"];
				ansthname6= questionListTh[i]["answerDS"][5]["answerName"];
			    ansthVal6 =questionListTh[i]["answerDS"][5]["answerValue"];
		      }
              
			  var questen = questionListEn[i]["questionLabel"];
			  if(questen!= "" && questen != null){questen = questionListEn[i]["questionLabel"];
			  }else { questen ="";}	
			  var ansenname1="",ansenname2="",ansenname3="",ansenname4="",ansenname5="",ansenname6="";
			  var ansenVal1="",ansenVal2="",ansenVal3="",ansenVal4="",ansenVal5="",ansenVal6="";
			  var ansen1 ="",ansen2 ="",ansen3 ="",ansen4 ="",ansen5 ="",ansen6 ="";
			  if(isNotBlank(questionListEn[i]["answerDS"][0]))
			  {
				  ansen1 = questionListEn[i]["answerDS"][0]["answerLabel"];
				  ansenname1= questionListEn[i]["answerDS"][0]["answerName"];
				  ansenVal1 =questionListEn[i]["answerDS"][0]["answerValue"];
			  }
			  if(isNotBlank(questionListEn[i]["answerDS"][1]))
			  {	
				ansen2 = questionListEn[i]["answerDS"][1]["answerLabel"];
				ansenname2= questionListEn[i]["answerDS"][1]["answerName"];
				ansenVal2 =questionListEn[i]["answerDS"][1]["answerValue"];
			  }
			  if(isNotBlank(questionListEn[i]["answerDS"][2]))
			  {
				ansen3 = questionListEn[i]["answerDS"][2]["answerLabel"];
				ansenname3 = questionListEn[i]["answerDS"][2]["answerName"];
				ansenVal3 = questionListEn[i]["answerDS"][2]["answerValue"];
			  }
			  if(isNotBlank(questionListEn[i]["answerDS"][3]))
			  {
				  ansen4 = questionListEn[i]["answerDS"][3]["answerLabel"];
				  ansenname4 = questionListEn[i]["answerDS"][3]["answerName"];
				  ansenVal4 = questionListEn[i]["answerDS"][3]["answerValue"];
			  }
			  if(isNotBlank(questionListEn[i]["answerDS"][4]))
			  {
				  ansen5 = questionListEn[i]["answerDS"][4]["answerLabel"];
				  ansenname5 = questionListEn[i]["answerDS"][4]["answerName"];
				  ansenVal5 = questionListEn[i]["answerDS"][4]["answerValue"];
			  }
              if(isNotBlank(questionListEn[i]["answerDS"][5]))
			  {
				  ansen5 = questionListEn[i]["answerDS"][5]["answerLabel"];
				  ansenname5 = questionListEn[i]["answerDS"][5]["answerName"];
				  ansenVal5 = questionListEn[i]["answerDS"][5]["answerValue"];
			  }
			  var tempRecord = { 
				"questionTH" : questth,
				"questionEN" : questen, 				
				
				"answerOneTH" : ansth1,
				"answerOneEN" : ansen1,
				"ansthname1" : ansthname1,
				"ansenname1": ansenname1,
				"ansthVal1" : ansthVal1,
				"ansenVal1"  : ansenVal1,
				
				
				"answerTwoTH" : ansth2,
				"answerTwoEN" : ansen2,
				"ansthname2"  : ansthname2,
				"ansenname2": ansenname2,
				"ansthVal2" : ansthVal2,
				"ansenVal2"   : ansenVal2,
				
				"answerThreeTH" :ansth3,
				"answerThreeEN" :ansen3,
				"ansthname3"  : ansthname3,
				"ansenname3": ansenname3,
				"ansthVal3" : ansthVal3,
				"ansenVal3"  : ansenVal3,
				
				"answerFourTH" : ansth4,
				"answerFourEN" : ansen4,
				"ansthname4"  : ansthname4,
				"ansenname4": ansenname4,
				"ansthVal4" : ansthVal4,
				"ansenVal4"  : ansenVal4,
                
                "answerFiveTH" : ansth5,
				"answerFiveEN" : ansen5,
				"ansthname5"  : ansthname5,
				"ansenname5": ansenname5,
				"ansthVal5" : ansthVal5,
				"ansenVal5"  : ansenVal5,
                 
                "answerSixTH" : ansth6,
				"answerSixEN" : ansen6,
				"ansthname6"  : ansthname6,
				"ansenname6": ansenname6,
				"ansthVal6" : ansthVal6,
				"ansenVal6"  : ansenVal6
				};
			 
			 gblMFSuitQuestions.push(tempRecord);
			}
			if(gblMFSuitQuestions.length < maxNoOfQuestion){
			  return false;
			}
			resetMFSuitQuestionToBeDefaultIB();
			setMFSuitQuestionAndAnswerDetailIB();
			frmIBMFSuitTest.show();
		  }else{
			alert("No question.");
			return false;
		  }
		  dismissLoadingScreenPopup();
		}else{
		  dismissLoadingScreenPopup();
		  alert(kony.i18n.getLocalizedString("ECGenericError"));
		  return false;
		}
	  }else{
		dismissLoadingScreenPopup();
		alert(kony.i18n.getLocalizedString("ECGenericError"));
		return false;
	  }
	}catch(e){
		kony.print("Exception in callBackMFSuitQuestionnaireServiceIB:"+e);
	}
}
function onClickMFSuitSubmitQuestionIB(){
  MFCalcSuitabilityServiceIB();
}

function MFCalcSuitabilityServiceIB() {
  showLoadingScreenPopup();
  var inputParam = {};
  if(GBL_MF_TEMENOS_ENABLE_FLAG == "OFF"){
              inputParam["documentID"] = gblCustomerIDValue;
        if (kony.string.isNumeric(gblCustomerIDValue) && gblCustomerIDValue.length == 13) {
          inputParam["documentType"] = "CI" // Citizen
        } else {
          inputParam["documentType"] = "PP" // Passport
        }

      //MIB-9990 - Mutual funds cannot update suitability - Send Unit Holder from backend 
      /* var data = frmIBMutualFundsPortfolio.segAccountDetails.data[1].lblunitHolderNumber
        var unitholder = data.replace(/-/gi, "");
        inputParam["unitHolderNo"] =  unitholder; //"110203000296"; 
        */
        inputParam["customerName"] = gblCustomerName; 
        inputParam["customerType"] = "P";
        inputParam["accountType"] = "S";
        inputParam["branchNo"] = "9882";
        inputParam["answers"] = gblMFSuitAnswers.toString();
        inputParam["fxAnswer"] = gblMFfxAnswer;
        inputParam["channelBr"] = "MIB-MF";
        invokeServiceSecureAsync("CalcSuitabilityETEInq", inputParam, callBackMFCalcSuitabilityServiceIB)
    
  }else{
      inputParam["documentID"] = gblCustomerIDValue;
      if (kony.string.isNumeric(gblCustomerIDValue) && gblCustomerIDValue.length == 13) {
        inputParam["documentType"] = "CI" // Citizen
      } else {
        inputParam["documentType"] = "PP" // Passport
      }

    //MIB-9990 - Mutual funds cannot update suitability - Send Unit Holder from backend 
    /* var data = frmIBMutualFundsPortfolio.segAccountDetails.data[1].lblunitHolderNumber
      var unitholder = data.replace(/-/gi, "");
      inputParam["unitHolderNo"] =  unitholder; //"110203000296"; 
      */
      var mfanswers = getMFSiutAnswerNames();
      kony.print("mfanswers>>"+mfanswers);
      //var fixans="";
      //if(gblMFfxAnswer == "1") fixans ="Yes"; else fixans ="No";
      inputParam["customerName"] = gblCustomerName; 
      inputParam["customerType"] = "P";
      inputParam["accountType"] = "S";
      inputParam["branchNo"] = "9882";
      inputParam["answers"] = mfanswers.toString();
      //inputParam["fxAnswer"] = fixans;
      inputParam["channelBr"] = "MIB-MF";
      inputParam["answersActLog"]= gblMFSuitAnswers.toString();
      invokeServiceSecureAsync("CalcSuitabilityETEInqNew", inputParam, callBackMFCalcSuitabilityServiceIB)
  }
   
}

function callBackMFCalcSuitabilityServiceIB(status, resulttable) {
  if (status == 400) {
    if (resulttable["opstatus"] == 0) {
      gblMFRiskLevel = resulttable["RiskLevel"];
      var expireDate = resulttable["ExpireDate"];
      var yyyyMMdd = expireDate.split("-");
      expireDate = yyyyMMdd[2] + "/" + yyyyMMdd[1] + "/" + yyyyMMdd[0];
      gblInvestorTypeEN = resulttable["InvestorTypeEN"];
      gblInvestorTypeTH = resulttable["InvestorTypeTH"];
      frmIBMFSuitReview.lblUrRiskAssesment.text = kony.i18n.getLocalizedString("MF_lbl_01");
      var levelRiskVal = kony.i18n.getLocalizedString("MF_lbl_02");
      levelRiskVal = levelRiskVal.replace("{x}", gblMFRiskLevel);
      levelRiskVal = levelRiskVal.replace("{y}", "5");
      frmIBMFSuitReview.lblMessage.text = levelRiskVal;
      frmIBMFSuitReview.imgRiskLevel.src = getMFRiskResultLevelImage(gblMFRiskLevel);
      frmIBMFSuitReview.lblDateOfExpiry.setVisibility(false);
      frmIBMFSuitReview.lblTypeOfInvest.text = kony.i18n.getLocalizedString("MF_lbl_InvestorType");
      if(kony.i18n.getCurrentLocale() == "en_US"){
        frmIBMFSuitReview.lblTypeOfInvestVal.text = gblInvestorTypeEN;
      }else{
        frmIBMFSuitReview.lblTypeOfInvestVal.text = gblInvestorTypeTH;
      }
      frmIBMFSuitReview.linkTakeItAgain.text = kony.i18n.getLocalizedString("MF_lbl_Takeagain");
      frmIBMFSuitReview.lblDateOfExpVal.text = kony.i18n.getLocalizedString("MF_lbl_ExpiredDate") + ": " +expireDate;
      frmIBMFSuitReview.show();
      dismissLoadingScreenPopup();
    }else{
      dismissLoadingScreenPopup();
      alert(kony.i18n.getLocalizedString("ECGenericError"));
      return false;
    }
  }else{
    dismissLoadingScreenPopup();
    alert(kony.i18n.getLocalizedString("ECGenericError"));
    return false;
  }
}

function getMFSiutAnswerNames(){
 var len = gblMFSuitAnswers.length;
 var amswernames=[];
  kony.print("gblMFSuitAnswers:"+gblMFSuitAnswers+", len>>>"+len);
 for (var i=0;i<len;i++){
	  kony.print("i value:"+gblMFSuitAnswers);
	  if(kony.i18n.getCurrentLocale() == "en_US"){ 
		 if(gblMFSuitAnswers[i] == "1"){
			amswernames.push(gblMFSuitQuestions[i]["ansenname1"]); 
		 }else if(gblMFSuitAnswers[i] == "2"){
			amswernames.push(gblMFSuitQuestions[i]["ansenname2"]) ;
		 }else if(gblMFSuitAnswers[i] == "3"){
			amswernames.push(gblMFSuitQuestions[i]["ansenname3"]); 
		 }else if(gblMFSuitAnswers[i] == "4"){
			//kony.print("inside4:"+gblMFSuitQuestions[i]["ansenname4"]);
			amswernames.push(gblMFSuitQuestions[i]["ansenname4"]); 
		 }else if(gblMFSuitAnswers[i] == "5"){
			//kony.print("inside5:"+gblMFSuitQuestions[i]["ansenname5"]);
			amswernames.push(gblMFSuitQuestions[i]["ansenname5"]); 
		 }else if(gblMFSuitAnswers[i] == "6"){
			//kony.print("inside6:"+gblMFSuitQuestions[i]["ansenname6"]);
			amswernames.push(gblMFSuitQuestions[i]["ansenname6"]); 
		 }
	}else{
		if(gblMFSuitAnswers[i] == "1"){
			amswernames.push(gblMFSuitQuestions[i]["ansthname1"]); 
		 }else if(gblMFSuitAnswers[i] == "2"){
			amswernames.push(gblMFSuitQuestions[i]["ansthname2"]); 
		 }else if(gblMFSuitAnswers[i] == "3"){
			amswernames.push(gblMFSuitQuestions[i]["ansthname3"]); 
		 }else if(gblMFSuitAnswers[i] == "4"){
			amswernames.push(gblMFSuitQuestions[i]["ansthname4"]);
		 }else if(gblMFSuitAnswers[i] == "5"){
			amswernames.push(gblMFSuitQuestions[i]["ansthname5"]);
		 }else if(gblMFSuitAnswers[i] == "6"){
			amswernames.push(gblMFSuitQuestions[i]["ansthname6"]);
		 }
      
	}	
 }
 kony.print("amswernames>>"+amswernames)
return amswernames;
}

function getMFRiskLevelImage(level){
  if(level == 1){
    return "mf_level_1.png";
  }else if(level == 2){
    return "mf_level_2.png";
  }else if(level == 3){
    return "mf_level_3.png";
  }else if(level == 4){
    return "mf_level_4.png";
  }else if(level == 5){
    return "mf_level_5.png";
  }else{
    return "";
  }
}

function getMFRiskResultLevelImage(level){
  if(level == 1){
    return "result_level_1.png";
  }else if(level == 2){
    return "result_level_2.png";
  }else if(level == 3){
    return "result_level_3.png";
  }else if(level == 4){
    return "result_level_4.png";
  }else if(level == 5){
    return "result_level_5.png";
  }else{
    return "";
  }
}


function getMFSuitibilityReviewDetailsIB(){
  //alert("GBL_MF_TEMENOS_ENABLE_FLAG:"+GBL_MF_TEMENOS_ENABLE_FLAG);
  showLoadingScreenPopup();
  var inputParam = {};
  if(GBL_MF_TEMENOS_ENABLE_FLAG == "OFF"){
  	inputParam["issuedIdentType"] = "CI";
  	inputParam["unitHolderNo"] = "fromAcctSummary";
  	invokeServiceSecureAsync("MFSuitabilityInq", inputParam, callBackGetMFSuitibilityReviewDetailsIB);
  }else{
    invokeServiceSecureAsync("MFSuitabilityInqNew", inputParam, callBackGetMFSuitibilityReviewDetailsIB);
    
  }
}

function callBackGetMFSuitibilityReviewDetailsIB(status, resulttable){ //mki, mib-11469/11470 start
  kony.print("MKI status="+status );
  if (status == 400) {
    if (resulttable["opstatus"] == 0) {
      if (resulttable["responseCode"] == "001") {
        dismissLoadingScreen();
        alert(resulttable["responseDesc"]);
        return false;
      }else{
        gblMFRiskLevel = resulttable["suitabilityScore"];
        var expireDate = resulttable["suitabilityExpireDate"];
        var createDate = resulttable["CreateDate"];
        gblInvestorTypeEN = resulttable["InvestorTypeEN"];
        gblInvestorTypeTH = resulttable["InvestorTypeTH"];
        var yyyyMMdd = expireDate.split("-");
        expireDate = yyyyMMdd[2] + "/" + yyyyMMdd[1] + "/" + yyyyMMdd[0];
        yyyyMMdd = createDate.split("-");
        createDate = yyyyMMdd[2] + "/" + yyyyMMdd[1] + "/" + yyyyMMdd[0];
        var expiryDays = getMFSuitabilityTestExpiryDays(expireDate);
        if(expiryDays == -1){
          gblIsExpiry = true;
          getMFSuitibilityReviewExpiryIB(expireDate);
        }else if(expiryDays <= 90 && gblMFSuitAlertDays.indexOf(expiryDays.toFixed(0)) != -1){
          if(!gblOrderFlow) {
            gblIsExpiry = false;
            getMFSuitibilityReviewExpiryWarningIB(expireDate);
          } else {
            IBcallGetFundList();
            return;
          }
        }else{
          if(gblOrderFlow) {
            IBcallGetFundList();
            return;
          } else {
            frmIBMFSuitReview.lblUrRiskAssesment.text = kony.i18n.getLocalizedString("MF_lbl_01");
            var levelRiskVal = kony.i18n.getLocalizedString("MF_lbl_02");
            levelRiskVal = levelRiskVal.replace("{x}", gblMFRiskLevel);
            levelRiskVal = levelRiskVal.replace("{y}", "5");
            frmIBMFSuitReview.lblMessage.text = levelRiskVal;
            frmIBMFSuitReview.imgRiskLevel.src = getMFRiskLevelImage(gblMFRiskLevel);
            frmIBMFSuitReview.lblDateOfExpiry.setVisibility(true);                     
            frmIBMFSuitReview.lblDateOfExpiry.text = kony.i18n.getLocalizedString("MF_lbl_SuitabilityDate") + ": "+createDate; // need return value
            frmIBMFSuitReview.lblTypeOfInvest.text = kony.i18n.getLocalizedString("MF_lbl_InvestorType");
            if(kony.i18n.getCurrentLocale() == "en_US"){
              frmIBMFSuitReview.lblTypeOfInvestVal.text = gblInvestorTypeEN;
            }else{
              frmIBMFSuitReview.lblTypeOfInvestVal.text = gblInvestorTypeTH;
            }
            frmIBMFSuitReview.linkTakeItAgain.text = kony.i18n.getLocalizedString("MF_lbl_Takeagain");
            frmIBMFSuitReview.lblDateOfExpVal.text = kony.i18n.getLocalizedString("MF_lbl_ExpiredDate") + ": " +expireDate;
            frmIBMFSuitReview.show();  
          }
        }
        dismissLoadingScreenPopup();              
      }
    }else{
                                                dismissLoadingScreenPopup();                  
                                                showAlert(kony.i18n.getLocalizedString("MF_P_ERR_01005"), kony.i18n.getLocalizedString("info"));
           return false; 
    }
  }else{
                dismissLoadingScreenPopup();                  
     showAlert(kony.i18n.getLocalizedString("MF_P_ERR_01005"), kony.i18n.getLocalizedString("info"));
     return false;
  }
}  //mki, mib-11469/11470 start End

function getMFSuitibilityReviewExpiryIB(expDate){
  frmIBMFSuitExpiry.lblMessage.text = kony.i18n.getLocalizedString("MF_lbl_MFSExpired");
  frmIBMFSuitExpiry.lblDateOfExpiry.text = kony.i18n.getLocalizedString("MF_lbl_ExpiredDate") + ": "+expDate;
  frmIBMFSuitExpiry.lblMessage.skin = "lblIB20pxRedNoMed";
  frmIBMFSuitExpiry.lblDateOfExpiry.skin = "lblIB20pxRedNoMed";
  frmIBMFSuitExpiry.imgMFWarning.src = "icon_failed.png";
  frmIBMFSuitExpiry.linkDoItLater.setVisibility(false);
  frmIBMFSuitExpiry.show();
  dismissLoadingScreenPopup();
}

function getMFSuitibilityReviewExpiryWarningIB(expDate){
  frmIBMFSuitExpiry.lblMessage.text = kony.i18n.getLocalizedString("MF_lbl_NearByExpired");
  frmIBMFSuitExpiry.lblDateOfExpiry.text = kony.i18n.getLocalizedString("MF_lbl_ExpiredDate") + ": "+expDate;
  frmIBMFSuitExpiry.lblMessage.skin = "lblIB20pxBlack";
  frmIBMFSuitExpiry.lblDateOfExpiry.skin = "lblIB20pxBlack";
  frmIBMFSuitExpiry.imgMFWarning.src = "icon_alert.png";
  frmIBMFSuitExpiry.linkDoItLater.setVisibility(true);
  frmIBMFSuitExpiry.show();
  dismissLoadingScreenPopup();
}

// for Terms & COnditions
function loadMFSuitabilityTermsNConditionsIB(){
  var input_param = {};
  var locale = kony.i18n.getCurrentLocale();
  if (locale == "en_US") {
    input_param["localeCd"]="en_US";
  }else{
    input_param["localeCd"]="th_TH";
  }	
  var prodDescTnC = "TMBMFSuitabilityTnC";	
  input_param["moduleKey"]= prodDescTnC;	
  showLoadingScreenPopup();	
  invokeServiceSecureAsync("readUTFFile", input_param, callBackloadMFSuitabilityTermsNConditionsIB);
}

/**
 * @function
 *
 */
function callBackloadMFSuitabilityTermsNConditionsIB(status, result){
  if(status == 400){
    if(result["opstatus"] == 0){
      frmIBMFSuitTnC.lblTnCTitle.text = kony.i18n.getLocalizedString("MF_Obj_Title");
      frmIBMFSuitTnC.lblTnCDetails.text = result["fileContent"]; // current 
      dismissLoadingScreenPopup();
      frmIBMFSuitTnC.show(); 
    }else{	
      dismissLoadingScreenPopup();
      alert(kony.i18n.getLocalizedString("ECGenericError"));
      return false;
    }
  }

}

function checkSuitabilityExpireIB(){
  // alert("checkSuitabilityExpireIB:"+GBL_MF_TEMENOS_ENABLE_FLAG);
  	showLoadingScreenPopup();
    var inputParam = {};
    if(GBL_MF_TEMENOS_ENABLE_FLAG == "OFF"){
      	inputParam["issuedIdentType"] = "CI";
      	inputParam["unitHolderNo"] = "fromAcctSummary";
      	invokeServiceSecureAsync("MFSuitabilityInq", inputParam, callBackCheckSuitabilityExpireIB);
    }else{
       invokeServiceSecureAsync("MFSuitabilityInqNew", inputParam, callBackCheckSuitabilityExpireIB);
    }
}

function callBackCheckSuitabilityExpireIB(status, resulttable) {  //mki, mib-11469/11470 start
                if (status == 400) {
      if (resulttable["opstatus"] == 0) {
        if (resulttable["responseCode"] == "001") {
            dismissLoadingScreen();
            alert(resulttable["responseDesc"]);
            return false;
        }else{
            gblMFRiskLevel = resulttable["suitabilityScore"];
            var expireDate = resulttable["suitabilityExpireDate"];
            var yyyyMMdd = expireDate.split("-");
            expireDate = yyyyMMdd[2] + "/" + yyyyMMdd[1] + "/" + yyyyMMdd[0];
            var expiryDays = getMFSuitabilityTestExpiryDays(expireDate);
            if(expiryDays == -1){
                gblIsExpiry = true;
                getMFSuitibilityReviewExpiryIB(expireDate);
            }else if(expiryDays <= 90 && gblMFSuitAlertDays.indexOf(expiryDays.toFixed(0)) != -1){
                gblIsExpiry = false;
                getMFSuitibilityReviewExpiryWarningIB(expireDate);
            }else{
                var curr_form = kony.application.getCurrentForm();
                var selectedItem = curr_form.segAccountDetails.selectedIndex[1];
                                                                var selectedData = curr_form.segAccountDetails.data[selectedItem];
                gblMFPortfolioType = "PT";  // for auto smart portfolio
              	gblUnitHolderNoSelected = "";
                frmIBMutualFundsPortfolio.btnMFFundPortflio.skin = "skinBtnBlueLeftRoundCorner";
                frmIBMutualFundsPortfolio.btnMFAutoSmartPortfolio.skin = "skinBtnWhiteRightRoundCorner";
                frmIBMutualFundsPortfolio.hBoxAutosmartPortfolio.setVisibility(false);
                frmIBMutualFundsPortfolio.hBoxNormalPortfolio.setVisibility(true);
                callMutualFundsSummary();
                frmIBMutualFundsSummary.imgProfile.src = curr_form.imgProfile.src;
                frmIBMutualFundsSummary.lblUsername.text = curr_form.lblUsername.text;
                frmIBMutualFundsSummary.lblInvestmentValue.text = selectedData["lblBalanceVal"];
            }
            dismissLoadingScreenPopup();                          
          }
      }else{
        dismissLoadingScreenPopup();
                                var curr_form = kony.application.getCurrentForm();
        var selectedItem = curr_form.segAccountDetails.selectedIndex[1];
                                var selectedData = curr_form.segAccountDetails.data[selectedItem];
        gblMFPortfolioType = "PT";  // for auto smart portfolio 
        gblUnitHolderNoSelected = "";
        frmIBMutualFundsPortfolio.btnMFFundPortflio.skin = "skinBtnBlueLeftRoundCorner";
        frmIBMutualFundsPortfolio.btnMFAutoSmartPortfolio.skin = "skinBtnWhiteRightRoundCorner";
        frmIBMutualFundsPortfolio.hBoxAutosmartPortfolio.setVisibility(false);
        frmIBMutualFundsPortfolio.hBoxNormalPortfolio.setVisibility(true);
        callMutualFundsSummary();
        frmIBMutualFundsSummary.imgProfile.src = curr_form.imgProfile.src;
        frmIBMutualFundsSummary.lblUsername.text = curr_form.lblUsername.text;
        frmIBMutualFundsSummary.lblInvestmentValue.text = selectedData["lblBalanceVal"];
          
      }
    }else{
      dismissLoadingScreenPopup();
        var curr_form = kony.application.getCurrentForm();
        var selectedItem = curr_form.segAccountDetails.selectedIndex[1];
        var selectedData = curr_form.segAccountDetails.data[selectedItem];
        gblMFPortfolioType = "PT";  // for auto smart portfolio 
      	gblUnitHolderNoSelected = "";
        frmIBMutualFundsPortfolio.btnMFFundPortflio.skin = "skinBtnBlueLeftRoundCorner";
        frmIBMutualFundsPortfolio.btnMFAutoSmartPortfolio.skin = "skinBtnWhiteRightRoundCorner";
        frmIBMutualFundsPortfolio.hBoxAutosmartPortfolio.setVisibility(false);
        frmIBMutualFundsPortfolio.hBoxNormalPortfolio.setVisibility(true);
        callMutualFundsSummary();
        frmIBMutualFundsSummary.imgProfile.src = curr_form.imgProfile.src;
        frmIBMutualFundsSummary.lblUsername.text = curr_form.lblUsername.text;
        frmIBMutualFundsSummary.lblInvestmentValue.text = selectedData["lblBalanceVal"];
    }
}  //mki, mib-11469/11470 start End

  
 