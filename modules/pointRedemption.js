function onPointRedemptionClickFromAccountDetails(curr_form){
    var selectedRow = frmAccountSummary.segAccountDetails.selectedRowItems[0];
    kony.print("onPointRedemptionClickFromAccountDetails -selectedRow>>"+selectedRow);
	var sbStr = selectedRow["accId"];
	var length = sbStr.length;
	sbStr = sbStr.substring(length - 4, length);
	var accNickName;
	if(isNotBlank(selectedRow["acctNickName"])){
		accNickName = selectedRow["acctNickName"];
	}
	else{
		if (kony.i18n.getCurrentLocale() == "en_US") {
			accNickName = selectedRow["ProductNameEng"]+" "+sbStr;
		} else {
			accNickName = selectedRow["ProductNameThai"]+" "+sbStr;
		}
	}
	curr_form.lblCardNickName.text=accNickName;
	curr_form.lblCardNumberValue.text=selectedRow["creditCardNumber"];
	curr_form.imgCredicard.src="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+selectedRow["ICON_ID"]+"&modIdentifier=PRODICON";;
	
	curr_form.lblAvailablePointsValue.text=commaFormattedPoints(selectedRow["bonusPointAvail"]);
	curr_form.lblExpireDateValue.text = commaFormattedPoints(selectedRow["bonusPointExpiryDate"]);
	curr_form.lblExpireDate.text =  kony.i18n.getLocalizedString("keyPointExpiringOn")+" "+reformatDate(selectedRow["bonusPointExpiryDate"])+":";
}

function onClickCancelPointRedemption(){
   if(kony.application.getPreviousForm().id == "frmAccountSummary"){
     showAccountSummaryNewWithRefresh();
   }else{
     showAccountDetails(); 
   }
	
	
}

function frmMBPointRedemptionProdFeaturePreShow() {
changeStatusBarColor();
	var currentLocale = kony.i18n.getCurrentLocale();
	frmMBPointRedemptionProdFeature.lblHdrTxt.text = kony.i18n.getLocalizedString('keyPointRedemption');
	frmMBPointRedemptionProdFeature.lblPointRedemptionTitle.text = kony.i18n.getLocalizedString("keyTMBRewardPlus");
		 
	if (flowSpa) {
		frmMBPointRedemptionProdFeature.btnCancelSpa.text = kony.i18n.getLocalizedString('Back')
		frmMBPointRedemptionProdFeature.btnNextSpa.text = kony.i18n.getLocalizedString('Next')
	} else {
		frmMBPointRedemptionProdFeature.btnCancel.text = kony.i18n.getLocalizedString('Back')
		frmMBPointRedemptionProdFeature.btnNext.text = kony.i18n.getLocalizedString('Next')
	}
	
	var pntRedeemTnCMsg = kony.i18n.getLocalizedString('keyPointRedmptionProdFeature');
	pntRedeemTnCMsg = pntRedeemTnCMsg.replace("{start_time}", pntRedeem_StartTime);
	pntRedeemTnCMsg = pntRedeemTnCMsg.replace("{end_time}", pntRedeem_EndTime);
	frmMBPointRedemptionProdFeature.lblPointRedemptionProdFeature.text = pntRedeemTnCMsg;
	var pointRedemptionProdFeature_image_name="PointRedemptionProdFeatureEN";
	if(currentLocale == "th_TH"){
		pointRedemptionProdFeature_image_name = "PointRedemptionProdFeatureTH";
	}	
	var pointRedemptionProdFeature_image="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+pointRedemptionProdFeature_image_name+"&modIdentifier=PRODUCTPACKAGEIMG";
		
	frmMBPointRedemptionProdFeature.imgPointRedemptionProdFeature.src = pointRedemptionProdFeature_image;
	frmMBPointRedemptionProdFeature.imgPointRedemptionProdFeature.setVisibility(true);
}

function frmMBPointRedemptionTnCPreShow() {
	changeStatusBarColor();
	var currentLocale = kony.i18n.getCurrentLocale();
	frmMBPointRedemptionTnC.lblHdrTxt.text = kony.i18n.getLocalizedString('keyPointRedemption');
		 
	if (flowSpa) {
		frmMBPointRedemptionTnC.btnCancelSpa.text = kony.i18n.getLocalizedString('Back')
		frmMBPointRedemptionTnC.btnAgreeSpa.text = kony.i18n.getLocalizedString('keyAgreeButton')
	} else {
		frmMBPointRedemptionTnC.btnPointCancel.text = kony.i18n.getLocalizedString('Back')
		frmMBPointRedemptionTnC.btnAgree.text = kony.i18n.getLocalizedString('keyAgreeButton')
	}
	
	var input_param = {};
	input_param["localeCd"]=currentLocale;
	input_param["moduleKey"]='TMBPointRedemption';
	showLoadingScreen();
    invokeServiceSecureAsync("readUTFFile", input_param, setPointRedemptionTnC);	
}

function setPointRedemptionTnC(status,result){
	if(status==400){
		dismissLoadingScreen();
		if(result["opstatus"]==0){
			frmMBPointRedemptionTnC.richTandC.text="";
			frmMBPointRedemptionTnC.richTandC.text=result["fileContent"];
		}else{
			showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
			return false;
		}
	}
}

function onClickEmailTnCMBPointRedemption() {
	
	showLoadingScreen()
	var inputparam = {};
	inputparam["channelName"] = "Mobile Banking";
	inputparam["channelID"] = "02";
	inputparam["notificationType"] = "Email"; // always email
	inputparam["phoneNumber"] = gblPHONENUMBER;
	inputparam["mail"] = gblEmailId;
	inputparam["customerName"] = gblCustomerName;
	inputparam["localeCd"] = kony.i18n.getCurrentLocale();
    inputparam["moduleKey"] = "PointRedemption";
    //if (kony.i18n.getCurrentLocale() == "en_US") {
	inputparam["productName"] = "";
	//} else {
	inputparam["productNameTH"] = "";
	//}
	invokeServiceSecureAsync("TCEMailService", inputparam, callBackEmailTnCPointRedemptionMB);
	
}

function callBackEmailTnCPointRedemptionMB(status, result) {
	if (status == 400) {
		if (result["opstatus"] == 0) {
			var StatusCode = result["StatusCode"];
			var Severity = result["Severity"];
			var StatusDesc = result["StatusDesc"];
			if (StatusCode == 0) {
				showAlert(kony.i18n.getLocalizedString("keytermOpenAcnt"), kony.i18n.getLocalizedString("info"));
				dismissLoadingScreen();
			} else {
				dismissLoadingScreen()
				return false;
			}
		} else {
			dismissLoadingScreen()
		}
	}
}

function onCLickNextPointsTnC(){
	//Resetting rewards name and common box
	frmMBPointRedemptionLanding.hbxRewardsName.isVisible=false;
	frmMBPointRedemptionLanding.hbxCommonRewardsEntry.isVisible=false;
	frmMBPointRedemptionLanding.txtPointsBeingRedeemed.text="";
	frmMBPointRedemptionLanding.txtRemainingPoints.text="";
	frmMBPointRedemptionLanding.txtAmntCashbackRopMile.text="";
	frmMBPointRedemptionLanding.hbxAmntCashbackRopMile.isVisible=false;
	frmMBPointRedemptionLanding.btnNext.skin="btnDisabledGray";
	frmMBPointRedemptionLanding.btnNext.focusSkin="btnDisabledGray";
	frmMBPointRedemptionLanding.btnNext.setEnabled(false);
	//End rewards name and common box reset

	//Resetting cashback box
	frmMBPointRedemptionLanding.hbxCashBack.isVisible=false;
	frmMBPointRedemptionLanding.lblTranLandToName.text="";
	frmMBPointRedemptionLanding.lblTranLandToNum.text="";
	frmMBPointRedemptionLanding.imgCashbackTo.src="avatar.png";
	//End cashback reset
	
	//Resetting Air asia , thai airways box
	frmMBPointRedemptionLanding.txtMemberNumber.text="";
	frmMBPointRedemptionLanding.txtLastName.text="";
	frmMBPointRedemptionLanding.hbxMemberNoLastName.isVisible=false;
	//End Air asia , thai airways reset
	
	frmMBPointRedemptionLanding.show();
}

function frmMBPointRedemptionLandingPreShow(){
	changeStatusBarColor();
	onPointRedemptionClickFromAccountDetails(frmMBPointRedemptionLanding);
	frmMBPointRedemptionLanding.lblHdrTxt.text=kony.i18n.getLocalizedString("keyPointRedemption");
	frmMBPointRedemptionLanding.lblCardNumber.text=kony.i18n.getLocalizedString("CardNo");
	frmMBPointRedemptionLanding.lblAvailablePoints.text=kony.i18n.getLocalizedString("keyMBAvailableR");
	frmMBPointRedemptionLanding.lblRewards.text=kony.i18n.getLocalizedString("keyChooseYourReward")+":";
	frmMBPointRedemptionLanding.btnCancel.text=kony.i18n.getLocalizedString("keyCancelButton");
	frmMBPointRedemptionLanding.btnNext.text=kony.i18n.getLocalizedString("Next");
	
	if(frmMBPointRedemptionLanding.hbxCashBack.isVisible){
		frmMBPointRedemptionLanding.lblCashbackTo.text=kony.i18n.getLocalizedString("keyCashbackToAccount");
		frmMBPointRedemptionLanding.lblAmntCashbackRopMile.text=kony.i18n.getLocalizedString("keyTotalCashbackAmount");
		cashbackAccountLanguageToggle(frmMBPointRedemptionLanding);
	}
	if(frmMBPointRedemptionLanding.hbxCommonRewardsEntry.isVisible){
		frmMBPointRedemptionLanding.lblPointsBeingRedeemed.text=kony.i18n.getLocalizedString("keyPointsbeingredeemed");
		frmMBPointRedemptionLanding.lblRemainingPoints.text=kony.i18n.getLocalizedString("keyRemainingPoints");
		
	}
	if(frmMBPointRedemptionLanding.hbxMemberNoLastName.isVisible){
		frmMBPointRedemptionLanding.lblMemberNumber.text=kony.i18n.getLocalizedString("keyRewardsMemberNo");
		frmMBPointRedemptionLanding.lblLastName.text=kony.i18n.getLocalizedString("keyLastName");
		var rewardType = identifyRewardType();
		if(rewardType == "AirAsia"){
			frmMBPointRedemptionLanding.lblAmntCashbackRopMile.text=kony.i18n.getLocalizedString("keyReceivedBigPoint");
		}else if(rewardType == "ROP"){
			frmMBPointRedemptionLanding.lblAmntCashbackRopMile.text=kony.i18n.getLocalizedString("keyReceivedROPMile");
		}else if(rewardType == "OtherMile"){
			frmMBPointRedemptionLanding.lblAmntCashbackRopMile.text=kony.i18n.getLocalizedString("keyReceivedMile");
		}		
		frmMBPointRedemptionLanding.txtLastName.placeholder=kony.i18n.getLocalizedString("keyLastNameFirstFiveChars");
	}
	rewardSelectedLanguageToggle(frmMBPointRedemptionLanding);
	
}

function loadPointRedemptionOptions(){
	var inputParam = {};
	if(isNotBlankObject(gblrewardsData)){
		reloadRewardsList();	
	}	
	else{
		showLoadingScreen();
		invokeServiceSecureAsync("getCardRewards", inputParam, loadPointRedemptionOptionsCallBack);
	}		
}

function loadPointRedemptionOptionsCallBack(status, resulttable){
	if(status == 400){
		dismissLoadingScreen();
		if(resulttable["opstatus"] == "0"){
			if(resulttable["rewardsDataSet"].length > 0){
				gblrewardsData=[];
				gblrewardsData = resulttable;
				reloadRewardsList();
			}	
		}else{
			showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
		}	
	}	
}

function reloadRewardsList(){
	if(gblrewardsData["rewardsDataSet"].length > 0){
		rewardsDataSegment=[];
		for(i=0;i<gblrewardsData["rewardsDataSet"].length;i++){
			var pointRedemptionRewards_image_name=gblrewardsData["rewardsDataSet"][i]["logoImageName"];
			var pointRedemptionRewards_image="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+pointRedemptionRewards_image_name+"&modIdentifier=PRODUCTPACKAGEIMG";
			var rewardsName = "";
			if(kony.i18n.getCurrentLocale() == "th_TH"){
				rewardsName = gblrewardsData["rewardsDataSet"][i]["rewardNameTh"];
			}else{
				rewardsName = gblrewardsData["rewardsDataSet"][i]["rewardNameEn"];
			}	
			var rewardsRate = commaFormattedPoints(gblrewardsData["rewardsDataSet"][i]["rewardPoint"])+" "+kony.i18n.getLocalizedString("keyMBPoints")+" = "+commaFormattedPoints(gblrewardsData["rewardsDataSet"][i]["rewardUnit"])+" "+kony.i18n.getLocalizedString(gblrewardsData["rewardsDataSet"][i]["tphraseRewardUnitKey"]);
			
			
			var tempRewardRecord = {
				"imgRewardsLogo":pointRedemptionRewards_image,
				"lblRewardsName":rewardsName,
				"lblRewardsRate":rewardsRate,
				"hiddenRewardCode":gblrewardsData["rewardsDataSet"][i]["rewardCode"]
			}
			rewardsDataSegment.push(tempRewardRecord);
		}
		frmMBPointRedemptionRewardsList.segRewardsList.setData(rewardsDataSegment);
		frmMBPointRedemptionRewardsList.show();
	}
}

function identifyRewardType(){
	var rewardType = gblrewardsData["rewardsDataSet"][gblSelectedReward]["rewardType"];
	var rewardName = gblrewardsData["rewardsDataSet"][gblSelectedReward]["rewardNameEn"];
	if(rewardType == "01"){
		if(rewardName.match(/Air Asia/gi) || rewardName.match(/Big Point/gi)){
			return "AirAsia";
		}
		if(rewardName.match(/ROYAL ORCHID/gi) || rewardName.match(/ROP/gi)){
			return "ROP";
		}
		return "OtherMile";
	}else if(rewardType == "00"){
		return "Cashback";
	}
	return "NotRecognisedRewardType";	
}

function frmMBPointRedemptionRewardsListPreShow(){
changeStatusBarColor();
	frmMBPointRedemptionRewardsList.lblHdrTxt.text = kony.i18n.getLocalizedString("keyChooseYourReward");
	frmMBPointRedemptionRewardsList.lblAvailableRewards.text = kony.i18n.getLocalizedString("keyAvailableRewardsForOnlineRedemption");
	frmMBPointRedemptionRewardsList.richTxtChooseDifferentReward.text = kony.i18n.getLocalizedString("keyChooseDifferentReward");
}

function onSelectingReward(){
	gblSelectedToAccount=[];
	gblSelectedReward = frmMBPointRedemptionRewardsList.segRewardsList.selectedIndex[1];
	frmMBPointRedemptionLanding.lblRewardsName.text=frmMBPointRedemptionRewardsList.segRewardsList.data[gblSelectedReward]["lblRewardsName"];
	frmMBPointRedemptionLanding.lblRewardsRate.text=frmMBPointRedemptionRewardsList.segRewardsList.data[gblSelectedReward]["lblRewardsRate"];
	frmMBPointRedemptionLanding.imgRewardSelected.src=frmMBPointRedemptionRewardsList.segRewardsList.data[gblSelectedReward]["imgRewardsLogo"];
	frmMBPointRedemptionLanding.hbxRewardsName.isVisible=true;
	
	frmMBPointRedemptionConfirmation.lblRewardsName.text=frmMBPointRedemptionRewardsList.segRewardsList.data[gblSelectedReward]["lblRewardsName"];
	frmMBPointRedemptionConfirmation.lblRewardsRate.text=frmMBPointRedemptionRewardsList.segRewardsList.data[gblSelectedReward]["lblRewardsRate"];
	frmMBPointRedemptionConfirmation.imgRewardSelected.src=frmMBPointRedemptionRewardsList.segRewardsList.data[gblSelectedReward]["imgRewardsLogo"];
	
	frmMBPointRedemptionComplete.lblRewardsName.text=frmMBPointRedemptionRewardsList.segRewardsList.data[gblSelectedReward]["lblRewardsName"];
	frmMBPointRedemptionComplete.lblRewardsRate.text=frmMBPointRedemptionRewardsList.segRewardsList.data[gblSelectedReward]["lblRewardsRate"];
	frmMBPointRedemptionComplete.imgRewardSelected.src=frmMBPointRedemptionRewardsList.segRewardsList.data[gblSelectedReward]["imgRewardsLogo"];
	
	frmMBPointRedemptionLanding.hbxCommonRewardsEntry.isVisible=true;
	frmMBPointRedemptionLanding.hbxAmntCashbackRopMile.isVisible=true;
	frmMBPointRedemptionLanding.txtAmntCashbackRopMile.setEnabled(false);
	frmMBPointRedemptionLanding.txtRemainingPoints.setEnabled(false);
	
	frmMBPointRedemptionLanding.txtLastName.text="";
	frmMBPointRedemptionLanding.txtMemberNumber.text="";
	frmMBPointRedemptionLanding.txtPointsBeingRedeemed.text="";
	frmMBPointRedemptionLanding.txtRemainingPoints.text="";
	frmMBPointRedemptionLanding.txtAmntCashbackRopMile.text="";
	frmMBPointRedemptionLanding.lblTranLandToName.text="";
	frmMBPointRedemptionLanding.lblTranLandToNum.text="";
	frmMBPointRedemptionLanding.imgCashbackTo.src="avatar.png";
	frmMBPointRedemptionLanding.btnNext.skin="btnDisabledGray";
	frmMBPointRedemptionLanding.btnNext.focusSkin="btnDisabledGray";
	frmMBPointRedemptionLanding.btnNext.setEnabled(false);
	
	var rewardType = identifyRewardType();
	
	if(rewardType == "Cashback"){
		//Cashback Related Begin
		frmMBPointRedemptionLanding.hbxCashBack.isVisible=true;
		frmMBPointRedemptionLanding.hbxMemberNoLastName.isVisible=false;
		frmMBPointRedemptionLanding.lblAmntCashbackRopMile.text = kony.i18n.getLocalizedString("keyTotalCashbackAmount");
		//Cashback Related End
	}else if(rewardType == "AirAsia"){
		//AirAsia Related Begin
		frmMBPointRedemptionLanding.hbxMemberNoLastName.isVisible=true;
		frmMBPointRedemptionLanding.txtMemberNumber.maxTextLength=10;
		frmMBPointRedemptionLanding.txtMemberNumber.textInputMode=constants.TEXTBOX_INPUT_MODE_NUMERIC;
		frmMBPointRedemptionLanding.hbxCashBack.isVisible=false;
		frmMBPointRedemptionLanding.lblAmntCashbackRopMile.text = kony.i18n.getLocalizedString("keyReceivedBigPoint");
		//AirAsia Related End
	}else if(rewardType == "ROP"){
		//Thai Airways Related Begin
		frmMBPointRedemptionLanding.hbxMemberNoLastName.isVisible=true;
		frmMBPointRedemptionLanding.txtMemberNumber.maxTextLength=7;
		frmMBPointRedemptionLanding.txtMemberNumber.textInputMode=constants.TEXTBOX_INPUT_MODE_ANY;
		frmMBPointRedemptionLanding.hbxCashBack.isVisible=false;
		frmMBPointRedemptionLanding.lblAmntCashbackRopMile.text = kony.i18n.getLocalizedString("keyReceivedROPMile");
		//Thai Airways Related End
	}else if(rewardType == "OtherMile"){
		//Other Airways Related Begin
		frmMBPointRedemptionLanding.hbxMemberNoLastName.isVisible=true;
		frmMBPointRedemptionLanding.txtMemberNumber.maxTextLength=10;
		frmMBPointRedemptionLanding.txtMemberNumber.textInputMode=constants.TEXTBOX_INPUT_MODE_ANY;
		frmMBPointRedemptionLanding.hbxCashBack.isVisible=false;
		frmMBPointRedemptionLanding.lblAmntCashbackRopMile.text = kony.i18n.getLocalizedString("keyReceivedMile");
		//Other Airways Related End
	}else{
		alert("Sorry, the selected Reward can not be redemmed online, please contact nearest TMB Branch");
		return false;
	}
	frmMBPointRedemptionLanding.show();
}

function rewardSelectedLanguageToggle(curr_form){
	if(gblSelectedReward != -1){
			curr_form.lblRewardsRate.text = commaFormattedPoints(gblrewardsData["rewardsDataSet"][gblSelectedReward]["rewardPoint"])+" "+
										   kony.i18n.getLocalizedString("keyMBPoints")+" = "+
										   commaFormattedPoints(gblrewardsData["rewardsDataSet"][gblSelectedReward]["rewardUnit"])+" "+
										   kony.i18n.getLocalizedString(gblrewardsData["rewardsDataSet"][gblSelectedReward]["tphraseRewardUnitKey"]);
				if(kony.i18n.getCurrentLocale() == "th_TH"){
					curr_form.lblRewardsName.text=gblrewardsData["rewardsDataSet"][gblSelectedReward]["rewardNameTh"];
				}else{
					curr_form.lblRewardsName.text=gblrewardsData["rewardsDataSet"][gblSelectedReward]["rewardNameEn"];
				}	
		}		
}

function onClickToAccountForCashback(){
	
	gblAccountsToCashback=[];
	
	for(var i=0;i<gblAccountTable["custAcctRec"].length; i++){
	
	var accountName;
	var accountNameEN;
	var accountNameTH;
	var accountNo;
	var accountNoTenDigit;
	var accountNoUnformatted;
	var isTMBAccount = gblAccountTable["custAcctRec"][i]["receiveCashBackEligible"];
		
		if(parseInt(isTMBAccount)){
		
			if ((gblAccountTable["custAcctRec"][i]["acctNickName"]) == null || (gblAccountTable["custAcctRec"][i]["acctNickName"]) == '') {
				var sbStr = gblAccountTable["custAcctRec"][i]["accId"];
				var length = sbStr.length;
				sbStr = sbStr.substring(length - 4, length);
				if (kony.i18n.getCurrentLocale() == "th_TH")
						accountName = gblAccountTable["custAcctRec"][i]["ProductNameThai"]+ " " + sbStr;
				else
						accountName = gblAccountTable["custAcctRec"][i]["ProductNameEng"]+ " " + sbStr;
				//Used for language toggle and PDF
				accountNameEN = gblAccountTable["custAcctRec"][i]["ProductNameEng"]+ " " + sbStr;
				accountNameTH = gblAccountTable["custAcctRec"][i]["ProductNameThai"]+ " " + sbStr;
				
			}else {
				accountName = gblAccountTable["custAcctRec"][i]["acctNickName"];
				accountNameEN = gblAccountTable["custAcctRec"][i]["acctNickName"];
				accountNameTH = gblAccountTable["custAcctRec"][i]["acctNickName"];
			}
			
			if (gblAccountTable["custAcctRec"][i]["accType"] == kony.i18n.getLocalizedString("CreditCard")){
				accountNo = gblAccountTable["custAcctRec"][i]["accountNoFomatted"];
			}else{
				accountNoUnformatted = gblAccountTable["custAcctRec"][i]["accId"];
				accountNoTenDigit = accountNoUnformatted.substring(accountNoUnformatted.length - 10,accountNoUnformatted.length);
				accountNo = formatAccountNo(accountNoTenDigit);
			}
		
			imageName="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+gblAccountTable["custAcctRec"][i]["ICON_ID"]+"&modIdentifier=PRODICON";
			
			var tempToCashbackAccount={
				"imgAccountType":imageName,
				"lblAccountName":accountName,
				"lblAccountNumber":accountNo,
				"lblBankName":"TMB",
				"lblAccountNameEN":accountNameEN,
				"lblAccountNameTH":accountNameTH,
				"hiddenAccountId":gblAccountTable["custAcctRec"][i]["accId"],
				"accountType":gblAccountTable["custAcctRec"][i]["accType"]
			}

		gblAccountsToCashback.push(tempToCashbackAccount);
		}
	}
	
	gblAccountsToCashback = sortToAccountForCashback(gblAccountsToCashback, frmMBPointRedemptionLanding);
	frmMBPointRedemptionToAccounts.segAccountList.setData(gblAccountsToCashback);
	frmMBPointRedemptionToAccounts.txbXferSearch.text="";
	frmMBPointRedemptionToAccounts.show();

}

function frmMBPointRedemptionToAccountsPreShow(){
	if(gblAccountsToCashback.length == 0){
		frmMBPointRedemptionToAccounts.lblMsg.text = kony.i18n.getLocalizedString("keyResultsNotFoundMB");
		frmMBPointRedemptionToAccounts.lblMsg.isVisible=true;	
	}else{
		frmMBPointRedemptionToAccounts.lblMsg.isVisible=false;
	}
	frmMBPointRedemptionToAccounts.txbXferSearch.placeholder = kony.i18n.getLocalizedString("keySearch");
	frmMBPointRedemptionToAccounts.lblHdrTxt.text = kony.i18n.getLocalizedString("keySelectAccount");
}

function searchToAccountsRewardsMB(fromWidgetName) {
	var manualSearch = false;
	if(fromWidgetName == "btnSearchCashBackAccounts"){
		manualSearch=true;
	}
	var searchText = frmMBPointRedemptionToAccounts.txbXferSearch.text;
	searchText = searchText.toLowerCase();
	
	var searchList = searchToAccountsRewardsLogic(searchText,manualSearch);
	
	frmMBPointRedemptionToAccounts.segAccountList.setData(searchList);
	
	if(searchList.length <= 0) {
		frmMBPointRedemptionToAccounts.lblMsg.isVisible=true;
	} else {
		frmMBPointRedemptionToAccounts.lblMsg.isVisible=false;
	}
}

function searchToAccountsRewardsLogic(searchText,manualSearch) {
	var rewardsToAccountSegData=gblAccountsToCashback;
	var searchList = [];
	var rewardsToAccountTxnLength = rewardsToAccountSegData.length;
	var lblAccountName = "";
	var lblAccountNumber = "";
	var lblBankName = "";
	
	if (rewardsToAccountTxnLength > 0) { // Atleast one transaction should be avilable
		if (searchText.length >= 3 || manualSearch == true) { //Atleast 3 characters should be entered in search box or search button should be clicked
			for (j = 0, i = 0; i < rewardsToAccountTxnLength; i++) {
				
				lblAccountName = rewardsToAccountSegData[i].lblAccountName;
				lblAccountNumber = rewardsToAccountSegData[i].lblAccountNumber;
				lblBankName = rewardsToAccountSegData[i].lblBankName;
				if (lblAccountName.toLowerCase().indexOf(searchText) > -1
					|| lblAccountNumber.indexOf(searchText) > -1
					|| lblBankName.toLowerCase().indexOf(searchText) > -1) {
					searchList[j] = rewardsToAccountSegData[i];
					j++;
				}
			}//For Loop End
		} else {//searchText condition End
			//If you Clear the Search Text then show All Transactions
			searchList = rewardsToAccountSegData;
		}
	}//rewardsToAccountTxnLength condition End
	
	return searchList;
}


function onSelectToAccountForCashback(){
	gblSelectedToAccount = frmMBPointRedemptionToAccounts.segAccountList.data[frmMBPointRedemptionToAccounts.segAccountList.selectedIndex[1]];
	frmMBPointRedemptionLanding.lblTranLandToName.text=gblSelectedToAccount["lblAccountName"];
	frmMBPointRedemptionLanding.lblTranLandToNum.text=gblSelectedToAccount["lblAccountNumber"];
	frmMBPointRedemptionLanding.imgCashbackTo.src=gblSelectedToAccount["imgAccountType"];
	
	frmMBPointRedemptionConfirmation.lblTranLandToName.text=gblSelectedToAccount["lblAccountName"];
	frmMBPointRedemptionConfirmation.lblTranLandToNum.text=gblSelectedToAccount["lblAccountNumber"];
	frmMBPointRedemptionConfirmation.imgCashbackTo.src=gblSelectedToAccount["imgAccountType"];
	
	frmMBPointRedemptionComplete.lblTranLandToName.text=gblSelectedToAccount["lblAccountName"];
	frmMBPointRedemptionComplete.lblTranLandToNum.text=gblSelectedToAccount["lblAccountNumber"];
	frmMBPointRedemptionComplete.imgCashbackTo.src=gblSelectedToAccount["imgAccountType"];
	checkNextEnableFromRewardsLanding();
	frmMBPointRedemptionLanding.show();
}

function cashbackAccountLanguageToggle(curr_form){
	if(gblSelectedToAccount != "[]"){
		if(kony.i18n.getCurrentLocale() == "en_US"){
			curr_form.lblTranLandToName.text=gblSelectedToAccount["lblAccountNameEN"];
		}else{
			curr_form.lblTranLandToName.text=gblSelectedToAccount["lblAccountNameTH"];
		}
	}	
}

function checkNextEnableFromRewardsLanding(){
	var all_conditions = 0;
	var passed_conditions = 0;
	if(frmMBPointRedemptionLanding.hbxMemberNoLastName.isVisible){
		var lastName = frmMBPointRedemptionLanding.txtLastName.text;
		all_conditions += (lastName.length > 0 && lastName.match(/^[a-zA-Z]+$/) != undefined)? 1 : 0; passed_conditions += 1;
		var memberNumber = frmMBPointRedemptionLanding.txtMemberNumber.text;
		if(identifyRewardType() == "AirAsia"){
			all_conditions += (memberNumber.length >0 && memberNumber.match(/[0-9]{10}/) != undefined && memberNumber.length == 10)? 1 : 0; passed_conditions += 1;
		}else if(identifyRewardType() == "ROP")	{
			all_conditions += (memberNumber.length ==7 && memberNumber.match(/\b[a-zA-Z]{2}[0-9]{5}\b/) != undefined)? 1 : 0; passed_conditions += 1;
		}else{
			all_conditions += (memberNumber.length >0)? 1 : 0; passed_conditions += 1;
		}
	}
	if(frmMBPointRedemptionLanding.hbxCashBack.isVisible){
		all_conditions += (frmMBPointRedemptionLanding.lblTranLandToName.text !="")? 1 : 0; passed_conditions += 1;
		all_conditions += (frmMBPointRedemptionLanding.lblTranLandToNum.text !="")? 1 : 0; passed_conditions += 1;
	}	
	all_conditions += (frmMBPointRedemptionLanding.txtPointsBeingRedeemed.text!="")? 1 : 0; passed_conditions += 1;
	all_conditions += (frmMBPointRedemptionLanding.txtRemainingPoints.text!="")? 1 : 0; passed_conditions += 1;
	all_conditions += (frmMBPointRedemptionLanding.txtAmntCashbackRopMile.text !="")? 1 : 0; passed_conditions += 1;

	if(all_conditions == passed_conditions) {
		frmMBPointRedemptionLanding.btnNext.skin="btnBlueSkin";
		frmMBPointRedemptionLanding.btnNext.focusSkin="btnBlueSkin";
		frmMBPointRedemptionLanding.btnNext.setEnabled(true);
	}else{
		frmMBPointRedemptionLanding.btnNext.skin="btnDisabledGray";
		frmMBPointRedemptionLanding.btnNext.focusSkin="btnDisabledGray";
		frmMBPointRedemptionLanding.btnNext.setEnabled(false);
	}
}

function calculateRemainingPoints(){
	var minimumPoints=Number(gblrewardsData["rewardsDataSet"][gblSelectedReward]["rewardPoint"]);
	var rewardsMinimum=Number(gblrewardsData["rewardsDataSet"][gblSelectedReward]["rewardUnit"]);
	var total_points = frmMBPointRedemptionLanding.lblAvailablePointsValue.text;
	total_points = total_points.replace(/[a-z, ]/gi,"");
	total_points = Number(total_points);
	var points_being_redemmed = Number(frmMBPointRedemptionLanding.txtPointsBeingRedeemed.text);
	if(total_points >= points_being_redemmed){
		var remaining_points = total_points-points_being_redemmed;
		if(remaining_points == 0)
			frmMBPointRedemptionLanding.txtRemainingPoints.text = "0";
		else
			frmMBPointRedemptionLanding.txtRemainingPoints.text = commaFormattedPoints(remaining_points);	
		if(points_being_redemmed >0 && points_being_redemmed%minimumPoints == 0){
			if(identifyRewardType() == "Cashback"){
				frmMBPointRedemptionLanding.txtAmntCashbackRopMile.text=commaFormattedPoints(points_being_redemmed/minimumPoints*rewardsMinimum)+" "+kony.i18n.getLocalizedString("currencyThaiBaht");
			}else{
				frmMBPointRedemptionLanding.txtAmntCashbackRopMile.text=commaFormattedPoints(points_being_redemmed/minimumPoints*rewardsMinimum);
			}	
		}else{
			frmMBPointRedemptionLanding.txtAmntCashbackRopMile.text = "";
			frmMBPointRedemptionLanding.btnNext.skin="btnDisabledGray";
			frmMBPointRedemptionLanding.btnNext.focusSkin="btnDisabledGray";
			frmMBPointRedemptionLanding.btnNext.setEnabled(false);
		}
	}else{
		frmMBPointRedemptionLanding.txtRemainingPoints.text = "";
		frmMBPointRedemptionLanding.txtAmntCashbackRopMile.text = "";
		frmMBPointRedemptionLanding.btnNext.skin="btnDisabledGray";
		frmMBPointRedemptionLanding.btnNext.focusSkin="btnDisabledGray";
		frmMBPointRedemptionLanding.btnNext.setEnabled(false);
	}
}

function checkRedeemPointsMoreAvailablePoints(){
	var total_points = frmMBPointRedemptionLanding.lblAvailablePointsValue.text;
	total_points = total_points.replace(/[a-z, ]/gi,"");
	total_points = Number(total_points);
	var points_being_redemmed = Number(frmMBPointRedemptionLanding.txtPointsBeingRedeemed.text);
	if(total_points < points_being_redemmed){
		showAlertWithCallBack(kony.i18n.getLocalizedString("keyRewardsErrorPointNotEnough"),kony.i18n.getLocalizedString("info"),putFocusOnPointsBeingRedemmedTextbox);
	}else if(frmMBPointRedemptionLanding.txtPointsBeingRedeemed.text.length > 0 && frmMBPointRedemptionLanding.txtAmntCashbackRopMile.text.length == 0){
		var alert_message =  kony.i18n.getLocalizedString("keyUsedPointNotMatchPointCriteria");
		var x = commaFormattedPoints(gblrewardsData["rewardsDataSet"][gblSelectedReward]["rewardPoint"]);
		var y = commaFormattedPoints(Number(gblrewardsData["rewardsDataSet"][gblSelectedReward]["rewardPoint"])*2);
		alert_message = alert_message.replace(/<X>/g, x);
		alert_message = alert_message.replace(/<Y>/g, y);
		showAlertWithCallBack(alert_message, kony.i18n.getLocalizedString("info"), putFocusOnPointsBeingRedemmedTextbox)
	}	
}

function putFocusOnPointsBeingRedemmedTextbox(){
	frmMBPointRedemptionLanding.txtPointsBeingRedeemed.setFocus(true);
}

function checkMinMemberNumberLength(){
	var memberNumber = frmMBPointRedemptionLanding.txtMemberNumber.text;
	if(memberNumber.length > 0){
		if(identifyRewardType() == "AirAsia" && (memberNumber.length < 10 || memberNumber.match(/[0-9]{10}/) == undefined)){
			showAlertWithCallBack(kony.i18n.getLocalizedString("keyRewardsErrorMemberNoAirAsia"),kony.i18n.getLocalizedString("info"),putFocusOnMemberNumberTextbox);
		}
		if(identifyRewardType()== "ROP" && (memberNumber.length != 7 || memberNumber.match(/\b[a-zA-Z]{2}[0-9]{5}\b/) == undefined)){
			showAlertWithCallBack(kony.i18n.getLocalizedString("keyRewardsErrorMemberNoROP"),kony.i18n.getLocalizedString("info"),putFocusOnMemberNumberTextbox);
		}
	}	
}

function putFocusOnMemberNumberTextbox(){
	frmMBPointRedemptionLanding.txtMemberNumber.setFocus(true);
}

function checkMinLastNameLength(){
	var lastName = frmMBPointRedemptionLanding.txtLastName.text;
	if(lastName.length >0 && lastName.match(/^[a-zA-Z]+$/) == undefined){
		showAlertWithCallBack(kony.i18n.getLocalizedString("keyRewardsErrorLastName"), kony.i18n.getLocalizedString("info"), putFocusOnLastNameTextbox)
	}
}

function putFocusOnLastNameTextbox(){
	frmMBPointRedemptionLanding.txtLastName.setFocus(true);
}

function saveRedeemRewardsInSession(){
	showLoadingScreen();
	var inputParam = {};
	var sbStr = gblAccountTable["custAcctRec"][gblIndex]["accId"];
	var length = sbStr.length;
	sbStr = sbStr.substring(length - 4, length);
	var accNickName;
	if(isNotBlank(gblAccountTable["custAcctRec"][gblIndex]["acctNickName"])){
		inputParam["cardNicknameEN"] = gblAccountTable["custAcctRec"][gblIndex]["acctNickName"];
		inputParam["cardNicknameTH"] = gblAccountTable["custAcctRec"][gblIndex]["acctNickName"];
	}
	else{
		inputParam["cardNicknameEN"] = gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"]+" "+sbStr;
		inputParam["cardNicknameTH"] = gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"]+" "+sbStr;
	}

    inputParam["cardAccId"] = gblAccountTable["custAcctRec"][gblIndex]["accId"];
    inputParam["cardNumber"] = frmMBPointRedemptionLanding.lblCardNumberValue.text;
    inputParam["totalAvailablePoints"] = replaceCommon(frmMBPointRedemptionLanding.lblAvailablePointsValue.text, ",", "");
    inputParam["expiryDate"] = reformatDate(gblAccountTable["custAcctRec"][gblIndex]["bonusPointExpiryDate"]);
    inputParam["pointsExpiringSoon"] = replaceCommon(frmMBPointRedemptionLanding.lblExpireDateValue.text, ",", "");
    
    inputParam["selectedRewardCode"] = gblrewardsData["rewardsDataSet"][gblSelectedReward]["rewardCode"];
    inputParam["selectedRewardPoint"] = gblrewardsData["rewardsDataSet"][gblSelectedReward]["rewardPoint"];
    inputParam["selectedRewardUnit"] = gblrewardsData["rewardsDataSet"][gblSelectedReward]["rewardUnit"];
    inputParam["selectedRewardType"] = identifyRewardType();
    inputParam["selectedRewardNameEN"] = gblrewardsData["rewardsDataSet"][gblSelectedReward]["rewardNameEn"];
    inputParam["selectedRewardNameTH"] = gblrewardsData["rewardsDataSet"][gblSelectedReward]["rewardNameTh"];
    
    var rewardType = identifyRewardType();
    
    if(rewardType == "AirAsia" || rewardType == "ROP" || rewardType == "OtherMile"){
    	inputParam["lastName"] = frmMBPointRedemptionLanding.txtLastName.text;
    	inputParam["memberNumber"] = frmMBPointRedemptionLanding.txtMemberNumber.text;
    }	
    
    if(rewardType == "Cashback"){
	    inputParam["transferToAccount"] = frmMBPointRedemptionLanding.lblTranLandToNum.text;
	    inputParam["transferToAccountNickname"] = frmMBPointRedemptionLanding.lblTranLandToName.text;
	    inputParam["transferToAccountNicknameEN"] = gblSelectedToAccount["lblAccountNameEN"];
	    inputParam["transferToAccountNicknameTH"] = gblSelectedToAccount["lblAccountNameTH"];
	    inputParam["transferToAccountId"] = gblSelectedToAccount["hiddenAccountId"];
	}    
    
    inputParam["pointsBeingRedeemed"] = replaceCommon(frmMBPointRedemptionLanding.txtPointsBeingRedeemed.text, ",", "");
    inputParam["remainingPoints"] = replaceCommon(frmMBPointRedemptionLanding.txtRemainingPoints.text, ",", "");
    inputParam["amntCashbackRopMile"] = parseInt(replaceCommon(frmMBPointRedemptionLanding.txtAmntCashbackRopMile.text, ",", ""));
    
    invokeServiceSecureAsync("saveRedeemPointsTxn", inputParam, saveRedeemRewardsInSessionCallbackfunction);
}

function saveRedeemRewardsInSessionCallbackfunction(status, resulttable){
	if (status == 400) {
		dismissLoadingScreen();
		if (resulttable["opstatus"] == "0") {
			//Show the Confirm Form
			onClickNextFromRedemptionLanding();
		}
		else{
			showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
			return false;
		}
	}	
}

function onClickNextFromRedemptionLanding(){
	var txtRedeemPoints = replaceCommon(frmMBPointRedemptionLanding.txtPointsBeingRedeemed.text,",","")
	frmMBPointRedemptionConfirmation.lblPointsBeingRedeemedValue.text = commaFormattedPoints(Number(txtRedeemPoints));
	frmMBPointRedemptionConfirmation.lblRemainingPointsValue.text = frmMBPointRedemptionLanding.txtRemainingPoints.text;
	frmMBPointRedemptionConfirmation.lblAmntCashbackRopMileValue.text = frmMBPointRedemptionLanding.txtAmntCashbackRopMile.text;
	
	if(frmMBPointRedemptionLanding.hbxMemberNoLastName.isVisible){
		frmMBPointRedemptionConfirmation.hbxMemberNoLastName.isVisible=true;
		frmMBPointRedemptionConfirmation.lblLastNameValue.text = frmMBPointRedemptionLanding.txtLastName.text;
		frmMBPointRedemptionConfirmation.lblMemberNumberValue.text = frmMBPointRedemptionLanding.txtMemberNumber.text;
	}else{
		frmMBPointRedemptionConfirmation.hbxMemberNoLastName.isVisible=false;
	}
	
	if(frmMBPointRedemptionLanding.hbxCashBack.isVisible){
		frmMBPointRedemptionConfirmation.hbxCashBack.isVisible = true;
	}else{
		frmMBPointRedemptionConfirmation.hbxCashBack.isVisible = false;
	}
	frmMBPointRedemptionConfirmation.show();
}

function frmMBPointRedemptionConfirmationPreShow(){
	changeStatusBarColor();
	onPointRedemptionClickFromAccountDetails(frmMBPointRedemptionConfirmation);
	frmMBPointRedemptionConfirmation.lblHdrTxt.text=kony.i18n.getLocalizedString("keyConfirmation");
	frmMBPointRedemptionConfirmation.lblCardNumber.text=kony.i18n.getLocalizedString("CardNo");
	frmMBPointRedemptionConfirmation.lblAvailablePoints.text=kony.i18n.getLocalizedString("keyMBAvailableR");
	frmMBPointRedemptionConfirmation.lblRewards.text=kony.i18n.getLocalizedString("keyChooseYourReward")+":";
	frmMBPointRedemptionConfirmation.btnCancel.text=kony.i18n.getLocalizedString("keyCancelButton");
	frmMBPointRedemptionConfirmation.btnNext.text=kony.i18n.getLocalizedString("Next");
	
	if(frmMBPointRedemptionConfirmation.hbxCashBack.isVisible){
		frmMBPointRedemptionConfirmation.lblCashbackTo.text=kony.i18n.getLocalizedString("keyCashbackToAccount");
		frmMBPointRedemptionConfirmation.lblAmntCashbackRopMile.text=kony.i18n.getLocalizedString("keyTotalCashbackAmount");
		cashbackAccountLanguageToggle(frmMBPointRedemptionConfirmation);
	}
	if(frmMBPointRedemptionConfirmation.hbxCommonRewardsEntry.isVisible){
		frmMBPointRedemptionConfirmation.lblPointsBeingRedeemed.text=kony.i18n.getLocalizedString("keyPointsbeingredeemed");
		frmMBPointRedemptionConfirmation.lblRemainingPoints.text=kony.i18n.getLocalizedString("keyRemainingPoints");
	}
	if(frmMBPointRedemptionConfirmation.hbxMemberNoLastName.isVisible){
		frmMBPointRedemptionConfirmation.lblMemberNumber.text=kony.i18n.getLocalizedString("keyRewardsMemberNo");
		frmMBPointRedemptionConfirmation.lblLastName.text=kony.i18n.getLocalizedString("keyLastName");
		var rewardType = identifyRewardType();
		if(rewardType == "AirAsia"){
			frmMBPointRedemptionConfirmation.lblAmntCashbackRopMile.text=kony.i18n.getLocalizedString("keyReceivedBigPoint");
		}else if(rewardType == "ROP"){
			frmMBPointRedemptionConfirmation.lblAmntCashbackRopMile.text=kony.i18n.getLocalizedString("keyReceivedROPMile");
		}else{
			frmMBPointRedemptionLanding.lblAmntCashbackRopMile.text = kony.i18n.getLocalizedString("keyReceivedMile");
		}
		
	}
	rewardSelectedLanguageToggle(frmMBPointRedemptionConfirmation);
}

function onClickNextRewardsConfirmation(){
	frmMBPointRedemptionComplete.lblPointsBeingRedeemedValue.text = frmMBPointRedemptionConfirmation.lblPointsBeingRedeemedValue.text;
	frmMBPointRedemptionComplete.lblRemainingPointsValue.text = frmMBPointRedemptionConfirmation.lblRemainingPointsValue.text;
	frmMBPointRedemptionComplete.lblAmntCashbackRopMileValue.text = frmMBPointRedemptionConfirmation.lblAmntCashbackRopMileValue.text;
	
	if(frmMBPointRedemptionConfirmation.hbxMemberNoLastName.isVisible){
		frmMBPointRedemptionComplete.hbxMemberNoLastName.isVisible=true;
		frmMBPointRedemptionComplete.lblLastNameValue.text = frmMBPointRedemptionConfirmation.lblLastNameValue.text;
		frmMBPointRedemptionComplete.lblMemberNumberValue.text = frmMBPointRedemptionConfirmation.lblMemberNumberValue.text;
	}else{
		frmMBPointRedemptionComplete.hbxMemberNoLastName.isVisible=false;
	}
	
	if(frmMBPointRedemptionConfirmation.hbxCashBack.isVisible){
		frmMBPointRedemptionComplete.hbxCashBack.isVisible = true;
		rewardsTxnPasswordConfirmation('');
	}else{
		frmMBPointRedemptionComplete.hbxCashBack.isVisible = false;
		popupTractPwd.lblPopupTract7.skin = lblPopupLabelTxt;
    	popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = tbxPopupBlue;
    	popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = tbxPopupBlue;
    	showTransactionPasswordPopupRewards();
	}
	
}

function showTransactionPasswordPopupRewards(){
   if(gblAuthAccessPin == true){
     showAccesspinPopup();
   }else{
     var lblText = kony.i18n.getLocalizedString("transPasswordSub");
	var refNo = "";
	var mobNO = "";
	showLoadingScreen();
	showOTPPopup(lblText, refNo, mobNO, rewardsTxnPasswordConfirmation, 3); 
   }
    
}

function rewardsTxnPasswordConfirmation(tranPassword) {
	
	if (tranPassword == null || tranPassword == '' && identifyRewardType() != "Cashback") {
		setTransPwdFailedError(kony.i18n.getLocalizedString("emptyMBTransPwd"));
		return false;
	}
	else
	{
		showLoadingScreen();
        if(gblAuthAccessPin != true){
          popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("keyTransPwdMsg");
		  popupTractPwd.lblPopupTract7.skin = lblPopupLabelTxt;
		  popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = tbxPopupBlue;
		  popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = tbxPopupBlue;
		  popupTractPwd.hbxPopupTranscPwd.skin = tbxPopupBlue;  
        }
		
    	var inputParam = {};
        inputParam["password"] = tranPassword;
        inputParam["notificationAdd_appID"] = appConfig.appId;
        inputParam["verifyPwdMB_loginModuleId"] = "MB_TxPwd";
        inputParam["verifyPwdMB_retryCounterVerifyAccessPin"] = "0";
        inputParam["verifyPwdMB_retryCounterVerifyTransPwd"] = "0";
        
        //Params for Redeem service
        inputParam["redeemPointAdd_cardId"] = gblAccountTable["custAcctRec"][gblIndex]["accId"];
        inputParam["redeemPointAdd_productCode"] = gblrewardsData["rewardsDataSet"][gblSelectedReward]["rewardCode"];
        inputParam["redeemPointAdd_baseRedeemPoint"] = gblrewardsData["rewardsDataSet"][gblSelectedReward]["rewardPoint"];
        inputParam["redeemPointAdd_redempUnit"] = gblrewardsData["rewardsDataSet"][gblSelectedReward]["rewardUnit"];
        inputParam["redeemPointAdd_tranDt"] = "";// should be picked from server
        inputParam["redeemPointAdd_memberCode"] = frmMBPointRedemptionConfirmation.lblMemberNumberValue.text;
        inputParam["redeemPointAdd_memberSurName"] = frmMBPointRedemptionConfirmation.lblLastNameValue.text;
        inputParam["redeemPointAdd_toCardNo"] = gblSelectedToAccount["hiddenAccountId"];
        inputParam["redeemPointAdd_toAcctNo"] = gblSelectedToAccount["hiddenAccountId"];
        inputParam["redeemPointAdd_totalPointsRedeemed"] = replaceCommon(frmMBPointRedemptionConfirmation.lblPointsBeingRedeemedValue.text, ",", "");
        
        invokeServiceSecureAsync("RedeemPointCompositeService", inputParam, callBackRedeemCompJavaServiceMB);
	}
}

function callingFrmDetailPage()
{
	shortCutActDetailsPage();
	frmAccountDetailsMB.show();
}

function callBackRedeemCompJavaServiceMB(status,resulttable){
	if (status == 400) {
		dismissLoadingScreen();
        if (resulttable["opstatus"] == 0) {
        	popupTractPwd.dismiss();
           onClickCancelAccessPin();
        	if(resulttable["pointRedeemActBusinessHrsFlag"] == "false") 
			{
				var startTime = resulttable["pointRedeemStartTime"];
         		var endTime = resulttable["pointRedeemEndTime"];
         		var messageUnavailable = kony.i18n.getLocalizedString("keySoGooODServiceUnavailable");
         		messageUnavailable = messageUnavailable.replace("{start_time}", startTime);
         		messageUnavailable = messageUnavailable.replace("{end_time}", endTime);
         		
               	showAlertWithCallBack(messageUnavailable, kony.i18n.getLocalizedString("info"),callingFrmDetailPage);
                //frmAccountDetailsMB.show();
               	return false;
			}else{
	            frmMBPointRedemptionComplete.lblRemainingPointsValue.text = commaFormattedPoints(resulttable["remainingCardPoint"]);
	            frmMBPointRedemptionComplete.imgRewardsInComplete.isVisible=false;
	            frmMBPointRedemptionComplete.imgRewardsComplete.isVisible=true;
	            frmMBPointRedemptionComplete.btnRight.skin="btnShare";
	            frmMBPointRedemptionComplete.btnRight.focusSkin="btnShare";
	            frmMBPointRedemptionComplete.btnRight.setEnabled(true);
	            frmMBPointRedemptionComplete.show();
	        }    
        }else if (resulttable["errCode"] == "VrfyAcPWDErr00001" || resulttable["errCode"] == "VrfyAcPWDErr00002") {
			popupEnterAccessPin.lblWrongPin.setVisibility(true);
            kony.print("invalid pin  flow"); //To do : set red skin to enter access pin
            resetAccessPinImg(resulttable["badLoginCount"]);
            return false;
		}else if (resulttable["errCode"] == "VrfyAcPWDErr00003") {
			onClickCancelAccessPin();
            gotoUVPINLockedScreenPopUp();
            return false;
		}
        else if (resulttable["errCode"] == "VrfyTxPWDErr00001" || resulttable["errCode"] == "VrfyTxPWDErr00002") {
			setTransPwdFailedError(kony.i18n.getLocalizedString("invalidTxnPwd"));
		} 
        else if (resulttable["errCode"] == "VrfyTxPWDErr00003") {
			showTranPwdLockedPopup();
		}else if (resulttable["opstatus"] == 8005) {
			dismissLoadingScreen();
            if (resulttable["errCode"] == "VrfyOTPErr00001") {
                gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
                dismissLoadingScreen();
                alert("" + kony.i18n.getLocalizedString("invalidOTP"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00002") {
                dismissLoadingScreen();
                alert("" + kony.i18n.getLocalizedString("ECVrfyOTPErr"));
                //startRcCrmUpdateProfilBPIB("04");
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00005") {
                dismissLoadingScreen();
                alert("" + kony.i18n.getLocalizedString("KeyTokenSerialNumError"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00006") {
            	 dismissLoadingScreen();
                gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
                alert("" + resulttable["errMessage"]);
                return false;
            }else if (resulttable["errCode"] == "VrfyOTPErr00004") {
                dismissLoadingScreen();
                alert("" + resulttable["errMsg"]);
                return false;
            }else{
             	dismissLoadingScreen();
              	alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
                return false;
            }
        } else {
            dismissLoadingScreen();
            alert("" + resulttable["errMsg"]);
            popupTractPwd.dismiss();
            onClickCancelAccessPin();
            frmMBPointRedemptionComplete.imgRewardsInComplete.isVisible=true;
            frmMBPointRedemptionComplete.imgRewardsComplete.isVisible=false;
            frmMBPointRedemptionComplete.btnRight.skin="btnReturn";
	        frmMBPointRedemptionComplete.btnRight.focusSkin="btnReturn";
	        frmMBPointRedemptionComplete.btnRight.setEnabled(false);
	        frmMBPointRedemptionComplete.lblPointsBeingRedeemedValue.text = "-";
			frmMBPointRedemptionComplete.lblAmntCashbackRopMileValue.text = "-";
			frmMBPointRedemptionComplete.lblRemainingPointsValue.text = commaFormattedPoints(gblAccountTable["custAcctRec"][gblIndex]["bonusPointAvail"]);
            frmMBPointRedemptionComplete.show();
        }
	} else {
        if (status == 300) {
            dismissLoadingScreen();
            alert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"));
        }
	}	
}

function frmMBPointRedemptionCompletePreShow(){
    changeStatusBarColor();
	onPointRedemptionClickFromAccountDetails(frmMBPointRedemptionComplete);
	frmMBPointRedemptionComplete.lblHdrTxt.text=kony.i18n.getLocalizedString("Complete");
	frmMBPointRedemptionComplete.lblCardNumber.text=kony.i18n.getLocalizedString("CardNo");
	frmMBPointRedemptionComplete.lblAvailablePoints.text=kony.i18n.getLocalizedString("keyMBAvailableR");
	frmMBPointRedemptionComplete.lblRewards.text=kony.i18n.getLocalizedString("keyChooseYourReward")+":";
	frmMBPointRedemptionComplete.btnCancel.text=kony.i18n.getLocalizedString("keyReturn");
	frmMBPointRedemptionComplete.btnNext.text=kony.i18n.getLocalizedString("keyRewardsRedeemMore");
	
	if(frmMBPointRedemptionComplete.hbxCashBack.isVisible){
		frmMBPointRedemptionComplete.lblCashbackTo.text=kony.i18n.getLocalizedString("keyCashbackToAccount");
		frmMBPointRedemptionComplete.lblAmntCashbackRopMile.text=kony.i18n.getLocalizedString("keyTotalCashbackAmount");
		frmMBPointRedemptionComplete.hbxCompleteMessage.isVisible=true;
		frmMBPointRedemptionComplete.lblCompleteMessage.text=kony.i18n.getLocalizedString("keyRewardsCompleteCashback");
		cashbackAccountLanguageToggle(frmMBPointRedemptionComplete);
	}
	if(frmMBPointRedemptionComplete.hbxCommonRewardsEntry.isVisible){
		frmMBPointRedemptionComplete.lblPointsBeingRedeemed.text=kony.i18n.getLocalizedString("keyPointsbeingredeemed");
		frmMBPointRedemptionComplete.lblRemainingPoints.text=kony.i18n.getLocalizedString("keyRemainingPoints");
	}
	if(frmMBPointRedemptionComplete.hbxMemberNoLastName.isVisible){
		frmMBPointRedemptionComplete.lblMemberNumber.text=kony.i18n.getLocalizedString("keyRewardsMemberNo");
		frmMBPointRedemptionComplete.lblLastName.text=kony.i18n.getLocalizedString("keyLastName");
		frmMBPointRedemptionComplete.hbxCompleteMessage.isVisible=true;
		var rewardType = identifyRewardType();
		if(rewardType == "AirAsia"){
			frmMBPointRedemptionComplete.lblAmntCashbackRopMile.text=kony.i18n.getLocalizedString("keyReceivedBigPoint");
			frmMBPointRedemptionComplete.lblCompleteMessage.text=kony.i18n.getLocalizedString("keyRewardsCompleteBigPoint");
		}else if(rewardType == "ROP"){
			frmMBPointRedemptionComplete.lblAmntCashbackRopMile.text=kony.i18n.getLocalizedString("keyReceivedROPMile");
			frmMBPointRedemptionComplete.lblCompleteMessage.text=kony.i18n.getLocalizedString("keyRewardsCompleteROP");
		}else{
			frmMBPointRedemptionComplete.lblAmntCashbackRopMile.text = kony.i18n.getLocalizedString("keyReceivedMile");
			frmMBPointRedemptionComplete.hbxCompleteMessage.isVisible=false;
		}
	}
	
	if(frmMBPointRedemptionComplete.imgRewardsInComplete.isVisible){
		frmMBPointRedemptionComplete.lblCompleteMessage.text=kony.i18n.getLocalizedString("keyPointRedeemFail");
	}
	rewardSelectedLanguageToggle(frmMBPointRedemptionComplete);	
}

function shareButtonHandlerPointRedemption(){
	var btnskin = "btnShare";
	var btnFocusSkin = "btnShareFoc";
	if (frmMBPointRedemptionComplete.hboxaddfblist.isVisible) {
		frmMBPointRedemptionComplete.hboxaddfblist.isVisible = true;
		frmMBPointRedemptionComplete.btnRight.skin = btnskin;
		frmMBPointRedemptionComplete.imgHeaderMiddle1.src = "arrowtop.png";
		frmMBPointRedemptionComplete.imgHeaderRight1.src = "empty.png";
	} else {
		frmMBPointRedemptionComplete.hboxaddfblist.isVisible = true;
		frmMBPointRedemptionComplete.btnRight.skin = btnFocusSkin;
		frmMBPointRedemptionComplete.imgHeaderMiddle1.src = "empty.png";
		frmMBPointRedemptionComplete.imgHeaderRight1.src = "arrowtop.png";
	}
}

function saveRedeemCompleteAsPDFImage(filetype){
    var inputParam = {};
    inputParam["filetype"] = filetype;
    inputParam["activityTypeId"] = "RedeemPoint";
    inputParam["futurePDF"] = "true";
    
    invokeServiceSecureAsync("generateImagePdf", inputParam, mbRenderFileCallbackfunction);
}

function postOnFBPointredemptionComplete(){
	requestfromform="frmMBPointRedemptionComplete";
	gblPOWcustNME=gblCustomerName;
	gblPOWtransXN="PointRedemption";
	gblPOWchannel="MB";
	postOnWall();
}

function checkMBCustStatus()
{
     	if(checkMBUserStatus())
		{
			checkpointRedeemBousinessHrsMB(); // common code in IBPointRedmption
			//frmMBPointRedemptionProdFeature.show();
		}
}

function checkpointRedeemBousinessHrsMB(){
	var input_param = {};
    invokeServiceSecureAsync("pointRedeemBousinessHrs", input_param, checkpointRedeemBousinessHrsCallBackMB);
}

function checkpointRedeemBousinessHrsCallBackMB(status,result){

       if(status == 400){
              if(result["opstatus"] == 0){
                     var serviceHrsFlag = result["pointRedeemActBusinessHrsFlag"];
                     var startTime = result["pointRedeemStartTime"];
                 		pntRedeem_StartTime=result["pointRedeemStartTime"]
                 		var endTime = result["pointRedeemEndTime"];
                 		pntRedeem_EndTime=result["pointRedeemEndTime"];
                 		var messageUnavailable = kony.i18n.getLocalizedString("keyPointRedmptionProdFeature");
                 		messageUnavailable = messageUnavailable.replace("{start_time}", startTime);
                 		messageUnavailable = messageUnavailable.replace("{end_time}", endTime);
                     if(serviceHrsFlag=="true"){
                     	frmMBPointRedemptionProdFeature.lblPointRedemptionProdFeature.text=messageUnavailable;
                        frmMBPointRedemptionProdFeature.show();
                     }else{
                     	var serviceHrs = kony.i18n.getLocalizedString("keySoGooODServiceUnavailable");
                 		serviceHrs = serviceHrs.replace("{start_time}", startTime);
                 		serviceHrs = serviceHrs.replace("{end_time}", endTime);
                       	showAlert(serviceHrs, kony.i18n.getLocalizedString("info"));
                        return false;
                     }                    
              }else{
                    	dismissLoadingScreenBasedOnChannel(channel);
						if (resulttable["errMsg"] != null || resulttable["errMsg"] != "") {
								showAlert("Sorry, System found error : " + resulttable["errMsg"], kony.i18n.getLocalizedString("info") );
								return false;
						}else{
								showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
								return false;
						}
              }
       }
}

function onClickRedeemMoreComplete(){
	showLoadingScreen();
	/*var points_before_redemption=Number(gblAccountTable["custAcctRec"][gblIndex]["bonusPointAvail"]);
	var unformatted_points_after_redemption = replaceCommon(frmMBPointRedemptionComplete.lblRemainingPointsValue.text,",","");
	var points_after_redemption = Number(unformatted_points_after_redemption);
	var points_expiring = Number(gblAccountTable["custAcctRec"][gblIndex]["bonusPointExpired"]);
	var redeemed_points =  points_before_redemption-points_after_redemption;
	if((points_expiring - redeemed_points) > 0)
		gblAccountTable["custAcctRec"][gblIndex]["bonusPointExpired"]=(points_expiring - redeemed_points)+"";
	else
		gblAccountTable["custAcctRec"][gblIndex]["bonusPointExpired"]="0";
	gblAccountTable["custAcctRec"][gblIndex]["bonusPointAvail"]=unformatted_points_after_redemption;*/
	creditCardCallServiceRewards();
}

function creditCardCallServiceRewards() {
	var inputparam = {};
	gblCC_StmtPointAvail = "";
	gblCC_RemainingPoint = "";	
	gblCC_PointExpRemaining = "";
	inputparam["cardId"] = gblAccountTable["custAcctRec"][gblIndex]["accId"];
	inputparam["tranCode"] = "1";
	var status = invokeServiceSecureAsync("creditcardDetailsInq", inputparam, creditCardRewardsCallBack);
}

function creditCardRewardsCallBack(status, resulttable) {
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			if (resulttable["stmtDate"] != "") {
				gblCC_StmtPointAvail = resulttable["bonusPointAvail"];
			}			
			creditCardCallServiceUnBillAmtRewards();
		}
	}
}

function creditCardCallServiceUnBillAmtRewards() {
	var inputparam = {};
	inputparam["cardId"] = gblAccountTable["custAcctRec"][gblIndex]["accId"];
	inputparam["tranCode"] = TRANSCODEUN;
	var status = invokeServiceSecureAsync("creditcardDetailsInq", inputparam, creditCardCallBackUnRewards);
}

function creditCardCallBackUnRewards(status, resulttable) {
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			var allowRedeemPoints = gblAccountTable["custAcctRec"][gblIndex]["allowRedeemPoints"];		
			if (allowRedeemPoints == "1" && resulttable["remainingPoint"] != "") {
				gblAccountTable["custAcctRec"][gblIndex]["bonusPointAvail"] = resulttable["remainingPoint"];
				gblCC_RemainingPoint = resulttable["remainingPoint"];						
			}				
			if (allowRedeemPoints == "1" && resulttable["bonusPointUsed"] != "") {
				if (resulttable["stmtDate"] != "") {
					gblAccountTable["custAcctRec"][gblIndex]["bonusPointExpiryDate"] = resulttable["stmtDate"];
				}
				gblAccountTable["custAcctRec"][gblIndex]["bonusPointExpired"] = resulttable["bonusPointUsed"];
				
				if (resulttable["bonusPointUsed"] != "" && gblCC_StmtPointAvail != "" && resulttable["remainingPoint"] != "") {
					gblCC_PointExpRemaining = Number(resulttable["bonusPointUsed"]) - (Number(gblCC_StmtPointAvail) - Number(resulttable["remainingPoint"]));		
					if (gblCC_PointExpRemaining < 0) {
						gblCC_PointExpRemaining = 0;
					}
					gblCC_PointExpRemaining = gblCC_PointExpRemaining + "";				
				}
				gblAccountTable["custAcctRec"][gblIndex]["bonusPointExpired"] = gblCC_PointExpRemaining;   
			}			
			dismissLoadingScreen();			
			frmMBPointRedemptionTnC.show();
		} else {
			alert(" " + resulttable["errMsg"]);
			dismissLoadingScreen();
		}
	}
}

function clearPointRedemptionLandingOnLogout(){
	frmMBPointRedemptionLanding.txtLastName.text="";
	frmMBPointRedemptionLanding.txtMemberNumber.text="";
	frmMBPointRedemptionLanding.txtPointsBeingRedeemed.text="";
}
