
BillerList = null;
function moveToBillPaymentFromAccountSummaryForLoan()// /* GREAT */
{
	// code to move to bill pay modules as per situation
	showLoadingScreenPopup();
	
	var accId = gblAccountTable["custAcctRec"][gblIndex]["accId"];
	accId = removeHyphenIB(accId);
	
	gblAccountType = gblAccountTable["custAcctRec"][gblIndex]["accType"];
    gblProductCode = gblAccountTable["custAcctRec"][gblIndex]["productID"];
  	accId=accId.substring(1,accId.length )
	//
	gblReference1 = accId;
	
	gblReference2 = gblReference1.substring(gblReference1.length-3,gblReference1.length);
    gblReference1 = gblReference1.substring(0,gblReference1.length-3);
	
	gblMyBillerTopUpBB = 0;
	gblBillerPresentInMyBills = false;
	gblFromAccountSummary = true;
	gblBillPaymentEdit = false;
	clearIBBillPaymentLP();
	getCompcode(gblProductCode, gblAccountType);//"AL01";
}

  function populateLoanonBillPayCw(LoanAcct,reference2) {

    //for TMB Loan
    var TMB_BANK_FIXED_CODE = "0001";
    var TMB_BANK_CODE_ADD = "0011";
    var ZERO_PAD = "0000";
    var ref2;
    var BRANCH_CODE;
    var ref1AccountIDLoan = LoanAcct;
    if(reference2 != null && reference2 != undefined){
     	ref2 = reference2;
    }
    else {
    	ref2 = "";
    }
    if (ref1AccountIDLoan.length == 10) {
        
        ref1AccountIDLoan = "0"+ ref1AccountIDLoan+ref2;
    }
   	else if (ref1AccountIDLoan.length == 13) {
        
        ref1AccountIDLoan = "0" + ref1AccountIDLoan
    } 
    var inputParam = {};
    inputParam["acctId"] = ref1AccountIDLoan;
    inputParam["acctType"] = "LOC";
    
    invokeServiceSecureAsync("doLoanAcctInq", inputParam, BillPaymentCWdoLoanAcctInqServiceCallBackIB);  
}
function BillPaymentCWdoLoanAcctInqServiceCallBackIB(status, result) {
    
    fullAmt="0.00"
    
    if (status == 400) //success responce
    {
        
        if (result["opstatus"] == 0) {
            
            var balAmount;
            fullAmt=result["regPmtCurAmt"];
            
            frmIBBillPaymentCW.lblFullPayment.text = numberWithCommas(removeCommos(fullAmt)) + kony.i18n.getLocalizedString("currencyThaiBaht");
            frmIBBillPaymentCW.lblFullPayment.setVisibility(true);
            
            frmIBBillPaymentCW.txtBillAmt.text = numberWithCommas(removeCommos(fullAmt));
            frmIBBillPaymentCW.txtBillAmt.setVisibility(false);
			gblPenalty = false;
			frmIBBillPaymentCW.lblBPAmount.text = kony.i18n.getLocalizedString("keyAmount");
			frmIBBillPaymentCW.btnAmtFull.setEnabled(true);
			frmIBBillPaymentCW.btnAmtFull.setVisibility(true);
			frmIBBillPaymentCW.btnAmtMinimum.setEnabled(false);
			frmIBBillPaymentCW.btnAmtMinimum.setVisibility(false);
			frmIBBillPaymentCW.btnAmtSpecified.setEnabled(true);
			frmIBBillPaymentCW.btnAmtSpecified.setVisibility(true);
			frmIBBillPaymentCW.hbxMinMax.setVisibility(true);
            eh_frmIBBillPaymentCW_btnAmtFull_onClick();
            gblBillPaymentEdit =true;
            callBillPaymentCustomerAccountServiceIB();
           // dismissLoadingScreenPopup();
        } else {
            alert(" " + result["errMsg"]);
            dismissLoadingScreenPopup();
        }
    }
}

