function onClickNextOfCashAdvTncScreen() {
	gblCASelectedToAcctNum = "";
	gblCASelectedToAcctNameEN = "";
	gblCASelectedToAcctNameTH = "";
  	gblCashAdvPaymentType = "";
  	if(isCashChillChill){ // Ready Cash
      	gblAvailableBalance = gblAvailableToSpend;
      	showCashAdvanceInstallmentPlanDetails();
    }else{
      	gblAvailableBalance = parseFloat(gblRemainDailyLimit) < parseFloat(gblAvailableToSpend) ? gblRemainDailyLimit : gblAvailableToSpend;
      	showCashAdvanceFullPaymentDetails();
    }
}

function callBackCheckCashChillChill(status,resulttable){
	if(status == 400){
		if (resulttable["opstatus"] == 0) {
          	isCashChillChill = true; // Cash Chill Chill
        }else{
          	isCashChillChill = false; // Ready Cash (productID = 701)
        }
      	dismissLoadingScreen();
      	frmMBCashAdvanceTnC.show();
	}else{
		dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
		return false;
	}
}

function customerAccountInquiryForSelectAccounts() {
	showLoadingScreen();
	var inputParam = {}
	inputParam["cashAdvanceFlag"] = "true";
    invokeServiceSecureAsync("customerAccountInquiry", inputParam, callBackGetToAccountCashAdvDetails);
}

function callBackGetToAccountCashAdvDetails(status,resulttable){
	if(status == 400){
		if (resulttable["opstatus"] == 0) {
			if(undefined != resulttable["custAcctRec"]){
				var cashAdvanceAccounts = [];
				for(var i=0; i<resulttable["custAcctRec"].length; i++){
					var accountName = "";
					var accountNameTH = "";
					var accountNameEN = "";
					var accId = resulttable["custAcctRec"][i]["accId"];
					var acctStatusCode = resulttable["custAcctRec"][i]["acctStatusCode"];
					if ((resulttable["custAcctRec"][i]["acctNickName"]) == null || (resulttable["custAcctRec"][i]["acctNickName"]) == '') {
						if (kony.i18n.getCurrentLocale() == "th_TH"){
							accountName = resulttable["custAcctRec"][i]["acctNickNameTH"];
						}else{
							accountName = resulttable["custAcctRec"][i]["acctNickNameEN"];
						}
					}else {
						accountName = resulttable["custAcctRec"][i]["acctNickName"];
					}
					if(isNotBlank(accountName)){
						accountNameEN = resulttable["custAcctRec"][i]["acctNickNameEN"];
						accountNameTH = resulttable["custAcctRec"][i]["acctNickNameTH"];
						var accountNo = accId.substring(accId.length - 10,accId.length);
						var tempRecord = {  
								"imgSelect": "tick_icon.png",
								"lblAccountNum": accountNo,
			            		"lblAccountName": accountName,
			            		"lblAccountNameTH": accountNameTH,
			            		"lblAccountNameEN": accountNameEN,
			            		"lblAccountStatus": acctStatusCode
			       		};
						cashAdvanceAccounts.push(tempRecord);
					}
				}
		       	frmMBCashAdvAcctSelect.segAccounts.setData(cashAdvanceAccounts);
		       	frmMBCashAdvAcctSelect.segAccounts.setVisibility(true);
		       	//#ifdef android
			   		frmMBCashAdvAcctSelect.flxLine.setVisibility(true);                  
			    //#endif
				frmMBCashAdvAcctSelect.lblErrMsg.setVisibility(false);
		       	dismissLoadingScreen();
		       	frmMBCashAdvAcctSelect.show();
			}else{
				dismissLoadingScreen();
				frmMBCashAdvAcctSelect.segAccounts.setVisibility(false);
				//#ifdef android
			   		frmMBCashAdvAcctSelect.flxLine.setVisibility(false);                  
			    //#endif
				frmMBCashAdvAcctSelect.lblErrMsg.setVisibility(true);
				frmMBCashAdvAcctSelect.lblErrMsg.text = kony.i18n.getLocalizedString("CAV04_msgNoEligibleAcc");
				frmMBCashAdvAcctSelect.show();
				return false;
			}
		}else{
			//Handle Case No Valid Accounts Available for Transfer
			dismissLoadingScreen();
            showAlert(kony.i18n.getLocalizedString("CAV04_msgNoEligibleAcc"), kony.i18n.getLocalizedString("info"));
			return false;
		}
	}else{
		dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
		return false;
	}
}

function callCashAdvanceCompositeService(tranPassword) {
	var inputParam = {};
    inputParam["password"] = tranPassword;
    inputParam["XferAdd_fromAcctNo"] = gblCACardRefNumber;
    inputParam["XferAdd_toAcctNo"] = gblCASelectedToAcctNum;
    inputParam["XferAdd_fromAcctNickName"] = frmCAPaymentPlanConfirmation.lblCardName.text;
    inputParam["XferAdd_toAcctNickName"] = frmCAPaymentPlanConfirmation.lblToAcctName.text;
  	if(gblCAFullPaymentFlag == true){
      	inputParam["CCOrCA"] = "CA"; // Cash Advance
      	inputParam["pmtTerm"] = ""; //
    }else{
      	inputParam["CCOrCA"] = "CC"; // Chill Chill
      	inputParam["pmtTerm"] = frmCAPaymentPlanConfirmation.lblTenorVal.text; //
    }
  	var caAmount = frmCAPaymentPlanConfirmation.lblAmountVal.text;
    if(isNotBlank(caAmount)){
    	caAmount = caAmount.replace(/,/g, "");
		caAmount = kony.string.replace(caAmount, kony.i18n.getLocalizedString("currencyThaiBaht"), "");
	    inputParam["XferAdd_amount"] = caAmount.trim();
	    invokeServiceSecureAsync("cashAdvanceCompositeService", inputParam, callBackConfirmCashAdvanceService);
    }else{
    	showAlert("Invalid amount.", kony.i18n.getLocalizedString("info"));
    }
}

function callBackConfirmCashAdvanceService(status,resulttable){
	if (status == 400) {
        if (resulttable["opstatus"] == 0) {
			popupTractPwd.dismiss();
            onClickCancelAccessPin();
            dismissLoadingScreen();
             
             if(resulttable["CA_BusinessHrsFlag"] == "false"){
             	 var CA_StartTime = resulttable["CA_StartTime"];
			 	 var CA_EndTime = resulttable["CA_EndTime"];
             	 var errorMsg = kony.i18n.getLocalizedString("Card_Err_ServiceHour");
				 errorMsg = errorMsg.replace("{service_hours}", CA_StartTime + " - " + CA_EndTime);
                 showAlertWithCallBack(errorMsg, kony.i18n.getLocalizedString("info"),  showAccuntSummaryScreen);
                 return false;
             } else {
            	gotoCAPaymentPlanCompleteMB("0");
             }
        }else if (resulttable["opstatus"] == 1) {  
        	popupTractPwd.dismiss();
            onClickCancelAccessPin();
            dismissLoadingScreen();
          	if(resulttable["errCode"] == "K898989") {
              	showAlertWithCallBack(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"),  invokeLogoutService);
            }else{
            	gotoCAPaymentPlanCompleteMB("-1");  
            }
        	
        }else if (resulttable["errCode"] == "VrfyAcPWDErr00001" || resulttable["errCode"] == "VrfyAcPWDErr00002") {
			popupEnterAccessPin.lblWrongPin.setVisibility(true);
            kony.print("invalid pin transfer flow"); //To do : set red skin to enter access pin
            resetAccessPinImg(resulttable["badLoginCount"]);
		}else if (resulttable["errCode"] == "VrfyAcPWDErr00003") {
           onClickCancelAccessPin();
		   gotoUVPINLockedScreenPopUp();	
		}else if (resulttable["errCode"] == "VrfyTxPWDErr00001" || resulttable["errCode"] == "VrfyTxPWDErr00002") {
			setTransPwdFailedError(kony.i18n.getLocalizedString("invalidTxnPwd"));
		}else if (resulttable["errCode"] == "VrfyTxPWDErr00003") {
			showTranPwdLockedPopup();
		}else if (resulttable["opstatus"] == 8005) {
			dismissLoadingScreen();
            if (resulttable["errCode"] == "VrfyOTPErr00001") {
                gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
                dismissLoadingScreen();
                showAlert("" + kony.i18n.getLocalizedString("invalidOTP"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00002") {
                dismissLoadingScreen();
                showAlert("" + kony.i18n.getLocalizedString("ECVrfyOTPErr"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00005") {
                dismissLoadingScreen();
                showAlert("" + kony.i18n.getLocalizedString("KeyTokenSerialNumError"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00006") {
            	dismissLoadingScreen();
                gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
                showAlert("" + resulttable["errMessage"], kony.i18n.getLocalizedString("info"));
                return false;
            }else if (resulttable["errCode"] == "VrfyOTPErr00004") {
                dismissLoadingScreen();
                if(isNotBlank(result["errMsg"])){
            	 	showAlert(resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
	            }else{
	            	showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
	            }
                return false;
            }else{
             	dismissLoadingScreen();
              	showAlert("" + kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                return false;
            }
        } else {
            dismissLoadingScreen();
			popupTractPwd.dismiss();
            onClickCancelAccessPin();
			gotoCAPaymentPlanCompleteMB("-1");
           	//var errorMsgTop = resulttable["errMsg"];
           	//if(!isNotBlank(resulttable["errMsg"])){
           	//	errorMsgTop = kony.i18n.getLocalizedString("ECGenOTPRtyErr00001");
           	//}
            //showAlert(errorMsgTop, kony.i18n.getLocalizedString("info"));
        }
    } else {
    	dismissLoadingScreen();
		popupTractPwd.dismiss();
        onClickCancelAccessPin();
        if (status == 300) {
            showAlert(kony.i18n.getLocalizedString("Receipent_alert_Error"), kony.i18n.getLocalizedString("info"));
        }else{
        	showAlert("status : " + status + " errMsg: " +resulttable["errMsg"] , kony.i18n.getLocalizedString("info"));
        }
    }
}

function callCashAdvanceSaveToSessionService() {
  try{
    kony.print("@@@ In callCashAdvanceSaveToSessionService() @@@");
	showLoadingScreen();
	var inputParam = {};
	var caAmount = frmCashAdvanceInstallmentPlanDetails.lblEnterAmount.text;
  
	if(isNotBlank(caAmount)){
		caAmount = parseFloat(caAmount.replace(/,/g, "")).toFixed(2);//caAmount.replace(/,/g, "");
	}else{
		caAmount = "0.00";
	}
  	var caFee = "";
  	var annualRate = "";
  	if(frmCashAdvanceInstallmentPlanDetails.flxFullPayment.isVisible){
    	caFee = ""+frmCashAdvanceInstallmentPlanDetails.lblFeeVal.text;  
      	annualRate = frmCashAdvanceInstallmentPlanDetails.lblAnnualRateVal.text;
    }else{
      	if(gblCAFullPaymentFlag){
          	annualRate = frmCashAdvanceInstallmentPlanDetails.lblTenorVal.text;
        }else{
          	annualRate = frmCashAdvanceInstallmentPlanDetails.lblRatePerMonthVal.text;
        }
      	caFee = ""+frmCashAdvanceInstallmentPlanDetails.lblInsFeeVal.text;  
    }	
	caFee = parseFloat(removeCommos(caFee)).toFixed(2);
	if(isNotBlank(annualRate)){
		annualRate = annualRate.replace("%", "");
	}else{
		annualRate = "0.00";
	}
  	var tenor = "";
  	var monthlyPmt = "";
  	var ccOrca = "";
  	if(gblCAFullPaymentFlag == false){
      	tenor = frmCashAdvanceInstallmentPlanDetails.lblTenorVal.text;
      	monthlyPmt = removeCommas(frmCashAdvanceInstallmentPlanDetails.lblPaymentAmtVal.text);
      	ccOrca = "CC";
    }else{
      	ccOrca = "CA";
      	gblCCReceivedByDate = frmCashAdvanceInstallmentPlanDetails.lblReceivedDate.text;
    }
  	inputParam["CCOrCA"] = ccOrca;
    inputParam["amount"] = caAmount;
    inputParam["fee"] = caFee;
    inputParam["toAcctNo"] = gblCASelectedToAcctNum;
    inputParam["pmtTerm"] = tenor;
  	inputParam["annualRate"] = annualRate;
  	inputParam["monthlyPmt"] = monthlyPmt;
    inputParam["cardRefId"] = gblCACardRefNumber;
  	inputParam["receivedByDate"] = gblCCReceivedByDate;
    invokeServiceSecureAsync("cashAdvanceSaveToSession", inputParam, callBackCashAdvanceSaveToSessionService);
  }catch(e){
    kony.print("@@@ In callCashAdvanceSaveToSessionService() Exception::::"+e);
  }
}

function callBackCashAdvanceSaveToSessionService(status,resulttable){
	if (status == 400) {
        if (resulttable["opstatus"] == 0) { 
			frmCAPaymentPlanConfirmation.lblFromAcctName.text = frmCashAdvanceInstallmentPlanDetails.lblCustName.text;
			var cardNumber = kony.string.replace(frmCashAdvanceInstallmentPlanDetails.lblCardNum.text, " ", "");
			frmCAPaymentPlanConfirmation.lblFromAcctNo.text = maskCreditCard(cardNumber);
			frmCAPaymentPlanConfirmation.lblToAcctName.text = frmCashAdvanceInstallmentPlanDetails.lblSelectedAccount.text;
			frmCAPaymentPlanConfirmation.lblToAcctNo.text = formatAccountNo(resulttable["toAcctNo"]);
			frmCAPaymentPlanConfirmation.lblReferenceVal.text = resulttable["referenceNum"];
          	frmCAPaymentPlanConfirmation.lblAmountVal.text = commaFormatted(parseFloat(resulttable["transferAmt"]).toFixed(2)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
			frmCAPaymentPlanConfirmation.lblFeeVal.text = commaFormatted(parseFloat(resulttable["fee"]).toFixed(2)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
          	if(gblCAFullPaymentFlag == true){
              	frmCAPaymentPlanConfirmation.lblAnnualRate.text = kony.i18n.getLocalizedString("CAV04_keyRate02");
                frmCAPaymentPlanConfirmation.lblAnnualRateVal.text = resulttable["annualRate"] + "%";
                frmCAPaymentPlanConfirmation.lblDebitCardOnVal.text = returnDateForFT();
                frmCAPaymentPlanConfirmation.lblRecieveDateVal.text = returnDateForFT();
              	frmCAPaymentPlanConfirmation.flxTenor.setVisibility(false);
              	frmCAPaymentPlanConfirmation.flxPaymentAmt.setVisibility(false);
            }else{
              	frmCAPaymentPlanConfirmation.lblAnnualRate.text = kony.i18n.getLocalizedString("CAV05_keyRate");
                frmCAPaymentPlanConfirmation.lblAnnualRateVal.text = resulttable["annualRate"] + "%";
              	frmCAPaymentPlanConfirmation.lblTenorVal.text = frmCashAdvanceInstallmentPlanDetails.lblTenorVal.text;
              	frmCAPaymentPlanConfirmation.lblPaymentAmtVal.text = frmCashAdvanceInstallmentPlanDetails.lblPaymentAmtVal.text + " " + 
                  kony.i18n.getLocalizedString("currencyThaiBaht");
                frmCAPaymentPlanConfirmation.lblDebitCardOnVal.text = returnDateForFT();
                frmCAPaymentPlanConfirmation.lblRecieveDateVal.text = gblCCReceivedByDate + " " + gblCCReceivedByTime;
              	frmCAPaymentPlanConfirmation.flxTenor.setVisibility(true);
              	frmCAPaymentPlanConfirmation.flxPaymentAmt.setVisibility(true);
            }
			frmCAPaymentPlanConfirmation.show();
			dismissLoadingScreen();
        }else{
        	dismissLoadingScreen();
        	if(isNotBlank(result["errMsg"])){
            	 showAlert(resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
            }else{
            	showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            }
			return false;
        }
    }else{
		dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
		return false;
	}
}

function cashAdvanceCreditCardDetailsInqGetLimits() {
	showLoadingScreen();
	var inputParam = {};
    inputParam["cardId"] = gblCACardRefNumber;
    inputParam["tranCode"] = "CA";
    invokeServiceSecureAsync("creditcardDetailsInq", inputParam, callBackCashAdvanceCreditCardDetailsInqGetLimits);
}

function callBackCashAdvanceCreditCardDetailsInqGetLimits(status,resulttable){
	if (status == 400) {
        if (resulttable["opstatus"] == 0) {
        	gblMinCashAdvanceAmount = resulttable["cashAdvanceMinAmt"];
			gblRemainDailyLimit = resulttable["RemDailyLimit"];
			gblAvailableToSpend = resulttable["AvailToSpend"];
			gblAvailableBalance = parseFloat(gblRemainDailyLimit) < parseFloat(gblAvailableToSpend) ? gblRemainDailyLimit : gblAvailableToSpend;
          	onClickNextOfCardInfoScreen();
        } else {
            dismissLoadingScreen();
            if(isNotBlank(result["errMsg"])){
            	 showAlert(resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
            }else{
            	showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            }
            return false;
        }
    }
}


function cashAdvanceCreditCardDetailsInqGetInterest() {
	showLoadingScreen();
	var inputParam = {};
    inputParam["cardId"] = gblCACardRefNumber;
    inputParam["tranCode"] = "CA";
    inputParam["amount"] = removeCommos(frmCashAdvanceInstallmentPlanDetails.lblEnterAmount.text);
    invokeServiceSecureAsync("creditcardDetailsInq", inputParam, callBackCashAdvanceCreditCardDetailsInqGetInterest);
}

function callBackCashAdvanceCreditCardDetailsInqGetInterest(status,resulttable){
  try{
    kony.print("@@@ In callBackCashAdvanceCreditCardDetailsInqGetInterest() @@@");
	if (status == 400) {
      if (resulttable["opstatus"] == 0) { 
        var InterestRate = resulttable["InterestRate"];
        var ActionFlag = resulttable["ActionFlag"];
        var Rate = resulttable["Rate"];
        var Amount = frmCashAdvanceInstallmentPlanDetails.lblEnterAmount.text;

        if(!isCashChillChill){
          frmCashAdvanceInstallmentPlanDetails.lblAnnualRateVal.text = InterestRate + "%";
          frmCashAdvanceInstallmentPlanDetails.lblFeeVal.text = commaFormatted(calculateCashAdvanceFee(Amount, Rate));
        }else{
          frmCashAdvanceInstallmentPlanDetails.lblTenorVal.text = InterestRate + "%";
          frmCashAdvanceInstallmentPlanDetails.lblInsFeeVal.text = commaFormatted(calculateCashAdvanceFee(Amount, Rate));
        }

        if(ActionFlag == "Y"){
          var msgPro = kony.i18n.getLocalizedString("msgPromotionRateWarning");
          msgPro = msgPro.replace("{Annual_Rate}", InterestRate);
          if(!isCashChillChill){
            frmCashAdvanceInstallmentPlanDetails.rchTxtNote.text = msgPro;
            frmCashAdvanceInstallmentPlanDetails.rchTxtNote.setVisibility(true);
          }else{
            frmCashAdvanceInstallmentPlanDetails.rchTxtNote2.text = msgPro;
            frmCashAdvanceInstallmentPlanDetails.rchTxtNote2.setVisibility(true);
          }
        }else{
          frmCashAdvanceInstallmentPlanDetails.rchTxtNote.text = "";
          frmCashAdvanceInstallmentPlanDetails.rchTxtNote.setVisibility(false);
          frmCashAdvanceInstallmentPlanDetails.rchTxtNote2.text = "";
          frmCashAdvanceInstallmentPlanDetails.rchTxtNote2.setVisibility(false);
        }
        if(kony.application.getCurrentForm().id != "frmCashAdvanceInstallmentPlanDetails"){
          frmCashAdvanceInstallmentPlanDetails.show();
          frmMBCashAdvanceTnC.destroy();
        }
        dismissLoadingScreen();
      } else {
        dismissLoadingScreen();
        if(isNotBlank(result["errMsg"])){
          showAlert(resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
        }else{
          showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        }
        return false;
      }
    }
  }catch(e){
    kony.print("@@@ In callBackCashAdvanceCreditCardDetailsInqGetInterest() Exception:::"+e);
  }
}

function cashAdvanceServiceHoursCheck() {
	showLoadingScreen();
	var inputParam = {};
    invokeServiceSecureAsync("cashAdvanceBusinessHrs", inputParam, callBackCashAdvanceServiceHoursCheck);
}

function callBackCashAdvanceServiceHoursCheck(status,result){
       if(status == 400){
              if(result["opstatus"] == 0){
                     
                     if(result["CA_BusinessHrsFlag"] == "true"){
                          cashAdvanceCreditCardDetailsInqGetLimits();
                     } else {
                     	 var CA_StartTime = result["CA_StartTime"];
					 	 var CA_EndTime = result["CA_EndTime"];
						 var errorMsg = kony.i18n.getLocalizedString("Card_Err_ServiceHour");
						 errorMsg = errorMsg.replace("{service_hours}", CA_StartTime + " - " + CA_EndTime);
						 dismissLoadingScreen();
                         showAlertWithCallBack(errorMsg, kony.i18n.getLocalizedString("info"),  showAccuntSummaryScreen);
                         return false;
                     }                    
              }else{
                	dismissLoadingScreen();
					showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
					return false;
              }
       }
}

function getCashAdvPaymentPlan() {
  kony.print("@@@ In getCashAdvPaymentPlan() @@@");
  showLoadingScreen();
  var inputParam = {};
  inputParam["cardRefId"] = gblCACardRefNumber;
  inputParam["applyValue"] = "1";
  inputParam["txnAmount"] = removeCommos(frmCashAdvanceInstallmentPlanDetails.lblEnterAmount.text);
  invokeServiceSecureAsync("cashChillChill", inputParam, callBackGetCashAdvPaymentPlan);
}

function callBackGetCashAdvPaymentPlan(status,resulttable){
  try{
    kony.print("@@@ In callBackGetCashAdvPaymentPlan() @@@");
    if(status == 400){
      if (resulttable["opstatus"] == 0) {
        if(undefined != resulttable["cashChillInstlDS"]){
          // get received by date and time
          var data = resulttable["receivedByDate"];
          if(isNotBlank(data)){
            var dateTime = data.split(" ");
            gblCCReceivedByDate = dateTime[0];
            if(dateTime.length > 1){
              gblCCReceivedByTime = dateTime[1];
            }
          }
          // get fee value 
          gblCCFee = resulttable["cashChillChillFee"];
          var paymentPlanList = [];
          var interestRate = "";
          if(isNotBlank(resulttable["effectiveRatePerYr"])){
            interestRate = resulttable["effectiveRatePerYr"];
          }
          frmMBCashAdvanceSelectPlan.lblInterest.text = kony.i18n.getLocalizedString("Loan_SelectPay_Installment_Interest")+" "+interestRate+"%";
          for(var i=0; i<resulttable["cashChillInstlDS"].length; i++){
            var tenor = resulttable["cashChillInstlDS"][i]["TenorCd"];
            //var rate = resulttable["cashChillInstlDS"][i]["InterestRate"] + "%";
            var fistPmtAmt = commaFormatted(parseFloat(resulttable["cashChillInstlDS"][i]["MonthlyInstlAmt"]).toFixed(2));
            var pmtAmt = commaFormatted(parseFloat(resulttable["cashChillInstlDS"][i]["TotalPaymentAmt"]).toFixed(2));
            var ref = resulttable["MIBReference"];
            /*if(isNotBlank(resulttable["cashChillInstlDS"][i]["InterestRate"])){
              interestRate = resulttable["cashChillInstlDS"][i]["InterestRate"];
            }*/
            var tempRecord = {  
              "imgSelect": "tick_icon.png",
              "lblRateVal": interestRate+"%", //rate,
              "lblTenorVal": tenor,
              "lblTotalPmtAmt": pmtAmt,
              "lblAmountVal": fistPmtAmt
            };
            paymentPlanList.push(tempRecord);
          }
          frmMBCashAdvanceSelectPlan.segPaymentPlan.setData(paymentPlanList);
          kony.print("@@@Special Interest Rate:::"+resulttable["effectiveRatePerYr"]);
          
          frmMBCashAdvanceSelectPlan.flxLine.setVisibility(true);
          frmMBCashAdvanceSelectPlan.flxSeg.setVisibility(false);
          frmMBCashAdvanceSelectPlan.flxErrMsg.setVisibility(false);
          frmMBCashAdvanceSelectPlan.flxInstallmentPlan.setEnabled(true);
          dismissLoadingScreen();
          frmMBCashAdvanceSelectPlan.show();
        }else{
          kony.print("---CashChillChill response empty---");
          frmMBCashAdvanceSelectPlan.flxLine.setVisibility(true);
          frmMBCashAdvanceSelectPlan.flxSeg.setVisibility(false);
          frmMBCashAdvanceSelectPlan.flxErrMsg.setVisibility(true);
          frmMBCashAdvanceSelectPlan.lblErrMsg.text = "No installment plan details";
          frmMBCashAdvanceSelectPlan.flxInstallmentPlan.setEnabled(false);
          dismissLoadingScreen();
          frmMBCashAdvanceSelectPlan.show();
          return false;
        }
      }else{
        dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        return false;
      }
	}else{
      dismissLoadingScreen();
      showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
      return false;
	}
  }catch(e){
    kony.print("@@@ In callBackGetCashAdvPaymentPlan() Exception:::"+e);
  }
}

function frmMBCashAdvanceSelectPlanMenuPreshow(){
  if (gblCallPrePost){
    frmMBCashAdvanceSelectPlanLocalePreshow();
  }
}

function frmMBCashAdvanceSelectPlanLocalePreshow(){
  try{
    kony.print("----- @@@ In frmMBCashAdvanceSelectPlanLocalePreshow() @@@ -----");
    changeStatusBarColor();
    frmMBCashAdvanceSelectPlan.lblCashAdvanceTitle.text = kony.i18n.getLocalizedString("CAV04_plPaymentPlan");
    frmMBCashAdvanceSelectPlan.lblInstallmentTitle.text = kony.i18n.getLocalizedString("CAV05_plInstallment");
    frmMBCashAdvanceSelectPlan.lblFullPayment.text = kony.i18n.getLocalizedString("CAV04_plFullPayment");
    frmMBCashAdvanceSelectPlan.lblTenor.text = kony.i18n.getLocalizedString("Loan_NoofMonth");
    frmMBCashAdvanceSelectPlan.lblRate.text = kony.i18n.getLocalizedString("Loan_TotalPayment");
    frmMBCashAdvanceSelectPlan.lblPayAmount.text = kony.i18n.getLocalizedString("Loan_FirstPayment");
    frmMBCashAdvanceSelectPlan.lblCancel.text = kony.i18n.getLocalizedString("keysave");

    frmMBCashAdvanceSelectPlan.flxFullPayment.onClick = onSelectFullPayment;
    frmMBCashAdvanceSelectPlan.flxInstallmentPlan.onClick = onSelectInstallment;
    frmMBCashAdvanceSelectPlan.segPaymentPlan.onRowClick = onRowClickSegmentOfPaymentPlanScreen;
    kony.print("@@@gblCashAdvPaymentType:::"+gblCashAdvPaymentType);
    if(isNotBlank(gblCashAdvPaymentType) && gblCashAdvPaymentType == "FullPayment"){
      frmMBCashAdvanceSelectPlan.imgFullPayment.src = "icon_checked_blue.png";
      frmMBCashAdvanceSelectPlan.imgInstallment.src = "icon_checked_grey.png";
      frmMBCashAdvanceSelectPlan.flxFooter.setVisibility(true);
      frmMBCashAdvanceSelectPlan.flxLine.setVisibility(true);
      frmMBCashAdvanceSelectPlan.flxSeg.setVisibility(false);
    }else if(isNotBlank(gblCashAdvPaymentType) && gblCashAdvPaymentType == "Installment"){
      frmMBCashAdvanceSelectPlan.imgFullPayment.src = "icon_checked_grey.png";
      frmMBCashAdvanceSelectPlan.imgInstallment.src = "icon_checked_blue.png";
      
      var selInstalItem = frmMBCashAdvanceSelectPlan.segPaymentPlan.selectedItems;
      if(selInstalItem != null && selInstalItem != "" && selInstalItem != undefined ){
        frmMBCashAdvanceSelectPlan.flxFooter.setVisibility(true);
      }else{
        frmMBCashAdvanceSelectPlan.flxFooter.setVisibility(false);
      }
    }else{
      frmMBCashAdvanceSelectPlan.imgFullPayment.src = "icon_checked_grey.png";
      frmMBCashAdvanceSelectPlan.imgInstallment.src = "icon_checked_grey.png";
      frmMBCashAdvanceSelectPlan.flxFooter.setVisibility(false);
    }

    frmMBCashAdvanceSelectPlan.lblFullMinDesp.text = kony.i18n.getLocalizedString("Loan_SelectPay_FullPayment_Desp");
    frmMBCashAdvanceSelectPlan.lblInstallmentTitleDesp.text = kony.i18n.getLocalizedString("Loan_SelectPay_Installment_Desp");
  }catch(e){
    kony.print("@@@ In frmMBCashAdvanceSelectPlanLocalePreshow() Exception:::"+e);
  }
}

function onSelectFullPayment(){
  try{
    kony.print("@@@ In onSelectFullPayment() @@@");
    var selIamge = frmMBCashAdvanceSelectPlan.imgFullPayment.src;
    if(isNotBlank(selIamge) && selIamge == "icon_checked_grey.png"){
      frmMBCashAdvanceSelectPlan.imgFullPayment.src = "icon_checked_blue.png";
      frmMBCashAdvanceSelectPlan.imgInstallment.src = "icon_checked_grey.png";
      gblCashAdvPaymentType = "FullPayment";
      frmMBCashAdvanceSelectPlan.flxLine.setVisibility(true);
      frmMBCashAdvanceSelectPlan.flxSeg.setVisibility(false);
      frmMBCashAdvanceSelectPlan.flxFooter.setVisibility(true);
    }else{
      gblCashAdvPaymentType = "";
      frmMBCashAdvanceSelectPlan.imgFullPayment.src = "icon_checked_grey.png";
      if(frmMBCashAdvanceSelectPlan.imgInstallment.src == "icon_checked_grey.png"){
        frmMBCashAdvanceSelectPlan.flxFooter.setVisibility(false);
      }else{
        frmMBCashAdvanceSelectPlan.flxFooter.setVisibility(true);
      }
    }
  }catch(e){
    kony.print("@@@ In onSelectFullPayment() Exception:::"+e);
  }
}

function onSelectInstallment(){
  try{
    kony.print("@@@ In onSelectInstallment() @@@");
    var selIamge = frmMBCashAdvanceSelectPlan.imgInstallment.src;
    if(isNotBlank(selIamge) && selIamge == "icon_checked_grey.png"){
      frmMBCashAdvanceSelectPlan.imgInstallment.src = "icon_checked_blue.png";
      frmMBCashAdvanceSelectPlan.imgFullPayment.src = "icon_checked_grey.png";
      gblCashAdvPaymentType = "Installment";
      var installData = frmMBCashAdvanceSelectPlan.segPaymentPlan.data;
      if(isNotBlank(installData)){
        frmMBCashAdvanceSelectPlan.flxLine.setVisibility(false);
        frmMBCashAdvanceSelectPlan.flxSeg.setVisibility(true);
        frmMBCashAdvanceSelectPlan.flxErrMsg.setVisibility(false);
        var selInstalItem = frmMBCashAdvanceSelectPlan.segPaymentPlan.selectedItems;
        if(selInstalItem != null && selInstalItem != "" && selInstalItem != undefined ){
          frmMBCashAdvanceSelectPlan.flxFooter.setVisibility(true);
        }else{
          frmMBCashAdvanceSelectPlan.flxFooter.setVisibility(false);
        }
      }else{
        frmMBCashAdvanceSelectPlan.flxLine.setVisibility(true);
        frmMBCashAdvanceSelectPlan.flxSeg.setVisibility(false);
        frmMBCashAdvanceSelectPlan.flxErrMsg.setVisibility(true);
        frmMBCashAdvanceSelectPlan.lblErrMsg.text = "No installment plan details";
      }
    }else{
      gblCashAdvPaymentType = "";
      frmMBCashAdvanceSelectPlan.imgInstallment.src = "icon_checked_grey.png";
      frmMBCashAdvanceSelectPlan.flxLine.setVisibility(true);
      frmMBCashAdvanceSelectPlan.flxSeg.setVisibility(false);
      frmMBCashAdvanceSelectPlan.flxFooter.setVisibility(false);
    }
  }catch(e){
    kony.print("@@@ In onSelectInstallment() Exception:::"+e);
  }
}

function onClickCancelOfSelectPlanScreen() {
  try{
    kony.print("@@@ In onClickCancelOfSelectPlanScreen() @@@");
    kony.print("@@@gblCashAdvPaymentType:::"+gblCashAdvPaymentType);
    if(isNotBlank(gblCashAdvPaymentType) && gblCashAdvPaymentType == "FullPayment"){
      kony.print("------- FullPayment ------");
	  gblCAFullPaymentFlag = true;
      gblAvailableBalance = parseFloat(gblRemainDailyLimit) < parseFloat(gblAvailableToSpend) ? gblRemainDailyLimit : gblAvailableToSpend;
      frmCashAdvanceInstallmentPlanDetails.lblInstallmentTitle.text = kony.i18n.getLocalizedString("CAV04_plFullPayment");
      frmCashAdvanceInstallmentPlanDetails.lblInsPaymentPlan.text = kony.i18n.getLocalizedString("CAV04_keyPaymentPlan");
      frmCashAdvanceInstallmentPlanDetails.lblInsFeeDesc.text = kony.i18n.getLocalizedString("CAV04_keyFee");
      frmCashAdvanceInstallmentPlanDetails.lblTenorDesc.text = kony.i18n.getLocalizedString("CAV04_keyRate02");
      frmCashAdvanceInstallmentPlanDetails.lblNowDesc.text = kony.i18n.getLocalizedString("keyNOW");
      frmCashAdvanceInstallmentPlanDetails.lblTime.text = kony.i18n.getLocalizedString("CAV04_keyTime01");
      frmCashAdvanceInstallmentPlanDetails.lblReceivedDate.text = getFormattedDate(currentSystemDate(), kony.i18n.getCurrentLocale());
      frmCashAdvanceInstallmentPlanDetails.lblNowDesc.text = kony.i18n.getLocalizedString("keyNOW");
      frmCashAdvanceInstallmentPlanDetails.flxInstallmentPlan.setVisibility(true);
      frmCashAdvanceInstallmentPlanDetails.flxDebitCardDateDetails.setVisibility(true);
      frmCashAdvanceInstallmentPlanDetails.flxSelectPaymentPlan.setVisibility(false);
      frmCashAdvanceInstallmentPlanDetails.flxFullPayment.setVisibility(false);
      frmCashAdvanceInstallmentPlanDetails.flxTenor.setVisibility(true);
      frmCashAdvanceInstallmentPlanDetails.flxAnnualRateDetails.setVisibility(false);
      frmCashAdvanceInstallmentPlanDetails.flxPaymentAmount.setVisibility(false);
      cashAdvanceCreditCardDetailsInqGetInterest();
      frmCashAdvanceInstallmentPlanDetails.show();
    }else if(isNotBlank(gblCashAdvPaymentType) && gblCashAdvPaymentType == "Installment"){
      kony.print("------- Installment ------");
      showLoadingScreen();
      gblCAFullPaymentFlag = false;
      gblAvailableBalance = gblAvailableToSpend;
      var tenorVal = frmMBCashAdvanceSelectPlan.segPaymentPlan.selectedItems[0].lblTenorVal;
      var rateVal = frmMBCashAdvanceSelectPlan.segPaymentPlan.selectedItems[0].lblRateVal;
      var amountVal = frmMBCashAdvanceSelectPlan.segPaymentPlan.selectedItems[0].lblAmountVal;
      frmCashAdvanceInstallmentPlanDetails.lblTenorVal.text = tenorVal;
      frmCashAdvanceInstallmentPlanDetails.lblRatePerMonthVal.text = rateVal;
      frmCashAdvanceInstallmentPlanDetails.lblPaymentAmtVal.text = amountVal;
      frmCashAdvanceInstallmentPlanDetails.flxInstallmentPlan.setVisibility(true);
      frmCashAdvanceInstallmentPlanDetails.flxDebitCardDateDetails.setVisibility(true);
      frmCashAdvanceInstallmentPlanDetails.flxSelectPaymentPlan.setVisibility(false);
      frmCashAdvanceInstallmentPlanDetails.lblReceivedDate.text = gblCCReceivedByDate;
      frmCashAdvanceInstallmentPlanDetails.lblNowDesc.text = gblCCReceivedByTime;
      frmCashAdvanceInstallmentPlanDetails.lblInsFeeVal.text = gblCCFee;
      frmCashAdvanceInstallmentPlanDetails.lblInstallmentTitle.text = kony.i18n.getLocalizedString("CAV04_plInstallment");
      frmCashAdvanceInstallmentPlanDetails.lblTenorDesc.text = kony.i18n.getLocalizedString("Loan_NoofMonth");
      frmCashAdvanceInstallmentPlanDetails.lblRatePerMonth.text = kony.i18n.getLocalizedString("CAV05_keyRate");
      frmCashAdvanceInstallmentPlanDetails.lblPaymentAmt.text = kony.i18n.getLocalizedString("Loan_FirstPayment");
      frmCashAdvanceInstallmentPlanDetails.lblTime.text = kony.i18n.getLocalizedString("CAV04_keyTime02");
      frmCashAdvanceInstallmentPlanDetails.flxTenor.setVisibility(true);
      frmCashAdvanceInstallmentPlanDetails.flxAnnualRateDetails.setVisibility(true);
      frmCashAdvanceInstallmentPlanDetails.flxPaymentAmount.setVisibility(true);
      frmCashAdvanceInstallmentPlanDetails.rchTxtNote2.text = "";
      frmCashAdvanceInstallmentPlanDetails.rchTxtNote2.setVisibility(false);
      frmCashAdvanceInstallmentPlanDetails.show();
      dismissLoadingScreen();
    }else{
      frmCashAdvanceInstallmentPlanDetails.show();
    }
  }catch(e){
    kony.print("@@@ In onClickCancelOfSelectPlanScreen() Exception:::"+e);
  }
}

function onClickBackOfSelectPlanScreen() {
  	var previousForm = kony.application.getPreviousForm();
  	previousForm.show();
	//frmMBCashAdvanceSelectPlan.destroy();
}

function onSelectFullInsallDetails(){
  frmMBCashAdvanceSelectPlan.show();
}