function getPlanHeaderText(plan, totalTransaction){
	var textPlan = "";
	var locale = kony.i18n.getCurrentLocale();
	
	arrayPlan = gblSoGooodMap[plan];
	planDesc = getPlanData(arrayPlan[0]["planId"]);
	percentIdx = planDesc.indexOf(" ");
	interest = planDesc.substring(0, percentIdx);
	textPlan = kony.i18n.getLocalizedString("keySoGooODSumTrans")
	textPlan = textPlan.replace("[n]", totalTransaction);
	if(totalTransaction == 1 && locale == "en_US") {
		textPlan = textPlan.replace("Transactions", "Transaction");
	}
	textPlan = textPlan.replace("[%]", interest);
	textPlan = textPlan.replace("[x]", plan);
	
	return textPlan;
}
function preshowfrmMBApplysogooodConfirm(){
	var screenwidth = gblDeviceInfo["deviceWidth"];
	var locale = kony.i18n.getCurrentLocale();
	//#ifdef android    
    //mki, mib-10789 Start
    // The below code snippet is to fix the whilte label on some screens
    frmMBApplysogooodConfirm.flexScroll.showFadingEdges  = false;
    //#endif  
    //mki, mib-10789 End
  
	if(gblIndex > -1) {
		if( null != gblAccountTable["custAcctRec"][gblIndex]["acctNickName"]){
			accNickName = gblAccountTable["custAcctRec"][gblIndex]["acctNickName"];
		}
		else{
			if (locale == "en_US") {
				accNickName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"];
			} else {
				accNickName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"];
			}
		}
		frmMBApplysogooodConfirm.lblCreditCardNickName.text = accNickName;
	} else {
		frmMBApplysogooodConfirm.lblCreditCardNickName.text = frmMBSoGooodPlanSelect.lblCreditCardNickName.text;
	}
	frmMBApplysogooodConfirm.lblHdrTxt.text = kony.i18n.getLocalizedString("lbSummaryTitle")
	frmMBApplysogooodConfirm.btnAddMore.text = kony.i18n.getLocalizedString("lbApplyAnotherInstallment");
	
	if(screenwidth > 640){
		frmMBApplysogooodConfirm.lblNextMonthInstallment.text = kony.i18n.getLocalizedString("lbTotalAppliedtoNextInstallment")
	} else {
		frmMBApplysogooodConfirm.lblNextMonthInstallment.text = kony.i18n.getLocalizedString("lbTotalAppliedtoNextInstallmentIP5")
	}
	
	frmMBApplysogooodConfirm.btnBack.text = kony.i18n.getLocalizedString("CAV05_btnCancel")
	frmMBApplysogooodConfirm.btnConfirm.text = kony.i18n.getLocalizedString("keyXferBtnConfirm")

	var keyPlan = Object.keys(gblSoGooodTotalMap);
    for(var i = 0; i < keyPlan.length; i++){
	   arrayTotalPlan = gblSoGooodTotalMap[keyPlan[i]];
	   totalTransaction = arrayTotalPlan[0]["totalTransaction"];
	   textPlan = getPlanHeaderText(keyPlan[i], totalTransaction);
	   frmMBApplysogooodConfirm["lblPlan"+keyPlan[i]].text = textPlan;
	   
	   frmMBApplysogooodConfirm["lblMonthlyPayment"+keyPlan[i]].text = kony.i18n.getLocalizedString("keySoGooODMonthlyInst") + ":";
	   frmMBApplysogooodConfirm["lblTotalInterest"+keyPlan[i]].text = kony.i18n.getLocalizedString("keySoGooODIntCharge") + ":";
	   frmMBApplysogooodConfirm["lblTotalAmount"+keyPlan[i]].text = kony.i18n.getLocalizedString("keySoGooODTotAmt");
       
       if(frmMBApplysogooodConfirm["segmentTransactionPlan"+keyPlan[i]].isVisible){ 
       		frmMBApplysogooodConfirm["btnExpand"+keyPlan[i]].text = kony.i18n.getLocalizedString("btnHideTransaction")
	   } else {
			frmMBApplysogooodConfirm["btnExpand"+keyPlan[i]].text = kony.i18n.getLocalizedString("btnViewTransaction")
	   }
       frmMBApplysogooodConfirm["segmentTransactionPlan"+keyPlan[i]].removeAll();
       setDataSoGooodTransactionSegment(keyPlan[i]);
    }
}
function preshowfrmMBApplySoGooodComplete(){
	changeStatusBarColor();
	var screenwidth = gblDeviceInfo["deviceWidth"];
	var locale = kony.i18n.getCurrentLocale();

	if(gblIndex > -1) {
		if( null != gblAccountTable["custAcctRec"][gblIndex]["acctNickName"]){
			accNickName = gblAccountTable["custAcctRec"][gblIndex]["acctNickName"];
		}
		else{
			if (locale == "en_US") {
				accNickName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"];
			} else {
				accNickName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"];
			}
		}
		frmMBApplySoGooodComplete.lblCreditCardNickName.text = accNickName;
	} else {
		frmMBApplySoGooodComplete.lblCreditCardNickName.text = frmMBApplysogooodConfirm.lblCreditCardNickName.text;
	}

	if(gblFailedTransaction.length == 0){
		message = kony.i18n.getLocalizedString("keyTransSuccess");
	} else{
		message = kony.i18n.getLocalizedString("keySoGooODSuccess");
		SuccessTran = soGoooDPlanSelectDataSet.length - gblFailedTransaction.length
		TotalTran = soGoooDPlanSelectDataSet.length
		message = message.replace("[succ]", SuccessTran);
		message = message.replace("[tot]", TotalTran); //
		if(TotalTran > 1 && locale == "en_US"){
			message = message.replace("[noOfTxn]", "s");
		} else{
			message = message.replace("[noOfTxn]", "");
		}
	}
	frmMBApplySoGooodComplete.lblTransactionsStatus.text = message;
	
	if(gblFailedTransaction.length > 0){
		frmMBApplySoGooodComplete.flexIncomplete.isVisible = true;
		frmMBApplySoGooodComplete.FlexFooter.isVisible = false;
		frmMBApplySoGooodComplete.lblHdrTxt.text = kony.i18n.getLocalizedString("keyIncomplete");
		frmMBApplySoGooodComplete.segFailedTransactions.removeAll();
        
        gblFailedTransaction = [];
		for(var i=0; i < soGoooDPlanSelectDataSet.length;i++){
            if(soGoooDPlanSelectDataSet[i]["status"]== "fail"){
              var tempRecord = { 
              	"lblTransDate": kony.i18n.getLocalizedString("keyMBTransactionDate") + ": " + soGoooDPlanSelectDataSet[i].txnDate,
    			"lblTransDesc": kony.i18n.getLocalizedString("keyMBDescription") + ": " + soGoooDPlanSelectDataSet[i].txnDesc,
    			"lblTransAmount": kony.i18n.getLocalizedString("keyTotalAmount"),
    			"lblTransAmountValue": commaFormatted(numberFormat(soGoooDPlanSelectDataSet[i].txnAmount, 2)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
    			"imgTransFail": "wrong.png"                                   
              };
              gblFailedTransaction.push(tempRecord);
            }
		}
		frmMBApplySoGooodComplete.segFailedTransactions.setData(gblFailedTransaction)
     
	} else {
		message = "";
		frmMBApplySoGooodComplete.lblHdrTxt.text = kony.i18n.getLocalizedString("Complete");
		frmMBApplySoGooodComplete.flexIncomplete.isVisible = false;
		frmMBApplySoGooodComplete.FlexFooter.isVisible = true;
		message = kony.i18n.getLocalizedString("msgTransactionFinanced");
		message = message.replace("[no]", soGoooDPlanSelectDataSet.length)
		if(soGoooDPlanSelectDataSet.length == 1	&& locale == "en_US") {
			message = message.replace("Transactions", "Transaction")
		}
		frmMBApplySoGooodComplete.lblTransSuccessStatus.text = message;
	} 
	
	frmMBApplySoGooodComplete.lblTransactionStatusDesc.text = kony.i18n.getLocalizedString("keySoGooODMoreDetails");
	
	if(screenwidth > 640){
		frmMBApplySoGooodComplete.lblTotalNextInstallment.text = kony.i18n.getLocalizedString("lbTotalAppliedtoNextInstallment")
	} else {
		frmMBApplySoGooodComplete.lblTotalNextInstallment.text = kony.i18n.getLocalizedString("lbTotalAppliedtoNextInstallmentIP5");
	}

	frmMBApplySoGooodComplete.btnBackHome.text = kony.i18n.getLocalizedString("CAV08_btnHome");
	frmMBApplySoGooodComplete.btnApplyMore.text = kony.i18n.getLocalizedString("keyApplyMore")
	
	frmMBApplySoGooodComplete.btnBackHome2.text = kony.i18n.getLocalizedString("CAV08_btnHome");
	frmMBApplySoGooodComplete.btnTryAgain.text = kony.i18n.getLocalizedString("btnTryAgain");
}
function performDeleteTransaction(eventobject){

	var objID = eventobject.text;
	var objIDname = objID;// 
	selectedPlan = objID.replace("btn", "")
	delimiterIndex = objID.indexOf("_", 0)
	plan = objID.substring(0, delimiterIndex);
	plan = plan.replace("btn", "")
	row = objID.substring(delimiterIndex+1, objID.length);
	
	arrayPlan = gblSoGooodMap[plan];

	tranID = arrayPlan[row]["transactionId"];
	objTran = arrayPlan[row];
	recalculateTotalValueByPlan(tranID, plan);
	frmMBApplysogooodConfirm["segmentTransactionPlan"+plan].removeAll();
	setDataSoGooodTransactionSegment(plan);
	nextMonthInstallmentValue = calculateNextMonthInstallmentPlan();
	frmMBApplysogooodConfirm["lblNextMonthInstallmentValue"].text = commaFormatted(numberFormat(nextMonthInstallmentValue, 2)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
	
	sumTransPlan = gblSoGooodTotalMap[plan];

	if(parseInt(sumTransPlan[0]["totalAmount"]) == 0) {
		delete gblSoGooodTotalMap[plan];
		delete gblSoGooodMap[plan];
		frmMBApplysogooodConfirm.flexScroll.remove(frmMBApplysogooodConfirm["flexPlan" + plan]);
		var keyPlan = Object.keys(gblSoGooodTotalMap);
		if(keyPlan.length == 0) {
			clearMap();
			getSoGooODTransactions();
		}
	} else {
		frmMBApplysogooodConfirm["lblMonthlyPaymentValue" + plan].text = commaFormatted(numberFormat(sumTransPlan[0]["totalInstallment"],2))+ " " + kony.i18n.getLocalizedString("currencyThaiBaht");
		frmMBApplysogooodConfirm["lblTotalAmountValue" + plan].text = commaFormatted(numberFormat(sumTransPlan[0]["totalAmount"],2))+ " " + kony.i18n.getLocalizedString("currencyThaiBaht")
		frmMBApplysogooodConfirm["lblTotalInterestValue" + plan].text = commaFormatted(numberFormat(sumTransPlan[0]["totalInterestVal"],2))+ " " + kony.i18n.getLocalizedString("currencyThaiBaht")
		
		var textPlan = getPlanHeaderText(plan, sumTransPlan[0]["totalTransaction"]);
		frmMBApplysogooodConfirm["lblPlan" + plan].text = textPlan;
	}
	deleteTranFromConfirm(objTran,tranID);
  	gblTranLists=[];
}

function removeFromArray(array, value) {
    var idx = array.indexOf(value);
    if (idx !== -1) {
        array.splice(idx, 1);
    }
    return array;
}

var flagDelete = false;
var remainingSoGoooDTxnDatas = [];
function deleteTranFromConfirm(objTran,tranID){
	var transactionData = gblSoGooODTransactionServiceData;
	
	if(!flagDelete){
		for (var i = 0; i < transactionData.length; i++) {
		 	if(selectedTxnIds.indexOf(transactionData[i].transactionId) < 0) {
				remainingSoGoooDTxnDatas.push(transactionData[i]);
			}
		}
	}
	flagDelete = true;
	remainingSoGoooDTxnDatas.push(objTran);
	removeFromArray(selectedTxnIds,tranID);
	gblTranLists = remainingSoGoooDTxnDatas;
}
	

gblSoGooodMap = {};
gblSoGooodTotalMap = {};
tmpGblSoGooodMap = {};
tmpGblSoGooodTotalMap = {};

function MBconfirmCalculateInterestForTxns(plan, channel){
	var inputparam = {};
	var planValues=plan.split(' ');
	
	if(planValues[1] == "3"){
		gblPlanNo = "3";
	}else if(planValues[1] == "6"){
		gblPlanNo = "6";
	}else{
		gblPlanNo = "10";
	}
	
	var selectedtrxnIds="";
	for(var i=0; i < gblCurrSelTxnIds.length;i++){
		selectedtrxnIds+= gblCurrSelTxnIds[i]+"#"
	}
	channel = channel;
	inputparam["selectedTxnIds"]=selectedtrxnIds.toString();
	inputparam["selectedPlanTerm"]=planValues[1];
	inputparam["selectedInterestValue"]=planValues[0].replace('%', ''); 
	inputparam["calculateOrApply"]= "calculate";
	showLoadingScreenBasedOnChannel(channel);
	invokeServiceSecureAsync("applySoGoodCalculateInterest", inputparam, MBcalculateInterestForTxnsCallBack);
	
}

function MBcalculateInterestForTxnsCallBack(status, resulttable){
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			var serviceHrsFlag = resulttable["soGoodServiceHoursFlag"];
            if(serviceHrsFlag != "true"){
               	dismissLoadingScreenBasedOnChannel(channel);
               	var soGooOD_StartTime = resulttable["soGooOD_StartTime"];
				var soGooOD_EndTime = resulttable["soGooOD_EndTime"];
				var errorMsg = kony.i18n.getLocalizedString("keySoGooODServiceUnavailable");
				errorMsg = errorMsg.replace("{start_time}", soGooOD_StartTime);
				errorMsg = errorMsg.replace("{end_time}", soGooOD_EndTime);
               	showAlertWithCallBack(errorMsg, kony.i18n.getLocalizedString("info"),callingFrmDetailPage);
               	return false;
            }
            dismissLoadingScreenBasedOnChannel(channel);
			tmpGblSoGooodMap = JSON.parse(JSON.stringify(gblSoGooodMap));
			tmpGblSoGooodTotalMap = JSON.parse(JSON.stringify(gblSoGooodTotalMap));
            clearFlexScroll();          
			soGoooDPlanSelectDataSet = resulttable["soGoooDPlanSelectDataSet"];
			showFrmSoGooodConfirmMB(soGoooDPlanSelectDataSet);
  			nextMonthInstallmentValue = calculateNextMonthInstallmentPlan();
      		showNextMonthInstallment(nextMonthInstallmentValue);
      		//frmMBApplysogooodConfirm.lblCreditCardNickName.text = frmMBSoGooodPlanSelect.lblCreditCardNickName.text;
      		frmMBApplysogooodConfirm.show();

		}else{
			dismissLoadingScreenBasedOnChannel(channel);
		}
		
	}else{
		dismissLoadingScreenBasedOnChannel(channel);
	}
}
function clearFlexScroll(){
  	frmMBApplysogooodConfirm.flexScroll.removeAll();
}

function showApplySoGooodConfirmationMB(plan, arrayTotalPlan){
	
	var totalTransaction = arrayTotalPlan["totalTransaction"];
	var totalInstallment = arrayTotalPlan["totalInstallment"];
	var totalInterestVal = arrayTotalPlan["totalInterestVal"];
	var totalAmount = arrayTotalPlan["totalAmount"];
	
	var textPlan = getPlanHeaderText(plan, totalTransaction);

    var lblPlan = new kony.ui.Label({
        "id": "lblPlan" + plan,
        "top": "0dp",
        "left": "5%",
        "width": "96%",
        "zIndex": 1,
        "isVisible": true,
        "text": textPlan,
        "skin": "lblGreyDBOzoMedium200"
    }, {
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {
        "textCopyable": false
    });
    var flexPlanHeader = new kony.ui.FlexContainer({
        "id": "flexPlanHeader" + plan,
        "top": "10dp",
        "left": "0dp",
        "width": "98.44%",
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "zIndex": 1,
        "isVisible": true,
        "clipBounds": true,
        "Location": "[0,10]",
        "skin": "noSkinFlexContainer",
        "layoutType": kony.flex.FREE_FORM
    }, {
        "padding": [0, 0, 0, 0]
    }, {});;
    flexPlanHeader.setDefaultUnit(kony.flex.DP)
    flexPlanHeader.add(lblPlan);
    var lblMonthlyPayment = new kony.ui.Label({
        "id": "lblMonthlyPayment" + plan,
        "top": "0dp",
        "left": "35dp",
        "zIndex": 1,
        "isVisible": true,
        "text": kony.i18n.getLocalizedString("keySoGooODMonthlyInst") + ":", 
        "skin": "lblDarkGray150"
    }, {
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {
        "textCopyable": false
    });
    var lblMonthlyPaymentValue = new kony.ui.Label({
        "id": "lblMonthlyPaymentValue" + plan,
        "top": "0dp",
        "right": "10%",
        "zIndex": 1,
        "isVisible": true,
        "text": commaFormatted(numberFormat(totalInstallment,2))+ " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
        "skin": "lblDarkGrayMedium150"
    }, {
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {
        "textCopyable": false
    });
    var flexMonthlyPayment = new kony.ui.FlexContainer({
        "id": "flexMonthlyPayment" + plan,
        "top": "0dp",
        "left": "0dp",
        "width": "100%",
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "zIndex": 1,
        "isVisible": true,
        "clipBounds": true,
        "Location": "[0,240]",
        "skin": "noSkinFlexContainer",
        "layoutType": kony.flex.FREE_FORM
    }, {
        "padding": [0, 0, 0, 0]
    }, {});;
    flexMonthlyPayment.setDefaultUnit(kony.flex.DP)
    flexMonthlyPayment.add(
    lblMonthlyPayment, lblMonthlyPaymentValue);
    var lblTotalInterest = new kony.ui.Label({
        "id": "lblTotalInterest" + plan,
        "top": "0dp",
        "left": "35dp",
        "zIndex": 1,
        "isVisible": true,
        "text": kony.i18n.getLocalizedString("keySoGooODIntCharge") + ":",
        "skin": "lblDarkGray150"
    }, {
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {
        "textCopyable": false
    });
    var lblTotalInterestValue = new kony.ui.Label({
        "id": "lblTotalInterestValue" + plan,
        "top": "0dp",
        "right": "10%",
        "zIndex": 1,
        "isVisible": true,
        "text": commaFormatted(numberFormat(totalInterestVal,2))+ " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
        "skin": "lblDarkGrayMedium150"
    }, {
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {
        "textCopyable": false
    });
    var flexTotalInterest = new kony.ui.FlexContainer({
        "id": "flexTotalInterest" + plan,
        "top": "0dp",
        "left": "0dp",
        "width": "100%",
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "zIndex": 1,
        "isVisible": true,
        "clipBounds": true,
        "Location": "[0,460]",
        "skin": "noSkinFlexContainer",
        "layoutType": kony.flex.FREE_FORM
    }, {
        "padding": [0, 0, 0, 0]
    }, {});;
    flexTotalInterest.setDefaultUnit(kony.flex.DP)
    flexTotalInterest.add(lblTotalInterest, lblTotalInterestValue);
    var lblTotalAmount = new kony.ui.Label({
        "id": "lblTotalAmount" + plan,
        "top": "0dp",
        "left": "35dp",
        "zIndex": 1,
        "isVisible": true,
        "text": kony.i18n.getLocalizedString("keySoGooODTotAmt"),
        "skin": "lblDarkGray150"
    }, {
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {
        "textCopyable": false
    });
    var lblTotalAmountValue = new kony.ui.Label({
        "id": "lblTotalAmountValue" + plan,
        "top": "0dp",
        "right": "10%",
        "zIndex": 1,
        "isVisible": true,
        "text": commaFormatted(numberFormat(totalAmount,2))+ " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
        "skin": "lblDarkGrayMedium150"
    }, {
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {
        "textCopyable": false
    });
    var flexTotalAmount = new kony.ui.FlexContainer({
        "id": "flexTotalAmount" + plan,
        "top": "0dp",
        "left": "0dp",
        "width": "100%",
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "zIndex": 1,
        "isVisible": true,
        "clipBounds": true,
        "Location": "[0,680]",
        "skin": "noSkinFlexContainer",
        "layoutType": kony.flex.FREE_FORM
    }, {
        "padding": [0, 0, 0, 0]
    }, {});;
    flexTotalAmount.setDefaultUnit(kony.flex.DP)
    flexTotalAmount.add(
    lblTotalAmount, lblTotalAmountValue);
    var btnExpand = new kony.ui.Button({
        "id": "btnExpand" + plan,
        "top": "0dp",
        "left": "35dp",
        "width": "258dp",
        "height": "29dp",
        "zIndex": 2,
        "isVisible": true,
        "text": kony.i18n.getLocalizedString("btnViewTransaction"), 
        "skin": "lblbluemedium150",
        "focusSkin": "lblbluemedium150",
        "onClick": performShowHideTransactions
    }, {
        "padding": [7, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {});
    var imgExpand = new kony.ui.Image2({
        "id": "imgExpand" + plan,
        "top": "5dp",
        "left": "35dp",
        "width": "6%",
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "zIndex": 1,
        "isVisible": true,
        "src": "hideico.png",
        "imageWhenFailed": null,
        "imageWhileDownloading": null
    }, {
        "padding": [0, 0, 0, 0],
        "imageScaleMode": constants.IMAGE_SCALE_MODE_CROP,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {});
    var flexExpand = new kony.ui.FlexContainer({
        "id": "flexExpand" + plan,
        "top": "10dp",
        "left": "0dp",
        "width": "98.44%",
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "zIndex": 1,
        "isVisible": true,
        "clipBounds": true,
        "Location": "[0,910]",
        "skin": "noSkinFlexContainer",
        "layoutType": kony.flex.FREE_FORM
    }, {
        "padding": [0, 0, 0, 0]
    }, {});;
    flexExpand.setDefaultUnit(kony.flex.DP)
    flexExpand.add(btnExpand, imgExpand);
    
    var Segment05f857764a1c648box = new kony.ui.FlexContainer({
        "id": "Segment05f857764a1c648box",
        "isVisible": true,
        "orientation": null,
        "width": "100%",
        "height": "40dp",
        "layoutType": kony.flex.FLOW_VERTICAL
    }, {
        "layoutAlignment": constants.BOX_LAYOUT_ALIGN_FROM_LEFT,
        "containerWeight": null
    }, {});
    
    var segmentTransactionPlan = new kony.ui.SegmentedUI2({
        "id": "segmentTransactionPlan" + plan,
        "top": "10dp",
        "left": "0dp",
        "width": "100%",
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "zIndex": 1,
        "isVisible": false,
        "retainSelection": false,
        "widgetDataMap": {
            "tempsegApplySoGoood": "tempsegApplySoGoood",
            "FlexContainer045a4694017834c": "FlexContainer045a4694017834c",
            "lblTotalAmount": "lblTotalAmount",
            "lblTotalAmountValue": "lblTotalAmountValue",
            "flexContainer476934975586617": "flexContainer476934975586617",
            "lblDate": "lblDate",
            "FlexContainer0a09c199c42cd4e": "FlexContainer0a09c199c42cd4e",
            "CopyFlexContainer0be6b47ea092d43": "CopyFlexContainer0be6b47ea092d43",
            "lblDescValue": "lblDescValue",
            "btntrash": "btntrash"
        },
        "data": [],
        "Location": "[0,955]",
        "rowTemplate": FlexContainer045a4694017834c,
        "widgetSkin" : "segLightBlueBorder",
        "rowSkin": "segRecNormal",
        "rowFocusSkin": "segRecNormal",
        "sectionHeaderSkin": "seg2Header",
        "separatorRequired": true,
        "separatorThickness": 1,
        "separatorColor": "cce2f100",
        "showScrollbars": false,
        "groupCells": false,
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "layoutType": kony.flex.FLOW_VERTICAL
    }, {
        "padding": [0, 0, 0, 0]
    }, {
        "indicator": constants.SEGUI_NONE,
        "enableDictionary": false,
        "showProgressIndicator": false,
        "progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
        "bounces": true,
        "editStyle": constants.SEGUI_EDITING_STYLE_NONE
    });
    Segment05f857764a1c648box.add();
    var flexPlan = new kony.ui.FlexContainer({
        "id": "flexPlan" + plan,
        "top": "0dp",
        "left": "0dp",
        "width": "100%",
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "zIndex": 1,
        "isVisible": true,
        "clipBounds": true,
        "Location": "[0,50]",
        "skin": "flexWhiteBGBlueBorder",
        "layoutType": kony.flex.FLOW_VERTICAL
    }, {
        "padding": [0, 0, 0, 0]
    }, {});;
    flexPlan.setDefaultUnit(kony.flex.DP)
    flexPlan.add(flexPlanHeader, flexMonthlyPayment, flexTotalInterest, flexTotalAmount, flexExpand, segmentTransactionPlan);
	
    frmMBApplysogooodConfirm.flexScroll.add(flexPlan);
}
function seperatePlan(){
    var flexSeperatePlan = new kony.ui.FlexContainer({
          "id": "flexSeperatePlan",
          "top": "0dp",
          "left": "0dp",
          "width": "100%",
      	  "height": "15dp",
          //"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
          "zIndex": 1,
          "isVisible": true,
          "clipBounds": true,
          "Location": "[0,740]",
          "skin": "flexLightGrey",
          "layoutType": kony.flex.FLOW_VERTICAL
    }, {
      "padding": [0, 0, 0, 0]
    }, {});;
    frmMBApplysogooodConfirm.flexScroll.add(flexSeperatePlan);
}
function showNextMonthInstallment(nextMonthInstallmentValue){
  
  //footer
	var lblNextMonthInstallment = new kony.ui.Label({
        "id": "lblNextMonthInstallment",
        "top": "0dp",
        "left": "0%",
        "zIndex": 1,
        "isVisible": true,
        "text": kony.i18n.getLocalizedString("lbTotalAppliedtoNextInstallment"),
        "skin": "lblDarkGray150"
    }, {
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {
        "textCopyable": false
    });
    var lblNextMonthInstallmentValue = new kony.ui.Label({
        "id": "lblNextMonthInstallmentValue",
        "top": "0dp",
        "right": "15%",
        "zIndex": 1,
        "isVisible": true,
        "text": commaFormatted(numberFormat(nextMonthInstallmentValue,2))+ " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
        "skin": "lblDarkGrayMedium150"
    }, {
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {
        "textCopyable": false
    });
	
	var flexNextMonthInstallment = new kony.ui.FlexContainer({
        "id": "flexNextMonthInstallment",
        "top": "10dp",
        "left": "0dp",
        "width": "100%",
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "zIndex": 1,
        "isVisible": true,
        "clipBounds": true,
        "Location": "[0,10]",
        "skin": "noSkinFlexContainer",
        "layoutType": kony.flex.FREE_FORM
    }, {
        "padding": [0, 0, 0, 0]
    }, {});;
    flexNextMonthInstallment.setDefaultUnit(kony.flex.DP)
    flexNextMonthInstallment.add(lblNextMonthInstallment, lblNextMonthInstallmentValue);
	
	var btnAddMore = new kony.ui.Button({
        "id": "btnAddMore",
        "top": "0dp",
        "left": "0dp",
        "width": "258dp",
        "height": "29dp",
        "zIndex": 2,
        "isVisible": true,
        "text": kony.i18n.getLocalizedString("lbApplyAnotherInstallment"),
        "skin": "lblbluemedium150",
        "focusSkin": "lblbluemedium150",
        "onClick" : performSelectMorePlan
    }, {
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {});
    var flexAddMore = new kony.ui.FlexContainer({
        "id": "flexAddMore",
        "top": "10dp",
        "left": "0dp",
        "width": "100%",
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "zIndex": 1,
        "isVisible": true,
        "clipBounds": true,
        "Location": "[0,10]",
        "skin": "noSkinFlexContainer",
        "layoutType": kony.flex.FREE_FORM
    }, {
        "padding": [0, 0, 0, 0]
    }, {});;
    flexAddMore.setDefaultUnit(kony.flex.DP)
    flexAddMore.add(btnAddMore);
	
    var flexFooter = new kony.ui.FlexContainer({
        "id": "flexFooter",
        "top": "0dp",
        "left": "5%",
        "width": "100%",
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "zIndex": 1,
        "isVisible": true,
        "clipBounds": true,
        "Location": "[0,740]",
        "skin": "flexLightGrey",
        "layoutType": kony.flex.FLOW_VERTICAL
    }, {
        "padding": [0, 0, 0, 0]
    }, {});;
    flexFooter.setDefaultUnit(kony.flex.DP)
    flexFooter.add(flexNextMonthInstallment,flexAddMore);
  
	frmMBApplysogooodConfirm.flexScroll.add(flexFooter);
}
function showFrmSoGooodConfirmMB(selectedTransPlan){ 
	// case click select from select installment plan
	groupTransactionForCalculateTotalValue(selectedTransPlan);
	calculateAllPlanTotalValue();
  	var keyPlan = Object.keys(gblSoGooodTotalMap);
    for(var i = 0; i < keyPlan.length; i++){
       arrayTotalPlan = gblSoGooodTotalMap[keyPlan[i]];
       showApplySoGooodConfirmationMB(keyPlan[i], arrayTotalPlan[0]);
       setDataSoGooodTransactionSegment(keyPlan[i]);
       if(keyPlan.length > 1 && i < keyPlan.length - 1){
          seperatePlan();
       }
    }
}
function setDataSoGooodTransactionSegment(selectedPlan){
  	var selectedTransPlan = gblSoGooodMap[selectedPlan];
	var data = [];
	if(selectedTransPlan.length != 0){
		for(var i = 0; i< selectedTransPlan.length; i++){
			var tranTemp = {"lblDate": {
								id: "lblDate" + selectedPlan + "_" +i,
								text: kony.i18n.getLocalizedString("keyMBTransactionDate") + ": " + selectedTransPlan[i]["txnDate"]
							},
							"lblDescValue": {
								id: "lblDescValue" + selectedPlan + "_" +i,
								text: kony.i18n.getLocalizedString("keyMBDescription") + ": " + selectedTransPlan[i]["txnDesc"]
							},
							"lblTotalAmount": {
								id: "lblTotalAmount" + selectedPlan + "_" +i,
								text: kony.i18n.getLocalizedString("keyTotalAmount")
							},
							"lblTotalAmountValue": commaFormatted(numberFormat(selectedTransPlan[i]["totalAmount"], 2)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
							"btntrash": {
								id: "btn" + selectedPlan + "_" +i,
								src: "trashicon.png",
								zIndex: 10,
								onClick: performDeleteTransaction,
								displayText: "false",
								text: "btn" + selectedPlan + "_" +i
							}}
							
			data.push(tranTemp);
		}
		frmMBApplysogooodConfirm["segmentTransactionPlan"+selectedPlan].setData(data);
	}
}
function performShowHideTransactions(eventobject){
	var objID = eventobject.id;
	var objText = frmMBApplysogooodConfirm[objID].text;
	var selectedPlan = objID;// 
	selectedPlan = selectedPlan.replace("btnExpand", "")
	if(objText == kony.i18n.getLocalizedString("btnHideTransaction")){ 
		frmMBApplysogooodConfirm["segmentTransactionPlan"+selectedPlan].isVisible = false;
		frmMBApplysogooodConfirm[objID].text = kony.i18n.getLocalizedString("btnViewTransaction"); 
		//frmMBApplysogooodConfirm["flexLine"+selectedPlan].isVisible = false;
	} else {
		frmMBApplysogooodConfirm["segmentTransactionPlan"+selectedPlan].isVisible = true;
		frmMBApplysogooodConfirm[objID].text = kony.i18n.getLocalizedString("btnHideTransaction");
		//frmMBApplysogooodConfirm["flexLine"+selectedPlan].isVisible = true;
	}
	
}
function performSelectMorePlan(){
	frmMBSoGooODTranasactions.segSoGooODTxns.removeAll();
	frmMBSoGooODTranasactions.segSoGooODTxns.selectedRowIndices = null;
	
	if(flagDelete){
		loadRemainingSoGoooDTransactionsMBDelete(gblTranLists)
	}
	else{
		loadRemainingSoGoooDTransactionsMB();
	}	
	
	disableTransactionNextButton();
	flagDelete = false;
}

function loadRemainingSoGoooDTransactionsMBDelete(tran) {
	loadSoGooODTxnsInSegment(tran);
}

function disableTransactionNextButton() {
	frmMBSoGooODTranasactions.btnNext.setEnabled(false);
	frmMBSoGooODTranasactions.btnNext.skin=btnDisabledGray;
	frmMBSoGooODTranasactions.btnNext.focusSkin=btnDisabledGray;
	frmMBSoGooODTranasactions.segSoGooODTxns.selectedRowIndices = null;
	frmMBSoGooODTranasactions.lblSel.text = kony.i18n.getLocalizedString("keyselectall");
	frmMBSoGooODTranasactions.lblSel.setEnabled(true);
}

function performCancelConfirm(){
	gblSoGooodMap = JSON.parse(JSON.stringify(tmpGblSoGooodMap));
	gblSoGooodTotalMap = JSON.parse(JSON.stringify(tmpGblSoGooodTotalMap)); 
	frmMBSoGooodPlanSelect.show();
}

function calculateAllPlanTotalValue(){
	calculateTotalValueForEachPlan("");
}		

function recalculateTotalValueByPlan(transactionId, plan){ 
	//case delete transaction, need to recal total value
	deleteTransactionFromPlan(transactionId, plan);
	calculateTotalValueForEachPlan(plan);
}
	
function clearMap(){
	gblSoGooodMap = {};
	gblSoGooodTotalMap = {};
	tmpGblSoGooodMap = {};
	tmpGblSoGooodTotalMap = {};
}
function groupTransactionForCalculateTotalValue(soGoooDPlanSelectDataSet){         
	for(var i=0; i < soGoooDPlanSelectDataSet.length;i++){
		arrayPlan = [];
		plan = soGoooDPlanSelectDataSet[i]["plan"];
		var selectedPlan ={
			"plan" : plan,
			"planId" : soGoooDPlanSelectDataSet[i]["planId"],
			"transactionId" : soGoooDPlanSelectDataSet[i]["transactionId"],
			"txnAmount" : soGoooDPlanSelectDataSet[i]["txnAmount"],
			"installment" : soGoooDPlanSelectDataSet[i]["installment"],
			"interestVal" : soGoooDPlanSelectDataSet[i]["interestVal"],
			"txnDate" : soGoooDPlanSelectDataSet[i]["txnDate"],
			"postedDate" : soGoooDPlanSelectDataSet[i]["postedDate"],
			"txnDesc": soGoooDPlanSelectDataSet[i]["txnDesc"],
			"totalAmount": soGoooDPlanSelectDataSet[i]["totalAmount"]
		};
		if(plan in gblSoGooodMap){
			arrayPlan = gblSoGooodMap[plan];
			arrayPlan.push(selectedPlan);
			
		} else {
			arrayPlan.push(selectedPlan);
		}
		gblSoGooodMap[plan] = arrayPlan;
	}
}
function findIndexOfTransactionFromPlan(transactionId, plan){
	if(plan in gblSoGooodMap){
		arrayPlan = gblSoGooodMap[plan];
		for(var i = 0; i < arrayPlan.length; i++){
			if(arrayPlan[i]["transactionId"] == transactionId){
				return i;
			}
		}
	} 
	return -1;
}
function deleteTransactionFromPlan(transactionId, plan){
	var index = -1;
	if(plan in gblSoGooodMap){
		arrayPlan = gblSoGooodMap[plan];
		index = findIndexOfTransactionFromPlan(transactionId, plan)
		if(index > -1) {
			arrayPlan.splice(index, 1);
		}
	}
}
function calculateTotalValueForEachPlan(plan){
	var totalAmount = 0;
	var totalInterestVal = 0;
	var totalInstallment = 0;
	
	if(plan == ""){
		var keyPlan = Object.keys(gblSoGooodMap);
		for(var i = 0; i < keyPlan.length; i++){
			calculateTotalValueForEachPlan(keyPlan[i]);
		}
	} else if(plan in gblSoGooodMap){
		arrayPlan = gblSoGooodMap[plan];

		for(var i = 0; i < arrayPlan.length; i++){
			totalAmount += removeCommos(arrayPlan[i]["totalAmount"]);
			totalInterestVal += removeCommos(arrayPlan[i]["interestVal"]);
			totalInstallment += removeCommos(arrayPlan[i]["installment"]);
		}
		
		if(plan in gblSoGooodTotalMap){
			delete gblSoGooodTotalMap[plan];
		}
			
		arrayTotal = [];

		var totalPlan ={
			"totalAmount" : totalAmount,
			"totalInterestVal" : totalInterestVal,
			"totalInstallment" : totalInstallment,
			"totalTransaction" : arrayPlan.length
		};

		arrayTotal.push(totalPlan);
		gblSoGooodTotalMap[plan] = arrayTotal;
	}
}
function calculateNextMonthInstallmentPlan(){
  	var keyPlan = Object.keys(gblSoGooodTotalMap);
    var nextMonthInstallmentValue = 0;
    for(var i = 0; i < keyPlan.length; i++){
       arrayTotalPlan = gblSoGooodTotalMap[keyPlan[i]];
       nextMonthInstallmentValue += arrayTotalPlan[0]["totalInstallment"];
    }
    return nextMonthInstallmentValue;
}

function MBcallApplySogoodConfirmationService(){
  try{
             if(countTryAgain < maxTryAgain){
          //countTryAgain++; //MKI, MIB-10723 Start
           
            frmMBApplySoGooodComplete.btnTryAgain.setEnabled(true);
            frmMBApplySoGooodComplete.btnTryAgain.skin = btnBlueSkin;
            frmMBApplySoGooodComplete.btnTryAgain.focusSkin = btnBlueClose;
          
                var inputparam = {};
            var keyPlan = Object.keys(gblSoGooodTotalMap);
            var selectedtrxnIds="";
            for(var i = 0; i < keyPlan.length; i++){
                var arrayPlan = gblSoGooodMap[keyPlan[i]];

                for(var j = 0; j < arrayPlan.length; j++){
                    selectedtrxnIds += arrayPlan[j]["transactionId"]+"#";
                }
            } 

            inputparam["selectedTxnIds"] = selectedtrxnIds.toString();
            inputparam["selectedPlanTerm"] = "3";
            inputparam["selectedInterestValue"] = "0"; 
            inputparam["calculateOrApply"] = "confirm";
            showLoadingScreenBasedOnChannel(channel);
          kony.print("applySoGoodCalculateInterest 2");
            invokeServiceSecureAsync("applySoGoodCalculateInterest", inputparam, MBconfirmApplySoGoodTxnsCallBack);
        }else{
                
            
            frmMBApplySoGooodComplete.btnTryAgain.skin = btnDisabledGray;
            frmMBApplySoGooodComplete.btnTryAgain.focusSkin = btnDisabledGray;
                frmMBApplySoGooodComplete.btnTryAgain.setEnabled(false);
        } //MKI, MIB-10723 End
  }
  catch(ex)
    {
      kony.print("MBcallApplySogoodConfirmationService Exception"+ ex.message);
    }
                
}

function numberFormat(val, decimalPlaces) {
	var multiplier = Math.pow(10, decimalPlaces);
    return (Math.round(val * multiplier) / multiplier).toFixed(decimalPlaces);
}

function MBconfirmApplySoGoodTxnsCallBack(status, resulttable){
	
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			var serviceHrsFlag = resulttable["soGoodServiceHoursFlag"];
             
            if(serviceHrsFlag!="true"){
				dismissLoadingScreenBasedOnChannel(channel);
               	var soGooOD_StartTime = resulttable["soGooOD_StartTime"];
			 	var soGooOD_EndTime = resulttable["soGooOD_EndTime"];
			 	var errorMsg = kony.i18n.getLocalizedString("keySoGooODServiceUnavailable");
			 	errorMsg = errorMsg.replace("{start_time}", soGooOD_StartTime);
			 	errorMsg = errorMsg.replace("{end_time}", soGooOD_EndTime);
               	showAlert(errorMsg, kony.i18n.getLocalizedString("info"));
               
                frmAccountDetailsMB.show();
               
                return false;
			}          
			frmMBApplySoGooodComplete.flxSaveCamEmail.isVisible = true;
			frmMBApplySoGooodComplete.btnRight.skin = "btnRightShare";
			frmMBApplySoGooodComplete.flexScrollContainer47826097261431.height = "87%";
		
			soGoooDPlanSelectDataSet = resulttable["soGoooDPlanSelectDataSet"];
			gblFailedTransaction=[];
		
			for(var i=0; i < soGoooDPlanSelectDataSet.length;i++){
                if(soGoooDPlanSelectDataSet[i]["status"]== "fail"){
                  var tempRecord = { 
                  	"lblTransDate": kony.i18n.getLocalizedString("keyMBTransactionDate") + ": " + soGoooDPlanSelectDataSet[i].txnDate,
        			"lblTransDesc": kony.i18n.getLocalizedString("keyMBDescription") + ": " + soGoooDPlanSelectDataSet[i].txnDesc,
        			"lblTransAmount": kony.i18n.getLocalizedString("keyTotalAmount"),
        			"lblTransAmountValue": commaFormatted(numberFormat(soGoooDPlanSelectDataSet[i].txnAmount, 2)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
        			"imgTransFail": "wrong.png"                                   
                  };
                  gblFailedTransaction.push(tempRecord);
                } else {
                	recalculateTotalValueByPlan(soGoooDPlanSelectDataSet[i].transactionId, soGoooDPlanSelectDataSet[i].plan);
                }
			}
          	if(gblFailedTransaction.length > 0){
              	frmMBApplySoGooodComplete.imgHeaderResult.src = "mes2.png"
              	//frmMBApplySoGooodComplete.lblTransactionsStatus.text = "Complete X of Y transactions."
                frmMBApplySoGooodComplete.segFailedTransactions.setData(gblFailedTransaction)
              	frmMBApplySoGooodComplete.flexTransSuccessStatus.isVisible = false;
              	frmMBApplySoGooodComplete.segFailedTransactions.isVisible = true;
                frmMBApplySoGooodComplete.flxSaveCamEmail.setVisibility(false);
            } else {
              	frmMBApplySoGooodComplete.imgHeaderResult.src = "iconcomplete.png"
           		//frmMBApplySoGooodComplete.lblTransactionsStatus.text = "All Transactions Complete."
                frmMBApplySoGooodComplete.flexTransSuccessStatus.isVisible = true;
            	frmMBApplySoGooodComplete.lblTransSuccessStatus.text = soGoooDPlanSelectDataSet.length + " Transactions financed";
            	frmMBApplySoGooodComplete.segFailedTransactions.isVisible = false;
                frmMBApplySoGooodComplete.flxSaveCamEmail.setVisibility(true);
          	}	
          	
			frmMBApplySoGooodComplete.lblTotalNextInstallmentVal.text = commaFormatted(numberFormat(resulttable["totalMonthlyInstallment"], 2)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht"); 
			dismissLoadingScreenBasedOnChannel(channel);
			frmMBApplySoGooodComplete.show();
					
		}else{
			dismissLoadingScreenBasedOnChannel(channel);
			showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
			return false;
		}
		//dismissLoadingScreenBasedOnChannel(channel);
	}	
}
function performBackFromTransactionList(){
	var keyPlan = Object.keys(gblSoGooodTotalMap);
  	if(keyPlan.length > 0){
  		frmMBApplysogooodConfirm.show();
  	} else {
  		frmMBSoGooodProdBrief.show();
  	}
}
function performShareSoGooODComplete(){

	if (frmMBApplySoGooodComplete.flxSaveCamEmail.isVisible == false) {
		frmMBApplySoGooodComplete.flxSaveCamEmail.isVisible = true;		
		frmMBApplySoGooodComplete.btnRight.skin = "btnShareNewOpenAc";
		frmMBApplySoGooodComplete.flexScrollContainer47826097261431.height = "77%";		
	} else {
		frmMBApplySoGooodComplete.flxSaveCamEmail.isVisible = true;		
		frmMBApplySoGooodComplete.btnRight.skin = "btnRightShare";
		frmMBApplySoGooodComplete.flexScrollContainer47826097261431.height = "87%";			
	}
}
function performTryAgain(){
	countTryAgain++;
	MBcallApplySogoodConfirmationService();
}