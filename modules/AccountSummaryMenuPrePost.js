

function frmAccountDetailsMBMenuPreshow() {
  	loadFunctionalModuleSync("myAccountsModule"); 
    if (gblCallPrePost) {
		//#ifdef android    
      	//mki, mib-10789 Start
       	// The below code snippet is to fix the whilte label on some screens
        frmAccountDetailsMB.FlexScrollContainer0daaa04af3fa445.showFadingEdges  = false;
        //#endif  
        //mki, mib-10789 End
      
        isMenuShown = false;
        //frmAccountDetailsMB.scrollboxMain.scrollToEnd();
		gblPreviousForm = "frmAccountDetailsMB";
		
		frmAccountDetailsMB.flxCardImage.width = "90%";
        frmAccountDetailsMB.flxCardImage.height = "41%";

        frmAccountDetailsMB.imgcard.width = "95%";
        frmAccountDetailsMB.imgcard.height = "100%";
        frmAccountDetailsMB.lblCardNumber.skin = getCardNoSkin(frmAccountDetailsMB.imgcard.src); //MKI, MIB-12137 start
    	if(frmAccountDetailsMB.imgcard.src.toLowerCase().includes("debittmbwave")){      
        	frmAccountDetailsMB.lblCardAccountName.skin = "lblBlackCard22px";
        }
        else{         
        	frmAccountDetailsMB.lblCardAccountName.skin = "lblWhiteCard22px";
		} //MKI, MIB-12137 end
        frmAccountDetailsMB.lblCardNumber.top = "53%";
        frmAccountDetailsMB.lblCardNumber.left = "10%";
        frmAccountDetailsMB.lblCardNumber.width = "80%";
        frmAccountDetailsMB.lblCardNumber.height = "15%";

        frmAccountDetailsMB.lblCardAccountName.top = "67%";
        frmAccountDetailsMB.lblCardAccountName.left = "10%";
        frmAccountDetailsMB.lblCardAccountName.width = "80%";
        frmAccountDetailsMB.lblCardAccountName.height = "15%";
        
        //#ifdef iphone
        //frmAccountDetailsMB.flxCardImage.height = "37.75%";
        //frmAccountDetailsMB.flxCardImage.width = "83%";
        //frmAccountDetailsMB.lblRibbon.top="16%";
        frmAccountDetailsMB.lblCardAccountName.top = "73%";
        frmAccountDetailsMB.lblCardNumber.top = "59%";
        frmAccountDetailsMB.lblCardAccountName.left = "12%";
        frmAccountDetailsMB.lblCardNumber.left = "12%";
        //#else
        //#endif
        //frmAccountDetailsMBPreShow.call(this);
         accountDetailsMBPreShow();
        //DisableFadingEdges.call(this, frmAccountDetailsMB);
        //currForm = frmAccountDetailsMB;
        //var deviceHght = kony.os.deviceInfo().deviceHeight;
        //var osType = kony.os.deviceInfo().name;
        //var btnskin = "btnToDoIcon";
        //var btnFocusSkin = "btnToDoIconFoc";
        //frmAccountDetailsMB.hbxOnHandClick.isVisible = true;
        //hbxHdrCommon.btnRight.skin = btnFocusSkin;
        //frmAccountDetailsMB.btnOptions.skin = btnFocusSkin;
        //frmAccountDetailsMB.imgHeaderMiddle.src = "arrowtop.png";
        //frmAccountDetailsMB.imgHeaderRight.src = "empty.png";
        //populateBenfDetails(frmAccountDetailsMB);
    }
}

function frmAccountDetailsMBMenuPostshow() {
    assignGlobalForMenuPostshow();
}

function frmAccountStatementMBMenuPreshow() {
	if(gblCallPrePost)
	{
		isMenuShown = false;
	    isSignedUser = true;
      	serviceCallTriggered=false;
	    frmAccountStatementMB.scrollboxMain.scrollToEnd();
	    clearCCValues.call(this);
	    frmAccountStatementMBPreShow.call(this);
	    viewAccntFullStatement.call(this);
	    DisableFadingEdges.call(this, frmAccountStatementMB);
	}
}

function frmAccountStatementMBMenuPostshow() {
	if(gblCallPrePost)
	{
	 	deviceInfo = kony.os.deviceInfo();
    	if (deviceInfo["name"] == "thinclient" & deviceInfo["type"] == "spa") {
	        isMenuRendered = false;
	        isMenuShown = false;
	        frmAccountStatementMB.scrollboxMain.scrollToEnd();
   		}
	}
	assignGlobalForMenuPostshow();
}


function frmAccountSummaryLandingMenuPreshow() {
	//#ifdef android    
    //mki, mib-10789 Start
    // The below code snippet is to fix the whilte label on some screens
    frmAccountSummaryLanding.vbox4751247744173.showFadingEdges  = false;
    //#endif  
	//mki, mib-10789 End
    if (gblCallPrePost) {
    registerForTimeOut.call(this);
    //frmAccountSummaryLanding.scrollboxMain.scrollToEnd();
    gblIndex = -1;
    isMenuShown = false;
    frmAccountSummaryLandingPreShow.call(this);
    //#ifdef android
	kony.print(" FPRINT SETTING isUserStatusActive TO TURE PRESHOW");
	isUserStatusActive=true;
    //#endif
    }
    initNewhbxMenu();
  	TMBBonusShortcutEligibleCheck();
}

/*
function initNewhbxMenu() {
    var locale_app = kony.i18n.getCurrentLocale();
	frmAccountSummaryLanding.MenuFlex1.setVisibility(false);
    kony.print("gblTMBBonusflag sec if 2222" + gblTMBBonusflag);
  	kony.print("GLOBAL_UV_STATUS_FLAG sec if 2222" + GLOBAL_UV_STATUS_FLAG);

	if (isRTPEnable() && isTMBWOWEnable() && GLOBAL_UV_STATUS_FLAG == "ON") {
        kony.print("inside first if 1234");
      assignDynValue(true,  true, true, true, true, "25%", "25%", "25%", "25%" ,"0%", "0%", "0%", "0%");
        if (isNotBlank(gblRTPBadgeCount) && gblRTPBadgeCount > 0) {
            frmAccountSummaryLanding.imgRTP5.setBadge(gblRTPBadgeCount + "");
        } else {
            frmAccountSummaryLanding.imgRTP5.setBadge("");
        }
        if (locale_app == "th_TH") {
            frmAccountSummaryLanding.imgQR5.src = "th_qr_med.png";
            frmAccountSummaryLanding.imgWow5.src = "wow_med.png";
            frmAccountSummaryLanding.imgRTP5.src = "th_rtp_med.png";
			frmAccountSummaryLanding.imgCardl5.src = "th_cardless_med.png";
        } else {
            frmAccountSummaryLanding.imgQR5.src = "qr_med.png";
            frmAccountSummaryLanding.imgWow5.src = "wow_med.png";
            frmAccountSummaryLanding.imgRTP5.src = "rtp_med.png";
			frmAccountSummaryLanding.imgCardl5.src = "cardless_med.png";
        }
        frmAccountSummaryLanding.vbxQR5.onClick = assignQRFlow;
        frmAccountSummaryLanding.vbxTMBWow5.onClick = onClickTMBWow;
        frmAccountSummaryLanding.vbxRTP5.onClick = onClickRTP;
		frmAccountSummaryLanding.vbxCardless5.onClick=onClickOfCardlessWithDrawMenu;
    }else if (isRTPEnable() && isTMBWOWEnable() && GLOBAL_UV_STATUS_FLAG == "OFF") {
        kony.print("inside sec if 2222");
       assignDynValue(true,  true, false, true, true, "33.3%", "0%", "33.3%", "33.3%" ,"0%", "0%", "0%", "0%");
        if (isNotBlank(gblRTPBadgeCount) && gblRTPBadgeCount > 0) {
            frmAccountSummaryLanding.imgRTP5.setBadge(gblRTPBadgeCount + "");
        } else {
            frmAccountSummaryLanding.imgRTP5.setBadge("");
        }
        if (locale_app == "th_TH") {
            frmAccountSummaryLanding.imgQR5.src = "th_qr_large.png";
            frmAccountSummaryLanding.imgWow5.src = "wow_large.png";
            frmAccountSummaryLanding.imgRTP5.src = "th_rtp_med.png";
			
        } else {
            frmAccountSummaryLanding.imgQR5.src = "qr_large.png";
            frmAccountSummaryLanding.imgWow5.src = "wow_large.png";
            frmAccountSummaryLanding.imgRTP5.src = "rtp_med.png";
			
        }
        frmAccountSummaryLanding.vbxQR5.onClick = assignQRFlow;
        frmAccountSummaryLanding.vbxTMBWow5.onClick = onClickTMBWow;
        frmAccountSummaryLanding.vbxRTP5.onClick = onClickRTP;
    } else if (!isRTPEnable() && isTMBWOWEnable() && GLOBAL_UV_STATUS_FLAG == "ON") {
        kony.print("inside third if 333");
		assignDynValue(true,  true, true, false, true, "33.3%", "33.3%", "0%", "33.3%" ,"0%", "0%", "0%", "0%");
        if (locale_app == "th_TH") {
            frmAccountSummaryLanding.imgQR5.src = "th_qr_large.png";
            frmAccountSummaryLanding.imgWow5.src = "wow_large.png";
            frmAccountSummaryLanding.imgCardl5.src = "th_cardless_large.png";
			
        } else {
            frmAccountSummaryLanding.imgQR5.src = "qr_large.png";
            frmAccountSummaryLanding.imgWow5.src = "wow_large.png";
            frmAccountSummaryLanding.imgCardl5.src = "cardless_large.png";
			
        }
        frmAccountSummaryLanding.vbxQR5.onClick = assignQRFlow;
        frmAccountSummaryLanding.vbxTMBWow5.onClick = onClickTMBWow;
        frmAccountSummaryLanding.vbxCardless5.onClick=onClickOfCardlessWithDrawMenu;
    }else if (isRTPEnable() && !isTMBWOWEnable() && GLOBAL_UV_STATUS_FLAG == "ON") {
        kony.print("inside four if 444");
		assignDynValue(true,  false, true, true, true, "0%", "33.3%", "33.3%", "33.3%" ,"0%", "0%", "0%", "0%");
        if (isNotBlank(gblRTPBadgeCount) && gblRTPBadgeCount > 0) {
            frmAccountSummaryLanding.imgRTP5.setBadge(gblRTPBadgeCount + "");
        } else {
            frmAccountSummaryLanding.imgRTP5.setBadge("");
        }
        if (locale_app == "th_TH") {
            frmAccountSummaryLanding.imgQR5.src = "th_qr_large.png";
            frmAccountSummaryLanding.imgRTP5.src = "th_rtp_med.png";
            frmAccountSummaryLanding.imgCardl5.src = "th_cardless_large.png";
			
        } else {
            frmAccountSummaryLanding.imgQR5.src = "qr_large.png";
            frmAccountSummaryLanding.imgRTP5.src = "rtp_med.png";
            frmAccountSummaryLanding.imgCardl5.src = "cardless_large.png";
			
        }
        frmAccountSummaryLanding.vbxQR5.onClick = assignQRFlow;
        frmAccountSummaryLanding.vbxRTP5.onClick = onClickRTP;
        frmAccountSummaryLanding.vbxCardless5.onClick=onClickOfCardlessWithDrawMenu;
    }else if (!isRTPEnable() && isTMBWOWEnable() && GLOBAL_UV_STATUS_FLAG == "OFF") {
        kony.print("inside five if 555");
		assignDynValue(true,  true, false, false, true, "50%", "0%", "0%", "50%" ,"0%", "0%", "0%", "0%");
        if (locale_app == "th_TH") {
            frmAccountSummaryLanding.imgQR5.src = "th_qr_xl.png";
          	frmAccountSummaryLanding.imgWow5.src = "wow_xl.png";
        } else {
            frmAccountSummaryLanding.imgQR5.src = "qr_xl.png";
          	frmAccountSummaryLanding.imgWow5.src = "wow_xl.png";
        }
        frmAccountSummaryLanding.vbxTMBWow5.onClick = onClickTMBWow;
        frmAccountSummaryLanding.vbxQR5.onClick = assignQRFlow;
    } else if (isRTPEnable() && !isTMBWOWEnable() && GLOBAL_UV_STATUS_FLAG == "OFF") {
        kony.print("inside six if 666");
		assignDynValue(true,  false, false, true, true, "0%", "0%", "50%", "50%" ,"0%", "0%", "0%", "0%");
        if (locale_app == "th_TH") {
            frmAccountSummaryLanding.imgRTP5.src = "th_rtp_med.png";
            frmAccountSummaryLanding.imgQR5.src = "th_qr_xl.png";
        } else {
            frmAccountSummaryLanding.imgQR5.src = "qr_xl.png";
            frmAccountSummaryLanding.imgRTP5.src = "rtp_med.png";
        }
        if (isNotBlank(gblRTPBadgeCount) && gblRTPBadgeCount > 0) {
            frmAccountSummaryLanding.imgRTP5.setBadge(gblRTPBadgeCount + "");
        } else {
            frmAccountSummaryLanding.imgRTP5.setBadge("");
        }
        frmAccountSummaryLanding.vbxRTP5.onClick = onClickRTP;
        frmAccountSummaryLanding.vbxQR5.onClick = assignQRFlow;
    }else if (!isRTPEnable() && !isTMBWOWEnable() && GLOBAL_UV_STATUS_FLAG == "ON") {
        kony.print("inside seven if 777");
		assignDynValue(true,  false, true, false, true, "0%", "50%", "0%", "50%" ,"0%", "0%", "0%", "0%");
        if (locale_app == "th_TH") {
            frmAccountSummaryLanding.imgCardl5.src = "th_cardless_xl.png";
            frmAccountSummaryLanding.imgQR5.src = "th_qr_xl.png";
        } else {
            frmAccountSummaryLanding.imgQR5.src = "qr_xl.png";
            frmAccountSummaryLanding.imgCardl5.src = "cardless_xl.png";
        }
        
        frmAccountSummaryLanding.vbxCardless5.onClick=onClickOfCardlessWithDrawMenu;
        frmAccountSummaryLanding.vbxQR5.onClick = assignQRFlow;
    }else if (!isRTPEnable() && !isTMBWOWEnable() && GLOBAL_UV_STATUS_FLAG == "OFF") {
        kony.print("inside eight if 888");
		assignDynValue(true,  false, false, false, true, "0%", "0%", "0%", "50%" ,"0%", "0%", "0%", "0%");
        if (locale_app == "th_TH") {

            frmAccountSummaryLanding.imgQR5.src = "th_qr_xl.png";
        } else {
            frmAccountSummaryLanding.imgQR5.src = "qr_xl.png";

        }
        frmAccountSummaryLanding.vbxQR5.left = "25%";
        frmAccountSummaryLanding.vbxQR5.onClick = assignQRFlow;

    } 
} */

function initNewhbxMenu() {
    var locale_app = kony.i18n.getCurrentLocale();
	frmAccountSummaryLanding.MenuFlex1.setVisibility(false);
    kony.print("gblTMBBonusflag sec if 2222" + gblTMBBonusflag);
  	kony.print("GLOBAL_UV_STATUS_FLAG sec if 2222" + GLOBAL_UV_STATUS_FLAG);
	frmAccountSummaryLanding.vbxQR5.onClick = assignQRFlow;
    frmAccountSummaryLanding.vbxTMBWow5.onClick = onClickTMBWow;
    frmAccountSummaryLanding.vbxRTP5.onClick = onClickRTP;
    if (isNotBlank(gblRTPBadgeCount) && gblRTPBadgeCount > 0) {
         frmAccountSummaryLanding.imgRTP5.setBadge(gblRTPBadgeCount + "");
     } else {
        frmAccountSummaryLanding.imgRTP5.setBadge("");
    }

	if (GLOBAL_UV_STATUS_FLAG == "ON") {
        kony.print("inside first if 1234");
      assignDynValue(true,  true, true, true, true, "25%", "25%", "25%", "25%" ,"0%", "0%", "0%", "0%");
        
        if (locale_app == "th_TH") {
            frmAccountSummaryLanding.imgQR5.src = "th_qr_med.png";
            frmAccountSummaryLanding.imgWow5.src = "wow_med.png";
            frmAccountSummaryLanding.imgRTP5.src = "th_rtp_med.png";
			frmAccountSummaryLanding.imgCardl5.src = "th_cardless_med.png";
        } else {
            frmAccountSummaryLanding.imgQR5.src = "qr_med.png";
            frmAccountSummaryLanding.imgWow5.src = "wow_med.png";
            frmAccountSummaryLanding.imgRTP5.src = "rtp_med.png";
			frmAccountSummaryLanding.imgCardl5.src = "cardless_med.png";
        }        
		frmAccountSummaryLanding.vbxCardless5.onClick=onClickCardLessIcon;
    }else if (GLOBAL_UV_STATUS_FLAG == "OFF") {
        kony.print("inside sec if 2222");
       assignDynValue(true,  true, false, true, true, "33.3%", "0%", "33.3%", "33.3%" ,"0%", "0%", "0%", "0%");
        
        if (locale_app == "th_TH") {
            frmAccountSummaryLanding.imgQR5.src = "th_qr_large.png";
            frmAccountSummaryLanding.imgWow5.src = "wow_large.png";
            frmAccountSummaryLanding.imgRTP5.src = "th_rtp_med.png";
			
        } else {
            frmAccountSummaryLanding.imgQR5.src = "qr_large.png";
            frmAccountSummaryLanding.imgWow5.src = "wow_large.png";
            frmAccountSummaryLanding.imgRTP5.src = "rtp_med.png";
			
        }        
    } 
}



function assignDynValue(MenuFlex1,  vbxTMBWow5, vbxCardless5, vbxRTP5, vbxQR5, vbxTMBWow5Width, vbxCardless5Width, vbxRTP5Width, vbxQR5Width ,vbxTMBWow5Left, vbxCardless5Left, vbxRTP5Left, vbxQR5Left) {

		frmAccountSummaryLanding.MenuFlex1.setVisibility(MenuFlex1);
		frmAccountSummaryLanding.vbxTMBWow5.setVisibility(vbxTMBWow5);
		frmAccountSummaryLanding.vbxCardless5.setVisibility(vbxCardless5);
		frmAccountSummaryLanding.vbxRTP5.setVisibility(vbxRTP5);
		frmAccountSummaryLanding.vbxQR5.setVisibility(vbxQR5);
        frmAccountSummaryLanding.vbxTMBWow5.width = vbxTMBWow5Width;	
		frmAccountSummaryLanding.vbxCardless5.width = vbxCardless5Width;
		frmAccountSummaryLanding.vbxRTP5.width = vbxRTP5Width;
		frmAccountSummaryLanding.vbxQR5.width = vbxQR5Width;
		frmAccountSummaryLanding.vbxTMBWow5.left = vbxTMBWow5Left;
		frmAccountSummaryLanding.vbxCardless5.left = vbxCardless5Left;
		frmAccountSummaryLanding.vbxRTP5.left = vbxRTP5Left;
		frmAccountSummaryLanding.vbxQR5.left = vbxQR5Left;
}

function setRTPBadgeCount(){
  rtp_log("@@@ In setRTPBadgeCount() @@@");
  kony.print("@@@gblRTPBadgeCount:::"+gblRTPBadgeCount);
   if (isNotBlank(gblRTPBadgeCount) && gblRTPBadgeCount > 0) {
       frmAccountSummaryLanding.imgRTP5.setBadge(gblRTPBadgeCount + "");
   } else {
      frmAccountSummaryLanding.imgRTP5.setBadge("");
   }
}

function isTMBWOWEnable(){
  if(gblTMBBonusflag == "ON" && isNotBlank(gblReferCD)) {
    return true;
  }else{
    return false;
  }
}

function onClickTMBWow(){
  gblMenuSource = "TMB WOW icon";
  kony.print("clicked TMB WOW @@");
  if (isNotBlank(gblReferCD)){
    showLoadingScreen();
    frmLoyaltylanding.show();  
  }else {
    showTMBWOWAlert();
  }
}

function onClickRTP(){
  kony.print("clicked on rtp @@"+isRTPCustomer);
  if(isRTPCustomer){
    if(checkMBUserStatus()){ // checking if transaction password is locked
     onClickRTPIconFromAccountSum();
   }
  }else{
    showPayAlert();
  }
  
 /* if(checkMBUserStatus()()){ 
     onClickRTPIconFromAccountSum();
   }else{
     showPayAlert();
   } */
  //frmAccountSummaryLanding.hbxlist2.setVisibility(false);
}

function onClickCardLessIcon(){
   loadFunctionalModuleSync("cardlessModule");   
   onClickOfCardlessWithDrawMenu();
}

function showPayAlert(){
  try{
  kony.print("entered on rtp @@");
  frmAccountSummaryLanding.btnClose.skin = "btnCloseBluePopup";
  //frmAccountSummaryLanding.btnClose.focusSkin = "btnblueGreybg";
  frmAccountSummaryLanding.btnClose.onClick = closePayAlert;
  frmAccountSummaryLanding.flxClose.onClick = closePayAlert;
  frmAccountSummaryLanding.imgPopup.src = "pay_alert.png";
  frmAccountSummaryLanding.lblPopup.text = kony.i18n.getLocalizedString("PayAlert_Heading");
  frmAccountSummaryLanding.lblContent.text = kony.i18n.getLocalizedString("PayAlert_Content");
  frmAccountSummaryLanding.btnRegister.text = kony.i18n.getLocalizedString("PayAlert_Register");
  frmAccountSummaryLanding.btnRegister.skin = "btnBlue28PxRounded";
  frmAccountSummaryLanding.btnRegister.onClick = menuSetUserIDonClick;
  frmAccountSummaryLanding.btnLater.text  = kony.i18n.getLocalizedString("MF_lbl_DoItLater");
  //frmAccountSummaryLanding.btnRegister.onClick = registerPromptPay;
  frmAccountSummaryLanding.btnLater.onClick = closePayAlert;
  if(frmAccountSummaryLanding.btnLater.isVisible === false){
    frmAccountSummaryLanding.btnLater.setVisibility(true);
  }
  kony.print("before UI on rtp @@");
  //dynamic UI changes
  frmAccountSummaryLanding.flxPopup.skin = "flexWhiteBG";
  frmAccountSummaryLanding.lblPopup.skin = "lblBlack200DBOzoneX";
  frmAccountSummaryLanding.lblContent.skin = "lblBlackMedium171Ozone";
  frmAccountSummaryLanding.FlexMain.setVisibility(false);
  frmAccountSummaryLanding.flxPopup.setVisibility(true);
  kony.print("after UI on rtp @@");
  }catch(e){
    kony.print("error in showPayAlert "+e);
  }
}

function showTMBWOWAlert(){
  try{
  kony.print("entered on TMB WOW @@");
  frmAccountSummaryLanding.btnClose.skin = "btnCloseWhitePopup";
//  frmAccountSummaryLanding.btnClose.focusSkin = "btnBlueSkin";
  frmAccountSummaryLanding.btnClose.onClick = closePayAlert;
  frmAccountSummaryLanding.flxClose.onClick = closePayAlert;
  frmAccountSummaryLanding.imgPopup.src = "tmb_wow.png";
  frmAccountSummaryLanding.lblPopup.text = kony.i18n.getLocalizedString("WOW_Heading");
  frmAccountSummaryLanding.lblContent.text = kony.i18n.getLocalizedString("WOW_Content");
  frmAccountSummaryLanding.btnRegister.text = kony.i18n.getLocalizedString("WOW_Button");
  frmAccountSummaryLanding.btnRegister.skin = "btnBlueBGNoRoundWhiteBr";
  frmAccountSummaryLanding.btnLater.setVisibility(false);
  frmAccountSummaryLanding.btnRegister.onClick = closePayAlert;
  kony.print("before UI on TMB WOW @@");
  //dynamic UI changes
  frmAccountSummaryLanding.flxPopup.skin = "flxBlue";
  frmAccountSummaryLanding.lblPopup.skin = "lblWhite200";
  frmAccountSummaryLanding.lblContent.skin = "lblWhiteMedium171Ozone";
  frmAccountSummaryLanding.FlexMain.setVisibility(false);
  frmAccountSummaryLanding.flxPopup.setVisibility(true);
  kony.print("after UI TMB WOW @@");
  }catch(e){
    kony.print("error in showTMBWOWAlert "+e);
  }
}

function closePayAlert(){
  kony.print("closePayAlert @@");
  frmAccountSummaryLanding.flxPopup.setVisibility(false);
  frmAccountSummaryLanding.FlexMain.setVisibility(true);
}