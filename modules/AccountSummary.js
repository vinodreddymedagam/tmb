function accountSummaryInit(){
  try{
    kony.print("@@@ In accountSummaryInit() @@@");
    accountSummaryActions();
    frmAccountSummary.preShow = preShowFrmAccountSummarynew;
  }catch(e){
    kony.print("@@@ In accountSummaryInit() Exception:::"+e);
  }
}

function accountSummaryActions(){
  try{
    kony.print("@@@ In accountSummaryActions() @@@");
    if (isNotBlank(gblDeviceInfo["name"]) && gblDeviceInfo["name"] == "android"){
      frmAccountSummary.onDeviceBack = onDeviceBackAS;
    }
    frmAccountSummary.btnMenu.onClick = handleMenuBtn;
    frmAccountSummary.flxTMBWow.onClick = onClickTMBWownew;
    frmAccountSummary.flxPayAlert.onClick = onClickRTPnew;
    frmAccountSummary.flxQR.onClick = assignQRFlow;
    frmAccountSummary.flxWithdrawl.onClick = onClickCardLessIcon;

    var setSwipe = {fingers:1, swipedistance:75, swipevelocity:105};
    frmAccountSummary.segAccountDetails.rowTemplate.addGestureRecognizer(2, setSwipe, onSwipeAccountSummarySeg);
    frmAccountSummary.segAccountDetails.onRowClick = onRowClickSegAccSummary;
    
    frmAccountSummary.btnAddaccount.onClick = menuOpenNewAccountonClick;
    frmAccountSummary.btnGetCreditCard.onClick = onClickCreditCardsfromAccSummary;
    frmAccountSummary.btnGetLoan.onClick = onClickPersonalLoanfromAccSummary;
    frmAccountSummary.btnStartMutualFund.onClick = onClickMutualFundfromAccSummary;
    frmAccountSummary.btnInsurance.onClick = onClickPersonalLoan;
    
    frmAccountSummary.btnTMBID.onClick = onClickOfTMBID;
    
    //Actions for Change Profile Pic.
    frmAccountSummary.flxProfilePic.onClick = changeASProfilePic;
    frmAccountSummary.flxTakeProfilePic.onClick = dismissASProfileChange;
    frmAccountSummary.btnCamera.onCapture = cameraPermissionCheck;
    frmAccountSummary.flxImport.onClick = openImageGallery;
    frmAccountSummary.flxDelete.onClick = useDefaultImage; 
    frmAccountSummary.btnCancel.onClick = dismissASProfileChange;
  }catch(e){
    kony.print("@@@ In accountSummaryActions() Exception:::"+e);
  }
}

function changeASProfilePic(){
  frmAccountSummary.flxTakeProfilePic.setVisibility(true);
}

function dismissASProfileChange(){
  frmAccountSummary.flxTakeProfilePic.setVisibility(false);
}

function updateASProfileImageCallBack(status, resulttable){
  try{
    kony.print("@@@ In updateASProfileImageCallBack() @@@");
    kony.print("@@@Status:::"+status+"||Opstatus:::"+resulttable["opstatus"]);
    if(status == 400){
      if(resulttable["opstatus"] == 0){
        dismissASProfileChange();
        setProfilePicBalance();
        dismissLoadingScreen();
      }
    }
  }catch(e){
    kony.print("@@@ In updateASProfileImageCallBack() Exception:::"+e);
  }
}

function onPullAccountSummary(){
  try{
    kony.print("@@@ onPullAccountSummary() @@@");
    showLoadingScreen();
    var inputparam = {};
	inputparam["activationCompleteFlag"] = "true";
    if(gblChangeLanguage == true){
      if (kony.i18n.getCurrentLocale() == "en_US") {
        inputparam["languageCd"] = "EN";
      } else {
        inputparam["languageCd"] = "TH";
      }
    }
    isLoginFlow = false;
    invokeServiceSecureAsync("customerAccountInquiry", inputparam, onSuccessPullAccountSummary);
  }catch(e){
    kony.print("@@@ onPullAccountSummary() Exeption:::"+e);
  }
}

function onSuccessPullAccountSummary(status, resultTable){
  try{
    kony.print("@@@ onSuccessPullAccountSummary() @@@");
    kony.print("@@@ Customer Name:::"+resultTable["totalBal"]);
    if(status == 400){
      if(isNotBlank(resultTable["opstatus"]) && resultTable["opstatus"] == 0){
        gblAccountSummaryData = resultTable;
        setAccountSummaryBalance(gblAccountSummaryData["totalBal"]);
        setAccountSummaryData();
        dismissLoadingScreen();
      }
    }
  }catch(e){
    kony.print("@@@ onSuccessPullAccountSummary() Exeption:::"+e);
  }
}

function setASBGImageOrColor(asBGImageOrColor){
  try{
    kony.print("@@@ setASBGImageOrColor() @@@");
    kony.print("@@@asBGImageOrColor:::"+asBGImageOrColor);
    if(isNotBlank(asBGImageOrColor) && asBGImageOrColor == "Y"){
      frmAccountSummary.flxProileDeatils.skin = "slFbox";
      frmAccountSummary.imgBg.setVisibility(true);
      frmAccountSummary.imgBg.src = getSpecialOccasionImageForAS();
    }else{
      frmAccountSummary.imgBg.setVisibility(false);
      frmAccountSummary.flxProileDeatils.skin = "flexHeaderSkin";
    }
  }catch(e){
    kony.print("@@@ setASBGImageOrColor() Exeption:::"+e);
  }
}

function setProfilePicBalance() { //createNoOfRowsDynamically
  try{
    kony.print("@@@ setProfilePicBalance() @@@");
    //Setting Account Summary Balance Amount.
    setAccountSummaryBalance(gblAccountSummaryData["totalBal"]);
    
	if (kony.i18n.getCurrentLocale() == "en_US"){
      frmAccountSummary.lblAccntHolderName.text = gblCustomerName
	}else{
      frmAccountSummary.lblAccntHolderName.text = gblCustomerNameTh;
	}
    
    var base64ProfilePic = kony.store.getItem("cachedProfilePic");
    var allowProfilePicUpdate = false;
    var profilePicSavedVersion = getCacheData("gblProfilePicSavedVersion");
    if(gblProfilePicCurrentVersion !== null && profilePicSavedVersion !== null && gblProfilePicCurrentVersion > profilePicSavedVersion){
      allowProfilePicUpdate = true;
      setCacheData("gblProfilePicSavedVersion", gblProfilePicCurrentVersion);
    }

    if(allowProfilePicUpdate || !isNotBlank(base64ProfilePic)){
      frmAccountSummary.imgProfile.src="avatar1.png";
      kony.print("@@@--- Loading Profile Pic ---@@@");
      loadProfilePic();
    }else{
      frmAccountSummary.imgProfile.base64 = base64ProfilePic;
    }
    gblRefreshGraph = false;
  }catch(e){
    kony.print("@@@ setProfilePicBalance() Exception:::"+e);
  }
}

function setAccountSummaryBalance(totalBalance) {
  try{
    kony.print("@@@ setAccountSummaryBalance() @@@");
    kony.print("@@@ totalBalance::::"+totalBalance);
    //Setting Balnce Amount.
    frmAccountSummary.lblThaiCurrencySymbol.text = kony.i18n.getLocalizedString("currencyThaiBaht");
    if(isNotBlank(totalBalance)){
      var intpart = formatAmountForDashboard(totalBalance,"intpart");
      var decpart = formatAmountForDashboard(totalBalance,"decimalpart");
      kony.print("@@@ After split--TotalBalance:::"+intpart+"|||decpart::"+decpart);
      frmAccountSummary.lblBalanceValue.text = intpart;
      frmAccountSummary.lblBalanceafterDecimalval.text= decpart;
    }else{
      frmAccountSummary.lblBalanceValue.text = "0.";
      frmAccountSummary.lblBalanceafterDecimalval.text= "00";
    }
  }catch(e){
    kony.print("@@@ setAccountSummaryBalance() Exception:::"+e);
  }
}

function setAccountSummaryData(){
  try{
    kony.print("@@@ In setAccountSummaryData() @@@");
    var locale = kony.i18n.getCurrentLocale();
    var accountSection = [{lblHeaderName:{text: kony.i18n.getLocalizedString("MB_AcctH")}, btnHeaderMore: {text: kony.i18n.getLocalizedString("MB_AcctMorelink1"), "onClick": menuOpenNewAccountonClick}}];
    var creditCardSection = [{lblHeaderName:{text: kony.i18n.getLocalizedString("MB_CCH")}, btnHeaderMore: {text: kony.i18n.getLocalizedString("MB_CCMorelink1"), "onClick": onClickCreditCardsfromAccSummary}}];
    var loanSection = [{lblHeaderName:{text: kony.i18n.getLocalizedString("MB_LoanH")}, btnHeaderMore: {text: kony.i18n.getLocalizedString("MB_LoanMorelink1"), "onClick": onClickPersonalLoanfromAccSummary}}];
    var mutualFundSection = [{lblHeaderName:{text: kony.i18n.getLocalizedString("MB_MFH")}, btnHeaderMore: {text: kony.i18n.getLocalizedString("MB_MFMorelink1"), "onClick": onClickMutualFundfromAccSummary}}];
    var insuranceSection = [{lblHeaderName:{text: kony.i18n.getLocalizedString("MB_BAH")}/*, btnHeaderMore: {text: kony.i18n.getLocalizedString("MB_BAMorelink1"), "onClick": onClickPersonalLoan}*/}];
    var isAccountsAvail = false, isCreditCardAvail =false, isLonasAvail = false, isMutualFundAvail = false, isInsuranceAvail = false;
    
    var accountSummaryData = [];
    var showTransferIcon = false;
    var showPaybillIcon = false;
    var showTopupIcon = false;
    var showPaymycreditIcon = false;
    var showPaymyloanIcon = false;
    var showCardlessIcon = false;
    var resultSet = gblAccountSummaryData;
    var imageIcon;
      if(resultSet != null && resultSet != "" && resultSet != undefined){
        var accounts =  resultSet["depositAccountDS"];
        //For Accounts Rows
        if(accounts != null && accounts != "" && accounts != undefined){
          var accountsData = [];
          var nickName = "", productName = "";
          var isAllFreeAccountCount = 0;
          for(var i = 0; i < accounts.length; i++){
            isAccountsAvail = true;
            if (locale == "th_TH") {
              productName = accounts[i]["ProductNameThai"];
            } else if (locale == "en_US") {
              productName = accounts[i]["ProductNameEng"];
            }
            nickName = getAccountNickName(accounts[i]["acctNickName"], productName, accounts[i]["accId"]);
           
            var remainFee = "", isRemainFee = false;
            if (isNotBlank(accounts[i]["VIEW_NAME"]) && accounts[i]["VIEW_NAME"] == "PRODUCT_CODE_NOFEESAVING_TABLE") {
              isRemainFee = true;
              remainFee =  kony.i18n.getLocalizedString("keyRemainingFree")+" "+accounts[i]["remainingFee"];
            } else {
              isRemainFee = false;
              remainFee = "";
            }
            //Checking All Free Accounts.
            if(isNotBlank(accounts[i]["productID"]) && accounts[i]["productID"] == "225"){
              isAllFreeAccountCount = isAllFreeAccountCount + 1;
            }
            showTransferIcon = getValueOfIcons(accounts[i]["Transfer"]);
            showPaybillIcon = getValueOfIcons(accounts[i]["Paybill"]);
            showTopupIcon = getValueOfIcons(accounts[i]["Topup"]);
            showCardlessIcon = getValueOfIcons(accounts[i]["cardlessWithdraw"]);
            var fwidths= setIconsSizeandValues("90",showTransferIcon,showPaybillIcon,showTopupIcon,showCardlessIcon);
           	
            var intpart = formatAmountForDashboard(accounts[i]["availableBalDisplay"],"intpart");
            var decpart = formatAmountForDashboard(accounts[i]["availableBalDisplay"],"decimalpart");
            var accoutsRowData = {
              imageIcon: loadAccntProdImages(accounts[i]["ICON_ID"]),
              lblAccountName: {text: nickName, isVisible: true},
              lblAccountNumber: accounts[i]["accountNoFomatted"],
              lblRemainTrans: {text: remainFee, isVisible: isRemainFee},
              lblAmount: intpart ,//accounts[i]["availableBalDisplay"],
              lblAmountDecvalue: decpart,
              flxTransfer: {isVisible: showTransferIcon, width: fwidths, onClick: onclickgetTransferFromAccountsFromAccSmry}, 
              flxPayBill: {isVisible: showPaybillIcon, width: fwidths, onClick: onclickgetBillPaymentFromAccountsFromAccSmry}, 
              flxTopUp: {isVisible: showTopupIcon, width: fwidths, onClick: onclickgetTopUpFromAccountsFromAccSmry}, 
              flxWithdrawl: {isVisible: showCardlessIcon, width: fwidths, onClick: onClickCardLessSwipeBtn},
              flxASRowDetails : {left :"0%"},
              flxIcon: {"highlightOnParentFocus":true, "highlightedSkin":"skinFlexOnFocusAccSumm"},
			  flxDetails: {"highlightOnParentFocus":true, "highlightedSkin":"skinFlexOnFocusAccSumm"},
              flxAllFreeAccount: {isVisible: false},
              lblBalText: kony.i18n.getLocalizedString("MF_RD_lbl_balance"),
              imgTansfer: "transfershortcut.png",
              lblTransfer: kony.i18n.getLocalizedString("Transfer"), 
              imgPaybill: "paybillshortcut.png",
              lblPayBill: kony.i18n.getLocalizedString("PayBill"), 
              imgTopUp: "topupshortcut.png",
              lblTopUp: kony.i18n.getLocalizedString("TopUp"), 
              imgWithdrawl: "cardlessshortcut.png", 
              lblWithdrawl: kony.i18n.getLocalizedString("Withdraw"),
              accId: checkStringVal(accounts[i]["accId"]),
              acctStatus: checkStringVal(accounts[i]["acctStatusCode"]),
              partyAcctRelDesc: checkStringVal(accounts[i]["partyAcctRelDesc"]),
              accType: checkStringVal(accounts[i]["accType"]),
              viewName : checkStringVal(accounts[i]["VIEW_NAME"]),
              persionlizeAcctId: checkStringVal(accounts[i]["persionlizeAcctId"]),
              acctNickName: checkStringVal(accounts[i]["acctNickName"]),
              ProductImg: checkStringVal(accounts[i]["ProductImg"]),
              ICON_ID: checkStringVal(accounts[i]["ICON_ID"]),
              allowRedeemPoints: checkStringVal(accounts[i]["allowRedeemPoints"]),
              allowApplySoGoood: checkStringVal(accounts[i]["allowApplySoGoood"]),
              ProductNameEng: checkStringVal(accounts[i]["ProductNameEng"]),
              BranchNameEh: checkStringVal(accounts[i]["BranchNameEh"]),
              Type_EN: checkStringVal(accounts[i]["Type_EN"]),
              ProductNameThai: checkStringVal(accounts[i]["ProductNameThai"]),
              BranchNameTh: checkStringVal(accounts[i]["BranchNameTh"]),
              Type_TH: checkStringVal(accounts[i]["Type_TH"]),
              allowCardTypeForRedeem: checkStringVal(accounts[i]["allowCardTypeForRedeem"]),
              bonusPointExpiryDate: checkStringVal(accounts[i]["bonusPointExpiryDate"]),
              productID: checkStringVal(accounts[i]["productID"]),
              PayMyCreditCard: checkStringVal(accounts[i]["PayMyCreditCard"]),
			  PayMyLoan: checkStringVal(accounts[i]["PayMyLoan"]),
              bankCD: checkStringVal(accounts[i]["bankCD"]),
			  fiident: checkStringVal(accounts[i]["fiident"]),
              defaultCurrentNickNameEN:checkStringVal(accounts[i]["defaultCurrentNickNameEN"]),
              defaultCurrentNickNameTH:checkStringVal(accounts[i]["defaultCurrentNickNameTH"]),
              remainingFee:checkStringVal(accounts[i]["remainingFee"]),
              openingMethod:checkStringVal(accounts[i]["openingMethod"]) 
              
            };
            accountsData.push(accoutsRowData);
          }//for loop end.
          kony.print("@@@isAllFreeAccountCount:::"+isAllFreeAccountCount);
          if(isAllFreeAccountCount == 0){ 
            var allFreeAccRow = {flxASRowDetails: {isVisible: false}, flxActionButtons: {isVisible: false}, flxAllFreeAccount: {isVisible: true}, lblAllFree: kony.i18n.getLocalizedString("MB_AFBanner"), allFreeAccFlag: "Y"};
            accountsData.push(allFreeAccRow);
          }
          accountSection.push(accountsData);
        }//End of Accounds data.sss
        
        kony.print("@@@ Credit Cards....!");
        //For CreditCard Rows
        var crediCards =  resultSet["creditCardAccountDS"];
        if(crediCards != null && crediCards != "" && crediCards != undefined){
          var creditCardData = [];
          var nickName = "", productName = "";
          for(var i = 0; i < crediCards.length; i++){
            isCreditCardAvail = true;
            if (locale == "th_TH") {
              productName = crediCards[i]["ProductNameThai"];
            } else if (locale == "en_US") {
              productName = crediCards[i]["ProductNameEng"];
            }
            nickName = getAccountNickName(crediCards[i]["acctNickName"], productName, crediCards[i]["accId"]);
            showTransferIcon = getValueOfIcons(crediCards[i]["Transfer"]);
            showPaybillIcon = getValueOfIcons(crediCards[i]["PayMyCreditCard"]);
             if(isNotBlank(crediCards[i]["allowApplySoGoood"]) && crediCards[i]["allowApplySoGoood"] == "1"){
               showTopupIcon = getValueOfIcons("Y");
             }else{
               showTopupIcon = getValueOfIcons("N");
             }
            if(isNotBlank(crediCards[i]["allowRedeemPoints"]) && crediCards[i]["allowRedeemPoints"] == "1"){
               showCardlessIcon = getValueOfIcons("Y");
             }else{
               showCardlessIcon = getValueOfIcons("N");
             }
            var intpart = formatAmountForDashboard(crediCards[i]["availableCreditBalDisplay"],"intpart");
            var decpart = formatAmountForDashboard(crediCards[i]["availableCreditBalDisplay"],"decimalpart");
            var fwidths= setIconsSizeandValues("90",showTransferIcon,showPaybillIcon,showTopupIcon,showCardlessIcon);
            var creditCardRowData = {
              imageIcon: loadAccntProdImages(crediCards[i]["ICON_ID"]),
              lblAccountName: {text: nickName, isVisible: true},
              lblAccountNumber: "",//crediCards[i]["accountNoFomatted"],
              lblRemainTrans: {isVisible: false},
              lblAmount: intpart,
              lblAmountDecvalue: decpart,
              lblBalText: kony.i18n.getLocalizedString("CAV02_keyAvailToSpend"),
              flxTransfer: {isVisible: showTransferIcon, width: fwidths, onClick: onclickgetTransferFromAccountsFromAccSmry}, 
              flxPayBill: {isVisible: showPaybillIcon, width: fwidths, onClick: onClickCreditAcctSummary}, 
              flxTopUp: {isVisible: showTopupIcon, width: fwidths, onClick: onClickApplySoGooODLink}, 
              flxWithdrawl: {isVisible: showCardlessIcon, width: fwidths, onClick: checkMBCustStatus},
              flxASRowDetails : {left :"0%"},
              flxIcon: {"highlightOnParentFocus":true, "highlightedSkin":"skinFlexOnFocusAccSumm"},
			  flxDetails: {"highlightOnParentFocus":true, "highlightedSkin":"skinFlexOnFocusAccSumm"},
              flxAllFreeAccount: {isVisible: false},
              imgTansfer: "transfershortcut.png",
              lblTransfer: kony.i18n.getLocalizedString("Transfer"), 
              imgPaybill: "paybillshortcut.png",
              lblPayBill: kony.i18n.getLocalizedString("PayBill"), 
              imgTopUp: "sogoodshortcut.png",
              lblTopUp: kony.i18n.getLocalizedString("applySoGood"), 
              imgWithdrawl: "redemptionshotrcut.png", 
              lblWithdrawl: kony.i18n.getLocalizedString("Redemtion"),
              accId: checkStringVal(crediCards[i]["accId"]),
              acctStatus: checkStringVal(crediCards[i]["productID"]),
              partyAcctRelDesc: checkStringVal(crediCards[i]["partyAcctRelDesc"]),
              accType: checkStringVal(crediCards[i]["accType"]),
              viewName : checkStringVal(crediCards[i]["VIEW_NAME"]),
              persionlizeAcctId: checkStringVal(crediCards[i]["persionlizeAcctId"]),
              acctNickName: checkStringVal(crediCards[i]["acctNickName"]),
              ProductImg: checkStringVal(crediCards[i]["ProductImg"]),
              ICON_ID: checkStringVal(crediCards[i]["ICON_ID"]),
              allowRedeemPoints: checkStringVal(crediCards[i]["allowRedeemPoints"]),
              allowApplySoGoood: checkStringVal(crediCards[i]["allowApplySoGoood"]),
              ProductNameEng: checkStringVal(crediCards[i]["ProductNameEng"]),
              BranchNameEh: checkStringVal(crediCards[i]["BranchNameEh"]),
              Type_EN: checkStringVal(crediCards[i]["Type_EN"]),
              ProductNameThai: checkStringVal(crediCards[i]["ProductNameThai"]),
              BranchNameTh: checkStringVal(crediCards[i]["BranchNameTh"]),
              Type_TH: checkStringVal(crediCards[i]["Type_TH"]),
              allowCardTypeForRedeem: checkStringVal(crediCards[i]["allowCardTypeForRedeem"]),
              bonusPointExpiryDate: checkStringVal(crediCards[i]["bonusPointExpiryDate"]),
              productID: checkStringVal(crediCards[i]["productID"]),
              PayMyCreditCard: checkStringVal(crediCards[i]["PayMyCreditCard"]),
			  PayMyLoan: checkStringVal(crediCards[i]["PayMyLoan"]),
              bankCD: checkStringVal(crediCards[i]["bankCD"]),
			  fiident: checkStringVal(crediCards[i]["fiident"]),
              defaultCurrentNickNameEN:checkStringVal(crediCards[i]["defaultCurrentNickNameEN"]),
              defaultCurrentNickNameTH:checkStringVal(crediCards[i]["defaultCurrentNickNameTH"]),
              cardRefId:checkStringVal(crediCards[i]["cardRefId"]),
              bonusPointAvail:checkStringVal(crediCards[i]["bonusPointAvail"]),
              creditCardNumber:checkStringVal(crediCards[i]["accountNoFomatted"])
            };
            creditCardData.push(creditCardRowData);
          }
          creditCardSection.push(creditCardData);
        }//End of CreditCards data.
        
        kony.print("@@@ Loans....!");
        //For Loans Data
        var Loans =  resultSet["loanAccountDS"];
        if(Loans != null && Loans != "" && Loans != undefined){
          var LoanData = [];
          var nickName = "", productName = "";
          for(var i = 0; i < Loans.length; i++){
            isLonasAvail = true;
            if (locale == "th_TH") {
              productName = Loans[i]["ProductNameThai"];
            } else if (locale == "en_US") {
              productName = Loans[i]["ProductNameEng"];
            }
            nickName = getAccountNickName(Loans[i]["acctNickName"], productName, Loans[i]["accId"]);
            showTransferIcon = getValueOfIcons(Loans[i]["Transfer"]);
            showPaybillIcon = getValueOfIcons(Loans[i]["PayMyLoan"]);
            showTopupIcon = getValueOfIcons(Loans[i]["Topup"]);
            showCardlessIcon = getValueOfIcons(Loans[i]["cardlessWithdraw"]);
            var fwidths= setIconsSizeandValues("90",showTransferIcon,showPaybillIcon,showTopupIcon,showCardlessIcon);
            var intpart = formatAmountForDashboard(Loans[i]["availableBalDisplay"],"intpart");
            var decpart = formatAmountForDashboard(Loans[i]["availableBalDisplay"],"decimalpart");

            var LoanRowData = {
              imageIcon: loadAccntProdImages(Loans[i]["ICON_ID"]),
              lblAccountName: {text: nickName, isVisible: true},
              lblAccountNumber: "", //Loans[i]["accountNoFomatted"],
              lblRemainTrans: {isVisible: false},
              lblAmount: intpart , 	//Loans[i]["availableBalDisplay"],
              lblAmountDecvalue: decpart,
              lblBalText: kony.i18n.getLocalizedString("keyMBOutstandingstmt"),
              flxTransfer: {isVisible: showTransferIcon, width: fwidths, onClick: onclickgetTransferFromAccountsFromAccSmry}, 
              flxPayBill: {isVisible: showPaybillIcon, width: fwidths, onClick: onClickLoanAcctSummary}, 
              flxTopUp: {isVisible: showTopupIcon, width: fwidths, onClick: onclickgetTopUpFromAccountsFromAccSmry}, 
              flxWithdrawl: {isVisible: showCardlessIcon, width: fwidths, onClick: onClickCardLessSwipeBtn},
              flxASRowDetails : {left :"0%"},
              flxIcon: {"highlightOnParentFocus":true, "highlightedSkin":"skinFlexOnFocusAccSumm"},
			  flxDetails: {"highlightOnParentFocus":true, "highlightedSkin":"skinFlexOnFocusAccSumm"},
              flxAllFreeAccount: {isVisible: false},
              imgTansfer: "transfershortcut.png",
              lblTransfer: kony.i18n.getLocalizedString("Transfer"), 
              imgPaybill: "paybillshortcut.png",
              lblPayBill: kony.i18n.getLocalizedString("PayBill"), 
              imgTopUp: "topupshortcut.png",
              lblTopUp: kony.i18n.getLocalizedString("TopUp"), 
              imgWithdrawl: "cardlessshortcut.png", 
              lblWithdrawl: kony.i18n.getLocalizedString("Withdraw"),
              accId: checkStringVal(Loans[i]["accId"]),
              acctStatus: checkStringVal(Loans[i]["productID"]),
              partyAcctRelDesc: checkStringVal(Loans[i]["partyAcctRelDesc"]),
              accType: checkStringVal(Loans[i]["accType"]),
              viewName : checkStringVal(Loans[i]["VIEW_NAME"]),
              persionlizeAcctId: checkStringVal(Loans[i]["persionlizeAcctId"]),
              acctNickName: checkStringVal(Loans[i]["acctNickName"]),
              ProductImg: checkStringVal(Loans[i]["ProductImg"]),
              ICON_ID: checkStringVal(Loans[i]["ICON_ID"]),
              allowRedeemPoints: checkStringVal(Loans[i]["allowRedeemPoints"]),
              allowApplySoGoood: checkStringVal(Loans[i]["allowApplySoGoood"]),
              ProductNameEng: checkStringVal(Loans[i]["ProductNameEng"]),
              BranchNameEh: checkStringVal(Loans[i]["BranchNameEh"]),
              Type_EN: checkStringVal(Loans[i]["Type_EN"]),
              ProductNameThai: checkStringVal(Loans[i]["ProductNameThai"]),
              BranchNameTh: checkStringVal(Loans[i]["BranchNameTh"]),
              Type_TH: checkStringVal(Loans[i]["Type_TH"]),
              allowCardTypeForRedeem: checkStringVal(Loans[i]["allowCardTypeForRedeem"]),
              bonusPointExpiryDate: checkStringVal(Loans[i]["bonusPointExpiryDate"]),
              productID: checkStringVal(Loans[i]["productID"]),
              PayMyCreditCard: checkStringVal(Loans[i]["PayMyCreditCard"]),
			  PayMyLoan: checkStringVal(Loans[i]["PayMyLoan"]),
              bankCD: checkStringVal(Loans[i]["bankCD"]),
			  fiident: checkStringVal(Loans[i]["fiident"])
             };
            LoanData.push(LoanRowData);
          }
          loanSection.push(LoanData);
        }//End of Loans Data
        kony.print("@@@ Mutual Funds....!");
        //Mutula Funds data
        if(isNotBlank(resultSet["mfAccountFlag"]) && resultSet["mfAccountFlag"] == "true"){
          isMutualFundAvail = true;
          var mutualFundData = [];
          var mfAmount = "";
          if(isNotBlank(resultSet["mfTotalAmount"])){
            mfAmount = resultSet["mfTotalAmount"];
          }
            var intpart = formatAmountForDashboard(mfAmount,"intpart");
            var decpart = formatAmountForDashboard(mfAmount,"decimalpart");

            var accId = "";
			var accStatus="";
          var mfRowData = {
              imageIcon: loadAccntProdImages("mf_product"),
              lblAccountName: {text: kony.i18n.getLocalizedString("MU_SUM_Title"), isVisible: true},
              lblAccountNumber: resultSet["accountNoFomatted"],
              lblRemainTrans: {isVisible: false},
              lblAmount: intpart ,	//mfAmount,
              lblAmountDecvalue: decpart,
              lblBalText: kony.i18n.getLocalizedString("MF_thr_Investment_value"),
              mfFlag: "Y",
              flxIcon: {"highlightOnParentFocus":true, "highlightedSkin":"skinFlexOnFocusAccSumm"},
			  flxDetails: {"highlightOnParentFocus":true, "highlightedSkin":"skinFlexOnFocusAccSumm"},
            	
            };
          mutualFundData.push(mfRowData);
          mutualFundSection.push(mutualFundData);
        }else{
          isMutualFundAvail = false;
        }
        kony.print("@@@ Insurance....!");
        //Insurance data.
        if(isNotBlank(resultSet["baAccountFlag"]) && resultSet["baAccountFlag"] == "true"){
          isInsuranceAvail = true;
          var insuranceData = [];
          var totalSumInsured = resultSet["totalSumInsured"];
          if(isNotBlank(totalSumInsured) && totalSumInsured != "-") {
            totalSumInsured = commaFormatted(parseFloat(totalSumInsured).toFixed(2));
          }else{
            totalSumInsured = "";
          }
            var accId ="";
            var accStatus ="";
          	var intpart = formatAmountForDashboard(totalSumInsured,"intpart");
            var decpart = formatAmountForDashboard(totalSumInsured,"decimalpart");

            var insuranceRowData = {
              imageIcon: loadAccntProdImages("ba_prod_logo"),
              lblAccountName: {text: kony.i18n.getLocalizedString("BA_Acc_Summary_Title"), isVisible: true},
              lblAccountNumber: {isVisible: false},
              lblRemainTrans: {isVisible: false},
              lblAmount: intpart ,	//totalSumInsured,
              lblAmountDecvalue: decpart,
              flxIcon: {"highlightOnParentFocus":true, "highlightedSkin":"skinFlexOnFocusAccSumm"},
			  flxDetails: {"highlightOnParentFocus":true, "highlightedSkin":"skinFlexOnFocusAccSumm"},
              lblBalText: kony.i18n.getLocalizedString("MF_thr_Investment_value"),
              insFlag: "Y"
            };
          insuranceData.push(insuranceRowData);
          insuranceSection.push(insuranceData);
        }else{
          isInsuranceAvail = false;
        }
        
        if(isAccountsAvail){
          accountSummaryData.push(accountSection);
        }
        if(isCreditCardAvail){
          accountSummaryData.push(creditCardSection);
        }
        if(isLonasAvail){
          accountSummaryData.push(loanSection);
        }
        if(isMutualFundAvail){
          accountSummaryData.push(mutualFundSection);
        }
        if(isInsuranceAvail){
          accountSummaryData.push(insuranceSection);
        }
        frmAccountSummary.segAccountDetails.setData(accountSummaryData);
        setAccountSummaryLinks(isAccountsAvail, isCreditCardAvail, isLonasAvail, isMutualFundAvail, isInsuranceAvail);
        
        kony.print("@@@Accounts::"+isAccountsAvail+"||CC::"+isCreditCardAvail+"||Loans::"+isLonasAvail+"||MF::"+isMutualFundAvail+"||Ins::"+isInsuranceAvail)
        //Dont show Total Balance for only Credit Card Customers.
        if(!isAccountsAvail && isCreditCardAvail && !isLonasAvail && !isLonasAvail && !isMutualFundAvail && !isInsuranceAvail ){
          frmAccountSummary.flxTotalBalance.setVisibility(false);
        }else{
          frmAccountSummary.flxTotalBalance.setVisibility(true);
        }
        frmAccountSummary.flxScrollMain.setContentOffset({x: gblXCord, y: gblYCord}, false);
      }else{
        showAlert(kony.i18n.getLocalizedString("keyOpenActGenErr"), kony.i18n.getLocalizedString("info"))
      }
  }catch(e){
    kony.print("@@@ In setAccountSummaryData() Exception:::"+e);
  }
}

function setAccountSummaryLinks(isAccountsAvail, isCreditCardAvail, isLonasAvail, isMutualFundAvail, isInsuranceAvail){
  try{
    kony.print("@@@ In setAccountSummaryLinks() @@@");
    frmAccountSummary.btnAddaccount.setVisibility(!isAccountsAvail);
    frmAccountSummary.btnGetCreditCard.setVisibility(!isCreditCardAvail);
    frmAccountSummary.btnGetLoan.setVisibility(!isLonasAvail);
    frmAccountSummary.btnStartMutualFund.setVisibility(false);
    frmAccountSummary.btnInsurance.setVisibility(false);
  }catch(e){
    kony.print("@@@ In setAccountSummaryLinks() Exception:::"+e);
  }
}

function getAccountNickName(nickName, productName, accountId){
  try{
    var acccoutName = "";
    if (isNotBlank(nickName)) {
      acccoutName = nickName;
    }else{
      var sbStr = accountId;
      var length = sbStr.length;
      if (isNotBlank(productName) && productName == kony.i18n.getLocalizedString("Loan"))
        sbStr = sbStr.substring(7, 11);
      else
        sbStr = sbStr.substring(length - 4, length);
      acccoutName = productName + " " + sbStr;
    }
    return acccoutName;
  }catch(e){
    kony.print("@@@ In getAccountNickName() Exception:::"+e);
  }
}

function showAccountSummaryNew(){
  frmAccountSummary.show();
}

function showAccountSummaryNewWithRefresh(){
  setAccountSummaryData();
  frmAccountSummary.show();
}


function onSwipeAccountSummarySeg(widgetRef, gestureInfo, context){
  try{
    kony.print("@@@ In onSwipeAccountSummarySeg() @@@");
    var rowIndex = context.rowIndex;
    var sectionIndex = context.sectionIndex;
    kony.print("@@@rowIndex:::"+rowIndex+"||sectionIndex:::"+sectionIndex);
    var sectionData = frmAccountSummary.segAccountDetails.data[sectionIndex];
    var selAccoutDetails = sectionData[1][rowIndex];
    if(isNotBlank(selAccoutDetails)){
      var flexLeft = "";
      if(isNotBlank(gblASPrevSelRowVal) && isNotBlank(gblASSelRowIndex) && isNotBlank(gblASSelRowIndex[0]) && isNotBlank(gblASSelRowIndex[1])){
        kony.print("Inside if block");
        if( gblASSelRowIndex[0] != sectionIndex) {
          kony.print("gblASSelRowIndex[0] != sectionIndex");
          gblASPrevSelRowVal["flxASRowDetails"].left = "0%";
          frmAccountSummary.segAccountDetails.setDataAt(gblASPrevSelRowVal, gblASSelRowIndex[1], gblASSelRowIndex[0]);
        } else if(gblASSelRowIndex[1] != rowIndex) { 
          kony.print("gblASSelRowIndex[1] != rowIndex");
          gblASPrevSelRowVal["flxASRowDetails"].left = "0%";
          frmAccountSummary.segAccountDetails.setDataAt(gblASPrevSelRowVal, gblASSelRowIndex[1], gblASSelRowIndex[0]);
        }
      }
      if(isNotBlank(selAccoutDetails["mfFlag"]) && selAccoutDetails["mfFlag"] == "Y"){
        kony.print("Do Nothing when we swipe on mutual fund row");
      }else if(isNotBlank(selAccoutDetails["insFlag"]) && selAccoutDetails["insFlag"] == "Y"){
        kony.print("Do Nothing when we swipe on insurance row");
      }else{
        kony.print("@@SwipeData:::"+JSON.stringify(selAccoutDetails));
        if(isNotBlank(selAccoutDetails["allFreeAccFlag"]) && selAccoutDetails["allFreeAccFlag"] == "Y"){
          //Nothing to do on swipe to Open All free Account banner.
        }else if(isNotBlank(selAccoutDetails["flxTransfer"]).isVisible == false && isNotBlank(selAccoutDetails["flxPayBill"]).isVisible ==  false && isNotBlank(selAccoutDetails["flxTopUp"]).isVisible ==  false && isNotBlank(selAccoutDetails["flxWithdrawl"]).isVisible ==  false){
          flexLeft = "0%";
        }else{
          kony.print("@@@gblASSelRowIndex:::"+gblASSelRowIndex);
          kony.print("gblASPrevSelRowVal>>"+JSON.stringify(gblASPrevSelRowVal));
          kony.print("@@@ gestureInfo.swipeDirection:::"+gestureInfo.swipeDirection);
          if(gestureInfo.swipeDirection == 1 || gestureInfo.swipeDirection == 2){
            kony.print("Swipe direction is ok");
            if(selAccoutDetails["flxASRowDetails"].left != "-91%"){
              flexLeft = "-91%";
            }else{
              flexLeft = "0%";
            }   
          }else{
            return;   
          }  
        }
        selAccoutDetails["flxASRowDetails"].left = flexLeft;
        frmAccountSummary.segAccountDetails.setDataAt(selAccoutDetails, rowIndex, sectionIndex);
        gblASPrevSelRowVal = selAccoutDetails;
        gblASSelRowIndex  = [sectionIndex, rowIndex];
      }
    }
  }catch(e){
    kony.print("@@@ In onSwipeAccountSummarySeg() Exception:::"+e);
  }
}

function preShowFrmAccountSummarynew(){
  try{
    kony.print("@@@@in preShowFrmAccountSummarynew@@@@@");
    frmAccountSummary.lblVersion.text = kony.i18n.getLocalizedString("keyVersion")+" "+ appConfig.appVersion;
    frmAccountSummary.lblCopyRight.text=getCopyRightText();
    frmAccountSummary.btnAddaccount.text = kony.i18n.getLocalizedString("MB_AcctMorelink1");
    frmAccountSummary.btnGetCreditCard.text = kony.i18n.getLocalizedString("MB_CCMorelink1");
    frmAccountSummary.btnGetLoan.text = kony.i18n.getLocalizedString("MB_LoanMorelink1");
    frmAccountSummary.btnStartMutualFund.text = kony.i18n.getLocalizedString("MB_MFMorelink1");
    frmAccountSummary.btnInsurance.text = kony.i18n.getLocalizedString("MB_BAMorelink1");

    frmAccountSummary.lbliconPayalert.text= kony.i18n.getLocalizedString("PayAlert_Heading"); 
    frmAccountSummary.lbliconScan.text= kony.i18n.getLocalizedString("lbQRScan");
    frmAccountSummary.lbliconWithdraw.text= kony.i18n.getLocalizedString("Withdraw");
    frmAccountSummary.lbliconWow.text= kony.i18n.getLocalizedString("MB_Loyaltymenu");
    
    frmAccountSummary.lblTakeNew.text= kony.i18n.getLocalizedString("btnTakePhoto"); 
    frmAccountSummary.lblImport.text= kony.i18n.getLocalizedString("btnLibrary");
    frmAccountSummary.lblUseDefault.text= kony.i18n.getLocalizedString("btnDeleteProfileImg");
    frmAccountSummary.btnCancel.text= kony.i18n.getLocalizedString("btnPhotoCancel");
    
    //#ifdef android
    kony.print("@@inside android padding");
    frmAccountSummary.imgPayAlert.width = "100%";
    frmAccountSummary.imgPayAlert.padding = [9,9,9,9];
    //#endif
    //Home title able to read from accessibility function(Talkback)
    frmAccountSummary.flexHeader.accessibilityConfig = {"a11yHidden": false, "a11yLabel": "Home"};
    frmAccountSummary.flxScrollMain.showFadingEdges = false;
    
    frmAccountSummary.flxTakeProfilePic.setVisibility(false);
  }catch(e){
    kony.print("@@@ In preShowFrmAccountSummarynew() Exception:::"+e);
  }
}

function getValueOfIcons(Value)
{
  var flag = false;
  if(Value == "Y"){
    flag = true;
  }else{
    flag = false;
  }
  return flag;
}

function setIconsSizeandValues(widths,transfer,paybill,topup,cardless){
 try{
   var noofSections = 0;
   if(transfer == true)
     noofSections++;
   if(paybill == true)
     noofSections++;
   if(topup == true)
     noofSections++;
   if(cardless == true)
     noofSections++;

   if(noofSections == 1){
     widths = widths;
   }          
   if(noofSections == 2){
     widths = widths/2;
   } 
   if(noofSections == 3){
     widths = widths/3;
   }
   if(noofSections == 4){
     widths = widths/4;
   }
   widths = Math.round(widths) - 1;
   return widths+"%";
 }catch(e){
   kony.print("Exception setIconsSizeandValues of Account Summary>> "+e);
 }  
}


function checkStringVal(val){
  if(isNotBlank(val)){
    return val;
  }else{
    return "";
  }
}

function showTMBWOWAlertnew(){
  try{
    kony.print("entered on TMB WOW @@");
    frmAccountSummary.btnClose.skin = "btnCloseWhitePopup";
    frmAccountSummary.btnClose.onClick = closePayAlertnew;
    frmAccountSummary.flxClose.onClick = closePayAlertnew;
    frmAccountSummary.imgPopup.src = "tmb_wow.png";
    frmAccountSummary.lblPopup.text = kony.i18n.getLocalizedString("WOW_Heading");
    frmAccountSummary.lblContent.text = kony.i18n.getLocalizedString("WOW_Content");
    frmAccountSummary.btnRegister.text = kony.i18n.getLocalizedString("WOW_Button");
    frmAccountSummary.btnRegister.skin = "btnBlueBGNoRoundWhiteBr";
    frmAccountSummary.btnLater.setVisibility(false);
    frmAccountSummary.btnRegister.onClick = closePayAlertnew;
    //dynamic UI changes
    frmAccountSummary.flxPopup.skin = "flxBlue";
    frmAccountSummary.lblPopup.skin = "lblWhite200";
    frmAccountSummary.lblContent.skin = "lblWhiteMedium171Ozone";
    frmAccountSummary.flxMain.setVisibility(false);
    frmAccountSummary.flxPopup.setVisibility(true);
    kony.print("after UI TMB WOW @@");
  }catch(e){
    kony.print("error in showTMBWOWAlert "+e);
  }
}

function closePayAlertnew(){
  kony.print("closePayAlert @@");
  frmAccountSummary.flxPopup.setVisibility(false);
  frmAccountSummary.flxMain.setVisibility(true);
}

function showPayAlertnew(){
  try{
    kony.print("entered on rtp @@");
    frmAccountSummary.btnClose.skin = "btnCloseBluePopup";
    frmAccountSummary.btnClose.onClick = closePayAlertnew;
    frmAccountSummary.flxClose.onClick = closePayAlertnew;
    frmAccountSummary.imgPopup.src = "pay_alert.png";
    frmAccountSummary.lblPopup.text = kony.i18n.getLocalizedString("PayAlert_Heading");
    frmAccountSummary.lblContent.text = kony.i18n.getLocalizedString("PayAlert_Content");
    frmAccountSummary.btnRegister.text = kony.i18n.getLocalizedString("PayAlert_Register");
    frmAccountSummary.btnRegister.skin = "btnBlue28PxRounded";
    frmAccountSummary.btnRegister.onClick = menuSetUserIDonClick;
    frmAccountSummary.btnLater.text  = kony.i18n.getLocalizedString("MF_lbl_DoItLater");
    frmAccountSummary.btnLater.onClick = closePayAlertnew;
    if(frmAccountSummary.btnLater.isVisible === false){
      frmAccountSummary.btnLater.setVisibility(true);
    }
    kony.print("before UI on rtp @@");
    //dynamic UI changes
    frmAccountSummary.flxPopup.skin = "flexWhiteBG";
    frmAccountSummary.lblPopup.skin = "lblBlack200DBOzoneX";
    frmAccountSummary.lblContent.skin = "lblBlackMedium171Ozone";
    frmAccountSummary.flxMain.setVisibility(false);
    frmAccountSummary.flxPopup.setVisibility(true);
    kony.print("after UI on rtp @@");
  }catch(e){
    kony.print("error in showPayAlert "+e);
  }
}

function onClickRTPnew(){
  kony.print("clicked on rtp @@"+isRTPCustomer);
  if(isRTPCustomer){
    if(checkMBUserStatus()){ // checking if transaction password is locked
      onClickRTPIconFromAccountSum();
    }
  }else{
    showPayAlertnew();
  }
}

function onClickTMBWownew(){
  gblMenuSource = "TMB WOW icon";
  kony.print("clicked TMB WOW @@");
  if (isNotBlank(gblReferCD)){
    showLoadingScreen();
    frmLoyaltylanding.show();  
  }else {
    showTMBWOWAlertnew();
  }
}

function onRowClickSegAccSummary(){
  try{
    kony.print("@@@ In onRowClickSegAccSummary() @@@");
    var selectedRow = frmAccountSummary.segAccountDetails.selectedRowItems[0];
    //kony.print("@@@SelectedRowData:::"+JSON.stringify(selectedRow));
    if(selectedRow != null && selectedRow != "" && selectedRow != undefined){
      showLoadingScreen();
      if(isNotBlank(selectedRow["mfFlag"]) && selectedRow["mfFlag"] == "Y"){
        showMFAccountDetails();
      }else if(isNotBlank(selectedRow["insFlag"]) && selectedRow["insFlag"] == "Y"){
        showBAAccountDetails();
      }else if(isNotBlank(selectedRow["allFreeAccFlag"]) && selectedRow["allFreeAccFlag"] == "Y"){
        //Navigation to Open All free Account
        loadFunctionalModuleSync("openAcctModule");
        menuOpenNewAccountonClick();
      }else{
        gblXCord = frmAccountSummary.flxScrollMain.contentOffsetMeasured.x;
  		gblYCord = frmAccountSummary.flxScrollMain.contentOffsetMeasured.y;
        showAccountDetails();
      }
    }
  }catch(e){
    kony.print("@@@ In onRowClickSegAccSummary() Exception:::"+e);
  }
}

//Function to split amount according to new format
//totalBalance is the amount to be passed , type is possible values to be returned
function formatAmountForDashboard(totalBalance,type){
  try{
    var amountArr = totalBalance.split(".");
    if(type == "intpart"){
      return amountArr[0];
    }else if(type == "decimalpart"){
      return isNotBlank(amountArr[1])? "."+amountArr[1]:" ";//decimal part with dot
    }else{
      return totalBalance;
    }
  }catch(e){
    kony.print("Exception in formatAmountForDashboard 0f Account Summary>> "+e)
  }
}

function onDeviceBackAS(){
  try{
    kony.print("@@@In onDeviceBackAS() @@@");
    if (isMenuShown == false){
      if(!gblTimerFlg){
        showToastMsg();
        kony.timer.schedule("btnTimer", callback, 4, false);
        popGoback.show();
        gblTimerFlg = true;
        gblExitByBackBtn = true;
        resetValues(); 
      }
    }
    function callback(){
      popGoback.destroy();
      popGoback.dismiss();
      kony.timer.cancel("btnTimer");
      gblTimerFlg = false; 
      gblCallPrePost=true;	   
    }
  }catch(e){
    kony.print("@@@In onDeviceBackAS() Exception:::"+e);
  }
}

function setRTPBadgeCountNew(){
  rtp_log("@@@ In setRTPBadgeCount() @@@");
  kony.print("@@@gblRTPBadgeCount:::"+gblRTPBadgeCount);
  if (isNotBlank(gblRTPBadgeCount) && gblRTPBadgeCount > 0) {
    frmAccountSummary.imgPayAlert.setBadge(gblRTPBadgeCount + "");
  } else {
    frmAccountSummary.imgPayAlert.setBadge("");
  }
}

function onClickCreditCardsfromAccSummary(){
  if(gblEykcFlag == "Y"){
    showAlertForeKYC(kony.i18n.getLocalizedString("eKYC_msgNotAllowFeature"));
  }else{
  loadFunctionalModuleSync("loanModule");
  onClickCreditCards();
  }
}

function onClickPersonalLoanfromAccSummary(){
  if(gblEykcFlag == "Y"){
    showAlertForeKYC(kony.i18n.getLocalizedString("eKYC_msgNotAllowFeature"));
  }else{
  loadFunctionalModuleSync("loanModule");
  onClickPersonalLoan();
  }
}

function onClickMutualFundfromAccSummary(){
  if(gblEykcFlag == "Y"){
    showAlertForeKYC(kony.i18n.getLocalizedString("eKYC_msgNotAllowFeature"));
  }else{
  isInvestFromAccSumm = true;
  showMFAccountDetails();
  }
}

function showAlertForeKYC(keyMsg) {
  	var KeyTitle = "Info"
    var okk = kony.i18n.getLocalizedString("keyOK");
    
	//Defining basicConf parameter for alert
  	//var keyMsg = kony.i18n.getLocalizedString("eKYC_msgNotAllowFeature");
	var basicConf = {
		message: keyMsg,
		alertType: constants.ALERT_TYPE_INFO,
		alertTitle: KeyTitle,
		yesLabel: okk,
		noLabel: "",
		alertHandler: handleok
	};
	//Defining pspConf parameter for alert
	var pspConf = {};
	//Alert definition
	var infoAlert = kony.ui.Alert(basicConf, pspConf);
    function handleok(response){}
    
	return;
}