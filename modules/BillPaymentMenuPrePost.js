function frmBillPaymentMenuPreshow() {
	if(gblCallPrePost)
	{
		frmBillPayment.scrollboxMain.scrollToEnd();
	    frmBillPaymentPreShow.call(this);
	    isMenuShown = false;
	    isSignedUser = true;
	    gblMyBillerTopUpBB = 0;
	    GblBillTopFlag = true;
	    frmBillPayment.tbxMyNoteValue.numberOfVisibleLines = 1;
	    if (gblPaynow) {
	        frmBillPayment.lblPayBillOnValue.text = getFormattedDate(currentSystemDate(), kony.i18n.getCurrentLocale());
	    }
	    DisableFadingEdges.call(this, frmBillPayment);
	}
  	if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPhone Simulator" || gblDeviceInfo["name"] == "iPad"){
      frmBillPayment.segSlider.viewConfig = {coverflowConfig: {isCircular: true}};
    }
  	if(gblAmountPaymentFlag == 1 || gblRTPBillPayPush)  { //MKI, MIB-12109 start// added "||" condition to disable in RTP flow as well
    	frmBillPayment.tbxAmount.setEnabled(false);  
      	gblIstxtAmtEnable = false;
    }
  	else{
    	frmBillPayment.tbxAmount.setEnabled(true);
      	gblIstxtAmtEnable = true;
  	}//MKI, MIB-12109 Ends
}

function frmBillPaymentMenuPostshow() {
	if(gblCallPrePost)
	{
		frmBillPaymentPostShow.call(this);
	}
	assignGlobalForMenuPostshow();
}

function frmBillPaymentBillerCategoriesMenuPreshow() {
	if(gblCallPrePost)
	{
		frmBillPaymentBillerCategoriesPreShow.call(this);
	}
}

function frmBillPaymentCompleteMenuPreshow() {
	if(gblCallPrePost)
	{
		eh_frmBillPaymentComplete_preshow.call(this);
	}
}

function frmBillPaymentCompleteMenuPostshow() {
	if(gblCallPrePost)
	{
		eh_frmBillPaymentComplete_postshow.call(this);
	}
	assignGlobalForMenuPostshow();
}

function frmBillPaymentCompleteCalendarMenuPreshow() {
	if(gblCallPrePost)
	{
		isMenuShown = false;
	    isSignedUser = true;
	    frmBillPaymentCompleteCalendarPreShow();
	    frmBillPaymentCompleteCalendar.scrollboxMain.scrollToEnd();
	    DisableFadingEdges.call(this, frmBillPaymentCompleteCalendar);
	}
}

function frmBillPaymentCompleteCalendarMenuPostshow() {
	assignGlobalForMenuPostshow();
}

function frmBillPaymentConfirmationFutureMenuPreshow() {
	if(gblCallPrePost)
	{
		eh_frmBillPaymentConfirmationFuture_preshow.call(this);
	}
}

function frmBillPaymentEditMenuPreshow() {
	if(gblCallPrePost)
	{
	    frmBillPaymentEdit.scrollboxMain.scrollToEnd();
	    frmBillPaymentEditPreShow.call(this);
	    isMenuShown = false;
	    isSignedUser = true;
	    DisableFadingEdges.call(this, frmBillPaymentEdit);
	}
}

function frmBillPaymentViewMenuPreshow() {
	if(gblCallPrePost)
	{
		frmBillPaymentView.scrollboxMain.scrollToEnd();
	    frmBillPaymentViewPreShow.call(this);
	    isMenuShown = false;
	    isSignedUser = true;
	    DisableFadingEdges.call(this, frmBillPaymentView);
	}
}

function frmBillPaymentViewMenuPostshow() {
	assignGlobalForMenuPostshow();
}

function frmSelectBillerLandingMenuPreshow() {
	if(gblCallPrePost)
	{
		frmSelectBillerLandingPreShow.call(this);
	}
}

function frmSelectBillerLandingMenuPostshow() {
	if(gblCallPrePost)
	{
		frmSelectBillerLandingPostShow.call(this);
	}
	assignGlobalForMenuPostshow();
	frmSelectBillerLanding.scrollboxMain.scrollToEnd();
}

function frmEditFutureBillPaymentCompleteMenuPreshow() {
	if(gblCallPrePost)
	{
	    frmEditFutureBillPaymentComplete.scrollboxMain.scrollToEnd();
	    isMenuShown = false;
	    isSignedUser = true;
	    frmEditFutureBillPaymentComplete.hboximg.setVisibility(false);
	    frmEditFutureBillPaymentComplete.hboxSharelist.setVisibility(false);
	   
	    frmEditFutureBillPaymentCompletePreShow.call(this);
	    DisableFadingEdges.call(this, frmEditFutureBillPaymentComplete);
		
	}
}

function frmEditFutureBillPaymentCompleteMenuPostshow() {
	if(gblCallPrePost)
	{
		if (gblBPflag == true) {
	        campaginService.call(this, "imgAd", "frmEditFutureBillPaymentComplete", "M");
	    } else {
	        campaginService.call(this, "imgTwo", "frmEditFutureTopupComplete", "M");
	    }
	}
	assignGlobalForMenuPostshow();
}

function frmEditFutureBillPaymentConfirmMenuPreshow() {
	if(gblCallPrePost)
	{
		frmEditFutureBillPaymentConfirm.scrollboxMain.scrollToEnd();
	    isMenuShown = false;
	    isSignedUser = true;
	    frmEditFutureBillPaymentConfirmPreShow.call(this);
	    DisableFadingEdges.call(this, frmEditFutureBillPaymentConfirm);
		
	}
}

function frmEditFutureBillPaymentConfirmMenuPostshow() {
	assignGlobalForMenuPostshow();
}

function frmScheduleMenuPreshow() {
   	frmInternalSchedulePreShow.call(this);
	frmSchedulePreShow.call(this);
}

function frmScheduleMenuPostshow() {
	postShowFrmSchedule.call(this);
}

function frmSelectBillerMenuPreshow() {
	if(gblCallPrePost)
	{
		eh_frmSelectBiller_frmSelectBiller_preshow.call(this);
	}
}

function frmSelectBillerMenuPostshow() {
	if(gblCallPrePost)
	{
	 	frmSelectTopUpPostShow.call(this);
    	//disableSelectBillerCancelBtn.call(this);
	}
	assignGlobalForMenuPostshow();
}

function frmMBBankAssuranceDetailsMenuPreshow() {
	if(gblCallPrePost)
	{
	    isMenuShown = false;
	    frmMBBankAssuranceDetails.scrollboxMain.scrollToEnd();
	    frmMBBankAssuranceDetailsPreShow.call(this);
	    DisableFadingEdges.call(this, frmMBBankAssuranceDetails);
	}
}

function frmMBBankAssuranceDetailsMenuPostshow() {
	if(gblCallPrePost)
	{
	 	deviceInfo = kony.os.deviceInfo();
    	if (deviceInfo["name"] == "thinclient" & deviceInfo["type"] == "spa") {
	        isMenuRendered = false;
	        isMenuShown = false;
	        frmMBBankAssuranceDetails.scrollboxMain.scrollToEnd();
    	}
	}
	assignGlobalForMenuPostshow();
}


function frmMBBankAssuranceSummaryMenuPreshow() {
	if(gblCallPrePost)
	{
	    gblIndex = -1;
	    isMenuShown = false;
	    try {
	        frmMBBankAssuranceSummary.scrollboxMain.scrollToEnd();
	    } catch (e) {}
	    frmMBBankAssuranceSummaryPreShow.call(this);
	    DisableFadingEdges.call(this, frmMBBankAssuranceSummary);
	}
}

function frmMBBankAssuranceSummaryMenuPostshow() {
	if(gblCallPrePost)
	{
	    deviceInfo = kony.os.deviceInfo();
	    if (deviceInfo["name"] == "thinclient" & deviceInfo["type"] == "spa") {
	        isMenuRendered = false;
	        isMenuShown = false;
	        frmMBBankAssuranceSummary.scrollboxMain.scrollToEnd();
	    }
	}
	assignGlobalForMenuPostshow();
}


function frmMBMutualFundDetailsMenuPreshow() {
	if(gblCallPrePost)
	{
	    isMenuShown = false;
	    frmMBMutualFundDetails.scrollboxMain.scrollToEnd();
	    DisableFadingEdges.call(this, frmMBMutualFundDetails);
	    frmMBMutualFundsSummaryPreShow.call(this);
	}
}

function frmMBMutualFundDetailsMenuPostshow() {
	if(gblCallPrePost)
	{
	    deviceInfo = kony.os.deviceInfo();
	    if (deviceInfo["name"] == "thinclient" & deviceInfo["type"] == "spa") {
	        isMenuRendered = false;
	        isMenuShown = false;
	        frmMBMutualFundDetails.scrollboxMain.scrollToEnd();
	    }
	}
	assignGlobalForMenuPostshow();
}



function frmMFFullStatementMBMenuPreshow() {
	if(gblCallPrePost)
	{
	    currentpageStmt = 1;
	    isMenuShown = false;
	    isSignedUser = true;
	    frmMFFullStatementMB.scrollboxMain.scrollToEnd();
	    frmMBMFAcctFullStatementPreShow.call(this);
	    DisableFadingEdges.call(this, frmMFFullStatementMB);
	}
}

function frmMFFullStatementMBMenuPostshow() {
	if(gblCallPrePost)
	{
	    deviceInfo = kony.os.deviceInfo();
	    if (deviceInfo["name"] == "thinclient" & deviceInfo["type"] == "spa") {
	        isMenuRendered = false;
	        isMenuShown = false;
	        frmAccountStatementMB.scrollboxMain.scrollToEnd();
	    }
	}
	assignGlobalForMenuPostshow();
}



function frmMutualFundsSummaryLandingMenuPreshow() {
	if(gblCallPrePost)
	{
	    gblIndex = -1;
	    isMenuShown = false;
	    try {
	        frmMutualFundsSummaryLanding.scrollboxMain.scrollToEnd();
	    } catch (e) {}
	    frmMutualFundsSummaryLandingPreShow.call(this);
	    DisableFadingEdges.call(this, frmMutualFundsSummaryLanding);
	}
}

function frmMutualFundsSummaryLandingMenuPostshow() {
	if(gblCallPrePost)
	{
	    deviceInfo = kony.os.deviceInfo();
	    if (deviceInfo["name"] == "thinclient" & deviceInfo["type"] == "spa") {
	        isMenuRendered = false;
	        isMenuShown = false;
	        frmMutualFundsSummaryLanding.scrollboxMain.scrollToEnd();
	    }
	}
	assignGlobalForMenuPostshow();
}

function frmBillPaymentEditFutureNewMenuPostshow() {
	if (gblCallPrePost) {
        //..
    }
    assignGlobalForMenuPostshow();
    addAccessPinKeypad(frmBillPaymentEditFutureNew);
}

function frmBillPaymentEditFutureCompleteMenuPreshow() {
	if(gblCallPrePost)
	{
		frmBillPaymentEditFutureComplete_Preshow.call(this);
	}
}

function frmBillPaymentEditFutureCompleteMenuPostshow() {
	if(gblCallPrePost)
	{
		frmBillPaymentEditFutureComplete_Postshow.call(this);
	}
	assignGlobalForMenuPostshow();
}