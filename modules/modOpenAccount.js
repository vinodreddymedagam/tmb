







function onClickNextselAct() {
	frmOpenAccountNSConfirmation.hbxCardFee.setVisibility(false);
	frmOpenAccountNSConfirmation.hbxCardType.setVisibility(false);
	frmOpenAccountNSConfirmation.lblAddressHeader.setVisibility(false);
	frmOpenAccountNSConfirmation.lblHdrContactAddr.setVisibility(false);
	frmOpenAccountNSConfirmation.lblAddress1.setVisibility(false);
	frmOpenAccountNSConfirmation.lblAddress2.setVisibility(false);
	// added below line to fix UAT DEF559
	frmOpenAccountNSConfirmation.hbxCommon.setVisibility(false);
	
	if(frmOpnActSelAct.hbxSelActAmt.isVisible == true){
		if ((frmOpnActSelAct.txtOpenActSelAmt.text == null) || (frmOpnActSelAct.txtOpenActSelAmt.text == "")) {
	        showAlert(kony.i18n.getLocalizedString("keyMinLimitAmt"), kony.i18n.getLocalizedString("info"));
	            frmOpnActSelAct.txtOpenActSelAmt.skin = txtErrorBG;
	            return false
	     }
     }
	//var textformat1=frmOpnActSelAct.txtOpenActNicNam.text
//	var patrn1 = /^[a-z0-9]+$/;
//	var matching1 = patrn1.test(textformat1);
//	if ((textformat1 == null) || (textformat1 == "") || (!matching1) || (textformat1.length<3) || (textformat1.length>20)) {
//	showAlert(kony.i18n.getLocalizedString("keyOpenAccntNickname"), kony.i18n.getLocalizedString("info"));
//	frmOpnActSelAct.txtOpenActNicNam.skin = txtErrorBG;
//	return false;
//	}
	var textformat=NickNameValid(frmOpnActSelAct.txtOpenActNicNam.text);
	if(textformat== false) {
	showAlert(kony.i18n.getLocalizedString("keyOpenAccntNickname"), kony.i18n.getLocalizedString("info"));
	frmOpnActSelAct.txtOpenActNicNam.skin = txtErrorBG;
	return false;
	}
	
	var devNameFlag = NickNameValid(frmOpnActSelAct.txtOpenActNicNam.text);
	var enteredAmount;
	var isCrtFormt;
	//gblFinActivityLogOpenAct = []; 
	if (gblSelProduct == "TMBDreamSavings") {
		
		//enteredAmount = frmOpnActSelAct.txtDSAmountSel.text;
		//isCrtFormt = amountValidationMBOpenAct(enteredAmount);
		//if(!isCrtFormt){
		//	showAlert(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), kony.i18n.getLocalizedString("info"));
		//	frmOpnActSelAct.txtDSAmountSel.text="";
		//	return false
		//}
      	
      	//MIB-4723 No Fixed Account cannot open Dream Saving Account
        if (frmOpnActSelAct["segNSSlider"]["selectedItems"][0].hiddenProductId == "221") {
            showAlert(kony.i18n.getLocalizedString("keyWarnOpenAccountDS"), kony.i18n.getLocalizedString("info"));
            return false;  
        }
		if (devNameFlag == false) {
			showAlert(kony.i18n.getLocalizedString("keyincorrectNickName"), kony.i18n.getLocalizedString("info"));
			frmOpnActSelAct.txtOpenActNicNam.skin = txtErrorBG;
			return false;
		}
		
		
		var nickName = openActNickNameUniq(frmOpnActSelAct.txtOpenActNicNam.text);
			 if(nickName == false){
			 showAlert(kony.i18n.getLocalizedString("keyuniqueNickName"), kony.i18n.getLocalizedString("info"));
			 frmOpnActSelAct.txtOpenActNicNam.skin = txtErrorBG;
				return false
			 }
		
		if (frmOpnActSelAct.btnDreamSavecombo.text == kony.i18n.getLocalizedString("keyTransferEvryMnthOpen")) {
			showAlert(kony.i18n.getLocalizedString("keypleaseSelecttransfer"), kony.i18n.getLocalizedString("info"));
			return false;
		}
		var amountselDream = frmOpnActSelAct.txtDSAmountSel.text.trim();
		amountselDream = amountselDream.replace(kony.i18n.getLocalizedString("currencyThaiBaht"),"").replace("/", "").replace(kony.i18n.getLocalizedString("keyCalendarMonth"), "").replace(/,/g, "").replace(" ", "");
		
		var isCrtFormt=amountValidationMBOpenAct(amountselDream);		
				
				if(!isCrtFormt){
			   	showAlert(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), kony.i18n.getLocalizedString("info"));
			   	frmOpnActSelAct.txtDSAmountSel.skin = txtErrorBG;
			   	return false
				}
				
				if ((kony.os.toNumber(amountselDream)) < (kony.os.toNumber(gblMinOpenAmt))){
					showAlert(kony.i18n.getLocalizedString("keyMinLimitAmt"), kony.i18n.getLocalizedString("info"));
			   		frmOpnActSelAct.txtDSAmountSel.skin = txtErrorBG;
			   		return false				
				}
				
				if (gblMaxOpenAmt != "No Limit")
				{
					if ((kony.os.toNumber(amountselDream)) > (kony.os.toNumber(gblMaxOpenAmt))){
						showAlert(kony.i18n.getLocalizedString("keyMaxLimitAmt"), kony.i18n.getLocalizedString("info"));
				   		frmOpnActSelAct.txtDSAmountSel.skin = txtErrorBG;
				   		return false				
					}
				}	
			
		
		
		frmOpnActSelAct.txtOpenActNicNam.skin = txtNormalBG;
		frmOpenActDSConfirm.imgDSCnfmTitle.src = frmOpnActSelAct.imgNSProdName.src;
		
							if (kony.i18n.getCurrentLocale() == "en_US") {
								frmOpenActDSConfirm.lblOADIntRate.text = gblFinActivityLogOpenAct["intersetRateLabelEN"] + ":"
								frmOpenActDSConfirm.lblDSCnfmTitle.text = gblFinActivityLogOpenAct["prodNameEN"];
							} else {
								frmOpenActDSConfirm.lblOADIntRate.text = gblFinActivityLogOpenAct["intersetRateLabelTH"] + ":"
								frmOpenActDSConfirm.lblDSCnfmTitle.text = gblFinActivityLogOpenAct["prodNameTH"];
							}
		frmOpenActDSConfirm.lblOADSNickNameVal.text = frmOpnActSelAct.txtOpenActNicNam.text;
		gblFinActivityLogOpenAct["toActNickName"] = frmOpnActSelAct.txtOpenActNicNam.text;
		
        frmOpenActDSConfirm.lblOADSActNameVal.text = frmOpnActSelAct["segNSSlider"]["selectedItems"][0].hiddenActName;
        gblFinActivityLogOpenAct["toBranchName"] = frmOpnActSelAct["segNSSlider"]["selectedItems"][0].lblRemainFeeValue;
        frmOpenActDSConfirm.lblOADSBranchVal.text = frmOpnActSelAct["segNSSlider"]["selectedItems"][0].lblRemainFeeValue;
        frmOpenActDSConfirm.lblOAMnthlySavName.text = frmOpnActSelAct["segNSSlider"]["selectedItems"][0].lblCustName;
        frmOpenActDSConfirm.lblOAMnthlySavType.text = frmOpnActSelAct["segNSSlider"]["selectedItems"][0].lblSliderAccN2;
        frmOpenActDSConfirm.imgOAMnthlySavPic.src = frmOpnActSelAct["segNSSlider"]["selectedItems"][0].imghidden;
        frmOpenActDSConfirm.lblOAMnthlySavNum.text = frmOpnActSelAct["segNSSlider"]["selectedItems"][0].lblActNoval;
        gblFinActivityLogOpenAct["branchEN"] = frmOpnActSelAct["segNSSlider"]["selectedItems"][0].hiddenBranchNameEN;
        gblFinActivityLogOpenAct["branchTH"] = frmOpnActSelAct["segNSSlider"]["selectedItems"][0].hiddenBranchNameTH;
        gblFinActivityLogOpenAct["nickTH"] = frmOpnActSelAct["segNSSlider"]["selectedItems"][0].hiddenproductThaiAssign;
        gblFinActivityLogOpenAct["nickEN"] = frmOpnActSelAct["segNSSlider"]["selectedItems"][0].hiddenprodENGAssign;
        gblFinActivityLogOpenAct["actTypeFromOpen"] = frmOpnActSelAct["segNSSlider"]["selectedItems"][0].hiddenActType;
      
		frmOpenActDSConfirm.lblOADSAmtVal.text = commaFormattedOpenAct(amountselDream) + kony.i18n.getLocalizedString("currencyThaiBaht");//frmOpenAcDreamSaving.txtODMnthSavAmt.text;

		gblFinActivityLogOpenAct["transferAmount"] = amountselDream;
		frmOpenActDSConfirm.lblOADMnthVal.text = frmOpnActSelAct.btnDreamSavecombo.text;
		frmOpenActDSConfirm.lblOADIntRateVal.text = frmOpenProdDetnTnC.lblOpenAccInt.text + "%";		
		frmOpenActDSConfirm.lblOADSAmt.text = kony.i18n.getLocalizedString("keyMBMonthlySavingAmnt");
		frmOpenActDSConfirm.lblOADMnth.text = kony.i18n.getLocalizedString("keyMBTransferEvryMnth");

      	fromActId = frmOpnActSelAct["segNSSlider"]["selectedItems"][0].lblActNoval;
        fromActType = frmOpnActSelAct["segNSSlider"]["selectedItems"][0].hiddenActType;
      
		fromActId = fromActId.toString().replace(/-/g, "");
		checkIsEligibleToOpenAct(fromActId,fromActType);
		//invokePartyInquiryOpenAct();		
	} else if (gblSelProduct == "ForUse") {
		
		frmOpnActSelAct.txtOpenActSelAmt.text = autoFormatAmount(frmOpnActSelAct.txtOpenActSelAmt.text);
		enteredAmount = frmOpnActSelAct.txtOpenActSelAmt.text;
		enteredAmount = enteredAmount.replace(kony.i18n.getLocalizedString("currencyThaiBaht"),"").replace(/,/g, "");
		enteredAmount = enteredAmount.trim();
		gblFinActivityLogOpenAct["transferAmount"] = enteredAmount;
		//gblFinActivityLogOpenAct["transferAmount"] = kony.os.toNumber(gblFinActivityLogOpenAct["transferAmount"]);
		
		isCrtFormt = amountValidationMBOpenAct(enteredAmount);
		if (!isCrtFormt) {
			showAlert(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), kony.i18n.getLocalizedString("info"));
			frmOpnActSelAct.txtOpenActSelAmt.text = "";
			frmOpnActSelAct.txtOpenActSelAmt.skin = txtErrorBG;
			return false
		}
		if ((kony.os.toNumber(enteredAmount)) < (kony.os.toNumber(gblMinOpenAmt))){
			showAlert(kony.i18n.getLocalizedString("keyMinLimitAmt"), kony.i18n.getLocalizedString("info"));
			frmOpnActSelAct.txtOpenActSelAmt.skin = txtErrorBG;
			return false
		}
		if (gblMaxOpenAmt != "No Limit") {
			if ((kony.os.toNumber(enteredAmount)) > (kony.os.toNumber(gblMaxOpenAmt))) {
				showAlert(kony.i18n.getLocalizedString("keyMaxLimitAmt"), kony.i18n.getLocalizedString(
					"info"));
					frmOpnActSelAct.txtOpenActSelAmt.skin = txtErrorBG;
				return false
			}
		}
	
        if ((kony.os.toNumber(enteredAmount)) > (kony.os.toNumber(frmOpnActSelAct["segNSSlider"]["selectedItems"][0].hiddenActBal))) {
          showAlert(kony.i18n.getLocalizedString("keyAmtLessThanAvaBal"), kony.i18n.getLocalizedString("info"));
          frmOpnActSelAct.txtOpenActSelAmt.skin = txtErrorBG;
          return false
        }
      
		if (devNameFlag == false) {
			showAlert(kony.i18n.getLocalizedString("keyincorrectNickName"), kony.i18n.getLocalizedString("info"));
			frmOpnActSelAct.txtOpenActNicNam.skin = txtErrorBG;
			return false;
		}
		
		var nickName = openActNickNameUniq(frmOpnActSelAct.txtOpenActNicNam.text);
			 if(nickName == false){
			 showAlert(kony.i18n.getLocalizedString("keyuniqueNickName"), kony.i18n.getLocalizedString("info"));
			 frmOpnActSelAct.txtOpenActNicNam.skin = txtErrorBG;
				return false
			 }
	
	if(gblOpenActList["FOR_USE_PRODUCT_CODES"] != null && gblOpenActList["FOR_USE_PRODUCT_CODES"] != undefined && gblOpenActList["FOR_USE_PRODUCT_CODES"].indexOf(gblSelOpenActProdCode) > 0){
		frmOpenAccountNSConfirmation.hbxCardFee.setVisibility(true);
		frmOpenAccountNSConfirmation.hbxCardType.setVisibility(true);
		frmOpenAccountNSConfirmation.lblAddressHeader.text=appendColon(kony.i18n.getLocalizedString("keyAddressMailingCard"));
		frmOpenAccountNSConfirmation.lblAddressHeader.setVisibility(true);
		//frmOpenAccountNSConfirmation.lblHdrContactAddr.setVisibility(true); - not required as confirmed by PO
		frmOpenAccountNSConfirmation.lblAddress1.setVisibility(true);
		frmOpenAccountNSConfirmation.lblAddress2.setVisibility(true);
		
		
		if (province == kony.i18n.getLocalizedString("BangkokThaiValueProfile")){
				AddressLine1 =	gblAddress1Value + " " + gblAddress2Value + " " + kony.i18n.getLocalizedString("gblsubDtPrefixThaiB") + gblsubdistrictValue; 
				AddressLine2 =  kony.i18n.getLocalizedString("gblDistPrefixThaiB") + gbldistrictValue + " " + gblStateValue + " " + gblzipcodeValue; 
			}	
			else{
				AddressLine1 = gblAddress1Value + " " + gblAddress2Value + " " + kony.i18n.getLocalizedString("gblsubDtPrefixThai") + "." + gblsubdistrictValue;
				AddressLine2 = kony.i18n.getLocalizedString("gblDistPrefixThai") + "." + gbldistrictValue + " " + gblStateValue + " " + gblzipcodeValue;
			}
			//frmOpenAccountNSConfirmation.lblHdrContactAddr.text=kony.i18n.getLocalizedString("keyContactAddress");
			frmOpenAccountNSConfirmation.lblAddress1.text = AddressLine1;
			frmOpenAccountNSConfirmation.lblAddress2.text = AddressLine2;
			frmOpenAccountNSConfirmation.lblCardFeeValue.text = commaFormatted(parseFloat(removeCommos(gblFinActivityLogOpenAct["cardFee"])).toFixed(2)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
			frmOpenAccountNSConfirmation.lblCardTypeValue.text = gblFinActivityLogOpenAct["cardType"];
	
	}
	// added below if block to fix kony QA DEF526
	if(frmOpenAccountNSConfirmation.lblAddressHeader.isVisible){
		frmOpenAccountNSConfirmation.lineAboveAddress.setVisibility(true);
		frmOpenAccountNSConfirmation.lineBelowAddress.setVisibility(true);
	}
	frmOpenAccountNSConfirmation.lblGreetingNS.setVisibility(false);
	frmOpenAccountNSConfirmation.label47505874741589.text = kony.i18n.getLocalizedString("keyOpenCompNotifi");
	frmOpenAccountNSConfirmation.imgProdNSConfName.src = frmOpnActSelAct.imgNSProdName.src;
	frmOpenAccountNSConfirmation.lblHdrTxt.text = kony.i18n.getLocalizedString("Confirmation");
	frmOpenAccountNSConfirmation.lblAccuntNo.text = kony.i18n.getLocalizedString("AccountNo"); 
	
    frmOpenAccountNSConfirmation.btnOpenMore.text = kony.i18n.getLocalizedString("btnOpenMore");
    frmOpenAccountNSConfirmation.btnConfirm.text = kony.i18n.getLocalizedString("keyConfirm");
    frmOpenAccountNSConfirmation.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
	
		if (kony.i18n.getCurrentLocale() == "en_US") {
			frmOpenAccountNSConfirmation.lblInterestRate.text = gblFinActivityLogOpenAct["intersetRateLabelEN"] + ":"
			frmOpenAccountNSConfirmation.lblProdTypeVal.text = gblFinActivityLogOpenAct["prodNameEN"];
			frmOpenAccountNSConfirmation.lblProdNSConfName.text = gblFinActivityLogOpenAct["prodNameEN"];
			} else {
			frmOpenAccountNSConfirmation.lblInterestRate.text = gblFinActivityLogOpenAct["intersetRateLabelTH"] + ":"
			frmOpenAccountNSConfirmation.lblProdTypeVal.text =gblFinActivityLogOpenAct["prodNameTH"];
			frmOpenAccountNSConfirmation.lblProdNSConfName.text = gblFinActivityLogOpenAct["prodNameTH"];
			}
	
	frmOpenAccountNSConfirmation.lblNickNameVal.text = frmOpnActSelAct.txtOpenActNicNam.text;
	gblFinActivityLogOpenAct["toActNickName"] = frmOpnActSelAct.txtOpenActNicNam.text;
	frmOpnActSelAct.txtOpenActNicNam.skin = txtNormalBG;
	frmOpnActSelAct.txtOpenActSelAmt.skin = txtNormalBG;
	frmOpenAccountNSConfirmation.lblOpenAmtVal.text = commaFormatted(parseFloat(removeCommos(gblFinActivityLogOpenAct["transferAmount"])).toFixed(2)) + kony.i18n.getLocalizedString("currencyThaiBaht");
	
      frmOpenAccountNSConfirmation.lblBranchVal.text = frmOpnActSelAct["segNSSlider"]["selectedItems"][0].lblRemainFeeValue;
      gblFinActivityLogOpenAct["toBranchName"] = frmOpnActSelAct["segNSSlider"]["selectedItems"][0].lblRemainFeeValue;
      frmOpenAccountNSConfirmation.lblActName.text = frmOpnActSelAct["segNSSlider"]["selectedItems"][0].hiddenActName;
      frmOpenAccountNSConfirmation.imgNoFix.src = frmOpnActSelAct["segNSSlider"]["selectedItems"][0].imghidden;
      frmOpenAccountNSConfirmation.lblNFAccName.text = frmOpnActSelAct["segNSSlider"]["selectedItems"][0].lblCustName;
      frmOpenAccountNSConfirmation.lblNFAccBal.text = frmOpnActSelAct["segNSSlider"]["selectedItems"][0].lblSliderAccN2;
      frmOpenAccountNSConfirmation.lblNFAccType.text = frmOpnActSelAct["segNSSlider"]["selectedItems"][0].lblActNoval;
      gblFinActivityLogOpenAct["branchEN"] = frmOpnActSelAct["segNSSlider"]["selectedItems"][0].hiddenBranchNameEN;
      gblFinActivityLogOpenAct["branchTH"] = frmOpnActSelAct["segNSSlider"]["selectedItems"][0].hiddenBranchNameTH;
      gblFinActivityLogOpenAct["nickTH"] =frmOpnActSelAct["segNSSlider"]["selectedItems"][0].hiddenproductThaiAssign;
      gblFinActivityLogOpenAct["nickEN"] =frmOpnActSelAct["segNSSlider"]["selectedItems"][0].hiddenprodENGAssign;
      gblFinActivityLogOpenAct["actTypeFromOpen"] =frmOpnActSelAct["segNSSlider"]["selectedItems"][0].hiddenActType;
			
	frmOpenAccountNSConfirmation.lblNickName.text = kony.i18n.getLocalizedString("Nickname");
	frmOpenAccountNSConfirmation.lblProdType.text = kony.i18n.getLocalizedString("ProductName");
	frmOpenAccountNSConfirmation.lblAccuntNo.text = kony.i18n.getLocalizedString("AccountNo");
	frmOpenAccountNSConfirmation.lblAccounName.text = kony.i18n.getLocalizedString("AccountName");
	frmOpenAccountNSConfirmation.lblBranch.text = kony.i18n.getLocalizedString("Branch");
	frmOpenAccountNSConfirmation.lblOpeningAmt.text = kony.i18n.getLocalizedString("keyLblOpnAmount");
	frmOpenAccountNSConfirmation.lblOpeningDate.text = kony.i18n.getLocalizedString("keyLblOpeningDate");
	frmOpenAccountNSConfirmation.lblTransactionref.text = kony.i18n.getLocalizedString("keyTransactionRefNo");
	frmOpenAccountNSConfirmation.lblOpenAccountFrom.text = kony.i18n.getLocalizedString("keyOpenAmountFrom");
	
	
	frmOpenAccountNSConfirmation.lblInterestRateVal.text = frmOpenProdDetnTnC.lblOpenAccInt.text + "%" ;
	frmOpenAccountNSConfirmation.hbxOpenActNotify.setVisibility(false);
	frmOpenAccountNSConfirmation.hbxProdType.setVisibility(false);
	frmOpenAccountNSConfirmation.hbxAccuntNo.setVisibility(false);
	frmOpenAccountNSConfirmation.hbxAdv.setVisibility(false);
	
	  frmOpenAccountNSConfirmation.hboxBackMore.setVisibility(false);
      frmOpenAccountNSConfirmation.hbxCnfrmCncl.setVisibility(true);
      
	frmOpenAccountNSConfirmation.btnRight.skin = btnEdit;
	frmOpenAccountNSConfirmation.hbxCompIcon.setVisibility(false);
	frmOpenAccountNSConfirmation.vbxHideNS.setVisibility(false);
	//Made false by Vijay for share functionality PBI
    frmOpenAccountNSConfirmation.hboxaddfblist.setVisibility(false);
	frmOpenAccountNSConfirmation.hbxCannotTrans.setVisibility(false);
    frmOpenAccountNSConfirmation.lblNotAvailable.setVisibility(false);
	invokePartyInquiryOpenAct();
	}	
}

/*function onClickConfirmFromOpenAccount() {
	gblShowConfComplete = false
	frmOpenAccountNSConfirmation.hbxCompIcon.setVisibility(true);
	frmOpenAccountNSConfirmation.hbxAdv.setVisibility(true);
	frmOpenAccountNSConfirmation.hboxSubHeadr.setVisibility(false);
	frmOpenAccountNSConfirmation.hbxCnfrmCncl.setVisibility(false);
	frmOpenAccountNSConfirmation.show();
}

function onClickBackFromOpenAccount() {
	gblShowConfComplete = true
	frmOpenAccountNSConfirmation.hbxAdv.setVisibility(false);
	frmOpenAccountNSConfirmation.hbxCompIcon.setVisibility(false);
	frmOpenAccountNSConfirmation.hboxBackMore.setVisibility(false);
	frmOpenAccountNSConfirmation.hboxSubHeadr.setVisibility(true);
	frmOpenAccountNSConfirmation.hbxCnfrmCncl.setVisibility(true);
	frmOpenAccountNSConfirmation.show();
}*/

function onclickConfirmTransNSPwd() {
	frmOpenAccountNSConfirmation.btnRight.skin = btnShare;
	frmOpenAccountNSConfirmation.hbxProdType.setVisibility(false);
	frmOpenAccountNSConfirmation.hbxCompIcon.setVisibility(true);
	frmOpenAccountNSConfirmation.hbxAccuntNo.setVisibility(true);
	
	//frmOpenAccountNSConfirmation.hbxNickName.skin = hboxLightGrey;
	frmOpenAccountNSConfirmation.lblHdrTxt.text = kony.i18n.getLocalizedString("keylblComplete");
	//frmOpenAccountNSConfirmation.lblATMMsg.text = kony.i18n.getLocalizedString("ATMmsg");
	//frmOpenAccountNSConfirmation.scrollbox474135225108084.containerHeight = 74;
	frmOpenAccountNSConfirmation.hbxOpenActNotify.setVisibility(true);
	frmOpenAccountNSConfirmation.hbxAdv.setVisibility(true);
	
    frmOpenAccountNSConfirmation.hboxBackMore.setVisibility(true);
    frmOpenAccountNSConfirmation.hbxCnfrmCncl.setVisibility(false);
    frmOpenAccountNSConfirmation.btnOpenMore.text = kony.i18n.getLocalizedString("btnOpenMore");
    frmOpenAccountNSConfirmation.buttonBack.text = kony.i18n.getLocalizedString("keybtnReturn");
	
  	if (grtFormattedAmount(gblFinActivityLogOpenAct["transferAmount"]) >0){
	  frmOpenAccountNSConfirmation.vbxHideNS.setVisibility(true);
	}else{
	  frmOpenAccountNSConfirmation.vbxHideNS.setVisibility(false);
	}			
	
	//frmOpenAccountNSConfirmation.vbxHideNS.setVisibility(true);
	gblAckFlage = true;
	//if (kony.os.deviceInfo().name == "android"){
	//#ifdef android
	frmOpenAccountNSConfirmation.scrollboxMain.containerWeight = 185 ;
	//#endif
	//}
	//Code change for IOS9
	campaginService("imgOpnActNSAdd","frmOpenAccountNSConfirmation","M");
    frmOpenAccountNSConfirmation.hboxaddfblist.setVisibility(true);
	frmOpenAccountNSConfirmation.show();
}
prodNameSavingCare = "";
function invokePartyInquiryOpenAct(){
	kony.print("inside invokePartyInquiryOpenAct ----->1111111111111");
	showLoadingScreen();
	var inputparam = {};
	invokeServiceSecureAsync("partyInquiry", inputparam, partyInquiryOpenActCallBack);
}


function partyInquiryOpenActCallBack(status, callBackResponse) {
	if (status == 400) {
		
		if (callBackResponse["opstatus"] == 0) {
		kony.print("inside partyInquiryOpenActCallBack -----22222222" + JSON.stringify(callBackResponse));
		var inputparam = {};
		gblPartyInqOARes = callBackResponse;
		if (gblPartyInqOARes["issuedIdentType"] == "CI" ){
	    	frmOpenActTDConfirm.lblOATDActNameVal.text  = gblPartyInqOARes["FullName"];
			frmOpenActDSConfirm.lblOADSActNameVal.text  = gblPartyInqOARes["FullName"];
			frmOpenAccountNSConfirmation.lblActName.text  = gblPartyInqOARes["FullName"];
			}else{
				frmOpenActTDConfirm.lblOATDActNameVal.text = gblPartyInqOARes["PrefName"];
				frmOpenActDSConfirm.lblOADSActNameVal.text = gblPartyInqOARes["PrefName"];
				frmOpenAccountNSConfirmation.lblActName.text = gblPartyInqOARes["PrefName"];
			}
		inputparam["fullName"] = callBackResponse["customerName"];
		gblCustomerName = callBackResponse["customerName"];
		gblPOWcustNME = callBackResponse["customerName"];
		gblPOWchannel = "m";
		gblPOWtransXN = "Open Accounts";
		inputparam["prodCode"] = "02";
		//alert("isCmpFlow::"+isCmpFlow+"::gblSelOpenActProdCode::"+gblSelOpenActProdCode);
		//inputparam["productCode"] = "211";
		inputparam["productCode"] = gblSelOpenActProdCode;
		
		inputparam["cardNo"] = callBackResponse["issuedIdentValue"];
		//inputparam["amount"] = frmMBSavingsCareAddNickName.txtAmount.text;
		inputparam["amount"] = gblFinActivityLogOpenAct["transferAmount"];
		kony.print("gblSelProduct---->555555555" + gblSelProduct);
		
		if(gblSelProduct == "ForUse"){
			//inputparam["userAccountNum"] = "xxx-x-"+ frmOpenAccountNSConfirmation.lblNFAccType.text.substring(6, 11) + "-x";
			inputparam["userAccountNum"] = kony.string.replace(frmOpenAccountNSConfirmation.lblNFAccType.text, "-", "");
			inputparam["ProductName"] = frmOpenAccountNSConfirmation.lblProdNSConfName.text;
			inputparam["openAccType"] = "SDA";
		}
		else if(gblSelProduct == "TMBDreamSavings"){
				//inputparam["userAccountNum"] = "xxx-x-"+ frmOpenActDSConfirm.lblOAMnthlySavType.text.substring(6, 11) + "-x";
				inputparam["userAccountNum"] = kony.string.replace(frmOpenActDSConfirm.lblOAMnthlySavNum.text, "-", "");
				inputparam["ProductName"] = frmOpenActDSConfirm.lblDSCnfmTitle.text;
				inputparam["openAccType"] = "SDA";				
		}
		else if(gblSelProduct == "TMBSavingcare"){
			inputparam["productCode"] = "211";
			inputparam["amount"] = frmMBSavingsCareAddNickName.txtAmount.text; //Check
			if (kony.i18n.getCurrentLocale() == "en_US") {
				prodNameSavingCare = gblFinActivityLogOpenAct["prodNameEN"];
			} else {
				prodNameSavingCare = gblFinActivityLogOpenAct["prodNameTH"];
			}
			prodNameSavingCare="Savings Care Account";
			//inputparam["userAccountNum"] = "xxx-x-"+ frmOpenActSavingCareCnfNAck.lblOASCFrmNumAck.text.substring(6, 11) + "-x";
		if(gblSavingsCareFlow=="AccountDetailsFlow" || gblSavingsCareFlow=="MyAccountFlow"){
			inputparam["userAccountNum"] = gblAccountId;
			gblFinActivityLogOpenAct["transferAmount"] = "";
			inputparam["amount"] = "";
		}else{
			inputparam["userAccountNum"] = kony.string.replace(frmMBSavingsCareAddBal["segNSSlider"]["selectedItems"][0].lblActNoval, "-", "");
			var temp = frmMBSavingsCareAddNickName.txtAmount.text
			gblFinActivityLogOpenAct["transferAmount"] = removeCommos(frmMBSavingsCareAddNickName.txtAmount.text)+"";
			//commaFormattedOpenAct(frmMBSavingsCareAddNickName.txtAmount.text);
			inputparam["amount"] = removeCommos(frmMBSavingsCareAddNickName.txtAmount.text)+"";
		}
			inputparam["ProductName"] = prodNameSavingCare;
			inputparam["openAccType"] = "SDA";
		
			kony.print(" GOWRI gblFinActivityLogOpenAct[transferAmount]" + gblFinActivityLogOpenAct["transferAmount"]);			
		}
		else if(gblSelProduct == "ForTerm"){	
			//inputparam["userAccountNum"] = "xxx-x-"+ frmOpenActTDConfirm.lblTDTransFromAccBal.text.substring(6, 11) + "-x";
			inputparam["userAccountNum"] = kony.string.replace(frmOpenActTDConfirm.lblTDTransFromAccBal.text, "-", "");
			
			inputparam["activityTypeID"] = "041";
			inputparam["activityFlexValues1"] = gblSelOpenActProdCode + "+" + frmOpenActTDConfirm.lblTDCnfmTitle.text;
			inputparam["activityFlexValues3"] = frmOpenActTDConfirm.lblOATDNickNameVal.text;
			inputparam["activityFlexValues4"] = gblFinActivityLogOpenAct["transferAmount"];
			inputparam["activityFlexValues5"] = "";
			
			if(gblSelOpenActProdCode == "659"){
				inputparam["ProductName"] = "Up and Up Account 24 Months";
			}
			else{
				inputparam["ProductName"] = frmOpenActTDConfirm.lblTDCnfmTitle.text;	
			}
			inputparam["openAccType"] = "CDA";
		}
		//For Activity Logging


	var platformChannel = gblDeviceInfo.name;
	if (platformChannel == "thinclient")
		inputparam["channelId"] = GLOBAL_IB_CHANNEL;
	else
		inputparam["channelId"] = GLOBAL_MB_CHANNEL;
			inputparam["logLinkageId"] = "";
			inputparam["deviceNickName"] = "";
			inputparam["activityFlexValues2"] = "";

	if(gblSelOpenActProdCode == "200" || gblSelOpenActProdCode == "220"  || gblSelOpenActProdCode == "222" || (gblOpenActList["FOR_USE_PRODUCT_CODES"] != null && gblOpenActList["FOR_USE_PRODUCT_CODES"] != undefined && gblOpenActList["FOR_USE_PRODUCT_CODES"].indexOf(gblSelOpenActProdCode) > 0)){
		
			inputparam["activityTypeID"] = "039";
			inputparam["activityFlexValues1"] = gblSelOpenActProdCode + "+" + frmOpenAccountNSConfirmation.lblProdNSConfName.text;
			inputparam["activityFlexValues3"] = frmOpenAccountNSConfirmation.lblNickNameVal.text;
			inputparam["activityFlexValues4"] = gblFinActivityLogOpenAct["transferAmount"];
			inputparam["activityFlexValues5"] = "";
		
		//activityLogServiceCall("039", "", "00", "", gblSelOpenActProdCode + "+" + frmOpenAccountNSConfirmation.lblProdNSConfName.text, "",frmOpenAccountNSConfirmation.lblNickNameVal.text, gblFinActivityLogOpenAct["transferAmount"], "", ""); //Savings 
	}else if(gblSelOpenActProdCode == "221"){
		
		
			inputparam["activityTypeID"] = "042";
			inputparam["activityFlexValues1"] = gblSelOpenActProdCode + "+" + frmOpenAccountNSConfirmation.lblProdNSConfName.text;
			inputparam["activityFlexValues3"] = frmOpenAccountNSConfirmation.lblNickNameVal.text;
			inputparam["activityFlexValues4"] = gblFinActivityLogOpenAct["transferAmount"];
			inputparam["activityFlexValues5"] = "";
		
		//activityLogServiceCall("042", "", "00", "", gblSelOpenActProdCode + "+" + frmOpenAccountNSConfirmation.lblProdNSConfName.text, "",frmOpenAccountNSConfirmation.lblNickNameVal.text, gblFinActivityLogOpenAct["transferAmount"], "", "");
	}else if(gblSelOpenActProdCode == "206"){
		var transferAmount = frmOpenActDSConfirm.lblOADSAmtVal.text;
		var targetAmount = frmOpenAcDreamSaving.txtODTargetAmt.text;
		var bhat =kony.i18n.getLocalizedString("currencyThaiBaht");
		transferAmount = transferAmount.replace(bhat,"");
		targetAmount = targetAmount.replace(bhat, "").replace(/,/g, "")
    	frmOpenActDSAck.label475124774164.text = kony.i18n.getLocalizedString("Complete");
		
		transferAmount = transferAmount.replace(/,/g, "");
		
		
			inputparam["activityTypeID"] = "040";
			inputparam["activityFlexValues1"] = frmOpenActDSConfirm.lblOADMnthVal.text;
			inputparam["activityFlexValues3"] = frmOpenActDSConfirm.lblOAMnthlySavNum.text;
			inputparam["activityFlexValues4"] = transferAmount;
			inputparam["activityFlexValues5"] = frmOpenAcDreamSaving["segSliderOpenDream"]["selectedItems"][0].imgDreamName + "+" + targetAmount;
			
			
		/*if(flowSpa)
		{
		activityLogServiceCall("040", "", "00", "", frmOpenActDSConfirm.lblOADMnthVal.text, "",frmOpenActDSConfirm.lblOAMnthlySavNum.text, transferAmount, (gblspaDreamName + "+" + targetAmount), ""); //Dream Savings
		}
		else
		{
			
		activityLogServiceCall("040", "", "00", "", frmOpenActDSConfirm.lblOADMnthVal.text, "",frmOpenActDSConfirm.lblOAMnthlySavNum.text, transferAmount, (frmOpenAcDreamSaving["segSliderOpenDream"]["selectedItems"][0].imgDreamName + "+" + targetAmount), ""); //Dream Savings
		}*/
		}else if(gblAccountTable["SAVING_CARE_PRODUCT_CODES"].indexOf(gblSelOpenActProdCode) >= 0){
		
				/*
				var befPer = frmOpenActSavingCareCnfNAck.lblBefPerCnfrmVal1.text;
				var befPerVthSym = "";
				befPer = befPer.replace("%", "");
				var befName = frmOpenActSavingCareCnfNAck.lblBefNameValCnfrm1.text + " " + frmOpenActSavingCareCnfNAck.lblBefRsCnfrmVal1.text + " " + befPer;
				if (frmOpenActSavingCareCnfNAck.hbxBefNameCnfrm2.isVisible == true){
				befPerVthSym = frmOpenActSavingCareCnfNAck.lblBefPerCnfrmVal2.text;
				befPerVthSym =befPerVthSym.replace("%", "");
				befName = befName + "," + frmOpenActSavingCareCnfNAck.lblBefNameValCnfrm2.text + " " + frmOpenActSavingCareCnfNAck.lblBefRsCnfrmVal2.text + " " + befPerVthSym;
				}
				if (frmOpenActSavingCareCnfNAck.hbxBefNameCnfrm3.isVisible == true){
				befPerVthSym = frmOpenActSavingCareCnfNAck.lblBefPerCnfrmVal3.text;
				befPerVthSym =befPerVthSym.replace("%", "");
				befName = befName + "," + frmOpenActSavingCareCnfNAck.lblBefNameValCnfrm3.text + " " + frmOpenActSavingCareCnfNAck.lblBefRsCnfrmVal3.text + " " + befPerVthSym;
				}
				if (frmOpenActSavingCareCnfNAck.hbxBefNameCnfrm4.isVisible == true){
				befPerVthSym = frmOpenActSavingCareCnfNAck.lblBefPerCnfrmVal4.text;
				befPerVthSym =befPerVthSym.replace("%", "");
				befName = befName + "," + frmOpenActSavingCareCnfNAck.lblBefNameValCnfrm4.text + " " + frmOpenActSavingCareCnfNAck.lblBefRsCnfrmVal4.text + " " + befPerVthSym;
				}
				if (frmOpenActSavingCareCnfNAck.hbxBefNameCnfrm5.isVisible == true){
				befPerVthSym = frmOpenActSavingCareCnfNAck.lblBefPerCnfrmVal5.text;
				befPerVthSym =befPerVthSym.replace("%", "");
				befName = befName + "," + frmOpenActSavingCareCnfNAck.lblBefNameValCnfrm5.text + " " + frmOpenActSavingCareCnfNAck.lblBefPerCnfrmVal5.text + " " + befPerVthSym;
				}
				**/
					kony.print("GOWRI 9 ");			
				var beneficiaryNames = "";
				var relation = "";
				var percentage = "";
				var bName = "";
				for(var i = 0;i<arrayBenificiary.length;i++){
					beneficiaryNames = beneficiaryNames + arrayBenificiary[i]["firstName"] + arrayBenificiary[i]["lastName"]  + " " + arrayBenificiary[i]["relation"] + " " + arrayBenificiary[i]["percentage"] + "," ;
					relation = relation + arrayBenificiary[i]["relation"]+",";
					percentage = percentage + arrayBenificiary[i]["percentage"]+",";
					//bName = bName + arrayBenificiary[i]["firstName"]+ " " + arrayBenificiary[i]["lastName"] + ",";		
				}
				//bName = bName.substring(bName, bName.length - 1);
				relation = relation.substring(relation, relation.length - 1);
				percentage = percentage.substring(percentage, percentage.length - 1);
				beneficiaryNames = beneficiaryNames.substring(beneficiaryNames, beneficiaryNames.length - 1);
		
				kony.print("GOWRI beneficiaryNames------>" + beneficiaryNames);
		
			inputparam["activityTypeID"] = "081";
			inputparam["activityFlexValues1"] = gblSelOpenActProdCode + "+" + prodNameSavingCare;
			kony.print("GOWRI 11 gblSelOpenActProdCode="+gblSelOpenActProdCode);	
			
			if(gblSavingsCareFlow=="AccountDetailsFlow" || gblSavingsCareFlow=="MyAccountFlow"){
				inputparam["activityFlexValues3"] = gblOpenSavingsCareNickName;
				inputparam["activityFlexValues4"] = ""; //"5100";
			}else{
				inputparam["activityFlexValues3"] = frmMBSavingsCareAddNickName.txtNickName.text;
				inputparam["activityFlexValues4"] = removeCommos(frmMBSavingsCareAddNickName.txtAmount.text)+"";
			}
			
			 // TO -DO hard coded nick name, remove it
			
			inputparam["activityFlexValues5"] = beneficiaryNames;
		//activityLogServiceCall("081", "", "00", "", gblSelOpenActProdCode + "+" + frmOpenActSavingCareCnfNAck.lblOASCTitle.text, "",frmOpenActSavingCareCnfNAck.lblOASCNicNamVal.text, gblFinActivityLogOpenAct["transferAmount"], befName, ""); //Savings Care
		}
		
		
		//End of Activity Logging
		
		
		invokeServiceSecureAsync("SaveOpenAccountTxn", inputparam, SaveOpenAccountTxnCallBack);
		}else{
				dismissLoadingScreen();	
				showAlert(callBackResponse["errMsg"], kony.i18n.getLocalizedString("info"));
				return false;
		}
	}
}


function SaveOpenAccountTxnCallBack(status, callBackResponse){

	if (status == 400) {
		
		if (callBackResponse["opstatus"] == 0) {
					if(callBackResponse["openActBusinessHrsFlag"] == "false") {
             			var key = kony.i18n.getLocalizedString("keyBusinessHours");
           				var res = key.replace("<time>",callBackResponse["openActStartTime"] + "-" + callBackResponse["openActEndTime"]);
             			showAlert(res,kony.i18n.getLocalizedString("info"));
						return false;
            		}
			
					dismissLoadingScreen();
					kony.print("inside SaveOpenAccountTxnCallBack()--------->" + JSON.stringify(callBackResponse));
					var openingDate = callBackResponse["openingDate"];	
					gblFinActivityLogOpenAct["openingDate"] = openingDate;
					if(gblSelProduct == "ForUse"){				
						//if ((gblFinActivityLogOpenAct["transferAmount"]) >0){
						frmOpenAccountNSConfirmation.hbxTransRefNo.setVisibility(true);
						frmOpenAccountNSConfirmation.lblOpenActCnfmBal.setVisibility(true);
						frmOpenAccountNSConfirmation.lblOpenActCnfmBal.text = kony.i18n.getLocalizedString("keyBalanceBeforTrans");
						frmOpenAccountNSConfirmation.lblOpenActCnfmBalAmt.setVisibility(true);
						frmOpenAccountNSConfirmation.hbxOpenAmt.setVisibility(true);
												
						frmOpenAccountNSConfirmation.hbxNickName.skin = hboxLightGrey;
						frmOpenAccountNSConfirmation.hbox47420125326014.skin = hboxWhite;
						frmOpenAccountNSConfirmation.hbxBranch.skin = hboxLightGrey;
						frmOpenAccountNSConfirmation.hbxOpenAmt.skin = hboxWhite;						
						frmOpenAccountNSConfirmation.hbxIntrstRate.skin = hboxLightGrey;
						frmOpenAccountNSConfirmation.hbxOpenDate.skin = hboxWhite;
						frmOpenAccountNSConfirmation.hbxCardType.skin = hboxLightGrey;
						frmOpenAccountNSConfirmation.hbxCardFee.skin = hboxWhite;
						frmOpenAccountNSConfirmation.hbxTransRefNo.skin = hboxLightGrey;
						
                        frmOpenAccountNSConfirmation.lblOpenActCnfmBalAmt.text = frmOpnActSelAct["segNSSlider"]["selectedItems"][0].lblBalance;
						//frmOpenAccountNSConfirmation.vbxHideNS.setVisibility(true);
						callGenerateTransferRefNoserviceOpenActMB();
						
						/*
						}else{
						frmOpenAccountNSConfirmation.hbxTransRefNo.setVisibility(false);
						frmOpenAccountNSConfirmation.lblOpenActCnfmBal.setVisibility(false);
						frmOpenAccountNSConfirmation.lblOpenActCnfmBalAmt.setVisibility(false);
						frmOpenAccountNSConfirmation.hbxOpenAmt.setVisibility(false);
						frmOpenAccountNSConfirmation.hbxIntrstRate.skin = hboxWhite;
						frmOpenAccountNSConfirmation.hbxOpenDate.skin = hboxLightGrey;
						//frmOpenAccountNSConfirmation.vbxHideNS.setVisibility(false);
						frmOpenAccountNSConfirmation.show();
						}
						*/			
	   					frmOpenAccountNSConfirmation.lblOpeningDateVal.text = openingDate;
					}else if(gblSelProduct == "TMBDreamSavings"){
					frmOpenActDSConfirm.lblDSOpenDateVal.text = openingDate;
					frmOpenActDSConfirm.show();
					}else if(gblSelProduct == "TMBSavingcare"){	
						kony.print("1111111111111");				
						callGenerateTransferRefNoserviceOpenActMB();
						kony.print("GOWRI 333333333333");
						showOpenSavingscareConfirm();
					}else if(gblSelProduct == "ForTerm"){
						if ((gblFinActivityLogOpenAct["transferAmount"]) >0){
						frmOpenActTDConfirm.hbxTdTransRef.setVisibility(true);
						frmOpenActTDAck.hbxOATDTransRefAck.setVisibility(true);
						frmOpenActTDAck.vbox156335099531837.setVisibility(true);
						frmOpenActTDAck.hbxOATDAmtAck.setVisibility(true);
						frmOpenActTDAck.lblOpenActCnfmBalAmt.setVisibility(true);
						frmOpenActTDConfirm.lblOpenActCnfmBal.setVisibility(true);
						frmOpenActTDConfirm.lblOpenActCnfmBalAmt.setVisibility(true);
						frmOpenActTDConfirm.hbxOATDIntRate.skin = hboxLightGrey;
						frmOpenActTDConfirm.hbxTDOpenDate.skin = hboxWhite;
						frmOpenActTDConfirm.hbxOATDAmt.setVisibility(true);
						
						frmOpenActTDAck.lblOpenActCnfmBalAmt.text = frmOpenAccTermDeposit["segSliderTDFrom"]["selectedItems"][0].lblBalance;
						frmOpenActTDConfirm.lblOpenActCnfmBalAmt.text = frmOpenAccTermDeposit["segSliderTDFrom"]["selectedItems"][0].lblBalance;
                          
                        frmOpenActTDAck.label47505874739957.setVisibility(true);
						frmOpenActTDAck.hbxOATDIntRateAck.skin = hboxLightGrey;
						frmOpenActTDAck.hbxTDOpenDateAck.skin = hboxWhite;
						frmOpenActTDAck.hbxTDMatDateAck.skin = hboxLightGrey;
						callGenerateTransferRefNoserviceOpenActMB();
						}else{
						frmOpenActTDConfirm.hbxTdTransRef.setVisibility(false);
						frmOpenActTDAck.hbxOATDTransRefAck.setVisibility(false);
						frmOpenActTDAck.vbox156335099531837.setVisibility(false);
						frmOpenActTDAck.lblOpenActCnfmBalAmt.setVisibility(false);
						frmOpenActTDAck.label47505874739957.setVisibility(false);
						frmOpenActTDConfirm.lblOpenActCnfmBal.setVisibility(false);
						frmOpenActTDConfirm.lblOpenActCnfmBalAmt.setVisibility(false);
						frmOpenActTDConfirm.hbxOATDAmt.setVisibility(false);
						frmOpenActTDAck.hbxOATDAmtAck.setVisibility(false);
						frmOpenActTDConfirm.hbxOATDIntRate.skin = hboxWhite;
						frmOpenActTDConfirm.hbxTDOpenDate.skin = hboxLightGrey;
						frmOpenActTDAck.hbxOATDIntRateAck.skin = hboxWhite;
						frmOpenActTDAck.hbxTDOpenDateAck.skin = hboxLightGrey;
						frmOpenActTDAck.hbxTDMatDateAck.skin = hboxWhite;
						frmOpenActTDConfirm.show();	
						}
						frmOpenActTDConfirm.lblTDOpenDateVal.text = openingDate;
			}
		}else{
			
			
			 if (callBackResponse["errCode"] == "Entered amount is greater than maximum amount") {
	                dismissLoadingScreen();
	                showAlert(kony.i18n.getLocalizedString("keyMaxLimitAmt"), kony.i18n.getLocalizedString("info"));
	                return false;
            }else  if (callBackResponse["errCode"] == "Entered amount is less than minimum amount") {
	                dismissLoadingScreen();
	                showAlert(kony.i18n.getLocalizedString("keyMinLimitAmt"), kony.i18n.getLocalizedString("info"));
	                return false;
            }else{
	             dismissLoadingScreen();
	             showAlert(kony.i18n.getLocalizedString("keyCannotOpenAct"), kony.i18n.getLocalizedString("info"));
	             return false;
            }
			
		}
	}
	dismissLoadingScreen();
}


function checkTermDepositCount() {

 var count = 0;
 if(gblAccountTable["custAcctRec"]!=null)
 {
 for (var i = 0; i < gblAccountTable["custAcctRec"].length; i++) {
  if (gblAccountTable["custAcctRec"][i]["accType"] == "SDA" || gblAccountTable["custAcctRec"][i]["accType"] == "DDA" ) {
   count = count + 1;
   
  }
 }
 }
 return count;
}


function invokeDepositAOXferOpenAct(acctVal,accountName,fromactFiidentvalue,toactFiidentvalue,toDepositActNum,toDepositActType,fromDepositActType,transferAmount,dreamAmt,dreamMonth,fromProdId){
	var inputparam = {};
	var accTypeCd;
	var transCodeFrom;
	var transCodeTo;
	 var transferAmountIndex = transferAmount.indexOf(",");
	if(transferAmountIndex > 0)
		{	
		transferAmount = transferAmount.replace(/,/g, "");
		}
	
	
	
	dreamAmt = dreamAmt.replace(/,/g, "");
		inputparam["clientName"] = "MB";
			if (gblSelOpenActProdCode == "200" || gblAccountTable["SAVING_CARE_PRODUCT_CODES"].indexOf(gblSelOpenActProdCode) >= 0  || gblSelOpenActProdCode == "206" || gblSelOpenActProdCode == "220" || gblSelOpenActProdCode == "221" || gblSelOpenActProdCode == "222"){
			 accTypeCd = "0200";
			 inputparam["acctType"] = "SDA";
			 inputparam["acctTypeValAcctRef"] = "SDA";
			 transCodeTo = "2";
			}
			else if (gblSelOpenActProdCode == "300" || gblSelOpenActProdCode == "301"  || gblSelOpenActProdCode == "302" || gblSelOpenActProdCode == "601" || gblSelOpenActProdCode == "602" || gblSelOpenActProdCode == "659" || gblSelOpenActProdCode == "664" || gblSelOpenActProdCode == "666"){
			 accTypeCd = "0300";
			 inputparam["cnt"] = "";
			 inputparam["termUnits"] = "Months";
			 inputparam["acctType"] = "CDA";
			 //inputparam["intDisp"] = "TransferToOtherAcct";	
			 inputparam["acctTypeValAcctRef"] = "CDA";
			 transCodeTo = "3";
			
			if(gblisDisToActTD == "Y"){
			 //To Accouts input params
					inputparam["acctTypeDepAcct"] = toDepositActType; //SDA,DDA
					if (toDepositActType == "SDA"){
						inputparam["acctidentValKeys"] = "0000" + toDepositActNum;
						inputparam["fiIdentDepAcctData"] = toactFiidentvalue;
					}
					else if(toDepositActType == "DDA"){
						inputparam["acctidentValKeys"] = toDepositActNum;
						inputparam["fiIdentDepAcctData"] = toactFiidentvalue;
					}
			   		inputparam["recurIntrvl"] = "00";
			  }
			 //End of To Accouts input params  			
			
			 //If product code is 659 
					if(gblSelOpenActProdCode == "659"){
						inputparam["acctTypeDepAcctTD"] = toDepositActType; //SDA,DDA
						inputparam["renewalOption"] = "None";
						if (fromDepositActType == "SDA"){
							inputparam["actIndValTD"] = "0000" + toDepositActNum;
							inputparam["fiIdentDepAcctDataTD"] = toactFiidentvalue;
						}
						else if(fromDepositActType == "DDA"){
							inputparam["actIndValTD"] = toDepositActNum;
							inputparam["fiIdentDepAcctDataTD"] = toactFiidentvalue;
						}
					}
			//End of product code is 659 	
		
			   		
			} //End of code for TD accounts
		var fiIdentVal = fromactFiidentvalue.substring(0,12);
		inputparam["fiIdent"] = fiIdentVal+ accTypeCd;
		inputparam["brnchIdent"] = "00" + acctVal.substring(0,3);
		inputparam["taxWithHolding"] = "1";
		inputparam["prodIdent"] = gblSelOpenActProdCode;
		
		if (gblPartyInqOARes["issuedIdentType"] == "CI" ){
		inputparam["acctTitle"] = gblPartyInqOARes["FullName"];
		}else{
		inputparam["acctTitle"] = gblPartyInqOARes["PrefName"];
		}
					
		//inputparam["acctTitle"] = accountName;
			
		inputparam["miscDate"] = gblPartyInqOARes["birthDate"];
		for (var i = 0; i < gblPartyInqOARes["Persondata"].length; i++) {
			if (gblPartyInqOARes["Persondata"][i]["AddrType"] != null && gblPartyInqOARes["Persondata"][i]["AddrType"] != "") 
			{
				if (gblPartyInqOARes["Persondata"][i]["AddrType"] == "Primary"){
				
				inputparam["addrType"] = gblPartyInqOARes["Persondata"][i]["AddrType"];
				inputparam["addr1"] = gblPartyInqOARes["Persondata"][i]["addr1"];
				inputparam["addr2"] = gblPartyInqOARes["Persondata"][i]["addr2"];
				inputparam["addr3"] = gblPartyInqOARes["Persondata"][i]["addr3"];
				inputparam["city"] = gblPartyInqOARes["Persondata"][i]["City"];
				if(gblPartyInqOARes["Persondata"][i]["PostalCode"] != "" && gblPartyInqOARes["Persondata"][i]["PostalCode"] != null && gblPartyInqOARes["Persondata"][i]["PostalCode"] != undefined){
				
				inputparam["postalCode"] = gblPartyInqOARes["Persondata"][i]["PostalCode"];
				}else{
				
				inputparam["postalCode"] = "";
				}
				inputparam["cntryCodeVal"] = "TH"//gblPartyInqOARes["Persondata"][i]["countryCode"];
				break;
				}
				
				if (gblPartyInqOARes["Persondata"][i]["AddrType"] == "Registered"){
				
				inputparam["addrType"] = gblPartyInqOARes["Persondata"][i]["AddrType"];
				inputparam["addr1"] = gblPartyInqOARes["Persondata"][i]["addr1"];
				inputparam["addr2"] = gblPartyInqOARes["Persondata"][i]["addr2"];
				inputparam["addr3"] = gblPartyInqOARes["Persondata"][i]["addr3"];
				inputparam["city"] = gblPartyInqOARes["Persondata"][i]["City"];
				if(gblPartyInqOARes["Persondata"][i]["PostalCode"] != "" && gblPartyInqOARes["Persondata"][i]["PostalCode"] != null && gblPartyInqOARes["Persondata"][i]["PostalCode"] != undefined){
				
				inputparam["postalCode"] = gblPartyInqOARes["Persondata"][i]["PostalCode"];
				}else{
				
				inputparam["postalCode"] = "";
				}
				inputparam["cntryCodeVal"] ="TH"// gblPartyInqOARes["Persondata"][i]["countryCode"];
				break;
				}
			
			}
		}
		if (fromDepositActType == "SDA"){
		 	inputparam["acctidentValKeysFrom"] = "0000" + acctVal;
		 	inputparam["acctTypeValAcctRefMstrAcctkeys"] = "SDA";
		}
		else if(fromDepositActType == "DDA"){
			inputparam["acctidentValKeysFrom"] = acctVal;
			inputparam["acctTypeValAcctRefMstrAcctkeys"] = "DDA";
			
		}
		inputparam["fiIdentMstrAcct"] = fromactFiidentvalue;
		inputparam["channelId"] = "MB";
		
		
		//If transfer happens -- check condition if transfer amount is greater than zero or not and transfer  won't happen for dream savings.it happens only when bacth is executed
		if (transferAmount > 0 && gblSelOpenActProdCode != "206"){
			if (fromDepositActType == "SDA"){
			 	inputparam["acctidentValKeysIfTo"] = "0000" + acctVal;
			 	inputparam["acctTypeFrmAcctRef"] = "SDA";
			 	transCodeFrom = "2";
			 	
			}
			else if(fromDepositActType == "DDA"){
				inputparam["acctidentValKeysIfTo"] = acctVal;
				inputparam["acctTypeFrmAcctRef"] = "DDA";
				transCodeFrom = "1";
				
			}
		inputparam["fiIdentMstrAcctFrmAcct"] = fromactFiidentvalue;	
		inputparam["amtxferInfo"] = transferAmount;
		inputparam["transCode"] = "88" + transCodeFrom + transCodeTo;
		inputparam["postDate"] = kony.os.date("yyyy-mm-dd"); //Need to change to server date
		}
		//End of if transfer amount is greater than zero
		
		
		//If dream saving 

			if (gblSelOpenActProdCode == "206"){
			
					if (fromDepositActType == "SDA"){
					 	inputparam["acctidentValDream"] =  "0000" + acctVal;
					 	inputparam["accTypeValDream"] = "SDA";
					 	
					}
					else if(fromDepositActType == "DDA"){
						inputparam["acctidentValDream"] = acctVal;
						inputparam["accTypeValDream"] = "DDA";
						
					}
					inputparam["amtDream"] = dreamAmt;
					inputparam["dayOfMonthDream"] = dreamMonth;
					inputparam["fromAccProdIdDream"] = fromProdId;
					inputparam["toAcctProdIdDream"] = gblSelOpenActProdCode;
			}

		//End of Dream Saving
		
	
		invokeServiceSecureAsync("depositAOXferAdd", inputparam,srvOpenActAddOwnToRecepient);
	
}




function srvOpenActAddOwnToRecepient(status,resultable) {
	if (status == 400) {
		if (resultable["opstatus"] == 0){
		
			if(resultable["StatusCode"]!= 0){
			dismissLoadingScreen();
			showAlert(resultable["StatusDesc"], kony.i18n.getLocalizedString("info"));
			return false;
			}else{	
			
				
				var gblOverflowErrorFlag = true;
				var addRecipient_inputparam = {};
				addRecipient_inputparam["addAccntFlag"] = "true";
				for (var i = 0; i < gblPartyInqOARes["ContactNums"].length; i++) {
			    var PhnType = gblPartyInqOARes["ContactNums"][i]["PhnType"];
			    
			    
			    if (PhnType != null && PhnType != "" && gblPartyInqOARes["ContactNums"][i]["PhnNum"] != undefined) {
			     if (PhnType == "Mobile") {
			      if ((gblPartyInqOARes["ContactNums"][i]["PhnNum"] != null) && (gblPartyInqOARes["ContactNums"][i]["PhnNum"] != "" &&
			       gblPartyInqOARes["ContactNums"][i]["PhnNum"] != undefined)) {
			       gblPHONENUMBER = gblPartyInqOARes["ContactNums"][i]["PhnNum"];
			      }
			     }
			    }
			   }
				gblBalAfterXfer = resultable["fromActAvailBal"];
				gblFinActivityLogOpenAct["tdTerm"] = "";
				gblFinActivityLogOpenAct["tdInterestRate"] = "";
				gblFinActivityLogOpenAct["tdMaturityDate"] = "";
				//gblFinActivityLogOpenAct["tellerId"] = resultable["tellerId"];
				var accVal = resultable["acctIdentValue"];
				var accValLength = accVal.length;
				
				
				if (gblSelProduct == "TMBDreamSavings"){
				accVal = accVal.substring((accValLength-10), (accValLength-7)) + "-" + accVal.substring((accValLength-7), (accValLength-6)) + "-" + accVal.substring((accValLength-6), (accValLength-1)) + "-" + accVal.substring((accValLength-1), accValLength);
				frmOpenActDSAck.lblDSAckActNoVal.text = accVal;
				//frmOpenAccountNSConfirmation.lblOpenActCnfmBalAmt.text = gblBalAfterXfer;
				gblFinActivityLogOpenAct["toActId"] = resultable["acctIdentValue"];
				frmOpenActDSAck.label47505874741650.text =  kony.i18n.getLocalizedString("keyOpenNotifyOne");
				}else if(gblSelProduct == "TMBSavingcare"){
				accVal = accVal.substring((accValLength-10), (accValLength-7)) + "-" + accVal.substring((accValLength-7), (accValLength-6)) + "-" + accVal.substring((accValLength-6), (accValLength-1)) + "-" + accVal.substring((accValLength-1), accValLength);
				frmOpenActSavingCareCnfNAck.lblOASCActNumVal.text = accVal;
				frmOpenActSavingCareCnfNAck.label47505874741650.text = kony.i18n.getLocalizedString("keyOpenNotifyOne") ;
				gblFinActivityLogOpenAct["toActId"] = resultable["acctIdentValue"];
				
				if (resultable["xferStatusCode"] != 0){
					frmOpenActSavingCareCnfNAck.lblOpenActCnfmBalAmt.text = commaFormattedOpenAct("0") + kony.i18n.getLocalizedString("currencyThaiBaht");
					frmOpenActSavingCareCnfNAck.hbxCannotTrans.setVisibility(true);
					frmOpenActSavingCareCnfNAck.lblNotAvailable.setVisibility(true);
				}else{
					frmOpenActSavingCareCnfNAck.lblOpenActCnfmBalAmt.text = commaFormattedOpenAct(gblBalAfterXfer) + kony.i18n.getLocalizedString("currencyThaiBaht");
					frmOpenActSavingCareCnfNAck.hbxCannotTrans.setVisibility(false);
					frmOpenActSavingCareCnfNAck.lblNotAvailable.setVisibility(false);
				}
				
				}else if(gblSelProduct == "ForTerm"){
				accVal = accVal.substring((accValLength-13), (accValLength-10)) + "-" + accVal.substring((accValLength-10), (accValLength-9)) + "-" + accVal.substring((accValLength-9), (accValLength-4)) + "-" + accVal.substring((accValLength-4), (accValLength-3));
				frmOpenActTDAck.lblOATDAckActNoVal.text = accVal;
				frmOpenActTDAck.lblTDMatDateValAck.text = reformatDate(resultable["nxtMaturityDate"]);
				frmOpenActTDAck.lblOATDTermValAck.text = resultable["termCount"];
				
				frmOpenActTDAck.label47505874741650.text = kony.i18n.getLocalizedString("keyOpenNotifyOne");
				gblFinActivityLogOpenAct["toActId"] = "0000" + accVal.replace(/-/g, "");//resultable["acctIdentValue"];
				gblFinActivityLogOpenAct["tdTerm"] = resultable["termCount"];
				gblFinActivityLogOpenAct["tdMaturityDate"] = resultable["nxtMaturityDate"];
				gblFinActivityLogOpenAct["tdInterestRate"] = frmOpenActTDConfirm.lblOATDIntRateVal.text.replace("%", "");
				
				if (resultable["xferStatusCode"] != 0){
					frmOpenActTDAck.lblOpenActCnfmBalAmt.text = commaFormattedOpenAct("0") + kony.i18n.getLocalizedString("currencyThaiBaht");
					frmOpenActTDAck.hbxCannotTrans.setVisibility(true);
					frmOpenActTDAck.lblNotAvailable.setVisibility(true);
				}else{
					frmOpenActTDAck.lblOpenActCnfmBalAmt.text = commaFormattedOpenAct(gblBalAfterXfer) + kony.i18n.getLocalizedString("currencyThaiBaht");
					frmOpenActTDAck.hbxCannotTrans.setVisibility(false);
					frmOpenActTDAck.lblNotAvailable.setVisibility(false);
				}
				
				
				}else if(gblSelProduct == "ForUse"){
				
					accVal = accVal.substring((accValLength-10), (accValLength-7)) + "-" + accVal.substring((accValLength-7), (accValLength-6)) + "-" + accVal.substring((accValLength-6), (accValLength-1)) + "-" + accVal.substring((accValLength-1), accValLength);
					frmOpenAccountNSConfirmation.lblAccuntNoVal.text = accVal;
					gblFinActivityLogOpenAct["toActId"] = resultable["acctIdentValue"];
					gblFinActivityLogOpenAct["tdTerm"] = "";
					gblFinActivityLogOpenAct["tdMaturityDate"] = "";
					gblFinActivityLogOpenAct["tdInterestRate"] = "";
					frmOpenAccountNSConfirmation.label47505874741650.text = kony.i18n.getLocalizedString("keyOpenNotifyOne");
						
					if (resultable["xferStatusCode"] != 0){
					frmOpenAccountNSConfirmation.lblOpenActCnfmBalAmt.text = commaFormattedOpenAct("0") + kony.i18n.getLocalizedString("currencyThaiBaht");
					frmOpenAccountNSConfirmation.hbxCannotTrans.setVisibility(true);
					frmOpenAccountNSConfirmation.lblNotAvailable.setVisibility(true);
					
				}else{
						if (grtFormattedAmount(gblFinActivityLogOpenAct["transferAmount"]) >0){
						frmOpenAccountNSConfirmation.lblOpenActCnfmBal.text = kony.i18n.getLocalizedString("keyBalanceAfterTrans");
						frmOpenAccountNSConfirmation.lblOpenActCnfmBalAmt.text = commaFormatted(parseFloat(removeCommos(gblBalAfterXfer)).toFixed(2)) + kony.i18n.getLocalizedString("currencyThaiBaht");
						}
					frmOpenAccountNSConfirmation.hbxCannotTrans.setVisibility(false);
					frmOpenAccountNSConfirmation.lblNotAvailable.setVisibility(false);
				}	
				}
				var mobileNo = gblPHONENUMBER; // This should be retrieved from cache
				//
				var totalData = [];
				//var personnelRcData = ["_MyTMB_", , , mobileNo, , "Added", "Y"];
				// gblCustomerName = "JohnSmith"; //for testing  gblPartyInqOARes["customerName"]
				var personnelRcData = [gblPartyInqOARes["customerName"], , , mobileNo, , "Added", "Y"];
				totalData.push(personnelRcData);
				gblPersonalizeID = resultable["personalizedID"]; 
				 insertOpenActDateInDB();
				//addRecipient_inputparam["receipentList"] = totalData.toString();
				//invokeServiceSecureAsync("receipentNewAddService", addRecipient_inputparam, addOwnToOpenActRecepientCallBack);
				}
		}else{
		dismissLoadingScreen();	
		showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
		return false;
		}		
	}
}

/*function checkUserExistRelOpenActTableCallBack(status, resulttable) {
	
	if (status == 400) {
		
		
		
		if (resulttable["opstatus"] == 0) {
			if (resulttable["personalizedID"] == null) {
				
				if (gblOverflowErrorFlag == false)
					srvAddOwnToRecepient();
				else {
					dismissLoadingScreen();
					if (resulttable["errMsg"] != undefined && resulttable["errMsg"] != null)
						alert(" " + resulttable["errMsg"]);
					else
						showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
					
				}
			} else {
				gblOverflowErrorFlag = false;
				gblPersonalizeID = resulttable["personalizedID"];
				insertOpenActDateInDB();
				//myAccountListOtherBankService(gblRawDataTMB);
				
			}
		} else {
			dismissLoadingScreen();
			
			alert(" " + resulttable["errMsg"]);
		}
	} else if (status == 300) {
		dismissLoadingScreen();
		
	}
}*/


//function depositAOXfOpenActCallBack(status,callBackResponse){
function insertOpenActDateInDB(){
	
	//if(status == 400){
	//if(gblSelOpenActProdCode == "206" || gblSelOpenActProdCode == "203"){
		//if (callBackResponse["opstatus"] == 0){
			var inputparam = {};
			inputparam["channelId"] = "02";
			inputparam["personalizedId"] = gblPersonalizeID;
			
			if(gblSelOpenActProdCode == "206"){
				inputparam["nickName"] = frmOpnActSelAct.txtOpenActNicNam.text;
				inputparam["dreamTargetId"] = frmOpenAcDreamSaving["segSliderOpenDream"]["selectedItems"][0].hiddenDreamTargetId;
				inputparam["dreamDesc"] = frmOpenAcDreamSaving.txtODNickNameVal.text;
				inputparam["personalizedActId"] = gblFinActivityLogOpenAct["toActId"];
				var tarAmou = frmOpenAcDreamSaving.txtODTargetAmt.text;
				inputparam["dreamTargetAmnt"] = tarAmou.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(/,/g, "");
				inputparam["flowIdOpenAct"] = "dreamSaving";
			}else if (gblSelOpenActProdCode == "203"){
				inputparam["nickName"] = frmOpenAccountSavingCareMB.txtNickName.text;
				inputparam["flowIdOpenAct"] = "SavingCare";
				inputparam["personalizedActId"] = gblFinActivityLogOpenAct["toActId"];
				var befName = frmOpenActSavingCareCnfNAck.lblBefNameValCnfrm1.text;
				var befRel = frmOpenActSavingCareCnfNAck.lblBefRsCnfrmVal1.text;
				var befPer = frmOpenActSavingCareCnfNAck.lblBefPerCnfrmVal1.text;
				var befPerVthSym = "";
				befPer = befPer.replace("%", "");
				if (frmOpenActSavingCareCnfNAck.hbxBefNameCnfrm2.isVisible == true){
				befName = befName + "~" + frmOpenActSavingCareCnfNAck.lblBefNameValCnfrm2.text;
				befRel = befRel + "~" + frmOpenActSavingCareCnfNAck.lblBefRsCnfrmVal2.text;
				befPerVthSym = frmOpenActSavingCareCnfNAck.lblBefPerCnfrmVal2.text;
				befPerVthSym =befPerVthSym.replace("%", "");
				befPer = befPer + "~" + befPerVthSym;
				}
				if (frmOpenActSavingCareCnfNAck.hbxBefNameCnfrm3.isVisible == true){
				befName = befName + "~" + frmOpenActSavingCareCnfNAck.lblBefNameValCnfrm3.text;
				befRel = befRel + "~" + frmOpenActSavingCareCnfNAck.lblBefRsCnfrmVal3.text;
				befPerVthSym = frmOpenActSavingCareCnfNAck.lblBefPerCnfrmVal3.text;
				befPerVthSym =befPerVthSym.replace("%", "");
				befPer = befPer + "~" + befPerVthSym;
				}
				if (frmOpenActSavingCareCnfNAck.hbxBefNameCnfrm4.isVisible == true){
				befName = befName + "~" + frmOpenActSavingCareCnfNAck.lblBefNameValCnfrm4.text;
				befRel = befRel + "~" + frmOpenActSavingCareCnfNAck.lblBefRsCnfrmVal4.text;
				befPerVthSym = frmOpenActSavingCareCnfNAck.lblBefPerCnfrmVal4.text;
				befPerVthSym =befPerVthSym.replace("%", "");
				befPer = befPer + "~" + befPerVthSym;
				}
				if (frmOpenActSavingCareCnfNAck.hbxBefNameCnfrm5.isVisible == true){
				befName = befName + "~" + frmOpenActSavingCareCnfNAck.lblBefNameValCnfrm5.text;
				befRel = befRel + "~" + frmOpenActSavingCareCnfNAck.lblBefRsCnfrmVal5.text;
				befPerVthSym = frmOpenActSavingCareCnfNAck.lblBefPerCnfrmVal5.text;
				befPerVthSym =befPerVthSym.replace("%", "");
				befPer = befPer + "~" + befPerVthSym;
				}
				inputparam["benName"] = befName;
				inputparam["benRelation"] = befRel;
				
				inputparam["benPer"] = befPer;
			}else if(gblSelProduct == "ForTerm"){
				inputparam["nickName"] = frmOpenAccTermDeposit.txtDSNickName.text;
				inputparam["personalizedActId"] = gblFinActivityLogOpenAct["toActId"];
				inputparam["flowIdOpenAct"] = "";
			}else if(gblSelProduct == "ForUse"){
				inputparam["nickName"] = frmOpnActSelAct.txtOpenActNicNam.text;
				inputparam["personalizedActId"] = gblFinActivityLogOpenAct["toActId"];
				inputparam["flowIdOpenAct"] = "";
			}
			invokeServiceSecureAsync("addDreamSavingCareDataOpenAct", inputparam, addDreamSavingCareDataOpenActCallBack);
		//}
	//}
	//}

}


function addDreamSavingCareDataOpenActCallBack(status,callBackResponse){

if(status == 400){
		if (callBackResponse["opstatus"] == 0){
		
				popupTractPwd.dismiss();
				dismissLoadingScreen();
				
				if (gblSelProduct == "TMBDreamSavings"){
				//frmOpenActDSAck.lblDSAckActNoVal.text = callBackResponse["acctIdentValue"];
				frmOpenActDSAck.lblOADSAmtAck.text = kony.i18n.getLocalizedString("keyMBMonthlySavingAmnt"); //removed : i18 text has + ":";
				frmOpenActDSAck.lblOADMnthAck.text = kony.i18n.getLocalizedString("keyMBTransferEvryMnth"); //removed : i18 text has + ":";
				frmOpenActDSAck.show();
				}else if(gblSelProduct == "TMBSavingcare"){
				//frmOpenActSavingCareCnfNAck.lblOASCActNumVal.text = callBackResponse["acctIdentValue"];
				if (grtFormattedAmount(gblFinActivityLogOpenAct["transferAmount"]) >0){
				setFinancialActivityLogOpenAct(gblFinActivityLogOpenAct["fromActId"], gblFinActivityLogOpenAct["fromActName"], gblFinActivityLogOpenAct["fromActNickName"], gblFinActivityLogOpenAct["fromActType"], gblFinActivityLogOpenAct["toActId"], gblFinActivityLogOpenAct["toActName"], gblFinActivityLogOpenAct["toActNickName"],gblFinActivityLogOpenAct["toActType"], gblFinActivityLogOpenAct["transferAmount"], gblFinActivityLogOpenAct["tdTerm"], gblFinActivityLogOpenAct["tdInterestRate"], gblFinActivityLogOpenAct["tdMaturityDate"],"02");
				}
				onClickPreShowSCAck();
				}else if(gblSelProduct == "ForTerm"){
					if (grtFormattedAmount(gblFinActivityLogOpenAct["transferAmount"]) >0){
					setFinancialActivityLogOpenAct(gblFinActivityLogOpenAct["fromActId"], gblFinActivityLogOpenAct["fromActName"], gblFinActivityLogOpenAct["fromActNickName"], gblFinActivityLogOpenAct["fromActType"], gblFinActivityLogOpenAct["toActId"], gblFinActivityLogOpenAct["toActName"], gblFinActivityLogOpenAct["toActNickName"],gblFinActivityLogOpenAct["toActType"], gblFinActivityLogOpenAct["transferAmount"], gblFinActivityLogOpenAct["tdTerm"], gblFinActivityLogOpenAct["tdInterestRate"], gblFinActivityLogOpenAct["tdMaturityDate"],"02");
					}
					frmOpenActTDAck.show();
				}else if(gblSelProduct == "ForUse"){
				
					if (grtFormattedAmount(gblFinActivityLogOpenAct["transferAmount"]) >0){
					setFinancialActivityLogOpenAct(gblFinActivityLogOpenAct["fromActId"], gblFinActivityLogOpenAct["fromActName"], gblFinActivityLogOpenAct["fromActNickName"], gblFinActivityLogOpenAct["fromActType"], gblFinActivityLogOpenAct["toActId"], gblFinActivityLogOpenAct["toActName"], gblFinActivityLogOpenAct["toActNickName"],gblFinActivityLogOpenAct["toActType"], gblFinActivityLogOpenAct["transferAmount"], gblFinActivityLogOpenAct["tdTerm"], gblFinActivityLogOpenAct["tdInterestRate"], gblFinActivityLogOpenAct["tdMaturityDate"],"02");
					}
					
					onclickConfirmTransNSPwd();
				}
				
				if(gblSelOpenActProdCode == "200" || gblSelOpenActProdCode == "220"  || gblSelOpenActProdCode == "222" ){
				activityLogServiceCall("039", "", "01", "", gblSelOpenActProdCode + "+" + frmOpenAccountNSConfirmation.lblProdNSConfName.text, frmOpenAccountNSConfirmation.lblAccuntNoVal.text,frmOpenAccountNSConfirmation.lblNickNameVal.text, gblFinActivityLogOpenAct["transferAmount"], "", ""); //Savings  
				}else if(gblSelOpenActProdCode == "221"){
				activityLogServiceCall("042", "", "01", "", gblSelOpenActProdCode + "+" + frmOpenAccountNSConfirmation.lblProdNSConfName.text, frmOpenAccountNSConfirmation.lblAccuntNoVal.text,frmOpenAccountNSConfirmation.lblNickNameVal.text, gblFinActivityLogOpenAct["transferAmount"], "", "");
				}else if(gblSelOpenActProdCode == "206"){
				var tarAmount = frmOpenAcDreamSaving.txtODTargetAmt.text;
				tarAmount = tarAmount.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(/,/g, "");				
			
					activityLogServiceCall("040", "", "01", "", frmOpenActDSConfirm.lblOADMnthVal.text, frmOpenActDSAck.lblDSAckActNoVal.text,frmOpenActDSConfirm.lblOAMnthlySavNum.text, gblFinActivityLogOpenAct["transferAmount"], (frmOpenAcDreamSaving["segSliderOpenDream"]["selectedItems"][0].imgDreamName + "+" + tarAmount), ""); //Dream Savings
				}else if(gblSelOpenActProdCode == "203"){
				
				var befPer = frmOpenActSavingCareCnfNAck.lblBefPerCnfrmVal1.text;
				var befPerVthSym = "";
				befPer = befPer.replace("%", "");
				var befName = frmOpenActSavingCareCnfNAck.lblBefNameValCnfrm1.text + " " + frmOpenActSavingCareCnfNAck.lblBefRsCnfrmVal1.text + " " + befPer;
				if (frmOpenActSavingCareCnfNAck.hbxBefNameCnfrm2.isVisible == true){
				befPerVthSym = frmOpenActSavingCareCnfNAck.lblBefPerCnfrmVal2.text;
				befPerVthSym =befPerVthSym.replace("%", "");
				befName = befName + "," + frmOpenActSavingCareCnfNAck.lblBefNameValCnfrm2.text + " " + frmOpenActSavingCareCnfNAck.lblBefRsCnfrmVal2.text + " " + befPerVthSym;
				}
				if (frmOpenActSavingCareCnfNAck.hbxBefNameCnfrm3.isVisible == true){
				befPerVthSym = frmOpenActSavingCareCnfNAck.lblBefPerCnfrmVal3.text;
				befPerVthSym =befPerVthSym.replace("%", "");
				befName = befName + "," + frmOpenActSavingCareCnfNAck.lblBefNameValCnfrm3.text + " " + frmOpenActSavingCareCnfNAck.lblBefRsCnfrmVal3.text + " " + befPerVthSym;
				}
				if (frmOpenActSavingCareCnfNAck.hbxBefNameCnfrm4.isVisible == true){
				befPerVthSym = frmOpenActSavingCareCnfNAck.lblBefPerCnfrmVal4.text;
				befPerVthSym =befPerVthSym.replace("%", "");
				befName = befName + "," + frmOpenActSavingCareCnfNAck.lblBefNameValCnfrm4.text + " " + frmOpenActSavingCareCnfNAck.lblBefRsCnfrmVal4.text + " " + befPerVthSym;
				}
				if (frmOpenActSavingCareCnfNAck.hbxBefNameCnfrm5.isVisible == true){
				befPerVthSym = frmOpenActSavingCareCnfNAck.lblBefPerCnfrmVal5.text;
				befPerVthSym =befPerVthSym.replace("%", "");
				befName = befName + "," + frmOpenActSavingCareCnfNAck.lblBefNameValCnfrm5.text + " " + frmOpenActSavingCareCnfNAck.lblBefPerCnfrmVal5.text + " " + befPerVthSym;
				}
				activityLogServiceCall("081", "", "01", "", gblSelOpenActProdCode + "+" + frmOpenActSavingCareCnfNAck.lblOASCTitle.text, frmOpenActSavingCareCnfNAck.lblOASCActNumVal.text,frmOpenActSavingCareCnfNAck.lblOASCNicNamVal.text, gblFinActivityLogOpenAct["transferAmount"], befName, ""); //Savings Care
				}else if(gblSelOpenActProdCode == "300" || gblSelOpenActProdCode == "301"  || gblSelOpenActProdCode == "302" || gblSelOpenActProdCode == "601" || gblSelOpenActProdCode == "602" || gblSelOpenActProdCode == "659" || gblSelOpenActProdCode == "664" || gblSelOpenActProdCode == "666"){
				activityLogServiceCall("041", "", "01", "", gblSelOpenActProdCode + "+" +frmOpenActTDConfirm.lblTDCnfmTitle.text, frmOpenActTDAck.lblOATDAckActNoVal.text,frmOpenActTDConfirm.lblOATDNickNameVal.text, gblFinActivityLogOpenAct["transferAmount"], "", ""); 
				}
			OpenAcctEmailNotification();
			invokeCustAccInqToUpdateOpenAct();	
			
		}else {
		dismissLoadingScreen();	
		showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
		return false;
		}
}
}



function setFinancialActivityLogOpenAct(fromActIdVal, fromActName, fromActNickName, fromActType, toActIdVal, toActName, toActNickName,toAccType, finTxnAmount, tdTerm, tdIntrestRate, tdMaturityDate,channelId) {
	GBLFINANACIALACTIVITYLOG.finTxnRefId = gblTransferRefNo;
	GBLFINANACIALACTIVITYLOG.finTxnDate = "";
	GBLFINANACIALACTIVITYLOG.activityTypeId = "063";
	var txnCd = "";
	if (fromActType == "DDA") {
        txnCd = "881";
    } else {
        txnCd = "882";
    }
    if (toAccType == "DDA") {
        txnCd = txnCd + "1";
    }else if(toAccType == "SDA") {
        txnCd = txnCd + "2";
    }else{
        txnCd = txnCd + "3";
    }
	GBLFINANACIALACTIVITYLOG.txnCd = txnCd;
	//GBLFINANACIALACTIVITYLOG.tellerId = gblFinActivityLogOpenAct["tellerId"];
	GBLFINANACIALACTIVITYLOG.txnDescription = "done";
	GBLFINANACIALACTIVITYLOG.finLinkageId = "";
	//02 is for mobile banking
	GBLFINANACIALACTIVITYLOG.channelId = channelId;//"02";
	
	var fromAcctId = fromActIdVal.replace(/-/g, "");//kony.string.replace(fromActIdVal, "-", "");
	GBLFINANACIALACTIVITYLOG.fromAcctId = fromAcctId;
	GBLFINANACIALACTIVITYLOG.fromAcctName = fromActName + "";
	frmNickName = fromActNickName + "";
	frmNickName = frmNickName.substr(0, 20)
	GBLFINANACIALACTIVITYLOG.fromAcctNickname = frmNickName;
	
	var toAcctId = toActIdVal.replace(/-/g, "");// kony.string.replace(toActIdVal, "-", "");
	GBLFINANACIALACTIVITYLOG.toAcctId = toAcctId;
	GBLFINANACIALACTIVITYLOG.toAcctName = toActName + "";
	toNickName = toActNickName + "";
	toNickName = toNickName.substr(0, 20);
	GBLFINANACIALACTIVITYLOG.toAcctNickname = toNickName;	
	GBLFINANACIALACTIVITYLOG.toBankAcctCd = "11";
	finTxnAmount = finTxnAmount.replace(/,/g, "");
	if (kony.string.containsChars(finTxnAmount, ["."]))
		finTxnAmount = finTxnAmount;
	else
		finTxnAmount = finTxnAmount + ".00";
	GBLFINANACIALACTIVITYLOG.finTxnAmount = finTxnAmount;
	GBLFINANACIALACTIVITYLOG.finTxnFee = "";
	GBLFINANACIALACTIVITYLOG.finTxnBalance = gblBalAfterXfer;
	GBLFINANACIALACTIVITYLOG.finTxnMemo = "";
	GBLFINANACIALACTIVITYLOG.billerCommCode = "";
	GBLFINANACIALACTIVITYLOG.billerRef1 = "";
	GBLFINANACIALACTIVITYLOG.billerRef2 = "";
	GBLFINANACIALACTIVITYLOG.billerCustomerName = "";
	GBLFINANACIALACTIVITYLOG.billerBalance = "";	
	GBLFINANACIALACTIVITYLOG.noteToRecipient = "";
	GBLFINANACIALACTIVITYLOG.recipientMobile = "";
	GBLFINANACIALACTIVITYLOG.recipientEmail = "";
	// 01 for success 02 for fail
	GBLFINANACIALACTIVITYLOG.finTxnStatus = "01";
	GBLFINANACIALACTIVITYLOG.smartFlag = "N";
	GBLFINANACIALACTIVITYLOG.clearingStatus = "";
	GBLFINANACIALACTIVITYLOG.errorCd = "0000";
	GBLFINANACIALACTIVITYLOG.finSchduleRefId = "";
	
	GBLFINANACIALACTIVITYLOG.activityRefId = ""; 	
	GBLFINANACIALACTIVITYLOG.eventId = gblTransferRefNo.substring(2, 18);
	
	GBLFINANACIALACTIVITYLOG.txnType = "009";
	GBLFINANACIALACTIVITYLOG.openTdTerm = tdTerm;
	GBLFINANACIALACTIVITYLOG.openTdInterestRate = tdIntrestRate;
	GBLFINANACIALACTIVITYLOG.openTdMaturityDate = tdMaturityDate;
	GBLFINANACIALACTIVITYLOG.affiliatedAcctNickname = toNickName;
	GBLFINANACIALACTIVITYLOG.affiliatedAcctId = toAcctId;
	GBLFINANACIALACTIVITYLOG.finFlexValues1 = "OWN";
	GBLFINANACIALACTIVITYLOG.finFlexValues2 = fromAcctId;
	GBLFINANACIALACTIVITYLOG.finFlexValues3 = toAcctId;
	GBLFINANACIALACTIVITYLOG.finFlexValues4 = "TMB";
	GBLFINANACIALACTIVITYLOG.finFlexValues5 = finTxnAmount;
	GBLFINANACIALACTIVITYLOG.openProdCode = gblSelOpenActProdCode
	financialActivityLogServiceCall(GBLFINANACIALACTIVITYLOG);
}


function callGenerateTransferRefNoserviceOpenActMB(){
kony.print("inside callGenerateTransferRefNoserviceOpenActMB");
	var inputParam = {};
	inputParam["transRefType"]="NT";
		kony.print("**here 1112**");
	invokeServiceSecureAsync("generateTransferRefNo", inputParam, callGenerateTransferRefNoserviceOpenActMBCallBack);
}


function callGenerateTransferRefNoserviceOpenActMBCallBack(status,result){
  kony.print("inside callGenerateTransferRefNoserviceOpenActMBCallBack" + JSON.stringify(result));
 kony.print("**here 2**");
 if(status == 400) //success responce
 {
    kony.print("**here 3**");
  if(result["opstatus"] == 0)
  {
      
   var refNum = result["transRefNum"];  
   gblTransferRefNo = refNum + "00";
    kony.print("**here 4**"+gblTransferRefNo);

   			if(gblSelProduct == "ForUse"){
   				frmOpenAccountNSConfirmation.lbltransactionVal.text = refNum + "00";
              		//frmOpenAccountNSConfirmation.hboxaddfblist.setVisibility(true);
					frmOpenAccountNSConfirmation.show();
				}else if(gblSelProduct == "TMBDreamSavings"){
					frmOpenActDSConfirm.show();				
				}else if(gblSelProduct == "TMBSavingcare"){
					kony.print("222222222222222");
					frmMBSavingsCareConfirmation.lblRefNoVal.text = refNum + "00";
					//onClickPreShowSCConfirm();				
				}else if(gblSelProduct == "ForTerm"){
					frmOpenActTDConfirm.lblTdTransRefVal.text = refNum + "00";
					frmOpenActTDConfirm.show();				
				}
      
  //return refNum;
  }
  else
    {
     dismissLoadingScreen();
     alert(" "+result["errMsg"]);  
    }
 } 
}


function hideUnhideOA(lblBal,lblBalValue,lblHide) {
	if (gblAckFlage == "true") {
	
		lblBal.setVisibility(false);
		lblBalValue.setVisibility(false)
		lblHide.text = kony.i18n.getLocalizedString("keyUnhide")
		gblAckFlage = "false"
	} else {
	
		lblBal.setVisibility(true);
		lblBalValue.setVisibility(true)
		lblHide.text = kony.i18n.getLocalizedString("Hide")
		gblAckFlage = "true"
	}
}


function amountValidationMBOpenAct(amount) {
	if (amount != null && amount != "") {
		if (kony.string.containsOnlyGivenChars(amount, ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9",  "."])) {
			
			if (amount.indexOf(".") > 0) {
				var amountFormat = amount.split(".")[1];
				
				if (amountFormat.length > 2) {
				return false;
				}
			}
			
			
		//	amount = amount.replace(",", "");
			amount = amount.replace(".", "");
			//ADDED os.tonumber(amount)== nil TO CHECK IF THE USER ONLY ENTER "." or ","
			if (kony.os.toNumber(amount) < 0 || kony.os.toNumber(amount) == null) {
				return false;
			} else
				return true;
		} else
			return false;
	} else
		return false;
}
//for Normal saving
function saveAsPdfImageOpenActNS(filetype) {
    var inputParam = {}
    inputParam["templatename"] = "OpenAccountNS"; // name of xsl file without extension
    inputParam["filetype"] = filetype;
    //var deviceInfo = kony.os.deviceInfo();
    var pdfImagedata = {
        "localeId": kony.i18n.getCurrentLocale(),
        "toAccountNo": formatAccountNumberOpenAct(gblFinActivityLogOpenAct["toActId"]),
        "toAccountName": gblFinActivityLogOpenAct["toActName"],
        "productName": frmOpenAccountNSConfirmation.lblProdNSConfName.text,
        "branch": gblFinActivityLogOpenAct["toBranchName"],
        "fromAccountNo": "xxx-x-" + gblFinActivityLogOpenAct["fromActIdFormatted"].substring(6, 11) + "-x",
        "fromAccountName": gblFinActivityLogOpenAct["fromActName"],
        "Amount": commaFormattedOpenAct(gblFinActivityLogOpenAct["transferAmount"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
        "openingDate": gblFinActivityLogOpenAct["openingDate"],
        "TransactionRefNo": gblTransferRefNo
    };
    inputParam["datajson"] = JSON.stringify(pdfImagedata, "", "");
    inputParam["outputtemplatename"] = "OpenAccountDetails_" + kony.os.date("DDMMYYYY");

    if (gblFinActivityLogOpenAct["transferAmount"] == 0) {
        if (deviceInfo["name"] == "iPhone" || deviceInfo["name"] == "android" ) {
            invokeServiceSecureAsync("generatePdfImage", inputParam, mbRenderFileCallbackfunction);
        } else {
            invokeServiceSecureAsync("generatePdfImage", inputParam, ibRenderFileCallbackfunction);
        }
    } else {
        saveOpenAccountPDFIB(filetype, "063", gblTransferRefNo, "NS");
    }
}
//for dream saving
function saveAsPdfImageOpenActDS(filetype) 
{
    var inputParam = {}
    inputParam["templatename"] = "OpenAccountDS"; // name of xsl file without extension
    inputParam["filetype"] = filetype;
    //var deviceInfo = kony.os.deviceInfo();
    var pdfImagedata = 
    {
      "localeId":kony.i18n.getCurrentLocale(), 
      "toAccountNo":formatAccountNumberOpenAct(gblFinActivityLogOpenAct["toActId"]),
      "toAccountName":gblFinActivityLogOpenAct["toActName"],
      "productName":frmOpenActDSAck.lblDSAckTitle.text,
      "branch":gblFinActivityLogOpenAct["toBranchName"],
      "fromAccountNo":"xxx-x-" + gblFinActivityLogOpenAct["fromActIdFormatted"].substring(6, 11) + "-x",
      "fromAccountName":gblFinActivityLogOpenAct["fromActName"],
      "dreamdesc":frmOpenActDSAck.lblMyDreamDesValAck.text,
      "targetAmount":frmOpenActDSAck.lblOADreamDetailTarAmtValAck.text,
      "term":frmOpenActDSAck.lblOADSAmtValAck.text,
      "transferrecdate":frmOpenActDSAck.lblOADMnthValAck.text,
      "openingDate":frmOpenActDSAck.lblDSOpenDateValAck.text
    };
     inputParam["datajson"] = JSON.stringify(pdfImagedata, "", "");
     inputParam["outputtemplatename"] = "OpenAccountDetails_"+kony.os.date("DDMMYYYY");
	
	saveOpenAccountPDFIB(filetype, "063", "", "DS");
	
	/*if(gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "android" || flowSpa == true)
	{
	  invokeServiceSecureAsync("generatePdfImage", inputParam, mbRenderFileCallbackfunction);
	}
	else
	{  
	 invokeServiceSecureAsync("generatePdfImage", inputParam, ibRenderFileCallbackfunction);
	}
	*/
}

//for Term deposit
function saveAsPdfImageOpenActTD(filetype) 
{
    var inputParam = {}
    inputParam["templatename"] = "OpenAccountTD"; // name of xsl file without extension
    inputParam["filetype"] = filetype;
    //var deviceInfo = kony.os.deviceInfo();
    var pdfImagedata = {
      "localeId":kony.i18n.getCurrentLocale(), 
      "toAccountNo":formatAccountNumberOpenAct(gblFinActivityLogOpenAct["toActId"]),
      "toAccountName":gblFinActivityLogOpenAct["toActName"],
      "productName":frmOpenActTDAck.lblOATDAckTitle.text,
      "branch":gblFinActivityLogOpenAct["toBranchName"],
      "fromAccountNo":"xxx-x-" + gblFinActivityLogOpenAct["fromActIdFormatted"].substring(6, 11) + "-x",
      "fromAccountName":gblFinActivityLogOpenAct["fromActName"],
      "affiliatedAccountNo":"",
      "affiliatedAccountName":"",
      "Amount":frmOpenActTDAck.lblOATDAmtValAck.text,
      "openingDate":frmOpenActTDAck.lblTDOpenDateValAck.text,
      "term":frmOpenActTDAck.lblOATDTermValAck.text,
      "maturityDate":frmOpenActTDAck.lblTDMatDateValAck.text,
      "TransactionRefNo":frmOpenActTDAck.lblOATDTransRefAckVal.text
    };
    
    
    	if (gblisDisToActTD == "Y")
		{
 		pdfImagedata.affiliatedAccountNo = "xxx-x-" + gblFinActivityLogOpenAct["affiliatedAcctId"].substring(6, 11) + "-x";
        pdfImagedata.affiliatedAccountName = gblFinActivityLogOpenAct["affiliatedAcctNickname"];
        }
    
    
     inputParam["datajson"] = JSON.stringify(pdfImagedata, "", "");
     inputParam["outputtemplatename"] = "OpenAccountDetails_"+kony.os.date("DDMMYYYY");
     
     saveOpenAccountPDFIB(filetype, "063", frmOpenActTDAck.lblOATDTransRefAckVal.text, "TD");
	
	/*if(deviceInfo["name"] == "iPhone" || deviceInfo["name"] == "android" || flowSpa == true)
	{
	  invokeServiceSecureAsync("generatePdfImage", inputParam, mbRenderFileCallbackfunction);
	}
	else
	{  
	 invokeServiceSecureAsync("generatePdfImage", inputParam, ibRenderFileCallbackfunction);
	}
	*/
}

//for dream saving
function saveAsPdfImageOpenActSavingCare(filetype) 
{
    var inputParam = {}
    inputParam["templatename"] = "OpenAccountSC"; // name of xsl file without extension
    inputParam["filetype"] = filetype;
    //var deviceInfo = kony.os.deviceInfo();
    var lengthOfBenList = 0;
    if(gblOpenActBenList!=null)
    {
    	lengthOfBenList = gblOpenActBenList.length;
    }
   // alert(lengthOfBenList);
    //alert(gblOpenActBenList[0]["careBeneficiaryName"]);
    //alert(gblOpenActBenList[0]["careBeneficiaryRelation"]);
     //alert(gblOpenActBenList[1]["careBeneficiaryName"]);
    //var testing = JSON.stringify(gblOpenActBenList);
    //alert(testing);
   // var reln=[];
    var accountNameinPDF = "";
	if (gblPartyInqOARes["issuedIdentType"] == "CI" ){
	
		accountNameinPDF = gblPartyInqOARes["FullName"];
	}
	else
	{
		accountNameinPDF = gblPartyInqOARes["PrefName"];
	}
      var pdfImagedata = 
    {
      "localeId":kony.i18n.getCurrentLocale(), 
      "toAccountNo":formatAccountNumberOpenAct(gblFinActivityLogOpenAct["toActId"]),
      "toAccountName":accountNameinPDF,
      "productName":frmOpenActSavingCareCnfNAck.lblOASCTitle.text,
      "branch":gblFinActivityLogOpenAct["toBranchName"],
      "fromAccountNo":"xxx-x-" + gblFinActivityLogOpenAct["fromActIdFormatted"].substring(6, 11) + "-x",
      "fromAccountName":accountNameinPDF,
      "Amount":frmOpenActSavingCareCnfNAck.lblOASCOpenActVal.text,
      "openingDate":frmOpenActSavingCareCnfNAck.lblOASCOpenDateVal.text,
      "TransactionRefNo":frmOpenActSavingCareCnfNAck.lblOASCTranRefNoVal.text,
      "beneficiariesList" : gblOpenActBenList,
      "localeId" : kony.i18n.getCurrentLocale()
    };
    
     inputParam["datajson"] = JSON.stringify(pdfImagedata, "", "");
     inputParam["outputtemplatename"] = "OpenAccountDetails_"+kony.os.date("DDMMYYYY");
	if(gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "android" )
	{
	  invokeServiceSecureAsync("generatePdfImage", inputParam, mbRenderFileCallbackfunction);
	}
	else
	{  
	 invokeServiceSecureAsync("generatePdfImage", inputParam, ibRenderFileCallbackfunction);
	}
}


/*function commaFormattedOpenAct(amount) {
   var ifCommaExists = amount.indexOf(",");
   var newvalue;
   
	if (ifCommaExists == -1){
	amount = amount.replace(",", "");
	}
	
	if (amount.indexOf(".") > 0) {
		var amountFormat = amount.split(".");
 
 			if (amountFormat[1].length == 1) {
			amount = amount + "0";
			} else if (amountFormat[1].length == 0) {
			amount = amountFormat[0] + ".00";
			}
			
	} else {
		amount = amount + ".00"
	}

	decimalPart = "." + amount.split('.')[1];
	amount = amount.split('.')[0];
	

	while(amount.length>3){
	decimalPart = "," + amount.substr(amount.length-3,3) + decimalPart;
	amount  = amount.substr(0,amount.length-3);
	}	
	
	newvalue = amount+ decimalPart;
	
	return newvalue;
}*/

//Moved this method to Util.js
/*
function commaFormattedOpenAct(amount) {
	if (amount == '0.00' || amount == 0) {
		return '0.00';
	}else{
	    //checking for if ZERO exists at first left place
	    for (i = 0; i < amount.length; i++) {
	        if (amount.charAt(0) == '0') {
	            amount = amount.substring(1, amount.length);
	        } else {
	            amount = amount;
	        }
	    }
	    var ifCommaExists = amount.indexOf(",");
	    var newvalue;
	    amount = amount.trim();
	    if (ifCommaExists != -1) {
	        amount = amount.replace(/,/g, "");
	    }
	
	    if (amount.indexOf(".") > 0) {
	        var amountFormat = amount.split(".");
	
	        if (amountFormat[1].length == 1) {
	            amount = amount + "0";
	        } else if (amountFormat[1].length == 0) {
	            amount = amountFormat[0] + ".00";
	        }
	
	    } else {
	        if (amount == "") {
	            amount = 0;
	        }
	        amount = amount + ".00"
	    }
	
	    var decimalPart = "." + amount.split('.')[1];
	    amount = amount.split('.')[0];
	
	
	    while (amount.length > 3) {
	        decimalPart = "," + amount.substr(amount.length - 3, 3) + decimalPart;
	        amount = amount.substr(0, amount.length - 3);
	    }
	    newvalue = amount + decimalPart;
    }
    return newvalue;
}
*/

function checkIsEligibleToOpenAct(acctId,linkedAccType){

 	var inputparam = {};
        if (linkedAccType != null && linkedAccType != "" && linkedAccType == "SDA") {
            inputparam["spName"] = "com.fnf.xes.ST"; //14digits
		}
        if (linkedAccType != null && linkedAccType != "" && linkedAccType == "DDA") {
            inputparam["spName"] = "com.fnf.xes.IM"; //14digits
        }

        inputparam["acctId"] = acctId;
        //   
        inputparam["acctType"] = linkedAccType;
        
        inputparam["clientDt"] = "";
        inputparam["name"] = "MB-INQ";
        invokeServiceSecureAsync("RecurringFundTransinq", inputparam, callBackcheckIsEligibleToOpenAct);
    
}

function callBackcheckIsEligibleToOpenAct(status,resulttable){
 if (status == 400) {
        
        if (resulttable["opstatus"] == 0) {
		
            if (resulttable["toAcctId"] != null && resulttable["toAcctId"] != "") {
            dismissLoadingScreen();
		 	showAlert(kony.i18n.getLocalizedString("keyOpenActMapped"), kony.i18n.getLocalizedString("info"));
		 	return false            
            }
            else{
            invokePartyInquiryOpenAct();
            //invokeDepositAOXferOpenAct(gblFinActivityLogOpenAct["fromActIdFormated"],gblFinActivityLogOpenAct["fromActName"],gblFinActivityLogOpenAct["fromFiident"],gblFinActivityLogOpenAct["toFiident"],gblFinActivityLogOpenAct["toActId"],gblFinActivityLogOpenAct["toActType"],gblFinActivityLogOpenAct["fromActType"],gblFinActivityLogOpenAct["transferAmount"],gblFinActivityLogOpenAct["dreamAmt"],gblFinActivityLogOpenAct["dreamMonth"],gblFinActivityLogOpenAct["fromProdId"]);//Invoke depositAOXFER service
            }

    }else{
    		dismissLoadingScreen();
		 	showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
		 	return false;    
    }
  }
}


function getOPenActMBIndex() {}

	getOPenActMBIndex.prototype.paginationSwitch = function (sectionIndex, rowIndex,segName) {
	var segdata = segName.data;
	rowIndex = parseFloat(rowIndex);
	segName.selectedIndex = [0, rowIndex];

}


function onClickEmailTnCMBOpenAccount(tnckeyword,chnlName,chnlId) {
	//call notification add service for email with the tnc keyword
	//alert("tnckeyword::"+tnckeyword);
	showLoadingScreen()
	var inputparam = {};
	inputparam["channelName"] = chnlName;
	inputparam["channelID"] = chnlId;
	inputparam["notificationType"] = "Email"; // always email
	inputparam["phoneNumber"] = gblPHONENUMBER;
	inputparam["mail"] = gblEmailId;
	inputparam["customerName"] = gblCustomerName;
	inputparam["localeCd"] = kony.i18n.getCurrentLocale();
    inputparam["moduleKey"] = "OpenAccount";
    inputparam["fileNameOpen"] = tnckeyword;
    //if (kony.i18n.getCurrentLocale() == "en_US") {
	inputparam["productName"] = gblFinActivityLogOpenAct["prodNameEN"];
	//} else {
	inputparam["productNameTH"] = gblFinActivityLogOpenAct["prodNameTH"];
	//}
	inputparam["prodCode"] = gblSelOpenActProdCode;
	invokeServiceSecureAsync("TCEMailService", inputparam, callBackNotificationOpenAccntAddServiceMB);
	
}


function callBackNotificationOpenAccntAddServiceMB(status, result) {


	if (status == 400) {
		if (result["opstatus"] == 0) {
			var StatusCode = result["StatusCode"];
			var Severity = result["Severity"];
			var StatusDesc = result["StatusDesc"];
			if (StatusCode == 0) {
				//populateEditBillPaymentCompleteScreen();
				showAlert(kony.i18n.getLocalizedString("keytermOpenAcnt"), kony.i18n.getLocalizedString("info"));
				
				dismissIBLoadingScreen()
			} else {
				dismissIBLoadingScreen()
				
				return false;
			}
		} else {
		dismissIBLoadingScreen()
			
		}
	}
}

function autoFormatAmount(amount){
	//ENH_087:DEF322 - Fixed Amount formatting issue
	amount = parseFloat(removeCommos(amount)).toFixed(2);
	var formattedAmount = commaFormatted(amount) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");;
	return formattedAmount;
}

function calMonthlySavingCalculatorOpen() {

    var tarAmount = frmOpenAcDreamSaving.txtODTargetAmt.text;
    
    var tarAmount1 = tarAmount.split(".");
    var tarAmount2 = tarAmount1.length - 1;
    var tarAmount = tarAmount.replace(/,/g, "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");

    
    var noOfMnths = frmOpenAcDreamSaving.txtODMyDream.text;

    
    noOfMnths = noOfMnths.replace(kony.i18n.getLocalizedString("keymonths"), "");
    
    /*if (gblDreamMnths == noOfMnths){
 
 return false;
 }*/


    var indexMnt = noOfMnths.indexOf(".");
    //if(indexMnt > 0 || kony.string.isNumeric(noOfMnths) ==false )
    //		{	
    //			showAlert(kony.i18n.getLocalizedString("keyenterMnths"), kony.i18n.getLocalizedString("info"));
    //			 return false;
    //			 
    //		}



    var entAmount;
    tarAmount = kony.os.toNumber(tarAmount);
    noOfMnths = kony.os.toNumber(noOfMnths);
    entAmount = tarAmount / noOfMnths;
    

    entAmount = entAmount + "";
    
    entAmount = entAmount.trim();
    var indexdot = entAmount.indexOf(".");
    var decimal = "";
    var remAmt = "";
    
    if (indexdot > 0) {
        decimal = entAmount.substr(indexdot, 3);
        remAmt = entAmount.substr(0, indexdot);
        
        entAmount = remAmt + decimal;
        
    }

    
    //noOfMnths = Math.round(noOfMnths);
    

    //if(frmDreamCalculator.txtODMnthSavAmt.text == ""|| frmDreamCalculator.txtODMnthSavAmt.text == null)	{
   frmOpenAcDreamSaving.txtODMnthSavAmt.text = commaFormattedOpenAct(entAmount) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
    StringnoOfMnths = frmOpenAcDreamSaving.txtODMyDream.text;

    StringnoOfMnths = StringnoOfMnths.trim();
    if (StringnoOfMnths == null || StringnoOfMnths == "" || StringnoOfMnths == "0") {
       frmOpenAcDreamSaving.txtODMnthSavAmt.text = "";

    } else {
        frmOpenAcDreamSaving.txtODMyDream.text = frmOpenAcDreamSaving.txtODMyDream.text + kony.i18n.getLocalizedString("keymonths");

    }

    //}


}

/**
 * @function
 *
 */
function hideOpenAccountNSShareOptions(){
    frmOpenAccountNSConfirmation.btnRight.setVisibility(true);
	frmOpenAccountNSConfirmation.hbxArrow.setVisibility(true);
	frmOpenAccountNSConfirmation.hboxaddfblist.setVisibility(false);
}

/**
 * @function
 *
 */
function displayOpenAccountNSShareOptions(){
    frmOpenAccountNSConfirmation.btnRight.setVisibility(false);
	frmOpenAccountNSConfirmation.hbxArrow.setVisibility(false);
	frmOpenAccountNSConfirmation.hboxaddfblist.setVisibility(true);
}
