
function setDropDownDatainSeg(content,selectedFlag,selectedStatus, flowName) {
	var locale = kony.i18n.getCurrentLocale();
    var currForm =kony.application.getCurrentForm();
  	var segData = [];
  
  	 if(gblloanCollectionData.length> 15 && content !== gblPersonalInfo.EMPLOYMENT_STATUS){
          currForm.flxSearch.setVisibility(true);
     }else {
         currForm.flxSearch.setVisibility(false);
     }
    content.sort(dynamicSortLoan("entryName2"));
	for (var i = 0; i < content.length; i++) {
		kony.print("getcontent @@ in loop");
		var entryName = "";
		if (locale == "th_TH") {
			entryName = content[i].entryName2;
		} else {
			entryName = content[i].entryName2;
		}
		var entryCode = content[i].entryCode;
      	var entryID = content[i].entryID;
		var temprowData = {};
		if (selectedFlag && selectedStatus == entryName) {
			kony.print("getcontent @@ matched");
			temprowData = {
				"lblName": entryName,
				"lblSegRowLine": ".",
				"imgTick": "tick_icon.png",
				"entryID": entryID,
              	"lblEntryCode": entryCode,
				"flxSegLoanGeneric": {
					"skin": flexGreyBG
				}
			};
			segData.push(temprowData);
			kony.print("getcontent @@ pushed selected");
		} else {
			temprowData = {
				"lblName": entryName,
				"lblSegRowLine": ".",
				"imgTick": "",
				"entryID": entryID,
              	"lblEntryCode": entryCode,
				"flxSegLoanGeneric": {
					"skin": flexWhiteBG
				}
			};
			segData.push(temprowData);
		}
	}
  frmLoanPersonalInfo.segSourceofIncome.removeAll();
  if(null !== flowName && "SALUTATION"=== flowName){
    
    frmLoanPersonalInfo.lblSIHeading.text = kony.i18n.getLocalizedString("ThaiSalutation");
	frmLoanPersonalInfo.segSourceofIncome.onRowClick = segThaiSalutionOnRowClick;
    frmLoanPersonalInfo.btnCloseSIPopUp.onClick = btnCloseSIPopUponClick;
    frmLoanPersonalInfo.segSourceofIncome.setData(segData);
    
    frmLoanPersonalInfo.flxSourceOfIncomePopUp.setVisibility(true);
//     frmLoanPersonalInfo.flxMain.setVisibility(false);
    dismissLoadingScreen();
  }if(null !== flowName && "SCI_COUNTRY"=== flowName){
    frmLoanPersonalInfo.lblSIHeading.text = kony.i18n.getLocalizedString("SourceofIncome");
	frmLoanPersonalInfo.segSourceofIncome.onRowClick = segSourceofIncomeonRowClick;
    frmLoanPersonalInfo.btnCloseSIPopUp.onClick = btnCloseSIPopUponClickscroldown;
    frmLoanPersonalInfo.segSourceofIncome.setData(segData);
    
    frmLoanPersonalInfo.flxSourceOfIncomePopUp.setVisibility(true);
//     frmLoanPersonalInfo.flxMain.setVisibility(false);
    dismissLoadingScreen();
  }else{
    frmLoanWorkInfo.segDyna.setData(segData);
    if(currForm.id == "frmLoanWorkInfo"){
	frmLoanWorkInfo.flxDynamicPopUp.setVisibility(true);
    frmLoanWorkInfo.segDyna.onRowClick = onRowClicksegDyna;  
    }
	frmLoanWorkInfo.flxDynamicPopUp.forceLayout();
	frmLoanWorkInfo.flxBankPopupSeg.forceLayout();
    frmLoanWorkInfo.btnPopUpClosePopUp.onClick = closeDynamicPopup;
  }
	
}

function getDropdwonByCategary( categoryCode){
	showLoadingScreen();	
  var inputParam = {};
	inputParam["categoryCode"]=categoryCode;
  	if(categoryCode=="EMPLOYMENT_STATUS"){
      	invokeServiceSecureAsync("getPersonalInfoFields", inputParam, callbackEMPStatus);
    }else if(categoryCode=="OCCUPATION"){
      	inputParam["empEntryCode"]= frmLoanWorkInfo.lblEmpCode.text;
      	invokeServiceSecureAsync("getPersonalInfoFields", inputParam, callbackOfgetOccupation);
    }else if(categoryCode=="PROFFESIONAL"){
      	
		inputParam["empEntryCode"] = frmLoanWorkInfo.lblOccupationCode.text ;
      invokeServiceSecureAsync("getPersonalInfoFields", inputParam, callbackOfgetProf);
    }else if(categoryCode=="SALUTATION"){
      inputParam["entryCode"] = frmLoanPersonalInfo.lblTitleTypeEntryCode.text;
      invokeServiceSecureAsync("getPersonalInfoFields", inputParam, callbackOfSaluationDropDowns);
    }else if(categoryCode=="SCI_COUNTRY"){
      	invokeServiceSecureAsync("getPersonalInfoFields", inputParam, callbackOfSciCountryDropDowns);
    }else if (categoryCode == "GET_ADDRESS_FROM_DB"){
         invokeServiceSecureAsync("getPersonalInfoFields", inputParam, callbackFullAddress);
    }
	
}
function callbackOfgetOccupation(status, result){
  if (status == 400) {
    try{
      if (result.opstatus == "0") {
        dismissLoadingScreen();
        kony.print("result value = "+ JSON.stringify(result));
        var employeeStatus = result["OCCUPATION"];
         gblloanCollectionData =result["OCCUPATION"]; 
		 gblLoanSearchFlow = "OCCUPATION";
        var selectedFlag=false;
        var selectedStatus="";
        if(frmLoanWorkInfo.lblOccupationDesc.isVisible === true) {
          selectedFlag=true;
          kony.print("getEmployeeStatus @@ already");
          selectedStatus = frmLoanWorkInfo.lblOccupationDesc.text;
        }
        setDropDownDatainSeg(employeeStatus,selectedFlag,selectedStatus,gblLoanSearchFlow);
        
      }else {
        dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        return false;
      }
    }catch(e){
    alert("Exception in callbackOfgetPersonalInfoFields, e : "+e);
  }
  }
}
function callbackOfgetProf(status, result){
  if (status == 400) {
    try{
      if (result.opstatus == "0") {
        dismissLoadingScreen();
        kony.print("result value = "+ JSON.stringify(result));
        var employeeStatus = result["PROFFESIONAL"];
          gblloanCollectionData =result["PROFFESIONAL"]; 
           gblLoanSearchFlow = "PROFFESIONAL";
        var selectedFlag=false;
        var selectedStatus="";
        if(frmLoanWorkInfo.lblProfessionalDesc.isVisible === true) {
          selectedFlag=true;
          kony.print("getEmployeeStatus @@ already");
          selectedStatus = frmLoanWorkInfo.lblProfessionalDesc.text;
        }
        setDropDownDatainSeg(employeeStatus,selectedFlag,selectedStatus,gblLoanSearchFlow);
      }else {
        dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        return false;
      }
    }catch(e){
    alert("Exception in callbackOfgetPersonalInfoFields, e : "+e);
  }
  }
}

function callbackFullAddress(status, result){
  if (status == 400) {
    try{
      if (result.opstatus == "0") {
        dismissLoadingScreen();
        kony.print("result value = "+ JSON.stringify(result));
        var segAddressData = [];
        var fullAddress = result["addressDetails"];
        fullAddress.sort(dynamicSortLoan("subDistProvince"));
        var temprowData = {};
		for (var i = 0; i < fullAddress.length; i++) {
          var subDistrictFulladdress = "";
          gblLoanFullAddressSearch = fullAddress;
		  subDistrictFulladdress = gblLoanFullAddressSearch[i].subDistProvince;
          temprowData = {
				"lblTotalAddress": subDistrictFulladdress,
				};  
          segAddressData.push(temprowData);
        }
      }else {
        dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        return false;
      }
      frmLoanWorkInfo.segSubDistrictSearch.setData(segAddressData);
      frmLoanWorkInfo.segSubDistrictSearch.onRowClick = selectFullAddress;
      workingAddr();
    }catch(e){
      alert("Exception in callbackOfgetPersonalInfoFields, e : "+e);
    }
  }
}

function selectFullAddress(){
  var lblFullAddresstext = frmLoanWorkInfo.segSubDistrictSearch.selectedRowItems[0].lblTotalAddress;
  var addressSplit = replaceAll(lblFullAddresstext," >> " , "~");
  frmLoanWorkInfo.flxSearchAddress.setVisibility(false);
  frmLoanWorkInfo.tbxSubDistrict.text = addressSplit.split("~")[0] !== undefined ? addressSplit.split("~")[0] : "";
  frmLoanWorkInfo.lblDistrict.setVisibility(true);
  frmLoanWorkInfo.tbxDistrict.setVisibility(true);
  frmLoanWorkInfo.lblMainDistrict.setVisibility(false);
  frmLoanWorkInfo.tbxDistrict.text = addressSplit.split("~")[1] !== undefined ? addressSplit.split("~")[1] : "";
  frmLoanWorkInfo.lblProvince.setVisibility(true);
  frmLoanWorkInfo.tbxProvince.setVisibility(true);
  frmLoanWorkInfo.lblMainProvince.setVisibility(false);
  frmLoanWorkInfo.tbxProvince.text = addressSplit.split("~")[2] !== undefined ? addressSplit.split("~")[2] : "";
  frmLoanWorkInfo.lblZipCode.setVisibility(true);
  frmLoanWorkInfo.tbxZipCode.setVisibility(true);
  frmLoanWorkInfo.lblMainZipCode.setVisibility(false);
  frmLoanWorkInfo.tbxZipCode.text = addressSplit.split("~")[3] !== undefined ? addressSplit.split("~")[3] : "";
  handleAddrSaveBtn();
}

function getEmployeeStatus(){
  kony.print("getEmployeeStatus @@");
  frmLoanWorkInfo.segDyna.removeAll();
  frmLoanWorkInfo.tbxSearch.text = "";
  frmLoanWorkInfo.lblDynamic.text=kony.i18n.getLocalizedString("Employment_Status_textbox");
  gblLoanSearchFlow = "EMPLOYMENT_STATUS";
  var selectedFlag = false;
  var selectedStatus = "";
  if (frmLoanWorkInfo.lblEmploymentStatusDesc.isVisible === true) {
    selectedFlag = true;
    kony.print("getEmployeeStatus @@ already");
    selectedStatus = frmLoanWorkInfo.lblEmploymentStatusDesc.text;
  }
  setDropDownDatainSeg(gblPersonalInfo.EMPLOYMENT_STATUS, selectedFlag, selectedStatus, gblLoanSearchFlow);
}

function getOccupation(){
  frmLoanWorkInfo.segDyna.removeAll();
  frmLoanWorkInfo.tbxSearch.text = "";
  frmLoanWorkInfo.lblDynamic.text= kony.i18n.getLocalizedString("Occupation_textbox");
  getDropdwonByCategary("OCCUPATION"); 
}

function getProfessions(){
  frmLoanWorkInfo.segDyna.removeAll();
  frmLoanWorkInfo.tbxSearch.text = "";
  frmLoanWorkInfo.flxDynamicPopUp.lblDynamic.text=kony.i18n.getLocalizedString("Professional_textbox");
  getDropdwonByCategary("PROFFESIONAL"); 
}

function getfulladdress(){
    frmLoanWorkInfo.segSubDistrictSearch.removeAll();
    getDropdwonByCategary("GET_ADDRESS_FROM_DB");
}

function assignValue() {
    kony.print("row click assignValue");
    try {
        // var lbltext = frmLoanWorkInfo.segDyna.selectedRowItems[0].lblName;
        var selectedIndex = frmLoanWorkInfo.segDyna.selectedIndex[1];
        var selectedData = frmLoanWorkInfo.segDyna.data[selectedIndex];

        var lbltext = selectedData["lblName"];
        kony.print("row value : " + JSON.stringify(selectedData));
        kony.print("selected lbltext " + selectedData["lblName"]);
        kony.print("selected imgTick " + selectedData["imgTick"]);
        var title = frmLoanWorkInfo.lblDynamic.text;
        kony.print("title @@: " + title);


        var newselected=selectedData["lblEntryCode"] !== undefined ? selectedData["lblEntryCode"] : "";
        if(newselected == "01"){
           frmLoanWorkInfo.flxContractEmployed.setVisibility(true);
        }else if(newselected == "02"){
           frmLoanWorkInfo.flxContractEmployed.setVisibility(false);
        }
        if (title === kony.i18n.getLocalizedString("Employment_Status_textbox")) {
            gblPersonalInfo["EmpStatus"] = selectedData;
            if (selectedData["imgTick"] == "") {
                selectedData.flxSegLoanGeneric.skin = "flexTransBG";
                selectedData["imgTick"] = "tick_icon.png";
                kony.print("selected value " + selectedData["imgTick"]);
                kony.print("selected value " + selectedData["flxSegLoanGeneric"]);
				frmLoanWorkInfo.flxProfessional.onClick = null;
				frmLoanWorkInfo.lblProfessional.skin = "lblGrey171";
              	frmLoanWorkInfo.lblProfessional.setVisibility(true);
				frmLoanWorkInfo.lblProfessionalDesc.setVisibility(false);
				frmLoanWorkInfo.lblProfessionalHeader.setVisibility(false);


                if(newselected !=frmLoanWorkInfo.lblEmpCode.text){
                frmLoanWorkInfo.lblEmpCode.text =newselected;
                frmLoanWorkInfo.lblOccupationDesc.text="";  
                frmLoanWorkInfo.lblOccupation.setVisibility(true);
                frmLoanWorkInfo.lblOccupationDesc.setVisibility(false);
                frmLoanWorkInfo.lblOccupationHeader.setVisibility(false);
             	
                }
                
            }
            if (gblPersonalInfo["EmpStatus"]["lblEntryCode"] != null || gblPersonalInfo["EmpStatus"]["lblEntryCode"] != "") {
                frmLoanWorkInfo.flxOccupation.onClick = getOccupation;
                frmLoanWorkInfo.lblOccupation.skin = "lblblue24px007abc";
               

            } else {
                frmLoanWorkInfo.flxOccupation.onClick = null;
                frmLoanWorkInfo.flxProfessional.onClick = null;
                frmLoanWorkInfo.lblOccupation.skin = "lblGrey171";
                frmLoanWorkInfo.lblProfessional.skin = "lblGrey171";

            }
            frmLoanWorkInfo.lblEmploymentStatus.setVisibility(false);
            frmLoanWorkInfo.lblEmploymentStatusDesc.text = lbltext;
            frmLoanWorkInfo.lblEmploymentStatusDesc.setVisibility(true);
            frmLoanWorkInfo.lblEmploymentStatusHeader.setVisibility(true);
            kony.print("row click Employment_Status_textbox");

        } else if (title === kony.i18n.getLocalizedString("Occupation_textbox")) {
            if (selectedData["imgTick"] == "") {
                selectedData.flxSegLoanGeneric.skin = "flexTransBG";
                selectedData["imgTick"] = "tick_icon.png";
                kony.print("selected value " + selectedData["imgTick"]);
                kony.print("selected value " + selectedData["flxSegLoanGeneric"]);
				 frmLoanWorkInfo.lblProfessional.skin = "lblblue24px007abc";
              	 frmLoanWorkInfo.flxProfessional.onClick = getProfessions;
                 if(newselected !=frmLoanWorkInfo.lblOccupationCode.text){
                frmLoanWorkInfo.lblOccupationCode.text =newselected;
                frmLoanWorkInfo.lblProfessionalDesc.text="";   
                frmLoanWorkInfo.lblProfessional.setVisibility(true);
                frmLoanWorkInfo.lblProfessionalDesc.setVisibility(false);
                frmLoanWorkInfo.lblProfessionalHeader.setVisibility(false);
                }
                frmLoanWorkInfo.lblOccupationCode.text = selectedData["lblEntryCode"] !== undefined ? selectedData["lblEntryCode"] : "";
            }
            frmLoanWorkInfo.lblOccupation.setVisibility(false);
            frmLoanWorkInfo.lblOccupationDesc.text = lbltext;
            frmLoanWorkInfo.lblOccupationDesc.setVisibility(true);
            frmLoanWorkInfo.lblOccupationHeader.setVisibility(true);
            kony.print("row click Occupation");

        } else if (title === kony.i18n.getLocalizedString("Professional_textbox")) {
            gblPersonalInfo["ProfStatus"] = selectedData;
            if (selectedData["imgTick"] == "") {
                selectedData.flxSegLoanGeneric.skin = "flexTransBG";
                selectedData["imgTick"] = "tick_icon.png";
                kony.print("selected value " + selectedData["imgTick"]);
                kony.print("selected value " + selectedData["flxSegLoanGeneric"]);

                frmLoanWorkInfo.lblProfessionalCode.text = selectedData["lblEntryCode"] !== undefined ? selectedData["lblEntryCode"] : "";

            }
            frmLoanWorkInfo.lblProfessional.setVisibility(false);
            frmLoanWorkInfo.lblProfessionalDesc.text = lbltext;
            frmLoanWorkInfo.lblProfessionalDesc.setVisibility(true);
            frmLoanWorkInfo.lblProfessionalHeader.setVisibility(true);
            kony.print("row click Professional_textbox");
        }
        frmLoanWorkInfo.flxDynamicPopUp.setVisibility(false);
        handleSaveBtnWorkInfo();
    } catch (e) {
        // alert(e);
    }
}

function checkIsSaveEnable() {
    var count = 0;
    if (isNullorEmpty(frmLoanWorkInfo.calStartDate.year)) {
        count++;
    } else if (isNullorEmpty(frmLoanWorkInfo.calEndDate.text)) {
        count++;
    }
    if (count === 0) {
        return true;
    }
}

function checkEnable(){     
  var count = 0;
  
  if(isNullorEmpty(frmLoanWorkInfo.lblEmploymentStatusDesc.text)){
    count++;
  }else if(isNullorEmpty(frmLoanWorkInfo.lblOccupationDesc.text)){
    count++;
  }else if(isNullorEmpty(frmLoanWorkInfo.lblProfessionalDesc.text)){
    count++;
  }else if(isNullorEmpty(frmLoanWorkInfo.tbxDept.text)){
    count++;
//   }else if(isNullorEmpty(frmLoanWorkInfo.lblBuildingValTumbolEntry.text)){
//     count++;
//   }else if(isNullorEmpty(frmLoanWorkInfo.lblMobileNo.text)){
//     count++;
//   }else if(isNullorEmpty(frmLoanWorkInfo.lblBuildingValTumbolEntry.text)){
//     count++;
  }else if(isNullorEmpty(frmLoanWorkInfo.lblContractEmpVal.text)){
    
    if(isNullorEmpty(frmLoanWorkInfo.lblContractEmployedDesc.text)){
       count++;
       }    
  }
  
  if(count===0){
    return true;
  }else{
    return false;
  }
}


function preLoadWInfo(){
  
  try{//Contract Employed
         
  frmLoanWorkInfo.flxContractEmployed.height = 60+"dp";
  frmLoanWorkInfo.lblContractEmp.setVisibility(false);
  frmLoanWorkInfo.lblContractEmpVal.setVisibility(false);
  frmLoanWorkInfo.lblContractStartDate.setVisibility(false);
  frmLoanWorkInfo.lblContractStartDateVal.setVisibility(false);
  frmLoanWorkInfo.lblContractEndDt.setVisibility(false);
  frmLoanWorkInfo.lblContractEndDtVal.setVisibility(false);
  frmLoanWorkInfo.lblContrtPeriod.setVisibility(false);
  frmLoanWorkInfo.lblContrtPeriodVal.setVisibility(false);   
  frmLoanWorkInfo.btnNext.skin = "btnGreyBGNoRound";
    frmLoanWorkInfo.btnNext.focusSkin = "btnGreyBGNoRound";
//     frmLoanWorkInfo.flxFooter.skin = "flexBlueBG";
   frmLoanWorkInfo.lblContractEmployed.setVisibility(true); 
    

var flag = checkEnable();

  if(flag){
    frmLoanWorkInfo.flxcircle.skin="flxGreenCircleLoan";
    frmLoanWorkInfo.lblcircle.skin = skinpiGreenLineLone;
    frmLoanWorkInfo.btnNext.skin = "btnBlue28pxLoan2";
    frmLoanWorkInfo.btnNext.focusSkin = "btnBlueBGNoRound150pxLoan";
//     frmLoanWorkInfo.flxFooter.skin = "flexBlueBG";
    frmLoanWorkInfo.btnNext.onClick = onClickNextWInfo; 
  }else{
    frmLoanWorkInfo.btnNext.skin = "btnGreyBGNoRound";
    frmLoanWorkInfo.btnNext.focusSkin = "btnGreyBGNoRound"; 
    frmLoanWorkInfo.flxFooter.skin = "flexWhiteBG";
    frmLoanWorkInfo.flxcircle.skin="slFbox";
    frmLoanWorkInfo.lblcircle.skin = skinpiorangeLineLone; 
  }
  
  frmLoanWorkInfo.lblWorkingAddress.setVisibility(true);
  frmLoanWorkInfo.flxWorkingAddress.height = 60+"dp";
  
  }catch(e){
   kony.print("preload@@@"+e);
  }  
  
}

function addDynamicContract(){

  frmLoanWorkInfo.lblContractEmployed.setVisibility(false);

  if(frmLoanWorkInfo.flxContractPopUp.flxbuttons.btnEmploy.skin === btnBlueTabBGNoRound){
    frmLoanWorkInfo.lblContractEmployedHeader.setVisibility(true);
    frmLoanWorkInfo.lblContractEmployedDesc.setVisibility(true);
    frmLoanWorkInfo.lblContractEmployedDesc.text = kony.i18n.getLocalizedString("Employ_label");
    frmLoanWorkInfo.flxContractEmployed.height = 60+"dp";

    frmLoanWorkInfo.lblContractEmp.setVisibility(false);
    frmLoanWorkInfo.lblContractEmpVal.setVisibility(false);
    frmLoanWorkInfo.lblContractStartDate.setVisibility(false);
    frmLoanWorkInfo.lblContractStartDateVal.setVisibility(false);
    frmLoanWorkInfo.lblContractEndDt.setVisibility(false);
    frmLoanWorkInfo.lblContractEndDtVal.setVisibility(false);
    frmLoanWorkInfo.lblContrtPeriod.setVisibility(false);
    frmLoanWorkInfo.lblContrtPeriodVal.setVisibility(false);

  }else{
    frmLoanWorkInfo.flxContractEmployed.height = 240+"dp";

    frmLoanWorkInfo.lblContractEmployedHeader.setVisibility(false);
    frmLoanWorkInfo.lblContractEmployedDesc.setVisibility(false);

    frmLoanWorkInfo.lblContractEmp.setVisibility(true);
    frmLoanWorkInfo.lblContractEmpVal.setVisibility(true);
    frmLoanWorkInfo.lblContractStartDate.setVisibility(true);
    frmLoanWorkInfo.lblContractStartDateVal.setVisibility(true);
    frmLoanWorkInfo.lblContractEndDt.setVisibility(true);
    frmLoanWorkInfo.lblContractEndDtVal.setVisibility(true);
    frmLoanWorkInfo.lblContrtPeriod.setVisibility(true);
    frmLoanWorkInfo.lblContrtPeriodVal.setVisibility(true);

    frmLoanWorkInfo.lblContractEmpVal.text = kony.i18n.getLocalizedString("Contract_Employed_label");
    frmLoanWorkInfo.lblContractStartDateVal.text = frmLoanWorkInfo.calStartDate.day+"/"+frmLoanWorkInfo.calStartDate.month+"/"+frmLoanWorkInfo.calStartDate.year;
    frmLoanWorkInfo.lblContractEndDtVal.text = frmLoanWorkInfo.calEndDate.day+"/"+frmLoanWorkInfo.calEndDate.month+"/"+frmLoanWorkInfo.calEndDate.year;

  }

}

function addDynamicWAddress(){
  frmLoanWorkInfo.flxMain.isVisible = true;
  frmLoanWorkInfo.lblWorkingAddress.setVisibility(false);
  frmLoanWorkInfo.flxWorkingAddress.height = 165+"dp";
  
}


function yearly(){
//   frmLoanWorkInfo.lblContrtPeriodVal.text =  kony.i18n.getLocalizedString("Yearly_tab");
  frmLoanWorkInfo.btnYearlystmt.skin = "btnTabBlueRightRound";
  frmLoanWorkInfo.btnMonthlystmt.skin = "btnWhiteTabBGNoRound";
  frmLoanWorkInfo.btnYearlystmt.focusSkin = "btnTabBlueRightRound";
  frmLoanWorkInfo.btnMonthlystmt.focusSkin = "btnWhiteTabBGNoRound";
}

function monthly(){
//   frmLoanWorkInfo.lblContrtPeriodVal.text = kony.i18n.getLocalizedString("Monthly_tab");
  frmLoanWorkInfo.btnMonthlystmt.skin = "btnBlueTabBGNoRound";
  frmLoanWorkInfo.btnYearlystmt.skin = "btnTabWhiteRightRound";
  frmLoanWorkInfo.btnMonthlystmt.focusSkin = "btnBlueTabBGNoRound";
  frmLoanWorkInfo.btnYearlystmt.focusSkin = "btnTabWhiteRightRound";
}

function enableWSave(){     
  var count = 0;
  
  if(isNullorEmpty(frmLoanWorkInfo.tbxAddrVal.text)){
    count++;
  }else if(isNullorEmpty(frmLoanWorkInfo.tbcBuildingNo.text)){
    count++;
  }else if(isNullorEmpty(frmLoanWorkInfo.tbxSoi.text)){
    count++;
  }else if(isNullorEmpty(frmLoanWorkInfo.tbxRoad.text)){
    count++;
  }else if(isNullorEmpty(frmLoanWorkInfo.tbxMoo.text)){
    count++;
  }
  
  if(count===0){
    return true;
  }
}

function addContractData(){
  if(!isNullorEmpty(frmLoanWorkInfo.lblContractStartDateVal.text)){
    var fromDate = frmLoanWorkInfo.lblContractStartDateVal.text;
    var day =  fromDate.split('/')[0];
    var month =  fromDate.split('/')[1];
    var year =  fromDate.split('/')[2];
  }

  if(!isNullorEmpty(frmLoanWorkInfo.lblContractEndDtVal.text)){

    var endDate = frmLoanWorkInfo.lblContractEndDtVal.text;
    var day1 =  endDate.split('/')[0];
    var month1 =  endDate.split('/')[1];
    var year1 =  endDate.split('/')[2];    
  }  

  if(frmLoanWorkInfo.lblContractEndDtVal.text === "Yearly"){
    yearly();
  }else if(frmLoanWorkInfo.lblContractEndDtVal.text === "Monthly"){
    monthly();
  }

}

function onTextChangeWAddressPop(){
  var flag = enableWSave();  
  if(flag){
    frmLoanWorkInfo.btnSave.setEnabled(true);
    frmLoanWorkInfo.btnSave.skin = "btnBlueBGNoRound150pxLoan";
    frmLoanWorkInfo.btnSave.focusSkin = "btnBlueBGNoRound150pxLoan";
  }else{
    frmLoanWorkInfo.btnSave.setEnabled(false);
    frmLoanWorkInfo.btnSave.skin = "btnGreyBGNoRoundLoan";
    frmLoanWorkInfo.btnSave.focusSkin = "btnGreyBGNoRoundLoan";
  }
}

function addPreviousDataWA(){
  
  if(!isNullorEmpty(frmLoanWorkInfo.tbxAddrVal.text)){
    
    frmLoanWorkInfo.btnSave.setEnabled(true);
    frmLoanWorkInfo.lblAddressTitle.setVisibility(false);
    frmLoanWorkInfo.tbxAddrVal.setVisibility(true);
    frmLoanWorkInfo.lblAddressNo.setVisibility(true);
  }
  if(!isNullorEmpty(frmLoanWorkInfo.tbcBuildingNo.text)){
    
    frmLoanWorkInfo.lblbuildingMoo.setVisibility(false);
    frmLoanWorkInfo.tbcBuildingNo.setVisibility(true);
    frmLoanWorkInfo.lblBuildingNo.setVisibility(true);
  }
  
  if(!isNullorEmpty(frmLoanWorkInfo.tbxSoi.text)){
    
    frmLoanWorkInfo.lblSoiNo.setVisibility(false);
    frmLoanWorkInfo.tbxSoi.setVisibility(true);
    frmLoanWorkInfo.lblSoi.setVisibility(true);
  }
  if(!isNullorEmpty(frmLoanWorkInfo.tbxRoad.text)){
    
    frmLoanWorkInfo.lblRoadNo.setVisibility(false);
    frmLoanWorkInfo.tbxRoad.setVisibility(true);
    frmLoanWorkInfo.lblRoad.setVisibility(true);
  }
  if(!isNullorEmpty(frmLoanWorkInfo.tbxMoo.text)){
    
    frmLoanWorkInfo.lblMooNo.setVisibility(false);
    frmLoanWorkInfo.tbxMoo.setVisibility(true);
    frmLoanWorkInfo.lblMoo.setVisibility(true);
  }
                  
}

function onDoneWsave(){
  frmLoanWorkInfo.flxMain.isVisible = false;
  var flag = enableWSave();

  if(flag){
    frmLoanWorkInfo.btnSave.setEnabled(true);
    frmLoanWorkInfo.btnSave.skin = "btnBlue28pxLoan2";
    frmLoanWorkInfo.btnNext.focusSkin = "btnBlueBGNoRound150pxLoan"; 
//     frmLoanWorkInfo.flxFooter.skin = "flexBlueBG";
  }else{
    frmLoanWorkInfo.btnSave.setEnabled(false);
    frmLoanWorkInfo.btnSave.skin = "btnGreyBGNoRoundLoan";
    frmLoanWorkInfo.flxFooter.skin = "flexWhiteBG";
  }
}

function saveDepartmentName(){
  if(frmLoanWorkInfo.flxDepartmentName.tbxDept.text === ""){
    frmLoanWorkInfo.lblDepartmentName.setVisibility(true);
//     frmLoanWorkInfo.tbxDept.setFocus(false);
    frmLoanWorkInfo.tbxDept.setVisibility(false);
    frmLoanWorkInfo.lblDepartmentNameHeader.setVisibility(false);
  }
}

function onClickNextWInfo(){
  gblPersonalInfo.saveWI = true;
  
  var emp_status = frmLoanWorkInfo.lblEmploymentStatusDesc.text;
  var occupation = frmLoanWorkInfo.lblOccupationDesc.text;
  var professional = frmLoanWorkInfo.lblProfessionalDesc.text;
  var dept_name = frmLoanWorkInfo.tbxDept.text;
  //var residential_Status = frmLoanWorkInfo.lblResidentialStatusDesc.text;
   var lenght_employement = frmLoanWorkInfo.tbxLengthOfEmployment.text;
  var lblNo2 = frmLoanWorkInfo.lblContractEmpVal.text;
  var lblNo4 = frmLoanWorkInfo.lblContractStartDateVal.text;
  var lblNo6 = frmLoanWorkInfo.lblContractEndDtVal.text;
  var lblNo8 = frmLoanWorkInfo.lblContrtPeriodVal.text;
  gblPersonalInfo = {"emp_status" : emp_status,"occupation" : occupation, "professional" : professional,"dept_name" : dept_name, 
                    "lenght_employement" : lenght_employement,"lblNo2" : lblNo2,"lblNo4" : lblNo4, "lblNo6" : lblNo6,"lblNo8" : lblNo8};
  gblJSONWorkingInfo();
}

function onOpenWInfo() {
    try {
        var locale = kony.i18n.getCurrentLocale();
        if (isNotBlank(gblPersonalInfo.OCCUPATION.refEntryCode)) {
            for (var i = 0; i < gblPersonalInfo.EMPLOYMENT_STATUS.length; i++) {
                if (gblPersonalInfo.EMPLOYMENT_STATUS[i].entryCode ==  gblPersonalInfo.OCCUPATION.refEntryCode) {
                    frmLoanWorkInfo.lblEmploymentStatusDesc.text = gblPersonalInfo.EMPLOYMENT_STATUS[i].entryName2;
                }
            }
            frmLoanWorkInfo.lblEmploymentStatus.setVisibility(false);
            frmLoanWorkInfo.lblEmploymentStatusDesc.setVisibility(true);
            frmLoanWorkInfo.lblEmploymentStatusHeader.setVisibility(true);
            if (gblPersonalInfo.OCCUPATION.refEntryCode == "01") {
                frmLoanWorkInfo.flxContractEmployed.setVisibility(true);
            } else if (gblPersonalInfo.OCCUPATION.refEntryCode == "02") {
                frmLoanWorkInfo.flxContractEmployed.setVisibility(false);
            }else{
                gblPersonalInfo.profCode = "";
                frmLoanWorkInfo.lblOccupationCode.text="";
                frmLoanWorkInfo.lblEmploymentStatus.setVisibility(true);
                frmLoanWorkInfo.lblEmploymentStatusDesc.setVisibility(false);
                frmLoanWorkInfo.lblEmploymentStatusHeader.setVisibility(false);
                frmLoanWorkInfo.flxOccupation.onClick = null;
                frmLoanWorkInfo.flxProfessional.onClick = null;
                frmLoanWorkInfo.lblOccupation.skin = "lblGrey171";
                frmLoanWorkInfo.lblProfessional.skin = "lblGrey171";
            }
            //frmLoanWorkInfo.lblEmpCode.text = gblPersonalInfo.empStatus;
            if (isNotBlank(frmLoanWorkInfo.lblOccupationCode.text)) {
                frmLoanWorkInfo.lblOccupation.setVisibility(false);
                frmLoanWorkInfo.lblOccupationDesc.setVisibility(true);
                frmLoanWorkInfo.lblOccupationHeader.setVisibility(true);
                frmLoanWorkInfo.flxOccupation.onClick = getOccupation;
                frmLoanWorkInfo.flxProfessional.onClick = getProfessions;
                frmLoanWorkInfo.lblOccupationDesc.text = gblPersonalInfo.OCCUPATION.entryName2;
            }else{
                frmLoanWorkInfo.lblOccupation.setVisibility(true);
                frmLoanWorkInfo.lblOccupationDesc.setVisibility(false);
                frmLoanWorkInfo.lblOccupationHeader.setVisibility(false);
                frmLoanWorkInfo.lblOccupationDesc.text = "";
            }
        } else {
            frmLoanWorkInfo.lblEmploymentStatus.setVisibility(true);
            frmLoanWorkInfo.lblEmploymentStatusDesc.setVisibility(false);
            frmLoanWorkInfo.lblEmploymentStatusHeader.setVisibility(false);
            frmLoanWorkInfo.lblOccupationDesc.text = "";
        }
        // 		if (gblPersonalInfo.saveWI) {
        frmLoanWorkInfo.lblContractEmployed.setVisibility(false);
        if (isNotBlank(gblPersonalInfo.totalOfficeAddr)) {
            frmLoanWorkInfo.flxWorkingAddress.setVisibility(false);
            frmLoanWorkInfo.flxWorkingAddressFilled.setVisibility(true);
            var resultaddress = gblPersonalInfo.totalOfficeAddr;
            var filteraddress = replaceAll(resultaddress, "~", " ");
            frmLoanWorkInfo.lbltempOfficeAddr.text = gblPersonalInfo.totalOfficeAddr;
            var filteredChangedAddress = retrivechangeWorkAddress();
            frmLoanWorkInfo.lblWorkingAddressInfor.text = filteredChangedAddress;
        } else {
            frmLoanWorkInfo.flxWorkingAddress.setVisibility(true);
            frmLoanWorkInfo.flxWorkingAddressFilled.setVisibility(false);
            frmLoanWorkInfo.lblWorkingAddressInfor.text = "";
            frmLoanWorkInfo.lbltempOfficeAddr.text = gblPersonalInfo.totalOfficeAddr;
        }
        if (isNotBlank(gblPersonalInfo.profCode)) {
            frmLoanWorkInfo.lblProfessional.setVisibility(false);
            frmLoanWorkInfo.lblProfessionalDesc.setVisibility(true);
            frmLoanWorkInfo.lblProfessionalHeader.setVisibility(true);
            frmLoanWorkInfo.lblProfessionalCode.text = gblPersonalInfo.profCode;
            frmLoanWorkInfo.lblProfessionalDesc.text = gblPersonalInfo.profCodeTH;
        } else {
            frmLoanWorkInfo.lblProfessional.setVisibility(true);
            frmLoanWorkInfo.lblProfessionalDesc.setVisibility(false);
            frmLoanWorkInfo.lblProfessionalHeader.setVisibility(false);
            frmLoanWorkInfo.lblProfessionalDesc.text = "";
        }
        if (isNotBlank(gblPersonalInfo.empYear) || isNotBlank(gblPersonalInfo.empMonth)) {
            frmLoanWorkInfo.lblDateOfCitID.setVisibility(true);
            frmLoanWorkInfo.lblLengthOfCurrentEmployee.setVisibility(true);
            frmLoanWorkInfo.lblEmpMonth.text = gblPersonalInfo.empMonth;
            frmLoanWorkInfo.lblEmpYear.text = gblPersonalInfo.empYear;
            frmLoanWorkInfo.lblLengthOfCurrentEmployee.text = gblPersonalInfo.empYear + " " + getLocalizedString("Years_textbox") + " " + gblPersonalInfo.empMonth + " " + getLocalizedString("Months_textbox");
        } else {
            frmLoanWorkInfo.lblLengthOfCurrentEmployee.text = "0" + " " + getLocalizedString("Years_textbox") + " " + "0" + " " + getLocalizedString("Months_textbox");
        }
        if (isNotBlank(gblPersonalInfo.empDept)) {
            frmLoanWorkInfo.lblDepartmentName.setVisibility(false);
            frmLoanWorkInfo.lblDepartmentNameHeader.setVisibility(true);
            frmLoanWorkInfo.tbxDept.setVisibility(true);
            frmLoanWorkInfo.tbxDept.text = gblPersonalInfo.empDept;
        } else {
            frmLoanWorkInfo.lblDepartmentName.setVisibility(true);
            frmLoanWorkInfo.lblDepartmentNameHeader.setVisibility(false);
            frmLoanWorkInfo.tbxDept.setVisibility(false);
            frmLoanWorkInfo.tbxDept.text = "";
        }
        if (isNotBlank(gblPersonalInfo.empTelephoneNo)) {
            frmLoanWorkInfo.lblMainOfficeNo.setVisibility(false);
            frmLoanWorkInfo.lblOfficeNo.setVisibility(true);
            frmLoanWorkInfo.tbxOfficeNo.setVisibility(true);
            var officeNo = formatMobileNumber(gblPersonalInfo.empTelephoneNo);
            frmLoanWorkInfo.tbxOfficeNo.text = officeNo;
            if(officeNo.length == 11){
              formatLoanLandlineNumberopen(officeNo);
            }  
        } else {
            frmLoanWorkInfo.lblMainOfficeNo.setVisibility(true);
            frmLoanWorkInfo.lblOfficeNo.setVisibility(false);
            frmLoanWorkInfo.tbxOfficeNo.setVisibility(false);
            frmLoanWorkInfo.tbxOfficeNo.text = "";
        }
        if (isNotBlank(gblPersonalInfo.empTelephoneExtNo)) {
            frmLoanWorkInfo.lblMainExtNo.setVisibility(false);
            frmLoanWorkInfo.lblExtNo.setVisibility(true);
            frmLoanWorkInfo.tbxExtNo.setVisibility(true);
            frmLoanWorkInfo.tbxExtNo.text = gblPersonalInfo.empTelephoneExtNo;
        } else {
            frmLoanWorkInfo.lblMainExtNo.setVisibility(true);
            frmLoanWorkInfo.lblExtNo.setVisibility(false);
            frmLoanWorkInfo.tbxExtNo.setVisibility(false);
            frmLoanWorkInfo.tbxExtNo.text = "";
        }
        if (isNotBlank(gblPersonalInfo.contractEmployedFlag)) {
            frmLoanWorkInfo.lblContractEmployed.setVisibility(false);
            frmLoanWorkInfo.lblContractEmployedHeader.setVisibility(true);
            frmLoanWorkInfo.lblContractEmployedDesc.setVisibility(true);
            frmLoanWorkInfo.lblContractFlag.text = gblPersonalInfo.contractEmployedFlag;
            if ("N" == gblPersonalInfo.contractEmployedFlag) {
                frmLoanWorkInfo.lblContractEmployedDesc.text = kony.i18n.getLocalizedString("Employ_label");
            } else {
                //frmLoanWorkInfo.lblContractEmployedDesc.text = kony.i18n.getLocalizedString("Contract_Employed_label");
                frmLoanWorkInfo.flxContractEmployed.height = 240 + "dp";
                frmLoanWorkInfo.lblContractEmployedHeader.setVisibility(false);
                frmLoanWorkInfo.lblContractEmployedDesc.setVisibility(false);
                frmLoanWorkInfo.lblContractEmp.setVisibility(true);
                frmLoanWorkInfo.lblContractEmpVal.setVisibility(true);
                frmLoanWorkInfo.lblContractStartDate.setVisibility(true);
                frmLoanWorkInfo.lblContractStartDateVal.setVisibility(true);
                frmLoanWorkInfo.lblContractEndDt.setVisibility(true);
                frmLoanWorkInfo.lblContractEndDtVal.setVisibility(true);
                frmLoanWorkInfo.lblContrtPeriod.setVisibility(true);
                frmLoanWorkInfo.lblContrtPeriodVal.setVisibility(true);
                frmLoanWorkInfo.lblContractEmpVal.text = getLocalizedString("Contract_Employed_label");
                frmLoanWorkInfo.lblContractStartDateVal.text = gblPersonalInfo.contractEmpStartDate;
                frmLoanWorkInfo.lblContractEndDtVal.text = gblPersonalInfo.contractEmpEndDate;
                frmLoanWorkInfo.lblContrtPeriodVal.text = getLocalizedString("Monthly_tab");
            }
        } else {
            frmLoanWorkInfo.lblContractEmployed.setVisibility(true);
            frmLoanWorkInfo.lblContractEmployedHeader.setVisibility(false);
            frmLoanWorkInfo.lblContractEmployedDesc.setVisibility(false);
            frmLoanWorkInfo.lblContractFlag.text = gblPersonalInfo.contractEmployedFlag;
        }
//         frmLoanWorkInfo.flxProfessional.onClick = getProfessions;
//         frmLoanWorkInfo.flxOccupation.onClick = getOccupation;
        // 		} else {
        // 			frmLoanWorkInfo.lblOccupation.setVisibility(true);
        // 			frmLoanWorkInfo.lblOccupationDesc.setVisibility(false);
        // 			frmLoanWorkInfo.lblOccupationHeader.setVisibility(false);
        //             frmLoanWorkInfo.lblProfessional.setVisibility(true);
        // 			frmLoanWorkInfo.lblProfessionalDesc.setVisibility(false);
        // 			frmLoanWorkInfo.lblProfessionalHeader.setVisibility(false);
        // 			frmLoanWorkInfo.flxProfessional.onClick = null;
        // 			frmLoanWorkInfo.flxOccupation.onClick = null;
        // 			frmLoanWorkInfo.lblOccupation.skin = "lblGrey171";
        // 			frmLoanWorkInfo.lblProfessional.skin = "lblGrey171";
        // 			frmLoanWorkInfo.lblEmploymentStatusDesc.text = "";
        // 			frmLoanWorkInfo.lblOccupationDesc.text = "";
        // 			frmLoanWorkInfo.lblProfessionalDesc.text = "";
        // 			frmLoanWorkInfo.tbxDept.text = "";
        // 			frmLoanWorkInfo.lblContractEmployedHeader.setVisibility(false);
        // 			frmLoanWorkInfo.lblContractEmployedDesc.setVisibility(false);
        // 			frmLoanWorkInfo.lblBuildingValTumbolEntry.text = "";
        // 			frmLoanWorkInfo.lblMobileNo.text = "";
        // 			frmLoanWorkInfo.btnSave.setEnabled(false);
        // 			frmLoanWorkInfo.btnSave.skin = "btnGreyBGNoRoundLoan";
        // 			frmLoanWorkInfo.flxContact.isVisible = true;
        // 		}
        handleSaveBtnWorkInfo();
    } catch (e) {
        kony.print("onOpenWInfo@@@@" + e);
    }
}

function FloorAddPrefix(i) {
    var j = i % 10,
        k = i % 100;
    if (j == 1 && k != 11) {
        return i + "st";
    }
    if (j == 2 && k != 12) {
        return i + "nd";
    }
    if (j == 3 && k != 13) {
        return i + "rd";
    }
    return i + "th";
}

function disableAndroidDeviceBack(){}


function gblJSONWorkingInfo() {
  try {
    var lblEmploymentStatusDesc = frmLoanWorkInfo.lblEmploymentStatusDesc.text;
    var lblOccupationDesc = frmLoanWorkInfo.lblOccupationDesc.text;
    var lblProfessionalDesc = frmLoanWorkInfo.lblProfessionalDesc.text;
    var tbxDept = frmLoanWorkInfo.tbxDept.text;
    var lblContractEmployedDesc = frmLoanWorkInfo.lblContractEmployedDesc.text;
    var contractEmployed = frmLoanWorkInfo.lblContractEmpVal.text;
    var contractStartDate = frmLoanWorkInfo.lblContractStartDateVal.text;
    var ContractEndDate = frmLoanWorkInfo.lblContractEndDtVal.text;
    var ContractPeriod = frmLoanWorkInfo.lblContrtPeriodVal.text;
    var lblWorkingAddressDesc = frmLoanWorkInfo.lblWorkingAddressDesc.text;
    gblPersonalInfo = {"lblEmploymentStatusDesc" : lblEmploymentStatusDesc, "lblOccupationDesc" : lblOccupationDesc, "lblProfessionalDesc" : lblProfessionalDesc,
                      "tbxDept" : tbxDept, "lblContractEmployedDesc" : lblContractEmployedDesc,
                     "contractEmployed" : contractEmployed, "contractStartDate" : contractStartDate, "ContractEndDate" : ContractEndDate,
                     "ContractPeriod" : ContractPeriod, "lblWorkingAddressDesc" : lblWorkingAddressDesc};
  } catch(e) {
    alert("Error in gblJSONWorkingInfo : "+e);
  }
}

function flexAddressEmptyCheckWI(arry) {
  if(arry !== null && arry.length > 0) {
    for(var i = 0; i < arry.length; i++) {
      var val = frmLoanWorkInfo[arry[i]].text;
      if(arry[i] === "tbxAddrVal") {
        if(val === "") {
          frmLoanWorkInfo.lblMainAddressNo.isVisible=true;
          frmLoanWorkInfo.lblAddressNo.isVisible=false;
          frmLoanWorkInfo.tbxAddrVal.isVisible=false;
          frmLoanWorkInfo.tbxAddrVal.setFocus(false);
        }

      } else if (arry[i] === "tbcBuildingNo") {
        if(val === "") {
          frmLoanWorkInfo.lblMainBuildingNo.isVisible=true;
          frmLoanWorkInfo.lblBuildingNo.isVisible=false;
          frmLoanWorkInfo.tbcBuildingNo.isVisible=false;
          frmLoanWorkInfo.tbcBuildingNo.setFocus(false);
        }

      } else if (arry[i] === "tbxFloorNo") {
        if(val === "") {
          frmLoanWorkInfo.lblMainFloorNo.isVisible=true;
          frmLoanWorkInfo.lblFloorNo.isVisible=false;
        }

      } else if (arry[i] === "tbxSoi") {
        if(val === "") {
          frmLoanWorkInfo.lblMainSoi.isVisible=true;
          frmLoanWorkInfo.lblSoi.isVisible=false;
          frmLoanWorkInfo.tbxSoi.isVisible=false;
          frmLoanWorkInfo.tbxSoi.setFocus(false);
        }

      } else if (arry[i] === "tbxRoad") {
        if(val === "") {
          frmLoanWorkInfo.LblMainRoadNo.isVisible=true;
          frmLoanWorkInfo.lblRoad.isVisible=false;
          frmLoanWorkInfo.tbxRoad.isVisible=false;
          frmLoanWorkInfo.tbxRoad.setFocus(false);
        }

      } else if (arry[i] === "tbxMoo") {
        if(val === "") {
          frmLoanWorkInfo.lblMainMoo.isVisible=true;
          frmLoanWorkInfo.lblMoo.isVisible=false;
          frmLoanWorkInfo.tbxMoo.isVisible=false;
          frmLoanWorkInfo.tbxMoo.setFocus(false);
        }

      }   else if (arry[i] === "tbxFloorNo") {
        if(val === "") {
        }

      } else if (arry[i] === "txtMooNo") {
        if(val === "") {
          frmLoanWorkInfo.txtMooNo.isVisible=false;
          frmLoanWorkInfo.txtMooNo.setFocus(false);
        }
      }
    }
  }
}

function formatLoanLandlineNumberopen(txt){
  if (txt === null) return false;
	var noChars = txt.length;
	var temp = "";
	var i, txtLen = noChars;
	var currLen = noChars;
		for (i = 0; i < noChars; ++i) {
			if (txt[i] != '-') {
				temp = temp + txt[i];
			} else {
				txtLen--;
			}			
		}
		var iphenText = "";
		for (i = 0; i < txtLen; i++) {
			iphenText += temp[i];
			if (i == 1 || i == 4) {
				iphenText += '-';
			}
		}
			frmLoanWorkInfo.tbxOfficeNo.text = iphenText;
	
  gblPrevLen = currLen;

}

function openWorkingInfoFlex(){
  try{
//     if(isNullorEmpty(frmLoanWorkInfo.lblEmploymentStatusDesc.text)){
//       frmLoanWorkInfo.lblEmploymentStatus.setVisibility(true);
//       frmLoanWorkInfo.lblEmploymentStatusDesc.setVisibility(false);
//       frmLoanWorkInfo.lblEmploymentStatusHeader.setVisibility(false);
//     }else{
//       frmLoanWorkInfo.lblEmploymentStatus.setVisibility(false);
//       frmLoanWorkInfo.lblEmploymentStatusDesc.setVisibility(true);
//       frmLoanWorkInfo.lblEmploymentStatusHeader.setVisibility(true);
//     }

//     if(isNullorEmpty(frmLoanWorkInfo.lblOccupationDesc.text)){
//       frmLoanWorkInfo.lblOccupation.setVisibility(true);
//       frmLoanWorkInfo.lblOccupationDesc.setVisibility(false);
//       frmLoanWorkInfo.lblOccupationHeader.setVisibility(false);
//     }else{
//       frmLoanWorkInfo.lblOccupation.setVisibility(false);
//       frmLoanWorkInfo.lblOccupationDesc.setVisibility(true);
//       frmLoanWorkInfo.lblOccupationHeader.setVisibility(true);
//     }

//     if(isNullorEmpty(frmLoanWorkInfo.lblProfessionalDesc.text)){
//       frmLoanWorkInfo.lblProfessional.setVisibility(true);
//       frmLoanWorkInfo.lblProfessionalDesc.setVisibility(false);
//       frmLoanWorkInfo.lblProfessionalHeader.setVisibility(false);
//     }else{
//       frmLoanWorkInfo.lblProfessional.setVisibility(false);
//       frmLoanWorkInfo.lblProfessionalDesc.setVisibility(true);
//       frmLoanWorkInfo.lblProfessionalHeader.setVisibility(true);
//     }  

//     if(isNullorEmpty(frmLoanWorkInfo.tbxDept.text)){
//       frmLoanWorkInfo.lblDepartmentName.setVisibility(true);
//       frmLoanWorkInfo.tbxDept.setVisibility(false);
//       frmLoanWorkInfo.lblDepartmentNameHeader.setVisibility(false);
//     }else{
//       frmLoanWorkInfo.lblDepartmentName.setVisibility(false);
//       frmLoanWorkInfo.tbxDept.setVisibility(true);
//       frmLoanWorkInfo.lblDepartmentNameHeader.setVisibility(true);
//     }  

//     if(isNullorEmpty(frmLoanWorkInfo.tbxOfficeNo.text)){
//       frmLoanWorkInfo.lblMainOfficeNo.setVisibility(true);
//       frmLoanWorkInfo.lblOfficeNo.setVisibility(false);
//       frmLoanWorkInfo.tbxOfficeNo.setVisibility(false);
//     }else{
//       frmLoanWorkInfo.lblMainOfficeNo.setVisibility(false);
//       frmLoanWorkInfo.lblOfficeNo.setVisibility(true);
//       frmLoanWorkInfo.tbxOfficeNo.setVisibility(true);
//     }  

//     if(isNullorEmpty(frmLoanWorkInfo.tbxExtNo.text)){
//       frmLoanWorkInfo.lblMainExtNo.setVisibility(true);
//       frmLoanWorkInfo.tbxExtNo.setVisibility(false);
//       frmLoanWorkInfo.lblExtNo.setVisibility(false);
//     }else{
//       frmLoanWorkInfo.lblMainExtNo.setVisibility(false);
//       frmLoanWorkInfo.tbxExtNo.setVisibility(true);
//       frmLoanWorkInfo.lblExtNo.setVisibility(true);
//     }  
    //workingInformationPreShow();
    frmLoanWorkInfo.show();
  }catch(e){
    kony.print("Ecxeption at1 "+e);
  }
}
