gblGoDirectlyToPrompPay =false;
gblPromptPayAllDetails = [];
gblMyQRonMobile = false;
gblMyQRonCitizenId = false;
gblQRByMobileDefault = false;
gblPromptPayResultValue = [];

function frmMyQRCodeInit(){
 
  frmMyQRCode.preShow = frmMyQRCodePreshow;
  frmMyQRCode.postShow = frmMyQRCodePostshow;
  frmMyQRCode.onDeviceBack =  doNothing;
  //frmMyQRCode.btnShare.onClick =  onClickshareMYQR;
  frmMyQRCode.vbxImage.onClick = onClickShareImageMYQR;
  frmMyQRCode.vbxMessenger.onClick = onClickMessengerImageMYQR;
  frmMyQRCode.vbxLine.onClick = onClickShareLineMYQR;
  frmMyQRCode.vbxOthers.onClick = onClickshareOthersMYQR;
  frmMyQRCode.btnDone.onClick = btnDoneonClickMyQR;
  frmMyQRCode.btnBack.onClick = showQRScanningLanding;
  
} 




function btnDoneonClickMyQR(){
  if(isSignedUser){
              	frmMyQRCode.btnDone.onClick = showAccountSummaryFromMenu
            }else{
             	frmMyQRCode.btnDone.onClick = showAccessPinScreenQR; 
            }
}

function cancelMyQrFlow(){
    if(kony.application.getPreviousForm().id == "frmscanqrLanding"){
	 	if(isSignedUser){
          showAccountSummaryFromMenu();
        }else{
          showAccessPinScreenQR();
        }
    }else if(kony.application.getPreviousForm().id == "frmMyQRSelectPromptPay"){
     	frmMyQRCode.show();
    }else{		  
    	kony.application.getPreviousForm().show();
	}
}
function frmMyQRCodePreshow(){
  
  frmMyQRCode.lblBodyHeader.text = kony.i18n.getLocalizedString("msgShowQRCode")
  if(gblShowlblQRDisclaimer == "true"){
    frmMyQRCode.lblQRDisclaimer.isVisible = true;
  }else{
    frmMyQRCode.lblQRDisclaimer.isVisible = false;
  }
  
}

function frmMyQRCodePostshow(){
  
  
}

function onClickBackfrmMyQRCode(){
     //onClickBackfrmMyQRCode();
}



function frmMyQRInformationInit(){
  frmMyQRInformation.preShow = frmMyQRInformationPreshow;
  frmMyQRInformation.postShow = frmMyQRInformationPostshow;
  frmMyQRInformation.btnGenerate.onClick = generateQRCode;
  frmMyQRInformation.btnCancel.onClick = showAccountSummaryFromMenu;
  frmMyQRInformation.onDeviceBack =  doNothing;
  frmMyQRInformation.lblAmount.onTouchEnd = onClickAmountLable;
  frmMyQRInformation.btnBack.onClick = onClickBackfrmMyQRInformation;
  frmMyQRInformation.btnCancel.onClick = onClickBackfrmMyQRInformation;
  frmMyQRInformation.txtAmount.onTextChange = ontextChangeAmountMYQRInfo;
  frmMyQRInformation.txtAmount.onDone = onDoneAmountMYQRInfo;
  //#ifdef iphone
  frmMyQRInformation.txtAmount.onEndEditing = onDoneAmountMYQRInfo;
  //#endif
  
}


function ontextChangeAmountMYQRInfo(){
	var enteredAmount = frmMyQRInformation.txtAmount.text;
	if(isNotBlank(enteredAmount)) {
		enteredAmount = kony.string.replace(enteredAmount, ",", "");
		if(isNotBlank(enteredAmount) && enteredAmount.length > 0 && parseFloat(enteredAmount, 10) == 0){
			
		}else{
			frmMyQRInformation.txtAmount.text = commaFormattedTransfer(enteredAmount);
		}
	}
}



function onDoneAmountMYQRInfo(){ 
  if(frmMyQRInformation.txtAmount.text != "" ){
     if (kony.string.containsChars(frmMyQRInformation.txtAmount.text, ["."]))
    frmMyQRInformation.txtAmount.text = frmMyQRInformation.txtAmount.text + " " + kony.i18n.getLocalizedString("currencyThaiBaht") ;
  else
    frmMyQRInformation.txtAmount.text = frmMyQRInformation.txtAmount.text + ".00";
    var num = parseFloat(replaceAll(onDoneEditingAmountValue(frmMyQRInformation.txtAmount.text),",",""));
    num = commaFormattedTransfer(num.toFixed(2)+"");
    if(num != "" ){
    frmMyQRInformation.txtAmount.text = num + " "+ kony.i18n.getLocalizedString("currencyThaiBaht")
    }else{
      frmMyQRInformation.txtAmount.text = num;
      
    }
    
  }
 

}


function onClickBackfrmMyQRInformation(){
  frmMyQRSelectPromptPay.imgSlectMobileNumber.isVisible = false;
  frmMyQRSelectPromptPay.imgSlectCitizenId.isVisible = false;
  //frmMyQRCode.show()
  if(kony.application.getPreviousForm().id == "frmscanqrLanding"){
	  showQRScanningLanding();
  }else if(kony.application.getPreviousForm().id == "frmMyQRSelectPromptPay"){
    frmMyQRCode.show();
  }else{
		  
  kony.application.getPreviousForm().show();
	}
}
function frmMyQRInformationPreshow(){
  
  
}

function frmMyQRInformationPostshow(){
  
  
}

function frmMyQRSelectPromptPayInit(){
   frmMyQRSelectPromptPay.preShow = frmMyQRSelectPromptPayPreshow;
   frmMyQRSelectPromptPay.postShow = frmMyQRSelectPromptPayPostshow;
   frmMyQRSelectPromptPay.flexMobileNumber.onClick = onClickMobileFlex;
   frmMyQRSelectPromptPay.flexCitizenId.onClick = onClickCitizenIdFlex;
   frmMyQRSelectPromptPay.onDeviceBack =  doNothing;
   frmMyQRSelectPromptPay.flexCrossmark.onClick = closeSelectPromptpay;
  
}

function closeSelectPromptpay(){
    frmMyQRInformation.show();                           
}
function frmMyQRSelectPromptPayPreshow(){
  
  
}

function frmMyQRSelectPromptPayPostshow(){
  
  
}
function onClickDoItLater(){
    if(isSignedUser){
              	showAccountSummaryWithoutRefresh();
            }else{
             	showAccessPinScreenQR();; 
            }
}
function frmMyQRPromptPayRegisterinit(){
  frmMyQRPromptPayRegister.preShow = frmMyQRPromptPayRegisterPreshow;
  frmMyQRPromptPayRegister.postShow = frmMyQRPromptPayRegisterPostshow;
  frmMyQRPromptPayRegister.btnDoLater.onTouchEnd = onClickDoItLater;
  frmMyQRPromptPayRegister.onDeviceBack =  doNothing;
  frmMyQRPromptPayRegister.btnRegister.onClick =  onClickRegisterForPrompPayMyQR;
  frmMyQRPromptPayRegister.btnBack.onClick =  showQRScanningLanding;
  
}


function onClickRegisterForPrompPayMyQR(){
  if(isSignedUser){
    menuSetUserIDonClick();
  }else{
   if(frmMBPreLoginAccessesPin.FlexQuickBalanceContainer.left == "0%"){
        frmMBPreLoginAccessesPin.FlexQuickBalanceContainer.left="-100%";
        frmMBPreLoginAccessesPin.FlexLoginContainer.left="0%";
        //showFPFlex(false)
  	    //closeQuickBalance();
      }
    gblGoDirectlyToPrompPay = true;
	frmMBPreLoginAccessesPin.flexCrossmark.isVisible = true;
    frmMBPreLoginAccessesPin.flexHeader.isVisible = false;
	frmMBPreLoginAccessesPin.flexSwipeContainer.top = "20%"
	frmMBPreLoginAccessesPin.FlexContainer0b16eb3557e0043.isVisible = false
	frmMBPreLoginAccessesPin.imgQRHeaderLogo.isVisible = true;
    showAccessPinScreenQR();
  }
  
}

function onClickCloseMarkQR(){
    gblGoDirectlyToPrompPay = false;
	frmMBPreLoginAccessesPin.flexCrossmark.isVisible = false;
    frmMBPreLoginAccessesPin.flexHeader.isVisible = true;
	frmMBPreLoginAccessesPin.flexSwipeContainer.top = "0%"
	frmMBPreLoginAccessesPin.FlexContainer0b16eb3557e0043.isVisible = true
	frmMBPreLoginAccessesPin.imgQRHeaderLogo.isVisible = false; 
    frmMBPreLoginAccessesPin.LabelLoginForQR.isVisible = false; 
    frmMBPreLoginAccessesPin.FlexLine.isVisible = false; 
    frmMBPreLoginAccessesPin.FlexLoginContainer.top = "0dp";
  	var prevsForm=kony.application.getPreviousForm();
  	if(gblPreLoginBillPayFlow)
    {
      showQRScanningLanding();
    } else if (gblQrSetDefaultAccnt){
      showQRScanningLanding();
    }else if (prevsForm.id=="frmscanqrLanding"){
      showQRScanningLanding();
    }
  	else{
      frmMyQRPromptPayRegister.show();
    }
  	//resetting to default values;
    gblPreLoginBillPayFlow=false;
  	gblQrSetDefaultAccnt=false;
}
function frmMyQRPromptPayRegisterPreshow(){
  
  
}

function frmMyQRPromptPayRegisterPostshow(){
  
}

function clearQRFormContent (){
  frmMyQRInformation.txtAmount.text = "";
  frmMyQRInformation.imgArrow.isVisible = false;
  frmMyQRSelectPromptPay.imgSlectMobileNumber.isVisible = false;
  frmMyQRSelectPromptPay.imgSlectCitizenId.isVisible = false;
  frmMyQRSelectPromptPay.lblCitizenId.text = "";
  frmMyQRSelectPromptPay.lblMobileNumber.isVitextsible = "";
}
function checkPromptPayRegiser(){
  clearQRFormContent();
  gblPromptPayAllDetails = [];
  showLoadingScreen();
  if(isSignedUser){
     var inputParams = {};
  invokeServiceSecureAsync("AnyIDInqComposite", inputParams, checkPromptPayRegiserCallBack);
    
  }else{
    inputParam = {};
                                inputParam["channel"] = "02";
                                inputParam["locale"] = "en_US";
                                inputParam["encryptDeviceId"]=getDeviceID();
                                inputParam["chkUserStatusOnly"]="NO";
                                inputParam["appID"] = "01";
                                inputParam["qrCodeType"] = "qrMobileNo";
                                inputParam["transType"] = "TRANSFER_PROMATPAY";
                                invokeServiceSecureAsync("GenerateQRTransaction", inputParam, checkPromptPayRegiserCallBack);

    
  }
 
  
}



function GenerateMYQRCode(qrType,anyIDValue,QRAmount){
  var inputParams = {};
  inputParams["qrCodeType"] = qrType;
  inputParams["qrPromptpayID"] = anyIDValue; 
  if(QRAmount != undefined && QRAmount != ""){
     //QRAmount = replaceAll(QRAmount," ",""); 
   var amountQr = replaceAll(QRAmount," ","");
    inputParams["amount"] = encryptData(amountQr);
//     inputParams["amount"] =replaceAll(QRAmount," ","");
  }
  // Mobile number all zero's check
  if("0000000000" == anyIDValue || "000000000" == anyIDValue){
      showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
         dismissLoadingScreen()
       return false;
                  }
  invokeServiceSecureAsync("BOTQRCodeGenerator", inputParams, BOTQRCodeGeneratorCallback);
}

function onClickMyQR(){
  
  
}
function checkPromptPayRegiserCallBack(status,resulttable){
  if(status == 400){
  	gblQRByMobileDefault  = false;
  	gblPromptPayResultValue = [];
  	if(resulttable!=null){
    	if(resulttable["opstatus"] == 0) {
          	gblPromptPayResultValue = resulttable;
	          if(!isSignedUser){
	            if(resulttable["PROMATPAYINQ"] == "Y"){
	               kony.print("--->"+resulttable['customerName'])
	               frmMyQRCode.lblCustomerName.text = resulttable["customerName"]
	               frmMyQRCode.lblCustomerNameTH.text = resulttable["customerNameTh"]
	               frmMyQRCode.btnDone.text = kony.i18n.getLocalizedString("btn_EditQR")
	               frmMyQRCode.lblHdrTxt.text = kony.i18n.getLocalizedString("key_titleMyQR")
	               frmMyQRCode.btnBack.isVisible = true;
	               frmMyQRCode.btnDone.onClick = onClickCustomizeQRCode;
	               //frmMyQRCode.btnBack.onClick = onClickMyQR;
	               if(resulttable.MobileDS!=null && resulttable.MobileDS.length>0)
				 	{
	                  frmMyQRCode.lblAmount.text = ""  
		              frmMyQRCode.lblAmount.isVisible = false;
		              SetAlignmentsNoAmount();
		              GenerateMYQRCode("qrMobileNo",resulttable.MobileDS[0]["anyIDValue"]);
	              		return; 
	            	}
		           if(resulttable.CIDS!=null && resulttable.CIDS.length>0)
					{
		            /*  frmMyQRCode.lblAmount.text = ""  
		              frmMyQRCode.lblAmount.isVisible = false;
		              GenerateMYQRCode("qrCitizenId",resulttable.CIDS[0]["anyIDValue"]); */
		               onClickCustomizeQRCode();
		          	}
		        }else{
		              frmMyQRPromptPayRegister.show();		              
		        }
          }else{
          	  
              if((resulttable.MobileDS!=null && resulttable.MobileDS.length>0) || (resulttable.CIDS!=null && resulttable.CIDS.length>0)  ){
	               frmMyQRCode.btnDone.text = kony.i18n.getLocalizedString("btn_EditQR")
	               frmMyQRCode.lblHdrTxt.text = kony.i18n.getLocalizedString("key_titleMyQR")
	               frmMyQRCode.btnBack.isVisible = true;
	               frmMyQRCode.btnDone.onClick = onClickCustomizeQRCode;
	               if(resulttable.MobileDS!=null && resulttable.MobileDS.length>0){
		                if(resulttable.MobileDS[0]["iTMXFlag"] == "Y" && resulttable.MobileDS[0]["onUSFlag"] == "Y"){
			                frmMyQRCode.lblAmount.text = ""  
			                frmMyQRCode.lblAmount.isVisible = false;
			                 SetAlignmentsNoAmount();
			                GenerateMYQRCode("qrMobileNo",resulttable.MobileDS[0]["anyIDValue"]);
			                return;
		                }		                
		                /*else{
							frmMyQRPromptPayRegister.show();
			                dismissLoadingScreen();
			                return;
					    }*/
	            	}
	            	
	            	if(resulttable.CIDS!=null && resulttable.CIDS.length>0){
	              		if(resulttable.CIDS[0]["iTMXFlag"] == "Y" && resulttable.CIDS[0]["onUSFlag"] == "Y"){
			             /*  frmMyQRCode.lblAmount.text = ""  
			              frmMyQRCode.lblAmount.isVisible = false;
			              GenerateMYQRCode("qrCitizenId",resulttable.CIDS[0]["anyIDValue"]);*/
	                		onClickCustomizeQRCode();
	                		dismissLoadingScreen();
	                		return;
		               }
		               /*else{
							frmMyQRPromptPayRegister.show();
			                dismissLoadingScreen();
			                return;
					   }*/
	                }
	                
	                if((resulttable.MobileDS[0]["iTMXFlag"] == "N" && resulttable.MobileDS[0]["onUSFlag"] == "N") && (resulttable.CIDS[0]["iTMXFlag"] == "N" && resulttable.CIDS[0]["onUSFlag"] == "N") ){
	                  	frmMyQRPromptPayRegister.show();
	                  	dismissLoadingScreen();
	                  	return;
	                }else{
	                	frmMyQRPromptPayRegister.show();
	                  	dismissLoadingScreen();
	                  	return;
	                }
	                            
              }else{
              	frmMyQRPromptPayRegister.show();
              	dismissLoadingScreen();
	            return;
              }
          }
        }else{          
         showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
        }
    }
  	dismissLoadingScreen();
  }else{
    dismissLoadingScreen();
    showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
  }
}
  
function showPromptPayAccounts(){
  frmMyQRSelectPromptPay.show();
}


function onClickMobileFlex(){
  gblMyQRonMobile = true;
  gblMyQRonCitizenId = false;
  frmMyQRSelectPromptPay.imgSlectMobileNumber.isVisible = true;
  frmMyQRSelectPromptPay.imgSlectCitizenId.isVisible = false;
  frmMyQRInformation.lblPromptPayValue.text =formatMobileNumber(replaceAll(frmMyQRSelectPromptPay.lblMobileNumber.text,"-",""));
  frmMyQRInformation.lblPromptPayValue.centerY = "35%"
  frmMyQRInformation.lblPromptPayKey.isVisible = true;
  frmMyQRInformation.lblPromptPayValue.isVisible = true;
  frmMyQRInformation.lblPromptPayKey.text = kony.i18n.getLocalizedString("lbLinkedPPwithMobileNumber")
  frmMyQRInformation.btnGenerate.isVisible = true;
  frmMyQRInformation.btnCancel.width = "50%";
  frmMyQRInformation.show();
  if(frmMyQRInformation.txtAmount.text == ""){
    frmMyQRInformation.txtAmount.setFocus(true);
  }
  
}

function onClickCitizenIdFlex(){
  gblMyQRonMobile = false;
  gblMyQRonCitizenId = true;
  frmMyQRInformation.lblPromptPayValue.centerY = "35%"
  frmMyQRSelectPromptPay.imgSlectMobileNumber.isVisible = false;
  frmMyQRSelectPromptPay.imgSlectCitizenId.isVisible = true;
  frmMyQRInformation.lblPromptPayKey.isVisible = true;
  frmMyQRInformation.lblPromptPayValue.isVisible = true;
  frmMyQRInformation.lblPromptPayValue.text =formatCitizenID(replaceAll(frmMyQRSelectPromptPay.lblCitizenId.text,"-",""));
  frmMyQRInformation.lblPromptPayKey.text = kony.i18n.getLocalizedString("lbLinkedPPwithCitizenID")
  frmMyQRInformation.btnGenerate.isVisible = true;
  frmMyQRInformation.btnCancel.width = "50%";
  frmMyQRInformation.show();
  if(frmMyQRInformation.txtAmount.text == ""){
    frmMyQRInformation.txtAmount.setFocus(true);
  }
}


function generateQRCode(){
         showLoadingScreen();
  		  var promtPayAmount = "";
         if(gblMyQRonCitizenId){
           var promtPayType = "qrCitizenId";
            
         }else{
           var promtPayType = "qrMobileNo";
          
         }
            if(frmMyQRInformation.txtAmount.text != null){
              
              var promtPayAmount=  replaceAll(frmMyQRInformation.txtAmount.text,",",""); 
              promtPayAmount=  replaceAll(promtPayAmount,kony.i18n.getLocalizedString("currencyThaiBaht"),""); 
              var str = parseFloat(promtPayAmount).toFixed(2);
               //frmMyQRCode.lblAmount.text = str+" "+ kony.i18n.getLocalizedString('currencyThaiBaht') 
               frmMyQRCode.lblAmount.text = commaFormattedTransfer(str.substring(0,str.indexOf('.'))) + str.substring(str.indexOf('.'),str.length)
              // frmMyQRCode.richTextAmount.text = "<label style='color:#333333'><font size='12'>"+str.substring(0,str.indexOf('.'))+"</font></label><label style='color:#333333'><font size='6'>"+str.substring(str.indexOf('.'),str.length)+ kony.i18n.getLocalizedString('currencyThaiBaht') +"</font></label>";
              //frmMyQRCode.lblAmount.text = "<label style='color:#333333'><font size='12'>"+str.substring(0,str.indexOf('.'))+"</label><label style='color:#333333'><font size='6'>"+str.substring(str.indexOf('.'),str.length)+ kony.i18n.getLocalizedString('currencyThaiBaht') +"</label>";
              frmMyQRCode.lblAmount.isVisible =true;
              setAlignmentWithAmount();
              //frmMyQRCode.lblAmount.text = "<label style='color:#333333'><font size='12'>10,000</label><label style='color:#333333'><font size='6'>.00฿</label>"
              
              //frmMyQRCode.lblAmount1.text = str.substring(0,str.indexOf('.'));
              //frmMyQRCode.lblAmount2.text = str.substring(str.indexOf('.'),str.length)+kony.i18n.getLocalizedString('currencyThaiBaht');
              //frmMyQRCode.richTextAmount.isVisible = true;     
              if(parseFloat(promtPayAmount) <= 0 || parseFloat(promtPayAmount).toFixed(2) == 0.00 || parseFloat(promtPayAmount) == 0 || promtPayAmount == ""){
			   frmMyQRInformation.txtAmount.text = "";
                SetAlignmentsNoAmount();
              // showAlertWithCallBack(kony.i18n.getLocalizedString("lbEnterAmount"), kony.i18n.getLocalizedString("info"),setFocustoAmountQR);
               
              // dismissLoadingScreen()
             //  return false;
              }
            }else{
              showAlertWithCallBack(kony.i18n.getLocalizedString("lbEnterAmount"), kony.i18n.getLocalizedString("info"),setFocustoAmountQR);
               
               dismissLoadingScreen()
              return false;
            }
  			
  			if(frmMyQRInformation.lblPromptPayValue.text != null && frmMyQRInformation.lblPromptPayValue.text != ""){
           	var promtPayValue = replaceAll(frmMyQRInformation.lblPromptPayValue.text,"-",""); 
            }else{
              showAlert(kony.i18n.getLocalizedString("msgSelectPromptpayforQR"), kony.i18n.getLocalizedString("info"));
              dismissLoadingScreen()
              return false;
            }
  			frmMyQRCode.btnDone.text = kony.i18n.getLocalizedString("CAV08_btnHome")
            frmMyQRCode.lblHdrTxt.text = kony.i18n.getLocalizedString("TRComplete_Title")
            frmMyQRCode.btnBack.isVisible = false;
            if(isSignedUser){
              	frmMyQRCode.btnDone.onClick = showAccountSummaryFromMenu
            }else{
             	frmMyQRCode.btnDone.onClick = showAccessPinScreenQR; 
            }
            
            GenerateMYQRCode(promtPayType, promtPayValue, promtPayAmount);
           // BOTQRCodeGeneratorCallback()
}
function setFocustoAmountQR(){
  frmMyQRInformation.txtAmount.setFocus(true);
}
 function showAccessPinScreenQR(){
   gblLoginTab = "FP";
   gblFPLoginSvsCalled = false;
   frmMBPreLoginAccessesPin.show();
 }
function BOTQRCodeGeneratorCallback(status,resulttable){
                if(status == 400){
                  if(resulttable["opstatus"] == 0){
                    if(resulttable["qrDataSet"].length > 0){
                     frmMyQRCode.imgQRCode.base64 = resulttable["qrDataSet"][0]["base64QR"];
                      frmMyQRCode.imgQRCodeShare.base64 = resulttable["qrDataSet"][0]["base64QR"];
                      
                     if(isSignedUser){
                       frmMyQRCode.lblCustomerName.text = resulttable["qrDataSet"][0]["customerName"]
                       frmMyQRCode.lblCustomerNameTH.text = resulttable["qrDataSet"][0]["customerNameTh"]
                     }
                     else{
                        frmMyQRCode.lblCustomerName.text = resulttable["qrDataSet"][0]["customerName"]
                        frmMyQRCode.lblCustomerNameTH.text = resulttable["qrDataSet"][0]["customerNameTh"]
                     }
                     if(resulttable["qrDataSet"][0]["anyIDValue"].length == 13){
                       frmMyQRCode.lblPromotPayAccount.text =  formatCitizenID(resulttable["qrDataSet"][0]["anyIDValue"]); 
                     }else if(resulttable["qrDataSet"][0]["anyIDValue"].length == 9){
                       frmMyQRCode.lblPromotPayAccount.text =  formatMobileNumber("0"+resulttable["qrDataSet"][0]["anyIDValue"]); 
                     }else{
                       frmMyQRCode.lblPromotPayAccount.text = resulttable["qrDataSet"][0]["anyIDValue"];
                     }
                     frmMyQRCode.lblPromotPayAccountShare.text = frmMyQRCode.lblPromotPayAccount.text;
                      frmMyQRCode.lblCustomerNameShare.text = frmMyQRCode.lblCustomerName.text;
                      frmMyQRCode.lblCustomerNameShareTH.text = frmMyQRCode.lblCustomerNameTH.text;
                      frmMyQRCode.lblAmountShare.text = frmMyQRCode.lblAmount.text;
                     frmMyQRCode.show();
                     dismissLoadingScreen();
                  }
                    }else{
                      showAlert(kony.i18n.getLocalizedString("msg_ErrGenerateQRFail"), kony.i18n.getLocalizedString("info"));
                      dismissLoadingScreen();
                    }
                    
}
  dismissLoadingScreen();
}

function onClickAmountLable(){
  frmMyQRInformation.lblAmount.isVisible = false;
  frmMyQRInformation.txtAmount.isVisible = true;
  frmMyQRInformation.txtAmount.setFocus(true);
}

function onClickShareImageMYQR(){
  //#ifdef iphone
  		screenShotCall("screenshot");
  //#endif 
  //#ifdef android
      	shareIntentmoreCallandroid("screenshot","MyQR");
  //#endif 
  
}

function onClickMessengerImageMYQR(){
  shareIntentCall("facebook","MyQR");
  
  
}


function onClickShareLineMYQR(){
  shareIntentCall("line","MyQR");
  
  
}

function onClickshareOthersMYQR(){
  //#ifdef iphone
  	shareIntentmoreCall("more", "MyQR");
   //#endif
  //#ifdef android
  	shareIntentmoreCallandroid("more", "MyQR");
    //#endif
}
 
/* function onClickshareMYQR (){
  	
		if(frmMyQRCode.hbxShareOption.isVisible){
			frmMyQRCode.hbxShareOption.isVisible = false;
			frmMyQRCode.btnShare.skin = "btnShare";
		}else{
			frmMyQRCode.hbxShareOption.isVisible = true
			frmMyQRCode.btnShare.skin = "btnShareFoc";
		}	
		
  
}*/


function onClickCustomizeQRCode(){
  //frmMyQRInformation.txtAmount.setFocus(true);
   gblMyQRonMobile = false;
   gblMyQRonCitizenId = false;
   gblPromptPayAllDetails = [];
   frmMyQRInformation.lblPromptPayValue.text = "";
   frmMyQRInformation.txtAmount.text = "";
   frmMyQRSelectPromptPay.lblMobileNumber.text = "";
   frmMyQRSelectPromptPay.lblCitizenId.text = "";
   if(gblPromptPayResultValue.MobileDS!=null && gblPromptPayResultValue.MobileDS.length>0)
		 {
              if(gblPromptPayResultValue.MobileDS[0]["iTMXFlag"] == "Y" && gblPromptPayResultValue.MobileDS[0]["onUSFlag"] == "Y"){
              gblMyQRonMobile =  true;
              gblPromptPayAllDetails.push(gblPromptPayResultValue.MobileDS[0]["anyIDValue"]);  
              }
              
            }
          if(gblPromptPayResultValue.CIDS!=null && gblPromptPayResultValue.CIDS.length>0)
			{
               if(gblPromptPayResultValue.CIDS[0]["iTMXFlag"] == "Y" && gblPromptPayResultValue.CIDS[0]["onUSFlag"] == "Y"){
              gblMyQRonCitizenId =  true;
              gblPromptPayAllDetails.push(gblPromptPayResultValue.CIDS[0]["anyIDValue"])
               }
            }
           if(gblPromptPayAllDetails.length == 1){
                frmMyQRInformation.txtAmount.setFocus(true);
                frmMyQRInformation.btnGenerate.isVisible = true;
 				frmMyQRInformation.btnCancel.width = "50%";
               frmMyQRInformation.lblPromptPayValue.centerY = "35%"
               if(gblPromptPayAllDetails[0].length == 13){
                 frmMyQRInformation.lblPromptPayValue.text = formatCitizenID(gblPromptPayAllDetails[0])
               }else if(gblPromptPayAllDetails[0].length == 10){
                // frmMyQRInformation.txtAmount.setFocus(true);
                 frmMyQRInformation.lblPromptPayValue.text = formatMobileNumber(gblPromptPayAllDetails[0])
               }else{
                 frmMyQRInformation.lblPromptPayValue.text = gblPromptPayAllDetails[0]
               }
              // frmMyQRInformation.lblPromptPayValue.text = gblPromptPayAllDetails[0]
               frmMyQRInformation.imgArrow.isVisible = false;
               if(gblMyQRonCitizenId){
                 frmMyQRInformation.lblPromptPayKey.text = kony.i18n.getLocalizedString("lbLinkedPPwithCitizenID")
               }else{
                frmMyQRInformation.lblPromptPayKey.text = kony.i18n.getLocalizedString("lbLinkedPPwithMobileNumber")
               }
               frmMyQRInformation.lblPromptPayKey.isVisible = true;
               frmMyQRInformation.flexSelectPromptPay.onClick = doNothing;
               frmMyQRInformation.lblAmount.isVisible = false;
               frmMyQRInformation.txtAmount.isVisible = true;
             }else{
                frmMyQRInformation.txtAmount.isVisible = true;
                frmMyQRInformation.lblAmount.isVisible = false;
                frmMyQRInformation.lblPromptPayValue.centerY = "50%"
                frmMyQRInformation.lblPromptPayValue.text = kony.i18n.getLocalizedString("msgSelectPromptpayforQR")
                frmMyQRInformation.lblPromptPayKey.isVisible = false;
                frmMyQRInformation.imgArrow.isVisible = true;
                frmMyQRInformation.lblPromptPayValue.isVisible = true;
                frmMyQRSelectPromptPay.lblMobileNumber.text = formatMobileNumber(gblPromptPayAllDetails[0]);
                frmMyQRSelectPromptPay.lblCitizenId.text = formatCitizenID(gblPromptPayAllDetails[1]); 
                frmMyQRInformation.flexSelectPromptPay.onClick =  showPromptPayAccounts; 
                frmMyQRInformation.btnGenerate.isVisible = false;
 				frmMyQRInformation.btnCancel.width = "100%";
                frmMyQRInformation.txtAmount.setFocus(false);
             }
             frmMyQRInformation.show(); 
  			 if(gblPromptPayAllDetails.length == 1){
               frmMyQRInformation.txtAmount.setFocus(true);
             }
}

function SetAlignmentsNoAmount(){
  frmMyQRCode.flexCustomerNames.width = "100%"
  frmMyQRCode.flexCustomerNamesShare.width = "100%"
  var screenwidth = gblDeviceInfo["deviceWidth"];
  var screenheight = gblDeviceInfo["deviceHeight"];
  //#ifdef iphone
		if (screenwidth <= 730 && screenheight < 1280) {
			 frmMyQRCode.flexCustomerDetailsShare.width = "100%"
             frmMyQRCode.lblCustomerNameShareTH.skin = "lblBlackMedium140RSUBold"
             frmMyQRCode.lblAmountShare.skin = "lblBlackMedium140RSUBold"
			}
  //#endif	
  //#ifdef android
		if (screenwidth<= 768 && screenheight < 1280) {
			 frmMyQRCode.flexCustomerDetailsShare.width = "100%"
             frmMyQRCode.lblCustomerNameShareTH.skin = "lblBlackMedium140RSUBold"
             frmMyQRCode.lblAmountShare.skin = "lblBlackMedium140RSUBold"
		}
	//#endif
  frmMyQRCode.flexCustomerAmount.isVisible = false;
  frmMyQRCode.flexCustomerAmountShare.isVisible = false;
  frmMyQRCode.lblCustomerName.contentAlignment =  5;
  frmMyQRCode.lblCustomerNameTH.contentAlignment =  5;
  frmMyQRCode.lblCustomerNameShare.contentAlignment =  5;
  frmMyQRCode.lblCustomerNameShareTH.contentAlignment =  5;
}

function setAlignmentWithAmount(){
  frmMyQRCode.flexCustomerNames.width = "65%"
  frmMyQRCode.flexCustomerNamesShare.width = "65%"
  var screenwidth = gblDeviceInfo["deviceWidth"];
  var screenheight = gblDeviceInfo["deviceHeight"];
  //#ifdef iphone
		if (screenwidth <= 730 && screenheight < 1280) {
			 frmMyQRCode.flexCustomerDetailsShare.width = "100%"
             frmMyQRCode.lblCustomerNameShareTH.skin = "lblBlackMedium140RSUBold"
             frmMyQRCode.lblAmountShare.skin = "lblBlackMedium140RSUBold"
			}
  //#endif	
  //#ifdef android
		if (screenwidth<= 768 && screenheight < 1280) {
			 frmMyQRCode.flexCustomerDetailsShare.width = "100%"
             frmMyQRCode.lblCustomerNameShareTH.skin = "lblBlackMedium140RSUBold"
             frmMyQRCode.lblAmountShare.skin = "lblBlackMedium140RSUBold"
		}
	//#endif
  frmMyQRCode.flexCustomerAmount.isVisible = true;
  frmMyQRCode.flexCustomerAmountShare.isVisible = true;
  frmMyQRCode.lblCustomerName.contentAlignment =  4;
  frmMyQRCode.lblCustomerNameTH.contentAlignment =  4;
  frmMyQRCode.lblCustomerNameShare.contentAlignment =  4;
  frmMyQRCode.lblCustomerNameShareTH.contentAlignment =  4;
}

