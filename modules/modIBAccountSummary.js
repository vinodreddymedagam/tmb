//MKI, MIB-mutual fund IB On/Off change - START
function showAlertMFWarnIB() {
  	var KeyTitle = "Info"//kony.i18n.getLocalizedString("keyOK");
   // var keyMsg = kony.i18n.getLocalizedString("keyTouchMessage");
    var okk = kony.i18n.getLocalizedString("keyOK");
    
	//Defining basicConf parameter for alert
  	var keyMsg = kony.i18n.getLocalizedString("Mutual_Fund_Service_Warn");
	var basicConf = {
		message: keyMsg,
		alertType: constants.ALERT_TYPE_INFO,
		alertTitle: KeyTitle,
		yesLabel: okk,
		noLabel: "",
		alertHandler: checkSuitabilityExpireIB
	};
	//Defining pspConf parameter for alert
	var pspConf = {};
	//Alert definition
	var infoAlert = kony.ui.Alert(basicConf, pspConf);
    function handleok(response){}
    
	return;
}

function showAlertMFoffIB() {
  	var KeyTitle = "Info"//kony.i18n.getLocalizedString("keyOK");
   // var keyMsg = kony.i18n.getLocalizedString("keyTouchMessage");
    var okk = kony.i18n.getLocalizedString("keyOK");
    
	//Defining basicConf parameter for alert
  	var keyMsg = kony.i18n.getLocalizedString("Mutual_Fund_Service_Off");
	var basicConf = {
		message: keyMsg,
		alertType: constants.ALERT_TYPE_INFO,
		alertTitle: KeyTitle,
		yesLabel: okk,
		noLabel: "",
		alertHandler: handleok
	};
	//Defining pspConf parameter for alert
	var pspConf = {};
	//Alert definition
	var infoAlert = kony.ui.Alert(basicConf, pspConf);
    function handleok(response){}
    
	return;
}
function checkMutualFundServiceStatusIB(){
  try{
    if(isNotBlank(gblMutualFundService)){
      if(gblMutualFundService=="ON"||gblMutualFundService=="on"){
        checkSuitabilityExpireIB();
      }else if(gblMutualFundService=="WARN"||gblMutualFundService=="warn"){
            showAlertMFWarnIB();
      }else if(gblMutualFundService=="OFF"||gblMutualFundService=="off"){
        	showAlertMFoffIB();
      }
    }else{
      //do nothing
    }
  }catch(e){
    //do nothing
  }
}
//MKI, MIB-mutual fund IB On/Off change - END
function showIBAccountDetails() {
	curr_form = kony.application.getCurrentForm();
	
	if(curr_form.segAccountDetails.selectedIndex != null)
	{
		var selectedIndex = curr_form.segAccountDetails.selectedIndex[1];
	//var indexString = hboxID.split("hboxSwipe");
		gblIndex = selectedIndex;
	}
	else
	{
		if (frmIBPostLoginDashboard.segAccountDetails.selectedIndex != null)
			frmIBAccntSummary.segAccountDetails.selectedIndex = frmIBPostLoginDashboard.segAccountDetails.selectedIndex;
		else
			frmIBAccntSummary.segAccountDetails.selectedIndex = [0,0];	
		var selectedIndex = curr_form.segAccountDetails.selectedIndex[1];
		gblIndex = selectedIndex;
	}
	
	var selectedItem = curr_form.segAccountDetails.selectedIndex[1];
	var selectedData = curr_form.segAccountDetails.data[selectedItem];
	
	//ENH_106_107 & ENH_108 - MF(Mutual Fund) Details and BA View Policy Details - Logic to navigate to MF & BA Account Summary
					
	if(selectedData.isMFAccount == "Y") {
      		checkMutualFundServiceStatusIB(); //MKI, MIB-mutual fund IB On/Off change
	} else if(selectedData.isBAAccount == "Y") {
		callBAPolicyListService();
		frmIBBankAssuranceSummary.imgProfile.src = curr_form.imgProfile.src;
		frmIBBankAssuranceSummary.lblUsername.text = curr_form.lblUsername.text;
		frmIBBankAssuranceSummary.lblInvestmentValue.text = selectedData["lblBalanceVal"];
		frmIBBankAssuranceSummary.lblnvestment.text = kony.i18n.getLocalizedString("BA_lbl_Total_Sum_Insured");
	} else {
	
	resetdetailspage();
	
	frmIBAccntSummary.hbxApplySoGoooD.setVisibility(false);
	frmIBAccntSummary.hbxRedeemPoint.setVisibility(false);
	
	
	frmIBAccntSummary.scrollToWidget(hbxIBPostLogin);
	
	sbStr = gblAccountTable["custAcctRec"][gblIndex]["accId"];
   	var length = sbStr.length;
   	if (gblAccountTable["custAcctRec"][gblIndex]["accType"] == kony.i18n.getLocalizedString("Loan"))
		sbStr = sbStr.substring(7, 11);
	else
		sbStr = sbStr.substring(length - 4, length);
	var productId = gblAccountTable["custAcctRec"][gblIndex]["productID"];
	gaccType = gblAccountTable["custAcctRec"][gblIndex]["accType"];
	glb_viewName = gblAccountTable["custAcctRec"][gblIndex]["VIEW_NAME"];
	frmIBAccntSummary.lnkMyActivities.text = kony.i18n.getLocalizedString('MyActivities')
	frmIBAccntSummary.lnkFullStatement.text = kony.i18n.getLocalizedString('FullStat')
	frmIBAccntSummary.linkMore.text = kony.i18n.getLocalizedString('More')
	frmIBAccntSummary.lblDateHeader.text = kony.i18n.getLocalizedString('MatDate')
	frmIBAccntSummary.lblHeaderTransaction.text = kony.i18n.getLocalizedString('InterestRate')
	frmIBAccntSummary.lblHeaderBalance.text = kony.i18n.getLocalizedString('Balance')
	frmIBAccntSummary.lblDepositDetails.text = kony.i18n.getLocalizedString('DepDetails')
	frmIBAccntSummary.BtnTopUp.text = kony.i18n.getLocalizedString("TopUp");
	frmIBAccntSummary.btnTransfer.text = kony.i18n.getLocalizedString("Transfer");	
	frmIBAccntSummary.btnPayBill.text = kony.i18n.getLocalizedString("PayBill");
	frmIBAccntSummary.payMyCredit.text=kony.i18n.getLocalizedString("PayBill");
	frmIBAccntSummary.payMyLoan.text=kony.i18n.getLocalizedString("PayBill");
	var payBill=gblAccountTable["custAcctRec"][gblIndex]["Paybill"];
    var topup=gblAccountTable["custAcctRec"][gblIndex]["Topup"];
    var payMyCredit=gblAccountTable["custAcctRec"][gblIndex]["PayMyCreditCard"];
    var transfer=gblAccountTable["custAcctRec"][gblIndex]["Transfer"];
    var payMyLoan=gblAccountTable["custAcctRec"][gblIndex]["PayMyLoan"];
    var allowRedeemPoints = gblAccountTable["custAcctRec"][gblIndex]["allowRedeemPoints"];
    var allowApplySoGoood = gblAccountTable["custAcctRec"][gblIndex]["allowApplySoGoood"];
    var allowCardTypeForRedeem = gblAccountTable["custAcctRec"][gblIndex]["allowCardTypeForRedeem"];
    frmIBAccntSummary.lnkMyActivities.setVisibility(true);
    frmIBAccntFullStatement.button589518614375454.setVisibility(true);
    
    if(payBill=="Y")
    frmIBAccntSummary.btnPayBill.isVisible = true;
     if(topup=="Y")
    frmIBAccntSummary.BtnTopUp.isVisible = true;
     if(transfer=="Y")
    frmIBAccntSummary.btnTransfer.isVisible = true;
     if(payMyCredit=="Y")
    frmIBAccntSummary.payMyCredit.isVisible = true;
     if(payMyLoan=="Y")
    frmIBAccntSummary.payMyLoan.isVisible = true;
    if(payMyCredit=="Y" && payMyLoan=="N" && transfer=="N" && topup=="N"&& payBill=="N"){
	    frmIBAccntSummary.btnTransfer.containerWeight = 0;
		frmIBAccntSummary.payMyLoan.containerWeight=0;
        frmIBAccntSummary.payMyCredit.containerWeight=60;
		frmIBAccntSummary.btnPayBill.containerWeight=0;
		frmIBAccntSummary.BtnTopUp.containerWeight=0;
		frmIBAccntSummary.payMyCredit.skin=btnIB80px;
	}
	else if(payMyCredit=="N" && payMyLoan=="Y" && transfer=="N" && topup=="N"&& payBill=="N"){
	    frmIBAccntSummary.btnTransfer.containerWeight = 0;
		frmIBAccntSummary.payMyLoan.containerWeight=60;
        frmIBAccntSummary.payMyCredit.containerWeight=0;
		frmIBAccntSummary.btnPayBill.containerWeight=0;
		frmIBAccntSummary.BtnTopUp.containerWeight=0;
		frmIBAccntSummary.payMyLoan.skin=btnIB80px;
	}
	else if(payMyCredit=="N" && payMyLoan=="N" && transfer=="Y" && topup=="N" && payBill=="N"){
	    frmIBAccntSummary.btnTransfer.containerWeight = 60;
		frmIBAccntSummary.payMyLoan.containerWeight=0;
        frmIBAccntSummary.payMyCredit.containerWeight=0;
		frmIBAccntSummary.btnPayBill.containerWeight=0;
		frmIBAccntSummary.BtnTopUp.containerWeight=0;
	}
	else if(payMyCredit=="N" && payMyLoan=="Y" && transfer=="Y" && topup=="N" && payBill=="N")
	{
	    frmIBAccntSummary.btnTransfer.containerWeight =50;
		frmIBAccntSummary.payMyLoan.containerWeight=50;
        frmIBAccntSummary.payMyCredit.containerWeight=0;
		frmIBAccntSummary.btnPayBill.containerWeight=0;
		frmIBAccntSummary.BtnTopUp.containerWeight=0;
		frmIBAccntSummary.btnTransfer.skin=btnIB80px;
		frmIBAccntSummary.payMyLoan.skin=btnIB80px;
	}
	else if(payMyCredit=="N" && payMyLoan=="N" && transfer=="Y" && topup=="Y" && payBill=="Y"){
	    frmIBAccntSummary.btnTransfer.containerWeight =33;
		frmIBAccntSummary.payMyLoan.containerWeight=0;
        frmIBAccntSummary.payMyCredit.containerWeight=0;
		frmIBAccntSummary.btnPayBill.containerWeight=34;
		frmIBAccntSummary.BtnTopUp.containerWeight=34;
	}
	else
	{
	    frmIBAccntSummary.btnTransfer.containerWeight =23;
		frmIBAccntSummary.payMyLoan.containerWeight=13;
        frmIBAccntSummary.payMyCredit.containerWeight=14;
		frmIBAccntSummary.btnPayBill.containerWeight=27;
		frmIBAccntSummary.BtnTopUp.containerWeight=23;
	}
      var randomnum = Math.floor((Math.random() * 10000) + 1);
	 var imageName="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+gblAccountTable["custAcctRec"][gblIndex]["ICON_ID"]+"&modIdentifier=PRODICON"+randomnum;
	 frmIBAccntSummary.imgAccountDetailsPic.src=imageName;
	//view_type
	if (glb_viewName == "PRODUCT_CODE_DREAM_SAVING") {
		frmIBAccntSummary.lblDepositDetails.setVisibility(false);
		frmIBAccntSummary.hbxSegHeader.setVisibility(false);
		frmIBAccntSummary.hboxApplySendToSave.setVisibility(false);
		//frmIBAccntSummary.btnPayBill.skin=btnIB71;	
		frmIBAccntSummary.lblUseful1.text = kony.i18n.getLocalizedString("ProductName");
		frmIBAccntSummary.lblUseful2.text = kony.i18n.getLocalizedString("DreamDes");
		frmIBAccntSummary.lblUseful3.text = kony.i18n.getLocalizedString("DreamTarAmt");
		frmIBAccntSummary.lblUseful6.text = kony.i18n.getLocalizedString("AccountType");
		frmIBAccntSummary.lblUseful7.text = kony.i18n.getLocalizedString("AccountNo");
		frmIBAccntSummary.lblUseful8.text = kony.i18n.getLocalizedString("AccountName");
		frmIBAccntSummary.lblUseful10.text = kony.i18n.getLocalizedString("BranchName");
		frmIBAccntSummary.lblUseful11.text = kony.i18n.getLocalizedString("LedgerBalance");
		frmIBAccntSummary.lblUseful9.text = kony.i18n.getLocalizedString("Status");
		frmIBAccntSummary.lblUseful4.text = kony.i18n.getLocalizedString("DreamMonTranAmt");
		frmIBAccntSummary.lblUseful5.text = kony.i18n.getLocalizedString("DreamMonTranDate");
		frmIBAccntSummary.lblUseful12.text = kony.i18n.getLocalizedString("LinkedAcc");
		frmIBAccntSummary.lnkDreamSaving.isVisible = true;
		IBdreamSavingCallService();
	} else if (glb_viewName == "PRODUCT_CODE_NOFIXED") {
		frmIBAccntSummary.lblDepositDetails.setVisibility(false);
		frmIBAccntSummary.hbxSegHeader.setVisibility(false);
		frmIBAccntSummary.lblUsefulValue4.text = kony.i18n.getLocalizedString("SavingMB");
		frmIBAccntSummary.lblUseful1.text = kony.i18n.getLocalizedString("ProductName");
		frmIBAccntSummary.lblUseful4.text = kony.i18n.getLocalizedString("AccountType");
		frmIBAccntSummary.lblUseful5.text = kony.i18n.getLocalizedString("AccountNo");
		frmIBAccntSummary.lblUseful6.text = kony.i18n.getLocalizedString("AccountName");
		frmIBAccntSummary.lblUseful8.text = kony.i18n.getLocalizedString("BranchName");
		frmIBAccntSummary.lblUseful7.text = kony.i18n.getLocalizedString("Status");
		frmIBAccntSummary.lblUseful10.text = kony.i18n.getLocalizedString("keyLblInterestRate");
		frmIBAccntSummary.lblUseful9.text = kony.i18n.getLocalizedString("LedgerBalance");
		frmIBAccntSummary.lblUseful11.text = kony.i18n.getLocalizedString("IntrestAmount")
		frmIBAccntSummary.lblUseful12.text = kony.i18n.getLocalizedString("LinkedAcc")
		frmIBAccntSummary.lblUseful2.text = kony.i18n.getLocalizedString("SendToMaxPoint");
		frmIBAccntSummary.lblUseful3.text = kony.i18n.getLocalizedString("SendToMinPoint");
		IBdepositAccInqCallServiceForNoFixed();
	} else if (glb_viewName == "PRODUCT_CODE_NOFEESAVING_TABLE") {
	
		frmIBAccntSummary.lblUsefulValue3.text = kony.i18n.getLocalizedString("SavingMB");
		frmIBAccntSummary.lblDepositDetails.setVisibility(false);
		frmIBAccntSummary.hbxSegHeader.setVisibility(false);
		frmIBAccntSummary.hboxApplySendToSave.setVisibility(false);
		frmIBAccntSummary.lblUseful1.text = kony.i18n.getLocalizedString("ProductName");
		frmIBAccntSummary.lblUseful2.text = kony.i18n.getLocalizedString("remFreeTran");
		frmIBAccntSummary.lblUseful3.text = kony.i18n.getLocalizedString("AccountType");
		frmIBAccntSummary.lblUseful4.text = kony.i18n.getLocalizedString("AccountNo");
		frmIBAccntSummary.lblUseful5.text = kony.i18n.getLocalizedString("AccountName");
		frmIBAccntSummary.lblUseful7.text = kony.i18n.getLocalizedString("BranchName");
		frmIBAccntSummary.lblUseful8.text = kony.i18n.getLocalizedString("LedgerBalance");
		frmIBAccntSummary.lblUseful6.text = kony.i18n.getLocalizedString("Status");
		IBcallDepositAccountInquiryServiceNofee();
	} else if (glb_viewName == "PRODUCT_CODE_SAVING_TABLE" || glb_viewName == "PRODUCT_CODE_CURRENT_TABLE" || glb_viewName ==
		"PRODUCT_CODE_SAVINGCARE" || glb_viewName == "PRODUCT_CODE_NEWREADYCASH_TABLE") {
		if (gblAccountTable["custAcctRec"][gblIndex]["accType"] == kony.i18n.getLocalizedString("Current"))
			frmIBAccntSummary.lblUsefulValue2.text = kony.i18n.getLocalizedString("CurrentMB");
		else
			frmIBAccntSummary.lblUsefulValue2.text = kony.i18n.getLocalizedString("SavingMB");
		frmIBAccntSummary.lblDepositDetails.setVisibility(false);
		frmIBAccntSummary.hbxSegHeader.setVisibility(false);
		frmIBAccntSummary.hboxApplySendToSave.setVisibility(false);
		frmIBAccntSummary.lblUseful5.text = kony.i18n.getLocalizedString("Status");
		frmIBAccntSummary.lblUseful1.text = kony.i18n.getLocalizedString("ProductName");
		frmIBAccntSummary.lblUseful2.text = kony.i18n.getLocalizedString("AccountType");
		frmIBAccntSummary.lblUseful3.text = kony.i18n.getLocalizedString("AccountNo");
		frmIBAccntSummary.lblUseful4.text = kony.i18n.getLocalizedString("AccountName");
		frmIBAccntSummary.lblUseful6.text = kony.i18n.getLocalizedString("BranchName");
		frmIBAccntSummary.lblUseful7.text = kony.i18n.getLocalizedString("LedgerBalance");
		/*if(gblAccountTable["custAcctRec"][gblIndex]["accType"]=="DDA" && productId=="103"){
				frmIBAccntSummary.BtnTopUp.isVisible = false;
				frmIBAccntSummary.btnTransfer.isVisible = false;
				frmIBAccntSummary.btnPayBill.isVisible = true;
				frmIBAccntSummary.btnPayBill.skin = "btnIB80px";
        		frmIBAccntSummary.btnPayBill.hoverSkin = "btnIB80px";
				frmIBAccntSummary.btnPayBill.text = kony.i18n.getLocalizedString("PayBill");
				frmIBAccntSummary.btnPayBill.onClick = moveToBillPaymentFromAccountSummaryForLoan;
		}else{
				frmIBAccntSummary.BtnTopUp.isVisible = true
				frmIBAccntSummary.btnTransfer.isVisible = true
				frmIBAccntSummary.btnPayBill.isVisible = true
				frmIBAccntSummary.BtnTopUp.text = kony.i18n.getLocalizedString("TopUp");
				frmIBAccntSummary.btnTransfer.text = kony.i18n.getLocalizedString("Transfer");
				frmIBAccntSummary.btnPayBill.text = kony.i18n.getLocalizedString("PayBill");
		}*/
		IBcallDepositAccountInquiryService();
	} else if (glb_viewName == "PRODUCT_CODE_CREDITCARD_TABLE") {
		if(allowApplySoGoood == "1"){
			frmIBAccntSummary.hbxApplySoGoooD.setVisibility(true);
		}
		
		if (allowRedeemPoints == "1") {
			frmIBAccntSummary.hbxRedeemPoint.setVisibility(true);
		}
		
		frmIBAccntSummary.lblDepositDetails.setVisibility(false);
		frmIBAccntSummary.hbxSegHeader.setVisibility(false);
		frmIBAccntSummary.hboxApplySendToSave.setVisibility(false);
		frmIBAccntSummary.lnkMyActivities.setVisibility(false);
		frmIBAccntFullStatement.button589518614375454.setVisibility(false);
		frmIBAccntSummary.lblUseful2.text = kony.i18n.getLocalizedString("CardNo");
		frmIBAccntSummary.lblUseful3.text = kony.i18n.getLocalizedString("CardHolderName");
		frmIBAccntSummary.lblUseful4.text = kony.i18n.getLocalizedString("creditLimit");
		frmIBAccntSummary.lblUseful5.text = kony.i18n.getLocalizedString("StatDate");
		frmIBAccntSummary.lblUseful7.text = kony.i18n.getLocalizedString("TotalAmtDue");
		frmIBAccntSummary.lblUseful8.text = kony.i18n.getLocalizedString("MinAmtDue");
		frmIBAccntSummary.lblUseful1.text = kony.i18n.getLocalizedString("CardType");
		frmIBAccntSummary.lblUseful6.text = kony.i18n.getLocalizedString("PayDueDate");
		frmIBAccntSummary.lblUseful9.text = kony.i18n.getLocalizedString("DirectCredit");

		if (allowCardTypeForRedeem == "1") {
			frmIBAccntSummary.hbxUseful14.setVisibility(true);
			frmIBAccntSummary.hbxUseful15.setVisibility(true);		
			frmIBAccntSummary.lblUseful14.text = kony.i18n.getLocalizedString("keyIBAvailable");
			frmIBAccntSummary.lblUseful15.text = kony.i18n.getLocalizedString("keyPointExpiringOn");	
		}	
		IBcreditCardCallService();
	} else if (glb_viewName == "PRODUCT_CODE_OLDREADYCASH_TABLE") {		
		frmIBAccntSummary.lnkMyActivities.setVisibility(false);
		frmIBAccntFullStatement.button589518614375454.setVisibility(false);
		frmIBAccntSummary.lblUseful2.text = kony.i18n.getLocalizedString("CardNo");
		frmIBAccntSummary.lblUseful3.text = kony.i18n.getLocalizedString("CardHolderName");
		frmIBAccntSummary.lblUseful4.text = kony.i18n.getLocalizedString("creditLimit");
		frmIBAccntSummary.lblUseful5.text = kony.i18n.getLocalizedString("StatDate");
		frmIBAccntSummary.lblUseful7.text = kony.i18n.getLocalizedString("TotalAmtDue");
		frmIBAccntSummary.lblUseful8.text = kony.i18n.getLocalizedString("MinAmtDue");
		frmIBAccntSummary.lblUseful1.text = kony.i18n.getLocalizedString("CardType");
		frmIBAccntSummary.lblUseful6.text = kony.i18n.getLocalizedString("PayDueDate");
		frmIBAccntSummary.lblUseful9.text = kony.i18n.getLocalizedString("DirectCredit");		
		IBcreditCardCallService();
	} else if (glb_viewName == "PRODUCT_CODE_TD_TABLE") {
		frmIBAccntSummary.lblUsefulValue3.text = kony.i18n.getLocalizedString("TermDepositMB");
		frmIBAccntSummary.hboxApplySendToSave.setVisibility(false);
		frmIBAccntFullStatement.button589518614375454.setVisibility(false);
		frmIBAccntSummary.lblUseful1.text = kony.i18n.getLocalizedString("ProductName");
		frmIBAccntSummary.lblUseful3.text = kony.i18n.getLocalizedString("AccountType");
		frmIBAccntSummary.lblUseful4.text = kony.i18n.getLocalizedString("AccountNo");
		frmIBAccntSummary.lblUseful5.text = kony.i18n.getLocalizedString("AccountName");
		frmIBAccntSummary.lblUseful7.text = kony.i18n.getLocalizedString("BranchName");
		frmIBAccntSummary.lblUseful8.text = kony.i18n.getLocalizedString("LedgerBalance");
		frmIBAccntSummary.lblUseful9.text = kony.i18n.getLocalizedString("LinkedAcc");
		frmIBAccntSummary.lblUseful2.text = kony.i18n.getLocalizedString("tenor");
		frmIBAccntSummary.lblUseful6.text = kony.i18n.getLocalizedString("Status");
		//IBdepositAccInqCallService();
		IBcallTimeDepositDetailService();
	} else if (glb_viewName == "PRODUCT_CODE_LOAN_CASH2GO") {
		frmIBAccntSummary.lnkMyActivities.setVisibility(false);
		frmIBAccntFullStatement.button589518614375454.setVisibility(false);	
		frmIBAccntSummary.lblDepositDetails.setVisibility(false);
		frmIBAccntSummary.hbxSegHeader.setVisibility(false);
		frmIBAccntSummary.hboxApplySendToSave.setVisibility(false);
		frmIBAccntSummary.lblUseful1.text = kony.i18n.getLocalizedString("ProductName");
		frmIBAccntSummary.lblUseful2.text = kony.i18n.getLocalizedString("AccountNo");
		frmIBAccntSummary.lblUseful3.text = kony.i18n.getLocalizedString("SuffixNo");
		frmIBAccntSummary.lblUseful4.text = kony.i18n.getLocalizedString("AccountName");
		frmIBAccntSummary.lblUseful5.text = kony.i18n.getLocalizedString("PayDueDate");
		frmIBAccntSummary.lblUseful6.text = kony.i18n.getLocalizedString("MonthlyInstallement");
		frmIBAccntSummary.lblUseful9.text = kony.i18n.getLocalizedString("DirectCredit");
		IBloanAccountCallService(IBcash2GoAccountCallBck);
	} else if (glb_viewName == "PRODUCT_CODE_LOAN_HOMELOAN") {
		frmIBAccntSummary.lnkMyActivities.setVisibility(false);
		frmIBAccntFullStatement.button589518614375454.setVisibility(false);
		frmIBAccntSummary.lblUseful1.text = kony.i18n.getLocalizedString("ProductName");
		frmIBAccntSummary.lblUseful2.text = kony.i18n.getLocalizedString("AccountNo");
		frmIBAccntSummary.lblUseful3.text = kony.i18n.getLocalizedString("SuffixNo");
		frmIBAccntSummary.lblUseful4.text = kony.i18n.getLocalizedString("AccountName");
		frmIBAccntSummary.lblUseful5.text = kony.i18n.getLocalizedString("PayDueDate");
		frmIBAccntSummary.lblUseful6.text = kony.i18n.getLocalizedString("MonthlyInstallement");
		frmIBAccntSummary.lblUseful7.text = kony.i18n.getLocalizedString("DirectCredit");
		frmIBAccntSummary.lblDateHeader.text = kony.i18n.getLocalizedString("keyLoanLimit");
		frmIBAccntSummary.lblHeaderBalance.text = kony.i18n.getLocalizedString("keyHomeLoanExpiryDate");
		frmIBAccntSummary.lblDepositDetails.text = kony.i18n.getLocalizedString("interestRateDetail");
		IBloanAccountCallService(IBhomeLoanAccountCallBck);
		
	} else if (glb_viewName == "PRODUCT_CODE_READYCASH_TABLE") {		
		frmIBAccntSummary.lnkMyActivities.setVisibility(false);
		frmIBAccntFullStatement.button589518614375454.setVisibility(false);
		frmIBAccntSummary.lblUseful2.text = kony.i18n.getLocalizedString("CardNo");
		frmIBAccntSummary.lblUseful3.text = kony.i18n.getLocalizedString("CardHolderName");
		frmIBAccntSummary.lblUseful4.text = kony.i18n.getLocalizedString("creditLimit");
		frmIBAccntSummary.lblUseful5.text = kony.i18n.getLocalizedString("StatDate");
		frmIBAccntSummary.lblUseful7.text = kony.i18n.getLocalizedString("TotalAmtDue");
		frmIBAccntSummary.lblUseful8.text = kony.i18n.getLocalizedString("MinAmtDue");
		frmIBAccntSummary.lblUseful1.text = kony.i18n.getLocalizedString("CardType");
		frmIBAccntSummary.lblUseful6.text = kony.i18n.getLocalizedString("PayDueDate");
		frmIBAccntSummary.lblUseful9.text = kony.i18n.getLocalizedString("DirectCredit");
		IBcreditCardCallService();
	}
	
	}
	swapSelectedIBAcctSkin();  //MIB-1158
}

var oldIBAcctSelectedInd  = 0;
var currIBAcctSelectedInd = 0;

function swapSelectedIBAcctSkin(){  
	var j = 0;
	var liLenght = document.getElementsByTagName("li").length; 
	for (var i=0;i < liLenght; i++) {  
		var tmp = document.getElementsByTagName("li")[i];  
		if(undefined != tmp && undefined != tmp.parentNode){
			if(tmp.parentNode.parentNode.id.indexOf("frmIBPostLoginDashboard") != -1){
				if(tmp.childNodes[0].id == "segAccountDetailsbox_segAccountDetailsbox"){break;}
				if(tmp.childNodes[0].id != "segAccountDetailsbox_segAccountDetailsbox"){	
					if(tmp.childNodes[0].id.indexOf("progressdiv") == -1){
						j += 1;
					}
				}				
			}
			if(tmp.parentNode.parentNode.id.indexOf("frmIBAccntSummary") != -1){
				if(tmp.childNodes[0].id == "hbox5036590321206468_hbox5036590321206468"){break;}
				if(tmp.childNodes[0].id != "hbox5036590321206468_hbox5036590321206468"){	
					if(tmp.childNodes[0].id.indexOf("progressdiv") == -1){
						j +=1; 
					}
				}					
			}								
		}
	} 	
	if(j == 0){
		swapSelectedIBAcctSkinSingle()
	} else {
		swapSelectedIBAcctSkinMultiple(j);
	}
}

function swapSelectedIBAcctSkinSingle(){  
	currIBAcctSelectedInd = parseInt(gblIndex);
	var tempOld = document.getElementsByTagName("li")[oldIBAcctSelectedInd];	
	if(undefined != tempOld && undefined != tempOld.parentNode){
		if(tempOld.parentNode.parentNode.id == "frmIBPostLoginDashboard_segAccountDetails" || tempOld.parentNode.parentNode.id == "frmIBAccntSummary_segAccountDetails"){ 		
			var o = tempOld.childNodes;
			for(var i = 0; i < o.length; i++){
				if(o[i].id == "hbox5036590321206468_hbox5036590321206468" || o[i].id == "segAccountDetailsbox_segAccountDetailsbox"){ 
					removeBGColor(o[i]); removeBGImage(o[i]);				
					removeClass(o[i],"segSelectedRow");	removeClass(o[i],"hbxAcctSummarySegItemFocus");	
					if(tempOld.parentNode.parentNode.id == "frmIBPostLoginDashboard_segAccountDetails"){ 		
						tempOld.parentNode.parentNode.setAttribute("class","seg2Normal");				
					}else if(tempOld.parentNode.parentNode.id == "frmIBAccntSummary_segAccountDetails"){ 		
						tempOld.parentNode.parentNode.setAttribute("class","sknsegm");
						if(!hasClass(o[i],"hbxAcctSummarySeg")){
							addClass(o[i],"hbxAcctSummarySeg");
						}
					}				
					o[i].parentNode.onmouseover = function() { 
						if(oldIBAcctSelectedInd != currIBAcctSelectedInd){
							addClass(o[i],"hbxAcctSummarySegItemFocus");
						} 
					}
				}
			}
		}
	}
    var tempCurr = document.getElementsByTagName("li")[currIBAcctSelectedInd];
    if(undefined != tempCurr && undefined != tempCurr.parentNode){
		if(tempCurr.parentNode.parentNode.id == "frmIBPostLoginDashboard_segAccountDetails" || tempCurr.parentNode.parentNode.id == "frmIBAccntSummary_segAccountDetails"){ 
			var c = tempCurr.childNodes;
			for(var i = 0; i < c.length; i++){  
				if(c[i].id == "hbox5036590321206468_hbox5036590321206468" || c[i].id == "segAccountDetailsbox_segAccountDetailsbox"){ 
					c[i].parentNode.onmouseover = function() { 
						removeClass(this ,"hbxAcctSummarySegItemFocus");
						removeClass(this.childNodes[0] ,"hbxAcctSummarySegItemFocus");				  				  
						this.childNodes[0].style.backgroundColor = '#e1eff8'; 
						this.childNodes[0].style.backgroundImage = "url(./desktopweb/images/bg_arrow_right_grayb2.png)";
						if(oldIBAcctSelectedInd != currIBAcctSelectedInd){		
							addClass(this,"segSelectedRow");	
							addClass(this.childNodes[0],"segSelectedRow");	
						} 
					}			
					if(hasClass(c[i],"segSelectedRow")){ 
					  removeClass(c[i],"segSelectedRow");	
					}else{ 
					  addClass(c[i],"segSelectedRow");				
					}
				}	
			}
		}
	}	
	oldIBAcctSelectedInd = currIBAcctSelectedInd;
}

function swapSelectedIBAcctSkinMultiple(j){  
	currIBAcctSelectedInd = parseInt(gblIndex);	
	//var k = j;  
	currIBAcctSelectedInd += j;
	oldIBAcctSelectedInd  += j;
	var tempOld = document.getElementsByTagName("li")[oldIBAcctSelectedInd];	
	if(undefined != tempOld && undefined != tempOld.parentNode){
		if(tempOld.parentNode.parentNode.id == "frmIBPostLoginDashboard_segAccountDetails" || tempOld.parentNode.parentNode.id == "frmIBAccntSummary_segAccountDetails"){ 		
			var o = tempOld.childNodes;
			for(var i = 0; i < o.length; i++){
				if(o[i].id == "hbox5036590321206468_hbox5036590321206468" || o[i].id == "segAccountDetailsbox_segAccountDetailsbox"){ 
					removeBGColor(o[i]); removeBGImage(o[i]);				
					removeClass(o[i],"segSelectedRow");	removeClass(o[i],"hbxAcctSummarySegItemFocus");	
					if(tempOld.parentNode.parentNode.id == "frmIBPostLoginDashboard_segAccountDetails"){ 		
						tempOld.parentNode.parentNode.setAttribute("class","seg2Normal");				
					}else if(tempOld.parentNode.parentNode.id == "frmIBAccntSummary_segAccountDetails"){ 		
						tempOld.parentNode.parentNode.setAttribute("class","sknsegm");
						if(!hasClass(o[i],"hbxAcctSummarySeg")){
							addClass(o[i],"hbxAcctSummarySeg");
						}
					}				
					o[i].parentNode.onmouseover = function() { 
						if(oldIBAcctSelectedInd != currIBAcctSelectedInd){
							addClass(o[i],"hbxAcctSummarySegItemFocus");
						} 
					}
				}
			}
		}
	}
    var tempCurr = document.getElementsByTagName("li")[currIBAcctSelectedInd];
    if(undefined != tempCurr && undefined != tempCurr.parentNode){
		if(tempCurr.parentNode.parentNode.id == "frmIBPostLoginDashboard_segAccountDetails" || tempCurr.parentNode.parentNode.id == "frmIBAccntSummary_segAccountDetails"){ 
			var c = tempCurr.childNodes;
			for(var i = 0; i < c.length; i++){  
				if(c[i].id == "hbox5036590321206468_hbox5036590321206468" || c[i].id == "segAccountDetailsbox_segAccountDetailsbox"){ 
					c[i].parentNode.onmouseover = function() { 
						removeClass(this ,"hbxAcctSummarySegItemFocus");
						removeClass(this.childNodes[0] ,"hbxAcctSummarySegItemFocus");				  				  
						this.childNodes[0].style.backgroundColor = '#e1eff8'; 
						this.childNodes[0].style.backgroundImage = "url(./desktopweb/images/bg_arrow_right_grayb2.png)";
						if(oldIBAcctSelectedInd != currIBAcctSelectedInd){		
							addClass(this,"segSelectedRow");	
							addClass(this.childNodes[0],"segSelectedRow");	
						} 
					}			
					if(hasClass(c[i],"segSelectedRow")){ 
					  removeClass(c[i],"segSelectedRow");	
					}else{ 
					  addClass(c[i],"segSelectedRow");				
					}
				}	
			}
		}
	}	

	oldIBAcctSelectedInd = currIBAcctSelectedInd-j;
}

/*
 *************** First Handle ***************
 */

function IBdreamSavingCallService() {
	
	var inputparam = {};
	//inputparam["crmId"] = gblAccountTable["custAcctRec"][gblIndex]["crmId"];
	//inputparam["bankCD"] = kony.i18n.getLocalizedString("bankCode");
	inputparam["personalizedAcctId"] = gblAccountTable["custAcctRec"][gblIndex]["persionlizeAcctId"];
	//inputparam["rqUUId"] = "";
	showLoadingScreenPopup();
	var status = invokeServiceSecureAsync("crmAccountEnquiry", inputparam, IBdreamSavingCallBack);
}

function IBdreamSavingCallBack(status, resulttable) {
	
	if (status == 400) {
		
		if (resulttable["opstatus"] == 0) {
			//code to map data in form
			if (resulttable["crmAccountRec"].length > 0 && null != resulttable["crmAccountRec"][0]["dreamTargetAmount"]) {
				frmIBAccntSummary.lblUsefulValue2.text = (resulttable["crmAccountRec"][0]["dreamDesc"] != null) ? resulttable["crmAccountRec"][0]["dreamDesc"] : "";
				frmIBAccntSummary.lblUsefulValue3.text = commaFormatted(resulttable["crmAccountRec"][0]["dreamTargetAmount"])+" "+kony.i18n.getLocalizedString(
					"currencyThaiBaht");
			}
			IBdepositAccountInqCallService();
		} else {
			alert(" " + resulttable["errMsg"]);
			dismissLoadingScreenPopup();
		}
	}
}

function IBdepositAccountInqCallService() {
	var inputparam = {};
	inputparam["acctId"] =gblAccountTable["custAcctRec"][gblIndex]["accId"];
	var status = invokeServiceSecureAsync("depositAccountInquiry", inputparam, IBdepositAccountInqCallBack);
}

function IBdepositAccountInqCallBack(status, resulttable) {
	
	if (status == 400) {
		
		if (resulttable["opstatus"] == 0) {
			if (resulttable["bankAccountStatus"] != null && resulttable["bankAccountStatus"] != "") {
				var status=resulttable["bankAccountStatus"].split("|");
				if(kony.i18n.getCurrentLocale() == "th_TH")
				frmIBAccntSummary.lblUsefulValue9.text = status[1];
				else
				frmIBAccntSummary.lblUsefulValue9.text = status[0];
				frmIBAccntSummary.hbxUseful9.isVisible=true;
			}
			if (resulttable["accountTitle"] != null && resulttable["accountTitle"] != "") {
				frmIBAccntSummary.lblUsefulValue8.text = resulttable["accountTitle"];
				frmIBAccntSummary.hbxUseful8.isVisible=true;
			}
			var availableBal;
			var ledgerBal;
			if (resulttable["AcctBal"].length > 0) {
				for (var i = 0; i < resulttable["AcctBal"].length; i++) {
					if (resulttable["AcctBal"][i]["BalType"] == "Avail") {
						availableBal = resulttable["AcctBal"][i]["Amt"];
					}
					if (resulttable["AcctBal"][i]["BalType"] == "Ledger") {
						ledgerBal = resulttable["AcctBal"][i]["Amt"];
					}
				}
			}
			if (null != availableBal) {
				frmIBAccntSummary.lblAccountBalanceHeader.text = commaFormatted(availableBal) + " " + kony.i18n.getLocalizedString(
					"currencyThaiBaht");
			}
			if (null != ledgerBal) {
				frmIBAccntSummary.lblUsefulValue11.text = commaFormatted(ledgerBal) + " " + kony.i18n.getLocalizedString(
					"currencyThaiBaht");
					frmIBAccntSummary.hbxUseful11.isVisible=true;
			}
			/*if (resulttable["linkedacc"] != null && resulttable["linkedacc"] != "") {
				var linkAccount = resulttable["linkedacc"].substring(20);
				frmIBAccntSummary.lblUsefulValue13.text = formatAccountNo(linkAccount);	
				frmIBAccntSummary.hbxUseful13.isVisible=true;			
				IBrexferInqCallService(linkAccount);
			}*/
			if (resulttable["linkedacc"].length == 0) {
				
				frmIBAccntSummary.hbxUseful4.isVisible = false;
				frmIBAccntSummary.hbxUseful5.isVisible = false;
				frmIBAccntSummary.hbxUseful12.isVisible=false;	
			} else {
			    var linkAccount;
			    var lenlinkAccount=resulttable["linkedacc"].length;
			    if(lenlinkAccount==30)
			    linkAccount=resulttable["linkedacc"].substring(20);
			    else
			    linkAccount=resulttable["linkedacc"].substring(16);
			    var accType=resulttable["linkedAccType"];
				frmIBAccntSummary.lblUsefulValue12.text = formatAccountNo(linkAccount);
				frmIBAccntSummary.hbxUseful12.isVisible=true;	
				frmIBAccntSummary.hbxUseful4.isVisible = true;
				frmIBAccntSummary.hbxUseful5.isVisible = true;
				IBrexferInqCallService(linkAccount,accType)
			}
			IBdreamSavingAccount();
			
		} else {
			alert(" " + resulttable["errMsg"]);
			
		}
	}
}

function IBrexferInqCallService(linkAccount,accType) {
	
	var inputparam = {};
	/*inputparam["accId"] = frmAccont;
	inputparam["acctType"] = gblAccountTable["custAcctRec"][gblIndex]["accType"];
	inputparam["rqUUId"] = "";
	inputparam["clientDt"] = "";*/
	
	 if (accType == "SDA") {
        inputparam["spName"] = "com.fnf.xes.ST"; //14digits
    }
    if (accType == "DDA") {
        inputparam["spName"] = "com.fnf.xes.IM"; //14digits
        
    }
	inputparam["acctId"] = linkAccount;
    inputparam["acctType"] = accType;
    
    inputparam["clientDt"] = "";
    inputparam["name"] = "IB-INQ";
	var status = invokeServiceSecureAsync("RecurringFundTransinq", inputparam, IBrexferInqCallBack);
	
}

function IBrexferInqCallBack(status, resulttable) {
	
	if (status == 400) {
		
		if (resulttable["opstatus"] == 0) {
			if (resulttable["monInstAmt"] != "") {
				frmIBAccntSummary.lblUsefulValue4.text = commaFormatted(resulttable["monInstAmt"])+" "+kony.i18n.getLocalizedString(
					"currencyThaiBaht");
			}
			 if (resulttable["frequency"] != null && resulttable["frequency"] != "" && resulttable["frequency"] =="EndOfMonth") {
          
            frmIBAccntSummary.lblUsefulValue5.text  = "31" + "st";
            mnthlyTransferDate = "31";
          }
			if (resulttable["monIntDate"] != null && resulttable["monIntDate"] != "") {
                mnthlyTransferDate = resulttable["monIntDate"];
                if (mnthlyTransferDate == 1 || mnthlyTransferDate == 1 == 21) {
                    frmIBAccntSummary.lblUsefulValue5.text = mnthlyTransferDate + "st";
                } else if (mnthlyTransferDate == 2 || mnthlyTransferDate == 22) {
                    frmIBAccntSummary.lblUsefulValue5.text = mnthlyTransferDate + "nd";
                } else if (mnthlyTransferDate == 3 || mnthlyTransferDate == 23) {
                    frmIBAccntSummary.lblUsefulValue5.text = mnthlyTransferDate + "rd";
                } else {
                    frmIBAccntSummary.lblUsefulValue5.text = mnthlyTransferDate + "th";
                }

            }
			
		} else {
			alert(" " + resulttable["errMsg"]);
			kony.application.dismissLoadingScreen();
		}
	}
}

function IBdreamSavingAccount() {
	
	//frmIBAccntSummary.btnPayBill.onClick = IBmoveToTransfers			---- leads to Paybill button click landing on transfers page
	//frmIBAccntSummary.lblAccountNameHeader.text = curr_form.segAccountDetails.selectedItems[0].lblAccountName
	//frmIBAccntSummary.imgAccountDetailsPic.src = "prod_savingd.png";
	frmIBAccntSummary.lblLinkedDebit.text = "";
	frmIBAccntSummary.lblLinkedDebit.text = "";
	frmIBAccntSummary.segMaturityDisplay.setVisibility(false);
	frmIBAccntSummary.segDebitCard.setVisibility(false);
	var locale = kony.i18n.getCurrentLocale();
	var branchName;
	var accType;
	if (locale == "en_US") {
		productName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"];
		branchName = gblAccountTable["custAcctRec"][gblIndex]["BranchNameEh"];
		accNickName= gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"]+" "+sbStr;
		accType=gblAccountTable["custAcctRec"][gblIndex]["Type_EN"];
	} else {
		productName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"];
		branchName = gblAccountTable["custAcctRec"][gblIndex]["BranchNameTh"];
		accNickName=gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"]+" "+sbStr;
		accType=gblAccountTable["custAcctRec"][gblIndex]["Type_TH"];
	}
	if(null !=gblAccountTable["custAcctRec"][gblIndex]["acctNickName"]){
		frmIBAccntSummary.lblAccountNameHeader.text =gblAccountTable["custAcctRec"][gblIndex]["acctNickName"];
	}
	else{
		 frmIBAccntSummary.lblAccountNameHeader.text =accNickName;
	}
	frmIBAccntSummary.lblUsefulValue1.text = productName;
	frmIBAccntSummary.lblUsefulValue6.text = accType;
	frmIBAccntSummary.lblUsefulValue7.text = gblAccountTable["custAcctRec"][gblIndex]["accountNoFomatted"]
	/*var payBill=gblAccountTable["custAcctRec"][gblIndex]["Paybill"];
    var topup=gblAccountTable["custAcctRec"][gblIndex]["Topup"];
    var payMyCredit=gblAccountTable["custAcctRec"][gblIndex]["PayMyCreditCard"];
    var transfer=gblAccountTable["custAcctRec"][gblIndex]["Transfer"];
    var payMyLoan=gblAccountTable["custAcctRec"][gblIndex]["PayMyLoan"];
    
    if(payBill=="Y")
    frmIBAccntSummary.btnPayBill.isVisible = true;
     if(topup=="Y")
    frmIBAccntSummary.BtnTopUp.isVisible = true;
     if(transfer=="Y")
    frmIBAccntSummary.btnTransfer.isVisible = true;
     if(payMyCredit=="Y")
    frmIBAccntSummary.payMyCredit.isVisible = true;
     if(payMyLoan=="Y")
    frmIBAccntSummary.payMyLoan.isVisible = true;
    if(frmIBAccntSummary.payMyCredit.isVisible == true || frmIBAccntSummary.payMyLoan.isVisible == true){
    
	   if(payMyCredit=="Y"){
	      frmIBAccntSummary.payMyCredit.containerWeight = 50;
	      frmIBAccntSummary.payMyCredit.widgetAlignment = constants.WIDGET_ALIGN_CENTER;
	      }
	      if(payMyLoan=="Y"){
	      frmIBAccntSummary.payMyLoan.containerWeight = 50;
	      frmIBAccntSummary.payMyLoan.widgetAlignment = constants.WIDGET_ALIGN_CENTER;	      
	      }
	}
	else if(frmIBAccntSummary.payMyCredit.isVisible == false && frmIBAccntSummary.payMyLoan.isVisible == false && frmIBAccntSummary.btnTransfer.isVisible ==true && frmIBAccntSummary.BtnTopUp.isVisible == false && frmIBAccntSummary.btnPayBill.isVisible == false){
	    frmIBAccntSummary.btnTransfer.containerWeight = 60;
		frmIBAccntSummary.payMyLoan.containerWeight=0;
        frmIBAccntSummary.payMyCredit.containerWeight=0;
		frmIBAccntSummary.btnPayBill.containerWeight=0;
		frmIBAccntSummary.BtnTopUp.containerWeight=0;
	}*/
	frmIBAccntSummary.hbxUseful7.isVisible=true;
	frmIBAccntSummary.lblUsefulValue10.text = branchName;
	frmIBAccntSummary.hbxUseful10.isVisible=true;
	frmIBAccntSummary.hbxUseful13.isVisible=false;
	dismissLoadingScreenPopup();
	frmIBAccntSummary.show();		
	OTPcheckOnAccountSummary();
	
}
/*
 *************** Second Handle ***************
 */

function IBIntersetRateInqCallService(){
	var inputparam = {};
	var status = invokeServiceSecureAsync("interestRateInquiry", inputparam, IBIntersetRateInqCallBackService);
}

function IBIntersetRateInqCallBackService(status,resulttable){
	//if (status == 400) {
	//	if (resulttable["opstatus"] == 0) {
		 frmIBAccntSummary.lblUsefulValue10.text = resulttable["INTERESTRATE"] + "%";
	//	}
	//}
}
function IBdepositAccInqCallServiceForNoFixed() {
	showLoadingScreenPopup();
    
	var inputparam = {};
	inputparam["acctId"] = gblAccountTable["custAcctRec"][gblIndex][
		"accId"];
	
	var status = invokeServiceSecureAsync("depositAccountInquiry", inputparam, IBdepositAccInqCallBackNoFixed);
}

function IBdepositAccInqCallBackNoFixed(status, resulttable) {
	if (status == 400) {
		
		
		if (resulttable["opstatus"] == 0) {
			var availableBal = "";
			var ledgerBal = "";
          frmIBAccntSummary.lblUsefulValue10.text = gblInterestRateAcc;
			frmIBAccntSummary.lblUsefulValue11.text = commaFormatted(resulttable["intrestAmount"]) + kony.i18n.getLocalizedString(
				"currencyThaiBaht");
		    frmIBAccntSummary.hbxUseful10.isVisible=true;		
			frmIBAccntSummary.lblUsefulValue6.text = resulttable["accountTitle"];
			var status=resulttable["bankAccountStatus"].split("|");
			if(kony.i18n.getCurrentLocale() == "th_TH")
			frmIBAccntSummary.lblUsefulValue7.text = status[1];
			else
			frmIBAccntSummary.lblUsefulValue7.text = status[0];
			 frmIBAccntSummary.hbxUseful7.isVisible=true;
			if (resulttable["AcctBal"].length > 0) {
				for (var i = 0; i < resulttable["AcctBal"].length; i++) {
					
					if (resulttable["AcctBal"][i]["BalType"] == "Avail") {
						availableBal = resulttable["AcctBal"][i]["Amt"];
					}
					if (resulttable["AcctBal"][i]["BalType"] == "Ledger") {
						ledgerBal = resulttable["AcctBal"][i]["Amt"];
					}
				}
			}
			if (null != ledgerBal) {
				frmIBAccntSummary.lblUsefulValue9.text = commaFormatted(ledgerBal) + kony.i18n.getLocalizedString(
					"currencyThaiBaht");
				 frmIBAccntSummary.hbxUseful9.isVisible=true;	
			}
			if (null != availableBal) {
				frmIBAccntSummary.lblAccountBalanceHeader.text = commaFormatted(availableBal) + kony.i18n.getLocalizedString(
					"currencyThaiBaht");
			}
			dismissLoadingScreenPopup();
			//IBcallLiquidityInqService();		:#  Commented as it is out of scope after sendtosave removal.
			frmIBAccntSummary.hbxUseful2.isVisible = false;
			frmIBAccntSummary.hbxUseful3.isVisible = false;
			frmIBAccntSummary.hbxUseful12.isVisible = false;
			IBnoFixedAccount();
		} else {
			//alert(" " + resulttable["errMsg"]);
			dismissLoadingScreenPopup();
		}
	}
  	IBIntersetRateInqCallService();
}

function IBnoFixedAccount() {
	//frmIBAccntSummary.btnPayBill.onClick = IBmoveToTransfers
	//frmIBAccntSummary.lblAccountNameHeader.text = curr_form.segAccountDetails.selectedItems[0].lblAccountName
	//frmIBAccntSummary.imgAccountDetailsPic.src = "prod_savingd.png";
	frmIBAccntSummary.lblUseful12.text = "";
	frmIBAccntSummary.lblUseful13.text = "";
	frmIBAccntSummary.lblUseful14.text = "";
	frmIBAccntSummary.lblUseful15.text = "";	
	frmIBAccntSummary.lblUsefulValue14.text = "";
	frmIBAccntSummary.lblUsefulValue15.text = "";
	frmIBAccntSummary.lblLinkedDebit.text = "";
	frmIBAccntSummary.hbxUseful12.isVisible=false;
	frmIBAccntSummary.hbxUseful13.isVisible=false;
	frmIBAccntSummary.hbxUseful14.isVisible=false;
	frmIBAccntSummary.hbxUseful15.isVisible=false;		
	frmIBAccntSummary.lblLinkedDebit.text = "";
	frmIBAccntSummary.segDebitCard.setVisibility(false);
	frmIBAccntSummary.segMaturityDisplay.setVisibility(false);
	var productName;
	var branchName;
	var accType;
	var locale = kony.i18n.getCurrentLocale();
	if (locale == "en_US") {
		productName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"];
		branchName = gblAccountTable["custAcctRec"][gblIndex]["BranchNameEh"];
		accNickName= gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"]+" "+sbStr;
		accType=gblAccountTable["custAcctRec"][gblIndex]["Type_EN"];
	} else {
		productName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"];
		branchName = gblAccountTable["custAcctRec"][gblIndex]["BranchNameTh"];
		accNickName= gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"]+" "+sbStr;
		accType=gblAccountTable["custAcctRec"][gblIndex]["Type_TH"];
	}
	if(null !=gblAccountTable["custAcctRec"][gblIndex]["acctNickName"]){
		frmIBAccntSummary.lblAccountNameHeader.text =gblAccountTable["custAcctRec"][gblIndex]["acctNickName"];
	}
	else{
		frmIBAccntSummary.lblAccountNameHeader.text =accNickName;
	}
	frmIBAccntSummary.lblUsefulValue1.text = productName;
	frmIBAccntSummary.lblUsefulValue4.text = accType;
	frmIBAccntSummary.lblUsefulValue5.text = gblAccountTable["custAcctRec"][gblIndex]["accountNoFomatted"]
	frmIBAccntSummary.lblUsefulValue8.text = branchName;
	frmIBAccntSummary.hbxUseful8.isVisible=true;
	OTPcheckOnAccountSummary();
	frmIBAccntSummary.show();
}
/*
 *************** Third Handle ***************
 */

function IBnoFeeSavingAccount() {
	//frmIBAccntSummary.btnTransfer.onClick = IBmoveToTransfers()
	//frmIBAccntSummary.btnPayBill.onClick = IBmoveToBillPayment()
	//frmIBAccntSummary.BtnTopUp.onClick = IBmovetoTopUp
	OTPcheckOnAccountSummary();
	//curr_form=kony.application.getCurrentForm();
	//frmIBAccntSummary.imgAccountDetailsPic.src = "prod_nofee.png";
	frmIBAccntSummary.lblUseful9.text = "";
	frmIBAccntSummary.lblUseful10.text = "";
	frmIBAccntSummary.lblUseful11.text = "";
	frmIBAccntSummary.lblUseful12.text = "";
	frmIBAccntSummary.lblUseful13.text = "";
	frmIBAccntSummary.lblUseful14.text = "";
	frmIBAccntSummary.lblUseful15.text = "";	
	frmIBAccntSummary.lblUsefulValue14.text = "";
	frmIBAccntSummary.lblUsefulValue15.text = "";		
	frmIBAccntSummary.hbxUseful9.isVisible=false;
	frmIBAccntSummary.hbxUseful10.isVisible=false;
	frmIBAccntSummary.hbxUseful11.isVisible=false;
	frmIBAccntSummary.hbxUseful12.isVisible=false;
	frmIBAccntSummary.hbxUseful13.isVisible=false;
	frmIBAccntSummary.hbxUseful14.isVisible=false;
	frmIBAccntSummary.hbxUseful15.isVisible=false;		
	//frmIBAccntSummary.lblLinkedDebit.text = kony.i18n.getLocalizedString("LinkedTo");
	frmIBAccntSummary.segMaturityDisplay.setVisibility(false);
	frmIBAccntSummary.segDebitCard.setVisibility(true);
	var locale = kony.i18n.getCurrentLocale();
	var accType;
	var branchName;
	if (locale == "en_US") {
		productName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"];
		branchName = gblAccountTable["custAcctRec"][gblIndex]["BranchNameEh"];
		accNickName= gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"]+" "+sbStr;
		accType=gblAccountTable["custAcctRec"][gblIndex]["Type_EN"];
	} else {
		branchName = gblAccountTable["custAcctRec"][gblIndex]["BranchNameTh"];
		productName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"];
		accNickName= gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"]+" "+sbStr;
		accType=gblAccountTable["custAcctRec"][gblIndex]["Type_TH"];
	}
	if(null !=gblAccountTable["custAcctRec"][gblIndex]["acctNickName"]){
		frmIBAccntSummary.lblAccountNameHeader.text = gblAccountTable["custAcctRec"][gblIndex]["acctNickName"];
	}else{
		frmIBAccntSummary.lblAccountNameHeader.text = accNickName;
	}	
	frmIBAccntSummary.lblUsefulValue1.text = productName;
	frmIBAccntSummary.lblUsefulValue2.text = gblAccountTable["custAcctRec"][gblIndex]["remainingFee"];
	frmIBAccntSummary.lblUsefulValue3.text = accType;
	frmIBAccntSummary.lblUsefulValue4.text = gblAccountTable["custAcctRec"][gblIndex]["accountNoFomatted"]
	frmIBAccntSummary.lblUsefulValue7.text = branchName;
	frmIBAccntSummary.hbxUseful7.isVisible=true;
}
/*
 *************** Fourth Handle ***************
 */

function IBsimpleAccountFunction() {
	//callDepositAccountInquiryService();
	//frmIBAccntSummary.btnTransfer.onClick = IBmoveToTransfers
	//frmIBAccntSummary.btnPayBill.onClick = IBmoveToBillPayment
	//frmIBAccntSummary.BtnTopUp.onClick = IBmovetoTopUp
	OTPcheckOnAccountSummary();
	var locale = kony.i18n.getCurrentLocale();
	var branchName;
	var accType;
	if (locale == "en_US") {
		branchName = gblAccountTable["custAcctRec"][gblIndex]["BranchNameEh"];
		productName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"];
		accNickName= gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"]+" "+sbStr;
		accType=gblAccountTable["custAcctRec"][gblIndex]["Type_EN"];
	} else {
		branchName = gblAccountTable["custAcctRec"][gblIndex]["BranchNameTh"];
		productName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"];
		accNickName= gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"]+" "+sbStr;
		accType=gblAccountTable["custAcctRec"][gblIndex]["Type_TH"];
	}
	if(null !=gblAccountTable["custAcctRec"][gblIndex]["acctNickName"]){
		frmIBAccntSummary.lblAccountNameHeader.text = gblAccountTable["custAcctRec"][gblIndex]["acctNickName"];
	}else{
		frmIBAccntSummary.lblAccountNameHeader.text = accNickName;
	}
	/*if (glb_viewName == "PRODUCT_CODE_CURRENT_TABLE" || glb_viewName == "PRODUCT_CODE_SAVING_TABLE")
		frmIBAccntSummary.imgAccountDetailsPic.src = "prod_currentac.png";
	else
	if (glb_viewName == "PRODUCT_CODE_NEWREADYCASH_TABLE")
		frmIBAccntSummary.imgAccountDetailsPic.src = "prod_cash2go.png";
	else
		frmIBAccntSummary.imgAccountDetailsPic.src = "prod_savingd.png";*/
	frmIBAccntSummary.lblUseful8.text = "";
	frmIBAccntSummary.lblUseful9.text = "";
	frmIBAccntSummary.lblUseful10.text = "";
	frmIBAccntSummary.lblUseful11.text = "";
	frmIBAccntSummary.lblUseful12.text = "";
	frmIBAccntSummary.lblUseful13.text = "";
	frmIBAccntSummary.lblUseful14.text = "";
	frmIBAccntSummary.lblUseful15.text = "";	
	frmIBAccntSummary.lblUsefulValue14.text = "";
	frmIBAccntSummary.lblUsefulValue15.text = "";		
	frmIBAccntSummary.hbxUseful8.isVisible=false;
	frmIBAccntSummary.hbxUseful9.isVisible=false;
	frmIBAccntSummary.hbxUseful10.isVisible=false;
	frmIBAccntSummary.hbxUseful11.isVisible=false;
	frmIBAccntSummary.hbxUseful12.isVisible=false;
	frmIBAccntSummary.hbxUseful13.isVisible=false;
	frmIBAccntSummary.hbxUseful14.isVisible=false;
	frmIBAccntSummary.hbxUseful15.isVisible=false;		
	//frmIBAccntSummary.lblLinkedDebit.text = kony.i18n.getLocalizedString("LinkedTo");
	frmIBAccntSummary.segMaturityDisplay.setVisibility(false);
	frmIBAccntSummary.segDebitCard.setVisibility(true);
	frmIBAccntSummary.lblUsefulValue1.text = productName;
	if (gblAccountTable["custAcctRec"][gblIndex]["accType"] == kony.i18n.getLocalizedString("Current"))
		frmIBAccntSummary.lblUsefulValue2.text = accType;
	else
		frmIBAccntSummary.lblUsefulValue2.text = accType;
	frmIBAccntSummary.lblUsefulValue3.text = gblAccountTable["custAcctRec"][gblIndex]["accountNoFomatted"];
	frmIBAccntSummary.lblUsefulValue6.text = branchName;
	frmIBAccntSummary.lblUsefulValue8.text = "";
	frmIBAccntSummary.lblUsefulValue9.text = "";
	frmIBAccntSummary.lblUsefulValue10.text = "";
	frmIBAccntSummary.lblUsefulValue11.text = "";
	frmIBAccntSummary.lblUsefulValue12.text = "";
	frmIBAccntSummary.lblUsefulValue13.text = "";
	frmIBAccntSummary.lblUsefulValue14.text = "";
	frmIBAccntSummary.lblUsefulValue15.text = "";
	//frmIBAccntSummary.lblLinkedDebit.text = kony.i18n.getLocalizedString("LinkedTo");
}
/*
 *************** Fifth Handle ***************
 */

function IBcreditCardCallService() {
	//calling creditCardservice for getting 'Total amount due' and 'Minimum amount due
	var inputparam = {};
	
	gblCC_StmtPointAvail = "";
	gblCC_RemainingPoint = "";	
	gblCC_PointExpRemaining = "";

	inputparam["cardId"] = gblAccountTable["custAcctRec"][gblIndex]["accId"];	
	inputparam["tranCode"] = "1";	// as TRANSCODEMIN was made to 2 for ENH_111
	
	showLoadingScreenPopup();
	var status = invokeServiceSecureAsync("creditcardDetailsInq", inputparam, IBcreditCardCallBack);
}

function IBcreditCardCallBack(status, resulttable) {
	if (status == 400) {
		
		if (resulttable["opstatus"] == 0) {
			//if(resulttable["errMsg"]== undefined || resulttable["errMsg"]==""){		-- it seems it is coming always 
			    frmIBAccntSummary.hbxUseful2.isVisible=true;
				if (resulttable["cardNo"] != "" && resulttable["cardNo"] != undefined) {
					frmIBAccntSummary.lblUsefulValue2.text = resulttable["cardNo"];
				}

				if (resulttable["fromAcctName"] != "" && resulttable["fromAcctName"] != undefined) {
					//frmIBAccntSummary.lblUseful3.text = kony.i18n.getLocalizedString("CardHolderName");
					frmIBAccntSummary.lblUsefulValue3.text = resulttable["fromAcctName"];
				}
				if (resulttable["stmtDate"] != "" && resulttable["stmtDate"] != undefined) {
					//frmIBAccntSummary.lblUseful5.text = kony.i18n.getLocalizedString("StatDate");
					frmIBAccntSummary.lblUsefulValue5.text = reformatDate(resulttable["stmtDate"]);
				}
				
				if (frmIBAccntSummary.hbxApplySoGoooD.isVisible && validCicsTranCode(resulttable["cicsTranCode"])) {
					frmIBAccntSummary.hbxApplySoGoooD.setVisibility(true);
				} else {
					frmIBAccntSummary.hbxApplySoGoooD.setVisibility(false);
				}
				
				frmIBAccntSummary.lblUseful1.text = kony.i18n.getLocalizedString("CardType");
				if (resulttable["cardType"] != "" && resulttable["cardType"] != undefined) {
					frmIBAccntSummary.lblUsefulValue1.text = resulttable["cardType"];
				}
				
				if (resulttable["bonusPointAvail"] != "" && resulttable["bonusPointAvail"] != undefined) {
					gblCC_StmtPointAvail = resulttable["bonusPointAvail"];
				}
								
				IBcreditCardCallServiceUnBillAmt();
				IBcreditCardAccount();
			/*}else{
				dismissLoadingScreenPopup();
				alert(resulttable["errMsg"]);
			}*/	
		}
	}
}

function IBcreditCardCallServiceUnBillAmt() {
	var inputparam = {};
	//var toDayDate = getTodaysDate();
	inputparam["cardId"] = gblAccountTable["custAcctRec"][gblIndex]["accId"];
	//inputparam["waiverCode"] = "";
	inputparam["tranCode"] = TRANSCODEUN;
	//inputparam["postedDt"] = toDayDate;
	//inputparam["rqUUId"] = "";
	
	var status = invokeServiceSecureAsync("creditcardDetailsInq", inputparam, IBcreditCardCallBackUn);
}

function IBcreditCardCallBackUn(status, resulttable) {
	if (status == 400) {
		
		if (resulttable["opstatus"] == 0) {
				frmIBAccntSummary.lblAccountBalanceHeader.text = commaFormatted(resulttable["availCrLimit"])  + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
				if (resulttable["dueDate"] != "" && resulttable["dueDate"] != undefined ) {
					frmIBAccntSummary.lblUsefulValue6.text = reformatDate(resulttable["dueDate"]);
				}
				frmIBAccntSummary.lblUsefulValue4.text = commaFormatted(resulttable["totalCrLimit"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
			
			if (resulttable["fullPmtAmt"] != "") {
				//frmIBAccntSummary.lblUseful7.text = kony.i18n.getLocalizedString("TotalAmtDue");
				frmIBAccntSummary.lblUsefulValue7.text = commaFormatted(resulttable["fullPmtAmt"]) + " " + kony.i18n.getLocalizedString(
					"currencyThaiBaht");
				frmIBAccntSummary.hbxUseful7.isVisible=true;	
			}
			if (resulttable["minPmtAmt"] != "") {
				//frmIBAccntSummary.lblUseful8.text = kony.i18n.getLocalizedString("MinAmtDue");
				frmIBAccntSummary.lblUsefulValue8.text = commaFormatted(resulttable["minPmtAmt"]) + " " + kony.i18n.getLocalizedString(
					"currencyThaiBaht");
				frmIBAccntSummary.hbxUseful8.isVisible=true;		
			}

			var allowCardTypeForRedeem = gblAccountTable["custAcctRec"][gblIndex]["allowCardTypeForRedeem"];
			if (allowCardTypeForRedeem == "1" && resulttable["remainingPoint"] != "") {
				frmIBAccntSummary.lblUsefulValue14.text= commaFormattedPoints(resulttable["remainingPoint"]);
				frmIBAccntSummary.hbxUseful14.isVisible=true;		
				gblAccountTable["custAcctRec"][gblIndex]["bonusPointAvail"] = resulttable["remainingPoint"];
				gblCC_RemainingPoint = resulttable["remainingPoint"];
			}			
			
			if (allowCardTypeForRedeem == "1" && resulttable["bonusPointUsed"] != "") {
				if (resulttable["stmtDate"] != "") {
					frmIBAccntSummary.lblUseful15.text = frmIBAccntSummary.lblUseful15.text + " " +reformatDate(resulttable["stmtDate"])+" :";
					gblAccountTable["custAcctRec"][gblIndex]["stmtDate"] = resulttable["stmtDate"];	
				}
				
				if (resulttable["bonusPointUsed"] != "" && gblCC_StmtPointAvail != "" && resulttable["remainingPoint"] != "") {
					gblCC_PointExpRemaining = Number(resulttable["bonusPointUsed"]) - (Number(gblCC_StmtPointAvail) - Number(resulttable["remainingPoint"]));		
					if (gblCC_PointExpRemaining < 0) {
						gblCC_PointExpRemaining = 0;
					}
					gblCC_PointExpRemaining = gblCC_PointExpRemaining + "";
					frmIBAccntSummary.lblUsefulValue15.text= commaFormattedPoints(gblCC_PointExpRemaining);
					frmIBAccntSummary.hbxUseful15.isVisible=true;	
				}			
				gblAccountTable["custAcctRec"][gblIndex]["bonusPointUsed"] = gblCC_PointExpRemaining;
			}

				
			if (resulttable["directDebitAccNo"]== "" || resulttable["directDebitAccNo"] == undefined ) {  
				//frmIBAccntSummary.lblUseful9.text = kony.i18n.getLocalizedString("DirectCredit");
                frmIBAccntSummary.lblUseful9.text = "";
                frmIBAccntSummary.lblUsefulValue9.text = "";
                frmIBAccntSummary.hbxUseful9.isVisible=false;                
                frmIBAccntSummary.hbxUseful14.skin = "hboxWhite";
				frmIBAccntSummary.hbxUseful15.skin = "hboxWhite";  
                frmIBAccntSummary.show();
                dismissLoadingScreenPopup();
			}
			else{
			    frmIBAccntSummary.lblUsefulValue9.text = formatAccountNo(resulttable["directDebitAccNo"]);
			    //frmIBAccntSummary.lblUseful9.setVisibility(true);
                frmIBAccntSummary.hbxUseful9.isVisible=true;	
                frmIBAccntSummary.hbxUseful14.skin = "hboxWhite";
				frmIBAccntSummary.hbxUseful15.skin = "hboxWhite";              
                frmIBAccntSummary.show();
                dismissLoadingScreenPopup();
			}
			
			
		} else {
			alert(" " + resulttable["errMsg"]);
			dismissLoadingScreenPopup();
		}
	}
}

function IBcreditCardAccount() {
	
	//frmIBAccntSummary.btnPayBill.onClick = moveToBillPaymentFromAccountSummaryForCreditCard
	//frmIBAccntSummary.imgAccountDetailsPic.src = "prod_creditcard.png";
	var locale = kony.i18n.getCurrentLocale();
	if (locale == "en_US") {
			productName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"];
			accNickName= gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"]+" "+sbStr
			
	} else {
			productName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"];
			accNickName=gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"]+" "+sbStr
	}
	if(null !=gblAccountTable["custAcctRec"][gblIndex]["acctNickName"]){
		frmIBAccntSummary.lblAccountNameHeader.text = gblAccountTable["custAcctRec"][gblIndex]["acctNickName"];
	}else{
		frmIBAccntSummary.lblAccountNameHeader.text = accNickName;
	}
	frmIBAccntSummary.lblUseful10.text = "";
	frmIBAccntSummary.lblUseful11.text = "";
	frmIBAccntSummary.lblUseful12.text = "";
	frmIBAccntSummary.lblUseful13.text = "";
	frmIBAccntSummary.hbxUseful10.isVisible=false;
	frmIBAccntSummary.hbxUseful11.isVisible=false;
	frmIBAccntSummary.hbxUseful12.isVisible=false;	
	frmIBAccntSummary.hbxUseful13.isVisible=false;
	frmIBAccntSummary.lblLinkedDebit.text = "";
	frmIBAccntSummary.lblLinkedDebit.text = "";
	frmIBAccntSummary.segDebitCard.setVisibility(false);
	frmIBAccntSummary.segMaturityDisplay.setVisibility(false);
	frmIBAccntSummary.lblUsefulValue10.text = "";
	frmIBAccntSummary.lblUsefulValue11.text = "";
	frmIBAccntSummary.lblUsefulValue12.text = "";
	frmIBAccntSummary.lblUsefulValue13.text = "";
	OTPcheckOnAccountSummary();
}
/*
 *************** Sixth Handle ***************
 */

function IBcallTimeDepositDetailService() {
	showLoadingScreenPopup();
	var inputparam = {};
	inputparam["acctIdentValue"] = (gblAccountTable["custAcctRec"][gblIndex]["accId"])
		.substring(4);
	invokeServiceSecureAsync("tDDetailinq", inputparam, IBtimeDepositAccountCallBack);
}

function IBtimeDepositAccountCallBack(status, resulttable) {
	
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			var temp = [];
			if (resulttable["tdDetailsRec"].length > 0) {
					for (var i = 0; i < resulttable["tdDetailsRec"].length; i++) {
					var segTable = {
						lblDate: reformatDate(resulttable["tdDetailsRec"][i]["maturityDate"]),
						lblTransaction: parseFloat(resulttable["tdDetailsRec"][i]["interestRate"])
							.toFixed(2) + kony.i18n.getLocalizedString("percent"),
						lblDepositBalance: commaFormatted(resulttable["tdDetailsRec"][i]["depositAmtVal"])
					}
					temp.push(segTable);
					//frmIBAccntSummary.segMaturityDisplay.setData(temp);
				}
				frmIBAccntSummary.segMaturityDisplay.setData(temp);
			}
			IBdepositAccInqCallService();
			IBTDAccountFunction();
		}else
			dismissLoadingScreenPopup();
	} else {
		//alert(" " + resulttable["errMsg"]);
		dismissLoadingScreenPopup();
	}
}

function IBdepositAccInqCallService() {
    //showLoadingScreenPopup();
	var inputparam = {};
	inputparam["acctId"] = gblAccountTable["custAcctRec"][gblIndex][
		"accId"];
	var status = invokeServiceSecureAsync("depositAccountInquiry", inputparam, IBdepositAccInqCallBack);
}

function IBdepositAccInqCallBack(status, resulttable) {
	if (status == 400) {
		
		if (resulttable["opstatus"] == 0) {
			if (resulttable["linkedacc"] != null && resulttable["linkedacc"] != "") {
			    var linkAccount;
			    var lenlinkAccount=resulttable["linkedacc"].length;
			    if(lenlinkAccount==30)
			    linkAccount=resulttable["linkedacc"].substring(20);
			    else
			    linkAccount=resulttable["linkedacc"].substring(16);
				frmIBAccntSummary.lblUsefulValue9.text = formatAccountNo(linkAccount);
				frmIBAccntSummary.hbxUseful9.isVisible=true;
			}
			else{
				frmIBAccntSummary.hbxUseful9.isVisible=false;
			}
			if (resulttable["count"] != null && resulttable["count"] != "") {
				frmIBAccntSummary.lblUsefulValue2.text = resulttable["count"] +" "+ resulttable["termUnit"];
				
			}
			if (resulttable["bankAccountStatus"] != null && resulttable["bankAccountStatus"] != "") {
				var status=resulttable["bankAccountStatus"].split("|");
				if (kony.i18n.getCurrentLocale() == "th_TH")
				frmIBAccntSummary.lblUsefulValue6.text = status[1];
				else
				frmIBAccntSummary.lblUsefulValue6.text = status[0];
			}
			frmIBAccntSummary.lblUsefulValue5.text = resulttable["accountTitle"];
			var availableBal;
			var ledgerBal;
			if (resulttable["AcctBal"].length > 0) {
				for (var i = 0; i < resulttable["AcctBal"].length; i++) {
					if (resulttable["AcctBal"][i]["BalType"] == "Avail") {
						availableBal = resulttable["AcctBal"][i]["Amt"];
					}
					if (resulttable["AcctBal"][i]["BalType"] == "Ledger") {
						ledgerBal = resulttable["AcctBal"][i]["Amt"];
					}
				}
			}
			if (null != availableBal) {
				frmIBAccntSummary.lblAccountBalanceHeader.text = commaFormatted(availableBal) + " " + kony.i18n.getLocalizedString(
					"currencyThaiBaht");
			}
			if (null != ledgerBal) {
				frmIBAccntSummary.lblUsefulValue8.text = commaFormatted(ledgerBal) + " " + kony.i18n.getLocalizedString(
					"currencyThaiBaht");
				frmIBAccntSummary.hbxUseful8.isVisible=true;
			}
			IBTDAccountFunction();
			dismissLoadingScreenPopup();
			frmIBAccntSummary.show();	
		} else {
			alert(" " + resulttable["errMsg"]);
			dismissLoadingScreenPopup();
		}
	}
}

function IBTDAccountFunction() {
	
	//frmIBAccntSummary.btnPayBill.onClick = IBmoveToTransfers
	frmIBAccntSummary.lblUseful10.text = "";
	frmIBAccntSummary.lblUseful11.text = "";
	frmIBAccntSummary.lblUseful12.text = "";
	frmIBAccntSummary.lblUseful13.text = "";
	frmIBAccntSummary.lblUseful14.text = "";
	frmIBAccntSummary.lblUseful15.text = "";	
	frmIBAccntSummary.hbxUseful10.isVisible=false;
	frmIBAccntSummary.hbxUseful11.isVisible=false;
	frmIBAccntSummary.hbxUseful12.isVisible=true;
	frmIBAccntSummary.hbxUseful13.isVisible=false;
	frmIBAccntSummary.hbxUseful14.isVisible=false;
	frmIBAccntSummary.hbxUseful15.isVisible=false;		
	frmIBAccntSummary.lblLinkedDebit.text = "";
	frmIBAccntSummary.line449290336723181.isVisible=false;
	frmIBAccntSummary.segDebitCard.setVisibility(false);
	frmIBAccntSummary.segMaturityDisplay.setVisibility(true);
	frmIBAccntSummary.hbxSegHeader.setVisibility(true);
	frmIBAccntSummary.lblDepositDetails.setVisibility(true);
	var locale = kony.i18n.getCurrentLocale();
	var branchName;
	var accType;
	if (locale == "en_US") {
		branchName = gblAccountTable["custAcctRec"][gblIndex]["BranchNameEh"];
		productName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"];
		accNickName= gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"]+" "+sbStr;
		accType=gblAccountTable["custAcctRec"][gblIndex]["Type_EN"];
	} else {
		branchName = gblAccountTable["custAcctRec"][gblIndex]["BranchNameTh"];
		productName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"];
		accNickName= gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"]+" "+sbStr;
		accType=gblAccountTable["custAcctRec"][gblIndex]["Type_TH"];
	}
	if(null !=gblAccountTable["custAcctRec"][gblIndex]["acctNickName"]){
		frmIBAccntSummary.lblAccountNameHeader.text = gblAccountTable["custAcctRec"][gblIndex]["acctNickName"];
	}else{
		frmIBAccntSummary.lblAccountNameHeader.text = accNickName;
	}
	frmIBAccntSummary.imgAccountDetailsPic.src = "prod_termdeposits.png";
	frmIBAccntSummary.lblUsefulValue1.text = productName;
	frmIBAccntSummary.lblUsefulValue3.text = accType;
	frmIBAccntSummary.lblUsefulValue4.text = gblAccountTable["custAcctRec"][gblIndex]["accountNoFomatted"]
	frmIBAccntSummary.lblUsefulValue7.text = branchName;
	frmIBAccntSummary.hbxUseful7.isVisible=true;
	frmIBAccntSummary.lblUsefulValue10.text = "";
	frmIBAccntSummary.lblUsefulValue11.text = "";
	frmIBAccntSummary.lblUsefulValue12.text = "";
	frmIBAccntSummary.lblUsefulValue13.text = "";
	frmIBAccntSummary.lblUsefulValue14.text = "";
	frmIBAccntSummary.lblUsefulValue15.text = "";		
	OTPcheckOnAccountSummary();
}
/*
 *************** Seventh Handle ***************
 */

function IBcash2GoAccountCallBck(status, resulttable) {
	
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			gblLoanAccountTable = resulttable;
			var outStandingBal;
			for (var i = 0; i < resulttable["acctBal"].length; i++) {
				if (resulttable["acctBal"][i]["balType"] == "Outstanding") {
					outStandingBal = resulttable["acctBal"][i]["balAmount"];
					break;
				}
			}
			if (null != outStandingBal) {
				frmIBAccntSummary.lblAccountBalanceHeader.text = commaFormatted(outStandingBal) + " " + kony.i18n.getLocalizedString(
					"currencyThaiBaht");
			}
			dismissLoadingScreenPopup();
			frmIBAccntSummary.show();	
			IBcash2GoAccount();
		} else {
			alert(" " + resulttable["errMsg"]);
			dismissLoadingScreenPopup();
		}
	}
}

function IBcash2GoAccount() {
	frmIBAccntSummary.imgAccountDetailsPic.src = "prod_cash2go.png";
	var productName="";
	var locale = kony.i18n.getCurrentLocale();
	if (locale == "en_US"){
		productName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"];
		accNickName=gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"]+" "+sbStr;
	}else{
		productName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"];
		accNickName=gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"]+" "+sbStr;		
	}
	if(null !=gblAccountTable["custAcctRec"][gblIndex]["acctNickName"]){
		frmIBAccntSummary.lblAccountNameHeader.text = gblAccountTable["custAcctRec"][gblIndex]["acctNickName"];
	}else{
		frmIBAccntSummary.lblAccountNameHeader.text = accNickName;
	}
	frmIBAccntSummary.lblUsefulValue1.text=productName
	//frmIBAccntSummary.BtnTopUp.isVisible = false;
	//frmIBAccntSummary.btnTransfer.isVisible = false;
	//frmIBAccntSummary.btnPayBill.isVisible = true;
	frmIBAccntSummary.lblUseful10.text = "";
	frmIBAccntSummary.lblUseful11.text = "";
	frmIBAccntSummary.lblUseful12.text = "";
	frmIBAccntSummary.lblUseful13.text = "";
	frmIBAccntSummary.lblUseful14.text = "";
	frmIBAccntSummary.lblUseful15.text = "";	
	frmIBAccntSummary.hbxUseful10.isVisible=false;
	frmIBAccntSummary.hbxUseful11.isVisible=false;
	frmIBAccntSummary.hbxUseful12.isVisible=false;
	frmIBAccntSummary.hbxUseful13.isVisible=false;
	frmIBAccntSummary.hbxUseful14.isVisible=false;
	frmIBAccntSummary.hbxUseful15.isVisible=false;

	
	frmIBAccntSummary.lblLinkedDebit.text = "";
	//frmIBAccntSummary.btnPayBill.text = kony.i18n.getLocalizedString("PayBill");
	//frmIBAccntSummary.btnPayBill.onClick = moveToBillPaymentFromAccountSummaryForLoan;
	//frmIBAccntSummary.btnPayBill.onClick = moveToBillPaymentFromAccountSummaryForLoan
	OTPcheckOnAccountSummary();
	frmIBAccntSummary.lblLinkedDebit.text = "";
	frmIBAccntSummary.segDebitCard.setVisibility(false);
	frmIBAccntSummary.segMaturityDisplay.setVisibility(false);
	if (gblLoanAccountTable["loanAccounNo"] != "" && gblLoanAccountTable["loanAccounNo"] != null) {
		frmIBAccntSummary.lblUsefulValue2.text = formatAccountNo(gblLoanAccountTable["loanAccounNo"].substring(17, 27));
		frmIBAccntSummary.lblUsefulValue3.text = gblLoanAccountTable["loanAccounNo"].substring(27);
	}
	frmIBAccntSummary.lblUsefulValue4.text = gblLoanAccountTable["accountName"]
	frmIBAccntSummary.lblUsefulValue5.text = reformatDate(gblLoanAccountTable["nextPmtDt"]);
	if (gblLoanAccountTable["regPmtCurAmt"] != "" && gblLoanAccountTable["regPmtCurAmt"] != null) {
		frmIBAccntSummary.lblUsefulValue6.text = commaFormatted(gblLoanAccountTable["regPmtCurAmt"]) + kony.i18n.getLocalizedString(
			"currencyThaiBaht");
	}
	if (gblLoanAccountTable["debitAccount"].length != 0) {
	    //frmIBAccntSummary.hbxUseful9.isVisible=true
		frmIBAccntSummary.lblUsefulValue9.text = formatAccountNo(gblLoanAccountTable["debitAccount"].substring(4));
		frmIBAccntSummary.hbxUseful9.isVisible=true;
	}
	else
	{
	  frmIBAccntSummary.lblUseful9.text = "";
	}
	frmIBAccntSummary.lblUseful7.text = "";
	frmIBAccntSummary.hbxUseful7.isVisible=false;
	frmIBAccntSummary.lblUsefulValue7.text = ""
	frmIBAccntSummary.lblUsefulValue8.text = ""
	frmIBAccntSummary.lblUsefulValue10.text = "";
	frmIBAccntSummary.lblUsefulValue11.text = "";
	frmIBAccntSummary.lblUsefulValue12.text = "";
	frmIBAccntSummary.lblUsefulValue13.text = "";
	frmIBAccntSummary.lblUsefulValue14.text = "";
	frmIBAccntSummary.lblUsefulValue15.text = "";

}

function IBloanAccountCallService(callback) {
	showLoadingScreenPopup();
	var inputparam = {};
	inputparam["acctId"] =gblAccountTable["custAcctRec"][gblIndex][
		"accId"]
	//inputparam["acctType"] = gblAccountTable["custAcctRec"][gblIndex]["accType"];
	//inputparam["rqUUId"] = "";
	var status = invokeServiceSecureAsync("doLoanAcctInq", inputparam, callback);
}
/*
 *************** Eight Handle ***************
 */

function IBhomeLoanAccountCallBck(status, resulttable) {
	
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			gblLoanAccountTable = resulttable;
			var outStandingBal;
			var temp = [];
			for (var i = 0; i < resulttable["acctBal"].length; i++) {
				if (resulttable["acctBal"][i]["balType"] == "Outstanding") {
					outStandingBal = resulttable["acctBal"][i]["balAmount"];
					break;
				}
			}
			if (null != outStandingBal) {
				frmIBAccntSummary.lblAccountBalanceHeader.text = commaFormatted(outStandingBal) + " " + kony.i18n.getLocalizedString(
					"currencyThaiBaht");
			}
			var expiryDate = [];
			if (resulttable["creditLimitInfo"].length > 0) {
				for (var i = 0; i < resulttable["creditLimitInfo"].length; i++) {
					if (resulttable["creditLimitInfo"][i]["expiryDate"] != "" && resulttable["creditLimitInfo"][i]["expiryDate"] !=
						null) {
						expiryDate[i] = reformatDate(resulttable["creditLimitInfo"][i]["expiryDate"])
					}
					var segTable = {
						lblDate: commaFormatted(resulttable["creditLimitInfo"][i]["loanCreditLimit"]),
						lblTransaction: parseFloat(resulttable["creditLimitInfo"][i]["loanRate"])
							.toFixed(3) + kony.i18n.getLocalizedString("percent"),
						lblDepositBalance: expiryDate[i]
					}
					temp.push(segTable);
					//frmIBAccntSummary.segMaturityDisplay.setData(temp);
				}
				frmIBAccntSummary.segMaturityDisplay.setData(temp);
			}
			dismissLoadingScreenPopup();
			frmIBAccntSummary.show();	
			IBhomeLoanAccount();
		} else {
			alert(" " + resulttable["errMsg"]);
			dismissLoadingScreenPopup();
		}
	}
}

function IBhomeLoanAccount() {
	frmIBAccntSummary.imgAccountDetailsPic.src = "prod_homeloan.png";
	//frmIBAccntSummary.lblAccountBalanceHeader.text=gblAccountTable["custAcctRec"][gblIndex]["availableBal"]+" "+kony.i18n.getLocalizedString("currencyThaiBaht");    
	//frmIBAccntSummary.btnPayBill.onClick = moveToBillPaymentFromAccountSummaryForLoan;
	OTPcheckOnAccountSummary();
	frmIBAccntSummary.lblUseful9.text = "";
	frmIBAccntSummary.lblUsefulValue9.text = "";
	frmIBAccntSummary.lblUseful10.text = "";
	frmIBAccntSummary.lblUsefulValue10.text="";
	frmIBAccntSummary.lblUseful11.text = "";
	frmIBAccntSummary.lblUsefulValue11.text="";
	frmIBAccntSummary.lblUseful12.text = "";
	frmIBAccntSummary.lblUsefulValue12.text="";
	frmIBAccntSummary.lblUseful13.text = "";
	frmIBAccntSummary.lblUsefulValue13.text="";
	frmIBAccntSummary.lblUseful14.text = "";
	frmIBAccntSummary.lblUsefulValue14.text="";
	frmIBAccntSummary.lblUseful15.text = "";
	frmIBAccntSummary.lblUsefulValue15.text="";	
	frmIBAccntSummary.hbxUseful8.isVisible=false;
	frmIBAccntSummary.hbxUseful9.isVisible=false;
	//frmIBAccntSummary.hbxUseful2.isVisible=false;
	frmIBAccntSummary.hbxUseful10.isVisible=false;
	frmIBAccntSummary.hbxUseful11.isVisible=false;
	frmIBAccntSummary.hbxUseful12.isVisible=false;
	frmIBAccntSummary.hbxUseful13.isVisible=false;	
	frmIBAccntSummary.hbxUseful14.isVisible=false;
	frmIBAccntSummary.hbxUseful15.isVisible=false;
	frmIBAccntSummary.lblLinkedDebit.text = "";
	frmIBAccntSummary.lblLinkedDebit.text = "";
	frmIBAccntSummary.hbxSegHeader.setVisibility(true);
	frmIBAccntSummary.segDebitCard.setVisibility(false);
	frmIBAccntSummary.lblDepositDetails.setVisibility(true);
	frmIBAccntSummary.lblDepositDetails.text = kony.i18n.getLocalizedString("interestRateDetail");
	frmIBAccntSummary.segMaturityDisplay.setVisibility(true);
	var locale = kony.i18n.getCurrentLocale();
	if (locale == "en_US"){
		productName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"];
		accNickName=gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"]+" "+sbStr;
	}else{
		productName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"];
		accNickName=gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"]+" "+sbStr;
	}	
	if(null !=gblAccountTable["custAcctRec"][gblIndex]["acctNickName"]){
		frmIBAccntSummary.lblAccountNameHeader.text = gblAccountTable["custAcctRec"][gblIndex]["acctNickName"];
	}else{
		frmIBAccntSummary.lblAccountNameHeader.text = accNickName;
	}
	frmIBAccntSummary.lblUsefulValue1.text = productName;
	if (gblLoanAccountTable["loanAccounNo"] != "" && gblLoanAccountTable["loanAccounNo"] != null) {
		frmIBAccntSummary.lblUsefulValue2.text = formatAccountNo(gblLoanAccountTable["loanAccounNo"].substring(17, 27));
		frmIBAccntSummary.lblUsefulValue3.text = gblLoanAccountTable["loanAccounNo"].substring(27);
	}
	frmIBAccntSummary.lblUsefulValue4.text = gblLoanAccountTable["accountName"];
	//frmIBAccntSummary.lblUsefulValue.text = parseInt(gblLoanAccountTable["currentInterestRate"]) + kony.i18n.getLocalizedString("percent");
	frmIBAccntSummary.lblUsefulValue5.text = reformatDate(gblLoanAccountTable["nextPmtDt"]);
	frmIBAccntSummary.lblUsefulValue6.text = commaFormatted(gblLoanAccountTable["regPmtCurAmt"]);
	frmIBAccntSummary.hbxUseful7.isVisible=true;
	if (gblLoanAccountTable["debitAccount"].length != 0) 
	{
		frmIBAccntSummary.lblUseful7.setVisibility(true);
		frmIBAccntSummary.lblUsefulValue7.text = formatAccountNo(gblLoanAccountTable["debitAccount"].substring(4));
		frmIBAccntSummary.hbxUseful7.isVisible=true;
	}
	else
	{
		frmIBAccntSummary.lblUseful7.text="";
		frmIBAccntSummary.hbxUseful7.isVisible=false;
	}
}

function IBonClickOfAccountDetailsMore() {
	if (frmIBAccntSummary.linkMore.text == kony.i18n.getLocalizedString("More")) {
		frmIBAccntSummary.linkMore.text = kony.i18n.getLocalizedString("Hide");
		frmIBAccntSummary.hbxUseful7.isVisible = true;
		frmIBAccntSummary.hbxUseful8.isVisible = true;
		frmIBAccntSummary.hbxUseful9.isVisible = true;
		frmIBAccntSummary.hbxUseful10.isVisible = true;
		if (glb_viewName != "PRODUCT_CODE_NOFIXED") {
			frmIBAccntSummary.hbxUseful11.isVisible = true;
		}
		//if (glb_viewName == "PRODUCT_CODE_NOFIXED") {
			//frmIBAccntSummary.hboxApplySendToSave.isVisible = true;
		//}
		frmIBAccntSummary.hbxUseful12.isVisible = true;
		frmIBAccntSummary.hbxUseful13.isVisible = true;
		frmIBAccntSummary.hbxLinkedTo.isVisible = true;
		if (glb_viewName == "PRODUCT_CODE_SAVING_TABLE" || glb_viewName == "PRODUCT_CODE_CURRENT_TABLE" || glb_viewName ==
			"PRODUCT_CODE_NOFEESAVING_TABLE") {
			frmIBAccntSummary.hbxLinkedTo.isVisible = true;
			frmIBAccntSummary.segDebitCard.setVisibility(true);
			if (GLOBAL_DEBITCARD_TABLE != null && GLOBAL_DEBITCARD_TABLE != "") {
				var temp = [];
				for (var i = 0; i < GLOBAL_DEBITCARD_TABLE.length; i++) {
					var segTable = {
						imgCreditCard: "piggyblueico.png",
						lblCreditCardNo: GLOBAL_DEBITCARD_TABLE[i]["maskedCardNo"],
						lblExpiryDate: kony.i18n.getLocalizedString("ExpiryDate"),
						lblExpiryDateV: GLOBAL_DEBITCARD_TABLE[i]["expiryDate"]
					}
					temp.push(segTable);
					//frmIBAccntSummary.segDebitCard.setData(temp);
				}
				frmIBAccntSummary.segDebitCard.setData(temp);
			}
		}
	} else {
		frmIBAccntSummary.linkMore.text = kony.i18n.getLocalizedString("More");
		//if (glb_viewName == "PRODUCT_CODE_NOFIXED") {
			//frmIBAccntSummary.hboxApplySendToSave.isVisible = true;
		//}
		frmIBAccntSummary.hbxUseful7.isVisible = false;
		frmIBAccntSummary.hbxUseful8.isVisible = false;
		frmIBAccntSummary.hbxUseful9.isVisible = false;
		frmIBAccntSummary.hbxUseful10.isVisible = false;
		frmIBAccntSummary.hbxUseful11.isVisible = false;
		frmIBAccntSummary.hbxUseful12.isVisible = false;
		frmIBAccntSummary.hbxUseful13.isVisible = false;
		frmIBAccntSummary.segDebitCard.setVisibility(false);
		frmIBAccntSummary.hbxLinkedTo.isVisible = false;
		frmIBAccntSummary.hboxApplySendToSave.isVisible = false;
		frmIBAccntSummary.hbxUseful11.isVisible = false;
	}
}

function IBcallDepositAccountInquiryServiceNofee() {
	showLoadingScreenPopup();
	var inputparam = {};
	inputparam["acctId"] = gblAccountTable["custAcctRec"][gblIndex][
		"accId"];
	//inputparam["acctType"] = gblAccountTable["custAcctRec"][gblIndex]["accType"];
	//inputparam["rqUUId"] = "";
	//inputparam["spName"] = "com.fnf.xes.ST"
	var status = invokeServiceSecureAsync("depositAccountInquiry", inputparam, IBdepositAccountInquiryCallBackNofee);
}

function IBdepositAccountInquiryCallBackNofee(status, resulttable) {
	if (status == 400) {
		
		if (resulttable["opstatus"] == 0) {
			var StatusCode = resulttable["StatusCode"];
			var Severity = resulttable["Severity"];
			var StatusDesc = resulttable["StatusDesc"];
			
			if (resulttable["bankAccountStatus"] != null && resulttable["bankAccountStatus"] != "") {
				var status=resulttable["bankAccountStatus"].split("|");
				if(kony.i18n.getCurrentLocale() == "th_TH")
				frmIBAccntSummary.lblUsefulValue6.text = status[1];
				else
				frmIBAccntSummary.lblUsefulValue6.text = status[0];
			}
			if (resulttable["accountTitle"] != null && resulttable["accountTitle"] != "") {
				frmIBAccntSummary.lblUsefulValue5.text = resulttable["accountTitle"];
			}
			var availableBal;
			var ledgerBal;
			if (resulttable["AcctBal"].length > 0) {
				for (var i = 0; i < resulttable["AcctBal"].length; i++) {
					if (resulttable["AcctBal"][i]["BalType"] == "Avail") {
						availableBal = resulttable["AcctBal"][i]["Amt"];
					}
					if (resulttable["AcctBal"][i]["BalType"] == "Ledger") {
						ledgerBal = resulttable["AcctBal"][i]["Amt"];
					}
				}
			}
			if (null != availableBal) {
				frmIBAccntSummary.lblAccountBalanceHeader.text = commaFormatted(availableBal) + " " + kony.i18n.getLocalizedString(
					"currencyThaiBaht");
			}
			if (null != ledgerBal) {
				frmIBAccntSummary.lblUsefulValue8.text = commaFormatted(ledgerBal) + " " + kony.i18n.getLocalizedString(
					"currencyThaiBaht");
				frmIBAccntSummary.hbxUseful8.isVisible=true;	
			}
			IBcallDebitCardInqService();
			IBnoFeeSavingAccount();
			//kony.application.dismissLoadingScreen();
		} else {
			alert(" " + resulttable["errMsg"]);
			kony.application.dismissLoadingScreen();
		}
	}
}

function IBcallDebitCardInqService() {
	//showLoadingScreenPopup();
	var inputparam = {};
	//inputparam["rqUUId"] = "";
   
	inputparam["acctIdentValue"] = gblAccountTable["custAcctRec"][gblIndex]["accId"]
	
	//inputparam["fIIdent"] = gblAccountTable["custAcctRec"][gblIndex]["fiident"];
	//inputparam["spName"] = "com.fnis.xes.TS";
	//inputparam["locale"] = kony.i18n.getCurrentLocale();
	
	var status = invokeServiceSecureAsync("debitCardInq", inputparam, IBdebitCardInqCallBack)
}

function IBdebitCardInqCallBack(status, resulttable) {
	if (status == 400) {
		
		if (resulttable["opstatus"] == 0) {
			if (resulttable["StatusCode"] == 0) {
				GLOBAL_DEBITCARD_TABLE = resulttable["debitCardRec"];
			frmIBAccntSummary.hbxLinkedTo.isVisible = true;
			frmIBAccntSummary.lblLinkedDebit.text = kony.i18n.getLocalizedString("LinkedTo");
			frmIBAccntSummary.segDebitCard.setVisibility(true);
			if (resulttable["debitCardRec"] != null && resulttable["debitCardRec"] != "") {
				var temp = [];
				var imageForDisplay="";
				for (var i = 0; i < resulttable["debitCardRec"].length; i++) {
	              imageForDisplay="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+resulttable["debitCardRec"][i]["cardLogo"]+"&modIdentifier=AccountDetails";
			
				       //imageForDisplay="debitcard.png"

				    var cardStatus = resulttable["debitCardRec"][i]["cardStatusCode"];   
				    var linkActivate = "";
				    var imgActivateCard = "";
				    
				    if (cardStatus == "" || cardStatus == "Active") {
				    	// Active - Activated
						cardStatus = kony.i18n.getLocalizedString("keyCardActivated");
				    } else if (cardStatus == "Pin Assign(P)") {
				    	// Pin Assign(P) - Waiting for Activate
						cardStatus = kony.i18n.getLocalizedString("keyCardWaitForActivated");
						linkActivate = kony.i18n.getLocalizedString('keyActivateCard');
				    } else {
						cardStatus = "";						
				    }
	       
					var segTable = {
						imgCreditCard: imageForDisplay,
						lblCreditCardNo: resulttable["debitCardRec"][i]["maskedCardNo"],
						lblExpiryDate: kony.i18n.getLocalizedString("ExpiryDate"),
						lblExpiryDateV: resulttable["debitCardRec"][i]["expiryDate"],
						lblCardStatus: cardStatus,	
						linkActivateCard: linkActivate
					}
					temp.push(segTable);
					//frmIBAccntSummary.segDebitCard.setData(temp);
				}
				frmIBAccntSummary.segDebitCard.setData(temp);
			}
				if(gblAccountTable["SAVING_CARE_PRODUCT_CODES"].indexOf(gblAccountTable["custAcctRec"][gblIndex]["productID"]) < 0){
					dismissLoadingScreenPopup();
				}
				frmIBAccntSummary.show();
			} else {
				dismissLoadingScreenPopup();
				//alert(" " + resulttable["errMsg"]);
			}
		} else {
			dismissLoadingScreenPopup();
			//alert(" " + resulttable["errMsg"]);
			frmIBAccntSummary.show();	
		}
	}
}

function IBcallDepositAccountInquiryService() {
	showLoadingScreenPopup();
	var inputparam = {};
	inputparam["acctId"] = gblAccountTable["custAcctRec"][gblIndex]["accId"];
	
	var status = invokeServiceSecureAsync("depositAccountInquiry", inputparam, IBdepositAccountInquiryCallBack);
}

function IBdepositAccountInquiryCallBack(status, resulttable) {
	if (status == 400) {
		
		if (resulttable["opstatus"] == 0) {
			
			if (resulttable["bankAccountStatus"] != null && resulttable["bankAccountStatus"] != "") {
				var status=resulttable["bankAccountStatus"].split("|");
				if(kony.i18n.getCurrentLocale() == "th_TH")
				frmIBAccntSummary.lblUsefulValue5.text =status[1];
				else
				frmIBAccntSummary.lblUsefulValue5.text =status[0];
			}
			if (resulttable["accountTitle"] != null && resulttable["accountTitle"] != "") {
				frmIBAccntSummary.lblUsefulValue4.text = resulttable["accountTitle"];
			}
			var availableBal;
			var ledgerBal;
			if (resulttable["AcctBal"].length > 0) {
				for (var i = 0; i < resulttable["AcctBal"].length; i++) {
					if (resulttable["AcctBal"][i]["BalType"] == "Avail") {
						availableBal = resulttable["AcctBal"][i]["Amt"];
					}
					if (resulttable["AcctBal"][i]["BalType"] == "Ledger") {
						ledgerBal = resulttable["AcctBal"][i]["Amt"];
					}
				}
			}
			if (null != availableBal) {
				frmIBAccntSummary.lblAccountBalanceHeader.text = commaFormatted(availableBal) + " " + kony.i18n.getLocalizedString(
					"currencyThaiBaht");
			}
			if (null != ledgerBal) {
				frmIBAccntSummary.lblUsefulValue7.text = commaFormatted(ledgerBal) + " " + kony.i18n.getLocalizedString(
					"currencyThaiBaht");
				frmIBAccntSummary.hbxUseful7.isVisible=true;	
			}
			//dismissLoadingScreenPopup();
			IBcallDebitCardInqService();
			IBsimpleAccountFunction();
			if(gblAccountTable["SAVING_CARE_PRODUCT_CODES"].indexOf(gblAccountTable["custAcctRec"][gblIndex]["productID"]) >= 0){
				getBeneficaryDetailsIB();
			}
		} else {
			alert(" " + resulttable["errMsg"]);
			dismissLoadingScreenPopup();
		}
	}
}

function IBmoveToTransfers() {
	//MIB-1063

	//logic to move to transfera page with a particular accno.
	var accId = gblAccountTable["custAcctRec"][gblIndex]["accId"];
	//var accName=
	
	//getFromXferAccountsIB();
	showLoadingScreenPopup();
	getFromXferAccountsIBFromSummary();
	
	//alert
	
	/*
	TMBUtil.DestroyForm(frmIBTranferLP);
	var selectedItem = frmIBTransferCustomWidgetLP.custom1016496273208904.selectedItem;
	gblcwselectedData = frmIBTransferCustomWidgetLP.custom1016496273208904.data[gblCWSelectedItem];
	
	dismissLoadingScreenPopup();
	frmIBTranferLP.show();
	frmIBTranferLP.hbxwaterWheel.setVisibility(true);
	if ( gblAccountTable["custAcctRec"][gblIndex]["accountNoFomatted"] == undefined) {
		frmIBTranferLP.lblTranLandFromNum.text = "undefined";
	} else {
		frmIBTransferNowConfirmation.image247327596551482.src =// gblcwselectedData.img1;
		frmIBTransferNowCompletion.image247327596551482.src = //gblcwselectedData.img1;
		frmIBTransferNowConfirmation.lblBeforBal.text =  gblAccountTable["custAcctRec"][gblIndex]["availableBalDisplay"]//gblcwselectedData.lblBalance;
		frmIBTranferLP.lblXferFromNameRcvd.text = gblAccountTable["custAcctRec"][gblIndex]["acctNickName"]//gblcwselectedData.lblCustName;
		frmIBTranferLP.lblTranLandFromNum.text = gblAccountTable["custAcctRec"][gblIndex]["accountNoFomatted"] //gblcwselectedData.accountWOF;

		gbltdFlag = gblAccountTable["custAcctRec"][gblIndex]["accType"];//gblcwselectedData.lblSliderAccN2;
		productCode =gblAccountTable["custAcctRec"][gblIndex]["productID"];//fromData[selectedItem].prodCode;
		
		
		
		toggleTDMaturityComboboxIB(gbltdFlag);
		
	}*/
	//var selectedItem = frmIBTransferCustomWidgetLP.custom1016496273208904.selectedItem;
	//gblcwselectedData = frmIBTransferCustomWidgetLP.custom1016496273208904.data[gblCWSelectedItem];
	
	//frmIBTranferLP.lblXferFromNameRcvd =
	//frmIBTranferLP.lblTranLandFromNum =
}

function IBmoveToBillPayment()
{
	// code to move to bill pay modules as per situation
	
	//callBillPaymentCustomerAccountServiceIB();
	billPayFromSummary = true;
	billPaySummary = true;
	gblBillPaymentEdit = false;
	
	var jointActXfer  = gblAccountTable["custAcctRec"][gblIndex].partyAcctRelDesc;
	
	if(jointActXfer == "PRIJNT" || jointActXfer == "SECJNT" || jointActXfer == "OTHJNT" || jointActXfer == "SECJAN"){
		alert(""+ kony.i18n.getLocalizedString('keyNotAllloedTransactions'));
		return;
	}
	var accountStatus = gblAccountTable["custAcctRec"][gblIndex].acctStatusCode;
	frmIBBillPaymentCW.customIBBPFromAccount.data = undefined;
	gblProductCode = gblAccountTable["custAcctRec"][gblIndex].productID;
	if(accountStatus.indexOf("Active") == -1){
		alert(""+ kony.i18n.getLocalizedString('keyNotAllloedTransactions'));
		return;
	}
	frmIBBillPaymentLP.hbxFromBalance.setVisibility(true);
	frmIBBillPaymentLP.lblBPFromNameRcvd.text = frmIBAccntSummary.lblAccountNameHeader.text;
	frmIBBillPaymentLP.imgFrom.src = frmIBAccntSummary.imgAccountDetailsPic.src;
	frmIBBillPaymentLP.lblProductType.text = frmIBAccntSummary.lblUsefulValue1.text;
	frmIBBillPaymentLP.lblFromAccNo.text = gblAccountTable["custAcctRec"][gblIndex]["accountNoFomatted"];
	frmIBBillPaymentLP.lblBalanceFromValue.text = gblAccountTable["custAcctRec"][gblIndex]["availableBalDisplay"] + kony.i18n.getLocalizedString("currencyThaiBaht");
	
	if(gblSMART_FREE_TRANS_CODES.indexOf(gblAccountTable["custAcctRec"][gblIndex]["productID"]) >= 0){
    frmIBBillPaymentLP.lblFreeTransValue.text = gblAccountTable["custAcctRec"][gblIndex]["remainingFee"];
    frmIBBillPaymentLP.hbxfreeTransaction.setVisibility(true);
    gblBillpaymentNoFee=true;
    
    }
    else {
    frmIBBillPaymentLP.hbxfreeTransaction.setVisibility(false);
    frmIBBillPaymentLP.lblFreeTransValue.text="";
    gblBillpaymentNoFee=false;
    
    }
 	frmIBBillPaymentLP.lblAcctType.text=gblAccountTable["custAcctRec"][gblIndex]["accType"];
	frmIBBillPaymentLP.lblDummy.text=gblAccountTable["custAcctRec"][gblIndex]["fiident"];
	
	showLoadingScreenPopup();
	callcrmProfileInqForTopUP();
	customerAccountServiceForFromAcctCheckIB();
	dismissLoadingScreenPopup();
	frmIBBillPaymentLP.show();
	//navigateMenuBillPayment();
}

function IBmovetoTopUp()
{
	//code to move to top up flow with selected account
	//callTopUpPaymentCustomerAccountServiceIB();
	//navigateMenuTopupPayment();
	billPayFromSummary = true;
	billPayTopup = true;
	//if(frmIBTopUpLandingPage != undefined)
	//frmIBTopUpLandingPage.destroy();
	
	var jointActXfer  = gblAccountTable["custAcctRec"][gblIndex].partyAcctRelDesc;
	
	if(jointActXfer == "PRIJNT" || jointActXfer == "SECJNT" || jointActXfer == "OTHJNT" || jointActXfer == "SECJAN"){
		alert(""+ kony.i18n.getLocalizedString('keyNotAllloedTransactions'));
		return;
	}
	var accountStatus = gblAccountTable["custAcctRec"][gblIndex].acctStatusCode;
	
	if(accountStatus.indexOf("Active") == -1){
		alert(""+ kony.i18n.getLocalizedString('keyNotAllloedTransactions'));
		return;
	}
	frmIBTopUpLandingPage.hbxFrmSegmentDataContainer.setVisibility(true);
	frmIBTopUpLandingPage.hbxBalance.setVisibility(true);
	frmIBTopUpLandingPage.lblName.text = frmIBAccntSummary.lblAccountNameHeader.text;
	frmIBTopUpLandingPage.imgFrom.src = frmIBAccntSummary.imgAccountDetailsPic.src;
	frmIBTopUpLandingPage.lblAcctType.text = frmIBAccntSummary.lblUsefulValue1.text;
	frmIBTopUpLandingPage.lblAccountNo.text = gblAccountTable["custAcctRec"][gblIndex]["accountNoFomatted"];
	frmIBTopUpLandingPage.lblBalanceValue.text = gblAccountTable["custAcctRec"][gblIndex]["availableBalDisplay"] + kony.i18n.getLocalizedString("currencyThaiBaht");

	if(gblSMART_FREE_TRANS_CODES.indexOf(gblAccountTable["custAcctRec"][gblIndex]["productID"]) >= 0){
    frmIBTopUpLandingPage.lblFreeTransValue.text = gblAccountTable["custAcctRec"][gblIndex]["remainingFee"];
    frmIBTopUpLandingPage.hbxFreeTrans.setVisibility(true);
    
    }
    else {
    frmIBTopUpLandingPage.hbxFreeTrans.setVisibility(false);
    frmIBTopUpLandingPage.lblFreeTransValue.text="";
    
    }
	
	showLoadingScreenPopup();
	callcrmProfileInqForTopUP();
	customerAccountServiceForFromAcctCheckIB();
	dismissLoadingScreenPopup();
	clearAllTopUpGlobalsIB();
	frmIBTopUpLandingPage.show();
}

function customerAccountServiceForFromAcctCheckIB() {
	if(getCRMLockStatus())
	{
		curr_form=kony.application.getCurrentForm().id;
		dismissLoadingScreenPopup();
		popIBBPOTPLocked.show();
	} else {
		
		var inputParam = {}
		inputParam["billPayInd"] = "billPayInd";
		//billPaymentCustomerAccountCallBackIB(status,resulttable)
		showLoadingScreenPopup();
		invokeServiceSecureAsync("customerAccountInquiry", inputParam, customerAccountServiceForFromAcctCheckCallBackIB);
	}
}

function customerAccountServiceForFromAcctCheckCallBackIB(status, resulttable) {
	
	
	if (status == 400) //success responce
	{
		if (resulttable["opstatus"] == 0) {
		
		} else {
			dismissLoadingScreenPopup();
			alert(" " + resulttable["errMsg"]);
		}
	} //status 400
}

function resetdetailspage()
{
    frmIBAccntSummary.segDebitCard.removeAll();
	frmIBAccntSummary.lblUseful1.text = ""
	frmIBAccntSummary.lblUseful2.text = ""
	frmIBAccntSummary.lblUseful3.text = ""
	frmIBAccntSummary.lblUseful4.text = ""
	frmIBAccntSummary.lblUseful5.text = ""
	frmIBAccntSummary.lblUseful6.text = ""
	frmIBAccntSummary.lblUseful7.text = ""
	frmIBAccntSummary.lblUseful8.text = ""
	frmIBAccntSummary.lblUseful9.text = ""
	frmIBAccntSummary.lblUseful10.text = ""
	frmIBAccntSummary.lblUseful11.text = ""
	frmIBAccntSummary.lblUseful12.text = ""
	frmIBAccntSummary.lblUseful13.text = ""
	frmIBAccntSummary.lblUseful14.text = ""
	frmIBAccntSummary.lblUseful15.text = ""
	
	frmIBAccntSummary.lblUsefulValue1.text = ""
	frmIBAccntSummary.lblUsefulValue2.text = ""
	frmIBAccntSummary.lblUsefulValue3.text = ""
	frmIBAccntSummary.lblUsefulValue4.text = ""
	frmIBAccntSummary.lblUsefulValue5.text = ""
	frmIBAccntSummary.lblUsefulValue6.text = ""
	frmIBAccntSummary.lblUsefulValue7.text = ""
	frmIBAccntSummary.lblUsefulValue8.text = ""
	frmIBAccntSummary.lblUsefulValue9.text = ""
	frmIBAccntSummary.lblUsefulValue10.text = ""
	frmIBAccntSummary.lblUsefulValue11.text = ""
	frmIBAccntSummary.lblUsefulValue12.text = ""
	frmIBAccntSummary.lblUsefulValue13.text = ""
	frmIBAccntSummary.lblUsefulValue14.text = ""
	frmIBAccntSummary.lblUsefulValue15.text = ""
	frmIBAccntSummary.hboxApplySendToSave.isVisible=false;
	frmIBAccntSummary.lblAccountNameHeader.text = ""
	frmIBAccntSummary.lblAccountBalanceHeader.text = ""
	frmIBAccntSummary.imgAccountDetailsPic.src = "transparent.png"
	frmIBAccntSummary.btnPayBill.isVisible=false;
	frmIBAccntSummary.BtnTopUp.isVisible=false;
	frmIBAccntSummary.btnTransfer.isVisible=false;
	frmIBAccntSummary.payMyCredit.isVisible=false;
	frmIBAccntSummary.payMyLoan.isVisible=false;
	frmIBAccntSummary.lnkDreamSaving.isVisible = false;
	frmIBAccntSummary.btnTransfer.containerWeight = 26;
	frmIBAccntSummary.payMyLoan.containerWeight=0;
    frmIBAccntSummary.payMyCredit.containerWeight=0;
	frmIBAccntSummary.btnPayBill.containerWeight=26;
	frmIBAccntSummary.BtnTopUp.containerWeight=23;
	frmIBAccntSummary.btnTransfer.skin=btnIB80px;
	frmIBAccntSummary.payMyLoan.skin=btnIB80px;
	frmIBAccntSummary.payMyCredit.skin=btnIB80px;
	frmIBAccntSummary.hbxSegHeader.setVisibility(false);
	frmIBAccntSummary.segMaturityDisplay.setVisibility(false);	
	frmIBAccntSummary.lblDepositDetails.setVisibility(false);
	frmIBAccntSummary.hbxLinkedTo.isVisible = false;
	frmIBAccntSummary.hbxUseful2.isVisible = true;
	frmIBAccntSummary.hbxUseful3.isVisible = true;
	
	frmIBAccntSummary.hbxUseful7.isVisible = true;
	frmIBAccntSummary.hbxUseful8.isVisible = true;
	frmIBAccntSummary.hbxUseful9.isVisible = true;
	frmIBAccntSummary.hbxUseful10.isVisible = true;
	frmIBAccntSummary.hbxUseful11.isVisible = true;
	frmIBAccntSummary.hbxUseful12.isVisible = true;
	frmIBAccntSummary.hbxUseful13.isVisible = true;
	frmIBAccntSummary.hbxUseful14.isVisible = false;
	frmIBAccntSummary.hbxUseful15.isVisible = false;
	
	//Resetting Beneficiary Details Data here
	frmIBAccntSummary.hbxBeneficiaryTitle.isVisible=false;
	frmIBAccntSummary.hbxEditBeneficiaryLink.isVisible=false;
	frmIBAccntSummary.hbxBeneficiaryText.isVisible=false;
	frmIBAccntSummary.hbxBeneficiaryNotSpecified.isVisible=false;
	frmIBAccntSummary.segBeneficiaryData.setData([]);
}



	
function OTPcheckOnAccountSummary(){
	if(getCRMLockStatus())
		{
			curr_form=kony.application.getCurrentForm().id;
			frmIBAccntSummary.btnPayBill.onClick=showOTPLockedPop
			frmIBAccntSummary.BtnTopUp.onClick=showOTPLockedPop;
			frmIBAccntSummary.btnTransfer.onClick=showOTPLockedPop;
		}
		
}
		
function getFromXferAccountsIBFromSummary() {
 		GBLFINANACIALACTIVITYLOG = {}
 		var inputParam = {}
		inputParam["transferFlag"] = "true";
		recipientAddFromTransfer = false;
		gblTransferFromRecipient = false;
  		gblTrasferFrmAcntSummary = true;
		gblSelTransferMode = "";//default account transfer -- Commented for implementation of MIB-5612 - TaxID Implementation IB
  		gblShowCoverFlow = false;
		showLoadingScreenPopup();
		clearValueIBTransferLP();
    	setDateOnly();		
		invokeServiceSecureAsync("customerAccountInquiry", inputParam, callBackTransferFromAccountsInBkSummary)
}
/**************************************************************************************
		Module	: callBackFromXferAccountsIB
		Author  : Kony
		Date    : May 05, 2013
		Purpose : This function pulls  FROM reciepients for  transfers
*****************************************************************************************/
promptPayAccountFlag = false; //To handle account summary Transfer short cut
var fromData = [];
function callBackTransferFromAccountsInBkSummary(status, resulttable) {
	if (status == 400) {
		fromData = [];
		
		loadBankListForTransfers(resulttable["BankList"]);// transfer REdesign calling bank list in biggining and string in global variable
		
		var allowed=false;
		if (resulttable["opstatus"] == 0) {
			var resuls = resulttable["custAcctRec"];
			var setyourIDTransfer = resulttable["setyourIDTransfer"];
			ITMX_TRANSFER_ENABLE = resulttable["ITMX_TRANSFER_ENABLE"];
          	ITMX_TRANSFER_EWALLET_ENABLE = resulttable["ITMX_TRANSFER_EWALLET_ENABLE"];
			ITMX_TRANSFER_FEE_LIMITS = resulttable["ITMX_TRANSFER_FEE_LIMITS"];
			
			var length = resuls.length;
			var count = 0;
			gblshotcutToACC=false;
			gblTransfersOtherAccounts = []
			gblFinancialTxnIBLock  =resulttable.enableMaintainenceFinancialTxnIB;
			gblNoOfFromAcs = resulttable.custAcctRec.length;
			
			/*** below are configurable params driving from Backend and get Cached. **/
			gblMaxTransferORFT = resulttable.ORFTTransLimit;
			gblMaxTransferSMART = resulttable.SMARTTransLimit;
			gblTransORFTSplitAmnt = resulttable.ORFTTransSplitAmnt;
			gblTransSMARTSplitAmnt = resulttable.SMARTTransAmnt
			gblLimitORFTPerTransaction = resulttable.ORFTTransSplitAmnt;
			gblLimitSMARTPerTransaction = resulttable.SMARTTransAmnt
			var ORFTRange1Lower = resulttable.ORFTRange1Lower
			var ORFTRange1Higher = resulttable.ORFTRange1Higher
			var SMARTRange1Higher = resulttable.SMARTRange1Higher
			var SMARTRange1Lower = resulttable.SMARTRange1Lower
			var ORFTRange2Lower = resulttable.ORFTRange2Lower
			var ORFTRange2Higher = resulttable.ORFTRange2Higher
			var SMARTRange2Higher = resulttable.SMARTRange2Higher
			var SMARTRange2Lower = resulttable.SMARTRange2Lower
			var ORFTSPlitFeeAmnt1 = resulttable.ORFTSPlitFeeAmnt1
			var ORFTSPlitFeeAmnt2 = resulttable.ORFTSPlitFeeAmnt2
			var SMARTSPlitFeeAmnt1 = resulttable.SMARTSPlitFeeAmnt1
			var SMARTSPlitFeeAmnt2 = resulttable.SMARTSPlitFeeAmnt2
			var WithinBankOwnAccLimit =resulttable.WithinBankOwnAccLimit
			var WithinBankOwnAccLimitTransaction =resulttable.WithinBankOwnAccLimitTransaction
			
			gblXerSplitData = [];
			var temp1 = [];
			var temp = {
				ORFTRange1Lower: resulttable.ORFTRange1Lower,
				ORFTRange1Higher: resulttable.ORFTRange1Higher,
				SMARTRange1Higher: resulttable.SMARTRange1Higher,
				SMARTRange1Lower: resulttable.SMARTRange1Lower,
				ORFTRange2Lower: resulttable.ORFTRange2Lower,
				ORFTRange2Higher: resulttable.ORFTRange2Higher,
				SMARTRange2Higher: resulttable.SMARTRange2Higher,
				SMARTRange2Lower: resulttable.SMARTRange2Lower,
				ORFTSPlitFeeAmnt1: resulttable.ORFTSPlitFeeAmnt1,
				ORFTSPlitFeeAmnt2: resulttable.ORFTSPlitFeeAmnt2,
				SMARTSPlitFeeAmnt1: resulttable.SMARTSPlitFeeAmnt1,
				SMARTSPlitFeeAmnt2: resulttable.SMARTSPlitFeeAmnt2,
				WithinBankOwnAccLimit:resulttable.WithinBankOwnAccLimit,
				WithinBankOwnAccLimitTransaction:resulttable.WithinBankOwnAccLimitTransaction
			}
			kony.table.insert(temp1, temp)
			gblXerSplitData = temp1;
			/*** till hereee  ***/
			for (j = 0; j < length; j++) {
				//availableBalance+=parseFloat(resultable["custAcctRec"][j]["availableBal"]);
					 if (resulttable.custAcctRec[j].personalisedAcctStatusCode == "01" || resulttable.custAcctRec[j].personalisedAcctStatusCode == undefined || resulttable.custAcctRec[j].personalisedAcctStatusCode == "")
				{
				var availableBalance = resulttable["custAcctRec"][j]["availableBal"] + " " + kony.i18n.getLocalizedString(
					"currencyThaiBaht")
				//Filtering joint accounts 
				var jointActXfer = resulttable.custAcctRec[j].partyAcctRelDesc
				if (jointActXfer == "PRIJNT" || jointActXfer == "SECJNT" || jointActXfer == "OTHJNT" || jointActXfer == "SECJAN") {
					var temp = [{
						custName: resulttable.custAcctRec[j].accountName,
						acctNo: addHyphenIB(resulttable.custAcctRec[j].accId),
						nickName: resulttable.custAcctRec[j].acctNickName,
						productName: resulttable.custAcctRec[j].productNmeEN,
						bankCD:resulttable.OtherAccounts[i].bankCD
                    }]
					kony.table.insert(gblTransfersOtherAccounts, temp[0])
				} else {
					// code for product icons in custom widgets
						var icon = "";
                  		var randomnum = Math.floor((Math.random() * 10000) + 1);
						icon="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+resulttable["custAcctRec"][j]["ICON_ID"]+"&modIdentifier=PRODICON" + randomnum;
					// end of the code for product icons in custom widgets
					/*
					 * Account number formating
					 */
					var acctID = resulttable.custAcctRec[j].accId;
					if (acctID.length == 14) {
						var frmId = "";
						for (var k = 4; k < 14; k++) {
							frmId = frmId + acctID[k];
						}
						acctID = frmId;
					}
					//end of account formating
                  
                  //Checking PromptPay Eligible for transfer short cut from account summary
                  if ((addHyphenIB(acctID) == gblAccountTable["custAcctRec"][gblIndex]["accountNoFomatted"]) && gblAccountTable["custAcctRec"][gblIndex].acctStatus.indexOf("Active") >= 0) {
                    	promptPayAccountFlag = resulttable.custAcctRec[j].anyIdAllowed == "Y";
                  }
                  
					var accountName = resulttable["custAcctRec"][j]["acctNickName"];
					if (accountName == "" || accountName == undefined) {
						var acctnum = acctID;
						acctnum = acctnum.substr(6, 4);
						accountName = resulttable["custAcctRec"][j]["productNmeEN"] + " " + acctnum;
					}
					//checking for Remaining Fee transactions	
					var lblRem = "";
					var remanFee = "";
					if (gblSMART_FREE_TRANS_CODES.indexOf(resulttable.custAcctRec[j].productID) >= 0) {
						lblRem = kony.i18n.getLocalizedString("keyremainingFree") + ":";
						remanFee = resulttable["custAcctRec"][j]["remainingFee"];
					} else {
						lblRem = "";
						remanFee = "";
					}
					if (count == 0) {
						fromData.push({
							lblProduct: kony.i18n.getLocalizedString("Product"),
							lblProductVal: resulttable["custAcctRec"][j]["productNmeEN"],
							lblACno: kony.i18n.getLocalizedString("AccountNo"),
							lblRemFee: lblRem,
							lblRemFeeval: remanFee,
							img1: icon,
							lblCustName: accountName,
							lblBalance: commaFormatted(resulttable["custAcctRec"][j]["availableBal"]) + " " + kony.i18n.getLocalizedString(
								"currencyThaiBaht"),
							lblActNoval: addHyphenIB(acctID),
							lblSliderAccN1: kony.i18n.getLocalizedString("Product") + ".:",
							lblSliderAccN2: resulttable["custAcctRec"][j]["accType"],
							lblDummy: resulttable["custAcctRec"][j]["fiident"],
							remainingFee: resulttable["custAcctRec"][j]["remainingFee"],
							prodCode: resulttable["custAcctRec"][j]["productID"],
							accountWOF: addHyphenIB(resulttable["custAcctRec"][j]["accId"]),
							nickName: resulttable["custAcctRec"][j]["acctNickName"],
							custAcctName:resulttable["custAcctRec"][j]["accountName"],
                            isOtherBankAllowed:resulttable["custAcctRec"][j]["isOtherBankAllowed"],
                            isOtherTMBAllowed:resulttable["custAcctRec"][j]["isOtherTMBAllowed"],
                            isAllowedSA:resulttable["custAcctRec"][j]["allowedSA"],
                            isAllowedCA:resulttable["custAcctRec"][j]["allowedCA"],
                            isAllowedTD:resulttable["custAcctRec"][j]["allowedTD"]
							
						})
						count = count + 1;
					} else if (count == 1) {
						fromData.push({
							lblProduct: kony.i18n.getLocalizedString("Product"),
							lblProductVal: resulttable["custAcctRec"][j]["productNmeEN"],
							lblACno: kony.i18n.getLocalizedString("AccountNo"),
							lblRemFee: lblRem,
							lblRemFeeval: remanFee,
							img1: icon,
							lblCustName: accountName,
							lblBalance: commaFormatted(resulttable["custAcctRec"][j]["availableBal"]) + " " + kony.i18n.getLocalizedString(
								"currencyThaiBaht"),
							lblActNoval: addHyphenIB(acctID),
							lblSliderAccN1: kony.i18n.getLocalizedString("Product") + ".:",
							lblSliderAccN2: resulttable["custAcctRec"][j]["accType"],
							lblDummy: resulttable["custAcctRec"][j]["fiident"],
							remainingFee: resulttable["custAcctRec"][j]["remainingFee"],
							prodCode: resulttable["custAcctRec"][j]["productID"],
							accountWOF: addHyphenIB(resulttable["custAcctRec"][j]["accId"]),
							nickName: resulttable["custAcctRec"][j]["acctNickName"],
							custAcctName:resulttable["custAcctRec"][j]["accountName"],
                            isOtherBankAllowed:resulttable["custAcctRec"][j]["isOtherBankAllowed"],
                            isOtherTMBAllowed:resulttable["custAcctRec"][j]["isOtherTMBAllowed"],
                            isAllowedSA:resulttable["custAcctRec"][j]["allowedSA"],
                            isAllowedCA:resulttable["custAcctRec"][j]["allowedCA"],
                            isAllowedTD:resulttable["custAcctRec"][j]["allowedTD"]
						})
						count = count + 1;
					} else if (count == 2) {
						fromData.push({
							lblProduct: kony.i18n.getLocalizedString("Product"),
							lblProductVal: resulttable["custAcctRec"][j]["productNmeEN"],
							lblACno: kony.i18n.getLocalizedString("AccountNo"),
							lblRemFee: lblRem,
							lblRemFeeval: remanFee,
							img1: icon,
							lblCustName: accountName,
							lblBalance: commaFormatted(resulttable["custAcctRec"][j]["availableBal"]) + " " + kony.i18n.getLocalizedString(
								"currencyThaiBaht"),
							lblActNoval: addHyphenIB(acctID),
							lblSliderAccN1: kony.i18n.getLocalizedString("Product") + ".:",
							lblSliderAccN2: resulttable["custAcctRec"][j]["accType"],
							lblDummy: resulttable["custAcctRec"][j]["fiident"],
							remainingFee: resulttable["custAcctRec"][j]["remainingFee"],
							prodCode: resulttable["custAcctRec"][j]["productID"],
							accountWOF: addHyphenIB(resulttable["custAcctRec"][j]["accId"]),
							nickName: resulttable["custAcctRec"][j]["acctNickName"],
							custAcctName:resulttable["custAcctRec"][j]["accountName"],
                            isOtherBankAllowed:resulttable["custAcctRec"][j]["isOtherBankAllowed"],
                            isOtherTMBAllowed:resulttable["custAcctRec"][j]["isOtherTMBAllowed"],
                            isAllowedSA:resulttable["custAcctRec"][j]["allowedSA"],
                            isAllowedCA:resulttable["custAcctRec"][j]["allowedCA"],
                            isAllowedTD:resulttable["custAcctRec"][j]["allowedTD"]
						})
						count = 0;
					}
				}
			}
			   }//end of checking for accounts status
			for (var i = 0; i < resulttable.OtherAccounts.length; i++) {
				var prdCode = resulttable.OtherAccounts[i].productID;
				
				
				var otherAccID =resulttable.OtherAccounts[i].accId;
				
				if (otherAccID.length == 14) {
						var frmId = "";
						for (var k = 4; k < 14; k++) {
							frmId = frmId + otherAccID[k]
						}
						otherAccID = frmId;
					}
				if (prdCode == 290) {
					var temp = [{
						custName: resulttable.OtherAccounts[i].accountName,
						acctNo: addHyphenMB(otherAccID),
						nickName: resulttable.OtherAccounts[i].acctNickName,
						productName: resulttable.OtherAccounts[i].productNmeEN
                    }]
					kony.table.insert(gblTransfersOtherAccounts, temp[0]);
				}
			}
			
			
			frmIBTransferCustomWidgetLP.custom1016496273208904.data = fromData;
			/*** decide whether the current selected from is TD accnt */
			frmIBTransferCustomWidgetLP.custom1016496273208904.onSelect = customWidgetSelectEventIBTransfer;
			var selectedItemTmp =0;
			for(var i=0;i<fromData.length;i++){
					if((fromData[i].lblActNoval == gblAccountTable["custAcctRec"][gblIndex]["accountNoFomatted"])&&
					gblAccountTable["custAcctRec"][gblIndex].acctStatus.indexOf("Active") >=0)
					{
						selectedItemTmp = i;					
						frmIBTransferCustomWidgetLP.custom1016496273208904.selectedItem=i;		
						allowed=true;				
						break;
					}
			}	
			if(!allowed){
				dismissLoadingScreenPopup()
				alert(kony.i18n.getLocalizedString("keyNotAllloedTransactions"));
				return false;
			}
			if(gblshorCutToAccounts.indexOf(gblAccountTable["custAcctRec"][gblIndex]["productID"])>= 0){
				//fromData.splice(frmIBTransferCustomWidgetLP.custom1016496273208904.selectedItem,1);
				frmIBTransferCustomWidgetLP.custom1016496273208904.selectedItem=0;
				frmIBTransferCustomWidgetLP.custom1016496273208904.data = fromData;
				setTabBankAccountOrMobile(false);
              
              frmIBTransferCustomWidgetLP.hbxCustomWidget.setVisibility(true);
              frmIBTransferCustomWidgetLP.hbxTMBImage.setVisibility(false);
              frmIBTransferCustomWidgetLP.imgTransferType.setVisibility(true);
             
              //for TD Default Transfer Option is To Account
              gblSelTransferMode = 1;
              frmIBTransferCustomWidgetLP.lblSelectTransfer.text = kony.i18n.getLocalizedString("MIB_P2PToAcctLabel");
              frmIBTransferCustomWidgetLP.imgTransferType.src = "icon_to_account.png";
					 
				frmIBTransferCustomWidgetLP.show();
				frmIBTransferCustomWidgetLP.lblXferToNameRcvd.text= fromData[selectedItemTmp]["nickName"];
				frmIBTransferCustomWidgetLP.lblXferToContactRcvd.text=gblAccountTable["custAcctRec"][gblIndex]["accountNoFomatted"];
				frmIBTransferCustomWidgetLP.lblXferToBankNameRcvd.text=kony.i18n.getLocalizedString("keyTMBBank");
				frmIBTransferCustomWidgetLP.imgXferToImage.src=gblMyProfilepic;
				frmIBTransferCustomWidgetLP.imgXferToImage.isVisible=true;
				frmIBTransferCustomWidgetLP.imgXferToImage.isVisible = true; 
				//frmIBTranferLP.imgXferToImage.isVisible=true;
				gblshotcutToACC=true;
			}else{
				gblshotcutToACC=false;
				//TMBUtil.DestroyForm(frmIBTranferLP);
				var selectedItem = frmIBTransferCustomWidgetLP.custom1016496273208904.selectedItem;
				gblCWSelectedItem=selectedItem;
				gblcwselectedData = frmIBTransferCustomWidgetLP.custom1016496273208904.data[selectedItemTmp];			
				
				dismissLoadingScreenPopup();
                frmIBTranferLP.lblSelectTransfer.text = kony.i18n.getLocalizedString("MIB_P2PTransferOption");
                frmIBTranferLP.imgTransferType.setVisibility(false);
                showFromAccountsfrmIBTranferLP(false);
                   
				frmIBTranferLP.show();
				setSelTabBankAccountOrMobile();
				closeRightPanelTransfer();
					if (gblcwselectedData == undefined) {
						frmIBTranferLP.lblTranLandFromNum.text = "undefined";
					} else {
							
                      		displayTxtMobileP2P(false);
							displayHbxTo(true);
                      		displayITMXFeeP2P(false);
							frmIBTranferLP.btnXferShowContact.setVisibility(false);
							frmIBTranferLP.btnXferShowMobileContact.setVisibility(false);                        
                            displayBtnSchedule("frmIBTranferLP", false); 
                      		
                      		if(gblcwselectedData.prodCode == 206 || !promptPayAccountFlag)
                            {
                                //for Dreamsaving and From Account not eligible for Promptpay then Default Transfer Option is To Account
                                gblSelTransferMode = 1;
                                showFromAccountsfrmIBTranferLP(true);
                                frmIBTranferLP.lblSelectTransfer.text = kony.i18n.getLocalizedString("MIB_P2PToAcctLabel");
                                frmIBTranferLP.imgTransferType.src = "icon_to_account.png";
                                frmIBTranferLP.imgTransferType.setVisibility(true);
								frmIBTranferLP.btnXferShowContact.setVisibility(true);
                               	displayBtnSchedule("frmIBTranferLP", true);
                            }
                      		
                      
					    	if(gblSMART_FREE_TRANS_CODES.indexOf(gblcwselectedData.prodCode) >= 0){
							frmIBTranferLP.hbxFreeTransactions.setVisibility(true);
							frmIBTransferNowConfirmation.hbxFreeTransactions.setVisibility(true);
							frmIBTransferNowCompletion.hbxFreeTransactions.setVisibility(true);		
							frmIBTranferLP.lblremFreeTran.text=kony.i18n.getLocalizedString("keyremainingFree");
							frmIBTransferNowConfirmation.lblremFreeTran.text=kony.i18n.getLocalizedString("keyremainingFree");
							frmIBTransferNowCompletion.lblremFreeTran.text=kony.i18n.getLocalizedString("keyremainingFree");
							frmIBTranferLP.lblremFreeTranVAlue.text=gblcwselectedData.remainingFee;
							frmIBTransferNowConfirmation.lblremFreeTranVAlue.text=gblcwselectedData.remainingFee;
							frmIBTransferNowCompletion.lblremFreeTranVAlue.text=(parseFloat(gblcwselectedData.remainingFee-1))<0 ? 0:(parseFloat(gblcwselectedData.remainingFee) -1).toString();
						  }else{
							frmIBTranferLP.hbxFreeTransactions.setVisibility(false);
							frmIBTransferNowConfirmation.hbxFreeTransactions.setVisibility(false);
							frmIBTransferNowCompletion.hbxFreeTransactions.setVisibility(false);		
						  }
							frmIBTransferNowConfirmation.image247327596551482.src = gblcwselectedData.img1;
							frmIBTransferNowCompletion.image247327596551482.src = gblcwselectedData.img1;
							frmIBTransferNowConfirmation.lblBeforBal.text = gblcwselectedData.lblBalance;
							frmIBTranferLP.lblXferFromNameRcvd.text = gblcwselectedData.lblCustName;
							frmIBTranferLP.lblTranLandFromNum.text = gblcwselectedData.accountWOF;
							frmIBTranferLP.lblTranLandFromAccntType.text=gblcwselectedData.custAcctName; //gblCustomerName; //gblcwselectedData.lblProductVal;
							frmIBTranferLP.lblTranLandFromBalanceAmtVal.text=gblcwselectedData.lblBalance;
							frmIBTranferLP.imageXferFrmLP.src=gblcwselectedData.img1;
							gbltdFlag = gblcwselectedData.lblSliderAccN2;
							productCode =fromData[selectedItem].prodCode;
							
                      		toggleTDMaturityComboboxIB(gbltdFlag);
                      
						}
					}
			
				dismissLoadingScreenPopup();
				nameofform.btnMenuTransfer.skin = "btnIBMenuTransferFocus";
		
		} else {
			dismissLoadingScreenPopup();
			alert(kony.i18n.getLocalizedString("keyIBNtwrkFail"));
			frmIBPreLogin.show();
		}
	}
}		

function getBeneficaryDetailsIB(){
	showLoadingScreenPopup();
	var inputparam = {};
	inputparam["acctId"] = gblAccountTable["custAcctRec"][gblIndex]["accId"];
	invokeServiceSecureAsync("beneficiaryInq", inputparam, getBeneficaryDetailsIBCallback);
}

function getBeneficaryDetailsIBCallback(status,resulttable){
	if(status == 400){
		if(resulttable["opstatus"]==0){
			if(resulttable["StatusCode"] == "0"){				
				if (resulttable["neverSetBeneficiary"] != undefined){
					gblNeverSetBeneficiary=resulttable["neverSetBeneficiary"];
				}else{
					gblNeverSetBeneficiary="";
				}				
				//inq service is success
				if(resulttable["beneficiaryDS"].length >0){
					gblBeneficiaryData = resulttable["beneficiaryDS"];
					handleSavingCareIBDetails(true);
				}else{
					handleSavingCareIBDetails(false);
				}
			}else{
				dismissLoadingScreenPopup();
				//error case when service fail
			}
		}
	}
}
 
function handleSavingCareIBDetails(hasData){
	frmIBAccntSummary.hbxBeneficiaryTitle.isVisible=true;
	frmIBAccntSummary.hbxEditBeneficiaryLink.isVisible=true;
	frmIBAccntSummary.linkEditBeneficiary.text=kony.i18n.getLocalizedString("keyBeneficiaryEdit");
	if (gblAccountTable["custAcctRec"][gblIndex]["openingMethod"] != undefined) {
		gblOpeningMethod=gblAccountTable["custAcctRec"][gblIndex]["openingMethod"];
	}else{		
		gblOpeningMethod="";
	}	
	if(hasData){
		frmIBAccntSummary.hbxBeneficiaryText.isVisible=true;
		frmIBAccntSummary.hbxBeneficiaryNotSpecified.isVisible=false;
		frmIBAccntSummary.lblBeneficiarySpecify.text=appendColon(kony.i18n.getLocalizedString("keyBeneficiaries"))+" "+kony.i18n.getLocalizedString("keyBeneficiariesSpecify");
		frmIBAccntSummary.lblSegBeneficiaryName.text=kony.i18n.getLocalizedString("keyBeneficiaryName");
		frmIBAccntSummary.lblSegBeneficiaryRelation.text=kony.i18n.getLocalizedString("keyBeneficiaryRelationship");
		frmIBAccntSummary.lblSegBeneficiaryBenefit.text=kony.i18n.getLocalizedString("keyBeneficiaryBenefit");
		var locale = kony.i18n.getCurrentLocale();
		var relationValue="";
		
		var segBeneficiarydata=[];
		for (var i=0;i<gblBeneficiaryData.length;i++){
			if("th_TH" == locale){
				relationValue = gblBeneficiaryData[i].relationTH;
			}else{
				relationValue = gblBeneficiaryData[i].relationEN;
			}
			var tempSegmentRecord = {
				"lblSegBeneficiaryNameValue":gblBeneficiaryData[i].fullName,
				"lblSegBeneficiaryRelationValue":relationValue,
				"lblSegBeneficiaryBenefitValue":gblBeneficiaryData[i].percentValue
			};
			segBeneficiarydata.push(tempSegmentRecord);
		}
		frmIBAccntSummary.segBeneficiaryData.setData(segBeneficiarydata);
	}else{	
		frmIBAccntSummary.hbxBeneficiaryText.isVisible=false;
		frmIBAccntSummary.hbxBeneficiaryNotSpecified.isVisible=true;
		frmIBAccntSummary.segBeneficiaryData.setData([]);
		if(kony.string.equalsIgnoreCase(gblAccountTable["custAcctRec"][gblIndex]["openingMethod"],"BRN") && kony.string.equalsIgnoreCase(gblNeverSetBeneficiary, "Y")){
			frmIBAccntSummary.lblBeneficiaryNotSpecified.text=kony.i18n.getLocalizedString("keyBeneficiaryNoDataBranch");
			frmIBAccntSummary.lblBeneficiarySpecify.text=appendColon(kony.i18n.getLocalizedString("keyBeneficiaries"));
		}else{
			frmIBAccntSummary.lblBeneficiaryNotSpecified.text=kony.i18n.getLocalizedString("keyBeneficiariesNotSpecifyDetail");
			frmIBAccntSummary.lblBeneficiarySpecify.text=appendColon(kony.i18n.getLocalizedString("keyBeneficiaries"))+" "+kony.i18n.getLocalizedString("keyBeneficiariesNotSpecify");
		}
	}
	dismissLoadingScreenPopup();
}