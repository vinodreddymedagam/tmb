gblRegActivationIndex = 0;
function frmRegActivationSuccessInit(){
  cacheBasedActivationFlow = true;
  frmRegActivationSuccess.preShow = frmRegActivationSuccessPreshow;
  frmRegActivationSuccess.postShow = frmRegActivationSuccessPostshow;
  frmRegActivationSuccess.onDeviceBack = disableBackButton;
}

function frmRegActivationSuccessPreshow() {
  frmRegActivationSuccess.segRegActivation.pageSkin=noSkinSegmentRow;
  frmRegActivationSuccess.segRegActivation.selectedIndex = [0, 0];
  gblRegActivationIndex = 0;

  frmRegActivationSuccess.btnNext.onClick = onClickRegActivationNext;
  frmRegActivationSuccess.btnStart.onClick = callServiceStartMB;
  
  frmRegActivationSuccessSetVisible();
}

function frmRegActivationSuccessPostshow(){
    assignGlobalForMenuPostshow();  
}

function onClickRegActivationNext(){
  gblRegActivationIndex += 1;
  frmRegActivationSuccess.segRegActivation.selectedIndex = [0, gblRegActivationIndex];
  frmRegActivationSuccessSetVisible();
}
function frmRegActivationSuccessSetVisible(){

  if (gblRegActivationIndex == 0) {
    frmRegActivationSuccess.lblHdrTxt.text=kony.i18n.getLocalizedString("FIN01_txtTittle");
    frmRegActivationSuccess.btnNext.text=kony.i18n.getLocalizedString("FIN01_btnNext");
  }else if (gblRegActivationIndex == 1) {
    frmRegActivationSuccess.lblHdrTxt.text=kony.i18n.getLocalizedString("FIN02_txtTittle");
    frmRegActivationSuccess.btnNext.text=kony.i18n.getLocalizedString("FIN02_btnNext");
  }else if (gblRegActivationIndex == 2) {
    frmRegActivationSuccess.lblHdrTxt.text=kony.i18n.getLocalizedString("FIN03_txtTittle");
  }

  if(gblRegActivationIndex == 2){
    frmRegActivationSuccess.btnStart.isVisible = true;  
    frmRegActivationSuccess.btnNext.isVisible = false; 
    frmRegActivationSuccess.flexFooterLine.isVisible = false;
  }else{
    frmRegActivationSuccess.btnStart.isVisible = false;  
    frmRegActivationSuccess.btnNext.isVisible = true; 
    frmRegActivationSuccess.flexFooterLine.isVisible = true;
  }
}
function getTutorialForRegActivationSuccess(){}
getTutorialForRegActivationSuccess.prototype.paginationSwitch = function (sectionIndex, rowIndex) {
  try{
    gblRegActivationIndex = parseFloat(rowIndex);
    frmRegActivationSuccess.segRegActivation.selectedIndex = [0, gblRegActivationIndex];
    frmRegActivationSuccessSetVisible();
  } catch(e){
    kony.print("Error getTutorialForRegActivationSuccess "+e.stack);
  }
};