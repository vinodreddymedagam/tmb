//Type your code here
 gbleKYCflow=false;
 gbleKYCStep="1"; // 2,3

function checkUserConnectivityeKYC() {
  loadFunctionalModuleSync("eKYCOpenAcct");
  gbleKYCflow=true;
  if(isActiveNetworkAvailable() === false){
     retryInActivationNetworkCheck();
  }else if(isNetworkType3G() === true){
      gblRetryCountRequestOTP = "0";
      showLoadingScreen();
      checkMobileNumberServlet();  
  }else{
	  activationAlertForSettings();
  }
}

function showMobileProviderNo(){
  
  checkUserConnectivityeKYC();
  //frmMBeKYCVerifyMobile.show();
}

function preShowfrmMBeKYCVerifyMobile(){
//   frmMBeKYCVerifyMobile.postShow = postShowfrmMBeKYCVerifyMobile;
  setHeaderContentToForm(kony.i18n.getLocalizedString("eKYC_TitleSetupTMBTouch"),null,false,gbleKYCStep);
  setFooterSinglebtnToForm(kony.i18n.getLocalizedString("btnNext"),doPartyInqWithMobileAndDOPACheck);
  var mobileNo=mobileNumberFromUserDevice;
  mobileNo=mobileNo.substring(0, 3) +"-" +mobileNo.substring(3, 6)+"-" +mobileNo.substring(6, 10);
  frmMBeKYCVerifyMobile.lblMobileNo.text = mobileNo;
  frmMBeKYCVerifyMobile.lblMobile.text = kony.i18n.getLocalizedString("eKYC_CheckMobileNumger");
  frmMBeKYCVerifyMobile.lblMobileNoDesc.text = kony.i18n.getLocalizedString("eKYC_msgNotYrMobileNo");
}

// function postShowfrmMBeKYCVerifyMobile(){

// }

function doPartyInqWithMobileAndDOPACheck(){
  //Need to send mobileno, citizen Id issueDate, citizen Id expiry Date and laser code
  	var inputParam = {};
  kony.print("Calling service ekycPartyDOPAJavaService");
  inputParam.mobileNo = mobileNumberFromUserDevice;
  inputParam.citizenIdDOI = frmMBeKYCCitezenIDInfo.calIssueDate.day+"/"+frmMBeKYCCitezenIDInfo.calIssueDate.month+"/"+frmMBeKYCCitezenIDInfo.calIssueDate.year;
  //Shail New
  inputParam.citizenIdDOE = frmMBeKYCCitezenIDInfo.calExpireDate.day+"/"+frmMBeKYCCitezenIDInfo.calExpireDate.month+"/"+frmMBeKYCCitezenIDInfo.calExpireDate.year;
  var txtlaserCode = frmMBeKYCCitezenIDInfo.txtLaserCode.text;
  var txtlaserCode = replaceAll(txtlaserCode, "-", "");
  inputParam.laserCode = txtlaserCode;
    showLoadingScreen();
    invokeServiceSecureAsync("ekycPartyDOPAJavaService", inputParam, callBackPartyInqWithMobileAndDOPACheck);
}

function callBackPartyInqWithMobileAndDOPACheck(status, result){
  if (status == 400) {
   		if (result.opstatus === "0" || result.opstatus === 0) {
          var ekycexpirydays = result.ExpiryDays;
          gblEKYCFlowExpireDays = ekycexpirydays;
//           alert(JSON.stringify(result));
          setHeaderContentToForm(kony.i18n.getLocalizedString("eKYC_TitleSetupTMBTouch"),null,false,gbleKYCStep);
          setFooterSinglebtnToForm(kony.i18n.getLocalizedString("btnNext"),doPartyInqWithMobileAndDOPACheck);

          frmMBeKYCVerifyMobile.flexBodyScroll.setVisibility(true);
          dismissLoadingScreen();
          showcreatePINIntro();
        } else {
          frmEkycMobileNoErrorScreen.preShow = ekycErrMsgPreShow;
          frmEkycMobileNoErrorScreen.postShow = postShowfrmEkycMobileNoErrorScreen;
          if(result.errCode === "eKYCErrAF3"){
          showAlertWithCallBack(kony.i18n.getLocalizedString("eKYC_msgExistingTMBCustomer"), kony.i18n.getLocalizedString("info"),btnTnCBackeKYC);
          }else if(result.errCode === "eKYCErrAF4"){
             var msg = kony.i18n.getLocalizedString("eKYC_msgDOPAfailed");
             var title = kony.i18n.getLocalizedString("info");
             var okk = kony.i18n.getLocalizedString("Btn_Retry");
             var Noo = kony.i18n.getLocalizedString("WIFI_btnCancel");
             showAlertWithYesNoHandler(msg,title,okk,Noo,retryCheck, doNothing);
          }else if(result.errCode === "eKYCErrAF5"){
            frmEkycMobileNoErrorScreen.show();
          }else{
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
          }
          //If record found in partyInq : displays popup error message 13_003 with OK button
          //Press on OK button; dismiss the popup and navigates to "Activate TMB Touch" screen
          dismissLoadingScreen();
//           alert(JSON.stringify(result));
          
          return false;
        }
  }
}

function ekycErrMsgPreShow(){
    setHeaderContentToForm(kony.i18n.getLocalizedString("Receipent_alert_Error"),null,false,gbleKYCStep);
    frmEkycMobileNoErrorScreen.lblTryAgain.text = kony.i18n.getLocalizedString("eKYC_msgReachMaximumAttempts");
    frmEkycMobileNoErrorScreen.lblMessage.text = kony.i18n.getLocalizedString("eKYC_msgDOPAfailedReachAttempt");
}

function postShowfrmEkycMobileNoErrorScreen(){
  animateIdAndSelfieImages(frmEkycMobileNoErrorScreen.flxArrow,45);
}

function retryCheck(){
  doPartyInqWithMobileAndDOPACheck();
}

function showcreatePINIntro(){
  frmMBeKYCCreatePinIntro.show();
}

function showAccessPINeKYC(){
   frmMBeKYCAccessPin.show();
}

function btnTnCBackeKYC(){
  frmeKYCStartUp.show();
}


function btnTnCNexteKYC() {
  // frmMBeKYCIntroduction.btnNext.onClick=btnNexteKYCIntro;
  var nfcFlag = isNFCEnabled();
  if (null === nfcFlag) {
     gblEKYCFlowCIDFlag = true;
    //#ifdef iphone
    showAlert(kony.i18n.getLocalizedString("eKYC_msgDeviceNotSupporteKYCflow"), kony.i18n.getLocalizedString("info"));
    //#endif
	//#ifdef android
    chkRunTimePermissionsForeKYC_CID();
    //#endif
  } else {
    if (checkNFCSupprt()){
		if (GLOBAL_EKYC_CID_FLAG === "OFF") {
        scanPassport();
      }else {
          gbleKYCStep = "1";
          navigateKYCIntroduction();
        }
    } 
  }
}

function navigateKYCIntroduction() {
    frmMBeKYCIntroduction.show();
}

function openMBActivationForm(){
  //startup = frmMBanking;
  gblCurrentForm = "frmMBanking";
  frmMBanking.show();
}

function changeLocaleeKYC(){
  try{
    var locale = kony.i18n.getCurrentLocale();
    var list;

    if(frmChangeLanguage.btnThai.skin == "btnLangEnabled"){
      kony.print("btnThai enabled@@");
      kony.i18n.setCurrentLocaleAsync("th_TH", onSuccessLocaleChangeNew, onFailureLocaleChange, "");
      list = {
        appLocale: "th_TH"
      }
    } else if(frmChangeLanguage.btnEng.skin == "btnLangEnabled"){
      kony.print("btnEng enabled@@");
      kony.i18n.setCurrentLocaleAsync("en_US", onSuccessLocaleChangeNew, onFailureLocaleChange, "");
      list = {
        appLocale: "en_US"
      }
    }
    kony.store.removeItem("curAppLocale");
    kony.store.setItem("curAppLocale", list);
  }catch(e){
    kony.print("Exception in locale change>>>>"+e);
  }
}

function readUTFFileServiceeKYC() {
    var input_param = {};
    var locale = kony.i18n.getCurrentLocale();
    if (locale == "en_US") {
        input_param["localeCd"] = "en_US";
    } else {
        input_param["localeCd"] = "th_TH";
    }
    var moduleKey = "TMBeKYC";//"TMBeKYC"; 
    input_param["moduleKey"] = moduleKey;
    showLoadingScreen();
    invokeServiceSecureAsync("readUTFFile", input_param, callBackreadUTFFileServiceeKYC);
}

function callBackreadUTFFileServiceeKYC(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
        	kony.print("success");
            frmMBNewTnCLoan.btnRight.setVisibility(false);
            frmMBNewTnCLoan.lblDescSubTitle.setVisibility(false);
            frmMBNewTnCLoan.richtextTnC.text = result["fileContent"];
          	frmMBNewTnCLoan.lblHdrTxt.text = kony.i18n.getLocalizedString("LoanTC_Title");
            dismissLoadingScreen();
            frmMBNewTnCLoan.show();
        } else {
            dismissLoadingScreen();
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}

function btnBackeKYCIntro(){
  frmMBNewTnCLoan.show();
}
function btnNexteKYCIntro(){
 showeKYCCitezenIDInfo();
}

// function preShoweKYCCreatePINIntro(){
//   frmMBeKYCCreatePinIntro.btnNext.onClick=showAccessPINeKYC;
// }

function preShoweKYCSETPIN(){
  frmMBeKYCAccessPin.btnBack.onClick=btnTnCNexteKYC;
}

function showeKYCCitezenIDInfo(){
  frmMBeKYCCitezenIDInfo.show();
}

function setHeaderContentToForm(txtHeader,callbtnBack,isBtnback,isStep){
  flxHeader.lblHdrTxt.text=txtHeader;
  flxHeader.btnMainMenu.isVisible = isBtnback; // mki
  if(isBtnback){
    flxHeader.btnMainMenu.onClick=callbtnBack;
  }
  if(isStep==="1"){
   	  flxHeader.flex1.skin="flexBlueLine";
      flxHeader.flex2.skin="flexBlueLine";
      flxHeader.flex3.skin="flexBlueLine";
  }else if(isStep==="2"){
      flxHeader.flex1.skin="flexGreenCustomRounded";
      flxHeader.flex2.skin="flexBlueLine";
      flxHeader.flex3.skin="flexBlueLine";
  }else if(isStep==="3"){
      flxHeader.flex1.skin="flexGreenCustomRounded";
      flxHeader.flex2.skin="flexGreenCustomRounded";
      flxHeader.flex3.skin="flexBlueLine";
  }else{
      flxHeader.flex1.skin="flexGreenCustomRounded";
      flxHeader.flex2.skin="flexGreenCustomRounded";
      flxHeader.flex3.skin="flexGreenCustomRounded";
  }
  if(gblIDPApproveRequestFlow || gblUpdateIALFlow){
    flxHeader.FlexContainer0a8eb595ab19e44.setVisibility(false);
  }else{
    flxHeader.FlexContainer0a8eb595ab19e44.setVisibility(true);
  }
  
}

function setFooterSinglebtnToForm(txtbtn,callbtnBack){
  flxFooter.btnBack.text=txtbtn;
  flxFooter.btnBack.onClick=callbtnBack;
}

function setFooterTwoVerticalbtnToForm(btntxtblue,btntxtwhite,callbtnblue,callbtnwhite,isbtnblue,isbtnwhite){
  flxbtnBlue.isVisible= isbtnblue;
  flxbtnWhite.isVisible= isbtnwhite;
  if(isbtnblue){
    flxbtnBlue.btnBlue.text=btntxtblue;
  	flxbtnBlue.btnBlue.onClick=callbtnblue;
  }
   if(isbtnwhite){
    flxbtnWhite.btnWhite.text=btntxtwhite;
  	flxbtnWhite.btnWhite.onClick=callbtnwhite;
  }
}

function setFooterTwobtnToForm(txtbtnCancel,txtbtnNext,txtsinglebtnNext,callbtnCancel,callbtnNext,callSinglebtnNext,istwobtn,issinglebtn){
  if(istwobtn){
    flexFooter.setVisibility(true);
    flxSingleBtn.setVisibility(false);
    flexFooter.btnCancel.text=txtbtnCancel;
    flexFooter.btnCancel.onClick=callbtnCancel;
    flexFooter.btnNext.text=txtbtnNext;
    flexFooter.btnNext.onClick=callbtnNext;
  }
   if(issinglebtn){
    flxSingleBtn.setVisibility(true);
    flexFooter.setVisibility(false);
    flxSingleBtn.btnSingle.text=txtsinglebtnNext;
  	flxSingleBtn.btnSingle.onClick=callSinglebtnNext;
  }
}

function initfrmMBeKYCCreatePinIntro(){
  frmMBeKYCCreatePinIntro.preShow=preShowfrmMBeKYCCreatePinIntro;
  frmMBeKYCCreatePinIntro.onDeviceBack=doNothing;
}

function preShowfrmMBeKYCCreatePinIntro(){
  setHeaderContentToForm(kony.i18n.getLocalizedString("eKYC_TitleSetupTMBTouch"),null,false,gbleKYCStep);
  setFooterSinglebtnToForm(kony.i18n.getLocalizedString("eKYC_btnSetPIN"),showMBeKYCAccessPin);
  frmMBeKYCCreatePinIntro.lblsubheader.text = kony.i18n.getLocalizedString("eKYC_msgNextStepSetPIN");
  frmMBeKYCCreatePinIntro.RichTextInstruction.text = kony.i18n.getLocalizedString("eKYC_msgPINuseFor");
  var pinrules = replaceAll(kony.i18n.getLocalizedString("eKYC_RemarkPIN"), "<Application age>",gblEKYCFlowExpireDays);
  frmMBeKYCCreatePinIntro.RichTextRemarkPin.text = pinrules;
  //frmMBeKYCCreatePinIntro.btnBack.onClick=showAccessPINeKYC;
}