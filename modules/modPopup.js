







/**
 * description
 * @returns {}
 */

//function showConfPopup(imgIcon, confText, callBackOnConfirm) {
//	popupConfirmation.imgPopConfirmIcon.src = imgIcon;
//	popupConfirmation.lblPopupConfText.text = confText;
//	popupConfirmation.btnpopConfConfirm.onClick = callBackOnConfirm;
//	popupConfirmation.show();
//}
/**
 * description
 * @returns {}
 */

//function onConfirmCall() {
//	alert("ConfCalled");
//}
/*
 * description
 * @returns {}
 */

//function showLogoutPopup() {
//	popUpLogout.containerWeight = 94;
//	popUpLogout.show();
//}
/**
 * typeFlag 1 means OTP , 2 means access pin and 3 means transaction pwd
 * @returns {}
 */

function showOTPPopup(lblText, refNo, mobNO, callBackConfirm, typeFlag) {
	//Please DO NOT KEEP THIS FUNCTION OUTSIDE. This is inner function assigned to popupTractPwd.btnPopupTractConf.onClick
	popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = tbxPopupBlue;
	function onConfClick() {
		//popupTractPwd.dismiss();
		var enteredText = "";
		if (typeFlag == 1) {
			//enteredText = popupTractPwd.tbxPopupTractPwdtxt.text;
			enteredText = popupTractPwd.txtOTP.text;
			if (popupTractPwd.hbxIncorrectOTP.isVisible == true) {
				enteredText = popupTractPwd.txtIncorrectOTP.text;
			}
		} else if (typeFlag == 2) {
			enteredText = popupTractPwd.tbxPopupTractPwdtxtAccPin.text;
		} else if (typeFlag == 3) {
			enteredText = popupTractPwd.tbxPopupTractPwdtxtTranscPwd
				.text;
		}
		callBackConfirm(enteredText);
	}
	popupTractPwd.containerWeight = 94;
	popupTractPwd.btnPopupTractConf.onClick = onConfClick;
	popupTractPwd.btnPopupTractConf.text = kony.i18n.getLocalizedString(
		"keyConfirm");
	// popupTractPwd.tbxPopupTractPwdtxt.text = "";
	popupTractPwd.txtOTP.text = "";
	//popupTractPwd.lblPopTractPwdtxt.text = lblText;
	popupTractPwd.lblOTP.text = lblText;
	popupTractPwd.hbxIncorrectOTP.isVisible = false;
	if (typeFlag == 1) {
		popupTractPwd.lblPopupTract1.isVisible = true;
		if (flowSpa) {
			popupTractPwd.hbxPopupTractlblHoldSpa.isVisible = true;
		} else {
			popupTractPwd.hbxPopupTractlblHold.isVisible = true;
		}
		popupTractPwd.lblPopupTract5.text = kony.i18n.getLocalizedString("keyotpmsgreq");
		popupTractPwd.lblPopupTract5.isVisible = true;
		if (flowSpa) {
			popupTractPwd.lblPopupTract4Spa.text = mobNO;
		} else {
			popupTractPwd.lblPopupTract4.text = mobNO;
		}
		popupTractPwd.btnPopUpTractCancel.skin = btnLightBlue;
		popupTractPwd.btnPopUpTractCancel.focusSkin = btnLightBlue;
		popupTractPwd.btnPopUpTractCancel.setEnabled(true);
		popupTractPwd.btnPopUpTractCancel.onClick = onClickAccessPinCancel;
		popupTractPwd.btnPopUpTractCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
		if (kony.i18n.getCurrentLocale() == "th_TH") {
			//popupTractPwd.lblPopTractPwdtxt.containerWeight = 25;
			popupTractPwd.lblOTP.containerWeight = 12;
		} else {
			//popupTractPwd.lblPopTractPwdtxt.containerWeight = 12;
			popupTractPwd.lblOTP.containerWeight = 12;
		}
		//popupTractPwd.lblPopupTract1.text = kony.i18n.getLocalizedString("keybankrefno")+gblRefNum; Commented as added in onClickOTPRequest
		//popupTractPwd.hbxPoupOTP.isVisible = true;
		popupTractPwd.hbxOTP.isVisible = true;
		popupTractPwd.hbxPoupAccesspin.isVisible = false;
		popupTractPwd.hbxPopupTranscPwd.isVisible = false;
		//popupTractPwd.lblPopTractPwdtxt.text = lblText;
		popupTractPwd.lblOTP.text = lblText;
		//gblOTPFlag = true;
		//onClickOTPRequest();
		//popupTractPwd.tbxPopupTractPwdtxt.text = "";
		popupTractPwd.txtOTP.textInputMode = constants.TEXTBOX_INPUT_MODE_NUMERIC;
		
		popupTractPwd.txtOTP.keyBoardStyle = constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD;
		
		if (flowSpa) {
			popupTractPwd.txtOTP.textInputMode = constants.TEXTBOX_INPUT_MODE_ANY;
		}
		if(gblMBActivationVia == "2"){
			// activation via ib login
			popupTractPwd.btnOtpRequest.onClick = activationViaIBUserOTP;
			popupTractPwd.btnOtpRequest.setEnabled(false);
			popupTractPwd.btnOtpRequest.skin=btnDisabledGray;
			popupTractPwd.btnOtpRequest.focusSkin=btnDisabledGray;
			popupTractPwd.btnPopupTractConf.onClick=verifyOTPActivationIBUserOTP;
		}else{
			popupTractPwd.btnOtpRequest.onClick = onClickActiRequestOtp;
		}
	} else if (typeFlag == 2) {
		popupTractPwd.line502709349207.isVisible = false;
		popupTractPwd.lblPopupTract1.isVisible = false;
		if (flowSpa) {
			popupTractPwd.hbxPopupTractlblHoldSpa.isVisible = false;
		} else {
			popupTractPwd.hbxPopupTractlblHold.isVisible = false;
		}
		popupTractPwd.lblPopupTract7.isVisible = false;
		popupTractPwd.lblPopupTract5.isVisible = false;
		popupTractPwd.btnPopUpTractCancel.text = kony.i18n.getLocalizedString(
			"keyCancelButton");
		popupTractPwd.btnPopUpTractCancel.skin = btnLightBlue;
		popupTractPwd.btnPopUpTractCancel.focusSkin = btnLightBlue;
		//popupTractPwd.lblPopTractPwdtxt.containerWeight = 28;
		popupTractPwd.lblOTP.containerWeight = 28;
		//popupTractPwd.hbxPoupOTP.isVisible = false;
		popupTractPwd.hbxOTP.isVisible = false;
		popupTractPwd.tbxPopupTractPwdtxtAccPin.text = "";
		popupTractPwd.hbxPoupAccesspin.isVisible = true;
		popupTractPwd.hbxPopupTranscPwd.isVisible = false;
		popupTractPwd.lblPopupPwdAccPin.text = kony.i18n.getLocalizedString("AccessPin");
		popupTractPwd.lblPopupPwdAccPin.skin = lblPopupLabelTxt;
		popupTractPwd.btnPopUpTractCancel.onClick =
			onClickAccessPinCancel;
		gblOTPFlag = true;
		popupTractPwd.tbxPopupTractPwdtxtAccPin.textInputMode =
			constants.TEXTBOX_INPUT_MODE_NUMERIC;
		popupTractPwd.tbxPopupTractPwdtxtAccPin.keyBoardStyle =
			constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD;
		if ((null != gblDeviceInfo
			.name) && (kony.string.equalsIgnoreCase(
			"android", gblDeviceInfo
			.name))) {
			popupTractPwd.tbxPopupTractPwdtxtAccPin.keyBoardStyle =
				constants.TEXTBOX_NUMERIC_PASSWORD;
		}
	} else if (typeFlag == 3) {
		popupTractPwd.line502709349207.isVisible = false;
		popupTractPwd.lblPopupTract1.isVisible = false;
		if (flowSpa) {
			popupTractPwd.hbxPopupTractlblHoldSpa.isVisible = false;
		} else {
			popupTractPwd.hbxPopupTractlblHold.isVisible = false;
		}
		popupTractPwd.lblPopupTract5.isVisible = false;
		popupTractPwd.lblPopupTract7.isVisible = true;
		popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("keyTransPwdMsg");
		popupTractPwd.lblPopupTract7.skin = lblPopupLabelTxt;
		popupTractPwd.btnPopUpTractCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
		popupTractPwd.btnPopUpTractCancel.skin = btnLightBlue;
		popupTractPwd.btnPopUpTractCancel.focusSkin = btnLightBlue;
		//popupTractPwd.lblPopTractPwdtxt.containerWeight = 53;
		popupTractPwd.lblOTP.containerWeight = 53;
		//popupTractPwd.hbxPoupOTP.isVisible = false;
		popupTractPwd.hbxOTP.isVisible = false;
		popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text = "";
		popupTractPwd.hbxPoupAccesspin.isVisible = false;
		popupTractPwd.hbxPopupTranscPwd.isVisible = true;
		//popupTractPwd.lblPopupTranscPwd.text = lblText;
		popupTractPwd.btnPopUpTractCancel.onClick =
			onClickAccessPinCancel;
		gblOTPFlag = true;
		//popupTractPwd.tbxPopupTractPwdtxt.textInputMode = constants.TEXTBOX_INPUT_MODE_ANY;
		//commenting the lines as it was causing issues in change mobile after any transaction pdw pop up
		//popupTractPwd.txtOTP.textInputMode = constants.TEXTBOX_INPUT_MODE_ANY;
		//popupTractPwd.txtOTP.maxTextLength = gblOTPLENGTH;
	}
	dismissLoadingScreen();
	popupTractPwd.show();
	if(popupTractPwd.hbxOTP.isVisible){
			kony.print("$$$$$$$$$$$$$$$$$"+popupTractPwd.hbxPopupTranscPwd.isVisible)
			popupTractPwd.txtOTP.setFocus(true)
	
	}else if(popupTractPwd.hbxPopupTranscPwd.isVisible){
		popupTractPwd.tbxPopupTractPwdtxtTranscPwd.setFocus(true)
	}else if(popupTractPwd.hbxPoupAccesspin.isVisible){
		popupTractPwd.tbxPopupTractPwdtxtAccPin.setFocus(true)
	}
}

/**
 * 
 *  lblText
 *  callBackConfirmSSL
 *  typeFlag - Used for module differentation in SLL callback
 *  returns {} 
 */
function showOTPSSLPopup(callBackConfirmSSL) {
	//Please DO NOT KEEP THIS FUNCTION OUTSIDE. This is inner function assigned to popupTractPwd.btnPopupTractConf.onClick
	function onConfClick() {
		showLoadingScreen();
		var enteredText = "";
		enteredText = popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text;
		//if (deviceInfo["name"] == "iPhone" || deviceInfo["name"] == "iPhone Simulator") {
		
		/**
		 * The below code is commented, MIB-6454 Analysis of Trusteer code flow
		 * 
		 * We are going to send the KonydeviceId isnted of Trusteer unique id
		 ***/
			callBackFromSSLImpl();
		
		/**
		 * 
		
		//#ifdef iphone
			VerifyTransSSLValidation();
		//}else if (deviceInfo["name"] == "android"){
		//#else
			//#ifdef android
				sslVerifyTransAndroid();
			//#endif
		//#endif
		//callBackConfirm(enteredText);
		
		 ***/
	}
	function callBackFromSSLImpl() {
		dismissLoadingScreen();
		callBackConfirmSSL(popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text);
	}
	function sslVerifyTransAndroid(){
	//Commented for Arxan
		/*if(TmbTrusteerFFIObject == null)
			TmbTrusteerFFIObject = new trust.TmbTrusteerFFI();
		 var url = appConfig.serverIp;
			TmbTrusteerFFIObject.sslValidationAndroid(callbackVerifyTransSSL,url);*/
	}

	function callbackVerifyTransSSL(sslResult) {
	    if(sslResult == 0){
	    	
	    	//callBackFromSSLImpl();
		}else{
			/*
			dismissLoadingScreen();
			showAlertForUniqueGenFail(kony.i18n.getLocalizedString("keySSLValidationFailureMsg"));*/
			//alert(kony.i18n.getLocalizedString("keySSLValidationFailureMsg"));
		}
	    callBackFromSSLImpl();
	}
	function VerifyTransSSLValidation()
	{
		//Commented for Arxan
		/*var sslObj = {};
		sslObj["SSLValidation"] = verifyTransSSLCallBack;
		sslObj["message"] = "yes";
		sslObj["url"] = appConfig.serverIp;
		
		
		ffinamespace.getsslValidation(sslObj);*/
	}
	function verifyTransSSLCallBack(sslresult){
		if(sslresult == 0 || sslresult == "0" || sslresult == 99 || sslresult == "99"){
			
			//callBackFromSSLImpl();
		}else{
			/*
			dismissLoadingScreen();
			showAlertForUniqueGenFail(kony.i18n.getLocalizedString("keySSLValidationFailureMsg"));*/
			//alert(kony.i18n.getLocalizedString("keySSLValidationFailureMsg"));
		}
		callBackFromSSLImpl();
	}
	
	popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = txtFocusBG;
	popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = txtFocusBG;
	popupTractPwd.containerWeight = 94;
	popupTractPwd.btnPopupTractConf.onClick = onConfClick;
	popupTractPwd.btnPopupTractConf.text = kony.i18n.getLocalizedString(
		"keyConfirm");
	// popupTractPwd.tbxPopupTractPwdtxt.text = "";
	popupTractPwd.txtOTP.text = "";
	//popupTractPwd.lblPopTractPwdtxt.text = lblText;
	popupTractPwd.lblOTP.text = kony.i18n.getLocalizedString("transPasswordSub");
	popupTractPwd.hbxIncorrectOTP.isVisible = false;
	popupTractPwd.line502709349207.isVisible = false;
	popupTractPwd.lblPopupTract1.isVisible = false;
	popupTractPwd.hbxPopupTractlblHold.isVisible = false;
	popupTractPwd.lblPopupTract5.isVisible = false;
	popupTractPwd.lblPopupTract7.isVisible = true;
	popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("keyTransPwdMsg");
	popupTractPwd.lblPopupTract7.skin = lblPopupLabelTxt;
	popupTractPwd.btnPopUpTractCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
	popupTractPwd.btnPopUpTractCancel.skin = btnLightBlue;
	popupTractPwd.btnPopUpTractCancel.focusSkin = btnLightBlue;
	//popupTractPwd.lblPopTractPwdtxt.containerWeight = 53;
	popupTractPwd.lblOTP.containerWeight = 53;
	//popupTractPwd.hbxPoupOTP.isVisible = false;
	popupTractPwd.hbxOTP.isVisible = false;
	popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text = "";
	popupTractPwd.hbxPoupAccesspin.isVisible = false;
	popupTractPwd.hbxPopupTranscPwd.isVisible = true;
	//popupTractPwd.lblPopupTranscPwd.text = kony.i18n.getLocalizedString("transPasswordSub");
	popupTractPwd.btnPopUpTractCancel.onClick = onClickAccessPinCancel;
	gblOTPFlag = true;
	//popupTractPwd.tbxPopupTractPwdtxt.textInputMode = constants.TEXTBOX_INPUT_MODE_ANY;
	//commenting the lines of code to fix the text box issue on MB
	//popupTractPwd.txtOTP.textInputMode = constants.TEXTBOX_INPUT_MODE_ANY;
	//popupTractPwd.txtOTP.maxTextLength = gblOTPLENGTH;
	dismissLoadingScreen();
	popupTractPwd.show();
	if(popupTractPwd.hbxOTP.isVisible){
			kony.print("$$$$$$$$$$$$$$$$$"+popupTractPwd.hbxPopupTranscPwd.isVisible)
			popupTractPwd.txtOTP.setFocus(true)
	
	}else if(popupTractPwd.hbxPopupTranscPwd.isVisible){
		popupTractPwd.tbxPopupTractPwdtxtTranscPwd.setFocus(true)
	}else if(popupTractPwd.hbxPoupAccesspin.isVisible){
		popupTractPwd.tbxPopupTractPwdtxtAccPin.setFocus(true)
	}
}

//function onSaveChngAcessPinClick() {
//	popupTractPwd.dismiss();
//	//callBackConfirm(popupTractPwd.tbxPopupTractPwdtxt.text);
//}

//function onCancelChngAcessPinClick() {
//	popupTractPwd.dismiss();
//}
/**
 * description
 * Cancelling the otpTimer
 * @returns {}
 */

function otpTimerCallBack() {
	popupTractPwd.btnOtpRequest.setEnabled(true);
	popupTractPwd.btnOtpRequest.skin=btnLightBlue;
	popupTractPwd.btnOtpRequest.focusSkin=btnLightBlue;
	popupTractPwd.btnOtpRequest.onClick = onClickActiRequestOtp;
	gblOTPFlag = true;
	try {
		kony.timer.cancel("otpTimer")
	} catch (e) {
		
	}
}
/*
*************************************************************************************
		Module	: onClickOTPRequest
		Author  : Kony
		Purpose : Defining onclick for next button in activation confirmation page
****************************************************************************************
*/

function onClickActiConfirm() {
	showLoadingScreen();
	
	gblOTPFlag = true;
/*	gblOTPFlag = false;
	if(gblLastClicked == null) {
		gblLastClicked = getPresentTime();
		gblOTPFlag = true;
	} else if(getPresentTime() - gblLastClicked > 10000){
		gblLastClicked = getPresentTime();
		gblOTPFlag = true;
	} else {
		gblOTPFlag = false;
		dismissLoadingScreen();
	}
	
	*/
	gblOnClickReq = false;
	//Commenting this for DEF11700
	/*
	try {
		kony.timer.cancel("otpTimer")
	} catch (e) {
		
	}
	*/
	if (flowSpa) {
		spaChnage = "activation";
		successFlag=false;
		//onClickOTPRequestSpa();
	onClickOTPSpaAactivate();
	}
	else
	{
		//showLoadingScreen();
		requestOTPActivate();
	}
}

function requestOTPActivate(){
//Added for ENH_207_6 to send uniqueid in Activation Code verification OTP call - function onClickOTPRequest()

	GBL_FLOW_ID_CA=10;
	//#ifdef iphone
		TrusteerDeviceId();
	//#else
		//#ifdef android
			getUniqueID();
		//#endif
	//#endif 
}
/*
*************************************************************************************
		Module	: onClickActiRequestOtp
		Author  : Kony
		Purpose : Defining onclick for Request button for OTP pop up
****************************************************************************************
*/

function onClickActiRequestOtp() {
	gblOTPFlag = true;
	gblOnClickReq = true;
	onClickOTPRequest();
}
/*
*************************************************************************************
		Module	: onClickOTPRequest
		Author  : Kony
		Purpose : Invoking request OTP kony service
****************************************************************************************
*/

function onClickOTPRequest(uniqueid) {
	
	if (gblOTPFlag) {
		//popupTractPwd.btnPopUpTractCancel.skin = btnDisabledGray;
		//popupTractPwd.btnPopUpTractCancel.setEnabled(false);
		//Disabled for ENH_207 and below code implemented
		popupTractPwd.btnOtpRequest.setEnabled(false);
		popupTractPwd.btnOtpRequest.skin=btnDisabledGray;
		popupTractPwd.btnOtpRequest.focusSkin=btnDisabledGray;
		var inputParam = {};
		//inputParam["tokenUUID"] = gblTokenNum
		//inputParam["sessionId"] = "session";
		/*	inputParam["policyId"] = "";
		
		var locale=kony.i18n.getCurrentLocale();
		// For Activation Flow
		if(gblActionCode == "21"){
			if(locale=="en_US"){
				
				inputParam["eventNotificationPolicyId"] = "MIB_OTPActivate_EN";
				inputParam["SMS_Subject"] = "MIB_OTPActivateMB_EN";
			}
			else{
				inputParam["eventNotificationPolicyId"] = "MIB_OTPActivate_TH";
				inputParam["SMS_Subject"] = "MIB_OTPActivateMB_TH";
			} 
		}// For Add device Flow
		else if(gblActionCode == "23"){
			if(locale=="en_US"){
				
				inputParam["eventNotificationPolicyId"] = "MIB_OTPAddDevice_EN";
				inputParam["SMS_Subject"] = "MIB_OTPAddDevice_EN";
			}
			else{
				inputParam["eventNotificationPolicyId"] = "MIB_OTPAddDevice_TH";
				inputParam["SMS_Subject"] = "MIB_OTPAddDevice_TH";
			} 
		}// For Reset Password Flow
		else if(gblActionCode == "22"){
			if(locale=="en_US"){
				
				inputParam["eventNotificationPolicyId"] = "MIB_OTPResetPWD_EN";
				inputParam["SMS_Subject"] = "MIB_OTPResetPWDMB_EN";
			}
			else{
				inputParam["eventNotificationPolicyId"] = "MIB_OTPResetPWD_TH";
				inputParam["SMS_Subject"] = "MIBOTPResetPWDMB_TH";
			} 
		}
		
		
		
		inputParam["retryCounterRequestOTP"] = gblRetryCountRequestOTP;  
	    inputParam["storeId"] = "";
	    inputParam["Batch_No"] = "";
	    inputParam["Bank_Ref"] = "";
	    inputParam["Product_Code"] = "";
	    inputParam["Recipient_Name"] = "";
	    inputParam["MobileNumber"] = gblPHONENUMBER;
	    inputParam["Channel"] = "";
	    inputParam["inputSeed"]="";
		invokeServiceSecureAsync("requestOTP",inputParam,CallbackonClickOTPRequest);*/
		var locale = kony.i18n.getCurrentLocale();
		var spaChannel
		// For Activation Flow
		
		if (flowSpa) //if else condition for SPA
		{
			if (gblActionCode == "11") {
				if (locale == "en_US") {
					inputParam["eventNotificationId"] =
						"MIB_OTPActivate_EN";
					inputParam["smsSubject"] =
						"MIB_OTPActivateIB_EN";
					spaChannel = "IB-ACTIVATION";
				} else {
					inputParam["eventNotificationId"] =
						"MIB_OTPActivate_TH";
					inputParam["smsSubject"] =
						"MIB_OTPActivateIB_TH";
					spaChannel = "IB-ACTIVATION";
				}
			} else if (gblActionCode == "12") {
				if (locale == "en_US") {
					inputParam["eventNotificationId"] =
						"MIB_OTPResetPWD_EN";
					inputParam["smsSubject"] =
						"MIB_OTPResetPWDIB_EN";
					spaChannel = "IB-ACTIVATION";
				} else {
					inputParam["eventNotificationId"] =
						"MIB_OTPResetPWD_TH";
					inputParam["smsSubject"] =
						"MIB_OTPResetPWDIB_TH";
					spaChannel = "IB-ACTIVATION";
				}
			} else if (spaChnage == "chguseridib") {
				inputParam["eventNotificationId"] =
					"MIB_ChangeUSERID_" + kony.i18n.getCurrentLocale();
				inputParam["smsSubject"] = "MIB_ChangeUSERID_" +
					kony.i18n.getCurrentLocale();
				spaChannel = "IB-CHANGEUSERID";
			} else if (spaChnage == "chgpwdidib") {
				inputParam["eventNotificationId"] =
					"MIB_ChangeIBPWD_" + kony.i18n.getCurrentLocale();
				inputParam["smsSubject"] = "MIB_ChangeIBPWD_" +
					kony.i18n.getCurrentLocale();
				spaChannel = "IB-CHANGEPWD";
			}
		} else {
			inputParam["deviceId"]=uniqueid;	// Added for device name in MB activation
			if (true) {//no action codes enh217
				if (locale == "en_US") {
					inputParam["eventNotificationId"] =	"MIB_OTPActivate_EN";
					inputParam["smsSubject"] = "MIB_OTPActivateMB_EN";
				} else {
					inputParam["eventNotificationId"] =	"MIB_OTPActivate_TH";
					inputParam["smsSubject"] = "MIB_OTPActivateMB_TH";
				}
			} // For Add device Flow
			else if (gblActionCode == "23") {
				if (locale == "en_US") {
					inputParam["eventNotificationId"] =	"MIB_OTPAddDevice_EN";
					inputParam["smsSubject"] = "MIB_OTPAddDevice_EN";
				} else {
					inputParam["eventNotificationId"] = "MIB_OTPAddDevice_TH";
					inputParam["smsSubject"] = "MIB_OTPAddDevice_TH";
				}
			} // For Reset Password Flow
			else if (gblActionCode == "22") {
				if (locale == "en_US") {
					inputParam["eventNotificationId"] =	"MIB_OTPResetPWDMB_EN";
					inputParam["smsSubject"] = "MIB_OTPResetPWDMB_EN";
				} else {
					inputParam["eventNotificationId"] =	"MIB_OTPResetPWDMB_TH";
					inputParam["smsSubject"] = "MIBOTPResetPWDMB_TH";
				}
			}
		}
		inputParam["bankRef"] = "";
		inputParam["batchNo"] = "";
		if (flowSpa) //for SPA
		{
			
			inputParam["Channel"] = spaChannel;
		} else {
			inputParam["channel"] = "";
			if(gblMobileNewChange=="Y")
			{
				inputParam["Channel"] = "ChangeMobileNumberNew";
			}
		}
		// inputParam["eventNotificationId"] = "";
		inputParam["inputSeedString"] = "";
		//inputParam["mobileNo"] = gblPHONENUMBER; //"0865832721" Mobile num VIT offshore; mib xpress inq already has it
		inputParam["param"] = "";
		inputParam["entries"] = "";
		inputParam["key"] = "";
		inputParam["value"] = "";
		inputParam["policyId"] = "";
		inputParam["productCode"] = "";
		inputParam["receipientName"] = "";
		inputParam["session"] = "";
		//inputParam["smsSubject"] = ""; 
		inputParam["storeId"] = "";
		inputParam["tokenUUID"] = "";
		inputParam["retryCounterRequestOTP"] = gblRetryCountRequestOTP;
		invokeServiceSecureAsync("RequestOTPKony", inputParam,
			CallbackonClickOTPRequest);
	}
}

function CallbackonClickOTPRequest(status, resulttable) {
	if (status == 400) {
		
		
		if (resulttable["opstatus"] == 0) {
		 //DEF11700 Change start
		   if (resulttable["errCode"] == "GenOTPRtyErr00002"){
		    return false;
		  }   
		  	try {
		       kony.timer.cancel("otpTimer")
	         } catch (e) {
		       
	        }
	     //DEF11700 Change End   
			gblRetryCountRequestOTP = resulttable[
				"retryCounterRequestOTP"];
			var reqOtpTimer = kony.os.toNumber(resulttable[
				"requestOTPEnableTime"]);
			gblOTPLENGTH = kony.os.toNumber(resulttable["otpLength"]);
			
			kony.timer.schedule("otpTimer", otpTimerCallBack,
				reqOtpTimer, false);
				
				
			if (gblOnClickReq == false) {
				showOTPPopup(kony.i18n.getLocalizedString(
						"keyOTP"), "ABCD", "xxx-xxx-" +
					gblPHONENUMBER.substring(6, 10),
					otpValidation, 1)
			}
			if (flowSpa) {
				if (spaChnage) {
					showOTPPopup(kony.i18n.getLocalizedString(
							"keyOTP"), "ABCD",
						"xxx-xxx-" +
						gblPHONENUMBER.substring(6, 10),
						otpValidationspa, 1)
				}
			}
			popupTractPwd.lblPopupTract1.text = kony.i18n.getLocalizedString(
				"keybankrefno") + resulttable[
				"pac"];
				
			if (flowSpa) {
				popupTractPwd.lblPopupTract2Spa.text = kony.i18n
					.getLocalizedString("keyotpmsg");
				popupTractPwd.lblPopupTract4Spa.text =
					"xxx-xxx-" + gblPHONENUMBER.substring(6,
						10);
				popupTractPwd.lblPopupTract7.text = "";
			} else {
				popupTractPwd.lblPopupTract2.text = kony.i18n.getLocalizedString(
					"keyotpmsg");
				popupTractPwd.lblPopupTract7.text = "";
				popupTractPwd.txtOTP.text="";
				popupTractPwd.lblPopupTract4.text = "xxx-xxx-" +
					gblPHONENUMBER.substring(6, 10);
				otpConfirmEnable();
			}
			gblOTPFlag = false;
		} else {
			if (resulttable["errCode"] == "GenOTPRtyErr00002") {
				kony.application.dismissLoadingScreen();
				showAlert(kony.i18n.getLocalizedString(
						"ECGenOTPRtyErr00002"), kony.i18n
					.getLocalizedString(
						"info"));
				return false;
			} else if (resulttable["errCode"] == "GenOTPRtyErr00001") {
				kony.application.dismissLoadingScreen();
				if(resulttable["mobileAllZero"] != undefined && resulttable["mobileAllZero"] == "true" && resulttable["mobileAllZero"] != null){
					showAlert(kony.i18n.getLocalizedString("keyMobileAllZero"),kony.i18n.getLocalizedString("info"));
				}else{
                	showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                 }
				gblOTPFlag = false;
				//return false;
			} else {
				kony.application.dismissLoadingScreen();
				showAlert(kony.i18n.getLocalizedString(
					"ECGenericError"), kony.i18n.getLocalizedString(
					"info"));
				gblOTPFlag = false;
				//return false;
			}
			//popupTractPwd.lblPopupTract1.text = kony.i18n.getLocalizedString("keybankrefno") + resulttable["pac"];
			gblRetryCountRequestOTP = resulttable[
				"retryCounterRequestOTP"];
			var reqOtpTimer = kony.os.toNumber(resulttable[
				"requestOTPEnableTime"]);
			gblOTPLENGTH = kony.os.toNumber(resulttable["otpLength"]);
			kony.timer.schedule("otpTimer", otpTimerCallBack,
				reqOtpTimer, false);
			
		}
		kony.application.dismissLoadingScreen();
	}
}
/*
 * This function enables / disables the confirm button on UI
 * as per the OTP being entered in OTP text field
 */

function otpConfirmEnable() {
	var otpText = popupTractPwd.txtOTP.text;
	if (otpText.trim()
		.length > 0) {
		popupTractPwd.btnPopupTractConf.skin = btnBlueSkin;
		popupTractPwd.btnPopupTractConf.setEnabled(true);
	} else {
		popupTractPwd.btnPopupTractConf.skin = btnDisabledGray;
		popupTractPwd.btnPopupTractConf.focusSkin = btnDisabledGray;
		popupTractPwd.btnPopupTractConf.setEnabled(false);
	}
}
/**
 * description
 * @returns {}
 */

function onClickAccessPinCancel() {
	popupTractPwd.btnPopupTractConf.skin = btnBlueSkin;
	popupTractPwd.btnPopupTractConf.focusSkin = btnBlueSkin;
	popupTractPwd.btnPopupTractConf.setEnabled(true);
	if(gblMBActivationVia == "2"){
		try{
			kony.timer.cancel("activateViaIBTimer");
		}catch (e) {
		}	
	}
	if(kony.application.getCurrentForm().id == "frmCardActivationDetails"){
		//frmMBCardList.show();
		if(frmCardActivationDetails.flexCardDetails.isVisible){
			//frmCardActivationDetails.txtCCNumbetInputThree.text ="";
		}
		
		if(gblCardType == "C"){
			isPopupCancel = true;
		    showCVVDetails();
		}else if(gblCardType == "R"){
			frmCardActivationDetails.txtCCNumbetInputThree.text ="";
			frmCardActivationDetailsInit();
			showCitizenId();
		}else{
			frmCardActivationDetails.txtCCNumbetInputThree.text="";
			gblCardTrans = "T";
			activationReadUTFFile();
            showCardDetailsFlex();
            frmMBNewTncCreditCardClickBtnNext();
		}
	} else if(kony.application.getCurrentForm().id == "frmMBChangePINEnterExistsPin"){
		clearExistChangePIN();
	}
	popupTractPwd.dismiss();	
}
/**
 * description
 * @returns {}
 */

//function transPopupCallBack(text) {
//	alert(text);
//}

//function toShowMBsetPasswd() {
//	frmMBsetPasswd.show()
//}
/**
 * description
 * @returns {}
 */

//function showTerminationPopup() {
//	popUpTermination.containerWeight = 94;
//	popUpTermination.show();
//}
/**
 * description
 * @returns {}
 */

function onClickNextPwdRules() {
	var prevForm = kony.application.getPreviousForm();
	if (prevForm["id"] == "frmMyProfiles") {
		if (gblChangePWDFlag == 0) {
			frmCMChgAccessPin.tbxCurAccPin.text = "";
			frmCMChgAccessPin.tbxCurAccPinUnMask.text = "";
			frmCMChgAccessPin.txtAccessPwd.text = "";
			frmCMChgAccessPin.txtAccessPwdUnMask.text = "";
			frmCMChgAccessPin.show();
		} else if (gblChangePWDFlag == 1) {
			frmCMChgTransPwd.tbxTranscCrntPwd.text = "";
			frmCMChgTransPwd.tbxTranscCrntPwdTemp.text = "";
			frmCMChgTransPwd.txtTemp.text = "";
			frmCMChgTransPwd.txtTransPass.text = "";
			frmCMChgTransPwd.show();
		}
	} else {
		
		showAddDevOrActivation();
		//invokeDeviceInquiry();
		//frmMBsetPasswd.show();
		//gblAddOrAuth = 0;
		//frmMBsetPasswd.lblDeviceName.isVisible = false;
		//frmMBsetPasswd.txtDeviceName.isVisible = false;
	}
}

function showOTPPopupForOTPValidation(lblText, refNo, mobNO, callBackConfirm) {
	//Please DO NOT KEEP THIS FUNCTION OUTSIDE. This is inner function assigned to popupTractPwd.btnPopupTractConf.onClick
	/*function onConfClick() {
		//popupTractPwd.dismiss();
		var enteredText = "";
		if (typeFlag == 1) {
			//enteredText = popupTractPwd.tbxPopupTractPwdtxt.text;
			enteredText = popupTractPwd.txtOTP.text;
			if (popupTractPwd.hbxIncorrectOTP.isVisible == true) {
				enteredText = popupTractPwd.txtIncorrectOTP.text;
			}
		} 
		callBackConfirm(enteredText);
	}*/
	function onConfClickOTP() {
		showLoadingScreen();
		var enteredText = "";
		enteredText = popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text;
		
		/**
		 * The below code is commented, MIB-6454 Analysis of Trusteer code flow
		 * 
		 * We are going to send the KonydeviceId isnted of Trusteer unique id
		 ***/
	
		callBackFromSSLImplOTP();
		/**
		 * 
		 * 
		//if (deviceInfo["name"] == "iPhone" || deviceInfo["name"] == "iPhone Simulator") {
		//#ifdef iphone
			VerifyTransSSLValidationOTP();
		//}else if (deviceInfo["name"] == "android"){
		//#else
			//#ifdef android
				sslVerifyTransAndroidOTP();
			//#endif
		//#endif
		//callBackConfirm(enteredText);
		**/
	}
	function callBackFromSSLImplOTP() {
		dismissLoadingScreen();
		callBackConfirm(popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text);
	}
	function sslVerifyTransAndroidOTP(){
	//Commented for Arxan
		/*if(TmbTrusteerFFIObject == null)
			TmbTrusteerFFIObject = new trust.TmbTrusteerFFI();
		  var url = appConfig.serverIp;
			TmbTrusteerFFIObject.sslValidationAndroid(callbackVerifyTransSSLOTP,url);*/
	}

	function callbackVerifyTransSSLOTP(sslResult) {
	    if(sslResult == 0){
	    	
	    	//callBackFromSSLImpl();
		}else{
			/*
			dismissLoadingScreen();
			showAlertForUniqueGenFail(kony.i18n.getLocalizedString("keySSLValidationFailureMsg"));*/
			//alert(kony.i18n.getLocalizedString("keySSLValidationFailureMsg"));
		}
	    callBackFromSSLImplOTP();
	}
	function VerifyTransSSLValidationOTP()
	{
		//Commented for Arxan
		/*var sslObj = {};
		sslObj["SSLValidation"] = verifyTransSSLCallBackOTP;
		sslObj["message"] = "yes";
		sslObj["url"] = appConfig.serverIp;
		
		
		ffinamespace.getsslValidation(sslObj);*/
	}
	function verifyTransSSLCallBackOTP(sslresult){
		if(sslresult == 0 || sslresult == "0" || sslresult == 99 || sslresult == "99" ){
			
			//callBackFromSSLImpl();
		}else{
			/*
			dismissLoadingScreen();
			showAlertForUniqueGenFail(kony.i18n.getLocalizedString("keySSLValidationFailureMsg"));*/
			//alert(kony.i18n.getLocalizedString("keySSLValidationFailureMsg"));
		}
		callBackFromSSLImplOTP();
	}
	popupTractPwd.containerWeight = 94;
	popupTractPwd.btnPopupTractConf.skin = btnBlueSkin;
	popupTractPwd.btnPopupTractConf.setEnabled(true);
	popupTractPwd.btnPopupTractConf.onClick = onConfClickOTP;
	popupTractPwd.btnPopupTractConf.text = kony.i18n.getLocalizedString(
		"keyConfirm");
	// popupTractPwd.tbxPopupTractPwdtxt.text = "";
	popupTractPwd.txtOTP.text = "";
	//popupTractPwd.lblPopTractPwdtxt.text = lblText;
	popupTractPwd.lblOTP.text = lblText;
	popupTractPwd.hbxIncorrectOTP.isVisible = false;
	//if (typeFlag == 1) {
		popupTractPwd.lblPopupTract1.isVisible = true;
		if (flowSpa) {
			popupTractPwd.hbxPopupTractlblHoldSpa.isVisible = true;
		} else {
			popupTractPwd.hbxPopupTractlblHold.isVisible = true;
		}
		popupTractPwd.lblPopupTract5.text = kony.i18n.getLocalizedString("keyotpmsgreq");
		popupTractPwd.lblPopupTract5.isVisible = true;
		if (flowSpa) {
			popupTractPwd.lblPopupTract4Spa.text = mobNO;
		} else {
			popupTractPwd.lblPopupTract4.text = mobNO;
		}
		//popupTractPwd.btnPopUpTractCancel.skin = btnDisabledGray;
//		popupTractPwd.btnPopUpTractCancel.focusSkin = btnDisabledGray;
//		popupTractPwd.btnPopUpTractCancel.setEnabled(false);
		//popupTractPwd.btnPopUpTractCancel.onClick = onClickOTPRequest;
		//popupTractPwd.btnPopUpTractCancel.text = kony.i18n.getLocalizedString(
//			"keyRequest");
		popupTractPwd.btnPopUpTractCancel.skin = btnLightBlue;
		popupTractPwd.btnPopUpTractCancel.focusSkin = btnLightBlue;
        popupTractPwd.btnPopUpTractCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
        popupTractPwd.btnPopUpTractCancel.onClick = onClickOTPCancel;
		if (kony.i18n.getCurrentLocale() == "th_TH") {
			//popupTractPwd.lblPopTractPwdtxt.containerWeight = 25;
			popupTractPwd.lblOTP.containerWeight = 12;
		} else {
			//popupTractPwd.lblPopTractPwdtxt.containerWeight = 12; btnOtpRequest
			popupTractPwd.lblOTP.containerWeight = 12;
		}
		//popupTractPwd.lblPopupTract1.text = kony.i18n.getLocalizedString("keybankrefno")+gblRefNum; Commented as added in onClickOTPRequest
		//popupTractPwd.hbxPoupOTP.isVisible = true;
		popupTractPwd.hbxOTP.isVisible = true;
		popupTractPwd.btnOtpRequest.skin=btnDisabledGray;
		popupTractPwd.btnOtpRequest.focusSkin=btnDisabledGray;
		popupTractPwd.btnOtpRequest.setEnabled(false);
		popupTractPwd.hbxPoupAccesspin.isVisible = false;
		popupTractPwd.hbxPopupTranscPwd.isVisible = false;
		//popupTractPwd.lblPopTractPwdtxt.text = lblText;
		popupTractPwd.lblOTP.text = lblText;
		//gblOTPFlag = true;
		//onClickOTPRequest();
		//popupTractPwd.tbxPopupTractPwdtxt.text = "";
		popupTractPwd.txtOTP.textInputMode = constants.TEXTBOX_INPUT_MODE_NUMERIC;
		
		popupTractPwd.txtOTP.keyBoardStyle = constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD;
		
		if (flowSpa) {
			popupTractPwd.txtOTP.textInputMode = constants.TEXTBOX_INPUT_MODE_ANY;
		}
	//} 
	dismissLoadingScreen();
	popupTractPwd.show();
	if(popupTractPwd.hbxOTP.isVisible){
			kony.print("$$$$$$$$$$$$$$$$$"+popupTractPwd.hbxPopupTranscPwd.isVisible)
			popupTractPwd.txtOTP.setFocus(true)
	
	}else if(popupTractPwd.hbxPopupTranscPwd.isVisible){
		popupTractPwd.tbxPopupTractPwdtxtTranscPwd.setFocus(true)
	}else if(popupTractPwd.hbxPoupAccesspin.isVisible){
		popupTractPwd.tbxPopupTractPwdtxtAccPin.setFocus(true)
	}
}
/**
 * description
 * @returns {}
 */

function onClickOTPCancel(){
	popupTractPwd.btnPopupTractConf.skin = btnBlueSkin;
	popupTractPwd.btnPopupTractConf.focusSkin = btnBlueSkin;
	popupTractPwd.btnPopupTractConf.setEnabled(true);
	popupTractPwd.dismiss();
	/*
	if(gblOpenAccountFlow){
      	frmCheckContactInfo.show();	
	}else{
		gblOpenAccountFlow = false;
		onClickMyProfileFlow();
	}
	**/
}


function activationViaIBUserOTP(){
	showLoadingScreen();
	var inputParams={};
	inputParams["Channel"] = "ActivateMB";
    inputParams["locale"] = kony.i18n.getCurrentLocale();
	invokeServiceSecureAsync("generateOTPWithUser", inputParams, callBackactivationViaIBUserOTP);
}

function callBackactivationViaIBUserOTP(status, resulttable){
	if(status == 400){
		dismissLoadingScreen();
		if (resulttable["errCode"] == "GenOTPRtyErr00002") {
				showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00002"), kony.i18n.getLocalizedString("info"));
				disableActivateMBViaIBPopup();
				return false;
		}else if (resulttable["errCode"] == "JavaErr00001") {
				showAlert(kony.i18n.getLocalizedString("ECJavaErr00001"), kony.i18n.getLocalizedString("info"));
				return false;
		}else if (resulttable["errCode"] == "GenOTPRtyErr00001") {
				if(resulttable["mobileAllZero"] != undefined && resulttable["mobileAllZero"] == "true"){
					showAlert(kony.i18n.getLocalizedString("keyMobileAllZero"),kony.i18n.getLocalizedString("info"));
				}else{
                	showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                }	
                return false;
        }else if(resulttable["code"]== "10403"){
      			showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr"), kony.i18n.getLocalizedString("info"));
      			return false;
      	}else if (resulttable["opstatus"] == 0) {
				//Put success handler here
				
				var refVal="";
				for(var d=0;resulttable["Collection1"].length;d++){
					if(resulttable["Collection1"][d]["keyName"] == "pac"){
						refVal=resulttable["Collection1"][d]["ValueString"];
						break;
					}
				}
				gblOTPLENGTH = kony.os.toNumber(resulttable["otpLength"]);
				
				popupTractPwd.lblPopupTract1.text = kony.i18n.getLocalizedString("keybankrefno") + refVal;
				popupTractPwd.lblPopupTract2.text = kony.i18n.getLocalizedString("keyotpmsg");
				popupTractPwd.lblPopupTract7.text = "";
				popupTractPwd.txtOTP.text="";
				popupTractPwd.lblPopupTract4.text = glbMobileNo;
				var reqOtpTimer = kony.os.toNumber(resulttable["requestOTPEnableTime"]);
				try {kony.timer.cancel("activateViaIBTimer")} catch (e) {}
				kony.timer.schedule("activateViaIBTimer", activateViaIBTimerCallBack,reqOtpTimer, false);
				showOTPPopup(kony.i18n.getLocalizedString("keyOTP"), "ABCD",glbMobileNo,otpValidation, 1)
				otpConfirmEnable();
		}
	}	
}

function activateViaIBTimerCallBack() {
	popupTractPwd.btnOtpRequest.setEnabled(true);
	popupTractPwd.btnOtpRequest.skin = btnLightBlue;
	popupTractPwd.btnOtpRequest.focusSkin=btnLightBlue;
	gblOTPFlag = true;
	try {
		kony.timer.cancel("activateViaIBTimer");
	} catch (e) {
		
	}
}

function disableActivateMBViaIBPopup(){
	popupTractPwd.btnOtpRequest.setEnabled(false);
	popupTractPwd.btnOtpRequest.skin=btnDisabledGray;
	popupTractPwd.btnOtpRequest.focusSkin=btnDisabledGray;
	kony.timer.cancel("activateViaIBTimer");
}

function verifyOTPActivationIBUserOTP(){
	var otpText= popupTractPwd.txtOTP.text;
	showLoadingPopUpScreen();
	otpCodePattStr = "^[0-9]{" + gblOTPLENGTH + "}$";
	var otpCodePatt = new RegExp(otpCodePattStr, "g");
	resultOtpCodePatt = otpCodePatt.test(otpText);
	if (resultOtpCodePatt) {
		var inputParam = {};
		inputParam["verifyPasswordEx_password"] = otpText;
		invokeServiceSecureAsync("verifyOTPActivateMBUsingIB", inputParam, callBackOTPVerify)
	}else {
		popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("invalidOTP");
		popupTractPwd.lblPopupTract2.text = "";
		popupTractPwd.lblPopupTract4.text = "";
		kony.application.dismissLoadingScreen();
		return false;
	}
}


// Added by Vijay for OTP and transaction pwd flipflop - 27-01
function showOTPSSLPopupTransPwd(callBackConfirmSSL) {
	//Please DO NOT KEEP THIS FUNCTION OUTSIDE. This is inner function assigned to popupTractPwd.btnPopupTractConf.onClick
	function onConfClick() {
		showLoadingScreen();
		var enteredText = "";
		enteredText = popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text;
		//if (deviceInfo["name"] == "iPhone" || deviceInfo["name"] == "iPhone Simulator") {
	/**
	 * The below code is commented, MIB-6454 Analysis of Trusteer code flow
	 * 
	 * We are going to send the KonydeviceId isnted of Trusteer unique id
	 ***/
		
		callBackFromSSLImpl();
		
		/**
		 * 
		 *
		//#ifdef iphone
			VerifyTransSSLValidation();
		//}else if (deviceInfo["name"] == "android"){
		//#else
			//#ifdef android
				sslVerifyTransAndroid();
			//#endif
		//#endif
		//callBackConfirm(enteredText);
		**/
	}
	function callBackFromSSLImpl() {
		dismissLoadingScreen();
		callBackConfirmSSL(popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text);
	}
	function sslVerifyTransAndroid(){
	//Commented for Arxan
		/*if(TmbTrusteerFFIObject == null)
			TmbTrusteerFFIObject = new trust.TmbTrusteerFFI();
		 var url = appConfig.serverIp;
			TmbTrusteerFFIObject.sslValidationAndroid(callbackVerifyTransSSL,url);*/
	}

	function callbackVerifyTransSSL(sslResult) {
	    if(sslResult == 0){
	    	
	    	//callBackFromSSLImpl();
		}else{
			/*
			dismissLoadingScreen();
			showAlertForUniqueGenFail(kony.i18n.getLocalizedString("keySSLValidationFailureMsg"));*/
			//alert(kony.i18n.getLocalizedString("keySSLValidationFailureMsg"));
		}
	    callBackFromSSLImpl();
	}
	function VerifyTransSSLValidation()
	{
		//Commented for Arxan
		/*var sslObj = {};
		sslObj["SSLValidation"] = verifyTransSSLCallBack;
		sslObj["message"] = "yes";
		sslObj["url"] = appConfig.serverIp;
		
		
		ffinamespace.getsslValidation(sslObj);*/
	}
	function verifyTransSSLCallBack(sslresult){
		if(sslresult == 0 || sslresult == "0" || sslresult == 99 || sslresult == "99"){
			
			//callBackFromSSLImpl();
		}else{
			/*
			dismissLoadingScreen();
			showAlertForUniqueGenFail(kony.i18n.getLocalizedString("keySSLValidationFailureMsg"));*/
			//alert(kony.i18n.getLocalizedString("keySSLValidationFailureMsg"));
		}
		callBackFromSSLImpl();
	}
	popupTractPwd.containerWeight = 94;
	popupTractPwd.btnPopupTractConf.onClick = onConfClick;
	popupTractPwd.btnPopupTractConf.text = kony.i18n.getLocalizedString(
		"keyConfirm");
	// popupTractPwd.tbxPopupTractPwdtxt.text = "";
	popupTractPwd.txtOTP.text = "";
	//popupTractPwd.lblPopTractPwdtxt.text = lblText;
	popupTractPwd.lblOTP.text = kony.i18n.getLocalizedString("transPasswordSub");
	popupTractPwd.hbxIncorrectOTP.isVisible = false;
	popupTractPwd.line502709349207.isVisible = false;
	popupTractPwd.lblPopupTract1.isVisible = false;
	popupTractPwd.hbxPopupTractlblHold.isVisible = false;
	popupTractPwd.lblPopupTract5.isVisible = false;
	popupTractPwd.lblPopupTract7.isVisible = true;
	popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("keyTransPwdMsg");
	popupTractPwd.lblPopupTract7.skin = lblPopupLabelTxt;
	popupTractPwd.btnPopUpTractCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
	popupTractPwd.btnPopUpTractCancel.skin = btnLightBlue;
	popupTractPwd.btnPopUpTractCancel.focusSkin = btnLightBlue;
	//popupTractPwd.lblPopTractPwdtxt.containerWeight = 53;
	popupTractPwd.lblOTP.containerWeight = 53;
	//popupTractPwd.hbxPoupOTP.isVisible = false;
	popupTractPwd.hbxOTP.isVisible = false;
	popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text = "";
	popupTractPwd.hbxPoupAccesspin.isVisible = false;
	popupTractPwd.hbxPopupTranscPwd.isVisible = true;
	//popupTractPwd.lblPopupTranscPwd.text = kony.i18n.getLocalizedString("transPasswordSub");
	popupTractPwd.btnPopUpTractCancel.onClick = onClickAccessPinCancel;
	gblOTPFlag = true;
	//popupTractPwd.tbxPopupTractPwdtxt.textInputMode = constants.TEXTBOX_INPUT_MODE_ANY;
	//commenting the lines of code to fix the text box issue on MB
	//popupTractPwd.txtOTP.textInputMode = constants.TEXTBOX_INPUT_MODE_ANY;
	//popupTractPwd.txtOTP.maxTextLength = gblOTPLENGTH;
	dismissLoadingScreen();
	popupTractPwd.show();
	if(popupTractPwd.hbxOTP.isVisible){
			kony.print("$$$$$$$$$$$$$$$$$"+popupTractPwd.hbxPopupTranscPwd.isVisible)
			popupTractPwd.txtOTP.setFocus(true)
	
	}else if(popupTractPwd.hbxPopupTranscPwd.isVisible){
		popupTractPwd.tbxPopupTractPwdtxtTranscPwd.setFocus(true)
	}else if(popupTractPwd.hbxPoupAccesspin.isVisible){
		popupTractPwd.tbxPopupTractPwdtxtAccPin.setFocus(true)
	}
}


/*
* To highlight the access pin img on click of numbers
*/
function showMaskPin() {

    if (gblPinCount < 6) {
        gblPinCount++;
    }
	switch(gblPinCount) {
	    case 1:
	        popupEnterAccessPin.imgOne.src = "key_sel.png";	
	        break;
	   case 2:
	        popupEnterAccessPin.imgTwo.src = "key_sel.png";
	        break;
	   case 3:
	        popupEnterAccessPin.imgThree.src = "key_sel.png";
	        break;
	    case 4:
	        popupEnterAccessPin.imgFour.src = "key_sel.png";
	        break;
	    case 5:
	         popupEnterAccessPin.imgFive.src = "key_sel.png";
	        break;
	    case 6:
	        popupEnterAccessPin.imgSix.src = "key_sel.png";
	        break;               
	} 
}

function getAccessPin(obj) {
    var temp;
    var caseVar = obj.id;
    
    switch(caseVar) {
	    case "btnOne":
	    	temp = "1";
	     break;
	    case "btnTwo":
	    	temp = "2";
    	break;
    	case "btnThree":
    		temp = "3";
    	break;
    	case "btnFour":
    		temp = "4";
    	break;
    	case "btnFive":
    		temp = "5";
    	break;
    	case "btnSix":
    		temp = "6";
    	break;
    	case "btnSeven":
    		 temp = "7";
    	break;
    	case "btnEight":
    		 temp = "8";
    	break;	
    	case "btnNine":
    		 temp = "9";
    	break;
    	case "btnZero":
    		 temp = "0";
    	break;			
    }
    
    //if (gblNum.length < 6) {
        gblNum = gblNum + temp;

    //}
     gblNum = gblNum.trim();
   
     if (gblNum.length > 6)
       return false;
   
    if (gblNum.length == 6) {        
        //accsPwdValidatnLogin(gblNum.trim());
       //call respective callback with access pin 
      kony.print("calling access pin validation :"+gblNum + "test");
       verifyPinAndExecuteTxn();
    }  
}

function verifyPinAndExecuteTxn(){
   var currForm = "";
   var curFrmObj = kony.application.getCurrentForm();
   if(isNotBlank(curFrmObj))
     currForm = curFrmObj.id;
  kony.print("current form: "+currForm);
  
    gblNum = gblNum.trim();
    if (gblNum.length < 6)
      {
        kony.print("Pin length not correct");
        return false;
      }
  
  if(currForm == "frmTransferConfirm"){
    onClickConfirmPop();
  }else if(currForm == "frmBillPaymentConfirmationFuture"){
    billPaymentConfirmation();
  }else if(currForm == "frmMyRecipientAddAccConf"){
     addAccountRecipentCompletePre();
  }else if(currForm == "frmMyRecipientEditProfile"){
    transConfirmEditProfile();
  }else if(currForm == "frmMyRecipientSelectContactsConf") {
    transConfirmSelectContactsPre();
  }else if(currForm == "frmAddTopUpBillerconfrmtn"){
    topupConfirmationAgree(gblNum);
  }else if(currForm == "frmMBSavingsCareConfirmation" || currForm == "frmOpenActDSConfirm" || currForm == "frmOpenAccountNSConfirmation" || currForm == "frmOpenActTDConfirm"){
     onClickConfirmPopDS();
  }else if(currForm == "frmMBEStatementConfirmation"){
      eStatementTxnPasswordConfirmation(gblNum);
  }else if (currForm == "frmFPSetting" || currForm == "frmTouchIdSettings"){
     onConfirmGetDeviceID();
  }else if(currForm == "frmMBBlockDebitCardConfirm"){
    onClickBlockCardConfirmPop(gblNum);
  }else if(currForm == "frmMBChangePINEnterExistsPin"){
    onClickConfirmChangePINTransPop(gblNum);
  }else if(currForm == "frmMBRequestNewPin"){
    onClickRequestPINConfirmTxnPopup(gblNum);
  }else if(currForm == "frmCAPaymentPlanConfirmation") {
    onClickCashAdvaceConfirmPop(gblNum);
  }else if(currForm == "frmChangeMobNoTransLimitMB"){
    if(gblUpdateProfileFlag == "mobile")
       CompositeChangeMobileMB();
    else
      CompositeChangeLimit();
  }else if(currForm == "frmMyAccntConfirmationAddAccount"){
     srvVerifyTxnPassword();
  }else if(currForm =="frmMBPointRedemptionConfirmation"){
    rewardsTxnPasswordConfirmation(gblNum);
  }else if(currForm == "frmeditMyProfile"){
     verifyPWDMyProfileMB();
  }else if(currForm == "frmApplyInternetBankingMB"){
    callBackConfirm(gblNum);
  }else if(currForm == "frmeditContactInfo"){
    verifyPWDEditContactsOpenAc();
  }else if(currForm == "frmEditFutureBillPaymentConfirm"){
    checkEditBillPayVerifyPWDMB(gblNum);
  }else if(currForm == "frmMBFTEdit"){
      srvVerifyPasswordExFT_CS_MB(gblNum);
  }else if(currForm == "frmeditMyProfiles"){
     verifyPWDMyAddressMB();
  }else if(currForm == "frmDreamSavingEdit"){
      editDreamSavingCompositeServiceMB(gblNum);
  }
}


function onClickCancelAccessPin(){ 
   var currForm = "";
   var curFrmObj = kony.application.getCurrentForm();
   
   if(isNotBlank(curFrmObj))
      currForm = curFrmObj.id;
   var flxForms = ["frmFPSetting","frmMBBlockCardCCDBConfirm","frmMBBlockDebitCardConfirm","frmMBChangePINEnterExistsPin","frmMBRequestNewPin","frmMBSavingsCareConfirmation","frmCardlesswithdrawConfirmation","frmMFConfirmMB","frmMFSwitchConfirmMB","frmCAPaymentPlanConfirmation","frmCardActivationDetails","frmBillPaymentEditFutureNew","frmMBFTEdit","frmMBBlockCardRecommendation","frmMFcancelOrderToProcess","frmeditMyProfiles"];
   var frmIndex = flxForms.indexOf(currForm);
  
   if(frmIndex == -1){      
      popupEnterAccessPin.dismiss();
      kony.print("current form: "+currForm);
     if (currForm == "frmFPSetting" || currForm == "frmTouchIdSettings"){
        setEnableDisableTouchLogin();
      } 
   }else{
     closeApprovalKeypad();   
   }
   dismissLoadingScreen(); 
}

function resetAccessPinImg(badLoginCount){
   gblPinCount = 0;
   gblNum = "";
   var currForm = "";
   var curFrmObj = kony.application.getCurrentForm();
   if(isNotBlank(curFrmObj))
     currForm = curFrmObj.id;
  
   var flxForms = ["frmFPSetting","frmMBBlockCardCCDBConfirm","frmMBBlockDebitCardConfirm","frmMBChangePINEnterExistsPin","frmMBRequestNewPin","frmMBSavingsCareConfirmation","frmCardlesswithdrawConfirmation","frmMFConfirmMB","frmMFSwitchConfirmMB","frmCAPaymentPlanConfirmation","frmMBFTEdit","frmBillPaymentEditFutureNew","frmMBBlockCardRecommendation","frmCardActivationDetails","frmMFcancelOrderToProcess","frmeditMyProfiles"];
   var frmIndex = flxForms.indexOf(currForm);
   var totalPinAttempts = 3;  
   var incorrectPinText = kony.i18n.getLocalizedString("wrongPinKey");
   //kony.print("incorrectPinText: "+incorrectPinText);
    
  
  if(isNotBlank(badLoginCount)){
     incorrectPinText = kony.i18n.getLocalizedString("PIN_Incorrect");
     incorrectPinText = incorrectPinText.replace("{rem_attempt}", totalPinAttempts - badLoginCount); 
  }
  
  if(frmIndex == -1){
    popupEnterAccessPin.imgOne.src = "key_default.png";	
    popupEnterAccessPin.imgTwo.src = "key_default.png";	
    popupEnterAccessPin.imgThree.src = "key_default.png";	
    popupEnterAccessPin.imgFour.src = "key_default.png";	
    popupEnterAccessPin.imgFive.src = "key_default.png";	
    popupEnterAccessPin.imgSix.src = "key_default.png";
    popupEnterAccessPin.lblWrongPin.text = incorrectPinText;
    kony.print("lblWrongPin is visible: "+popupEnterAccessPin.lblWrongPin.isVisible);
    //popupEnterAccessPin.lblWrongPin.setVisibility(false);
  }else{
     kony.print("resetAccessPinImg: show incorrect pin text");
     resetKeypadApproval();
   //  var badLoginCount = result["badLoginCount"];
     
     //incorrectPinText = incorrectPinText.replace("{rem_attempt}", gblTotalPinAttempts - badLoginCount);
     curFrmObj.lblForgotPin.text = incorrectPinText;
     curFrmObj.lblForgotPin.skin = "lblBlackMed150NewRed";
     curFrmObj.lblForgotPin.onTouchEnd = doNothing;
      //kony.print("resetAccessPinImg:"+incorrectPinText);
      kony.print("lblForgotPin: "+curFrmObj.lblForgotPin.isVisible);
     dismissLoadingScreen();
  }
  dismissLoadingScreen();
}

function onClickAccessPinBack() {

    if (gblPinCount > 0) {
        gblPinCount--;
        var pinLen = gblNum;
        var updatedPin = gblNum.substring(0, pinLen.length - 1)
        gblNum = updatedPin;
    }

  switch(gblPinCount) {
	    case 0:
	        popupEnterAccessPin.imgOne.src = "key_default.png";	        
		break;
    	case 1:	        
	        popupEnterAccessPin.imgTwo.src = "key_default.png";	       
		break;
    	case 2:	      
	        popupEnterAccessPin.imgThree.src = "key_default.png";	      
		break;
   		case 3:	        
	        popupEnterAccessPin.imgFour.src = "key_default.png";	      
		break;
    	case 4:	        
	        popupEnterAccessPin.imgFive.src = "key_default.png";	       
		break;
    	case 5:	       
	        popupEnterAccessPin.imgSix.src = "key_default.png";
		break;
    }
    
}


function onClickAccessPinBtn(eventobject)
{
  showMaskPin();
  getAccessPin(eventobject);
}



/*
 set onclick events of Access pin popup
*/
function initAccessPinPopup(){
  //onclick of btns
  //onclick of del
  //onclick of cancel
  popupEnterAccessPin.btnZero.onClick = onClickAccessPinBtn;
  popupEnterAccessPin.btnOne.onClick = onClickAccessPinBtn;
  popupEnterAccessPin.btnTwo.onClick = onClickAccessPinBtn;
  popupEnterAccessPin.btnThree.onClick = onClickAccessPinBtn;
  popupEnterAccessPin.btnFour.onClick = onClickAccessPinBtn;
  popupEnterAccessPin.btnFive.onClick = onClickAccessPinBtn;
  popupEnterAccessPin.btnSix.onClick = onClickAccessPinBtn;
  popupEnterAccessPin.btnSeven.onClick = onClickAccessPinBtn;
  popupEnterAccessPin.btnEight.onClick = onClickAccessPinBtn;
  popupEnterAccessPin.btnNine.onClick = onClickAccessPinBtn;
  
  popupEnterAccessPin.btnDel.onClick = onClickAccessPinBack;
  popupEnterAccessPin.btnClose.onClick = onClickCancelAccessPin;
  
  
}


/* To display access pin popup
*/
function showAccesspinPopup(frmName){
  
   var currForm = "";
   var curFrmObj = kony.application.getCurrentForm();
   if(isNotBlank(curFrmObj))
     currForm = curFrmObj.id;
  
  var flxForms = ["frmFPSetting","frmMBBlockCardCCDBConfirm","frmMBBlockDebitCardConfirm","frmMBChangePINEnterExistsPin","frmMBRequestNewPin","frmMBSavingsCareConfirmation","frmCardlesswithdrawConfirmation","frmMFConfirmMB","frmMFSwitchConfirmMB","frmCAPaymentPlanConfirmation","frmCardActivationDetails","frmBillPaymentEditFutureNew","frmMBFTEdit","frmMBBlockCardRecommendation","frmMFcancelOrderToProcess","frmeditMyProfiles"];
  var frmIndex = flxForms.indexOf(currForm);
  
  if(frmIndex == -1){
      if(isNotBlank(curFrmObj)){
        var context={"widget":curFrmObj.lineAccessPin,"anchor":"top","sizetoanchorwidth":false};
        popupEnterAccessPin.setContext(context);  
      }
     popupEnterAccessPin.lblWrongPin.setVisibility(false); 
     resetAccessPinImg();
     popupEnterAccessPin.lblAccessPin.text = kony.i18n.getLocalizedString("enterpin");
	 popupEnterAccessPin.btnClose.text = kony.i18n.getLocalizedString("CAV05_btnCancel");
     popupEnterAccessPin.btnDel.text = kony.i18n.getLocalizedString("keyDeletebenefit");
    //#ifdef iphone 
     popupEnterAccessPin.hboxAccessPinCircles.padding = [8, 0, 0, 0]
     //#endif
	 
	 //#ifdef android 
	 popupEnterAccessPin.respectDisplayRealSize =true; //  MIB-13935
	 //#endif
     popupEnterAccessPin.show();
  }else{
      kony.print("Before showAccessPinScreenKeypad111");
      showAccessPinScreenKeypad();
  }
}



