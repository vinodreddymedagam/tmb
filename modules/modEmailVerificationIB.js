//Type your code here
gblResendOTPOptionValueIB = 0;
gblEmailOTPTimeIB = 59;
gblEmailIncorrectOTPIB = 0;
function RequestEMailOTPIB(){
  gblEmailIncorrectOTPIB = 0;
  var inputParam = {};
  var locale = kony.i18n.getCurrentLocale();
  inputParam["ChannelId"] = "OTP_EMAIL";
  inputParam["toEmail"] = frmIBCustomerBasicInfo.txtEmail.text;
  inputParam["retryCounterRequestOTP"] = gblResendOTPOptionValueIB;
  frmIBCustomerBasicInfo.btnVerityEmail.onClick = doNothing;
  invokeServiceSecureAsync("RequestOTPEmail", inputParam, RequestEMailOTPIBCallback);
}

function setDefaultUVSectionDisplayValue(){
  
}

function frmIBCustomerBasicInfoInit(){
  frmIBCustomerBasicInfo.lblAccntname.text = "";
  frmIBCustomerBasicInfo.preShow = frmIBCustomerBasicInfoPreshow;
  frmIBCustomerBasicInfo.lblAccntNo.text = "";
  frmIBCustomerBasicInfo.btnChangeEmailId.onClick = changeEmailAddressIBActivation;
  frmIBCustomerBasicInfo.btnVerityEmail.onClick = VerityEmailOTPIB;
  frmIBCustomerBasicInfo.hboxResend.onClick = onClickResendEmailIB;
  frmIBCustomerBasicInfo.txtPin1.onTextChange = onTextChangeEMailOTP;
  frmIBCustomerBasicInfo.txtPin2.onTextChange = onTextChangeEMailOTP;
  frmIBCustomerBasicInfo.txtPin3.onTextChange = onTextChangeEMailOTP;
  frmIBCustomerBasicInfo.txtPin4.onTextChange = onTextChangeEMailOTP;
 // frmIBCustomerBasicInfo.txtPin1.onBeginEditing = onTextChangeEMailOTP;
 // frmIBCustomerBasicInfo.txtPin2.onBeginEditing = onTextChangeEMailOTP;
  //frmIBCustomerBasicInfo.txtPin3.onBeginEditing = onTextChangeEMailOTP;
  //frmIBCustomerBasicInfo.txtPin2.onTextChange = onTextChangeEMailOTP;
 // frmIBCustomerBasicInfo.txtPin3.onTextChange = onTextChangeEMailOTP;
  //frmIBCustomerBasicInfo.txtPin4.onTextChange = onTextChangeEMailOTP;  

}

function frmIBCustomerBasicInfoPreshow(){
  frmIBCustomerBasicInfo.btnVerityEmail.onClick = doNothing;
  gblEmailIncorrectOTPIB = 0;
  frmIBCustomerBasicInfo.hbox50285458136.isVisible = true;
  frmIBCustomerBasicInfo.hboxConfirmEmail.isVisible = false;
  frmIBCustomerBasicInfo.lblInvalidEmail.isVisible = false;
  frmIBCustomerBasicInfo.hboxResend.isVisible = true;
  frmIBCustomerBasicInfo.btnVerityEmail.skin = btnIBGreyBGGreyBorder;
  frmIBCustomerBasicInfo.btnVerityEmail.focusSkin = btnIBGreyBGGreyBorder;
  resetVerifyEmailFormContent();
}

function resetVerifyEmailFormContent(){
  frmIBCustomerBasicInfo.txtPin1.text = "";
  frmIBCustomerBasicInfo.txtPin2.text = "";
  frmIBCustomerBasicInfo.txtPin3.text = "";
  frmIBCustomerBasicInfo.txtPin4.text = "";
  frmIBCustomerBasicInfo.lblResendTime.isVisible = false;
  frmIBCustomerBasicInfo.lblResendTime.text = kony.i18n.getLocalizedString("VRF_msgRemark");
  frmIBCustomerBasicInfo.hboxResend.onClick = onClickResendEmailIB;
  frmIBCustomerBasicInfo.lblResend.skin = lblBlueNormal110;
  frmIBCustomerBasicInfo.hboxOTP1.skin = "hboxIBBottomGreyBorder";
  frmIBCustomerBasicInfo.hboxOTP2.skin = "hboxIBBottomGreyBorder";
  frmIBCustomerBasicInfo.hboxOTP3.skin = "hboxIBBottomGreyBorder";
  frmIBCustomerBasicInfo.hboxOTP4.skin = "hboxIBBottomGreyBorder";
  frmIBCustomerBasicInfo.lblInvalidPin.isVisible = false;
}

function onTextChangeEMailOTP(eventObject){
  frmIBCustomerBasicInfo.hboxOTP1.skin = "hboxIBBottomGreyBorder";
  frmIBCustomerBasicInfo.hboxOTP2.skin = "hboxIBBottomGreyBorder";
  frmIBCustomerBasicInfo.hboxOTP3.skin = "hboxIBBottomGreyBorder";
  frmIBCustomerBasicInfo.hboxOTP4.skin = "hboxIBBottomGreyBorder";
  frmIBCustomerBasicInfo.lblInvalidPin.isVisible = false;
   if(eventObject["id"] == "txtPin1"){
    if(frmIBCustomerBasicInfo.txtPin1.text.length > 0){
      frmIBCustomerBasicInfo.txtPin2.setFocus(true);
    }
  }else if(eventObject["id"] == "txtPin2"){
    if(frmIBCustomerBasicInfo.txtPin2.text.length > 0){
      frmIBCustomerBasicInfo.txtPin3.setFocus(true);
    }
  }else if(eventObject["id"] == "txtPin3"){
    if(frmIBCustomerBasicInfo.txtPin3.text.length > 0){
      frmIBCustomerBasicInfo.txtPin4.setFocus(true);
    }
  } 
  
  var otp = frmIBCustomerBasicInfo.txtPin1.text + frmIBCustomerBasicInfo.txtPin2.text + frmIBCustomerBasicInfo.txtPin3.text + frmIBCustomerBasicInfo.txtPin4.text;
  frmIBCustomerBasicInfo.btnVerityEmail.skin = btnIBGreyBGGreyBorder;
  frmIBCustomerBasicInfo.btnVerityEmail.focusSkin = btnIBGreyBGGreyBorder;
  frmIBCustomerBasicInfo.btnVerityEmail.onClick = doNothing;
  if(otp.length == 4){
    frmIBCustomerBasicInfo.btnVerityEmail.skin = btnIBBlueBGBlueBorderWhiteFont;
    frmIBCustomerBasicInfo.btnVerityEmail.onClick = VerityEmailOTPIB;
    frmIBCustomerBasicInfo.btnVerityEmail.focusSkin = btnIBBlueBGBlueBorderWhiteFont;
  }
}
function RequestEMailOTPIBCallback(status,result){
   dismissLoadingScreen()
      if (status == 400) {
        if (result["opstatus"] == "0") {
          	frmIBCustomerBasicInfo.lblEmailDrescription.text=kony.i18n.getLocalizedString("VRF_txtDesc1").replace("emailRefNo", result['bankRefNo']);
          	frmIBCustomerBasicInfo.lblEmailVerify.text = frmIBCustomerBasicInfo.txtEmail.text;
          	frmIBCustomerBasicInfo.hbox50285458136.isVisible = false;
          	frmIBCustomerBasicInfo.hboxConfirmEmail.isVisible = true;
            frmIBCustomerBasicInfo.hboxResend.isVisible = true;
            frmIBCustomerBasicInfo.lblResend.skin = lblIBLightGrey120pxNormal;
             frmIBCustomerBasicInfo.lblResendTime.isVisible = true;
             frmIBCustomerBasicInfo.lblResendTime.text = kony.i18n.getLocalizedString("VRF_msgRemark");
            var timer = kony.timer.schedule("emailOTPimer", setTimetoEmailCountDownTextIB, 1, true);
        }else{
          showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
        }
      }
}

function VerityEmailOTPIB(){
   	showLoadingScreen();
  	var inputParam = {};
  	var enteredOTP = frmIBCustomerBasicInfo.txtPin1.text + frmIBCustomerBasicInfo.txtPin2.text + frmIBCustomerBasicInfo.txtPin3.text + frmIBCustomerBasicInfo.txtPin4.text
    if(enteredOTP.length < 4){
      alert("Please enter 4 digit OTP");
      return false;
    }
    var locale = kony.i18n.getCurrentLocale();
    inputParam["otp"] = enteredOTP;
  	inputParam["serviceID"] = "verifyOTPEmail";
  	inputParam["retryCounterVerifyOTP"] = gblResendOTPOptionValueIB;
    invokeServiceSecureAsync("verifyOTPEmail", inputParam, VerityEmailOTPIBCallback);
}


function VerityEmailOTPIBCallback(status,result){
  dismissLoadingScreen()
      if (status == 400) {
        if (result["opstatus"] == "0") {
          	updateEmailIB();
          	//IBcrmProfileUpdate();
          	//alert("Activation successfull")
        }else{
          wrongEmailOTPIB();
          //alert("Entered wrong OTP")
        }
      }
}
function CheckEmailStatusIB(){
    showLoadingScreen();
    var inputParam = {};
    inputParam["actionId"] = "getEmailStatus";
  	inputParam["moduleName"] = "Activation";
  	inputParam["emailId"] = frmIBCustomerBasicInfo.txtEmail.text;
  	invokeServiceSecureAsync("emailVerification", inputParam, CheckEmailStatusIBCallback);
}

function CheckEmailStatusIBCallback(status,result){
  dismissLoadingScreen()
      if (status == 400) {
        if (result["opstatus"] == "0") {
          	 if(result["verifyEmailStatus"] == "Y"){
               	//alert("Activation successfull")
               IBcrmProfileUpdate();
          }else{
            resetVerifyEmailFormContent();
            RequestEMailOTPIB();
          }
        }else{
          showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
        }
      }
}
function wrongEmailOTPIB(){
  gblEmailIncorrectOTPIB++;
   if(gblEmailIncorrectOTPIB>=3){
    showAlertWithCallBack(kony.i18n.getLocalizedString("VRF_msgIncorrectCode"), kony.i18n.getLocalizedString("info"), changeEmailAddressIBActivation);
    return false;
  }
  frmIBCustomerBasicInfo.txtPin1.text = "";
  frmIBCustomerBasicInfo.txtPin2.text = "";
  frmIBCustomerBasicInfo.txtPin3.text = "";
  frmIBCustomerBasicInfo.txtPin4.text = "";
  frmIBCustomerBasicInfo.hboxOTP1.skin = "hbxIBbottomRedBorder";
  frmIBCustomerBasicInfo.hboxOTP2.skin = "hbxIBbottomRedBorder";
  frmIBCustomerBasicInfo.hboxOTP3.skin = "hbxIBbottomRedBorder";
  frmIBCustomerBasicInfo.hboxOTP4.skin = "hbxIBbottomRedBorder";
  frmIBCustomerBasicInfo.lblInvalidPin.isVisible = true;
  frmIBCustomerBasicInfo.btnVerityEmail.onClick = doNothing;
   frmIBCustomerBasicInfo.btnVerityEmail.skin = btnIBGreyBGGreyBorder;
  frmIBCustomerBasicInfo.btnVerityEmail.focusSkin = btnIBGreyBGGreyBorder;
  
  
}

function changeEmailAddressIBActivation(){
  frmIBCustomerBasicInfo.txtEmail.text = "";
  gblResendOTPOptionValueIB = 0;
  gblEmailIncorrectOTPIB = 0;
  gblEmailOTPTimeIB = 59;
  frmIBCustomerBasicInfo.hbox50285458136.isVisible = true;
  frmIBCustomerBasicInfo.hboxConfirmEmail.isVisible = false;
  frmIBCustomerBasicInfo.hboxResend.isVisible = true;
  frmIBCustomerBasicInfo.lblResendTime.isVisible = false;
  frmIBCustomerBasicInfo.lblResendTime.text = kony.i18n.getLocalizedString("VRF_msgRemark");
  frmIBCustomerBasicInfo.hboxResend.onClick = onClickResendEmailIB;
  frmIBCustomerBasicInfo.btnVerityEmail.skin = btnIBGreyBGGreyBorder;
  frmIBCustomerBasicInfo.btnVerityEmail.onClick = doNothing;
  frmIBCustomerBasicInfo.btnVerityEmail.focusSkin = btnIBGreyBGGreyBorder;
}


function onClickResendEmailIB(){
   var inputParam = {};
  var locale = kony.i18n.getCurrentLocale();
  inputParam["ChannelId"] = "OTP_EMAIL";
  inputParam["toEmail"] = frmIBCustomerBasicInfo.txtEmail.text;
  inputParam["retryCounterRequestOTP"] = gblResendOTPOptionValueIB;
  invokeServiceSecureAsync("RequestOTPEmail", inputParam, startCountingEMAILOTPTimer);
}
function startCountingEMAILOTPTimer(status,result){
       dismissLoadingScreen()
          if (status == 400) {
          if (result["opstatus"] == "0") {
             showAlert(kony.i18n.getLocalizedString("VRF_msgSentCode").replace("{email}", frmIBCustomerBasicInfo.lblEmailVerify.text), kony.i18n.getLocalizedString("info"))
             frmIBCustomerBasicInfo.hboxResend.onClick = doNothing;
             //frmIBCustomerBasicInfo.hboxResend.isVisible = true;
             frmIBCustomerBasicInfo.lblResend.skin = lblIBLightGrey120pxNormal;
             frmIBCustomerBasicInfo.lblResendTime.isVisible = true;
             frmIBCustomerBasicInfo.lblResendTime.text = kony.i18n.getLocalizedString("VRF_msgRemark");
             frmIBCustomerBasicInfo.lblEmailDrescription.text=kony.i18n.getLocalizedString("VRF_txtDesc1").replace("emailRefNo", result['bankRefNo']);
             if(gblResendOTPOptionValueIB < 3){
               gblResendOTPOptionValueIB++;
               var uniqueIdGen = Math.random().toString(36).substr(2, 9) + "";
               var timer = kony.timer.schedule("emailOTPimer", setTimetoEmailCountDownTextIB, 1, true);
            }else {
                    frmIBCustomerBasicInfo.lblResendTime.isVisible = true;
                    frmIBCustomerBasicInfo.lblResendTime.text = kony.i18n.getLocalizedString("VRF_msgNotReceived");
                    //frmIBCustomerBasicInfo.hboxResend.isVisible = false;
                  }
          }else{
          showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
        }
     }
}

function setTimetoEmailCountDownTextIB(){
  	if(gblEmailOTPTimeIB>0){
     frmIBCustomerBasicInfo.lblResendTime.text = kony.i18n.getLocalizedString("VRF_msgRemark").replace("60", gblEmailOTPTimeIB);
  		gblEmailOTPTimeIB--; 
    }else{
      kony.timer.cancel("emailOTPimer");
      gblEmailOTPTimeIB = 59;
      frmIBCustomerBasicInfo.lblResendTime.isVisible = false;
      frmIBCustomerBasicInfo.lblResendTime.text = kony.i18n.getLocalizedString("VRF_msgRemark");
      frmIBCustomerBasicInfo.hboxResend.onClick = onClickResendEmailIB;
      frmIBCustomerBasicInfo.lblResend.skin = lblBlueNormal110;
       if(gblResendOTPOptionValueIB >= 3){
         frmIBCustomerBasicInfo.lblResendTime.isVisible = true;
         frmIBCustomerBasicInfo.lblResendTime.text = kony.i18n.getLocalizedString("VRF_msgNotReceived");
        // frmIBCustomerBasicInfo.hboxResend.isVisible = true;
         frmIBCustomerBasicInfo.hboxResend.onClick = doNothing;
       }
    }
  	
}

function updateEmailIB(){
    showLoadingScreen();
    var inputParam = {};
    inputParam["actionId"] = "updateEmail";
  	inputParam["moduleName"] = "Activation IB";
  	inputParam["emailId"] = frmIBCustomerBasicInfo.lblEmailVerify.text;
  	invokeServiceSecureAsync("emailVerification", inputParam, updateEmailIBCallback);
}

function updateEmailIBCallback(status,result){
  dismissLoadingScreen()
  if (status == 400) {
    if (result["opstatus"] == "0") {
      	//alert("activation success")
      	IBcrmProfileUpdate();
    }else{
          showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
        }
  }
}

function onClickNextEmailValidationIB(eventobject){
  
  function alert_onClick_98877201224761776_True() {}
    function alert_onClick_40394201224767853_True() {}
    if ((frmIBCustomerBasicInfo.txtEmail.text == "")) {
        function alert_onClick_40394201224767853_Callback() {
            alert_onClick_40394201224767853_True();
        }
        kony.ui.Alert({
            "alertType": constants.ALERT_TYPE_ERROR,
            "alertTitle": kony.i18n.getLocalizedString("Receipent_alert_Error"),
            "yesLabel": kony.i18n.getLocalizedString("keyOK"),
            "noLabel": "No",
            "message": kony.i18n.getLocalizedString("keyMBEnteremailID"),
            "alertHandler": alert_onClick_40394201224767853_Callback
        }, {
            "iconPosition": constants.ALERT_ICON_POSITION_LEFT
        });
        frmIBCustomerBasicInfo.txtEmail.setFocus(true)
    } else {
        if ((emailValidatnIB(frmIBCustomerBasicInfo.txtEmail.text))) {
            CheckEmailStatusIB.call(this);
        } else {
            function alert_onClick_98877201224761776_Callback() {
                alert_onClick_98877201224761776_True();
            }
           frmIBCustomerBasicInfo.lblInvalidEmail.isVisible = true;
          
          /*  kony.ui.Alert({
                "alertType": constants.ALERT_TYPE_ERROR,
                "alertTitle": "Error",
                "yesLabel": kony.i18n.getLocalizedString("keyOK"),
                "noLabel": "No",
                "message": kony.i18n.getLocalizedString("invalidEmail"),
                "alertHandler": alert_onClick_98877201224761776_Callback
            }, {
                "iconPosition": constants.ALERT_ICON_POSITION_LEFT
            });*/
        }
    }
}
