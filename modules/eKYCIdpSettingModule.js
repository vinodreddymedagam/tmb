function frmEKYCIdpSetting_init_Action(){
  frmEKYCIdpSetting.btnBack.onClick = checkNotification;
  //#ifdef android
  frmEKYCIdpSetting.btnIDVerification.onClick = frmEKYCIdpSetting_switchOnOff;
  //#endif
  //#ifdef iphone
  frmEKYCIdpSetting.switchIDVerification.onSlide = frmEKYCIdpSetting_switchOnOff;
  //#endif
  frmEKYCIdpSetting.onDeviceBack = disableBackButton;
  frmEKYCIdpSetting.preShow = frmEKYCIdpSetting_preShow_function;
  frmEKYCIdpSetting.postShow = frmEKYCIdpSetting_postShow_function;
  frmEKYCIdpSetting.btnUpdateIAL.onClick = updateIALViaSettingPage;
  frmEKYCIdpSetting.btnCloseAIL.onClick = closeIAL;
  frmEKYCIdpSetting.btnUpGradeIAL.onClick = frmEKYCIdpSetting_btnUpGradeIAL_function;
}

function frmEKYCIdpSetting_preShow_function(){
  showLoadingScreen();
  frmEKYCIdpSetting.flexIDPPopup.setVisibility(false);
  frmEKYCIdpSetting.lblHeader.text = kony.i18n.getLocalizedString("eKYC_titleAuthenRequest");
  frmEKYCIdpSetting.lblIDVerification.text = kony.i18n.getLocalizedString("eKYC_tgIdPEnrollment");
  frmEKYCIdpSetting.lblSettingDesc.text = kony.i18n.getLocalizedString("eKYC_msgExplainIDPconsent");
  frmEKYCIdpSetting.btnUpGradeIAL.text = kony.i18n.getLocalizedString("eKYC_lkUpgradeIAL");
  //#ifdef android
  frmEKYCIdpSetting.btnIDVerification.setVisibility(true);
  frmEKYCIdpSetting.switchIDVerification.setVisibility(false);
  //#endif
  //#ifdef iphone
  frmEKYCIdpSetting.btnIDVerification.setVisibility(false);
  frmEKYCIdpSetting.switchIDVerification.setVisibility(true);
  //#endif
  var nfcFlag = isNFCEnabled();
  frmEKYCIdpSetting.flxArrow.isVisible = false;
  frmEKYCIdpSetting.flexNoNFC.isVisible = false;
  frmEKYCIdpSetting.btnUpdateIAL.isVisible = false;
  if (null !== nfcFlag && false !== nfcFlag) {
    frmEKYCIdpSetting.btnUpdateIAL.isVisible = true;
  }else{
    frmEKYCIdpSetting.flxArrow.isVisible = true;
    frmEKYCIdpSetting.flexNoNFC.isVisible = true;
  }
}

function frmEKYCIdpSetting_postShow_function(){
  rotateImages(frmEKYCIdpSetting.flexArrow,45);
  dismissLoadingScreen();
}
function frmEKYCIdpSetting_switchOnOff() {
  try{
    if (frmEKYCIdpSetting.btnIDVerification.skin == btnUnCheckedImage) {
      frmEKYCIdpSetting.btnIDVerification.skin = btnCheckedImage;
    } else {
      frmEKYCIdpSetting.btnIDVerification.skin = btnUnCheckedImage;

    }
    if ((frmEKYCIdpSetting.switchIDVerification.selectedIndex == 1 && !isAndroid()) || (frmEKYCIdpSetting.btnIDVerification.skin == btnUnCheckedImage && isAndroid())) {
      showLoadingScreen();
      gblIDVerificationFlag = "N";
      updateEkycIdpSetting();
    } else {
      ekycIdpReadUTFFile();
    }
  }catch (e) {
    kony.print("Error in EKYCSetting"+e);
    kony.print("Error in EKYCSetting"+e.message);
  }
}
function lunchSettingIDPPage(){
  callBackGetIDPFlagFunction();
  frmEKYCIdpSetting.show();
}
function callBackGetIDPFlagFunction(){
  if(gblIDVerificationFlag == "Y"){
    frmEKYCIdpSetting.switchIDVerification.selectedIndex = 0;
    frmEKYCIdpSetting.btnIDVerification.skin = btnCheckedImage;
  }else{
//     showLoadingScreen();
//     gblIDVerificationFlag = "N";
    frmEKYCIdpSetting.switchIDVerification.selectedIndex = 1;
    frmEKYCIdpSetting.btnIDVerification.skin = btnUnCheckedImage;
//     updateEkycIdpSetting();
  }

}
function frmEKYCIdpSetting_btnUpGradeIAL_function(){
	if(gblUpdateIAlCount >= gblAllowUpdateIAl){
      showAlert(kony.i18n.getLocalizedString("eKYC_msgNotAllowtoUpgradeIAL").replace("<Maximum No. of time>",gblAllowUpdateIAl), kony.i18n.getLocalizedString("info"));
    }else{
      openIALPopUp();
    }
}
function ekycIdpReadUTFFile() {
  try{
    gblMBNewTncFlow = "TMB_EKYC_IDP_FLOW";
    var input_param = {};
    var locale = kony.i18n.getCurrentLocale();
    if (locale == "en_US") {
      input_param["localeCd"] = "en_US";
    } else {
      input_param["localeCd"] = "th_TH";
    }
    input_param["activationVia"]="3";
    input_param["moduleKey"] = "TMBeKYCIdp";
    showLoadingScreen();
    invokeServiceSecureAsync("readUTFFile", input_param, ekycIdpReadUTFFileCallBack);
  }catch(err)
  {
  }
}


function ekycIdpReadUTFFileCallBack(status, result) {
  if (status == 400) {
    if (result["opstatus"] == 0) {
      gblDPPk=result["pk"];
      gblDPRandNumber=result["randomNumber"];
      frmMBNewTnCLoan.btnRight.setVisibility(false);
      frmMBNewTnCLoan.lblHdrTxt.text = kony.i18n.getLocalizedString("eKYC_titleTermsConditions");
      frmMBNewTnCLoan.richtextTnC.text = result["fileContent"];
      frmMBNewTnCLoan.lblDescSubTitle.setFocus(true);
      dismissLoadingScreen();
      frmMBNewTnCLoan.show();
    } else {
      dismissLoadingScreen();
      showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
      return false;
    }
  }
}

function frmMBNewTncApplyEkycIdpClickBtnNext(){
  showLoadingScreen();
  var inputParams ={};
  inputParams["moduleName"] = "setAutRequestSettings";
  inputParams["consentFlag"] =  "Y";
  invokeServiceSecureAsync("IDPApprovalFlow", inputParams, enableEkycIdpSettingCallBack);
}
function enableEkycIdpSettingCallBack(status, resulttable){
  if (status == 400) {
    if(resulttable["opstatus"] == 0){
      gblIDVerificationFlag = "Y";
      lunchSettingIDPPage();
    }else{
      showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
      gblIDVerificationFlag = "N";
      lunchSettingIDPPage();
    }
  }else{
    showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
    gblIDVerificationFlag = "N";
    lunchSettingIDPPage();
  }
  dismissLoadingScreen();
}
function updateEkycIdpSetting(){
  showLoadingScreen();
  var inputParams ={};
  inputParams["moduleName"] = "setAutRequestSettings";
  inputParams["consentFlag"] =  "N";
  invokeServiceSecureAsync("IDPApprovalFlow", inputParams, updateEkycIdpSettingCallBack);
}
function updateEkycIdpSettingCallBack(status, resulttable){
  if (status == 400) {
    if(resulttable["opstatus"] == 0){
      gblIDVerificationFlag = "N";
    }else{
      showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
    }
  }else{
    showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
  }
  dismissLoadingScreen();
}
function checkNotification(){
  if(gblIDVerificationFlag == "Y"){
    checkUserPushNotificationEnable(loadEKYCIDPLandingPage);
  }else{
    gblIDVerificationFlag = "N";
    loadEKYCIDPLandingPage();
  }
}

function rotateImages(frmName, rotationAngle){
  var trans100 = kony.ui.makeAffineTransform();
  trans100.rotate(rotationAngle);
  frmName.animate(
    kony.ui.createAnimation({
      "100": {
        "stepConfig": {
          "timingFunction": kony.anim.EASE
        },
        "transform": trans100
      }
    }), {
      "delay": 0,
      "iterationCount": 1,
      "fillMode": kony.anim.FILL_MODE_FORWARDS,
      "duration": 0.001
    }, {
      "animationEnd": null
    });
}
function openIALPopUp(){
  frmEKYCIdpSetting.flexIDPPopup.setVisibility(true);
}
function closeIAL(){
  frmEKYCIdpSetting.flexIDPPopup.setVisibility(false);
}
function updateIALViaSettingPage(){
  gblIALFlow = "frmEKYCIdpSetting";
  gblIALFailCount = 0;
  updateIAL();
}
function continueEKYCIDPFlowPage(){
  gotoCompleteMethodUpdateIAL("success");
}
function LoadLaserCodeFromIALUpdateFlow(){
  frmMBeKYCCitezenIDInfo.txtLaserCode.setVisibility(true);
  frmMBeKYCCitezenIDInfo.txtLaserCode.setFocus(true);
  frmMBeKYCCitezenIDInfo.txtLaserCode.text="";
  var currentdatearray = eKYCCurrentDate();
  frmMBeKYCCitezenIDInfo.calExpireDate.validStartDate = currentdatearray;
  frmMBeKYCCitezenIDInfo.calIssueDate.validEndDate = currentdatearray;
  gblPrevLen=0;
  frmMBeKYCCitezenIDInfo.txtLaserCode.maxTextLength = 14;
  frmMBeKYCCitezenIDInfo.txtLaserCode.onTextChange=ontextchangeLaserCode;
  frmMBeKYCCitezenIDInfo.txtLaserCode.onDone=onDonetxtLaserCode;
  frmMBeKYCCitezenIDInfo.lblsubheader.text=getLocalizedString("eKYC_msgPlsEnterSerialNo");
  frmMBeKYCCitezenIDInfo.imgIDCard.src="ekyc_enter_serial.png";
  frmMBeKYCCitezenIDInfo.flexLineUp.setVisibility(false);
  frmMBeKYCCitezenIDInfo.FlexIssueDate.setVisibility(false);
  frmMBeKYCCitezenIDInfo.Line1.setVisibility(false);
  frmMBeKYCCitezenIDInfo.FlexExpireDate.setVisibility(false);
  frmMBeKYCCitezenIDInfo.FlexExpireDateToggle.setVisibility(false);
  frmMBeKYCCitezenIDInfo.Line3.setVisibility(false);
  frmMBeKYCCitezenIDInfo.Line2.setVisibility(false);
  setHeaderContentToForm(kony.i18n.getLocalizedString("eKYC_TitleEnterCIDSerialNumber"),NavfrmeKYCGetYourIdReady,true,gbleKYCStep);
  setFooterSinglebtnToForm(kony.i18n.getLocalizedString("btnNext"),showfrmMBeKYCFaceRecg);
  frmMBeKYCFaceRecg.flexBodyScroll.setVisibility(true);
  frmMBeKYCFaceRecg.flxFaceRecognitionCheck.setVisibility(false);
}