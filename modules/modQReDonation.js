var gbleDonationType = "";

function showeDonationAmount(){
  if(gblDonationNormalFlow){
    gbleDonationType = "Barcode_Type";
  }else{
    gbleDonationType = "QR_Type"
  }
  kony.print("inside Show eDonation mode");
  frmDonationSelectAmount.show();
}

function setSelectAmountScreen(){
  kony.print("gblqrAmnt"+gblqrAmnt);
  var locale = kony.i18n.getCurrentLocale();
  if (locale == "en_US"){
  		if(gblQRPayData["billerCompCode"].length <= 4){
      		frmDonationSelectAmount.lblFoundationName.text = gblQRPayData["billerNameEN"];
      	} else {
      		frmDonationSelectAmount.lblFoundationName.text = gblQRPayData["toAccountName"];
      	}
  } else if (locale == "th_TH") {
  		if(gblQRPayData["billerCompCode"].length <= 4){
      		frmDonationSelectAmount.lblFoundationName.text = gblQRPayData["billerNameTH"];
      	} else {
      		frmDonationSelectAmount.lblFoundationName.text = gblQRPayData["toAccountName"];
      	}
  }
  frmDonationSelectAmount.lblBillerFoundationGrp.text = gblQRPayData["qrCompCode"];
  var prevForm = kony.application.getPreviousForm().id;
  kony.print("prevForm" +prevForm);
  if(prevForm == "frmDonationSelectAmount" || prevForm == "frmEDonationPaymentConfirm" || prevForm == "frmSelAccntMutualFund"){
      kony.print("Show same");
      frmDonationSelectAmount.flexFoundationImage.setFocus(true);
    }else{
        if(gblqrAmnt == "" || gblqrAmnt == undefined){
          frmDonationSelectAmount.FlexContainerRoundCorners.setVisibility(false);
          frmDonationSelectAmount.lblAmountToDonate.setVisibility(true);
          frmDonationSelectAmount.FlexContainer0fa42aff8647044.setVisibility(true);
          frmDonationSelectAmount.flexDonationAmtTextBox.setVisibility(true);
          frmDonationSelectAmount.flexDisplayAmount.setVisibility(false);
          frmDonationSelectAmount.btnConfirm.skin = "sknBtnLightGreyDisableSKin";
          frmDonationSelectAmount.btnConfirm.focusSkin = "sknBtnLightGreyDisableSKin";
          frmDonationSelectAmount.txtAmount.text = "";
          frmDonationSelectAmount.imgTaxCheckbox.src = "radio_check.png"
          frmDonationSelectAmount.flexDummy.setVisibility(true);
          gblTaxDec = "T";
          assignDonationAmounts();
          setDefaultSkinToDonationAmt();
        }else{
          gblEDonation["amount"] = gblqrAmnt;
          frmDonationSelectAmount.imgTaxCheckbox.src = "radio_check.png"
          gblTaxDec = "T";
          frmDonationSelectAmount.FlexContainerRoundCorners.setVisibility(false);
          frmDonationSelectAmount.lblAmountToDonate.setVisibility(false);
          frmDonationSelectAmount.FlexContainer0fa42aff8647044.setVisibility(false);
          frmDonationSelectAmount.flexDonationAmtTextBox.setVisibility(false);
          frmDonationSelectAmount.flexDisplayAmount.setVisibility(true);
          frmDonationSelectAmount.lblAmount.text = Formatdecimalamount(gblEDonation["amount"]);//+ " " +kony.i18n.getLocalizedString("currencyThaiBaht");
          frmDonationSelectAmount.txtAmount.text = frmDonationSelectAmount.lblAmount.text;
          frmDonationSelectAmount.btnConfirm.skin = "btnBlueSkin";
          frmDonationSelectAmount.btnConfirm.focusSkin = "btnDarkBlueFoc";
          frmDonationSelectAmount.flexDummy.setVisibility(true); 	
        }
    }
}
// function onClickDonateAfterAmount_QR(){
//     showDonationConfirmation();
// }
// function showDonationConfirmation(){
//   kony.print("Inside QR");
//   if(frmDonationSelectAmount.btnConfirm.skin == "btnBlueSkin"){
//     var amount = gblQRPayData["availableBalDisplay"].replace(/,/g, "");
//     var serviceAmount = parseFloat(amount);
//     var inputAmount = parseFloat(gblEDonation["amount"].replace(/,/g, ""));
//     kony.print("serviceAmount"+serviceAmount);
//     kony.print("inputAmount"+inputAmount);
//     if (inputAmount > serviceAmount) {
//       alert("Sorry amount exceeding balance");
//     }else{
//       callGenerateTransferRefNoserviceqrBillPay_donation();
//       frmEDonationPaymentConfirm.show();
//     }
//   }
  
// }

// function frmEDonationPaymentConfirmPreshow_QR(){
//   var locale = kony.i18n.getCurrentLocale();
//   frmEDonationPaymentConfirm.lblConfirmationHeader.text = kony.i18n.getLocalizedString("keyConfirmation");
//   frmEDonationPaymentConfirm.lblFrom.text = kony.i18n.getLocalizedString("MB_eDFromAcct");
//   if (locale == "en_US"){
//         frmEDonationPaymentConfirm.lblFromAcctName.text = gblQRPayData["custAcctName"];
//       	frmEDonationPaymentConfirm.lblBillerName.text = gblQRPayData["toAccountName"];
//   		frmEDonationPaymentConfirm.lblBillerCompCode.text = gblQRPayData["billerCompCode"];
//   } else if (locale == "th_TH") {
//         frmEDonationPaymentConfirm.lblFromAcctName.text = gblQRPayData["custAcctName"];
//       	frmEDonationPaymentConfirm.lblBillerName.text = gblQRPayData["toAccountName"];
//   		frmEDonationPaymentConfirm.lblBillerCompCode.text = gblQRPayData["billerCompCode"];
//   }
  
//   frmEDonationPaymentConfirm.lblChangeAcct.text = kony.i18n.getLocalizedString("MB_eDChangeAcctLink");
//   frmEDonationPaymentConfirm.lblTo.text = kony.i18n.getLocalizedString("MB_eDToBiller");
//   frmEDonationPaymentConfirm.lblAmtDesc.text = kony.i18n.getLocalizedString("MB_eDAmountLab");
//   frmEDonationPaymentConfirm.lblAmountTxt.text = Formatdecimalamount(gblEDonation["amount"]);// + " " +kony.i18n.getLocalizedString("currencyThaiBaht");
//   if(gblTaxDec == "T"){
//     frmEDonationPaymentConfirm.lblTaxDeclared.setVisibility(true);
//     frmEDonationPaymentConfirm.lblTaxDeclared.text = kony.i18n.getLocalizedString("MB_eDTax");
//   }else{
//     frmEDonationPaymentConfirm.lblTaxDeclared.setVisibility(false);
//   }
//   frmEDonationPaymentConfirm.lblPaymentDate.text = removeColonFromEnd(kony.i18n.getLocalizedString("keyPaymentDateTime"));
//   frmEDonationPaymentConfirm.lblTransactionRefNo.text = removeColonFromEnd(kony.i18n.getLocalizedString("keyTransactionRefNo"));
//   frmEDonationPaymentConfirm.transactionRefNoVal.text = gblbilPayRefNum;
//   frmEDonationPaymentConfirm.flxSection2.setVisibility(false);
//   frmEDonationPaymentConfirm.flxLineBox2.setVisibility(false);
//   frmEDonationPaymentConfirm.flxSection3.setVisibility(false);
//   //frmEDonationPaymentConfirm.lblStartOn.text = removeColonFromEnd(kony.i18n.getLocalizedString("keyBillPaymentStartOn"));
//   //frmEDonationPaymentConfirm.lblEnding.text = removeColonFromEnd(kony.i18n.getLocalizedString("Transfer_Ending"));
//   //frmEDonationPaymentConfirm.lblRepeatAs.text = removeColonFromEnd(kony.i18n.getLocalizedString("Transfer_Repeat"));
//   //frmEDonationPaymentConfirm.repeatAsVal.text = getFeqLocale();
//   //frmEDonationPaymentConfirm.lblExecute.text = removeColonFromEnd(kony.i18n.getLocalizedString("TRConfirm_SchExe"));
//   //frmEDonationPaymentConfirm.executeVal.text = "xx"  + " " +kony.i18n.getLocalizedString("keyTimesMB");
//   frmEDonationPaymentConfirm.btnBack.text = kony.i18n.getLocalizedString("MB_eDBackBtn");
//   frmEDonationPaymentConfirm.btnNext.text = kony.i18n.getLocalizedString("keyConfirm");
// }

// function frmEDonationPaymentConfirm_btnBackArrow_QR(){
//   frmDonationSelectAmount.show();
// }

function frmEDonationPaymentConfirm_btnNext_QR(){
	kony.print("onClick Confirm");
  validateQRPayment_donation();
}

function frmQReDonationSelectAccount(){
  if(isSignedUser){
      onSelectFromAccntQRPay();
    } else {
      gblQrSetDefaultAccnt = true;
	  
      if(frmMBPreLoginAccessesPin.FlexQuickBalanceContainer.left == "0%"){
        frmMBPreLoginAccessesPin.FlexQuickBalanceContainer.left="-100%";
        frmMBPreLoginAccessesPin.FlexLoginContainer.left="0%";
      }
      frmMBPreLoginAccessesPin.flexCrossmark.setVisibility(true);
      frmMBPreLoginAccessesPin.flexHeader.setVisibility(false);
      frmMBPreLoginAccessesPin.flexSwipeContainer.top = "20%"
      frmMBPreLoginAccessesPin.FlexContainer0b16eb3557e0043.setVisibility(false);
      frmMBPreLoginAccessesPin.imgQRHeaderLogo.setVisibility(true);
      frmMBPreLoginAccessesPin.LabelLoginForQR.setVisibility(true);
      frmMBPreLoginAccessesPin.LabelLoginForQR.text = kony.i18n.getLocalizedString("msgeLoginforQRPayment");
      frmMBPreLoginAccessesPin.FlexLine.setVisibility(true);
      frmMBPreLoginAccessesPin.FlexLoginContainer.top = "9%"; 
	  showAccessPinScreenQR();
    }
}
function preShowFrmQRSelectAccount_donation() {
  
  if(gblPlatformName == "iPhone"  || gblDeviceInfo["name"] == "iPad"|| gblPlatformName == "iPhone Simulator" ){
    frmQRSelectAccount.segTransFrm.viewConfig = {coverflowConfig: {isCircular: true}};
  }

  frmQRSelectAccount.lblHdrTxt.text = kony.i18n.getLocalizedString("lb_QRselectAccount");
  frmQRSelectAccount.LabelShowDefaultAcct.text = kony.i18n.getLocalizedString("msgQRDefaultAccount");
  frmQRSelectAccount.LinkSetDefaultAcct.text = kony.i18n.getLocalizedString("msgSetQRDefaultAccount");
  frmQRSelectAccount.LabelCaution.text = kony.i18n.getLocalizedString("msg_QRInsufficientFund");
  
  frmQRSelectAccount.flexRefNumber.setVisibility(false);
  frmQRSelectAccount.flexAccountNum.setVisibility(false);
  frmQRSelectAccount.LabelAccountName.setVisibility(false);

  frmQRSelectAccount.ButtonPayNow.setVisibility(true);
  frmQRSelectAccount.HBoxError.setVisibility(false);

  frmQRSelectAccount.LabelShowDefaultAcct.setVisibility(true);
  frmQRSelectAccount.LinkSetDefaultAcct.setVisibility(false);
  frmQRSelectAccount.ButtonPayNow.text = kony.i18n.getLocalizedString("btnPayQR");
  checkAmountQRPay_donation();
}

function checkAmountQRPay_donation() {
  kony.print("Inside Check amount donation");
  enteredAmountF = parseFloat(removeCommos(gblEDonation["amount"]));
  avaiLength = frmSelAccntMutualFund.segTransFrm.selectedItems[0].lblBalance.length;
  avaiAmount = frmSelAccntMutualFund.segTransFrm.selectedItems[0].lblBalance;
  avaiAmount = avaiAmount.replace(/,/g, "");
  avaiAmount = avaiAmount.substring(0, avaiLength - 2);
  avaiAmountF = parseFloat(avaiAmount);
  //TODO add UI
  if (avaiAmountF >= enteredAmountF) {
    frmSelAccntMutualFund.btnTransCnfrmCancel.setVisibility(true);
    frmSelAccntMutualFund.HBoxError.setVisibility(false);
    frmSelAccntMutualFund.btnTransCnfrmCancel.onClick=settingAccountEDonation;

  } else {
    frmSelAccntMutualFund.HBoxError.setVisibility(true);
    frmSelAccntMutualFund.lblErrormsg.text = kony.i18n.getLocalizedString("MB_eDLessAvailBal");
    //frmSelAccntMutualFund.LabelCaution.text = kony.i18n.getLocalizedString("msg_QRInsufficientFund");
    frmSelAccntMutualFund.btnTransCnfrmCancel.setVisibility(false);
  }
}
function frmEDonationPaymentCompleteResponse(){
  kony.print("inside Preshow edonation");
  var locale = kony.i18n.getCurrentLocale();
  if (gblTaxDec == "T"){
  	if (locale == "en_US"){
	  	if(gblQRPayData["billerCompCode"].length <= 4){
	    	frmEDonationPaymentComplete.lblOnBehalfOf.text = kony.i18n.getLocalizedString("MB_onbehalf")+" "+gblQRPayData["billerNameEN"]+ "\n" +kony.i18n.getLocalizedString("MB_eDSuccessTxn");
	    } else {
	    	frmEDonationPaymentComplete.lblOnBehalfOf.text = kony.i18n.getLocalizedString("MB_onbehalf")+" "+gblQRPayData["toAccountName"]+ "\n" +kony.i18n.getLocalizedString("MB_eDSuccessTxn");    
	    }
	} else if (locale == "th_TH") {
	  	if(gblQRPayData["billerCompCode"].length <= 4){
	    	frmEDonationPaymentComplete.lblOnBehalfOf.text = kony.i18n.getLocalizedString("MB_onbehalf")+" "+gblQRPayData["billerNameTH"]+ "\n" +kony.i18n.getLocalizedString("MB_eDSuccessTxn");
	    } else {
	    	frmEDonationPaymentComplete.lblOnBehalfOf.text = kony.i18n.getLocalizedString("MB_onbehalf")+" "+gblQRPayData["toAccountName"]+ "\n" +kony.i18n.getLocalizedString("MB_eDSuccessTxn");    
	    }
	}
  }else{
  	if (locale == "en_US"){
	  	if(gblQRPayData["billerCompCode"].length <= 4){
	    	frmEDonationPaymentComplete.lblOnBehalfOf.text = kony.i18n.getLocalizedString("MB_onbehalf")+" "+gblQRPayData["billerNameEN"]+ "\n" +kony.i18n.getLocalizedString("MB_eDSuccessTxnNoConsent");
	    } else {
	     	frmEDonationPaymentComplete.lblOnBehalfOf.text = kony.i18n.getLocalizedString("MB_onbehalf")+" "+gblQRPayData["toAccountName"]+ "\n" +kony.i18n.getLocalizedString("MB_eDSuccessTxnNoConsent");   
	    }
	} else if (locale == "th_TH") {
	  	if(gblQRPayData["billerCompCode"].length <= 4){
	    	frmEDonationPaymentComplete.lblOnBehalfOf.text = kony.i18n.getLocalizedString("MB_onbehalf")+" "+gblQRPayData["billerNameTH"]+ "\n" +kony.i18n.getLocalizedString("MB_eDSuccessTxnNoConsent");
	    } else {
	     	frmEDonationPaymentComplete.lblOnBehalfOf.text = kony.i18n.getLocalizedString("MB_onbehalf")+" "+gblQRPayData["toAccountName"]+ "\n" +kony.i18n.getLocalizedString("MB_eDSuccessTxnNoConsent");   
	    }
	}
  }

  frmEDonationPaymentComplete.lblDonateFormValue.text = frmEDonationPaymentConfirm.lblFromAcctName.text;
  var imagesUrl ="";
  imagesUrl=loadBillerIcons(gblQRPayData["qrCompCode"]);
  frmEDonationPaymentComplete.imgTo.src = imagesUrl;
  frmEDonationPaymentComplete.imgForm.src = loadBankIcon(gblTMBBankCD);
  frmEDonationPaymentComplete.lblAccountNumberValue.text = gblQRPayData["accountNoFomatted"];
  frmEDonationPaymentComplete.lblAccountNumberValueMasked.text = "xxx-x-" + gblQRPayData["accountNoFomatted"].substring(6, 11) + "-x";
  frmEDonationPaymentComplete.lblAccountNameValue.text = gblQRPayData["accountName"];
  if(gblTaxDec == "T"){
    frmEDonationPaymentComplete.flxTaxDeclared.setVisibility(true);
    frmEDonationPaymentComplete.flxCitizenID.setVisibility(true);
    frmEDonationPaymentComplete.lblTaxDeclared.setVisibility(true);
    frmEDonationPaymentComplete.lblCitizenIDValueMasked.text =maskCitizenID(gblQRPayData["CI_IdentValue"]);
    frmEDonationPaymentComplete.lblCitizenIDValue.text = convetFormatCitizenField(gblQRPayData["CI_IdentValue"]);
    frmEDonationPaymentComplete.imgForm.top = "12dp";
  }else{
    frmEDonationPaymentComplete.flxTaxDeclared.setVisibility(false);
    frmEDonationPaymentComplete.flxCitizenID.setVisibility(false);
    frmEDonationPaymentComplete.lblTaxDeclared.setVisibility(false);
    frmEDonationPaymentComplete.lblCitizenIDValue.text = "";
    frmEDonationPaymentComplete.lblCitizenIDValueMasked.text ="";
    frmEDonationPaymentComplete.imgForm.top = "6dp";
  }
  
 if(locale == "en_US"){
  	if(gblQRPayData["billerCompCode"].length <= 4){
    	frmEDonationPaymentComplete.lblBillerName.text = gblQRPayData["billerNameEN"] + " (" + gblQRPayData["qrCompCode"] + ")";
    } else {
    	frmEDonationPaymentComplete.lblBillerName.text = gblQRPayData["toAccountName"] + " (" + gblQRPayData["qrCompCode"] + ")";
    }
    kony.print("billerNameEN"+gblQRPayData["billerNameEN"]);
  }else if(locale == "th_TH") {
    kony.print("billerNameEN"+gblQRPayData["billerNameTH"]);
  	if(gblQRPayData["billerCompCode"].length <= 4){
    	frmEDonationPaymentComplete.lblBillerName.text = gblQRPayData["billerNameTH"] + " (" + gblQRPayData["qrCompCode"] + ")";
    } else {
    	frmEDonationPaymentComplete.lblBillerName.text = gblQRPayData["toAccountName"] + " (" + gblQRPayData["qrCompCode"] + ")";    
    }
  }
  frmEDonationPaymentComplete.lblBillerCompcode.text = "";
  
  
  frmEDonationPaymentComplete.lblAmountTxt.text = Formatdecimalamount(commaFormatted(parseFloat(removeCommos(gblEDonation["amount"])).toFixed(2)));// + " " +kony.i18n.getLocalizedString("currencyThaiBaht");
  
  frmEDonationPaymentComplete.transactionRefNoVal.text = gblbilPayRefNum;
//   frmEDonationPaymentComplete.lblPaymentDateVal.text = resulttable["paymentServerDate"] + " [" + resulttable["paymentServerTime"] +"]";
  
  //frmEDonationPaymentComplete.lblStartOn.text = removeColonFromEnd(kony.i18n.getLocalizedString("keyBillPaymentStartOn"));
  //frmEDonationPaymentComplete.lblEnding.text = removeColonFromEnd(kony.i18n.getLocalizedString("Transfer_Ending"));
  //frmEDonationPaymentComplete.lblRepeatAs.text = removeColonFromEnd(kony.i18n.getLocalizedString("Transfer_Repeat"));
  //frmEDonationPaymentComplete.repeatAsVal.text = getFeqLocale();
  //frmEDonationPaymentComplete.lblExecute.text = removeColonFromEnd(kony.i18n.getLocalizedString("TRConfirm_SchExe"));
  //frmEDonationPaymentComplete.executeVal.text = "xx"  + " " +kony.i18n.getLocalizedString("keyTimesMB");
  frmEDonationPaymentComplete.flxFooter.setVisibility(true);
  frmEDonationPaymentComplete.flxFooterHistoryView.setVisibility(false);
  frmEDonationPaymentComplete.flxSection2.setVisibility(false);
  frmEDonationPaymentComplete.flxLineBox2.setVisibility(false);
  frmEDonationPaymentComplete.flxSection3.setVisibility(false);
  frmEDonationPaymentComplete.show();
  kony.timer.schedule("edonation", function(){if(kony.application.getPreviousForm().id == "frmEDonationPaymentConfirm"){
    if(frmEDonationPaymentComplete.imagestatusicon.src == "completeico.png")
    autoShareImageQRDonation();
  }kony.timer.cancel("edonation");}, 1, false);
//   gbleDonationType = "";
}

// function validateQRPayment_donation() {
//   var amount = gblqrToAccntBal.replace(/,/g, "");
//   var serviceAmount = parseFloat(amount);
//   var inputAmount = parseFloat(gblEDonation["amount"]);
//   kony.print("serviceAmount"+serviceAmount);
//   if (inputAmount > 0.00) {
//     if (inputAmount > serviceAmount) {
// 		showAlertWithCallBack(kony.i18n.getLocalizedString("MB_eDLessAvailBal"), kony.i18n.getLocalizedString("info"));
//     } else {
//       if (isSignedUser) {
//         gblqrflow="postLogin";
//         gblqrTransferAmnt = gblEDonation["amount"];
//         if(gblpp_isOwnAccount)
//        	{
//           //write code for access pin skip
//           kony.print("validateQRPayment_donation   loadMapforQrPreLoginPay ");
//           loadMapforQrPreLoginPay();
//         }
//         else{
//           kony.print("validateQRPayment_donation   showAccessPinScreenKeypad ");
//         	showAccessPinScreenKeypad();  
//         }
//       } else {
//         gblqrflow="preLogin";
//         gblqrTransferAmnt = gblEDonation["amount"];
//         showAccessPinScreenKeypad();
//       }
//     }
//   } else {
//      showAlertWithCallBack(kony.i18n.getLocalizedString("MB_eDLessAvailBal"), kony.i18n.getLocalizedString("info"));
//   }
// }

function callBackCrmProfileInqForQRPay_donation(status, resulttable) {
  if (status == 400) {
    gblDPPk = undefined != resulttable["pk"] ? resulttable["pk"] : "";
    gblDPRandNumber = undefined != resulttable["randomNumber"] ? resulttable["randomNumber"] : "";
    var reload = undefined != resulttable["reload"] ? resulttable["reload"] : "";
    if (resulttable["opstatus"] == 0) {

      if ("true" == reload) {
        checkCrmProfileInqForQRPay();
        return false;
      }

      var StatusCode = resulttable["statusCode"];
      var Severity = resulttable["Severity"];
      var StatusDesc = resulttable["StatusDesc"];
      gblEmailId = resulttable["emailAddr"];
      gblQRPayData["transferDate"] = resulttable["serverDate"];
      if (StatusCode == 0) {

        if (resulttable["mbFlowStatusIdRs"] == "03") {
          dismissLoadingScreen();
          alert("" + kony.i18n.getLocalizedString("keyErrTxnPasslock"));
          return false;
        }

        gblTransferRefNo = resulttable["acctIdentValue"]
        gblXferLnkdAccnts = resulttable["p2pLinkedAcct"]
        var temp1 = [];
        var temp = {
          ebAccuUsgAmtDaily: resulttable["ebAccuUsgAmtDaily"],
          mbFlowStatusIdRs: resulttable["mbFlowStatusIdRs"]
        }

        kony.table.insert(temp1, temp);
        gblCRMProfileData = temp1;
        dismissLoadingScreen()
        kony.print("here1" + gblTransEmail + gblTrasSMS + gblisTMB);
        if (gblTransEmail == 1 && gblTrasSMS == 1 && gblisTMB == gblTMBBankCD) {
          //alert("here1 inside1");
         if (!verifyExceedAvalibleBalanceQRPay_donation()) {
            return false;
          }  
          kony.print("here12345  checkDepositAccountInqQRPay");
          checkDepositAccountInqQRPay()();

        } else {
          kony.print("here12345  else");
          if (!verifyExceedAvalibleBalanceQRPay_donation()) {
            return false;
          }  
          var UsageLimit;
          var DailyLimit = resulttable["ebMaxLimitAmtCurrent"];
          DailyLimit = parseFloat(DailyLimit)
          if (resulttable["ebAccuUsgAmtDaily"] == "" && resulttable["ebAccuUsgAmtDaily"].length == 0) {
            UsageLimit = 0;
          } else {
            UsageLimit = parseFloat(resulttable["ebAccuUsgAmtDaily"]);
          }
			kony.print("here12");
          var transferAmt = gblEDonation["amount"];
          kony.print("here1");
          var channelLimit = DailyLimit - UsageLimit;
          kony.print("here12d");
          if (channelLimit < 0) {
            channelLimit = 0;
          }
          kony.print("here123");
          var dailylimitExceedMsg = kony.i18n.getLocalizedString("keyUserchannellimitexceedsfortheday")
          kony.print("here1dsds2");
         // dailylimitExceedMsg = dailylimitExceedMsg.replace("{1}", commaFormatted(fixedToTwoDecimal(channelLimit) + ""));
         
          if (transferAmt > channelLimit) {
			dismissLoadingScreen();
			errorText = dailylimitExceedMsg;
			alert(errorText);
			closeApprovalKeypad();
			return false;
          }
/*          
          if ((gblSelTransferMode != 1 && gblSelTransferMode != 0 ) && !isOwnAccountP2P && gblisTMB != gblTMBBankCD && gblPaynow == true) { //Case Transfer to other bank by Prompt Pay                       
            var p2pTranLimit = 0;
            kony.print("here inside 12");
            if (isNotBlank(resulttable["P2P_TRANSACTION_LIMIT"])) {
              p2pTranLimit = parseFloat(resulttable["P2P_TRANSACTION_LIMIT"]);
            }

            var p2pTranlimitExceedMsg = kony.i18n.getLocalizedString("MIB_P2PTRErr_TxnLimit")
            p2pTranlimitExceedMsg = p2pTranlimitExceedMsg.replace("{transaction limit}", commaFormatted(p2pTranLimit + ""));

				kony.print("here1256565");
            //Compare base on minimum value                                                                                
            if (channelLimit < p2pTranLimit) {
              //Display DailyLimit
              if (transferAmt > channelLimit) {
                dismissLoadingScreen();
                errorText = dailylimitExceedMsg;
                  alert(errorText);
                  closeApprovalKeypad();
                  return false;
              }
            }

            if (p2pTranLimit < channelLimit) {
              //Display p2pTranLimit
              if (transferAmt > p2pTranLimit) {
                dismissLoadingScreen();
                errorText = p2pTranlimitExceedMsg;
                  alert(errorText);
                  closeApprovalKeypad();
                  return false;
              }
            }

            if (p2pTranLimit == channelLimit) {
              //Display p2pTranLimit
              if (transferAmt > p2pTranLimit) {
                dismissLoadingScreen();
                errorText = p2pTranlimitExceedMsg;
                  alert(errorText);
                  closeApprovalKeypad();
                  return false;
              }
            }

          } else if (gblSelTransferMode != 1 && isOwnAccountP2P) { //Transfer Own by Prompt Pay
			kony.print("Inside else not needed");
          } else if (gblSelTransferMode != 1 && gblisTMB == gblTMBBankCD && gblPaynow == true) { //Transfer to other TMB by Prompt Pay            
            //Check only daily limit
            kony.print("Inside another else ");
            if (channelLimit < transferAmt) {
              dismissLoadingScreen();
              errorText = dailylimitExceedMsg;
                alert(errorText);
                closeApprovalKeypad();
                return false;                
            }
          } else {                                                                                   
            if (!(gblTransEmail == 1 && gblTrasSMS == 1) && gblisTMB == gblTMBBankCD) {
              if (channelLimit < transferAmt && gblPaynow == true) {
                dismissLoadingScreen();
                errorText = dailylimitExceedMsg;
                  alert(errorText);
                  closeApprovalKeypad();
                  return false;
              }
            }

          } 
*/          
          dismissLoadingScreen();

          if (gblisTMB == null && gblisTMB == "" && gblisTMB == undefined) {
            alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
            //setEnabledTransferLandingPage(true);
            return false;
          }

           transferAmt = gblQRPayData["transferAmt"];
          //TODO EDonation
          if(gbleDonationType == "Barcode_Type"){
             kony.print("End call CRM for normal edonate go to execute");
         	 verifyBillPaymentEdonation(gblNumApproval);
          }else
          if (gblSelTransferMode != 1 && gblisTMB != gblTMBBankCD) {
            if (kony.application.getCurrentForm().id != "frmQRSelectAccount"){
                saveToSessionQRBillPayment();
            }else{
              if(gblpp_isOwnAccount){
                saveToSessionQRBillPayment();
              }
              else{
                  showAccessPinScreenKeypad();  
              }
            }
          }else if (gblisTMB == gblTMBBankCD) {
            if (kony.application.getCurrentForm().id != "frmQRSelectAccount"){
                saveToSessionQRBillPayment();          
            }else{
              if(gblpp_isOwnAccount){
                  saveToSessionQRBillPayment();
              }
              else{
                  showAccessPinScreenKeypad();  
              }
            }            
          }else {
            if (kony.application.getCurrentForm().id != "frmQRSelectAccount")
            {
                saveToSessionQRBillPayment();         
            }else{
              if(gblpp_isOwnAccount)
              {
                 saveToSessionQRBillPayment();
              }
              else{
                  showAccessPinScreenKeypad();  
              }
            }
          } 

        }
      } else {
        dismissLoadingScreen();
        showCommonAlert(resulttable["errMsg"], resulttable["XPServerStatCode"]);
        //setEnabledTransferLandingPage(true);
        return false;
      }
    } else {
      dismissLoadingScreen();
      if (resulttable["opstatus"] == "8005") {
       showAlert(kony.i18n.getLocalizedString("ECGenericError"),kony.i18n.getLocalizedString("info")); //MKI, 8005 error
      } else {
        showCommonAlert(resulttable["errMsg"], resulttable["XPServerStatCode"]);
        //setEnabledTransferLandingPage(true);
      }
    }
  }
}
function callBackDepositAccountInqQRPay_donation(status, resulttable) {
  if (status == 400) {
    if (resulttable["opstatus"] == 0) {
      // fetching To Account Name for TMB Inq
      var StatusCode = resulttable["StatusCode"];
      var Severity = resulttable["Severity"];
      var StatusDesc = resulttable["StatusDesc"];
      dismissLoadingScreen();

      if (StatusCode != "0") {
        errorText = kony.i18n.getLocalizedString("Receipent_alert_correctdetails");
        alert(errorText);
        return false;
      } else {
        var ToAccountName = resulttable["accountTitle"]
        kony.print("mki8"+resulttable["accountTitle"]);
        //gblQRPayData["custName"] = resulttable["accountTitle"]; // MKI, MIB-0000, Incorrect From account in TMB bank QR trasfer completion.screen shot screen
        if (ToAccountName != null && ToAccountName.length > 0) {} else {
          errorText = kony.i18n.getLocalizedString("Receipent_alert_correctdetails");
          alert(errorText);
          return false;
        } /** checking if transfer is happeninf from TD account(i.e AccountPreWithDrawInq  */
        if (gbltdFlag != kony.i18n.getLocalizedString("termDeposit")) {
          checkFundTransferInqMBQRPay();
        }
      }
    } else {
      dismissLoadingScreen();
      showCommonAlert(resulttable["errMsg"], resulttable["XPServerStatCode"]);
    }
  } else {
    //setEnabledTransferLandingPage(true);
  }
}

function callBackMBCompositeBillQRPay_donation(status, resultTable) {
    try {
        if (status == 400) {
          if (resultTable["opstatus"] == 0 && typeof(resultTable["paymentServerDate"]) != "undefined"){
              kony.print("Inside else true");
              closeApprovalKeypad();
              gblEDonation["paymentServerDate"] = resultTable["paymentServerDate"];
              frmEDonationPaymentComplete.lblPaymentDateVal.text = gblserverDate;
              gblQRPayData["CI_IdentValue"] = resultTable["CI_IdentValue"];
              frmEDonationPaymentComplete.hbxShareOption.setVisibility(true);
              frmEDonationPaymentComplete.flxBody.height = "70%";
              frmEDonationPaymentComplete.flxTopSection.setVisibility(true);
              frmEDonationPaymentComplete.imagestatusicon.src = "completeico.png";
              frmEDonationPaymentComplete.lblPaymentDateVal.text = resultTable["paymentServerDate"] + " [" + resultTable["paymentServerTime"] +"]";
              frmEDonationPaymentCompleteResponse();
              dismissLoadingScreen();
          }else if(resultTable["errCode"] == "VrfyAcPWDErr00001" || resultTable["errCode"] == "VrfyAcPWDErr00002" || resultTable["errCode"] == "E10020"){
            gblTotalPinAttemptsTransfers = 3;
              dismissLoadingScreen();
              resetKeypadApproval();           
              var badLoginCount = resultTable["badLoginCount"];
              var incorrectPinText = kony.i18n.getLocalizedString("PIN_Incorrect");
              incorrectPinText = incorrectPinText.replace("{rem_attempt}", gblTotalPinAttemptsTransfers - badLoginCount); 
              frmEDonationPaymentConfirm.lblForgotPin.text = incorrectPinText; //kony.i18n.getLocalizedString("invalidCurrPIN"); //MKI, MIB-9892
              frmEDonationPaymentConfirm.lblForgotPin.skin = "lblBlackMed150NewRed";
              frmEDonationPaymentConfirm.lblForgotPin.onTouchEnd = doNothing;
            } else if (resultTable["errCode"] == "VrfyAcPWDErr00003") {
              closeApprovalKeypad();
              dismissLoadingScreen();
              //showTranPwdLockedPopup();
              gotoUVPINLockedScreenPopUp();
              
            } else {
              dismissLoadingScreen();
              closeApprovalKeypad();
              alert(kony.i18n.getLocalizedString("ECGenericError"));
            }    
        }
    } catch (ex) {
        kony.print("callBackMBCompositeBillQRPay_donation ex=" + ex.message);
    }
}

function verifyExceedAvalibleBalanceQRPay_donation() {

  var availableBal;
  var i = gbltranFromSelIndex[1];
  kony.print("Inside Check balance")
  availableBal = gblqrToAccntBal;
  var findDot;
  var isCrtFormt;
  enteredAmount = gblEDonation["amount"];

  var fee = parseFloat(gblQRPayData["fee"]);
  kony.print("Inside Check balance" + enteredAmount + availableBal + " Flags" + gblPaynow + gblSelTransferMode);
  if (enteredAmount != null && enteredAmount != undefined && (availableBal < (parseFloat(enteredAmount) + fee)) && gblPaynow) {
   kony.print("Inside Check balance  if")
    if (gblSelTransferMode != 1) {
      errorText = kony.i18n.getLocalizedString("msg_QRInsufficientFund");
      alert(errorText);
      return false;
    }
  }
  return true;
}

function callGenerateTransferRefNoserviceqrBillPay_donation() {
    inputParam = {};
    inputParam["transRefType"] = "NB";
    invokeServiceSecureAsync("generateTransferRefNo", inputParam, generateTransferRefNoserviceQRBillPayCallBack_donation);
}


function generateTransferRefNoserviceQRBillPayCallBack_donation(status, result) {


    if (status == 400) //success responce
    {
        if (result["opstatus"] == 0) {
            gblbilPayRefNum = result["transRefNum"] + "00"; 
          	gblserverDate = result["serverDate"];
          frmEDonationPaymentConfirm.transactionRefNoVal.text = result["transRefNum"] + "00"; 
          frmEDonationPaymentConfirm.lblPaymentDateVal.text = result["serverDate"]; 
        } else {
            dismissLoadingScreenPopup();
            alert(" " + result["errMsg"]);
        }
    }
}

function frmEDonationPaymentComplete_btnNext_QR(){
  gblQRPayData = {};
  gblqrmobileno = "";
  gblEDonation = {};
  showQRScanningLanding();
}

function frmEDonationPaymentComplete_btnBack_QR(){
  gblQRPayData = {};
  gblEDonation = {};
  gblqrmobileno = "";
  frmQRPaymentSuccessOnClickDone();
}

function autoShareImageQRDonation(){
  //#ifdef android
    shareIntentmoreCallandroid("screenshot","eDonation");
  //#endif
  
  //#ifdef iphone
	screenShotCall("screenshot");
  //#endif
}