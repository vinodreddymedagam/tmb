function callMFOrderValidation() {
  inputParam = {};
  inputParam["toAccountNo"] = gblMFOrder["MFtoAccountKey"]; //actual value
  inputParam["billerMethod"] = "0"; // offline
  inputParam["fromAccountNo"] = removeHyphenIB(gblAccountNoval); // yes we have
  inputParam["ref1"] = removeHyphenIB(gblMFOrder["unitHolderNo"]); //unit holder number
  inputParam["amount"] = frmIBMFSelPurchaseFundsEntry.txtAmountVal.text; // amount
  inputParam["compCode"] = gblMFOrder["fundHouseCode"]; //fundHouseCode
  inputParam["orderType"] = MF_ORD_TYPE_PURCHASE;
  inputParam["flex1"] = gblMFOrder["fundHouseCode"];
  inputParam["unitHolderNo"] = gblMFOrder["unitHolderNo"];
  inputParam["fundCode"] = gblFundRulesData["fundCode"];
  inputParam["flex2"] = "0";
  inputParam["typeID"] = "027";
  inputParam["billerId"] = gblBillerId;
  inputParam["fundShortName"] = gblMFOrder["fundShortName"];
  invokeServiceSecureAsync("MFOrderValidationService", inputParam, MFOrderValidationServiceCallBack);
}

function OTPTimercallbackMFOrder() {
  frmIBMFConfirm.btnOTPReq.skin = "btnIBREQotpFocus";
  frmIBMFConfirm.btnOTPReq.setEnabled(true);
  frmIBMFConfirm.btnOTPReq.onClick = orderMutualFundOTPRequestService;

  try {
    kony.timer.cancel("OtpTimer");
  } catch (e) {

  }
}

function saveToSessionMFOrder(){
  inputParam = {};

  inputParam["billerMethod"] = "0";
  inputParam["fromAccountNo"] = "";
  inputParam["ref1"] = "";
  inputParam["amount"] = gblMFOrder["amount"];
  inputParam["compCode"] = gblMFOrder["fundHouseCode"];  //fundHouseCode
  inputParam["orderType"] = gblMFOrder["orderType"];
  inputParam["fundCode"] = gblMFOrder["fundCode"];
  inputParam["unitHolderNo"] = gblMFOrder["unitHolderNo"];
  inputParam["fundShortName"] = gblMFOrder["fundShortName"];
  inputParam["ref2"] = "";
  inputParam["typeID"] = "027";
  inputParam["flex1"] = "";
  inputParam["flex2"] = "";

  if(gblMFOrder["orderType"] == MF_ORD_TYPE_REDEEM){
    inputParam["toAccountNo"] = ""; 
    if(gblMFOrder["redeemUnit"] == ORD_UNIT_ALL) {
      inputParam["redeemType"] = ORD_UNIT_UNIT;
    } else {
      inputParam["redeemType"] = gblMFOrder["redeemUnit"];
    }
  } else if(gblMFOrder["orderType"] == MF_ORD_TYPE_PURCHASE){
    inputParam["redeemType"] = "";
	if(gbltdFlag == "CCA"){
      inputParam["cardRefId"] = gblMFOrder["fromAccountNo"].split("-").join("");
      inputParam["fromAccountNo"] = gblMFOrder["fromAccountNo"].split("-").join("");
    }else{
      inputParam["fromAccountNo"] = gblMFOrder["fromAccountNo"].split("-").join("");
    }
    inputParam["toAccountNo"] = gblMFOrder["MFtoAccountKey"]; //actual value
    inputParam["fromAccountNo"] = gblMFOrder["fromAccountNo"].split("-").join("");
    inputParam["ref1"] = gblMFOrder["unitHolderNo"];
    inputParam["flex1"] = gblMFOrder["fundHouseCode"]; 
    inputParam["flex2"] = "0";
    inputParam["billerId"] = gblBillerId;
  }


  //billerName = billerName.substring(0, billerName.lastIndexOf("("));


  invokeServiceSecureAsync("MFOrderValidationService", inputParam, MFOrderValidationServiceCallBack);
}

function MFOrderValidationServiceCallBack(status, resulttable) {
  if (status == 400) {
    if (resulttable["opstatus"] == 0) {
      if (gblChannel == "IB") {
        frmIBMFConfirm.tbxToken.text = "";
        frmIBMFConfirm.lblOTPinCurr.text = " "; //kony.i18n.getLocalizedString("invalidOTP"); //
        frmIBMFConfirm.lblPlsReEnter.text = " ";
        if (gblSwitchToken == false && gblTokenSwitchFlag == false) {
          checkMFOrderValidationPwdTokenFlag();
        } else if (gblTokenSwitchFlag == true && gblSwitchToken == false) {

          frmIBMFConfirm.hbxToken.setVisibility(true);
          frmIBMFConfirm.tbxToken.setFocus(true);
          frmIBMFConfirm.hbxOTPEntry.setVisibility(false);
          frmIBMFConfirm.lblBankRef.setVisibility(false);
          frmIBMFConfirm.lblkeyOTPMsg.text = kony.i18n.getLocalizedString("Receipent_tokenId");

        } else if (gblTokenSwitchFlag == false && gblSwitchToken == true) {
          frmIBMFConfirm.lblBankRef.setVisibility(true);
          frmIBMFConfirm.hbxToken.setVisibility(false);
          frmIBMFConfirm.hbxOTPEntry.setVisibility(true);
          frmIBMFConfirm.txtBxOTP.setFocus(true);
          frmIBMFConfirm.lblkeyOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
          orderMutualFundOTPRequestService();
        }

      } else if (gblChannel == "MB") {
        showMutualFundsPwdPopupForProcess();
      }
    }
  }
}

function checkMFOrderValidationPwdTokenFlag() {
  var inputParam = [];
  inputParam["crmId"] = gblcrmId;
  showLoadingScreenPopup();
  invokeServiceSecureAsync("tokenSwitching", inputParam, MFOrderPwdTokenFlagCallbackfunction);
}

function MFOrderPwdTokenFlagCallbackfunction(status, callbackResponse) {
  var currForm = kony.application.getCurrentForm().id;

  if (status == 400) {
    if (callbackResponse["opstatus"] == 0) {
      if (callbackResponse["deviceFlag"].length == 0) {
        orderMutualFundOTPRequestService();
        //return
      }
      if (callbackResponse["deviceFlag"].length > 0) {
        var tokenFlag = callbackResponse["deviceFlag"][0]["TOKEN_DEVICE_FLAG"];
        var mediaPreference = callbackResponse["deviceFlag"][0]["MEDIA_PREFERENCE"];
        var tokenStatus = callbackResponse["deviceFlag"][0]["TOKEN_STATUS_ID"];

        if (tokenFlag == "Y" && (mediaPreference == "Token" || mediaPreference == "TOKEN") && tokenStatus == '02') {
          frmIBMFConfirm.tbxToken.text = "";
          frmIBMFConfirm.lblOTPinCurr.text = " "; //kony.i18n.getLocalizedString("invalidOTP"); //
          frmIBMFConfirm.lblPlsReEnter.text = " ";
          frmIBMFConfirm.hbxOTPincurrect.isVisible = false;
          dismissLoadingScreenPopup();
          frmIBMFConfirm.hbxToken.setVisibility(true);
          frmIBMFConfirm.tbxToken.setFocus(true);
          frmIBMFConfirm.hbxOTPEntry.setVisibility(false);
          //frmIBMFConfirm.label476047582115288.text=kony.i18n.getLocalizedString("keyPleaseEnterToken");
          gblTokenSwitchFlag = true;
          //frmIBMFConfirm.label476047582115277.setVisibility(false);
          //frmIBMFConfirm.lblPhoneNo.setVisibility(false);
          frmIBMFConfirm.lblBankRef.setVisibility(false);
          frmIBMFConfirm.lblkeyOTPMsg.text = kony.i18n.getLocalizedString("Receipent_tokenId");
          frmIBMFConfirm.show();
        } else {

          frmIBMFConfirm.lblBankRef.setVisibility(true);
          frmIBMFConfirm.hbxToken.setVisibility(false);
          frmIBMFConfirm.hbxOTPEntry.setVisibility(true);
          frmIBMFConfirm.txtBxOTP.setFocus(true);
          frmIBMFConfirm.lblkeyOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
          invokeRequestOtpChngPWD();
          gblTokenSwitchFlag = false;
        }
      } else {
        frmIBMFConfirm.tbxToken.text = "";
        frmIBMFConfirm.lblOTPinCurr.text = " "; //kony.i18n.getLocalizedString("invalidOTP"); //
        frmIBMFConfirm.lblPlsReEnter.text = " ";
        frmIBMFConfirm.hbxOTPincurrect.isVisible = false;
        frmIBMFConfirm.lblBankRef.setVisibility(true);
        frmIBMFConfirm.hbxToken.setVisibility(false);
        frmIBMFConfirm.hbxOTPEntry.setVisibility(true);
        frmIBMFConfirm.txtBxOTP.setFocus(true);
        frmIBMFConfirm.lblkeyOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
        invokeRequestOtpChngPWD();
        gblTokenSwitchFlag = false;
      }

    } else dismissLoadingScreenPopup();
  } else dismissLoadingScreenPopup();

}

function orderMutualFundOTPRequestService() {

  frmIBMFConfirm.txtBxOTP.text = "";
  frmIBMFConfirm.tbxToken.text = "";
  var locale = kony.i18n.getCurrentLocale();
  var eventNotificationPolicy;
  var SMSSubject;
  var Channel;

  frmIBMFConfirm.lblOTPinCurr.text = " "; //kony.i18n.getLocalizedString("invalidOTP"); //
  frmIBMFConfirm.lblPlsReEnter.text = " ";
  frmIBMFConfirm.hbxOTPincurrect.isVisible = false;
  if(gblMFOrder["orderType"] == MF_ORD_TYPE_REDEEM){
    Channel = "MFRedeem";
  } else if(gblMFOrder["orderType"] == MF_ORD_TYPE_PURCHASE){
    Channel = "MFPurchase";
  }


  frmIBMFConfirm.hbxOTPincurrect.isVisible = false;
  frmIBMFConfirm.hbxOTPEntry.isVisible = true;
  frmIBMFConfirm.hbxOtpBox.isVisible = true;
  frmIBMFConfirm.txtBxOTP.setFocus(true); 
  var inputParams = {
    retryCounterRequestOTP: gblRetryCountRequestOTP,
    Channel: Channel,
    billerCategoryID: "0", //gblBillerCategoryID,
    orderType: gblMFOrder["orderType"],
    locale: locale
  };
  showLoadingScreenPopup();
  try {
    invokeServiceSecureAsync("generateOTPWithUser", inputParams, orderMutualFundOTPRequestServiceCallBack);
  } catch (e) {
    // todo: handle exception
    invokeCommonIBLogger("Exception in invoking service");
  }
}

function orderMutualFundOTPRequestServiceCallBack(status, callBackResponse) {

  if (status == 400) {
    dismissLoadingScreenPopup();
    if (callBackResponse["errCode"] == "GenOTPRtyErr00001") {
      frmIBMFConfirm.hbxOTPsnt.setVisibility(false);
      frmIBMFConfirm.hbox476047582127699.isVisible = false;
      showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
      return false;
    }
    if (callBackResponse["errCode"] == "GenOTPRtyErr00002") {
      frmIBMFConfirm.hbxOTPsnt.setVisibility(false);
      frmIBMFConfirm.hbox476047582127699.isVisible = false;
      showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00002"), kony.i18n.getLocalizedString("info"));
      return false;
    }
    if (callBackResponse["opstatus"] == 0) {
      if (callBackResponse["errCode"] == "GenOTPRtyErr00001") {
        showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
        return false;
      } else if (callBackResponse["errCode"] == "JavaErr00001") {
        showAlert(kony.i18n.getLocalizedString("ECJavaErr00001"), kony.i18n.getLocalizedString("info"));
        return false;
      }
      //var reqOtpTimer = kony.os.toNumber(callBackResponse["requestOTPEnableTime"]);
      gblRetryCountRequestOTP = kony.os.toNumber(callBackResponse["retryCounterRequestOTP"]);
      gblOTPLENGTH = kony.os.toNumber(callBackResponse["otpLength"]);

      var reqOtpTimer = kony.os.toNumber(callBackResponse["requestOTPEnableTime"]);
      curr_form = kony.application.getCurrentForm();
      try {
        kony.timer.cancel("OTPTimer");
      } catch (e) {
        // todo: handle exception
      }
      if (gblOTPReqLimit < 3) {
        kony.timer.schedule("OtpTimer", OTPTimercallbackMFOrder, reqOtpTimer, false);
      }

      frmIBMFConfirm.show();

      frmIBMFConfirm.hbox476047582127699.setVisibility(true);
      frmIBMFConfirm.hbxOTPsnt.setVisibility(true);
      frmIBMFConfirm.txtBxOTP.setFocus(true);
      frmIBMFConfirm.lblBankRef.setVisibility(true);
      frmIBMFConfirm.lblBankRef.text = kony.i18n.getLocalizedString("keyIBbankrefno");

      frmIBMFConfirm.lblOTPinCurr.text = kony.i18n.getLocalizedString("keyIBbankrefno");
      var refVal="";
      for(var d=0;d<callBackResponse["Collection1"].length;d++){
        if(callBackResponse["Collection1"][d]["keyName"] == "pac"){
          refVal=callBackResponse["Collection1"][d]["ValueString"];
          break;
        }
      }
      frmIBMFConfirm.lblBankRefVal.text = refVal//callBackResponse["Collection1"][8]["ValueString"];
      frmIBMFConfirm.lblKeyotpmg.text = kony.i18n.getLocalizedString("keyotpmsg");
      frmIBMFConfirm.label476047582115279.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
      //frmIBMFConfirm.btnOTPReq.skin = btnIB158disabled;
      frmIBMFConfirm.btnOTPReq.skin = btnIBREQotp;
      frmIBMFConfirm.btnOTPReq.hoverSkin=btnIBREQotp;
      frmIBMFConfirm.btnOTPReq.setEnabled(false);


    } 
    else if (callBackResponse["opstatus"] == 8005 || callBackResponse["code"] == 10403) {
      popIBTransNowOTPLocked.show();
    }
    else {
      if(callBackResponse["errMsg"] != undefined)
      {
        alert(callBackResponse["errMsg"]);
      }
      else
      {
        alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
      }		
    }
  }  
}
function frmIBMFConfirmOnClickSwitchToMobileOTP() {
  frmIBMFConfirm.hbxToken.setVisibility(false);
  frmIBMFConfirm.hbxOTPEntry.setVisibility(true);
  frmIBMFConfirm.txtBxOTP.setFocus(true);
  frmIBMFConfirm.lblkeyOTPMsg.text=kony.i18n.getLocalizedString("keyotpmsgreq");
  frmIBMFConfirm.lblBankRef.text = kony.i18n.getLocalizedString("keyIBbankrefno");

  frmIBMFConfirm.lblOTPinCurr.text = kony.i18n.getLocalizedString("keyIBbankrefno");
  gblTokenSwitchFlag = false;
  orderMutualFundOTPRequestService()
}

function confirmMFOrderCompositeJavaService() {
  var inputParam = {};
  showLoadingScreenPopup();
  //
  if (gblMFOrder["orderType"] == MF_ORD_TYPE_PURCHASE) {
    var tranCode;
    var fromAccTypeTrn = gblAccountType;
    if (fromAccTypeTrn == "DDA") {
      tranCode = "8810";
    } else {
      tranCode = "8820";
    }
    var fromAcctNo = removeHyphenIB(gblAccountNoval);
    var fromAccType;
    if (fromAcctNo.length == 14) {
      fromAccType = "SDA";
    } else {
      fourthDigit = fromAcctNo.charAt(3);
      if (fourthDigit == "2" || fourthDigit == "7" || fourthDigit == "9") {
        fromAccType = "SDA";
        fromAcctNo = "0000" + fromAcctNo;
      } else {
        fromAccType = "DDA";
      }
    }
  }
  //
  inputParam["orderType"] = gblMFOrder["orderType"];
  inputParam["fundShortName"] = gblMFOrder["fundShortName"];
  if (gblMFOrder["orderType"] == MF_ORD_TYPE_REDEEM) {
    inputParam["gblFromAccountSummary"] = "";
  } else if (gblMFOrder["orderType"] == MF_ORD_TYPE_PURCHASE) {}
  if (gblTokenSwitchFlag == true) {
    var otpToken = frmIBMFConfirm.tbxToken.text;
    if (otpToken != null) {
      otpToken = otpToken.trim();
    } else {
      dismissLoadingScreenPopup();
      alert(kony.i18n.getLocalizedString("Receipent_tokenId"));
      return false;
    }
    if (otpToken == "") {
      dismissLoadingScreenPopup();
      alert(kony.i18n.getLocalizedString("Receipent_tokenId"));
      return false;
    }
    inputParam["verifyToken_loginModuleId"] = "IB_HWTKN";
    inputParam["verifyToken_userStoreId"] = "DefaultStore";
    inputParam["verifyToken_retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
    inputParam["verifyToken_retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
    inputParam["password"] = otpToken; //frmIBBillPaymentConfirm.tbxToken.text;
    inputParam["verifyToken_segmentId"] = "segmentId";
    inputParam["TokenSwitchFlag"] = true;
    inputParam["verifyToken_segmentIdVal"] = "MIB";
  } else {
    var otpText = frmIBMFConfirm.txtBxOTP.text;
    if (otpText != null) {
      otpText = otpText.trim();
    } else {
      dismissLoadingScreenPopup();
      alert(kony.i18n.getLocalizedString("Receipent_alert_correctOTP"));
      return false;
    }
    if (otpText == "") {
      dismissLoadingScreenPopup();
      alert(kony.i18n.getLocalizedString("Receipent_alert_correctOTP"));
      return false;
    }
    inputParam["verifyPwd_retryCounterVerifyOTP"] = gblRetryCountRequestOTP;
    inputParam["password"] = otpText;
  }
  var billerMethod = "0";
  var transferFee = "0.00";
  var transferAmount = "0.00";
  inputParam["compCode"] = "";
  inputParam["mforder_fundHouseCode"] = gblMFOrder["fundHouseCode"];
  if (inputParam["orderType"] == MF_ORD_TYPE_REDEEM) {
    if (gblMFOrder["redeemUnit"] == ORD_UNIT_ALL) {
      inputParam["redeemType"] = ORD_UNIT_UNIT;
    } else {
      inputParam["redeemType"] = gblMFOrder["redeemUnit"];
    }
    inputParam["redeemAmount"] = gblMFOrder["amount"];
    inputParam["unitHolderNo"] = gblMFOrder["unitHolderNo"];
    inputParam["mforder_invoiceNum"] = gblFundRulesData["fundCode"];
    inputParam["fundCode"] = gblMFOrder["fundCode"];
  } else if (inputParam["orderType"] == MF_ORD_TYPE_PURCHASE) {
    inputParam["mforder_fromAcctNo"] = removeHyphenIB(gblAccountNoval);
    inputParam["mforder_fromAcctTypeValue"] = fromAccType
    inputParam["mforder_toAcctNo"] = gblMFOrder["MFtoAccountKey"];
    inputParam["mforder_toAcctTypeValue"] = gblAccountType;
    inputParam["mforder_toFIIdent"] = "";
    inputParam["mforder_transferAmt"] = gblMFOrder["amount"];
    inputParam["mforder_billPmtFee"] = "0"
    inputParam["mforder_pmtRefIdent"] = removeHyphenIB(gblMFOrder["unitHolderNo"]);
    inputParam["mforder_invoiceNum"] = gblFundRulesData["fundCode"];
    inputParam["mforder_PostedDt"] = getTodaysDate();
    inputParam["mforder_postedTime"] = ""
    inputParam["mforder_EPAYCode"] = "";
    inputParam["mforder_CompCode"] = MFBillerCompcode;
    inputParam["mforder_frmFiident"] = "";
    inputParam["mforder_tranCode"] = tranCode;
    inputParam["mforder_channelName"] = "IB"
    inputParam["mforder_ClientDt"] = ""
    inputParam["mforder_rqUUId"] = ""

  }
  inputParam["serviceID"] = "MFOrderCompositeJavaService";
  invokeServiceSecureAsync("MFOrderCompositeJavaService", inputParam, confirmMFOrderCompositeJavaServiceCallBack);
}

function confirmMFOrderCompositeJavaServiceCallBack(status, resulttable) {
  if (status == 400) {
    if (resulttable["opstatus"] == 0) {
      if (GBL_MF_TEMENOS_ENABLE_FLAG != "ON" && gblMFOrder["fundHouseCode"] == "TMBAM" && resulttable["errMsg"] != "OK"){
        dismissLoadingScreenPopup();
        preShowfrmIBMFComplete();
        frmIBMFComplete.ImageStep.setVisibility(false);
        frmIBMFComplete.HboxContents.setVisibility(false);
        frmIBMFComplete.HboxFailedContents.setVisibility(true);
        frmIBMFComplete.show();
      }else{
        gblMFOrder["refID"] = resulttable["transactionRef"];
        if (GBL_MF_TEMENOS_ENABLE_FLAG != "ON" && gblMFOrder["fundHouseCode"] == "TMBAM"){
          gblMFOrder["orderTime"] = resulttable["orderTime"];
          var paymentMethod = "";
          var payment_type = "";
          var payin_bank_code = "";
          var payin_acc_no = "";         		

          if (gblMFOrder["orderType"] == MF_ORD_TYPE_REDEEM){
            if(resulttable["payment_type"] == null || resulttable["payment_type"] == undefined || resulttable["payment_type"] == ""){
              gblMFOrder["paymentType"] = "";
            }else{
              gblMFOrder["paymentType"] = resulttable["payment_type"];       			
            }
            if(resulttable["payin_bank_code"] == null || resulttable["payin_bank_code"] == undefined || resulttable["payin_bank_code"] == ""){
              payin_bank_code = "";
            }else{
              payin_bank_code = resulttable["payin_bank_code"] + " ";
            }
            if(resulttable["payin_acc_no"] == null || resulttable["payin_acc_no"] == undefined || resulttable["payin_acc_no"] == ""){
              payin_acc_no = "";
            }else{
              payin_acc_no = resulttable["payin_acc_no"];
            }	          		
            gblMFOrder["paymentDetail"] = payin_bank_code + payin_acc_no;
          }else{
            gblMFOrder["paymentType"] = "";
            gblMFOrder["paymentDetail"] = "";
          } 
        }else{
          gblMFOrder["orderTime"] = resulttable["transactionDate"].substring(10, resulttable["transactionDate"].lenght);
        }     
        temptransDate = resulttable["transactionDate"]; //YYYY-MM-DD
        tempYear = temptransDate.substring(0,4);
        tempMonth = temptransDate.substring(5,7);
        tempDate = temptransDate.substring(8,10);
        transDate = tempMonth + "/" + tempDate + "/" + tempYear;
        gblMFOrder["orderDate"] = transDate;

        tempeffectiveDate = resulttable["effectiveDate"]; //YYYY-MM-DD
        tempYear = tempeffectiveDate.substring(0,4);
        tempMonth = tempeffectiveDate.substring(5,7);
        tempDate = tempeffectiveDate.substring(8,10);
        effectiveDate = tempMonth + "/" + tempDate + "/" + tempYear;
        gblMFOrder["effectiveDate"] = effectiveDate;

        preShowfrmIBMFComplete();
        frmIBMFComplete.ImageStep.setVisibility(true);
        frmIBMFComplete.HboxContents.setVisibility(true);
        frmIBMFComplete.HboxFailedContents.setVisibility(false);
        frmIBMFComplete.show();
      }
    } else if (resulttable["opstatus"] == "-1"){
      preShowfrmIBMFComplete();
      frmIBMFComplete.ImageStep.setVisibility(false);
      frmIBMFComplete.HboxContents.setVisibility(false);
      frmIBMFComplete.HboxFailedContents.setVisibility(true);
      frmIBMFComplete.show();            
    } else if (resulttable["opstatus"] == 1001) {
      preShowfrmIBMFComplete();
      dismissLoadingScreenPopup();
      frmIBMFComplete.ImageStep.setVisibility(false);
      frmIBMFComplete.HboxContents.setVisibility(false);
      frmIBMFComplete.HboxFailedContents.setVisibility(true);
      frmIBMFComplete.show();
      //alert("Not Allow to send order");
      //frmIBMutualFundsSummary.show();
    } else if (resulttable["opstatus"] == 8005) {
      dismissLoadingScreenPopup();
      if (resulttable["errCode"] == "VrfyOTPErr00001") {
        gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
        frmIBMFConfirm.hbxOTPincurrect.setVisibility(true);
        frmIBMFConfirm.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");//kony.i18n.getLocalizedString("invalidOTP"); //
        frmIBMFConfirm.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
        frmIBMFConfirm.lblOTPinCurr.setVisibility(true);
        frmIBMFConfirm.lblPlsReEnter.setVisibility(true);
        frmIBMFConfirm.hbox476047582127699.setVisibility(false);
        frmIBMFConfirm.hbxOTPsnt.setVisibility(false);
        frmIBMFConfirm.txtBxOTP.text="";
        frmIBMFConfirm.tbxToken.text="";
        //alert("" + kony.i18n.getLocalizedString("invalidOTP"));
        return false;
      } else if (resulttable["errCode"] == "VrfyOTPErr00002") {
        handleOTPLockedIB(resulttable);
        //startRcCrmUpdateProfilBPIB("04");
        return false;
      } else if (resulttable["errCode"] == "VrfyOTPErr00005") {
        alert("" + kony.i18n.getLocalizedString("KeyTokenSerialNumError"));
        return false;
      } else if (resulttable["errCode"] == "VrfyOTPErr00006") {
        gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
        alert("" + resulttable["errMsg"]);
        return false;
      } else if (resulttable["errCode"] == "GenOTPRtyErr00001") {
        dismissLoadingScreenPopup();
        showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
        return false;
      } else if (resulttable["errCode"] == "VrfyOTPErr00004") {
        alert("" + resulttable["errMsg"]);
        return false;
      } else{
        alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
        return false;
      }
    } else {
      dismissLoadingScreenPopup();
      preShowfrmIBMFComplete();
      frmIBMFComplete.ImageStep.setVisibility(false);
      frmIBMFComplete.HboxContents.setVisibility(false);
      frmIBMFComplete.HboxFailedContents.setVisibility(true);
      frmIBMFComplete.show();
    } 
  } 
  else {
    if (status == 300) {
      dismissLoadingScreenPopup();
      alert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"));
      preShowfrmIBMFComplete();
      frmIBMFComplete.ImageStep.setVisibility(false);
      frmIBMFComplete.HboxContents.setVisibility(false);
      frmIBMFComplete.HboxFailedContents.setVisibility(true);
      frmIBMFComplete.show();
    }else{
      preShowfrmIBMFComplete();
      frmIBMFComplete.ImageStep.setVisibility(false);
      frmIBMFComplete.HboxContents.setVisibility(false);
      frmIBMFComplete.HboxFailedContents.setVisibility(true);
      frmIBMFComplete.show();
    }
  }
}


function MBconfirmMFOrder(tranPassword) {

  if (tranPassword == null || tranPassword == '') {
    setTransPwdFailedError(kony.i18n.getLocalizedString("emptyMBTransPwd"));
    return false;
  }
  else
  {
    //showLoadingScreen();
    
    popupTractPwd.show();
    popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("keyTransPwdMsg");
    popupTractPwd.lblPopupTract7.skin = lblPopupLabelTxt;
    popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = tbxPopupBlue;
    popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = tbxPopupBlue;
    popupTractPwd.hbxPopupTranscPwd.skin = tbxPopupBlue;

    MBconfirmMFOrderCompositeJavaService(tranPassword);
  }
}

function MBconfirmMFOrderCompositeJavaService(pwd){
  var inputParam = {};

  showLoadingScreen();	
  inputParam["channel"] = "rc";

  inputParam["verifyPwdMB_appID"] = appConfig.appId;
  inputParam["verifyPwdMB_channel"] = "MB";
  inputParam["password"] = pwd;

  inputParam["verifyPwdMB_appID"] = appConfig.appId;
  inputParam["billerMethod"] = "0";

  inputParam["isPayNow"] = true;
  inputParam["compCode"] = "";

  inputParam["orderType"] = gblMFOrder["orderType"];
  inputParam["fundShortName"] = gblMFOrder["fundShortName"];
  inputParam["mforder_fundHouseCode"] = gblMFOrder["fundHouseCode"];
  if(inputParam["orderType"] == MF_ORD_TYPE_REDEEM){
    if(gblMFOrder["redeemUnit"] == "A"){
      inputParam["redeemType"] = "U";
    } else { 
      inputParam["redeemType"] = gblMFOrder["redeemUnit"];
    }
    inputParam["redeemAmount"] = gblMFOrder["amount"];
    inputParam["unitHolderNo"] = gblMFOrder["unitHolderNo"];
    inputParam["fundCode"] = gblMFOrder["fundCode"];
    inputParam["mforder_invoiceNum"] = gblFundRulesData["fundCode"];

  }else if(inputParam["orderType"] == MF_ORD_TYPE_PURCHASE){

    var tranCode;
    var fromAccTypeTrn = gblMFOrder["acctType"];
    if (fromAccTypeTrn == "DDA") {
      tranCode = "8810";
    } else {
      tranCode = "8820";
    }
    var fromAcctNo = gblMFOrder["fromAccountNo"].split("-").join("");
    var fromAccType;
    if (fromAcctNo.length == 14) {
      fromAccType = "SDA";
    } else {
      fourthDigit = fromAcctNo.charAt(3);
      if (fourthDigit == "2" || fourthDigit == "7" || fourthDigit == "9") {
        fromAccType = "SDA";
        fromAcctNo = "0000" + fromAcctNo;
      } else {
        fromAccType = "DDA";
      }
    }

    //
    inputParam["orderType"] = gblMFOrder["orderType"];

    var billerMethod = "0";
    var transferFee = "0.00";
    var transferAmount = gblMFOrder["amount"];
    if(gbltdFlag == "CCA"){
      inputParam["mforder_cardRefId"] = gblMFOrder["fromAccountNo"].split("-").join("");
      inputParam["mforder_fromAcctNo"] = gblMFOrder["fromAccountNo"].split("-").join("");
    }else{
      inputParam["mforder_fromAcctNo"] = gblMFOrder["fromAccountNo"].split("-").join("");
    }
    inputParam["compCode"] = "";//gblMFOrder["fundHouseCode"];
    inputParam["mforder_fromAcctNo"] = gblMFOrder["fromAccountNo"].split("-").join("");
    inputParam["mforder_fromAcctTypeValue"] = fromAccType
    inputParam["mforder_toAcctNo"] = gblMFOrder["MFtoAccountKey"];
    inputParam["mforder_toAcctTypeValue"] = gblMFOrder["acctType"];
    inputParam["mforder_toFIIdent"] = "";
    inputParam["mforder_transferAmt"] = gblMFOrder["amount"];
    inputParam["mforder_billPmtFee"] = "0"
    inputParam["mforder_pmtRefIdent"] = gblMFOrder["unitHolderNo"];
    inputParam["mforder_invoiceNum"] = gblFundRulesData["fundCode"];
    inputParam["mforder_PostedDt"] = getTodaysDate();
    inputParam["mforder_postedTime"] = ""
    inputParam["mforder_EPAYCode"] = "";
    inputParam["mforder_CompCode"] = gblMFOrder["MFBillerCompcode"] ;
    inputParam["mforder_frmFiident"] = "";
    inputParam["mforder_tranCode"] = tranCode;
    inputParam["mforder_channelName"] = "MB"
    inputParam["mforder_ClientDt"] = ""
    inputParam["mforder_rqUUId"] = ""

    inputParam["serviceID"] = "MFOrderCompositeJavaService";

  }
  inputParam["crmProfileMod_appID"] = appConfig.appId;
  inputParam["crmProfileMod_channel"] = "MB";


  var activityFlexValues5 = "";
  inputParam["serviceID"] = "MFOrderCompositeService";


  invokeServiceSecureAsync("MFOrderCompositeJavaService", inputParam, MBconfirmMFOrderCompositeJavaServiceCallBack);
}


function MBconfirmMFOrderCompositeJavaServiceCallBack(status,resulttable){

  if (status == 400) {
    if (resulttable["opstatus"] == 0) {
      dismissLoadingScreen();
      popupTractPwd.dismiss();
      onClickCancelAccessPin();

      if (GBL_MF_TEMENOS_ENABLE_FLAG != "ON" && gblMFOrder["fundHouseCode"] == "TMBAM" && resulttable["errMsg"] != "OK"){
        frmMFFailedTransactionMB.show();				
      }else{				
        if (gblMFOrder["fundHouseCode"] == "TMBAM"){
          var paymentMethod = "";
          var payment_type = "";
          var payin_bank_code = "";
          var payin_acc_no = "";         		

          if (gblMFOrder["orderType"] == MF_ORD_TYPE_REDEEM){
            if(resulttable["payment_type"] == null || resulttable["payment_type"] == undefined || resulttable["payment_type"] == ""){
              gblMFOrder["paymentType"] = "";
            }else{
              gblMFOrder["paymentType"] = resulttable["payment_type"];       			
            }
            if(resulttable["payin_bank_code"] == null || resulttable["payin_bank_code"] == undefined || resulttable["payin_bank_code"] == ""){
              payin_bank_code = "";
            }else{
              payin_bank_code = resulttable["payin_bank_code"] + " ";
            }
            if(resulttable["payin_acc_no"] == null || resulttable["payin_acc_no"] == undefined || resulttable["payin_acc_no"] == ""){
              payin_acc_no = "";
            }else{
              payin_acc_no = resulttable["payin_acc_no"];
            }	          		
            gblMFOrder["paymentDetail"] = payin_bank_code + payin_acc_no;

          }else{
            gblMFOrder["paymentType"] = "";
            gblMFOrder["paymentDetail"] = "";
          } 
        }

        gblMFOrder["refID"] = resulttable["transactionRef"];
        gblMFOrder["orderTime"] = resulttable["transactionDate"].substring(10, resulttable["transactionDate"].lenght);
        temptransDate = resulttable["transactionDate"]; //YYYY-MM-DD
        tempYear = temptransDate.substring(0,4);
        tempMonth = temptransDate.substring(5,7);
        tempDate = temptransDate.substring(8,10);
        transDate = tempMonth + "/" + tempDate + "/" + tempYear;
        gblMFOrder["orderDate"] = transDate; //formatDateMF(transDate);

        tempeffectiveDate = resulttable["effectiveDate"]; //YYYY-MM-DD
        tempYear = tempeffectiveDate.substring(0,4);
        tempMonth = tempeffectiveDate.substring(5,7);
        tempDate = tempeffectiveDate.substring(8,10);
        effectiveDate = tempMonth + "/" + tempDate + "/" + tempYear;
        gblMFOrder["effectiveDate"] = effectiveDate; //formatDateMF(effectiveDate);

        frmMFCompleteMB.show();
        showLoadingScreenGif();
        dismissLoadingScreenGif();
      }
    } else if (resulttable["opstatus"] == "-1") {
      dismissLoadingScreen();
      popupTractPwd.dismiss();
      onClickCancelAccessPin();
      frmMFFailedTransactionMB.show();
    } else if (resulttable["opstatus"] == 1001) {
      dismissLoadingScreen();
      popupTractPwd.dismiss();
      onClickCancelAccessPin();
      frmMFFailedTransactionMB.show();
    } else if (resulttable["errCode"] == "VrfyAcPWDErr00001" || resulttable["errCode"] == "VrfyAcPWDErr00002") {
      dismissLoadingScreen();
      popupEnterAccessPin.lblWrongPin.setVisibility(true);
      resetAccessPinImg(resulttable["badLoginCount"]);
    }else if (resulttable["errCode"] == "VrfyAcPWDErr00003") {
      dismissLoadingScreen();
      onClickCancelAccessPin();
      gotoUVPINLockedScreenPopUp();
    } else if (resulttable["errCode"] == "VrfyTxPWDErr00001" || resulttable["errCode"] == "VrfyTxPWDErr00002") {
      dismissLoadingScreen();
      setTransPwdFailedError(kony.i18n.getLocalizedString("invalidTxnPwd"));
    }else if (resulttable["errCode"] == "VrfyTxPWDErr00003") {
      dismissLoadingScreen();
      showTranPwdLockedPopup();
    }else if (resulttable["opstatus"] == 8005) {
      popupTractPwd.dismiss();
      onClickCancelAccessPin();
      dismissLoadingScreen();
      if (resulttable["errCode"] == "VrfyOTPErr00001") {
        gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];

        alert("" + kony.i18n.getLocalizedString("invalidOTP"));
        //return false;
      } else if (resulttable["errCode"] == "VrfyOTPErr00002") {

        alert("" + kony.i18n.getLocalizedString("ECVrfyOTPErr"));
        //startRcCrmUpdateProfilBPIB("04");
        //return false;
      } else if (resulttable["errCode"] == "VrfyOTPErr00005") {

        alert("" + kony.i18n.getLocalizedString("KeyTokenSerialNumError"));
        //return false;
      } else if (resulttable["errCode"] == "VrfyOTPErr00006") {

        gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
        alert("" + resulttable["errMessage"]);
        //return false;
      }else if (resulttable["errCode"] == "VrfyOTPErr00004") {

        alert("" + resulttable["errMsg"]);
        //return false;
      }else{

        alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
        //return false;
      }
      frmMFFailedTransactionMB.show();
    } else {
      popupTractPwd.dismiss();
      onClickCancelAccessPin();
      dismissLoadingScreen();

      if (resulttable["opstatus"] == 1 && resulttable["errCode"] == "ERRDUPTR0001") {
        alert(kony.i18n.getLocalizedString("keyErrDuplicatemt"));
      }
      var errorMsgTop = resulttable["errMsg"];
      if(resulttable["errMsg"] == null || resulttable["errMsg"] == undefined || resulttable["errMsg"] == ""){
        errorMsgTop = kony.i18n.getLocalizedString("ECGenOTPRtyErr00001");
        alert(errorMsgTop);
      }

      if(resulttable["isServiceFailed"] == "true"){
        gblMyBillerTopUpBB = 0;
        alert("Service Failed");
      }
      frmMFFailedTransactionMB.show();
    }
  } else {
    if (status == 300) {
      popupTractPwd.dismiss();
      onClickCancelAccessPin();
      dismissLoadingScreen();
      alert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"));
      frmMFFailedTransactionMB.show();
    }else{
      dismissLoadingScreen();
      popupTractPwd.dismiss();
      onClickCancelAccessPin();
      frmMFFailedTransactionMB.show();
    }
  }
}
function frmIBMFConfirmOnClickSwitchToMobileOTP() {
  frmIBMFConfirm.hbxToken.setVisibility(false);
  frmIBMFConfirm.hbxOTPEntry.setVisibility(true);
  frmIBMFConfirm.txtBxOTP.setFocus(true);
  frmIBMFConfirm.lblkeyOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
  frmIBMFConfirm.lblBankRef.text = kony.i18n.getLocalizedString("keyIBbankrefno");

  frmIBMFConfirm.lblOTPinCurr.text = kony.i18n.getLocalizedString("keyIBbankrefno");
  gblTokenSwitchFlag = false;
  gblSwitchToken = true;
  orderMutualFundOTPRequestService();
}