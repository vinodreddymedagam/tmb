function init_eYCGetYoutIdReady(){
  	frmeKYCGetYourIdReady.btnBack.onClick = navigateToeKYCIntroduction;
  frmeKYCGetYourIdReady.onDeviceBack = disableBackButton;
//  //#ifdef android
  frmeKYCGetYourIdReady.flxPassport.onClick = scanPassport;
  frmeKYCGetYourIdReady.flxPassportNoCID.onClick = NavCIDFlow; 
//  //#endif
//  	//#ifdef iphone
//  	frmeKYCGetYourIdReady.imgYourID.onTouchEnd = null;
// 	//#endif
  frmeKYCGetYourIdReady.postShow = postShowfrmeKYCGetYourIdReady;
}

function NavCIDFlow(){
  gblEKYCFlowCIDFlag = false;
  frmMBeKYCIDInfo.destroy();
  chkRunTimePermissionsForeKYC_CID();
}

function scanCitizenId(){
  kony.print("This is invokePassportScanFFI function");
  //Scan Passport Start
  eKYCData = {};
  eKYCData.scanType = "CI";
  eKYCData.scanData = null;
//   showLoadingScreen();
  scanEKYC.scanPassport(scanCIDFFICallBack,false,false,false,30,60,false);
}

function scanCIDFFICallBack(data) {
    //    alert(data);
  try{
//     dismissLoadingScreen();
    showLoadingScreen();
   	if(data !== null){
      eKYCData.scanData = JSON.parse(data);
      //alert("scandata:->" + data);
      kony.print("PASSPORTIMAGE : "+eKYCData.scanData.PASSPORTIMAGE);
      kony.print("NFCIMAGE : "+eKYCData.scanData.NFCIMAGE);
      eKYCData.NFCImage = eKYCData.scanData.NFCIMAGE;
      //frmTest.img1.base64 = eKYCData.scanData.PASSPORTIMAGE;
      if(undefined !== eKYCData.scanData.NFCIMAGE && null !== eKYCData.scanData.NFCIMAGE){
        openCitizenID();
      }else{
        showAlert("NFCIMAGE is not there", getLocalizedString("info"));
      }
      dismissLoadingScreen();
    }
    else{
		showAlert(getLocalizedString("ECGenericError"), getLocalizedString("info"));
      dismissLoadingScreen();
    }   
  }
  catch(e){
    dismissLoadingScreen();
    kony.print("Expection in scanningFFICallback"+e);
  }
 }

function openCitizenID(){
//   chkRunTimePermissionsForeKYC_CID();
  registerForTimeOut();
  frmMBeKYCIDInfo.flxBody2.setVisibility(true);
  frmMBeKYCIDInfo.show();
}

function scanPassport(){
  var nfcFlag = isNFCEnabled();
  if (null !== nfcFlag && false !== nfcFlag) {
    registerForTimeOut();
    frmeKYCGetYourIdReady.flxBody.setVisibility(false);
    frmeKYCGetYourIdReady.flxPassportExpired.setVisibility(false);
    frmeKYCGetYourIdReady.flxHoldYourPassport.setVisibility(true);
    frmeKYCGetYourIdReady.flxSelectPrefferedDocument.setVisibility(false);
    frmeKYCGetYourIdReady.lblTitle.text = getLocalizedString("eKYC_TitleGetPassportReady");
    frmeKYCGetYourIdReady.btnopenCamera.onClick = chkRunTimePermissionsForeKYC;
    frmeKYCGetYourIdReady.btnBack.onClick = btnBackeKYCSelectDoc;
  }else{
    navigateToGetYourId(); 
      checkNFCSupprt();  
  }
//   frmMBeKYCCitezenIDInfo.txtLaserCode.text = "";
//   frmMBeKYCCitezenIDInfo.calExpireDate.removeDataAt();
//   frmMBeKYCCitezenIDInfo.calIssueDate.removeDataAt();
  TMBUtil.DestroyForm(frmMBeKYCCitezenIDInfo);
}

function btnBackeKYCSelectDoc(){
  if(gblUpdateIALFlow){
    if(gblIALFlow == "frmEKYCIdpSetting"){
      lunchSettingIDPPage();
    }else{
      loadEKYCIDPLandingPage();
    }
  }else{
    frmeKYCGetYourIdReady.lblTitle.text = getLocalizedString("eKYC_titleSelectDocument");
    frmeKYCGetYourIdReady.flxBody.setVisibility(false);
    frmeKYCGetYourIdReady.flxPassportExpired.setVisibility(false);
    frmeKYCGetYourIdReady.flxHoldYourPassport.setVisibility(false);
    frmeKYCGetYourIdReady.flxSelectPrefferedDocument.setVisibility(true);
    frmeKYCGetYourIdReady.btnBack.onClick = navigateToeKYCIntroduction;
    gblIALFlow = "";
  }
}

function postShowfrmeKYCGetYourIdReady(){
  animateIdAndSelfieImages(frmeKYCGetYourIdReady.flxArrow,45);
  animateIdAndSelfieImages(frmeKYCGetYourIdReady.flxErrorMsgArrow, 45);
  linki18Keys();
  frmeKYCGetYourIdReady.lblScanCitizenID.onTouchStart = chkRunTimePermissionsForeKYC_CID;
}

function linki18Keys(){
  frmeKYCGetYourIdReady.lblInfoTitle.text = getLocalizedString("eKYC_msgCheckInfoPP");
  frmeKYCGetYourIdReady.lblPassport.text = getLocalizedString("Passport");
  frmeKYCGetYourIdReady.lblThaiName.text = getLocalizedString("ThaiName");
  frmeKYCGetYourIdReady.lblThaiSurname.text = getLocalizedString("ThaiSurname");
  frmeKYCGetYourIdReady.lblName.text = getLocalizedString("EngName");
  frmeKYCGetYourIdReady.lblLastName.text = getLocalizedString("EngSurname");
  frmeKYCGetYourIdReady.lblDOB.text = getLocalizedString("DateofBirth");
  frmeKYCGetYourIdReady.lblDOI.text = getLocalizedString("IssueDate");
  frmeKYCGetYourIdReady.lblDOE.text = getLocalizedString("ExpiredDate");
  frmeKYCGetYourIdReady.lblCitizenIDNo.text = getLocalizedString("CitiZenIDno");
  frmeKYCGetYourIdReady.lblPassportExpired.text = getLocalizedString("eKYC_msgErrPassportExpiredHeader");
  frmeKYCGetYourIdReady.lblOR.text = getLocalizedString("Or");
  frmeKYCGetYourIdReady.lblMessage.text = getLocalizedString("eKYC_msgHoldYourPassport");
  frmeKYCGetYourIdReady.lblScanCitizenID.text = getLocalizedString("eKYC_lkScanCID");
  frmeKYCGetYourIdReady.btnReScan.text = getLocalizedString("Rescan");
  frmeKYCGetYourIdReady.btnCorrect.text = getLocalizedString("Correct");
  frmeKYCGetYourIdReady.lblScanYourPassport.text = getLocalizedString("eKYC_msgScanPassport");
  frmeKYCGetYourIdReady.lblMessage.text = getLocalizedString("eKYC_msgHoldYourPassport");
  frmeKYCGetYourIdReady.btnopenCamera.text = getLocalizedString("eKYC_btnOpenCamera");
  frmeKYCGetYourIdReady.lblScanIDCitizen.text = getLocalizedString("eKYC_lkScanCIDOpt");
  frmeKYCGetYourIdReady.lblScanPassport.text = getLocalizedString("eKYC_lkScanPassport");
}

function navigateToeKYCIntroduction(){
  frmMBeKYCIntroduction.show();
  gblIALFlow = "";
}

// function showAlertNFCNotEnable(keyMsg, KeyTitle) {
// 	var okk = kony.i18n.getLocalizedString("AcceptNCB");
//     var Noo = kony.i18n.getLocalizedString("NotAcceptNCB");
// 	//Defining basicConf parameter for alert
// 	var basicConf = {
// 		message: keyMsg,
// 		alertType: constants.ALERT_TYPE_INFO,
// 		alertTitle: KeyTitle,
// 		yesLabel: okk,
// 		noLabel: Noo,
// 		alertHandler: NavigateNFCSettings
// 	};
// 	//Defining pspConf parameter for alert
// 	var pspConf = {};
// 	//Alert definition
// 	var infoAlert = kony.ui.Alert(basicConf, pspConf);

// 	function NavigateNFCSettings(response) {
//       if(response == true){
//       openSettings.settings();
//         }else{
          
//         }
//     }
// 	return;
// }

function checkNFCSupprt(){
  var nfcFlag = isNFCEnabled();
  
  if(null !== nfcFlag){
   //nfcFlag = nfcFlag+"";
    if(true === nfcFlag){
    	//alert("NFC is ON");
      return true;
  	}else{
//       alert("NFC is OFF, Please on the NFC in settings");
      var msg = kony.i18n.getLocalizedString("eKYC_msgAllowNFC");
      var title = kony.i18n.getLocalizedString("info");
      var okk = kony.i18n.getLocalizedString("notificationAllow");
      var Noo = kony.i18n.getLocalizedString("btnDeny");
//       showAlertNFCNotEnable(msg, title);
      showAlertWithYesNoHandler(msg,title,okk,Noo,NavigateNFCSettings, doNothing);
    }
  }else{
    //alert("NFC is not supported in this device");
    return false;
  }
}

function NavigateNFCSettings() {
   openSettings.settings();
}


function isNFCEnabled(){
	var nfcStatus = null;
   if(!isAndroid()){
     return nfcStatus;
   }
   var KonyMain = java.import("com.konylabs.android.KonyMain");
   var context = KonyMain.getActivityContext();

   var adapterClass = java.import("android.nfc.NfcAdapter");
   var adapter = adapterClass.getDefaultAdapter(context);

   if (adapter !== null) {
     kony.print("Is Enabled : "+adapter.isEnabled());
     nfcStatus = adapter.isEnabled();
   }

  return nfcStatus;
}

function navigateToGetYourId(){
  resetGetYourIdReadyDetails();
  frmeKYCGetYourIdReady.show();
}

/**
this function will check the camera permission, 
if given then call the Scan passport FFI
*/
function chkRunTimePermissionsForeKYC(){
   var options = {};
   var result = kony.application.checkPermission(kony.os.RESOURCE_CAMERA, options);
  var messageErr = "";
   kony.print("chkRunTimePermissionsForeKYC result ###########  "+result.status);
   if (result.status == kony.application.PERMISSION_DENIED){
      kony.print("chkRunTimePermissionsForeKYC Permission denied  ");
	  if (result.canRequestPermission){
         kony.application.requestPermission(kony.os.RESOURCE_CAMERA, reqCameraPermissionEKYCCallback);
       }else{
         kony.print("IN FINAL CALLBACK PERMISSION IS UNAVAIALABLE");
         messageErr = "App cannot proceed further unless the required permisssions are granted!"; //Need to chk with customer on msg text
         alert(messageErr);
       }
   } else if (result.status == kony.application.PERMISSION_GRANTED) {
      kony.print("chkRunTimePermissionsForeKYC Permission granted  ");
      kony.print("Permission allowed.");
      invokePassportScanFFI();    
   } else if (result.status == kony.application.PERMISSION_RESTRICTED) {
      kony.print("chkRunTimePermissionsForeKYC Permission restricted ");
      kony.print("IN FINAL CALLBACK PERMISSION IS UNAVAIALABLE");
      messageErr = "App cannot proceed further unless the required permisssions are granted!"; //Need to chk with customer on msg text
      alert(messageErr);
   }
  
  function reqCameraPermissionEKYCCallback(response) {
        kony.print("reqCameraPermissionEKYCCallback "+result.status);
		if (response.status == kony.application.PERMISSION_GRANTED) {
			kony.print("reqCameraPermissionEKYCCallback-PERMISSION_GRANTED: " + JSON.stringify(response));
             kony.print("Permission allowed.");
             kony.print("IN FINAL CALLBACK PERMISSION IS AVAIALABLE");
        	invokePassportScanFFI();
		} else if (response.status == kony.application.PERMISSION_DENIED) {
			kony.print("reqCameraPermissionEKYCCallback-PERMISSION_DENIED: " + JSON.stringify(response));
			kony.print("IN FINAL CALLBACK PERMISSION IS UNAVAIALABLE");
            var messageErr = "App cannot proceed further unless the required permisssions are granted!"; //Need to chk with customer on msg text
            alert(messageErr);
		}
	}
}

function chkRunTimePermissionsForeKYC_CID(){
   var options = {};
   var result = kony.application.checkPermission(kony.os.RESOURCE_CAMERA, options);
  var messageErr = "";
   kony.print("chkRunTimePermissionsForeKYC result ###########  "+result.status);
   if (result.status == kony.application.PERMISSION_DENIED){
      kony.print("chkRunTimePermissionsForeKYC Permission denied  ");
	  if (result.canRequestPermission){
         kony.application.requestPermission(kony.os.RESOURCE_CAMERA, reqCameraPermissionEKYCCallbackCID);
       }else{
         kony.print("IN FINAL CALLBACK PERMISSION IS UNAVAIALABLE");
         messageErr = "App cannot proceed further unless the required permisssions are granted!"; //Need to chk with customer on msg text
         alert(messageErr);
       }
   } else if (result.status == kony.application.PERMISSION_GRANTED) {
      kony.print("chkRunTimePermissionsForeKYC Permission granted  ");
      kony.print("Permission allowed.");
      scanCitizenId();    
   } else if (result.status == kony.application.PERMISSION_RESTRICTED) {
      kony.print("chkRunTimePermissionsForeKYC Permission restricted ");
      kony.print("IN FINAL CALLBACK PERMISSION IS UNAVAIALABLE");
      messageErr = "App cannot proceed further unless the required permisssions are granted!"; //Need to chk with customer on msg text
      alert(messageErr);
   }
  
  function reqCameraPermissionEKYCCallbackCID(response) {
        kony.print("reqCameraPermissionEKYCCallback "+result.status);
		if (response.status == kony.application.PERMISSION_GRANTED) {
			kony.print("reqCameraPermissionEKYCCallback-PERMISSION_GRANTED: " + JSON.stringify(response));
             kony.print("Permission allowed.");
             kony.print("IN FINAL CALLBACK PERMISSION IS AVAIALABLE");
        	scanCitizenId();
		} else if (response.status == kony.application.PERMISSION_DENIED) {
			kony.print("reqCameraPermissionEKYCCallback-PERMISSION_DENIED: " + JSON.stringify(response));
			kony.print("IN FINAL CALLBACK PERMISSION IS UNAVAIALABLE");
            var messageErr = "App cannot proceed further unless the required permisssions are granted!"; //Need to chk with customer on msg text
            alert(messageErr);
		}
	}
}




function invokePassportScanFFI(){
  kony.print("This is invokePassportScanFFI function");
  //Scan Passport Start
  eKYCData = {};
  eKYCData.scanType = "PP";
  eKYCData.scanData = null;
  scanEKYC.scanPassport(scanPassportFFICallBack,true,true,false,30,60,false);
}

function scanPassportFFICallBack(data) {
    //    alert(data);
  try{
    showLoadingScreen();
   	if(data !== null){
      eKYCData.scanData = JSON.parse(data);
      kony.print("scandata:->" + data);
      if(gblIALFlow == "frmEKYCIdpSetting" || gblIALFlow == "frmEKYCIdpDetailsList"){
        var currentCID = "";
        if (gblCustomerIDType == "CI") {
          currentCID = gblCustomerIDValue;
        }else if(gblCustomerIDType == "PP"){
          currentCID = gblCustomerPassport;
        }
        if(eKYCData.scanData.DG1_PersonalNumber == currentCID){
          callServiceeKYC_validateDOEDOBService();
        }else{
          errorPassport();
          frmeKYCGetYourIdReady.lbErrorlMessage.text = getLocalizedString("eKYC_msgCIDnotMatch");
        }
      }else{
          callServiceeKYC_validateDOEDOBService();
        }
     // populatePassPortScanData();
    }
    else{
		  showAlert(getLocalizedString("ECGenericError"), getLocalizedString("info"));
      dismissLoadingScreen();
    }   
  }
  catch(e){
    dismissLoadingScreen();
    kony.print("Expection in scanningFFICallback"+e);
  }
 }

function callServiceeKYC_validateDOEDOBService(){
  showLoadingScreen();
  var inputParam = {};
  var ThaiFullNameeKYCData = eKYCData.scanData.DG13_Thainame;
  var splitThaiFullNameeKYCData = replaceAll(ThaiFullNameeKYCData, " ", "~");
  var ThaiSalutation = splitThaiFullNameeKYCData.split("~")[0];
  var ThaiFirstName =  splitThaiFullNameeKYCData.split("~")[1];
  var ThaiLastName =  splitThaiFullNameeKYCData.split("~")[2];
  inputParam.nationality = eKYCData.scanData.Nationality;
  inputParam.dateofBirth =eKYCData.scanData.DOB;
  inputParam.passportDOE =  eKYCData.scanData.ExpiredDate;//"18-Jun-2019";
  inputParam.firstName = ThaiFirstName;
  inputParam.lastName =  ThaiLastName;
  inputParam.thaiSalutation =  ThaiSalutation;
  inputParam.firstName_EN = eKYCData.scanData.GivenName;
  inputParam.lastName_EN =  eKYCData.scanData.SurName;
   var citizenId = eKYCData.scanData.DG1_PersonalNumber.length == 13 ? eKYCData.scanData.DG1_PersonalNumber : "";
          if(citizenId !== ""){
            citizenId = citizenId.slice(0,1)+"-"+citizenId.slice(1,5)+"-"+citizenId.slice(5,10)+"-"+citizenId.slice(10,12)+"-"+citizenId.slice(12,13);
          }
  var formattedCitienId = citizenId ? citizenId : eKYCData.scanData.DG1_PersonalNumber;
  inputParam.citizenId = replaceAll(formattedCitienId,'-','');
  inputParam.passportNo = eKYCData.scanData.PassportNo;
  inputParam.passportDOI = eKYCData.scanData.DG12_DateOfIssue;
  inputParam.placeofBirth =eKYCData.scanData.DG11_PlaceOfBirth;
  inputParam.idType = "PP";
  inputParam.gender = eKYCData.scanData.Gender;
  inputParam.DG1_EncodedData = eKYCData.scanData.DG1_EncodedData;
  inputParam.DG2_EncodedData = eKYCData.scanData.DG2_EncodedData;
  inputParam.sodData_EncodedData = eKYCData.scanData.sodData_EncodedData;
 
  invokeServiceSecureAsync("validateDOEDOBService", inputParam, callBackService_validateDOEDOBService);
}

function callBackService_validateDOEDOBService(status, result){
   if (status == 400) {
     var errorcode = result["errCode"];
      if (result["opstatus"] === 0 ||result["opstatus"] == "0") {
		kony.print(""+JSON.stringify(result));
		populatePassPortScanData();
      }else {
        errorPassport();
        if(errorcode == "eKYCDOE01"){
          frmeKYCGetYourIdReady.lblPassportExpired.text = getLocalizedString("eKYC_msgErrPassportExpiredHeader");
          frmeKYCGetYourIdReady.lbErrorlMessage.text = getLocalizedString("eKYC_msgPassportExpiredDesc");
        }else if(errorcode == "eKYCDOB01"){
          frmeKYCGetYourIdReady.lblPassportExpired.text = getLocalizedString("eKYC_msgErrAgeLesstan18Header");
          frmeKYCGetYourIdReady.lbErrorlMessage.text = getLocalizedString("eKYC_msgAgeLessthan18Y");
          frmeKYCGetYourIdReady.btnTryNewPassp.onClick = btnTnCBackeKYC;
          frmeKYCGetYourIdReady.btnTryNewPassp.text = getLocalizedString("keyClose");
          frmeKYCGetYourIdReady.btnTryNewPassp.centerY = "50%";
          frmeKYCGetYourIdReady.lblScanCitizenID.setVisibility(false);
        }else if(errorcode == "eKYCNATIONALITY01"){
          alert("Allowed Thai Passport only");
        }else if(errorcode == "ErrCannotReadPassport"){
          errorPassport();
        }
//       showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
      return false;
    }
   }else {
      dismissLoadingScreen();
      showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
      return false;
    }   
}

function errorPassport(){
      frmeKYCGetYourIdReady.btnTryNewPassp.onClick = chkRunTimePermissionsForeKYC;
      frmeKYCGetYourIdReady.btnTryNewPassp.text = getLocalizedString("eKYC_btnTryNewPassport");
      frmeKYCGetYourIdReady.lblPassportExpired.text = getLocalizedString("eKYC_lbErrorHeader");
      frmeKYCGetYourIdReady.lbErrorlMessage.text = getLocalizedString("eKYC_msgCannotReadPassport");
      frmeKYCGetYourIdReady.flxPassportExpired.setVisibility(true);
      frmeKYCGetYourIdReady.lblScanCitizenID.setVisibility(true);
      frmeKYCGetYourIdReady.btnTryNewPassp.centerY = "25%";
      if(gblIALFlow == "frmEKYCIdpSetting" || gblIALFlow == "frmEKYCIdpDetailsList"){
        frmeKYCGetYourIdReady.lblScanCitizenID.setVisibility(false);
        frmeKYCGetYourIdReady.btnTryNewPassp.centerY = "50%";
      }
      frmeKYCGetYourIdReady.flxSelectPrefferedDocument.setVisibility(false);
      frmeKYCGetYourIdReady.flxHoldYourPassport.setVisibility(false);
      frmeKYCGetYourIdReady.flxBody.setVisibility(false);
      dismissLoadingScreen();
}

function populatePassPortScanData(){
  try{
      if(eKYCData.scanType === "PP"){
          eKYCData.NFCImage = eKYCData.scanData.NFCIMAGE;
        //alert("Passport No : "+eKYCData.scanData.PassportNo);
        frmeKYCGetYourIdReady.lblPassportInfo.text = eKYCData.scanData.PassportNo;
        
        //frmeKYCGetYourIdReady.imgOpenAccountConfirm.base64 = eKYCData.scanData.NFCIMAGE;
          frmeKYCGetYourIdReady.lblLastNameInfo.text = eKYCData.scanData.SurName;
          frmeKYCGetYourIdReady.lblNameInfo.text = eKYCData.scanData.GivenName;
          frmeKYCGetYourIdReady.lblDOBInfo.text = eKYCData.scanData.DOB_Display;
          frmeKYCGetYourIdReady.lblDOIInfo.text = eKYCData.scanData.DG12_DateOfIssue_Display;
          frmeKYCGetYourIdReady.lblDOEInfo.text = eKYCData.scanData.ExpiredDate_Display;
          
          var citizenId = eKYCData.scanData.DG1_PersonalNumber.length == 13 ? eKYCData.scanData.DG1_PersonalNumber : "";
          if(citizenId !== ""){
            citizenId = citizenId.slice(0,1)+" "+citizenId.slice(1,5)+" "+citizenId.slice(5,10)+" "+citizenId.slice(10,12)+" "+citizenId.slice(12,13);
          }
          frmeKYCGetYourIdReady.lblCitizenIDNoInfo.text = citizenId ? citizenId : eKYCData.scanData.DG1_PersonalNumber;
          var thaiName = eKYCData.scanData.DG13_Thainame.split(" ");
          frmeKYCGetYourIdReady.lblThaiNameInfo.text = thaiName[1];
          frmeKYCGetYourIdReady.lblThaiSurnameInfo.text = thaiName[2];
         showGetYourIdReadyDetails();
    }
    dismissLoadingScreen();
  } catch(e){
    kony.print("Exception in populateScanData"+e);
    dismissLoadingScreen();
  }
}

function resetGetYourIdReadyDetails(){
  frmeKYCGetYourIdReady.lblTitle.text = getLocalizedString("eKYC_titleSelectDocument");
  frmeKYCGetYourIdReady.flxBody.setVisibility(false);
  frmeKYCGetYourIdReady.flxPassportExpired.setVisibility(false);
  frmeKYCGetYourIdReady.flxHoldYourPassport.setVisibility(false);
  frmeKYCGetYourIdReady.flxSelectPrefferedDocument.setVisibility(true);
}

function showGetYourIdReadyDetails(){
  frmeKYCGetYourIdReady.lblTitle.text = getLocalizedString("eKYC_TitleInfoReadfromPassport");
//   frmeKYCGetYourIdReady.btnBack.onClick = resetGetYourIdReadyDetails;
  frmeKYCGetYourIdReady.btnBack.onClick = scanPassport;
  frmeKYCGetYourIdReady.btnReScan.onClick = rescanGetYourId;
  frmeKYCGetYourIdReady.btnCorrect.onClick = correctGetYourId;
  if(gblIALFlow == "frmEKYCIdpSetting" || gblIALFlow == "frmEKYCIdpDetailsList"){
    frmeKYCGetYourIdReady.btnCorrect.onClick = navToEnterLaserCode;
    frmeKYCGetYourIdReady.btnBack.onClick = populatePassPortScanData;
  }
  frmeKYCGetYourIdReady.flxBody.setVisibility(true);
  frmeKYCGetYourIdReady.flxSelectPrefferedDocument.setVisibility(false);
  frmeKYCGetYourIdReady.flxHoldYourPassport.setVisibility(false);
  frmeKYCGetYourIdReady.flxPassportExpired.setVisibility(false);
  registerForTimeOut();
}

function rescanGetYourId(){
  invokePassportScanFFI();
}

function correctGetYourId(){
  frmMBeKYCCitezenIDInfo.show();
}