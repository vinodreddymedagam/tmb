







var gblNotificationDetailsCache = new Array();
var tempPushArray = new Array();
var gblNotificationHistoryModified = true;
var NotificationHomePageState = false;
var locale = kony.i18n.getCurrentLocale();
var ServiceResult = new Array();
var tempNotfnDetails = null;
var gblPushNotfnDetailsFrom = null;

//GET NOTIFICATION HISTORY
function getNotificationHistoryMB(){
	var inputParams = {};
	showLoadingScreen();
	invokeServiceSecureAsync("notificationhistory", inputParams, getNotificationHistoryMBCB)
}

function getNotificationHistoryMBCB(status, resultset){
	if(status == 400){
		
		if(resultset["status"] == "1"){
			if(resultset["Results"] != null){
				//DO NOTHING
			}
			else{
				showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error");
			}
			frmNotificationHome.lblNoNotfn.isVisible = true;
			frmNotificationHome.lblNoNotfn.text = kony.i18n.getLocalizedString("keyNoNotifications");
			//frmNotificationHome.btnCancel.text = "";
			frmNotificationHome.btnCancel.isVisible=false;
			//frmNotificationHome.btnCancel.skin = "btnBlueDark";
			//frmNotificationHome.btnCancel.focusSkin = "btnBlueDark";
			//frmNotificationHome.btnCancel.onClick =CancelDeleteOnClick; 
			//frmNotificationHome.btnRight.text = "DeleteDisabled";
			frmNotificationHome.btnRight.skin = "btnDeleteActive";
			frmNotificationHome.btnRight.focusSkin = "btnDeleteActive";
			frmNotificationHome.btnRight.setEnabled(false);
			frmNotificationHome.btnRight.isVisible = false;
			frmNotificationHome.segMyRecipient.removeAll();
		}
		else if(resultset["status"] == "0"){
			//
			if(resultset["Results"].length > 0){
				ServiceResult.length = 0;
				for(var i = 0; i < resultset["Results"].length; i++){
					ServiceResult.push(resultset["Results"][i]);
				}
				gblMaxSetImportant = resultset["maxsetimportant"];
				frmNotificationHome.lblNoNotfn.isVisible = false;
				populateNotificationsNormalState();
			}
			else{
				frmNotificationHome.lblNoNotfn.isVisible = true;
				frmNotificationHome.lblNoNotfn.text = kony.i18n.getLocalizedString("keyNoNotifications");
				ServiceResult.length = 0;
				//frmNotificationHome.btnCancel.text = "";
				frmNotificationHome.btnCancel.isVisible=false;
				//frmNotificationHome.btnCancel.skin = "btnBlueDark";
				//frmNotificationHome.btnCancel.focusSkin = "btnBlueDark";
				//frmNotificationHome.btnCancel.onClick =CancelDeleteOnClick; 
				//frmNotificationHome.btnRight.text = "DeleteDisabled";
				frmNotificationHome.btnRight.skin = "btnDeleteActive";
				frmNotificationHome.btnRight.focusSkin = "btnDeleteActive";
				frmNotificationHome.btnRight.setEnabled(false);
				frmNotificationHome.btnRight.isVisible = false;
				frmNotificationHome.segMyRecipient.removeAll();
			}
		}
		else{
			showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error");
			frmNotificationHome.segMyRecipient.removeAll();
		}
		kony.application.dismissLoadingScreen();
	}
}

function populateNotificationsNormalState(){
	showLoadingScreen();
	locale = kony.i18n.getCurrentLocale();
	NotificationHomePageState = false;
	populateNotifications();
	frmNotificationHome.segMyRecipient.selectionBehavior = constants.SEGUI_DEFAULT_BEHAVIOR;
	frmNotificationHome.segMyRecipient.onRowClick = getNotificationDetailsMB;
	frmNotificationHome.btnCancel.isVisible=false;
	if(gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo.name == "iPhone Simulator"){
		frmNotificationHome.btnCancel.isVisible=true;
		frmNotificationHome.btnCancel.skin = "btnBlueDark";
		frmNotificationHome.btnCancel.focusSkin = "btnBlueDark";
		frmNotificationHome.btnCancel.onClick =CancelDeleteOnClick; 
	}
	//frmNotificationHome.btnRight.text = "Delete";
	frmNotificationHome.btnRight.setEnabled(true);
	frmNotificationHome.btnRight.skin = "btnDeleteDB";
	frmNotificationHome.btnRight.focusSkin = "btnDeleteDB";
	frmNotificationHome.btnRight.isVisible = true;
	kony.application.dismissLoadingScreen();
}

function populateNotificationsDeleteState(){
	showLoadingScreen();
	locale = kony.i18n.getCurrentLocale();
	NotificationHomePageState = true;
	populateNotifications();
	frmNotificationHome.segMyRecipient.selectionBehavior = constants.SEGUI_MULTI_SELECT_BEHAVIOR;
	frmNotificationHome.segMyRecipient.onRowClick = SelectForDeleteTracker;
	//frmNotificationHome.btnCancel.text = "Cancel";
	frmNotificationHome.btnCancel.isVisible=true;
	if(gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo.name == "iPhone Simulator"){
		frmNotificationHome.btnCancel.skin = "btnCancel";
		frmNotificationHome.btnCancel.focusSkin = "btnCancel";
	}
	frmNotificationHome.btnCancel.onClick =CancelDeleteOnClick; 
	//frmNotificationHome.btnRight.text = "DeleteNo";
	frmNotificationHome.btnRight.setEnabled(true);
	frmNotificationHome.btnRight.skin = "btnDeleteActive";
	frmNotificationHome.btnRight.focusSkin = "btnDeleteActive";
	frmNotificationHome.btnRight.isVisible = true;
	frmNotificationHome.btnRight.onClick=NotificationDeleteOnClick;
	kony.application.dismissLoadingScreen();
}

function SelectForDeleteTracker(){
	if(frmNotificationHome.segMyRecipient.selectedItems != null){
		//frmNotificationHome.btnRight.text = "DeleteYes";
		frmNotificationHome.btnRight.skin = "btnDeleteDB";
		frmNotificationHome.btnRight.focusSkin = "btnDeleteDB";
	}
	else{
		//frmNotificationHome.btnRight.text = "DeleteNo";
		frmNotificationHome.btnRight.skin = "btnDeleteActive";
		frmNotificationHome.btnRight.focusSkin = "btnDeleteActive";
	}
}

function populateNotifications(){
	gblNotificationDetailsCache.length = 0;
	var NotificationOnDate = new Array(); 
	var localeSup = "EN";
	var skinImp = null;
	var skinRowRead = null;
	if(locale == "th_TH"){
		localeSup = "TH";
	}
	var onclickBTNI = null;
	if(!NotificationHomePageState){
		onclickBTNI = markNotificationImportantMB;
	}
	
	if(ServiceResult.length > 0 ){
		var date = ServiceResult[0]["date"];
		for(var i = 0; i < ServiceResult.length; i++){
			skinImp = "btnImpN";
			//skinRowRead = "segRowReadN";
			skinRowRead = "hbxbggray";
			skinLblNotfnType="lblBlackMediumNormal";
			skinLblNotfnSub="lblBlack114Normal";
			
			if(ServiceResult[i]["imp"] == "Y"){
				skinImp = "btnImpY";
			}
			if(ServiceResult[i]["read"] == "Y"){
				skinRowRead = "hboxWhiteback";
				skinLblNotfnType="lblBlackMedium";
			    skinLblNotfnSub="lblBlack114";
				
			    
			}
			if(date == ServiceResult[i]["date"]){
				var timeR = cutTime(ServiceResult[i]["time"]);
				//var timeR = time24toAMPM(ServiceResult[i]["time"]);
				var tempRecord = {
					hbox101086657956686: {
						skin: skinRowRead
					},	
					ID: ServiceResult[i]["id"],
					lblNotfnType: {
						text: ServiceResult[i]["type"+localeSup],
						skin: skinLblNotfnType
					},
					lblNotfnSub: {
						text: ServiceResult[i]["subject"+localeSup],
						skin: skinLblNotfnSub
					},
					lblTime: {
						text: timeR
						
					},
					btnImp: {
						skin: skinImp, onClick: onclickBTNI
					},
					imgArrow: {
						src: "navarrow3.png"
					}
				}
				if(NotificationHomePageState) {
					tempRecord["imgDeleteSelect"] = "chbox.png";
				}
				//
				NotificationOnDate.push(tempRecord);
			}
			else{
				var tempPushArray = new Array();
				//var dateR = dateNumToWords(date);
				var dateR = dateFormatTMB(date);
				tempPushArray.push({lblDateHeader: dateR});
				tempPushArray.push(NotificationOnDate);
				gblNotificationDetailsCache.push(tempPushArray);
				var NotificationOnDate = new Array();
				date = ServiceResult[i]["date"];
				i = i - 1;
			}
		}
		var tempPushArray = new Array();
		//var dateR = dateNumToWords(date);
		var dateR = dateFormatTMB(date);
		tempPushArray.push({lblDateHeader: dateR});
		tempPushArray.push(NotificationOnDate);
		gblNotificationDetailsCache.push(tempPushArray);
		var NotificationOnDate = new Array();
		//frmNotificationHome.segMyRecipient.removeAll();
		
		//frmNotificationHome.segMyRecipient.data = gblNotificationDetailsCache;
		// commented above 2 lines and add below line to fix DEF181
		frmNotificationHome.segMyRecipient.setData(gblNotificationDetailsCache);
	}
	
	
}

//GET NOTIFICATION DETAILS
function getNotificationDetailsMB(){
	showLoadingScreen();
	var notfnID = null;
	if(gblPushNotfnDetailsFrom == "pushnotification"){
	
	
		notfnID = gblPayLoadKpns.notificationID;
		gblPushNotfnDetailsFrom = "";
	}
	else{
		notfnID = frmNotificationHome.segMyRecipient.selectedItems[0].ID;
	}
	if(notfnID != null && notfnID != undefined && notfnID.trim() != ""){
		var inputParams = {
			messageId : notfnID
		};
		invokeServiceSecureAsync("notificationdetails", inputParams, getNotificationDetailsMBCB)
	}
//	gblNotfnDetailsFrom = null;
}

function getNotificationDetailsMBCB(status, resultset){
	var deviceInfo = kony.os.deviceInfo();
	if(status == 400){
		if(resultset["status"] == "1"){
			showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
		}
		else if(resultset["status"] == "0"){
			if(resultset["Result"].length > 0){
				tempNotfnDetails = {
						lblNotfnTypeEN : resultset["Result"][0]["typeEN"],
						lblNotfnTypeTH : resultset["Result"][0]["typeTH"],
					    lblNotfnSubEN : resultset["Result"][0]["subjectEN"],
				        lblNotfnSubTH : resultset["Result"][0]["subjectTH"],
					    lblNotfnMessageEN : resultset["Result"][0]["messageEN"],
						lblNotfnMessageTH : resultset["Result"][0]["messageTH"],
						lblNotfnDate : dateFormatTMB(resultset["Result"][0]["date"]) + ", " + cutTime(resultset["Result"][0]["time"]),
						msgCODE: resultset["Result"][0]["msgCODE"],
						executeDate: resultset["Result"][0]["executeDate"],
						executeTime: resultset["Result"][0]["executeTime"],
						dueDate:resultset["Result"][0]["dueDate"],
						contentEN:resultset["Result"][0]["messageENParams"],
						contentTH:resultset["Result"][0]["messageTHParams"]
				}
				//Code change for IOS9
				if(deviceInfo["name"] == "android")
				{
					frmNotificationDetails.show();
					populateNotiDetails();
				}
				else
				{
					populateNotiDetails();
					frmNotificationDetails.show();				
				}
				unreadInboxMessagesMBLogin();//updating the count values.

			}
		}
		else if(resultset["status"] == "2"){
			showAlertRcMB(kony.i18n.getLocalizedString("keyMessageDeleted"), kony.i18n.getLocalizedString("info"), "info")
			gblNotificationHistoryModified = true;
			frmNotificationHome.show();
		}
		else{
			showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
		}
	}
	kony.application.dismissLoadingScreen();
}

//2013-10-04 00:00:00
function populateNotiDetails(){
	var localeSup = "EN";
	if(locale == "th_TH"){
		localeSup = "TH";
	}
	frmNotificationDetails.lblNotfnType.text = tempNotfnDetails["lblNotfnType"+localeSup];
	frmNotificationDetails.lblNotfnSub.text = tempNotfnDetails["lblNotfnSub"+localeSup];
	frmNotificationDetails.lblNotfnDate.text = tempNotfnDetails["lblNotfnDate"];
	frmNotificationDetails.lblNotfnMessage.text = tempNotfnDetails["lblNotfnMessage"+localeSup];
	var buttonExecute = null;
	if(flowSpa){
		buttonExecute = frmNotificationDetails.btnAgreeSpa;
	}
	else{
		buttonExecute = frmNotificationDetails.button4733076529055;
	}
	if(tempNotfnDetails["msgCODE"] == "N02" || tempNotfnDetails["msgCODE"] == "N03"){
		//frmNotificationDetails.button4733076529055.isVisible = true;
		//frmNotificationDetails.button4733076529055.text = kony.i18n.getLocalizedString("Transfer");
		//frmNotificationDetails.button4733076529055.onClick = ExecuteSend2SaveFromNotification;
		//buttonExecute.isVisible = true;
		if(flowSpa){
			frmNotificationDetails.hbxBackPay.setVisibility(true);
			frmNotificationDetails.HbxbackSpa.setVisibility(false);
			//frmNotificationDetails.hbox4733076529053.isVisible = false;
			//frmNotificationDetails.hbox448252584392275.isVisible = false;
		}
		else{
			frmNotificationDetails.hbox4733076529053.isVisible = true;
			frmNotificationDetails.hbox448252584392275.isVisible = false;
			//frmNotificationDetails.hbxBackPay.setVisibility(false);
			//frmNotificationDetails.HbxbackSpa.setVisibility(false);
		}
		buttonExecute.skin = "btnBlueSkin";
		buttonExecute.text = kony.i18n.getLocalizedString("Transfer");
		buttonExecute.onClick = ExecuteSend2SaveFromNotification;
	}
	else if(tempNotfnDetails["msgCODE"] == "N04"){
	
		var validateExecution = compareDatesBB(kony.os.date("yyyy-mm-dd"), tempNotfnDetails["dueDate"]);
		
		if(validateExecution != null){
			if(validateExecution == "less"){
				//frmNotificationDetails.button4733076529055.skin = "btnBlueSkin";
				//frmNotificationDetails.button4733076529055.onClick = ExecuteBeepAndBillFromNotification;
				buttonExecute.skin = "btnBlueSkin";
				buttonExecute.onClick = ExecuteBeepAndBillFromNotification;
			}
			else if(validateExecution == "equal"){
				//validateExecution = compareTimes(getCurrentTime(), tempNotfnDetails["executeTime"]);
			//	if(validateExecution == "less"){
					//frmNotificationDetails.button4733076529055.skin = "btnBlueSkin";
					//frmNotificationDetails.button4733076529055.onClick = ExecuteBeepAndBillFromNotification;
					buttonExecute.skin = "btnBlueSkin";
					buttonExecute.onClick = ExecuteBeepAndBillFromNotification;
				//}
//				else if(validateExecution == "more" || validateExecution == "less"){
//					//frmNotificationDetails.button4733076529055.skin = "btnDisabledGray";
//					//frmNotificationDetails.button4733076529055.onClick = null;
//					buttonExecute.skin = "btnDisabledGray";
//					buttonExecute.onClick = null;
//				}
			}
			else if(validateExecution == "more"){
				//frmNotificationDetails.button4733076529055.skin = "btnDisabledGray";
				//frmNotificationDetails.button4733076529055.onClick = null;
				buttonExecute.skin = "btnDisabledGray";
				buttonExecute.onClick = null;
			}
		}
		//frmNotificationDetails.button4733076529055.isVisible = true;
		//frmNotificationDetails.button4733076529055.text = kony.i18n.getLocalizedString("keyPay");
		//buttonExecute.isVisible = true;
		if(flowSpa){
			frmNotificationDetails.hbxBackPay.setVisibility(true);
			frmNotificationDetails.HbxbackSpa.setVisibility(false);
			//frmNotificationDetails.hbox4733076529053.isVisible = false;
			//frmNotificationDetails.hbox448252584392275.isVisible = false;
		}
		else{
			frmNotificationDetails.hbox4733076529053.isVisible = true;
			frmNotificationDetails.hbox448252584392275.isVisible = false;
			//frmNotificationDetails.hbxBackPay.setVisibility(false);
			//frmNotificationDetails.HbxbackSpa.setVisibility(false);
		}
		buttonExecute.text = kony.i18n.getLocalizedString("keyPay");
		//frmNotificationDetails.button4733076529055.onClick = ExecuteBeepAndBillFromNotification();
	}
	else{
		//frmNotificationDetails.button4733076529055.isVisible = false;
		//buttonExecute.isVisible = false;
		if(flowSpa)
		{
			frmNotificationDetails.hbxBackPay.setVisibility(false);
			frmNotificationDetails.HbxbackSpa.setVisibility(true);
			//frmNotificationDetails.hbox4733076529053.isVisible = false;
			//frmNotificationDetails.hbox448252584392275.isVisible = false;
		}
		else{
			frmNotificationDetails.hbox4733076529053.isVisible = false;
			frmNotificationDetails.hbox448252584392275.isVisible = true;
			//frmNotificationDetails.hbxBackPay.setVisibility(false);
			//frmNotificationDetails.HbxbackSpa.setVisibility(false);
		}
	}
}

//2013-10-04 - 2013-09-21
function compareDatesBB(d1, d2){
	if(d2 == null || d2 == undefined){
		return null;
	}
	
	var fromDate = kony.os.date("yyyy-mm-dd").toString().split("-");
	var toDate = d2.split("-");
	var fromDt = new Date(fromDate[0], fromDate[1] - 1, fromDate[2]);
	var toDt =  new Date(toDate[0], toDate[1] - 1, toDate[2]);
	
	 if(fromDt.getTime() > toDt.getTime()){
        return "more";
    }
    if(fromDt.getTime()== toDt.getTime()){
        return "equal";
    }
    if(fromDt.getTime() < toDt.getTime()){
        return "less";
    }
}

//Mon Dec 09 2013
//function getCurrentDate(){
//	var months = ["Jan","Feb","Mar","Apr","May","June","July","Aug","Sep","Oct","Nov","Dec"];
//	var d = new Date();
//	var date = d.toDateString();
//	var month = months.indexOf(date.substring(4,7));
//	if(month.length == 1){
//		month = "0" + month;
//	}
//	return date.substring(11,15) + "-" + month + "-" + date.substring(8,10);
//}

//DELETE NOTIFICATION/S
function deleteNotificationMB(){
	showLoadingScreen();
	var messages = "";
	var length = frmNotificationHome.segMyRecipient.selectedItems.length;
	for(var i = 0; i < length; i++){
		if(i == (length - 1)){
			messages = messages + frmNotificationHome.segMyRecipient.selectedItems[i].ID;
		}
		else{
			messages = messages + frmNotificationHome.segMyRecipient.selectedItems[i].ID + ",";
		}
	}
	var inputParams = {
		messageId : messages
	};
	invokeServiceSecureAsync("notificationdelete", inputParams, deleteNotificationMBCB)
}

function deleteNotificationMBCB(status, resultset){
	if(status == 400){
		if(resultset["status"] == "1"){
			showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
		}
		else if(resultset["status"] == "0"){
			gblNotificationHistoryModified = true;
			frmNotificationHome.show();
			unreadInboxMessagesMBLogin();
		}
		else{
			showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
		}
	}
	kony.application.dismissLoadingScreen();
}

//MARK NOTIFICATION IMPORTANT
function markNotificationImportantMB(){
	showLoadingScreen();
	var imp = frmNotificationHome.segMyRecipient.selectedItems[0]["btnImp"]["skin"];
	var ind = frmNotificationHome.segMyRecipient.selectedIndex;
	var imp1 = ind[0];
	var imp2 = ind[1];
	var msgID = gblNotificationDetailsCache[imp1][1][imp2]["ID"];
	var impStat = "Y";
	if(imp == "btnImpY"){
		impStat = "N";
	}
	var inputParams = {
		messageId : msgID,
		impStatus : impStat
	};
	invokeServiceSecureAsync("notificationimportant", inputParams, markNotificationImportantMBCB)
}

function markNotificationImportantMBCB(status, resultset){
	if(status == 400){
		if(resultset["status"] == "1"){
			showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
		}
		else if(resultset["status"] == "0"){
			gblNotificationHistoryModified = true;
			frmNotificationHome.show();
		}
		else if(resultset["status"] == "2"){
			if(resultset["error"] == "MaxLimitReached"){
				showAlertRcMB(kony.i18n.getLocalizedString("keyMessagesMaxLimit") + " " + gblMaxSetImportant + " " + kony.i18n.getLocalizedString("keyMessagesMaxLimit1"), kony.i18n.getLocalizedString("info"), "info")
			}
			else if(resultset["error"] == "NotificationDeleted"){
				gblNotificationHistoryModified = true;
				frmNotificationHome.show();
			}
		}
		else{
			showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
		}
	}
	kony.application.dismissLoadingScreen();
}

function cutTime(time){
	if(time.length >= 8){
		time = time.substring(0,5);
	}
	return time;
}

function dateFormatTMB(date){
	date = date.substring(8) + "/" + date.substring(5,7) + "/" + date.substring(0,4);
	return date;
}

function popupDeleteConfirm(){
	popUpNotfnDelete.dismiss();
	deleteNotificationMB();
}

function popupDeleteCancel(){
	popUpNotfnDelete.dismiss();
}

function NotificationDeleteOnClick(eventobject){
	if(eventobject.skin == "btnDeleteDB"){
		if(NotificationHomePageState == false){
			populateNotificationsDeleteState();
		}
		else{
			//deleteNotificationMB();
			var msg =kony.i18n.getLocalizedString("keyNotSure")+" "+  frmNotificationHome.segMyRecipient.selectedItems.length +" "+kony.i18n.getLocalizedString("keyMessages");
			if(flowSpa)
			popUpNotfnDelete.skin = "popUpBg";//"popUpSpaWhiteBg";
			popUpNotfnDelete.lblNotfnMsg.text = msg;
			popUpNotfnDelete.btnPopDeleteCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
			popUpNotfnDelete.btnPopDeleteYEs.text = kony.i18n.getLocalizedString("keyYes");
			popUpNotfnDelete.btnPopDeleteCancel.onClick = popupDeleteCancel;
			popUpNotfnDelete.btnPopDeleteYEs.onClick = popupDeleteConfirm;
			popUpNotfnDelete.show();
		}
	}
	else if(eventobject.skin == "btnDeleteActive"){
		//do nothing
	}
}

function CancelDeleteOnClick(eventobject){
	if(eventobject.skin == "btnCancel"){
		NotificationHomePageState = false;
		locale = kony.i18n.getCurrentLocale();
		populateNotificationsNormalState();
	}
	else{
		//do nothing
	}
}

//167|28112013|aaaaaaaaaaaaaaaa|4966941120145007|26,557.76|0.00|
//aaaaaaaaaaaaaaaa Ref. 4966941120145007 Total Amount due is 204,516.45 THB Deposit Account Balance is THB Please pay this bill before 28Nov13
//Pincode|Expriedate|BBNickname|Reference1|Amount|FeeAmount|Message for sending EN
function ExecuteBeepAndBillFromNotification(){
	   
    var locale = kony.i18n.getCurrentLocale();
	var result= ""
	if(locale="en_US"){
		result =tempNotfnDetails["contentEN"].split("|");
	} else {
		result =tempNotfnDetails["contentTH"].split("|");
	}
    
 	var bnb = new Object();


	 bnb["Pincode"] = result[0];
	 bnb["Expriedate"]= tempNotfnDetails["dueDate"];
	 bnb["BBNickname"] = result[2];
	 bnb["Reference1"] = result[3];
	 bnb["Amount"] = result[4];
	 bnb["FeeAmount"] = result[5];
	 bnb["cmpCode"] = result[6];
	 bnb["ref2"] = "";
 
 if(result.length > 7)
  bnb["ref2"] = result[7];
 
 
    
     customerPaymentStatusInquiryForBBPushMsg(bnb,"true");
}

function ExecuteSend2SaveFromNotification(){
	gblSSExcuteCnfrm = "randomXC"; // to control S2S execution flow 
	gblExeS2S = "true";
	gblNotificationFor = "randomXC";
	getIBMBStatus();
}

function clearGVNotificationInboxMB(){
	//NOTIFICATION
	gblNotificationDetailsCache = new Array();
	tempPushArray = new Array();
	gblNotificationHistoryModified = true;
	NotificationHomePageState = false;
	locale = kony.i18n.getCurrentLocale();
	ServiceResult = new Array();
	tempNotfnDetails = null;
	//INBOX
	gblUnreadCount = "0";
	gblInboxDetailsCache = new Array();
	gblInboxDetailsCacheForSort = new Array();
	tempPushArrayInbox = new Array();
	gblInboxHistoryModified = true;
	InboxHomePageState = false;
	var ServiceResultInbox = new Array();
	SortByType = "Date/Time";
	SortByOrder = "ascending";
	gblInboxSortedOrNormal = false;
	tempInboxDetails = null;
}
function frmNotificationHome_btnHdrMenu_onClick() {
//	  frmNotificationHome.scrollboxMain.scrollToEnd();
//    gblIndex = -1;
//    isMenuShown = false;
//    handleMenuBtn();
    unreadInboxMessagesTrackerLocalDBSync();
};