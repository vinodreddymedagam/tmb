 function createNewPopUP() {
	
  pupUpShowinfoLTF = new kony.ui.Popup({
        "id": "pupUpShowinfoLTF",
        "transparencyBehindThePopup": 20,
        "skin": "popUpBgTrans",
        "isModal": false
    }, {
        "padding": [0, 0, 0, 0],
        "containerWeight": 100,
        "containerHeight": null,
        "paddingInPixel": false,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_DEFAULT,
        "bounces": true,
        "configureExtendTop": false,
        "footerOverlap": false,
        "headerOverlap": false,
        "inTransitionConfig": {
            "transitionDirection": "none",
            "transitionEffect": "none",
            "transitionDuration": 0.3
        },
        "outTransitionConfig": {
            "transitionDirection": "none",
            "transitionEffect": "none",
            "transitionDuration": 0.3
        }
    });
    
    var btnMarginLeft=1;
    var indLeft=0;
    var indright=27;
    //#ifdef android
		btnMarginLeft=295;
		indLeft=60;
   		indright=0;
	//#endif
   
    var btnclose = new kony.ui.Button({
        "id": "btnclose",
        "isVisible": true,
        "text": "",
        "skin": "btnCloseblue",
        "onClick":closePopUpbutton
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_MIDDLE_RIGHT,
        "vExpand": false,
        "hExpand": true,
        "margin": [btnMarginLeft, 1, 1, 1],
        "padding": [0, 3, 0, 3],
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "marginInPixel": true,
        "paddingInPixel": false,
        "containerWeight": 12
    }, {
        "glowEffect": false,
        "showProgressIndicator": true
    });
    var hbox76335790771243 = new kony.ui.Box({
        "id": "hbox76335790771243",
        "isVisible": true,
        "skin": "hboxWhiteback",
        "orientation": constants.BOX_LAYOUT_HORIZONTAL,
        "layoutAlingent":constants.BOX_LAYOUT_ALIGN_FROM_RIGHT
    }, {
        "containerWeight": 20,
        "percent": false,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
        "margin": [0, 0, 0, 0],
        "padding": [0, 0, 0, 0],
        "vExpand": false,
        "marginInPixel": false,
        "paddingInPixel": false,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {});
    hbox76335790771243.add(btnclose);
    var lblinfo = new kony.ui.Label({
        "id": "lblinfo",
        "isVisible": true,
        "text": kony.i18n.getLocalizedString("MF_lbl_LTF_Units"),
        "skin": "lblGrey36px"
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "vExpand": false,
        "hExpand": true,
        "margin": [40, 1, 40, 5],
        "padding": [1, 2, 1, 6],
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "marginInPixel": true,
        "paddingInPixel": false,
        "containerWeight": 100
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var hbox76335790771242 = new kony.ui.Box({
        "id": "hbox76335790771242",
        "isVisible": true,
        "skin": "hboxWhiteback",
        "orientation": constants.BOX_LAYOUT_HORIZONTAL
    }, {
        "containerWeight": 64,
        "percent": true,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
        "margin": [0, 0, 0, 0],
        "padding": [0, 0, 0, 0],
        "vExpand": false,
        "marginInPixel": false,
        "paddingInPixel": false,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {});
    hbox76335790771242.add(lblinfo);
    var imgindicate = new kony.ui.Image2({
        "id": "imgindicate",
        "isVisible": true,
        "src": "arrowdown.png",
        "imageWhenFailed": null,
        "imageWhileDownloading": null
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "margin": [0, 0, 0, 0],
        "padding": [0, 0, 0, 0],
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "referenceWidth": null,
        "referenceHeight": null,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 7
    }, {
        "glossyEffect": constants.IMAGE_GLOSSY_EFFECT_DEFAULT
    });
    var hbox76335790771244 = new kony.ui.Box({
        "id": "hbox76335790771244",
        "isVisible": true,
        "skin": "hboxTransparentBG",
        "orientation": constants.BOX_LAYOUT_HORIZONTAL,
        "layoutAlingent":constants.BOX_LAYOUT_ALIGN_FROM_CENTER
    }, {
        "containerWeight": 16,
        "percent": false,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
        //"margin": [0, 0, 27, 0],
        "margin": [indLeft, 0, indright, 0],
        "padding": [0, 0, 0, 0],
        "vExpand": false,
        "marginInPixel": false,
        "paddingInPixel": false,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {});
    hbox76335790771244.add(imgindicate);
    var vbox76335790771241 = new kony.ui.Box({
        "id": "vbox76335790771241",
        "isVisible": true,
        "orientation": constants.BOX_LAYOUT_VERTICAL
    }, {
        "containerWeight": 100,
        "margin": [0, 0, 0, 0],
        "padding": [0, 0, 0, 0],
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "vExpand": false,
        "hExpand": true,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {});
    vbox76335790771241.add(hbox76335790771243, hbox76335790771242, hbox76335790771244);
    var hbxmain = new kony.ui.Box({
        "id": "hbxmain",
        "isVisible": true,
        "skin": "hboxTransparentBG",
        "orientation": constants.BOX_LAYOUT_HORIZONTAL,
        "onClick":closePopUpbutton
    }, {
        "containerWeight": 39,
        "percent": true,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
        "margin": [5, 0, 5, 0],
        "padding": [0, 0, 0, 0],
        "vExpand": false,
        "marginInPixel": false,
        "paddingInPixel": false,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {});
    hbxmain.add(vbox76335790771241);
    pupUpShowinfoLTF.add(hbxmain);
    
    //Defining the context Object
    var context1 = {
        "widget": frmMFFullStatementNFundSummary.Imghelpinfo,
        "anchor": "top",
        "sizetoanchorwidth": false
    };

    //setContext method call
    pupUpShowinfoLTF.setContext(context1);
    //Type your code here
   pupUpShowinfoLTF.show();
}
function closePopUpbutton(){
	pupUpShowinfoLTF.dismiss();
}
/**
 * @function
 *
 */
function showInfoPupUp(){
	try{
		createNewPopUP();
	}catch (e) {
	alert(e);
}
 	//createNewPopUP();
}

function initFundsummaryMethods(){

  frmMFFullStatementNFundSummary.btnMainMenu.onClick=MBcallMutualFundsSummary;
  frmMFFullStatementNFundSummary.btnFundSummary1.onClick=showFundsummarydetails;
  frmMFFullStatementNFundSummary.btnFullStatement.onClick=showFullSatement;
  frmMFFullStatementNFundSummary.onDeviceBack=callDummy;

  frmMFFullStatementNFundSummary.CopyImage05bc0f99dc9aa4c.onTouchEnd=callMFPDF;
  frmMFFullStatementNFundSummary.btnDownload.onClick=callMFPDF;
  frmMFFullStatementNFundSummary.btnViewTaxDoc.onClick=generateTaxDocPdfMB;
  frmMFFullStatementNFundSummary.segFullSatementData.onRowClick=popUpShowTransactionDetails;
  frmMFFullStatementNFundSummary.preShow=preShowfrmMFFullStatementNFundSummary;
  frmMFFullStatementNFundSummary.postShow=assignGlobalForMenuPostshow;
  frmMFFullStatementNFundSummary.btnback.onClick = onClickRedeem;
  frmMFFullStatementNFundSummary.btnnext.onClick = onClickPurchase;
  frmMFFullStatementNFundSummary.btnSwitch.onClick = onClickSwitchMBMenu;
}
/**
 * @function
 *
 */
function callMFPDF(){
  var fileType="pdf";
  var templateName="MFFullStatementTemplate";
  savePDFMutualFundFullStatementServiceMB(fileType, templateName);
}

/**
 * @function
 *
 */
function preShowfrmMFFullStatementNFundSummary(){
		try{
    kony.print("gblCallPrePost = " + gblCallPrePost);
    if (gblCallPrePost) {
        kony.print("In preShowfrmMFFullStatementNFundSummary");
        frmMFFullStatementNFundSummary.flexFundDetails.setVisibility(true);
        frmMFFullStatementNFundSummary.FlexFSAmount.setVisibility(true);
        frmMFFullStatementNFundSummary.FlexFSDate.setVisibility(true);
        frmMFFullStatementNFundSummary.FlexFSNavUnit.setVisibility(true);
        frmMFFullStatementNFundSummary.FlexFSCost.setVisibility(true);
        frmMFFullStatementNFundSummary.FlexFSTaxDoc.setVisibility(true);
        frmMFFullStatementNFundSummary.Imghelpinfo.onTouchEnd = showInfoPupUp;
        frmMFFullStatementNFundSummary.Imghelpinfo.zIndex = 5;
        frmMFFullStatementNFundSummary.lblDate.text = kony.i18n.getLocalizedString("MF_lbl_Date_as_of");
        if (gblMBMFDetailsResulttable["TaxDoc"] == "N") {
            frmMFFullStatementNFundSummary.FlexFSTaxDoc.setVisibility(false);
        } else {
            frmMFFullStatementNFundSummary.FlexFSTaxDoc.setVisibility(true);
        }
        frmMFFullStatementNFundSummary.flxFooter.setVisibility(true);
        frmMFFullStatementNFundSummary.FlexScroll.height = "81%";
        frmMFFullStatementNFundSummary.FlexScroll.top = "8%";
        frmMFFullStatementNFundSummary.FlexOrderToProcess.setVisibility(false);
        frmMFFullStatementNFundSummary.FlexOrderToProcesssegBg.setVisibility(false);
        frmMFFullStatementNFundSummary.FlexLoadingFullSatement.setVisibility(false);
        frmMFFullStatementNFundSummary.flexLoading.setVisibility(false);
        frmMFFullStatementNFundSummary.FlexMonthTab.setVisibility(false);
        frmMFFullStatementNFundSummary.FlexdetailHeader.setVisibility(false);
        frmMFFullStatementNFundSummary.Flexpdf.setVisibility(false);
        frmMFFullStatementNFundSummary.segFullSatementData.setVisibility(false);
	 //--setfocus for funsummary
        //frmMFFullStatementNFundSummary.flexFullStatement.skin="flexLineBlue"; //MKI , MIB-10761
      	//frmMFFullStatementNFundSummary.flexFundSummary.skin="flexLinedarkBlack"; //MKI , MIB-10761
        frmMFFullStatementNFundSummary.btnFullStatement.height = "98%";
	    frmMFFullStatementNFundSummary.btnFullStatement.skin = "btnblueGreybg"; //MKI , MIB-10761
      
        frmMFFullStatementNFundSummary.btnFullStatement.focusSkin = "btnblueGreybg"; // mki, MIB- 9502
      	frmMFFullStatementNFundSummary.btnFundSummary1.height = "97%";
	    frmMFFullStatementNFundSummary.btnFundSummary1.skin = "btnblack20px";
		frmMFFullStatementNFundSummary.btnFundSummary1.focusSkin = "btnblack20px"; // mki, MIB- 9502
	 
        frmMFFullStatementNFundSummary.btnViewTaxDoc.text = kony.i18n.getLocalizedString("MF_lbl_Tax_doc");
        frmMFFullStatementNFundSummary.btnFullStatement.text = kony.i18n.getLocalizedString("MF_TB_Full_Statement");
        frmMFFullStatementNFundSummary.btnFundSummary1.text = kony.i18n.getLocalizedString("lbFundSummary");
        frmMFFullStatementNFundSummary.btnback.text = kony.i18n.getLocalizedString("key_MF_Redeem");
        frmMFFullStatementNFundSummary.btnnext.text = kony.i18n.getLocalizedString("key_MF_Purchase");
        frmMFFullStatementNFundSummary.btnDownload.text = kony.i18n.getLocalizedString("keyDownload");
        frmMFFullStatementNFundSummary.lblLoadingFullSatement.text = kony.i18n.getLocalizedString("key_Loading");
        frmMFFullStatementNFundSummary.lblInvestmentVal.skin = "lblDarkGrey257Bold";
    } else {
        kony.print("no gblCallPrePost = " + gblCallPrePost);
    }
    }catch(e){
    	kony.print(e.stack);
    }
}

function showFundsummarydetails() {
	try{
  
        kony.print("In Fundsummarydetails");
        frmMFFullStatementNFundSummary.flexFundDetails.setVisibility(true);
        frmMFFullStatementNFundSummary.FlexFSAmount.setVisibility(true);
        frmMFFullStatementNFundSummary.FlexFSDate.setVisibility(true);
        frmMFFullStatementNFundSummary.FlexFSNavUnit.setVisibility(true);
        frmMFFullStatementNFundSummary.FlexFSCost.setVisibility(true);
        frmMFFullStatementNFundSummary.FlexFSTaxDoc.setVisibility(true);
        frmMFFullStatementNFundSummary.Imghelpinfo.onTouchEnd = showInfoPupUp;
        frmMFFullStatementNFundSummary.Imghelpinfo.zIndex = 5;
        frmMFFullStatementNFundSummary.lblDate.text = kony.i18n.getLocalizedString("MF_lbl_Date_as_of");
        if (gblMBMFDetailsResulttable["TaxDoc"] == "N") {
            frmMFFullStatementNFundSummary.FlexFSTaxDoc.setVisibility(false);
        } else {
            frmMFFullStatementNFundSummary.FlexFSTaxDoc.setVisibility(true);
        }
        frmMFFullStatementNFundSummary.flxFooter.setVisibility(true);
        frmMFFullStatementNFundSummary.FlexScroll.height = "81%";
        frmMFFullStatementNFundSummary.FlexScroll.top = "8%";
        frmMFFullStatementNFundSummary.FlexOrderToProcess.setVisibility(false);
        frmMFFullStatementNFundSummary.FlexOrderToProcesssegBg.setVisibility(false);
        frmMFFullStatementNFundSummary.FlexLoadingFullSatement.setVisibility(false);
        frmMFFullStatementNFundSummary.flexLoading.setVisibility(false);
        frmMFFullStatementNFundSummary.FlexMonthTab.setVisibility(false);
        frmMFFullStatementNFundSummary.FlexdetailHeader.setVisibility(false);
        frmMFFullStatementNFundSummary.Flexpdf.setVisibility(false);
        frmMFFullStatementNFundSummary.segFullSatementData.setVisibility(false);
       
      	//frmMFFullStatementNFundSummary.flexFullStatement.skin="flexLineBlue"; //MKI , MIB-10761
      	//frmMFFullStatementNFundSummary.flexFundSummary.skin="flexLinedarkBlack"; //MKI , MIB-10761
        frmMFFullStatementNFundSummary.btnFullStatement.height = "98%";
	    frmMFFullStatementNFundSummary.btnFullStatement.skin = "btnblueGreybg"; //MKI , MIB-10761
      	frmMFFullStatementNFundSummary.btnFullStatement.focusSkin = "btnblueGreybg";  // mki, MIB- 9502
      	frmMFFullStatementNFundSummary.btnFundSummary1.height = "97%";
	    frmMFFullStatementNFundSummary.btnFundSummary1.skin = "btnblack20px";
       	frmMFFullStatementNFundSummary.btnFundSummary1.focusSkin = "btnblack20px"; // mki, MIB- 9502


    }catch(e){
    	kony.print(e.stack);
    }
}
/**
 * @function
 *
 */
function showFullSatement() {
    kony.print("In FullSatement");
    // alert("In FullSatement "+gblMBMFDetailsResulttable["TaxDoc"]);
    try {
    	frmMFFullStatementNFundSummary.FlexLoadingFullSatement.setVisibility(true);
    	frmMFFullStatementNFundSummary.flexLoading.setVisibility(true);
    	showAnimationMFLoading1();
    	showAnimationMFLoading2();
		frmMFFullStatementNFundSummary.FlexOrderToProcess.setVisibility(false);
        frmMFFullStatementNFundSummary.FlexOrderToProcesssegBg.setVisibility(false);
        frmMFFullStatementNFundSummary.FlexOrderToProcesssegBg.removeAll();
        frmMFFullStatementNFundSummary.FlexFSAmount.setVisibility(false);
        frmMFFullStatementNFundSummary.FlexFSDate.setVisibility(false);
        frmMFFullStatementNFundSummary.FlexFSNavUnit.setVisibility(false);
        frmMFFullStatementNFundSummary.FlexFSCost.setVisibility(false);
        frmMFFullStatementNFundSummary.FlexFSTaxDoc.setVisibility(false);
        frmMFFullStatementNFundSummary.flxFooter.setVisibility(false);
        frmMFFullStatementNFundSummary.FlexMonthTab.setVisibility(false);
        frmMFFullStatementNFundSummary.FlexdetailHeader.setVisibility(false);

        frmMFFullStatementNFundSummary.segFullSatementData.setVisibility(false);
        //frmMFFullStatementNFundSummary.FlexTab.height="10.8%";
        //frmMFFullStatementNFundSummary.FlexScroll.height="90%";

        //frmMFFullStatementNFundSummary.btnDownload.skin="btnRoundWhite200";
        //frmMFFullStatementNFundSummary.btnDownload.focusSkin="btnRoundWhite200Focus";
        //frmMFFullStatementNFundSummary.FlexScroll.height="90%";
        //frmMFFullStatementNFundSummary.FlexScroll.top="8%";
        frmMFFullStatementNFundSummary.btn1.skin = "btnBlue128";
        frmMFFullStatementNFundSummary.btn2.skin = "btnBlue128";
        frmMFFullStatementNFundSummary.btn3.skin = "btnBlue128";
        frmMFFullStatementNFundSummary.btn4.skin = "btnBlue128";
        frmMFFullStatementNFundSummary.btn5.skin = "btnBlue128";

        frmMFFullStatementNFundSummary.btn6.skin = "btnBlueSkin150";

        //--setfocus for full statement
        //frmMFFullStatementNFundSummary.flexFullStatement.skin = "flexLinedarkBlack"; //MKI , MIB-10761
        //frmMFFullStatementNFundSummary.flexFundSummary.skin = "flexLineBlue"; //MKI , MIB-10761
        frmMFFullStatementNFundSummary.btnFullStatement.height = "97%";
        frmMFFullStatementNFundSummary.btnFullStatement.skin = "btnblack20px";
       	frmMFFullStatementNFundSummary.btnFullStatement.focusSkin = "btnblack20px"; // mki, MIB- 9502
        frmMFFullStatementNFundSummary.btnFundSummary1.height = "98%";
        //frmMFFullStatementNFundSummary.btnFundSummary1.skin = "btnblueWhitebg"; //MKI , MIB-10761
      	frmMFFullStatementNFundSummary.btnFundSummary1.skin = "btnblueGreybg"; //MKI , MIB-10761
      	frmMFFullStatementNFundSummary.btnFundSummary1.focusSkin = "btnblueGreybg"; // mki, MIB- 9502

        frmMFFullStatementNFundSummary.lblLoadingFullSatement.text = kony.i18n.getLocalizedString("key_Loading");
        viewMFFullStatementMB();
    } catch (e) {
        kony.print("error showFullSatement " + e.stack);
    }
}
/**
 * @function
 *
 */
function getMonthtabMF(date) {
        var monthRecord = []; 
        var months = [kony.i18n.getLocalizedString("keyCalendarJan"), kony.i18n.getLocalizedString("keyCalendarFeb"), kony.i18n.getLocalizedString("keyCalendarMar"), kony.i18n.getLocalizedString("keyCalendarApr"), kony.i18n.getLocalizedString("keyCalendarMay"), kony.i18n.getLocalizedString("keyCalendarJun"), kony.i18n.getLocalizedString("keyCalendarJul"), kony.i18n.getLocalizedString("keyCalendarAug"), kony.i18n.getLocalizedString("keyCalendarSep"), kony.i18n.getLocalizedString("keyCalendarOct"), kony.i18n.getLocalizedString("keyCalendarNov"), kony.i18n.getLocalizedString("keyCalendarDec")];

        var todaysDate = date.split("/");
        var mm = parseInt(todaysDate[1]);
        var fullYear = todaysDate[0];
        var yy = fullYear.toString().substring(2, 4);
        var i = 6;
        while (i >= 0) {
            if (mm > 0) {
                var monthTab = months[mm - 1] + " " + yy;
                monthRecord.push(monthTab);
                mm = mm - 1;
            } else {
                mm = 12;
                yy--;
            }
            i--;
        }
        kony.print("monthRecord " + monthRecord);
        return monthRecord;
    }
/**
 * @function
 *
 */
function setMonthTab(monthTab){
  frmMFFullStatementNFundSummary.btn6.text=monthTab[0];
  frmMFFullStatementNFundSummary.btn5.text=monthTab[1];
  frmMFFullStatementNFundSummary.btn4.text=monthTab[2];
  frmMFFullStatementNFundSummary.btn3.text=monthTab[3];
  frmMFFullStatementNFundSummary.btn2.text=monthTab[4];
  frmMFFullStatementNFundSummary.btn1.text=monthTab[5];
  
}
function onTabSelectMFMonth(selectedIndex) {
  frmMFFullStatementNFundSummary.lblLoadingFullSatement.text = kony.i18n.getLocalizedString("key_Loading"); 
  var monthtab=getMonthtabMF(gblDate);
    //alert("index"+selectedIndex);
     frmMFFullStatementNFundSummary.btn1.skin="btnBlue128";
     frmMFFullStatementNFundSummary.btn2.skin="btnBlue128";
     frmMFFullStatementNFundSummary.btn3.skin="btnBlue128";
     frmMFFullStatementNFundSummary.btn4.skin="btnBlue128";
     frmMFFullStatementNFundSummary.btn5.skin="btnBlue128";
     frmMFFullStatementNFundSummary.btn6.skin="btnBlue128";
   if(selectedIndex=="5") {
    //frmMFFullStatementNFundSummary.segFullSatementData.setData(data1);
    frmMFFullStatementNFundSummary.btn1.skin="btnBlueSkin150";
  }else  if(selectedIndex=="4") {
    //frmMFFullStatementNFundSummary.segFullSatementData.setData(data2);
    frmMFFullStatementNFundSummary.btn2.skin="btnBlueSkin150";
  }else  if(selectedIndex=="3") {
    //frmMFFullStatementNFundSummary.segFullSatementData.setData(data3);
    frmMFFullStatementNFundSummary.btn3.skin="btnBlueSkin150";
  }else  if(selectedIndex=="2") {
   // frmMFFullStatementNFundSummary.segFullSatementData.setData(data4);
    frmMFFullStatementNFundSummary.btn4.skin="btnBlueSkin150";
  }else  if(selectedIndex=="1") {
    //frmMFFullStatementNFundSummary.segFullSatementData.setData(data5);
    frmMFFullStatementNFundSummary.btn5.skin="btnBlueSkin150";
  }else  if(selectedIndex=="0") {
    //frmMFFullStatementNFundSummary.segFullSatementData.setData(data6);
    frmMFFullStatementNFundSummary.btn6.skin="btnBlueSkin150";
  }
	frmMFFullStatementNFundSummary.segFullSatementData.removeAll();
	frmMFFullStatementNFundSummary.segFullSatementData.setVisibility(false);
  	frmMFFullStatementNFundSummary.FlexdetailHeader.setVisibility(false);
  	frmMFFullStatementNFundSummary.FlexLoadingFullSatement.setVisibility(true);
  	frmMFFullStatementNFundSummary.flexLoading.setVisibility(true);
  	frmMFFullStatementNFundSummary.FlexFSDate.setVisibility(false);
  	frmMFFullStatementNFundSummary.Flexpdf.setVisibility(false);
  	showAnimationMFLoading1();
    showAnimationMFLoading2();
  	selectedMonth=monthtab[selectedIndex];
   	getDateFormatStmtMF(selectedMonth,selectedIndex);
  
	//onRowSelect="Y";
}



function callOrderToBeProcessServiceCallBackMB1233(status, resulttable) {
   	frmMFFullStatementNFundSummary.FlexMonthTab.setVisibility(true);
    viewMFFullStmt(startDate, endDate, "1");
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (resulttable["StatusCode"] == 0) {
				 //dismissLoadingScreen();
				kony.application.dismissLoadingScreen();
                //alert("OrderToBeProcessDS "+JSON.stringify(resulttable["OrderToBeProcessDS"]));
                if (resulttable["OrderToBeProcessDS"].length == 0) {
                    frmMFFullStatementNFundSummary.FlexMonthTab.top="0dp";
                    frmMFFullStatementNFundSummary.FlexOrderToProcess.setVisibility(false);
                    frmMFFullStatementNFundSummary.FlexOrderToProcesssegBg.setVisibility(visible)(false);
                   frmMFFullStatementNFundSummary.FlexOrderToProcesssegBg.layoutType = kony.flex.FLOW_VERTICAL
                  return;
                }
                var tempData = [];
                var date = "";
                var time = "";
                var locale = kony.i18n.getCurrentLocale();
                var tranTypeHub = "TranTypeHubEN";
                var statusHub = "StatusHubEN";
                var channelHub = "ChannelHubEN";
                if (locale == "th_TH") {
                    tranTypeHub = "TranTypeHubTH";
                    statusHub = "StatusHubTH";
                    channelHub = "ChannelHubTH";
                }

                for (var i = 0; i < resulttable["OrderToBeProcessDS"].length; i++) {
                    try {
                        var orderDate = resulttable["OrderToBeProcessDS"][i]["OrderDate"].split(" ");
                        date = orderDate[0];
                        time = orderDate[1];
						var amount=commaFormatted(resulttable["OrderToBeProcessDS"][i]["Amount"]);
						if(parseInt(resulttable["OrderToBeProcessDS"][i]["Amount"])==0&&parseInt(resulttable["OrderToBeProcessDS"][i]["Unit"])!=0){
							amount=verifyDisplayUnit(resulttable["OrderToBeProcessDS"][i]["Unit"]);
						}
                        var segData = {
                            seqid: i,
                            lblorderDateVal: date,
                            //resulttable["OrderToBeProcessDS"][i]["OrderDate"],
                            lblTimeVal: time,
                            lblAmountVal: amount,
                            lbltransVal: resulttable["OrderToBeProcessDS"][i][tranTypeHub],
                            imgindicator: "",
                            ishidden: "yes",
                            effectiveDate: resulttable["OrderToBeProcessDS"][i]["EfftDate"],
                            unit: resulttable["OrderToBeProcessDS"][i]["Unit"],
                            unitBal: resulttable["OrderToBeProcessDS"][i]["UnitBalance"],
                            price: resulttable["OrderToBeProcessDS"][i]["Price"],
                            Status: resulttable["OrderToBeProcessDS"][i][statusHub],
                            channelexpval: resulttable["OrderToBeProcessDS"][i][channelHub]
                           
                        }
                        tempData.push(segData);
                    } catch (e) {
                        // todo: handle exception
                        alert(" " + e);
                    }

                }
                //frmMFFullStatementNFundSummary.segOrderToProcess.removeAll();
                frmMFFullStatementNFundSummary.FlexOrderToProcess.setVisibility(true);
                var nooftransaction=kony.i18n.getLocalizedString("key_ordertoprocess_count");
              	if(locale == "en_US" && (resulttable["OrderToBeProcessDS"].length==1 || resulttable["OrderToBeProcessDS"].length=="1")){
                  nooftransaction=replaceAll(nooftransaction, "orders", "order")
                }
              	nooftransaction=replaceAll(nooftransaction, "{count}", resulttable["OrderToBeProcessDS"].length);
				frmMFFullStatementNFundSummary.lblOdertoProcess.text=nooftransaction;
                //frmMFFullStatementNFundSummary.segOrderToProcess.rowSkin="segBgLightGry";
                frmMFFullStatementNFundSummary.FlexOrderToProcesssegBg.setVisibility(true);
                frmMFFullStatementNFundSummary.segOrderToProcess.setData(tempData);
                frmMFFullStatementNFundSummary.FlexMonthTab.top="-3dp";    
                
                //frmMFFullStatementNFundSummary.Flexpdf.setVisibility(true);
                //gblsegmentdataMB = frmMFFullStatementNFundSummary.segFullSatementData.data;
               
               
            }
             
        } else {
        	kony.application.dismissLoadingScreen();
           // dismissLoadingScreen();

        }
    } else {
    	kony.application.dismissLoadingScreen();
        //dismissLoadingScreen();
    }
}
 function viewMFFullStatementMB() {
    try {
    	//alert("in start viewMFFullStatementMB");
        var monthTab = getMonthtabMF(gblDate);
        setMonthTab(monthTab);
        endDate = getTodaysDateStmtMF();
        startDate = dateFormatChangeMF(new Date(new Date().getFullYear(), new Date().getMonth(), 1));
        startDateStmt = "";
        endDateStmt = "";
        selectedIndex = 0;
        kony.print("startDate " + startDate);
        kony.print("endDate " + endDate);
        currentpageStmt = 1;
        frmMFFullStatementNFundSummary.segFullSatementData.removeAll();
        frmMFFullStatementNFundSummary.segFullSatementData.setVisibility(false);
        frmMFFullStatementNFundSummary.FlexLoadingFullSatement.setVisibility(true);
        frmMFFullStatementNFundSummary.flexLoading.setVisibility(true);
        //frmMFFullStatementNFundSummary.segOrderToProcess.width="94%";
        showAnimationMFLoading1();
    	showAnimationMFLoading2();
        callOrderToBeProcessServiceMB();
        //viewMFFullStmt(startDate, endDate, "1");
        //alert("in  end viewMFFullStatementMB");
    } catch (e) {
        alert("" + e);
    }
    
}
function getGlobalDate() {
	showLoadingScreen();
	var input_params={};
	invokeServiceSecureAsync("GetServerDateTime", input_params, clBackgetGlobalDate)
} 
function clBackgetGlobalDate(status,result){
	if(status==400){
		if(result["opstatus"]==0){
			gblDate=result["date"];
			dismissLoadingScreen();
		}
		else{
			dismissLoadingScreen();
		}
	}
	dismissLoadingScreen();
}
function startFullStmtMFServiceMBCallBack(status, callBackResponse) {
    kony.print("call back startFullStmtMFServiceMBCallBack");
    frmMFFullStatementNFundSummary.FlexLoadingFullSatement.setVisibility(false);
    frmMFFullStatementNFundSummary.flexLoading.setVisibility(false);
    if (status == 400) {
        if (callBackResponse["opstatus"] == '0' || callBackResponse["opstatus"] == 0) {
            gblStmntSessionFlag = callBackResponse["sesFlag"];
            kony.application.dismissLoadingScreen();
            bindata = callBackResponse["BinData"];
            binlength = callBackResponse["BinLength"];
            var temp = [];
            try {
                if (callBackResponse["StatusCode"] == 0) {
                	 //alert("listOrderDS "+JSON.stringify(callBackResponse["listOrderDS"]));
                    if (callBackResponse["listOrderDS"].length == 0) {


                        //frmMFFullStatementNFundSummary.lblDate.text=kony.i18n.getLocalizedString("MF_MSG_No_Record");
                        //frmMFFullStatementNFundSummary.lblDateVal.text="";
                        var data = frmMFFullStatementNFundSummary.segFullSatementData.data;
                        if (data != null && data.length > 0) {
                            frmMFFullStatementNFundSummary.FlexFSDate.setVisibility(false);
                            frmMFFullStatementNFundSummary.FlexdetailHeader.setVisibility(true);
                            frmMFFullStatementNFundSummary.segFullSatementData.setVisibility(true);
                        } else {
                            //frmMFFullStatementNFundSummary.FlexFSDate.setVisibility(true);
                            frmMFFullStatementNFundSummary.FlexdetailHeader.setVisibility(false);
                            frmMFFullStatementNFundSummary.segFullSatementData.setVisibility(false);
                            //frmMFFullStatementNFundSummary.lblDateVal.contentAlignment=constants.CONTENT_ALIGN_CENTER;
                          	frmMFFullStatementNFundSummary.FlexLoadingFullSatement.setVisibility(true);
                          	frmMFFullStatementNFundSummary.lblLoadingFullSatement.text = kony.i18n.getLocalizedString("key_notransactionMFMsg");
                            //frmMFFullStatementNFundSummary.lblDate.text = "";
                        }
						frmMFFullStatementNFundSummary.FlexScroll.height = "90%";
                        frmMFFullStatementNFundSummary.Flexpdf.setVisibility(false);
                    } else {
                        var locale = kony.i18n.getCurrentLocale();
                        var tranTypeHub = "TranTypeHubEN";
                        var channelHub = "ChannelHubEN";
                        if (locale == "th_TH") {
                            tranTypeHub = "TranTypeHubTH";
                            channelHub = "ChannelHubTH";
                        }
                        frmMFFullStatementNFundSummary.segFullSatementData.setVisibility(true);
                        for (var i = 0; i < callBackResponse["listOrderDS"].length; i++) {
                            var orderDate = callBackResponse["listOrderDS"][i]["OrderDate"].split(" ");
                            var date = orderDate[0];
                            var time = orderDate[1];

                            var segData = {
                                seqid: i,
                                lblorderDateVal: date,
                                //callBackResponse["listOrderDS"][i]["OrderDate"],
                                lblTimeVal: time,
                                //callBackResponse["listOrderDS"][i][tranTypeHub],
                                lblAmountVal: commaFormatted(callBackResponse["listOrderDS"][i]["Amount"]),
                                lbltransVal: callBackResponse["listOrderDS"][i][tranTypeHub],
                                imgindicator: "navarrowblue.png",
                                ishidden: "yes",
                                effectiveDate: callBackResponse["listOrderDS"][i]["EffDate"],
                                unit: verifyDisplayUnit(callBackResponse["listOrderDS"][i]["Unit"]),
                                investmentVal: verifyDisplayInvestVal(callBackResponse["listOrderDS"][i]["InvestValue"]),
                                unitBal: verifyDisplayUnit(callBackResponse["listOrderDS"][i]["UnitBalance"]),
                                price: verifyDisplayUnit(callBackResponse["listOrderDS"][i]["Price"]),
                                Status: kony.i18n.getLocalizedString("Complete"),
                                channelexpval: callBackResponse["listOrderDS"][i][channelHub]

                            }
                            temp.push(segData);
                        }
                        totalPage = callBackResponse["listOrderDS"]["TotalPage"];
                        totalRecord = callBackResponse["listOrderDS"]["TotalRecord"];
                        gblMaxRecordPerPage = callBackResponse["transPerPageMB"];
                        if (!isNotBlank(gblMaxRecordPerPage)) gblMaxRecordPerPage = 15;
                        frmMFFullStatementNFundSummary.FlexdetailHeader.setVisibility(true);
                        frmMFFullStatementNFundSummary.segFullSatementData.setVisibility(true);
                        frmMFFullStatementNFundSummary.segFullSatementData.setData(temp);
                      	frmMFFullStatementNFundSummary.segFullSatementData.bottom = "-1dp";

                        gblsegmentdataMB = frmMFFullStatementNFundSummary.segFullSatementData.data;
                        frmMFFullStatementNFundSummary.lblTime.text = removeColonFromEnd(kony.i18n.getLocalizedString("TREnter_Time_01"));
                        //frmMFFullStatementNFundSummary.lblorderDate.text = removeColonFromEnd(kony.i18n.getLocalizedString("MF_lbl_Order_date"));//MKI, MIB-12159 start
                        frmMFFullStatementNFundSummary.lblorderDate.text = kony.i18n.getLocalizedString("MF_thr_TransactionDate"); //MKI, MIB-12159 End
                      	frmMFFullStatementNFundSummary.lbltrans.text = kony.i18n.getLocalizedString("MF_thr_Trans");
                        frmMFFullStatementNFundSummary.lblAmount.text = removeColonFromEnd(kony.i18n.getLocalizedString("MF_thr_Amount")) + " (" + kony.i18n.getLocalizedString("currencyThaiBaht") + ")";
                        frmMFFullStatementNFundSummary.FlexLoadingFullSatement.setVisibility(false);
                        frmMFFullStatementNFundSummary.flexLoading.setVisibility(false);
                        frmMFFullStatementNFundSummary.FlexScroll.height = "81%";
                        frmMFFullStatementNFundSummary.Flexpdf.setVisibility(true);
                    }
                }
            } catch (e) {
                alert("" + e);
            }


        } else {
            if (status == 300) {
                alert("" + kony.i18n.getLocalizedString("ECGenericError"));
            }
        }
    }
  	frmMFFullStatementNFundSummary.btnFundSummary1.setEnabled(true);
    kony.application.dismissLoadingScreen();
}


function callMutualFundsDetailsCallBackMB(status,resulttable){
  if (status == 400) {
    if (resulttable["opstatus"] == 0) {
      dismissLoadingScreen();
      gblMBMFDetailsResulttable = resulttable;
      var locale = kony.i18n.getCurrentLocale();
      
      gblMFOrder["fundTypeB"] = gblMBMFDetailsResulttable.fundTypeB;//MKI,MIB-11522
      //#ifdef android
         gblFundShort = frmMFSummary.segAccountDetails.selectedItems[0]["fundShortName"];
      	 gblSelFundNickNameEN = frmMFSummary.segAccountDetails.selectedItems[0]["lblfundNickNameEN"];
      	 gblSelFundNickNameTH = frmMFSummary.segAccountDetails.selectedItems[0]["lblfundNickNameTH"];
      	 frmMFFullStatementNFundSummary.imgAccountDetailsPic.src = frmMFSummary.segAccountDetails.selectedItems[0]["imgLogo"];

      //#else
      	gblSelFundNickNameEN = frmMFSummary.segAccountDetails.selectedRowItems[0].lblfundNickNameEN;
      	gblSelFundNickNameTH = frmMFSummary.segAccountDetails.selectedRowItems[0].lblfundNickNameTH;
     	frmMFFullStatementNFundSummary.imgAccountDetailsPic.src = frmMFSummary.segAccountDetails.selectedRowItems[0].imgLogo;
      //#endif
      
     
      if(locale == "en_US"){
        frmMFFullStatementNFundSummary.lblHdrTxt.text = gblSelFundNickNameEN;
        frmMFFullStatementNFundSummary.lblFundName.text = resulttable["FundNameEN"];
      }else{
        frmMFFullStatementNFundSummary.lblHdrTxt.text = gblSelFundNickNameTH;
        frmMFFullStatementNFundSummary.lblFundName.text = resulttable["FundNameTH"];
      }

      //frmMFFullStatementNFundSummary.lblUnitHolder.text = kony.i18n.getLocalizedString("MF_lbl_Unit_holder_no") +" "+ resulttable["UnitHolderNo"]; //mki, mib-11222
      frmMFFullStatementNFundSummary.lblUnitHolder.text = resulttable["UnitHolderNo"]; //mki, mib-11222
      frmMFFullStatementNFundSummary.lblDate.text = kony.i18n.getLocalizedString("MF_lbl_Date_as_of");
      frmMFFullStatementNFundSummary.lblDateVal.text = resulttable["DateAsOf"];
      
      frmMFFullStatementNFundSummary.lblInvestment.text = removeColonFromEnd(kony.i18n.getLocalizedString("MF_lbl_Investment_value_h2"))+ " (" + kony.i18n.getLocalizedString("currencyThaiBaht")+")";
      var amount=commaFormatted(resulttable["InvestmentValue"]);
      var decimalSplitAmt=amount.split(".");
      frmMFFullStatementNFundSummary.lblInvestmentVal.text = decimalSplitAmt[0];
      frmMFFullStatementNFundSummary.lblInvestmentValDot.text = "."+decimalSplitAmt[1];
      frmMFFullStatementNFundSummary.lblUnrelizedProfit.text =  removeColonFromEnd(kony.i18n.getLocalizedString("MF_lbl_Unreal_profit_baht"))+ " (" + kony.i18n.getLocalizedString("currencyThaiBaht")+")";
      frmMFFullStatementNFundSummary.lblUnrelizedProfitVal.text = getValueForUnrealPL(resulttable["UnrealizedProfit"]);

      frmMFFullStatementNFundSummary.lblUnrelizedProfitVal.skin = getSkinValueForUnrealPLMBBold(resulttable["UnrealizedProfit"]);

      frmMFFullStatementNFundSummary.lblUnrelizedProfitPer.text = getValueForUnrealPL(resulttable["UnrealizedProfitPerc"]) + " %";

      frmMFFullStatementNFundSummary.lblUnrelizedProfitPer.skin = getSkinValueForUnrealPLMB(resulttable["UnrealizedProfitPerc"]);
      
      frmMFFullStatementNFundSummary.lblcost.text = removeColonFromEnd(kony.i18n.getLocalizedString("MF_lbl_Cost"))+ " (" + kony.i18n.getLocalizedString("currencyThaiBaht")+")";
      frmMFFullStatementNFundSummary.lblcostVal.text = commaFormatted(resulttable["Cost"]);
      if(resulttable["UnitLTF5Y"]!=null && resulttable["UnitLTF5Y"]!=""){
        kony.print(" FlexLTF visible");
      	frmMFFullStatementNFundSummary.FlexLTF.setVisibility(true);
      	frmMFFullStatementNFundSummary.lblLTFUnit.text = verifyDisplayUnit(resulttable["UnitLTF5Y"]);
      	frmMFFullStatementNFundSummary.lblUnitval.top="0dp";
      }else{
        kony.print(" FlexLTF not visible");
      	frmMFFullStatementNFundSummary.FlexLTF.setVisibility(false);
      	frmMFFullStatementNFundSummary.lblUnitval.top="20dp";
      }
      frmMFFullStatementNFundSummary.lblNavUnit.text =  removeColonFromEnd(kony.i18n.getLocalizedString("MF_lbl_NAV_unit"));
      frmMFFullStatementNFundSummary.lblNavUnitVal.text = verifyDisplayUnit(resulttable["Nav"]);
      frmMFFullStatementNFundSummary.lblNavUnitVal.skin= "lblBlackMedium171";
      frmMFFullStatementNFundSummary.lblUnit.text = removeColonFromEnd(kony.i18n.getLocalizedString("MF_lbl_Units"));
      frmMFFullStatementNFundSummary.lblUnitval.text = verifyDisplayUnit(resulttable["Unit"]);
       getGlobalDate();

      frmMFFullStatementNFundSummary.show();
    } else {
      dismissLoadingScreen();
      alert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
      return false;
    }
  }else{
    dismissLoadingScreen();
  }
} 

function getSkinValueForUnrealPLMBBold(data){
	if(parseFloat(data) > 0){
		return lblGreenComplete;
	}else if(parseFloat(data) < 0){
		return lblRed36px;
	}else{
		return lblGray;
	}
}

function showAnimationMFLoading1() {
    frmMFFullStatementNFundSummary.loadingBtnAnim1.animate(
        kony.ui.createAnimation({
            "0": {
                "stepConfig": {
                    "timingFunction": kony.anim.LINEAR
                },
                "opacity": 0.0
            },
            "12": {
                "stepConfig": {
                    "timingFunction": kony.anim.LINEAR
                },
                "opacity": 0.42
            },
            "25": {
                "stepConfig": {
                    "timingFunction": kony.anim.LINEAR
                },
                "opacity": 0.85
            },
            "29": {
                "stepConfig": {
                    "timingFunction": kony.anim.LINEAR
                },
                "opacity": 1.0
            },
            "38": {
                "stepConfig": {
                    "timingFunction": kony.anim.LINEAR
                },
                "opacity": 0.85
            },
            "50": {
                "stepConfig": {
                    "timingFunction": kony.anim.LINEAR
                },
                "opacity": 0.42
            },
            "54": {
                "stepConfig": {
                    "timingFunction": kony.anim.LINEAR
                },
                "opacity": 0.14
            },
            "75": {
                "stepConfig": {
                    "timingFunction": kony.anim.LINEAR
                },
                "opacity": 0.0
            },
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.LINEAR
                },
                "opacity": 0.0
            }
        }), {
            "delay": 0,
            "iterationCount": 0,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 1.0
        }, {
            "animationEnd": function() {}
        }
    );
    //    }
}

function showAnimationMFLoading2() {
    frmMFFullStatementNFundSummary.loadingBtnAnim2.animate(
        kony.ui.createAnimation({
            "0": {
                "stepConfig": {
                    "timingFunction": kony.anim.LINEAR
                },
                "opacity": 0.0
            },
            "33": {
                "stepConfig": {
                    "timingFunction": kony.anim.LINEAR
                },
                "opacity": 0.14
            },
            "38": {
                "stepConfig": {
                    "timingFunction": kony.anim.LINEAR
                },
                "opacity": 0.42
            },
            "50": {
                "stepConfig": {
                    "timingFunction": kony.anim.LINEAR
                },
                "opacity": 0.85
            },
            "54": {
                "stepConfig": {
                    "timingFunction": kony.anim.LINEAR
                },
                "opacity": 1.0
            },
            "62": {
                "stepConfig": {
                    "timingFunction": kony.anim.LINEAR
                },
                "opacity": 0.85
            },
            "70": {
                "stepConfig": {
                    "timingFunction": kony.anim.LINEAR
                },
                "opacity": 0.42
            },
            "75": {
                "stepConfig": {
                    "timingFunction": kony.anim.LINEAR
                },
                "opacity": 0.14
            },
            "79": {
                "stepConfig": {
                    "timingFunction": kony.anim.LINEAR
                },
                "opacity": 0.0
            },
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.LINEAR
                },
                "opacity": 0.0
            }
        }), {
            "delay": 0,
            "iterationCount": 0,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 1.0
        }, {
            "animationEnd": function() {}
        }
    );
    //}  
}

//Form JS File
function closepopUpShowTransactionDetails(){
	//popUpShowTransactionDetails.dismiss();
  	frmMFFullStatementNFundSummary.flexShowPopUp.setVisibility(false);
}
/**
 * @function
 *
 */
function popUpShowTransactionDetails() {
  	var selectedindex=frmMFFullStatementNFundSummary.segFullSatementData.selectedIndex[1];
    //alert("selectedindex: "+selectedindex);
    var indexOfSelectedIndex = gblsegmentdataMB[selectedindex];
  	frmMFFullStatementNFundSummary.flexShowPopUp.setVisibility(true);
  	frmMFFullStatementNFundSummary.lblkey1.text=kony.i18n.getLocalizedString("MF_lbl_Order_date");
  	frmMFFullStatementNFundSummary.lblvalue1.text=indexOfSelectedIndex.lblorderDateVal;
  	frmMFFullStatementNFundSummary.lblkey2.text=kony.i18n.getLocalizedString("TREnter_Time_01")+":";
  	frmMFFullStatementNFundSummary.lblvalue2.text=indexOfSelectedIndex.lblTimeVal;
  	frmMFFullStatementNFundSummary.lblkey3.text=kony.i18n.getLocalizedString("MF_thr_TransactionDate")+":";
  	frmMFFullStatementNFundSummary.lblvalue3.text=indexOfSelectedIndex.effectiveDate;
  	frmMFFullStatementNFundSummary.lblkey4.text=kony.i18n.getLocalizedString("MF_lbl_Unit");
  	frmMFFullStatementNFundSummary.lblvalue4.text=indexOfSelectedIndex.unit;
  	frmMFFullStatementNFundSummary.lblkey6.text=kony.i18n.getLocalizedString("MF_thr_Price")+":";
  	frmMFFullStatementNFundSummary.lblvalue6.text=indexOfSelectedIndex.price;
  	frmMFFullStatementNFundSummary.lblkey7.text=kony.i18n.getLocalizedString("MF_lbl_Investment_value_h2");
  	frmMFFullStatementNFundSummary.lblvalue7.text=indexOfSelectedIndex.investmentVal;
  	frmMFFullStatementNFundSummary.lblkey8.text=kony.i18n.getLocalizedString("MF_lbl_Transaction_type");
  	frmMFFullStatementNFundSummary.lblvalue8.text=indexOfSelectedIndex.lbltransVal;
  	frmMFFullStatementNFundSummary.lblkey9.text=kony.i18n.getLocalizedString("MF_lbl_Channel");
  	frmMFFullStatementNFundSummary.lblvalue9.text=indexOfSelectedIndex.channelexpval;
  
  	frmMFFullStatementNFundSummary.FlexContainer0ba914fc1a06c42.setVisibility(false); //MKI, MIB-12159 start
    frmMFFullStatementNFundSummary.CopyFlexContainer0d8991af2d33e47.setVisibility(false);  //MKI, MIB-12159 End
  frmMFFullStatementNFundSummary.btnclose.onClick=closepopUpShowTransactionDetails;
  frmMFFullStatementNFundSummary.flexShowPopUp.onTouchEnd=closepopUpShowTransactionDetails;
}
function verifyDisplayInvestVal(data){
	if(parseFloat(data) == 0){
		 return data;
	}else{
		var value = parseFloat(data);
	    value = value.toFixed(2);
		return commaFormatted(value);
	}
}
