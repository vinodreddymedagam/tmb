gblPushData=null;
gblPushTransferRTPData=null;
var gblRecordFound = false;
function showPopUpPush(content) {
  	
  	gblofflinepush=false;
	if(arePushNotificationsEnabled())  {
     var jsonData = base64ToJson(content);
     gblPushData = jsonData;
     if (gblPushData!=null && gblPushData["moduleType"] == "RTP") {
        popUpPushNotification.lblRequestpay.text = kony.i18n.getLocalizedString("MB_RTPTittle");
        popUpPushNotification.lbldesc.text = gblPushData["desc"];
        kony.print("amount = " + gblPushData["amount"]);
        popUpPushNotification.lblamount.text = gblPushData["amount"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
        popUpPushNotification.lblDuedate.text = kony.i18n.getLocalizedString("MIB_RTPDDab") + gblPushData["RTPExpireDate"];
        var curTime = getCurrentTime();
        popUpPushNotification.lbltime.text = curTime;
        popUpPushNotification.lblrequser.text = gblPushData["reqUser"];
        //popUpPushNotification.btnreject.onClick = rejectRTPPush;
        popUpPushNotification.btnclose.onClick = closeButtonPush;
        popUpPushNotification.btnpayNow.text = kony.i18n.getLocalizedString("MIB_RTPPayBtn");
        //popUpPushNotification.btnreject.text = kony.i18n.getLocalizedString("MIB_RTPRejectBtn");
        if (gblPushData["Type"] == "RTPSC") {
            popUpPushNotification.imgRTPType.src = "icon_firend.png";
            popUpPushNotification.lblrefno.text = kony.i18n.getLocalizedString("MIB_RTPReflab") + gblPushData["refNo"];
            //popUpPushNotification.lblamount.text=commaFormatted(gblPushData["TranAmt"])+kony.i18n.getLocalizedString("currencyThaiBaht");
            popUpPushNotification.btnpayNow.onClick = payNowPush;
        } else if (gblPushData["Type"] == "RTPBP") {
            popUpPushNotification.imgRTPType.src = "icon_store.png";
            popUpPushNotification.lblrefno.text = kony.i18n.getLocalizedString("MIB_RTPReflab") + gblPushData["refNo"];
            popUpPushNotification.btnpayNow.onClick = payNowPush;

        } else {
          	popUpPushNotification.isModal=false;          	
          	popUpPushNotification.dismiss();          	
            return;
        }
       
              
       //#ifdef iphone
        if(!isSignedUser){
          try{
            kony.timer.cancel("rtptimer");
          }
          catch(e){
          }
          kony.timer.schedule("rtptimer", rtppushpopcallback, 3, false)
        }else{
          	popUpPushNotification.show();
        } 
        //#else
       	popUpPushNotification.show();       
       //#endif
       
        
    } else {      	
      	popUpPushNotification.isModal=false; 
      	popUpPushNotification.dismiss();
      	return;
    }
  }else{
     kony.print("Push Notification turn off in Device " );
  }

}

function rtppushpopcallback(){
  popUpPushNotification.isModal=false;
  popUpPushNotification.show();
}

function getCurrentTime() {
    var currentTime = "";
    var date = new Date();
    var hh = date.getHours();
    var mm = date.getMinutes();
    var ampm = "";
  
    if (hh > 12) {
        hh = hh - 12;
        if (hh < 10) {
            hh = "0" + hh;
        }
        ampm = "PM";
    } else {
        if (hh == 12) {
            ampm = "PM";
        } else {
            ampm = "AM";
        }
    }
    if (mm < 10) {
        mm = "0" + mm;
    }
   kony.print("hh " + hh);
    kony.print("mm " + mm);
    currentTime = hh + ":" + mm + ampm;
    kony.print("currentTime " + currentTime);
    return currentTime;
}
function dismissPushNotificationPopUp(){
  popUpPushNotification.dismiss();
}

function payNowPush (){
  GblBillTopFlag = true;// to navigate to billpayment on edit
  if(isSignedUser){
	showLoadingScreen();
    dismissPushNotificationPopUp();
    inquiryRTPPush();
  }else{
	gblRTPBillPayPush=true;
	gblTransferPush = true;
    var currForm=kony.application.getCurrentForm().id;
    kony.print("currForm = "+currForm);
    if(currForm=="frmMBPreLoginAccessesPin"){
      dismissPushNotificationPopUp();
    }else{
      frmMBPreLoginAccessesPin.show();
    }
  }
}

function preFillDataTransfer(){
  	makeWidgetsBelowTransferFeeButtonVisible(true);
  	frmTransferLanding.txtTranLandMyNote.text = "";
  	//frmTransferLanding.txtTranLandRecNote.text = ""; 
  	//frmTransferLanding.txtTranLandRecNote.placeholder =  kony.i18n.getLocalizedString("TREnter_PL_SNote"); 
    if(gblSelTransferMode == 2){
      setOnClickSetCanlenderBtn(true);
      displayOnUsEnterMobileNumber(true);
      displayP2PITMXBank(false);
      displayOnUsEnterBankAccount(false);	
      frmTransferLanding.txtOnUsMobileNo.setVisibility(true);
      frmTransferLanding.txtOnUsMobileNo.text = gblPushTransferRTPData["SenderProxyID"];
      frmTransferLanding.imgOnUsMob.src = "tran_smartphone.png";
      frmTransferLanding.imgOnUsMob.margin = [0,1,0,0];
    }else{
      displayOnUsEnterMobileNumber(false);
      displayOnUsEnterCitizenID(true);
      displayP2PITMXBank(false);
      displayOnUsEnterBankAccount(false);
      frmTransferLanding.txtCitizenID.setVisibility(true);
      if(gblSelTransferMode == 3){	
        frmTransferLanding.imgOnUsMob.src = "citizen_icon_id.png";
        frmTransferLanding.imgOnUsMob.margin = [0,2,0,0];
        frmTransferLanding.txtCitizenID.maxTextLength = citizen_max_text_length;
        frmTransferLanding.txtCitizenID.placeholder =   kony.i18n.getLocalizedString("MIB_P2PTREnter_CI");
      }else if(gblSelTransferMode == 4){
        frmTransferLanding.imgOnUsMob.src = "icon_tax_grey.png";
        frmTransferLanding.imgOnUsMob.margin = [0,2,0,0];
        frmTransferLanding.txtCitizenID.maxTextLength = citizen_max_text_length;
        frmTransferLanding.txtCitizenID.placeholder =   kony.i18n.getLocalizedString("MIB_P2PTREnter_Tax");
      }else if(gblSelTransferMode == 5){
        frmTransferLanding.imgOnUsMob.src = "icon_wallet_grey.png";
        frmTransferLanding.imgOnUsMob.margin = [0,2,0,0];
        frmTransferLanding.txtCitizenID.maxTextLength = ewallet_max_text_length;
        frmTransferLanding.txtCitizenID.placeholder =   kony.i18n.getLocalizedString("MIB_P2PTREnter_eWal");
      }
      frmTransferLanding.txtCitizenID.text = gblPushTransferRTPData["SenderProxyID"];
    }
    var amt = "0.00";
    if(isNotBlank(gblPushTransferRTPData["TranAmt"])){
      amt = gblPushTransferRTPData["TranAmt"];
    }
    if(isNotBlank(gblPushTransferRTPData["ToBankCd"])){
      	gblisTMB = gblPushTransferRTPData["ToBankCd"] == gblTMBBankCD;
    }
    frmTransferLanding.txtTranLandAmt.text = commaFormatted(parseFloat(removeCommaIB(amt)).toFixed(2));
    enteredAmount = frmTransferLanding.txtTranLandAmt.text;
  	gblTransferPush = true;
  	if(isNotBlank(glb_accId)){
      mobileNumberTransferNext();
    }else{
      callCheckOnUsPromptPayinqServiceMB();
      frmTransferLanding.show();
    }
}
function resetTransferRTPPush(){
    gblTransferPush = false;
  	gblRTPBillPayPush=false;
  	gblPushData = {};
  	gblPushTransferRTPData = {};
	gblRTPBillerNickName="";
  	gblRTPBillerNameFromPrompPay="";
}
function callBackPushRTP(reasonTxt){
  kony.print("reasonTxt" +JSON.stringify(reasonTxt));
  if(typeof reasonTxt != 'undefined' && typeof reasonTxt != 'boolean'){
      var inputParams={};
      inputParams["rtpTranRef"]=gblPushData["refNo"];
      inputParams["rtpStatus"]="03";
      inputParams["rtpSenderName"]=gblPushData["reqUser"];
      inputParams["reason"]=reasonTxt;
      showLoadingScreen();
      callUpdateRTPService(inputParams,cancelByBankWithReasonCallBack);
  }else{
	  gotoRTPDetail();
  }
}

function gotoRTPDetail(){
      resetTransferRTPPush();
      dismissLoadingScreen();
      popUpPushNotification.dismiss();
  	  invokeRTPDetails();
      return;
}

function gotoBPEdit(){
      resetTransferRTPPush();
      dismissLoadingScreen();
      frmBillPayment.show();
      popUpPushNotification.dismiss();
      return;
}

function callbackVerifyTransferRTPMB(status, resultTable) {
  	  var fromAccount = "";
      var amt = "";
      gblisTMB = false;
      var proxyType = "";
      if (status == 400) {
        if (resultTable["opstatus"] == 0) {
          	if(typeof resultTable["RTPActivityDS"][0] != 'undefined'){
              executeRTPTransfer(resultTable["RTPActivityDS"][0]);
            }else{
              alert(kony.i18n.getLocalizedString("ECGenericError"));
              dismissLoadingScreen();
              resetTransferRTPPush();
            }
			
        }else{
        	alert(kony.i18n.getLocalizedString("ECGenericError"));
          	dismissLoadingScreen();
          	resetTransferRTPPush();
        }
      }else{
      	alert(kony.i18n.getLocalizedString("ECGenericError"));
        dismissLoadingScreen();
        resetTransferRTPPush();
      }
}

function executeRTPTransfer(resultTable){
    gblPushTransferRTPData = resultTable;
  	var proxyType = "";
  	var fromAccount = "";
  	gblPushData["reqUser"] = resultTable["SenderAcctName"];
  	gblPushData["refNo"] = resultTable["RTPTranRef"];
    if(isNotBlank(gblPushTransferRTPData["TranAmt"])){
      amt = gblPushTransferRTPData["TranAmt"];
    }
    if(isNotBlank(gblPushTransferRTPData["SenderProxyType"])){
      proxyType = gblPushTransferRTPData["SenderProxyType"];
    }
    if(isNotBlank(gblPushTransferRTPData["ReceiverAcctNo"])){
      fromAccount = gblPushTransferRTPData["ReceiverAcctNo"];
    }
    if(proxyType == 2){
      gblSelTransferMode = 2;
    }else{
      if(proxyType == 1){
        gblSelTransferMode = 3;
      }else if(proxyType == 3){
        gblSelTransferMode = 4;
      }else if(proxyType == 4){
        gblSelTransferMode = 5;
      }
    }
    getTransferFromAccountsFromAccSmry(fromAccount);
}
function validatingRTPPushdata(contentData){  	
		gblisTMB = false;
		gblPushData = contentData;
  		gblCallPrePost = true;
		if(contentData["RTPProduct"]=="RTPBP"){  //RTPProduct==RTPBP to add for RTPTranRef
			gblRTPBillPayPush=true;
			gblTransferPush = false;
			validatingBPPushdata(contentData);
		}else{
			 gblRTPBillPayPush=false;
			gblTransferPush = true;
			executeRTPTransfer(contentData); 
		} 

}

function closeButtonPush(){
    dismissPushNotificationPopUp();
//   if(isSignedUser){
//     dismissPushNotificationPopUp();
//   }else{
//    closeApplicationBackBtn();
//   }
}

function onEditConfirmBillPayment(custAcctRec, glb_accId) {
    var fromData = []
    var j = 1
    var nonCASAAct = 0;
	 gblRecordFound = false;
    gbltranFromSelIndex = [0, 0]
try{
    for (var i = 0; i < custAcctRec.length; i++) {
        var accountStatus = custAcctRec[i].acctStatus;
        if (accountStatus.indexOf("Active") == -1) {
            nonCASAAct = nonCASAAct + 1;
            //showAlertWithCallBack(kony.i18n.getLocalizedString("MB_StatusNotEligible"), kony.i18n.getLocalizedString("info"),onclickActivationCompleteStart);
            //return false;
        }
        if (accountStatus.indexOf("Active") >= 0) {
            var icon = "";
            var iconcategory = "";
            if (custAcctRec[i].personalisedAcctStatusCode == "02") {
                continue; //do not populate if account is deleted
            }
            //icon="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+"NEW_"+custAcctRec[i]["ICON_ID"]+"&modIdentifier=PRODICON";
            icon = loadFromPalleteIcons(custAcctRec[i]["ICON_ID"]);
            kony.print("^^^^^BillPayment^^^^^" + icon);
            iconcategory = custAcctRec[i]["ICON_ID"];
            if (iconcategory == "ICON-01" || iconcategory == "ICON-02") {
                var temp = createSegmentRecordBills(custAcctRec[i], hbxSliderNew1, icon)
            } else if (iconcategory == "ICON-03") {
                var temp = createSegmentRecordBills(custAcctRec[i], hbxSliderNew2, icon)
            } else if (iconcategory == "ICON-04") {
                var temp = createSegmentRecordBills(custAcctRec[i], hbxSliderNew3, icon)

            }
            // added for removing the joint account and others for SPA and MB
            var jointActXfer = custAcctRec[i].partyAcctRelDesc;
            if (jointActXfer == "PRIJNT" || jointActXfer == "SECJNT" || jointActXfer == "OTHJNT" || jointActXfer == "SECJAN") {} else {

                kony.table.insert(fromData, temp[0])
            }
            j++;
        }

    } //end for loop
	kony.print("glb_accId  "+glb_accId);
    var noofaccount = fromData.length;
    var selectedIndex=0;
  kony.print("Setting to Seg RTP Flow "+noofaccount);
  
  if(noofaccount == 0 ){
    return;
  }
  
  for (var i = 0; i < noofaccount; i++) {
      kony.print("In LOOP  glb_accId -  "+noofaccount+"  >fromData[i].accountNo <"+fromData[i].accountNo);
      
        if (glb_accId == fromData[i].accountNo || glb_accId.indexOf(fromData[i].accountNo) >=0) {
          gblRecordFound = true;  
          selectedIndex = i;
            glb_accId = 0;
            break;
        }
    }
    if (selectedIndex != 0) {
        gbltranFromSelIndex = [0, selectedIndex];
    } else {
        gbltranFromSelIndex = [0, 0];
    }
    frmBillPayment.segSlider.widgetDataMap = [];
    frmBillPayment.segSlider.widgetDataMap = {
        lblAcntType: "lblAcntType",
        img1: "img1",
        lblCustName: "lblCustName",
        lblBalance: "lblBalance",
        lblActNoval: "lblActNoval",
        lblDummy: "lblDummy",
        lblSliderAccN2: "lblSliderAccN2",
        lblRemainFee: "lblRemainFee",
        lblRemainFeeValue: "lblRemainFeeValue"
    }
    frmBillPayment.segSlider.data = [];
    if (gblDeviceInfo["name"] == "android") {
        if (fromData.length == 1) {
            frmBillPayment.segSlider.viewConfig = {
                "coverflowConfig": {
                    "rowItemRotationAngle": 0,
                    "isCircular": false,
                    "spaceBetweenRowItems": 10,
                    "projectionAngle": 90,
                    "rowItemWidth": 80
                }
            };
        } else {
            frmBillPayment.segSlider.viewConfig = {
                "coverflowConfig": {
                    "rowItemRotationAngle": 0,
                    "isCircular": true,
                    "spaceBetweenRowItems": 10,
                    "projectionAngle": 90,
                    "rowItemWidth": 80
                }
            };
        }
    }
    frmBillPayment.segSlider.data = fromData;
    frmBillPayment.segSlider.selectedIndex = gbltranFromSelIndex;
	kony.print("selected account "+JSON.stringify(frmBillPayment.segSlider.selectedItems[0]));
    // setting values from confirmation form
    //frmBillPayment.lblAmount.text = frmBillPaymentConfirmationFuture.lblAmount.text;
    //frmBillPayment.tbxAmount.value = gblPushData["amount"];
    //frmBillPayment.lblPayBillOnValue.value = frmBillPaymentConfirmationFuture.lblPaymentDateValue.text;
    frmBillPayment.tbxMyNoteValue.text = gblPushData["Memo"];
    //disabling the set values
    frmBillPayment.lblAmount.setEnabled(false);
    frmBillPayment.tbxAmount.setEnabled(false);
    frmBillPayment.tbxBillerNickName.setEnabled(false);
//     frmBillPayment.lblPayBillOnValue.setEnabled(false);
//     frmBillPayment.hbxPayBillOn.setEnabled(false);
   // frmBillPayment.tbxMyNoteValue.setEnabled(false);
	frmBillPayment.vboxBillerName.isVisible = true;
	frmBillPayment.lblBillerNameDisplay.isVisible = true;
	frmBillPayment.lblBillerCompCodeDisplay.isVisible = true;
    //frmBillPayment.lblCompCode.text = gblCompCode;
   frmBillPayment.lblAmountValue.text = gblPushData["amount"];
    //frmBillPayment.lblRef1.text=gblCompCode;
    //frmBillPayment.lblRef1Value.isVisible = false;
	frmBillPayment.lblRef1Value.text = gblPushData["ref1"];
    frmBillPayment.lblRef1ValueMasked.isVisible=false;
    frmBillPayment.lblRef1ValueMasked.text = maskCreditCard(gblPushData["ref1"]);
    frmBillPayment.lblBillerCompCodeDisplay.text = gblCompCode;
    frmBillPayment.tbxAmount.text = gblPushData["amount"];
  if(kony.i18n.getCurrentLocale() == "th_TH"){
    frmBillPayment.lblBillerNameDisplay.text = gblBillerCompCodeTH;
    if(isNotBlank(gblPushData["ref2"])){
      frmBillPayment.lblRef2.text = gblRef2LblTH;
      frmBillPayment.tbxRef2Value.text = gblPushData["ref2"];
      frmBillPayment.hbxRef2.setVisibility(true);
    }else{
       frmBillPayment.lblRef2.text = "";
      frmBillPayment.tbxRef2Value.text = "";
      frmBillPayment.hbxRef2.setVisibility(false);
    }
  }else{
    frmBillPayment.lblBillerNameDisplay.text = gblBillerCompCodeEN;
    if(isNotBlank(gblPushData["ref2"])){
      frmBillPayment.lblRef2.text = gblRef2LblEN;
      frmBillPayment.tbxRef2Value.text = gblPushData["ref2"];
      frmBillPayment.hbxRef2.setVisibility(true);
    }else{
      frmBillPayment.lblRef2.text = "";
      frmBillPayment.tbxRef2Value.text = "";
      frmBillPayment.hbxRef2.setVisibility(false);
    }
  }

   
   }catch(e){
   	kony.print("exception "+e.message);
   } 
}


function validatingBPPushdata(RTPInqData) {
  			gblRTPBillPayPush=true;
  			gblPushData=RTPInqData;
			var inputParam={};
			 inputParam["BillerTaxID"] = RTPInqData["SenderProxyID"];
			 inputParam["ref1"] = RTPInqData["Reference1"];
			 inputParam["ref2"] = RTPInqData["Reference2"];
			 inputParam["TranAmt"] = RTPInqData["TranAmt"];
			 inputParam["RTPTranRef"] = RTPInqData["RTPTranRef"];
			 inputParam["fromAccount"] = RTPInqData["ReceiverAcctNo"];
			 inputParam["ProxyTypeValue"] = RTPInqData["ReceiverProxyType"];
			 inputParam["ProxyTypeID"] = RTPInqData["ReceiverProxyID"];
			 inputParam["prodCode"] = gblPushData["RTPProduct"];
			 inputParam["BillerGroupType"] = "0";
  			 inputParam["toAccount"] = gblPushData["SenderAcctNo"];
			 showLoadingScreen();
             invokeServiceSecureAsync("RTPBillPayValidationCompositeJavaService", inputParam, promptPayInqRTPCallBack);
		
  
}
function validatingBPPushdataonEdit() {
  			 var availBal= kony.os.toNumber(parseFloat(removeCommos(frmBillPayment["segSlider"]["selectedItems"][0].lblBalance)).toFixed(2));
             var amt=kony.os.toNumber(parseFloat(removeCommos(gblPushData["amount"])).toFixed(2));
             if(availBal<amt){
               		alert(kony.i18n.getLocalizedString("MIB_BPLessAvailBal"));
             }	
			 var inputParam={};
			 var fromActId=frmBillPayment["segSlider"]["selectedItems"][0].accountNo;
			 fromActId=fromActId.toString().replace(/-/g, "");
			 inputParam["BillerTaxID"] = gblPushData["SenderProxyID"];
			 inputParam["ref1"] = gblPushData["Reference1"];
			 inputParam["ref2"] = gblPushData["Reference2"];
			 inputParam["TranAmt"] = gblPushData["TranAmt"];
			 inputParam["RTPTranRef"] = gblPushData["RTPTranRef"];
			 inputParam["fromAccount"] = fromActId;
			 inputParam["ProxyTypeValue"] = gblPushData["ReceiverProxyType"];
			 inputParam["ProxyTypeID"] = gblPushData["ReceiverProxyID"];
			 inputParam["prodCode"] = gblPushData["RTPProduct"];
			 inputParam["BillerGroupType"] = "0";
  			 inputParam["toAccount"] = gblPushData["SenderAcctNo"];
			 showLoadingScreen();
             invokeServiceSecureAsync("RTPBillPayValidationCompositeJavaService", inputParam, promptPayInqRTPCallBackEdit);
  
}
function promptPayInqRTPCallBackEdit(status, result) {
    if (status == 400) //success responce
    {
        if (result["opstatus"] == 0) {
            if (result["customerAcctRec"] != null) {
                kony.print("success");
                dismissLoadingScreenPopup();
                //setDataonpromptPayInqRTPCallBack(result);  
                if (result["errCode"] == "errorInvalidAccount") {
                    var errorInvalidAccount = kony.i18n.getLocalizedString("MIB_BPEliFrom");
                    var payment_type = kony.i18n.getLocalizedString("MIB_RTP_BP");
                    errorInvalidAccount = errorInvalidAccount.replace("{payment_type}", payment_type);
                    showAlertWithCallBack(errorInvalidAccount, kony.i18n.getLocalizedString("info"));
                } else {
                    frmBillPaymentConfirmationFuture.show();
                }
            }
        } else {
            resetTransferRTPPush();
            dismissLoadingScreen();
            handleErrorpromptPayInqRTPCallBack(result);
        }
    } else {
        resetTransferRTPPush();
        dismissLoadingScreen();
        showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_TRErrNoDest"), kony.i18n.getLocalizedString("info"), gotoRTPDetail);
    }
}
function setDataonpromptPayInqRTPCallBack(result){
				gblPaynow = true;
                gblreccuringDisablePay="N";
                gblBillerMethod = result["billerMethod"];
                gblCompCode = result["compCode"];
                gblPayPremium = "";
                gblPolicyNumber = "";
                gblToAccountKey = result["billerTaxID"];
                gblreccuringDisableAdd=result["requiredRefNumber2Add"];
                gblreccuringDisablePay=result["requiredRefNumber2Pay"];
                gblCustomerName = ""; //result["customerAcctRec"];
                gblBillerCompCodeEN = result["billerNameEN"];
                gblBillerCompCodeTH = result["billerNameTH"];
				gblRTPBillerNickName= result["billerNickNameUser"];
  				gblRTPBillerNameFromPrompPay= result["billerNameFromPrompPay"];
                gblBillerId=result["billerId"];
                gblRef1LblEN = result["labelReferenceNumber1EN"];
                gblRef1LblTH = result["labelReferenceNumber1TH"];
             	gblRef2LblEN = result["labelReferenceNumber2EN"];
                gblRef2LblTH = result["labelReferenceNumber2TH"];
                gblPushData["ref1"] = result["ref1"];
                gblPushData["ref2"] = result["ref2"];
                gblPushData["transferRefNo"] = result["transferRefNo"];
                //gblPushData["amount"]="10090.01"
                gblPushData["amount"] = result["amount"];
                if(result["FeeAmt"]!=null || result["FeeAmt"]!=undefined){
                	gblPushData["FeeAmt"] = result["FeeAmt"];
                }else{
                	gblPushData["FeeAmt"] = "0";
                }
                glb_accId = result["fromAccount"];
  				gblPushData["paymentServerDate"] = result["paymentServerDate"];
                gblPushData["paymentServerTime"] = result["paymentServerTime"];
                kony.print("gblRef1LblEN " + gblRef1LblEN);
                kony.print("gblBillerCompCodeEN " + gblBillerCompCodeEN);
                kony.print("gblCompCode " + gblCompCode);
                kony.print("gblToAccountKey " + gblToAccountKey);
                kony.print("FeeAmt " + result["FeeAmt"]);
                kony.print("availBalance  " + result["availBalance"]); 
                kony.print("gblRTPBillerNickName  " + gblRTPBillerNickName);
				kony.print("gblRTPBillerNameFromPrompPay  " + gblRTPBillerNameFromPrompPay);
 			 	kony.print("gblRTPBillerNickName from result  " + result["billerNickNameUser"]);
  				kony.print("gblRTPBillerNameFromPrompPay from result " + result["billerNameFromPrompPay"]);
                
}

function handleErrorpromptPayInqRTPCallBack(result){
  kony.print("errocode handleErrorpromptPayInqRTPCallBack: "+result["errCode"]);
			  if (result["errCode"] == "noValidBiller") {
              showAlertWithCallBack("The biller is invalid", kony.i18n.getLocalizedString("info"), gotoRTPDetail);
            }else if (result["errCode"] == "channelLimitExceeded") {
              var remainingLimit = result["remainingLimit"];
                var BPdailylimitExceedMsg = kony.i18n.getLocalizedString("MIB_BPExcDaily_RTP");
                BPdailylimitExceedMsg = BPdailylimitExceedMsg.replace("{remaining limit}", commaFormatted(remainingLimit + ""));
                showAlertWithCallBack(BPdailylimitExceedMsg, kony.i18n.getLocalizedString("info"), gotoRTPDetail);
            }else if (result["errCode"].indexOf("B240071") != -1  || result["errCode"].indexOf("B240072") != -1 ) {
              showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_TRErrNoDest"), kony.i18n.getLocalizedString("info"), gotoRTPDetail);
            }else if (result["errCode"].indexOf("B240067") != -1 ) {
              showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_ORFTITMXservice"), kony.i18n.getLocalizedString("info"), gotoRTPDetail);
            }else if (result["errCode"].indexOf("B240063") != -1 ) {
              showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_P2PRecBankRejectAmt_RTP"), kony.i18n.getLocalizedString("info"), OverAMLOCallBack);
            }else if (result["errCode"].indexOf("B240088") != -1 ) {
              showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_P2PAccInActive"), kony.i18n.getLocalizedString("info"), InactiveAccountCallBack);
            }else {
              if (result["errCode"] != undefined && result["errCode"].indexOf("B24")!= -1 && result["errMsg"].indexOf("B24")!= -1){
                showAlertWithCallBack(result["errMsg"], kony.i18n.getLocalizedString("info"), gotoRTPDetail);
              }else if (result["errCode"] != undefined && result["errCode"].indexOf("B24")!= -1 && result["errMsg"].indexOf("B24") == -1) {
              	showAlertWithCallBack(result["errMsg"]+"("+result["errCode"]+")", kony.i18n.getLocalizedString("info"), gotoRTPDetail);
			  }else{
                showAlertWithCallBack(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"), gotoRTPDetail);
              }
            }

}
function promptPayInqRTPCallBack(status, result) {

    if (status == 400) //success responce
    {
        if (result["opstatus"] == 0) {
           if (result["customerAcctRec"] != null) {
                kony.print("success");
                dismissLoadingScreenPopup();
              setDataonpromptPayInqRTPCallBack(result);  
             var custAcctRec = result["customerAcctRec"];
             kony.print("custAcctRec  "+custAcctRec.length);
           
             
             if(custAcctRec.length == 0){
                showAlert(kony.i18n.getLocalizedString("MIB_BPErr_NoFromAcc"), kony.i18n.getLocalizedString("info"));
               return false;
             }
             
             onEditConfirmBillPayment(custAcctRec, glb_accId);
             var availBal= kony.os.toNumber(parseFloat(removeCommos(frmBillPayment["segSlider"]["selectedItems"][0].lblBalance)).toFixed(2));
             var amt=kony.os.toNumber(parseFloat(removeCommos(gblPushData["amount"])).toFixed(2));
             
            /**
            *   Added By Jagadish : Account Validation in RTP Flow	
            **/
             kony.print("Printing RTP "+custAcctRec.length);
             kony.print("glb_accId "+glb_accId);
  			 if(custAcctRec.length ==0){
               showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
               return false;
             }else if(custAcctRec.length > 1 && gblRecordFound ==false){
                 frmBillPayment.show();
                 return false;
             }

			 
             if(availBal<amt){
               alert(kony.i18n.getLocalizedString("MIB_BPLessAvailBal"));
               frmBillPayment.show();
             }else  if(result["errCode"] == "errorInvalidAccount"){
               var errorInvalidAccount = kony.i18n.getLocalizedString("MIB_BPEliFrom");
               var payment_type = kony.i18n.getLocalizedString("MIB_RTP_BP");
               errorInvalidAccount = errorInvalidAccount.replace("{payment_type}", payment_type);
               showAlertWithCallBack(errorInvalidAccount, kony.i18n.getLocalizedString("info"));
               frmBillPayment.show();
             }
             else{
               frmBillPaymentConfirmationFuture.show(); 
             }
            }else{
             	 showAlertWithCallBack(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"), gotoRTPDetail);
                 return false;
            }
        } else {
          
          	 if (result["errCode"].indexOf("B240088") == -1 ){
                 resetTransferRTPPush();
             }
            dismissLoadingScreen();
             handleErrorpromptPayInqRTPCallBack(result);
        }
    }

}
 
//RTPInq
function inquiryRTPPush(){
  if (checkMBUserStatus()) {
    var inputParams={};
    inputParams["rtpTranRef"]=gblPushData["refNo"];
    showLoadingScreen();
    invokeServiceSecureAsync("RTPInq", inputParams, inquiryRTPPushCallBack);   
  }
}

function inquiryRTPPushCallBack(status, resultTable) {  	 
      
      if (status == 400) {
        if (resultTable["opstatus"] == 0) {
			var RTPActivityDS= resultTable["RTPActivityDS"];
			kony.print("RTPActivityDS "+JSON.stringify(RTPActivityDS));
          	if(typeof RTPActivityDS[0] != 'undefined'){
		       if(RTPActivityDS[0]["RTPStatus"]=="00" || RTPActivityDS[0]["RTPStatus"]==0){				
					validatingRTPPushdata(RTPActivityDS[0]);
			   }else{
					  showAlertWithCallBack(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"), gotoRTPDetail);
					  dismissLoadingScreen();
					  resetTransferRTPPush();
			   }
			  
			               
            }else{
              showAlertWithCallBack(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"), gotoRTPDetail);
              dismissLoadingScreen();
              resetTransferRTPPush();
            }
			
        }else{
        	showAlertWithCallBack(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"), gotoRTPDetail);
          	dismissLoadingScreen();
          	resetTransferRTPPush();
        }
      }else{
      	showAlertWithCallBack(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"), gotoRTPDetail);
        dismissLoadingScreen();
        resetTransferRTPPush();
      }
}

/*
Populate data for the confirmation form. Data is taken from promptPayInq response and the payload of RTP
 */

function populateDataForConfirmation() {
    try {
        frmBillPaymentConfirmationFuture.imgFromAccount.src = loadBankIcon("11");
        frmBillPaymentConfirmationFuture.lblAccountName.text = frmBillPayment["segSlider"]["selectedItems"][0].lblCustName;
        frmBillPaymentConfirmationFuture.lblAccUserName.text=frmBillPayment["segSlider"]["selectedItems"][0].custAcctName;
      	//gblCustomerName = frmBillPayment["segSlider"]["selectedItems"][0].custAcctName; // added 
        gblProductNameEN = frmBillPayment["segSlider"]["selectedItems"][0].lblAccountNickNameEN;
        gblProductNameTH = frmBillPayment["segSlider"]["selectedItems"][0].lblAccountNickNameTH;
        frmBillPaymentConfirmationFuture.lblAccountNum.text = frmBillPayment["segSlider"]["selectedItems"][0].lblActNoval;
        frmBillPaymentConfirmationFuture.lblBalBeforePayValue.text = frmBillPayment["segSlider"]["selectedItems"][0].lblBalance;
        gblSelectedAccBal = frmBillPayment["segSlider"]["selectedItems"][0].lblBalance;
        frmBillPaymentConfirmationFuture.lblBillerNameCompCode.text="("+gblCompCode+")"
        //frmBillPaymentConfirmationFuture.lblBillerNickname.text=gblBillerCompCodeEN;
        
        if (gblPaynow && isNotBlank(frmBillPayment["segSlider"]["selectedItems"][0].lblRemainFee) && isNotBlank(frmBillPayment["segSlider"]["selectedItems"][0].lblRemainFeeValue)) {
            frmBillPaymentConfirmationFuture.lblFreeTran.text = frmBillPayment["segSlider"]["selectedItems"][0].lblRemainFee;
            frmBillPaymentConfirmationFuture.lblFreeTransValue.text = frmBillPayment["segSlider"]["selectedItems"][0].lblRemainFeeValue;
            frmBillPaymentConfirmationFuture.hbxFreeTrans.setVisibility(true);
            gblBillpaymentNoFee = true;
        } else {
            gblBillpaymentNoFee = false;
            frmBillPaymentConfirmationFuture.hbxFreeTrans.setVisibility(false)
        }
        kony.print("gblPushData pre " + JSON.stringify(gblPushData));


        frmBillPaymentConfirmationFuture.lblRef1ValueMasked.text = maskCreditCard(gblPushData["ref1"]);
        frmBillPaymentConfirmationFuture.lblRef1Value.text = gblPushData["ref1"];
        frmBillPaymentConfirmationFuture.lblAmountValue.text = commaFormatted(gblPushData["amount"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
        frmBillPaymentConfirmationFuture.lblPaymentFeeValue.text = commaFormatted(gblPushData["FeeAmt"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
        frmBillPaymentConfirmationFuture.lblPaymentFeeValueH.text = commaFormatted(gblPushData["FeeAmt"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
        frmBillPaymentConfirmationFuture.lblPaymentDateValue.text = gblPushData["paymentServerDate"] + " [" + gblPushData["paymentServerTime"] +"]";
        frmBillPaymentConfirmationFuture.hbxStartEndDate.setVisibility(false);
        frmBillPaymentConfirmationFuture.hbxRepeatExcuteTimes.setVisibility(false);
        frmBillPaymentConfirmationFuture.lblTxnNumValue.text = gblPushData["transferRefNo"];
        if (kony.i18n.getCurrentLocale() == "th_TH") {
          
             if(isNotBlank(gblRTPBillerNickName)){
                frmBillPaymentConfirmationFuture.lblBillerNickname.text = gblRTPBillerNickName;
               frmBillPaymentConfirmationFuture.lblBillerNameCompCodeShrtn.text = gblRTPBillerNickName;
               frmBillPaymentConfirmationFuture.lblBillerNameCompCode.text= gblRTPBillerNickName;
             }else{
               
               if(gblCompCode.length ==4 ){
                 gblBillerCompCodeTH = gblBillerCompCodeTH +" ("+gblCompCode+")";
                 gblBillerCompCodeEN = gblBillerCompCodeEN +" ("+gblCompCode+")";
                 frmBillPaymentConfirmationFuture.lblBillerNameCompCode.text=gblBillerCompCodeTH;
                  frmBillPaymentConfirmationFuture.lblBillerNickname.text = gblBillerCompCodeTH;
                  frmBillPaymentConfirmationFuture.lblBillerNameCompCodeShrtn.text = gblBillerCompCodeTH;
               }else{
                 gblBillerCompCodeTH = gblRTPBillerNameFromPrompPay +" ("+gblToAccountKey+")";
                 gblBillerCompCodeEN = gblRTPBillerNameFromPrompPay +" ("+gblToAccountKey+")";
                 frmBillPaymentConfirmationFuture.lblBillerNameCompCode.text=gblBillerCompCodeTH;
                   frmBillPaymentConfirmationFuture.lblBillerNickname.text = gblBillerCompCodeTH;
                   frmBillPaymentConfirmationFuture.lblBillerNameCompCodeShrtn.text = gblBillerCompCodeTH;
               }
               
             }
          
          
          

//           	frmBillPaymentConfirmationFuture.lblBillerNickname.text=gblBillerCompCodeTH;
//             frmBillPaymentConfirmationFuture.lblBillerNameCompCodeShrtn.text = gblBillerCompCodeTH;
            if (gblRef1LblTH != null) frmBillPaymentConfirmationFuture.lblRef1.text = gblRef1LblTH; //appendColon(gblRef1LblTH);
          	if(isNotBlank(gblPushData["ref2"])){
              frmBillPaymentConfirmationFuture.lblRef2.text = gblRef2LblTH; //appendColon(gblRef2LblEN);
              frmBillPaymentConfirmationFuture.lblRef2Value.text = gblPushData["ref2"]; 
              }else{
              frmBillPaymentConfirmationFuture.hbxRef2.setVisibility(false);
            }
        } else {
          	
          
             if(isNotBlank(gblRTPBillerNickName)){
                frmBillPaymentConfirmationFuture.lblBillerNickname.text = gblRTPBillerNickName;
               frmBillPaymentConfirmationFuture.lblBillerNameCompCodeShrtn.text = gblRTPBillerNickName;
             	frmBillPaymentConfirmationFuture.lblBillerNameCompCode.text=gblRTPBillerNickName;
             }else{
               
               if(gblCompCode.length ==4 ){
                 gblBillerCompCodeEN = gblBillerCompCodeEN +" ("+gblCompCode+")";
                 gblBillerCompCodeTH = gblBillerCompCodeTH +" ("+gblCompCode+")";
                  frmBillPaymentConfirmationFuture.lblBillerNickname.text = gblBillerCompCodeEN;
                  frmBillPaymentConfirmationFuture.lblBillerNameCompCodeShrtn.text =  gblBillerCompCodeEN;
                 frmBillPaymentConfirmationFuture.lblBillerNameCompCode.text= gblBillerCompCodeEN;
               }else{
                   gblBillerCompCodeEN = gblRTPBillerNameFromPrompPay +" ("+gblToAccountKey+")";
                   gblBillerCompCodeTH = gblRTPBillerNameFromPrompPay +" ("+gblToAccountKey+")";
                   frmBillPaymentConfirmationFuture.lblBillerNickname.text = gblBillerCompCodeEN;
                   frmBillPaymentConfirmationFuture.lblBillerNameCompCodeShrtn.text = gblBillerCompCodeEN;
              	   frmBillPaymentConfirmationFuture.lblBillerNameCompCode.text=gblBillerCompCodeEN;	
               }
               
             }
         
//           	frmBillPaymentConfirmationFuture.lblBillerNickname.text=gblBillerCompCodeEN;
//             frmBillPaymentConfirmationFuture.lblBillerNameCompCodeShrtn.text = gblBillerCompCodeEN;
            if (gblRef1LblEN != null) frmBillPaymentConfirmationFuture.lblRef1.text = gblRef1LblEN; //appendColon(gblRef1LblEN);
          	 if(isNotBlank(gblPushData["ref2"])){
              frmBillPaymentConfirmationFuture.lblRef2.text = gblRef2LblEN; //appendColon(gblRef2LblEN);
              frmBillPaymentConfirmationFuture.lblRef2Value.text = gblPushData["ref2"]; 
              }else{
              frmBillPaymentConfirmationFuture.hbxRef2.setVisibility(false);
            }
        }
       // if (!(LocaleController.isFormUpdatedWithLocale(frmBillPaymentConfirmationFuture.id))) {
            frmBillPaymentConfirmationFuture.lblHdrTxt.text = kony.i18n.getLocalizedString("keyConfirmation");
            frmBillPaymentConfirmationFuture.lblPayBy.text = kony.i18n.getLocalizedString("MIB_BPkeyPayFrom");
            frmBillPaymentConfirmationFuture.lblPayToNickname.text = kony.i18n.getLocalizedString("MIB_BPkeyPayTo");
            if (GblBillTopFlag) {
                frmBillPaymentConfirmationFuture.lblPayToBillerName.text = kony.i18n.getLocalizedString("MIB_BPkeyPayTo");
            } else {
                frmBillPaymentConfirmationFuture.lblPayToBillerName.text = kony.i18n.getLocalizedString("keyTopUpCompltPayTo");
            }
            frmBillPaymentConfirmationFuture.lblBalBeforePay.text = kony.i18n.getLocalizedString("keyBalanceBeforePayment");
            frmBillPaymentConfirmationFuture.lblAccNumber.text = kony.i18n.getLocalizedString("MIB_BPkeyAccountNumber");
            frmBillPaymentConfirmationFuture.lblAccName.text = kony.i18n.getLocalizedString("MIB_BPkeyAccountName");
            if (gblPaynow) {
                frmBillPaymentConfirmationFuture.lblPaymentDetails.text = kony.i18n.getLocalizedString("keyBillPaymentPaymentDetails");
            } else {
                frmBillPaymentConfirmationFuture.lblPaymentDetails.text = kony.i18n.getLocalizedString("keyBillPaymentScheduleDetails");
            }
            //frmBillPaymentConfirmationFuture.label1010748312179650.text = kony.i18n.getLocalizedString("keyEasyPassCustomerName");
            frmBillPaymentConfirmationFuture.lblAmount.text = kony.i18n.getLocalizedString("MyActivitiesIB_Amount");
            frmBillPaymentConfirmationFuture.lblfee.text = kony.i18n.getLocalizedString("keylblfee1");
            //frmBillPaymentConfirmationFuture.lblPaymentFee.text = kony.i18n.getLocalizedString("keyBillPaymentPaymentFee");
            frmBillPaymentConfirmationFuture.lblPaymentOrderDate.text = kony.i18n.getLocalizedString("keyPaymentDateTime");

            //frmBillPaymentConfirmationFuture.lblMyNote.text = kony.i18n.getLocalizedString("keyMyNote");
            frmBillPaymentConfirmationFuture.lblTxnNum.text = kony.i18n.getLocalizedString("keyTransactionRefNumber");


            frmBillPaymentConfirmationFuture.btnTransCnfrmCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
            frmBillPaymentConfirmationFuture.btnTransCnfrmConfirm.text = kony.i18n.getLocalizedString("keyConfirm");

           // LocaleController.updatedForm(frmBillPaymentConfirmationFuture.id);
        //}
        frmBillPaymentConfirmationFuture.hbxAmtValue.setVisibility(false);
        frmBillPaymentConfirmationFuture.hbxAmtInterest.setVisibility(false);
        frmBillPaymentConfirmationFuture.hbxAmtDisconnected.setVisibility(false);
        frmBillPaymentConfirmationFuture.btnHdrMenu.setEnabled(true);
        frmBillPaymentConfirmationFuture.hbxMEACustDetails.setVisibility(false);
        frmBillPaymentConfirmationFuture.hbxCustAddress.setVisibility(false);
        frmBillPaymentComplete.hbox101271281131304.setVisibility(true);
        
        gblLocale = false;
    } catch (e) {
        kony.print("Exception " + e.message);
    }
    // var personalizedId = gblPushData["personalizedId"];
    //frmBillPaymentConfirmationFuture.imgFromAccount.src = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + 11 + "&modIdentifier=BANKICON";
    frmBillPaymentConfirmationFuture.imgBillerPic.src = loadBillerIcons(gblCompCode);
    //frmBillPaymentConfirmationFuture.lblHiddenVariableForScheduleEnd.text = gblPushData["lblHiddenVariableForScheduleEnd"];


    kony.print("frmBillPaymentConfirmationFuture.lblRef1.text in confirm@@@: " + frmBillPaymentConfirmationFuture.lblRef1.text);
    kony.print("frmBillPaymentConfirmationFuture.lblRef2.text in confirm@@@: " + frmBillPaymentConfirmationFuture.lblRef2.text);
    kony.print("frmBillPaymentConfirmationFuture.hbxRef2.isVisible in confirm@@@: " + frmBillPaymentConfirmationFuture.hbxRef2.isVisible);
    kony.print("frmBillPaymentConfirmationFuture.lblAccountName in confirm@@@: " + frmBillPaymentConfirmationFuture.lblAccountName.text);
    kony.print("frmBillPaymentConfirmationFuture.lblBillerNameCompCodeShrtn.text in confirm@@@lblBillerNameCompCodeShrtn: " + frmBillPaymentConfirmationFuture.lblBillerNameCompCodeShrtn.text);
  kony.print("frmBillPaymentConfirmationFuture.lblBillerNameCompCodeShrtn.text in confirm@@@lblBillerNickname: " + frmBillPaymentConfirmationFuture.lblBillerNickname.text);
  kony.print("frmBillPaymentConfirmationFuture.lblBillerNameCompCode.text !!!"+frmBillPaymentConfirmationFuture.lblBillerNameCompCode.text);
}
function showAlertwithHandler(keyMsg,KeyTitle,yes,no,handler){

	//Defining basicConf parameter for alert
	var basicConf = {
		message: keyMsg,
		alertType: constants.ALERT_TYPE_CONFIRMATION,
		alertTitle: KeyTitle,
		yesLabel: yes,
		noLabel:no,
		alertHandler:function(response) {
                if (response == true) {
                    handler();
                } else {
						return false;                    	
                }
        }
	};
	//Defining pspConf parameter for alert
	var pspConf = {contentAlignment:constants.ALERT_CONTENT_ALIGN_CENTER};
	//Alert definition
	var infoAlert = kony.ui.Alert(basicConf, pspConf);
}

// RTPUpdate
// function rejectRTPPush(){
//  showAlertwithHandler(kony.i18n.getLocalizedString("MB_RejectConfirm"),"",kony.i18n.getLocalizedString("keyYes"),kony.i18n.getLocalizedString("RTP_cancel"),callrejectRTPPush);
// }
function callrejectRTPPush(){
  var inputParams={};
  inputParams["rtpTranRef"]=gblPushData["refNo"];
  inputParams["deviceId"]=getDeviceID();
  inputParams["rtpSenderName"]=gblPushData["reqUser"];
  
  inputParams["rtpStatus"]="05";
  showLoadingScreen();
  dismissPushNotificationPopUp();
 // callUpdateRTPService(inputParams,rejectRTPPushCallBack);
  invokeServiceSecureAsync("RTPUpdate", inputParams, rejectRTPPushCallBack);
}

function callUpdateRTPService(inputParams,callBackFunction){
  showLoadingScreen();
  invokeServiceSecureAsync("RTPUpdate", inputParams, callBackFunction);
}
function rejectRTPPushCallBack(status,resulttable){
  
  if(status==400){
    dismissLoadingScreen();		
    	if(resulttable["opstatus"]=="0" || resulttable["opstatus"]==0 ){
          kony.print("UpdateRTP success");
           closeButtonPush();
        }else{
          dismissLoadingScreen();	
        }
  }else{
    dismissLoadingScreen();		
  }
}

function cancelByBankWithReasonCallBack(status,resulttable){
    if(status==400){	
          if(resulttable["opstatus"]=="0" || resulttable["opstatus"]==0 ){
		  	gotoRTPDetail();
          }else{
            showAlertWithCallBack(kony.i18n.getLocalizedString("ECGenericError"), 
					kony.i18n.getLocalizedString("info"), gotoRTPDetail);
          }
    }else{
            showAlertWithCallBack(kony.i18n.getLocalizedString("ECGenericError"), 
					kony.i18n.getLocalizedString("info"), gotoRTPDetail);
    }
  	resetTransferRTPPush();
}

function OverAMLOCallBack(){
	callBackPushRTP("Over AMLO");
}
function InactiveAccountCallBack(){
	callBackPushRTP("Inactive Destination Account");
}